<?php

namespace App\Imports;

use App\Models\Accounting\BudgetChart;
use App\Models\Accounting\BudgetPlan;
use App\Models\Accounting\BudgetSource;
use App\Models\Accounting\OpexType;
use App\Models\Accounting\RequestForPayment;
use App\Models\Accounting\RequestForPaymentTypeReference;
use App\Models\Division;
use App\Traits\PopUpMessagesTrait;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ReqeustForPaymentImport implements ToModel, WithHeadingRow, WithValidation
{
    use PopUpMessagesTrait;

    public function model(array $row)
    {

        DB::beginTransaction();

        try {
            $division_id = null;
            $source_id = null;
            $chart_id = null;
            $budget_id = null;
            $opex_id = null;
            $total_amount = 0;

            $request_type = RequestForPaymentTypeReference::where('display', $row['rfp_type'])->first();

            // $division = Division::where('name', $row['division'])->first();

            // if ($division) {
            //     $division_id = $division->id;
            // } else if (!$division) {
            //     throw new Exception("Division not found.");
            // }

            // $opex = OpexType::where('name', $row['opex'])->first();

            // if ($opex) {
            //     $opex_id = $opex->id;
            // } else if (!$opex && $row['opex']) {
            //     throw new Exception("Opex Type not found.");
            // }

            // $source = BudgetSource::where([
            //     ['division_id', $division_id],
            //     ['name', $row['budget_source']]
            // ])->first();
            // if ($source) {
            //     $source_id = $source->id;
            // } else if (!$source && $row['budget_source']) {
            //     throw new Exception("Source not found.");
            // }

            // $chart = BudgetChart::where([
            //     ['division_id', $division_id],
            //     ['budget_source_id', $source_id],
            //     ['name', $row['budget_chart']]
            // ])->first();

            // if ($chart) {
            //     $chart_id = $chart->id;
            // } else if (!$chart && $row['budget_chart']) {
            //     throw new Exception("Chart not found.");
            // }

            // $budget_plan = BudgetPlan::where([
            //     ['division_id', $division_id],
            //     ['budget_source_id', $source_id],
            //     ['budget_chart_id', $chart_id],
            //     ['item', $row['budget_plan']]
            // ])->first();

            // if ($budget_plan) {
            //     $budget_id = $budget_plan->id;
            // } else if (!$budget_plan && $row['budget_plan']) {
            //     throw new Exception("Budget not found.");
            // }

            $total_amount = $row['total_amount'];

            // $approver_1_id = null;
            // $approver_2_id = null;
            // $approver_3_id = null;

            // if ($budget_plan->budget_plan && $budget_plan->budget_plan->budgetLoa) {
            //     $approvers = $budget_plan->budget_plan->budgetLoa()->where([
            //         ['limit_min_amount', '<=', $total_amount],
            //         ['limit_max_amount', '>=', $total_amount]
            //     ])->first();

            //     if ($approvers) {
            //         $this->approver_1_id = $approvers->first_approver_id;
            //         $this->approver_2_id = $approvers->second_approver_id;
            //         $this->approver_3_id = $approvers->third_approver_id;
            //     }
            // }

// dd($row['date_of_transaction_from'], $this->convertDate($row['date_of_transaction_from']));
            $request_for_payment = RequestForPayment::create([
                'reference_id' => $row['reference_no'],
                'type_id' => $request_type->id,
                'amount' => $total_amount,
                'date_of_transaction_from' =>  date('Y-m-d', strtotime($this->convertDate($row['date_of_transaction_from']))),
                'date_of_transaction_to' => date('Y-m-d', strtotime($this->convertDate($row['date_of_transaction_to']))),
                'budget_id' => $row['budget_id'],
                'user_id' => Auth::user()->id,
                'approver_1_id' => $row['approver_1_id'],
                'approver_2_id' => $row['approver_2_id'],
                'approver_3_id' => $row['approver_3_id'],
                'description' => $row['description'],
                'status_id' => 1,
            ]);

            $request_for_payment->requestForPaymentDetails()->create([
                'reference_id' => $row['reference_no']
            ]);

            // if ($row['status_id']) {
            //     $request_for_payment->budgetAvailments()->create([
            //         'division_id' => $request_for_payment->budget->division_id,
            //         'budget_source_id' => $request_for_payment->budget->budget_source_id,
            //         'budget_chart_id' => $request_for_payment->budget->budget_chart_id,
            //         'budget_plan_id' => $request_for_payment->budget_id,
            //         'amount' => $request_for_payment->amount,
            //         'month' => strtolower(date('F', strtotime($row['date_of_transaction']))),
            //         'year' => date('Y', strtotime($row['date_of_transaction'])),
            //     ]);
            // }

            DB::commit();
            return;
        } catch (Exception $e) {
            DB::rollback();
            $this->sweetAlertDefault('error', 'Something Went Wrong', $e->getMessage());
            return redirect()->route('accounting.request-management.index');
        }
    }



    function convertDate($dateValue) {    

        $unixDate = ($dateValue - 25569) * 86400;
        return gmdate("Y-m-d", $unixDate);
    
      
      }

    public function rules(): array
    {
        return [
            'reference_no' => 'required',
            // 'rfp_type' => 'required|exists:accounting_rfp_type_reference,display',
            // // 'division' => 'required|exists:division,name',
            // 'budget_source' => 'required|exists:accounting_budget_source,name',
            // 'budget_chart' => 'required|exists:accounting_budget_chart,name',
            'approver_1_id' => 'required|exists:users,id',
            'approver_2_id' => 'nullable|exists:users,id',
            'approver_3_id' => 'nullable|exists:users,id',
            'budget_id' => 'required|exists:accounting_budget_plan,id',
            // 'date_of_transaction' => 'required',
            'total_amount' => 'required',
            'status_id' => 'sometimes|exists:accounting_status_reference,id',
        ];
    }
}
