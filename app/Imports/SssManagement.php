<?php

namespace App\Imports;

use App\Models\Hrim\SssManagement as HrimSssManagement;
use App\Traits\PopUpMessagesTrait;
use Exception;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class SssManagement implements ToModel, WithHeadingRow, WithValidation
{
    use PopUpMessagesTrait;

    public function model(array $row)
    {
        try {

            $sss_management = HrimSssManagement::create([
                'year' => $row['year'],
                'grosspay_min' => $row['grosspay_min'],
                'grosspay_max' => $row['grosspay_max'],
                'regular_er' => $row['regular_er'],
                'regular_ee' => $row['regular_ee'],
                'compensation_er' => $row['compensation_er'],
                'compensation_ee' => $row['compensation_ee'],
                'mandatory_er' => $row['mandatory_er'],
                'mandatory_ee' => $row['mandatory_ee'],
            ]);


            DB::commit();
            return;
        } catch (Exception $e) {
            DB::rollback();
            $this->sweetAlertDefault('error', 'Something Went Wrong', $e->getMessage());
            return redirect()->route('hrim.payroll-management.sss-management.index');
        }
    }
    public function rules(): array
    {
        return [
            'year' => 'required',
            'grosspay_min' => 'required',
            'grosspay_max' => 'required',
            'regular_er' => 'required',
            'regular_ee' => 'required',
            'compensation_er' => 'required',
            'compensation_ee' => 'required',
            'mandatory_er' => 'required',
            'mandatory_ee' => 'required',
        ];
    }
}
