<?php

namespace App\Imports;

use App\Models\Accounting\BudgetChart;
use App\Models\Accounting\BudgetPlan;
use App\Models\Accounting\BudgetSource;
use App\Models\Accounting\OpexType;
use App\Models\Accounting\RequestForPaymentTypeReference;
use App\Models\Division;
use App\Traits\PopUpMessagesTrait;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

HeadingRowFormatter::default('none');

class BudgetPlanImport implements ToModel, WithHeadingRow, WithValidation
{
    use PopUpMessagesTrait;

    public function model(array $row)
    {
        try {
            $division_id = null;
            $source_id = null;
            $chart_id = null;
            $opex_id = null;
            $total_amount = 0;

            $request_type = RequestForPaymentTypeReference::where('display', $row['rfp_type'])->first();

            $division = Division::where('name', $row['division'])->first();

            if ($division) {
                $division_id = $division->id;
            } else if (!$division) {
                $division = Division::create([
                    'name' => $row['division']
                ]);
                $division_id = $division->id;
            }

            $opex = OpexType::where('name', $row['opex'])->first();

            if ($opex) {
                $opex_id = $opex->id;
            } else if (!$opex && $row['opex']) {
                $opex = OpexType::create([
                    'name' => $row['opex']
                ]);
                $opex_id = $opex->id;
            }

            $source = BudgetSource::where([
                ['division_id', $division_id],
                ['name', $row['source']]
            ])->first();

            if ($source) {
                $source_id = $source->id;
            } else if (!$source && $row['source']) {
                $source = BudgetSource::create([
                    'division_id' => $division_id,
                    'name' => $row['source']
                ]);
                $source_id = $source->id;
            }

            $chart = BudgetChart::where([
                ['division_id', $division_id],
                ['budget_source_id', $source_id],
                ['name', $row['chart']]
            ])->first();

            if ($chart) {
                $chart_id = $chart->id;
            } else if (!$chart && $row['chart']) {
                $chart = BudgetChart::create([
                    'division_id' => $division_id,
                    'budget_source_id' => $source_id,
                    'name' => $row['chart']
                ]);
                $chart_id = $chart->id;
            }

            $total_amount = $row['january'] +
                $row['february'] +
                $row['march'] +
                $row['april'] +
                $row['may'] +
                $row['june'] +
                $row['july'] +
                $row['august'] +
                $row['september'] +
                $row['october'] +
                $row['november'] +
                $row['december'];

            if ($row['item']) {
                return new BudgetPlan([
                    'rfp_type_id' => $request_type->id,
                    'division_id' => $division_id,
                    'budget_source_id' => $source_id,
                    'budget_chart_id' => $chart_id,
                    'item' => $row['item'],
                    'opex_type_id' => $opex_id,
                    'year' => date("Y"),

                    'january' => $row['january'],
                    'february' => $row['february'],
                    'march' => $row['march'],
                    'april' => $row['april'],
                    'may' => $row['may'],
                    'june' => $row['june'],
                    'july' => $row['july'],
                    'august' => $row['august'],
                    'september' => $row['september'],
                    'october' => $row['october'],
                    'november' => $row['november'],
                    'december' => $row['december'],
                    'total_amount' => $total_amount,
                ]);
            }

            return;
        } catch (\Exception $e) {
            $this->sweetAlertDefault('error', 'Something Went Wrong', $e->getMessage());
        }
    }

    public function rules(): array
    {
        return [
            'rfp_type' => 'required|exists:accounting_rfp_type_reference,display',
            'source' => 'sometimes',
            'chart' => 'sometimes',
            'item' => 'sometimes',
            'division' => 'required',

            'january' => 'sometimes|numeric',
            'february' => 'sometimes|numeric',
            'march' => 'sometimes|numeric',
            'april' => 'sometimes|numeric',
            'may' => 'sometimes|numeric',
            'june' => 'sometimes|numeric',
            'july' => 'sometimes|numeric',
            'august' => 'sometimes|numeric',
            'september' => 'sometimes|numeric',
            'october' => 'sometimes|numeric',
            'november' => 'sometimes|numeric',
            'december' => 'sometimes|numeric',
        ];
    }
}
