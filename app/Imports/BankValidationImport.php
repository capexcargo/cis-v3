<?php

namespace App\Imports;

use App\Models\Accounting\BankValidations;
use App\Models\Accounting\CashFlowDetails;
use App\Models\Accounting\CheckVoucherDetails;
use App\Traits\PopUpMessagesTrait;
use Exception;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class BankValidationImport implements ToCollection, WithCustomCsvSettings
{
    use PopUpMessagesTrait;

    public function collection(Collection $rows)
    {

        $account_number = null;
        if ($rows[7][0] == 'Account Number:') {
            $account_number = $rows[7][1];
        }

        DB::beginTransaction();
        try {
            foreach ($rows as $i => $row) {
                if ($i > 10 && $row[0] != 'Confidential Information') {
                    $bank_validation = BankValidations::create([
                        'transaction_date' => date('Y-m-d', strtotime($row[0])),
                        'check_no' => $row[3],
                        'description' => $row[4],
                        'debit_amount' => (float) str_replace(',', '', $row[5]),
                        'credit_amount' => (float) str_replace(',', '', $row[6]),
                        'balance_amount' => (float) str_replace(',', '', $row[7]),
                        'branch' => $row[8],
                        'mb_branch' => $row[8],
                        'account_no' => $account_number,
                        'payment_type_id' => ($row[3] ? 3 : 1),
                    ]);

                    $cash_flow_details = CashFlowDetails::where('check_no', $bank_validation->check_no)->first();
                    if ($cash_flow_details) {
                        $cash_flow_details->update([
                            'encashment_date' => $bank_validation->transaction_date,
                            'encashment_amount' => $bank_validation->debit_amount,
                            'status_id' => 3,
                        ]);
                    }
                }
            }

            DB::commit();
            return;
        } catch (\Exception $e) {
            DB::rollback();
            $this->sweetAlertDefault('error', 'Something Went Wrong', $e->getMessage());
            return redirect()->route('accounting.bank-validation.index');
        }
    }


    public function getCsvSettings(): array
    {
        return [
            'input_encoding' => 'ISO-8859-1',
        ];
    }
}
