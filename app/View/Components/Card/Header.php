<?php

namespace App\View\Components\Card;

use Illuminate\View\Component;

class Header extends Component
{
    public $card = [];
    public $text_color;
    public $icon_color;

    public function __construct($card = [])
    {
        $this->card = $card;
    }

    public function render()
    {
        return view('components.card.header');
    }
}
