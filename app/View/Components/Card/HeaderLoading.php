<?php

namespace App\View\Components\Card;

use Illuminate\View\Component;

class HeaderLoading extends Component
{
    public $count;
    public $icon;

    public function __construct($count = 6, $icon = true)
    {
        $this->count = $count;
        $this->icon = $icon;
    }
    
    public function render()
    {
        return view('components.card.header-loading');
    }
}
