<?php

namespace App\View\Components\Crm;

use Illuminate\View\Component;

class Form extends Component
{
    public function __construct()
    {
        //
    }

    public function render()
    {
        return view('components.crm.form');
    }
}
