<?php

namespace App\View\Components\Crm;

use Illuminate\View\Component;

class Button extends Component
{
    public $title;
    public function __construct($title)
    {
        $this->title = $title;
    }

    public function render()
    {
        return view('components.crm.button');
    }
}
