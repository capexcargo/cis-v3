<?php

namespace App\View\Components\Crm;

use Illuminate\View\Component;

class Select extends Component
{
    public $name;
    public $is_disabled;

    public function __construct($name)
    {
        $this->name = $name;
    }
    
    public function render()
    {
        return view('components.crm.select');
    }
}
