<?php

namespace App\View\Components\Transparent;

use Illuminate\View\Component;

class Textarea extends Component
{
    public $label;
    public $required;
    public function __construct($label, $required = false)
    {
        $this->label = $label;
        $this->required = $required;
    }
    
    public function render()
    {
        return view('components.transparent.textarea');
    }
}
