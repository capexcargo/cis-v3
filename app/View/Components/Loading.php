<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Loading extends Component
{
    
    public function __construct()
    {
        //
    }

    public function render()
    {
        return view('components.loading');
    }
}
