<?php

namespace App\View\Components\Table;

use Illuminate\View\Component;

class SortIcon extends Component
{
    public $field;
    public $sortField;
    public $sortAsc;
    
    public function __construct($field, $sortField, $sortAsc)
    {
        $this->field = $field;
        $this->sortField = $sortField;
        $this->sortAsc = $sortAsc;
    }
    
    public function render()
    {
        return view('components.table.sort-icon');
    }
}
