<?php

namespace App\View\Components\Table;

use Illuminate\View\Component;

class Th extends Component
{
    public $name;
    
    public function __construct($name)
    {
        $this->name = $name;
    }
    
    public function render()
    {
        return view('components.table.th');
    }
}
