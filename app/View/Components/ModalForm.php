<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ModalForm extends Component
{

    public function __construct()
    {
        //
    }

    public function render()
    {
        return view('components.modal-form');
    }
}
