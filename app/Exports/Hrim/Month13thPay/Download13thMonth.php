<?php

namespace App\Exports\Hrim\Month13thPay;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class Download13thMonth implements FromView
{
    public $export;

    public function __construct($export)
    {
        $this->export = $export;
    }

    public function view(): View
    {
        return view('exports.hrim.month-13th-pay.download-13th-month', $this->export);
    }
}
