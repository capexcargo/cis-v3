<?php

namespace App\Exports\Hrim\Month13thPay;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class Generate13thMonthBankFile implements FromView
{
    public $export;

    public function __construct($export)
    {
        $this->export = $export;
    }

    public function view(): View
    {
        return view('exports.hrim.month-13th-pay.generate-13th-month-bank-file', $this->export);
    }
}
