<?php

namespace App\Exports\Hrim\Payroll;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class Payrollgenerate implements FromView
{

    public $export;

    public function __construct($export)
    {
        $this->export = $export;
    }

    public function view(): View
    {
        return view('exports.hrim.payroll.generatepayroll', $this->export);
    }


}


// <?php

// namespace App\Exports\Admin;

// use Illuminate\Contracts\View\View;
// use Maatwebsite\Excel\Concerns\FromView;

// class AccountManagementExport implements FromView
// {
//     public $export;

//     public function __construct($export)
//     {
//         $this->export = $export;
//     }

//     public function view(): View
//     {
//         return view('exports.admin.account-management-export', $this->export);
//     }
// }