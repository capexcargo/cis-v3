<?php

namespace App\Exports\Accounting\BudgetPlan;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class BudgetPlanExport implements FromView
{
    public $export;

    public function __construct($export)
    {
        $this->export = $export;
    }

    public function view(): View
    {
        return view('exports.accounting.budget-plan.budget-plan-export', $this->export);
    }
}

// <?php

// namespace App\Exports\Admin;

// use Illuminate\Contracts\View\View;
// use Maatwebsite\Excel\Concerns\FromView;

// class AccountManagementExport implements FromView
// {
//     public $export;

//     public function __construct($export)
//     {
//         $this->export = $export;
//     }

//     public function view(): View
//     {
//         return view('exports.admin.account-management-export', $this->export);
//     }
// }
