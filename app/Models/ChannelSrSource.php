<?php

namespace App\Models;

use App\Models\Crm\CrmLead;
use App\Models\Crm\CrmServiceRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChannelSrSource extends Model
{
    use HasFactory;

    protected $table = "crm_channel_sr_source";

    protected $fillable = [
        'name',
    ];

    public function ServiceChannel()
    {
        return $this->belongsTo(CrmServiceRequest::class, 'id', 'channel_id');
    }
    
    public function leads()
    {
        return $this->hasMany(CrmLead::class, 'channel_source_id', 'id');
    }
}
