<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmActivityLogMeetingAttendees extends Model
{
    use HasFactory;

    protected $table = "crm_activity_log_a_meeting_attendees";

    protected $fillable = [
        'attendees_id',
        'meeting_id',
    ];

    public function AttendeesId()
    {
        return $this->belongsTo(User::class, 'attendees_id', 'id');
    }

    public function MeetingId()
    {
        return $this->belongsTo(CrmActivityLogMeeting::class, 'meeting_id', 'id');
    }
}
