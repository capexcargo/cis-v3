<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsPurchaseRequestAttachment extends Model
{
    use HasFactory;

    protected $table = "acctng_purchasing_purchase_req_atta";

    protected $fillable = [
        'reference_no_id',
        'purchase_req_id',
        'path',
        'name',
        'extension',
    ];
}
