<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmOpportunityStatusMgmt extends Model
{
    use HasFactory;
    protected $table = "crm_opportunity_status_reference";
    protected $fillable = [
        'name',
        'status',
    ];
}
