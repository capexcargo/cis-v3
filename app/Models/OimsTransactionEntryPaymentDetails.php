<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntryPaymentDetails extends Model
{
    use HasFactory;

    protected $table = "oims_transaction_entry_payment_details";

    protected $fillable = [
        'waybill_id',
        'is_single_waybill',
        'is_partial_payment',
        'partial_payment_status',
        'payment_type',
        'amount',
        'or/ar',
        'payor',
        'payor_ mobile_no',
    ];
}
