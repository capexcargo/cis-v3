<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Checkmobitest extends Model
{
    use HasFactory;
    protected $table = "checkmobi_column";

    protected $fillable = [
        'des',
        'number',
    ];
}
