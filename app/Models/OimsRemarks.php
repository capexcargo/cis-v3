<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsRemarks extends Model
{
    use HasFactory;
    protected $table = "oims_remarks_management";

    protected $fillable = [
        'name',
        'type_of_remarks',
        'transaction_stage_id',
        'status',
    ];

    public function Transaction()
    {
        return $this->belongsTo(OimsCargoStatusReference::class, 'transaction_stage_id', 'id');
    }
}
