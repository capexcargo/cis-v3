<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsBudgetManagement extends Model
{
    use HasFactory;

    protected $table = "acctng_budget_management";

    protected $fillable = [
        'subaccount_name',
        'budget_source_id',
        'budget_category_id',
        'budget_coa_id',
        'budget_opex_id',
        'type',
        'year',
        'jan',
        'feb',
        'mar',
        'may',
        'apr',
        'jun',
        'jul',
        'aug',
        'sep',
        'oct',
        'nov',
        'dece',
        'total_amount',
    ];

    public function budgetTrans(){
        return $this->hasMany(FimsBudgetTransfer::class, 'coa_id', 'id');
    }

    public function opex(){
        return $this->hasOne(FimsOpexCategory::class, 'id', 'budget_coa_id');
    }

    public function coa(){
        return $this->hasOne(FimsChartsOfAccounts::class, 'id', 'budget_coa_id');
    }

    public function budgetSource(){
        return $this->hasOne(FimsBudgetSource::class, 'id', 'budget_source_id');
    }
}
