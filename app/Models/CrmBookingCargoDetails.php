<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmBookingCargoDetails extends Model
{
    use HasFactory;
    protected $table = "crm_booking_cargo_details";

    protected $fillable = [
        'consignee_id',
        'quantity',
        'weight',
        'length',
        'width',
        'height',
        'size',
    ];

    public function BookingConsigneeeDetailsCD()
    {
       return $this->belongsTo(CrmBookingConsignee::class, 'consignee_id', 'id');
    }
}
