<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsEdtr extends Model
{
    use HasFactory;
    protected $table = "oims_edtr";

    protected $fillable = [
        'reference_no',
        'tra_id',
        'dispatch_date',
        'branch_id',
        'status',
        'requested_start_break',
        'requested_end_break',
        'actual_start_break',
        'actual_end_break',
        'break_approval',
        'created_by',
    ];

    public function Traref()
    {
        return $this->belongsTo(OimsTeamRouteAssignmentDetails::class, 'tra_id', 'id');
    }

    public function cbref()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function edtrDetails()
    {
        return $this->hasMany(OimsEdtrDetails::class, 'edtr_id', 'id');
    }

    public function edtrBranch()
    {
        return $this->belongsTo(OimsBranchReference::class, 'branch_id', 'id');

    }
}
