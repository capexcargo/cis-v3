<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsVehicle extends Model
{
    use HasFactory;

    protected $table = "oims_vehicle";

    protected $fillable = [
        'plate_no',
        'vehicle_type',
        'branch_id',
    ];

    public function teamDetails()
    {
        return $this->belongsTo(OimsTeamRouteAssignmentDetails::class, 'id', 'plate_no_id');
    }
}
