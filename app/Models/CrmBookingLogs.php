<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmBookingLogs extends Model
{
    use HasFactory;
    protected $table = "crm_booking_logs";

    protected $fillable = [
        'booking_id',
        'booking_reference_no',
        'status_id',
        'cancel_reason',
        'resched_reason',
        'user_id',
        'approve_user_id',
        'approve_date',
    ];

    public function BookingTypeDetailsLG()
    {
       return $this->belongsTo(CrmBooking::class, 'booking_id', 'id');
    }

    public function StatusReferenceLG()
    {
       return $this->belongsTo(CrmBookingStatusReference::class, 'status_id', 'id');
    }

    public function CancelReasonReferenceLG()
    {
       return $this->belongsTo(CrmBookingFailedReasonReference::class, 'cancel_reason', 'id');
    }

    public function ReschedReasonReferenceLG()
    {
       return $this->belongsTo(CrmBookingFailedReasonReference::class, 'resched_reason', 'id');
    }

    public function UserLogsReferenceLG()
    {
       return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function ApproverLogsReferenceLG()
    {
       return $this->belongsTo(User::class, 'approve_user_id', 'id');
    }
}
