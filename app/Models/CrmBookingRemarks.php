<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmBookingRemarks extends Model
{
    use HasFactory;
    protected $table = "crm_booking_remarks";

    protected $fillable = [
        'booking_id',
        'booking_reference_no',
        'remarks',
        'status_id',
        'user_id',
    ];

    public function BookingTypeDetailsRM()
    {
       return $this->belongsTo(CrmBooking::class, 'booking_id', 'id');
    }

    public function StatusReferenceRM()
    {
       return $this->belongsTo(CrmBookingStatusReference::class, 'status_id', 'id');
    }

    public function UserLogsReferenceRM()
    {
       return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
