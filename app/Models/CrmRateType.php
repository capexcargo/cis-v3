<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmRateType extends Model
{
    use HasFactory;

    protected $table = "crm_rate_type_reference";

    protected $fillable = [
        'name',
    ];
}
