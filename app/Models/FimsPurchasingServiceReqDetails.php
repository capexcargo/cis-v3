<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsPurchasingServiceReqDetails extends Model
{
    use HasFactory;

    protected $table = "acctng_purchasing_service_req_details";

    protected $fillable = [
        'reference_no_id',
        'expected_start_date',
        'expected_end_date',
        'service_category_id',
        'service_description_id',
        'number_of_workers',
        'man_hours',
        'estimated_rate',
        'preferred_worker',
        'purpose',
        'location_id',
        'comments',
        'budget_id',
        'approver1_id',
        'approver2_id',
        'approver3_id',
        'approver1_status',
        'approver2_status',
        'approver3_status',
        'approver1_date',
        'approver2_date',
        'approver3_date',
        'approver1_remarks',
        'approver2_remarks',
        'approver3_remarks',
    ];

    public function serviceReqDetAttachmentHasManyBK()
    {
        return $this->hasMany(FimsPurchasingServiceReqAttachment::class, 'service_req_id', 'id');
    }

    public function serviceCategory(){
        return $this->hasOne(FimsServiceCategory::class, 'id', 'service_category_id');
    }
    public function serviceDescription(){
        return $this->hasOne(FimsServiceDescription::class, 'id', 'service_description_id');
    }
    public function purpose(){
        return $this->hasOne(FimsPurpose::class, 'id', 'purpose');
    }
    public function location(){
        return $this->hasOne(OimsBranchReference::class, 'id', 'location_id');
    }
}
