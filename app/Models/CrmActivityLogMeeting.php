<?php

namespace App\Models;

use App\Models\Crm\CrmCustomerInformation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmActivityLogMeeting extends Model
{
    use HasFactory;

    protected $table = "crm_activity_log_a_meeting";

    protected $fillable = [
        'customer_id',
        'outcome_id',
        'datetime',
        'meeting_time',
        'notes',
        'sr_no',
    ];

    public function CustomerId()
    {
        return $this->belongsTo(CrmCustomerInformation::class, 'customer_id', 'id');
    }

}
