<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmBookingFailedReasonReference extends Model
{
    use HasFactory;
    protected $table = "crm_booking_failed_reason_reference";

    protected $fillable = [
        'name',
    ];

    public function ReschedReason()
    {
        return $this->belongsTo(CrmBookingLogs::class, 'id', 'resched_reason');
    }

    public function CancelledReason()
    {
        return $this->belongsTo(CrmBookingLogs::class, 'id', 'cancel_reason');
    }
}
