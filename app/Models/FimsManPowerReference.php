<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsManPowerReference extends Model
{
    use HasFactory;
    protected $table = "acctng_manpower_reference";

    protected $fillable = [
        'name',
        'email_address',
        'mobile_number',
        'tel_number',
        'agency',
        'tin',
        'status',
    ];
}
