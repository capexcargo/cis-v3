<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmRateApplyFor extends Model
{
    use HasFactory;

    protected $table = "crm_rate_apply_for_reference";

    protected $fillable = [
        'name',
    ];

    public function BookingChannel()
    {
        return $this->belongsTo(CrmBooking::class, 'id', 'booking_channel');
    }

    public function BookingChannelH()
    {
        return $this->belongsTo(CrmBookingHistory::class, 'id', 'booking_channel');
    }
}
