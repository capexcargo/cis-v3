<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmRateBoxDetails extends Model
{
    use HasFactory;

    protected $table = "crm_rate_box_details";

    protected $fillable = [
        'rate_id',
        'origin_id',
        'destination_id',
        'amount_small',
        'amount_medium',
        'amount_large',
        'is_primary',
    ];

    public function RateBoxDetails()
    {
       return $this->belongsTo(CrmRateBox::class, 'rate_id', 'id');
    }

    public function OriginDetails()
    {
       return $this->belongsTo(BranchReference::class, 'origin_id', 'id');
    }

    public function DestinationDetails()
    {
       return $this->belongsTo(BranchReference::class, 'destination_id', 'id');
    }
}
