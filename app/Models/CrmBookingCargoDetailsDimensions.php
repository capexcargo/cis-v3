<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmBookingCargoDetailsDimensions extends Model
{
    use HasFactory;
    protected $table = "crm_booking_cargo_details_dimensions";

    protected $fillable = [
        'consignee_id',
        'cargo_details_id',
        'length',
        'width',
        'height',
    ];

    public function BookingConsigneeeDetailsCDD()
    {
       return $this->belongsTo(CrmBookingConsignee::class, 'consignee_id', 'id');
    }

    public function BookingCargoDetailsCDD()
    {
       return $this->belongsTo(CrmBookingCargoDetails::class, 'cargo_details_id', 'id');
    }
}
