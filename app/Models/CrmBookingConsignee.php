<?php

namespace App\Models;

use App\Models\Crm\CrmAccountTypeReference;
use App\Models\Crm\CrmCustomerInformation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmBookingConsignee extends Model
{
    use HasFactory;
    protected $table = "crm_booking_consignee";

    protected $fillable = [
        'booking_id',
        'customer_no',
        'account_type_id',
        'company_name',
        'name',
        'first_name',
        'middle_name',
        'last_name',
        'mobile_number',
        'email_address',
        'address',
        'declared_value',
        'transposrt_mode_id',
        'service_mode_id',
        'description_goods',
        'mode_of_payment_id',
        'services_type_id',
        'charge_to_id',
        'charge_to_name',
        'state_id',
        'city_id',
        'barangay_id',
        'postal_id',
    ];

    public function BookingTypeDetailsCS()
    {
       return $this->belongsTo(CrmBooking::class, 'booking_id', 'id');
    }

    public function AccountTypeReferenceCS()
    {
       return $this->belongsTo(CrmAccountTypeReference::class, 'account_type_id', 'id');
    }

    public function TransportReferenceCD()
    {
       return $this->belongsTo(CrmTransportMode::class, 'transposrt_mode_id', 'id');
    }

    public function ServiceModeReferenceCD()
    {
       return $this->belongsTo(CrmServiceMode::class, 'service_mode_id', 'id');
    }

    public function ModeOfPaymenReferenceCD()
    {
       return $this->belongsTo(CrmModeOfPaymentReference::class, 'mode_of_payment_id', 'id');
    }

    public function BookingCargoDetailsHasManyCD()
    {
       return $this->hasMany(CrmBookingCargoDetails::class, 'consignee_id', 'id');
    }

    public function BookingChargeTo()
    {
       return $this->belongsTo(CrmCustomerInformation::class, 'charge_to_id', 'id');
    }
}
