<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolesChildAccessReference extends Model
{
    use HasFactory;

    protected $table = 'roles_child_access_reference';

    protected $fillable = [
        'roles_parent_access_id',
        'code',
        'display',
        'is_visible',
    ];
}
