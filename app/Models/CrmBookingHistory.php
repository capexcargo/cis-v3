<?php

namespace App\Models;

use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmMarketingChannel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmBookingHistory extends Model
{
    use HasFactory;
    protected $table = "crm_booking_history";

    protected $fillable = [
        'booking_reference_no',
        'booking_type_id',
        'vehicle_type_id',
        'pickup_date',
        'booking_date',
        'time_slot_id',
        'booking_category',
        'walk_in_branch_id',
        'activity_type',
        'shipper_id',
        'shipper_customer_no',
        'consignee_category',
        'consignee_count',
        'work_instruction',
        'remarks',
        'assigned_team_id',
        'final_status_id',
        'booking_branch_id',
        'marketing_channel_id',
        'booking_channel',
        'created_user',
        'approval_status',
    ];

    public function BookingTypeReferenceBK()
    {
        return $this->belongsTo(CrmBookingType::class, 'booking_type_id', 'id');
    }

    public function VehicleTypeReferenceBK()
    {
        return $this->belongsTo(CrmVehicleTypeReference::class, 'vehicle_type_id', 'id');
    }

    public function TimeslotReferenceBK()
    {
        return $this->belongsTo(CrmTimeslot::class, 'time_slot_id', 'id');
    }

    public function WalkinReferenceBK()
    {
        return $this->belongsTo(BranchReference::class, 'walk_in_branch_id', 'id');
    }

    public function ActivityReferenceBK()
    {
        return $this->belongsTo(CrmActivityType::class, 'activity_type', 'id');
    }

    public function ShipperReferenceBK()
    {
        return $this->belongsTo(CrmCustomerInformation::class, 'shipper_id', 'id');
    }

    public function FinalStatusReferenceBK()
    {
        return $this->belongsTo(CrmBookingStatusReference::class, 'final_status_id', 'id');
    }

    public function BookingBranchReferenceBK()
    {
        return $this->belongsTo(BranchReference::class, 'booking_branch_id', 'id');
    }

    public function MarketingChannelReferenceBK()
    {
        return $this->belongsTo(CrmMarketingChannel::class, 'marketing_channel_id', 'id');
    }

    public function BookingChannelReferenceBK()
    {
        return $this->belongsTo(CrmRateApplyFor::class, 'booking_channel', 'id');
    }

    public function CreatedByBK()
    {
        return $this->belongsTo(User::class, 'created_user', 'id');
    }

    public function BookingAttachmentHasManyBK()
    {
        return $this->hasMany(CrmBookingAttachment::class, 'booking_id', 'id');
    }

    public function BookingLogsHasManyBK()
    {
        return $this->hasMany(CrmBookingLogs::class, 'booking_id', 'id');
    }

    public function BookingRemarksHasManyBK()
    {
        return $this->hasMany(CrmBookingRemarks::class, 'booking_id', 'id');
    }

    public function BookingConsigneeHasManyBK()
    {
        return $this->hasMany(CrmBookingConsignee::class, 'booking_id', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(CrmBookingAttachment::class, 'booking_id', 'id');
    }

    public function BookingShipper()
    {
        return $this->hasOne(CrmBookingShipper::class, 'booking_id', 'id');
    }
}
