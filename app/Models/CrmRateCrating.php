<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmRateCrating extends Model
{
    use HasFactory;
    protected $table = "crm_rate_crating";
    protected $fillable = [
        'name',
        'effectivity_date',
        'description',
        'crating_type_id',
        'apply_for_id',
        'amount_per_cbm', 
        'ancillary_charge',
        'is_vice_versa',
        'approver1_id',
        'approver2_id',
        'approver1_status_id',
        'approver2_status_id',
        'approver1_date',
        'appover2_date',
        'final_status_id',
        'base_rate_id',
        'created_by',
    ];

    public function CrateTypeReference()
    {
       return $this->belongsTo(CrmCrateType::class, 'crating_type_id', 'id');
    }

    public function RateApplyReference()
    {
       return $this->belongsTo(CrmRateApplyFor::class, 'apply_for_id', 'id');
    }

    public function App1()
    {
       return $this->belongsTo(CrmRateLoa::class, 'approver1_id', 'id');
    }

    public function App2()
    {
       return $this->belongsTo(CrmRateLoa::class, 'approver2_id', 'id');
    }

    public function App1StatusReference()
    {
       return $this->belongsTo(CrmApproval::class, 'approver1_status_id', 'id');
    }

    public function App2StatusReference()
    {
       return $this->belongsTo(CrmApproval::class, 'approver2_status_id', 'id');
    }

    public function FinalStatusReference()
    {
       return $this->belongsTo(CrmApproval::class, 'final_status_id', 'id');
    }

    public function CreatedBy()
    {
       return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
