<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntryCustomerMobileList extends Model
{
    use HasFactory;

    protected $table = "oims_transaction_entry_customer_mobile_list";

    protected $fillable = [
        'account_id',
        'account_type',
        'mobile',
        'is_primary',
    ];
}
