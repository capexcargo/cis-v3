<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsBranchReferenceContactNo extends Model
{
    use HasFactory;

    protected $table = "oims_branch_reference_contact_no";

    protected $fillable = [
        'branch_reference_id',
        'contact_no',
    ];

    public function OBRContactBT()
    {
       return $this->belongsTo(OimsBranchReference::class, 'branch_reference_id', 'id');
    }
}
