<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsBudgetCategoryReference extends Model
{
    use HasFactory;

    protected $table = "acctng_budget_category_reference";

    protected $fillable = [
        'name',
        'status',
    ];
}
