<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransportReference extends Model
{
    use HasFactory;

    protected $table = "oims_transport_reference";

    protected $fillable = [
        'name',
        'status',
    ];
    public function SMTransportBT()
    {
       return $this->belongsTo(OimsServiceModeReference::class, 'transport_mode_id', 'id');
    }
}
