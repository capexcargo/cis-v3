<?php

namespace App\Models;

use App\Models\Crm\BarangayReference;
use App\Models\Crm\CityReference;
use App\Models\Crm\StateReference;
use Google\Service\CloudRedis\StateInfo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsSupplier extends Model
{
    use HasFactory;
    protected $table = "acctng_supplier";

    protected $fillable = [
        'name',
        'trade_name',
        'tin',
        'industry_id',
        'region_id',
        'province_id',
        'municipal_id',
        'barangay_id',
        'postal_code',
        'address',
        'first_name',
        'middle_name',
        'last_name',
        'email_address',
        'mobile_number',
        'tel_number',
        'payee_name',
        'payee_account_number',
        'bank_account_number',
        'bank_account_name',
        'bank_id',
        'status',
    ];

    public function IndSup(){
        return $this->belongsTo(FimsIndustry::class, 'industry_id', 'id');
    }

    public function RegSup(){
        return $this->belongsTo(Region::class, 'region_id', 'id');
    }

    public function ProSup(){
        return $this->belongsTo(StateReference::class, 'province_id', 'id');
    }

    public function MunSup(){
        return $this->belongsTo(CityReference::class, 'municipal_id', 'id');
    }

    public function BarSup(){
        return $this->belongsTo(BarangayReference::class, 'barangay_id', 'id');
    }

    public function BankSup(){
        return $this->belongsTo(FimsBankNameReference::class, 'bank_id', 'id');
    }
}
