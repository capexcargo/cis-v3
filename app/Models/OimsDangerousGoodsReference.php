<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsDangerousGoodsReference extends Model
{
    use HasFactory;

    protected $table = "oims_dangerous_goods_reference";

    protected $fillable = [
        'name',
        'status',
    ];
}
