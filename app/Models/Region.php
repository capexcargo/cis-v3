<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    use HasFactory;

    protected $table = "region";

    protected $fillable = [
        'name',
        'island_group_id',
    ];

    public function IslandGroupReference()
    {
        return $this->belongsTo(IslandGroup::class, 'island_group_id', 'id');
    }
}
