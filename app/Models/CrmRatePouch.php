<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmRatePouch extends Model
{
    use HasFactory;

    protected $table = "crm_rate_pouch";

    protected $fillable = [
        'name',
        'effectivity_date',
        'description',
        'rate_type_id',
        'transport_mode_id',
        'booking_type_id', 
        'apply_for_id',
        'pouch_type_id',
        'is_vice_versa',
        'approver1_id',
        'approver2_id',
        'approver1_status_id',
        'approver2_status_id',
        'approver1_date',
        'appover2_date',
        'final_status_id',
        'base_rate_id',
        'created_by',
    ];

    public function RateTypeReference()
    {
       return $this->belongsTo(CrmRateType::class, 'rate_type_id', 'id');
    }

    public function RateTransportReference()
    {
       return $this->belongsTo(CrmTransportMode::class, 'transport_mode_id', 'id');
    }

    public function BookingTypeReference()
    {
       return $this->belongsTo(CrmBookingType::class, 'booking_type_id', 'id');
    }

    public function RateApplyReference()
    {
       return $this->belongsTo(CrmRateApplyFor::class, 'apply_for_id', 'id');
    }

    public function RatePouchReference()
    {
       return $this->belongsTo(CrmPouchType::class, 'pouch_type_id', 'id');
    }

    public function PouchTypeReference()
    {
       return $this->belongsTo(CrmPouchType::class, 'pouch_type_id', 'id');
    }

    public function App1()
    {
       return $this->belongsTo(CrmRateLoa::class, 'approver1_id', 'id');
    }

    public function App2()
    {
       return $this->belongsTo(CrmRateLoa::class, 'approver2_id', 'id');
    }

    public function App1StatusReference()
    {
       return $this->belongsTo(CrmApproval::class, 'approver1_status_id', 'id');
    }

    public function App2StatusReference()
    {
       return $this->belongsTo(CrmApproval::class, 'approver2_status_id', 'id');
    }

    public function FinalStatusReference()
    {
       return $this->belongsTo(CrmApproval::class, 'final_status_id', 'id');
    }

    public function CreatedBy()
    {
       return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function RatePouchHasMany()
    {
       return $this->hasMany(CrmRatePouchDetails::class, 'pouch_id', 'id');
    }
    

}
