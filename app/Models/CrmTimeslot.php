<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmTimeslot extends Model
{
    use HasFactory;
    protected $table = "crm_timeslot";

    protected $fillable = [
        'name',
    ];
}
