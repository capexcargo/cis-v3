<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsPaymodeReference extends Model
{
    use HasFactory;

    protected $table = "oims_paymode_reference";

    protected $fillable = [
        'name',
        'description',
        'status',
    ];
}
