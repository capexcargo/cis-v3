<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmBookingStatusReference extends Model
{
    use HasFactory;
    protected $table = "crm_booking_status_reference";

    protected $fillable = [
        'name',
    ];

    public function BookingHistorystatus()
    {
        return $this->belongsTo(CrmBookingHistory::class, 'id', 'final_status_id');
    }
}
