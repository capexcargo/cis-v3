<?php

namespace App\Models;

use App\Models\Accounting\RequestForPayment;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmLead;
use App\Models\Crm\CrmOpportunity;
use App\Models\Crm\CrmServiceRequest;
use App\Models\Hrim\BenefitsManagement;
use App\Models\Hrim\BreakdownPercentage;
use App\Models\Hrim\CalendarUsers;
use App\Models\Hrim\CharacterReference;
use App\Models\Hrim\DlbManagement;
use App\Models\Hrim\Earnings;
use App\Models\Hrim\EducationalAttainment;
use App\Models\Hrim\EmercgencyContactDetails;
use App\Models\Hrim\EmploymentCategory;
use App\Models\Hrim\EmploymentRecord;
use App\Models\Hrim\EvaluatorManagement;
use App\Models\Hrim\FamilyInformation;
use App\Models\Hrim\GovernmentInformation;
use App\Models\Hrim\Leave;
use App\Models\Hrim\LoansAndLedger;
use App\Models\Hrim\Overtime;
use App\Models\Hrim\PerformanceEvaluation;
use App\Models\Hrim\ResignationAttachements;
use App\Models\Hrim\ResignationReason;
use App\Models\Hrim\ScheduleAdjustment;
use App\Models\Hrim\Tar;
use App\Models\Hrim\TimeLog;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    protected $fillable = [
        'role_id',
        'division_id',
        'level_id',
        'status_id',
        'photo_path',
        'photo_name',
        'name',
        'email',
        'email_verification_code',
        'password',
        'attempt',
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'profile_photo_url',
    ];

    public function bookingscreated()
    {
        return $this->hasMany(CrmBooking::class, 'created_user', 'id');
    }

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(AccountStatusReference::class, 'status_id', 'id');
    }

    public function serviceRequest()
    {
        return $this->belongsTo(CrmServiceRequest::class, 'id', 'assigned_to_id');
    }

    // public function breakdownPct()
    // {
    //     return $this->belongsTo(BreakdownPercentage::class, 'id', 'job_level_id');
    // }

    public function role()
    {
        return $this->hasOne(DivisionRolesReference::class, 'id', 'role_id');
    }

    public function userDetails()
    {
        return $this->hasOne(UserDetails::class, 'user_id', 'id');
    }

    public function governmentInformation()
    {
        return $this->hasOne(GovernmentInformation::class, 'user_id', 'id');
    }

    public function edutcationalAttainment()
    {
        return $this->hasOne(EducationalAttainment::class, 'user_id', 'id');
    }

    public function employmentRecord()
    {
        return $this->hasOne(EmploymentRecord::class, 'user_id', 'id');
    }

    public function characterReference()
    {
        return $this->hasOne(CharacterReference::class, 'user_id', 'id');
    }

    public function emergencyContactDetails()
    {
        return $this->hasOne(EmercgencyContactDetails::class, 'user_id', 'id');
    }

    public function familyInformation()
    {
        return $this->hasOne(FamilyInformation::class, 'user_id', 'id');
    }

    public function earnings()
    {
        return $this->hasOne(Earnings::class, 'user_id', 'id');
    }

    public function resignationReason()
    {
        return $this->hasOne(ResignationReason::class, 'user_id', 'id');
    }

    public function timeLog()
    {
        return $this->hasMany(TimeLog::class, 'user_id', 'id');
    }

    public function calendars()
    {
        return $this->hasMany(CalendarUsers::class, 'user_id', 'id');
    }

    public function tars()
    {
        return $this->hasMany(Tar::class, 'user_id', 'id');
    }

    public function leaves()
    {
        return $this->hasMany(Leave::class, 'user_id', 'id');
    }

    public function overtimes()
    {
        return $this->hasMany(Overtime::class, 'user_id', 'id');
    }

    public function scheduleAdjustments()
    {
        return $this->hasMany(ScheduleAdjustment::class, 'user_id', 'id');
    }

    public function accesses()
    {
        return $this->role->accesses->map->accesses->flatten()->pluck('code')->unique();
    }

    public function rfpFirstApprover()
    {
        return $this->hasMany(RequestForPayment::class, 'approver_1_id', 'id');
    }

    public function rfpSecondApprover()
    {
        return $this->hasMany(RequestForPayment::class, 'approver_2_id', 'id');
    }

    public function rfpThirdApprover()
    {
        return $this->hasMany(RequestForPayment::class, 'approver_3_id', 'id');
    }

    public function firstEvaluator()
    {
        return $this->hasMany(EvaluatorManagement::class, 'first_approver', 'id');
    }

    public function secondEvaluator()
    {
        return $this->hasMany(EvaluatorManagement::class, 'second_approver', 'id');
    }

    public function thirdEvaluator()
    {
        return $this->hasMany(EvaluatorManagement::class, 'third_approver', 'id');
    }

    public function performanceEvals()
    {
        return $this->hasMany(PerformanceEvaluation::class, 'user_id', 'id');
    }

    public function dlbUsers()
    {
        return $this->hasMany(DlbManagement::class, 'user_id', 'id');
    }

    public function loans()
    {
        return $this->hasMany(LoansAndLedger::class, 'user_id', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(ResignationAttachements::class, 'user_id', 'id');
    }

    public function ServiceReq()
    {
        return $this->belongsTo(CrmServiceRequest::class, 'id', 'assigned_to_id');
    }

    public function customers()
    {
        return $this->hasMany(CrmCustomerInformation::class, 'contact_owner_id', 'id');
    }
    
    public function leads()
    {
        return $this->hasMany(CrmLead::class, 'contact_owner_id', 'id');
    }
    
    public function opportunities()
    {
        return $this->hasMany(CrmOpportunity::class, 'contact_owner_id', 'id');
    }
}
