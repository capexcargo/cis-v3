<?php

namespace App\Models;

use App\Models\Crm\BarangayReference;
use App\Models\Crm\CityReference;
use App\Models\Crm\StateReference;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsServiceAreaCoverage extends Model
{
    use HasFactory;

    protected $table = "oims_service_area_coverage";

    protected $fillable = [
        'island_group_id',
        'region_id',
        'state_id',
        'city_id',
        'barangay_id',
        'zipcode',
        'port_to_cater_id',
        'serviceability_id',
        'status',
    ];

    public function Island()
    {
        return $this->belongsTo(IslandGroup::class, 'island_group_id', 'id');
    }

    public function Region()
    {
        return $this->belongsTo(Region::class, 'region_id', 'id');
    }

    public function State()
    {
        return $this->belongsTo(StateReference::class, 'state_id', 'id');
    }

    public function City()
    {
        return $this->belongsTo(CityReference::class, 'city_id', 'id');
    }

    public function Barangay()
    {
        return $this->belongsTo(BarangayReference::class, 'barangay_id', 'id');
    }

    public function Ports()
    {
        return $this->belongsTo(OimsBranchReference::class, 'port_to_cater_id', 'id');
    }

    public function Serviceability()
    {
        return $this->belongsTo(OimsServiceabilityReference::class, 'serviceability_id', 'id');
    }
}
