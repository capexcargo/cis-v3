<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolesParentAccessReference extends Model
{
    use HasFactory;

    protected $table = 'roles_parent_access_reference';

    protected $fillable = [
        'division_id',
        'code',
        'display',
        'is_visible',
    ];
    public function division()
    {
       return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function accesses()
    {
        return $this->hasMany(RolesChildAccessReference::class, 'roles_parent_access_id', 'id');
    }
}
