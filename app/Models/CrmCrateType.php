<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmCrateType extends Model
{
    use HasFactory;
    protected $table = "crm_crating_type_reference";

    protected $fillable = [
        'name',
    ];

}
