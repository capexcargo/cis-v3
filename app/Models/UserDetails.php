<?php

namespace App\Models;

use App\Models\Hrim\Agencies;
use App\Models\Hrim\BreakdownPercentage;
use App\Models\Hrim\Department;
use App\Models\Hrim\EmploymentCategory;
use App\Models\Hrim\EmploymentStatus;
use App\Models\Hrim\GenderReference;
use App\Models\Hrim\JobLevel;
use App\Models\Hrim\KpiTagging;
use App\Models\Hrim\Overtime;
use App\Models\Hrim\PerformanceEvaluation;
use App\Models\Hrim\Position;
use App\Models\Hrim\TimeLog;
use App\Models\Hrim\WorkSchedule;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    use HasFactory;

    protected $table = 'user_details';

    protected $fillable = [
        'user_id',
        'agency_id',
        'branch_id',
        'schedule_id',
        'employee_number',
        'first_name',
        'middle_name',
        'last_name',
        'position_id',
        'job_level_id',
        'department_id',
        'division_id',
        'employment_category_id',
        'employment_status_id',
        'gender_id',
        'suffix_id',
        'company_email',
        'age',
        'birth_date',
        'hired_date',
        'personal_mobile_number',
        'personal_telephone_number',
        'personal_email',
        'company_mobile_number',
        'company_telephone_number',
        'current_country_code',
        'current_region_code',
        'current_province_code',
        'current_city_code',
        'current_barangay_code',
        'current_street',
        'current_address',

        'permanent_country_code',
        'permanent_region_code',
        'permanent_province_code',
        'permanent_city_code',
        'permanent_barangay_code',
        'permanent_street',
        'permanent_address',

        'employee_code',
        'bank_account_number',
        'agency_contact_person',
        'agency_contact_no',
        'agency_type',
        'agency_position',
        'salary_grade_id',

        'erf_reference_no',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function agency()
    {
        return $this->belongsTo(Agencies::class, 'agency_id', 'id');
    }

    public function branch()
    {
        return $this->belongsTo(BranchReference::class, 'branch_id', 'id');
    }

    public function workSchedule()
    {
        return $this->belongsTo(WorkSchedule::class, 'schedule_id', 'id');
    }

    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }

    public function jobLevel()
    {
        return $this->belongsTo(JobLevel::class, 'job_level_id', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function employmentCategory()
    {
        return $this->belongsTo(EmploymentCategory::class, 'employment_category_id', 'id');
    }

    public function employmentStatus()
    {
        return $this->belongsTo(EmploymentStatus::class, 'employment_status_id', 'id');
    }

    public function gender()
    {
        return $this->belongsTo(GenderReference::class, 'gender_id', 'id');
    }

    public function suffix()
    {
        return $this->belongsTo(SuffixReference::class, 'suffix_id', 'id');
    }

    public function breakdownPct()
    {
        return $this->belongsTo(BreakdownPercentage::class, 'job_level_id', 'job_level_id');
    }

    public function kpiTagged()
    {
        return $this->hasMany(KpiTagging::class, 'position_id', 'position_id');
    }

    public function performanceEvals()
    {
        return $this->hasMany(PerformanceEvaluation::class, 'user_id', 'user_id');
    }

    public function overtime()
    {
        return $this->hasMany(Overtime::class, 'user_id', 'user_id');
    }

    public function timeLog()
    {
        return $this->hasMany(TimeLog::class, 'user_id', 'user_id');
    }

    // public function TeamRouteDetailsDriverReference()
    // {
    //     return $this->belongsTo(OimsTeamRouteAssignmentDetails::class, 'position_id', 'driver_id');
    // }


}
