<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntryForm2307 extends Model
{
    use HasFactory;

    protected $table = "oims_transaction_entry_form_2307";

    protected $fillable = [
        'waybill_id',
        'name',
        'path',
        'extension',
    ];
}
