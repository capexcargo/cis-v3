<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsReasonForRejection extends Model
{
    use HasFactory;
    protected $table = "acctng_reason_for_rejection_reference";

    protected $fillable = [
        'name',
        'status',
    ];
}
