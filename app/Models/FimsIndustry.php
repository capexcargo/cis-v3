<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsIndustry extends Model
{
    use HasFactory;
    protected $table = "acctng_industry_reference";

    protected $fillable = [
        'name',
        'status',
    ];
}
