<?php

namespace App\Models;

use App\Models\Hrim\LeaveTypeReference;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveDetails extends Model
{
    use HasFactory;

    protected $table = 'hrim_leave_details';

    protected $fillable = [
        'leave_id',
        'user_id',
        'leave_date',
        'leave_type',
        'apply_for',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function leaveType()
    {
        return $this->belongsTo(LeaveTypeReference::class, 'leave_type', 'id');
    }
}
