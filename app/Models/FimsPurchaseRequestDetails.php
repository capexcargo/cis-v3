<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsPurchaseRequestDetails extends Model
{
    use HasFactory;

    protected $table = "acctng_purchasing_purchase_req_details";

    protected $fillable = [
        'reference_no_id',
        'delivery_date',
        'is_sample_product',
        'item_category_id',
        'item_description_id',
        'qty',
        'unit',
        'estimated_rate',
        'estimated_total',
        'preferred_supplier_id',
        'purpose',
        'beneficiary_branch_id',
        'series_start_no',
        'series_end_no',
        'remarks',
        'budget_id',
        'reason_id',
        'approver1_id',
        'approver2_id',
        'approver3_id',
        'approver1_status',
        'approver2_status',
        'approver3_status',
        'approver1_date',
        'approver2_date',
        'approver3_date',
        'approver1_remarks',
        'approver2_remarks',
        'approver3_remarks',
    ];

    public function purchaseReqDetAttachmentHasManyBK()
    {
        return $this->hasMany(FimsPurchaseRequestAttachment::class, 'purchase_req_id', 'id');
    }
}
