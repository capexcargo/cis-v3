<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsZipcodeReference extends Model
{
    use HasFactory;
    protected $table = "oims_zipcode_reference";

    protected $fillable = [
        'name',
        'status',
    ];
}
