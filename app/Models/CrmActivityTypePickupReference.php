<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmActivityTypePickupReference extends Model
{
    use HasFactory;
    protected $table = "crm_activity_type_pickup_reference";

    protected $fillable = [
        'name',
        'time',
        'unit_time',
    ];
}
