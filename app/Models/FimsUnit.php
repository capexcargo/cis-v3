<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsUnit extends Model
{
    use HasFactory;
    protected $table = "acctng_unit_reference";

    protected $fillable = [
        'name',
        'status',
    ];
}
