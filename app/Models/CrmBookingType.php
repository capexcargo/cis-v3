<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmBookingType extends Model
{
    use HasFactory;

    protected $table = "crm_booking_type_reference";

    protected $fillable = [
        'name',
    ];

    public function pouchBooking()
    {
        return $this->belongsTo(CrmRatePouch::class, 'id', 'booking_type_id');
    }

    public function boxBooking()
    {
        return $this->belongsTo(CrmRateBox::class, 'id', 'booking_type_id');
    }
}
