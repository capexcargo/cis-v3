<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmTransportMode extends Model
{
    use HasFactory;

    protected $table = "crm_rate_transport_mode_reference";

    protected $fillable = [
        'name',
    ];
}
