<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmRateWarehousingDetails extends Model
{
    use HasFactory;
    protected $table = "crm_rate_warehousing_details";

    protected $fillable = [
        'rate_id',
        'warehousing_option_id',
        'amount_short_term',
        'amount_long_term',
    ];

    public function RateWarehousingDetails()
    {
        return $this->belongsTo(CrmRateWarehousing::class, 'rate_id', 'id');
    }

    public function WarehousingOptionDetails()
    {
        return $this->belongsTo(CrmWarehousingOption::class, 'warehousing_option_id', 'id');
    }
}
