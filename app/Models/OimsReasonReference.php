<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsReasonReference extends Model
{
    use HasFactory;
    protected $table = "oims_reason_reference";

    protected $fillable = [
        'name',
        'type_of_reason',
        'application_id',
        'status',
    ];

    public function Apps()
    {
        return $this->belongsTo(OimsReasonApplication::class, 'application_id', 'id');
    }
}
