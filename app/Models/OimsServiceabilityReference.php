<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsServiceabilityReference extends Model
{
    use HasFactory;

    protected $table = "oims_serviceability_reference";

    protected $fillable = [
        'name',
        'color',
        'status',
    ];
}
