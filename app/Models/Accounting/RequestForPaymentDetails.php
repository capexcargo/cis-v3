<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestForPaymentDetails extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "accounting_rfp_details";

    protected $fillable = [
        'request_for_payment_id',
        'reference_id',

        'ca_reference_no',
        'particulars',
        'plate_no',
        'labor_cost',
        'unit_cost',
        'quantity',
        'amount',

        'loaders_id',
        'freight_reference_no',
        'freight_reference_type_id',
        'soa_no',
        'trucking_type_id',
        'trucking_amount',
        'freight_amount',
        'freight_usage_id',
        'transaction_date',
        'allowance',

        'account_no',
        'payment_type_id',
        'terms_id',
        'pdc_from',
        'pdc_to',
        'pdc_date',
        'invoice',
        'invoice_amount',
    ];

    public function requestForPayment()
    {
        return $this->belongsTo(RequestForPayment::class, 'request_for_payment_id', 'id');
    }

    public function loaders()
    {
        return $this->belongsTo(Loaders::class, 'loaders_id', 'id');
    }

    public function freightReferenceType()
    {
        return $this->belongsTo(FreightReferenceType::class, 'freight_reference_type_id', 'id');
    }

    public function truckingType()
    {
        return $this->belongsTo(TruckingTypeReference::class, 'trucking_type_id', 'id');
    }

    public function freightUsage()
    {
        return $this->belongsTo(FreightUsageReference::class, 'freight_usage_id', 'id');
    }

    public function paymentType()
    {
        return $this->belongsTo(PaymentTypeReference::class, 'payment_type_id', 'id');
    }

    public function terms()
    {
        return $this->belongsTo(TermsReference::class, 'terms_id', 'id');
    }
}
