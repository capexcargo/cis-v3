<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashFlowStatusReference extends Model
{
    use HasFactory;

    protected $table = "accounting_cf_status_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];

    public function scopeActive($query)
    {
        return $query->where('is_visible', 1);
    }
    
    public function cashFlow()
    {
        return $this->hasMany(CashFlowDetails::class, 'status_id', 'id');
    }
}
