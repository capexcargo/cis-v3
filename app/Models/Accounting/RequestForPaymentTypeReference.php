<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestForPaymentTypeReference extends Model
{
    use HasFactory;

    protected $table = "accounting_rfp_type_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible'
    ];

    public function scopeActive($query)
    {
        return $query->where('is_visible', 1);
    }

    public function requestForPayments()
    {
        return $this->hasMany(RequestForPayment::class, 'type_id', 'id');
    }
}
