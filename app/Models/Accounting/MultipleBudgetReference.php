<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MultipleBudgetReference extends Model
{
    use HasFactory;

    protected $table = "accounting_multiple_budget_reference";
}
