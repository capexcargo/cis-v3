<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestForPaymentAttachments extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "accounting_rfp_attachments";

    protected $fillable = [
        'request_for_payment_id',
        'path',
        'name',
        'extension'
    ];

    public function canvassingSupplier()
    {
        return $this->belongsTo(canvassingSupplier::class, 'request_for_payment_id', 'id');
    }
}
