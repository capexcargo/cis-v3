<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CanvassingSupplierAttachments extends Model
{
    use HasFactory;

    protected $table = "accounting_cvs_supp_attachments";

    protected $fillable = [
        'canvassing_supplier_id',
        'path',
        'name',
        'extension'
    ];

    public function canvassingSupplier()
    {
        return $this->belongsTo(CanvassingSupplier::class, 'canvassing_supplier_id', 'id');
    }
}
