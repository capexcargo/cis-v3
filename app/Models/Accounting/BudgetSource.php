<?php

namespace App\Models\Accounting;

use App\Models\Division;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BudgetSource extends Model
{
    use HasFactory;

    protected $table = "accounting_budget_source";

    protected $fillable = [
        'division_id',
        'name'
    ];

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function budgetChart()
    {
        return $this->hasMany(BudgetChart::class, 'budget_source_id', 'id');
    }

    public function budgetPlan()
    {
        return $this->hasMany(BudgetPlan::class, 'budget_source_id', 'id');
    }

    public function transferBudgetFrom()
    {
        return $this->hasMany(TransferBudget::class, 'budget_source_id_from', 'id');
    }

    public function transferBudgetTo()
    {
        return $this->hasMany(TransferBudget::class, 'budget_source_id_to', 'id');
    }

    public function availment()
    {
        return $this->hasMany(BudgetAvailment::class, 'budget_source_id', 'id');
    }
}
