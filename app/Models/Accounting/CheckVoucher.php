<?php

namespace App\Models\Accounting;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckVoucher extends Model
{
    use HasFactory;

    protected $table = "accounting_check_voucher";

    protected $fillable = [
        'request_for_payment_id',
        'cv_no',
        'reference_no',
        'total_amount',
        'description',
        'status_id',
        'is_approved_1',
        'is_approved_2',
        'approver_1_id',
        'approver_2_id',
        'approver_1_date',
        'approver_2_date',
        'approver_1_remarks',
        'approver_2_remarks',
        'release_date',
        'received_by',
        'received_date',
        'received_remarks',
        'created_by',
        'schedule_release_date',

    ];

    public function requestForPayment()
    {
        return $this->belongsTo(RequestForPayment::class, 'request_for_payment_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(StatusReference::class, 'status_id', 'id');
    }

    public function firstApprover()
    {
        return $this->belongsTo(User::class, 'approver_1_id', 'id');
    }

    public function secondApprover()
    {
        return $this->belongsTo(User::class, 'approver_2_id', 'id');
    }

    public function receivedBy()
    {
        return $this->belongsTo(User::class, 'received_by', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function checkVoucherDetails()
    {
        return $this->hasMany(CheckVoucherDetails::class, 'check_voucher_id', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(CheckVoucherAttachment::class, 'check_voucher_id');
    }
}
