<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CanvassingSupplier extends Model
{
    use HasFactory;

    protected $table = "accounting_canvassing_supplier";

    protected $fillable = [
        'canvassing_id',
        'supplier_id',
        'po_reference_no',
    ];

    public function canvassing()
    {
        return $this->belongsTo(Canvassing::class, 'canvassing_id', 'id');
    }

    public function waybillseries()
    {
        return $this->hasMany(WaybillSeriesPo::class, 'cvs_reference_id', 'canvassing_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id', 'id');
    }

    public function canvassingSupplierItem()
    {
        return $this->hasMany(CanvassingSupplierItem::class, 'canvassing_supplier_id', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(CanvassingSupplierAttachments::class, 'canvassing_supplier_id', 'id');
    }

    public function requestForPayment()
    {
        return $this->hasOne(RequestForPayment::class, 'canvassing_supplier_id', 'id');
    }
}
