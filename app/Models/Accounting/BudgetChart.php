<?php

namespace App\Models\Accounting;

use App\Models\Division;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BudgetChart extends Model
{
    use HasFactory;
    
    protected $table = "accounting_budget_chart";

    protected $fillable = [
        'division_id',
        'budget_source_id',
        'name'
    ];

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function budgetSource()
    {
        return $this->belongsTo(BudgetSource::class, 'budget_source_id', 'id');
    }

    public function budgetPlan()
    {
        return $this->hasMany(BudgetPlan::class, 'budget_chart_id', 'id');
    }

    public function transferBudgetFrom()
    {
        return $this->hasMany(TransferBudget::class, 'budget_chart_id_from', 'id');
    }

    public function transferBudgetTo()
    {
        return $this->hasMany(TransferBudget::class, 'budget_chart_id_to', 'id');
    }

    public function availment()
    {
        return $this->hasMany(BudgetAvailment::class, 'budget_chart_id', 'id');
    }
}
