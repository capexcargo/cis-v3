<?php

namespace App\Models\Accounting;

use App\Models\BranchReference;
use App\Models\User;
use App\Models\UserSignature;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestForPayment extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'accounting_request_for_payment';

    protected $fillable = [
        'parent_reference_no',
        'reference_id',
        'type_id',
        'is_payee_contact_person',
        'payee_id',
        'description',
        'amount',
        'remarks',
        'multiple_budget_id',
        'user_id',
        'branch_id',
        'budget_id',
        'priority_id',
        'canvasser',
        'canvassing_supplier_id',
        'ca_no',
        'ca_reference_type_id',
        'account_type_id',
        'date_needed',
        'date_of_transaction_from',
        'date_of_transaction_to',
        'is_set_approver_1',
        'approver_1_id',
        'approver_2_id',
        'approver_3_id',
        'is_approved_1',
        'is_approved_2',
        'is_approved_3',
        'approver_date_1',
        'approver_date_2',
        'approver_date_3',
        'approver_remarks_1',
        'approver_remarks_2',
        'approver_remarks_3',
        'opex_type_id',
        'status_id',
        'subpayee',
        'is_subpayee',
    ];

    public function type()
    {
        return $this->belongsTo(RequestForPaymentTypeReference::class, 'type_id', 'id');
    }

    public function payee()
    {
        return $this->belongsTo(Supplier::class, 'payee_id', 'id');
    }

    public function multipleBudget()
    {
        return $this->belongsTo(MultipleBudgetReference::class, 'multiple_budget_id', 'id');
    }

    public function budget()
    {
        return $this->belongsTo(BudgetPlan::class, 'budget_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function branch()
    {
        return $this->belongsTo(BranchReference::class, 'branch_id', 'id');
    }

    public function priority()
    {
        return $this->belongsTo(PriorityReference::class, 'priority_id', 'id');
    }

    public function canvassingSupplier()
    {
        return $this->belongsTo(CanvassingSupplier::class, 'canvassing_supplier_id', 'id');
    }

    public function CAReferenceType()
    {
        return $this->belongsTo(CAReferenceTypes::class, 'ca_reference_type_id', 'id');
    }

    public function accountType()
    {
        return $this->belongsTo(AccountTypeReference::class, 'account_type_id', 'id');
    }

    public function approver1()
    {
        return $this->belongsTo(User::class, 'approver_1_id', 'id');
    }

    public function approver2()
    {
        return $this->belongsTo(User::class, 'approver_2_id', 'id');
    }

    public function approver3()
    {
        return $this->belongsTo(User::class, 'approver_3_id', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(RequestForPaymentAttachments::class, 'request_for_payment_id', 'id');
    }

    public function requestForPaymentDetails()
    {
        return $this->hasMany(RequestForPaymentDetails::class, 'request_for_payment_id', 'id');
    }

    public function checkVoucher()
    {
        return $this->hasOne(CheckVoucher::class, 'request_for_payment_id', 'id');
    }

    public function checkVouchers()
    {
        return $this->hasMany(CheckVoucher::class, 'request_for_payment_id', 'id');
    }

    public function opex()
    {
        return $this->belongsTo(OpexType::class, 'opex_type_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(StatusReference::class, 'status_id', 'id');
    }

    public function budgetAvailments()
    {
        return $this->hasMany(BudgetAvailment::class, 'request_for_payment_id', 'id');
    }

    public function liquidation()
    {
        return $this->hasOne(LiquidationRequest::class, 'rfp_id', 'id');
    }

    public function liquidationDetails()
    {
        return $this->hasMany(LiquidationDetailsRequest::class, 'rfp_id', 'id');
    }

    public function approver1Signature()
    {
        return $this->belongsTo(UserSignature::class, 'approver_1_id', 'user_id');
    }

    public function approver2Signature()
    {
        return $this->belongsTo(UserSignature::class, 'approver_2_id', 'user_id');
    }

    public function approver3Signature()
    {
        return $this->belongsTo(UserSignature::class, 'approver_3_id', 'user_id');
    }
}
