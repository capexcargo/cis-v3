<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentTypeReference extends Model
{
    use HasFactory;

    protected $table = "accounting_payment_type_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible'
    ];

    public function scopeActive($query)
    {
        return $query->where('is_visible', 1);
    }
}
