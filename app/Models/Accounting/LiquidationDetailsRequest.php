<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LiquidationDetailsRequest extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "accounting_liquidation_details_request";

    protected $fillable = [
        'ld_id',
        'rfp_id',
    ];

    public function liquidationDetails()
    {
        return $this->belongsTo(LiquidationDetails::class, 'ld_id', 'id');
    }

    public function requestForPayment()
    {
        return $this->belongsTo(RequestForPayment::class, 'rfp_id', 'id');
    }
}
