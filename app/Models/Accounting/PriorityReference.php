<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriorityReference extends Model
{
    use HasFactory;

    protected $table = "accounting_priority_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible'
    ];

    public function scopeActive($query)
    {
        return $query->where('is_visible', 1);
    }

    public function requestForPayment()
    {
        return $this->hasMany(RequestForPayment::class, 'priority_id', 'id');
    }
}
