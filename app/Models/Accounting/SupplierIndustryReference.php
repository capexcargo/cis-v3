<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SupplierIndustryReference extends Model
{
    use HasFactory;

    protected $table = "accounting_supplier_industry_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visibe'
    ];
}
