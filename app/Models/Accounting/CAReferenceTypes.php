<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CAReferenceTypes extends Model
{
    use HasFactory;

    protected $table = "accounting_ca_reference_types";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];
}
