<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankValidations extends Model
{
    use HasFactory;

    protected $table = "accounting_bank_validations";

    protected $fillable = [
        'transaction_date',
        'check_no',
        'description',
        'debit_amount',
        'credit_amount',
        'balance_amount',
        'branch',
        'mb_branch',
        'account_no',
        'payment_type_id',
    ];

    public function paymentType()
    {
        return $this->belongsTo(PaymentTypeReference::class, 'payment_type_id', 'id');
    }
}
