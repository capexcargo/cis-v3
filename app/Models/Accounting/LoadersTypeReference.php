<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoadersTypeReference extends Model
{
    use HasFactory;

    public $table = "accounting_loaders_type_reference";

    public $fillable = [
        'code',
        'display',
        'is_visible',
    ];

    public function scopeActive($query)
    {
        return $query->where('is_visible', 1);
    }
}
