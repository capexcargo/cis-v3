<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WaybillSeriesPo extends Model
{
    use HasFactory;

    protected $table = "accounting_po_waybill";

    protected $fillable = [
        'waybill',
        'branch_id',
        'cvs_reference_id'
    ];
}
