<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusReference extends Model
{
    use HasFactory;

    protected $table = "accounting_status_reference";

    protected $fillable = [
        'type',
        'code',
        'display',
        'is_visible'

    ];

    public function scopeRequestForPayment($query)
    {
        return $query->where('type', 1);
    }

    public function scopeCheckVoucher($query)
    {
        return $query->where('type', 2);
    }

    public function scopeActive($query)
    {
        return $query->where('is_visible', 1);
    }

    public function checkVouchers()
    {
        return $this->hasMany(CheckVoucher::class, 'status_id', 'id');
    }
}
