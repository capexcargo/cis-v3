<?php

namespace App\Models\Accounting;

use App\Models\Division;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BudgetPlan extends Model
{
    use HasFactory;

    protected $table = 'accounting_budget_plan';

    protected $fillable = [
        'rfp_type_id',
        'division_id',
        'budget_source_id',
        'budget_chart_id',
        'item',
        'opex_type_id',
        'year',

        'january',
        'february',
        'march',
        'april',
        'may',
        'june',
        'july',
        'august',
        'september',
        'october',
        'november',
        'december',
        'total_amount',
    ];

    public function requestForPaymentType()
    {
        return $this->belongsTo(RequestForPaymentTypeReference::class, 'rfp_type_id', 'id');
    }

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function source()
    {
        return $this->belongsTo(BudgetSource::class, 'budget_source_id', 'id');
    }

    public function chart()
    {
        return $this->belongsTo(BudgetChart::class, 'budget_chart_id', 'id');
    }

    public function opexType()
    {
        return $this->belongsTo(OpexType::class, 'opex_type_id', 'id');
    }

    public function transferBudgetFrom()
    {
        return $this->hasMany(TransferBudget::class, 'budget_plan_id_from', 'id');
    }

    public function transferBudgetTo()
    {
        return $this->hasMany(TransferBudget::class, 'budget_plan_id_to', 'id');
    }

    public function budgetLoa()
    {
        return $this->hasMany(BudgetLoa::class, 'budget_plan_id', 'id');
    }

    public function requestForPayment()
    {
        return $this->hasMany(RequestForPayment::class, 'budget_id', 'id');
    }

    public function availment()
    {
        return $this->hasMany(BudgetAvailment::class, 'budget_plan_id', 'id');
    }
}
