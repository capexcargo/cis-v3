<?php

namespace App\Models\Accounting;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LiquidationDetails extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "accounting_liquidation_details";

    protected $fillable = [
        'liquidation_id',
        'or_no',
        'transaction_date',
        'amount',
        'cash_on_hand',
        'description',
        'remarks',
        'created_by',
    ];

    public function liquidation()
    {
        return $this->belongsTo(Liquidation::class, 'liquidation_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(LiquidationAttachments::class, 'ld_id', 'id');
    }

    public function requestForPayments()
    {
        return $this->hasMany(LiquidationDetailsRequest::class, 'ld_id', 'id');
    }
}
