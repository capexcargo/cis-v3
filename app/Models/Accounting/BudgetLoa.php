<?php

namespace App\Models\Accounting;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BudgetLoa extends Model
{
    use HasFactory;

    protected $table = "accounting_budget_loa";

    protected $fillable = [
        'budget_plan_id',
        'limit_min_amount',
        'limit_max_amount',
        'first_approver_id',
        'second_approver_id',
        'third_approver_id',
    ];

    public function budgetPlan()
    {
        return $this->belongsTo(BudgetPlan::class, 'budget_plan_id', 'id');
    }

    public function firstApprover()
    {
        return $this->belongsTo(User::class, 'first_approver_id', 'id');
    }

    public function secondApprover()
    {
        return $this->belongsTo(User::class, 'second_approver_id', 'id');
    }

    public function thirdApprover()
    {
        return $this->belongsTo(User::class, 'third_approver_id', 'id');
    }
}
