<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckVoucherDetails extends Model
{
    use HasFactory;

    protected $table = "accounting_check_voucher_details";

    protected $fillable = [
        'check_voucher_id',
        'voucher_no',
        'description',
        'date',
        'amount'
    ];

    public function checkVoucher()
    {
        return $this->belongsTo(CheckVoucher::class, 'check_voucher_id', 'id');
    }
}
