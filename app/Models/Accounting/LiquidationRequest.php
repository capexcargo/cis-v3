<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LiquidationRequest extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "accounting_liquidation_request";

    protected $fillable = [
        'liquidation_id',
        'rfp_id',
    ];

    public function liquidation()
    {
        return $this->belongsTo(Liquidation::class, 'liquidation_id', 'id');
    }

    public function requestForPayment()
    {
        return $this->belongsTo(RequestForPayment::class, 'rfp_id', 'id');
    }
}
