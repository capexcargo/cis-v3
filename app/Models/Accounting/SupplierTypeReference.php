<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SupplierTypeReference extends Model
{
    use HasFactory;

    protected $table = "accounting_supplier_type_reference";
}
