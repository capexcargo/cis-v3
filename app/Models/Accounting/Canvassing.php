<?php

namespace App\Models\Accounting;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Canvassing extends Model
{
    use HasFactory;

    protected $table = "accounting_canvassing";

    protected $fillable = [
        'reference_id',
        'created_by'
    ];

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function canvassingSupplier()
    {
        return $this->hasMany(CanvassingSupplier::class, 'canvassing_id', 'id');
    }
}
