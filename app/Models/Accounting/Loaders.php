<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loaders extends Model
{
    use HasFactory;

    public $table = "accounting_loaders";

    public $fillable = [
        'loaders_type_id',
        'code',
        'name',
        'address',
        'is_visible',
    ];

    public function scopeActive($query)
    {
        return $query->where('is_visible', 1);
    }

    public function loaderTypeReference()
    {
        return $this->belongsTo(LoadersTypeReference::class, 'loaders_type_id', 'id');
    }
}
