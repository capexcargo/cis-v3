<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Liquidation extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "accounting_liquidation";

    protected $fillable = [
        'liquidation_reference_no',
        'liquidation_total_amount',
        'status_id',
    ];

    public function status()
    {
        return $this->belongsTo(LiquidationStatusReference::class, 'status_id', 'id');
    }

    public function requestForPayments()
    {
        return $this->hasMany(LiquidationRequest::class, 'liquidation_id', 'id');
    }

    public function liquidationDetails()
    {
        return $this->hasMany(LiquidationDetails::class, 'liquidation_id', 'id');
    }
}
