<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LiquidationAttachments extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "accounting_liquidation_attachments";

    protected $fillable = [
        'ld_id',
        'path',
        'name',
        'extension',
    ];

    public function liquidationDetails()
    {
        return $this->belongsTo(LiquidationDetails::class, 'ld_id', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(LiquidationAttachments::class, 'ld_id', 'id');
    }
}
