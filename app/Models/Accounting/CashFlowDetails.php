<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashFlowDetails extends Model
{
    use HasFactory;

    protected $table = "accounting_cash_flow_details";

    protected $fillable = [
        'encashment_date',
        'check_no',
        'cv_date',
        'payee_id',
        'check_description',
        'encashment_amount',
        'locked_amount',
        'status_id',
    ];

    public function payee()
    {
        return $this->belongsTo(Supplier::class, 'payee_id', 'id');
    }

    public function checkVoucherDetails()
    {
        return $this->belongsTo(CheckVoucherDetails::class, 'check_no', 'voucher_no');
    }

    public function status()
    {
        return $this->belongsTo(CashFlowStatusReference::class, 'status_id', 'id');
    }
}
