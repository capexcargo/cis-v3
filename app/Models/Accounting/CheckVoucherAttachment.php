<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CheckVoucherAttachment extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'accounting_check_voucher_attachments';

    protected $fillable = [
        'path',
        'name',
        'extension'
    ];

    public function checkVoucher()
    {
        return $this->belongsTo(checkVoucher::class, 'check_voucher_id');
    }
}
