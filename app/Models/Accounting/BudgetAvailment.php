<?php

namespace App\Models\Accounting;

use App\Models\Division;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BudgetAvailment extends Model
{
    use HasFactory;

    protected $table = "accounting_budget_availment";

    protected $fillable = [
        'request_for_payment_id',
        'division_id',
        'budget_source_id',
        'budget_chart_id',
        'budget_plan_id',
        'amount',
        'month',
        'year',
    ];

    public function requestForPayment()
    {
        return $this->belongsTo(RequestForPayment::class, 'request_for_payment_id', 'id');
    }

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function source()
    {
        return $this->belongsTo(BudgetSource::class, 'budget_source_id', 'id');
    }

    public function chart()
    {
        return $this->belongsTo(BudgetChart::class, 'budget_chart_id', 'id');
    }

    public function budgetPlan()
    {
        return $this->belongsTo(BudgetPlan::class, 'budget_plan_id', 'id');
    }
}
