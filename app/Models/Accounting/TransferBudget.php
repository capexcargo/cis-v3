<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransferBudget extends Model
{
    use HasFactory;

    protected $table = "accounting_transfer_budget";

    protected $fillable = [
        'division_id_from',
        'division_id_to',
        'budget_source_id_from',
        'budget_source_id_to',
        'budget_chart_id_from',
        'budget_chart_id_to',
        'budget_plan_id_from',
        'budget_plan_id_to',
        'transfer_type_id',
        'month_from',
        'month_to',
        'amount',
        'year'
    ]; 

    public function budgetPlanFrom()
    {
        return $this->belongsTo(BudgetPlan::class, 'budget_plan_id_from', 'id');
    }

    public function budgetPlanTo()
    {
        return $this->belongsTo(BudgetPlan::class, 'budget_plan_id_to', 'id');
    }

    public function transferType()
    {
        return $this->belongsTo(TransferTypeReference::class, 'transfer_type_id', 'id');
    }
    
}
