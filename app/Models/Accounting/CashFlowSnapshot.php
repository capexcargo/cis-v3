<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashFlowSnapshot extends Model
{
    use HasFactory;

    protected $table = "accounting_cash_flow_snapshot";

    protected $fillable = [
        'current_balance',
        'floating_balance',
        'year',
    ];
}
