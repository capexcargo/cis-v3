<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransferTypeReference extends Model
{
    use HasFactory;

    protected $table = "accounting_transfer_type_reference";
}
