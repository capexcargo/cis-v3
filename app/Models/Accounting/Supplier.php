<?php

namespace App\Models\Accounting;

use App\Models\BranchReference;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;

    protected $table = "accounting_supplier";

    protected $fillable = [
        'code',
        'company',
        'industry_id',
        'type_id',
        'branch_id',
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'mobile_number',
        'telephone_number',
        'address',
        'contact_person',
        'terms',
    ];

    public function industry()
    {
        return $this->belongsTo(SupplierIndustryReference::class, 'industry_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo(SupplierTypeReference::class, 'type_id', 'id');
    }

    public function branch()
    {
        return $this->belongsTo(BranchReference::class, 'branch_id', 'id');
    }

    public function items()
    {
        return $this->hasMany(SupplierItem::class, 'supplier_id', 'id');
    }

    public function payeeRequestForPayments()
    {
        return $this->hasMany(RequestForPayment::class, 'payee_id', 'id');
    }

    public function canvassingSupplierRequestForPayments()
    {
        return $this->hasMany(RequestForPayment::class, 'canvassing_supplier_id', 'id');
    }
}
