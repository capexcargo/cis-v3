<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OpexType extends Model
{
    use HasFactory;

    protected $table = "accounting_opex_type";

    protected $fillable = [
        'name',
        'description'
    ];
}
