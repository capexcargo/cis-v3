<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CanvassingSupplierItem extends Model
{
    use HasFactory;

    protected $table = "accounting_cvs_supplier_item";

    protected $fillable = [
        'canvassing_supplier_id',
        'supplier_item_id',
        'unit_cost',
        'quantity',
        'total_amount',
        'recommended_status',
        'final_status',
        'waybill_from',
        'waybill_to',
        'waybill_identify'
        
    ];

    public function canvassingSupplier()
    {
        return $this->belongsTo(Canvassing::class, 'canvassing_supplier_id', 'id');
    }

    public function supplierItem()
    {
        return $this->belongsTo(SupplierItem::class, 'supplier_item_id', 'id');
    }
}
