<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsPackagingReference extends Model
{
    use HasFactory;
    
    protected $table = "oims_type_of_packaging_reference";

    protected $fillable = [
        'name',
        'status',
    ];
}
