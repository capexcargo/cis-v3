<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmQualificationMgmt extends Model
{
    use HasFactory;

    protected $table = "crm_qualification_question_management";
    protected $fillable = [
        'question',
        'qualification_type',
    ];
}
