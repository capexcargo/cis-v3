<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsBranchReferenceTelephone extends Model
{
    use HasFactory;

    protected $table = "oims_branch_reference_telephone";

    protected $fillable = [
        'branch_reference_id',
        'telephone',
    ];

    public function OBRTelephoneBT()
    {
       return $this->belongsTo(OimsBranchReference::class, 'branch_reference_id', 'id');
    }
}
