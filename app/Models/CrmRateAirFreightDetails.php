<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmRateAirFreightDetails extends Model
{
    use HasFactory;
    protected $table = "crm_rate_airfreight_details";

    protected $fillable = [
        'rate_id',
        'origin_id',
        'destination_id',
        'amount_weight_1',
        'amount_weight_2',
        'amount_weight_3',
        'amount_weight_4',
        'amount_weight_5',
        'is_primary',
    ];

    public function RateAirFreightDetails()
    {
       return $this->belongsTo(CrmRateAirFreight::class, 'rate_id', 'id');
    }

    public function OriginDetails()
    {
       return $this->belongsTo(BranchReference::class, 'origin_id', 'id');
    }

    public function DestinationDetails()
    {
       return $this->belongsTo(BranchReference::class, 'destination_id', 'id');
    }
}

