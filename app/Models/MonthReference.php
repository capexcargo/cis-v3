<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MonthReference extends Model
{
    use HasFactory;

    protected $table = "month_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];
}
