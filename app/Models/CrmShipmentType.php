<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmShipmentType extends Model
{
    use HasFactory;
    protected $table = "crm_shipment_type";

    protected $fillable = [
        'name',
        'type',
    ];
}
