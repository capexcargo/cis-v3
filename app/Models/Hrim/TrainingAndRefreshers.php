<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrainingAndRefreshers extends Model
{

    use HasFactory, SoftDeletes;

    protected $table = "training_and_refreshers";

    protected $fillable = [
        'title',
        'description',
        'video_ref',
        'is_tag_all_position',
        'tagged_division',
        'position',
    ];

    public function taggedPosition()
    {
        return $this->belongsTo(Position::class, 'position', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(TrainingAndRefreshersAttachment::class, 'video_ref', 'video_ref');
    }
}
