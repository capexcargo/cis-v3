<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BreakdownPercentage extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_performance_breakdown_pct";

    
    protected $fillable = [
        'job_level_id',
        'goals',
        'core_values',
        'leadership_competencies',
    ];

    public function job_level()
    {
        return $this->belongsTo(JobLevel::class, 'job_level_id', 'id');
    }
}
