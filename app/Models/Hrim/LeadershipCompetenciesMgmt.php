<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeadershipCompetenciesMgmt extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_leadership_competencies";
    
    protected $fillable = [
        'description',
    ];
}
