<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DisciplinaryAttachments extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_disciplinary_attachments";

    protected $fillable = [
        'disciplinary_id',
        'path',
        'name',
        'extension',
    ];
    
    public function disciplinary()
    {
        return $this->belongsTo(DisciplinaryHistory::class, 'disciplinary_id', 'id');
    }

}
