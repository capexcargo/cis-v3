<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SssManagement extends Model
{
    use HasFactory;

    protected $table = "hrim_sss";

    protected $fillable = [
        'year',
        'grosspay_min',
        'grosspay_max',
        'regular_er',
        'regular_ee',
        'compensation_er',
        'compensation_ee',
        'mandatory_er',
        'mandatory_ee'
    ];
}
