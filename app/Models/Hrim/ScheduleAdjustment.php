<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScheduleAdjustment extends Model
{
    use HasFactory;

    protected $table = "hrim_schedule_adjustment";

    protected $fillable = [
        'user_id',
        'current_work_schedule_id',
        'new_work_schedule_id',
        'inclusive_date_from',
        'inclusive_date_to',
        'reason',
        'first_approver',
        'second_approver',
        'third_approver',
        'admin_approver',
        'first_status',
        'second_status',
        'third_status',
        'admin_status',
        'final_status_id',
        'first_approval_date',
        'second_approval_date',
        'third_approval_date',
        'admin_approval_date',
        'first_approver_remarks',
        'second_approver_remarks',
        'third_approver_remarks',
        'admin_approver_remarks',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function currentWorkSchedule()
    {
        return $this->belongsTo(WorkSchedule::class, 'current_work_schedule_id', 'id');
    }

    public function newWorkSchedule()
    {
        return $this->belongsTo(WorkSchedule::class, 'new_work_schedule_id', 'id');
    }

    public function firstApprover()
    {
        return $this->belongsTo(User::class, 'first_approver', 'id');
    }

    public function secondApprover()
    {
        return $this->belongsTo(User::class, 'second_approver', 'id');
    }

    public function thirdApprover()
    {
        return $this->belongsTo(User::class, 'third_approver', 'id');
    }

    public function adminApprover()
    {
        return $this->belongsTo(User::class, 'admin_approver', 'id');
    }

    public function finalStatus()
    {
        return $this->belongsTo(StatusReference::class, 'final_status_id', 'id');
    }
}
