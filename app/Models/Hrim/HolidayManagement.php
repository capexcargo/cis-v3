<?php

namespace App\Models\Hrim;

use App\Models\BranchReference;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HolidayManagement extends Model
{
    use HasFactory;

    protected $table = "hrim_holiday";

    protected $fillable = [
        'name',
        'date',
        'branch_id',
        'type_id',
    ];

    public function branch()
    {
        return $this->belongsTo(BranchReference::class, 'branch_id', 'id');
    }

    public function holidayType()
    {
        return $this->belongsTo(HolidayTypeReference::class, 'type_id', 'id');
    }

}
