<?php

namespace App\Models\Hrim;

use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmploymentStatus extends Model
{
    use HasFactory;

    protected $table = "hrim_employment_status";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];

    public function users()
    {
        return $this->hasManyThrough(
            User::class,
            UserDetails::class,
            'employment_status_id', // Foreign key on the environments table...
            'id', // Foreign key on the deployments table...

        );
    }
}
