<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatutoryManagement extends Model
{
    use HasFactory;

    protected $table = "hrim_statutory";

    protected $fillable = [
        'statutor_benefit_type',
        'description',
        'created_by',
    ];

    public function StatutoryUser()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
