<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveTypeReference extends Model
{
    use HasFactory;

    protected $table = "hrim_leave_type_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];

    public function leaves()
    {
        return $this->hasMany(Leave::class, 'leave_type_id', 'id');
    }
}
