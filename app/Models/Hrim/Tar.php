<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tar extends Model
{
    use HasFactory;

    protected $table = "hrim_tar";

    protected $fillable = [
        'user_id',
        'time_log_id',
        'actual_time_in',
        'actual_time_out',
        'current_time_in',
        'current_time_out',
        'date_from',
        'date_to',
        'tar_reason_id',
        'first_approver',
        'second_approver',
        'third_approver',
        'admin_approver',
        'first_status',
        'second_status',
        'third_status',
        'admin_status',
        'final_status_id',
        'first_approval_date',
        'second_approval_date',
        'third_approval_date',
        'admin_approval_date',
        'first_approver_remarks',
        'second_approver_remarks',
        'third_approver_remarks',
        'admin_approver_remarks',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function timeLog()
    {
        return $this->belongsTo(TimeLog::class, 'time_log_id', 'id');
    }
    public function timeLogs()
    {
        return $this->belongsTo(TimeLog::class, 'date_from', 'date');
    }

    public function tarReason()
    {
        return $this->belongsTo(TarReasonReference::class, 'tar_reason_id', 'id');
    }

    public function firstApprover()
    {
        return $this->belongsTo(User::class, 'first_approver', 'id');
    }

    public function secondApprover()
    {
        return $this->belongsTo(User::class, 'second_approver', 'id');
    }

    public function thirdApprover()
    {
        return $this->belongsTo(User::class, 'third_approver', 'id');
    }

    public function adminApprover()
    {
        return $this->belongsTo(User::class, 'admin_approver', 'id');
    }

    public function finalStatus()
    {
        return $this->belongsTo(StatusReference::class, 'final_status_id', 'id');
    }
}
