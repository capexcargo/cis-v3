<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PositionJobRequirements extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_position_job_requirements";

    protected $fillable = [
        'position_id',
        'code',
        'created_by',
    ];

    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }
}
