<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DateCategoryReference extends Model
{
    use HasFactory;

    protected $table = "hrim_date_category_reference";

    protected $fillable = [
        'code',
        'display',
        'rate_percentage',
        'is_visible',
        'created_by',
    ];

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
