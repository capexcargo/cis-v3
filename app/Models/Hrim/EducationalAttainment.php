<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EducationalAttainment extends Model
{
    use HasFactory;

    protected $table = "hrim_educational_attainment";

    protected $fillable = [
        'user_id',
        'ea_level_id',
        'educational_institution',
        'inclusive_year_from',
        'inclusive_year_to',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function level()
    {
        return $this->belongsTo(EALevelReference::class, 'ea_level_id', 'id');
    }
}
