<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Violation extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $table = "hrim_violation_reference";
    
    protected $fillable = [
        'code',
        'display',
        'is_visible',
        'created_by',
    ];

    public function disciplinary_history()
    {
        // return $this->hasMany(Position::class, 'job_level_id', 'id');
    }
}
