<?php

namespace App\Models\Hrim;

use App\Models\Division;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamsAttachment extends Model
{

    use HasFactory, SoftDeletes;

    protected $table = "hrim_teams_attachment";

    protected $fillable = [
        'division_id',
        'path',
        'name',
        'extension',
    ];

    public function announcement()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }
}
