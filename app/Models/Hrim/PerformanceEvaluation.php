<?php

namespace App\Models\Hrim;

use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerformanceEvaluation extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_performance_evaluation";

    protected $fillable = [
        'user_id',
        'position_id',
        'kra_id',
        'kpi_id',
        'core_values_id',
        'leadership_comp_id',
        'first_points',
        'second_points',
        'third_points',
        'first_status',
        'second_status',
        'third_status',
        'first_evaluator',
        'second_evaluator',
        'third_evaluator',
        'first_eval_date',
        'second_eval_date',
        'third_eval_date',
        'first_remarks',
        'second_remarks',
        'third_remarks',
        'employee_remarks',
        'critical_remarks',
        'core_val_1st_critical_remarks',
        'core_val_2nd_critical_remarks',
        'core_val_3rd_critical_remarks',
        'lead_com_1st_critical_remarks',
        'lead_com_2nd_critical_remarks',
        'lead_com_3rd_critical_remarks',
        'user_id_remarks',
        'quarter',
        'year',
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function userDetails()
    {
        return $this->belongsTo(UserDetails::class, 'user_id', 'user_id');
    }

    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }

    public function kra()
    {
        return $this->belongsTo(KraManagement::class, 'kra_id', 'id');
    }
    
    public function kpi()
    {
        return $this->belongsTo(KpiManagement::class, 'kpi_id', 'id');
    }
    
    public function core_values()
    {
        return $this->belongsTo(CoreValueManagement::class, 'core_values_id', 'id');
    }
        
    public function leadership_comp()
    {
        return $this->belongsTo(LeadershipCompetenciesMgmt::class, 'leadership_comp_id', 'id');
    }
    
    public function performanceEval()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }
}


