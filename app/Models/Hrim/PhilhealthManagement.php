<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhilhealthManagement extends Model
{
    use HasFactory;

    protected $table = "hrim_philhealth";

    protected $fillable = [
        'year',
        'premium_rate'
    ];
}
