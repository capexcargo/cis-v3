<?php

namespace App\Models\Hrim;

use App\Models\MonthReference;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminAdjustment extends Model
{
    use HasFactory;

    protected $table = "hrim_admin_adjustment";

    protected $fillable = [
        'user_id',
        'category',
        'description',
        'amount',
        'year',
        'month',
        'payout_cutoff',
        'first_approver',
        'second_approver',
        'first_status',
        'second_status',
        'first_remarks',
        'second_remarks',
        'first_date',
        'second_date',
        'final_status',
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function categoryRef()
    {
        return $this->belongsTo(AdjustmentCategoryReference::class, 'category', 'id');
    }

    public function approver()
    {
        return $this->belongsTo(User::class, 'first_approver', 'id');
    }

    public function status()
    {
        return $this->belongsTo(StatusReference::class, 'first_status', 'id');
    }

    public function monthRef()
    {
        return $this->belongsTo(MonthReference::class, 'month', 'id');
    }
}
