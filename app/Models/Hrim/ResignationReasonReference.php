<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResignationReasonReference extends Model
{
    use HasFactory;

    protected $table = "hrim_resignation_reason_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];
}
