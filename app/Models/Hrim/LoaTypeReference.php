<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoaTypeReference extends Model
{
    use HasFactory;

    protected $table = "hrim_loa_type_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];

    public function loaManagement()
    {
        return $this->hasMany(LoaManagement::class, 'loa_type_id', 'id');
    }
}
