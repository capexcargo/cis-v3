<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrainingAndRefreshersAttachment extends Model
{

    use HasFactory, SoftDeletes;

    protected $table = "training_and_refreshers_attachment";

    protected $fillable = [
        'video_ref',
        'path',
        'name',
        'extension',
        'original_filename'
    ];

    public function trainingRef()
    {
        return $this->belongsTo(TrainingAndRefreshers::class, 'video_ref', 'video_ref');
    }
}
