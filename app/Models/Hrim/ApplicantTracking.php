<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicantTracking extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_applicant_tracking";

    protected $fillable = [
        'erf_reference_no_id',
        'firstname',
        'middlename',
        'lastname',
        'date_applied',
        'hr_notes',
        'hiring_manager_notes',
        'gm_notes',
        'status_id',
        'division_id',
        'remarks',
        '2nd_approval_date',
    ];

    public function erfReference()
    {
        return $this->belongsTo(EmployeeRequisition::class, 'erf_reference_no_id', 'id');
    }

    public function applicantStatus()
    {
        return $this->belongsTo(ApplicantStatusMgmt::class, 'status_id', 'id');
    }

    public function applicant_trackings()
    {
        return $this->hasMany(EmployeeRequisition::class, 'division_id', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(ApplicantTrackingAttachments::class, 'applicant_track_id', 'id');
    }
}
