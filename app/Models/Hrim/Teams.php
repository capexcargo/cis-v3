<?php

namespace App\Models\Hrim;

use App\Models\Division;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    use HasFactory;

    protected $table = "hrim_teams";

    protected $fillable = [
        'division_id',
        'position_id',
        'reports_to',
        'department_id',
        'section_id',
        'created_by',
    ];

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }

    public function reportsTo()
    {
        return $this->belongsTo(Position::class, 'reports_to', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    public function section()
    {
        return $this->belongsTo(SectionReference::class, 'section_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
