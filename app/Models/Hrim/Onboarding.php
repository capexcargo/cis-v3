<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Onboarding extends Model
{
    use HasFactory;

    protected $table = "hrim_onboarding";

    protected $fillable = [
        'erf_reference_no_id',
        'firstname',
        'middlename',
        'lastname',
        'date_applied',
        'status_id',
        '2nd_approval_date',
    ];

    public function erfReference()
    {
        return $this->belongsTo(EmployeeRequisition::class, 'erf_reference_no_id', 'id');
    }

    public function onboardingStatus()
    {
        return $this->belongsTo(OnboardingStatusMgmt::class, 'status_id', 'id');
    }

}
