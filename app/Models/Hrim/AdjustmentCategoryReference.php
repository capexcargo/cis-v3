<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdjustmentCategoryReference extends Model
{
    use HasFactory;

    protected $table = "hrim_admin_adj_category_reference";

}
