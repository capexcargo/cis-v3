<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventTypeReference extends Model
{
    use HasFactory;

    protected $table = "hrim_event_type_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];
}
