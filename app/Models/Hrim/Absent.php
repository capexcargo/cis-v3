<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absent extends Model
{
    use HasFactory;

    protected $table = "hrim_absent";

    protected $fillable = [
        'user_id',
        'month',
        'cutoff',
        'year',
        'payroll_status',
        'initial_payslip_status',
        'final_payslip_status',
        'bank_file_status',
        'payroll_updated_at',
        'initial_payslip_updated_at',
        'final_payslip_updated_at',
        'bank_file_updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
