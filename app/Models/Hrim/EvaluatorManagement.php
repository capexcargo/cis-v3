<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EvaluatorManagement extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_evaluator_management";
    
    protected $fillable = [
        'user_id',
        'first_approver',
        'second_approver',
        'third_approver',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'user_id', 'id');
    }
    
    public function firstEvaluator()
    {
        return $this->belongsTo(User::class, 'first_approver', 'id');
    }
    
    public function secondEvaluator()
    {
        return $this->belongsTo(User::class, 'second_approver', 'id');
    }

    public function thirdEvaluator()
    {
        return $this->belongsTo(User::class, 'third_approver', 'id');
    }

    public function evaluator()
    {
        return $this->hasMany(PerformanceEvaluation::class, 'user_id', 'user_id');
    }
}
