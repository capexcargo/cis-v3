<?php

namespace App\Models\Hrim;

use App\Models\Division;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Memos extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_memos";

    protected $fillable = [
        'memorandum_no',
        'date_posted',
        'attention_to',
        'title',
        'details',
        'posted_by',
    ];

    public function attentionTo()
    {
        return $this->belongsTo(Division::class, 'division_id', 'attention_to');
    }

    public function postedBy()
    {
        return $this->belongsTo(User::class, 'posted_by', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(MemoAttachment::class, 'memo_id', 'id');
    }
}
