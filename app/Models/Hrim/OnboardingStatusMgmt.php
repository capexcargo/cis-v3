<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OnboardingStatusMgmt extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_onboarding_status";

    protected $fillable = [
        'code',
        'display',
        'description',
        'created_by',
    ];

    public function onboardings()
    {
        return $this->hasMany(Onboarding::class, 'status_id', 'id');
    }
}
