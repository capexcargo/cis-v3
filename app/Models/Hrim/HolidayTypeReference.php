<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HolidayTypeReference extends Model
{
    use HasFactory;

    protected $table = "hrim_holiday_type";

    protected $fillable = [
        'name',
    ];
}
