<?php

namespace App\Models\Hrim;

use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmploymentCategory extends Model
{
    use HasFactory;

    protected $table = "hrim_employment_category";

    protected $fillable = [
        'code',
        'display',
        'sick_leave',
        'vacation_leave',
        'is_visible',
    ];

    public function users()
    {
        return $this->hasManyThrough(
            User::class,
            UserDetails::class,
            'employment_category_id', // Foreign key on the environments table...
            'id', // Foreign key on the deployments table...

        );
    }
}
