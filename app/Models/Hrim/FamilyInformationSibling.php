<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyInformationSibling extends Model
{
    use HasFactory;

    protected $table = "hrim_family_information_sibling";

    protected $fillable = [
        'family_information_id',
        'name',
    ];

    public function familyInformation()
    {
        return $this->belongsTo(FamilyInformation::class, 'family_information_id', 'id');
    }
}
