<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CocSection extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $table = "hrim_coc_section_reference";
    
    protected $fillable = [
        'code',
        'display',
        'is_visible',
        'created_by',
    ];

    public function disciplinaryHistories()
    {
        return $this->hasMany(DisciplinaryHistory::class, 'coc_section_id', 'id');
    }
}
