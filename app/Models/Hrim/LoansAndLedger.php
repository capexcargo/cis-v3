<?php

namespace App\Models\Hrim;

use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoansAndLedger extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_loan";

    protected $fillable = [
        'user_id',
        'reference_no',
        'type',
        'principal_amount',
        'interest',
        'terms',
        'term_type',
        'start_date',
        'end_date',
        'purpose',
        'overrule_approver',
        'overrule_approval_date',
        'first_approver',
        'second_approver',
        'third_approver',
        'first_status',
        'second_status',
        'third_status',
        'final_status',
        'approved_amount',
        'decline_reason',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function userDetails()
    {
        return $this->belongsTo(UserDetails::class, 'user_id', 'user_id');
    }

    public function loanType()
    {
        return $this->belongsTo(LoanType::class, 'type', 'id');
    }

    public function first_approver()
    {
        return $this->belongsTo(LoanType::class, 'first_approver', 'first_approver');
    }

    public function second_approver()
    {
        return $this->belongsTo(LoanType::class, 'second_approver', 'second_approver');
    }

    public function third_approver()
    {
        return $this->belongsTo(LoanType::class, 'third_approver', 'third_approver');
    }
    public function finalStatus()
    {
        return $this->belongsTo(StatusReference::class, 'final_status', 'id');
    }

    public function loanDetails()
    {
        return $this->hasMany(LoanDetails::class, 'loan_id', 'id');
    }
}
