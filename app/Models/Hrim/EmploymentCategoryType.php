<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmploymentCategoryType extends Model
{
    use HasFactory;

    protected $table = "hrim_employment_category_type";

    protected $fillable = [
        'code',
        'display',
        'employment_category_id',
    ];

    public function emp_category()
    {
        return $this->belongsTo(EmploymentCategory::class, 'employment_category_id', 'id');
    }
}
