<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;

    protected $table = "hrim_province";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];
}
