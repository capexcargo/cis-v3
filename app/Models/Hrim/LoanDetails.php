<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoanDetails extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_loan_details";

    protected $fillable = [
        'loan_id',
        'payment_date',
        'year',
        'month',
        'cutoff',
        'amount',
        'interest',
        'status',
    ];


    public function loan()
    {
        return $this->belongsTo(LoansAndLedger::class, 'loan_id', 'id');
    }
}
