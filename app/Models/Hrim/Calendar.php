<?php

namespace App\Models\Hrim;

use App\Models\BranchReference;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Calendar extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_calendar";

    protected $fillable = [
        'branch_id',
        'event_type_id',
        'event_date_time',
        'title',
        'description',
        'created_by',
    ];

    public function branch()
    {
        return $this->belongsTo(BranchReference::class, 'branch_id', 'id');
    }

    public function eventType()
    {
        return $this->belongsTo(EventTypeReference::class, 'event_type_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function users()
    {
        return $this->hasMany(CalendarUsers::class, 'calendar_id', 'id');
    }
}
