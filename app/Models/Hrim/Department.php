<?php

namespace App\Models\Hrim;

use App\Models\Division;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_departments";

    protected $fillable = [
        'code',
        'division_id',
        'display',
        'is_visible',
        'created_by',
    ];

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }
    

}
