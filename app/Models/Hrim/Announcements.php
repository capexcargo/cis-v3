<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Announcements extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $table = "hrim_announcements";

    protected $fillable = [
        'announcement_no',
        'date_posted',
        'title',
        'details',
        'posted_by',
    ];
    
    public function postedBy()
    {
        return $this->belongsTo(User::class, 'posted_by', 'id');
    }
    
    public function attachments()
    {
        return $this->hasMany(AnnouncementAttachment::class, 'announcement_id', 'id');
    }
}
