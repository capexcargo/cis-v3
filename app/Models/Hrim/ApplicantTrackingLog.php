<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicantTrackingLog extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_applicant_tracking_log";

    protected $fillable = [
        'erf_reference_no_id',
        'date',
        'status',
        'remarks',
        'type',
        'created_by',
    ];

    public function applicantTracking(){
        return $this->belongsTo(ApplicantTracking::class, 'erf_reference_no_id', 'erf_reference_no_id');
    }
}
