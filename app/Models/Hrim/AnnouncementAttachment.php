<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Hrim\Announcements;

class AnnouncementAttachment extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $table = "hrim_announcement_attachments";

    protected $fillable = [
        'announcement_id',
        'path',
        'name',
        'extension',
    ];
    
    public function announcement()
    {
        return $this->belongsTo(Announcements::class, 'announcement_id', 'id');
    }
}
