<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KpiTagging extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_kpi_tagged";
    
    protected $fillable = [
        'position_id',
        'quarter',
        'year',
        'kra_id',
        'kpi_id',
    ];

    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }

    public function positionID()
    {
        return $this->hasMany(PerformanceEvaluation::class, 'position_id', 'position_id');
    }

}
