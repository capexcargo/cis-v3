<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hrim201Files extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_201_files";

    protected $fillable = [
        'user_id',
        'hrim_201_reference_id',
        'created_by',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function hrim201Reference()
    {
        return $this->belongsTo(Hrim201FilesReference::class, 'hrim_201_reference_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(Hrim201FilesAttachments::class, 'hrim_201_files_id', 'id');
    }
}
