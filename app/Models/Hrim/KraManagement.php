<?php

namespace App\Models\Hrim;

use App\Models\Hrim\JobLevel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KraManagement extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_kra";

    protected $fillable = [
        'name',
        'type',
        'quarter',
        'year',
    ];

    public function job_levels()
    {
        return $this->belongsTo(JobLevel::class, 'type', 'id');
    }
}
