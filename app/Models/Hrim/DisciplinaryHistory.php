<?php

namespace App\Models\Hrim;

use App\Models\BranchReference;
use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\Sanctum;

class DisciplinaryHistory extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $table = "hrim_disciplinary_history";
    
    protected $fillable = [
        'incident_date',
        'incident_time',
        'description',
        'user_id',
        'incident_branch',
        'coc_section_id',
        'violation_id',
        'disciplinary_record_id',
        'sanction_id',
        'sanction_status_id',
        'created_by',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function branch()
    {
        return $this->belongsTo(BranchReference::class, 'incident_branch', 'id');
    }

    public function cocSection()
    {
        return $this->belongsTo(CocSection::class, 'coc_section_id', 'id');
    }
    
    public function userDetails()
    {
        return $this->belongsTo(UserDetails::class, 'user_id', 'user_id');
    }


    public function violation()
    {
        return $this->belongsTo(Violation::class, 'violation_id', 'id');
    }
    
    public function disciplinaryRecord()
    {
        return $this->belongsTo(DisciplinaryRecord::class, 'disciplinary_record_id', 'id');
    }

    public function sanction()
    {
        return $this->belongsTo(Sanction::class, 'sanction_id', 'id');
    }

    public function sanctionStatus()
    {
        return $this->belongsTo(SanctionStatus::class, 'sanction_status_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(DisciplinaryAttachments::class, 'disciplinary_id', 'id');
    }
}
