<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveDayTypeReference extends Model
{
    use HasFactory;

    protected $table = "hrim_leave_day_type_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
        'points',
    ];

    public function leaves()
    {
        return $this->hasMany(Leave::class, 'leave_type_id', 'id');
    }
}
