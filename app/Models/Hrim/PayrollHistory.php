<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayrollHistory extends Model
{
    use HasFactory;

    protected $table = "hrim_payroll_history";

    protected $fillable = [
        'user_id',
        'month',
        'cutoff',
        'year',
        'employee_id',
        'employee_code',
        'bank_account',
        'date_hired',
        'employment_status',
        'job_rank',
        'position',
        'branch',
        'basic_pay',
        'cola',
        'gross_pay',
        'holiday_pay',
        'nd_pay',
        'ot_pay',
        'holiday_ot_pay',
        'special_ot_pay',
        'restday_ot_pay',
        'restday_pay',
        'leave_wt_pay',
        'leave_wo_pay',
        'tardiness',
        'undertime',
        'absence',
        'government_contri_sss',
        'government_contri_philhealth',
        'government_contri_pagibig',
        'government_loans_sss',
        'government_loans_philhealth',
        'government_loans_pagibig',
        'tax',
        'other_loans_deductions',
        'admin_adjustments',
        'dlb',
        'net_pay',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
