<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EALevelReference extends Model
{
    use HasFactory;

    protected $table = "hrim_ea_level_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];
}
