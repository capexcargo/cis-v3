<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CutOffManagement extends Model
{
    use HasFactory;

    protected $table = "hrim_cutoff_management";
}
