<?php

namespace App\Models\Hrim;


use App\Models\Hrim\PositionResponsibilities;
use App\Models\Hrim\PositionJobRequirements;
use App\Models\Hrim\JobLevel;
use App\Models\Hrim\Department;
use App\Models\Division;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_positions";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
        'job_level_id',
        'department_level_id',
        'division_id',
        'job_overview',
        'created_by',
    ];


    public function jobLevel()
    {
        return $this->belongsTo(JobLevel::class, 'job_level_id', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_level_id', 'id');
    }

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }
    
    public function responsibilities()
    {
        return $this->hasMany(PositionResponsibilities::class, 'position_id', 'id');
    }
    
    public function requirements()
    {
        return $this->hasMany(PositionJobRequirements::class, 'position_id', 'id');
    }

    public function kpi_tagging(){
        return $this->hasMany(KpiTagging::class, 'position_id', 'id');
    }
}
