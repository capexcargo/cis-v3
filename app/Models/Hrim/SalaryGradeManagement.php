<?php

namespace App\Models\Hrim;

use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalaryGradeManagement extends Model
{
    use HasFactory;

    protected $table = "hrim_salary_grade";

    protected $fillable = [
        'grade',
        'minimun_amount',
        'maximum_amount',
        'created_by',
    ];

    public function salary_grade()
    {
        return $this->hasManyThrough(
            User::class,
            UserDetails::class,
            'salary_grade_id', // Foreign key on the environments table...
            'id', // Foreign key on the deployments table...
        );
    }
}
