<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BenefitsManagement extends Model
{
    use HasFactory;

    protected $table = "hrim_benefits";

    protected $fillable = [
        'benefit_type',
        'description',
        'created_by',
    ];

    public function benefitsUser()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
