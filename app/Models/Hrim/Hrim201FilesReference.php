<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hrim201FilesReference extends Model
{
    use HasFactory;

    protected $table = "hrim_201_files_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];

    public function hrim201Files()
    {
        return $this->hasMany(Hrim201Files::class, 'hrim_201_reference_id', 'id');
    }
}
