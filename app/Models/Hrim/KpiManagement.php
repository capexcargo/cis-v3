<?php

namespace App\Models\Hrim;

use App\Models\Hrim\JobLevel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KpiManagement extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_kpi";
    
    protected $fillable = [
        'name',
        'quarter',
        'year',
        'points',
        'target',
        'year',
        'remarks',
    ];

}
