<?php

namespace App\Models\Hrim;

use App\Models\Hrim\Memos;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemoAttachment extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_memo_attachments";

    protected $fillable = [
        'memo_id',        
        'path',
        'name',
        'extension',
    ];
    public function memo()
    {
        return $this->belongsTo(Memos::class, 'memo_id', 'id');
    }
}
