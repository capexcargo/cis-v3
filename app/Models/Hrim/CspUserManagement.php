<?php

namespace App\Models\Hrim;

use App\Models\DivisionRolesReference;
use App\Models\UserDetails;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CspUserManagement extends Model
{
    use HasFactory;

    protected $table = 'users';

    protected $fillable = [
        'role_id',
        'division_id',
        'level_id',
        'status_id',
        'photo_path',
        'photo_name',
        'name',
        'email',
        'email_verification_code',
        'password',
        'attempt',
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'profile_photo_url',
    ];

    public function userDetails()
    {
        return $this->hasOne(UserDetails::class, 'user_id', 'id');
    }

    public function role()
    {
        return $this->hasOne(DivisionRolesReference::class, 'id', 'role_id');
    }

    
}
