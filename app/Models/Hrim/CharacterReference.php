<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CharacterReference extends Model
{
    use HasFactory;

    protected $table = "hrim_character_reference";

    protected $fillable = [
        'user_id',
        'name',
        'position',
        'company',
        'mobile_number',
        'telephone_number',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
