<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PositionResponsibilities extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_position_responsibilities";

    protected $fillable = [
        'position_id',
        'duties',
        'created_by',
    ];

    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }
}
