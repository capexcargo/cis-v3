<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestReason extends Model
{
    use HasFactory;

    protected $table = "hrim_requisition_reason";

    protected $fillable = [
        'code',
        'display',
    ];
}
