<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyInformation extends Model
{
    use HasFactory;

    protected $table = "hrim_family_information";

    protected $fillable = [
        'user_id',
        'fathers_name',
        'mothers_maiden_name',
        'spouse',
    ];

    public function siblings()
    {
        return $this->hasMany(FamilyInformationSibling::class, 'family_information_id', 'id');
    }

    public function childrens()
    {
        return $this->hasMany(FamilyInformationChildren::class, 'family_information_id', 'id');
    }
}
