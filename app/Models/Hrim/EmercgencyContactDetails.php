<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmercgencyContactDetails extends Model
{
    use HasFactory;

    protected $table = "hrim_emergency_contact_details";

    protected $fillable = [
        'user_id',
        'name',
        'ecd_relationship_id',
        'address',
        'mobile_number',
        'telephone_number',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function relationship()
    {
        return $this->belongsTo(ECDRelationshipReference::class, 'ecd_relationship_id', 'id');
    }
}
