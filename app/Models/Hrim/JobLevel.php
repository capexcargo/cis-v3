<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobLevel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_job_levels";
    
    protected $fillable = [
        'code',
        'display',
        'is_visible',
        'created_by',
    ];

    public function positions()
    {
        return $this->hasMany(Position::class, 'job_level_id', 'id');
    }
}
