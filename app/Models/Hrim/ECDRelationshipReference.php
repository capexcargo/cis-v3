<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ECDRelationshipReference extends Model
{
    use HasFactory;

    protected $table = "hrim_ecd_relationship_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];
}
