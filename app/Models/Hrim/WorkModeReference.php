<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkModeReference extends Model
{
    use HasFactory;

    protected $table = "hrim_work_mode_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible'
    ];
}
