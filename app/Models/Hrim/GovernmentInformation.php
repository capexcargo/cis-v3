<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GovernmentInformation extends Model
{
    use HasFactory;

    protected $table = "hrim_government_information";

    protected $fillable = [
        'user_id',
        'sss_no',
        'pagibig_no',
        'philhealth_no',
        'tin_no',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
