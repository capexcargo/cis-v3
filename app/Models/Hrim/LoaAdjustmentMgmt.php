<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoaAdjustmentMgmt extends Model
{
    use HasFactory;

    protected $table = "hrim_loa_adjustment_management";
    
    protected $fillable = [
        'min_amount',
        'max_amount',
        'category_id',
        'approver_id',
    ];

    public function approver()
    {
        return $this->belongsTo(User::class, 'approver_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(AdjustmentCategoryReference::class, 'category_id', 'id');
    }
}
