<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkSchedule extends Model
{
    use HasFactory;

    protected $table = "hrim_work_schedule";

    protected $fillable = [
        'name',
        'work_mode_id',
        'shift',
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday',
        'sunday',
        'time_from',
        'time_to',
        'break_from',
        'break_to',
        'ot_break_from',
        'ot_break_to',
        'created_by',
    ];

    public function workMode()
    {
        return $this->belongsTo(WorkModeReference::class, 'work_mode_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
