<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResignationAttachements extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_resignation_attachments";

    protected $fillable = [
        'user_id',
        'path',
        'name',
        'extension',
    ];

    public function resignation()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
