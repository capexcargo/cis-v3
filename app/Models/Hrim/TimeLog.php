<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TimeLog extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_time_log";

    protected $fillable = [
        'user_id',
        'work_sched_id',
        'date_category_id',
        'work_mode_id',
        'time_in_img_path',
        'time_in_img_name',
        'time_in',
        'time_out_img_path',
        'time_out_img_name',
        'time_out',
        'time_out_date',
        'date',
        'rendered_time',
        'late',
        'undertime',
        'computed_ot',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function workSchedule()
    {
        return $this->belongsTo(WorkSchedule::class, 'work_sched_id', 'id');
    }

    public function dateCategory()
    {
        return $this->belongsTo(DateCategoryReference::class, 'date_category_id', 'id');
    }

    public function workMode()
    {
        return $this->belongsTo(WorkModeReference::class, 'work_mode_id', 'id');
    }

    public function overtime()
    {
        return $this->hasOne(Overtime::class, 'time_log_id', 'id');
    }
}
