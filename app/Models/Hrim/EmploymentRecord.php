<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmploymentRecord extends Model
{
    use HasFactory;

    protected $table = "hrim_employment_record";

    protected $fillable = [
        'user_id',
        'employer',
        'position',
        'inclusive_year_from',
        'inclusive_year_to',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
