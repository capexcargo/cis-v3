<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    use HasFactory;

    protected $table = "hrim_leave";

    protected $fillable = [
        'user_id',
        'inclusive_date_from',
        'inclusive_date_to',
        'days',
        'points',
        'resume_date',
        'leave_type_id',
        'other',
        'is_with_medical_certificate',
        'leave_day_type_id',
        'is_with_pay',
        'reason',
        'reliever',
        'first_approver',
        'second_approver',
        'third_approver',
        'admin_approver',
        'first_status',
        'second_status',
        'third_status',
        'admin_status',
        'final_status_id',
        'first_approval_date',
        'second_approval_date',
        'third_approval_date',
        'admin_approval_date',
        'first_approver_remarks',
        'second_approver_remarks',
        'third_approver_remarks',
        'admin_approver_remarks',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function leaveType()
    {
        return $this->belongsTo(LeaveTypeReference::class, 'leave_type_id', 'id');
    }

    public function leaveDayType()
    {
        return $this->belongsTo(LeaveDayTypeReference::class, 'leave_day_type_id', 'id');
    }

    public function relieverUser()
    {
        return $this->belongsTo(User::class, 'reliever', 'id');
    }

    public function firstApprover()
    {
        return $this->belongsTo(User::class, 'first_approver', 'id');
    }

    public function secondApprover()
    {
        return $this->belongsTo(User::class, 'second_approver', 'id');
    }

    public function thirdApprover()
    {
        return $this->belongsTo(User::class, 'third_approver', 'id');
    }

    public function adminApprover()
    {
        return $this->belongsTo(User::class, 'admin_approver', 'id');
    }

    public function finalStatus()
    {
        return $this->belongsTo(StatusReference::class, 'final_status_id', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(LeaveAttachments::class, 'leave_id', 'id');
    }
}
