<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KraPointsManagement extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_kra_points";
    
    protected $fillable = [
        'points',
        'description',
        'quarter',
        'year',
    ];
}
