<?php

namespace App\Models\Hrim;

use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GenderReference extends Model
{
    use HasFactory;

    protected $table = "hrim_gender";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];

    public function users()
    {
        return $this->hasManyThrough(
            User::class,
            UserDetails::class,
            'gender_id', // Foreign key on the environments table...
            'id', // Foreign key on the deployments table...

        );
    }
}
