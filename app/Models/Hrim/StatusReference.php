<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusReference extends Model
{
    use HasFactory;

    protected $table = "hrim_status_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];

    public function leaveRecords()
    {
        return $this->hasMany(Leave::class, 'final_status_id', 'id');
    }

    public function overtimeRecords()
    {
        return $this->hasMany(Overtime::class, 'final_status_id', 'id');
    }

    public function tars()
    {
        return $this->hasMany(Tar::class, 'final_status_id', 'id');
    }
    public function scheduleAdjustments()
    {
        return $this->hasMany(ScheduleAdjustment::class, 'final_status_id', 'id');
    }

    public function employee_requisitions()
    {
        return $this->hasMany(EmployeeRequisition::class, 'final_status', 'id');
    }
}
