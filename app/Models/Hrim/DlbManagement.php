<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DlbManagement extends Model
{
    use HasFactory;


    protected $table = "hrim_dlb";

    protected $fillable = [
        'user_id',
        'year',
        'month',
        'payout_cutoff',
        'amount',

    ];

    public function dlbUsers()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
