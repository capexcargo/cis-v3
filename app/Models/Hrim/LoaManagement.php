<?php

namespace App\Models\Hrim;

use App\Models\BranchReference;
use App\Models\Division;
use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoaManagement extends Model
{
    use HasFactory;

    protected $table = "hrim_loa_management";

    protected $fillable = [
        'division_id',
        'department_id',
        'branch_id',
        'loa_type_id',
        'first_approver',
        'second_approver',
        'third_approver',
    ];

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function departments()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    public function branch()
    {
        return $this->belongsTo(BranchReference::class, 'branch_id', 'id');
    }

    public function loaType()
    {
        return $this->belongsTo(LoaTypeReference::class, 'loa_type_id', 'id');
    }

    public function firstApprover()
    {
        return $this->belongsTo(User::class, 'first_approver', 'id');
    }

    public function secondApprover()
    {
        return $this->belongsTo(User::class, 'second_approver', 'id');
    }

    public function thirdApprover()
    {
        return $this->belongsTo(User::class, 'third_approver', 'id');
    }
}
