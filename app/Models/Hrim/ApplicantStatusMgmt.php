<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicantStatusMgmt extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_applicant_tracking_status";

    protected $fillable = [
        'code',
        'display',
        'created_by',
    ];
}
