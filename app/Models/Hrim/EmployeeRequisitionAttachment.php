<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeRequisitionAttachment extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $table = "hrim_requisition_attachment";

    protected $fillable = [
        'requisition_id',
        'path',
        'name',
        'extension',
    ];
    
    public function employee_requisition()
    {
        return $this->belongsTo(EmployeeRequisition::class, 'requisition_id', 'id');
    }
}
