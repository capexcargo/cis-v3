<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AreaReference extends Model
{
    use HasFactory;

    protected $table = "area_reference";

    protected $fillable = [
        'code',
        'name',
        'region',
    ];
}
