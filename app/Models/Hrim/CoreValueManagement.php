<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoreValueManagement extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_core_value";
    
    protected $fillable = [
        'description',
    ];
}
