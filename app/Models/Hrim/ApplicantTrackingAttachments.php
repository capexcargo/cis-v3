<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicantTrackingAttachments extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_applicant_tracking_attachments";

    protected $fillable = [
        'applicant_track_id',
        'path',
        'name',
        'extension',
    ];
    
    public function applicant_tracking()
    {
        return $this->belongsTo(ApplicantTracking::class, 'applicant_track_id', 'id');
    }
}
