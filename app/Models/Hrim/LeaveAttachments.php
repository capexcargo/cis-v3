<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveAttachments extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_leave_attachments";

    protected $fillable = [
        'leave_id',
        'original_name',
        'path',
        'name',
        'extension',
    ];

    public function leave()
    {
        return $this->belongsTo(Leave::class, 'leave_id', 'id');
    }
}
