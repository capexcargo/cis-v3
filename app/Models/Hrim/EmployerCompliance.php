<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployerCompliance extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_employer_compliance";

    protected $fillable = [
        'path',
        'name',
        'extension',
        'title',
        'description',
        'is_visible',
        'created_by',
    ];

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
