<?php

namespace App\Models\Hrim;

use App\Models\BranchReference;
use App\Models\Division;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeRequisition extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'hrim_employee_requisition';

    protected $fillable = [
        'erf_reference_no',
        'branch_id',
        'division_id',
        'position_id',
        'job_level_id',
        'employment_category_id',
        'employment_type_id',
        'project_name',
        'duration_from',
        'duration_to',
        'requisition_reason_id',
        'target_hire',
        'first_approver',
        'second_approver',
        'first_status',
        'second_status',
        'final_status',
        'first_approval_date',
        'second_approval_date',
        'created_by',
    ];

    public function branch()
    {
        return $this->belongsTo(BranchReference::class, 'branch_id', 'id');
    }

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }

    public function job_level()
    {
        return $this->belongsTo(JobLevel::class, 'job_level_id', 'id');
    }

    public function employment_category()
    {
        return $this->belongsTo(EmploymentStatus::class, 'employment_category_id', 'id');
    }

    public function employment_category_type()
    {
        return $this->belongsTo(EmploymentCategoryType::class, 'employment_type_id', 'id');
    }

    public function requisition_reason()
    {
        return $this->belongsTo(RequestReason::class, 'requisition_reason_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function firstApprover()
    {
        return $this->belongsTo(User::class, 'first_approver', 'id');
    }

    public function secondApprover()
    {
        return $this->belongsTo(User::class, 'second_approver', 'id');
    }

    public function status()
    {
        return $this->belongsTo(StatusReference::class, 'final_status', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(EmployeeRequisitionAttachment::class, 'requisition_id', 'id');
    }

    public function erfReference()
    {
        return $this->belongsTo(ApplicantTracking::class, 'id', 'erf_reference_no_id');
    }

    public function applicant_tracking()
    {
        return $this->hasMany(ApplicantTracking::class, 'id', 'erf_reference_no_id');
    }
}
