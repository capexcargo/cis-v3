<?php

namespace App\Models\Hrim;

use App\Models\BranchReference;
use App\Models\UserDetails;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agencies extends Model
{
    use HasFactory;

    protected $table = "hrim_agencies";

    protected $fillable = [
        'name',
        'description',
        'branch_id',
        'is_active',
    ];

    public function branch()
    {
        return $this->belongsTo(BranchReference::class, 'branch_id', 'id');
    }

    public function users()
    {
        return $this->hasMany(UserDetails::class, 'agency_id', 'id');
    }
}
