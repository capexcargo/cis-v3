<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PagibigManagement extends Model
{
    use HasFactory;

    protected $table = "hrim_pagibig";

    protected $fillable = [
        'year',
        'share',
    ];
}
