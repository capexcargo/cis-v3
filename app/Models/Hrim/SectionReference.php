<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SectionReference extends Model
{
    use HasFactory;

    protected $table = "hrim_section_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];
}
