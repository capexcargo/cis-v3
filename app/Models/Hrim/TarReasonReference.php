<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TarReasonReference extends Model
{
    use HasFactory;

    protected $table = "hrim_tar_reason_reference";

    protected $fillable = [
        'code',
        'display',
        'is_visible',
        'created_by',
    ];
}
