<?php

namespace App\Models\Hrim;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hrim201FilesAttachments extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "hrim_201_files_attachments";

    protected $fillable = [
        'hrim_201_files_id',
        'path',
        'name',
        'extension',
    ];

    public function hrim201Files()
    {
        return $this->belongsTo(Hrim201Files::class, 'hrim_201_files_id', 'id');
    }
}
