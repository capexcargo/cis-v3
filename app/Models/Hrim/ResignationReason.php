<?php

namespace App\Models\Hrim;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResignationReason extends Model
{
    use HasFactory;

    protected $table = "hrim_resignation_reason";

    protected $fillable = [
        'user_id',
        'date_resignation',
        'reason_description',
        'reason_reference_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function reason()
    {
        return $this->belongsTo(ResignationReasonReference::class, 'reason_reference_id', 'id');
    }
}
