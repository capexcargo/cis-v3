<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntryCustomerInfo extends Model
{
    use HasFactory;

    protected $table = "oims_transaction_entry_customer_info";

    protected $fillable = [
        'account_type',
        'account_no',
        'fullname',
        'company_name',
        'first_name',
        'middle_name',
        'last_name',
    ];

    public function addresses()
    {
        return $this->hasMany(OimsTransactionEntryCustomerAddressList::class, 'account_id', 'id');
    }
}
