<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntryItemDetails extends Model
{
    use HasFactory;

    protected $table = "oims_transaction_entry_item_details";

    protected $fillable = [
        'waybill_id',
        'qty',
        'weight',
        'length',
        'width',
        'height',
        'unit_of_measurement_id',
        'measurement_type_id',
        'type_of_packaging_id',
        'is_for_crating',
        'crating_status',
        'crating_type_id',
        'cwt',
        'cbm',
        'container',
        'rate_id_applied',
        'cargotype',
    ];
}
