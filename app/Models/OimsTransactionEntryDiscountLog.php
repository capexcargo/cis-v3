<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntryDiscountLog extends Model
{
    use HasFactory;

    protected $table = "oims_transaction_entry_discount_log";

    protected $fillable = [
        'waybill_id',
        'discount_id',
        'discount_name',
        'discount_price',
        'transaction_date',
        'promo_code',
    ];
}
