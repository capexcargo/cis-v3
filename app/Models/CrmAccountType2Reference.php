<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmAccountType2Reference extends Model
{
    use HasFactory;
    protected $table = "crm_account_type_2";

    protected $fillable = [
        'name'
    ];
}
