<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsPurchasingServiceReqAttachment extends Model
{
    use HasFactory;

    protected $table = "acctng_purchasing_service_req_attachment";

    protected $fillable = [
        'reference_no_id',
        'service_req_id',
        'path',
        'name',
        'extension',
    ];
}
