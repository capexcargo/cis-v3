<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntryCustomerAddressList extends Model
{
    use HasFactory;

    protected $table = "oims_transaction_entry_customer_address_list";

    protected $fillable = [
        'account_type',
        'account_id',
        'address_line_1',
        'address_line_2 ',
        'address_type',
        'address_label',
        'state_id',
        'city_id',
        'barangay_id',
        'country_id',
        'postal_id',
        'is_primary',
        'contact_person_reference',
    ];
}
