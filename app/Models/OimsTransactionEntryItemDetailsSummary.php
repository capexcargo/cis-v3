<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntryItemDetailsSummary extends Model
{
    use HasFactory;

    protected $table = "oims_transaction_entry_item_details_summary";

    protected $fillable = [
        'waybill_id',
        'total_qty',
        'total_weight',
        'total_dimension',
        'total_container',
        'total_cbm',
        'total_cwt',
    ];
}
