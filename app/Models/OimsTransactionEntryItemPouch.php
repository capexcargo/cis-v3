<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntryItemPouch extends Model
{
    use HasFactory;

    protected $table = "oims_transaction_entry_item_pouch";

    protected $fillable = [
        'waybill_id',
        'qty',
        'pouch_size',
        'amount',
        'rate_id_applied',
        'cargotype',
    ];
}
