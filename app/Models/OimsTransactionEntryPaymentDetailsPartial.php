<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntryPaymentDetailsPartial extends Model
{
    use HasFactory;

    protected $table = "oims_transaction_entry_payment_details_partial";

    protected $fillable = [
        'waybill_id',
        'payment_amount',
        'remaining_balance',
    ];
}
