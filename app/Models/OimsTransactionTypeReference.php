<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionTypeReference extends Model
{
    use HasFactory;

    protected $table = "oims_transaction_type_reference";

    protected $fillable = [
        'name',
    ];
}
