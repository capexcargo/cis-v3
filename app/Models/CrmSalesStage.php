<?php

namespace App\Models;

use App\Models\Crm\CrmOpportunity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmSalesStage extends Model
{
    use HasFactory;
    protected $table = "crm_sales_stage";

    protected $fillable = [
        'sales_stage',
        'opportunity_status_id',
        'status',
    ];

    public function OpportunityStatus()
    {
        return $this->belongsTo(CrmOpportunityStatusMgmt::class, 'opportunity_status_id', 'id');
    }

    public function opportunities()
    {
        return $this->hasMany(CrmOpportunity::class, 'sales_stage_id', 'id');
    }
}
