<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsCargoStatusReference extends Model
{
    use HasFactory;
    protected $table = "oims_cargo_status_reference";

    protected $fillable = [
        'name',
        'status',
    ];
}
