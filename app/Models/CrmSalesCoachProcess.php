<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmSalesCoachProcess extends Model
{
    use HasFactory;

    protected $table = "crm_sales_coach_process";

    protected $fillable = [
        'process_content',
        'sales_coach_id',
    ];

    public function SCP()
    {
       return $this->belongsTo(CrmSalesCoach::class, 'sales_coach_id', 'id');
    }

    public function subprocess_many()
    {
       return $this->hasMany(CrmSalesCoachSubprocess::class, 'sales_coach_process_id', 'id');
    }

}
