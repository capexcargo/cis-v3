<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsItemDescription extends Model
{
    use HasFactory;
    protected $table = "acctng_item_description_reference";

    protected $fillable = [
        'item_category_id',
        'description',
        'price',
        'status',
    ];

    public function ItemCat(){
        return $this->belongsTo(FimsItemCategory::class, 'item_category_id', 'id');
    }
}
