<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntryTypeGoods extends Model
{
    use HasFactory;

    protected $table = "oims_transaction_entry_type_goods";

    protected $fillable = [
        'name',
    ];
}
