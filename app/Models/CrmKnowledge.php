<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmKnowledge extends Model
{
    use HasFactory;

    protected $table = "crm_knowledge";

    protected $fillable = [
        'faq_concern',
        'status',
    ];

    public function respHasMany()
    {
       return $this->hasMany(CrmKnowledgeResponse::class, 'knowledge_id', 'id');
    }
}
