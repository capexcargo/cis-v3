<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsBudgetTransfer extends Model
{
    use HasFactory;

    protected $table = "acctng_budget_transfer";

    protected $fillable = [
        'source_id_from',
        'source_id_to',
        'coa_id',
        'coa_from',
        'coa_to',
        'month_from',
        'month_to',
        'year',
        'transfer_amount',
        'transfer_type',
        'transferred_by',
    ];
}
