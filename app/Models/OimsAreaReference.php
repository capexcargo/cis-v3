<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsAreaReference extends Model
{
    use HasFactory;
    protected $table = "oims_area_reference";

    protected $fillable = [
        'name',
        'quadrant_id',
        'status',
    ];
}
