<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmBookingAttachment extends Model
{
    use HasFactory;
    protected $table = "crm_booking_work_instruction_attachment";

    protected $fillable = [
        'booking_id',
        'path',
        'name',
        'extension',
    ];

    public function BookingTypeDetailsA()
    {
       return $this->belongsTo(CrmBooking::class, 'booking_id', 'id');
    }
}
