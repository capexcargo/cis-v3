<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsBranchReference extends Model
{
    use HasFactory;

    protected $table = "oims_branch_reference";

    protected $fillable = [
        'region',
        'name',
        'address',
        'latitude',
        'longitude',
        'address',
        'origin_destination_id',
        'status',
        'default',
    ];

    public function TeamRouteAssignRef()
    {
        return $this->belongsTo(OimsTeamRouteAssignment::class, 'id', 'branch_id');
    }

    public function BookingHasMany()
    {
        return $this->hasMany(CrmBooking::class, 'booking_branch_id', 'id');
    }

    public function TransactionHasMany()
    {
        return $this->hasMany(OimsTransactionEntry::class, 'destination_id', 'id');
    }

    public function OriginRefs()
    {
        return $this->belongsTo(OriginDestinationPortReference::class, 'origin_destination_id', 'id');
    }

    public function RegionRefs()
    {
        return $this->belongsTo(Region::class, 'region', 'id');
    }

    public function OBRTelephoneHasMany()
    {
        return $this->hasMany(OimsBranchReferenceTelephone::class, 'branch_reference_id', 'id');
    }
    public function OBRContactHasMany()
    {
        return $this->hasMany(OimsBranchReferenceContactNo::class, 'branch_reference_id', 'id');
    }
}
