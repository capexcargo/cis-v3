<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OnboardingCustomerInformationContactPerson extends Model
{
    use HasFactory;

    protected $connection = "cisv3_customer_onboarding";

    protected $table = "crm_contact_person";

    protected $fillable = [
        'account_id',
        'first_name',
        'middle_name',
        'last_name',
        'position',
        'is_primary',
    ];

    public function conPerEmails()
    {
        return $this->hasMany(OnboardingCustomerInformationEmailAddressList::class, 'contact_person_id', 'id');
    }

    public function conPerTelephoneNumbers()
    {
        return $this->hasMany(OnboardingCustomerInformationTelephoneList::class, 'contact_person_id', 'id');
    }

    public function conPerMobileNumbers()
    {
        return $this->hasMany(OnboardingCustomerInformationMobileList::class, 'contact_person_id', 'id');
    }
}
