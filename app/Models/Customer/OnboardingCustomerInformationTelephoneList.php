<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OnboardingCustomerInformationTelephoneList extends Model
{
    use HasFactory;

    protected $connection = "cisv3_customer_onboarding";

    protected $table = "crm_telephone_list";

    protected $fillable = [
        'contact_person_id',
        'account_type',
        'account_id',
        'telephone',
        'is_primary',
    ];
}
