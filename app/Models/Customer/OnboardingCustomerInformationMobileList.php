<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OnboardingCustomerInformationMobileList extends Model
{
    use HasFactory;
    
    protected $connection = "cisv3_customer_onboarding";

    protected $table = "crm_mobile_list";

    protected $fillable = [
        'contact_person_id',
        'account_type',
        'account_id',
        'mobile',
        'is_primary',
    ];
}
