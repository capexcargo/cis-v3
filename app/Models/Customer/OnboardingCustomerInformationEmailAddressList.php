<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OnboardingCustomerInformationEmailAddressList extends Model
{
    use HasFactory;

    protected $connection = "cisv3_customer_onboarding";

    protected $table = "crm_email_address_list";

    protected $fillable = [
        'contact_person_id',
        'account_type',
        'account_id',
        'email',
        'is_primary',
    ];
}
