<?php

namespace App\Models\Customer;

use App\Models\Crm\BarangayReference;
use App\Models\Crm\CityReference;
use App\Models\Crm\StateReference;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OnboardingCustomerInformationAddressList extends Model
{
    use HasFactory;
    
    protected $connection = "cisv3_customer_onboarding";

    protected $table = "crm_address_list";

    protected $fillable = [
        'account_type',
        'account_id',
        'address_line_1',
        'address_line_2',
        'address_type',
        'address_label',
        'state_id',
        'city_id',
        'barangay_id',
        'country_id',
        'postal_id',
        'is_primary',
    ];

    public function stateRef()
    {
        return $this->belongsTo(StateReference::class, 'state_id', 'id');
    }

    public function cityRef()
    {
        return $this->belongsTo(CityReference::class, 'city_id', 'id');
    }

    public function barangayRef()
    {
        return $this->belongsTo(BarangayReference::class, 'barangay_id', 'id');
    }

}
