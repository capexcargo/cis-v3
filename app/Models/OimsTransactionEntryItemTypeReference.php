<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntryItemTypeReference extends Model
{
    use HasFactory;

    protected $table = "oims_transaction_entry_item_type_reference";

    protected $fillable = [
        'name',
        'transport_mode',
    ];
}
