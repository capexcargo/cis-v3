<?php

namespace App\Models;

use App\Models\Accounting\BudgetAvailment;
use App\Models\Accounting\BudgetChart;
use App\Models\Accounting\BudgetPlan;
use App\Models\Accounting\BudgetSource;
use App\Models\Accounting\TransferBudget;
use App\Models\Hrim\ApplicantTracking;
use App\Models\Hrim\EmployeeRequisition;
use App\Models\Hrim\Teams;
use App\Models\Hrim\TeamsAttachment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    use HasFactory;

    protected $table = "division";

    protected $fillable = [
        'name',
        'portal_name',
        'description',
        'teams_status'
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'division_id', 'id');
    }

    public function roles()
    {
        return $this->hasMany(DivisionRolesReference::class, 'division_id', 'id');
    }

    public function budgetSource()
    {
        return $this->hasMany(BudgetSource::class, 'division_id', 'id');
    }

    public function budgetChart()
    {
        return $this->hasMany(BudgetChart::class, 'division_id', 'id');
    }

    public function budgetPlan()
    {
        return $this->hasMany(BudgetPlan::class, 'division_id', 'id');
    }

    public function transferBudgetFrom()
    {
        return $this->hasMany(TransferBudget::class, 'division_id_from', 'id');
    }

    public function transferBudgetTo()
    {
        return $this->hasMany(TransferBudget::class, 'division_id_to', 'id');
    }

    public function availment()
    {
        return $this->hasMany(BudgetAvailment::class, 'division_id', 'id');
    }

    public function teams()
    {
        return $this->hasMany(Teams::class, 'division_id', 'id');
    }

    
    public function employee_requisitions()
    {
        return $this->hasMany(EmployeeRequisition::class, 'division_id', 'id');
    }

    public function applicant_trackings()
    {
        return $this->hasMany(ApplicantTracking::class, 'division_id', 'id');
    }
    
    public function attachments()
    {
        return $this->hasMany(TeamsAttachment::class, 'division_id', 'id');
    }
}
