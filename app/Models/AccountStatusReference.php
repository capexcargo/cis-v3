<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountStatusReference extends Model
{
    use HasFactory;

    protected $table = "account_status_reference";

    public function users()
    {
        return $this->hasMany(User::class, 'status_id', 'id');
    }
}
