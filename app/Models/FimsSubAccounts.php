<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsSubAccounts extends Model
{
    use HasFactory;
    protected $table = "acctng_sub_accounts_reference";

    protected $fillable = [
        'name',
        'account_code',
        'status',
    ];
}
