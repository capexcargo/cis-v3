<?php

namespace App\Models;

use CreateCrmHeirarchy;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmHeirarchyDetails extends Model
{
    use HasFactory;

    protected $table = "crm_heirarchy_details";

    protected $fillable = [
        'tagged_position_id',
        'heirarchy_id',
    ];

    public function tagged()
    {
       return $this->belongsTo(Hierarchy::class, 'heirarchy_id', 'id');
    }
}

