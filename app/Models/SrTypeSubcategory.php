<?php

namespace App\Models;

use App\Models\Crm\CrmServiceRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SrTypeSubcategory extends Model
{
    use HasFactory;

    protected $table = "crm_sr_type_subcategory";

    protected $fillable = [
        'name',
        'sr_type_id',
    ];

    public function SrSubCat()
    {
        return $this->belongsTo(SrType::class, 'sr_type_id', 'id');
    }

    public function ServiceRequest()
    {
        return $this->hasMany(CrmServiceRequest::class, 'sub_category_id', 'id');
    }
}
