<?php

namespace App\Models\Ticketing;

use App\Models\Hrim\Department;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskHolder extends Model
{
    use HasFactory;

    protected $table = "ticket_task_holder";

    protected $fillable = [
        'user_id',
        'category_id',
        'subcategory_id',
        'division_id',
        'department_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(TicketCategoryManagement::class, 'category_id', 'id');
    }

    public function sub_category()
    {
        return $this->belongsTo(TicketSubcategoryMgmt::class, 'subcategory_id', 'id');
    }
    
    public function tickets()
    {
        return $this->hasMany(TicketManagement::class, 'task_holder', 'user_id');
    }
}
