<?php

namespace App\Models\Ticketing;

use App\Models\Division;
use App\Models\Hrim\Department;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketSubcategoryMgmt extends Model
{
    use HasFactory;

    protected $table = "ticket_subcategory";

    protected $fillable = [
        'name',
        'division_id',
        'department_id',
        'category_id',
        'sla',
        'severity',
        'created_by'
    ];

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(TicketCategoryManagement::class, 'category_id', 'id');
    }

    public function subcategories()
    {
        return $this->hasMany(TaskHolder::class, 'subcategory_id', 'id');
    }
}
