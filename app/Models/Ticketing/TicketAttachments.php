<?php

namespace App\Models\Ticketing;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketAttachments extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "ticketing_ticket_attachments";

    protected $fillable = [
        'ticket_id',
        'path',
        'name',
        'extension',
    ];
    
    public function ticket()
    {
        return $this->belongsTo(TicketManagement::class, 'ticket_id', 'id');
    }
}
