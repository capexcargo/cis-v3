<?php

namespace App\Models\Ticketing;

use App\Models\Division;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketCategoryManagement extends Model
{
    use HasFactory;

    protected $table = "ticket_category";

    protected $fillable = [
        'name',
        'division_id',
        'created_by'
    ];

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }


    public function tickets()
    {
        return $this->hasMany(TicketManagement::class, 'category_id', 'id');
    }
}
