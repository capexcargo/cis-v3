<?php

namespace App\Models\Ticketing;

use App\Models\Accounting\StatusReference;
use App\Models\Division;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketManagement extends Model
{
    use HasFactory;

    protected $table = "ticket_ticketing_system";

    protected $fillable = [
        'ticket_reference_no',
        'division_id',
        'subject',
        'category_id',
        'subcategory_id',
        'message',
        'task_holder',
        'concern_reference',
        'concern_reference_url',
        'requested_by',
        'actual_start_date',
        'actual_end_date',
        'target_start_date',
        'target_end_date',
        'task_holder_status',
        'requester_status',
        'final_status',
        'remarks',
        'assignment_date',
        'task_holder_status_date',
        'requester_status_date',
        'final_status_date',
        'created_at',
    ];

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(TicketCategoryManagement::class, 'category_id', 'id');
    }

    public function subcategory()
    {
        return $this->belongsTo(TicketSubcategoryMgmt::class, 'subcategory_id', 'id');
    }

    public function taskholder()
    {
        return $this->belongsTo(TaskHolder::class, 'task_holder', 'id');
    }

    public function taskholder_name()
    {
        return $this->belongsTo(User::class, 'task_holder', 'id');
    }

    public function requestedBy()
    {
        return $this->belongsTo(User::class, 'requested_by', 'id');
    }

    public function finalstatus()
    {
        return $this->belongsTo(StatusReference::class, 'final_status', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(TicketAttachments::class, 'ticket_id', 'id');
    }

}
