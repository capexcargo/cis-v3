<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hierarchy extends Model
{
    use HasFactory;

    protected $table = "crm_hierarchy";

    protected $fillable = [
        'email',
    ];

    public function tagHasMany()
    {
       return $this->hasMany(CrmHeirarchyDetails::class, 'heirarchy_id', 'id');
    }
}
