<?php

namespace App\Models;

use App\Models\Crm\CrmAncillaryDisplay;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmRateWarehousing extends Model
{
    use HasFactory;
    protected $table = "crm_rate_warehousing";
    protected $fillable = [
        'name',
        'effectivity_date',
        'description',
        'apply_for_id',
        'approver1_id',
        'approver2_id',
        'ancillary_charge_id',
        'approver1_status_id',
        'approver2_status_id',
        'approver1_date',
        'appover2_date',
        'final_status_id',
        'base_rate_id',
        'created_by',
    ];

    public function AncillaryReference()
    {
       return $this->belongsTo(CrmAncillaryDisplay::class, 'ancillary_charge_id', 'id');
    }

    public function RateApplyReference()
    {
       return $this->belongsTo(CrmRateApplyFor::class, 'apply_for_id', 'id');
    }

    public function App1()
    {
       return $this->belongsTo(CrmRateLoa::class, 'approver1_id', 'id');
    }

    public function App2()
    {
       return $this->belongsTo(CrmRateLoa::class, 'approver2_id', 'id');
    }

    public function App1StatusReference()
    {
       return $this->belongsTo(CrmApproval::class, 'approver1_status_id', 'id');
    }

    public function App2StatusReference()
    {
       return $this->belongsTo(CrmApproval::class, 'approver2_status_id', 'id');
    }

    public function FinalStatusReference()
    {
       return $this->belongsTo(CrmApproval::class, 'final_status_id', 'id');
    }

    public function CreatedBy()
    {
       return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function RateWarehousingHasMany()
    {
       return $this->hasMany(CrmRateWarehousingDetails::class, 'rate_id', 'id');
    }
}
