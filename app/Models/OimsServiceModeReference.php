<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsServiceModeReference extends Model
{
    use HasFactory;

    protected $table = "oims_service_mode_reference";

    protected $fillable = [
        'name',
        'code',
        'transport_mode_id',
        'status',
    ];

    public function SMTransportHasOne()
    {
        return $this->belongsTo(OimsTransportReference::class, 'transport_mode_id', 'id');
    }
}
