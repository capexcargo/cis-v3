<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsAgent extends Model
{
    use HasFactory;

    protected $table = "oims_agent";

    protected $fillable = [
        'name',
    ];
}
