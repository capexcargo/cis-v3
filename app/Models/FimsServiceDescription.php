<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsServiceDescription extends Model
{
    use HasFactory;
    protected $table = "acctng_service_description_reference";

    protected $fillable = [
        'service_category_id',
        'description',
        'price',
        'status',
    ];

    public function ServCat(){
        return $this->belongsTo(FimsServiceCategory::class, 'service_category_id', 'id');
    }
}
