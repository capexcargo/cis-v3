<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntryCustomerContPerson extends Model
{
    use HasFactory;
    protected $table = "oims_transaction_entry_customer_contact_person";

    protected $fillable = [
        'account_id',
        'first_name',
        'middle_name',
        'last_name',
        'position',
        'is_primary',
    ];
}
