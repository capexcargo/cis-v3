<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmQuotationShipmentDimensions extends Model
{
    use HasFactory;
    protected $table = "crm_shipment_dimensions";

    protected $fillable = [
        'quotation_id',
        'quantity',
        'weight',
        'length',
        'width',
        'height',
        'unit_of_measurement_id',
        'measurement_type_id',
        'type_of_packaging_id',
        'is_for_crating',
        'crating_type_id',
        'pouch_box_size',
        'pouch_box_amount',
        'container',
    ];
}
