<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmAccountTypeReference extends Model
{
    use HasFactory;

    protected $table = "crm_account_type";

    protected $fillable = [
        'name'
    ];

    public function customers()
    {
        return $this->hasMany(CrmCustomerInformation::class, 'account_type', 'id');
    }
}
