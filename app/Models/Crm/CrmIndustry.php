<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmIndustry extends Model
{
    use HasFactory;

    protected $table = "crm_industry";
    
    public function customers()
    {
        return $this->hasMany(CrmCustomerInformation::class, 'industry_id', 'id');
    }
}
