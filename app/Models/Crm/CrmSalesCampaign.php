<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmSalesCampaign extends Model
{
    use HasFactory;

    protected $table= "crm_sales_campaign";
    public $timestamps = false;


    protected $fillable = [
        'reference_no',
        'name',
        'voucher_code',
        'discount_type',
        'discount_amount',
        'discount_percentage',
        'start_datetime',
        'end_datetime',
        'maximum_usage',
        'terms_conditon',
        'status',
    ];

    public function Salesattachments()
    {
        return $this->hasMany(CrmSalesCampaignAttachment::class, 'sales_campaign_id', 'id');
    }

}
