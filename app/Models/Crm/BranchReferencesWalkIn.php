<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchReferencesWalkIn extends Model
{
    use HasFactory;

    protected $table = "branch_references_walkin";
}
