<?php

namespace App\Models\Crm;

use App\Models\CrmBooking;
use App\Models\CrmBookingShipper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CityReference extends Model
{
    use HasFactory;

    protected $table = "city";

    protected $fillable = [
        'name',
        'city_postal',
        'state_id',
        'region_id',
        'island_group_id',
        'psgcCode',
        'regDesc',
        'citymunCode',
        'status',
    ];

    public function BookingCity()
    {
        return $this->belongsTo(CrmBookingShipper::class, 'id', 'city_id');
    }

    public function BarangayCity()
    {
        return $this->hasMany(BarangayReference::class, 'city_id', 'id');
    }

    // public function ZipCity()
    // {
    //     return $this->belongsTo(CrmBookingShipper::class, 'id', 'city_id');
    // }
}
