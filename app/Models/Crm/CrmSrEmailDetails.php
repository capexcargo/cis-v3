<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmSrEmailDetails extends Model
{
    use HasFactory;

    protected $table = "crm_sr_email_details";

    protected $fillable = [
        'sr_no',
        'email_external',
        'email_internal',
        'subject',
        'body',
        'message_id',
        'email_uid',
        'references',
        'status',
        'type',
        'is_first_email',
        'is_read',
    ];

    public function srDetails()
    {
        return $this->belongsTo(CrmServiceRequest::class, 'sr_no', 'sr_no');
    }
}
