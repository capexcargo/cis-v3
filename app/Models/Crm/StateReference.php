<?php

namespace App\Models\Crm;

use App\Models\IslandGroup;
use App\Models\Region;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StateReference extends Model
{
    use HasFactory;
    protected $table = "state";

    protected $fillable = [
        'name',
        'region_id',
        'island_group_id',
        'psgcCode',
        'regCode',
        'provCode',
        'status',
    ];

    public function cityProvince()
    {
        return $this->hasMany(CityReference::class, 'state_id', 'id');
    }

    public function barangayProvince()
    {
        return $this->hasMany(BarangayReference::class, 'state_id', 'id');
    }

    public function regionProvince()
    {
        return $this->belongsTo(Region::class, 'region_id', 'id');
    }
    public function islandProvince()
    {
        return $this->belongsTo(IslandGroup::class, 'island_group_id', 'id');
    }

    // public function regionProvince()
    // {
    //     return $this->belongsTo(Region::class, 'region_id', 'id');
    // }
}
