<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmActivitiesTask extends Model
{
    use HasFactory;

    protected $table = "crm_activity_task";

    protected $fillable = [
        'customer_id',
        'title',
        'priority',
        'due_date',
        'task',
        'completed_date',
        'sr_no',
        'status',
    ];
}
