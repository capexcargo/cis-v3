<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmAncillary extends Model
{
    use HasFactory;

    protected $table = "crm_ancillary_management";

    protected $fillable = [
        'name',
        'charges_amount',
        'charges_rate',
        'description',
        'status',
    ];
}
