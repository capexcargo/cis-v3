<?php

namespace App\Models\Crm;

use App\Models\OimsZipcodeReference;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BarangayReference extends Model
{
    use HasFactory;

    protected $table = "barangay";

    protected $fillable = [
        'name',
        'state_id',
        'city_id',
        'zipcode_id',
        'region_id',
        'island_group_id',
        'status',
    ];

    public function ZipcodeReference()
    {
        return $this->belongsTo(OimsZipcodeReference::class, 'zipcode_id', 'id');
    }

    public function citybar()
    {
        return $this->belongsTo(CityReference::class, 'city_id', 'id');
    }
}
