<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmCustomerStatusReferences extends Model
{
    use HasFactory;

    protected $table = "crm_customer_status_references";

    public function customers()
    {
        return $this->hasMany(CrmCustomerInformation::class, 'status', 'id');
    }
}
