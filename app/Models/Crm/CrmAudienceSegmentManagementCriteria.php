<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmAudienceSegmentManagementCriteria extends Model
{
    use HasFactory;

    protected $table = "crm_audience_segment_management_criteria";
    protected $fillable = [
        'audience_id',
        'criteria_id',
        'criteria_name',
        'onboarded_date_from',
        'onboarded_date_to',
    ];
    public function audience()
    {
        return $this->belongsTo(CrmAudienceSegmentManagement::class, 'audience_id', 'id');
    }
}
