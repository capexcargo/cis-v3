<?php

namespace App\Models\Crm;

use App\Models\OimsAreaReference;
use App\Models\OimsTeamReference;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchReferencesQuadrant extends Model
{
    use HasFactory;

    protected $table = "branch_references_quadrant";

    protected $fillable = [
        'name',
        'branch_reference_id',
        'quadrant',
        'status',
    ];

    public function AreaReference()
    {
        return $this->hasMany(OimsAreaReference::class, 'quadrant_id', 'id');
    }

    public function TeamReference()
    {
        return $this->hasMany(OimsTeamReference::class, 'quadrant_id', 'id');
    }
    
}
