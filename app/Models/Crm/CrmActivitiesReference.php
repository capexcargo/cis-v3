<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmActivitiesReference extends Model
{
    use HasFactory;

    protected $table = "crm_activities_references";

    protected $fillable = [
        'name',
    ];

    public function ServiceRequestget()
    {
        return $this->hasManyThrough(
            CrmServiceRequest::class,
            CrmActivities::class,
            'activity_type_id', # CrmActivities.activity_type_id = 3rd->id
            'sr_no', #  CrmServiceRequest.sr_no = 4th->sr_no (CrmActivities)
            'id', # ID OF REFERENCE OR SEEDER -> CrmActivitiesReference
            'sr_no' #CrmActivities.sr_no = 2nd->sr_no
        );
    }
}
