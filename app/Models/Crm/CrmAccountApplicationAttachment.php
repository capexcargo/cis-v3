<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmAccountApplicationAttachment extends Model
{
    use HasFactory;
    protected $table = "crm_account_application_attachment";

    protected $fillable = [
        'account_info_id',
        'application_requirement_id',
        'path',
        'name',
        'extension',
    ];
   
}
