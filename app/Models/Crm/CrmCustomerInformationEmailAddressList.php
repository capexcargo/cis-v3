<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmCustomerInformationEmailAddressList extends Model
{
    use HasFactory;

    protected $table = "crm_email_address_list";
    
    protected $fillable = [
        'contact_person_id',
        'account_type',
        'account_id',
        'email',
        'is_primary',
    ];
}
