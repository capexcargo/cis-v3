<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmQuotationShipmentCharges extends Model
{
    use HasFactory;
    protected $table = "crm_shipment_charges";

    protected $fillable = [
        'quotation_id',
        'weight_charge',
        'awb_fee',
        'valuation',
        'cod_charge',
        'insurance',
        'handling_fee',
        'doc_fee',
        'is_other_fee',
        'total_other_fee',
        'oda_fee',
        'opa_fee',
        'equipment_rental',
        'lashing',
        'manpower',
        'dangerous_goods_fee',
        'trucking',
        'perishable_fee',
        'packaging_fee',
        'discount_amount',
        'discount_percentage',
        'subtotal',
        'evat',
        'is_evat',
        'grandtotal'
    ];
}
