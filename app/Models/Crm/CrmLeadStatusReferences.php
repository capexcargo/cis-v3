<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmLeadStatusReferences extends Model
{
    use HasFactory;

    protected $table = "crm_lead_status";
}
