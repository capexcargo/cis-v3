<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmQuotaCsmDistribution extends Model
{
    use HasFactory;
    
    protected $table = "crm_quota_csm_distribution_type";

    protected $fillable = [
        'csm_user_id',
        'type_id',
        'distribution_pct',
        'quota_amount',
    ];
}
