<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmOpportunityAttachments extends Model
{
    use HasFactory;

    protected $table = "crm_opportunity_attachment";

    protected $fillable = [
        'opportunity_id',
        'path',
        'name',
        'extension',
    ];
}
