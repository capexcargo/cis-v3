<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmActivitiesNotes extends Model
{
    use HasFactory;

    protected $table = "crm_activity_notes";

    protected $fillable = [
        'customer_id',
        'title',
        'note',
        'sr_no',
    ];
}
