<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmStakeholderSearchMgmtReferences extends Model
{
    use HasFactory;

    protected $table = "crm_stakeholder_search_management_reference";

    public function subcategories()
    {
        return $this->hasMany(StakeholderCategory::class, 'management_id', 'id');
    }
}
