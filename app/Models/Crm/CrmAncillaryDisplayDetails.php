<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmAncillaryDisplayDetails extends Model
{
    use HasFactory;

    protected $table = "crm_ancillary_display_details";

    protected $fillable = [
        'ancillary_display_id',
        'ancillary_charge_id',
    ];

    public function AncillaryDisplay()
    {
        return $this->belongsTo(CrmAncillaryDisplay::class, 'ancillary_display_id', 'id');
    }

    public function AncillaryCharge()
    {
        return $this->belongsTo(CrmAncillary::class, 'ancillary_charge_id', 'id');
    }
}
