<?php

namespace App\Models\Crm\CustomerInformation;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmCusInformationInitiatedCall extends Model
{
    use HasFactory;

    protected $table = "crm_customer_information_initiated_call";

    protected $fillable = [
        'customer_number',
        'mobile_number',
        'CallSid',
        'CallStatus',
    ];
}
