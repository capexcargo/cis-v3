<?php

namespace App\Models\Crm;

use App\Models\User;
use CreateCrmIndustry;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmCustomerInformation extends Model
{
    use HasFactory;

    protected $table = "crm_customer_information";

    protected $fillable = [
        'account_type',
        'account_no',
        'fullname',
        'company_name',
        'first_name',
        'middle_name',
        'last_name',
        'company_link',
        'company_email_address',
        'company_tel_no',
        'company_mobile_no',
        'industry_id',
        'life_stage_id',
        'is_mother_account',
        'child_account_id',
        'contact_owner_id',
        'tin',
        'company_anniversary',
        'marketing_channel_id',
        'onboarding_id',
        'notes',
        'customer_type',
        'status',
        'created_by',
        'approval_date',
        'approver1_id',
        'approver2_id',
        'approver3_id',
        'approver4_id',
        'approver1_status',
        'approver2_status',
        'approver3_status',
        'approver4_status',
        'approver1_remarks',
        'approver2_remarks',
        'approver3_remarks',
        'approver4_remarks',
        'approver1_date',
        'approver2_date',
        'approver3_date',
        'approver4_date',
        'final_status',
        'rate_name',
        

    ];

    public function accountType()
    {
        return $this->belongsTo(CrmAccountTypeReference::class, 'account_type', 'id');
    }

    public function industry()
    {
        return $this->belongsTo(CrmIndustry::class, 'industry_id', 'id');
    }

    public function subAccount()
    {
        return $this->belongsTo(CrmCustomerInformation::class, 'child_account_id', 'id');
    }

    public function lifeStage()
    {
        return $this->belongsTo(CrmLifeStage::class, 'life_stage_id', 'id');
    }

    public function contactOwner()
    {
        return $this->belongsTo(User::class, 'contact_owner_id', 'id');
    }

    public function marketingChannel()
    {
        return $this->belongsTo(CrmMarketingChannel::class, 'marketing_channel_id', 'id');
    }

    public function onboardingChannel()
    {
        return $this->belongsTo(CrmOnboardingChannelReference::class, 'onboarding_id', 'id');
    }

    public function statusRef()
    {
        return $this->belongsTo(CrmCustomerStatusReferences::class, 'status', 'id');
    }

    public function lead()
    {
        return $this->belongsTo(CrmLead::class, 'id', 'account_id');
    }

    public function serviceRequest()
    {
        return $this->belongsTo(CrmServiceRequest::class, 'id', 'account_id');
    }

    public function contactPersons()
    {
        return $this->hasMany(CrmCustomerInformationContactPerson::class, 'account_id', 'id');
    }

    public function emails()
    {
        return $this->hasMany(CrmCustomerInformationEmailAddressList::class, 'account_id', 'id');
    }

    public function mobileNumbers()
    {
        return $this->hasMany(CrmCustomerInformationMobileList::class, 'account_id', 'id');
    }

    public function telephoneNumbers()
    {
        return $this->hasMany(CrmCustomerInformationTelephoneList::class, 'account_id', 'id');
    }

    public function addresses()
    {
        return $this->hasMany(CrmCustomerInformationAddressList::class, 'account_id', 'id');
    }

    public function appAttachment()
    {
        return $this->hasMany(CrmAccountApplicationAttachment::class, 'id', 'account_info_id');
    }
}
