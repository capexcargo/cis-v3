<?php

namespace App\Models\Crm;

use App\Models\MonthReference;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmQuotaManagementStakeholder extends Model
{
    use HasFactory;

    protected $table = "crm_quota_management_stakeholder";

    protected $fillable = [
        'stakeholder_category_name',
        'subcategory',
        'management_id',
        'account_type',
        'month',
        'year',
        'distribution_pct',
        'amount',
        'total_days_count',
        'monthly_target_amount',
    ];


    public function accountTypeBT()
    {
        return $this->belongsTo(CrmQuotaAccountType::class, 'subcategory', 'id');
    }

    public function nlslBT()
    {
        return $this->belongsTo(BranchReference::class, 'subcategory', 'id');
    }

    public function quadrantBT()
    {
        return $this->belongsTo(BranchReferencesQuadrant::class, 'subcategory', 'id');
    }

    public function walkinBT()
    {
        return $this->belongsTo(BranchReferencesWalkIn::class, 'subcategory', 'id');
    }

    public function provincialBT()
    {
        return $this->belongsTo(BranchReference::class, 'subcategory', 'id');
    }

    public function channelSourceBT()
    {
        return $this->belongsTo(ChannelSrSource::class, 'subcategory', 'id');
    }

    public function accountTypeReference()
    {
        return $this->belongsTo(CrmQuotaAccountType::class, 'account_type', 'id');
    }

    public function accountType()
    {
        return $this->hasMany(CrmQuotaAccountType::class, 'id', 'subcategory');
    }

    public function month()
    {
        return $this->belongsTo(MonthReference::class, 'month', 'id');
    }

    public function nlsl()
    {
        return $this->hasMany(BranchReference::class, 'id', 'subcategory');
    }

    public function quadrant()
    {
        return $this->hasMany(BranchReferencesQuadrant::class, 'id', 'subcategory');
    }

    public function walkin()
    {
        return $this->hasMany(BranchReferencesWalkIn::class, 'id', 'subcategory');
    }

    public function provincial()
    {
        return $this->hasMany(BranchReference::class, 'id', 'subcategory');
    }

    public function channelSource()
    {
        return $this->hasMany(ChannelSrSource::class, 'id', 'subcategory');
    }
}
