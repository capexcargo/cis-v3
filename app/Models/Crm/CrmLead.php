<?php

namespace App\Models\Crm;

use App\Models\ChannelSrSource;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmLead extends Model
{
    use HasFactory;

    protected $table = "crm_leads";

    protected $fillable = [
        'sr_no',
        'account_id',
        'lead_name',
        'shipment_type_id',
        'service_requirement_id',
        'industry_id', //nullable
        'lead_status_id', //nullable
        'retirement_reason_id', //nullable
        'retirement_reason_remarks', //nullable
        'qualification_score', //nullable
        'lead_classification_id', //nullable
        'description',
        'contact_owner_id',
        'customer_type_id',
        'channel_source_id',
        'currency',
        'deal_size', //nullable
        'created_by',
    ];

    public function serviceRequest()
    {
        return $this->belongsTo(CrmServiceRequest::class, 'sr_no', 'sr_no');
    }

    public function shipmentType()
    {
        return $this->belongsTo(CrmMovementSfTypeReferences::class, 'shipment_type_id', 'id');
    }

    public function leadStatus()
    {
        return $this->belongsTo(CrmLeadStatusReferences::class, 'lead_status_id', 'id');
    }

    public function classification()
    {
        return $this->belongsTo(CrmLeadClassification::class, 'lead_classification_id', 'id');
    }

    public function contactOwner()
    {
        return $this->belongsTo(User::class, 'contact_owner_id', 'id');
    }

    public function customerType()
    {
        return $this->belongsTo(CrmAccountTypeReference::class, 'customer_type_id', 'id');
    }

    public function channelSource()
    {
        return $this->belongsTo(ChannelSrSource::class, 'channel_source_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function account()
    {
        return $this->belongsTo(CrmCustomerInformation::class, 'account_id', 'id');
    }

    public function opportunity()
    {
        return $this->belongsTo(CrmOpportunity::class, 'sr_no', 'sr_no');
    }
}
