<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmQuotaAccountType extends Model
{
    use HasFactory;

    protected $table = "crm_quota_account_type";
}
