<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SrStatusReferences extends Model
{
    use HasFactory;

    protected $table = "crm_sr_status_references";
}
