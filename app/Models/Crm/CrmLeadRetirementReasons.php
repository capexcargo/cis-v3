<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmLeadRetirementReasons extends Model
{
    use HasFactory;

    protected $table = "crm_lead_retirement_reasons";
}
