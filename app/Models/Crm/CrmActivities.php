<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmActivities extends Model
{
    use HasFactory;

    protected $table = "crm_activities";

    protected $fillable = [
        'employee_id',
        'activity_type_id',
        'account_id',
        'description',
        'sr_no',
        'quotation_reference_no',
        'status',
        'datetime_completed',
    ];

    public function ServiceRequest()
    {
        return $this->hasMany(CrmServiceRequest::class, 'sr_no', 'sr_no');
    }
}
