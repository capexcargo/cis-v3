<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmAncillaryDisplay extends Model
{
    use HasFactory;

    protected $table = "crm_ancillary_display_management";

    protected $fillable = [
        'name',
        'status',
    ];

    public function AncillaryDisplayDetails()
    {
        return $this->hasMany(CrmAncillaryDisplayDetails::class, 'ancillary_display_id', 'id');
    }
}
