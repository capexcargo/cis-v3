<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmSrAttachments extends Model
{
    use HasFactory;
    
    protected $table = "crm_sr_attachments";
    
    protected $fillable = [
        'sr_id',
        'path',
        'name',
        'extension',
        'original_file_name'
    ];
    
    public function serviceRequest()
    {
        return $this->belongsTo(CrmServiceRequest::class, 'sr_id', 'id');
    }
}
