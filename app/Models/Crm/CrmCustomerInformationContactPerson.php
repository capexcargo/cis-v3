<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmCustomerInformationContactPerson extends Model
{
    use HasFactory;

    protected $table = "crm_contact_person";

    protected $fillable = [
        'account_id',
        'first_name',
        'middle_name',
        'last_name',
        'position',
        'is_primary',
    ];

    public function conPerEmails()
    {
        return $this->hasMany(CrmCustomerInformationEmailAddressList::class, 'contact_person_id', 'id');
    }

    public function conPerTelephoneNumbers()
    {
        return $this->hasMany(CrmCustomerInformationTelephoneList::class, 'contact_person_id', 'id');
    }

    public function conPerMobileNumbers()
    {
        return $this->hasMany(CrmCustomerInformationMobileList::class, 'contact_person_id', 'id');
    }
}
