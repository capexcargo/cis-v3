<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmCustomerInformationTelephoneList extends Model
{
    use HasFactory;

    protected $table = "crm_telephone_list";

    protected $fillable = [
        'contact_person_id',
        'account_type',
        'account_id',
        'telephone',
        'is_primary',
    ];
}
