<?php

namespace App\Models\Crm;

use App\Models\CrmOpportunityStatusMgmt;
use App\Models\CrmSalesStage;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmOpportunity extends Model
{
    use HasFactory;

    protected $table = "crm_opportunity";

    protected $fillable = [
        'sr_no',
        'account_id',
        'opportunity_name',
        'industry_id',
        'sales_stage_id',
        'opportunity_status_id',
        'contact_owner_id',
        'deal_size',
        'actual_amount',
        'booking_reference',
        'lead_conversion_date',
        'close_date',
        'created_by',
    ];

    public function lead()
    {
        return $this->belongsTo(CrmLead::class, 'sr_no', 'sr_no');
    }

    public function sr()
    {
        return $this->belongsTo(CrmServiceRequest::class, 'sr_no', 'sr_no');
    }

    public function industry()
    {
        return $this->belongsTo(CrmIndustry::class, 'industry_id', 'id');
    }

    public function saleStage()
    {
        return $this->belongsTo(CrmSalesStage::class, 'sales_stage_id', 'id');
    }

    public function opportunityStatus()
    {
        return $this->belongsTo(CrmOpportunityStatusMgmt::class, 'opportunity_status_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function account()
    {
        return $this->belongsTo(CrmCustomerInformation::class, 'account_id', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(CrmOpportunityAttachments::class, 'opportunity_id', 'id');
    }
}
