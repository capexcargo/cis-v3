<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmSalesCampaignAttachment extends Model
{
    use HasFactory;

    protected $table = "crm_sales_campaign_attachment";
    
    protected $fillable = [
        'sales_campaign_id',
        'path',
        'name',
        'extension',
    ];

    public function salesCampaign()
    {
        return $this->belongsTo(CrmSalesCampaign::class, 'sales_campaign_id', 'id');
    }
}
