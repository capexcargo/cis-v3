<?php

namespace App\Models\Crm;

use App\Models\BranchReference;
use App\Models\CrmModeOfPaymentReference;
use App\Models\CrmServiceMode;
use App\Models\CrmTransportMode;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmQuotationShipmentDetails extends Model
{
    use HasFactory;
    protected $table = "crm_shipment_details";

    protected $fillable = [
        'quotation_id',
        'origin_id',
        'destination_id',
        'origin_address',
        'destination_address',
        'transhipment_id',
        'transport_mode_id',
        'commodity_type_id',
        'commodity_applicable_rate_id',
        'paymode_id',
        'servicemode_id',
        'service_type_id',
        'sea_shipment_type',
        'total_cbm',
        'total_chgwt',
        'total_amount',
    ];

    public function origin()
    {
        return $this->belongsTo(BranchReference::class, 'origin_id', 'id');
    }

    public function destination()
    {
        return $this->belongsTo(BranchReference::class, 'destination_id', 'id');
    }

    public function transportMode()
    {
        return $this->belongsTo(CrmTransportMode::class, 'transport_mode_id', 'id');
    }

    public function serviceMode()
    {
        return $this->belongsTo(CrmServiceMode::class, 'servicemode_id', 'id');
    }
    
    public function payMode()
    {
        return $this->belongsTo(CrmModeOfPaymentReference::class, 'paymode_id', 'id');
    }
}
