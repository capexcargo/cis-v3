<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmSrQualificationQuestion extends Model
{
    use HasFactory;

    protected $table = "crm_sr_qualification_question";

    protected $fillable = [
        'lead_id',
        'sr_no',
        'question_id',
        'response',
        'remarks',
    ];

    public function lead()
    {
        return $this->belongsTo(CrmLead::class, 'lead_id', 'id');
    }

    
    public function questions()
    {
        return $this->belongsTo(CrmQualificationQuestionnaire::class, 'question_id', 'id');
    }
}
