<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChargesManagement extends Model
{
    use HasFactory;
    
    protected $table = "charges_management";

    protected $fillable = [
        'rate_calcu_charges',
        'charges_category',
        'status',
    ];
}
