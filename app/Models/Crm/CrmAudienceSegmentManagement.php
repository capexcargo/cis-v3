<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmAudienceSegmentManagement extends Model
{
    use HasFactory;
    protected $table = "crm_audience_segment_management";
    protected $fillable = [
        'name',
        'filter_id',
        'onboarded_date_from',
        'onboarded_date_to',
        'status_id',
    ];
    function criteria(){
        return $this->hasMany(CrmAudienceSegmentManagementCriteria::class, 'audience_id', 'id');
       }
}
