<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmQualificationQuestionnaire extends Model
{
    use HasFactory;

    protected $table = "crm_qualification_question_management";

    protected $fillable = [
        'question',
        'qualification_type',
    ];

    public function qualifications()
    {
        return $this->hasMany(CrmSrQualificationQuestion::class, 'question_id', 'id');
    }
}
