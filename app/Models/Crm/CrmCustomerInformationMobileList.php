<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmCustomerInformationMobileList extends Model
{
    use HasFactory;

    protected $table = "crm_mobile_list";
    
    protected $fillable = [
        'contact_person_id',
        'account_type',
        'account_id',
        'mobile',
        'is_primary',
    ];
}
