<?php

namespace App\Models\Crm;

use App\Models\CrmShipmentType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmQuotation extends Model
{
    use HasFactory;
    protected $table = "crm_quotation";

    protected $fillable = [
        'reference_no',
        'account_id',
        'sr_no',
        'subject',
        'shipment_type_id',
        'assigned_to',
        'is_save',
    ];

    public function shipmentType()
    {
        return $this->belongsTo(CrmShipmentType::class, 'shipment_type_id', 'id');
    }

    // public function customer()
    // {
    //     return $this->belongsTo(CrmServiceRequest::class, 'sr_no', 'sr_no');
    // }

    public function customerDetails()
    {
        return $this->belongsTo(CrmServiceRequest::class, 'account_id', 'account_id');
    }

    public function shipmentDetails()
    {
        return $this->hasOne(CrmQuotationShipmentDetails::class, 'quotation_id', 'id');
    }

    public function shipmentDimensions()
    {
        return $this->hasOne(CrmQuotationShipmentDimensions::class, 'quotation_id', 'id');
    }

    public function shipmentCharges()
    {
        return $this->hasOne(CrmQuotationShipmentCharges::class, 'quotation_id', 'id');
    }

    public function activities()
    {
        return $this->hasOne(CrmActivities::class, 'sr_no', 'sr_no');
    }
}
