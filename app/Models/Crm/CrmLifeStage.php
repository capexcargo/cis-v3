<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmLifeStage extends Model
{
    use HasFactory;

    protected $table = "crm_life_stage";

    protected $fillable = [
        'name',
    ];

    public function customers()
    {
        return $this->hasMany(CrmCustomerInformation::class, 'life_stage_id', 'id');
    }
}
