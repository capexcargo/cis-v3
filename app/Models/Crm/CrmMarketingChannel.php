<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmMarketingChannel extends Model
{
    use HasFactory;

    protected $table = "crm_marketing_channel";
}
