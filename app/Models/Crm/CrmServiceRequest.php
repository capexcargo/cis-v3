<?php

namespace App\Models\Crm;

use App\Models\ChannelSrSource;
use App\Models\MileResolution;
use App\Models\MileResponse;
use App\Models\ServiceRequirements;
use App\Models\SrType;
use App\Models\SrTypeSubcategory;
use App\Models\User;
use CreateCrmMilestoneResolution;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmServiceRequest extends Model
{
    use HasFactory;

    protected $table = "crm_service_request";

    protected $fillable = [
        'sr_no',
        'subject',
        'customer',
        'account_id',
        'service_request_type_id',
        'severity_id',
        'sub_category_id',
        'sub_category_reference',
        'service_requirement_id',
        'assigned_to_id',
        'channel_id',
        'status_id',
        'life_stage_id',
        'description',
        'created_by',
        'updated_by',
    ];
    
    
    public function severityReferences()
    {
        return $this->belongsTo(MileResolution::class, 'severity_id', 'id');
    }
    

    public function activities()
    {
        return $this->belongsTo(CrmActivities::class, 'sr_no', 'sr_no');
    }

    public function customerCorpDetails()
    {
        return $this->belongsTo(CrmCustomerInformation::class, 'customer', 'id');
    }

    public function customerIndDetails()
    {
        return $this->belongsTo(CrmCustomerInformation::class, 'account_id', 'id');
    }

    public function srType()
    {
        return $this->belongsTo(SrType::class, 'service_request_type_id', 'id');
    }

    public function subCategory()
    {
        return $this->belongsTo(SrTypeSubcategory::class, 'sub_category_id', 'id');
    }

    public function serviceRequirement()
    {
        return $this->belongsTo(ServiceRequirements::class, 'service_requirement_id', 'id');
    }

    public function assignedTo()
    {
        return $this->belongsTo(User::class, 'assigned_to_id', 'id');
    }

    public function channelSource()
    {
        return $this->belongsTo(ChannelSrSource::class, 'channel_id', 'id');
    }

    public function lifeStage()
    {
        return $this->belongsTo(CrmLifeStage::class, 'life_stage_id', 'id');
    }

    public function statusRef()
    {
        return $this->belongsTo(SrStatusReferences::class, 'status_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function srEmailDetails()
    {
        return $this->hasMany(CrmSrEmailDetails::class, 'sr_no', 'sr_no');
    }

    public function attachments()
    {
        return $this->hasMany(CrmSrAttachments::class, 'sr_id', 'id');
    }
}
