<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmOnboardingChannelReference extends Model
{
    use HasFactory;

    protected $table = "crm_onboarding_channel_reference";
}
