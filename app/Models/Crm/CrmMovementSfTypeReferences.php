<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmMovementSfTypeReferences extends Model
{
    use HasFactory;

    protected $table = "crm_movement_sea_freight_type_reference";
}
