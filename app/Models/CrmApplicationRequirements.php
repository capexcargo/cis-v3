<?php

namespace App\Models;

use App\Models\Crm\CrmAccountApplicationAttachment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmApplicationRequirements extends Model
{
    use HasFactory;

    protected $table = "crm_application_requirements";

    protected $fillable = [
        'name',
        'status',
    ];

    public function appRequirements()
    {
        return $this->belongsTo(CrmAccountApplicationAttachment::class, 'id', 'application_requirement_id');
    }

}
