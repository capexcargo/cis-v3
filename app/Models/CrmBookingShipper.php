<?php

namespace App\Models;

use App\Models\Crm\CityReference;
use App\Models\Crm\CrmAccountTypeReference;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmBookingShipper extends Model
{
    use HasFactory;
    protected $table = "crm_booking_shipper";

    protected $fillable = [
        'booking_id',
        'customer_no',
        'account_type_id',
        'company_name',
        'name',
        'first_name',
        'middle_name',
        'last_name',
        'mobile_number',
        'email_address',
        'address',
        'state_id',
        'city_id',
        'barangay_id',
        'postal_id',
    ];

    public function BookingTypeDetailsSP()
    {
       return $this->belongsTo(CrmBooking::class, 'booking_id', 'id');
    }

    public function BookingTypeDetailsSPs()
    {
       return $this->belongsTo(CrmBookingHistory::class, 'booking_id', 'id');
    }

    public function AccountTypeReferenceSP()
    {
       return $this->belongsTo(CrmAccountTypeReference::class, 'account_type_id', 'id');
    }

    public function ShipperCity()
    {
       return $this->belongsTo(CityReference::class, 'city_id', 'id');
    }
}
