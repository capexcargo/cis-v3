<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntryItemCharges extends Model
{
    use HasFactory;

    protected $table = "oims_transaction_entry_item_charges";

    protected $fillable = [
        'waybill_id',
        'weight_charge',
        'awb_fee',
        'declared_value',
        'valuation',
        'cod_charge',
        'insurance',
        'handling_fee',
        'doc_fee',
        'is_others_fee',
        'opa_fee',
        'oda_fee',
        'crating_fee',
        'equipment_rental',
        'lashing',
        'manpower',
        'dangerous_goods_fee',
        'trucking',
        'perishable_fee',
        'packaging_fee',
        'subtotal',
        'rate_discount',
        'promo_code',
        'promo_discount',
        'evat',
        'grand_total',
    ];
}
