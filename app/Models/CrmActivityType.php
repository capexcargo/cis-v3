<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmActivityType extends Model
{
    use HasFactory;
    protected $table = "crm_activity_type_management";

    protected $fillable = [
        'activity_type',
        'pickup_execution_time_id',
        'delivery_execution_time_id',
        'status',
    ];

    public function PickupReference()
    {
       return $this->belongsTo(CrmActivityTypePickupReference::class, 'pickup_execution_time_id', 'id');
    }

    public function DeliveryReference()
    {
       return $this->belongsTo(CrmActivityTypeDeliveryReference::class, 'delivery_execution_time_id', 'id');
    }
}
