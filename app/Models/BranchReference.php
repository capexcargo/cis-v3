<?php

namespace App\Models;

use App\Models\Hrim\AreaReference;
use CreateAreaReference;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchReference extends Model
{
    use HasFactory;

    protected $table = 'branch_reference';

    protected $fillable = [
        'code',
        'display',
        'area_type_id',
        'area_level_id',
        'area_destination_id',
        'area_box_destination_id',
        'area_id',
    ];

    public function bookings()
    {
        return $this->hasMany(CrmBooking::class, 'booking_branch_id', 'id');
    }

    public function users()
    {
        return $this->hasMany(UserDetails::class, 'branch_id', 'id');
    }

    public function areaId()
    {
        return $this->hasMany(AreaReference::class, 'id', 'area_id');
    }

    public function userByBranchId()
    {
        return $this->hasManyThrough(
            User::class,
            UserDetails::class,
            'branch_id', // Foreign key on the UserDetails table...
            'id', // connection of users to userdetails...
        );
    }
}
