<?php

namespace App\Models;

use App\Models\Crm\CrmCustomerInformation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MarketingChannel extends Model
{
    use HasFactory;

    protected $table = "crm_marketing_channel";

    protected $fillable = [
        'name',
    ];

    public function customers()
    {
        return $this->hasMany(CrmCustomerInformation::class, 'marketing_channel_id', 'id');
    }
}
