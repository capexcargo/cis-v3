<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IslandGroup extends Model
{
    use HasFactory;
    protected $table = "island_group";

    protected $fillable = [
        'name',
    ];
}
