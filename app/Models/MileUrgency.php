<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MileUrgency extends Model
{
    
    use HasFactory;
    
    protected $table = "crm_milestone_urgency_impact";

    protected $fillable = [
        'severity_id',
        'high_priority_level',
        'medium_priority_level',
        'low_priority_level',
        'status',
    ];

    public function MileUrgSev()
    {
       return $this->belongsTo(MileResolution::class, 'severity_id', 'id');
    }

    public function MileUrgHigh()
    {
       return $this->belongsTo(MileResolution::class, 'high_priority_level', 'priority_level');
    }

    public function MileUrgMid()
    {
       return $this->belongsTo(MileResolution::class, 'medium_priority_level', 'priority_level');
    }

    public function MileUrgLow()
    {
       return $this->belongsTo(MileResolution::class, 'low_priority_level', 'priority_level');
    }

}
