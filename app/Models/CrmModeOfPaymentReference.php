<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmModeOfPaymentReference extends Model
{
    use HasFactory;
    protected $table = "crm_mode_of_payment_reference";

    protected $fillable = [
        'name',
    ];
}
