<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountTypeReference extends Model
{
    use HasFactory;

    protected $table = 'account_type_reference';
    
    protected $fillable = [
        'code',
        'display',
        'is_visible',
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'account_type_id', 'id');
    }

    public function roles()
    {
        return $this->hasMany(AccountTypeRolesReference::class, 'account_type_id' , 'id');
    }
}
