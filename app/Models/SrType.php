<?php

namespace App\Models;

use App\Models\Crm\CrmServiceRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SrType extends Model
{
    use HasFactory;

    protected $table = "crm_sr_type";

    protected $fillable = [
        'name',
    ];

    public function SrSubCateg()
    {
       return $this->hasMany(SrTypeSubcategory::class, 'sr_type_id', 'id');
    }

    public function ServiceRequestsr()
    {
        return $this->hasMany(CrmServiceRequest::class, 'service_request_type_id', 'id');
    }
    
}
