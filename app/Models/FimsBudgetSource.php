<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsBudgetSource extends Model
{
    use HasFactory;
    protected $table = "acctng_budget_source_reference";

    protected $fillable = [
        'name',
        'status',
    ];
}
