<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsPurchasing extends Model
{
    use HasFactory;

    protected $table = "acctng_purchasing";

    protected $fillable = [
        'reference_no',
        'priority_level_id',
        'expected_req_start_date',
        'service_category_id',
        'expected_purchase_delivery_date',
        'purchasing_type_id',
        'requisition_final_status',
        'canvassing_final_status',
        'request_final_status',
        'order_final_status',
        'rfp_final_status',
        'check_voucher_final_status',
        'requisition_status_update_date',
        'canvassing_status_update_date',
        'request_status_update_date',
        'order_status_update_date',
        'rfp_status_update_date',
        'check_voucher_status_update_date',
        'budget_id',
        'created_by',
    ];

    public function attachments()
    {
        return $this->hasMany(FimsPurchasingServiceReqAttachment::class, 'reference_no_id', 'id');
    }

    public function Purchaseattachments()
    {
        return $this->hasMany(FimsPurchaseRequestAttachment::class, 'reference_no_id', 'id');
    }

    public function details()
    {
        return $this->hasOne(FimsPurchasingServiceReqDetails::class, 'reference_no_id', 'id');
    }

    public function ServReqDetailsPriorityy()
    {
        return $this->belongsTo(FimsPriorityLevelReference::class, 'priority_level_id', 'id');
    }

    public function ServReqDetails2()
    {
        return $this->belongsTo(FimsPurchasingServiceReqDetails::class, 'id', 'reference_no_id');
    }

    public function PurchReqDetails2()
    {
        return $this->hasMany(FimsPurchaseRequestDetails::class, 'reference_no_id', 'id');
    }

    public function PurchReqDetailsPriorityy()
    {
        return $this->belongsTo(FimsPriorityLevelReference::class, 'priority_level_id', 'id');
    }
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function budget()
    {
        return $this->hasOne(FimsBudgetManagement::class, 'id', 'budget_id');
    }
}
