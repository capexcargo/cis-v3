<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsServiceCategory extends Model
{
    use HasFactory;
    protected $table = "acctng_service_category_reference";

    protected $fillable = [
        'name',
        'is_recurring',
        'budget_source_id',
        'opex_category_id',
        'chart_of_acccounts_id',
        'sub_accounts_id',
        'status',
    ];

    public function ScbudgetSrc(){
        return $this->belongsTo(FimsBudgetSource::class, 'budget_source_id', 'id');
    }
    public function ScopexCat(){
        return $this->belongsTo(FimsOpexCategory::class, 'opex_category_id', 'id');
    }
    public function ScchartOfAccs(){
        return $this->belongsTo(FimsChartsOfAccounts::class, 'chart_of_acccounts_id', 'id');
    }
    public function ScsubAccs(){
        return $this->belongsTo(FimsSubAccounts::class, 'sub_accounts_id', 'id');
    }
}
