<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmSalesCoach extends Model
{
    use HasFactory;

    protected $table = "crm_sales_coach";

    protected $fillable = [
        'title',
        'description',
        'status',
        'created_by',
    ];

    public function SC()
    {
       return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function processHasMany()
    {
       return $this->hasMany(CrmSalesCoachProcess::class, 'sales_coach_id', 'id');
    }

    public function process_many()
    {
        return $this->hasManyThrough(
            CrmSalesCoachSubprocess::class,
            CrmSalesCoachProcess::class,
            'sales_coach_id', // Foreign key on the environments table...
            'sales_coach_process_id', // Foreign key on the deployments table...
        );
    }
}
