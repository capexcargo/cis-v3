<?php

namespace App\Models;

use App\Models\Crm\CrmCustomerInformation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsWaybillRegistry extends Model
{
    use HasFactory;

    protected $table = "oims_waybill_registry";

    protected $fillable = [
        'waybill_start',
        'waybill_end',
        'po_reference',
        'branch_id',
        'status',
        'issued_to',
        'issued_to_emp_id',
        'issued_to_customer_id',
        'issued_to_agent_id',
        'issued_to_om',
        'issued_to_fls',
        'issuance_status',
        'pad_no',
        'closed_date',
        'issuance_date',
        'created_by',
    ];

    public function waybillDetails()
    {
        return $this->hasMany(OimsWaybillRegistryDetails::class, 'waybill_registry_id', 'id');
    }

    public function waybillBranch()
    {
        return $this->belongsTo(OimsBranchReference::class, 'branch_id', 'id');
    }

    public function waybillEmpUser()
    {
        return $this->belongsTo(UserDetails::class, 'issued_to_emp_id', 'id');
    }

    public function waybillCustInfo()
    {
        return $this->belongsTo(CrmCustomerInformation::class, 'issued_to_customer_id', 'id');
    }

    public function waybillAgent()
    {
        return $this->belongsTo(OimsAgent::class, 'issued_to_agent_id', 'id');
    }

    public function waybillOmUser()
    {
        return $this->belongsTo(UserDetails::class, 'issued_to_om', 'id');
    }

    public function waybillFlsUser()
    {
        return $this->belongsTo(UserDetails::class, 'issued_to_fls', 'id');
    }

    public function waybillCbUser()
    {
        return $this->belongsTo(UserDetails::class, 'created_by', 'id');
    }
}
