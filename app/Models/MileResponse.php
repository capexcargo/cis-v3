<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MileResponse extends Model
{
    use HasFactory;

    protected $table = "crm_milestone_response";


    protected $fillable = [
        'email',
        'target_response_time',
       
    ];
}
