<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DivisionRolesAccess extends Model
{
    use HasFactory;

    protected $table = 'division_roles_access';

    protected $fillable = [
        'role_id',
        'access_id',
    ];

    public function accesses()
    {
        return $this->hasMany(RolesChildAccessReference::class, 'id', 'access_id');
    }
}
