<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsPriorityLevelReference extends Model
{
    use HasFactory;

    protected $table = "acctng_priority_level_reference";

    protected $fillable = [
        'name',
        'description',
        'day_min',
        'day_max',
    ];
}
