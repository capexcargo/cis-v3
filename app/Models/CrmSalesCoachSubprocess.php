<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmSalesCoachSubprocess extends Model
{
    use HasFactory;

    protected $table = "crm_sales_coach_subprocess";

    protected $fillable = [
        'process_content',
        'sales_coach_process_id',
    ];

    public function SCP()
    {
       return $this->belongsTo(CrmSalesCoachProcess::class, 'sales_coach_process_id', 'id');
    }
}
