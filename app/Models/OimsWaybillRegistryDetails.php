<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsWaybillRegistryDetails extends Model
{
    use HasFactory;

    protected $table = "oims_waybill_registry_details";

    protected $fillable = [
        'waybill_registry_id',
        'waybill',
        'branch_id',
        'status',
        'pad_no',
        'skipped_status',
        'skipped_status2',
        'skipped_status3',
        'issue_to_om_identify',
        'issue_to_checker_identify',
        'date_fls_issued',
        'date_om_issued',
        'date_checker_issued',
        'date_customer_issued',
        'date_agent_issued',
    ];

    public function waybillReg()
    {
        return $this->belongsTo(OimsWaybillRegistry::class, 'waybill_registry_id', 'id');
    }

    public function waybillBranchDet()
    {
        return $this->belongsTo(OimsBranchReference::class, 'branch_id', 'id');
    }
}
