<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmCommodityType extends Model
{
    use HasFactory;
    protected $table = "crm_commodity_type_reference";

    protected $fillable = [
        'name',
    ];

    public function LandCommodity()
    {
        return $this->belongsTo(CrmRateLandFreight::class, 'id', 'commodity_type_id');
    }

    public function AirCommodity()
    {
        return $this->belongsTo(CrmRateAirFreight::class, 'id', 'commodity_type_id');
    }

    public function AirPremiumCommodity()
    {
        return $this->belongsTo(CrmRateAirFreightPremium::class, 'id', 'commodity_type_id');
    }

    public function SeaCommodity()
    {
        return $this->belongsTo(CrmRateSeaFreight::class, 'id', 'commodity_type_id');
    }
}
