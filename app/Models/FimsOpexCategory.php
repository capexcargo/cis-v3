<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsOpexCategory extends Model
{
    use HasFactory;
    protected $table = "acctng_opex_category_reference";

    protected $fillable = [
        'name',
        'opex_name',
        'status',
    ];
}
