<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmServiceMode extends Model
{
    use HasFactory;
    protected $table = "crm_service_mode_reference";

    protected $fillable = [
        'name',
        'type',
    ];
}
