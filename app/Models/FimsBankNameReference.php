<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsBankNameReference extends Model
{
    use HasFactory;
    protected $table = "acctng_bank_name_reference";

    protected $fillable = [
        'name',
        'status',
    ];
}
