<?php

namespace App\Models;

use App\Models\Crm\CrmLead;
use App\Models\Crm\CrmServiceRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceRequirements extends Model
{
    use HasFactory;

    protected $table = "crm_service_requirements";

    protected $fillable = [
        'name',
    ];

    public function ServRequirements()
    {
        return $this->belongsTo(CrmServiceRequest::class, 'id', 'service_requirement_id');
    }

    public function leads()
    {
        return $this->hasMany(CrmLead::class, 'service_requirement_id', 'id');
    }
}
