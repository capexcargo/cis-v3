<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DivisionRolesReference extends Model
{
    use HasFactory;

    protected $table = 'division_roles_reference';

    protected $fillable = [
        'division_id',
        'level_id',
        'code',
        'display',
        'is_visible',
    ];

    public function level(){
        return $this->belongsTo(LevelReference::class, 'level_id', 'id');
    }

    public function division(){
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function accesses()
    {
        return $this->hasMany(DivisionRolesAccess::class, 'role_id', 'id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'role_id', 'id');
    }
}
