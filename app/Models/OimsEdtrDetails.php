<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsEdtrDetails extends Model
{
    use HasFactory;
    protected $table = "oims_edtr_details";

    protected $fillable = [
        'edtr_id',
        'edtr_item_id',
        'edtr_item_type',
        'estimated_time_travel',
        'estimated_time_activity',
        'actual_start_travel_time',
        'actual_end_travel_time',
        'pickup_confirmed_time',
        'pickup_start_time',
        'pickup_end_time',
        'delivery_start_time',
        'delivery_end_time',
        'delivery_status',
        'completed_date',
    ];

    public function edtrref()
    {
        return $this->belongsTo(OimsEdtr::class, 'edtr_id', 'id');
    }
    public function edtrbook()
    {
        return $this->belongsTo(CrmBooking::class, 'edtr_item_id', 'id');
    }

    public function edtrtransaction()
    {
        return $this->belongsTo(OimsTransactionEntry::class, 'edtr_item_id', 'id');
    }
}
