<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OriginDestinationPortReference extends Model
{
    use HasFactory;

    protected $table = "oims_origin_destination_port_reference";

    protected $fillable = [
        'name',
        'status',
    ];
}
