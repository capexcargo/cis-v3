<?php

namespace App\Models;

use Database\Seeders\Fims\DataManagement\FimsPurchasingTypeSeeder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsPurchasingLoaMgmt extends Model
{
    use HasFactory;
    protected $table = "acctng_purchasing_loa_management";

    protected $fillable = [
        'purchasing_type_id',
        'division_id',
        'min_amount',
        'max_amount',
        'approval1_id',
        'approval2_id',
        'approval3_id',
    ];

    public function PurTypeRef(){
        return $this->belongsTo(FimsPurchasingTypeReference::class, 'purchasing_type_id', 'id');
    }

    public function BudgetRef(){
        return $this->belongsTo(FimsBudgetSource::class, 'division_id', 'id');
    }

    public function App1Ref(){
        return $this->belongsTo(User::class, 'approval1_id', 'id');
    }

    public function App2Ref(){
        return $this->belongsTo(User::class, 'approval2_id', 'id');
    }

    public function App3Ref(){
        return $this->belongsTo(User::class, 'approval3_id', 'id');
    }
}
