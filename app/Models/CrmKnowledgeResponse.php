<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmKnowledgeResponse extends Model
{
    use HasFactory;

    protected $table = "crm_knowledge_response";

    protected $fillable = [
        'knowledge_id',
        'response_area',
    ];

    public function resp()
    {
       return $this->belongsTo(CrmKnowledge::class, 'knowledge_id', 'id');
    }
}
