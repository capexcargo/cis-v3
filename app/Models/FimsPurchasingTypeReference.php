<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsPurchasingTypeReference extends Model
{
    use HasFactory;
    protected $table = "acctng_purchasing_type_reference";

    protected $fillable = [
        'name',
    ];
}
