<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsOdaOpaReference extends Model
{
    use HasFactory;

    protected $table = "oims_oda_opa_reference";

    protected $fillable = [
        'name',
        'status',
    ];
}
