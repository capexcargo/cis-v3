<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RouteCategoryReference extends Model
{
    use HasFactory;

    protected $table = "oims_route_category_reference";

    protected $fillable = [
        'name',
        'status',
    ];

    public function teamDetails()
    {
        return $this->hasMany(OimsTeamRouteAssignmentDetails::class, 'route_category_id', 'id');
    }
}
