<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmPouchType extends Model
{
    use HasFactory;

    protected $table = "crm_rate_pouch_type";

    protected $fillable = [
        'name',
    ];
}
