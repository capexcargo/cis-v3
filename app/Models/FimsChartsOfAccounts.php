<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsChartsOfAccounts extends Model
{
    use HasFactory;
    protected $table = "acctng_chart_of_accounts_reference";

    protected $fillable = [
        'name',
        'account_code',
        'status',
    ];
}
