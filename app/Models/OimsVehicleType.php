<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsVehicleType extends Model
{
    use HasFactory;

    protected $table = "oims_vehicle_type";

    protected $fillable = [
        'name',
    ];
    
}
