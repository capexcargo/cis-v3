<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTeamRouteAssignmentDetails extends Model
{
    use HasFactory;

    protected $table = "oims_team_route_assignment_details";

    protected $fillable = [
        'team_route_id',
        'team_id',
        'plate_no_id',
        'vehicle_type',
        'route_category_id',
        'driver_id',
        'checker1_id',
        'checker2_id',
        'dispatch_date',
        'remarks',
    ];

    public function EDTRReference()
    {
        return $this->hasMany(OimsEdtr::class, 'tra_id', 'id');
    }

    public function teamRouteAssignmentReference()
    {
        return $this->belongsTo(OimsTeamRouteAssignment::class, 'team_route_id', 'id');
    }

    public function teamIdReference()
    {
        return $this->belongsTo(OimsTeamReference::class, 'team_id', 'id');
    }

    public function teamRouteReference()
    {
        return $this->belongsTo(RouteCategoryReference::class, 'route_category_id', 'id');
    }

    public function teamPlateReference()
    {
        return $this->belongsTo(OimsVehicle::class, 'plate_no_id', 'id');
    }

    public function vehicleReference()
    {
        return $this->belongsTo(OimsVehicle::class, 'vehicle_type', 'id');
    }

    public function teamDriverReference()
    {
        return $this->belongsTo(User::class, 'driver_id', 'id');
    }

    public function teamChecker1Reference()
    {
        return $this->belongsTo(User::class, 'checker1_id', 'id');
    }

    public function teamChecker2Reference()
    {
        return $this->belongsTo(User::class, 'checker2_id', 'id');
    }
}
