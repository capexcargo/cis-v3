<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MileResolution extends Model
{
    use HasFactory;

    protected $table = "crm_milestone_resolution";

    protected $fillable = [
        'priority_level',
        'severity',
        'target_response_time',
    ];
}
