<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FimsBudgetAvailment extends Model
{
    use HasFactory;

    protected $table = "acctng_budget_availment";

    protected $fillable = [
        'requisition_id',
        'budget_subaccount_id',
        'request_amount',
        'accounting_savings',
        'division_savings',
        'request_month',
        'year',
    ];
}
