<?php

namespace App\Models;

use App\Models\Crm\CrmAncillaryDisplay;
use App\Models\Crm\CrmAncillaryDisplayDetails;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmRateSeaFreight extends Model
{
    use HasFactory;
    protected $table = "crm_rate_seafreight";
    protected $fillable = [
        'name',
        'effectivity_date',
        'description',
        'transport_mode_id',
        'shipment_type_id',
        'commodity_type_id',
        'apply_for_id',
        'ancillary_charge_id',
        'is_vice_versa',
        'approver1_id',
        'approver2_id',
        'approver1_status_id',
        'approver2_status_id',
        'approver1_date',
        'appover2_date',
        'final_status_id',
        'base_rate_id',
        'created_by',
    ];

    public function ShipmentTypeReference()
    {
       return $this->belongsTo(CrmShipmentType::class, 'shipment_type_id', 'id');
    }

    public function RateTransportReference()
    {
       return $this->belongsTo(CrmTransportMode::class, 'transport_mode_id', 'id');
    }

    public function CommodityTypeReference()
    {
       return $this->belongsTo(CrmCommodityType::class, 'commodity_type_id', 'id');
    }

    public function RateApplyReference()
    {
       return $this->belongsTo(CrmRateApplyFor::class, 'apply_for_id', 'id');
    }

    public function AncillaryReference()
    {
       return $this->belongsTo(CrmAncillaryDisplay::class, 'ancillary_charge_id', 'id');
    }

    public function ServiceModeReference()
    {
       return $this->belongsTo(CrmServiceMode::class, 'service_mode_id', 'id');
    }

    public function App1()
    {
       return $this->belongsTo(CrmRateLoa::class, 'approver1_id', 'id');
    }

    public function App2()
    {
       return $this->belongsTo(CrmRateLoa::class, 'approver2_id', 'id');
    }

    public function App1StatusReference()
    {
       return $this->belongsTo(CrmApproval::class, 'approver1_status_id', 'id');
    }

    public function App2StatusReference()
    {
       return $this->belongsTo(CrmApproval::class, 'approver2_status_id', 'id');
    }

    public function FinalStatusReference()
    {
       return $this->belongsTo(CrmApproval::class, 'final_status_id', 'id');
    }

    public function CreatedBy()
    {
       return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function RateSeafreightHasMany()
    {
       return $this->hasMany(CrmRateSeaFreightDetails::class, 'rate_id', 'id');
    }
    
   public function getAncillaries()
   {
      return $this->hasManyThrough(
         CrmAncillaryDisplayDetails::class,
         CrmAncillaryDisplay::class,
         'id',
         'ancillary_display_id',
      );
   }
}
