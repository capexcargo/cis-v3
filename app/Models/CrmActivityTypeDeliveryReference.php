<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmActivityTypeDeliveryReference extends Model
{
    use HasFactory;
    protected $table = "crm_activity_type_delivery_reference";

    protected $fillable = [
        'name',
        'time',
        'unit_time',
    ];
}
