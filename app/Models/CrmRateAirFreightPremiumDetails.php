<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmRateAirFreightPremiumDetails extends Model
{
    use HasFactory;
    protected $table = "crm_rate_airfreight_premium_details";

    protected $fillable = [
        'rate_id',
        'origin_id',
        'destination_id',
        'amount_weight_1',
        'amount_weight_2',
        'amount_weight_3',
        'amount_weight_4',
        'service_mode_id',
        'is_primary',
    ];

    public function RateAirFreightPremiumDetails()
    {
       return $this->belongsTo(CrmRateAirFreightPremium::class, 'rate_id', 'id');
    }

    public function OriginDetails()
    {
       return $this->belongsTo(BranchReference::class, 'origin_id', 'id');
    }

    public function DestinationDetails()
    {
       return $this->belongsTo(BranchReference::class, 'destination_id', 'id');
    }

    public function ServiceModeReference()
    {
       return $this->belongsTo(CrmServiceMode::class, 'service_mode_id', 'id');
    }
}
