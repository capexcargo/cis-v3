<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTeamRouteAssignment extends Model
{
    use HasFactory;

    protected $table = "oims_team_route_assignment";

    protected $fillable = [
        'tra_no',
        'branch_id',
        'created_user_id',
        'dispatch_date',
    ];

    public function teamDetails()
    {
        return $this->hasMany(OimsTeamRouteAssignmentDetails::class, 'team_route_id', 'id');
    }

    public function teamBranch()
    {
        return $this->belongsTo(OimsBranchReference::class, 'branch_id', 'id');
    }
}
