<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmRateLoa extends Model
{
    use HasFactory;

    protected $table = "crm_rate_loa_management";

    protected $fillable = [
        'rate_category',
        'approver1_id',
        'approver2_id',
    ];

    public function App1()
    {
       return $this->belongsTo(User::class, 'approver1_id', 'id');
    }

    public function App2()
    {
       return $this->belongsTo(User::class, 'approver2_id', 'id');
    }
}

