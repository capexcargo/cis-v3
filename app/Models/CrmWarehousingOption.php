<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmWarehousingOption extends Model
{
    use HasFactory;
    protected $table = "crm_rate_warehousing_option_reference";

    protected $fillable = [
        'option',
        'rate_per_unit',
    ];
}
