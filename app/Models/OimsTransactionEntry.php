<?php

namespace App\Models;

use App\Models\Crm\CrmCustomerInformation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTransactionEntry extends Model
{
    use HasFactory;
    
    protected $table = "oims_transaction_entry";
    protected $fillable = [
        'waybill',
        'scope_of_work',
        'transaction_date',
        'transaction_type',
        'booking_reference_id',
        'booking_type_id',
        'branch_id',
        'origin_id',
        'destination_id',
        'transhipment_id',
        'transport_mode_id',
        'shipper_id',
        'shipper_contact_person_id',
        'shipper_mobile_no_id',
        'shipper_address_id',
        'shipper_address',
        'consignee__id',
        'consignee_contact_person_id',
        'consignee_mobile_no_id',
        'consignee_address_id',
        'consignee_address',
        'batch_no',
        'pickup_notation',
        'transType',
        'item_type',
        'commodity_app_rate',
        'commodity_type',
        'type_of_goods',
        'paymode',
        'service_mode',
        'charge_to_id',
        'account_owner_id',
        'prepared_by_id',
        'qc_by_id',
        'overriden_by_id',
        'qc_datetime',
        'override_datetime',
        'collection_status',
        'track_status',
        'is_dispatched'
    ];

    public function TransportModeReference()
    {
        return $this->belongsTo(CrmTransportMode::class, 'transport_mode_id', 'id');
    }

    public function CrmBookingReference()
    {
        return $this->belongsTo(CrmBooking::class, 'booking_reference_id', 'id');
    }

    public function OriginReference()
    {
        return $this->belongsTo(BranchReference::class, 'origin_id', 'id');
    }

    public function DestinationReference()
    {
        return $this->belongsTo(BranchReference::class, 'destination_id', 'id');
    }

    public function OimsBranchReference()
    {
        return $this->belongsTo(OimsBranchReference::class, 'branch_id', 'id');
    }

    public function ShipperReference()
    {
        return $this->belongsTo(OimsTransactionEntryCustomerInfo::class, 'shipper_id', 'id');
    }

    public function ConsigneeReference()
    {
        return $this->belongsTo(OimsTransactionEntryCustomerInfo::class, 'consignee__id', 'id');
    }
}
