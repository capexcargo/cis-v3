<?php

namespace App\Models;

use App\Models\Crm\BranchReferencesQuadrant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsTeamReference extends Model
{
    use HasFactory;
    
    protected $table = "oims_team_reference";

    protected $fillable = [
        'name',
        'quadrant_id',
        'status',
    ];

    public function quadrantReference()
    {
       return $this->belongsTo(BranchReferencesQuadrant::class, 'quadrant_id', 'id');
    }
}
