<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OimsReasonApplication extends Model
{
    use HasFactory;

    protected $table = "oims_reason_application_reference";

    protected $fillable = [
        'name',
    ];
}
