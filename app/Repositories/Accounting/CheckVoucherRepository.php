<?php

namespace App\Repositories\Accounting;

use App\Interfaces\Accounting\CheckVoucherInterface;
use App\Interfaces\Accounting\Notifications\CheckVoucherNotificationInterface;
use App\Models\Accounting\BankValidations;
use App\Models\Accounting\CashFlowDetails;
use App\Models\Accounting\CashFlowSnapshot;
use App\Models\Accounting\CheckVoucher;
use App\Models\Accounting\RequestForPayment;
use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class CheckVoucherRepository implements CheckVoucherInterface
{
    use ResponseTrait;

    public $check_voucher_notification_interface;

    public function __construct(CheckVoucherNotificationInterface $check_voucher_notification_interface)
    {
        $this->check_voucher_notification_interface = $check_voucher_notification_interface;
    }

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $request_for_payments = RequestForPayment::with(['type', 'payee', 'user', 'status', 'requestForPaymentDetails', 'checkVoucher' => function ($query) {
                $query->with('status', 'firstApprover', 'secondApprover', 'receivedBy', 'createdBy', 'checkVoucherDetails', 'attachments');
            }])
                ->where('status_id', 2)
                ->when($request['rfp_reference_no'] ?? false, function ($query) use ($request) {
                    $query->where('reference_id', 'like', '%' . $request['rfp_reference_no'] . '%');
                })
                ->when($request['have_check_voucher'] == 1, function ($query) {
                    $query->whereHas('checkVoucher');
                })
                ->when($request['have_check_voucher'] == 2, function ($query) {
                    $query->whereDoesntHave('checkVoucher');
                })
                ->when($request['check_reference_no'] ?? false, function ($query) use ($request) {
                    $query->whereHas('checkVoucher', function ($query) use ($request) {
                        $query->where('reference_no', 'like', '%' . $request['check_reference_no'] . '%');
                    });
                })
                ->when($request['cv_no'] ?? false, function ($query) use ($request) {
                    $query->whereHas('checkVoucher', function ($query) use ($request) {
                        $query->where('cv_no', 'like', '%' . $request['cv_no'] . '%');
                    });
                })
                ->when($request['check_no'] ?? false, function ($query) use ($request) {
                    $query->whereHas('checkVoucher', function ($query) use ($request) {
                        $query->whereHas('checkVoucherDetails', function ($query) use ($request) {
                            $query->where('voucher_no', 'like', '%' . $request['check_no'] . '%');
                        });
                    });
                })
                ->when($request['created_by'] ?? false, function ($query) use ($request) {
                    $query->whereHas('checkVoucher', function ($query) use ($request) {
                        $query->whereHas('createdBy', function ($query) use ($request) {
                            $query->where('name', 'like', '%' . $request['created_by'] . '%');
                        });
                    });
                })
                ->when($request['received_by'] ?? false, function ($query) use ($request) {
                    $query->whereHas('checkVoucher', function ($query) use ($request) {
                        $query->whereHas('receivedBy', function ($query) use ($request) {
                            $query->where('name', 'like', '%' . $request['received_by'] . '%');
                        });
                    });
                })
                ->when($request['received_date_from'] ?? false, function ($query) use ($request) {
                    $query->whereHas('checkVoucher', function ($query) use ($request) {
                        $query->whereDate('received_date', '>=', $request['received_date_from']);
                    });
                })
                ->when($request['received_date_to'] ?? false, function ($query) use ($request) {
                    $query->whereHas('checkVoucher', function ($query) use ($request) {
                        $query->whereDate('received_date', '<=', $request['received_date_to']);
                    });
                })
                ->when($request['created_date_from'] ?? false, function ($query) use ($request) {
                    $query->whereHas('checkVoucher', function ($query) use ($request) {
                        $query->whereDate('created_at', '>=', $request['created_date_from']);
                    });
                })
                ->when($request['created_date_to'] ?? false, function ($query) use ($request) {
                    $query->whereHas('checkVoucher', function ($query) use ($request) {
                        $query->whereDate('created_at', '<=', $request['created_date_to']);
                    });
                })
                ->when($request['released_date_from'] ?? false, function ($query) use ($request) {
                    $query->whereHas('checkVoucher', function ($query) use ($request) {
                        $query->whereDate('release_date', '>=', $request['released_date_from']);
                    });
                })
                ->when($request['released_date_to'] ?? false, function ($query) use ($request) {
                    $query->whereHas('checkVoucher', function ($query) use ($request) {
                        $query->whereDate('release_date', '<=', $request['released_date_to']);
                    });
                })
                ->when($request['status'] ?? false, function ($query) use ($request) {
                    $query->whereHas('checkVoucher', function ($query) use ($request) {
                        $query->where('status_id', $request['status']);
                    });
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Check Voucher List', $request_for_payments);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createOrUpdate(RequestForPayment $request_for_payment, $validated)
    {
        DB::beginTransaction();
        try {

            $voucher_collections = collect($validated['vouchers']);
            $total_voucher_amount = $voucher_collections->sum('amount');

            if (number_format($request_for_payment->amount, 2) != number_format($total_voucher_amount, 2)) {
                return $this->response(400, 'Bad Request', [
                    'total_voucher_amount' => 'Must be equal to Total Amount.'
                ]);
            }

            if (!$request_for_payment->checkVoucher) {
                $count_cv = CheckVoucher::count();
                $validated['cv_no'] = "CVNO-" . date('Y') . "-" . ($count_cv + 1);
            }

            if (empty($request_for_payment->parent_reference_no)) {
                $request_for_payment->update([
                    'payee_id' => $validated['pay_to']
                ]);
            }

            $deputy_head = User::where([['division_id', 6]])
                ->whereHas('role', function ($query) {
                    $query->where('code', 'deputy_head');
                })->first();

            $division_head = User::where([['division_id', 6]])
                ->whereHas('role', function ($query) {
                    $query->where('code', 'division_head');
                })->first();

            if (!$deputy_head) {
                return $this->response(500, 'Something Went Wrong', 'Can`t find Deputy Head of FLS');
            }

            if (!$division_head) {
                return $this->response(500, 'Something Went Wrong', 'Can`t find Division Head of FLS');
            }

            $approver_1_id = null;
            $approver_2_id = null;

            if ($total_voucher_amount < 50001) {
                $approver_1_id = $deputy_head->id;
            } elseif ($total_voucher_amount < 100001) {
                $approver_1_id = $deputy_head->id;
                $approver_2_id = $division_head->id;
            } elseif ($total_voucher_amount >= 100001) {
                $approver_1_id = $division_head->id;
            }

            $check_voucher = $request_for_payment->checkVoucher()->updateOrCreate([
                'reference_no' => $validated['reference_no'],
            ], [
                'reference_no' => $validated['reference_no'],
                'cv_no' => $validated['cv_no'],
                'description' => $validated['description'],
                'status_id' => 4,
                'approver_1_id' => 45,
                'approver_2_id' => NULL,
                // 'approver_1_id' => $approver_1_id,
                // 'approver_2_id' => $approver_2_id,
                'total_amount' => $total_voucher_amount,
                'created_by' => $request_for_payment->checkVoucher->created_by ?? Auth::user()->id,
            ]);

            foreach ($validated['vouchers'] as $voucher) {
                if ($voucher['id'] && $voucher['is_deleted']) {
                    $check_voucher_details = $check_voucher->checkVoucherDetails()->find($voucher['id']);
                    $check_voucher_details->delete();
                } else {
                    $check_voucher->checkVoucherDetails()->updateOrCreate([
                        'id' => $voucher['id'],
                    ], [
                        'voucher_no' => $voucher['voucher_no'],
                        'description' => $voucher['description'],
                        'date' => $voucher['date'],
                        'amount' => $voucher['amount'],
                    ]);
                }
            }

            if ($check_voucher->wasRecentlyCreated) {
                $this->check_voucher_notification_interface->create($check_voucher);
            }

            DB::commit();
            return $this->response(200, 'Check Voucher Successfully ' . ($request_for_payment->checkVoucher ? 'Updated' : 'Created'), $check_voucher);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function approved(CheckVoucher $check_voucher, $validated)
    {
        DB::beginTransaction();
        try {
            $final_approved = false;

            if ($check_voucher->approver_1_id) {
                if ($check_voucher->approver_1_id == Auth::user()->id || Auth::user()->level_id == 5 && $check_voucher->approver_1_id) {
                    // if (!$validated['is_approved_1']) {
                    //     return $this->response(400, 'Bad Request', [
                    //         'approver_remarks_1' => 'The approver remarks 1 field is required.'
                    //     ]);
                    // }

                    if ($check_voucher->is_approved_2 && !$validated['is_approved_1']) {
                        return $this->response(400, 'Bad Request', [
                            'approver_2_id' => 'The approver 2 must be disapproved.'
                        ]);
                    } else {
                        $check_voucher->update([
                            'is_approved_1' => $validated['is_approved_1'] ?? 0,
                            'approver_1_date' => date("Y-m-d"),
                            'approver_1_remarks' => $validated['approver_remarks_1'],
                        ]);
                    }

                    if ($validated['is_approved_1']) {
                        $final_approved = true;
                    } else {
                        $final_approved = false;
                    }
                }
            }

            if ($check_voucher->approver_2_id) {
                if ($validated['is_approved_1'] || !$validated['is_approved_2']) {
                    if ($check_voucher->approver_2_id == Auth::user()->id || Auth::user()->level_id == 5 && $check_voucher->approver_2_id) {
                        // if (!$validated['is_approved_2']) {
                        //     return $this->response(400, 'Bad Request', [
                        //         'approver_remarks_2' => 'The approver remarks 2 field is required.'
                        //     ]);
                        // }

                        $check_voucher->update([
                            'is_approved_2' => $validated['is_approved_2'] ?? 0,
                            'approver_2_date' => date("Y-m-d"),
                            'approver_2_remarks' => $validated['approver_remarks_2'],
                        ]);
                    }

                    if ($validated['is_approved_2']) {
                        $final_approved = true;
                    } else {
                        $final_approved = false;
                    }
                } else {
                    return $this->response(400, 'Bad Request', [
                        'approver_1_id' => 'The approver 1 must be approved.'
                    ]);
                }
            }

            if ($check_voucher->approver_2_id && !$check_voucher->is_approved_2 && $check_voucher->approver_2_remarks) {
                $check_voucher->update([
                    'status_id' => 6
                ]);

                foreach ($check_voucher->checkVoucherDetails as $check_voucher_details) {
                    $cash_flow_details = CashFlowDetails::where('check_no', $check_voucher_details->voucher_no)->first();
                    if ($cash_flow_details) {
                        $cash_flow_details->delete();
                    }
                }
            } else if ($check_voucher->approver_1_id && !$check_voucher->is_approved_1 && $check_voucher->approver_1_remarks) {
                $check_voucher->update([
                    'status_id' => 6
                ]);

                foreach ($check_voucher->checkVoucherDetails as $check_voucher_details) {
                    $cash_flow_details = CashFlowDetails::where('check_no', $check_voucher_details->voucher_no)->first();
                    if ($cash_flow_details) {
                        $cash_flow_details->delete();
                    }
                }
            } else {
                $check_voucher->update([
                    'status_id' => ($final_approved ? 5 : 4)
                ]);

                if ($final_approved) {
                    foreach ($check_voucher->checkVoucherDetails as $check_voucher_details) {
                        $bank_validation = BankValidations::where('check_no', $check_voucher_details->voucher_no)->first();

                        $cash_flow_details = CashFlowDetails::create([
                            'encashment_date' => ($bank_validation ? $bank_validation->transaction_date : null),
                            'check_no' => $check_voucher_details->voucher_no,
                            'cv_date' => $check_voucher_details->date,
                            'payee_id' => $check_voucher->requestForPayment->payee_id,
                            'check_description' => $check_voucher_details->description,
                            'encashment_amount' => ($bank_validation ? $bank_validation->debit_amount : 0),
                            'locked_amount' => 0,
                            'status_id' => ($bank_validation ? 3 : 1),
                        ]);
                    }

                    $this->check_voucher_notification_interface->finalApproved($check_voucher);
                }
            }

            DB::commit();
            return $this->response(200, 'Check Voucher Successfully ' . ($final_approved ? 'Approved' : 'Rejected'), $check_voucher);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateStatusCashFlow(CashFlowDetails $cash_flow_details, $status_id)
    {
        DB::beginTransaction();
        try {

            $cash_flow_details->update([
                'status_id' => $status_id
            ]);

            DB::commit();
            return $this->response(200, 'Cash Flow Details Successfully Updated The Status', $cash_flow_details);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function confirmation(CheckVoucher $check_voucher, $validated)
    {
        DB::beginTransaction();
        try {
            $check_voucher->update([
                'release_date' => $validated['released_date'],
                'received_by' => $validated['received_by'],
                'received_date' => $validated['received_date'],
                'received_remarks' => $validated['remarks'],
            ]);

            $this->attachment($check_voucher, $validated['attachments']);

            DB::commit();
            return $this->response(200, 'Check Voucher Successfully Updated', $check_voucher);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function attachment(CheckVoucher $check_voucher, $attachments)
    {
        DB::beginTransaction();
        try {
            $month = strtolower(date('F', strtotime($check_voucher->created_at)));
            $year = date('Y', strtotime($check_voucher->created_at));

            foreach ($attachments as $attachment) {
                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $check_voucher->attachments()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(" ", "_", $year . '/' . $month . '/attachments/check_voucher/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        Storage::disk('accounting_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } else if (in_array($attachment->extension(), config('filesystems.file_type'))) {
                        Storage::disk('accounting_gcs')->putFileAs($file_path, $attachment, $file_name);
                    }

                    $check_voucher->attachments()->updateOrCreate([
                        'id' => $attachment['id']
                    ], [
                        'path' => $file_path,
                        'name' => $file_name,
                        'extension' => $attachment['attachment']->extension(),
                    ]);
                }
            }

            DB::commit();
            return $this->response(200, 'Check Voucher Successfully Attached Files', $check_voucher->attachments);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function createOrUpdateCashFlowSnapshot($validated)
    {
        DB::beginTransaction();
        try {

            $cash_flow_snapshot = CashFlowSnapshot::updateOrCreate([
                'year' => $validated['year'],
            ], [
                'current_balance' => $validated['current_balance'],
                'floating_balance' => $validated['floating_balance'],
            ]);

            DB::commit();
            return $this->response(200, 'Cash Flow Snapshot Successfully Updated', $cash_flow_snapshot);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
