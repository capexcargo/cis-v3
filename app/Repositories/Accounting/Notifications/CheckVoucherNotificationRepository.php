<?php

namespace App\Repositories\Accounting\Notifications;

use App\Interfaces\Accounting\Notifications\CheckVoucherNotificationInterface;
use App\Models\Accounting\CheckVoucher;
use App\Models\User;
use App\Notifications\Accounting\CheckVoucherNotification;
use App\Traits\SMSTrait;
use Illuminate\Support\Facades\Auth;

class CheckVoucherNotificationRepository implements CheckVoucherNotificationInterface
{
    use SMSTrait;

    public function create(CheckVoucher $check_voucher)
    {
        $data = [
            'id' => $check_voucher->id,
            'action' => 'Create',
            'storage' => 'accounting_gcs',
            'image_path' => null,
            'image_name' => null,
            'title' => 'Check Voucher',
            'message' => $check_voucher->requestForPayment->reference_id . ' -  You have a new pending request that needs an approval in CHECK VOUCHER.',
            'redirection'  => route('accounting.approval-management.check-voucher.index', ['edit_modal' => true, 'check_voucher_id' => $check_voucher->id]),
            'created_by' => Auth::user()->id,
        ];

        $message = $data['title'] . "\n\n" . $data['message'];
        if ($check_voucher->approver_1_id) {
            $check_voucher->firstApprover->notify(new CheckVoucherNotification($data));
            $this->globeSendSMS($message, $check_voucher->firstApprover->userDetails->mobile_number);
        }

        if ($check_voucher->approver_2_id) {
            $check_voucher->secondApprover->notify(new CheckVoucherNotification($data));
            $this->globeSendSMS($message, $check_voucher->secondApprover->userDetails->mobile_number);
        }
    }

    public function finalApproved(CheckVoucher $check_voucher)
    {
        $data = [
            'id' => $check_voucher->id,
            'action' => 'Final Approve',
            'storage' => 'accounting_gcs',
            'image_path' => null,
            'image_name' => null,
            'title' => 'Check Voucher',
            'message' => $check_voucher->cv_no . ' - You have a new approved voucher to receive.',
            'redirection'  => route('accounting.check-voucher.index', ['confirmation_modal' => true, 'check_voucher_id' => $check_voucher->id]),
            'created_by' => Auth::user()->id,
        ];

        // $user = User::find(4);
        // $user->notify(new CheckVoucherNotification($data));
        // $message = $data['title'] . "\n\n" . $data['message'];
        // $this->globeSendSMS($message, $user->userDetails->mobile_number);

         $users = User::whereHas('role', function ($query) {
            $query->where('code', 'accounting_staff_liquidation');
        })->get();
        foreach ($users as $user) {
            $user->notify(new CheckVoucherNotification($data));
            $message = $data['title'] . "\n\n" . $data['message'];
            $this->globeSendSMS($message, $user->userDetails->mobile_number);
        }
    }
}
