<?php

namespace App\Repositories\Accounting\Notifications;

use App\Interfaces\Accounting\Notifications\RequestForPaymentNotificationInterface;
use App\Models\Accounting\RequestForPayment;
use App\Models\User;
use App\Notifications\Accounting\RequestForPaymentNotification;
use App\Traits\SMSTrait;
use Illuminate\Support\Facades\Auth;

class RequestForPaymentNotificationRepository implements RequestForPaymentNotificationInterface
{
    use SMSTrait;

    public function create(RequestForPayment $request_for_payment)
    {
        $data = [
            'id' => $request_for_payment->id,
            'action' => 'Create',
            'storage' => 'accounting_gcs',
            'image_path' => null,
            'image_name' => null,
            'title' => 'Request For Payment',
            'message' => $request_for_payment->reference_id . ' (' . $request_for_payment->priority->display . ')' . ' - You have a new pending request that needs an approval. ',
            'redirection'  => route('accounting.approval-management.request-for-payment.index', ['edit_modal' => true, 'request_id' => $request_for_payment->id]),
            'created_by' => Auth::user()->id,
        ];

        $message = $data['title'] . "\n\n" . $data['message'];
        if ($request_for_payment->approver_1_id) {
            $user = User::find($request_for_payment->approver_1_id);
            $user->notify(new RequestForPaymentNotification($data));
            $this->globeSendSMS($message, $user->userDetails->mobile_number);
        }

        if ($request_for_payment->approver_2_id) {
            $user = User::find($request_for_payment->approver_2_id);
            $user->notify(new RequestForPaymentNotification($data));
            $this->globeSendSMS($message, $user->userDetails->mobile_number);
        }

        if ($request_for_payment->approver_3_id) {
            $user = User::find($request_for_payment->approver_3_id);
            $user->notify(new RequestForPaymentNotification($data));
            $this->globeSendSMS($message, $user->userDetails->mobile_number);
        }
    }

    public function update(RequestForPayment $request_for_payment)
    {
        $data = [
            'id' => $request_for_payment->id,
            'action' => 'Update',
            'storage' => 'accounting_gcs',
            'image_path' => null,
            'image_name' => null,
            'title' => 'Request For Payment',
            'message' => $request_for_payment->reference_id . ' (' . $request_for_payment->priority->display . ')' . ' - Changes has been made to this request. Kindly check your approval. ',
            'redirection'  => route('accounting.approval-management.request-for-payment.index', ['edit_modal' => true, 'request_id' => $request_for_payment->id]),
            'created_by' => Auth::user()->id,
        ];

        $message = $data['title'] . "\n\n" . $data['message'];
        if ($request_for_payment->approver_1_id) {
            $user = User::find($request_for_payment->approver_1_id);
            $user->notify(new RequestForPaymentNotification($data));
            $this->globeSendSMS($message, $user->userDetails->mobile_number);
        }

        if ($request_for_payment->approver_2_id) {
            $user = User::find($request_for_payment->approver_2_id);
            $user->notify(new RequestForPaymentNotification($data));
            $this->globeSendSMS($message, $user->userDetails->mobile_number);
        }

        if ($request_for_payment->approver_3_id) {
            $user = User::find($request_for_payment->approver_3_id);
            $user->notify(new RequestForPaymentNotification($data));
            $this->globeSendSMS($message, $user->userDetails->mobile_number);
        }
    }

    public function approved(RequestForPayment $request_for_payment)
    {
        $data = [];
        if ($request_for_payment->approver_1_id) {
            if ($request_for_payment->is_approved_1) {
                $data = [
                    'id' => $request_for_payment->id,
                    'action' => 'Approve - Approver 1',
                    'storage' => 'accounting_gcs',
                    'image_path' => null,
                    'image_name' => null,
                    'title' => 'Request For Payment',
                    'message' => $request_for_payment->reference_id . ' - Your request has approved by ' . $request_for_payment->approver1->name . ' (Approver 1).',
                    'redirection'  => route('accounting.request-management.index', ['view_modal' => true, 'parent_reference_no' => $request_for_payment->parent_reference_no]),
                    'created_by' => Auth::user()->id,
                ];
            } else if (!is_null($request_for_payment->is_approved_1)) {
                $data = [
                    'id' => $request_for_payment->id,
                    'action' => 'Reject - Approver 1',
                    'storage' => 'accounting_gcs',
                    'image_path' => null,
                    'image_name' => null,
                    'title' => 'Request For Payment',
                    'message' => $request_for_payment->reference_id . ' - Your request has rejected by ' . $request_for_payment->approver1->name . ' (Approver 1). Check their remarks for your reference.',
                    'redirection'  => route('accounting.request-management.index', ['view_modal' => true, 'parent_reference_no' => $request_for_payment->parent_reference_no]),
                    'created_by' => Auth::user()->id,
                ];
            }
        }

        if ($request_for_payment->approver_2_id) {
            if ($request_for_payment->is_approved_2) {
                $data = [
                    'id' => $request_for_payment->id,
                    'action' => 'Approve - Approver 2',
                    'storage' => 'accounting_gcs',
                    'image_path' => null,
                    'image_name' => null,
                    'title' => 'Request For Payment',
                    'message' => $request_for_payment->reference_id . ' - Your request has approved by ' . $request_for_payment->approver2->name . ' (Approver 2).',
                    'redirection'  => route('accounting.request-management.index', ['view_modal' => true, 'parent_reference_no' => $request_for_payment->parent_reference_no]),
                    'created_by' => Auth::user()->id,
                ];
            } else if (!is_null($request_for_payment->is_approved_2)) {
                $data = [
                    'id' => $request_for_payment->id,
                    'action' => 'Reject - Approver 2',
                    'storage' => 'accounting_gcs',
                    'image_path' => null,
                    'image_name' => null,
                    'title' => 'Request For Payment',
                    'message' => $request_for_payment->reference_id . ' - Your request has rejected by ' . $request_for_payment->approver2->name . ' (Approver 2). Check their remarks for your reference.',
                    'redirection'  => route('accounting.request-management.index', ['view_modal' => true, 'parent_reference_no' => $request_for_payment->parent_reference_no]),
                    'created_by' => Auth::user()->id,
                ];
            }
        }

        if ($request_for_payment->approver_3_id) {
            if ($request_for_payment->is_approved_3) {
                $data = [
                    'id' => $request_for_payment->id,
                    'action' => 'Approve - Approver 3',
                    'storage' => 'accounting_gcs',
                    'image_path' => null,
                    'image_name' => null,
                    'title' => 'Request For Payment',
                    'message' => $request_for_payment->reference_id . ' - Your request has approved by ' . $request_for_payment->approver3->name . ' (Approver 3).',
                    'redirection'  => route('accounting.request-management.index', ['view_modal' => true, 'parent_reference_no' => $request_for_payment->parent_reference_no]),
                    'created_by' => Auth::user()->id,
                ];
            } else if (!is_null($request_for_payment->is_approved_3)) {
                $data = [
                    'id' => $request_for_payment->id,
                    'action' => 'Reject - Approver 3',
                    'storage' => 'accounting_gcs',
                    'image_path' => null,
                    'image_name' => null,
                    'title' => 'Request For Payment',
                    'message' => $request_for_payment->reference_id . ' - Your request has rejected by ' . $request_for_payment->approver3->name . ' (Approver 3). Check their remarks for your reference.',
                    'redirection'  => route('accounting.request-management.index', ['view_modal' => true, 'parent_reference_no' => $request_for_payment->parent_reference_no]),
                    'created_by' => Auth::user()->id,
                ];
            }
        }

        $request_for_payment->user->notify(new RequestForPaymentNotification($data));
        $message = $data['title'] . "\n\n" . $data['message'];
        $this->globeSendSMS($message, $request_for_payment->user->userDetails->mobile_number);
    }

    public function finalApproved(RequestForPayment $request_for_payment)
    {
        $data = [
            'id' => $request_for_payment->id,
            'action' => 'Final Approve',
            'storage' => 'accounting_gcs',
            'image_path' => null,
            'image_name' => null,
            'title' => 'Request For Payment',
            'message' => $request_for_payment->reference_id . ' - You can now create voucher for this request.',
            'redirection'  => route('accounting.check-voucher.index', ['create_modal' => true, 'request_for_payment_id' => $request_for_payment->id]),
            'created_by' => Auth::user()->id,
        ];

        // $user = User::find(4);
        // $user->notify(new RequestForPaymentNotification($data));
        // $message = $data['title'] . "\n\n" . $data['message'];
        // $this->globeSendSMS($message, $user->userDetails->mobile_number);

        $users = User::whereHas('role', function ($query) {
            $query->where('code', 'accounting_staff_voucher');
        })->get();
        foreach ($users as $user) {
            $user->notify(new RequestForPaymentNotification($data));
            $message = $data['title'] . "\n\n" . $data['message'];
            $this->globeSendSMS($message, $user->userDetails->mobile_number);
        }
    }
}
