<?php

namespace App\Repositories\Accounting;

use App\Interfaces\Accounting\DivisionManagementInterface;
use App\Models\Division;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class DivisionManagementRepository implements DivisionManagementInterface
{
    use ResponseTrait;

    public function create($validated)
    {
        DB::beginTransaction();
        try {

            $division_checked = Division::where('name', $validated['name'])->first();

            if ($division_checked) {
                return $this->response(400, 'Bad Request', [
                    'name' => 'The name has already been taken.'
                ]);
            }

            $division = Division::create([
                'name' => $validated['name'],
                'description' => $validated['description']
            ]);

            DB::commit();
            return $this->response(200, 'Division has been successfully created', $division);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($division, $validated)
    {
        DB::beginTransaction();
        try {

            $division_checked = Division::where('name', $validated['name'])->first();

            if ($division_checked && $division_checked->id != $division->id) {
                return $this->response(400, 'Bad Request', [
                    'name' => 'The name has already been taken.'
                ]);
            }
            
            $division->update([
                'name' => $validated['name'],
                'description' => $validated['description']
            ]);
            
            DB::commit();
            return $this->response(200, 'Division has been successfully updated', $division);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
