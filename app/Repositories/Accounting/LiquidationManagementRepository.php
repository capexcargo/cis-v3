<?php

namespace App\Repositories\Accounting;

use App\Interfaces\Accounting\LiquidationManagementInterface;
use App\Models\Accounting\Liquidation;
use App\Models\Accounting\RequestForPayment;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class LiquidationManagementRepository implements LiquidationManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $liquidations = Liquidation::with([
                'requestForPayments' => function ($query) {
                    $query->with(['requestForPayment' => function ($query) {
                        $query->with('payee', 'opex', 'user');
                    }]);
                }, 'liquidationDetails' => function ($query) {
                    $query->with(['createdBy', 'attachments', 'requestForPayments' => function ($query) {
                        $query->with(['requestForPayment' => function ($query) {
                            $query->with('payee', 'opex', 'user');
                        }]);
                    }]);
                }
            ])
                ->when($request['liquidation_reference_no'], function ($query) use ($request) {
                    $query->where('liquidation_reference_no', 'like', '%' . $request['liquidation_reference_no'] . '%');
                })
                ->whereHas('requestForPayments', function ($query) use ($request) {
                    $query->whereHas('requestForPayment', function ($query) use ($request) {
                        $query->whereHas('budget', function ($query) use ($request) {
                            $query->when($request['division'], function ($query) use ($request) {
                                $query->where('division_id', $request['division']);
                            });
                        })
                            ->when($request['rfp_reference_no'], function ($query) use ($request) {
                                $query->where('reference_id', 'like', '%' . $request['rfp_reference_no'] . '%');
                            })
                            ->whereHas('checkVoucher', function ($query) use ($request) {
                                $query->where('received_date', '!=', null)
                                    ->when($request['cv_no'], function ($query) use ($request) {
                                        $query->where('cv_no', 'like', '%' . $request['cv_no'] . '%');
                                    });
                            })
                            ->whereHas('payee', function ($query) use ($request) {
                                $query->when($request['payee'], function ($query) use ($request) {
                                    $query->where('company', 'like', '%' . $request['payee'] . '%');
                                });
                            })
                            ->when($request['opex_type'], function ($query) use ($request) {
                                $query->where('opex_type_id', $request['opex_type']);
                            })
                            ->whereHas('user', function ($query) use ($request) {
                                $query->when($request['rfp_requester'], function ($query) use ($request) {
                                    $query->where('name', 'like', '%' . $request['rfp_requester'] . '%');
                                });
                            });
                    });
                })
                ->whereHas('liquidationDetails', function ($query) use ($request) {
                    $query->whereHas('createdBy', function ($query) use ($request) {
                        $query->where('name', 'like', '%' . $request['liquidated_by'] . '%');
                    });
                })
                ->when($request['status'], function ($query) use ($request) {
                    $query->where('status_id', $request['status']);
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Liquidation List', $liquidations);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        $liquidation = Liquidation::with([
            'requestForPayments' => function ($query) {
                $query->with('requestForPayment');
            }, 'liquidationDetails' => function ($query) {
                $query->with(['createdBy', 'attachments', 'requestForPayments' => function ($query) {
                    $query->with('requestForPayment');
                }]);
            }
        ])
            ->find($id);
        if (!$liquidation) return $this->response(404, 'Liquidation', 'Not Found!');

        return $this->response(200, 'Liquidation', $liquidation);
    }

    public function loadRequestForPaymentReference($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'request_for_payment_search' => 'sometimes',
                'where_request_for_payments' => 'required',
                'request_for_payments' => 'sometimes',
                'liquidation_details' => 'required_if:where_request_for_payments,where_in',
                'liquidation_details_index' => 'required_if:where_request_for_payments,where_in',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            $validated = $validator->validated();
            $request_for_payments_collection = collect($validated['request_for_payments'])->where('is_deleted', false);
            $request_total_amount = $request_for_payments_collection->sum('amount');

            $request_for_payments = RequestForPayment::select('id', 'reference_id')
                ->where('reference_id', 'like', '%' . $validated['request_for_payment_search'] . '%')
                ->when($validated['where_request_for_payments'] == 'where_not_in', function ($query) use ($request_for_payments_collection) {
                    $query->whereNotIn('id', $request_for_payments_collection->pluck('request_for_payment_id'))
                        ->doesntHave('liquidation');
                })
                ->when($validated['where_request_for_payments'] == 'where_in', function ($query) use ($request_for_payments_collection, $validated) {
                    $query->whereIn('id', $request_for_payments_collection->pluck('request_for_payment_id'));
                    $query->whereNotIn('id', collect($validated['liquidation_details'][$validated['liquidation_details_index']]['request_for_payments'])->where('is_deleted', false)->pluck('request_for_payment_id'));
                })
                ->whereHas('checkVoucher', function ($query) {
                    $query->where('received_date', '!=', null);
                })
                ->orderBy('created_at', 'desc')
                ->take(10)
                ->get();

            DB::commit();
            return $this->response(200, 'Reqeust For Payment Reference', compact('request_total_amount', 'request_for_payments'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function showRequestForPaymentReference($id, $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'where_request_for_payments' => 'required',
                'request_for_payments' => 'sometimes',
                'liquidation_details' => 'required_if:where_request_for_payments,where_in',
                'liquidation_details_index' => 'required_if:where_request_for_payments,where_in',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            $validated = $validator->validated();
            $request_for_payments_collection = collect($validated['request_for_payments'])->where('is_deleted', false);
            $request_total_amount = $request_for_payments_collection->sum('amount');

            $request_for_payment = RequestForPayment::with('payee', 'user', 'opex')->when($validated['where_request_for_payments'] == 'where_not_in', function ($query) use ($request_for_payments_collection) {
                $query->whereNotIn('id', $request_for_payments_collection->where('is_deleted', false)->pluck('request_for_payment_id'));
            })
                ->when($validated['where_request_for_payments'] == 'where_in', function ($query) use ($validated, $request_for_payments_collection) {
                    $query->whereIn('id', $request_for_payments_collection->where('is_deleted', false)->pluck('request_for_payment_id'));
                    $query->whereNotIn('id', collect($validated['liquidation_details'][$validated['liquidation_details_index']]['request_for_payments'])->where('is_deleted', false)->pluck('request_for_payment_id'));
                })
                ->whereHas('checkVoucher', function ($query) {
                    $query->where('received_date', '!=', null);
                })->find($id);

            if (!$request_for_payment) return $this->response(404, 'Reqeust For Payment', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Reqeust For Payment', compact('request_total_amount', 'request_for_payment'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['liquidation_details'] as $i => $liquidation_detail) {
                $request['liquidation_details'][$i]['total_amount'] = ($liquidation_detail['amount'] + $liquidation_detail['cash_on_hand']);
            }

            $liquidation_details_collections = collect($request['liquidation_details'])->where('is_deleted', false);
            $liquidation_total_amount = $liquidation_details_collections->sum('total_amount');

            $request_for_payments_collections = collect($request['request_for_payments']);
            $request_for_payments_total_amount = $request_for_payments_collections->sum('amount');

            $request_data = array_merge(
                $request,
                [
                    'liquidation_total_amount' => $liquidation_total_amount,
                    'request_for_payments_total_amount' => $request_for_payments_total_amount,
                ]
            );

            $validator = Validator::make($request_data, [
                'liquidation_reference_no' => 'required',
                'status' => 'required',
                'request_for_payments' => 'required',
                'liquidation_details' => 'required',

                'request_for_payments.*.request_for_payment_id' => 'required|unique:accounting_liquidation_request,rfp_id',

                'liquidation_details.*.id' => 'sometimes',
                'liquidation_details.*.or_no' => 'required|unique:accounting_liquidation_details,or_no',
                'liquidation_details.*.transaction_date' => 'required',
                'liquidation_details.*.amount' => 'required',
                'liquidation_details.*.cash_on_hand' => 'required',
                'liquidation_details.*.description' => 'required',
                'liquidation_details.*.remarks' => 'sometimes',
                'liquidation_details.*.is_deleted' => 'sometimes',

                'liquidation_details.*.attachments' => 'required',
                'liquidation_details.*.attachments.*.attachment' => 'required_if:liquidation_details.*.attachments.*.id,null|' .  config('filesystems.validation_all'),

                'liquidation_details.*.request_for_payments' => 'required',

                'liquidation_total_amount' => 'required|lte:request_for_payments_total_amount'
            ], [
                'liquidation_details.*.or_no.required' => 'The or no field is required.',
                'liquidation_details.*.transaction_date.required' => 'The transaction date field is required.',
                'liquidation_details.*.amount.required' => 'The amount field is required.',
                'liquidation_details.*.cash_on_hand.required' => 'The cash on hand field is required.',
                'liquidation_details.*.description.required' => 'The description field is required.',
                'liquidation_details.*.remarks.required' => 'The remarks field is required.',

                'liquidation_details.*.attachments.required' => 'The attachments is required in each details.',
                'liquidation_details.*.attachments.*.attachment.required_if' => 'The attachment field is required.',
                'liquidation_details.*.attachments.*.attachment.mimes' => 'The attachment must be one of this jpg, jpeg, png, xlsx, doc, docx.',

                'liquidation_details.*.request_for_payments.required' => 'The request for payment is required in each details.',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());
            $validated = $validator->validated();

            $liquidation = Liquidation::create([
                'liquidation_reference_no' => $validated['liquidation_reference_no'],
                'liquidation_total_amount' => $liquidation_total_amount,
                'status_id' => $validated['status'],
            ]);

            if ($request_for_payments_total_amount == $liquidation_total_amount) {
                $liquidation->update([
                    'status_id' => 2,
                ]);
            }

            $response = $this->requestForPayments($liquidation, $validated['request_for_payments']);
            if ($response['code'] == 500) return $this->response($response['code'], $response['message'], $response['result']);

            $response = $this->liquidationDetails($liquidation, $validated['liquidation_details']);
            if ($response['code'] == 500) return $this->response($response['code'], $response['message'], $response['result']);

            DB::commit();
            return $this->response(200, 'Liquidation Successfully Created', $liquidation);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $liquidation = Liquidation::find($id);
            if (!$liquidation) return $this->response(404, 'Liquidation', 'Not Found!');

            foreach ($request['liquidation_details'] as $i => $liquidation_detail) {
                $request['liquidation_details'][$i]['total_amount'] = ($liquidation_detail['amount'] + $liquidation_detail['cash_on_hand']);
            }

            $liquidation_details_collections = collect($request['liquidation_details'])->where('is_deleted', false);
            $liquidation_total_amount = $liquidation_details_collections->sum('total_amount');

            $request_for_payments_collections = collect($request['request_for_payments']);
            $request_for_payments_total_amount = $request_for_payments_collections->sum('amount');

            $request_data = array_merge(
                $request,
                [
                    'liquidation_total_amount' => $liquidation_total_amount,
                    'request_for_payments_total_amount' => $request_for_payments_total_amount,
                ]
            );

            $rules = [
                'liquidation_reference_no' => 'required',
                'status' => 'required',
                'request_for_payments' => 'required',
                'liquidation_details' => 'required',

                'liquidation_details.*.id' => 'sometimes',
                'liquidation_details.*.transaction_date' => 'required',
                'liquidation_details.*.amount' => 'required',
                'liquidation_details.*.cash_on_hand' => 'required',
                'liquidation_details.*.description' => 'required',
                'liquidation_details.*.remarks' => 'sometimes',
                'liquidation_details.*.is_deleted' => 'sometimes',

                'liquidation_details.*.attachments' => 'required',

                'liquidation_details.*.request_for_payments' => 'required',

                'liquidation_total_amount' => 'required|lte:request_for_payments_total_amount'
            ];

            foreach ($request['request_for_payments'] as $i => $request_for_payment) {
                $rules['request_for_payments.' . $i . '.request_for_payment_id'] = 'required|unique:accounting_liquidation_request,rfp_id,' . $request_for_payment['id'] . ',id';
            }

            foreach ($request['liquidation_details'] as $a => $liquidation_detail) {
                $rules['liquidation_details.' . $a . '.or_no'] = 'required|unique:accounting_liquidation_details,or_no,' . $liquidation_detail['id'] . ',id';
                foreach ($liquidation_detail['attachments'] as $b => $attachment) {
                    if ($attachment['attachment']) {
                        $rules['liquidation_details.' . $a . '.attachments.' . $b . '.attachment'] = 'required_if:liquidation_details.*.attachments.*.id,null|' .  config('filesystems.validation_all');
                    } else {
                        $rules['liquidation_details.' . $a . '.attachments.' . $b . '.attachment'] = 'required_if:liquidation_details.*.attachments.*.id,null';
                    }
                }
            }

            $validator = Validator::make($request_data, $rules, [
                'liquidation_details.*.or_no.required' => 'The or no field is required.',
                'liquidation_details.*.transaction_date.required' => 'The transaction date field is required.',
                'liquidation_details.*.amount.required' => 'The amount field is required.',
                'liquidation_details.*.cash_on_hand.required' => 'The cash on hand field is required.',
                'liquidation_details.*.description.required' => 'The description field is required.',
                'liquidation_details.*.remarks.required' => 'The remarks field is required.',

                'liquidation_details.*.attachments.required' => 'The attachments is required in each details.',
                'liquidation_details.*.attachments.*.attachment.required_if' => 'The attachment field is required.',
                'liquidation_details.*.attachments.*.attachment.mimes' => 'The attachment must be one of this jpg, jpeg, png, xlsx, doc, docx.',

                'liquidation_details.*.request_for_payments.required' => 'The request for payment is required in each details.',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());
            $validated = $validator->validated();

            $liquidation->update([
                'liquidation_reference_no' => $validated['liquidation_reference_no'],
                'liquidation_total_amount' => $liquidation_total_amount,
                'status_id' => $validated['status'],
            ]);

            if ($request_for_payments_total_amount == $liquidation_total_amount) {
                $liquidation->update([
                    'status_id' => 2,
                ]);
            }

            $response = $this->requestForPayments($liquidation, $validated['request_for_payments']);
            if ($response['code'] == 500) return $this->response($response['code'], $response['message'], $response['result']);

            $response = $this->liquidationDetails($liquidation, $validated['liquidation_details']);
            if ($response['code'] == 500) return $this->response($response['code'], $response['message'], $response['result']);

            DB::commit();
            return $this->response(200, 'Liquidation Successfully updated', $liquidation);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function requestForPayments($liquidation, $request_for_payments)
    {
        DB::beginTransaction();
        try {
            foreach ($request_for_payments as $request_for_payment) {
                if ($request_for_payment['id'] && $request_for_payment['is_deleted']) {
                    $request_for_payment_model = $liquidation->requestForPayments()->find($request_for_payment['id']);
                    if ($request_for_payment_model) {
                        $request_for_payment_model->delete();
                    }
                } else {
                    $liquidation->requestForPayments()->updateOrCreate([
                        'id' => $request_for_payment['id'],
                    ], [
                        'rfp_id' => $request_for_payment['request_for_payment_id'],
                    ]);
                }
            }

            DB::commit();
            return $this->response(200, 'Liquidation Request For Payments Successfully Created/Updated', $liquidation->requestForPayments);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function liquidationDetails($liquidation, $liquidation_details)
    {
        DB::beginTransaction();
        try {
            foreach ($liquidation_details as $liquidation_detail) {
                if ($liquidation_detail['id'] && $liquidation_detail['is_deleted']) {
                    $liquidation_detail_model = $liquidation->liquidationDetails()->find($liquidation_detail['id']);
                    if ($liquidation_detail_model) {
                        $liquidation_detail_model->update(['or_no' => $liquidation_detail['or_no'] . '-deleted']);
                        $liquidation_detail_model->delete();
                    }
                } else {
                    $liquidation_detail_model = $liquidation->liquidationDetails()->updateOrCreate([
                        'id' => $liquidation_detail['id'],
                    ], [
                        'or_no' => $liquidation_detail['or_no'],
                        'transaction_date' => $liquidation_detail['transaction_date'],
                        'amount' => $liquidation_detail['amount'],
                        'cash_on_hand' => $liquidation_detail['cash_on_hand'],
                        'description' => $liquidation_detail['description'],
                        'remarks' => $liquidation_detail['remarks'],
                    ]);

                    if (!$liquidation_detail['id']) {
                        $liquidation_detail_model->update([
                            'created_by' => Auth::user()->id,
                        ]);
                    }

                    $response = $this->liquidationDetailsAttachments($liquidation_detail_model, $liquidation_detail['attachments']);
                    if ($response['code'] == 500) {
                        return $this->response($response['code'], $response['message'], $response['result']);
                    }

                    $response = $this->liquidationDetailsRequestForPayments($liquidation_detail_model, $liquidation_detail['request_for_payments']);
                    if ($response['code'] == 500) {
                        return $this->response($response['code'], $response['message'], $response['result']);
                    }
                }
            }

            DB::commit();
            return $this->response(200, 'Liquidation Details Successfully Created/Updated', $liquidation->liquidationDetails);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function liquidationDetailsAttachments($liquidation_details, $attachments)
    {
        DB::beginTransaction();
        try {
            $month = strtolower(date('F', strtotime($liquidation_details->created_at)));
            $year = date('Y', strtotime($liquidation_details->created_at));

            foreach ($attachments as $attachment) {
                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $liquidation_details->attachments()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(" ", "_", $year . '/' . $month . '/attachments/liquidation/liquidation_details/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        Storage::disk('accounting_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } else if (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('accounting_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }

                    $liquidation_details->attachments()->updateOrCreate(
                        [
                            'id' => $attachment['id']
                        ],
                        [
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                        ]
                    );
                }
            }

            DB::commit();
            return $this->response(200, 'Liquidation Details Successfully Attached Files', $liquidation_details->attachments);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function liquidationDetailsRequestForPayments($liquidation_details, $request_for_payments)
    {
        DB::beginTransaction();
        try {
            foreach ($request_for_payments as $request_for_payment) {
                if ($request_for_payment['id'] && $request_for_payment['is_deleted']) {
                    $request_for_payment_model = $liquidation_details->requestForPayments()->find($request_for_payment['id']);
                    $request_for_payment_model->delete();
                } else {
                    $liquidation_details->requestForPayments()->updateOrCreate([
                        'id' => $request_for_payment['id'],
                    ], [
                        'rfp_id' => $request_for_payment['request_for_payment_id'],
                    ]);
                }
            }
            DB::commit();
            return $this->response(200, 'Liquidation Details Successfully Attached Request For Payments', $liquidation_details->requestForPayments);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateStatus($id, $status_id)
    {
        DB::beginTransaction();
        try {
            $liquidation = Liquidation::whereIn('status_id', [2, 3])->find($id);
            if (!$liquidation) return $this->response(404, 'Liquidation', 'Not Found!');

            $liquidation->update([
                'status_id' => $status_id
            ]);

            DB::commit();
            return $this->response(200, 'Liquidation Status Successfully Updated', $liquidation);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
