<?php

namespace App\Repositories\Accounting;

use App\Interfaces\Accounting\Notifications\RequestForPaymentNotificationInterface;
use App\Interfaces\Accounting\RequestManagementInterface;
use App\Models\Accounting\BudgetChart;
use App\Models\Accounting\RequestForPayment;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class RequestManagementRepository implements RequestManagementInterface
{
    use ResponseTrait;

    public $request_for_payments_notification_interface;

    public function __construct(RequestForPaymentNotificationInterface $request_for_payments_notification_interface)
    {
        $this->request_for_payments_notification_interface = $request_for_payments_notification_interface;
    }

    public function createRequestForPayments($validated, $validated_details, $user_id)
    {
        DB::beginTransaction();
        try {
            $request_for_payments_collections = collect($validated_details['request_for_payments']);
            $totals = $request_for_payments_collections->map(function ($item, $key) {
                return ['total' => ($item['unit_cost'] * $item['quantity']) + ($item['labor_cost'] ? $item['labor_cost'] : 0)];
            });
            $total_amount = $totals->sum('total');

            $request_for_payment = $this->createRequestForPayment($validated, $total_amount, $user_id);

            foreach ($validated_details['request_for_payments'] as $request_for_payment_data) {
                $request_for_payment->requestForPaymentDetails()->create([
                    'reference_id' => $validated['reference_number'],

                    'particulars' => $request_for_payment_data['particulars'],
                    'plate_no' => $request_for_payment_data['plate_no'] ?? null,
                    'labor_cost' => $request_for_payment_data['labor_cost'] ? $request_for_payment_data['labor_cost'] : 0,
                    'unit_cost' => $request_for_payment_data['unit_cost'],
                    'quantity' => $request_for_payment_data['quantity'],
                    'amount' => $request_for_payment_data['amount'],
                ]);
            }

            DB::commit();

            return $this->response(200, 'Request For Payment Successfully Created', $request_for_payment);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createFreights($validated, $validated_details, $user_id)
    {
        DB::beginTransaction();
        try {
            $freights_collections = collect($validated_details['freights']);
            $total_amount = $freights_collections->sum('trucking_amount') + $freights_collections->sum('freight_amount') + $freights_collections->sum('allowance');

            $request_for_payment = $this->createRequestForPayment($validated, $total_amount, $user_id);

            foreach ($freights_collections as $freight) {
                $request_for_payment->requestForPaymentDetails()->create([
                    'reference_id' => $validated['reference_number'],

                    'loaders_id' => $validated['loader'],
                    'freight_reference_no' => $freight['freight_reference_no'],
                    'freight_reference_type_id' => $freight['freight_reference_type'],
                    'soa_no' => $freight['soa_no'],
                    'trucking_type_id' => $freight['trucking_type'],
                    'trucking_amount' => $freight['trucking_amount'],
                    'freight_amount' => $freight['freight_amount'],
                    'freight_usage_id' => $freight['freight_usage'],
                    'transaction_date' => $freight['transaction_date'],
                    'allowance' => $freight['allowance'],
                    'amount' => ($freight['trucking_amount'] + $freight['freight_amount'] + $freight['allowance']),
                ]);
            }
            DB::commit();

            return $this->response(200, 'Freight Successfully Created', $request_for_payment);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createPayables($validated, $validated_details, $user_id)
    {
        DB::beginTransaction();
        try {
            $payables_collections = collect($validated_details['payables']);
            $total_amount = $payables_collections->sum('invoice_amount');

            $request_for_payment = $this->createRequestForPayment($validated, $total_amount, $user_id);

            foreach ($payables_collections as $payable) {
                $request_for_payment->requestForPaymentDetails()->create([
                    'reference_id' => $validated['reference_number'],

                    'account_no' => $validated_details['account_no'],
                    'payment_type_id' => $validated_details['payment_type'],
                    'terms_id' => $validated_details['terms'],
                    'pdc_from' => $validated_details['pdc_from'],
                    'pdc_to' => $validated_details['pdc_to'],
                    // 'pdc_date'=> $validated_details['invoice'],

                    'invoice' => $payable['invoice_no'],
                    'invoice_amount' => $payable['invoice_amount'],
                    'transaction_date' => $validated_details['payment_type'] == 2 ? $payable['date_of_transaction'] : null,
                    'amount' => $payable['invoice_amount'],
                ]);
            }

            DB::commit();

            return $this->response(200, 'Payable Successfully Created', $request_for_payment);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createCashAdvances($validated, $validated_details, $user_id)
    {
        DB::beginTransaction();
        try {
            $cash_advances_collections = collect($validated_details['cash_advances']);
            $totals = $cash_advances_collections->map(function ($item, $key) {
                return ['total' => ($item['unit_cost'] * $item['quantity'])];
            });
            $total_amount = $totals->sum('total');

            $request_for_payment = $this->createRequestForPayment($validated, $total_amount, $user_id);

            foreach ($validated_details['cash_advances'] as $cash_advance) {
                $request_for_payment->requestForPaymentDetails()->create([
                    'reference_id' => $validated['reference_number'],

                    'particulars' => $cash_advance['particulars'],
                    'ca_reference_no' => $cash_advance['ca_reference_no'],
                    'unit_cost' => $cash_advance['unit_cost'],
                    'quantity' => $cash_advance['quantity'],
                    'amount' => $cash_advance['amount'],
                ]);
            }

            DB::commit();

            return $this->response(200, 'Cash Advance Successfully Created', $request_for_payment);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateRequestForPayments(RequestForPayment $request_for_payment, $validated, $validated_details, $user_id)
    {
        DB::beginTransaction();
        try {
            $request_for_payments_collections = collect($validated_details['request_for_payments']);
            $totals = $request_for_payments_collections->map(function ($item, $key) {
                return ['total' => ($item['unit_cost'] * $item['quantity']) + ($item['labor_cost'] ? $item['labor_cost'] : 0)];
            });
            $total_amount = $totals->sum('total');

            $is_change_rfp_type = false;
            if ($request_for_payment->type_id != $validated['rfp_type']) {
                $is_change_rfp_type = true;
            }

            $request_for_payment = $this->updateRequestForPayment($request_for_payment, $validated, $total_amount, $user_id);

            if ($is_change_rfp_type) {
                foreach ($request_for_payment->requestForPaymentDetails as $request_for_payment_detail) {
                    $request_for_payment_detail->delete();
                }
            }

            foreach ($validated_details['request_for_payments'] as $request_for_payment_data) {
                if ($request_for_payment_data['is_deleted']) {
                    $request_for_payment_detail = $request_for_payment->requestForPaymentDetails()->find($request_for_payment_data['id']);
                    if ($request_for_payment_detail) {
                        $request_for_payment_detail->delete();
                    }
                } else {
                    $request_for_payment->requestForPaymentDetails()->updateOrCreate([
                        'id' => $request_for_payment_data['id'],
                    ], [
                        'reference_id' => $validated['reference_number'],

                        'particulars' => $request_for_payment_data['particulars'],
                        'plate_no' => $request_for_payment_data['plate_no'] ?? null,
                        'labor_cost' => $request_for_payment_data['labor_cost'] ? $request_for_payment_data['labor_cost'] : 0,
                        'unit_cost' => $request_for_payment_data['unit_cost'],
                        'quantity' => $request_for_payment_data['quantity'],
                        'amount' => $request_for_payment_data['amount'],
                    ]);
                }
            }

            DB::commit();

            return $this->response(200, 'Request For Payment Successfully Updated', $request_for_payment);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateFreights(RequestForPayment $request_for_payment, $validated, $validated_details, $user_id)
    {
        DB::beginTransaction();
        try {
            $freights_collections = collect($validated_details['freights']);
            $total_amount = $freights_collections->sum('trucking_amount') + $freights_collections->sum('freight_amount') + $freights_collections->sum('allowance');

            $is_change_rfp_type = false;
            if ($request_for_payment->type_id != $validated['rfp_type']) {
                $is_change_rfp_type = true;
            }

            $request_for_payment = $this->updateRequestForPayment($request_for_payment, $validated, $total_amount, $user_id);

            if ($is_change_rfp_type) {
                foreach ($request_for_payment->requestForPaymentDetails as $request_for_payment_detail) {
                    $request_for_payment_detail->delete();
                }
            }

            foreach ($validated_details['freights'] as $freight) {
                if ($freight['is_deleted']) {
                    $request_for_payment_detail = $request_for_payment->requestForPaymentDetails()->find($freight['id']);
                    if ($request_for_payment_detail) {
                        $request_for_payment_detail->delete();
                    }
                } else {
                    $request_for_payment->requestForPaymentDetails()->updateOrCreate([
                        'id' => $freight['id'],
                    ], [
                        'reference_id' => $validated['reference_number'],

                        'loaders_id' => $validated['loader'],
                        'freight_reference_no' => $freight['freight_reference_no'],
                        'freight_reference_type_id' => $freight['freight_reference_type'],
                        'soa_no' => $freight['soa_no'],
                        'trucking_type_id' => $freight['trucking_type'],
                        'trucking_amount' => $freight['trucking_amount'],
                        'freight_amount' => $freight['freight_amount'],
                        'freight_usage_id' => $freight['freight_usage'],
                        'transaction_date' => $freight['transaction_date'],
                        'allowance' => $freight['allowance'],
                        'amount' => ($freight['trucking_amount'] + $freight['freight_amount'] + $freight['allowance']),
                    ]);
                }
            }

            DB::commit();

            return $this->response(200, 'Freight Successfully Updated', $request_for_payment);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updatePayables(RequestForPayment $request_for_payment, $validated, $validated_details, $user_id)
    {
        DB::beginTransaction();
        try {
            $payables_collections = collect($validated_details['payables']);
            $total_amount = $payables_collections->sum('invoice_amount');

            $is_change_rfp_type = false;
            if ($request_for_payment->type_id != $validated['rfp_type']) {
                $is_change_rfp_type = true;
            }

            $request_for_payment = $this->updateRequestForPayment($request_for_payment, $validated, $total_amount, $user_id);

            if ($is_change_rfp_type) {
                foreach ($request_for_payment->requestForPaymentDetails as $request_for_payment_detail) {
                    $request_for_payment_detail->delete();
                }
            }

            foreach ($validated_details['payables'] as $payable) {
                if ($payable['is_deleted']) {
                    $request_for_payment_detail = $request_for_payment->requestForPaymentDetails()->find($payable['id']);
                    if ($request_for_payment_detail) {
                        $request_for_payment_detail->delete();
                    }
                } else {
                    $request_for_payment->requestForPaymentDetails()->updateOrCreate([
                        'id' => $payable['id'],
                    ], [
                        'reference_id' => $validated['reference_number'],

                        'account_no' => $validated_details['account_no'],
                        'payment_type_id' => $validated_details['payment_type'],
                        'terms_id' => $validated_details['terms'],
                        'pdc_from' => $validated_details['pdc_from'],
                        'pdc_to' => $validated_details['pdc_to'],
                        // 'pdc_date'=> $validated_details['invoice'],

                        'invoice' => $payable['invoice_no'],
                        'invoice_amount' => $payable['invoice_amount'],
                        'transaction_date' => $validated_details['payment_type'] == 2 ? $payable['date_of_transaction'] : null,
                        'amount' => $payable['invoice_amount'],
                    ]);
                }
            }

            DB::commit();

            return $this->response(200, 'Payable Successfully Updated', $request_for_payment);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateCashAdvances(RequestForPayment $request_for_payment, $validated, $validated_details, $user_id)
    {
        DB::beginTransaction();
        try {
            $cash_advances_collections = collect($validated_details['cash_advances']);
            $totals = $cash_advances_collections->map(function ($item, $key) {
                return ['total' => ($item['unit_cost'] * $item['quantity'])];
            });
            $total_amount = $totals->sum('total');

            $is_change_rfp_type = false;
            if ($request_for_payment->type_id != $validated['rfp_type']) {
                $is_change_rfp_type = true;
            }

            $request_for_payment = $this->updateRequestForPayment($request_for_payment, $validated, $total_amount, $user_id);

            if ($is_change_rfp_type) {
                foreach ($request_for_payment->requestForPaymentDetails as $request_for_payment_detail) {
                    $request_for_payment_detail->delete();
                }
            }

            foreach ($validated_details['cash_advances'] as $cash_advance) {
                if ($cash_advance['is_deleted']) {
                    $request_for_payment_detail = $request_for_payment->requestForPaymentDetails()->find($cash_advance['id']);
                    if ($request_for_payment_detail) {
                        $request_for_payment_detail->delete();
                    }
                } else {
                    $request_for_payment->requestForPaymentDetails()->updateOrCreate([
                        'id' => $cash_advance['id'],
                    ], [
                        'reference_id' => $validated['reference_number'],

                        'particulars' => $cash_advance['particulars'],
                        'ca_reference_no' => $cash_advance['ca_reference_no'],
                        'unit_cost' => $cash_advance['unit_cost'],
                        'quantity' => $cash_advance['quantity'],
                        'amount' => $cash_advance['amount'],
                    ]);
                }
            }

            DB::commit();

            return $this->response(200, 'Cash Advance Successfully Updated', $request_for_payment);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function approved(RequestForPayment $request_for_payment, $validated, $user_id, $level)
    {
        DB::beginTransaction();
        try {
            $final_approved = false;

            // $budget_plan = BudgetPlan::withSum('transferBudgetFrom', 'amount')
            //     ->withSum('transferBudgetTo', 'amount')
            //     ->withSum('availment', 'amount')
            //     ->with('transferBudgetFrom', 'transferBudgetTo', 'availment')->find($request_for_payment->budget_id);

            // $plan_persent_availed = $budget_plan->availment_sum_amount && $budget_plan->total_amount ? (($budget_plan->availment_sum_amount + $budget_plan->transfer_budget_from_sum_amount) / ($budget_plan->total_amount + $budget_plan->transfer_budget_to_sum_amount)) * 100 : 0;

            // if ($budget_plan->total_amount && $plan_persent_availed >= 80) {
            //     return $this->response(500, 'Budget Plan', 'Budget Availed is ' . round($plan_persent_availed, 2));
            // } else if (!$budget_plan->total_amount) {
            //     return $this->response(500, 'Budget Plan', 'Budget Total Amount is 0');
            // }

            $budget_availment_created = 0;
            if ($request_for_payment->budgetAvailments ?? false) {
                // dd($request_for_payment);
                foreach ($request_for_payment->budgetAvailments as $budget_availment) {
                    $budget_availment_created += $budget_availment->amount;
                }
            }
            // dd($request_for_payment->budgetAvailments[0]->amount);
            $identifypm = '';
            if ($request_for_payment->type_id == 2 && $request_for_payment->account_type_id == 1) {
                $identifypm = 'freight';
                foreach ($request_for_payment->requestForPaymentDetails as $request_for_payment_details) {
                    $trandates = date('F', strtotime($request_for_payment_details->transaction_date));
                    $budget_chart = BudgetChart::withSum(['transferBudgetFrom' => function ($query) use ($request_for_payment_details) {
                        $query->where('month_from', strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                            ->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                    }], 'amount')
                        ->withSum(['transferBudgetTo' => function ($query) use ($request_for_payment_details) {
                            $query->where('month_to', strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                                ->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                        }], 'amount')
                        ->withSum(['availment' => function ($query) use ($request_for_payment_details) {
                            $query->where('month', strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                                ->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                        }], 'amount')
                        ->withSum(['budgetPlan' => function ($query) use ($request_for_payment_details) {
                            $query->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                        }], strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                        // ->withSum('budgetPlan', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
                        ->find($request_for_payment->budget->budget_chart_id);
                }
            } elseif ($request_for_payment->type_id == 3) {
                $identifypm = 'pdc';
                if ($request_for_payment->requestForPaymentDetails[0]->payment_type_id == 2) {
                    $current_year = $request_for_payment->requestForPaymentDetails()->whereYear('transaction_date', date('Y', strtotime(now())))->get();
                    $next_year = $request_for_payment->requestForPaymentDetails()->whereYear('transaction_date', '>', date('Y', strtotime(now())))->get();

                    if (isset($current_year)) {
                        foreach ($current_year as $request_for_payment_details) {
                            $trandates = date('F', strtotime($request_for_payment_details->transaction_date));
                            $budget_chart = BudgetChart::withSum(['transferBudgetFrom' => function ($query) use ($request_for_payment_details) {
                                $query->where('month_from', strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                                    ->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                            }], 'amount')
                                ->withSum(['transferBudgetTo' => function ($query) use ($request_for_payment_details) {
                                    $query->where('month_to', strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                                        ->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                                }], 'amount')
                                ->withSum(['availment' => function ($query) use ($request_for_payment_details) {
                                    $query->where('month', strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                                        ->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                                }], 'amount')
                                ->withSum(['budgetPlan' => function ($query) use ($request_for_payment_details) {
                                    $query->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                                }], strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                                // ->withSum('budgetPlan', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
                                ->find($request_for_payment->budget->budget_chart_id);
                        }
                    }

                    // if (isset($next_year)) {

                    //     if ($next_year[0]->payment_type_id == 2) {
                    //         $request_for_payment_new = (object) [];
                    //         foreach ($next_year as $i => $request_for_payment_details) {
                    //             if ($i == 0) {
                    //                 $budget_chart = BudgetChart::withSum(['transferBudgetFrom' => function ($query) use ($request_for_payment_details) {
                    //                     $query->where('month_from', strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                    //                         ->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                    //                 }], 'amount')
                    //                     ->withSum(['transferBudgetTo' => function ($query) use ($request_for_payment_details) {
                    //                         $query->where('month_to', strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                    //                             ->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                    //                     }], 'amount')
                    //                     ->withSum(['availment' => function ($query) use ($request_for_payment_details) {
                    //                         $query->where('month', strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                    //                             ->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                    //                     }], 'amount')
                    //                     ->withSum(['budgetPlan' => function ($query) use ($request_for_payment_details) {
                    //                         $query->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                    //                     }], strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                    //                     // ->withSum('budgetPlan', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
                    //                     ->find($request_for_payment->budget->budget_chart_id);
                    //             }
                    //         }
                    //     }
                    // }
                } else {
                    $trandates = date('F', strtotime($request_for_payment->date_of_transaction_from));
                    $budget_chart = BudgetChart::withSum(['transferBudgetFrom' => function ($query) use ($request_for_payment) {
                        $query->where('month_from', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
                            ->where('year', date('Y', strtotime($request_for_payment->date_of_transaction_from)));
                    }], 'amount')
                        ->withSum(['transferBudgetTo' => function ($query) use ($request_for_payment) {
                            $query->where('month_to', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
                                ->where('year', date('Y', strtotime($request_for_payment->date_of_transaction_from)));
                        }], 'amount')
                        ->withSum(['availment' => function ($query) use ($request_for_payment) {
                            $query->where('month', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
                                ->where('year', date('Y', strtotime($request_for_payment->date_of_transaction_from)));
                        }], 'amount')
                        ->withSum(['budgetPlan' => function ($query) use ($request_for_payment) {
                            $query->where('year', date('Y', strtotime($request_for_payment->date_of_transaction_from)));
                        }], strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
                        // ->withSum('budgetPlan', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
                        ->find($request_for_payment->budget->budget_chart_id);
                }
            } else {
                $identifypm = 'others';
                $trandates = date('F', strtotime($request_for_payment->date_of_transaction_from));
                $budget_chart = BudgetChart::withSum(['transferBudgetFrom' => function ($query) use ($request_for_payment) {
                    $query->where('month_from', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
                        ->where('year', date('Y', strtotime($request_for_payment->date_of_transaction_from)));
                }], 'amount')
                    ->withSum(['transferBudgetTo' => function ($query) use ($request_for_payment) {
                        $query->where('month_to', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
                            ->where('year', date('Y', strtotime($request_for_payment->date_of_transaction_from)));
                    }], 'amount')
                    ->withSum(['availment' => function ($query) use ($request_for_payment) {
                        $query->where('month', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
                            ->where('year', date('Y', strtotime($request_for_payment->date_of_transaction_from)));
                    }], 'amount')
                    ->withSum(['budgetPlan' => function ($query) use ($request_for_payment) {
                        $query->where('year', date('Y', strtotime($request_for_payment->date_of_transaction_from)));
                    }], strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
                    // ->withSum('budgetPlan', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
                    ->find($request_for_payment->budget->budget_chart_id);
            }


            // $budget_chart = BudgetChart::withSum(['transferBudgetFrom' => function ($query) use ($request_for_payment) {
            //     $query->where('month_from', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
            //         ->where('year', date('Y', strtotime($request_for_payment->date_of_transaction_from)));
            // }], 'amount')
            //     ->withSum(['transferBudgetTo' => function ($query) use ($request_for_payment) {
            //         $query->where('month_to', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
            //             ->where('year', date('Y', strtotime($request_for_payment->date_of_transaction_from)));
            //     }], 'amount')
            //     ->withSum(['availment' => function ($query) use ($request_for_payment) {
            //         $query->where('month', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
            //             ->where('year', date('Y', strtotime($request_for_payment->date_of_transaction_from)));
            //     }], 'amount')
            //     ->withSum(['budgetPlan' => function ($query) use ($request_for_payment) {
            //         $query->where('year', date('Y', strtotime($request_for_payment->date_of_transaction_from)));
            //     }], strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
            //     // ->withSum('budgetPlan', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
            //     ->find($request_for_payment->budget->budget_chart_id);

            $total_amount = ($budget_chart['budget_plan_sum_' . strtolower($trandates)] +
                $budget_chart->transfer_budget_to_sum_amount) - $budget_chart->transfer_budget_from_sum_amount;

            // dd($budget_chart['budget_plan_sum_december'], ($budget_chart['budget_plan_sum_' . strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from)))] +
            // $budget_chart->transfer_budget_to_sum_amount), $budget_chart->transfer_budget_from_sum_amount);
            $availed = ($budget_chart->availment_sum_amount + $request_for_payment->amount) - $budget_availment_created;
            $availed3 = ($budget_chart->availment_sum_amount) - $budget_availment_created;
            $plan_persent_availed = $availed && $total_amount ? ($availed / ($budget_chart->total_amount + $total_amount)) * 100 : 0;
            $plan_persent_availed3 = $availed3 && $total_amount ? ($availed3 / ($budget_chart->total_amount + $total_amount)) * 100 : 0;
            // $plan_persent_availed3 = $availed && $total_amount ? ($availed / ($budget_chart->total_amount)) * 100 : 0;

            // dd($availed , $total_amount, $availed3, $plan_persent_availed);

            if ($request_for_payment->approver_1_id == Auth::user()->id) {
                if ($request_for_payment->approver_1_id == $user_id || $level == 5 && $request_for_payment->approver_1_id) {
                    // if (!$validated['is_approved_1']) {
                    //     return $this->response(400, 'Bad Request', [
                    //         'approver_remarks_1' => 'The approver remarks 1 field is required.'
                    //     ]);
                    // }

                    if ($request_for_payment->is_approved_2 && !$validated['is_approved_1']) {
                        return $this->response(400, 'Bad Request', [
                            'approver_2_id' => 'The approver 2 must be disapproved.',
                        ]);
                    } else {
                        $request_for_payment->update([
                            'is_approved_1' => $validated['is_approved_1'] ?? 0,
                            'approver_date_1' => date('Y-m-d'),
                            'approver_remarks_1' => $validated['approver_remarks_1'],
                        ]);
                    }

                    $final_approved = false;
                }
            }

            if ($request_for_payment->approver_2_id == Auth::user()->id) {
                if ($validated['is_approved_1'] || !$validated['is_approved_2']) {
                    if ($request_for_payment->approver_2_id == $user_id || $level == 5 && $request_for_payment->approver_2_id) {
                        // if (!$validated['is_approved_2']) {
                        //     return $this->response(400, 'Bad Request', [
                        //         'approver_remarks_2' => 'The approver remarks 2 field is required.'
                        //     ]);
                        // }

                        if ($request_for_payment->is_approved_3 && !$validated['is_approved_2']) {
                            return $this->response(400, 'Bad Request', [
                                'approver_3_id' => 'The approver 3 must be disapproved.',
                            ]);
                        } else {
                            $request_for_payment->update([
                                'is_approved_2' => $validated['is_approved_2'] ?? 0,
                                'approver_date_2' => date('Y-m-d'),
                                'approver_remarks_2' => $validated['approver_remarks_2'],
                            ]);
                        }
                    }

                    if ($validated['is_approved_2'] && $request_for_payment->approver_3_id == '') {
                        // dd('enter', $request_for_payment->approver_3_id);
                        $final_approved = true;
                    } else {
                        // dd('enter2');

                        $final_approved = false;
                    }
                } else {
                    return $this->response(400, 'Bad Request', [
                        'approver_1_id' => 'The approver 1 must be approved.',
                    ]);
                }
            }

            if ($request_for_payment->approver_3_id == Auth::user()->id) {
                if ($validated['is_approved_2'] || !$validated['is_approved_3']) {
                    if ($request_for_payment->approver_3_id == $user_id || $level == 5 && $request_for_payment->approver_3_id) {
                        // if (!$validated['is_approved_3']) {
                        //     return $this->response(400, 'Bad Request', [
                        //         'approver_remarks_3' => 'The approver remarks 3 field is required.'
                        //     ]);
                        // }

                        $request_for_payment->update([
                            'is_approved_3' => $validated['is_approved_3'] ?? 0,
                            'approver_date_3' => date('Y-m-d'),
                            'approver_remarks_3' => $validated['approver_remarks_3'],
                        ]);
                    }

                    if ($validated['is_approved_3']) {
                        $final_approved = true;
                    } else {
                        $final_approved = false;
                    }
                } else {
                    return $this->response(400, 'Bad Request', [
                        'approver_2_id' => 'The approver 2 must be approved.',
                    ]);
                }
            }

            if ($request_for_payment->budgetAvailments) {
                // dd("delete")
                foreach ($request_for_payment->budgetAvailments as $budget_availment) {
                    $budget_availment->delete();
                }

                $request_for_payment_currents = RequestForPayment::where('reference_id', $request_for_payment->reference_id)
                    ->whereYear('created_at', date('Y', strtotime(now())))->get();

                foreach ($request_for_payment_currents as $i => $request_for_payment_current) {
                    if ($i) {
                        foreach ($request_for_payment_current->requestForPaymentDetails as $request_for_payment_details) {
                            $request_for_payment_details->delete();
                        }
                        $request_for_payment_current->delete();
                    }
                }
            }

            if ((!$validated['is_approved_2'] || !$validated['is_approved_3']) && $request_for_payment->checkVoucher) {
                if ($request_for_payment->checkVoucher->status_id == 5) {
                    return $this->response(500, 'Check Voucher', 'Cannot edit or reject when voucher is already approved.');
                }
            }

            if ($request_for_payment->type_id == 3 && $request_for_payment->requestForPaymentDetails[0]->payment_type_id == 2) {
                $current_yearss = $request_for_payment->requestForPaymentDetails()->whereYear('transaction_date', date('Y', strtotime(now())))->get();

                if ($current_yearss) {
                    foreach ($current_yearss as $request_for_payment_details) {
                        $trandates = date('F', strtotime($request_for_payment_details->transaction_date));
                        $budget_chart = BudgetChart::withSum(['transferBudgetFrom' => function ($query) use ($request_for_payment_details) {
                            $query->where('month_from', strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                                ->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                        }], 'amount')
                            ->withSum(['transferBudgetTo' => function ($query) use ($request_for_payment_details) {
                                $query->where('month_to', strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                                    ->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                            }], 'amount')
                            ->withSum(['availment' => function ($query) use ($request_for_payment_details) {
                                $query->where('month', strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                                    ->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                            }], 'amount')
                            ->withSum(['budgetPlan' => function ($query) use ($request_for_payment_details) {
                                $query->where('year', date('Y', strtotime($request_for_payment_details->transaction_date)));
                            }], strtolower(date('F', strtotime($request_for_payment_details->transaction_date))))
                            // ->withSum('budgetPlan', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
                            ->find($request_for_payment->budget->budget_chart_id);


                        $total_amount = ($budget_chart['budget_plan_sum_' . strtolower($trandates)] +
                            $budget_chart->transfer_budget_to_sum_amount) - $budget_chart->transfer_budget_from_sum_amount;

                        $plan_persent_availed = $availed && $total_amount ? ($availed / ($budget_chart->total_amount + $total_amount)) * 100 : 0;

                        if ($validated['approver_1_id'] == Auth::user()->id && $validated['approver_2_id'] == Auth::user()->id) {
                            if ($total_amount && $plan_persent_availed >= 98.3) {
                                return $this->response(500, 'Budget Plan', "You've reached your budget limit for this month (" . strtoupper($trandates) . '). Check your budget to proceed approval on this request');
                            } elseif (!$total_amount) {
                                return $this->response(500, 'Budget Plan', 'Budget Total Amount is 0');
                            }
                        }
                        if ($validated['approver_1_id'] != Auth::user()->id && $validated['approver_2_id'] == Auth::user()->id && $validated['is_approved_2']) {
                            // dd($total_amount, $plan_persent_availed);
                            if ($total_amount && $plan_persent_availed >= 98.3) {
                                return $this->response(500, 'Budget Plan', "You've reached your budget limit for this month (" . strtoupper($trandates) . '). Check your budget to proceed approval on this request');
                            } elseif (!$total_amount) {
                                return $this->response(500, 'Budget Plan', 'Budget Total Amount is 0');
                            }
                        }
                    }
                }
            } else {
                if ($validated['approver_1_id'] == Auth::user()->id && $validated['approver_2_id'] == Auth::user()->id) {
                    if ($total_amount && $plan_persent_availed >= 98.3) {
                        return $this->response(500, 'Budget Plan', "You've reached your budget limit for this month (" . strtoupper($trandates) . '). Check your budget to proceed approval on this request');
                    } elseif (!$total_amount) {
                        return $this->response(500, 'Budget Plan', 'Budget Total Amount is 0');
                    }
                }
                if ($validated['approver_1_id'] != Auth::user()->id && $validated['approver_2_id'] == Auth::user()->id && $validated['is_approved_2']) {
                    // dd($total_amount, $plan_persent_availed);
                    if ($total_amount && $plan_persent_availed >= 98.3) {
                        return $this->response(500, 'Budget Plan', "You've reached your budget limit for this month (" . strtoupper($trandates) . '). Check your budget to proceed approval on this request');
                    } elseif (!$total_amount) {
                        return $this->response(500, 'Budget Plan', 'Budget Total Amount is 0');
                    }
                }
            }

            if ($final_approved || $validated['is_approved_2']) {
                if ($budget_chart->availment_sum_amount <= 0) {
                    if ($total_amount && $plan_persent_availed >= 98.3 && $request_for_payment->approver_1_id != Auth::user()->id) {
                        if ($validated['is_approved_2'] == '1') {
                            return $this->response(500, 'Budget Plan', "You've reached your budget limit for this month (" . strtoupper($trandates) . '). Check your budget to proceed approval on this request');
                        }
                    } elseif (!$total_amount) {
                        return $this->response(500, 'Budget Plan', 'Budget Total Amount is 0');
                    }
                }
            }

            if ($final_approved || $validated['is_approved_3']) {
                if ($budget_chart->availment_sum_amount <= 0) {
                    if (
                        $total_amount && $plan_persent_availed3 >= 98.3 && $request_for_payment->approver_1_id != Auth::user()->id
                        && $request_for_payment->approver_2_id != Auth::user()->id
                    ) {
                        if ($validated['is_approved_3'] == '1') {
                            return $this->response(500, 'Budget Plan', "You've reached your budget limit for this month (" . strtoupper($trandates) . '). Check your budget to proceed approval on this request');
                        }
                    } elseif (!$total_amount) {
                        return $this->response(500, 'Budget Plan', 'Budget Total Amount is 0');
                    }
                }
            }

            $is_create_availment = false;
            if ($request_for_payment->approver_2_id || $request_for_payment->approver_3_id) {
                if ($validated['is_approved_2']) {
                    $is_create_availment = true;
                }
                if ($request_for_payment->approver_3_id == Auth()->user()->id) {
                    if ($request_for_payment->approver_3_id) {
                        if ($validated['is_approved_3']) {
                            $is_create_availment = true;
                        } elseif (isset($validated['is_approved_3'])) {
                            $is_create_availment = false;
                        }
                    }
                }
            }
            //   dd($is_create_availment);
            // if ($final_approved || $validated['is_approved_2'] || $validated['is_approved_3'] ) {
            if ($is_create_availment) {
                if ($request_for_payment->type_id == 2 && $request_for_payment->account_type_id == 1) {
                    foreach ($request_for_payment->requestForPaymentDetails as $request_for_payment_details) {
                        $request_for_payment->budgetAvailments()->create([
                            'division_id' => $request_for_payment->budget->division_id,
                            'budget_source_id' => $request_for_payment->budget->budget_source_id,
                            'budget_chart_id' => $request_for_payment->budget->budget_chart_id,
                            'budget_plan_id' => $request_for_payment->budget_id,
                            'amount' => $request_for_payment_details->amount,
                            'month' => strtolower(date('F', strtotime($request_for_payment_details->transaction_date))),
                            'year' => date('Y', strtotime($request_for_payment_details->transaction_date)),
                        ]);
                    }
                } elseif ($request_for_payment->type_id == 3) {
                    if ($request_for_payment->requestForPaymentDetails[0]->payment_type_id == 2) {
                        $current_year = $request_for_payment->requestForPaymentDetails()->whereYear('transaction_date', date('Y', strtotime(now())))->get();
                        $next_year = $request_for_payment->requestForPaymentDetails()->whereYear('transaction_date', '>', date('Y', strtotime(now())))->get();

                        if ($current_year) {
                            foreach ($current_year as $request_for_payment_details) {
                                $request_for_payment->budgetAvailments()->create([
                                    'division_id' => $request_for_payment->budget->division_id,
                                    'budget_source_id' => $request_for_payment->budget->budget_source_id,
                                    'budget_chart_id' => $request_for_payment->budget->budget_chart_id,
                                    'budget_plan_id' => $request_for_payment->budget_id,
                                    'amount' => $request_for_payment_details->amount,
                                    'month' => strtolower(date('F', strtotime($request_for_payment_details->transaction_date))),
                                    'year' => date('Y', strtotime($request_for_payment_details->transaction_date)),
                                ]);

                                // dd($request_for_payment_details->amount);

                            }
                        }
                        // if ($next_year) {
                        //     if ($next_year[0]->payment_type_id == 2) {
                        //         $request_for_payment_new = (object) [];
                        //         foreach ($next_year as $i => $request_for_payment_details) {
                        //             if ($i == 0) {
                        //                 $request_for_payment_new = RequestForPayment::create([
                        //                     'parent_reference_no' => 'PRNT-' . date('ymdhis') . '-' . rand(111, 999),
                        //                     'reference_id' => $request_for_payment_details->requestForPayment->reference_id,
                        //                     'type_id' => $request_for_payment_details->requestForPayment->type_id,
                        //                     'payee_id' => $request_for_payment_details->requestForPayment->payee_id,
                        //                     'description' => $request_for_payment_details->requestForPayment->description,
                        //                     'amount' => $next_year->sum('amount'),
                        //                     'remarks' => $request_for_payment_details->requestForPayment->remarks,
                        //                     'multiple_budget_id' => $request_for_payment_details->requestForPayment->multiple_budget_id,
                        //                     'user_id' => $request_for_payment_details->requestForPayment->user_id,
                        //                     'branch_id' => $request_for_payment_details->requestForPayment->branch_id,
                        //                     'priority_id' => $request_for_payment_details->requestForPayment->priority_id,
                        //                     'canvasser' => $request_for_payment_details->requestForPayment->canvasser,
                        //                     'canvassing_supplier_id' => $request_for_payment_details->requestForPayment->canvassing_supplier_id,
                        //                     'ca_no' => $request_for_payment_details->requestForPayment->ca_no,
                        //                     'ca_reference_type_id' => $request_for_payment_details->requestForPayment->ca_reference_type_id,
                        //                     'account_type_id' => $request_for_payment_details->requestForPayment->account_type_id,
                        //                     'date_needed' => $request_for_payment_details->requestForPayment->date_needed,
                        //                     'date_of_transaction_from' => $request_for_payment_details->requestForPayment->date_of_transaction_from,
                        //                     'date_of_transaction_to' => $request_for_payment_details->requestForPayment->date_of_transaction_to,
                        //                     'opex_type_id' => $request_for_payment_details->requestForPayment->opex_type_id,
                        //                     'status_id' => 1,
                        //                 ]);
                        //             }

                        //             $request_for_payment_new->requestForPaymentDetails()->create([
                        //                 'reference_id' => $request_for_payment_details->reference_id,

                        //                 'account_no' => $request_for_payment_details->account_no,
                        //                 'payment_type_id' => $request_for_payment_details->payment_type_id,
                        //                 'terms_id' => $request_for_payment_details->terms_id,
                        //                 'pdc_from' => $request_for_payment_details->pdc_from,
                        //                 'pdc_to' => $request_for_payment_details->pdc_to,

                        //                 'invoice' => $request_for_payment_details->invoice,
                        //                 'invoice_amount' => $request_for_payment_details->invoice_amount,
                        //                 'transaction_date' => $request_for_payment_details->transaction_date,
                        //                 'amount' => $request_for_payment_details->amount,
                        //             ]);
                        //         }
                        //     }
                        // }
                    } else {
                        $request_for_payment->budgetAvailments()->create([
                            'division_id' => $request_for_payment->budget->division_id,
                            'budget_source_id' => $request_for_payment->budget->budget_source_id,
                            'budget_chart_id' => $request_for_payment->budget->budget_chart_id,
                            'budget_plan_id' => $request_for_payment->budget_id,
                            'amount' => $request_for_payment->amount,
                            'month' => strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))),
                            'year' => date('Y', strtotime($request_for_payment->date_of_transaction_from)),
                        ]);
                    }
                } else {
                    $request_for_payment->budgetAvailments()->create([
                        'division_id' => $request_for_payment->budget->division_id,
                        'budget_source_id' => $request_for_payment->budget->budget_source_id,
                        'budget_chart_id' => $request_for_payment->budget->budget_chart_id,
                        'budget_plan_id' => $request_for_payment->budget_id,
                        'amount' => $request_for_payment->amount,
                        'month' => strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))),
                        'year' => date('Y', strtotime($request_for_payment->date_of_transaction_from)),
                    ]);
                }
            }

            $message = 'Approved';

            if (Auth::user()->id == $request_for_payment->approver_1_id) {
                if ($request_for_payment->approver_1_id && !$request_for_payment->is_approved_1 && $request_for_payment->approver_remarks_1) {
                    $request_for_payment->update([
                        'status_id' => 3,
                    ]);
                    $message = 'Rejected';
                }

                if ($request_for_payment->approver_1_id == Auth::user()->id && $request_for_payment->approver_2_id == Auth::user()->id) {
                    if ($request_for_payment->is_approved_1 && $request_for_payment->is_approved_2) {
                        $request_for_payment->update([
                            'status_id' => ($final_approved ? 2 : 1),
                        ]);
                    } else {
                        $request_for_payment->update([
                            'status_id' => 3,
                        ]);
                        $message = 'Rejected';
                    }
                }
            } elseif (Auth::user()->id == $request_for_payment->approver_2_id) {
                if ($request_for_payment->approver_2_id && !$request_for_payment->is_approved_2 && $request_for_payment->approver_remarks_2) {
                    $request_for_payment->update([
                        'status_id' => 3,
                    ]);
                    $message = 'Rejected';
                } else {
                    if ($request_for_payment->approver_2_id && $request_for_payment->approver_3_id) {
                        if ($request_for_payment->is_approved_2 && $request_for_payment->is_approved_3) {
                            $request_for_payment->update([
                                'status_id' => ($final_approved ? 2 : 1),
                            ]);
                        }
                    } elseif ($request_for_payment->approver_2_id && !$request_for_payment->approver_3_id) {
                        if ($request_for_payment->is_approved_2) {
                            $request_for_payment->update([
                                'status_id' => ($final_approved ? 2 : 1),
                            ]);
                        }
                    }
                }
            } elseif (Auth::user()->id == $request_for_payment->approver_3_id) {
                if ($request_for_payment->approver_3_id && !$request_for_payment->is_approved_3 && $request_for_payment->approver_remarks_3) {
                    $request_for_payment->update([
                        'status_id' => 3,
                    ]);
                    $message = 'Rejected';
                } else {
                    $request_for_payment->update([
                        'status_id' => ($final_approved ? 2 : 1),
                    ]);
                }
            } else {
                if ($request_for_payment->approver_2_id && $request_for_payment->approver_3_id) {
                    if ($request_for_payment->is_approved_2 && $request_for_payment->is_approved_3) {
                        $request_for_payment->update([
                            'status_id' => ($final_approved ? 2 : 1),
                        ]);
                    }
                } elseif ($request_for_payment->approver_2_id && !$request_for_payment->approver_3_id) {
                    if ($request_for_payment->is_approved_2) {
                        $request_for_payment->update([
                            'status_id' => ($final_approved ? 2 : 1),
                        ]);
                    }
                }
            }
            // dd('asd');
            // RFP-220914021839-219
            // $2y$10$xkTO9uiSWKjss9jQJ.wc7.syvCCDNeasRW4TwDLFQtNN3V4lWaRf6

            // if ($request_for_payment->approver_3_id && !$request_for_payment->is_approved_3 && $request_for_payment->approver_remarks_3 ) {
            //     $request_for_payment->update([
            //         'status_id' => 3
            //     ]);
            //     $message = 'Rejected';
            // } else if ($request_for_payment->approver_2_id && !$request_for_payment->is_approved_2 && $request_for_payment->approver_remarks_2) {
            //     $request_for_payment->update([
            //         'status_id' => 3
            //     ]);
            //     $message = 'Rejected';
            // } else if ($request_for_payment->approver_1_id && !$request_for_payment->is_approved_1 && $request_for_payment->approver_remarks_1) {
            //     $request_for_payment->update([
            //         'status_id' => 3
            //     ]);
            //     $message = 'Rejected';
            // } else {
            //     $request_for_payment->update([
            //         'status_id' => ($final_approved ? 2 : 1)
            //     ]);
            // }

            // $this->request_for_payments_notification_interface->approved($request_for_payment);

            if ($final_approved) {
                // $this->request_for_payments_notification_interface->finalApproved($request_for_payment);
            }

            DB::commit();

            return $this->response(200, 'Request For Payment Successfully ' . $message, $request_for_payment);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createRequestForPayment($validated, $total_amount, $user_id)
    {
        // dd($validated);
        $request_for_payment = RequestForPayment::create([
            'parent_reference_no' => $validated['parent_reference_number'],
            'reference_id' => $validated['reference_number'],
            'type_id' => $validated['rfp_type'],
            'is_payee_contact_person' => $validated['is_payee_contact_person'],
            'payee_id' => $validated['payee'],
            'subpayee' => $validated['subpayee'],
            'is_subpayee' => 0,
            'description' => $validated['description'],
            'amount' => $total_amount,
            'remarks' => $validated['remarks'],
            // 'multiple_budget_id' => $validated['aaaaaaaaaaaa'],
            'user_id' => $user_id,
            'branch_id' => $validated['branch'],
            'budget_id' => $validated['coa_category'],
            'date_needed' => $validated['date_needed'],
            'date_of_transaction_from' => $validated['date_of_transaction_from'],
            'date_of_transaction_to' => $validated['date_of_transaction_to'],
            'priority_id' => $validated['priority'],
            'canvasser' => $validated['canvasser'],
            'canvassing_supplier_id' => $validated['rfp_type'] == 1 ? $validated['canvassing_supplier_id'] : null,
            'ca_no' => $validated['rfp_type'] == 4 ? $validated['ca_no'] : null,
            'ca_reference_type_id' => $validated['rfp_type'] == 4 ? $validated['ca_reference_type'] : null,
            'account_type_id' => $validated['rfp_type'] == 2 ? $validated['accounting_account_type'] : null,
            'is_set_approver_1' => $validated['is_set_approver_1'],
            'approver_1_id' => $validated['approver_1_id'],
            'approver_2_id' => $validated['approver_2_id'],
            'approver_3_id' => $validated['approver_3_id'],
            'opex_type_id' => $validated['opex_type'],
        ]);

        $this->attachment($request_for_payment, $validated['attachments']);
        // $this->request_for_payments_notification_interface->create($request_for_payment);

        return $request_for_payment;
    }

    public function updateRequestForPayment(RequestForPayment $request_for_payment, $validated, $total_amount, $user_id)
    {
        if ($request_for_payment->approver_1_id != $validated['approver_1_id']) {
            $request_for_payment->update([
                'is_approved_1' => false,
                'approver_date_1' => null,
                'approver_remarks_1' => null,
            ]);
        }

        if ($request_for_payment->approver_2_id != $validated['approver_2_id']) {
            $request_for_payment->update([
                'is_approved_2' => false,
                'approver_date_2' => null,
                'approver_remarks_2' => null,
            ]);
        }

        if ($request_for_payment->approver_3_id != $validated['approver_3_id']) {
            $request_for_payment->update([
                'is_approved_3' => false,
                'approver_date_3' => null,
                'approver_remarks_3' => null,
            ]);
        }

        $request_for_payment->update([
            'parent_reference_no' => $validated['parent_reference_number'],
            'reference_id' => $validated['reference_number'],
            'type_id' => $validated['rfp_type'],
            'is_payee_contact_person' => $validated['is_payee_contact_person'],
            'payee_id' => $validated['payee'],
            'subpayee' => $validated['subpayee'],
            'is_subpayee' => 0,
            'description' => $validated['description'],
            'amount' => $total_amount,
            'remarks' => $validated['remarks'],
            // 'multiple_budget_id' => $validated['aaaaaaaaaaaa'],
            'branch_id' => $validated['branch'],
            'budget_id' => $validated['coa_category'],
            'date_needed' => $validated['date_needed'],
            'date_of_transaction_from' => $validated['date_of_transaction_from'],
            'date_of_transaction_to' => $validated['date_of_transaction_to'],
            'priority_id' => $validated['priority'],
            'canvasser' => $validated['canvasser'],
            'canvassing_supplier_id' => $validated['rfp_type'] == 1 ? $validated['canvassing_supplier_id'] : null,
            'ca_no' => $validated['rfp_type'] == 4 ? $validated['ca_no'] : null,
            'ca_reference_type_id' => $validated['rfp_type'] == 4 ? $validated['ca_reference_type'] : null,
            'account_type_id' => $validated['rfp_type'] == 2 ? $validated['accounting_account_type'] : null,
            'is_set_approver_1' => $validated['is_set_approver_1'],
            'approver_1_id' => $validated['approver_1_id'],
            'approver_2_id' => $validated['approver_2_id'],
            'approver_3_id' => $validated['approver_3_id'],
            'opex_type_id' => $validated['opex_type'],
        ]);

        $this->attachment($request_for_payment, $validated['attachments']);
        // $this->request_for_payments_notification_interface->update($request_for_payment);

        return $request_for_payment;
    }

    public function attachment($request_for_payment, $attachments)
    {
        DB::beginTransaction();
        try {
            $month = strtolower(date('F', strtotime($request_for_payment->created_at)));
            $year = date('Y', strtotime($request_for_payment->created_at));

            foreach ($attachments as $attachment) {
                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $request_for_payment->attachments()->find($attachment['id']);
                    $attachment_model->delete();
                } elseif ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(' ', '_', $year . '/' . $month . '/attachments/request_for_payment/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        Storage::disk('accounting_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } elseif (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('accounting_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }

                    $request_for_payment->attachments()->updateOrCreate(
                        [
                            'id' => $attachment['id'],
                        ],
                        [
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                        ]
                    );
                }
            }

            DB::commit();

            return $this->response(200, 'Request For Payment Successfully Attached', $request_for_payment);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function untaggedPO(RequestForPayment $request_for_payment)
    {
        DB::beginTransaction();
        try {
            $request_for_payment->update([
                'canvassing_supplier_id' => null,
                'is_approved_1' => false,
                'is_approved_2' => false,
                'is_approved_3' => false,
                'approver_date_1' => null,
                'approver_date_2' => null,
                'approver_date_3' => null,
                'approver_remarks_1' => null,
                'approver_remarks_2' => null,
                'approver_remarks_3' => null,
                'status_id' => 1,
            ]);

            foreach ($request_for_payment->requestForPaymentDetails as $request_for_payment_details) {
                $request_for_payment_details->forceDelete();
            }
            foreach ($request_for_payment->budgetAvailments as $budget_availment) {
                $budget_availment->delete();
            }

            DB::commit();

            return $this->response(200, 'Successfully Untagged P.O', $request_for_payment);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy(RequestForPayment $request_for_payment)
    {
        DB::beginTransaction();
        try {
            $request_for_payment->update([
                'status_id' => 1,
            ]);

            $request_for_payment->delete();

            foreach ($request_for_payment->budgetAvailments as $budget_availment) {
                $budget_availment->delete();
            }

            DB::commit();

            return $this->response(200, 'Request For Payment Successfully Deleted', $request_for_payment);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function restore(RequestForPayment $request_for_payment)
    {
        DB::beginTransaction();
        try {
            $request_for_payment->restore();

            DB::commit();

            return $this->response(200, 'Request For Payment Successfully Restored', $request_for_payment);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
