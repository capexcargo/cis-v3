<?php

namespace App\Repositories\Accounting;

use App\Interfaces\Accounting\CanvassingManagementInterface;
use App\Models\Accounting\Canvassing;
use App\Models\Accounting\CanvassingSupplier;
use App\Models\Accounting\CanvassingSupplierItem;
use App\Models\Accounting\Supplier;
use App\Models\Accounting\WaybillSeriesPo;
use App\Models\BranchReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class CanvassingManagementRepository implements CanvassingManagementInterface
{
    use ResponseTrait;

    public function create($validated, $user_id)
    {
        DB::beginTransaction();
        try {
            $canvassing = Canvassing::create([
                'reference_id' => $validated['reference_number'],
                'created_by' => $user_id
            ]);

            foreach ($validated['suppliers'] as $supplier) {
                $supplier_model = Supplier::updateOrCreate(
                    [
                        'company' => $supplier['company']
                    ],
                    [
                        'company' => $supplier['company'],
                        'industry_id' => $supplier['industry'],
                        'type_id' => $supplier['type'],
                        'branch_id' => $supplier['branch'],
                        'first_name' => $supplier['first_name'],
                        'middle_name' => $supplier['middle_name'],
                        'last_name' => $supplier['last_name'],
                        'email' => $supplier['email'],
                        'mobile_number' => $supplier['mobile_number'],
                        'telephone_number' => $supplier['telephone_number'],
                        'address' => $supplier['address'],
                        'terms' => $supplier['terms'],
                    ]
                );
                $canvassing_supplier = $canvassing->canvassingSupplier()->create([
                    'supplier_id' => $supplier_model->id
                ]);

                foreach ($supplier['items'] as $item) {
                    $item_model = $supplier_model->items()->updateOrCreate(
                        [
                            'name' => $item['name']
                        ],
                        [
                            'name' => $item['name'],
                            'unit_cost' => $item['unit_cost']
                        ]
                    );

                    $canvassing_supplier->canvassingSupplierItem()->create([
                        'supplier_item_id' => $item_model->id,
                        'unit_cost' => $item['unit_cost'],
                        'quantity' => $item['quantity'],
                        'total_amount' => $item['unit_cost'] * $item['quantity'],
                    ]);
                }
            }

            DB::commit();
            return $this->response(200, 'Canvassing Successfully Created', $canvassing);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($canvassing, $validated)
    {
        DB::beginTransaction();
        try {
            foreach ($validated['suppliers'] as $supplier) {
                $approve_item = false;
                if ($supplier['is_deleted'] && $supplier['id']) {
                    $canvassing_supplier = $canvassing->canvassingSupplier()->find($supplier['id']);

                    foreach ($canvassing_supplier->canvassingSupplierItem as $canvassing_supplier_item) {
                        $canvassing_supplier_item->delete();
                    }

                    $canvassing_supplier->delete();
                } else {
                    $supplier_model = Supplier::updateOrCreate(
                        [
                            'company' => $supplier['company']
                        ],
                        [
                            'company' => $supplier['company'],
                            'industry_id' => $supplier['industry'],
                            'type_id' => $supplier['type'],
                            'branch_id' => $supplier['branch'],
                            'first_name' => $supplier['first_name'],
                            'middle_name' => $supplier['middle_name'],
                            'last_name' => $supplier['last_name'],
                            'email' => $supplier['email'],
                            'mobile_number' => $supplier['mobile_number'],
                            'telephone_number' => $supplier['telephone_number'],
                            'address' => $supplier['address'],
                            'terms' => $supplier['terms'],
                        ]
                    );

                    $canvassing_supplier = $canvassing->canvassingSupplier()->updateOrCreate(
                        [
                            'id' => $supplier['id']
                        ],
                        [
                            'supplier_id' => $supplier_model->id,
                        ]
                    );

                    foreach ($supplier['items'] as $item) {
                        if ($item['is_deleted'] && $item['id']) {
                            $canvassing_supplier_item = $canvassing_supplier->canvassingSupplierItem()->find($item['id']);
                            $canvassing_supplier_item->delete();
                        } else {
                            $item_model = $supplier_model->items()->updateOrCreate(
                                [
                                    'name' => $item['name']
                                ],
                                [
                                    'name' => $item['name'],
                                    'unit_cost' => $item['unit_cost'],
                                ]
                            );

                            $canvassing_supplier->canvassingSupplierItem()->updateOrCreate(
                                [
                                    'id' => $item['id'],
                                ],
                                [
                                    'supplier_item_id' => $item_model->id,
                                    'unit_cost' => $item['unit_cost'],
                                    'quantity' => $item['quantity'],
                                    'total_amount' => $item['unit_cost'] * $item['quantity'],
                                    'recommended_status' => $item['recommended_status'],
                                    'final_status' => $item['final_status'],
                                ]
                            );

                            if ($item['final_status']) {
                                $approve_item = true;
                            }
                        }
                    }
                    if ($approve_item) {
                        if (!$canvassing_supplier->po_reference_no) {
                            $canvassing_supplier->update([
                                'po_reference_no' => "PO-" . date("ymdhis") . "-" . rand(111, 999)
                            ]);
                        }
                    } else {
                        $canvassing_supplier->update([
                            'po_reference_no' => null
                        ]);
                    }
                }
            }

            DB::commit();
            return $this->response(200, 'Canvassing Successfully Updated', $canvassing);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function deleteSupplier($canvassing_supplier_id)
    {
        DB::beginTransaction();
        try {
            $canvassing_supplier = CanvassingSupplier::with('canvassingSupplierItem')->findOrFail($canvassing_supplier_id);
            foreach ($canvassing_supplier->canvassingSupplierItem as $canvassing_supplier_item) {
                $canvassing_supplier_item->delete();
            }
            $canvassing_supplier->delete();

            DB::commit();
            return $this->response(200, 'Canvassing Supplier Successfully Deleted', $canvassing_supplier);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function deleteSupplierItem($canvassing_supplier_id, $canvassing_supplier_item_id)
    {
        DB::beginTransaction();
        try {
            $canvassing_supplier_item = CanvassingSupplierItem::where([['id', $canvassing_supplier_item_id], ['canvassing_supplier_id', $canvassing_supplier_id]])->first();
            $canvassing_supplier_item->delete();

            DB::commit();
            return $this->response(200, 'Canvassing Supplier Item Successfully Deleted', $canvassing_supplier_item);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function attachment($canvassing_supplier, $validated)
    {
        DB::beginTransaction();
        try {

            if ($validated['attachment']) {
                $month = strtolower(date('F', strtotime($canvassing_supplier->created_at)));
                $year = date('Y', strtotime($canvassing_supplier->created_at));

                $file_path = strtolower(str_replace(" ", "_", $year . '/' . $month . '/attachments/purchase_order/'));
                $file_name = date('mdYHis') . uniqid() . '.' . $validated['attachment']->extension();

                if (in_array($validated['attachment']->extension(), config('filesystems.image_type'))) {
                    $image_resize = Image::make($validated['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });

                    Storage::disk('accounting_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                } else if (in_array($validated['attachment']->extension(), config('filesystems.file_type'))) {
                    Storage::disk('accounting_gcs')->putFileAs($file_path, $validated['attachment'], $file_name);
                }

                $canvassing_supplier->attachments()->create([
                    'path' => $file_path,
                    'name' => $file_name,
                    'extension' => $validated['attachment']->extension(),
                ]);
            } else {
                return $this->response(400, 'Bad Request', [
                    'attachment' => 'The attachment empty.'
                ]);
            }

            DB::commit();
            return $this->response(200, 'Canvassing Supplier Successfully Attached File', $canvassing_supplier);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function create_waybill($waybill, $canvas_id)
    {


        DB::beginTransaction();
        try {

            $canvassing_supplier = CanvassingSupplier::with(['waybillseries', 'supplier' => function ($query) {
                $query->with('industry', 'type', 'branch');
            }, 'canvassingSupplierItem' => function ($query) {
                $query->with('supplierItem')->where('final_status', true);
            }])->findOrFail($canvas_id);
            // dd($canvassing_supplier->po_reference_no);

            foreach ($waybill as $i => $val) {

                $field_data = array(
                    'min' => $waybill[$i]['waybill_from'],
                    'max' => $waybill[$i]['waybill_to'],
                    'po_reference' => $canvassing_supplier->po_reference_no,
                    'branch' => strtoupper($waybill[$i]['branch_id']),
                    'created_by' => Auth::user()->name,
                    'pad_no' => 1

                );

                // dd($field_data);

                $ch = curl_init();                    // Initiate cURL
                $url = "https://server2.capex.com.ph/waybill/add_waybill_issued.php"; // Where you want to post data
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);  // Tell cURL you want to post something
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($field_data)); // Define what you want to post
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return the output in string format
                $output = curl_exec($ch); // Execute

                curl_close($ch); // Close cURL handle


                // $curl = curl_init();

                // curl_setopt_array($curl, array(
                //     CURLOPT_URL => "https://server2.capex.com.ph/waybill/add_waybill_issued.php",
                //     CURLOPT_RETURNTRANSFER => true,
                //     CURLOPT_ENCODING => "",
                //     CURLOPT_MAXREDIRS => 10,
                //     CURLOPT_TIMEOUT => 0,
                //     CURLOPT_FOLLOWLOCATION => true,
                //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                //     CURLOPT_CUSTOMREQUEST => "POST",
                //     CURLOPT_POSTFIELDS => json_encode($field_data),
                //     // CURLOPT_POSTFIELDS => "{\r\n\"min\":\"\",\r\n\"password\":\"123456\",\r\n\"grant_type\":\"password\",\r\n\"client_id\":\"11\",\r\n\"client_secret\":\"gLiarL8aB3aAMUxlvVtBgWkQZSjF9E7GEgSYXxg4\",\r\n\"scope\":\"full-access\"\r\n}",
                //     CURLOPT_HTTPHEADER => array(
                //         "Content-Type: application/json",
                //         "Cookie: laravel_session=eyJpdiI6Ijc2YzNCemQ1cGhScTBtWXRZNGJESkE9PSIsInZhbHVlIjoiM2ZIcEw5ZFlKc3FcL09Nc1VuUVNnNTFKRnRialFDZ2kxTWhKdDRMeTkxSlV1bDY0MDlqcTVVVCt5VEVUTnlYWmQiLCJtYWMiOiJmOTQwMjM2MTZlMzc3N2Y2OWRiZjBkNjA1OGY3MjY4MWMxNGJjODUwM2IzMjUyN2ZjODg5ODRjNTkzZjhkMDhhIn0%3D"
                //     ),
                // ));

                // $response = curl_exec($curl);
                // $res = json_decode($response, true);
                // curl_close($curl);
                // dd($output);



                $getbranchid = BranchReference::where('code', $waybill[$i]['branch_id'])->first();


                for ($x = $waybill[$i]['waybill_from']; $x <= $waybill[$i]['waybill_to']; ++$x) {


                    $wayb = WaybillSeriesPo::create([
                        'waybill' => strtoupper($x),
                        'branch_id' => $getbranchid->id,
                        'cvs_reference_id' => $canvas_id,
                    ]);
                }





                $i++;
            }




            DB::commit();
            return $this->response(200, 'Waybill Series has been successfully created', $wayb);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
