<?php

namespace App\Repositories\Accounting\SupplierManagement;

use App\Interfaces\Accounting\SupplierManagement\SupplierInterface;
use App\Models\Accounting\Supplier;
use App\Models\Accounting\SupplierIndustryReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class SupplierRepository implements SupplierInterface
{
    use ResponseTrait;

    public function create($validated)
    {
        DB::beginTransaction();
        try {
            $supplier_checked = Supplier::where([
                ['company', $validated['company']]
            ])->first();
            if ($supplier_checked) {
                return $this->response(400, 'Bad Request', [
                    'company' => 'The company has already been taken.'
                ]);
            }

            $supplier = Supplier::create([
                'company' => $validated['company'],
                'industry_id' => $validated['industry'],
                'type_id' => $validated['type'],
                'branch_id' => $validated['branch'],
                'first_name' => $validated['first_name'],
                'middle_name' => $validated['middle_name'],
                'last_name' => $validated['last_name'],
                'email' => $validated['email'],
                'mobile_number' => $validated['mobile_number'],
                'telephone_number' => $validated['telephone_number'],
                'address' => $validated['address'],
                'terms' => $validated['terms'],
            ]);

            DB::commit();
            return $this->response(200, 'Supplier Successfully Created', $supplier);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($supplier, $validated)
    {
        DB::beginTransaction();
        try {

            $supplier_checked = Supplier::where([
                ['company', $validated['company']]
            ])->first();
            if ($supplier_checked && $supplier_checked->id != $supplier->id) {
                return $this->response(400, 'Bad Request', [
                    'company' => 'The company has already been taken.'
                ]);
            }

            $supplier->update([
                'company' => $validated['company'],
                'industry_id' => $validated['industry'],
                'type_id' => $validated['type'],
                'branch_id' => $validated['branch'],
                'first_name' => $validated['first_name'],
                'middle_name' => $validated['middle_name'],
                'last_name' => $validated['last_name'],
                'email' => $validated['email'],
                'mobile_number' => $validated['mobile_number'],
                'telephone_number' => $validated['telephone_number'],
                'address' => $validated['address'],
                'terms' => $validated['terms'],
            ]);

            DB::commit();
            return $this->response(200, 'Suppler Successfully Updated', $supplier);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createItem($supplier, $validated)
    {
        DB::beginTransaction();
        try {
            $supplier_item_checked = $supplier->items()->where('name', $validated['name'])->first();

            if ($supplier_item_checked) {
                return $this->response(400, 'Bad Request', [
                    'name' => 'The name has already been taken.'
                ]);
            }

            $supplier->items()->create([
                'name' => $validated['name']
            ]);

            DB::commit();
            return $this->response(200, 'Supplier Item Successfully Created', $supplier);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateItem($supplier, $supplier_item, $validated)
    {
        DB::beginTransaction();
        try {
            $supplier_item_checked = $supplier->items()->where('name', $validated['name'])->first();

            if ($supplier_item_checked && $supplier_item_checked->id != $supplier_item->id) {
                return $this->response(400, 'Bad Request', [
                    'name' => 'The name has already been taken.'
                ]);
            }

            $supplier_item->update([
                'name' => $validated['name']
            ]);

            DB::commit();
            return $this->response(200, 'Supplier Item Successfully Updated', $supplier_item);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createIndustry($validated)
    {
        DB::beginTransaction();
        try {

            $industry_checked = SupplierIndustryReference::where('display', $validated['name'])->first();

            if ($industry_checked) {
                return $this->response(400, 'Bad Request', [
                    'name' => 'The name has already been taken.'
                ]);
            }

            $industry = SupplierIndustryReference::create([
                'code' => strtolower(str_replace(" ", "_", $validated['name'])),
                'display' => $validated['name'],
            ]);

            DB::commit();
            return $this->response(200, 'Supplier Industry Successfully Created', $industry);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function updateIndustry($industry, $validated)
    {
        DB::beginTransaction();
        try {
            
            $industry_checked = SupplierIndustryReference::where('display', $validated['name'])->first();

            if ($industry_checked && $industry_checked->id != $industry->id) {
                return $this->response(400, 'Bad Request', [
                    'name' => 'The name has already been taken.'
                ]);
            }

            $industry->update([
                'code' => strtolower(str_replace(" ", "_", $validated['name'])),
                'display' => $validated['name'],
            ]);

            DB::commit();
            return $this->response(200, 'Supplier Industry Successfully Updated', $industry);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
