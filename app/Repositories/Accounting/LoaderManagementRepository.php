<?php

namespace App\Repositories\Accounting;

use App\Interfaces\Accounting\LoaderManagementInterface;
use App\Models\Accounting\Loaders;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LoaderManagementRepository implements LoaderManagementInterface
{
    use ResponseTrait;

    public function create($request)
    {
        DB::beginTransaction();
        try {

            $validator = Validator::make($request, [
                'name' => 'required|unique:accounting_loaders,name',
                'loaders_type_id' => 'required',
                'code' => 'required',
            ], [
                'loaders_type_id.required' => 'Loader Type is required.',
                'name.required' => 'Loader Name is required.',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            $validated = $validator->validated();

            $loaders = Loaders::create([
                'loaders_type_id' => $validated['loaders_type_id'],
                'code' => strtolower(str_replace(" ", "_", $validated['code'])),
                'name' => $validated['name'],
                'address' => $validated['name'],
            ]);

            DB::commit();
            return $this->response(200, 'Loader Successfully Created', $loaders);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {

            $validator = Validator::make($request, [
                'name' => 'required|unique:accounting_loaders,name,' . $id . ',id',
                'loaders_type_id' => 'required',
                'code' => 'required',
            ], [
                'loaders_type_id.required' => 'Loader Type is required.',
                'name.required' => 'Loader Name is required.',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            $validated = $validator->validated();
            // dd($validated);

            $loaders = Loaders::find($id);
            if (!$loaders) {
                return $this->response(404, 'Not Found', null);
            }

            $loaders->update([
                'loaders_type_id' => $validated['loaders_type_id'],
                'code' => strtolower(str_replace(" ", "_", $validated['code'])),
                'name' => $validated['name'],
                'address' => $validated['name'],
            ]);

            DB::commit();
            return $this->response(200, 'Loader Successfully Updated', $loaders);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update2($code, $request)
    {
        DB::beginTransaction();
        try {

            $validator = Validator::make($request, [
                'name' => 'required|unique:accounting_loaders,name,' . $code . ',code',
                'loaders_type_id' => 'required',
                'code' => 'required',
            ], [
                'loaders_type_id.required' => 'Loader Type is required.',
                'name.required' => 'Loader Name is required.',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            $validated = $validator->validated();

            $loaders = Loaders::where('code', $code);
            if (!$loaders) {
                return $this->response(404, 'Not Found', null);
            }

            $loaders->update([
                'loaders_type_id' => $validated['loaders_type_id'],
                'code' => strtolower(str_replace(" ", "_", $validated['code'])),
                'name' => $validated['name'],
                'address' => $validated['name'],
            ]);

            DB::commit();
            return $this->response(200, 'Loader Successfully Updated', $loaders);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
