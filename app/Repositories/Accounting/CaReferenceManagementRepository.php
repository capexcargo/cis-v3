<?php

namespace App\Repositories\Accounting;

use App\Interfaces\Accounting\CaReferenceManagementInterface;
use App\Models\Accounting\CAReferenceTypes;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class CaReferenceManagementRepository implements CaReferenceManagementInterface
{
    use ResponseTrait;

    public function create($validated)
    {
        DB::beginTransaction();
        try {

            $ca_reference_checked = CAReferenceTypes::where('display', $validated['display'])->first();

            if ($ca_reference_checked) {
                return $this->response(400, 'Bad Request', [
                    'name' => 'The CA Reference Type has already been taken.'
                ]);
            }

            $ca_reference = CAReferenceTypes::create([
                'display' => $validated['display'],
                'code' => strtolower(str_replace(" ","_", $validated['display'])),
            ]);

            DB::commit();
            return $this->response(200, 'CA Reference Type Successfully Created', $ca_reference);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($ca_reference, $validated)
    {
        DB::beginTransaction();
        try {

            $ca_reference_checked = CAReferenceTypes::where('display', $validated['display'])->first();

            if ($ca_reference_checked && $ca_reference_checked->id != $ca_reference_checked->id) {
                return $this->response(400, 'Bad Request', [
                    'name' => 'The CA Reference Type has already been taken.'
                ]);
            }
            
            $ca_reference->update([
                'display' => $validated['display'],
                'code' => strtolower(str_replace(" ","_", $validated['display'])),
            ]);
            
            DB::commit();
            return $this->response(200, 'CA Reference Type Successfully Updated', $ca_reference);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

}
