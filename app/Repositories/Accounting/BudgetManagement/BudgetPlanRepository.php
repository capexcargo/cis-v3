<?php

namespace App\Repositories\Accounting\BudgetManagement;

use App\Interfaces\Accounting\BudgetManagement\BudgetPlanInterface;
use App\Models\Accounting\BudgetPlan;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class BudgetPlanRepository implements BudgetPlanInterface
{
    use ResponseTrait;

    public function create($validated)
    {
        DB::beginTransaction();
        try {
            $total_amount = 0;

            $total_amount += $validated['january'] +
                $validated['february'] +
                $validated['march'] +
                $validated['april'] +
                $validated['may'] +
                $validated['june'] +
                $validated['july'] +
                $validated['august'] +
                $validated['september'] +
                $validated['october'] +
                $validated['november'] +
                $validated['december'];

            $budget_plan = BudgetPlan::create([
                'rfp_type_id' => $validated['request_type'],
                'budget_source_id' => $validated['source'],
                'budget_chart_id' => $validated['chart'],
                'item' => $validated['item'],
                'division_id' => $validated['division'],
                'opex_type_id' => $validated['opex_type'],
                'year' => $validated['year'],

                'january' => $validated['january'],
                'february' => $validated['february'],
                'march' => $validated['march'],
                'april' => $validated['april'],
                'may' => $validated['may'],
                'june' => $validated['june'],
                'july' => $validated['july'],
                'august' => $validated['august'],
                'september' => $validated['september'],
                'october' => $validated['october'],
                'november' => $validated['november'],
                'december' => $validated['december'],

                'total_amount' => $total_amount,
            ]);

            DB::commit();
            return $this->response(200, 'Budget Plan Successfully Created', $budget_plan);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($budget_plan, $validated)
    {
        DB::beginTransaction();
        try {
            $total_amount = 0;

            $total_amount += $validated['january'] +
                $validated['february'] +
                $validated['march'] +
                $validated['april'] +
                $validated['may'] +
                $validated['june'] +
                $validated['july'] +
                $validated['august'] +
                $validated['september'] +
                $validated['october'] +
                $validated['november'] +
                $validated['december'];

            $budget_plan->update([
                'rfp_type_id' => $validated['request_type'],
                'budget_source_id' => $validated['source'],
                'budget_chart_id' => $validated['chart'],
                'item' => $validated['item'],
                'division_id' => $validated['division'],
                'opex_type_id' => $validated['opex_type'],
                'year' => $validated['year'],

                'january' => $validated['january'],
                'february' => $validated['february'],
                'march' => $validated['march'],
                'april' => $validated['april'],
                'may' => $validated['may'],
                'june' => $validated['june'],
                'july' => $validated['july'],
                'august' => $validated['august'],
                'september' => $validated['september'],
                'october' => $validated['october'],
                'november' => $validated['november'],
                'december' => $validated['december'],

                'total_amount' => $total_amount,
            ]);
            DB::commit();
            return $this->response(200, 'Budget Plan Successfully Updated', $budget_plan);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function transferBudget($budget_plan_from, $budget_plan_to, $validated, $validated_amount)
    {
        DB::beginTransaction();
        try {
            $budget_plan_to->transferBudgetTo()->create([
                'division_id_from' => $budget_plan_from->division_id ?? null,
                'division_id_to' => $budget_plan_to->division_id,
                'budget_source_id_from' => $budget_plan_from->budget_source_id ?? null,
                'budget_source_id_to' => $budget_plan_to->budget_source_id,
                'budget_chart_id_from' => $budget_plan_from->budget_chart_id ?? null,
                'budget_chart_id_to' => $budget_plan_to->budget_chart_id,
                'budget_plan_id_from' => $budget_plan_from->id ?? null,
                'transfer_type_id' => $validated['transfer_type'],
                'month_from' => $validated['month_from'],
                'month_to' => $validated['month_to'],
                'amount' => $validated['transfer_type'] == 1 ? $validated['amount'] : $validated_amount['amount'],
                'year' => $budget_plan_to->year,
            ]);
            DB::commit();
            return $this->response(200, 'Transfer Budget Successfully Added', $budget_plan_to);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
