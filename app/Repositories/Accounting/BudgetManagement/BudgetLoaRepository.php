<?php

namespace App\Repositories\Accounting\BudgetManagement;

use App\Interfaces\Accounting\BudgetManagement\BudgetLoaInterface;
use App\Models\Accounting\BudgetLoa;
use App\Models\Accounting\BudgetPlan;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class BudgetLoaRepository implements BudgetLoaInterface
{
    use ResponseTrait;

    public function create($validated)
    {
        DB::beginTransaction();
        try {
            $budget_plan = BudgetPlan::withCount('budgetLoa')->findOrFail($validated['budget_plan']);

            if ($budget_plan->budget_loa_count > 2) {
                return $this->response(400, 'Bad Request', [
                    'budget_plan' => 'The budget plan loa is already 3.'
                ]);
            }

            $budget_loa = $budget_plan->budgetLoa()->create([
                'limit_min_amount' => $validated['limit_min_amount'],
                'limit_max_amount' => $validated['limit_max_amount'],
                'first_approver_id' => $validated['first_approver'],
                'second_approver_id' => $validated['second_approver'],
                'third_approver_id' => $validated['third_approver'],
            ]);

            DB::commit();
            return $this->response(200, 'Budget Chart Successfully Created', $budget_loa);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($budget_loa, $validated)
    {
        DB::beginTransaction();
        try {
            $budget_loa->update([
                'limit_min_amount' => $validated['limit_min_amount'],
                'limit_max_amount' => $validated['limit_max_amount'],
                'first_approver_id' => $validated['first_approver'],
                'second_approver_id' => $validated['second_approver'] ?? null,
                'third_approver_id' => $validated['third_approver'] ?? null,
            ]);

            $budget_loa_others = BudgetLoa::where('budget_plan_id', $budget_loa->budget_plan_id)->get();
            $total_loas = count($budget_loa_others);
            foreach ($budget_loa_others as $a => $budget_loa_other) {
                $previous = ($a < 1 ? 0 : ($a - 1));
                $next = ($a > 1 ? ($total_loas - 1) : ($a + 1));

                if ($a) {
                    // INCREMENT
                    if ($budget_loa_others[$previous]->limit_max_amount >= $budget_loa_other->limit_min_amount) {
                        $distance_amount = $budget_loa_other->limit_max_amount - $budget_loa_other->limit_min_amount;
                        $budget_loa_other->update([
                            'limit_min_amount' => ($budget_loa_others[$previous]->limit_max_amount + 0.01),
                            'limit_max_amount' => ($budget_loa_others[$previous]->limit_max_amount + 0.01 + $distance_amount),
                        ]);
                    }
                }

                if ($next < $total_loas) {
                    // DECREMENT
                    $distance_budget = $budget_loa_others[$next]->limit_min_amount - $budget_loa_other->limit_max_amount;
                    if (round($distance_budget, 2) > 0.01) {
                        $distance_amount = $budget_loa_others[$next]->limit_max_amount - $budget_loa_others[$next]->limit_min_amount;
                        $budget_loa_others[$next]->update([
                            'limit_min_amount' => $budget_loa_other->limit_max_amount,
                            'limit_max_amount' => $budget_loa_other->limit_max_amount + $distance_amount,
                        ]);
                    }
                }
            }

            DB::commit();
            return $this->response(200, 'Budget Chart Successfully Updated', $budget_loa);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
