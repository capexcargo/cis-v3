<?php

namespace App\Repositories\Accounting\BudgetManagement;

use App\Interfaces\Accounting\BudgetManagement\BudgetChartInterface;
use App\Models\Accounting\BudgetChart;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class BudgetChartRepository implements BudgetChartInterface
{
    use ResponseTrait;

    public function create($validated)
    {
        DB::beginTransaction();
        try {
            $budget_chart_checked = BudgetChart::where([
                ['division_id', $validated['division']],
                ['budget_source_id', $validated['source']],
                ['name', $validated['name']]
            ])->first();
            if ($budget_chart_checked) {
                return $this->response(400, 'Bad Request', [
                    'name' => 'The name must be unique in each source.'
                ]);
            }
            
            $budget_chart = BudgetChart::create([
                'division_id' => $validated['division'],
                'budget_source_id' => $validated['source'],
                'name' => $validated['name'],
            ]);

            DB::commit();
            return $this->response(200, 'Budget Chart Successfully Created', $budget_chart);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($budget_chart, $validated)
    {
        DB::beginTransaction();
        try {
            $budget_chart_checked = BudgetChart::where([
                ['division_id', $validated['division']],
                ['budget_source_id', $validated['source']],
                ['name', $validated['name']]
            ])->first();
            
            if ($budget_chart_checked && $budget_chart_checked->id != $budget_chart->id) {
                return $this->response(400, 'Bad Request', [
                    'name' => 'The name must be unique in each source.'
                ]);
            }
           
            $budget_chart->update([
                'division_id' => $validated['division'],
                'budget_source_id' => $validated['source'],
                'name' => $validated['name'],
            ]);

            DB::commit();
            return $this->response(200, 'Budget Chart Successfully Updated', $budget_chart);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
