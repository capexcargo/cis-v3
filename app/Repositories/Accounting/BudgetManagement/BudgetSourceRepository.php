<?php

namespace App\Repositories\Accounting\BudgetManagement;

use Exception;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use App\Models\Accounting\BudgetSource;
use Illuminate\Support\Facades\Validator;
use App\Interfaces\Accounting\BudgetManagement\BudgetSourceInterface;
use App\Models\FimsBudgetSource;

class BudgetSourceRepository implements BudgetSourceInterface
{
    use ResponseTrait;

    public function create($validated)
    {
        DB::beginTransaction();
        try {
            $budget_source_checked = BudgetSource::where([['division_id', $validated['division']], ['name', $validated['name']]])->first();
            if ($budget_source_checked) {
                return $this->response(400, 'Bad Request', [
                    'name' => 'The name must be unique in each division.'
                ]);
            }

            $budget_source = BudgetSource::create([
                'division_id' => $validated['division'],
                'name' => $validated['name'],
            ]);

            DB::commit();
            return $this->response(200, 'Budget Source Successfully Created', $budget_source);
        } catch (Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($budget_source, $validated)
    {
        DB::beginTransaction();
        try {
            $budget_source_checked = BudgetSource::where([['division_id', $validated['division']], ['name', $validated['name']]])->first();
            if ($budget_source_checked && $budget_source_checked->id != $budget_source->id) {
                return $this->response(400, 'Bad Request', [
                    'name' => 'The name must be unique in each division.'
                ]);
            }

            $budget_source->update([
                'division_id' => $validated['division'],
                'name' => $validated['name'],
            ]);

            DB::commit();
            return $this->response(200, 'Budget Source Successfully Updated', $budget_source);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // ========================================================================================================

    public function createValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required'=> 'Budget Source is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create2($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);
                $service_ctg = FimsBudgetSource::create([
                    'name' => $validated['name'],
                    'status' => 1,
                ]);

            DB::commit();

            return $this->response(200, 'Purpose has been successfully added!', ($service_ctg));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index2($request, $search_request)
    {
        DB::beginTransaction();
        try {
            $purpose_lists = FimsBudgetSource::when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', 'like', '%' . $search_request['name'] . '%');
            })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $purpose_lists,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required'=> 'Budget Source is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update2($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->updateValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);

            $purposeName = FimsBudgetSource::findOrFail($id);
                $purposeName->update([
                    'name' => $validated['name'],
                    'status' => 1,
                ]);

            DB::commit();
            return $this->response(200, 'Budget Source has been successfully updated!', ($purposeName));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
