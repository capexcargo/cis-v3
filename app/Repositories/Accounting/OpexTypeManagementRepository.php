<?php

namespace App\Repositories\Accounting;

use App\Interfaces\Accounting\OpexTypeManagementInterface;
use App\Models\Accounting\OpexType;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class OpexTypeManagementRepository implements OpexTypeManagementInterface
{
    use ResponseTrait;

    public function create($validated)
    {
        DB::beginTransaction();
        try {

            $opex_type_checked = OpexType::where('name', $validated['name'])->first();

            if ($opex_type_checked) {
                return $this->response(400, 'Bad Request', [
                    'name' => 'The name has already been taken.'
                ]);
            }

            $opex_type = OpexType::create([
                'name' => $validated['name'],
                'description' => $validated['description']
            ]);

            DB::commit();
            return $this->response(200, 'Opex Type Successfully Created', $opex_type);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($opex_type, $validated)
    {
        DB::beginTransaction();
        try {

            $opex_type_checked = OpexType::where('name', $validated['name'])->first();

            if ($opex_type_checked && $opex_type_checked->id != $opex_type->id) {
                return $this->response(400, 'Bad Request', [
                    'name' => 'The name has already been taken.'
                ]);
            }
            
            $opex_type->update([
                'name' => $validated['name'],
                'description' => $validated['description']
            ]);
            
            DB::commit();
            return $this->response(200, 'Opex Type Successfully Updated', $opex_type);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
