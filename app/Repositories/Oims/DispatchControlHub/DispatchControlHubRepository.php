<?php

namespace App\Repositories\Oims\DispatchControlHub;

use App\Interfaces\Oims\DispatchControlHub\DispatchControlHubInterface;
use App\Models\CrmBooking;
use App\Models\DispatchControlHubReference;
use App\Models\Hrim\AreaReference;
use App\Models\OimsAreaReference;
use App\Models\OimsBranchReference;
use App\Models\OimsEdtr;
use App\Models\OimsEdtrDetails;
use App\Models\OimsTeamRouteAssignment;
use App\Models\OimsTeamRouteAssignmentDetails;
use App\Models\OimsTransactionEntry;
use App\Traits\InitializeFirestoreTrait;
use App\Traits\ResponseTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DispatchControlHubRepository implements DispatchControlHubInterface
{
    use ResponseTrait, InitializeFirestoreTrait;

    public function index($request, $actStatusHead)
    {
        // dd($request, $actStatusHead);
        DB::beginTransaction();
        try {
            if ($actStatusHead == 2) {
                $pickup_lists = OimsTransactionEntry::with(
                    'CrmBookingReference',
                    'OimsBranchReference',
                    'ShipperReference',
                    'OriginReference',
                    'DestinationReference',
                )->where('is_dispatched', null)
                ->where('destination_id', $request['branch_id'])
                    ->when($request['stats'], function ($query) use ($request) {
                        if ($request['stats'] == false) {
                            $query->whereIn('transType', [1, 2]);
                        } else {
                            $query->where('transType', $request['stats']);
                        }
                    })
                    ->paginate($request['paginate']);
            } else {
                $pickup_lists = CrmBooking::with(
                    'BookingTypeReferenceBK',
                    'VehicleTypeReferenceBK',
                    'TimeslotReferenceBK',
                    'WalkinReferenceBK',
                    'ActivityReferenceBK',
                    'ShipperReferenceBK',
                    'FinalStatusReferenceBK',
                    'BookingBranchReferenceBK',
                    'MarketingChannelReferenceBK',
                    'CreatedByBK',
                    'BookingAttachmentHasManyBK',
                    'BookingLogsHasManyBK',
                    'BookingRemarksHasManyBK',
                    'BookingConsigneeHasManyBK',
                    'attachments',
                    'BookingShipper',
                    'BookingChannelReferenceBK',
                )->whereIn('final_status_id', [6, 1, 8])
                    ->where('booking_branch_id', $request['branch_id'])
                    ->when($request['stats'], function ($query) use ($request) {
                        if ($request['stats'] == false) {
                            $query->whereIn('final_status_id', [6, 1, 8]);
                        } else {
                            $query->where('final_status_id', $request['stats']);
                        }
                    })
                    ->paginate($request['paginate']);
            }
            DB::commit();

            return $this->response(200, 'List', compact('pickup_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function dispatch($request)
    {
        DB::beginTransaction();
        try {
            $team_lists = OimsTeamRouteAssignmentDetails::with(
                'teamRouteReference',
                'teamPlateReference',
                'teamDriverReference',
                'teamChecker1Reference',
                'teamChecker2Reference'
            )->whereHas('teamIdReference', function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request['tn'] . '%');
            })
                ->paginate($request['paginate']);
            DB::commit();
            return $this->response(200, 'List', compact('team_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'rdis' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'rdis.required' => 'Select team to dispatch',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            if ($request['actStatusHead'] == 1 || $request['actStatusHead'] == null) {

                $databranch = CrmBooking::with('BookingBranchReferenceBK')->where('booking_branch_id', $request['branch_id'])->first();
                $ref_no = $databranch->BookingBranchReferenceBK->code . '-' . strtoupper(uniqid());
                $get_ref_no = OimsEdtr::where('dispatch_date', Carbon::today())->where('tra_id', $request['rdis'])->first();

                $edtr = OimsEdtr::create([
                    'reference_no' => ($get_ref_no == null ? $ref_no : $get_ref_no['reference_no']),
                    'tra_id' => $request['rdis'],
                    'dispatch_date' => date('Y-m-d'),
                    'branch_id' => $request['branch_id'],
                    'status' => 2,
                    'created_by' => Auth::user()->id,
                ]);



                //firestore oimsedtr
                $collectionEDTRReference = $this->initializeFirestore()->collection('oims_edtr');
                $documentEDTRReference = $collectionEDTRReference->add([
                    'id' => $edtr->id,
                    'reference_no' => $ref_no,
                    'tra_id' => $request['rdis'],
                    'dispatch_date' => date('Y-m-d'),
                    'branch_id' => $request['branch_id'],
                    'status' => 2,
                    'created_by' => Auth::user()->id,
                ]);

                $get_edtr = OimsTeamRouteAssignmentDetails::where('id', $request['rdis'])->first();
                // dd($get_edtr['checker1_id'],$get_edtr['checker2_id']);

                $booking = [];

                foreach ($request['selecteds'] as $i => $select) {
                    $booking = CrmBooking::with('BookingShipper')->where('id', $select)->first();
                    $branchdata = OimsBranchReference::where('id', $request['branch_id'])->first();

                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . urlencode($branchdata->address) .
                            "&destinations=" . urlencode($booking->BookingShipper->address) . "&mode=motorcycle&language=en-EN&sensor=false&key=" . env('GOOGLE_API_KEY'),
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_HTTPHEADER => array(
                            "cache-control: no-cache"
                        ),
                    ));
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    $res = json_decode($response, true);

                   # dd($branchdata->address, $booking->BookingShipper->address);

                    $durationInSeconds = $res['rows'][0]['elements'][0]['duration']['value']; // Replace with your actual duration
                    $formattedTimestamp = date('Y-m-d H:i:s', strtotime("1970-01-01 +$durationInSeconds seconds"));
                    $formattedTimestampFinal = date('H:i:s', strtotime($formattedTimestamp));

                    $edtrdet = OimsEdtrDetails::create([
                        'edtr_id' => $edtr->id,
                        'edtr_item_id' => $booking['id'],
                        'edtr_item_type' => 1,
                        'estimated_time_travel' => $formattedTimestampFinal,
                        'estimated_time_activity' => '00:15:00',
                    ]);
                    // dd($booking['id']);

                    $pbooking = CrmBooking::where('id',  $booking['id'])->update([
                        'final_status_id' => 2,
                    ]);
                    // dd($pbooking);

                    //firestore oims edtr details
                    $collectionEdtrDetailsReference = $this->initializeFirestore()->collection('oims_edtr_details');
                    $documentEdtrDetailsReference = $collectionEdtrDetailsReference->add([
                        'id' => $edtrdet->id,
                        'edtr_id' => $edtr->id,
                        'edtr_item_id' => $booking['id'],
                        'edtr_item_type' => 1,
                        'estimated_time_travel' => $formattedTimestampFinal,
                        'estimated_time_activity' => '00:15:00',
                    ]);

                    //firestore add checker and team route id in orders
                    $collectionOrdersReference = $this->initializeFirestore()->collection('orders');
                    $queryOrders = $collectionOrdersReference->where('booking_reference_no_2', '=', $booking->booking_reference_no);
                    $documentsOrders = $queryOrders->documents();

                    foreach ($documentsOrders as $documentOrder) {
                        $documentOrder->reference()->set([
                            'checker1_id' => $get_edtr['checker1_id'],
                            'checker2_id' => $get_edtr['checker2_id'],
                            'team_route_id' => intval($request['rdis']),
                            'date_dispatched' => $edtr->dispatch_date,
                            'final_status' => 2,
                            'booking_id' => $booking['id'],
                        ], ['merge' => true]);
                    }
                }

            } elseif ($request['actStatusHead'] == 2) {
                $databranch = OimsTransactionEntry::with('DestinationReference')->where('destination_id', $request['branch_id'])->first();
                $ref_no = $databranch->DestinationReference->code . '-' . strtoupper(uniqid());
                $get_ref_no = OimsEdtr::where('dispatch_date', Carbon::today())->where('tra_id', $request['rdis'])->first();

                $edtr = OimsEdtr::create([
                    'reference_no' => ($get_ref_no == null ? $ref_no : $get_ref_no['reference_no']),
                    'tra_id' => $request['rdis'],
                    'dispatch_date' => date('Y-m-d'),
                    'branch_id' => $request['branch_id'],
                    'status' => 2,
                    'created_by' => Auth::user()->id,
                ]);



                $get_edtr = OimsTeamRouteAssignmentDetails::where('id', $request['rdis'])->first();


                $pbooking = null;


                $booking = [];

                foreach ($request['selecteds'] as $i => $select) {
                    // $booking = CrmBooking::with('BookingShipper')->where('id', $select)->first();
                    $booking = OimsTransactionEntry::with('CrmBookingReference', 'ConsigneeReference.addresses')->where('id', $select)->first();
                    // dd($booking);
                    // dd('asd');
                    $branchdata = OimsBranchReference::where('id', $request['branch_id'])->first();
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . urlencode($branchdata->address) .
                            "&destinations=" . urlencode($booking['consignee_address']) . "&mode=motorcycle&language=en-EN&sensor=false&key=" . env('GOOGLE_API_KEY'),
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_HTTPHEADER => array(
                            "cache-control: no-cache"
                        ),
                    ));
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    $res = json_decode($response, true);

                    $durationInSeconds = $res['rows'][0]['elements'][0]['duration']['value']; // Replace with your actual duration
                    $formattedTimestamp = date('Y-m-d H:i:s', strtotime("1970-01-01 +$durationInSeconds seconds"));
                    $formattedTimestampFinal = date('H:i:s', strtotime($formattedTimestamp));

                    $edtrdet = OimsEdtrDetails::create([
                        'edtr_id' => $edtr->id,
                        'edtr_item_id' => $booking['id'],
                        'edtr_item_type' => 2,
                        'estimated_time_travel' => $formattedTimestampFinal,
                        'estimated_time_activity' => '00:15:00',
                    ]);

                    $pbooking = OimsTransactionEntry::where('id',  $booking['id'])->update([
                        'is_dispatched' => 2,
                    ]);

                    //firestore oims_transaction_entry
                    $gettransactionentry = OimsTransactionEntry::where('id', $booking['id'])->first();
                    $collectionTransEntryReference = $this->initializeFirestore()->collection('oims_transaction_entry');
                    $documentTransEntryReference = $collectionTransEntryReference->add([
                        'transactionentry_id' => $booking['id'],
                        'waybill' => $booking['waybill'],
                        'transaction_date' => $booking['transaction_date'],
                        'transaction_type' => $booking['transaction_type'],
                        'booking_reference_id' => $booking['booking_reference_id'],
                        'booking_type_id' => $booking['booking_type_id'],
                        'branch_id' => $booking['branch_id'],
                        'origin_id' => $booking['origin_id'],
                        'transhipment_id' => $booking['transhipment_id'],
                        'transport_mode_id' => $booking['transport_mode_id'],
                        'shipper_id' => $booking['shipper_id'],
                        'shipper_contact_person_id' => $booking['shipper_contact_person_id'],
                        'shipper_mobile_no_id' => $booking['shipper_mobile_no_id'],
                        'shipper_address_id' => $booking['shipper_address_id'],
                        'consignee__id' => $booking['consignee__id'],
                        'consignee_contact_person_id' => $booking['consignee_contact_person_id'],
                        'consignee_mobile_no_id' => $booking['consignee_mobile_no_id'],
                        'consignee_address_id' => $booking['consignee_address_id'],
                        'consignee_address' => $booking['consignee_address'],
                        'batch_no' => $booking['batch_no'],
                        'transType' => $booking['transType'],
                        'pickup_notation' => $booking['pickup_notation'],
                        'item_type' => $booking['item_type'],
                        'commodity_app_rate' => $booking['commodity_app_rate'],
                        'commodity_type' => $booking['commodity_type'],
                        'type_of_goods' => $booking['type_of_goods'],
                        'paymode' => $booking['paymode'],
                        'service_mode' => $booking['service_mode'],
                        'charge_to_id' => $booking['charge_to_id'],

                        'tra_id' => $request['rdis'],
                        'dispatch_date' => date('Y-m-d'),
                        'branch_id' => $request['branch_id'],
                        'status' => 2,
                        'created_by' => Auth::user()->id,
                        'checker1_id' => $get_edtr['checker1_id'],
                        'checker2_id' => $get_edtr['checker2_id'],
                        'is_dispatched' => 2,
                    ]);

                    //firestore oims_edtr_details
                    $collectionEdtrDetailsReference = $this->initializeFirestore()->collection('oims_edtr_details');
                    $documentEdtrDetailsReference = $collectionEdtrDetailsReference->add([
                        'edtr_id' => $edtr->id,
                        'edtr_item_id' => $booking['id'],
                        'edtr_item_type' => 2,
                        'estimated_time_travel' => $formattedTimestampFinal,
                        'estimated_time_activity' => '00:15:00',
                    ]);

                    // //firestore add checker and team route id in orders
                    // $collectionOrdersReference = $this->initializeFirestore()->collection('orders');
                    // $queryOrders = $collectionOrdersReference->where('booking_reference_no_2', '=', $booking->booking_reference_no);
                    // $documentsOrders = $queryOrders->documents();

                    // foreach ($documentsOrders as $documentOrder) {
                    //     $documentOrder->reference()->set([
                    //         'checker1_id' => $get_edtr['checker1_id'],
                    //         'checker2_id' => $get_edtr['checker2_id'],
                    //         'team_route_id' => intval($request['rdis']),
                    //         'date_dispatched' => $edtr->dispatch_date,
                    //         'final_status' => 2,
                    //         'booking_id' => $booking['id'],
                    //     ], ['merge' => true]);
                    // }
                }
            }


            DB::commit();
            // return $this->response(200, 'Area has been successfully added!', compact($edtr, $edtrdet));
            return $this->response(
                200,
                '',
                compact($edtr, $edtrdet, $pbooking)
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function actualStartTravel($request, $id)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $actualStrt = OimsEdtrDetails::where('edtr_id', $id)->first();
            // dd($actualStrt);
            $actualStrt->update([
                'actual_start_travel_time' => date('Y-m-d H:i:s', strtotime($request['actual_start_travel_time'])),
            ]);


            DB::commit();

            return $this->response(200, 'Pick up Activity start travel time Has been successfully Updated!', $actualStrt);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function actualEndTravel($request, $id)
    {
        // dd($data['id']);
        DB::beginTransaction();
        try {

            $actualStrt = OimsEdtrDetails::where('edtr_id', $id)->first();
            // dd($actualStrt);
            $actualStrt->update([
                'actual_end_travel_time' => date('Y-m-d H:i:s', strtotime($request['actual_end_travel_time'])),
            ]);


            DB::commit();

            return $this->response(200, 'Pick up Activity end travel time Has been successfully Updated!', $actualStrt);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function pickupConfirmedtime($request, $id)
    {
        // dd($data['id']);
        DB::beginTransaction();
        try {

            $actualStrt = OimsEdtrDetails::where('edtr_id', $id)->first();
            // dd($actualStrt);
            $actualStrt->update([
                'pickup_confirmed_time' => date('Y-m-d H:i:s', strtotime($request['pickup_confirmed_time'])),
            ]);


            DB::commit();

            return $this->response(200, 'Pick up Activity confirmed time Has been successfully Updated!', $actualStrt);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function pickupStartTime($request, $id)
    {
        // dd($data['id']);
        DB::beginTransaction();
        try {

            $actualStrt = OimsEdtrDetails::where('edtr_id', $id)->first();
            // dd($actualStrt);
            $actualStrt->update([
                'pickup_start_time' => date('Y-m-d H:i:s', strtotime($request['pickup_start_time'])),
                'actual_end_travel_time' => date('Y-m-d H:i:s', strtotime($request['actual_end_travel_time'])),
            ]);


            DB::commit();

            return $this->response(200, 'Pick up Activity pickup start time Has been successfully Updated!', $actualStrt);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function pickupEndTime($request, $id)
    {
        // dd($data['id']);
        DB::beginTransaction();
        try {

            $actualStrt = OimsEdtrDetails::where('edtr_id', $id)->first();
            // dd($actualStrt);
            $actualStrt->update([
                'pickup_end_time' => date('Y-m-d H:i:s', strtotime($request['pickup_end_time'])),
            ]);


            DB::commit();

            return $this->response(200, 'Pick up Activity pickup end time Has been successfully Updated!', $actualStrt);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function deliveryStartTime($request, $id)
    {
        // dd($data['id']);
        DB::beginTransaction();
        try {

            $actualStrt = OimsEdtrDetails::where('edtr_id', $id)->first();
            // dd($actualStrt);
            $actualStrt->update([
                'delivery_start_time' => date('Y-m-d H:i:s', strtotime($request['delivery_start_time'])),
                'actual_end_travel_time' => date('Y-m-d H:i:s', strtotime($request['actual_end_travel_time'])),
            ]);


            DB::commit();

            return $this->response(200, 'Pick up Activity pickup start time Has been successfully Updated!', $actualStrt);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function deliveryEndTime($request, $id)
    {
        // dd($data['id']);
        DB::beginTransaction();
        try {

            $actualStrt = OimsEdtrDetails::where('edtr_id', $id)->first();
            // dd($actualStrt);
            $actualStrt->update([
                'delivery_end_time' => date('Y-m-d H:i:s', strtotime($request['delivery_end_time'])),
            ]);


            DB::commit();

            return $this->response(200, 'Pick up Activity pickup end time Has been successfully Updated!', $actualStrt);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
