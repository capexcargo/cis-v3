<?php

namespace App\Repositories\Oims\TeamMgmt;

use App\Interfaces\Oims\TeamMgmt\TeamMgmtInterface;
use App\Models\Crm\BranchReferencesQuadrant;
use App\Models\TeamMgmtReference;
use App\Models\Hrim\TeamReference;
use App\Models\OimsTeamReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TeamMgmtRepository implements TeamMgmtInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            // $quad_lists = [];
            $team_lists = OimsTeamReference::with(['quadrantReference' => function ($query) {
                $query->with('AreaReference', 'TeamReference');
            }])
                // $team_lists = BranchReferencesQuadrant::with('AreaReference', 'TeamReference')
                ->when($request['stats'], function ($query) use ($request) {
                    if ($request['stats'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['stats']);
                    }
                })->groupBy('quadrant_id')->paginate($request['paginate']);

            // $quad_lists = BranchReferencesQuadrant::with('AreaReference')
            //     ->paginate($request['paginate']);
            DB::commit();
            // dd($team_lists);

            return $this->response(200, 'List', compact('team_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
                'quad' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Team Name field is Required.',
                    'quad.required' => 'Quadrant Name field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $teams = OimsTeamReference::create([
                'name' => $validated['name'],
                'quadrant_id' => $validated['quad'],
                'status' => 1,
            ]);

            DB::commit();

            return $this->response(200, 'Team has been successfully added!', compact($teams));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
                'quad' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Team Name field is Required.',
                    'quad.required' => 'Quadrant Name field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $team_s = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $team_s->update([
                'name' => $validated['name'],
                'quadrant_id' => $validated['quad'],
            ]);

            DB::commit();

            return $this->response(200, 'Team has been successfully updated!', compact($team_s));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $team_s = OimsTeamReference::findOrFail($id);
            if (!$team_s) {
                return $this->response(404, 'Team', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Team Management', $team_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
