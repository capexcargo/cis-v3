<?php

namespace App\Repositories\Oims\WaybillRegistry;

use App\Interfaces\Oims\WaybillRegistry\WaybillRegistryInterface;
use App\Models\WaybillRegistryReference;
use App\Models\Hrim\AreaReference;
use App\Models\OimsAreaReference;
use App\Models\OimsWaybillRegistry;
use App\Models\OimsWaybillRegistryDetails;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class WaybillRegistryRepository implements WaybillRegistryInterface
{
    use ResponseTrait;

    public function index($request, $search_request)
    {
        // dd($search_request);
        DB::beginTransaction();
        try {
            $waybill_lists = OimsWaybillRegistry::with('waybillDetails', 'waybillBranch', 'waybillEmpUser', 'waybillCustInfo', 'waybillAgent', 'waybillOmUser', 'waybillFlsUser', 'waybillCbUser')
                ->when($request['stats'], function ($query) use ($request) {
                    if ($request['stats'] == false) {
                        $query->whereNull('issued_to');
                        $query->whereNotNull('issued_to');
                    } elseif ($request['stats'] == 1) {
                        $query->whereNotNull('issued_to');
                    } elseif ($request['stats'] == 2) {
                        $query->whereNull('issued_to');
                    }
                })
                ->when($search_request['from'] ?? false, function ($query) use ($search_request) {
                    $query->where('waybill_start', $search_request['from']);
                })
                ->when($search_request['to'] ?? false, function ($query) use ($search_request) {
                    $query->where('waybill_end', $search_request['to']);
                })
                ->when($search_request['isto'] ?? false, function ($query) use ($search_request) {
                    $query->where('issued_to', $search_request['isto']);
                })
                ->when($search_request['istoname'] ?? false, function ($query) use ($search_request) {
                    if ($search_request['isto'] == 1) {
                        $query->where('issued_to_emp_id', $search_request['istoname']);
                    } elseif ($search_request['isto'] == 2) {
                        $query->where('issued_to_customer_id', $search_request['istoname']);
                    } elseif ($search_request['isto'] == 3) {
                        $query->where('issued_to_agent_id', $search_request['istoname']);
                    } elseif ($search_request['isto'] == 4) {
                        $query->where('issued_to_om', $search_request['istoname']);
                    } elseif ($search_request['isto'] == 5) {
                        $query->where('issued_to_fls', $search_request['istoname']);
                    }
                })
                ->when($search_request['idate'] ?? false, function ($query) use ($search_request) {
                    $query->where('issuance_date', $search_request['idate']);
                })
                ->when($search_request['status'] ?? false, function ($query) use ($search_request) {
                    $query->where('status', $search_request['status']);
                })

                ->paginate($request['paginate']);
            DB::commit();
            // dd($waybill_lists);

            return $this->response(200, 'List', compact('waybill_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function view($request)
    {
        DB::beginTransaction();
        try {
            $waybill_views = OimsWaybillRegistryDetails::where('waybill_registry_id', $request['wid'])
                ->when($request['viewstats'], function ($query) use ($request) {
                    if ($request['viewstats'] == false) {
                        $query->whereIn('status', [1, 2, 3, 4, 5, 6]);
                    } elseif ($request['viewstats'] == 1) {
                        $query->whereIn('status', [1, 2, 3, 4]);
                    } elseif ($request['viewstats'] == 2) {
                        $query->where('status', 5);
                    } elseif ($request['viewstats'] == 3) {
                        $query->where('status', 6);
                    }
                })
                ->when($request['wb'] ?? false, function ($query) use ($request) {
                    $query->where('waybill', $request['wb']);
                })
                ->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'List', compact('waybill_views'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            foreach ($request['awbs'] as $i => $awb) {
                $rules['awbs.' . $i . '.' . $i . '.id'] = 'sometimes';
                $rules['awbs.' . $i . '.' . $i . '.from'] = 'required';
                $rules['awbs.' . $i . '.' . $i . '.to'] = 'required';
            }
            if (isset($request['subawbs'][$i])) {
                foreach ($request['subawbs'][$i] as $c => $subawb) {

                    $rules['subawbs.' . $i . '.' . $c . '.id'] = 'sometimes';
                    $rules['subawbs.' . $i . '.' . $c . '.name_1'] = 'required';
                    $rules['subawbs.' . $i . '.' . $c . '.name_2'] = 'required';
                    $rules['subawbs.' . $i . '.' . $c . '.branch'] = 'required';
                }
                $validator = Validator::make(
                    $request,
                    $rules,
                    [
                        'awbs.*.*.from.required' => 'From field is required.',
                        'awbs.*.*.to.required' => 'To field is required.',
                        'subawbs.*.*.name_1.required' => 'Name 1 field is required.',
                        'subawbs.*.*.name_2.required' => 'Name 2 field is required.',
                        'subawbs.*.*.branch.required' => 'Branch field is required.',

                    ],
                );
            } else {
                $validator = Validator::make(
                    $request,
                    $rules,
                    [
                        'awbs.*.*.from.required' => 'From field is required.',
                        'awbs.*.*.to.required' => 'To field is required.',
                        'subawbs.*.*.name_1.required' => 'Name 1 field is required.',
                        'subawbs.*.*.name_2.required' => 'Name 2 field is required.',
                        'subawbs.*.*.branch.required' => 'Branch field is required.',
                    ],
                );
            }


            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            foreach ($request['awbs'] as $a => $awb) {
                $b = 0;

                if (isset($request['subawbs'][$a])) {
                    foreach ($request['subawbs'][$a] as $c => $subpro) {
                        if ($request['subawbs'][$a][$c]['name_1'] == 1) {
                            $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$request['awbs'][$a][$a]['from'], $request['awbs'][$a][$a]['to']])->first();

                            $updateskipchecker = OimsWaybillRegistryDetails::whereBetween('waybill', [$request['awbs'][$a][$a]['from'], $request['awbs'][$a][$a]['to']])
                                ->whereIn('status', [1, 2])
                                ->update([
                                    'skipped_status' => 1,
                                    'skipped_status2' => 1,
                                ]);

                            $awbs1 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                                'issued_to' => $request['subawbs'][$a][$c]['name_1'],
                                'issued_to_emp_id' => $request['subawbs'][$a][$c]['name_2'],
                            ]);

                            $subawbs1 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs1)->whereBetween('waybill', [$request['awbs'][$a][$a]['from'], $request['awbs'][$a][$a]['to']])->update([
                                'issue_to_checker_identify' => 1,
                                'status' => 4,
                                'date_checker_issued' => date("Y/m/d"),
                            ]);
                        } elseif ($request['subawbs'][$a][$c]['name_1'] == 2) {

                            $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$request['awbs'][$a][$a]['from'], $request['awbs'][$a][$a]['to']])->first();

                            $awbs2 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                                'issued_to' => $request['subawbs'][$a][$c]['name_1'],
                                'issued_to_customer_id' => $request['subawbs'][$a][$c]['name_2'],
                                'issuance_date' => date("Y/m/d"),
                                'closed_date' => date("Y/m/d"),
                                'issuance_status' => 1,
                            ]);

                            $subawbs2 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs2)->whereBetween('waybill', [$request['awbs'][$a][$a]['from'], $request['awbs'][$a][$a]['to']])->update([
                                'date_customer_issued' => date("Y/m/d"),
                                'status' => 7,
                            ]);
                        } elseif ($request['subawbs'][$a][$c]['name_1'] == 3) {

                            $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$request['awbs'][$a][$a]['from'], $request['awbs'][$a][$a]['to']])->first();

                            $awbs3 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                                'issued_to' => $request['subawbs'][$a][$c]['name_1'],
                                'issued_to_agent_id' => $request['subawbs'][$a][$c]['name_2'],
                                'issuance_date' => date("Y/m/d"),
                                'closed_date' => date("Y/m/d"),
                                'issuance_status' => 1,
                            ]);

                            $subawbs3 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs3)->whereBetween('waybill', [$request['awbs'][$a][$a]['from'], $request['awbs'][$a][$a]['to']])->update([
                                'date_agent_issued' => date("Y/m/d"),
                                'status' => 8,
                            ]);
                        } elseif ($request['subawbs'][$a][$c]['name_1'] == 4) {
                            $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$request['awbs'][$a][$a]['from'], $request['awbs'][$a][$a]['to']])->first();
                            // dd($request['subawbs'][$a]);
                            $updateskipom = OimsWaybillRegistryDetails::whereBetween('waybill', [$request['awbs'][$a][$a]['from'], $request['awbs'][$a][$a]['to']])
                                ->where('status', 1)
                                ->update([
                                    'skipped_status' => 1,
                                ]);

                            // dd($updateskipom);

                            $awbs4 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                                'issued_to' => $request['subawbs'][$a][$c]['name_1'],
                                'issued_to_om' => $request['subawbs'][$a][$c]['name_2'],
                            ]);

                            $subawbs4 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs4)->whereBetween('waybill', [$request['awbs'][$a][$a]['from'], $request['awbs'][$a][$a]['to']])->update([
                                'issue_to_om_identify' => 1,
                                'status' => 3,
                                'date_om_issued' => date("Y/m/d"),
                            ]);
                        } elseif ($request['subawbs'][$a][$c]['name_1'] == 5) {
                            $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$request['awbs'][$a][$a]['from'], $request['awbs'][$a][$a]['to']])->first();


                            $awbs5 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                                'issued_to' => $request['subawbs'][$a][$c]['name_1'],
                                'issued_to_fls' => $request['subawbs'][$a][$c]['name_2'],
                                'issuance_status' => 1,
                                'issuance_date' => date("Y/m/d"),
                            ]);

                            $subawbs5 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs5)->whereBetween('waybill', [$request['awbs'][$a][$a]['from'], $request['awbs'][$a][$a]['to']])->update([
                                'issue_to_fls_identify' => 1,
                                'date_fls_issued' => date("Y/m/d"),
                                'status' => 2,
                            ]);
                        }

                        $b++;
                    }
                }
            }

            DB::commit();
            if ($request['subawbs'][$a][$c]['name_1'] == 1) {
                return $this->response(200, 'Waybill has been successfully added!', compact($updateskipchecker, $awbs1, $subawbs1));
            } elseif ($request['subawbs'][$a][$c]['name_1'] == 2) {
                return $this->response(200, 'Waybill has been successfully added!', compact($awbs2, $subawbs2));
            } elseif ($request['subawbs'][$a][$c]['name_1'] == 3) {
                return $this->response(200, 'Waybill has been successfully added!', compact($awbs3, $subawbs3));
            } elseif ($request['subawbs'][$a][$c]['name_1'] == 4) {
                return $this->response(200, 'Waybill has been successfully added!', compact($updateskipom, $awbs4, $subawbs4));
            } elseif ($request['subawbs'][$a][$c]['name_1'] == 5) {
                return $this->response(200, 'Waybill has been successfully added!', compact($awbs5, $subawbs5));
            }
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function issueToOMSkip()
    {
        DB::beginTransaction();
        try {

            $min = DB::select("SELECT MIN(id) AS id from oims_waybill_registry_details where status = 2 and issue_to_fls_identify = 1");
            $compact  = compact('min');
            // dd($ws);

            // dd();
            // dd($compact['min']);

            $ximum = DB::select("SELECT MAX(id) AS id from oims_waybill_registry_details where status = 3");
            $compactM  = compact('ximum');

            if ($compact['min'][0]->id != null) {

                for ($x = $compact['min'][0]->id; $x <= $compactM['ximum'][0]->id; $x++) {

                    $asd = OimsWaybillRegistryDetails::where('id', $x)->where('status', 3)->first();
                    // dd($asd);
                    if ($asd == null) {
                        $subawbsOM = OimsWaybillRegistryDetails::where('id', $x)->update(
                            [
                                'skipped_status' => 1,
                            ]
                        );
                    }
                    // else {
                    //     $subawbsOM = OimsWaybillRegistryDetails::where('id', $x)->update([
                    //         'skipped_status' => NULL,
                    //     ]);
                    // }
                }
            }

            DB::commit();
            return $this->response(200, 'Waybill has been successfully added!', compact($subawbsOM));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function issueToCheckerSkip()
    {
        DB::beginTransaction();
        try {

            $checkermin = DB::select("SELECT MIN(id) AS id from oims_waybill_registry_details where status = 3 and issue_to_checker_identify = 1");
            $checkercompact  = compact('checkermin');

            // dd($checkercompact['checkermin'][0]);

            $checkerximum = DB::select("SELECT MAX(id) AS id from oims_waybill_registry_details where status = 4");
            $checkercompactM  = compact('checkerximum');

            if ($checkercompact['checkermin'][0]->id != null) {
                for ($z = $checkercompact['checkermin'][0]->id; $z <= $checkercompactM['checkerximum'][0]->id; $z++) {

                    $checkerasd = OimsWaybillRegistryDetails::where('id', $z)->where('status', 4)->first();

                    if ($checkerasd == null) {
                        $subawbsCk = OimsWaybillRegistryDetails::where('id', $z)->update(
                            [
                                'skipped_status2' => 1,
                            ]
                        );
                    }
                    // else {
                    //     $subawbsCk = OimsWaybillRegistryDetails::where('id', $z)->update([
                    //         'skipped_status2' => NULL,
                    //     ]);
                    // }
                }
            }


            DB::commit();
            return $this->response(200, 'Waybill has been successfully added!', compact($subawbsCk));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'from' => 'required',
                'to' => 'required',
                'name_1' => 'required',
                'name_2' => 'required',
                'branch' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'from.required' => 'From field is Required.',
                    'to.required' => 'To field is Required.',
                    'name_1.required' => 'Name 1 field is Required.',
                    'name_2.required' => 'Name 2 field is Required.',
                    'branch.required' => 'Branch field is Required.',
                ]
            );

            // dd($validator);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $waybill_s = $response['result'];

            $response = $this->updateValidation($request, $id);
            // dd($this->updateValidation($request, $id));
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            // dd($request->name);
            // dd('a');

            // foreach ($request['awbs'] as $a => $awb) {
            //     $b = 0;

            // if (isset($request['subawbs'][$a])) {
            //     foreach ($request['subawbs'][$a] as $c => $subpro) {
            if ($validated['name_1'] == 1) {
                $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$validated['from'], $validated['to']])->first();

                $updateskipchecker = OimsWaybillRegistryDetails::whereBetween('waybill', [$validated['from'], $validated['to']])
                    ->whereIn('status', [1, 2])
                    ->update([
                        'skipped_status' => 1,
                        'skipped_status2' => 1,
                    ]);

                $awbs1 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                    'issued_to' => $validated['name_1'],
                    'issued_to_emp_id' => $validated['name_2'],
                ]);

                $subawbs1 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs1)->whereBetween('waybill', [$validated['from'], $validated['to']])->update([
                    'issue_to_checker_identify' => 1,
                    'status' => 4,
                    'date_checker_issued' => date("Y/m/d"),
                ]);
            } elseif ($validated['name_1'] == 2) {

                $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$validated['from'], $validated['to']])->first();

                $awbs2 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                    'issued_to' => $validated['name_1'],
                    'issued_to_customer_id' => $validated['name_2'],
                    'issuance_date' => date("Y/m/d"),
                    'closed_date' => date("Y/m/d"),
                    'issuance_status' => 1,
                ]);

                $subawbs2 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs2)->whereBetween('waybill', [$validated['from'], $validated['to']])->update([
                    'date_customer_issued' => date("Y/m/d"),
                    'status' => 7,
                ]);
            } elseif ($validated['name_1'] == 3) {

                $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$validated['from'], $validated['to']])->first();

                $awbs3 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                    'issued_to' => $validated['name_1'],
                    'issued_to_agent_id' => $validated['name_2'],
                    'issuance_date' => date("Y/m/d"),
                    'closed_date' => date("Y/m/d"),
                    'issuance_status' => 1,
                ]);

                $subawbs3 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs3)->whereBetween('waybill', [$validated['from'], $validated['to']])->update([
                    'date_agent_issued' => date("Y/m/d"),
                    'status' => 8,
                ]);
            } elseif ($validated['name_1'] == 4) {
                $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$validated['from'], $validated['to']])->first();
                // dd($request['subawbs'][$a]);
                $updateskipom = OimsWaybillRegistryDetails::whereBetween('waybill', [$validated['from'], $validated['to']])
                    ->where('status', 1)
                    ->update([
                        'skipped_status' => 1,
                    ]);

                // dd($updateskipom);

                $awbs4 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                    'issued_to' => $validated['name_1'],
                    'issued_to_om' => $validated['name_2'],
                ]);

                $subawbs4 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs4)->whereBetween('waybill', [$validated['from'], $validated['to']])->update([
                    'issue_to_om_identify' => 1,
                    'status' => 3,
                    'date_om_issued' => date("Y/m/d"),
                ]);
            } elseif ($validated['name_1'] == 5) {
                $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$validated['from'], $validated['to']])->first();


                $awbs5 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                    'issued_to' => $validated['name_1'],
                    'issued_to_fls' => $validated['name_2'],
                    'issuance_status' => 1,
                    'issuance_date' => date("Y/m/d"),
                ]);

                $subawbs5 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs5)->whereBetween('waybill', [$validated['from'], $validated['to']])->update([
                    'issue_to_fls_identify' => 1,
                    'date_fls_issued' => date("Y/m/d"),
                    'status' => 2,
                ]);
            }

            //         $b++;
            //     }
            // }
            // }

            DB::commit();
            if ($validated['name_1'] == 1) {
                return $this->response(200, 'Waybill has been successfully added!', compact($updateskipchecker, $awbs1, $subawbs1));
            } elseif ($validated['name_1'] == 2) {
                return $this->response(200, 'Waybill has been successfully added!', compact($awbs2, $subawbs2));
            } elseif ($validated['name_1'] == 3) {
                return $this->response(200, 'Waybill has been successfully added!', compact($awbs3, $subawbs3));
            } elseif ($validated['name_1'] == 4) {
                return $this->response(200, 'Waybill has been successfully added!', compact($updateskipom, $awbs4, $subawbs4));
            } elseif ($validated['name_1'] == 5) {
                return $this->response(200, 'Waybill has been successfully added!', compact($awbs5, $subawbs5));
            }
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function issueToOMSkipEdit()
    {
        DB::beginTransaction();
        try {

            $min = DB::select("SELECT MIN(id) AS id from oims_waybill_registry_details where status = 2 and issue_to_fls_identify = 1");
            $compact  = compact('min');
            // dd($ws);

            // dd();
            // dd($compact['min']);

            $ximum = DB::select("SELECT MAX(id) AS id from oims_waybill_registry_details where status = 3");
            $compactM  = compact('ximum');

            if ($compact['min'][0]->id != null) {

                for ($x = $compact['min'][0]->id; $x <= $compactM['ximum'][0]->id; $x++) {

                    $asd = OimsWaybillRegistryDetails::where('id', $x)->where('status', 3)->first();
                    // dd($asd);
                    if ($asd == null) {
                        $subawbsOM = OimsWaybillRegistryDetails::where('id', $x)->update(
                            [
                                'skipped_status' => 1,
                            ]
                        );
                    }
                    // else {
                    //     $subawbsOM = OimsWaybillRegistryDetails::where('id', $x)->update([
                    //         'skipped_status' => NULL,
                    //     ]);
                    // }
                }
            }

            DB::commit();
            return $this->response(200, 'Waybill has been successfully added!', compact($subawbsOM));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function issueToCheckerSkipEdit()
    {
        DB::beginTransaction();
        try {

            $checkermin = DB::select("SELECT MIN(id) AS id from oims_waybill_registry_details where status = 3 and issue_to_checker_identify = 1");
            $checkercompact  = compact('checkermin');

            // dd($checkercompact['checkermin'][0]);

            $checkerximum = DB::select("SELECT MAX(id) AS id from oims_waybill_registry_details where status = 4");
            $checkercompactM  = compact('checkerximum');

            if ($checkercompact['checkermin'][0]->id != null) {
                for ($z = $checkercompact['checkermin'][0]->id; $z <= $checkercompactM['checkerximum'][0]->id; $z++) {

                    $checkerasd = OimsWaybillRegistryDetails::where('id', $z)->where('status', 4)->first();

                    if ($checkerasd == null) {
                        $subawbsCk = OimsWaybillRegistryDetails::where('id', $z)->update(
                            [
                                'skipped_status2' => 1,
                            ]
                        );
                    }
                    // else {
                    //     $subawbsCk = OimsWaybillRegistryDetails::where('id', $z)->update([
                    //         'skipped_status2' => NULL,
                    //     ]);
                    // }
                }
            }


            DB::commit();
            return $this->response(200, 'Waybill has been successfully added!', compact($subawbsCk));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $waybill_s = OimsWaybillRegistry::with('waybillDetails')->findOrFail($id);
            if (!$waybill_s) {
                return $this->response(404, 'Waybill', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Waybill Management', $waybill_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function issueValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'from' => 'required',
                'to' => 'required',
                'name_1' => 'required',
                'name_2' => 'required',
                'branch' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'from.required' => 'From field is Required.',
                    'to.required' => 'To field is Required.',
                    'name_1.required' => 'Name 1 field is Required.',
                    'name_2.required' => 'Name 2 field is Required.',
                    'branch.required' => 'Branch field is Required.',
                ]
            );

            // dd($validator);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function issue($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $waybill_s = $response['result'];

            $response = $this->issueValidation($request, $id);
            // dd($this->updateValidation($request, $id));
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            // dd($request->name);
            // dd('a');

            // foreach ($request['awbs'] as $a => $awb) {
            //     $b = 0;

            // if (isset($request['subawbs'][$a])) {
            //     foreach ($request['subawbs'][$a] as $c => $subpro) {
            if ($validated['name_1'] == 1) {
                $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$validated['from'], $validated['to']])->first();

                $updateskipchecker = OimsWaybillRegistryDetails::whereBetween('waybill', [$validated['from'], $validated['to']])
                    ->whereIn('status', [1, 2])
                    ->update([
                        'skipped_status' => 1,
                        'skipped_status2' => 1,
                    ]);

                $awbs1 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                    'issued_to' => $validated['name_1'],
                    'issued_to_emp_id' => $validated['name_2'],
                ]);

                $subawbs1 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs1)->whereBetween('waybill', [$validated['from'], $validated['to']])->update([
                    'issue_to_checker_identify' => 1,
                    'status' => 4,
                    'date_checker_issued' => date("Y/m/d"),
                ]);
            } elseif ($validated['name_1'] == 2) {

                $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$validated['from'], $validated['to']])->first();

                $awbs2 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                    'issued_to' => $validated['name_1'],
                    'issued_to_customer_id' => $validated['name_2'],
                    'issuance_date' => date("Y/m/d"),
                    'closed_date' => date("Y/m/d"),
                    'issuance_status' => 1,
                ]);

                $subawbs2 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs2)->whereBetween('waybill', [$validated['from'], $validated['to']])->update([
                    'date_customer_issued' => date("Y/m/d"),
                    'status' => 7,
                ]);
            } elseif ($validated['name_1'] == 3) {

                $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$validated['from'], $validated['to']])->first();

                $awbs3 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                    'issued_to' => $validated['name_1'],
                    'issued_to_agent_id' => $validated['name_2'],
                    'issuance_date' => date("Y/m/d"),
                    'closed_date' => date("Y/m/d"),
                    'issuance_status' => 1,
                ]);

                $subawbs3 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs3)->whereBetween('waybill', [$validated['from'], $validated['to']])->update([
                    'date_agent_issued' => date("Y/m/d"),
                    'status' => 8,
                ]);
            } elseif ($validated['name_1'] == 4) {
                $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$validated['from'], $validated['to']])->first();
                // dd($request['subawbs'][$a]);
                $updateskipom = OimsWaybillRegistryDetails::whereBetween('waybill', [$validated['from'], $validated['to']])
                    ->where('status', 1)
                    ->update([
                        'skipped_status' => 1,
                    ]);

                // dd($updateskipom);

                $awbs4 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                    'issued_to' => $validated['name_1'],
                    'issued_to_om' => $validated['name_2'],
                ]);

                $subawbs4 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs4)->whereBetween('waybill', [$validated['from'], $validated['to']])->update([
                    'issue_to_om_identify' => 1,
                    'status' => 3,
                    'date_om_issued' => date("Y/m/d"),
                ]);
            } elseif ($validated['name_1'] == 5) {
                $getid = OimsWaybillRegistryDetails::whereBetween('waybill', [$validated['from'], $validated['to']])->first();


                $awbs5 = OimsWaybillRegistry::where('id', $getid->waybill_registry_id)->update([
                    'issued_to' => $validated['name_1'],
                    'issued_to_fls' => $validated['name_2'],
                    'issuance_status' => 1,
                    'issuance_date' => date("Y/m/d"),
                ]);

                $subawbs5 = OimsWaybillRegistryDetails::where('waybill_registry_id', $awbs5)->whereBetween('waybill', [$validated['from'], $validated['to']])->update([
                    'issue_to_fls_identify' => 1,
                    'date_fls_issued' => date("Y/m/d"),
                    'status' => 2,
                ]);
            }

            //         $b++;
            //     }
            // }
            // }

            DB::commit();
            if ($validated['name_1'] == 1) {
                return $this->response(200, 'Waybill has been successfully added!', compact($updateskipchecker, $awbs1, $subawbs1));
            } elseif ($validated['name_1'] == 2) {
                return $this->response(200, 'Waybill has been successfully added!', compact($awbs2, $subawbs2));
            } elseif ($validated['name_1'] == 3) {
                return $this->response(200, 'Waybill has been successfully added!', compact($awbs3, $subawbs3));
            } elseif ($validated['name_1'] == 4) {
                return $this->response(200, 'Waybill has been successfully added!', compact($updateskipom, $awbs4, $subawbs4));
            } elseif ($validated['name_1'] == 5) {
                return $this->response(200, 'Waybill has been successfully added!', compact($awbs5, $subawbs5));
            }
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function issueToOMSkipIssue()
    {
        DB::beginTransaction();
        try {

            $min = DB::select("SELECT MIN(id) AS id from oims_waybill_registry_details where status = 2 and issue_to_fls_identify = 1");
            $compact  = compact('min');
            // dd($ws);

            // dd();
            // dd($compact['min']);

            $ximum = DB::select("SELECT MAX(id) AS id from oims_waybill_registry_details where status = 3");
            $compactM  = compact('ximum');

            if ($compact['min'][0]->id != null) {

                for ($x = $compact['min'][0]->id; $x <= $compactM['ximum'][0]->id; $x++) {

                    $asd = OimsWaybillRegistryDetails::where('id', $x)->where('status', 3)->first();
                    // dd($asd);
                    if ($asd == null) {
                        $subawbsOM = OimsWaybillRegistryDetails::where('id', $x)->update(
                            [
                                'skipped_status' => 1,
                            ]
                        );
                    }
                    // else {
                    //     $subawbsOM = OimsWaybillRegistryDetails::where('id', $x)->update([
                    //         'skipped_status' => NULL,
                    //     ]);
                    // }
                }
            }

            DB::commit();
            return $this->response(200, 'Waybill has been successfully added!', compact($subawbsOM));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function issueToCheckerSkipIssue()
    {
        DB::beginTransaction();
        try {

            $checkermin = DB::select("SELECT MIN(id) AS id from oims_waybill_registry_details where status = 3 and issue_to_checker_identify = 1");
            $checkercompact  = compact('checkermin');

            // dd($checkercompact['checkermin'][0]);

            $checkerximum = DB::select("SELECT MAX(id) AS id from oims_waybill_registry_details where status = 4");
            $checkercompactM  = compact('checkerximum');

            if ($checkercompact['checkermin'][0]->id != null) {
                for ($z = $checkercompact['checkermin'][0]->id; $z <= $checkercompactM['checkerximum'][0]->id; $z++) {

                    $checkerasd = OimsWaybillRegistryDetails::where('id', $z)->where('status', 4)->first();

                    if ($checkerasd == null) {
                        $subawbsCk = OimsWaybillRegistryDetails::where('id', $z)->update(
                            [
                                'skipped_status2' => 1,
                            ]
                        );
                    }
                    // else {
                    //     $subawbsCk = OimsWaybillRegistryDetails::where('id', $z)->update([
                    //         'skipped_status2' => NULL,
                    //     ]);
                    // }
                }
            }


            DB::commit();
            return $this->response(200, 'Waybill has been successfully added!', compact($subawbsCk));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
