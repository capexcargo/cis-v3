<?php

namespace App\Repositories\Oims\TEDropdownMgmt\ProvinceMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ProvinceMgmt\ProvinceMgmtInterface;
use App\Models\Crm\BarangayReference;
use App\Models\Crm\CityReference;
use App\Models\Crm\StateReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\InitializeFirestoreTrait;


class ProvinceMgmtRepository implements ProvinceMgmtInterface
{
    use ResponseTrait;
    use InitializeFirestoreTrait;

    public function index($request, $search_request)
    {
        // dd($search_request);
        DB::beginTransaction();
        try {
            $province_lists = StateReference::with('cityProvince', 'barangayProvince',)
                // ->with(['cityProvince' => function ($query) use ($search_request) {
                //     $query->with('BarangayCity')
                //         ->when($search_request['brgy'] ?? false, function ($query) use ($search_request) {
                //             $query->where('brgy', $search_request['brgy']);
                //         });
                //     // $query->with('ZipcodeReference')->whereHas($search_request['zip'] ?? false, function ($query) use ($search_request) {
                //     //     $query->where('name', $search_request['zip']);
                //     // });
                //     // $query->whereHas('ZipcodeReference', function ($query) use ($search_request) {
                //     //     $query->when($search_request['zip'] ?? false, function ($query) use ($search_request) {
                //     //         $query->where('id', $search_request['zip']);
                //     //     });
                //     // });
                //     // $query->whereHas('BarangayCity', function ($query) use ($search_request) {
                //     //     $query->when($search_request['brgy'] ?? false, function ($query) use ($search_request) {
                //     //         $query->where('name', $search_request['brgy']);
                //     //     });
                //     // });
                // }])
                ->when($search_request['name'] ?? false, function ($query) use ($search_request) {
                    $query->where('name', $search_request['name']);
                })
                ->whereHas('cityProvince', function ($query) use ($search_request) {
                    $query->when($search_request['mun'] ?? false, function ($query) use ($search_request) {
                        $query->where('name', $search_request['mun']);
                    });
                })
                ->whereHas('barangayProvince', function ($query) use ($search_request) {
                    $query->when($search_request['brgy'] ?? false, function ($query) use ($search_request) {
                        $query->where('name', $search_request['brgy']);
                    });
                })
                // ->whereHas('cityProvince', function ($query) use ($search_request) {
                //     $query->when($search_request['brgy'] ?? false, function ($query) use ($search_request) {
                //         $query->where('name', $search_request['brgy']);
                //     });
                // })



                // ->when($search_request['brgy'] ?? false, function ($query) use ($search_request) {
                //     $query->where('name', $search_request['brgy']);
                // })
                // ->when($search_request['zip'] ?? false, function ($query) use ($search_request) {
                //     $query->where('zipcode_id', $search_request['zip']);
                // })
                ->when($request['stats'], function ($query) use ($request) {
                    if ($request['stats'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['stats']);
                    }
                })
                ->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'List', compact('province_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required|unique:state,name',
                'reg' => 'required',
                'isl' => 'required',
            ];
            foreach ($request['provs'] as $i => $tellno) {
                $rules['provs.' . $i . '.id'] = 'sometimes';
                $rules['provs.' . $i . '.mun'] = 'required';
            }
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Province field is Required.',
                    'name.unique' => 'Province is already taken.',
                    'reg.required' => 'Region field is Required.',
                    'provs.*.mun.required' => 'Municipal field is required.',

                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            // dd($validated);

            $provincial = StateReference::create([
                'name' => $validated['name'],
                'region_id' => $validated['reg'],
                'island_group_id' => $validated['isl'],
                'psgcCode' => null,
                'regCode' => null,
                'provCode' => null,
                'status' => 1,
            ]);
            // dd($provincial);

            $provId = $provincial->id;

            $collectionStateReference = $this->initializeFirestore()->collection('state'); 
            $documentStateReference = $collectionStateReference->add();
            $documentStateReference->set([
                'id' => $provincial->id,
                'name' => $validated['name'],
                'region_id' => intval($validated['reg']),
                'island_group_id' => $validated['isl'],
                'psgcCode' => null,
                'regCode' => null,
                'provCode' => null,
                'status' => 1,
            ]);

            $collectionCityReference = $this->initializeFirestore()->collection('city');
            $collectionBarangayReference = $this->initializeFirestore()->collection('barangay');


            foreach ($validated['provs'] as $i => $prov) {
                // dd($validated['provs']);
                $pmuni = CityReference::where('id', $validated['provs'][$i]['mun'])->update([
                    'state_id' => $provincial->id,
                    'region_id' => $validated['reg'],
                    'island_group_id' => $validated['isl'],
                ]);

                $cityQuery = $collectionCityReference->where('id','=', intval( $validated['provs'][$i]['mun']));
                $cityDocuments = $cityQuery->documents();

                foreach ($cityDocuments as $cityDocument) {
                $brgyDocumentId = $cityDocument->id();

                $collectionCityReference->document($brgyDocumentId)->update([
                        ['path' => 'state_id', 'value' => $provId],
                        ['path' => 'region_id', 'value' => intval($validated['reg'])],
                        ['path' => 'island_group_id', 'value' => $validated['isl']]
                    ]);
                }

                $pbrgy = BarangayReference::where('city_id', $validated['provs'][$i]['mun'])->update([
                    'state_id' => $provincial->id,
                    'region_id' => $validated['reg'],
                    'island_group_id' => $validated['isl'],
                ]);

                $brgyQuery = $collectionBarangayReference->where('city_id','=', intval( $validated['provs'][$i]['mun']));
                $brgyDocuments = $brgyQuery->documents();

                foreach ($brgyDocuments as $brgyDocument) {
                $brgyDocumentId = $brgyDocument->id();

                $collectionBarangayReference->document($brgyDocumentId)->update([
                        ['path' => 'state_id', 'value' => $provId],
                        ['path' => 'region_id', 'value' => intval($validated['reg'])],
                        ['path' => 'island_group_id', 'value' => $validated['isl']]
                    ]);
                }
            }
            // dd($pmuni);

            DB::commit();

            return $this->response(200, 'Province has been successfully added!', compact($provincial, $pmuni, $pbrgy));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required|unique:state,name,' . $id,
                'reg' => 'required',
                'isl' => 'required',
            ];
            foreach ($request['provs'] as $i => $asd) {
                $rules['provs.' . $i . '.id'] = 'sometimes';
                $rules['provs.' . $i . '.mun'] = 'required';
                $rules['provs.' . $i . '.is_deleted'] = 'sometimes';
            }
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Province field is Required.',
                    'name.unique' => 'Province is already taken.',
                    'reg.required' => 'Region field is Required.',
                    'provs.*.mun.required' => 'Municipal field is required.',

                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $provincec = StateReference::with('cityProvince')->findOrFail($id);
            if (!$provincec) {
                return $this->response(404, 'Province', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Province Management', $provincec);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $provincec = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $provincec->update([
                'name' => $validated['name'],
                'region_id' => $validated['reg'],
                'island_group_id' => $validated['isl'],
                'psgcCode' => null,
                'regCode' => null,
                'provCode' => null,
            ]);

            $collectionReference = $this->initializeFirestore()->collection('state'); 
            $query = $collectionReference->where('id', '=', intval($id));
            $documents = $query->documents();

            foreach ($documents as $document) {
                if ($document->exists()) {
                    $documentId = $document->id();
                    $collectionReference->document($documentId)->update([
                        ['path' => 'name', 'value' => $validated['name']],
                        ['path' => 'region_id', 'value' => intval($validated['reg'])],
                        ['path' => 'island_group_id', 'value' => intval($validated['isl'])],
                        ['path' => 'psgcCode', 'value' => null],
                        ['path' => 'regCode', 'value' => null],
                        ['path' => 'provCode', 'value' => null],
                    ]);
                }
            }
            // dd($provincec->id);

            // foreach ($request['provs'] as $i => $brgyzip) {
            //     if ($request['provs'][$i]['id'] && $request['provs'][$i]['is_deleted']) {
            //         $ancillarys = $provincec->BarangayCity()->find($brgyzip['id']); //query all in database

            //         if ($ancillarys) {
            //             $ancillarys->delete();
            //         }
            //     } else {
            //         $provincec->BarangayCity()->updateOrCreate(
            //             [
            //                 'id' => $brgyzip['id'],
            //             ],
            //             [
            //                 'city_id' => $provincec->id,
            //             ]
            //         );
            //     }
            // }

            foreach ($validated['provs'] as $a => $prov) {
                //city
                $collectionReferenceCity = $this->initializeFirestore()->collection('city'); 
                if ($validated['provs'][$a]['id'] && $validated['provs'][$a]['is_deleted']) {
                    $munic = CityReference::find($validated['provs'][$a]['id']); //query all in database
                    if ($munic) {
                        $munic->update(
                            [
                                'state_id' => null,
                                'region_id' => null,
                                'island_group_id' => null,
                            ]
                        );

                    $queryCityDel = $collectionReferenceCity->where('id', '=', intval($validated['provs'][$a]['id']));
                    $documentsCityDel = $queryCityDel->documents();
                
                    foreach ($documentsCityDel as $documentCityDel) {
                        if ($documentCityDel->exists()) {
                            $documentIdCityDel = $documentCityDel->id();
                            $collectionReferenceCity->document($documentIdCityDel)->update([
                                ['path' => 'state_id', 'value' => null],
                                ['path' => 'region_id', 'value' => null],
                                ['path' => 'island_group_id', 'value' => null],
                            ]);
                        }
                    }
                    }
                } else {
                    $munic = CityReference::where('id', $validated['provs'][$a]['mun'])->update(
                        [
                            'id' => $validated['provs'][$a]['mun'],
                            'state_id' => $provincec->id,
                            'region_id' => $validated['reg'],
                            'island_group_id' => $validated['isl'],
                        ]
                    );

                    $queryCity = $collectionReferenceCity->where('id', '=', intval($validated['provs'][$a]['mun']));
                    $documentsCity = $queryCity->documents();
                
                    foreach ($documentsCity as $documentCity) {
                        if ($documentCity->exists()) {
                            $documentIdCity = $documentCity->id();
                            $collectionReferenceCity->document($documentIdCity)->update([
                                ['path' => 'id', 'value' => $validated['provs'][$a]['mun']],
                                ['path' => 'state_id', 'value' => $provincec->id],
                                ['path' => 'region_id', 'value' => intval($validated['reg'])],
                                ['path' => 'island_group_id', 'value' => intval($validated['isl'])],
                            ]);
                        }
                    }
                }
                //Barangay
                $collectionReferenceBrgy = $this->initializeFirestore()->collection('barangay'); 
                if ($validated['provs'][$a]['id'] && $validated['provs'][$a]['is_deleted']) {
                    $barc = BarangayReference::find($validated['provs'][$a]['id']); //query all in database
                    if ($barc) {
                        $barc->update(
                            [
                                'state_id' => null,
                                'region_id' => null,
                                'island_group_id' => null,
                            ]
                        );

                        $queryBrgyDel = $collectionReferenceBrgy->where('id', '=', intval($validated['provs'][$a]['id']));
                        $documentsBrgyDel = $queryBrgyDel->documents();
                    
                        foreach ($documentsBrgyDel as $documentBrgyDel) {
                            if ($documentBrgyDel->exists()) {
                                $documentIdBrgyDel = $documentBrgyDel->id();
                                $collectionReferenceBrgy->document($documentIdBrgyDel)->update([
                                    ['path' => 'state_id', 'value' => null],
                                    ['path' => 'region_id', 'value' => null],
                                    ['path' => 'island_group_id', 'value' => null],
                                ]);
                            }
                        }
                    }
                } else {
                    $barc = BarangayReference::where('city_id', $validated['provs'][$a]['mun'])->update(
                        [
                            'id' => $validated['provs'][$a]['mun'],
                            'state_id' => $provincec->id,
                            'region_id' => $validated['reg'],
                            'island_group_id' => $validated['isl'],
                        ]
                    );

                    $queryBrgy = $collectionReferenceBrgy->where('id', '=', intval($validated['provs'][$a]['mun']));
                    $documentsBrgy = $queryBrgy->documents();
                
                    foreach ($documentsBrgy as $documentBrgy) {
                        if ($documentBrgy->exists()) {
                            $documentIdBrgy = $documentBrgy->id();
                            $collectionReferenceBrgy->document($documentIdBrgy)->update([
                                ['path' => 'id', 'value' => $validated['provs'][$a]['mun']],
                                ['path' => 'state_id', 'value' => $provincec->id],
                                ['path' => 'region_id', 'value' => intval($validated['reg'])],
                                ['path' => 'island_group_id', 'value' => intval($validated['isl'])],
                            ]);
                        }
                    }
                }
            }

            DB::commit();

            return $this->response(200, 'Province has been successfully updated!', compact($provincec, $munic, $barc));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
