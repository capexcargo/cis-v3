<?php

namespace App\Repositories\Oims\TEDropdownMgmt\BarangayMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\BarangayMgmt\BarangayMgmtInterface;
use App\Models\Crm\BarangayReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\InitializeFirestoreTrait;


class BarangayMgmtRepository implements BarangayMgmtInterface
{
    use ResponseTrait;
    use InitializeFirestoreTrait;

    public function index($request, $search_request)
    {
        DB::beginTransaction();
        try {
            $barangay_lists = BarangayReference::with('ZipcodeReference')->when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', $search_request['name']);
            })->when($search_request['zip'] ?? false, function ($query) use ($search_request) {
                $query->where('zipcode_id', $search_request['zip']);
            })->when($request['stats'], function ($query) use ($request) {
                if ($request['stats'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['stats']);
                }
            })->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'List', compact('barangay_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
                'zip' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Barangay field is Required.',
                    'zip.required' => 'Zipcode field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $bar = BarangayReference::create([
                'name' => $validated['name'],
                'state_id' => null,
                'city_id' => null,
                'zipcode_id' => $validated['zip'],
                'region_id' => null,
                'island_group_id' => null,
                'regCode' => null,
                'brgyCode' => null,
                'status' => 1,
            ]);

            DB::commit();

            $barId = $bar->id;

            $collectionReference = $this->initializeFirestore()->collection('barangay'); 
            $documentReference = $collectionReference->add();
            $documentReference->set([
                'id' => $barId,
                'name' => $validated['name'],
                'state_id' => null,
                'city_id' => null,
                'zipcode_id' => intval($validated['zip']),
                'region_id' => null,
                'island_group_id' => null,
                'regCode' => null,
                'brgyCode' => null,
                'status' => 1,
            ]);

            return $this->response(200, 'Barangay has been successfully added!', compact($bar));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
                'zip' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Barangay field is Required.',
                    'zip.required' => 'Zipcode field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $barangayc = BarangayReference::findOrFail($id);
            if (!$barangayc) {
                return $this->response(404, 'Barangay', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Barangay Management', $barangayc);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $barangayc = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $barangayc->update([
                'name' => $validated['name'],
                // 'state_id' => null,
                // 'city_id' => null,
                'zipcode_id' => $validated['zip'],
                // 'region_id' => null,
                // 'island_group_id' => null,
                // 'regCode' => null,
                // 'brgyCode' => null,
            ]);

            $collectionReference = $this->initializeFirestore()->collection('barangay'); 
    
            $query = $collectionReference->where('id', '=', intval($id));
            $documents = $query->documents();
        
            foreach ($documents as $document) {
                if ($document->exists()) {
                    $documentId = $document->id();
                    $collectionReference->document($documentId)->update([
                        ['path' => 'name', 'value' => $validated['name']],
                        ['path' => 'zipcode_id', 'value' => intval($validated['zip'])],
                    ]);
                }
            }

            DB::commit();

            return $this->response(200, 'Barangay has been successfully updated!', compact($barangayc));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
