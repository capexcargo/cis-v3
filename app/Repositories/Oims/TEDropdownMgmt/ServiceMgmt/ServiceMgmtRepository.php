<?php

namespace App\Repositories\Oims\TEDropdownMgmt\ServiceMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ServiceMgmt\ServiceMgmtInterface;
use App\Models\OimsServiceModeReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\InitializeFirestoreTrait;

class ServiceMgmtRepository implements ServiceMgmtInterface
{
    use ResponseTrait;
    use InitializeFirestoreTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $service_lists = OimsServiceModeReference::with('SMTransportHasOne')->when($request['stats'], function ($query) use ($request) {
                if ($request['stats'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['stats']);
                }
            })->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'List', compact('service_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
                'code' => 'required',
                't_mode' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Paymode field is Required.',
                    'code.required' => 'Code field is Required.',
                    't_mode.required' => 'Transport mode field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $service = OimsServiceModeReference::create([
                'name' => $validated['name'],
                'code' => $validated['code'],
                'transport_mode_id' => $validated['t_mode'],
                'status' => 1,
            ]);

            DB::commit();

            $serviceId = $service->id;

            $collectionReference = $this->initializeFirestore()->collection('oims_service_mode_reference'); 
            $documentReference = $collectionReference->add();
            $documentReference->set([
                'id' => $serviceId,
                'name' => $validated['name'],
                'code' => $validated['code'],
                'transport_mode_id' => intval($validated['t_mode']),
                'status' => 1,
            ]);

            return $this->response(200, 'Service mode has been successfully added!', compact($service));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
                'code' => 'required',
                't_mode' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Paymode field is Required.',
                    'code.required' => 'Code field is Required.',
                    't_mode.required' => 'Transport mode field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $servm = OimsServiceModeReference::findOrFail($id);
            if (!$servm) {
                return $this->response(404, 'Service mode', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Service mode Management', $servm);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $servm = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $servm->update([
                'name' => $validated['name'],
                'code' => $validated['code'],
                'transport_mode_id' => $validated['t_mode'],
            ]);

            $collectionReference = $this->initializeFirestore()->collection('oims_service_mode_reference'); 
    
            $query = $collectionReference->where('id', '=', intval($id));
            $documents = $query->documents();
        
            foreach ($documents as $document) {
                if ($document->exists()) {
                    $documentId = $document->id();
                    $collectionReference->document($documentId)->update([
                        ['path' => 'name', 'value' => $validated['name']],
                        ['path' => 'code', 'value' => $validated['code']],
                        ['path' => 'transport_mode_id', 'value' => intval($validated['t_mode'])],
                    ]);
                }
            }

            DB::commit();

            return $this->response(200, 'Service mode has been successfully updated!', compact($servm));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
