<?php

namespace App\Repositories\Oims\TEDropdownMgmt\MunicipalityMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\MunicipalityMgmt\MunicipalityMgmtInterface;
use App\Models\Crm\BarangayReference;
use App\Models\Crm\CityReference;
use App\Models\Crm\MunicipalityReference;
use App\Models\OimsZipcodeReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\InitializeFirestoreTrait;

class MunicipalityMgmtRepository implements MunicipalityMgmtInterface
{
    use ResponseTrait;
    use InitializeFirestoreTrait;

    public function index($request, $search_request)
    {
        // dd($search_request);
        if(isset($search_request['zip'])){
            $getzipcode = OimsZipcodeReference::where('name', $search_request['zip'])->first();
            $getzipcode_id = $getzipcode->id;
        }else{
            $getzipcode_id = "";
        }
        DB::beginTransaction();
        try {
            $municipality_lists = CityReference::with('BarangayCity')
                ->when($search_request['name'] ?? false, function ($query) use ($search_request) {
                    $query->where('name', $search_request['name']);
                })
                ->when($request['stats'], function ($query) use ($request) {
                    if ($request['stats'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['stats']);
                    }
                })
                ->whereHas('BarangayCity', function ($query) use ($search_request) {
                    $query->when($search_request['brgy'] ?? false, function ($query) use ($search_request) {
                        $query->where('name', $search_request['brgy']);
                    });
                })
                ->whereHas('BarangayCity', function ($query) use ($search_request, $getzipcode_id) {
                    $query->when($search_request['zip'] ?? false, function ($query) use ($search_request, $getzipcode_id) {
                        $query->where('zipcode_id', $getzipcode_id);
                    });
                })
                ->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'List', compact('municipality_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
            ];
            foreach ($request['brgyzips'] as $i => $tellno) {
                $rules['brgyzips.' . $i . '.id'] = 'sometimes';
                $rules['brgyzips.' . $i . '.brgy'] = 'required';
            }
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Municipal field is Required.',
                    'brgyzips.*.brgy.required' => 'Barangay field is required.',

                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $muni = CityReference::create([
                'name' => $validated['name'],
                'city_postal' => 1,
                'state_id' => null,
                'region_id' => null,
                'island_group_id' => null,
                'psgcCode' => null,
                'regDesc' => null,
                'citymunCode' => null,
                'status' => 1,
            ]);
            // dd($muni);

            $muniId = $muni->id;

            $collectionCityReference = $this->initializeFirestore()->collection('city'); 
            $documentCityReference = $collectionCityReference->add();
            $documentCityReference->set([
                'id'=>$muniId,
                'name' => $validated['name'],
                'city_postal' => strval(1),
                'state_id' => null,
                'region_id' => null,
                'island_group_id' => null,
                'psgcCode' => null,
                'regDesc' => null,
                'citymunCode' => null,
                'status' => 1,
            ]);

            $collectionBarangayReference = $this->initializeFirestore()->collection('barangay');
            
            foreach ($validated['brgyzips'] as $i => $brgyzip) {
                $mbrgy = BarangayReference::where('id', $validated['brgyzips'][$i]['brgy'])->update([
                    'city_id' => $muniId,
                ]);
            
                $query = $collectionBarangayReference->where('id','=', intval($validated['brgyzips'][$i]['brgy']));
                $documents = $query->documents();
                
                foreach ($documents as $document) {
                $documentId = $document->id();

                $collectionBarangayReference->document($documentId)->update([
                    ['path' => 'city_id', 'value' => $muniId]
                ]);
                
                }
            }


            DB::commit();

            return $this->response(200, 'Municipal has been successfully added!', compact($muni, $mbrgy));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
            ];
            foreach ($request['brgyzips'] as $i => $asd) {
                $rules['brgyzips.' . $i . '.id'] = 'sometimes';
                $rules['brgyzips.' . $i . '.brgy'] = 'required';
                $rules['brgyzips.' . $i . '.is_deleted'] = 'sometimes';
            }
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Municipal field is Required.',
                    'brgyzips.*.brgy.required' => 'Barangay field is required.',

                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $barangayc = CityReference::with('BarangayCity')->findOrFail($id);
            if (!$barangayc) {
                return $this->response(404, 'Municipality', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Municipality Management', $barangayc);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $barangayc = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $barangayc->update([
                'name' => $validated['name'],
                'city_postal' => 1,
                // 'state_id' => null,
                // 'region_id' => null,
                // 'island_group_id' => null,
                // 'psgcCode' => null,
                // 'regDesc' => null,
                // 'citymunCode' => null,
            ]);

            $collectionReferenceCity = $this->initializeFirestore()->collection('city');
            $queryCity = $collectionReferenceCity->where('id', '=', intval($id));
            $documentsCity = $queryCity->documents();
        
            foreach ($documentsCity as $documentCity) {
                if ($documentCity->exists()) {
                    $documentIdCity = $documentCity->id();
                    $collectionReferenceCity->document($documentIdCity)->update([
                        ['path' => 'name', 'value' => $validated['name']],
                        ['path' => 'city_postal', 'value' => 1],
                    ]);
                }
            }

            // dd($barangayc->id);

            // foreach ($request['brgyzips'] as $i => $brgyzip) {
            //     if ($request['brgyzips'][$i]['id'] && $request['brgyzips'][$i]['is_deleted']) {
            //         $ancillarys = $barangayc->BarangayCity()->find($brgyzip['id']); //query all in database

            //         if ($ancillarys) {
            //             $ancillarys->delete();
            //         }
            //     } else {
            //         $barangayc->BarangayCity()->updateOrCreate(
            //             [
            //                 'id' => $brgyzip['id'],
            //             ],
            //             [
            //                 'city_id' => $barangayc->id,
            //             ]
            //         );
            //     }
            // }

            foreach ($validated['brgyzips'] as $a => $brgyzip) {
                $collectionReferenceBrgy = $this->initializeFirestore()->collection('barangay');

                if ($validated['brgyzips'][$a]['id'] && $validated['brgyzips'][$a]['is_deleted']) {
                    $brgyc = BarangayReference::find($validated['brgyzips'][$a]['id']); //query all in database
                    if ($brgyc) {
                        $brgyc->update(
                            [
                                'city_id' => null,
                            ]
                        );

                        $queryBrgyDel = $collectionReferenceBrgy->where('id', '=', intval($validated['brgyzips'][$a]['id']));
                        $documentsBrgyDel = $queryBrgyDel->documents();

                        foreach ($documentsBrgyDel as $documentBrgyDel) {
                            if ($documentBrgyDel->exists()) {
                                $documentIdBrgyDel = $documentBrgyDel->id();
                                $collectionReferenceBrgy->document($documentIdBrgyDel)->update([
                                    ['path' => 'city_id', 'value' => null],
                                ]);
                            }
                        }
                    }
                } else {
                    $brgyc = BarangayReference::where('id', $validated['brgyzips'][$a]['brgy'])->update(
                        [
                            'id' => $validated['brgyzips'][$a]['brgy'],
                            'city_id' => $barangayc->id,
                        ]
                    );
                    
                    $queryBrgy = $collectionReferenceBrgy->where('id', '=', intval($validated['brgyzips'][$a]['brgy']));
                    $documentsBrgy = $queryBrgy->documents();

                    foreach ($documentsBrgy as $documentBrgy) {
                        if ($documentBrgy->exists()) {
                            $documentIdBrgy = $documentBrgy->id();
                            $collectionReferenceBrgy->document($documentIdBrgy)->update([
                                ['path' => 'id', 'value' => $validated['brgyzips'][$a]['brgy']],
                                ['path' => 'city_id', 'value' => $barangayc->id],
                            ]);
                        }
                    }
                }
            }


            DB::commit();

            return $this->response(200, 'Municipal has been successfully updated!', compact($barangayc, $brgyc));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
