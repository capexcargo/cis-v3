<?php

namespace App\Repositories\Oims\TEDropdownMgmt\ServiceAreaMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ServiceAreaMgmt\ServiceAreaMgmtInterface;
use App\Models\OimsBranchReference;
use App\Models\OimsOdaOpaReference;
use App\Models\OimsServiceAreaCoverage;
use App\Models\OimsTransportReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ServiceAreaMgmtRepository implements ServiceAreaMgmtInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $service_area_lists = OimsServiceAreaCoverage::with('Island', 'Region', 'State', 'City', 'Barangay', 'Ports', 'Serviceability',)->when($request['stats'], function ($query) use ($request) {
                if ($request['stats'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['stats']);
                }
            })->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'List', compact('service_area_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $rules = [
                'isl' => 'required',
                'reg' => 'required',
                'prov' => 'required',
                'mun' => 'required',
                'bar' => 'required',
                'zip' => 'sometimes',
                'ptc' => 'required',
                'serv' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'isl.required' => 'Island Group field is Required.',
                    'reg.required' => 'Region field is Required.',
                    'prov.required' => 'Province field is Required.',
                    'mun.required' => 'Municipality field is Required.',
                    'bar.required' => 'Barangay field is Required.',
                    'ptc.required' => 'Port to Cater field is Required.',
                    'serv.required' => 'Servicability field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            // dd($validated);
            $porttocater = OimsBranchReference::where('name', $validated['ptc'])->first();
            // dd($porttocater['id']);

            $servicearea = OimsServiceAreaCoverage::create([
                'island_group_id' => $validated['isl'],
                'region_id' => $validated['reg'],
                'state_id' => $validated['prov'],
                'city_id' => $validated['mun'],
                'barangay_id' => $validated['bar'],
                'zipcode' => $validated['zip'],
                'port_to_cater_id' => $porttocater['id'],
                'serviceability_id' => $validated['serv'],
                // 'cost_charge' => $request['odaopa'],
                'status' => 1,
            ]);

            DB::commit();

            return $this->response(200, 'Service Area Coverage has been successfully added!', compact($servicearea));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $rules = [
                'isl' => 'required',
                'reg' => 'required',
                'prov' => 'required',
                'mun' => 'required',
                'bar' => 'required',
                'zip' => 'sometimes',
                'ptc' => 'required',
                'serv' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'isl.required' => 'Island Group field is Required.',
                    'reg.required' => 'Region field is Required.',
                    'prov.required' => 'Province field is Required.',
                    'mun.required' => 'Municipality field is Required.',
                    'bar.required' => 'Barangay field is Required.',
                    'ptc.required' => 'Port to Cater field is Required.',
                    'serv.required' => 'Servicability field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $sac = OimsServiceAreaCoverage::findOrFail($id);
            if (!$sac) {
                return $this->response(404, 'Service Area Coverage', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Service Area Coverage', $sac);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $sac = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $porttocateru = OimsBranchReference::where('name', $validated['ptc'])->first();

            $sac->update([
                'island_group_id' => $validated['isl'],
                'region_id' => $validated['reg'],
                'state_id' => $validated['prov'],
                'city_id' => $validated['mun'],
                'barangay_id' => $validated['bar'],
                'zipcode' => $validated['zip'],
                'port_to_cater_id' => $porttocateru['id'],
                'serviceability_id' => $validated['serv'],
                // 'cost_charge' => $request['odaopa'],
                // 'status' => 1,
            ]);

            DB::commit();

            return $this->response(200, 'Service Area Coverage has been successfully updated!', compact($sac));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
