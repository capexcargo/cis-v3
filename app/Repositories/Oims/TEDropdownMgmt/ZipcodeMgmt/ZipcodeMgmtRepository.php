<?php

namespace App\Repositories\Oims\TEDropdownMgmt\ZipcodeMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ZipcodeMgmt\ZipcodeMgmtInterface;
use App\Models\OimsZipcodeReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\InitializeFirestoreTrait;


class ZipcodeMgmtRepository implements ZipcodeMgmtInterface
{
    use ResponseTrait;
    use InitializeFirestoreTrait;

    public function index($request, $search_request)
    {
        DB::beginTransaction();
        try {
            $zipcode_lists = OimsZipcodeReference::when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', $search_request['name']);
            })->when($request['stats'], function ($query) use ($request) {
                if ($request['stats'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['stats']);
                }
            })->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'List', compact('zipcode_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'name' => 'required|unique:oims_zipcode_reference,name',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Zipcode field is Required.',
                    'name.unique' => 'Zipcode is already taken',

                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $zip = OimsZipcodeReference::create([
                'name' => $validated['name'],
                'status' => 1,
            ]);

            DB::commit();

            $zipId = $zip->id;

            $collectionReference = $this->initializeFirestore()->collection('oims_zipcode_reference'); 
            $documentReference = $collectionReference->add();
            $documentReference->set([
                'id' => $zipId,
                'name' => $validated['name'],
                'status' => 1,
            ]);

            return $this->response(200, 'Zipcode has been successfully added!', compact($zip));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'name' => 'required|unique:oims_zipcode_reference,name,' . $id,
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Zipcode field is Required.',
                    'name.unique' => 'Zipcode is already taken',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $zipc = OimsZipcodeReference::findOrFail($id);
            if (!$zipc) {
                return $this->response(404, 'Zipcode', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Zipcode Management', $zipc);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $zipc = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $zipc->update([
                'name' => $validated['name'],
            ]);

            $collectionReference = $this->initializeFirestore()->collection('oims_zipcode_reference'); 
    
            $query = $collectionReference->where('id', '=', intval($id));
            $documents = $query->documents();
        
            foreach ($documents as $document) {
                if ($document->exists()) {
                    $documentId = $document->id();
                    $collectionReference->document($documentId)->update([
                        ['path' => 'name', 'value' => $validated['name']]
                    ]);
                }
            }

            DB::commit();

            return $this->response(200, 'Zipcode has been successfully updated!', compact($zipc));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
