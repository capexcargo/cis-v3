<?php

namespace App\Repositories\Oims\TEDropdownMgmt\BranchMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\BranchMgmt\BranchMgmtInterface;
use App\Models\OimsBranchReference;
use App\Models\OimsBranchReferenceContactNo;
use App\Models\OimsBranchReferenceTelephone;
use App\Models\OriginDestinationPortReference;
use App\Models\RouteCategoryReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\InitializeFirestoreTrait;

class BranchMgmtRepository implements BranchMgmtInterface
{
    use ResponseTrait;
    use InitializeFirestoreTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $branch_lists = OimsBranchReference::with('OBRTelephoneHasMany', 'OBRContactHasMany', 'RegionRefs', 'OriginRefs')->when($request['stats'], function ($query) use ($request) {
                if ($request['stats'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['stats']);
                }
            })->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'List', compact('branch_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $rules = [
                'r_name' => 'required',
                'b_name' => 'required',
                'adrs' => 'required',
                'orig' => 'required',
                'lat' => 'required',
                'long' => 'required',
            ];

            foreach ($request['tellnos'] as $i => $tellno) {
                $rules['tellnos.' . $i . '.id'] = 'sometimes';
                $rules['tellnos.' . $i . '.tell_no'] = 'required';
            }

            foreach ($request['contacts'] as $i => $contact) {
                $rules['contacts.' . $i . '.id'] = 'sometimes';
                $rules['contacts.' . $i . '.cont'] = 'required';
            }
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'r_name.required' => 'The Region field is Required.',
                    'b_name.required' => 'The Branch Name field is Required.',
                    'adrs.required' => 'The Address field is Required.',
                    'orig.required' => 'The Branch to Cater field is Required.',
                    'tellnos.*.tell_no.required' => 'The Telephone No field is required.',
                    'contacts.*.cont.required' => 'The Contact No field is required.',

                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {


        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];
            // dd($validated);

            //branch
            $OBR = OimsBranchReference::create([
                'region' => $validated['r_name'],
                'name' => $validated['b_name'],
                'address' => $validated['adrs'],
                'origin_destination_id' => $validated['orig'],
                'latitude' => $validated['lat'],
                'longitude' => $validated['long'],
                'status' => 1,
                'default' => 0,
            ]);

            $OBRid = $OBR->id;
            
            $collectionBranchReference = $this->initializeFirestore()->collection('oims_branch_reference'); 
            $documentBranchReference = $collectionBranchReference->add();
            $documentBranchReference->set([
                'id' => $OBRid,
                'region' => intval($validated['r_name']),
                'name' => $validated['b_name'],
                'address' => $validated['adrs'],
                'origin_destination_id' => intval($validated['orig']),
                'latitude' => $validated['lat'],
                'longitude' => $validated['long'],
                'status' => 1,
                'default' => 0,
            ]);

            //telephone
            foreach ($validated['tellnos'] as $i => $tellno) {
                $OBRTN = OimsBranchReferenceTelephone::create([
                    'branch_reference_id' => $OBR->id,
                    'telephone' => $validated['tellnos'][$i]['tell_no'],
                ]);
            }

            $collectionBranchReferenceTelephone = $this->initializeFirestore()->collection('oims_branch_reference_telephone'); 
            $documentBranchReferenceTelephone = $collectionBranchReferenceTelephone->add();
            $documentBranchReferenceTelephone->set([
                'id' => $OBRTN->id,
                'branch_reference_id' => $OBR->id,
                'telephone' => $OBRTN->telephone,
            ]);

            //contact no
            foreach ($validated['contacts'] as $i => $tellno) {
                $OBRCN = OimsBranchReferenceContactNo::create([
                    'branch_reference_id' => $OBR->id,
                    'contact_no' => $validated['contacts'][$i]['cont'],
                ]);
            }

            $collectionBranchReferenceContact = $this->initializeFirestore()->collection('oims_branch_reference_contact_no'); 
            $documentBranchReferenceContact = $collectionBranchReferenceContact->add();
            $documentBranchReferenceContact->set([
                'id' => $OBRCN->id,
                'branch_reference_id' => $OBR->id,
                'contact_no' => $OBRCN->contact_no,
            ]);

            DB::commit();

            return $this->response(200, 'Branch has been successfully added!', compact($OBR, $OBRTN, $OBRCN));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $rules = [
                'r_name' => 'required',
                'b_name' => 'required',
                'adrs' => 'required',
                'orig' => 'required',
                'lat' => 'required',
                'long' => 'required',
            ];

            foreach ($request['tellnos'] as $i => $tellno) {
                $rules['tellnos.' . $i . '.id'] = 'sometimes';
                $rules['tellnos.' . $i . '.tell_no'] = 'required';
                $rules['tellnos.' . $i . '.is_deleted'] = 'sometimes';

            }

            foreach ($request['contacts'] as $i => $contact) {
                $rules['contacts.' . $i . '.id'] = 'sometimes';
                $rules['contacts.' . $i . '.cont'] = 'required';
                $rules['contacts.' . $i . '.is_deleted'] = 'sometimes';

            }
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'r_name.required' => 'The Region field is Required.',
                    'b_name.required' => 'The Branch Name field is Required.',
                    'adrs.required' => 'The Address field is Required.',
                    'orig.required' => 'The Branch to Cater field is Required.',
                    'tellnos.*.tell_no.required' => 'The Telephone No field is required.',
                    'contacts.*.cont.required' => 'The Contact No field is required.',

                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $OBR = OimsBranchReference::findOrFail($id);
            if (!$OBR) {
                return $this->response(404, 'Branch', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Branch Management', $OBR);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $OBR = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $OBR->updateOrCreate([
                'id' => $id,
            ], [
                'region' => $validated['r_name'],
                'name' => $validated['b_name'],
                'address' => $validated['adrs'],
                'origin_destination_id' => $validated['orig'],
                'latitude' => $validated['lat'],
                'longitude' => $validated['long'],
            ]);

            $collectionReferenceBranch = $this->initializeFirestore()->collection('oims_branch_reference'); 
            $queryBranch = $collectionReferenceBranch->where('id', '=', intval($id));
            $documentsBranch = $queryBranch->documents();
        
            foreach ($documentsBranch as $documentBranch) {
                if ($documentBranch->exists()) {
                    $documentIdBranch = $documentBranch->id();
                    $collectionReferenceBranch->document($documentIdBranch)->set([
                        'id' => $id,
                        'region' => intval($validated['r_name']),
                        'name' => $validated['b_name'],
                        'address' => $validated['adrs'],
                        'origin_destination_id' => intval($validated['orig']),
                        'latitude' => $validated['lat'],
                        'longitude' => $validated['long'],
                        'status' => 1,
                        'default' => $OBR->default,
                    ]);
                }
            }

            foreach ($validated['tellnos'] as $a => $tellno) {

                $collectionReferenceTel = $this->initializeFirestore()->collection('oims_branch_reference_telephone'); 
                $queryTel = $collectionReferenceTel->where('id', '=', intval($validated['tellnos'][$a]['id']));
                $documentsTel = $queryTel->documents();

                if ($validated['tellnos'][$a]['id'] && $validated['tellnos'][$a]['is_deleted']) {
                    $OBRTN = OimsBranchReferenceTelephone::find($validated['tellnos'][$a]['id']); //query all in database

                    if ($OBRTN) {
                        $OBRTN->delete();

                        //firestore delete
                        foreach ($documentsTel as $documentTel) {
                            $documentTel->reference()->delete();
                        }
                        
                    }
                } else {
                    $OBRTN = OimsBranchReferenceTelephone::updateOrCreate(
                        [
                            'id' => $validated['tellnos'][$a]['id'],
                        ],
                        [
                            'branch_reference_id' => $OBR->id,
                            'telephone' => $validated['tellnos'][$a]['tell_no'],
                        ]
                    );

                    //firestore updateOrCreate
                    foreach ($documentsTel as $documentTel) {
                        $documentTel->reference()->set([
                            'id' => $validated['tellnos'][$a]['id'],
                            'branch_reference_id' => $OBR->id,
                            'telephone' => $validated['tellnos'][$a]['tell_no'],
                        ]);
                    }
                    
                    if ($documentsTel->isEmpty()) {
                        $collectionReferenceTel->add([
                            'id' => $OBRTN->id,
                            'branch_reference_id' => $OBR->id,
                            'telephone' => $validated['tellnos'][$a]['tell_no'],
                        ]);
                    }
                }
            }

            foreach ($validated['contacts'] as $b => $contact) {
                $collectionReferenceContact = $this->initializeFirestore()->collection('oims_branch_reference_contact_no'); 
                $queryContact = $collectionReferenceContact->where('id', '=', intval($validated['contacts'][$b]['id']));
                $documentsContact = $queryContact->documents();

                if ($validated['contacts'][$b]['id'] && $validated['contacts'][$b]['is_deleted']) {
                    $OBRCN = OimsBranchReferenceContactNo::find($validated['contacts'][$b]['id']); //query all in database
                    if ($OBRCN) {
                        $OBRCN->delete();

                        //firestore delete
                        foreach ($documentsContact as $documentContact ) {
                            $documentContact ->reference()->delete();
                        }
                    }
                } else {
                    $OBRCN = OimsBranchReferenceContactNo::updateOrCreate(
                        [
                            'id' => $validated['contacts'][$b]['id'],
                        ],
                        [
                            'branch_reference_id' => $OBR->id,
                            'contact_no' => $validated['contacts'][$b]['cont'],
                        ]
                    );

                    //firestore updateOrCreate
                    foreach ($documentsContact as $documentContact) {
                        $documentContact->reference()->set([
                            'id' => $validated['contacts'][$b]['id'],
                            'branch_reference_id' => $OBR->id,
                            'contact_no' =>  $validated['contacts'][$b]['cont'],
                        ]);
                    }
                    
                    if ($documentsContact->isEmpty()) {
                        $collectionReferenceContact->add([
                            'id' => $OBRCN->id,
                            'branch_reference_id' => $OBR->id,
                            'contact_no' =>  $validated['contacts'][$b]['cont'],
                        ]);
                    }
                    
                }
            }

            DB::commit();

            return $this->response(200, 'branch has been successfully updated!', compact($OBR, $OBRTN, $OBRCN));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
