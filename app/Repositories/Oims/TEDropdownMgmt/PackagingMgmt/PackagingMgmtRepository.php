<?php

namespace App\Repositories\Oims\TEDropdownMgmt\PackagingMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\PackagingMgmt\PackagingMgmtInterface;
use App\Models\OimsPackagingGoodsReference;
use App\Models\OimsOdaOpaReference;
use App\Models\OimsPackagingReference;
use App\Models\OimsTransportReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PackagingMgmtRepository implements PackagingMgmtInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $packaging_lists = OimsPackagingReference::when($request['stats'], function ($query) use ($request) {
                if ($request['stats'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['stats']);
                }
            })->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'List', compact('packaging_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Type of Packaging field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $paks = OimsPackagingReference::create([
                'name' => $validated['name'],
                'status' => 1,
            ]);

            DB::commit();

            return $this->response(200, 'Type of Packaging has been successfully added!', compact($paks));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Type of Packaging field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $packs = OimsPackagingReference::findOrFail($id);
            if (!$packs) {
                return $this->response(404, 'Type of Packaging', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Type of Packaging Management', $packs);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $packs = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $packs->update([
                'name' => $validated['name'],
            ]);

            DB::commit();

            return $this->response(200, 'Type of Packaging has been successfully updated!', compact($packs));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    
}
