<?php

namespace App\Repositories\Oims\TEDropdownMgmt\TransportMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\TransportMgmt\TransportMgmtInterface;
use App\Models\OimsTransportReference;
use App\Models\OriginDestinationPortReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\InitializeFirestoreTrait;

class TransportMgmtRepository implements TransportMgmtInterface
{
    use ResponseTrait;
    use InitializeFirestoreTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $transport_lists = OimsTransportReference::when($request['stats'], function ($query) use ($request) {
                if ($request['stats'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['stats']);
                }
            })->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'List', compact('transport_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Transport mode field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $transportins =OimsTransportReference::create([
                'name' => $validated['name'],
                'status' => 1,
            ]);

            DB::commit();

            $transportinsId = $transportins->id;

            $collectionReference = $this->initializeFirestore()->collection('oims_transport_reference'); 
            $documentReference = $collectionReference->add();
            $documentReference->set([
                'id' => $transportinsId,
                'name' => $validated['name'],
                'status' => 1,
            ]);

            return $this->response(200, 'Transport Mode has been successfully added!', compact($transportins));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Transport Mode field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $trans = OimsTransportReference::findOrFail($id);
            if (!$trans) {
                return $this->response(404, 'Transport Mode', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Transport Mode Management', $trans);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $trans = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $trans->update([
                'name' => $validated['name'],
            ]);

            $collectionReference = $this->initializeFirestore()->collection('oims_transport_reference'); 
    
            $query = $collectionReference->where('id', '=', intval($id));
            $documents = $query->documents();
        
            foreach ($documents as $document) {
                if ($document->exists()) {
                    $documentId = $document->id();
                    $collectionReference->document($documentId)->update([
                        ['path' => 'name', 'value' => $validated['name']]
                    ]);
                }
            }

            DB::commit();

            return $this->response(200, 'Transport Mode has been successfully updated!', compact($trans));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    
}
