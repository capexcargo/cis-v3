<?php

namespace App\Repositories\Oims\TEDropdownMgmt\ReasonMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ReasonMgmt\ReasonMgmtInterface;
use App\Models\OimsOdaOpaReference;
use App\Models\OimsReasonReference;
use App\Models\OimsTransportReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ReasonMgmtRepository implements ReasonMgmtInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $reason_lists = OimsReasonReference::with('Apps')->when($request['stats'], function ($query) use ($request) {
                if ($request['stats'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['stats']);
                }
            })->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'List', compact('reason_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
                't_reason' => 'required',
                'app' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Reason field is Required.',
                    't_reason.required' => 'Type of Reason field is Required.',
                    'app.required' => 'Application field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $reasons = OimsReasonReference::create([
                'name' => $validated['name'],
                'type_of_reason' => $validated['t_reason'],
                'application_id' => $validated['app'],
                'status' => 1,
            ]);

            DB::commit();

            return $this->response(200, 'Reason has been successfully added!', compact($reasons));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
                't_reason' => 'required',
                'app' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Reason field is Required.',
                    't_reason.required' => 'Type of Reason field is Required.',
                    'app.required' => 'Application field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $reas = OimsReasonReference::findOrFail($id);
            if (!$reas) {
                return $this->response(404, 'Reason', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Reason Management', $reas);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $reas = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $reas->update([
                'name' => $validated['name'],
                'type_of_reason' => $validated['t_reason'],
                'application_id' => $validated['app'],
            ]);

            DB::commit();

            return $this->response(200, 'Reason has been successfully updated!', compact($reas));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
