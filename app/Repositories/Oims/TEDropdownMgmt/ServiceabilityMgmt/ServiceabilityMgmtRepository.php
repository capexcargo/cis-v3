<?php

namespace App\Repositories\Oims\TEDropdownMgmt\ServiceabilityMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ServiceabilityMgmt\ServiceabilityMgmtInterface;
use App\Models\OimsOdaOpaReference;
use App\Models\OimsServiceabilityReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ServiceabilityMgmtRepository implements ServiceabilityMgmtInterface
{
    use ResponseTrait;

    public function index($request, $search_request)
    {
        DB::beginTransaction();
        try {
            $serviceability_lists = OimsServiceabilityReference::when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', $search_request['name']);
            })->when($request['stats'], function ($query) use ($request) {
                if ($request['stats'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['stats']);
                }
            })->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'List', compact('serviceability_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
                'color' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Serviceability field is Required.',
                    'color.required' => 'Color field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $serbility = OimsServiceabilityReference::create([
                'name' => $validated['name'],
                'color' => $validated['color'],
                'status' => 1,
            ]);

            DB::commit();

            return $this->response(200, 'Serviceability has been successfully added!', compact($serbility));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
                'color' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Serviceability field is Required.',
                    'color.required' => 'Color field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $servabs = OimsServiceabilityReference::findOrFail($id);
            if (!$servabs) {
                return $this->response(404, 'Serviceability', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Serviceability Management', $servabs);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $servabs = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $servabs->update([
                'name' => $validated['name'],
                'color' => $validated['color'],
            ]);

            DB::commit();

            return $this->response(200, 'Serviceability has been successfully updated!', compact($servabs));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
