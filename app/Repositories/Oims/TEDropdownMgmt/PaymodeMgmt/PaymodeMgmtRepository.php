<?php

namespace App\Repositories\Oims\TEDropdownMgmt\PaymodeMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\PaymodeMgmt\PaymodeMgmtInterface;
use App\Models\OimsPaymodeReference;
use App\Models\OimsTransportReference;
use App\Models\OriginDestinationPortReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\InitializeFirestoreTrait;

class PaymodeMgmtRepository implements PaymodeMgmtInterface
{
    use ResponseTrait;
    use InitializeFirestoreTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $paymode_lists = OimsPaymodeReference::when($request['stats'], function ($query) use ($request) {
                if ($request['stats'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['stats']);
                }
            })->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'List', compact('paymode_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
                'description' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Paymode field is Required.',
                    'description.required' => 'Description field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $paymode =OimsPaymodeReference::create([
                'name' => $validated['name'],
                'description' => $validated['description'],
                'status' => 1,
            ]);

            DB::commit();

            $paymodeId = $paymode->id;
            
            $collectionReference = $this->initializeFirestore()->collection('oims_paymode_reference'); 
            $documentReference = $collectionReference->add();
            $documentReference->set([
                'id' => $paymodeId,
                'name' => $validated['name'],
                'description' => $validated['description'],
                'status' => 1,
            ]);

            return $this->response(200, 'Paymode has been successfully added!', compact($paymode));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
                'description' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Paymode field is Required.',
                    'description.required' => 'Description field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $paym = OimsPaymodeReference::findOrFail($id);
            if (!$paym) {
                return $this->response(404, 'Paymode', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Paymode Management', $paym);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $paym = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $paym->update([
                'name' => $validated['name'],
                'description' => $validated['description'],
            ]);

            $collectionReference = $this->initializeFirestore()->collection('oims_paymode_reference'); 
    
            $query = $collectionReference->where('id', '=', intval($id));
            $documents = $query->documents();
        
            foreach ($documents as $document) {
                if ($document->exists()) {
                    $documentId = $document->id();
                    $collectionReference->document($documentId)->update([
                        ['path' => 'name', 'value' => $validated['name']],
                        ['path' => 'description', 'value' => $validated['description']],
                    ]);
                }
            }

            DB::commit();

            return $this->response(200, 'Paymode has been successfully updated!', compact($paym));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    
}
