<?php

namespace App\Repositories\Oims\TEDropdownMgmt\RemarksMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\RemarksMgmt\RemarksMgmtInterface;
use App\Models\OimsOdaOpaReference;
use App\Models\OimsRemarks;
use App\Models\OimsRemarksReference;
use App\Models\OimsTransportReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RemarksMgmtRepository implements RemarksMgmtInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $remarks_lists = OimsRemarks::with('Transaction')->when($request['stats'], function ($query) use ($request) {
                if ($request['stats'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['stats']);
                }
            })->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'List', compact('remarks_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
                't_remarks' => 'required',
                'transact' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Remarks field is Required.',
                    't_remarks.required' => 'Type of Remarks field is Required.',
                    'transact.required' => 'Transaction Stage field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $remarks = OimsRemarks::create([
                'name' => $validated['name'],
                'type_of_remarks' => $validated['t_remarks'],
                'transaction_stage_id' => $validated['transact'],
                'status' => 1,
            ]);

            DB::commit();

            return $this->response(200, 'Remarks has been successfully added!', compact($remarks));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
                't_remarks' => 'required',
                'transact' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Remarks field is Required.',
                    't_remarks.required' => 'Type of Remarks field is Required.',
                    'transact.required' => 'Transaction Stage field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $rems = OimsRemarks::findOrFail($id);
            if (!$rems) {
                return $this->response(404, 'Remarks', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Remark Management', $rems);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $rems = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $rems->update([
                'name' => $validated['name'],
                'type_of_remarks' => $validated['t_remarks'],
                'transaction_stage_id' => $validated['transact'],
            ]);

            DB::commit();

            return $this->response(200, 'Remarks has been successfully updated!', compact($rems));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
