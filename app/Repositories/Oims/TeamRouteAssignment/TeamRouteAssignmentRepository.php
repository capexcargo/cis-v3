<?php

namespace App\Repositories\Oims\TeamRouteAssignment;

use App\Interfaces\Oims\TeamRouteAssignment\TeamRouteAssignmentInterface;
use App\Models\OimsBranchReference;
use App\Models\OimsTeamRouteAssignment;
use App\Models\OimsTeamRouteAssignmentDetails;
use App\Models\OimsVehicle;
use App\Models\RouteCategoryReference;
use App\Traits\ResponseTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TeamRouteAssignmentRepository implements TeamRouteAssignmentInterface
{
    use ResponseTrait;

    public function index($request, $search_request)
    {
        // dd($request['getuserbranch']);
        // dd($search_request);
        // if(isset($search_request['zip'])){
        // $plates = OimsVehicle::get();
        // $plate_ids = $plates->id;
        // dd($plates);
        // }else{
        //     $getzipcode_id = "";
        // }

        // if ($request['stats'] == 2) {
        //     $plates = OimsVehicle::get();
        //     $plate_ids = $plates->id;
        // } else {
        //     $plate_ids = "";
        // }
        DB::beginTransaction();
        try {

            $getdataforvalidation = OimsVehicle::with(['teamDetails' => function ($query) use ($search_request) {
                $query->whereDate('dispatch_date', Carbon::today());
                $query->with('teamIdReference', 'teamRouteReference', 'teamPlateReference', 'teamChecker2Reference');
                $query->with(['teamRouteAssignmentReference' => function ($queryd1) use ($search_request) {
                    $queryd1->groupBy('id')->get();
                    // $queryd1->when($search_request['dispatch_date'] ?? false, function ($query) use ($search_request) {
                    //     $query->where('dispatch_date', $search_request['dispatch_date']);
                    // });
                }]);
                $query->with(['teamDriverReference' => function ($queryd1) {
                    $queryd1->with(['userDetails' => function ($queryd2) {
                        $queryd2->with('timeLog');
                    }]);
                }]);
                $query->with(['teamChecker1Reference' => function ($queryd1) {
                    $queryd1->with(['userDetails' => function ($queryd2) {
                        $queryd2->with('timeLog');
                    }]);
                }]);
                $query->with(['teamChecker2Reference' => function ($queryd1) {
                    $queryd1->with(['userDetails' => function ($queryd2) {
                        $queryd2->with('timeLog');
                    }]);
                }]);
                $query->when($search_request['route_category_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('route_category_id', $search_request['route_category_id']);
                });
                $query->when($search_request['plate_no_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('plate_no_id', $search_request['plate_no_id']);
                });
                $query->when($search_request['driver_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('driver_id', $search_request['driver_id']);
                });
                $query->when($search_request['checker1_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('checker1_id', $search_request['checker1_id']);
                });
                $query->whereHas('teamRouteAssignmentReference', function ($query) use ($search_request) {
                    $query->when($search_request['dispatch_date'] ?? false, function ($query) use ($search_request) {
                        $query->where('dispatch_date', 'like', '%' . $search_request['dispatch_date'] . '%');
                    });
                });
                // $query->when($search_request['dispatch_date'] ?? false, function ($query) use ($search_request) {
                //     $query->where('dispatch_date', 'like', '%' . $search_request['dispatch_date'] . '%');
                // });
                // $query->whereDate('dispatch_date', Carbon::today());
                // $query->orderBy('id');
            }])->where('branch_id',$request['getuserbranch'])

                // ->whereHas('teamDetails', function ($query) use ($search_request) {
                //     $query->when($search_request['booking_branch_id'] ?? false, function ($query) use ($search_request) {
                //         $query->where('booking_branch_id', 'like', '%' . $search_request['booking_branch_id'] . '%');
                //     });
                // })
                // ->whereHas('teamDetails', function ($query) {
                //     $query->whereDate('dispatch_date', Carbon::today());
                // })
                // ->when($search_request['dispatch_date'] ?? false, function ($query) use ($search_request) {
                //     $query->where('dispatch_date', $search_request['dispatch_date']);
                // })
                // ->when($search_request['branch_id'] ?? false, function ($query) use ($search_request) {
                //     $query->where('branch_id', $search_request['branch_id']);
                // })
                // ->when($search_request['route'] ?? false, function ($query) use ($search_request) {
                //     $query->where('route_category_id', $search_request['route']);
                // })
                // ->whereNull('teamDetails.id')
                ->paginate($request['paginate']);

            $activedata = [];
            $inactivedata = [];

            foreach ($getdataforvalidation as $val) {
                if (isset($val['teamDetails']['id'])) {
                    $activedata[] = $val['id'];
                } else {
                    $inactivedata[] = $val['id'];
                }
            }


            $team_routes = OimsVehicle::with(['teamDetails' => function ($query) use ($search_request) {
                $query->whereDate('dispatch_date', Carbon::today());
                $query->with('teamIdReference', 'teamRouteReference', 'teamPlateReference', 'teamChecker2Reference');
                $query->with(['teamRouteAssignmentReference' => function ($queryd1) use ($search_request) {
                    $queryd1->groupBy('id')->get();
                    // $queryd1->when($search_request['dispatch_date'] ?? false, function ($query) use ($search_request) {
                    //     $query->where('dispatch_date', $search_request['dispatch_date']);
                    // });
                }]);
                $query->with(['teamDriverReference' => function ($queryd1) {
                    $queryd1->with(['userDetails' => function ($queryd2) {
                        $queryd2->with('timeLog');
                    }]);
                }]);
                $query->with(['teamChecker1Reference' => function ($queryd1) {
                    $queryd1->with(['userDetails' => function ($queryd2) {
                        $queryd2->with('timeLog');
                    }]);
                }]);
                $query->with(['teamChecker2Reference' => function ($queryd1) {
                    $queryd1->with(['userDetails' => function ($queryd2) {
                        $queryd2->with('timeLog');
                    }]);
                }]);
                $query->when($search_request['route_category_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('route_category_id', $search_request['route_category_id']);
                });
                $query->when($search_request['plate_no_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('plate_no_id', $search_request['plate_no_id']);
                });
                $query->when($search_request['driver_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('driver_id', $search_request['driver_id']);
                });
                $query->when($search_request['checker1_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('checker1_id', $search_request['checker1_id']);
                });
                $query->whereHas('teamRouteAssignmentReference', function ($query) use ($search_request) {
                    $query->when($search_request['dispatch_date'] ?? false, function ($query) use ($search_request) {
                        $query->where('dispatch_date', 'like', '%' . $search_request['dispatch_date'] . '%');
                    });
                });
                // $query->when($search_request['dispatch_date'] ?? false, function ($query) use ($search_request) {
                //     $query->where('dispatch_date', $search_request['dispatch_date']);
                // });
                // $query->whereDate('dispatch_date', Carbon::today());
                // $query->orderBy('id');
            }])->where('branch_id',$request['getuserbranch'])
                ->when($request['stats'], function ($query) use ($request, $activedata, $inactivedata) {
                    if ($request['stats'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } elseif ($request['stats'] == 1) {
                        $query->whereIn('id', $activedata);
                    } elseif ($request['stats'] == 2) {
                        $query->whereIn('id', $inactivedata);
                    } else {
                        $query->where('status', $request['stats']);
                    }
                })
                // ->whereHas('teamDetails', function ($query) use ($search_request) {
                //     $query->when($search_request['dispatch_date'] ?? false, function ($query) use ($search_request) {
                //         $query->where('dispatch_date', 'like', '%' . $search_request['dispatch_date'] . '%');
                //     });
                // })
                // ->when($search_request['dispatch_date'] ?? false, function ($query) use ($search_request) {
                //     $query->where('dispatch_date', $search_request['dispatch_date']);
                // })
                // ->when($search_request['branch_id'] ?? false, function ($query) use ($search_request) {
                //     $query->where('branch_id', $search_request['branch_id']);
                // })
                // ->when($search_request['route'] ?? false, function ($query) use ($search_request) {
                //     $query->where('route_category_id', $search_request['route']);
                // })
                // ->whereNull('teamDetails.id')

                // ->whereIn('id', $inactivedata)

                ->paginate($request['paginate']);

            // dd($team_routes);


            DB::commit();
            // $asdasd = $team_routes->teamDetails->merge($asdasd);




            return $this->response(200, 'List', compact('team_routes'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function print($request, $search_request)
    {
        
        DB::beginTransaction();
        try {

            $getdataforvalidation = OimsVehicle::with(['teamDetails' => function ($query) use ($search_request) {
                $query->whereDate('dispatch_date', Carbon::today());
                $query->with('teamIdReference', 'teamRouteReference', 'teamPlateReference', 'teamChecker2Reference');
                $query->with(['teamRouteAssignmentReference' => function ($queryd1) use ($search_request) {
                    $queryd1->groupBy('id')->get();
                }]);
                $query->with(['teamDriverReference' => function ($queryd1) {
                    $queryd1->with(['userDetails' => function ($queryd2) {
                        $queryd2->with('timeLog');
                    }]);
                }]);
                $query->with(['teamChecker1Reference' => function ($queryd1) {
                    $queryd1->with(['userDetails' => function ($queryd2) {
                        $queryd2->with('timeLog');
                    }]);
                }]);
                $query->with(['teamChecker2Reference' => function ($queryd1) {
                    $queryd1->with(['userDetails' => function ($queryd2) {
                        $queryd2->with('timeLog');
                    }]);
                }]);
                $query->when($search_request['route_category_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('route_category_id', $search_request['route_category_id']);
                });
                $query->when($search_request['plate_no_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('plate_no_id', $search_request['plate_no_id']);
                });
                $query->when($search_request['driver_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('driver_id', $search_request['driver_id']);
                });
                $query->when($search_request['checker1_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('checker1_id', $search_request['checker1_id']);
                });
                $query->whereHas('teamRouteAssignmentReference', function ($query) use ($search_request) {
                    $query->when($search_request['dispatch_date'] ?? false, function ($query) use ($search_request) {
                        $query->where('dispatch_date', 'like', '%' . $search_request['dispatch_date'] . '%');
                    });
                });
            }])->where('branch_id',$request['getuserbranch'])

                ->paginate($request['paginate']);

            $activedata = [];
            $inactivedata = [];

            foreach ($getdataforvalidation as $val) {
                if (isset($val['teamDetails']['id'])) {
                    $activedata[] = $val['id'];
                } else {
                    $inactivedata[] = $val['id'];
                }
            }


            $team_routes = OimsVehicle::with(['teamDetails' => function ($query) use ($search_request) {
                $query->whereDate('dispatch_date', Carbon::today());
                $query->with('teamIdReference', 'teamRouteReference', 'teamPlateReference', 'teamChecker2Reference');
                $query->with(['teamRouteAssignmentReference' => function ($queryd1) use ($search_request) {
                    $queryd1->groupBy('id')->get();
                   
                }]);
                $query->with(['teamDriverReference' => function ($queryd1) {
                    $queryd1->with(['userDetails' => function ($queryd2) {
                        $queryd2->with('timeLog');
                    }]);
                }]);
                $query->with(['teamChecker1Reference' => function ($queryd1) {
                    $queryd1->with(['userDetails' => function ($queryd2) {
                        $queryd2->with('timeLog');
                    }]);
                }]);
                $query->with(['teamChecker2Reference' => function ($queryd1) {
                    $queryd1->with(['userDetails' => function ($queryd2) {
                        $queryd2->with('timeLog');
                    }]);
                }]);
                $query->when($search_request['route_category_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('route_category_id', $search_request['route_category_id']);
                });
                $query->when($search_request['plate_no_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('plate_no_id', $search_request['plate_no_id']);
                });
                $query->when($search_request['driver_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('driver_id', $search_request['driver_id']);
                });
                $query->when($search_request['checker1_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('checker1_id', $search_request['checker1_id']);
                });
                $query->whereHas('teamRouteAssignmentReference', function ($query) use ($search_request) {
                    $query->when($search_request['dispatch_date'] ?? false, function ($query) use ($search_request) {
                        $query->where('dispatch_date', 'like', '%' . $search_request['dispatch_date'] . '%');
                    });
                });
            }])->where('branch_id',$request['getuserbranch'])
                ->when($request['stats'], function ($query) use ($request, $activedata, $inactivedata) {
                    if ($request['stats'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } elseif ($request['stats'] == 1) {
                        $query->whereIn('id', $activedata);
                    } elseif ($request['stats'] == 2) {
                        $query->whereIn('id', $inactivedata);
                    } else {
                        $query->where('status', $request['stats']);
                    }
                })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'List', compact('team_routes'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $rules = [
                'tra' => 'required',
                'branch' => 'required',
                'dis_date' => 'required',
                'teams.*.team_name' => 'required|distinct',
                'teams.*.plate' => 'required|distinct',
                // 'teams.*.route' => 'required|distinct',
                'teams.*.driver' => 'required|distinct',
                'teams.*.check_1' => 'required|distinct',
                // 'teams.*.check_2' => 'sometimes',
                // 'teams.*.remarks' => 'sometimes',
            ];
            foreach ($request['teams'] as $i => $team) {
                // dd($request['teams'][$i]['check_2']);
                $rules['teams.' . $i . '.id'] = 'sometimes';
                // $rules['teams.' . $i . '.team_name'] = 'distinct';
                // $rules['teams.' . $i . '.plate'] = 'required';
                // $rules['teams.' . $i . '.plate'] = 'required';
                $rules['teams.' . $i . '.route'] = 'required';
                // $rules['teams.' . $i . '.driver'] = 'required';
                // $rules['teams.' . $i . '.check_1'] = 'required|distinct';
                $rules['teams.' . $i . '.check_2'] = 'sometimes';
                $rules['teams.' . $i . '.remarks'] = 'sometimes';
            }
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'tra.required' => 'Tra is Required.',
                    'branch.required' => 'Branch is Required.',
                    'dis_date.required' => 'Date is Required.',
                    'teams.*.team_name.required' => 'Team is required.',
                    'teams.*.team_name.distinct' => 'Team is already taken.',
                    'teams.*.plate.required' => 'Plate is required.',
                    'teams.*.plate.distinct' => 'Plate is already taken.',
                    'teams.*.route.required' => 'Route is required.',
                    // 'teams.*.route.distinct' => 'Route is already taken.',
                    'teams.*.driver.required' => 'Driver is required.',
                    'teams.*.driver.distinct' => 'Driver is already taken.',
                    'teams.*.check_1.required' => 'Checker is required.',
                    'teams.*.check_1.distinct' => 'Checker 1 is already taken.',
                    // 'teams.*.check_2.distinct' => 'Checker 2 is already taken.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($request);
            // dd($validated);

            $getbranchid = OimsBranchReference::where('name', $validated['branch'])->first();


            $teamRoute = OimsTeamRouteAssignment::create([
                'tra_no' => $validated['tra'],
                'branch_id' => $getbranchid->origin_destination_id,
                'created_user_id' => Auth::user()->id,
                'dispatch_date' => $validated['dis_date'],
            ]);

            foreach ($validated['teams'] as $i => $team) {
                $getvtype = OimsVehicle::where('id', $validated['teams'][$i]['plate'])->first();
                // dd($getvtype['vehicle_type']);

                $teamdet = OimsTeamRouteAssignmentDetails::create([
                    'team_route_id' => $teamRoute['id'],
                    'team_id' => $validated['teams'][$i]['team_name'],
                    'plate_no_id' => $validated['teams'][$i]['plate'],
                    'vehicle_type' => $getvtype['vehicle_type'],
                    'route_category_id' => $validated['teams'][$i]['route'],
                    'driver_id' => $validated['teams'][$i]['driver'],
                    'checker1_id' => $validated['teams'][$i]['check_1'],
                    'checker2_id' => ($validated['teams'][$i]['check_2'] != "" ? $validated['teams'][$i]['check_2'] : null),
                    'dispatch_date' => $validated['dis_date'],
                    'remarks' => $validated['teams'][$i]['remarks'],
                ]);
            }

            DB::commit();

            return $this->response(200, 'Team Route has been successfully added!', compact($teamRoute, $teamdet));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'tra' => 'required',
                'branch' => 'required',
                'dis_date' => 'required',
                'teams.*.team_name' => 'required|distinct',
                'teams.*.plate' => 'required|distinct',
                // 'teams.*.route' => 'required|distinct',
                'teams.*.driver' => 'required|distinct',
                'teams.*.check_1' => 'required|distinct',
            ];
            foreach ($request['teams'] as $i => $team) {
                $rules['teams.' . $i . '.id'] = 'sometimes';
                // $rules['teams.' . $i . '.team_name'] = 'required';
                // $rules['teams.' . $i . '.plate'] = 'required';
                $rules['teams.' . $i . '.route'] = 'required';
                // $rules['teams.' . $i . '.driver'] = 'required';
                // $rules['teams.' . $i . '.check_1'] = 'required';
                $rules['teams.' . $i . '.check_2'] = 'sometimes';
                $rules['teams.' . $i . '.remarks'] = 'sometimes';
                $rules['teams.' . $i . '.is_deleted'] = 'sometimes';
            }
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'tra.required' => 'Tra is Required.',
                    'branch.required' => 'Branch is Required.',
                    'dis_date.required' => 'Date is Required.',
                    'teams.*.team_name.required' => 'Team is required.',
                    'teams.*.team_name.distinct' => 'Team is already taken.',
                    'teams.*.plate.required' => 'Plate is required.',
                    'teams.*.plate.distinct' => 'Plate is already taken.',
                    'teams.*.route.required' => 'Route is required.',
                    // 'teams.*.route.distinct' => 'Route is already taken.',
                    'teams.*.driver.required' => 'Driver is required.',
                    'teams.*.driver.distinct' => 'Driver is already taken.',
                    'teams.*.check_1.required' => 'Checker is required.',
                    'teams.*.check_1.distinct' => 'Checker 1 is already taken.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $team_s = OimsTeamRouteAssignment::with('teamDetails', 'teamBranch')->findOrFail($id);
            if (!$team_s) {
                return $this->response(404, 'Team Route', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Team Route Management', $team_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $team_s = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);

            $getbranchid = OimsBranchReference::where('name', $validated['branch'])->first();

            $team_s->update([
                'tra_no' => $validated['tra'],
                'branch_id' => $getbranchid->origin_destination_id,
                'created_user_id' => Auth::user()->id,
                'dispatch_date' => $validated['dis_date'],
            ]);

            foreach ($validated['teams'] as $a => $team) {
                $getvtype = OimsVehicle::where('id', $validated['teams'][$a]['plate'])->first();


                if ($validated['teams'][$a]['id'] && $validated['teams'][$a]['is_deleted']) {
                    $teamroutes = OimsTeamRouteAssignmentDetails::find($validated['teams'][$a]['id']); //query all in database
                    if ($teamroutes) {
                        $teamroutes->delete();
                    }
                } else {
                    $teamroutes = OimsTeamRouteAssignmentDetails::updateOrCreate(
                        [
                            'id' => $validated['teams'][$a]['id'],
                        ],
                        [
                            'team_route_id' => $team_s['id'],
                            'team_id' => $validated['teams'][$a]['team_name'],
                            'plate_no_id' => $validated['teams'][$a]['plate'],
                            'vehicle_type' => $getvtype['vehicle_type'],
                            'route_category_id' => $validated['teams'][$a]['route'],
                            'driver_id' => $validated['teams'][$a]['driver'],
                            'checker1_id' => $validated['teams'][$a]['check_1'],
                            // 'checker2_id' => $validated['teams'][$a]['check_2'],
                            'dispatch_date' => $validated['dis_date'],
                            'checker2_id' => ($validated['teams'][$a]['check_2'] == "" ? null : $validated['teams'][$a]['check_2']),
                            'remarks' => ($validated['teams'][$a]['remarks'] == "" ? null : $validated['teams'][$a]['remarks']),

                        ]
                    );
                }
            }

            DB::commit();

            return $this->response(200, 'Team Route has been successfully updated!', compact($team_s, $teamroutes));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
