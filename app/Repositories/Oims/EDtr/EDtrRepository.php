<?php

namespace App\Repositories\Oims\EDtr;

use App\Interfaces\Oims\DispatchControlHub\DispatchControlHubInterface;
use App\Interfaces\Oims\EDtr\EDtrInterface;
use App\Models\CrmBooking;
use App\Models\DispatchControlHubReference;
use App\Models\Hrim\AreaReference;
use App\Models\OimsAreaReference;
use App\Models\OimsBranchReference;
use App\Models\OimsEdtr;
use App\Models\OimsEdtrDetails;
use App\Models\OimsTeamRouteAssignment;
use App\Models\OimsTeamRouteAssignmentDetails;
use App\Traits\ResponseTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class EDtrRepository implements EDtrInterface
{
    use ResponseTrait;

    public function index($request, $search_request)
    {
        // dd($search_request);

        DB::beginTransaction();
        try {
            $edtr_lists = OimsEdtr::with(
                'Traref',
                'cbref',
                'edtrBranch',
            )
                ->with(['edtrDetails' => function ($query) {
                    $query->with('edtrref', 'edtrbook');
                    
                    $query->withCount(['edtrbook as edtrcookstatus' => function ($query) {
                        $query->where('final_status_id', 5);
                    }]);
                }])
                ->withCount(['edtrDetails' => function ($query) {
                    // $query->withCount('edtrbook')->where('final_status_id', 3);
                    // $query->where();
                }])
                
                ->with(['Traref' => function ($query) use ($search_request) {
                    $query->with('teamIdReference', 'teamRouteAssignmentReference');
                    // $query->whereHas('teamIdReference', function ($query) use ($search_request) {
                        // $query->when($search_request['team'] ?? false, function ($query) use ($search_request) {
                        //     $query->where('team_id', $search_request['team']);
                        // });
                    // });
                }])
                ->when($search_request['refno'] ?? false, function ($query) use ($search_request) {
                    $query->where('reference_no', $search_request['refno']);
                })
                ->when($search_request['disdate'] ?? false, function ($query) use ($search_request) {
                    $query->where('dispatch_date', $search_request['disdate']);
                })
                ->when($search_request['brnch'] ?? false, function ($query) use ($search_request) {
                    $query->where('branch_id', $search_request['brnch']);
                })
                ->whereHas('Traref', function ($query) use ($search_request) {
                    $query->when($search_request['team'] ?? false, function ($query) use ($search_request) {
                        $query->where('team_id', $search_request['team']);
                    });
                    $query->when($search_request['driver'] ?? false, function ($query) use ($search_request) {
                        $query->where('driver_id', $search_request['driver']);
                    });
                    $query->when($search_request['chk'] ?? false, function ($query) use ($search_request) {
                        $query->where('checker1_id', $search_request['chk'])
                        ->orWhere('checker2_id', $search_request['chk']);
                    });
                    $query->when($search_request['plate'] ?? false, function ($query) use ($search_request) {
                        $query->where('plate_no_id', $search_request['plate']);
                    });
                })->groupBy('reference_no')->orderBy('dispatch_date')
                ->paginate($request['paginate']);
            DB::commit();
            // dd($edtr_lists);

            return $this->response(200, 'List', compact('edtr_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // public function dispatch($request)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $team_lists = OimsTeamRouteAssignmentDetails::with(
    //             'teamRouteReference',
    //             'teamPlateReference',
    //             'teamDriverReference',
    //             'teamChecker1Reference',
    //             'teamChecker2Reference'
    //         )->whereHas('teamIdReference', function ($query) use ($request) {
    //             $query->where('name', 'like', '%' . $request['tn'] . '%');
    //         })
    //             ->paginate($request['paginate']);
    //         DB::commit();
    //         return $this->response(200, 'List', compact('team_lists'));
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    // public function createValidation($request)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $rules = [
    //             'rdis' => 'required',
    //         ];
    //         $validator = Validator::make(
    //             $request,
    //             $rules,
    //             [
    //                 'rdis.required' => 'Select team to dispatch',
    //             ]
    //         );
    //         if ($validator->fails()) {
    //             return $this->response(400, 'Please Fill Required Field', $validator->errors());
    //         }

    //         DB::commit();

    //         return $this->response(200, 'Successfully Validated', $validator->validated());
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    // public function create($request)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $response = $this->createValidation($request);
    //         if ($response['code'] != 200) {
    //             return $this->response($response['code'], $response['message'], $response['result']);
    //         }
    //         $validated = $response['result'];

    //         $databranch = CrmBooking::with('BookingBranchReferenceBK')->where('booking_branch_id', $request['branch_id'])->first();
    //         $ref_no = $databranch->BookingBranchReferenceBK->code . '-' . strtoupper(uniqid());

    //         $datatrad = OimsTeamRouteAssignmentDetails::where('id', $request['rdis'])->first();
    //         $gettrid = $datatrad->team_route_id;

    //         $datatra = OimsTeamRouteAssignment::where('id', $gettrid)->first();


    //         $edtr = OimsEdtr::create([
    //             'reference_no' => $ref_no,
    //             'tra_id' => $datatra->id,
    //             'dispatch_date' => date('Y-m-d'),
    //             'branch_id' => $request['branch_id'],
    //             'status' => 2,
    //             'created_by' => Auth::user()->id,
    //         ]);

    //         $booking = [];

    //         foreach ($request['selecteds'] as $i => $select) {
    //             $booking = CrmBooking::with('BookingShipper')->where('id', $select)->first();
    //             $branchdata = OimsBranchReference::where('id', $request['branch_id'])->first();

    //             $curl = curl_init();
    //             curl_setopt_array($curl, array(
    //                 CURLOPT_URL => "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . urlencode($branchdata->address) .
    //                     "&destinations=" . urlencode($booking->BookingShipper->address) . "&mode=motorcycle&language=en-EN&sensor=false&key=" . env('GOOGLE_API_KEY'),
    //                 CURLOPT_RETURNTRANSFER => true,
    //                 CURLOPT_TIMEOUT => 30,
    //                 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //                 CURLOPT_CUSTOMREQUEST => "GET",
    //                 CURLOPT_HTTPHEADER => array(
    //                     "cache-control: no-cache"
    //                 ),
    //             ));
    //             $response = curl_exec($curl);
    //             $err = curl_error($curl);
    //             curl_close($curl);
    //             $res = json_decode($response, true);

    //             $durationInSeconds = $res['rows'][0]['elements'][0]['duration']['value']; // Replace with your actual duration
    //             $formattedTimestamp = date('Y-m-d H:i:s', strtotime("1970-01-01 +$durationInSeconds seconds")); 
    //             $formattedTimestampFinal = date('H:i:s', strtotime($formattedTimestamp)); 

    //             $edtrdet = OimsEdtrDetails::create([
    //                 'edtr_id' => $edtr->id,
    //                 'edtr_item_id' => $booking['id'],
    //                 'edtr_item_type' => 1,
    //                 'estimated_time_travel' => $formattedTimestampFinal,
    //                 'estimated_time_activity' => '00:15:00',
    //             ]);
    //         }

    //         DB::commit();

    //         // return $this->response(200, 'Area has been successfully added!', compact($edtr, $edtrdet));
    //         return $this->response(
    //             200,
    //             '',
    //             compact($edtr, $edtrdet,)
    //         );
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    // public function updateValidation($request, $id)
    // {
    //     DB::beginTransaction();
    //     try {

    //         $rules = [
    //             'name' => 'required',
    //         ];
    //         $validator = Validator::make(
    //             $request,
    //             $rules,
    //             [
    //                 'name.required' => 'Area field is Required.',
    //             ]
    //         );
    //         if ($validator->fails()) {
    //             return $this->response(400, 'Please Fill Required Field', $validator->errors());
    //         }

    //         DB::commit();

    //         return $this->response(200, 'Successfully Validated', $validator->validated());
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    // public function update($request, $id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $response = $this->show($id);
    //         if ($response['code'] != 200) {
    //             return $this->response($response['code'], $response['message'], $response['result']);
    //         }
    //         $area_s = $response['result'];

    //         $response = $this->updateValidation($request, $id);
    //         if ($response['code'] != 200) {
    //             return $this->response($response['code'], $response['message'], $response['result']);
    //         }
    //         $validated = $response['result'];

    //         $area_s->update([
    //             'name' => $validated['name'],
    //         ]);

    //         DB::commit();

    //         return $this->response(200, 'Area has been successfully updated!', compact($area_s));
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    // public function show($id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $area_s = OimsAreaReference::findOrFail($id);
    //         if (!$area_s) {
    //             return $this->response(404, 'Area', 'Not Found!');
    //         }

    //         DB::commit();

    //         return $this->response(200, 'Area Management', $area_s);
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }
}
