<?php

namespace App\Repositories\Oims\TransactionEntry;

use App\Interfaces\Oims\TransactionEntry\TransactionEntryInterface;
use App\Models\BranchReference;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmCustomerInformationAddressList;
use App\Models\Crm\CrmCustomerInformationContactPerson;
use App\Models\Crm\CrmCustomerInformationMobileList;
use App\Models\CrmBooking;
use App\Models\CrmBookingConsignee;
use App\Models\OimsTransactionEntry;
use App\Models\OimsTransactionEntryCustomerAddressList;
use App\Models\OimsTransactionEntryCustomerContPerson;
use App\Models\OimsTransactionEntryCustomerInfo;
use App\Models\OimsTransactionEntryCustomerMobileList;
use App\Models\OimsTransactionEntryItemDetails;
use App\Models\OimsTransactionEntryItemTypeReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\InitializeFirestoreTrait;
use Illuminate\Support\Facades\Auth;

class TransactionEntryRepository implements TransactionEntryInterface
{
    use ResponseTrait;

    // public function index($request, $search_request)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $zipcode_lists = OimsZipcodeReference::when($search_request['name'] ?? false, function ($query) use ($search_request) {
    //             $query->where('name', $search_request['name']);
    //         })->when($request['stats'], function ($query) use ($request) {
    //             if ($request['stats'] == false) {
    //                 $query->whereIn('status', [1, 2]);
    //             } else {
    //                 $query->where('status', $request['stats']);
    //             }
    //         })->paginate($request['paginate']);
    //         DB::commit();

    //         return $this->response(200, 'List', compact('zipcode_lists'));
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    public function selectedValidation($request)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'cons' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'cons.required' => 'Select atleast one.',

                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $rules = [
                'service' => 'sometimes',
                'waybill_no' => 'sometimes',
                'trans_type' => 'sometimes',
                'book_ref_no' => 'sometimes',
                'book_type' => 'sometimes',
                'origin' => 'sometimes',
                'trans_port' => 'sometimes',
                'branch' => 'sometimes',
                'trans_date' => 'sometimes',
                'transport_mode' => 'sometimes',
                'destination' => 'sometimes',
                'scust_no' => 'sometimes',
                'scname' => 'sometimes',
                'scont_person' => 'sometimes',
                'smobile' => 'sometimes',
                'sadrs' => 'sometimes',
                'ccust_no' => 'sometimes',
                'ccname' => 'sometimes',
                'ccont_person' => 'sometimes',
                'cmobile' => 'sometimes',
                'cadrs' => 'sometimes',
                'des_goods' => 'sometimes',
                'com_type' => 'sometimes',
                'types_goods' => 'sometimes',
                'dec_val' => 'sometimes',
                'paymode' => 'sometimes',
                'serv_mode' => 'sometimes',
                'charge_to' => 'sometimes',
                'com_app_rate' => 'sometimes',
                'weight_charge' => 'sometimes',
                'awb_fee' => 'sometimes',
                'valuation' => 'sometimes',
                'cod_charge' => 'sometimes',
                'insurance' => 'sometimes',
                'handling_fee' => 'sometimes',
                'doc_fee' => 'sometimes',
                'opa_fee' => 'sometimes',
                'oda_fee' => 'sometimes',
                'crating_fee' => 'sometimes',
                'equipment_rental' => 'sometimes',
                'lashing_fee' => 'sometimes',
                'manpower_fee' => 'sometimes',
                'dangerous_goods_fee' => 'sometimes',
                'trucking_fee' => 'sometimes',
                'perishable_fee' => 'sometimes',
                'packaging_fee' => 'sometimes',
                'subtotal' => 'sometimes',
                'rate_discount' => 'sometimes',
                'promo_code' => 'sometimes',
                'promo_discount' => 'sometimes',
                'promo_discount_amount' => 'sometimes',
                'acc_own' => 'sometimes',
                'prep_by' => 'sometimes',
                'cargo_stat' => 'sometimes',
                'coll_stat' => 'sometimes',
                'qc_by' => 'sometimes',
                'over_by' => 'sometimes',
                'qc_date_time' => 'sometimes',
                'date_time_overdn' => 'sometimes',
            ];
            // if ($request['air_cargo'] == true) {
            //     foreach ($request['air_cargos'] as $i => $air_cargo) {
            //         $rules['air_cargos.' . $i . '.id'] = 'required';
            //         $rules['air_cargos.' . $i . '.ac_qty'] = 'required';
            //         $rules['air_cargos.' . $i . '.ac_wt'] = 'required';
            //         $rules['air_cargos.' . $i . '.ac_dl'] = 'required';
            //         $rules['air_cargos.' . $i . '.ac_dw'] = 'required';
            //         $rules['air_cargos.' . $i . '.ac_dh'] = 'required';
            //         $rules['air_cargos.' . $i . '.ac_um'] = 'required';
            //         $rules['air_cargos.' . $i . '.ac_umt'] = 'required';
            //         $rules['air_cargos.' . $i . '.ac_tp'] = 'required';
            //         $rules['air_cargos.' . $i . '.ac_fc'] = 'sometimes';
            //         $rules['air_cargos.' . $i . '.ac_ct'] = 'sometimes';
            //         $rules['air_cargos.' . $i . '.ac_cwt'] = 'required';
            //     }
            // } else {
            foreach ($request['air_cargos'] as $i => $air_cargo) {
                $rules['air_cargos.' . $i . '.id'] = 'sometimes';
                $rules['air_cargos.' . $i . '.ac_qty'] = 'sometimes';
                $rules['air_cargos.' . $i . '.ac_wt'] = 'sometimes';
                $rules['air_cargos.' . $i . '.ac_dl'] = 'sometimes';
                $rules['air_cargos.' . $i . '.ac_dw'] = 'sometimes';
                $rules['air_cargos.' . $i . '.ac_dh'] = 'sometimes';
                $rules['air_cargos.' . $i . '.ac_um'] = 'sometimes';
                $rules['air_cargos.' . $i . '.ac_umt'] = 'sometimes';
                $rules['air_cargos.' . $i . '.ac_tp'] = 'sometimes';
                $rules['air_cargos.' . $i . '.ac_fc'] = 'sometimes';
                $rules['air_cargos.' . $i . '.ac_ct'] = 'sometimes';
                $rules['air_cargos.' . $i . '.ac_cwt'] = 'sometimes';
            }
            // }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'service.required' => 'Service is Required.',
                    'air_cargos.*.ac_qty.required' => 'Qty is required.',
                    // 'teams.*.check_2.distinct' => 'Checker 2 is already taken.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
        // dd($request, $validated);


            $getBooking = CrmBooking::with('BookShipper', 'BookingConsigneeHasManyBK')->where('booking_reference_no', $validated['book_ref_no'])->first();

            $getSCustomerInfo = CrmCustomerInformation::with('contactPersons', 'mobileNumbers', 'addresses')
                ->where('account_no', $validated['scust_no'])->first();

            $getSContactP = CrmCustomerInformationContactPerson::where('account_id', $getSCustomerInfo['id'])->first();

            $getSmobileNum = CrmCustomerInformationMobileList::where('mobile', $validated['smobile'])->first();
            $getSAddress = CrmCustomerInformationAddressList::where('account_id', $getSCustomerInfo['id'])
                ->where('state_id', $getBooking['BookShipper'][0]['state_id'])->where('city_id', $getBooking['BookShipper'][0]['city_id'])
                ->where('barangay_id', $getBooking['BookShipper'][0]['barangay_id'])->where('postal_id', $getBooking['BookShipper'][0]['postal_id'])->first();

            $getConsignee = CrmBookingConsignee::where('id', $request['ConsID'])->where('booking_id', $getBooking['id'])->first();
            $getCCustomerInfo = CrmCustomerInformation::with('contactPersons', 'mobileNumbers', 'addresses')
                ->where('account_no', $validated['ccust_no'])->first();
            $getCContactP = CrmCustomerInformationContactPerson::where('account_id', $getCCustomerInfo['id'])->first();
            $getCmobileNum = CrmCustomerInformationMobileList::where('mobile', $validated['cmobile'])->first();
            $getCAddress = CrmCustomerInformationAddressList::where('account_id', $getCCustomerInfo['id'])
                ->where('state_id', $getBooking['BookingConsigneeHasManyBK'][0]['state_id'])->where('city_id', $getBooking['BookingConsigneeHasManyBK'][0]['city_id'])
                ->where('barangay_id', $getBooking['BookingConsigneeHasManyBK'][0]['barangay_id'])->where('postal_id', $getBooking['BookingConsigneeHasManyBK'][0]['postal_id'])->first();
            $getBranchId = BranchReference::where('display', $validated['branch'])->first();


            $ShipperCustInfo = OimsTransactionEntryCustomerInfo::updateOrCreate(
                [
                    'account_no' => $validated['scust_no'],
                ],
                [
                    'account_type' => $getSCustomerInfo['account_type'],
                    'fullname' => $getSCustomerInfo['fullname'],
                    'company_name' => $getSCustomerInfo['company_name'] ?? null,
                    'first_name' => $getSCustomerInfo['first_name'],
                    'middle_name' => $getSCustomerInfo['middle_name'] ?? null,
                    'last_name' => $getSCustomerInfo['last_name'],
                ]
            );

            $ShipperContactP = OimsTransactionEntryCustomerContPerson::updateOrCreate(
                [
                    'account_id' => $ShipperCustInfo['id'],
                ],
                [
                    'first_name' => $getSContactP['first_name'],
                    'middle_name' => $getSContactP['middle_name'] ?? null,
                    'last_name' => $getSContactP['last_name'],
                    'position' => $getSContactP['position'],
                    'is_primary' => 1,
                ]
            );

            $ShipperAddressList = OimsTransactionEntryCustomerAddressList::updateOrCreate(
                [
                    'account_id' => $ShipperCustInfo['id'],
                ],
                [
                    'account_type' => $ShipperCustInfo['account_type'],
                    'address_line_1' => $getSAddress['address_line_1'],
                    'address_line_2' => $getSAddress['address_line_2'] ?? null,
                    'address_type' => $getSAddress['address_type'],
                    'address_label' => $getSAddress['address_label'],
                    'state_id' => $getSAddress['state_id'],
                    'city_id' => $getSAddress['city_id'],
                    'barangay_id' => $getSAddress['barangay_id'],
                    'country_id' => $getSAddress['country_id'] ?? null,
                    'postal_id' => $getSAddress['postal_id'],
                    'is_primary' => 1,
                    'contact_person_reference' => null,
                ]
            );
            // dd($ShipperCustInfo['account_type']);

            $ShipperMobileList = OimsTransactionEntryCustomerMobileList::updateOrCreate(
                [
                    'account_id' => $ShipperCustInfo['id'],
                ],
                [
                    'account_type' => $getSCustomerInfo['account_type'],
                    'mobile' => ($getSmobileNum == null ? $request['smobile'] : $getSmobileNum['mobile']),
                    'is_primary' => 1,
                ]
            );

            // =============================================================================================================================================
            $ConsigneeCustInfo = OimsTransactionEntryCustomerInfo::updateOrCreate(
                [
                    'account_no' => $validated['ccust_no'],
                ],
                [
                    'account_type' => $getCCustomerInfo['account_type'],
                    'fullname' => $getCCustomerInfo['fullname'],
                    'company_name' => $getCCustomerInfo['company_name'] ?? null,
                    'first_name' => $getCCustomerInfo['first_name'],
                    'middle_name' => $getCCustomerInfo['middle_name'] ?? null,
                    'last_name' => $getCCustomerInfo['last_name'],
                ]
            );

            $ConsigneeContactP = OimsTransactionEntryCustomerContPerson::updateOrCreate(
                [
                    'account_id' => $ConsigneeCustInfo['id'],
                ],
                [
                    'first_name' => $getCContactP['first_name'],
                    'middle_name' => $getCContactP['middle_name'] ?? null,
                    'last_name' => $getCContactP['last_name'],
                    'position' => $getCContactP['position'],
                    'is_primary' => 1,
                ]
            );

            $ConsigneeAddressList = OimsTransactionEntryCustomerAddressList::updateOrCreate(
                [
                    'account_id' => $ConsigneeCustInfo['id'],
                ],
                [
                    'account_type' => $getCAddress['account_type'],
                    'address_line_1' => $getCAddress['address_line_1'],
                    'address_line_2' => $getCAddress['address_line_2'] ?? null,
                    'address_type' => $getCAddress['address_type'],
                    'address_label' => $getCAddress['address_label'],
                    'state_id' => $getCAddress['state_id'],
                    'city_id' => $getCAddress['city_id'],
                    'barangay_id' => $getCAddress['barangay_id'],
                    'country_id' => $getCAddress['country_id'] ?? null,
                    'postal_id' => $getCAddress['postal_id'],
                    'is_primary' => 1,
                    'contact_person_reference' => null,
                ]
            );

            $ConsigneeMobileList = OimsTransactionEntryCustomerMobileList::updateOrCreate(
                [
                    'account_id' => $ConsigneeCustInfo['id'],
                ],
                [
                    'account_type' => $getCCustomerInfo['account_type'],
                    'mobile' => ($getCmobileNum == null ? $request['cmobile'] : $getCmobileNum['mobile']),
                    'is_primary' => 1,
                ]
            );

            // dd($request, $validated, $getCCustomerInfo);

 
            $transEnt = OimsTransactionEntry::create([
                'waybill' => $validated['waybill_no'],
                'scope_of_work' => 1,
                'transaction_date' => date('Y/m/d', strtotime($validated['trans_date'])),
                'transaction_type' => $validated['trans_type'],
                'booking_reference_id' => $getBooking['id'],
                'booking_type_id' => $validated['book_type'] ?? null,
                'branch_id' => $getBranchId['id'],
                'origin_id' => $validated['origin'],
                'destination_id' => $validated['destination'],
                'transhipment_id' => $validated['trans_port'] ?? null,
                'transport_mode_id' => $validated['transport_mode'],
                'shipper_id' => $ShipperCustInfo['id'],
                'shipper_contact_person_id' => $ShipperContactP['id'],
                'shipper_mobile_no_id' => $ShipperMobileList['id'],
                'shipper_address_id' => $ShipperAddressList['id'],
                'shipper_address' => $validated['sadrs'],
                'consignee__id' => $ConsigneeCustInfo['id'],
                'consignee_contact_person_id' => $ConsigneeContactP['id'],
                'consignee_mobile_no_id' => $ConsigneeMobileList['id'],
                'consignee_address_id' => $ConsigneeAddressList['id'],
                'consignee_address' => $validated['cadrs'],
                'batch_no' => 1,
                'pickup_notation' => 'test',
                'transType' => 1,
                'item_type' => null,
                'commodity_app_rate' => $validated['com_app_rate'],
                'commodity_type' => $validated['com_type'],
                'type_of_goods' => $validated['types_goods'] ?? null,
                'paymode' => $validated['paymode'],
                'service_mode' => $validated['serv_mode'],
                'charge_to_id' => ($getConsignee['account_type_id'] == 2 ? $validated['charge_to'] : null),
                'account_owner_id' => $validated['acc_own'] ?? null,
                'prepared_by_id' => $validated['prep_by'] ?? null,
                'qc_by_id' => $validated['qc_by'] ?? null,
                'overriden_by_id' => $validated['over_by'] ?? null,
                'qc_datetime' => $validated['qc_date_time'] ?? null,
                'override_datetime' => $validated['date_time_overdn'] ?? null,
                'collection_status' => $validated['coll_stat'] ?? null,
                'track_status' => 1,
            ]);


            if ($validated['transport_mode'] == 1) {

                foreach ($validated['air_cargos'] as $i => $air_cargo) {

                    if ($request['air_cargo'] == true) {
                        $transDet = OimsTransactionEntryItemDetails::create([
                            'waybill_id' => $transEnt->id,
                            'qty' => $validated['air_cargos'][$i]['ac_qty'],
                            'weight' => $validated['air_cargos'][$i]['ac_wt'],
                            'length' => $validated['air_cargos'][$i]['ac_dl'],
                            'width' => $validated['air_cargos'][$i]['ac_dw'],
                            'height' => $validated['air_cargos'][$i]['ac_dh'],
                            'unit_of_measurement_id' => $validated['air_cargos'][$i]['ac_um'],
                            'measurement_type_id' => $validated['air_cargos'][$i]['ac_umt'],
                            'type_of_packaging_id' => $validated['air_cargos'][$i]['ac_tp'],
                            'is_for_crating' => ($validated['air_cargos'][$i]['ac_fc'] == true ? 1 : 0),
                            'crating_status' => 0,
                            'crating_type_id' => $validated['air_cargos'][$i]['ac_ct'] ?? null,
                            'cwt' => $validated['air_cargos'][$i]['ac_cwt'] ?? null,
                            'cbm' => null,
                            'container' => null,
                            'rate_id_applied' => null,
                            'cargotype' => 1,
                        ]);
                    }

                    if ($request['air_pouch'] == true) {
                    }

                    if ($request['air_box'] == true) {
                    }
                }
            }

            if ($validated['transport_mode'] == 2) {
                if ($validated['sea_cargo'] == true) {
                }
                if ($validated['sea_box'] == true) {
                }
            }

            if ($validated['transport_mode'] == 3) {
                if ($validated['land'] == true) {
                }
            }


            DB::commit();

            // $transEntId = $transEnt->id;

            // $collectionReference = $this->initializeFirestore()->collection('oims_zipcode_reference'); 
            // $documentReference = $collectionReference->add();
            // $documentReference->set([
            //     'id' => $transEntId,
            //     'name' => $validated['name'],
            //     'status' => 1,
            // ]);

            return $this->response(200, 'Transaction Entry has been successfully added!', compact($transEnt, $transDet));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // public function updateValidation($request, $id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $rules = [
    //             'name' => 'required|unique:oims_zipcode_reference,name,' . $id,
    //         ];
    //         $validator = Validator::make(
    //             $request,
    //             $rules,
    //             [
    //                 'name.required' => 'Zipcode field is Required.',
    //                 'name.unique' => 'Zipcode is already taken',
    //             ]
    //         );
    //         if ($validator->fails()) {
    //             return $this->response(400, 'Please Fill Required Field', $validator->errors());
    //         }

    //         DB::commit();

    //         return $this->response(200, 'Successfully Validated', $validator->validated());
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    // public function show($id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $zipc = OimsZipcodeReference::findOrFail($id);
    //         if (!$zipc) {
    //             return $this->response(404, 'Zipcode', 'Not Found!');
    //         }

    //         DB::commit();

    //         return $this->response(200, 'Zipcode Management', $zipc);
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    // public function update($request, $id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $response = $this->show($id);
    //         if ($response['code'] != 200) {
    //             return $this->response($response['code'], $response['message'], $response['result']);
    //         }
    //         $zipc = $response['result'];

    //         $response = $this->updateValidation($request, $id);
    //         if ($response['code'] != 200) {
    //             return $this->response($response['code'], $response['message'], $response['result']);
    //         }
    //         $validated = $response['result'];

    //         $zipc->update([
    //             'name' => $validated['name'],
    //         ]);

    //         $collectionReference = $this->initializeFirestore()->collection('oims_zipcode_reference'); 

    //         $query = $collectionReference->where('id', '=', intval($id));
    //         $documents = $query->documents();

    //         foreach ($documents as $document) {
    //             if ($document->exists()) {
    //                 $documentId = $document->id();
    //                 $collectionReference->document($documentId)->update([
    //                     ['path' => 'name', 'value' => $validated['name']]
    //                 ]);
    //             }
    //         }

    //         DB::commit();

    //         return $this->response(200, 'Zipcode has been successfully updated!', compact($zipc));
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    public function destroy($id, $condition)
    {
        // dd($condition);
        DB::beginTransaction();
        try {
            $remove = CrmBookingConsignee::find($id);
            $removedet = CrmBookingCargoDetails::where('consignee_id', $id)->delete();

            //get cons id -> get booking id -> -1 cons count
            $cons = CrmBookingConsignee::where('id', $id)->first();
            $getBookingId = $cons->booking_id;
            $getBookingCount = CrmBooking::where('id', $getBookingId)->first();
            CrmBooking::where('id', $getBookingId)->decrement('consignee_count', 1);

            if (!$remove) return $this->response(404, 'Consignee', 'Not Found!');

            if ($condition == 'API') {
                $remove->delete();
            } else {
                $remove->each->delete();
            }

            DB::commit();
            return $this->response(200, 'Consignee has been successfully deleted!', compact($removedet, $remove));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

}
