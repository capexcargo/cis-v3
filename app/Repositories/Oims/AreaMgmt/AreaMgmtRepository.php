<?php

namespace App\Repositories\Oims\AreaMgmt;

use App\Interfaces\Oims\AreaMgmt\AreaMgmtInterface;
use App\Models\AreaMgmtReference;
use App\Models\Hrim\AreaReference;
use App\Models\OimsAreaReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AreaMgmtRepository implements AreaMgmtInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $area_names = OimsAreaReference::when($request['stats'], function ($query) use ($request) {
                if ($request['stats'] == false) {
                    $query->whereIn('status', [1,2]);
                } else {
                    $query->where('status', $request['stats']);
                }
            })->paginate($request['paginate']);
            DB::commit();
            // dd($area_names);

            return $this->response(200, 'List', compact('area_names'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Area field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $areas = OimsAreaReference::create([
                'name' => $validated['name'],
                'quadrant_id' => null,
                'status' => 1,
            ]);

            DB::commit();

            return $this->response(200, 'Area has been successfully added!', compact($areas));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Area field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $area_s = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $area_s->update([
                'name' => $validated['name'],
            ]);

            DB::commit();

            return $this->response(200, 'Area has been successfully updated!', compact($area_s));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $area_s = OimsAreaReference::findOrFail($id);
            if (!$area_s) {
                return $this->response(404, 'Area', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Area Management', $area_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
