<?php

namespace App\Repositories\Oims\QuadrantMgmt;

use App\Interfaces\Oims\QuadrantMgmt\QuadrantMgmtInterface;
use App\Models\Crm\BranchReferencesQuadrant;
use App\Models\QuadrantMgmtReference;
use App\Models\Hrim\AreaReference;
use App\Models\OimsAreaReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class QuadrantMgmtRepository implements QuadrantMgmtInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $quadrant_lists = BranchReferencesQuadrant::with('AreaReference')->when($request['stats'], function ($query) use ($request) {
                if ($request['stats'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['stats']);
                }
            })->paginate($request['paginate']);
            DB::commit();
            // dd($quadrant_lists);

            return $this->response(200, 'List', compact('quadrant_lists'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
            ];
            foreach ($request['areatags'] as $i => $areatag) {
                $rules['areatags.' . $i . '.id'] = 'sometimes';
                $rules['areatags.' . $i . '.area'] = 'required';
            }
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Quadrant Name field is Required.',
                    'areatags.*.area.required' => 'The Area field is required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $quad = BranchReferencesQuadrant::create([
                'name' => $validated['name'],
                'branch_reference_id' => 3,
                'quadrant' => 1,
                'status' => 1,
            ]);

            foreach ($validated['areatags'] as $i => $areatag) {
                $quadtag = OimsAreaReference::where('id', $validated['areatags'][$i]['area'])->update([
                    'quadrant_id' => $quad->id,
                ]);
            }

            DB::commit();

            return $this->response(200, 'Quadrant has been successfully added!', compact($quad, $quadtag));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required',
            ];
            foreach ($request['areatags'] as $i => $areatag) {
                $rules['areatags.' . $i . '.id'] = 'sometimes';
                $rules['areatags.' . $i . '.area'] = 'required';
                $rules['areatags.' . $i . '.is_deleted'] = 'sometimes';
            }
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'Quadrant Name field is Required.',
                    'areatags.*.area.required' => 'The Area field is required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $quadrant_s = BranchReferencesQuadrant::with('AreaReference')->findOrFail($id);
            if (!$quadrant_s) {
                return $this->response(404, 'Quadrant', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Quadrant Management', $quadrant_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $quadrant_s = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];




            $quadrant_s->update([
                'name' => $validated['name'],
            ]);

            // foreach ($validated['areatags'] as $a => $areatag) {

            //     if ($validated['areatags'][$a]['id'] && $validated['areatags'][$a]['is_deleted']) {
            //         $quads = OimsAreaReference::find($validated['areatags'][$a]['id']); //query all in database
            //         if ($quads) {
            //             $quads->update(
            //                 [
            //                     'quadrant_id' => null,
            //                 ]
            //             );
            //         }
            //     } else {
            //         $quads = OimsAreaReference::updateOrCreate(
            //             [
            //                 'id' => $validated['areatags'][$a]['id'],
            //             ],
            //             [
            //                 'quadrant_id' => $quadrant_s->id,
            //                 'area_id' => $validated['areatags'][$a]['area'],
            //             ]
            //         );
            //     }
            // }

            foreach ($validated['areatags'] as $a => $areatag) {

                if ($validated['areatags'][$a]['id'] && $validated['areatags'][$a]['is_deleted']) {
                    $quads = OimsAreaReference::find($validated['areatags'][$a]['id']); //query all in database
                    if ($quads) {
                        $quads->update(
                            [
                                'quadrant_id' => null,
                            ]
                        );
                    }
                } else {
                    $quads = OimsAreaReference::where('id', $validated['areatags'][$a]['area'])->update(
                        [
                            'id' => $validated['areatags'][$a]['area'],
                            'quadrant_id' => $quadrant_s->id,
                        ]
                    );
                }
            }

            DB::commit();

            return $this->response(200, 'Quadrant has been successfully updated!', compact($quadrant_s, $quads));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
