<?php

namespace App\Repositories\Crm\Activities;

use App\Interfaces\Crm\Activities\NotesInterface;
use App\Interfaces\Crm\Activities\TaskInterface;
use App\Models\Crm\CrmActivities;
use App\Models\Crm\CrmActivitiesNotes;
use App\Models\Crm\CrmActivitiesTask;
use App\Models\Crm\CrmCustomerInformation;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class NotesRepository implements NotesInterface
{
    use ResponseTrait;

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            // dd($request);
            $validator = Validator::make($request, [
                'customer_id' => 'required',
                'title' => 'required',
                'notes' => 'required',
                'sr_no' => 'required',
            ],[
                'customer_id.required' => 'The Customer Name field is required',
                'title.required' => 'The Title field is required',
                'notes.required' => 'The Note field is required',
                'sr_no.required' => 'The Associate With field is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }

        // DB::beginTransaction();
        // try {
        //     $validator = Validator::make($request, [
        //         'customer_id' => 'required',
        //         'title' => 'required',
        //         'note' => 'required',
        //         'sr_no' => 'sometimes',
        //     ]);

        //     if ($validator->fails()) {
        //         return $this->response(400, 'Please Fill Required Field', $validator->errors());
        //     }

        //     DB::commit();

        //     return $this->response(200, 'Successfully Validated', $validator->validated());
        // } catch (\Exception $e) {
        //     DB::rollback();

        //     return $this->response(500, 'Something Went Wrong', $e->getMessage());
        // }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $name = CrmCustomerInformation::where('fullname', $validated['customer_id'])->first();
            // dd($name->id);


            if ($name) {
                $notes = CrmActivitiesNotes::create([
                    'customer_id' => $name->id,
                    'title' => $validated['title'],
                    'note' => $validated['notes'],
                    'sr_no' => $validated['sr_no'] ?? 0,
                    'status' => 0,
                ]);


                $activities = CrmActivities::create([
                    'employee_id' => Auth::user()->id,
                    'activity_type_id' => 2,
                    'account_id' => $name->id,
                    'description' => $validated['notes'],
                    'sr_no' => $validated['sr_no'],
                    'status' => 0,
                ]);
            // dd('$s_box');

            }


            // dd($s_box);
            DB::commit();

            return $this->response(200, 'A note been successfully created!', compact($notes, $activities));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
