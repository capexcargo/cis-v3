<?php

namespace App\Repositories\Crm\Activities;

use App\Interfaces\Crm\Activities\LogMeetingInterface;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\CrmActivityLogMeeting;
use App\Models\CrmActivityLogMeetingAttendees;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class LogMeetingRepository implements LogMeetingInterface
{
    use ResponseTrait;

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'customer_id_meeting' => 'required',
                'attendees' => 'sometimes',
                'outcome' => 'required',
                'datetime' => 'required',
                'time' => 'required',
                'notes' => 'required',
                'sr_no' => 'required',
            ],[
                'customer_id_meeting.required' => 'The Customer Name field is required',
                'outcome.required' => 'The Outcome field is required',
                'datetime.required' => 'The Date field is required',
                'time.required' => 'The Time field is required',
                'notes.required' => 'The Notes on the Meeting field is required',
                'sr_no.required' => 'The Associate With field is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];


            $name = CrmCustomerInformation::where('fullname', $validated['customer_id_meeting'])->first();

            if ($name) {
                $meet = CrmActivityLogMeeting::create([
                    'customer_id' => $name->id,
                    'outcome_id' => $validated['outcome'],
                    'datetime' => $validated['datetime'],
                    'meeting_time' => $validated['time'],
                    'notes' => $validated['notes'],
                    'sr_no' => $validated['sr_no'] ?? 0,
                ]);

                $attend = CrmActivityLogMeetingAttendees::create([
                    'attendees_id' => Auth::user()->id,
                    'meeting_id' => $meet->id,
                ]);
            }


            // dd($s_box);
            DB::commit();

            return $this->response(200, 'New Meeting Log been Successfully Created!', compact($meet, $attend));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
