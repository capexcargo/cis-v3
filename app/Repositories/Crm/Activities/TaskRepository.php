<?php

namespace App\Repositories\Crm\Activities;

use App\Interfaces\Crm\Activities\TaskInterface;
use App\Models\Crm\CrmActivities;
use App\Models\Crm\CrmActivitiesTask;
use App\Models\Crm\CrmCustomerInformation;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class TaskRepository implements TaskInterface
{
    use ResponseTrait;

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'customer_id' => 'required',
                'title' => 'required',
                'priority' => 'required',
                'due_date' => 'required',
                'task' => 'required',
                'sr_no' => 'required',
            ],[
                'customer_id.required' => 'The Customer Name field is required',
                'title.required' => 'The Title field is required',
                'priority.required' => 'The Priority field is required',
                'due_date.required' => 'The Due Date and Time field is required',
                'task.required' => 'The Task field is required',
                'sr_no.required' => 'The Associate With field is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];


            $name = CrmCustomerInformation::where('fullname', $validated['customer_id'])->first();

            if ($name) {
                $task = CrmActivitiesTask::create([
                    'customer_id' => $name->id,
                    'title' => $validated['title'],
                    'priority' => $validated['priority'],
                    'due_date' => $validated['due_date'],
                    'task' => $validated['task'],
                    'sr_no' => $validated['sr_no'] ?? 0,
                    'status' => 0,
                ]);

                $activities = CrmActivities::create([
                    'employee_id' => Auth::user()->id,
                    'activity_type_id' => 1,
                    'account_id' => $name->id,
                    'description' => $validated['task'],
                    'sr_no' => $validated['sr_no'],
                    'status' => 0,
                ]);
            }


            // dd($s_box);
            DB::commit();

            return $this->response(200, 'A task has been successfully created!', compact($task, $activities));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
