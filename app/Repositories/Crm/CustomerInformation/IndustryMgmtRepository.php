<?php

namespace App\Repositories\Crm\CustomerInformation;

use App\Interfaces\Crm\CustomerInformation\IndustryMgmtInterface;
use App\Models\Industry;
use App\Models\MarketingChannel;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class IndustryMgmtRepository implements IndustryMgmtInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $industry_mgmts = Industry::orderBy('name', 'asc')->paginate($request['paginate']);

            // dd($industry_mgmtss);


            DB::commit();

            return $this->response(200, 'Industry List', compact('industry_mgmts'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'industry' => 'required|unique:crm_industry,name',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'industry.unique' => 'The industry channel already exist.',
                    'industry.required' => 'The industry channel field is Required.',
                ]
            );

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            // dd($response);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $conv_ind = ucwords( $validated['industry']);

            $industry_mgmt = Industry::create([
                'name' => $conv_ind,
            ]);

            DB::commit();

            return $this->response(200, 'Industry has been Successfully Created!', $industry_mgmt);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $industry_result = Industry::findOrFail($id);
            if (!$industry_result) {
                return $this->response(404, 'Industry', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Industry', $industry_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'industry' => 'required|unique:crm_industry,name,' . $id,
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'industry.unique' => 'The industry channel already exist.',
                    'industry.required' => 'The industry channel field is Required.',
                ]
            );

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $industry_result = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $conv_ind = strtolower($validated['industry']);

            $industry_result->update([
                'name' => $conv_ind,
            ]);

            DB::commit();

            return $this->response(200, 'Industry has been successfully updated!', $industry_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }




    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $remove = Industry::find($id);
            if (!$remove) return $this->response(404, 'Industry', 'Not Found!');
            $remove->delete();

            DB::commit();
            return $this->response(200, 'Industry has been successfully deleted!', $remove);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
