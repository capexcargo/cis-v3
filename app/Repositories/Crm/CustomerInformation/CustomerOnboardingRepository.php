<?php

namespace App\Repositories\Crm\CustomerInformation;

use App\Interfaces\Crm\CustomerInformation\CustomerOnboardingInterface;
use App\Mail\CustomerInformation\HTMLmail;
use App\Models\Checkmobitest;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmCustomerInformationAddressList;
use App\Models\Crm\CrmCustomerInformationEmailAddressList;
use App\Models\Crm\CrmCustomerInformationMobileList;
use App\Models\Customer\OnboardingCustomerInformation;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
// use App\Mail\CustomerInformation\HTMLMail;
use App\Models\Crm\CustomerInformation\CrmCusInformationInitiatedCall;
use App\Traits\InitializeFirestoreTrait;
use Myckhel\CheckMobi\Facades\CheckMobi;
use Twilio\TwiML\VoiceResponse;

use Twilio\Rest\Client;

class CustomerOnboardingRepository implements CustomerOnboardingInterface
{
    use ResponseTrait, InitializeFirestoreTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $customer_informations = CrmCustomerInformation::with(
                'accountType',
                'industry',
                'lifeStage',
                'contactOwner',
                'marketingChannel',
                'statusRef',
                'emails',
                'mobileNumbers',
                'telephoneNumbers',
                'addresses'
            )
                ->when($request['account_type'] ?? false, function ($query) use ($request) {
                    $query->where('account_type', $request['account_type']);
                })
                ->when($request['customer_param_id'] ?? false, function ($query) use ($request) {
                    $query->where('id', $request['customer_param_id']);
                })
                ->when($request['name'] ?? false, function ($query) use ($request) {
                    $query->where('fullname', 'like', '%' . $request['name'] . '%');
                    // $query->where('fullname', $request['name']);
                })
                ->when($request['email'] ?? false, function ($query) use ($request) {
                    $query->whereHas('emails', function ($query) use ($request) {
                        $query->where('email', ($request['email'] ?? false))
                            ->where('is_primary', 1);
                    });
                })
                ->when($request['mobile'] ?? false, function ($query) use ($request) {
                    $query->whereHas('mobileNumbers', function ($query) use ($request) {
                        $query->where('mobile', ($request['mobile'] ?? false))
                            ->where('is_primary', 1);
                    });
                })
                ->when($request['status'] ?? false, function ($query) use ($request) {
                    $query->where('status', $request['status']);
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                // ->where('account_type', $request['account_type'] ?? 1)
                ->paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'Customer Informations List', compact('customer_informations'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function search($request)
    {
        DB::beginTransaction();
        try {
            $search_customer = CrmCustomerInformation::with('emails', 'mobileNumbers')
                ->when($request['name_search'] ?? false, function ($query) use ($request) {
                    $query->where('fullname', $request['name_search']);
                })
                ->whereHas('emails', function ($query) use ($request) {
                    $query->when($request['email_search'] ?? false, function ($query) use ($request) {
                        $query->where('email', $request['email_search']);
                    });
                })
                ->whereHas('mobileNumbers', function ($query) use ($request) {
                    $query->when($request['mobile_number_search'] ?? false, function ($query) use ($request) {
                        $query->where('mobile', $request['mobile_number_search']);
                    });
                })
                ->when($request['tin_search'] ?? false, function ($query) use ($request) {
                    $query->where('tin', $request['tin_search']);
                })
                ->get();
            DB::commit();

            return $this->response(200, 'Search', $search_customer);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            if ($request['account_type'] == 1) {
                $rules = [
                    'account_type' => 'required',
                    'customer_number' => 'required',
                    'first_name' => 'required',
                    'middle_name' => 'nullable',
                    'last_name' => 'required',

                    'life_stage' => 'required',
                    'contact_owner' => 'required',
                    'marketing_channel' => 'required',
                    'notes' => 'nullable',
                ];

                foreach ($request['emails'] as $i => $email) {
                    $rules['emails.' . $i . '.email'] = 'nullable|email|regex:/\./';
                    $rules['emails.' . $i . '.default'] = 'nullable';
                }
            } else {
                $rules = [
                    'account_type' => 'required',
                    'customer_number' => 'required',
                    'company_name' => 'required',
                    'company_industry' => 'required',
                    'company_website_soc_med' => 'nullable',
                    'rate_name' => 'nullable',
                    // 'tin' => 'required',
                    'tin' => 'required|unique:crm_customer_information,tin',
                    'company_anniversary' => 'required',
                    'is_mother_account' => 'required',

                    // 'con_per_first_name' => 'required',
                    // 'con_per_middle_name' => 'nullable',
                    // 'con_per_last_name' => 'required',

                    'con_per_life_stage' => 'required',
                    'con_per_contact_owner' => 'required',
                    'con_per_marketing_channel' => 'required',
                    'con_per_notes' => 'nullable',
                ];

                if ($request['is_mother_account'] == 1 || $request['is_mother_account'] == null) {
                    $rules['associate_with'] = 'nullable';
                } else {
                    $rules['associate_with'] = 'required';
                }


                foreach ($request['contact_persons'] as $i => $con_per) {
                    $rules['contact_persons.' . $i . '.first_name'] = 'required';
                    $rules['contact_persons.' . $i . '.middle_name'] = 'nullable';
                    $rules['contact_persons.' . $i . '.last_name'] = 'required';
                    $rules['contact_persons.' . $i . '.position'] = 'required';
                    $rules['contact_persons.' . $i . '.default'] = 'required';

                    foreach ($request['contact_persons'][$i]['emails'] as $x => $email) {
                        $rules['contact_persons.' . $i . '.emails.' . $x . '.email'] = 'required|unique:crm_email_address_list,email|email|regex:/\./';
                        $rules['contact_persons.' . $i . '.emails.' . $x . '.default'] = 'required';
                    }

                    foreach ($request['contact_persons'][$i]['telephone_numbers'] as $x => $email) {
                        $rules['contact_persons.' . $i . '.telephone_numbers.' . $x . '.telephone'] = 'nullable'; //|regex:/^[0-9]{10}$/';
                        $rules['contact_persons.' . $i . '.telephone_numbers.' . $x . '.default'] = 'required';
                    }

                    foreach ($request['contact_persons'][$i]['mobile_numbers'] as $x => $email) {
                        $rules['contact_persons.' . $i . '.mobile_numbers.' . $x . '.mobile'] = 'required|unique:crm_mobile_list,mobile';
                        $rules['contact_persons.' . $i . '.mobile_numbers.' . $x . '.default'] = 'required';
                    }


                    $rules['contact_persons.' . $i . '.default'] = 'required';
                }

                foreach ($request['emails'] as $i => $email) {
                    $rules['emails.' . $i . '.email'] = 'required|unique:crm_email_address_list,email|email|regex:/\./';
                    $rules['emails.' . $i . '.default'] = 'required';
                }
            }

            foreach ($request['telephone_numbers'] as $i => $telephone) {
                $rules['telephone_numbers.' . $i . '.telephone'] = 'nullable';
                $rules['telephone_numbers.' . $i . '.default'] = 'nullable';
            }

            foreach ($request['mobile_numbers'] as $i => $mobile) {
                // $rules['mobile_numbers.' . $i . '.mobile'] = 'required|unique:crm_mobile_list,mobile';
                $rules['mobile_numbers.' . $i . '.id'] = 'sometimes';
                $rules['mobile_numbers.' . $i . '.mobile'] = 'required';
                $rules['mobile_numbers.' . $i . '.default'] = 'required';
                $rules['mobile_numbers.' . $i . '.is_deleted'] = 'sometimes';
            }

            foreach ($request['addresses'] as $i => $address) {
                $rules['addresses.' . $i . '.address_type'] = 'required';
                if ($request['addresses'][$i]['address_type'] == 2) {
                    $rules['addresses.' . $i . '.country'] = 'required';
                }
                $rules['addresses.' . $i . '.address_line1'] = 'required';
                $rules['addresses.' . $i . '.address_line2'] = 'nullable';
                $rules['addresses.' . $i . '.state_province'] = 'required';
                $rules['addresses.' . $i . '.city_municipality'] = 'required';
                if ($request['addresses'][$i]['address_type'] == 1) {
                    $rules['addresses.' . $i . '.barangay'] = 'required';
                }
                $rules['addresses.' . $i . '.postal_code'] = 'required';
                $rules['addresses.' . $i . '.address_label'] = 'required';
                $rules['addresses.' . $i . '.default'] = 'required';
            }

            // dd($request, $rules);
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'is_mother_account.required' => 'Please choose account.',
                    'tin.unique' => 'This TIN (Tax Identification Number) already exists in our CDM.',

                    'emails.*.email.required' => 'The company email address field is required.',
                    'emails.*.email.email' => 'Please enter a valid email address.',
                    'emails.*.email.regex' => 'Please enter a valid email address.',
                    'emails.*.email.unique' => 'This email address is already exists in our CDM.',
                    // 'emails.*.email.ends_with' => 'Please enter a valid email address.',
                    // 'telephone_numbers.*.telephone.required' => 'The Telephone Number field is required.',
                    'mobile_numbers.*.mobile.required' => 'The Mobile Number field is required.',
                    'mobile_numbers.*.mobile.unique' => 'This mobile number is already exists in our CDM.',

                    'addresses.*.address_type.required' => 'The Address Type is required.',
                    'addresses.*.country.required' => 'The Country field is required.',
                    'addresses.*.address_line1.required' => 'The Address Line1 field is required.',
                    // 'addresses.*.address_line2.required' => 'The Address Line2 field is required.',
                    'addresses.*.state_province.required' => 'The State/Province field is required.',
                    'addresses.*.city_municipality.required' => 'The City/Municipality field is required.',
                    'addresses.*.barangay.required' => 'The Barangay field is required.',
                    'addresses.*.postal_code.required' => 'The Postal Code field is required.',
                    'addresses.*.address_label.required' => 'Select Address Label.',

                    // Contact Person
                    'contact_persons.*.first_name.required' => 'The First Name field is required.',
                    'contact_persons.*.last_name.required' => 'The Last Name field is required.',
                    'contact_persons.*.position.required' => 'The Position field is required.',

                    'contact_persons.*.emails.*.email.required' => 'The Email field is required.',
                    'contact_persons.*.emails.*.email.email' => 'Please enter a valid email address.',
                    'contact_persons.*.emails.*.email.regex' => 'Please enter a valid email address.',
                    'contact_persons.*.emails.*.email.unique' => 'This email address is already exists in our CDM.',

                    'contact_persons.*.mobile_numbers.*.mobile.required' => 'The Mobile Number field is required.',
                    'contact_persons.*.mobile_numbers.*.mobile.unique' => 'This mobile number is already exists in our CDM.',

                    'con_per_life_stage.required' => 'The Life Stage field is required.',
                    'con_per_contact_owner.required' => 'The Contact Owner field is required.',
                    'con_per_marketing_channel.required' => 'The Marketing Channel field is required.',
                ]
            );

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

        public function verifyNumber($request)
        {
            DB::beginTransaction();
            try {
                // $sid = env('TWILIO_ACCOUNT_SID');
                // $token = env('TWILIO_AUTH_TOKEN');
                $sid = 'AC486054f0331fef903ece0140427f3586'; // Setup this in env instead
                $token = '13332bff34a3e27822f0582b26d75d44'; // Setup this in env instead

                $my_twilio_no = '+17177694625';

                $to_number = $request['number'];

                $twilio = new Client($sid, $token);
                $verification = $twilio->account->calls->create(
                    $request['number'],
                    $my_twilio_no,
                    array(
                        "method" => "GET",
                        "statusCallback" => "https://3ae6-124-6-130-29.ngrok-free.app/api/webhooks/twilio",
                        "statusCallbackMethod" => "POST",
                        "url" => "http://demo.twilio.com/docs/voice.xml"
                    )
                );

                $initiated_call = [];
                $CallSid = $verification->sid;

                if ($verification) {
                    $initiated_call = CrmCusInformationInitiatedCall::updateOrCreate(
                        [
                            'customer_number' => $request['customer_number'],
                            'mobile_number' => $request['number'],
                            'CallSid' => $verification->sid,
                        ],
                        [
                            'customer_number' => $request['customer_number'],
                            'mobile_number' => $request['number'],
                            'CallSid' => $verification->sid,
                            'CallStatus' => $verification->status,
                        ]
                    );
                }

                DB::commit();

                return $this->response(200, 'Call Initiated', compact('initiated_call', 'verification', 'CallSid'));
            } catch (\Exception $e) {
                DB::rollback();

                return $this->response(500, 'Something Went Wrong', $e->getMessage());
            }
        }


    // public function missedcall($request)
    // {
    //     DB::beginTransaction();
    //     try {

    //         $ch = curl_init();
    //         curl_setopt($ch, CURLOPT_URL, 'https://api.checkmobi.com/v1/call');
    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //         curl_setopt($ch, CURLOPT_POST, 1);
    //         curl_setopt($ch, CURLOPT_POSTFIELDS, '{
    //             "from": "+639177075044",
    //             "to": "'.$request['number'].'",
    //             "platform": "web",
    //             "timeout": 1,
    //             "notification_callback": "'.env('CHECKMOBI_NOTIFICATION_CALLBACK').'", 
    //             "events": [
    //                 {"action": "speak", "text": "Hi"}
    //             ]
    //         }');

    public function missedcall($request)
    {
        DB::beginTransaction();
        try {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.checkmobi.com/v1/call');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, '{
                "from": "+639177075044",
                "to": "'.$request['number'].'",
                "platform": "web",
                "ring_timeout": 5,
                "notification_callback": "'.env('CHECKMOBI_NOTIFICATION_CALLBACK').'", 
                "events": [
                    {"action": "speak", "text": "Hi"}
                ]
            }');


    //         // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //         // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    //         $headers = array();
    //         $headers[] = 'Authorization: 0D19571F-6E7C-4ED4-AFFC-631365ED15E9';
    //         $headers[] = 'Content-Type: application/json';
    //         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
    //         $result = curl_exec($ch);
    //         if (curl_errno($ch)) {
    //             echo 'Error:' . curl_error($ch);
    //         }
    
    //         // dd($result);

    //         return $this->response(200, 'Call Initiated', compact('result'));

    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    // public function missedcallnotif($request)
    // {
    //     DB::beginTransaction();
    
    //     try {
    //         // dd($request['event']['body']['event']);


    //         $customer_onboarding = Checkmobitest::updateOrCreate(
    //             [
    //             'number' => $request['event']['to'],
    //             ],
    //             [
    //             'number' => $request['event']['to'],
    //             'des' => $request['event']['event'],
    //             ]
    //         );

    //         CrmCusInformationInitiatedCall::where('mobile_number', $customer_onboarding->number)->update(['CallStatus' => $customer_onboarding->des]);


    //         DB::commit();

    //         return $this->response(200, 'Call Event', compact('customer_onboarding'));
    //     } catch (\Exception $e) {
    //         DB::rollback();
    
    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    public function updateCallStatus($request)
    {
        DB::beginTransaction();
        try {

            // if ($request['callStatus'] === 'in-progress' || $request['callStatus'] === 'ringing') {
                $updated_status = CrmCusInformationInitiatedCall::where('CallSid', $request['callSid'])->update(['CallStatus' => $request['callStatus']]);
                // }
    
                $response = new VoiceResponse();
                $response->hangup();

            DB::commit();

            return $this->response(200, 'Call Status', $updated_status);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function retrieveCallStatus($request_callSid)
    {
        DB::beginTransaction();
        try {

            $retrieved_status = CrmCusInformationInitiatedCall::where('CallSid', $request_callSid)->value('CallStatus');

            DB::commit();

            return $this->response(200, 'Retrieved Call Status', $retrieved_status);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request, $is_customer = false)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $customer_onboarding = [];
            $mailData = [];

            $fullaname = '';
            $primary_email = '';

            //Firestore initialization
            $collectionCusInfoReference = $this->initializeFirestore()->collection('crm_customer_information'); 
            $documentCusInfoReference = $collectionCusInfoReference->add();

            $collectionContactPersonReference = $this->initializeFirestore()->collection('crm_contact_person'); 
            $documentContactPersonReference = $collectionContactPersonReference->add();

            $collectionEmailReference = $this->initializeFirestore()->collection('crm_email_address_list'); 
            $documentEmailReference = $collectionEmailReference->add();

            $collectionMobileReference = $this->initializeFirestore()->collection('crm_mobile_list'); 
            $documentMobileReference = $collectionMobileReference->add();

            $collectionAddressReference = $this->initializeFirestore()->collection('crm_address_list'); 
            $documentAddressReference = $collectionAddressReference->add();

            if (!$is_customer) {
                if ($validated['account_type'] == 1) {

                    $fullaname = $request['first_name'] . " " . $request['middle_name'] . " " . $request['last_name'];

                    $customer_onboarding = CrmCustomerInformation::create([
                        'account_type' => $validated['account_type'],
                        'account_no' => $validated['customer_number'],
                        'fullname' => $fullaname,
                        'first_name' => $validated['first_name'],
                        'middle_name' => $validated['middle_name'],
                        'last_name' => $validated['last_name'],
                        // 'life_stage_id' => $validated['life_stage'],
                        'life_stage_id' => 1,
                        'contact_owner_id' => $validated['contact_owner'],
                        'marketing_channel_id' => $validated['marketing_channel'],
                        'onboarding_id' => 1,
                        'notes' => $validated['notes'],
                        'created_by' => Auth::user()->id,
                    ]);

                    //Firestore customerinformation if
                    $documentCusInfoReference->set([
                        'id' => $customer_onboarding->id,
                        'account_type' => $validated['account_type'],
                        'account_no' => $validated['customer_number'],
                        'fullname' => $fullaname,
                        'first_name' => $validated['first_name'],
                        'middle_name' => $validated['middle_name'],
                        'last_name' => $validated['last_name'],
                        // 'life_stage_id' => $validated['life_stage'],
                        'life_stage_id' => 1,
                        'contact_owner_id' => $validated['contact_owner'],
                        'marketing_channel_id' => $validated['marketing_channel'],
                        'onboarding_id' => 1,
                        'notes' => $validated['notes'],
                        'created_by' => Auth::user()->id,
                    ]);


                    $contactPerson = $customer_onboarding->contactPersons()->create([
                        'first_name' => $validated['first_name'],
                        'middle_name' => $validated['middle_name'],
                        'last_name' => $validated['last_name'],
                        'position' => "N/A",
                        'is_primary' => true,
                    ]);

                    //firestore contact person if
                    $documentContactPersonReference->set([
                        'id' => $contactPerson['id'],
                        'account_id' => $customer_onboarding->id,
                        'first_name' => $validated['first_name'],
                        'middle_name' => $validated['middle_name'],
                        'last_name' => $validated['last_name'],
                        'position' => "N/A",
                        'is_primary' => true,
                    ]);
                } else {


                    $fullaname = $request['company_name'];

                    $customer_onboarding = CrmCustomerInformation::create([
                        'account_type' => $validated['account_type'],
                        'account_no' => $validated['customer_number'],
                        'company_name' => $validated['company_name'],
                        'fullname' => $validated['company_name'],
                        'company_link' => $validated['company_website_soc_med'],
                        'rate_name' => $validated['rate_name'],
                        'industry_id' => $validated['company_industry'],
                        'is_mother_account' => $validated['is_mother_account'],
                        'child_account_id' => $validated['associate_with'] ?? null,
                        'tin' => $validated['tin'],
                        'company_anniversary' => $validated['company_anniversary'],
                        'life_stage_id' => 1,
                        'contact_owner_id' => $validated['con_per_contact_owner'],
                        'marketing_channel_id' => $validated['con_per_marketing_channel'],
                        'onboarding_id' => 1,
                        'notes' => $validated['con_per_notes'],
                        'created_by' => Auth::user()->id,
                    ]);

                    //firesore contact info else
                    $documentCusInfoReference->set([
                        'id' => $customer_onboarding->id,
                        'account_type' => $validated['account_type'],
                        'account_no' => $validated['customer_number'],
                        'company_name' => $validated['company_name'],
                        'fullname' => $validated['company_name'],
                        'company_link' => $validated['company_website_soc_med'],
                        'rate_name' => $validated['rate_name'],
                        'industry_id' => $validated['company_industry'],
                        'is_mother_account' => $validated['is_mother_account'],
                        'child_account_id' => $validated['associate_with'] ?? null,
                        'tin' => $validated['tin'],
                        'company_anniversary' => $validated['company_anniversary'],
                        'life_stage_id' => 1,
                        'contact_owner_id' => $validated['con_per_contact_owner'],
                        'marketing_channel_id' => $validated['con_per_marketing_channel'],
                        'onboarding_id' => 1,
                        'notes' => $validated['con_per_notes'],
                        'created_by' => Auth::user()->id,
                    ]);

                    // Contact Person Details Entry
                    foreach ($validated['contact_persons'] as $i => $con_per) {
                        if ($con_per['default'] == 1) {
                            $customer_onboarding->update([
                                'first_name' => $con_per['first_name'],
                                'middle_name' => $con_per['middle_name'],
                                'last_name' => $con_per['last_name'],
                            ]);

                        //firesore contact info else
                            $documentCusInfoReference->set([
                                'first_name' => $con_per['first_name'],
                                'middle_name' => $con_per['middle_name'],
                                'last_name' => $con_per['last_name'],
                            ],[
                                'merge' => true
                            ]);
                        }
                        $contact_person = $customer_onboarding->contactPersons()->updateOrCreate(
                            [
                                'account_id' => $customer_onboarding->id,
                            ],
                            [
                                'first_name' => $con_per['first_name'],
                                'middle_name' => $con_per['middle_name'],
                                'last_name' => $con_per['last_name'],
                                'position' => $con_per['position'],
                                'is_primary' => $con_per['default'],
                            ]
                        );
                        $contact_person->save();
                        //firestore contact person
                        $documentContactPersonReference->set([
                            'id' => $contact_person['id'],
                            'account_id' => $customer_onboarding->id,
                            'first_name' => $con_per['first_name'],
                            'middle_name' => $con_per['middle_name'],
                            'last_name' => $con_per['last_name'],
                            'position' => $con_per['position'],
                            'is_primary' => $con_per['default'],
                        ]);
                        $contact_person_id = $contact_person->id;

                        foreach ($con_per['emails'] as $i => $email) {
                            $customer_onboarding_email = CrmCustomerInformationEmailAddressList::updateOrCreate(
                                [
                                    'account_type' => 3,
                                    // 'email' => $email['email'],
                                    'account_id' => $customer_onboarding->id,
                                    'is_primary' => $email['default'],
                                ],
                                [
                                    'contact_person_id' => $contact_person_id,
                                    'account_type' => 3,
                                    'email' => $email['email'],
                                    'is_primary' => $email['default'],
                                ]
                            );

                            //firestore email
                            $documentEmailReference->set([
                                'id' => $customer_onboarding_email->id,
                                'account_id' => $customer_onboarding_email->account_id,
                                'contact_person_id' => $contact_person_id,
                                'account_type' => 3,
                                'email' => $email['email'],
                                'is_primary' => $email['default'],
                            ]);
                        }

                        foreach ($con_per['mobile_numbers'] as $i => $mobile_number) {
                            $customer_onboarding_mobile = CrmCustomerInformationMobileList::updateOrCreate(
                                [
                                    'account_type' => 3,
                                    'account_id' => $customer_onboarding->id,
                                    // 'mobile' => $mobile_number['mobile'],
                                    'is_primary' => $mobile_number['default'],
                                ],
                                [
                                    'contact_person_id' => $contact_person_id,
                                    'account_type' => 3,
                                    'mobile' => $mobile_number['mobile'],
                                    'is_primary' => $mobile_number['default'],
                                ]
                            );

                            //firestore mobile
                            $documentMobileReference->set([
                                'id' => $customer_onboarding_mobile->id,
                                'account_id' => $customer_onboarding_mobile->account_id,
                                'contact_person_id' => $contact_person_id,
                                'account_type' => 3,
                                'mobile' => $mobile_number['mobile'],
                                'is_primary' => $mobile_number['default'],
                            ]);
                        }

                        foreach ($con_per['telephone_numbers'] as $i => $telephone_number) {
                            $customer_onboarding->telephoneNumbers()->updateOrCreate(
                                [
                                    'account_type' => 3,
                                    // 'telephone' => $telephone_number['telephone'],
                                    'is_primary' => $telephone_number['default'],
                                ],
                                [
                                    'contact_person_id' => $contact_person_id,
                                    'account_type' => 3,
                                    'telephone' => $telephone_number['telephone'],
                                    'is_primary' => $telephone_number['default'],
                                ]
                            );
                        }
                    }
                }

                foreach ($validated['emails'] as $i => $email) {
                    $customer_onboarding_email = CrmCustomerInformationEmailAddressList::updateOrCreate(
                        [
                            'account_type' => $validated['account_type'],
                            'account_id' => $customer_onboarding->id,
                            'email' => $email['email']
                        ],
                        [
                            'account_type' => $validated['account_type'],
                            'email' => $email['email'],
                            'is_primary' => $email['default'],
                        ]
                    );

                    if ($email['default']) {
                        $primary_email = $email['email'];
                    }

                    //firestore email
                    $documentEmailReference->set([
                        'id' => $customer_onboarding_email->id,
                        'account_id' => $customer_onboarding_email->account_id,
                        'account_type' => $validated['account_type'],
                        'email' => $email['email'],
                        'is_primary' => $email['default'],
                    ]);
                }

                foreach ($validated['mobile_numbers'] as $i => $mobile_number) {
                    $customer_onboarding_mobile = CrmCustomerInformationMobileList::updateOrCreate(
                        [
                            'account_type' => $validated['account_type'],
                            'account_id' => $customer_onboarding->id,
                            'mobile' => $mobile_number['mobile']
                        ],
                        [
                            'account_type' => $validated['account_type'],
                            'mobile' => $mobile_number['mobile'],
                            'is_primary' => $mobile_number['default'],
                        ]
                    );

                    //firestore mobile
                    $documentMobileReference->set([
                        'id' => $customer_onboarding_mobile->id,
                        'account_id' => $customer_onboarding_mobile->account_id,
                        'account_type' => $validated['account_type'],
                        'mobile' => $mobile_number['mobile'],
                        'is_primary' => $mobile_number['default'],
                    ]);
                }

                foreach ($validated['addresses'] as $i => $address) {
                    $customer_onboarding_address = CrmCustomerInformationAddressList::updateOrCreate(
                        [
                            'account_type' => $validated['account_type'],
                            'account_id' => $customer_onboarding->id,
                        ],
                        [
                            'account_type' => $validated['account_type'],
                            'address_type' => $address['address_type'],
                            'country_id' => $address['country'] ?? null,
                            'address_line_1' => $address['address_line1'],
                            'address_line_2' => $address['address_line2'],
                            'state_id' => intval($address['state_province']),
                            'city_id' => $address['city_municipality'],
                            'barangay_id' => $address['barangay'] ?? null,
                            'postal_id' => $address['postal_code'],
                            'address_label' => $address['address_label'],
                            'is_primary' => $address['default'],
                        ]
                    );

                    //firestore address
                    $documentAddressReference->set([
                        'id' => $customer_onboarding_address->id,
                        'account_id' => $customer_onboarding_address->account_id,
                        'account_type' => $validated['account_type'],
                        'address_type' => intval($address['address_type']),
                        'country_id' => $customer_onboarding_address->country_id,
                        'address_line_1' => $address['address_line1'],
                        'address_line_2' => $address['address_line2'],
                        'state_id' => intval($address['state_province']),
                        'city_id' => intval($address['city_municipality']),
                        'barangay_id' => intval($address['barangay']) ?? null,
                        'postal_id' => intval($address['postal_code']),
                        'address_label' => intval($address['address_label']),
                        'is_primary' => $address['default'],
                    ]);
                }

                foreach ($validated['telephone_numbers'] as $i => $telephone_number) {
                    $customer_onboarding->telephoneNumbers()->updateOrCreate(
                        [
                            'account_type' => $validated['account_type'],
                            'telephone' => $telephone_number['telephone']
                        ],
                        [
                            'account_type' => $validated['account_type'],
                            'telephone' => $telephone_number['telephone'],
                            'is_primary' => $telephone_number['default'],
                        ]
                    );
                }

                $customer_onboarding_id = $customer_onboarding->id;
            } else {
                if ($validated['account_type'] == 1) {
                    $fullaname = $request['first_name'] . " " . $request['middle_name'] . " " . $request['last_name'];

                    $customer_onboarding = OnboardingCustomerInformation::create([
                        'account_type' => $validated['account_type'],
                        'account_no' => $validated['customer_number'],
                        'fullname' => $validated['first_name'] . " " . $validated['middle_name'] . " " . $validated['last_name'],
                        'first_name' => $validated['first_name'],
                        'middle_name' => $validated['middle_name'],
                        'last_name' => $validated['last_name'],
                        'life_stage_id' => 1,
                        'contact_owner_id' => $validated['contact_owner'],
                        'marketing_channel_id' => $validated['marketing_channel'],
                        'onboarding_id' => 1,
                        'notes' => $validated['notes'],
                        'created_by' => 0,
                    ]);

                    //firestore customer info
                    $documentCusInfoReference->set([
                        'id' => $customer_onboarding->id,
                        'account_type' => $validated['account_type'],
                        'account_no' => $validated['customer_number'],
                        'fullname' => $validated['first_name'] . " " . $validated['middle_name'] . " " . $validated['last_name'],
                        'first_name' => $validated['first_name'],
                        'middle_name' => $validated['middle_name'],
                        'last_name' => $validated['last_name'],
                        'life_stage_id' => 1,
                        'contact_owner_id' => $validated['contact_owner'],
                        'marketing_channel_id' => $validated['marketing_channel'],
                        'onboarding_id' => 1,
                        'notes' => $validated['notes'],
                        'created_by' => 0,
                    ]);

                    $contactPerson = $customer_onboarding->contactPersons()->create([
                        'first_name' => $validated['first_name'],
                        'middle_name' => $validated['middle_name'],
                        'last_name' => $validated['last_name'],
                        'position' => "N/A",
                        'is_primary' => true,
                    ]);

                    //firestore contact
                    $documentContactPersonReference->set([
                        'id' => $contactPerson['id'],
                        'account_id' => $customer_onboarding->id,
                        'first_name' => $validated['first_name'],
                        'middle_name' => $validated['middle_name'],
                        'last_name' => $validated['last_name'],
                        'position' => "N/A",
                        'is_primary' => true,
                    ]);
                } else {
                    $fullaname = $request['company_name'];

                    $customer_onboarding = OnboardingCustomerInformation::create([
                        'account_type' => $validated['account_type'],
                        'account_no' => $validated['customer_number'],
                        'company_name' => $validated['company_name'],
                        'fullname' => $validated['company_name'],
                        'company_link' => $validated['company_website_soc_med'],
                        'rate_name' => $validated['rate_name'],
                        'industry_id' => $validated['company_industry'],
                        'is_mother_account' => $validated['is_mother_account'],
                        'child_account_id' => $validated['associate_with'] ?? null,
                        'tin' => $validated['tin'],
                        'company_anniversary' => $validated['company_anniversary'],
                        'life_stage_id' => 1,
                        'contact_owner_id' => $validated['con_per_contact_owner'],
                        'marketing_channel_id' => $validated['con_per_marketing_channel'],
                        'onboarding_id' => 1,
                        'notes' => $validated['con_per_notes'],
                        'created_by' => 0,
                    ]);

                    //firestore customer info
                    $documentCusInfoReference->set([
                        'id' => $customer_onboarding->id,
                        'account_type' => $validated['account_type'],
                        'account_no' => $validated['customer_number'],
                        'company_name' => $validated['company_name'],
                        'fullname' => $validated['company_name'],
                        'company_link' => $validated['company_website_soc_med'],
                        'rate_name' => $validated['rate_name'],
                        'industry_id' => $validated['company_industry'],
                        'is_mother_account' => $validated['is_mother_account'],
                        'child_account_id' => $validated['associate_with'] ?? null,
                        'tin' => $validated['tin'],
                        'company_anniversary' => $validated['company_anniversary'],
                        'life_stage_id' => 1,
                        'contact_owner_id' => $validated['con_per_contact_owner'],
                        'marketing_channel_id' => $validated['con_per_marketing_channel'],
                        'onboarding_id' => 1,
                        'notes' => $validated['con_per_notes'],
                        'created_by' => 0,
                    ]);

                    // Contact Person Details Entry
                    foreach ($validated['contact_persons'] as $i => $con_per) {
                        $contact_person = $customer_onboarding->contactPersons()->updateOrCreate(
                            [
                                'account_id' => $customer_onboarding->id,
                            ],
                            [
                                'first_name' => $con_per['first_name'],
                                'middle_name' => $con_per['middle_name'],
                                'last_name' => $con_per['last_name'],
                                'position' => $con_per['position'],
                                'is_primary' => $con_per['default'],
                            ]
                        );
                        $contact_person->save();

                        //firestore contact person
                        $documentContactPersonReference->set([
                            'id' => $contact_person['id'],
                            'account_id' => $customer_onboarding->id,
                            'first_name' => $con_per['first_name'],
                            'middle_name' => $con_per['middle_name'],
                            'last_name' => $con_per['last_name'],
                            'position' => $con_per['position'],
                            'is_primary' => $con_per['default'],
                        ]);
                        $contact_person_id = $contact_person->id;

                        foreach ($con_per['emails'] as $i => $email) {
                            $customer_onboarding_email = CrmCustomerInformationEmailAddressList::updateOrCreate(
                                [
                                    'account_type' => 3,
                                    'account_id' => $customer_onboarding->id,
                                    // 'email' => $email['email'],
                                    'is_primary' => $email['default'],
                                ],
                                [
                                    'contact_person_id' => $contact_person_id,
                                    'account_type' => 3,
                                    'email' => $email['email'],
                                    'is_primary' => $email['default'],
                                ]
                            );

                            //firestore email
                            $documentEmailReference->set([
                                'id' => $customer_onboarding_email->id,
                                'account_id' => $customer_onboarding_email->account_id,
                                'contact_person_id' => $contact_person_id,
                                'account_type' => 3,
                                'email' => $email['email'],
                                'is_primary' => $email['default'],
                            ]);
                        }

                        foreach ($con_per['mobile_numbers'] as $i => $mobile_number) {
                            $customer_onboarding->mobileNumbers()->updateOrCreate(
                                [
                                    'account_type' => 3,
                                    // 'mobile' => $mobile_number['mobile'],
                                    'is_primary' => $mobile_number['default'],
                                ],
                                [
                                    'contact_person_id' => $contact_person_id,
                                    'account_type' => 3,
                                    'mobile' => $mobile_number['mobile'],
                                    'is_primary' => $mobile_number['default'],
                                ]
                            );

                            //firestore mobile
                            $documentMobileReference->set([
                                'id' => $mobile_number['id'],
                                'account_id' => $customer_onboarding->id,
                                'contact_person_id' => $contact_person_id,
                                'account_type' => 3,
                                'mobile' => $mobile_number['mobile'],
                                'is_primary' => $mobile_number['default'],
                            ]);
                        }

                        foreach ($con_per['telephone_numbers'] as $i => $telephone_number) {
                            $customer_onboarding->telephoneNumbers()->updateOrCreate(
                                [
                                    'account_type' => 3,
                                    // 'telephone' => $telephone_number['telephone'],
                                    'is_primary' => $telephone_number['default'],
                                ],
                                [
                                    'contact_person_id' => $contact_person_id,
                                    'account_type' => 3,
                                    'telephone' => $telephone_number['telephone'],
                                    'is_primary' => $telephone_number['default'],
                                ]
                            );
                        }
                    }
                }

                foreach ($validated['emails'] as $i => $email) {
                    $customer_onboarding_email = CrmCustomerInformationEmailAddressList::updateOrCreate(
                        [
                            'account_type' => $validated['account_type'],
                            'account_id' => $customer_onboarding->id,
                            'email' => $email['email']
                        ],
                        [
                            'account_type' => $validated['account_type'],
                            'email' => $email['email'],
                            'is_primary' => $email['default'],
                        ]
                    );

                    if ($email['default']) {
                        $primary_email = $email['email'];
                    }

                    //firestore email
                    $documentEmailReference->set([
                        'id' => $customer_onboarding_email->id,
                        'account_id' => $customer_onboarding_email->account_id,
                        'account_type' => $validated['account_type'],
                        'email' => $email['email'],
                        'is_primary' => $email['default'],
                    ]);
                }

                foreach ($validated['mobile_numbers'] as $i => $mobile_number) {
                    $customer_onboarding_mobile = CrmCustomerInformationMobileList::updateOrCreate(
                        [
                            'account_type' => $validated['account_type'],
                            'account_id' => $customer_onboarding->id,
                            'mobile' => $mobile_number['mobile']
                        ],
                        [
                            'account_type' => $validated['account_type'],
                            'mobile' => $mobile_number['mobile'],
                            'is_primary' => $mobile_number['default'],
                        ]
                    );

                    //firestore mobile
                    $documentMobileReference->set([
                        'id' => $customer_onboarding_mobile->id,
                        'account_id' => $customer_onboarding_mobile->account_id,
                        'account_type' => $validated['account_type'],
                        'mobile' => $mobile_number['mobile'],
                        'is_primary' => $mobile_number['default'],
                    ]);
                }

                foreach ($validated['addresses'] as $i => $address) {
                    $customer_onboarding_address = CrmCustomerInformationAddressList::updateOrCreate(
                        [
                            'account_type' => $validated['account_type'],
                            'account_id' => $customer_onboarding->id,
                        ],
                        [
                            'account_type' => $validated['account_type'],
                            'address_type' => $address['address_type'],
                            'country_id' => $address['country'] ?? null,
                            'address_line_1' => $address['address_line1'],
                            'address_line_2' => $address['address_line2'],
                            'state_id' => intval($address['state_province']),
                            'city_id' => $address['city_municipality'],
                            'barangay_id' => $address['barangay'] ?? null,
                            'postal_id' => $address['postal_code'],
                            'address_label' => $address['address_label'],
                            'is_primary' => $address['default'],
                        ]
                    );

                    //firestore address
                    $documentAddressReference->set([
                        'id' => $customer_onboarding_address->id,
                        'account_id' => $customer_onboarding_address->account_id,
                        'account_type' => $validated['account_type'],
                        'address_type' => intval($address['address_type']),
                        'country_id' => $customer_onboarding_address->country_id,
                        'address_line_1' => $address['address_line1'],
                        'address_line_2' => $address['address_line2'],
                        'state_id' => intval($address['state_province']),
                        'city_id' => intval($address['city_municipality']),
                        'barangay_id' => intval($address['barangay']) ?? null,
                        'postal_id' => intval($address['postal_code']),
                        'address_label' => intval($address['address_label']),
                        'is_primary' => $address['default'],
                    ]);
                }

                foreach ($validated['telephone_numbers'] as $i => $telephone_number) {
                    $customer_onboarding->telephoneNumbers()->updateOrCreate(
                        [
                            'account_type' => $validated['account_type'],
                            'telephone' => $telephone_number['telephone']
                        ],
                        [
                            'account_type' => $validated['account_type'],
                            'telephone' => $telephone_number['telephone'],
                            'is_primary' => $telephone_number['default'],
                        ]
                    );
                }

                $customer_onboarding_id = $customer_onboarding->id;
            }

            $mailData['name'] = $fullaname;
            $mailData['to'] = $primary_email;
            $mailData['from'] = 'yarnsupt1.capex@gmail.com'; // must change based on system email
            $mailData['subject'] = 'Welcome Aboard to CaPEx';
            $mailData['welcome_name'] = $fullaname;

            $onboarding_welcome_email = Mail::to($mailData['to'])->send(new HTMLmail($mailData));


            DB::commit();

            return $this->response(
                200,
                'Customer has been successfully onboarded!',
                compact('customer_onboarding', 'customer_onboarding_id', 'onboarding_welcome_email')
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $customer_informations = $response['result'];

            $response = $this->validation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            // dd($customer_informations, $validated);

            if ($validated['account_type'] == 1) {
                $customer_informations->update([
                    'account_type' => $validated['account_type'],
                    'account_no' => $validated['customer_number'],
                    'fullname' => $validated['first_name'] . " " . $validated['middle_name'] . " " . $validated['last_name'],
                    'first_name' => $validated['first_name'],
                    'middle_name' => $validated['middle_name'],
                    'last_name' => $validated['last_name'],
                    // 'life_stage_id' => $validated['life_stage'],
                    'life_stage_id' => 1,
                    'contact_owner_id' => $validated['contact_owner'],
                    'marketing_channel_id' => $validated['marketing_channel'],
                    'notes' => $validated['notes'],
                    // 'created_by' => Auth::user()->id,
                ]);
            } else {
                $customer_informations->update([
                    'account_type' => $validated['account_type'],
                    'account_no' => $validated['customer_number'],
                    'company_name' => $validated['company_name'],
                    'fullname' => $validated['company_name'],
                    'company_link' => $validated['company_website_soc_med'],
                    'industry_id' => $validated['company_industry'],
                    'is_mother_account' => $validated['is_mother_account'],
                    'child_account_id' => $validated['associate_with'] ?? null,
                    'tin' => $validated['tin'],
                    'company_anniversary' => $validated['company_anniversary'],
                    'life_stage_id' => 1,
                    'contact_owner_id' => $validated['con_per_contact_owner'],
                    'marketing_channel_id' => $validated['con_per_marketing_channel'],
                    'notes' => $validated['con_per_notes'],
                    // 'created_by' => Auth::user()->id,
                ]);

                // Contact Person Details Entry
                foreach ($validated['contact_persons'] as $i => $con_per) {
                    $contact_person = $customer_informations->contactPersons()->updateOrCreate(
                        [
                            'account_id' => $customer_informations->id,
                        ],
                        [
                            'first_name' => $con_per['first_name'],
                            'middle_name' => $con_per['middle_name'],
                            'last_name' => $con_per['last_name'],
                            'position' => $con_per['position'],
                            'is_primary' => $con_per['default'],
                        ]
                    );
                    $contact_person->save();

                    $contact_person_id = $contact_person->id;

                    foreach ($con_per['emails'] as $i => $email) {
                        $customer_informations->emails()->updateOrCreate(
                            [
                                'account_type' => 3,
                                // 'email' => $email['email'],
                                'is_primary' => $email['default'],
                            ],
                            [
                                'contact_person_id' => $contact_person_id,
                                'account_type' => 3,
                                'email' => $email['email'],
                                'is_primary' => $email['default'],
                            ]
                        );
                    }

                    foreach ($con_per['telephone_numbers'] as $i => $telephone_number) {
                        $customer_informations->telephoneNumbers()->updateOrCreate(
                            [
                                'account_type' => 3,
                                // 'telephone' => $telephone_number['telephone'],
                                'is_primary' => $telephone_number['default'],
                            ],
                            [
                                'contact_person_id' => $contact_person_id,
                                'account_type' => 3,
                                'telephone' => $telephone_number['telephone'],
                                'is_primary' => $telephone_number['default'],
                            ]
                        );
                    }

                    foreach ($con_per['mobile_numbers'] as $i => $mobile_number) {
                        $customer_informations->mobileNumbers()->updateOrCreate(
                            [
                                'account_type' => 3,
                                // 'mobile' => $mobile_number['mobile'],
                                'is_primary' => $mobile_number['default'],
                            ],
                            [
                                'contact_person_id' => $contact_person_id,
                                'account_type' => 3,
                                'mobile' => $mobile_number['mobile'],
                                'is_primary' => $mobile_number['default'],
                            ]
                        );
                    }
                }
            }

            foreach ($validated['emails'] as $i => $email) {
                $customer_informations->emails()->updateOrCreate(
                    [
                        'account_type' => $validated['account_type'],
                        // 'email' => $email['email']
                    ],
                    [
                        'account_type' => $validated['account_type'],
                        'email' => $email['email'],
                        'is_primary' => $email['default'],
                    ]
                );
            }

            foreach ($validated['telephone_numbers'] as $i => $telephone_number) {
                $customer_informations->telephoneNumbers()->updateOrCreate(
                    [
                        'account_type' => $validated['account_type'],
                        // 'telephone' => $telephone_number['telephone']
                    ],
                    [
                        'account_type' => $validated['account_type'],
                        'telephone' => $telephone_number['telephone'],
                        'is_primary' => $telephone_number['default'],
                    ]
                );
            }

            foreach ($validated['mobile_numbers'] as $i => $mobile_number) {
                if ($validated['mobile_numbers'][$i]['id'] && $validated['mobile_numbers'][$i]['is_deleted']) {
                    $teamroutes = CrmCustomerInformationMobileList::find($validated['mobile_numbers'][$i]['id']);
                    // dd($teamroutes);//query all in database
                    if ($teamroutes) {
                        $teamroutes->delete();
                    }
                } else {
                // dd($validated['mobile_numbers']);
                $customer_informations->mobileNumbers()->updateOrCreate(
                    [
                        'account_type' => $validated['account_type'],
                        'id' => $validated['mobile_numbers'][$i]['id'],
                        // 'mobile' => $mobile_number['mobile']
                    ],
                    [
                        'account_type' => $validated['account_type'],
                        'mobile' => $mobile_number['mobile'],
                        'is_primary' => $mobile_number['default'],
                    ]
                );
            }
            }

            foreach ($validated['addresses'] as $i => $address) {
                $customer_informations->addresses()->updateOrCreate(
                    [
                        'account_type' => $validated['account_type'],
                        'address_line_1' => $address['address_line1']
                    ],
                    [
                        'account_type' => $validated['account_type'],
                        'address_type' => $address['address_type'],
                        'country_id' => $address['country'] ?? null,
                        'address_line_1' => $address['address_line1'],
                        'address_line_2' => $address['address_line2'],
                        'state_id' => intval($address['state_province']),
                        'city_id' => $address['city_municipality'],
                        'barangay_id' => $address['barangay'] ?? null,
                        'postal_id' => $address['postal_code'],
                        'address_label' => $address['address_label'],
                        'is_primary' => $address['default'],
                    ]
                );
            }
            DB::commit();

            return $this->response(200, 'Contact has been successfully updated!', $customer_informations);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $customer_informations = CrmCustomerInformation::with(
                'accountType',
                'industry',
                'lifeStage',
                'contactOwner',
                'marketingChannel',
                'statusRef',
                'emails',
                'mobileNumbers',
                'telephoneNumbers',
                'addresses',
                'contactPersons'
            )->findOrFail($id);
            if (!$customer_informations) {
                return $this->response(404, 'Customer', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Customer Informations', $customer_informations);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateStatus($id, $status)
    {
        DB::beginTransaction();
        try {
            if ($status == 'Deactivate') { // Deactivate
                $customer_information_status = CrmCustomerInformation::where('id', $id)->update([
                    'status' => 2
                ]);
                $updated = 2;
            } else { // Activate
                $customer_information_status = CrmCustomerInformation::where('id', $id)->update([
                    'status' => 1
                ]);
                $updated = 1;
            }

            DB::commit();
            if ($updated == 2) {
                return $this->response(200, 'Contact has been successfully deactivated!', compact('customer_information_status'));
            } else {
                return $this->response(200, 'Contact has been successfully activated!', compact('customer_information_status'));
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
