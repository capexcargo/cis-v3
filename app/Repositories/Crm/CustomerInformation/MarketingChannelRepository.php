<?php

namespace App\Repositories\Crm\CustomerInformation;

use App\Interfaces\Crm\CustomerInformation\MarketingChannelInterface;
use App\Models\MarketingChannel;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class MarketingChannelRepository implements MarketingChannelInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $marketing_channels = MarketingChannel::orderby('name')->paginate($request['paginate']);
            // dd($marketing_channelss);


            DB::commit();

            return $this->response(200, 'Category List', compact('marketing_channels'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                // 'category' => 'required',
                'marketing' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'marketing' => 'required|unique:crm_marketing_channel,name',
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'marketing.unique' => 'The marketing channel already exist.',
                    'marketing.required' => 'The marketing channel field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);

            // dd($response);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $conv_mkt = ucwords($validated['marketing']);

            $marketing_channel = MarketingChannel::create([
                'name' => $conv_mkt,
            ]);

            DB::commit();

            return $this->response(200, 'New Marketing Channel has been successfully saved!', $marketing_channel);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $marketing_result = MarketingChannel::findOrFail($id);
            if (!$marketing_result) {
                return $this->response(404, 'Marketing Channel', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Marketing Channel', $marketing_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'marketing' => 'required|unique:crm_marketing_channel,name,' . $id,
            ];
            $validator = Validator::make(
                $request,
                $rules,
                [
                    'marketing.unique' => 'The marketing channel already exist.',
                    'marketing.required' => 'Marketing channel field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $marketing_result = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $conv_mkt = strtolower($validated['marketing']);


            $marketing_result->update([
                'name' => $conv_mkt,
            ]);

            DB::commit();

            return $this->response(200, 'Marketing channel has been successfully updated!', $marketing_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }




    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $del = MarketingChannel::find($id);
            if (!$del) return $this->response(404, 'Marketing Channel', 'Not Found!');
            $del->delete();

            DB::commit();
            return $this->response(200, 'Marketing Channel successfully deleted!', $del);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
