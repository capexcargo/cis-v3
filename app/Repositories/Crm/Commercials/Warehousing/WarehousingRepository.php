<?php

namespace App\Repositories\Crm\Commercials\Warehousing;

use App\Interfaces\Crm\Commercials\Box\BoxInterface;
use App\Interfaces\Crm\Commercials\Loa\LoaMgmtInterface;
use App\Interfaces\Crm\Commercials\Pouch\PouchInterface;
use App\Interfaces\Crm\Commercials\Warehousing\WarehousingInterface;
use App\Interfaces\Crm\ServiceRequest\SRSub\SrSubCategoryInterface;
use App\Interfaces\Hrim\RecruitmentAndHiring\EmploymentCategoryTypeInterface;
use App\Models\CrmPouchType;
use App\Models\CrmRateBox;
use App\Models\CrmRateBoxDetails;
use App\Models\CrmRateLoa;
use App\Models\CrmRatePouch;
use App\Models\CrmRatePouchDetails;
use App\Models\CrmRateWarehousing;
use App\Models\CrmRateWarehousingDetails;
use App\Models\CrmTransportMode;
use App\Models\Hrim\EmploymentCategoryType;
use App\Models\SrTypeSubcategory;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class WarehousingRepository implements WarehousingInterface
{
    use ResponseTrait;

    public function index($request, $search_request)
    {

        DB::beginTransaction();
        try {

            $warehousing_s = CrmRateWarehousing::with(
                'AncillaryReference',
                'RateApplyReference',
                'App1',
                'App2',
                'App1StatusReference',
                'App2StatusReference',
                'FinalStatusReference',
                'CreatedBy',
                'RateWarehousingHasMany'
            )->when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', 'like', '%' . $search_request['name'] . '%');
            })
                ->when($search_request['description'] ?? false, function ($query) use ($search_request) {
                    $query->where('description', 'like', '%' . $search_request['description'] . '%');
                })
                ->when($search_request['final_status_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('final_status_id', $search_request['final_status_id']);
                })->paginate($request['paginate']);


            DB::commit();

            return $this->response(200, 'List', compact('warehousing_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab1($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'created_by' => 'required',
                'name' => 'required',
                'effectivity_date' => 'required',
                'description' => 'sometimes',
                'apply_for_id' => 'sometimes',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab2($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $rules = [
                'ancillary_charge_id' => 'required',
            ];

            foreach ($request['options'] as $i => $option) {
                $rules['options.' . $i . '.warehousing_option_id'] = 'sometimes';
                $rules['options.' . $i . '.amount_short_term'] = 'required';
                $rules['options.' . $i . '.amount_long_term'] = 'required';
            }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'ancillary_charge_id.required' => 'Ancillary Charges field is required.',
                    'options.*.amount_short_term.required' => 'Amount Short Term is required.',
                    'options.*.amount_long_term.required' => 'Amount Long Term is required.',

                ]
            );

            // dd($request['options']);
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidationTab1($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated1 = $response['result'];
            $response = $this->createValidationTab2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            // dd($validated1);

            $validated2 = $response['result'];
            $validated = array_merge($validated1, $validated2);

            // dd($validated);

            $approver = CrmRateLoa::where('rate_category', 8)->first();

            $s_whouse = CrmRateWarehousing::create([
                'created_by' => Auth::user()->id,
                'name' => $validated['name'],
                'effectivity_date' => $validated['effectivity_date'],
                'ancillary_charge_id' => $validated['ancillary_charge_id'],
                'description' => $validated['description'],
                'apply_for_id' => ($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL),
                'approver1_id' => $approver->approver1_id,
                'approver2_id' => $approver->approver2_id,
                'final_status_id' => 1,
                'approver1_status_id' => 1,
                'approver2_status_id' => 1,
            ]);

            // dd($validated['options']);

            foreach ($validated['options'] as $i => $option) {
                $whousedetails = CrmRateWarehousingDetails::create([
                    'rate_id' => $s_whouse->id,
                    'warehousing_option_id' => $validated['options'][$i]['warehousing_option_id'],
                    'amount_short_term' => $validated['options'][$i]['amount_short_term'],
                    'amount_long_term' => $validated['options'][$i]['amount_long_term'],
                ]);
            }

            DB::commit();

            return $this->response(200, 'New Base Rate has been Successfully Created!', compact($s_whouse, $whousedetails));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab1of1($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'created_by' => 'required',
                'name' => 'required',
                'effectivity_date' => 'required',
                'description' => 'sometimes',
                'apply_for_id' => 'sometimes',
                'base_rate_id' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab2of2($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            foreach ($request['options'] as $i => $option) {
                $rules['options.' . $i . '.warehousing_option_id'] = 'sometimes';
                $rules['options.' . $i . '.amount_short_term'] = 'required';
                $rules['options.' . $i . '.short'] = 'required';
            }

            foreach ($request['optionl'] as $i => $option) {
                $rules['optionl.' . $i . '.warehousing_option_id'] = 'sometimes';
                $rules['optionl.' . $i . '.amount_long_term'] = 'required';
                $rules['optionl.' . $i . '.long'] = 'required';
            }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'options.*.amount_short_term.required' => 'Amount Short Term is required.',
                    'optionl.*.amount_long_term.required' => 'Amount Long Term is required.',

                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createWarehousing($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidationTab1of1($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }


            $validated1 = $response['result'];
            $response = $this->createValidationTab2of2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            // dd($validated1);

            $validated2 = $response['result'];
            $validated = array_merge($validated1, $validated2);

            // dd($validated);

            $approver = CrmRateLoa::where('rate_category', 8)->first();

            $details = CrmRateWarehousing::with(['RateWarehousingHasMany'])->findOrFail($validated['base_rate_id']);

            // dd($details->RateWarehousingHasMany);

            if (count($details->RateWarehousingHasMany) > 0) {
                $s_warehouse = CrmRateWarehousing::create([
                    'created_by' => Auth::user()->id,
                    'name' => $validated['name'],
                    'effectivity_date' => $validated['effectivity_date'],
                    'description' => $validated['description'],
                    'apply_for_id' => ($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL),
                    'base_rate_id' => $validated['base_rate_id'],
                    'approver1_id' => $approver->approver1_id,
                    'approver2_id' => $approver->approver2_id,
                    'final_status_id' => 1,
                    'approver1_status_id' => 1,
                    'approver2_status_id' => 1,
                    'ancillary_charge_id' => $details->ancillary_charge_id,


                ]);

                foreach ($validated['options'] as $optionss) {
                    $details = CrmRateWarehousingDetails::where('warehousing_option_id', $optionss['warehousing_option_id'])->where('rate_id', $validated['base_rate_id'])->first();

                    $warehousedetail_s = CrmRateWarehousingDetails::create([
                        'rate_id' => $s_warehouse->id,
                        'warehousing_option_id' => $optionss['warehousing_option_id'],
                        'amount_short_term' => ($optionss['short'] == 1 ? $details->amount_short_term + $optionss['amount_short_term'] : $details->amount_short_term - $optionss['amount_short_term']),
                    ]);

                    $long = CrmRateWarehousingDetails::find($warehousedetail_s->id);

                    foreach ($validated['optionl'] as $optionls) {
                        $long->update([
                            'rate_id' => $s_warehouse->id,
                            'amount_long_term' => ($optionls['long'] == 1 ? $details->amount_long_term + $optionls['amount_long_term'] : $details->amount_long_term - $optionls['amount_long_term']),
                        ]);
                    }
                }
            } else {
                return $this->response(500, 'Something Went Wrong', 'No rates found.');
            }
            DB::commit();

            return $this->response(200, 'Warehouse has been Successfully Created!', compact($s_warehouse, $warehousedetail_s));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // public function show($id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $pouch_result = CrmRatePouch::with('RatePouchHasMany')->findOrFail($id);
    //         if (!$pouch_result) {
    //             return $this->response(404, 'Pouch', 'Not Found!');
    //         }

    //         DB::commit();

    //         return $this->response(200, 'Pouch', $pouch_result);
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    // public function updateValidationTab1($request)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $validator = Validator::make($request, [

    //             'created_by' => 'required',
    //             'name' => 'required',
    //             'effectivity_date' => 'required',
    //             'description' => 'sometimes',
    //             'rate_type_id' => 'required',
    //             'transport_mode_id' => 'required',
    //             'booking_type_id' => 'required',
    //             'apply_for_id' => 'sometimes',
    //             'pouch_type_id' => 'required',
    //         ]);

    //         if ($validator->fails()) {
    //             return $this->response(400, 'Please Fill Required Field', $validator->errors());
    //         }

    //         DB::commit();

    //         return $this->response(200, 'Successfully Validated', $validator->validated());
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    // public function updateValidationTab2($request)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $rules = [
    //             'vice_versa' => 'sometimes',
    //         ];

    //         foreach ($request['rates'] as $i => $rate) {
    //             $rules['rates.' . $i . '.id'] = 'sometimes';
    //             $rules['rates.' . $i . '.origin_id'] = 'required';
    //             $rules['rates.' . $i . '.destination_id'] = 'required';
    //             $rules['rates.' . $i . '.amount_small'] = 'required';
    //             $rules['rates.' . $i . '.amount_medium'] = 'required';
    //             $rules['rates.' . $i . '.amount_large'] = 'required';
    //         }

    //         $validator = Validator::make(
    //             $request,
    //             $rules,
    //             [
    //                 'rates.*.origin_id.required' => 'Origin is required.',
    //                 'rates.*.destination_id.required' => 'Destination is required.',
    //                 'rates.*.amount_small.required' => 'Small is required.',
    //                 'rates.*.amount_medium.required' => 'Medium is required.',
    //                 'rates.*.amount_large.required' => 'Large is required.',

    //             ]
    //         );
    //         // dd($request['rates']);


    //         if ($validator->fails()) {
    //             return $this->response(400, 'Please Fill Required Field', $validator->errors());
    //         }

    //         DB::commit();

    //         return $this->response(200, 'Successfully Validated', $validator->validated());
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    // public function update($request, $id)
    // {
    //     // dd($request);

    //     DB::beginTransaction();
    //     try {
    //         $response = $this->show($id);
    //         if ($response['code'] != 200) {
    //             return $this->response($response['code'], $response['message'], $response['result']);
    //         }
    //         $pouch = $response['result'];

    //         // dd($pouch);

    //         $response = $this->updateValidationTab1($request);
    //         if ($response['code'] != 200) {
    //             return $this->response($response['code'], $response['message'], $response['result']);
    //         }


    //         $validated1 = $response['result'];
    //         $response = $this->updateValidationTab2($request);
    //         if ($response['code'] != 200) {
    //             return $this->response($response['code'], $response['message'], $response['result']);
    //         }

    //         // dd($validated1);

    //         $validated2 = $response['result'];
    //         $validated = array_merge($validated1, $validated2);

    //         // dd($validated, 'asdasdasd');

    //         $transportmode = CrmTransportMode::where('name', $validated['transport_mode_id'])->first();
    //         $approver = CrmRateLoa::where('rate_category', 5)->first();

    //         $pouch->update([
    //             'created_by' => Auth::user()->id,
    //             'name' => $validated['name'],
    //             'effectivity_date' => $validated['effectivity_date'],
    //             'description' => $validated['description'],
    //             'rate_type_id' => $validated['rate_type_id'],
    //             'transport_mode_id' => $transportmode->id,
    //             'booking_type_id' => $validated['booking_type_id'],
    //             'apply_for_id' => $validated['apply_for_id'],
    //             'pouch_type_id' => $validated['pouch_type_id'],
    //             'is_vice_versa' => $validated['vice_versa'],
    //             'approver1_id' => $approver->approver1_id,
    //             'approver2_id' => $approver->approver2_id,
    //             'final_status_id' => 1,
    //         ]);

    //         foreach ($request['rates'] as $a => $rate) {

    //             if ($request['rates'][$a]['id'] && $request['rates'][$a]['is_deleted']) {
    //                 $pouch_s = CrmRatePouchDetails::find($request['rates'][$a]['id']); //query all in database

    //                 if ($pouch_s) {
    //                     $pouch_s->delete();
    //                 }
    //             } else {
    //                 // dd($request['rates']);
    //                 $pouch_s = CrmRatePouchDetails::updateOrCreate(
    //                     [
    //                         'id' => $request['rates'][$a]['id'],
    //                     ],
    //                     [
    //                         'pouch_id' => $pouch->id,
    //                         'destination_id' => $validated['rates'][$a]['origin_id'],
    //                         'origin_id' => $validated['rates'][$a]['destination_id'],
    //                         'amount_small' => $validated['rates'][$a]['amount_small'],
    //                         'amount_medium' => $validated['rates'][$a]['amount_medium'],
    //                         'amount_large' => $validated['rates'][$a]['amount_large'],
    //                     ]
    //                 );
    //             }
    //         }


    //         DB::commit();

    //         return $this->response(200, 'Base Rate has been successfully updated!', compact($pouch_s, $pouch));
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }
}
