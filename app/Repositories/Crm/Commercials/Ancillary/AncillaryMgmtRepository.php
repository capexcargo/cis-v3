<?php

namespace App\Repositories\Crm\Commercials\Ancillary;

use App\Interfaces\Crm\Commercials\Ancillary\AncillaryMgmtInterface;
use App\Models\Crm\CrmAncillary;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class AncillaryMgmtRepository implements AncillaryMgmtInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $ancillary_charges = CrmAncillary::paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'List', compact('ancillary_charges'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            foreach ($request['ancillarycharges'] as $i => $ancillarycharge) {
                $rules['ancillarycharges.' . $i . '.id'] = 'sometimes';
                $rules['ancillarycharges.' . $i . '.name'] = 'required';
                $rules['ancillarycharges.' . $i . '.name'] = 'unique:crm_ancillary_management,name';
                $rules['ancillarycharges.' . $i . '.charges_amount'] = 'sometimes';
                $rules['ancillarycharges.' . $i . '.charges_rate'] = 'sometimes';
                $rules['ancillarycharges.' . $i . '.description'] = 'sometimes';
            }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'unique:crm_ancillary_management,name' => 'Ancillary Charges Name has already.',

                ],
                [
                    'ancillarycharges.*.name.required' => 'Ancillary Charges Name is required.',
                    'ancillarycharges.*.charges_amount.sometimes' => 'Charges Amount is required.',
                    'ancillarycharges.*.charges_rate.sometimes' => 'Charges Rate is required.',
                    'ancillarycharges.*.description.required' => 'Description is required.',

                ]
            );

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            foreach ($validated['ancillarycharges'] as $i => $ancillarycharge) {
                $ancillary = CrmAncillary::create([
                    'name' => $validated['ancillarycharges'][$i]['name'],
                    'charges_amount' => $validated['ancillarycharges'][$i]['charges_amount'],
                    'charges_rate' => $validated['ancillarycharges'][$i]['charges_rate'],
                    'description' => $validated['ancillarycharges'][$i]['description'],
                    'status' => 1,
                ]);
            }

            // dd($s_pouch);

            DB::commit();

            return $this->response(200, 'Ancillary Charges Name has been  added!', compact($ancillary));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'name' => 'required|unique:crm_ancillary_management,name,' . $id,
                'charges_amount' => 'required',
                'charges_rate' => 'required',
                'description' => 'sometimes',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $ancillary = CrmAncillary::findOrFail($id);
            if (!$ancillary) {
                return $this->response(404, 'Loa', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Ancillary', $ancillary);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $ancillary = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $ancillary->update([
                'name' => $validated['name'],
                'charges_amount' => $validated['charges_amount'],
                'charges_rate' => $validated['charges_rate'],
                'description' => $validated['description'],
            ]);

            DB::commit();

            return $this->response(200, 'Ancillary Charges has been successfully updated!', compact($ancillary));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
