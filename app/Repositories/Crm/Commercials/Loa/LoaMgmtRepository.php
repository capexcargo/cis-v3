<?php

namespace App\Repositories\Crm\Commercials\Loa;

use App\Interfaces\Crm\Commercials\Loa\LoaMgmtInterface;
use App\Interfaces\Crm\ServiceRequest\SRSub\SrSubCategoryInterface;
use App\Interfaces\Hrim\RecruitmentAndHiring\EmploymentCategoryTypeInterface;
use App\Models\CrmRateLoa;
use App\Models\Hrim\EmploymentCategoryType;
use App\Models\SrTypeSubcategory;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class LoaMgmtRepository implements LoaMgmtInterface
{
    use ResponseTrait;

    public function index($request, $search_request)
    {
        DB::beginTransaction();
        try {
            $loa_s = CrmRateLoa::
            with('App1','App2')
                ->when($search_request['rate_category'] ?? false, function ($query) use ($search_request) {
                    $query->where('rate_category', $search_request['rate_category'] . '%');
                })
                    ->when($search_request['approver1_id'] ?? false, function ($query) use ($search_request) {
                        $query->where('approver1_id', $search_request['approver1_id'] . '%');
                    })
                    ->when($search_request['approver2_id'] ?? false, function ($query) use ($search_request) {
                        $query->where('approver2_id', $search_request['approver2_id']);
                    })->
                paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'List', compact('loa_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'rate_category' => 'required',
                'approver1_id' => 'required',
                'approver2_id' => 'required',
            ],[
                'rate_category.required' => 'Rate Category is Required',
                'approver1_id.required' => 'First Approver is Required',
                'approver2_id.required' => 'Second Approver is Required',

            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $Loa = CrmRateLoa::create([
                'rate_category' => $validated['rate_category'],
                'approver1_id' => $validated['approver1_id'],
                'approver2_id' => $validated['approver2_id'],
            ]);

            DB::commit();

            return $this->response(200, 'Loa has been successfully created!', $Loa);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $loa_s = CrmRateLoa::findOrFail($id);
            if (!$loa_s) {
                return $this->response(404, 'Loa', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Loa', $loa_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'rate_category' => 'required',
                'approver1_id' => 'required',
                'approver2_id' => 'required',
            ],[
                'rate_category.required' => 'Rate Category is Required',
                'approver1_id.required' => 'First Approver is Required',
                'approver2_id.required' => 'Second Approver is Required',

            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $loa_s = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $loa_s->update([
                'rate_category' => $validated['rate_category'],
                'approver1_id' => $validated['approver1_id'],
                'approver2_id' => $validated['approver2_id'],
            ]);

            DB::commit();

            return $this->response(200, 'Loa has been successfully updated!', $loa_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

   

         
    // public function destroy($id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $srsc = SrTypeSubcategory::find($id);
    //         if (!$srsc) return $this->response(404, 'SR Subcategory', 'Not Found!');
    //         $srsc->delete();

    //         DB::commit();
    //         return $this->response(200, 'SR Subcategory Successfully Deleted!', $srsc);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }
}
