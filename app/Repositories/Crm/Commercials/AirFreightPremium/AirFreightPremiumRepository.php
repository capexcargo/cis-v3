<?php

namespace App\Repositories\Crm\Commercials\AirFreightPremium;

use App\Models\CrmRateLoa;
use App\Traits\ResponseTrait;
use App\Models\CrmTransportMode;
use App\Models\CrmRateAirFreight;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\CrmRateAirFreightDetails;
use App\Models\CrmRateAirFreightPremium;
use App\Traits\InitializeFirestoreTrait;
use Illuminate\Support\Facades\Validator;
use App\Models\CrmRateAirFreightPremiumDetails;
use App\Interfaces\Crm\Commercials\AirFreight\AirFreightInterface;
use App\Interfaces\Crm\Commercials\AirFreightPremium\AirFreightPremiumInterface;


class AirFreightPremiumRepository implements AirFreightPremiumInterface
{
    use ResponseTrait, InitializeFirestoreTrait;

    public function index($request, $search_request)
    {

        DB::beginTransaction();
        try {

            $airp_s = CrmRateAirFreightPremium::with(
                'RateTransportReference',
                'CommodityTypeReference',
                'RateApplyReference',
                'AncillaryReference',
                'App1',
                'App2',
                'App1StatusReference',
                'App2StatusReference',
                'FinalStatusReference',
                'CreatedBy',
                'RateAirfreightPremiumHasMany'
            )->when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', 'like', '%' . $search_request['name'] . '%');
            })
                ->when($search_request['description'] ?? false, function ($query) use ($search_request) {
                    $query->where('description', 'like', '%' . $search_request['description'] . '%');
                })
                ->when($search_request['final_status_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('final_status_id', $search_request['final_status_id']);
                })
                ->when($request['stats'], function ($query) use ($request) {
                    if ($request['stats'] == false) {
                        $query->whereIs('commodity_type_id', [1, 2]);
                    } else {
                        $query->where('commodity_type_id', $request['stats']);
                    }
                    // dd($request['stats']);
                })

                ->paginate($request['paginate']);


            DB::commit();

            return $this->response(200, 'List', compact('airp_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab1($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'created_by' => 'required',
                'name' => 'required',
                'effectivity_date' => 'required',
                'description' => 'sometimes',
                'commodity_type_id' => 'required',
                'transport_mode_id' => 'required',
                'apply_for_id' => 'sometimes',
            ],[
                'name.required' => 'Rate Name is required',
                'effectivity_date.required' => 'Effectivity Date is required',
                'commodity_type_id.required' => 'Commodity Type is required',
                'transport_mode_id.required' => 'Transport Mode is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab2($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $rules = [
                'vice_versa' => 'sometimes',
                'ancillary_charge_id' => 'required',
            ];

            foreach ($request['rates'] as $i => $rate) {
                $rules['rates.' . $i . '.id'] = 'sometimes';
                $rules['rates.' . $i . '.origin_id'] = 'required';
                $rules['rates.' . $i . '.destination_id'] = 'required';
                $rules['rates.' . $i . '.amount_weight_1'] = 'required';
                $rules['rates.' . $i . '.amount_weight_2'] = 'required';
                $rules['rates.' . $i . '.service_mode_id'] = 'required';
            }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'ancillary_charge_id.required' => 'Ancillary Charges field is required.',
                    'rates.*.origin_id.required' => 'Origin is required.',
                    'rates.*.destination_id.required' => 'Destination is required.',
                    'rates.*.amount_weight_1.required' => 'Amount is required.',
                    'rates.*.amount_weight_2.required' => 'Amount is required.',
                    'rates.*.service_mode_id.required' => 'Service is required.',

                ]
            );
            // dd($request['rates']);
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidationTab1($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated1 = $response['result'];
            $response = $this->createValidationTab2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            // dd($validated1);

            $validated2 = $response['result'];
            $validated = array_merge($validated1, $validated2);

            // dd($validated);

            $transportmode = CrmTransportMode::where('name', $validated['transport_mode_id'])->first();

            $approver = CrmRateLoa::where('rate_category', 4)->first();

            // dd($approver);

            $s_airpp = CrmRateAirFreightPremium::create([
                'created_by' => Auth::user()->id,
                'name' => $validated['name'],
                'effectivity_date' => $validated['effectivity_date'],
                'description' => $validated['description'],
                'transport_mode_id' => $transportmode->id,
                'apply_for_id' => ($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL),
                'commodity_type_id' => $validated['commodity_type_id'],
                'ancillary_charge_id' => $validated['ancillary_charge_id'],
                'is_vice_versa' => $validated['vice_versa'],
                'approver1_id' => $approver->approver1_id,
                'approver2_id' => $approver->approver2_id,
                'final_status_id' => 1,
                'approver1_status_id' => 1,
                'approver2_status_id' => 1,
            ]);

            // dd($s_airpp);


            foreach ($validated['rates'] as $i => $rate) {
                if ($validated['rates'][$i]['service_mode_id'] == 1) {
                    $airpdetails = CrmRateAirFreightPremiumDetails::create([
                        'rate_id' => $s_airpp->id,
                        'origin_id' => $validated['rates'][$i]['origin_id'],
                        'destination_id' => $validated['rates'][$i]['destination_id'],
                        'amount_weight_1' => $validated['rates'][$i]['amount_weight_1'],
                        'amount_weight_2' => $validated['rates'][$i]['amount_weight_2'],
                        'service_mode_id' => $validated['rates'][$i]['service_mode_id'],
                        'is_primary' => 1,
                    ]);
                    if ($validated['vice_versa'] == 1) {

                        // foreach ($validated['rates'] as $i => $rate) {
                            $airpdetails = CrmRateAirFreightPremiumDetails::create([
                                'rate_id' => $s_airpp->id,
                                'destination_id' => $validated['rates'][$i]['origin_id'],
                                'origin_id' => $validated['rates'][$i]['destination_id'],
                                'amount_weight_1' => $validated['rates'][$i]['amount_weight_1'],
                                'amount_weight_2' => $validated['rates'][$i]['amount_weight_2'],
                                'service_mode_id' => $validated['rates'][$i]['service_mode_id'],
                            ]);
                        // }
                    }
                } else {
                    $airpdetails = CrmRateAirFreightPremiumDetails::create([
                        'rate_id' => $s_airpp->id,
                        'origin_id' => $validated['rates'][$i]['origin_id'],
                        'destination_id' => $validated['rates'][$i]['destination_id'],
                        'amount_weight_3' => $validated['rates'][$i]['amount_weight_1'],
                        'amount_weight_4' => $validated['rates'][$i]['amount_weight_2'],
                        'service_mode_id' => $validated['rates'][$i]['service_mode_id'],
                        'is_primary' => 1,
                    ]);

                    if ($validated['vice_versa'] == 1) {

                        // foreach ($validated['rates'] as $i => $rate) {
                            $airpdetails = CrmRateAirFreightPremiumDetails::create([
                                'rate_id' => $s_airpp->id,
                                'destination_id' => $validated['rates'][$i]['origin_id'],
                                'origin_id' => $validated['rates'][$i]['destination_id'],
                                'amount_weight_3' => $validated['rates'][$i]['amount_weight_1'],
                                'amount_weight_4' => $validated['rates'][$i]['amount_weight_2'],
                                'service_mode_id' => $validated['rates'][$i]['service_mode_id'],
                            ]);
                        // }
                    }
                }
            }


            // dd($s_box);
            DB::commit();

            return $this->response(200, 'Rate has been successfully created!', compact($s_airpp, $airpdetails));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab1of1($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'created_by' => 'required',
                'name' => 'required',
                'effectivity_date' => 'required',
                'description' => 'sometimes',
                'transport_mode_id' => 'required',
                'commodity_type_id' => 'required',
                'apply_for_id' => 'sometimes',
                'origin_id' => 'sometimes',
                'destination_id' => 'sometimes',
                'base_rate_id' => 'required',
            ],[
                'name.required' => 'Rate Name is required',
                'effectivity_date.required' => 'Effectivity Date is required',
                'transport_mode_id.required' => 'Transport Mode is required',
                'commodity_type_id.required' => 'Commodity Type is required',
                'base_rate_id.required' => 'Base Rate is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab2of2($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'amount_weight_1' => 'required',
                'amount_weight_2' => 'required',
                'amount_weight_3' => 'required',
                'amount_weight_4' => 'required',
                'aw1' => 'required',
                'aw2' => 'required',
                'aw3' => 'required',
                'aw4' => 'required',
                'ancillary_charge_id' => 'required',
            ],[
                'amount_weight_1.required' => 'Amount is required',
                'amount_weight_2.required' => 'Amount is required',
                'amount_weight_3.required' => 'Amount is required',
                'amount_weight_4.required' => 'Amount is required',
                'ancillary_charge_id.required' => 'Ancillary Charges field is required.',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createAirPremium($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidationTab1of1($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }


            $validated1 = $response['result'];
            $response = $this->createValidationTab2of2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            // dd($validated1);

            $validated2 = $response['result'];
            $validated = array_merge($validated1, $validated2);

            // dd($validated);

            $transportmode = CrmTransportMode::where('name', $validated['transport_mode_id'])->first();

            $approver = CrmRateLoa::where('rate_category', 4)->first();

            $details = CrmRateAirFreightPremium::with(['RateAirfreightPremiumHasMany'])->findOrFail($validated['base_rate_id']);


            if (count($details->RateAirfreightPremiumHasMany) > 0) {
                $s_airp = CrmRateAirFreightPremium::create([
                    'created_by' => Auth::user()->id,
                    'name' => $validated['name'],
                    'effectivity_date' => $validated['effectivity_date'],
                    'description' => $validated['description'],
                    'transport_mode_id' => $transportmode->id,
                    'ancillary_charge_id' => $validated['ancillary_charge_id'],
                    'commodity_type_id' => $validated['commodity_type_id'],
                    'apply_for_id' => ($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL),
                    'base_rate_id' => $validated['base_rate_id'],
                    'approver1_id' => $approver->approver1_id,
                    'approver2_id' => $approver->approver2_id,
                    'final_status_id' => 1,
                    'approver1_status_id' => 1,
                    'approver2_status_id' => 1,
                ]);
                
                //firestore create AFP
                $collectionAirFreightPReference = $this->initializeFirestore()->collection('crm_rate_airfreight_premium'); 
                $documentAirFreightPReference = $collectionAirFreightPReference->add();
                $documentAirFreightPReference->set([
                    'ancillary_charge_id' => intval($validated['ancillary_charge_id']),
                    'apply_for_id' => intval(($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL)),
                    'base_rate_id' => intval($validated['base_rate_id']),
                    'commodity_type_id' => intval($validated['commodity_type_id']),
                    'created_by' => Auth::user()->id,
                    'description' => $validated['description'],
                    'effectivity_date' => $validated['effectivity_date'],
                    'final_status_id' => 1,
                    'id' => $s_airp->id,
                    'name' => $validated['name'],
                    'transport_mode_id' => $transportmode->id,
                ]);

                foreach ($details->RateAirfreightPremiumHasMany as $airpdetails) {

                    if ($airpdetails['service_mode_id'] == 1) {
                        $airpdetail_s = CrmRateAirFreightPremiumDetails::create([
                            'rate_id' => $s_airp->id,
                            'origin_id' => $airpdetails['origin_id'],
                            'destination_id' => $airpdetails['destination_id'],
                            'service_mode_id' => $airpdetails['service_mode_id'],
                            'is_primary' => $airpdetails['is_primary'],
                            'amount_weight_1' => ($validated['aw1'] == 1 ? $airpdetails['amount_weight_1'] + $validated['amount_weight_1'] : $airpdetails['amount_weight_1'] - $validated['amount_weight_1']),
                            'amount_weight_2' => ($validated['aw2'] == 1 ? $airpdetails['amount_weight_2'] + $validated['amount_weight_2'] : $airpdetails['amount_weight_2'] - $validated['amount_weight_2']),
                        ]);
                    } else {
                        $airpdetail_s = CrmRateAirFreightPremiumDetails::create([
                            'rate_id' => $s_airp->id,
                            'origin_id' => $airpdetails['origin_id'],
                            'destination_id' => $airpdetails['destination_id'],
                            'service_mode_id' => $airpdetails['service_mode_id'],
                            'is_primary' => $airpdetails['is_primary'],
                            'amount_weight_3' => ($validated['aw3'] == 1 ? $airpdetails['amount_weight_3'] + $validated['amount_weight_3'] : $airpdetails['amount_weight_3'] - $validated['amount_weight_3']),
                            'amount_weight_4' => ($validated['aw4'] == 1 ? $airpdetails['amount_weight_4'] + $validated['amount_weight_4'] : $airpdetails['amount_weight_4'] - $validated['amount_weight_4']),
                        ]);
                    }

                    //firestore AFP Details
                    $collectionAFPDetailsReference = $this->initializeFirestore()->collection('crm_rate_airfreight_premium_details'); 
                    $documentAFPDetailsReference = $collectionAFPDetailsReference->add();
                    $documentAFPDetailsReference->set([
                        'rate_id' => $s_airp->id,
                        'origin_id' => $airpdetails['origin_id'],
                        'destination_id' => $airpdetails['destination_id'],
                        'service_mode_id' => $airpdetails['service_mode_id'],
                        'is_primary' => $airpdetails['is_primary'],
                        'amount_weight_1' => ($validated['aw1'] == 1 ? $airpdetails['amount_weight_1'] + $validated['amount_weight_1'] : $airpdetails['amount_weight_1'] - $validated['amount_weight_1']),
                        'amount_weight_2' => ($validated['aw2'] == 1 ? $airpdetails['amount_weight_2'] + $validated['amount_weight_2'] : $airpdetails['amount_weight_2'] - $validated['amount_weight_2']),
                        'amount_weight_3' => ($validated['aw3'] == 1 ? $airpdetails['amount_weight_3'] + $validated['amount_weight_3'] : $airpdetails['amount_weight_3'] - $validated['amount_weight_3']),
                        'amount_weight_4' => ($validated['aw4'] == 1 ? $airpdetails['amount_weight_4'] + $validated['amount_weight_4'] : $airpdetails['amount_weight_4'] - $validated['amount_weight_4']),
                    ]);
                }
            } else {
                return $this->response(500, 'Something Went Wrong', 'No rates found.');
            }


            DB::commit();

            return $this->response(200, 'Rate has been successfully created!', compact($s_airp, $airpdetail_s));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
