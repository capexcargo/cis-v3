<?php

namespace App\Repositories\Crm\Commercials\Pouch;

use App\Models\CrmRateLoa;
use App\Models\CrmPouchType;
use App\Models\CrmRatePouch;
use App\Traits\ResponseTrait;
use App\Models\CrmTransportMode;
use App\Models\SrTypeSubcategory;
use Illuminate\Support\Facades\DB;
use App\Models\CrmRatePouchDetails;
use Illuminate\Support\Facades\Auth;
use App\Traits\InitializeFirestoreTrait;
use Illuminate\Support\Facades\Validator;
use App\Models\Hrim\EmploymentCategoryType;
use App\Interfaces\Crm\Commercials\Loa\LoaMgmtInterface;
use App\Interfaces\Crm\Commercials\Pouch\PouchInterface;
use App\Interfaces\Crm\ServiceRequest\SRSub\SrSubCategoryInterface;
use App\Interfaces\Hrim\RecruitmentAndHiring\EmploymentCategoryTypeInterface;


class PouchRepository implements PouchInterface
{
    use ResponseTrait, InitializeFirestoreTrait;

    public function index($request, $search_request)
    {

        DB::beginTransaction();
        try {

            $pouch_s = CrmRatePouch::with(
                'RateTypeReference',
                'RateTransportReference',
                'BookingTypeReference',
                'RateApplyReference',
                'RatePouchReference',
                'PouchTypeReference',
                'App1',
                'App2',
                'App1StatusReference',
                'App2StatusReference',
                'FinalStatusReference',
                'CreatedBy',
                'RatePouchHasMany'
            )->when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', 'like', '%' . $search_request['name'] . '%');
            })
                ->when($search_request['description'] ?? false, function ($query) use ($search_request) {
                    $query->where('description', 'like', '%' . $search_request['description'] . '%');
                })
                ->when($search_request['final_status_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('final_status_id', $search_request['final_status_id']);
                })
                ->when($request['stats'], function ($query) use ($request) {
                    if ($request['stats'] == false) {
                        $query->whereIs('booking_type_id', [1, 2]);
                    } else {
                        $query->where('booking_type_id', $request['stats']);
                    }
                    // dd($request['stats']);
                })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'List', compact('pouch_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab1($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'created_by' => 'required',
                'name' => 'required',
                'effectivity_date' => 'required',
                'description' => 'sometimes',
                'rate_type_id' => 'required',
                'transport_mode_id' => 'required',
                'booking_type_id' => 'required',
                'apply_for_id' => 'sometimes',
                'pouch_type_id' => 'required',
            ],[
                'name.required' => 'Rate Name is Required',
                'effectivity_date.required' => 'Effectivity Date is Required',
                'rate_type_id.required' => 'Rate Type is Required',
                'transport_mode_id.required' => 'Transport Mode is Required',
                'booking_type_id.required' => 'Booking Type is Required',
                'pouch_type_id.required' => 'Pouch Type is Required',

            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab2($request)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'vice_versa' => 'sometimes',
            ];

            foreach ($request['rates'] as $i => $rate) {
                $rules['rates.' . $i . '.id'] = 'sometimes';
                $rules['rates.' . $i . '.origin_id'] = 'required';
                $rules['rates.' . $i . '.destination_id'] = 'required';
                $rules['rates.' . $i . '.amount_small'] = 'required';
                $rules['rates.' . $i . '.amount_medium'] = 'required';
                $rules['rates.' . $i . '.amount_large'] = 'required';
            }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'rates.*.origin_id.required' => 'Origin is required.',
                    'rates.*.destination_id.required' => 'Destination is required.',
                    'rates.*.amount_small.required' => 'Small is required.',
                    'rates.*.amount_medium.required' => 'Medium is required.',
                    'rates.*.amount_large.required' => 'Large is required.',

                ]
            );
            // dd($request['rates']);


            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidationTab1($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }


            $validated1 = $response['result'];
            $response = $this->createValidationTab2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            // dd($validated1);

            $validated2 = $response['result'];
            $validated = array_merge($validated1, $validated2);

            $transportmode = CrmTransportMode::where('name', $validated['transport_mode_id'])->first();

            $approver = CrmRateLoa::where('rate_category', 5)->first();



            // dd($approver->approver1_id);


            $s_pouch = CrmRatePouch::create([
                'created_by' => Auth::user()->id,
                'name' => $validated['name'],
                'effectivity_date' => $validated['effectivity_date'],
                'description' => $validated['description'],
                'rate_type_id' => $validated['rate_type_id'],
                'transport_mode_id' => $transportmode->id,
                'booking_type_id' => $validated['booking_type_id'],
                'apply_for_id' => ($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL),
                'pouch_type_id' => $validated['pouch_type_id'],
                'is_vice_versa' => $validated['vice_versa'],
                'approver1_id' => $approver->approver1_id,
                'approver2_id' => $approver->approver2_id,
                'final_status_id' => 1,
                'approver1_status_id' => 1,
                'approver2_status_id' => 1,

            ]);

            foreach ($validated['rates'] as $i => $rate) {
                $pouchdetails = CrmRatePouchDetails::create([
                    'pouch_id' => $s_pouch->id,
                    'origin_id' => $validated['rates'][$i]['origin_id'],
                    'destination_id' => $validated['rates'][$i]['destination_id'],
                    'amount_small' => $validated['rates'][$i]['amount_small'],
                    'amount_medium' => $validated['rates'][$i]['amount_medium'],
                    'amount_large' => $validated['rates'][$i]['amount_large'],
                    'is_primary' => 1,

                ]);
            }

            if ($validated['vice_versa'] == 1) {

                foreach ($validated['rates'] as $i => $rate) {
                    $pouchdetails = CrmRatePouchDetails::create([
                        'pouch_id' => $s_pouch->id,
                        'destination_id' => $validated['rates'][$i]['origin_id'],
                        'origin_id' => $validated['rates'][$i]['destination_id'],
                        'amount_small' => $validated['rates'][$i]['amount_small'],
                        'amount_medium' => $validated['rates'][$i]['amount_medium'],
                        'amount_large' => $validated['rates'][$i]['amount_large'],

                    ]);
                }
            }

            // dd($s_pouch);


            DB::commit();

            return $this->response(200, 'Rate has been successfully created!', compact($s_pouch, $pouchdetails));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab1of1($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'created_by' => 'required',
                'name' => 'required',
                'effectivity_date' => 'required',
                'description' => 'sometimes',
                'rate_type_id' => 'required',
                'transport_mode_id' => 'required',
                'booking_type_id' => 'required',
                'apply_for_id' => 'sometimes',
                'origin_id' => 'sometimes',
                'destination_id' => 'sometimes',
                'base_rate_id' => 'required',
                'pouch_type_id' => 'required',
            ],[
                'name.required' => 'Rate Name is Required',
                'effectivity_date.required' => 'Effectivity Date is Required',
                'rate_type_id.required' => 'Rate Type is Required',
                'transport_mode_id.required' => 'Transport Mode is Required',
                'booking_type_id.required' => 'Booking Type is Required',
                'pouch_type_id.required' => 'Pouch Type is Required',
                'base_rate_id.required' => 'Base Rate is Required',

            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab2of2($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'amount_small' => 'required',
                'amount_medium' => 'required',
                'amount_large' => 'required',
                'small' => 'required',
                'medium' => 'required',
                'large' => 'required',
            ],[
                'amount_small.required' => 'Amount is required',
                'amount_medium.required' => 'Amount is required',
                'amount_large.required' => 'Amount is required',
                'small.required' => 'Amount is required',
                'medium.required' => 'Amount is required',
                'large.required' => 'Amount is required',

            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createPouch($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidationTab1of1($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }


            $validated1 = $response['result'];
            $response = $this->createValidationTab2of2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            // dd($validated1);

            $validated2 = $response['result'];
            $validated = array_merge($validated1, $validated2);

            $transportmode = CrmTransportMode::where('name', $validated['transport_mode_id'])->first();

            $approver = CrmRateLoa::where('rate_category', 5)->first();

            $asd = CrmRatePouch::with(['RatePouchHasMany'])->findOrFail($validated['base_rate_id']);

            if (count($asd->RatePouchHasMany) > 0) {
                // dd($asd->RatePouchHasMany );
                $s_pouch = CrmRatePouch::create([
                    'created_by' => Auth::user()->id,
                    'name' => $validated['name'],
                    'effectivity_date' => $validated['effectivity_date'],
                    'description' => $validated['description'],
                    'rate_type_id' => $validated['rate_type_id'],
                    'transport_mode_id' => $transportmode->id,
                    'booking_type_id' => $validated['booking_type_id'],
                    'apply_for_id' => ($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL),
                    'pouch_type_id' => $validated['pouch_type_id'],
                    'base_rate_id' => $validated['base_rate_id'],
                    'approver1_id' => $approver->approver1_id,
                    'approver2_id' => $approver->approver2_id,
                    'final_status_id' => 1,
                    'approver1_status_id' => 1,
                    'approver2_status_id' => 1,
                ]);

                //firestore create pouch
                $collectionPouchReference = $this->initializeFirestore()->collection('crm_rate_pouch');
                $documentPouchReference = $collectionPouchReference->add();
                $documentPouchReference->set([
                    'created_by' => Auth::user()->id,
                    'name' => $validated['name'],
                    'effectivity_date' => $validated['effectivity_date'],
                    'description' => $validated['description'],
                    'rate_type_id' => intval($validated['rate_type_id']),
                    'transport_mode_id' => $transportmode->id,
                    'booking_type_id' => intval($validated['booking_type_id']),
                    'apply_for_id' => intval(($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL)),
                    'pouch_type_id' => intval($validated['pouch_type_id']),
                    'base_rate_id' => intval($validated['base_rate_id']),
                    'approver1_id' => $approver->approver1_id,
                    'approver2_id' => $approver->approver2_id,
                    'final_status_id' => 1,
                    'approver1_status_id' => 1,
                    'approver2_status_id' => 1, 
                ]);

                foreach ($asd->RatePouchHasMany as $pouchdetails) {
                    $pouchdetailss = CrmRatePouchDetails::create([
                        'pouch_id' => $s_pouch->id,
                        'origin_id' => $pouchdetails['origin_id'],
                        'destination_id' => $pouchdetails['destination_id'],
                        'amount_small' => ($validated['small'] == 1 ? $pouchdetails['amount_small'] + $validated['amount_small'] : $pouchdetails['amount_small'] - $validated['amount_small']),
                        'amount_medium' => ($validated['medium'] == 1 ? $pouchdetails['amount_medium'] + $validated['amount_medium'] : $pouchdetails['amount_medium'] - $validated['amount_medium']),
                        'amount_large' => ($validated['large'] == 1 ? $pouchdetails['amount_large'] + $validated['amount_large'] : $pouchdetails['amount_large'] - $validated['amount_large']),
                        'is_primary' => $pouchdetails['is_primary'],
                    ]);

                    //firestore create pouch details
                    $collectionPouchDetailsReference = $this->initializeFirestore()->collection('crm_rate_pouch_details');
                    $documentPouchDetailsReference = $collectionPouchDetailsReference->add();
                    $documentPouchDetailsReference->set([
                        'pouch_id' => $s_pouch->id,
                        'origin_id' => $pouchdetails['origin_id'],
                        'destination_id' => $pouchdetails['destination_id'],
                        'amount_small' => ($validated['small'] == 1 ? $pouchdetails['amount_small'] + $validated['amount_small'] : $pouchdetails['amount_small'] - $validated['amount_small']),
                        'amount_medium' => ($validated['medium'] == 1 ? $pouchdetails['amount_medium'] + $validated['amount_medium'] : $pouchdetails['amount_medium'] - $validated['amount_medium']),
                        'amount_large' => ($validated['large'] == 1 ? $pouchdetails['amount_large'] + $validated['amount_large'] : $pouchdetails['amount_large'] - $validated['amount_large']),
                        'is_primary' => $pouchdetails['is_primary'],
                    ]);
                }
            } else {
                return $this->response(500, 'Something Went Wrong', 'No rates found.');
            }


            DB::commit();

            return $this->response(200, 'Rate has been successfully created!', compact($s_pouch, $pouchdetailss));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $pouch_result = CrmRatePouch::with('RatePouchHasMany')->findOrFail($id);
            if (!$pouch_result) {
                return $this->response(404, 'Pouch', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Pouch', $pouch_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidationTab1($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'created_by' => 'required',
                'name' => 'required',
                'effectivity_date' => 'required',
                'description' => 'sometimes',
                'rate_type_id' => 'required',
                'transport_mode_id' => 'required',
                'booking_type_id' => 'required',
                'apply_for_id' => 'sometimes',
                'pouch_type_id' => 'required',
            ],[
                'name.required' => 'Rate Name is Required',
                'effectivity_date.required' => 'Effectivity Date is Required',
                'rate_type_id.required' => 'Rate Type is Required',
                'transport_mode_id.required' => 'Transport Mode is Required',
                'booking_type_id.required' => 'Booking Type is Required',
                'pouch_type_id.required' => 'Pouch Type is Required',

            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidationTab2($request)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'vice_versa' => 'sometimes',
            ];

            foreach ($request['rates'] as $i => $rate) {
                $rules['rates.' . $i . '.id'] = 'sometimes';
                $rules['rates.' . $i . '.origin_id'] = 'required';
                $rules['rates.' . $i . '.destination_id'] = 'required';
                $rules['rates.' . $i . '.amount_small'] = 'required';
                $rules['rates.' . $i . '.amount_medium'] = 'required';
                $rules['rates.' . $i . '.amount_large'] = 'required';
            }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'rates.*.origin_id.required' => 'Origin is required.',
                    'rates.*.destination_id.required' => 'Destination is required.',
                    'rates.*.amount_small.required' => 'Small is required.',
                    'rates.*.amount_medium.required' => 'Medium is required.',
                    'rates.*.amount_large.required' => 'Large is required.',

                ]
            );
            // dd($request['rates']);


            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $pouch = $response['result'];

            // dd($pouch);

            $response = $this->updateValidationTab1($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }


            $validated1 = $response['result'];
            $response = $this->updateValidationTab2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            // dd($validated1);

            $validated2 = $response['result'];
            $validated = array_merge($validated1, $validated2);

            // dd($validated, 'asdasdasd');

            $transportmode = CrmTransportMode::where('name', $validated['transport_mode_id'])->first();
            $approver = CrmRateLoa::where('rate_category', 5)->first();

            $pouch->update([
                'created_by' => Auth::user()->id,
                'name' => $validated['name'],
                'effectivity_date' => $validated['effectivity_date'],
                'description' => $validated['description'],
                'rate_type_id' => $validated['rate_type_id'],
                'transport_mode_id' => $transportmode->id,
                'booking_type_id' => $validated['booking_type_id'],
                'apply_for_id' => $validated['apply_for_id'],
                'pouch_type_id' => $validated['pouch_type_id'],
                'is_vice_versa' => $validated['vice_versa'],
                'approver1_id' => $approver->approver1_id,
                'approver2_id' => $approver->approver2_id,
                'final_status_id' => 1,
            ]);

            foreach ($request['rates'] as $a => $rate) {

                if ($request['rates'][$a]['id'] && $request['rates'][$a]['is_deleted']) {
                    $pouch_s = CrmRatePouchDetails::find($request['rates'][$a]['id']); //query all in database

                    if ($pouch_s) {
                        $pouch_s->delete();
                    }
                } else {
                    // dd($request['rates']);
                    $pouch_s = CrmRatePouchDetails::updateOrCreate(
                        [
                            'id' => $request['rates'][$a]['id'],
                        ],
                        [
                            'pouch_id' => $pouch->id,
                            'destination_id' => $validated['rates'][$a]['origin_id'],
                            'origin_id' => $validated['rates'][$a]['destination_id'],
                            'amount_small' => $validated['rates'][$a]['amount_small'],
                            'amount_medium' => $validated['rates'][$a]['amount_medium'],
                            'amount_large' => $validated['rates'][$a]['amount_large'],
                        ]
                    );
                }
            }


            DB::commit();

            return $this->response(200, 'Rate has been successfully updated!', compact($pouch_s, $pouch));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
