<?php

namespace App\Repositories\Crm\Commercials\SeaFreight;

use App\Models\CrmRateLoa;
use App\Traits\ResponseTrait;
use App\Models\CrmTransportMode;
use App\Models\CrmRateAirFreight;
use App\Models\CrmRateSeaFreight;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\CrmRateAirFreightDetails;
use App\Models\CrmRateSeaFreightDetails;
use App\Traits\InitializeFirestoreTrait;
use Illuminate\Support\Facades\Validator;
use App\Interfaces\Crm\Commercials\AirFreight\AirFreightInterface;
use App\Interfaces\Crm\Commercials\SeaFreight\SeaFreightInterface;


class SeaFreightRepository implements SeaFreightInterface
{
    use ResponseTrait, InitializeFirestoreTrait;

    public function index($request, $search_request)
    {

        DB::beginTransaction();
        try {

            $sea_s = CrmRateSeaFreight::with(
                'RateTransportReference',
                'CommodityTypeReference',
                'RateApplyReference',
                'AncillaryReference',
                'ServiceModeReference',
                'ShipmentTypeReference',
                'App1',
                'App2',
                'App1StatusReference',
                'App2StatusReference',
                'FinalStatusReference',
                'CreatedBy',
                'RateSeafreightHasMany'
            )->when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', 'like', '%' . $search_request['name'] . '%');
            })
                ->when($search_request['description'] ?? false, function ($query) use ($search_request) {
                    $query->where('description', 'like', '%' . $search_request['description'] . '%');
                })
                ->when($search_request['final_status_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('final_status_id', $search_request['final_status_id']);
                })
                ->when($request['stats'], function ($query) use ($request) {
                    if ($request['stats'] == false) {
                        $query->whereIs('commodity_type_id', [1, 2]);
                    } else {
                        $query->where('commodity_type_id', $request['stats']);
                    }
                    // dd($request['stats']);
                })
                ->paginate($request['paginate']);


            DB::commit();

            return $this->response(200, 'List', compact('sea_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab1($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'created_by' => 'required',
                'name' => 'required',
                'effectivity_date' => 'required',
                'description' => 'sometimes',
                'commodity_type_id' => 'required',
                'shipment_type_id' => 'required',
                'transport_mode_id' => 'required',
                'apply_for_id' => 'sometimes',
            ],[
                'name.required' => 'Rate Name is Required',
                'effectivity_date.required' => 'Effectivity Date is Required',
                'commodity_type_id.required' => 'Commodity Type is Required',
                'shipment_type_id.required' => 'Shipment Type is Required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab2($request)
    {
        // dd($request);
        // dd($request['shipment_type_id']);

        DB::beginTransaction();
        try {

            if ($request['shipment_type_id'] == 1) {
                // if ($request->shipment_type_id == 1) {

                $rules = [
                    'vice_versa' => 'sometimes',
                ];

                foreach ($request['rates'] as $i => $rate) {
                    $rules['rates.' . $i . '.id'] = 'sometimes';
                    $rules['rates.' . $i . '.origin_id'] = 'required';
                    $rules['rates.' . $i . '.destination_id'] = 'required';
                    $rules['rates.' . $i . '.size'] = 'required';
                    $rules['rates.' . $i . '.amount_servicemode_1'] = 'required';
                    $rules['rates.' . $i . '.amount_servicemode_2'] = 'required';
                    $rules['rates.' . $i . '.amount_servicemode_3'] = 'required';
                    $rules['rates.' . $i . '.amount_servicemode_4'] = 'required';
                }
            } else {

                $rules = [
                    'vice_versa' => 'sometimes',
                    'ancillary_charge_id' => 'required',
                ];

                foreach ($request['rates'] as $i => $rate) {
                    $rules['rates.' . $i . '.id'] = 'sometimes';
                    $rules['rates.' . $i . '.origin_id'] = 'required';
                    $rules['rates.' . $i . '.destination_id'] = 'required';
                    $rules['rates.' . $i . '.amount_weight_1'] = 'required';
                    $rules['rates.' . $i . '.amount_weight_2'] = 'required';
                    $rules['rates.' . $i . '.amount_weight_3'] = 'required';
                    $rules['rates.' . $i . '.amount_weight_4'] = 'required';
                }
            }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'ancillary_charge_id.required' => 'Ancillary Charges field is required.',
                    'rates.*.origin_id.required' => 'Origin is required.',
                    'rates.*.destination_id.required' => 'Destination is required.',
                    'rates.*.size.required' => 'Size is required.',
                    'rates.*.amount_weight_1.required' => 'Amount is required.',
                    'rates.*.amount_weight_2.required' => 'Amount is required.',
                    'rates.*.amount_weight_3.required' => 'Amount is required.',
                    'rates.*.amount_weight_4.required' => 'Amount is required.',
                    'rates.*.amount_servicemode_1.required' => 'Amount is required.',
                    'rates.*.amount_servicemode_2.required' => 'Amount is required.',
                    'rates.*.amount_servicemode_3.required' => 'Amount is required.',
                    'rates.*.amount_servicemode_4.required' => 'Amount is required.',

                ]
            );
            // dd($request['rates']);
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidationTab1($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated1 = $response['result'];
            $response = $this->createValidationTab2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }


            $validated2 = $response['result'];
            $validated = array_merge($validated1, $validated2);

            // dd($validated);
            // dd($validated['shipment_type_id']);

            $transportmode = CrmTransportMode::where('name', $validated['transport_mode_id'])->first();

            $approver = CrmRateLoa::where('rate_category', 2)->first();

            if ($validated['shipment_type_id'] == 1) {

                $s_sea = CrmRateSeaFreight::create([
                    'created_by' => Auth::user()->id,
                    'name' => $validated['name'],
                    'effectivity_date' => $validated['effectivity_date'],
                    'description' => $validated['description'],
                    'transport_mode_id' => $transportmode->id,
                    'apply_for_id' => ($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL),
                    'commodity_type_id' => $validated['commodity_type_id'],
                    'shipment_type_id' => $validated['shipment_type_id'],
                    'is_vice_versa' => $validated['vice_versa'],
                    'approver1_id' => $approver->approver1_id,
                    'approver2_id' => $approver->approver2_id,
                    'final_status_id' => 1,
                    'approver1_status_id' => 1,
                    'approver2_status_id' => 1,
                ]);

                foreach ($validated['rates'] as $i => $rate) {
                    $seadetails = CrmRateSeaFreightDetails::create([
                        'rate_id' => $s_sea->id,
                        'origin_id' => $validated['rates'][$i]['origin_id'],
                        'destination_id' => $validated['rates'][$i]['destination_id'],
                        'size' => $validated['rates'][$i]['size'],
                        'amount_servicemode_1' => $validated['rates'][$i]['amount_servicemode_1'],
                        'amount_servicemode_2' => $validated['rates'][$i]['amount_servicemode_2'],
                        'amount_servicemode_3' => $validated['rates'][$i]['amount_servicemode_3'],
                        'amount_servicemode_4' => $validated['rates'][$i]['amount_servicemode_4'],
                        'is_primary' => 1,
                    ]);
                }

                if ($validated['vice_versa'] == 1) {

                    foreach ($validated['rates'] as $i => $rate) {
                        $seadetails = CrmRateSeaFreightDetails::create([
                            'rate_id' => $s_sea->id,
                            'destination_id' => $validated['rates'][$i]['origin_id'],
                            'origin_id' => $validated['rates'][$i]['destination_id'],
                            'size' => $validated['rates'][$i]['size'],
                            'amount_servicemode_1' => $validated['rates'][$i]['amount_servicemode_1'],
                            'amount_servicemode_2' => $validated['rates'][$i]['amount_servicemode_2'],
                            'amount_servicemode_3' => $validated['rates'][$i]['amount_servicemode_3'],
                            'amount_servicemode_4' => $validated['rates'][$i]['amount_servicemode_4'],
                        ]);
                    }
                }
            } else {

                $s_sea = CrmRateSeaFreight::create([
                    'created_by' => Auth::user()->id,
                    'name' => $validated['name'],
                    'effectivity_date' => $validated['effectivity_date'],
                    'description' => $validated['description'],
                    'ancillary_charge_id' => $validated['ancillary_charge_id'],
                    'transport_mode_id' => $transportmode->id,
                    'apply_for_id' => ($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL),
                    'commodity_type_id' => $validated['commodity_type_id'],
                    'shipment_type_id' => $validated['shipment_type_id'],
                    'is_vice_versa' => $validated['vice_versa'],
                    'approver1_id' => $approver->approver1_id,
                    'approver2_id' => $approver->approver2_id,
                    'final_status_id' => 1,
                    'approver1_status_id' => 1,
                    'approver2_status_id' => 1,
                ]);

                foreach ($validated['rates'] as $i => $rate) {
                    $seadetails = CrmRateSeaFreightDetails::create([
                        'rate_id' => $s_sea->id,
                        'origin_id' => $validated['rates'][$i]['origin_id'],
                        'destination_id' => $validated['rates'][$i]['destination_id'],
                        'amount_weight_1' => $validated['rates'][$i]['amount_weight_1'],
                        'amount_weight_2' => $validated['rates'][$i]['amount_weight_2'],
                        'amount_weight_3' => $validated['rates'][$i]['amount_weight_3'],
                        'amount_weight_4' => $validated['rates'][$i]['amount_weight_4'],
                        'is_primary' => 1,
                    ]);
                }

                if ($validated['vice_versa'] == 1) {

                    foreach ($validated['rates'] as $i => $rate) {
                        $seadetails = CrmRateSeaFreightDetails::create([
                            'rate_id' => $s_sea->id,
                            'destination_id' => $validated['rates'][$i]['origin_id'],
                            'origin_id' => $validated['rates'][$i]['destination_id'],
                            'amount_weight_1' => $validated['rates'][$i]['amount_weight_1'],
                            'amount_weight_2' => $validated['rates'][$i]['amount_weight_2'],
                            'amount_weight_3' => $validated['rates'][$i]['amount_weight_3'],
                            'amount_weight_4' => $validated['rates'][$i]['amount_weight_4'],
                        ]);
                    }
                }
            }

            // dd($s_box);
            DB::commit();

            return $this->response(200, 'Rate has been successfully created!', compact($s_sea, $seadetails));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab1of1($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'created_by' => 'required',
                'name' => 'required',
                'effectivity_date' => 'required',
                'description' => 'sometimes',
                'transport_mode_id' => 'required',
                'commodity_type_id' => 'required',
                'apply_for_id' => 'sometimes',
                'origin_id' => 'sometimes',
                'destination_id' => 'sometimes',
                'base_rate_id' => 'required',
            ],[
                'name.required' => 'Rate Name is Required',
                'effectivity_date.required' => 'Effectivity Date is Required',
                'commodity_type_id.required' => 'Commodity Type is Required',
                'base_rate_id.required' => 'Base Rate is Required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab2of2($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            if ($request['shipment_type_id'] == 1) {

                $validator = Validator::make($request, [
                    'amount_servicemode_1_ptp_10' => 'required',
                    'amount_servicemode_1_ptp_20' => 'required',
                    'amount_servicemode_1_ptp_40' => 'required',
                    'aw1ptp10' => 'required',
                    'aw1ptp20' => 'required',
                    'aw1ptp40' => 'required',
                    'amount_servicemode_2_ptp_10' => 'required',
                    'amount_servicemode_2_ptp_20' => 'required',
                    'amount_servicemode_2_ptp_40' => 'required',
                    'aw2ptp10' => 'required',
                    'aw2ptp20' => 'required',
                    'aw2ptp40' => 'required',
                    'amount_servicemode_3_ptp_10' => 'required',
                    'amount_servicemode_3_ptp_20' => 'required',
                    'amount_servicemode_3_ptp_40' => 'required',
                    'aw3ptp10' => 'required',
                    'aw3ptp20' => 'required',
                    'aw3ptp40' => 'required',
                    'amount_servicemode_4_ptp_10' => 'required',
                    'amount_servicemode_4_ptp_20' => 'required',
                    'amount_servicemode_4_ptp_40' => 'required',
                    'aw4ptp10' => 'required',
                    'aw4ptp20' => 'required',
                    'aw4ptp40' => 'required',
                    'ancillary_charge_id' => 'sometimes',
                ],[
                    'amount_servicemode_1_ptp_10.required' => 'Amount is required',
                    'amount_servicemode_1_ptp_20.required' => 'Amount is required',
                    'amount_servicemode_1_ptp_40.required' => 'Amount is required',
                    'amount_servicemode_2_ptp_10.required' => 'Amount is required',
                    'amount_servicemode_2_ptp_20.required' => 'Amount is required',
                    'amount_servicemode_2_ptp_40.required' => 'Amount is required',
                    'amount_servicemode_3_ptp_10.required' => 'Amount is required',
                    'amount_servicemode_3_ptp_20.required' => 'Amount is required',
                    'amount_servicemode_3_ptp_40.required' => 'Amount is required',
                    'amount_servicemode_4_ptp_10.required' => 'Amount is required',
                    'amount_servicemode_4_ptp_20.required' => 'Amount is required',
                    'amount_servicemode_4_ptp_40.required' => 'Amount is required',
                    
                ]);
            } else {

                $validator = Validator::make($request, [
                    'amount_weight_1' => 'required',
                    'amount_weight_2' => 'required',
                    'amount_weight_3' => 'required',
                    'amount_weight_4' => 'required',
                    'aw1' => 'required',
                    'aw2' => 'required',
                    'aw3' => 'required',
                    'aw4' => 'required',
                    'ancillary_charge_id' => 'required',

                ],[
                    'amount_weight_1.required' => 'Amount is required',
                    'amount_weight_2.required' => 'Amount is required',
                    'amount_weight_3.required' => 'Amount is required',
                    'amount_weight_4.required' => 'Amount is required',
                ]);
            }
            // $validator = Validator::make(
            //     $request,
            //     [
                    
            //     ]
            // );

            // dd($validator);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createSea($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidationTab1of1($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }


            $validated1 = $response['result'];
            $response = $this->createValidationTab2of2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            // dd($validated1);

            $validated2 = $response['result'];
            $validated = array_merge($validated1, $validated2);

            // dd($validated);

            $transportmode = CrmTransportMode::where('name', $validated['transport_mode_id'])->first();

            $approver = CrmRateLoa::where('rate_category', 2)->first();

            $details = CrmRateSeaFreight::with(['RateSeafreightHasMany'])->findOrFail($validated['base_rate_id']);

            $seafreightdata = CrmRateSeaFreight::findOrFail($validated['base_rate_id']);

            // dd($seafreightdata->is_vice_versa);
            // dd($details);
            // dd($details['shipment_type_id']);

            if (count($details->RateSeafreightHasMany) > 0) {
                $s_sea = CrmRateSeaFreight::create([
                    'created_by' => Auth::user()->id,
                    'name' => $validated['name'],
                    'effectivity_date' => $validated['effectivity_date'],
                    'description' => $validated['description'],
                    'ancillary_charge_id' => $validated['ancillary_charge_id'],
                    'is_vice_versa' => $seafreightdata->is_vice_versa,
                    'transport_mode_id' => $transportmode->id,
                    'commodity_type_id' => $validated['commodity_type_id'],
                    'apply_for_id' => ($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL),
                    'base_rate_id' => $validated['base_rate_id'],
                    'shipment_type_id' => $details->shipment_type_id,
                    'approver1_id' => $approver->approver1_id,
                    'approver2_id' => $approver->approver2_id,
                    'final_status_id' => 1,
                    'approver1_status_id' => 1,
                    'approver2_status_id' => 1,
                ]);

                $collectionSeafreightReference = $this->initializeFirestore()->collection('crm_rate_seafreight');
                $documentSeafreightReference = $collectionSeafreightReference->add();
                $documentSeafreightReference->set([
                    'created_by' => Auth::user()->id,
                    'name' => $validated['name'],
                    'effectivity_date' => $validated['effectivity_date'],
                    'description' => $validated['description'],
                    'ancillary_charge_id' => $validated['ancillary_charge_id'],
                    'is_vice_versa' => $seafreightdata->is_vice_versa,
                    'transport_mode_id' => $transportmode->id,
                    'commodity_type_id' => intval($validated['commodity_type_id']),
                    'apply_for_id' => intval(($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL)),
                    'base_rate_id' => intval($validated['base_rate_id']),
                    'shipment_type_id' => $details->shipment_type_id,
                    'approver1_id' => $approver->approver1_id,
                    'approver2_id' => $approver->approver2_id,
                    'final_status_id' => 1,
                    'approver1_status_id' => 1,
                    'approver2_status_id' => 1,
                ]);

                foreach ($details->RateSeafreightHasMany as $seadetails) {
                    // dd($details['shipment_type_id']);

                    // dd($seadetails['size']);
                if ($details['shipment_type_id'] == 1) {

                    if ($seadetails['size'] == 10) {
                        $seadetail_s = CrmRateSeaFreightDetails::create([
                            'rate_id' => $s_sea->id,
                            'origin_id' => $seadetails['origin_id'],
                            'destination_id' => $seadetails['destination_id'],
                            'size' => $seadetails['size'],
                            'amount_servicemode_1' => ($validated['aw1ptp10'] == 1 ? $seadetails['amount_servicemode_1'] + $validated['amount_servicemode_1_ptp_10'] : $seadetails['amount_servicemode_1'] - $validated['amount_servicemode_1_ptp_10']),
                            'amount_servicemode_2' => ($validated['aw2ptp10'] == 1 ? $seadetails['amount_servicemode_2'] + $validated['amount_servicemode_2_ptp_10'] : $seadetails['amount_servicemode_2'] - $validated['amount_servicemode_2_ptp_10']),
                            'amount_servicemode_3' => ($validated['aw3ptp10'] == 1 ? $seadetails['amount_servicemode_3'] + $validated['amount_servicemode_3_ptp_10'] : $seadetails['amount_servicemode_3'] - $validated['amount_servicemode_3_ptp_10']),
                            'amount_servicemode_4' => ($validated['aw4ptp10'] == 1 ? $seadetails['amount_servicemode_4'] + $validated['amount_servicemode_4_ptp_10'] : $seadetails['amount_servicemode_4'] - $validated['amount_servicemode_4_ptp_10']),
                            'is_primary' => $seadetails['is_primary'],
                        ]);
                    }

                    if ($seadetails['size'] == 20) {
                        $seadetail_s = CrmRateSeaFreightDetails::create([
                            'rate_id' => $s_sea->id,
                            'origin_id' => $seadetails['origin_id'],
                            'destination_id' => $seadetails['destination_id'],
                            'size' => $seadetails['size'],
                            'amount_servicemode_1' => ($validated['aw1ptp20'] == 1 ? $seadetails['amount_servicemode_1'] + $validated['amount_servicemode_1_ptp_20'] : $seadetails['amount_servicemode_1'] - $validated['amount_servicemode_1_ptp_20']),
                            'amount_servicemode_2' => ($validated['aw2ptp20'] == 1 ? $seadetails['amount_servicemode_2'] + $validated['amount_servicemode_2_ptp_20'] : $seadetails['amount_servicemode_2'] - $validated['amount_servicemode_2_ptp_20']),
                            'amount_servicemode_3' => ($validated['aw3ptp20'] == 1 ? $seadetails['amount_servicemode_3'] + $validated['amount_servicemode_3_ptp_20'] : $seadetails['amount_servicemode_3'] - $validated['amount_servicemode_3_ptp_20']),
                            'amount_servicemode_4' => ($validated['aw4ptp20'] == 1 ? $seadetails['amount_servicemode_4'] + $validated['amount_servicemode_4_ptp_20'] : $seadetails['amount_servicemode_4'] - $validated['amount_servicemode_4_ptp_20']),
                            'is_primary' => $seadetails['is_primary'],
                        ]);
                    }

                    if ($seadetails['size'] == 40) {
                        $seadetail_s = CrmRateSeaFreightDetails::create([
                            'rate_id' => $s_sea->id,
                            'origin_id' => $seadetails['origin_id'],
                            'destination_id' => $seadetails['destination_id'],
                            'size' => $seadetails['size'],
                            'amount_servicemode_1' => ($validated['aw1ptp40'] == 1 ? $seadetails['amount_servicemode_1'] + $validated['amount_servicemode_1_ptp_40'] : $seadetails['amount_servicemode_1'] - $validated['amount_servicemode_1_ptp_40']),
                            'amount_servicemode_2' => ($validated['aw2ptp40'] == 1 ? $seadetails['amount_servicemode_2'] + $validated['amount_servicemode_2_ptp_40'] : $seadetails['amount_servicemode_2'] - $validated['amount_servicemode_2_ptp_40']),
                            'amount_servicemode_3' => ($validated['aw3ptp40'] == 1 ? $seadetails['amount_servicemode_3'] + $validated['amount_servicemode_3_ptp_40'] : $seadetails['amount_servicemode_3'] - $validated['amount_servicemode_3_ptp_40']),
                            'amount_servicemode_4' => ($validated['aw4ptp40'] == 1 ? $seadetails['amount_servicemode_4'] + $validated['amount_servicemode_4_ptp_40'] : $seadetails['amount_servicemode_4'] - $validated['amount_servicemode_4_ptp_40']),
                            'is_primary' => $seadetails['is_primary'],
                        ]);
                    }

                }else{

                    $seadetail_s = CrmRateSeaFreightDetails::create([
                        'rate_id' => $s_sea->id,
                        'origin_id' => $seadetails['origin_id'],
                        'destination_id' => $seadetails['destination_id'],
                        'amount_weight_1' => ($validated['aw1'] == 1 ? $seadetails['amount_weight_1'] + $validated['amount_weight_1'] : $seadetails['amount_weight_1'] - $validated['amount_weight_1']),
                        'amount_weight_2' => ($validated['aw2'] == 1 ? $seadetails['amount_weight_2'] + $validated['amount_weight_2'] : $seadetails['amount_weight_2'] - $validated['amount_weight_2']),
                        'amount_weight_3' => ($validated['aw3'] == 1 ? $seadetails['amount_weight_3'] + $validated['amount_weight_3'] : $seadetails['amount_weight_3'] - $validated['amount_weight_3']),
                        'amount_weight_4' => ($validated['aw4'] == 1 ? $seadetails['amount_weight_4'] + $validated['amount_weight_4'] : $seadetails['amount_weight_4'] - $validated['amount_weight_4']),
                        'is_primary' => $seadetails['is_primary'],
                    ]);

                }

                $collectionSeafreightDeetsReference = $this->initializeFirestore()->collection('crm_rate_seafreight_details');
                $documentSeafreightDeetsReference = $collectionSeafreightDeetsReference->add();
                $documentSeafreightDeetsReference->set([
                    'rate_id' => $s_sea->id,
                    'origin_id' => $seadetails['origin_id'],
                    'destination_id' => $seadetails['destination_id'],
                    'size' => $seadetails['size'],
                    'amount_weight_1' => ($validated['aw1'] == 1 ? $seadetails['amount_weight_1'] + $validated['amount_weight_1'] : $seadetails['amount_weight_1'] - $validated['amount_weight_1']),
                    'amount_weight_2' => ($validated['aw2'] == 1 ? $seadetails['amount_weight_2'] + $validated['amount_weight_2'] : $seadetails['amount_weight_2'] - $validated['amount_weight_2']),
                    'amount_weight_3' => ($validated['aw3'] == 1 ? $seadetails['amount_weight_3'] + $validated['amount_weight_3'] : $seadetails['amount_weight_3'] - $validated['amount_weight_3']),
                    'amount_weight_4' => ($validated['aw4'] == 1 ? $seadetails['amount_weight_4'] + $validated['amount_weight_4'] : $seadetails['amount_weight_4'] - $validated['amount_weight_4']),
                    'amount_servicemode_1' => $seadetail_s->amount_servicemode_1,
                    'amount_servicemode_2' => $seadetail_s->amount_servicemode_2,
                    'amount_servicemode_3' => $seadetail_s->amount_servicemode_3,
                    'amount_servicemode_4' => $seadetail_s->amount_servicemode_4,
                    'is_primary' => $seadetails['is_primary'],
                ]);
                }
                // dd($s_sea);

            } else {
                return $this->response(500, 'Something Went Wrong', 'No rates found.');
            }
        


            DB::commit();

            return $this->response(200, 'Rate has been successfully created!', compact($s_sea, $seadetail_s));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
