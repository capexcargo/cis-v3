<?php

namespace App\Repositories\Crm\Commercials\Crating;

use App\Models\CrmRateBox;
use App\Models\CrmRateLoa;
use App\Traits\ResponseTrait;
use App\Models\CrmRateCrating;
use App\Models\CrmTransportMode;
use App\Models\CrmRateBoxDetails;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Traits\InitializeFirestoreTrait;
use Illuminate\Support\Facades\Validator;
use App\Interfaces\Crm\Commercials\Crating\CratingInterface;


class CratingRepository implements CratingInterface
{
    use ResponseTrait, InitializeFirestoreTrait;

    public function index($request, $search_request)
    {
        DB::beginTransaction();
        try {
            $crate_s = CrmRateCrating::with(
                'CrateTypeReference',
                'RateApplyReference',
                'App1',
                'App2',
                'App1StatusReference',
                'App2StatusReference',
                'FinalStatusReference',
                'CreatedBy',
            )->when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', 'like', '%' . $search_request['name'] . '%');
            })
                ->when($search_request['description'] ?? false, function ($query) use ($search_request) {
                    $query->where('description', 'like', '%' . $search_request['description'] . '%');
                })
                ->when($search_request['final_status_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('final_status_id', $search_request['final_status_id']);
                })
                ->when($request['stats'], function ($query) use ($request) {
                    if ($request['stats'] == false) {
                        $query->whereIs('crating_type_id', [1, 2]);
                    } else {
                        $query->where('crating_type_id', $request['stats']);
                    }
                    // dd($request['stats']);
                })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'List', compact('crate_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab1($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'created_by' => 'required',
                'name' => 'required',
                'effectivity_date' => 'required',
                'description' => 'sometimes',
                'crating_type_id' => 'required',
                'apply_for_id' => 'sometimes',
            ],[
                'name.required' => 'Rate Name is required',
                'effectivity_date.required' => 'Effectivity Date is required',
                'crating_type_id.required' => 'Crating Type is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab2($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'amount_per_cbm' => 'required',
                'ancillary_charge' => 'required',
            ],[
                'amount_per_cbm.required' => 'Amount is required',
                'ancillary_charge.required' => 'Ancillary Charges is required',
            ]);
            // dd($request['rates']);
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidationTab1($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated1 = $response['result'];
            $response = $this->createValidationTab2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            // dd($validated1);

            $validated2 = $response['result'];
            $validated = array_merge($validated1, $validated2);

            $approver = CrmRateLoa::where('rate_category', 7)->first();

            $s_crate = CrmRateCrating::create([
                'created_by' => Auth::user()->id,
                'name' => $validated['name'],
                'effectivity_date' => $validated['effectivity_date'],
                'description' => $validated['description'],
                'crating_type_id' => $validated['crating_type_id'],
                'amount_per_cbm' => $validated['amount_per_cbm'],
                'ancillary_charge' => $validated['ancillary_charge'],
                'apply_for_id' => ($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL),
                'approver1_id' => $approver->approver1_id,
                'approver2_id' => $approver->approver2_id,
                'final_status_id' => 1,
                'approver1_status_id' => 1,
                'approver2_status_id' => 1,
            ]);

            DB::commit();

            return $this->response(200, 'Rate has been successfully created!', compact($s_crate));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab1of1($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'created_by' => 'required',
                'name' => 'required',
                'effectivity_date' => 'required',
                'description' => 'sometimes',
                'crating_type_id' => 'required',
                'apply_for_id' => 'sometimes',
                'base_rate_id' => 'required',
            ],[
                'name.required' => 'Rate Name is required',
                'effectivity_date.required' => 'Effectivity Date is required',
                'crating_type_id.required' => 'Crating Type is required',
                'base_rate_id.required' => 'Base Rate is required',
            ]);


            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab2of2($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [

                'amount_per_cbm' => 'required',
                'ancillary_charge' => 'required',
                'cbm' => 'required',
                'charge' => 'required',
            ],[
                'amount_per_cbm.required' => 'Amount is required',
                'ancillary_charge.required' => 'Ancillary Charges is required',
                'cbm.required' => 'CBM is required',
                'charge.required' => 'Charge is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createCrating($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidationTab1of1($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated1 = $response['result'];
            $response = $this->createValidationTab2of2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            // dd($validated1);
            $validated2 = $response['result'];
            $validated = array_merge($validated1, $validated2);

            $approver = CrmRateLoa::where('rate_category', 7)->first();

            $details = CrmRateCrating::where('id', $validated['base_rate_id'])->get();

            // dd($approver);

            if (count($details) > 0) {

                foreach ($details as $cratedetails) {
                    $s_crate = CrmRateCrating::create([
                        'created_by' => Auth::user()->id,
                        'name' => $validated['name'],
                        'effectivity_date' => $validated['effectivity_date'],
                        'description' => $validated['description'],
                        'crating_type_id' => $validated['crating_type_id'],
                        'apply_for_id' => ($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL),
                        'base_rate_id' => $validated['base_rate_id'],
                        'approver1_id' => $approver->approver1_id,
                        'approver2_id' => $approver->approver2_id,
                        'final_status_id' => 1,
                        'approver1_status_id' => 1,
                        'approver2_status_id' => 1,

                        'amount_per_cbm' => ($validated['cbm'] == 1 ? $cratedetails['amount_per_cbm'] + $validated['amount_per_cbm'] : $cratedetails['amount_per_cbm'] - $validated['amount_per_cbm']),
                        'ancillary_charge' => ($validated['charge'] == 1 ? $cratedetails['ancillary_charge'] + $validated['ancillary_charge'] : $cratedetails['ancillary_charge'] - $validated['ancillary_charge']),

                    ]);
                    //firestore crate
                    $collectionCrateReference = $this->initializeFirestore()->collection('crm_rate_crating');
                    $documentCrateReference = $collectionCrateReference->add();
                    $documentCrateReference->set([
                        'created_by' => Auth::user()->id,
                        'name' => $validated['name'],
                        'effectivity_date' => $validated['effectivity_date'],
                        'description' => $validated['description'],
                        'crating_type_id' => intval($validated['crating_type_id']),
                        'apply_for_id' => intval(($validated['apply_for_id'] != '' ? $validated['apply_for_id'] : NULL)),
                        'base_rate_id' => intval($validated['base_rate_id']),
                        'approver1_id' => $approver->approver1_id,
                        'approver2_id' => $approver->approver2_id,
                        'final_status_id' => 1,
                        'approver1_status_id' => 1,
                        'approver2_status_id' => 1,
                        'amount_per_cbm' => ($validated['cbm'] == 1 ? $cratedetails['amount_per_cbm'] + $validated['amount_per_cbm'] : $cratedetails['amount_per_cbm'] - $validated['amount_per_cbm']),
                        'ancillary_charge' => ($validated['charge'] == 1 ? $cratedetails['ancillary_charge'] + $validated['ancillary_charge'] : $cratedetails['ancillary_charge'] - $validated['ancillary_charge']),
                    ]);
                }
            } else {
                return $this->response(500, 'Something Went Wrong', 'No rates found.');
            }


            DB::commit();

            return $this->response(200, 'Rate has been successfully created!', compact($s_crate));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
