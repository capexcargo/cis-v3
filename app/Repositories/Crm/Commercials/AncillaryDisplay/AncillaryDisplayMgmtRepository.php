<?php

namespace App\Repositories\Crm\Commercials\AncillaryDisplay;

use App\Interfaces\Crm\Commercials\AncillaryDisplay\AncillaryDisplayMgmtInterface;
use App\Models\Crm\CrmAncillaryDisplay;
use App\Models\Crm\CrmAncillaryDisplayDetails;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class AncillaryDisplayMgmtRepository implements AncillaryDisplayMgmtInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $ancillary_charges_displays = CrmAncillaryDisplay::paginate($request['paginate']);
            // dd($ancillary_charges_displays);
            DB::commit();

            return $this->response(200, 'List', compact('ancillary_charges_displays'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required|unique:crm_ancillary_display_management,name',
            ];

            foreach ($request['ancillarycharges'] as $i => $ancillarycharge) {
                $rules['ancillarycharges.' . $i . '.id'] = 'sometimes';
                $rules['ancillarycharges.' . $i . '.ancillary_charge_id'] = 'required';
            }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'ancillarycharges.*.ancillary_charge_id.required' => 'This field is Required.',
                    'name.required' => 'This field is Required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $ancillarydisplay = CrmAncillaryDisplay::create([
                'name' => $validated['name'],
                'status' => 1,
            ]);

            foreach ($validated['ancillarycharges'] as $i => $ancillarycharge) {
                $ancillarydisplaydetails = CrmAncillaryDisplayDetails::create([
                    'ancillary_display_id' => $ancillarydisplay->id,
                    'ancillary_charge_id' => $validated['ancillarycharges'][$i]['ancillary_charge_id'],
                ]);
            }

            DB::commit();

            return $this->response(200, 'Ancillary Charges item has been added!', compact($ancillarydisplaydetails, $ancillarydisplay));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name' => 'required|unique:crm_ancillary_display_management,name,' . $id,
            ];

            foreach ($request['ancillarycharges'] as $i => $ancillarycharge) {
                $rules['ancillarycharges.' . $i . '.id'] = 'sometimes';
                $rules['ancillarycharges.' . $i . '.ancillary_charge_id'] = 'required';
            }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    // 'ancillarycharges.*.ancillary_charge_id.required' => 'Ancillary Charges is required.',
                    'ancillarycharges.*.ancillary_charge_id.required' => 'This field is Required.',
                    'name.required' => 'This field is Required.',
                ]
            );


            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $ancillary = CrmAncillaryDisplay::with('AncillaryDisplayDetails')->findOrFail($id);
            if (!$ancillary) {
                return $this->response(404, 'Loa', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Ancillary Display', $ancillary);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $ancillary = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($ancillary->AncillaryDisplayDetails);

            $ancillary->update([
                'name' => $validated['name'],
            ]);

            foreach ($request['ancillarycharges'] as $i => $ancillarydisplay) {
                if ($request['ancillarycharges'][$i]['id'] && $request['ancillarycharges'][$i]['is_deleted']) {
                    $ancillarys = $ancillary->AncillaryDisplayDetails()->find($ancillarydisplay['id']); //query all in database

                    if ($ancillarys) {
                        $ancillarys->delete();
                    }
                } else {
                    $ancillary->AncillaryDisplayDetails()->updateOrCreate(
                        [
                            'id' => $ancillarydisplay['id'],
                        ],
                        [
                            'ancillary_display_id' => $ancillary->id,
                            'ancillary_charge_id' => $validated['ancillarycharges'][$i]['ancillary_charge_id'],
                        ]
                    );
                }
            }
            // foreach()
            DB::commit();

            return $this->response(200, 'Ancillary Charges has been updated!', compact($ancillary));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
