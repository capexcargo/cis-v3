<?php

namespace App\Repositories\Crm\Contracts\RequirementsMgmt;

use App\Interfaces\Crm\Contracts\RequirementsMgmt\RequirementsMgmtInterface;
use App\Interfaces\Crm\Sales\Opportunities\OpportunityStatus\OpportunityStatusInterface;
use App\Models\CrmApplicationRequirements;
use App\Models\CrmOpportunityStatusMgmt;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class RequirementsMgmtRepository implements RequirementsMgmtInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $req_s = CrmApplicationRequirements::
            
                paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'List', compact('req_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'name' => 'required',
            ],[
                'name.required' => 'Application Requirement field is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $req_s = CrmApplicationRequirements::create([
                'name' => $validated['name'],
                'status' => 1,
            ]);

            DB::commit();

            return $this->response(200, 'Application Requirements has been Successfully Created!', $req_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $req_s = CrmApplicationRequirements::findOrFail($id);
            if (!$req_s) {
                return $this->response(404, 'Application Requiremnts', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Application Requiremnts', $req_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $req_s = $response['result'];

            $response = $this->validation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $req_s->update([
                'name' => $validated['name'],
            ]);

            DB::commit();

            return $this->response(200, 'Application Requirements has been successfully updated!', $req_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
         
    // public function destroy($id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $req_s = CrmApplicationRequirements::find($id);
    //         if (!$req_s) return $this->response(404, 'Application Requirements', 'Not Found!');
    //         $req_s->delete();

    //         DB::commit();
    //         return $this->response(200, 'Accounts Application Requirements Successfully Deleted!', $req_s);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }
}
