<?php

namespace App\Repositories\Crm\Contracts\AccountsApplication;

use App\Interfaces\Crm\Contracts\AccountsApplication\AccountsApplicationInterface;
use App\Interfaces\Crm\Sales\Opportunities\OpportunityStatus\OpportunityStatusInterface;
use App\Models\Crm\CrmAccountApplicationAttachment;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\CrmApplicationRequirements;
use App\Models\CrmOpportunityStatusMgmt;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;



class AccountsApplicationRepository implements AccountsApplicationInterface
{
    use ResponseTrait;

    public function index($request, $search_request)
    {
        // dd($search_request);
        DB::beginTransaction();
        try {
            $acc_s = CrmCustomerInformation::with(
                'contactOwner'
            )
                ->when($search_request['created_at'] ?? false, function ($query) use ($search_request) {
                    $query->where('created_at', 'like', '%' . $search_request['created_at'] . '%');
                })
                ->when($search_request['account_no'] ?? false, function ($query) use ($search_request) {
                    $query->where('account_no', 'like', '%' . $search_request['account_no'] . '%');
                })
                ->when($search_request['company_name'] ?? false, function ($query) use ($search_request) {
                    $query->where('company_name', 'like', '%' . $search_request['company_name'] . '%');
                })
                ->where('account_type', 2)
                ->when($request['stats'], function ($query) use ($request) {
                    if ($request['stats'] == 3) {
                        $query->whereNull('final_status');
                    } else {
                        $query->where('final_status', $request['stats']);
                    }
                })

                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'List', compact('acc_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // public function validation($request)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $validator = Validator::make($request, [
    //             'name' => 'required',
    //         ]);

    //         if ($validator->fails()) {
    //             return $this->response(400, 'Please Fill Required Field', $validator->errors());
    //         }

    //         DB::commit();

    //         return $this->response(200, 'Successfully Validated', $validator->validated());
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    // public function create($request)
    // {

    //     DB::beginTransaction();
    //     try {
    //         $response = $this->validation($request);

    //         if ($response['code'] != 200) {
    //             return $this->response($response['code'], $response['message'], $response['result']);
    //         }
    //         $validated = $response['result'];

    //         $req_s = CrmApplicationRequirements::create([
    //             'name' => $validated['name'],
    //             'status' => 1,
    //         ]);

    //         DB::commit();

    //         return $this->response(200, 'Requirements has been Successfully Created!', $req_s);
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    public function create($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $applications = array_filter($request['applications'], function ($application) {
                return $application['file'];
            });

            foreach ($applications as $application) {
                // dd($request);
                // $CrmAccountApplicationAttachment = CrmAccountApplicationAttachment::find();

                $response = $this->storeImage($application['file']);
                if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
                $photo = $response['result'];
                // dd($application['file']->extension());

                $attach = CrmAccountApplicationAttachment::updateOrCreate([
                    'account_info_id' => $request['set_account_info_id'],
                    'application_requirement_id' => $request['set_requirement_id'],
                ],[
                    'path' => $photo['file_path'],
                    'name' => $photo['file_name'],
                    'extension' => $application['file']->extension(),
                ]);
            }

            DB::commit();
            return $this->response(200, 'Accounts Application Attachment Successfully Created', $attach);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function storeImage($image)
    {
        DB::beginTransaction();
        try {
            $year = date('Y', strtotime(now()));
            $file_path = strtolower(str_replace(" ", "_", 'contracts/accounts_application/' . $year . '/'));
            $file_name = date('mdYHis') . uniqid() . '.' . $image->extension();

            $image_resize = Image::make($image->getRealPath())->resize(1024, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            Storage::disk('crm_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
            DB::commit();
            return $this->response(200, 'Successfuly Save', compact('file_path', 'file_name'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $acc_s = CrmCustomerInformation::with(
                'accountType',
                'industry',
                'lifeStage',
                'contactOwner',
                'marketingChannel',
                'statusRef',
                'emails',
                'mobileNumbers',
                'telephoneNumbers',
                'addresses',
                'contactPersons'
            )->findOrFail($id);
            if (!$acc_s) {
                return $this->response(404, 'Accounts Application', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Accounts Application', $acc_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // public function update($request, $id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $response = $this->show($id);
    //         if ($response['code'] != 200) {
    //             return $this->response($response['code'], $response['message'], $response['result']);
    //         }
    //         $req_s = $response['result'];

    //         $response = $this->validation($request, $id);
    //         if ($response['code'] != 200) {
    //             return $this->response($response['code'], $response['message'], $response['result']);
    //         }
    //         $validated = $response['result'];

    //         $req_s->update([
    //             'name' => $validated['name'],
    //         ]);

    //         DB::commit();

    //         return $this->response(200, 'Application Requirements has been successfully updated!', $req_s);
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $acc_app_img = CrmAccountApplicationAttachment::find($id);
            if (!$acc_app_img) return $this->response(404, 'Accounts Application', 'Not Found!');
            $acc_app_img->delete();

            DB::commit();
            return $this->response(200, 'Accounts Application Successfully Deleted!', $acc_app_img);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
