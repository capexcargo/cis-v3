<?php

namespace App\Repositories\Crm\ServiceRequest\Response;

use App\Interfaces\Crm\CustomerInformation\MarketingChannelInterface;
use App\Interfaces\Crm\ServiceRequest\Response\ResponseMInterface;
use App\Interfaces\Crm\ServiceRequest\SRType\SRTypeInterface;
use App\Models\MarketingChannel;
use App\Models\MileResponse;
use App\Models\SrType;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class ResponseMRepository implements ResponseMInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $m_responses = MileResponse::paginate($request['paginate']);

            // dd($sr_typess);


            DB::commit();

            return $this->response(200, 'Response', compact('m_responses'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'email' => 'required',
                'target_response_time' => 'required',
            ],[
                'email.required' => 'The Email Address field is required',
                'target_response_time.required' => 'The Target Response Time field is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validationUpdate($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'email' => 'required',
                'target_response_time' => 'required',
            ],[
                'email.required' => 'The Email Address field is required',
                'target_response_time.required' => 'The Target Response Time field is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            // dd($response);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $res = MileResponse::create([
                'email' => $validated['email'],
                'target_response_time' => $validated['target_response_time'],
            ]);

            DB::commit();

            return $this->response(200, 'New Response has been successfully added!', $res);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

         public function show($id)
        {
            DB::beginTransaction();
            try {
                $resp_res = MileResponse::findOrFail($id);
                if (!$resp_res) {
                    return $this->response(404, 'Response', 'Not Found!');
                }

                DB::commit();

                return $this->response(200, 'Response', $resp_res);
            } catch (\Exception $e) {
                DB::rollback();

                return $this->response(500, 'Something Went Wrong', $e->getMessage());
            }
        }

        public function update($request, $id)
        {
            DB::beginTransaction();
            try {
                $response = $this->show($id);
                if ($response['code'] != 200) {
                    return $this->response($response['code'], $response['message'], $response['result']);
                }
                $resp_res = $response['result'];

                $response = $this->validation($request, $id);
                if ($response['code'] != 200) {
                    return $this->response($response['code'], $response['message'], $response['result']);
                }
                $validated = $response['result'];

                $resp_res->update([
                    'email' => $validated['email'],
                    'target_response_time' => $validated['target_response_time'],
                ]);

                DB::commit();

                return $this->response(200, 'New Response has been successfully saved!', $resp_res);
            } catch (\Exception $e) {
                DB::rollback();

                return $this->response(500, 'Something Went Wrong', $e->getMessage());
            }
        }


        public function destroy($id)
        {
            DB::beginTransaction();
            try {
                $del = MileResponse::find($id);
                if (!$del) return $this->response(404, 'Response', 'Not Found!');
                $del->delete();

                DB::commit();
                return $this->response(200, 'Response Successfully Deleted!', $del);
            } catch (\Exception $e) {
                DB::rollback();
                return $this->response(500, 'Something Went Wrong', $e->getMessage());
            }
        }

}
