<?php

namespace App\Repositories\Crm\ServiceRequest\Hierarchy;


use App\Interfaces\Crm\ServiceRequest\Hierarchy\HierarchyInterface;
use App\Models\CrmHeirarchyDetails;
use App\Models\Hierarchy;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class HierarchyRepository implements HierarchyInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $hierarchy_s = Hierarchy::with('tagHasMany')->


                paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Channel SR Source', compact('hierarchy_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // public function getposition($request)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $hierarchy_s = Hierarchy::where('email', $request)->get();

    //         DB::commit();

    //         return $this->response(200, 'Email position', compact('hierarchy_s'));
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }
    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'email' => 'required|unique:crm_hierarchy,email',
            ];

            foreach ($request['positions'] as $i => $position) {
                $rules['positions.' . $i . '.id'] = 'sometimes';
                $rules['positions.' . $i . '.tagged_position_id'] = 'required';
            }

            $validator = Validator::make($rules, [
                [
                    'positions.*.tagged_position_id.required' => 'The Tag field is required.',

                ],
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

  

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            // dd($response);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            
            $s_hierarchy = Hierarchy::create([
                'email' => $request['email'],
            ]);

            foreach ($request['positions'] as $i => $position) {
                $pos = CrmHeirarchyDetails::create([
                    'heirarchy_id' => $s_hierarchy->id,
                    'tagged_position_id' => $request['positions'][$i]['tagged_position_id'],

                ]);
            }

            DB::commit();

            return $this->response(200, 'New Hierarchy has been successfully added!', compact("s_hierarchy", "pos"));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updatevalidation($request, $id)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $rules = [
                'email' => 'required|unique:crm_hierarchy,email,' . $id . ',id'
            ];

            foreach ($request['positions'] as $i => $position) {
                $rules['positions.' . $i . '.id'] = 'sometimes';
                $rules['positions.' . $i . '.tagged_position_id'] = 'required';
                $rules['positions.' . $i . '.is_deleted'] = 'sometimes';

            }

            $validator = Validator::make($rules, [
                [
                    'positions.*.tagged_position_id.required' => 'The Tagged Position field is required.',

                ],
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

     public function show($id)
    {
        DB::beginTransaction();
        try {
            $hie_result = Hierarchy::with('tagHasMany')->findOrFail($id);
            if (!$hie_result) {
                return $this->response(404, 'Hierarchy', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Hierarchy', $hie_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $hie_management = $response['result'];
           
            $response = $this->updatevalidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            // dd($response);
            $validated = $response['result'];
           
            $hie_management->updateOrCreate([
                'id' => $id,
            ], [
                'email' => $request['email'],
            ]);
           
            foreach ($request['positions'] as $a => $position) {
             
                if ($request['positions'][$a]['id'] && $request['positions'][$a]['is_deleted']) 
                {
                    $tags = CrmHeirarchyDetails::find($request['positions'][$a]['id']); //query all in database

                    if ($tags) {
                        $tags->delete();
                    }
                } else {
                    // dd($request['positions']);
                    $tags = CrmHeirarchyDetails::updateOrCreate(
                        [
                            'id' => $request['positions'][$a]['id'],
                        ],
                        [
                            'heirarchy_id' => $hie_management->id,
                            'tagged_position_id' => $request['positions'][$a]['tagged_position_id'],
                        ]
                    );
                }

            }
            

            DB::commit();

            return $this->response(200, 'Hierarchy has been successfully saved!', compact($hie_management, $tags));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


}
