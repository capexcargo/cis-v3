<?php

namespace App\Repositories\Crm\ServiceRequest\Channel;

use App\Interfaces\Crm\CustomerInformation\MarketingChannelInterface;
use App\Interfaces\Crm\ServiceRequest\Channel\ChannelInterface;
use App\Interfaces\Crm\ServiceRequest\SRType\SRTypeInterface;
use App\Models\ChannelSrSource;
use App\Models\MarketingChannel;
use App\Models\SrType;
use App\Traits\ResponseTrait;
use Illuminate\Broadcasting\Channel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class ChannelRepository implements ChannelInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $m_channels = ChannelSrSource::


                paginate($request['paginate']);

            // dd($sr_typess);


            DB::commit();

            return $this->response(200, 'Channel Source', compact('m_channels'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'channel' => 'required',
            ],[
                'channel.required' => 'The Channel Source field is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validationUpdate($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'channel' => 'required',
            ],[
                'channel.required' => 'The Channel Source field is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            // dd($response);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $m_channel = ChannelSrSource::create([
                'name' => $validated['channel'],
            ]);

            DB::commit();

            return $this->response(200, 'New Channel Source has been successfully added!', $m_channel);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

     public function show($id)
    {
        DB::beginTransaction();
        try {
            $sr_result = ChannelSrSource::findOrFail($id);
            if (!$sr_result) {
                return $this->response(404, 'Channel Source', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Channel Source', $sr_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $sr_result = $response['result'];

            $response = $this->validationUpdate($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $sr_result->update([
                'name' => $validated['channel'],
            ]);

            DB::commit();

            return $this->response(200, 'Channel Source has been successfully saved!', $sr_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $del = ChannelSrSource::find($id);
            if (!$del) return $this->response(404, 'Channel Source', 'Not Found!');
            $del->delete();

            DB::commit();
            return $this->response(200, 'Channel Source successfully deleted!', $del);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    
}