<?php

namespace App\Repositories\Crm\ServiceRequest\Service;

use App\Interfaces\Crm\CustomerInformation\MarketingChannelInterface;
use App\Interfaces\Crm\ServiceRequest\Channel\ChannelInterface;
use App\Interfaces\Crm\ServiceRequest\Service\ServiceInterface;
use App\Interfaces\Crm\ServiceRequest\SRType\SRTypeInterface;
use App\Models\ChannelSrSource;
use App\Models\MarketingChannel;
use App\Models\ServiceRequirements;
use App\Models\SrType;
use App\Traits\ResponseTrait;
use Illuminate\Broadcasting\Channel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class ServiceRepository implements ServiceInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $m_services = ServiceRequirements::


                paginate($request['paginate']);

            // dd($sr_typess);


            DB::commit();

            return $this->response(200, 'Service Requirements', compact('m_services'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'service' => 'required',
            ],[
                'service.required' => 'The Service Requirements field is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validationUpdate($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'service' => 'required',
            ],[
                'service.required' => 'The Service Requirements field is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            // dd($response);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $m_service = ServiceRequirements::create([
                'name' => $validated['service'],
            ]);

            DB::commit();

            return $this->response(200, 'Service Requirements has been Successfully Created!', $m_service);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

     public function show($id)
    {
        DB::beginTransaction();
        try {
            $sr_result = ServiceRequirements::findOrFail($id);
            if (!$sr_result) {
                return $this->response(404, 'Service Requirements', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Service Requirements', $sr_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $sr_result = $response['result'];

            $response = $this->validation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $sr_result->update([
                'name' => $validated['service'],
            ]);

            DB::commit();

            return $this->response(200, 'Service Requirements has been successfully updated!', $sr_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $del = ServiceRequirements::find($id);
            if (!$del) return $this->response(404, 'Service Requirements', 'Not Found!');
            $del->delete();

            DB::commit();
            return $this->response(200, 'Service Requirements Successfully Deleted!', $del);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    
}