<?php

namespace App\Repositories\Crm\ServiceRequest;

use App\Interfaces\Crm\ServiceRequest\ServiceRequestMgmtInterface;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmLead;
use App\Models\Crm\CrmServiceRequest;
use App\Models\Crm\CrmSrEmailDetails;
use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Webklex\IMAP\Facades\Client;
use Webklex\PHPIMAP\Support\MessageCollection;
use App\Mail\HTMLMail;

class ServiceRequestMgmtRepository implements ServiceRequestMgmtInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $all_sr = CrmServiceRequest::count();

            $closed_sr = CrmServiceRequest::whereHas('statusRef', function ($query) use ($request) {
                $query->where('id', 3);
            })->count();

            $teams_sr = CrmServiceRequest::whereHas('assignedTo', function ($query) use ($request) {
                $query->where('level_id', '>=', 3);
            })->count();

            $my_open_sr = CrmServiceRequest::whereHas('assignedTo', function ($query) use ($request) {
                $query->where('id', Auth::user()->id);
            })->count();

            $service_requests = CrmServiceRequest::withCount(
                ['srEmailDetails as count_thread']
            )
                ->withCount(
                    ['srEmailDetails as read' => function ($query) {
                        $query->where('is_read', 1);
                    }]
                )
                ->when($request['card_header'] == 2, function ($query)  use ($request) {
                    $query->where('status_id', 3);
                })
                ->when($request['card_header'] == 3, function ($query)  use ($request) {
                    $query->whereHas('assignedTo', function ($query) use ($request) {
                        $query->where('level_id', '>=', 3);
                    });
                })
                ->when($request['card_header'] == 4, function ($query)  use ($request) {
                    $query->whereHas('assignedTo', function ($query) use ($request) {
                        $query->where('id', Auth::user()->id);
                    });
                })
                ->when($request['severity'], function ($query)  use ($request) {
                    $query->where('severity_id', $request['severity']);
                })
                ->when($request['sr_no'], function ($query)  use ($request) {
                    $query->where('sr_no', $request['sr_no']);
                })
                ->when($request['date_created'], function ($query)  use ($request) {
                    $query->whereDate('created_at', $request['date_created']);
                })
                ->when($request['status'], function ($query)  use ($request) {
                    $query->where('status_id', $request['status']);
                })
                ->when($request['sort_by'], function ($query) use ($request) {
                    $query->orderBy('status_id', $request['sort_by']);
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Service Request List', compact('all_sr', 'closed_sr', 'teams_sr', 'my_open_sr', 'service_requests'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function send($request)
    {
        DB::beginTransaction();
        try {

            $compose_email = [];
            $sr_email_details = [];

            // if (Auth::user()->email == 'yarnsupt1.capex@gmail.com') {
            $client = Client::account('email1');
            $client->connect();
            $folder = $client->getFolder('Sent Mail');
            // $folder = $client->getFolders(); // use this to check the name of folders in mail

            $mailData = [];

            $mailData['sr_number'] = $request['sr_number'];
            $mailData['to'] = $request['to'];
            $mailData['from'] = 'yarnsupt1.capex@gmail.com';
            $mailData['subject'] = $request['subject'];
            $mailData['body'] = $request['body'] .
                "<br><br><br><div>
                    The reference number of your Service Request is <span style='font-weight: 600'>" . $request['sr_number'] . "</span>.
                    </div>";
            $searchText = strval($mailData['sr_number']);

            //must compose email first
            $compose_email = Mail::to($mailData['to'])->send(new HTMLMail($mailData));

            // after email composed
            if ($folder) {
                $emails = $folder->query()->text($searchText)->get();

                if (!empty($emails)) {

                    foreach ($emails as $email) {
                        $mailData['message_id'] = $email->getMessageId();
                        $mailData['references'] = $email->getReferences();
                        $mailData['subject'] = $email->getSubject();
                        $mailData['body'] = $email->getHTMLBody(true);
                    }

                    $sr_email_details = CrmSrEmailDetails::create(
                        [
                            'sr_no' => $mailData['sr_number'],
                            'email_external' => $mailData['to'],
                            'email_internal' =>  'yarnsupt1.capex@gmail.com',
                            // // 'email_internal' =>  Auth::user()->email,  // must be same as email from config > imap
                            'subject' => $mailData['subject'],
                            'body' => $mailData['body'],
                            'message_id' => $mailData['message_id'],
                            'references' => $mailData['references'],
                            'status' => 1,
                            'type' => 3,
                            'is_first_email' => 1,
                            'is_read' => 1,
                        ]
                    );
                }
            }
            // }

            DB::commit();

            return $this->response(200, 'Email Sent.', compact('compose_email', 'sr_email_details'));
            // return $this->response(200, 'Email Sent.', compact('mailData', 'folder'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function retrieveEmail()
    {
        DB::beginTransaction();
        try {
            // $client = Client::account('default');
            $client = Client::account('email1');
            $client->connect();

            $client2 = Client::account('email2');
            $client2->connect();

            $folders = $client->getFolders();
            $folders2 = $client2->getFolders();

            $mailData = [];
            $mailData2 = [];

            $subject = [];
            $subject2 = [];
            $existing_emails = [];
            $existing_emails2 = [];

            $sr_email_details = [];
            $sr_email_details2 = [];

            // --- For auto assigned --- //
            $service_request_holders = User::withCount([
                'serviceRequest as new' => function ($query) { // assigned
                    $query->where('status_id', 1);
                },
                'serviceRequest as in_progress' => function ($query) {
                    $query->where('status_id', 2);
                },
                'serviceRequest as resolved' => function ($query) {
                    $query->where('status_id', 3);
                }
            ])->where('division_id', 2)
                ->get();

            foreach ($service_request_holders as $i => $sr_holder) {
                $serviceRequestHolders[] = [
                    'id' => $sr_holder->id,
                    'serviceRequestHolders' => $sr_holder->name,
                    'new' => $sr_holder->new,
                    'in_progress' => $sr_holder->in_progress,
                    'resolved' => $sr_holder->resolved,
                ];

                foreach ($serviceRequestHolders as $j => $sr) {
                    $new[$i] = $sr['new'];

                    $srholder[$j] = [
                        'new' => ($sr['new'] + $sr['in_progress']),
                        'id' => $sr['id']
                    ];
                }
            }
            // --- For auto assigned --- //

            foreach ($folders as $i => $folder) {
                $messages = $folder->query()->unseen()->since(now()->subDays(10))->markAsRead()->get();

                $last_sr_no = CrmServiceRequest::select('sr_no')->latest()->first();
                $last_inserted_sr_no = (int)substr($last_sr_no->sr_no ?? "SRN", 3);

                foreach ($messages as $i => $message) {

                    $count_sr = $last_inserted_sr_no += 1;
                    $sr_no = "SRN" . $count_sr;

                    $subject[$i] = str_replace("Re: ", "", $message->getSubject());
                    $existing_emails = CrmSrEmailDetails::where('subject', $subject[$i])->latest()->first();

                    if (empty($existing_emails)) {

                        $customer_no = CrmCustomerInformation::select('id')->latest()->first();
                        if ($customer_no) {
                            $last_inserted_id = $customer_no->id + 1;
                        } else {
                            $last_inserted_id = 1;
                        }

                        $customer_number = "CN-" . str_pad(($last_inserted_id), 5, "0", STR_PAD_LEFT);

                        $sender_removed_quotes = str_replace(['"', "'"], '', $message->getFrom()[0]->personal);
                        $senderNames = explode(' ', $sender_removed_quotes);
                        $firstName = $senderNames[0];
                        $lastName = count($senderNames) > 1 ? end($senderNames) : '';

                        $sr_email_details = CrmCustomerInformation::create([
                            'account_type' => 1,
                            'account_no' => $customer_number,
                            'fullname' =>  $sender_removed_quotes,
                            'first_name' => $firstName,
                            'last_name' => $lastName,
                            'life_stage_id' => 3,
                            'onboarding_id' => 1,
                            'customer_type' => 1,
                            'status' => 1,
                            'created_by' => Auth::user()->id,
                        ]);

                        $sr_email_details->contactPersons()->create([
                            'first_name' => $firstName,
                            'last_name' => $lastName,
                            'position' => "N/A",
                            'is_primary' => true,
                        ]);

                        $service_request = $sr_email_details->serviceRequest()->create([
                            'sr_no' => $sr_no,
                            'account_id' => $sr_email_details->id,
                            'subject' => $message->getSubject(),
                            'service_request_type_id' => 1,
                            'severity_id' => 1,
                            // 'sub_category_id' => 1,
                            'status_id' => 2,
                            'life_stage_id' => 1,
                            'assigned_to_id' => min($srholder)['id'],
                            'channel_id' => 5,
                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id,
                        ]);

                        $service_request->srEmailDetails()->create(
                            [
                                'sr_no' => $sr_no,
                                'email_external' => $message->getFrom()[0]->mail,
                                'email_internal' =>  $message->getTo()[0]->mail,
                                'subject' => $message->getSubject(),
                                'body' => $message->getHTMLBody(true),
                                'message_id' => $message->getMessageId(),
                                'references' => $message->getReferences(),
                                'status' => 1,
                                'type' => 1,
                                'is_first_email' => 1,
                            ]
                        );

                        // //save to activity
                        // $sr_email_details->activities()->create(
                        //     [
                        //         'employee_id' =>  min($srholder)['id'],
                        //         'activity_type_id' => 5,
                        //         'account_id' => null,
                        //         'description' => $message->getSubject(),
                        //         'sr_no' => $sr_no,
                        //         'quotation_reference_no' => null,
                        //         'status' => 1,
                        //         'datetime_completed' => null,
                        //     ]
                        // );

                        //system generated reply
                        if (empty($existing_emails) && $sr_email_details) {

                            $mailData['email_from'] = $message->getFrom()[0]->mail;
                            $mailData['email_to'] = $message->getTo()[0]->mail;
                            $mailData['headers'] = $message->getHeader();
                            $mailData['message_id'] = $message->getMessageId();
                            $mailData['references'] = $message->getReferences();
                            $mailData['subject'] = $message->getSubject();
                            $mailData['body'] = $message->getHTMLBody(true);

                            Mail::send('emails.email-sys-generated-temp', $mailData, function ($message) use ($mailData) {

                                $message->getHeaders()->addTextHeader('In-Reply-To', $mailData['message_id']);
                                $message->getHeaders()->addTextHeader('References', $mailData['references']);

                                $message->from($mailData['email_to'], 'Marvin Esguerra');
                                $message->sender($mailData['email_to'], 'Marvin Esguerra');
                                $message->to($mailData['email_from']);
                                $message->replyTo($mailData['email_to']);
                                $message->subject("Re: " . $mailData['subject']);
                                $message->subject($mailData['subject']);
                            });
                        }
                    } else {
                        $sr_email_details = CrmSrEmailDetails::updateOrCreate(
                            [
                                // 'body' => $message->getHTMLBody(true),
                                'subject' => $subject[$i],
                                'sr_no' => $existing_emails->sr_no,
                                'message_id' => $message->getMessageId(),
                                'references' => $message->getReferences(),
                            ],
                            [
                                'sr_no' => $existing_emails->sr_no,
                                'email_external' => $message->getFrom()[0]->mail,
                                'email_internal' =>  $message->getTo()[0]->mail,
                                'subject' => $message->getSubject(),
                                'body' => $message->getHTMLBody(true),
                                'message_id' => $message->getMessageId(),
                                'references' => $message->getReferences(),
                                'status' => 1,
                                'type' => 1,
                            ]
                        );
                    }
                }
            }

            foreach ($folders2 as $i => $folder) {
                $messages = $folder->query()->unseen()->since(now()->subDays(10))->markAsRead()->get();

                $last_sr_no = CrmServiceRequest::select('sr_no')->latest()->first();
                $last_inserted_sr_no = (int)substr($last_sr_no->sr_no ?? "SRN", 3);

                foreach ($messages as $i => $message) {

                    $count_sr = $last_inserted_sr_no += 1;
                    $sr_no = "SRN" . $count_sr;

                    $subject2[$i] = str_replace("Re: ", "", $message->getSubject());
                    $existing_emails2 = CrmSrEmailDetails::where('subject', $subject2[$i])->latest()->first();

                    if (empty($existing_emails2)) {

                        $customer_no = CrmCustomerInformation::select('id')->latest()->first();
                        if ($customer_no) {
                            $last_inserted_id = $customer_no->id + 1;
                        } else {
                            $last_inserted_id = 1;
                        }

                        $customer_number = "CN-" . str_pad(($last_inserted_id), 5, "0", STR_PAD_LEFT);

                        $sender_removed_quotes = str_replace(['"', "'"], '', $message->getFrom()[0]->personal);
                        $senderNames = explode(' ', $sender_removed_quotes);
                        $firstName = $senderNames[0];
                        $lastName = count($senderNames) > 1 ? end($senderNames) : '';

                        $sr_email_details2 = CrmCustomerInformation::create([
                            'account_type' => 1,
                            'account_no' => $customer_number,
                            'fullname' =>  $sender_removed_quotes,
                            'first_name' => $firstName,
                            'last_name' => $lastName,
                            'life_stage_id' => 3,
                            'onboarding_id' => 1,
                            'customer_type' => 1,
                            'status' => 1,
                            'created_by' => Auth::user()->id,
                        ]);

                        $sr_email_details2->contactPersons()->create([
                            'first_name' => $firstName,
                            'last_name' => $lastName,
                            'position' => "N/A",
                            'is_primary' => true,
                        ]);

                        $service_request = $sr_email_details->serviceRequest()->create(
                            [
                                'sr_no' => $sr_no,
                                'account_id' => $sr_email_details2->id,
                                'subject' => $message->getSubject(),
                                'service_request_type_id' => 1,
                                'severity_id' => 1,
                                // 'sub_category_id' => 1,
                                'status_id' => 2,
                                'life_stage_id' => 1,
                                'assigned_to_id' => min($srholder)['id'],
                                'channel_id' => 5,
                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id,
                            ]
                        );

                        $service_request->srEmailDetails()->create(
                            [
                                'sr_no' => $sr_no,
                                'email_external' => $message->getFrom()[0]->mail,
                                'email_internal' =>  $message->getTo()[0]->mail,
                                'subject' => $message->getSubject(),
                                'body' => $message->getHTMLBody(true),
                                'message_id' => $message->getMessageId(),
                                'references' => $message->getReferences(),
                                'status' => 1,
                                'type' => 1,
                            ]
                        );

                        // //save to activity
                        // $sr_email_details2->activities()->create(
                        //     [
                        //         'employee_id' =>  min($srholder)['id'],
                        //         'activity_type_id' => 5,
                        //         'account_id' => null,
                        //         'description' => $message->getSubject(),
                        //         'sr_no' => $sr_no,
                        //         'quotation_reference_no' => null,
                        //         'status' => 1,
                        //         'datetime_completed' => null,
                        //     ]
                        // );

                        //system generated reply
                        if (empty($existing_emails2) && $sr_email_details2) {

                            $mailData2['email_from'] = $message->getFrom()[0]->mail;
                            $mailData2['email_to'] = $message->getTo()[0]->mail;
                            $mailData2['headers'] = $message->getHeader();
                            $mailData2['message_id'] = $message->getMessageId();
                            $mailData2['references'] = $message->getReferences();
                            $mailData2['subject'] = $message->getSubject();
                            $mailData2['body'] = $message->getHTMLBody(true);

                            Mail::send('emails.email-sys-generated-temp', $mailData2, function ($message) use ($mailData2) {

                                $message->getHeaders()->addTextHeader('In-Reply-To', $mailData2['message_id']);
                                $message->getHeaders()->addTextHeader('References', $mailData2['references']);

                                $message->from($mailData2['email_to'], 'Marvin Esguerra');
                                $message->sender($mailData2['email_to'], 'Marvin Esguerra');
                                $message->to($mailData2['email_from']);
                                $message->replyTo($mailData2['email_to']);
                                $message->subject("Re: " . $mailData2['subject']);
                                $message->subject($mailData2['subject']);
                            });
                        }
                    } else {
                        $sr_email_details2 = CrmSrEmailDetails::updateOrCreate(
                            [
                                // 'body' => $message->getHTMLBody(true),
                                'subject' => $subject2[$i],
                                'sr_no' => $existing_emails2->sr_no,
                                'message_id' => $message->getMessageId(),
                                'references' => $message->getReferences(),
                            ],
                            [
                                'sr_no' => $existing_emails2->sr_no,
                                'email_external' => $message->getFrom()[0]->mail,
                                'email_internal' =>  $message->getTo()[0]->mail,
                                'subject' => $message->getSubject(),
                                'body' => $message->getHTMLBody(true),
                                'message_id' => $message->getMessageId(),
                                'references' => $message->getReferences(),
                                'status' => 1,
                                'type' => 1,
                            ]
                        );
                    }
                }
            }

            DB::commit();

            return $this->response(200, 'Service Request Email Details', compact('sr_email_details', 'sr_email_details2'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function retrieveReplyEmail($sr_no)
    {
        DB::beginTransaction();
        try {
            // $client = Client::account('default');
            $client = Client::account('email1');
            $client->connect();

            $client2 = Client::account('email2');
            $client2->connect();

            $folders = $client->getFolders();
            $folders2 = $client2->getFolders();

            $mailData = [];
            $mailData2 = [];

            $sr_email_details = [];
            $sr_email_details2 = [];

            $get_subject = CrmSrEmailDetails::where('sr_no', $sr_no)->latest()->first();
            $subject = str_replace("Re: ", "", $get_subject->subject);

            $update_is_read = CrmSrEmailDetails::where('sr_no', $sr_no)->where('is_read', 0)->update(['is_read' => 1]);

            foreach ($folders as $folder) {
                $messages = $folder->query()->text(strval($subject))->markAsRead()->get();

                foreach ($messages as $i => $message) {
                    $sr_email_details = CrmSrEmailDetails::updateOrCreate(
                        [
                            'body' => $message->getHTMLBody(true),
                            'subject' => $message->getSubject(),
                            'sr_no' => $sr_no,
                            'message_id' => $message->getMessageId(),
                        ],
                        [
                            'sr_no' => $sr_no,
                            'email_external' => $message->getFrom()[0]->mail,
                            'email_internal' =>  $message->getTo()[0]->mail,
                            'subject' => $message->getSubject(),
                            'body' => $message->getHTMLBody(true),
                            'message_id' => $message->getMessageId(),
                            'references' => $message->getReferences(),
                            'status' => 1,
                            'type' => 1,
                            'is_read' => 1,
                        ]
                    );
                }
            }

            foreach ($folders2 as $folder) {
                $messages = $folder->query()->text(strval($subject))->markAsRead()->get();

                foreach ($messages as $i => $message) {
                    $sr_email_details2 = CrmSrEmailDetails::updateOrCreate(
                        [
                            'body' => $message->getHTMLBody(true),
                            'subject' => $message->getSubject(),
                            'sr_no' => $sr_no,
                            'message_id' => $message->getMessageId(),
                        ],
                        [
                            'sr_no' => $sr_no,
                            'email_external' => $message->getFrom()[0]->mail,
                            'email_internal' =>  $message->getTo()[0]->mail,
                            'subject' => $message->getSubject(),
                            'body' => $message->getHTMLBody(true),
                            'message_id' => $message->getMessageId(),
                            'references' => $message->getReferences(),
                            'status' => 1,
                            'type' => 1,
                            'is_read' => 1,
                        ]
                    );
                }
            }
            DB::commit();

            return $this->response(200, 'Service Request Email Details', compact('sr_email_details', 'sr_email_details2', 'update_is_read'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function sendReply($sr_no, $body, $messageId)
    {
        // dd($sr_no, $body);
        DB::beginTransaction();
        try {
            $client = Client::account('email1');
            $client->connect();

            $client2 = Client::account('email2');
            $client2->connect();

            $folder = $client->getFolder('INBOX');
            $folder2 = $client2->getFolder('INBOX');
            // $folders2 = $client2->getFolders();

            $mailData = [];
            $mailData2 = [];

            $sr_email_details = [];
            $sr_email_details2 = [];

            if ($folder) {
                // foreach ($folders as $folder) {
                // $messages = $folder->query()->text(strval($sr_no))->get();

                $initialEmail = $folder->search()->messageId($messageId)->get()->first();

                if ($initialEmail) {
                    // Get the thread using the same message ID
                    $thread = $folder->search()->messageId($messageId)->get();

                    // Add the initial email to the thread
                    $messages = new MessageCollection([$initialEmail]);

                    // Merge the rest of the emails with the same message ID to the thread
                    $messages = $messages->merge($thread);

                    foreach ($messages as $i => $message) {

                        $get_existing = CrmSrEmailDetails::where('sr_no', $sr_no)->where('type', 1)->latest()->first();

                        $mailData['subject'] = $get_existing->subject;

                        $mailData['email_from'] = $message->getFrom()[0]->mail;
                        $mailData['email_to'] = $message->getTo()[0]->mail;
                        $mailData['headers'] = $message->getHeader();
                        $mailData['message_id'] = $message->getMessageId();
                        $mailData['references'] = $message->getReferences();
                        // $mailData['subject'] = $message->getSubject();
                        // $mailData['subject'] = str_replace("Re: ", "", $message->getSubject());
                    }

                    $sr_email_details = CrmSrEmailDetails::create(
                        [
                            'sr_no' => $sr_no,
                            'email_external' =>  $mailData['email_from'],
                            'email_internal' =>  $mailData['email_to'],
                            'subject' => $mailData['subject'],
                            'body' => nl2br($body),
                            'message_id' => $mailData['message_id'],
                            'references' => $mailData['references'],
                            'status' => 1,
                            'type' => 2,
                            // 'is_first_email' => 1,
                            'is_read' => 1,
                        ]
                    );

                    Mail::raw($body, function ($message) use ($mailData) {
                        $message->getHeaders()->addTextHeader('In-Reply-To', $mailData['message_id']);
                        $message->getHeaders()->addTextHeader('References', $mailData['references']);

                        $message->from($mailData['email_to'], 'Marvin Esguerra');
                        $message->sender($mailData['email_to'], 'Marvin Esguerra');
                        $message->to($mailData['email_from']);
                        $message->replyTo($mailData['email_to']);
                        $message->subject("Re: " . $mailData['subject']);
                        $message->subject($mailData['subject']);
                    });
                }
            }

            if ($folder2) {

                $initialEmail = $folder2->search()->messageId($messageId)->get()->first();

                if ($initialEmail) {
                    // Get the thread using the same message ID
                    $thread = $folder2->search()->messageId($messageId)->get();

                    // Add the initial email to the thread
                    $messages = new MessageCollection([$initialEmail]);

                    // Merge the rest of the emails with the same message ID to the thread
                    $messages = $messages->merge($thread);

                    foreach ($messages as $i => $message) {

                        $get_existing = CrmSrEmailDetails::where('sr_no', $sr_no)->where('type', 1)->latest()->first();

                        $mailData2['subject'] = $get_existing->subject;

                        $mailData2['email_from'] = $message->getFrom()[0]->mail;
                        $mailData2['email_to'] = $message->getTo()[0]->mail;
                        $mailData2['headers'] = $message->getHeader();
                        $mailData2['message_id'] = $message->getMessageId();
                        $mailData2['references'] = $message->getReferences();
                    }


                    $sr_email_details2 = CrmSrEmailDetails::create(
                        [
                            'sr_no' => $sr_no,
                            'email_external' => $message->getFrom()[0]->mail,
                            'email_internal' =>  $message->getTo()[0]->mail,
                            'subject' => $message->getSubject(),
                            'body' => nl2br($body),
                            'message_id' => $message->getMessageId(),
                            'references' => $message->getReferences(),
                            'status' => 1,
                            'type' => 2,
                            'is_read' => 1,
                        ]
                    );

                    Mail::raw($body, function ($message) use ($mailData2) {
                        $message->getHeaders()->addTextHeader('In-Reply-To', $mailData2['message_id']);
                        $message->getHeaders()->addTextHeader('References', $mailData2['references']);

                        $message->from($mailData2['email_to'], 'Marvin Esguerra');
                        $message->sender($mailData2['email_to'], 'Marvin Esguerra');
                        $message->to($mailData2['email_from']);
                        $message->replyTo($mailData2['email_to']);
                        $message->subject($mailData2['subject']);
                    });
                }
            }

            DB::commit();

            return $this->response(200, 'Reply Sent.', compact('sr_email_details', 'sr_email_details2'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'subject' => 'required',
                // 'company_business_name' => 'sometimes',
                // 'primary_contact_person' => 'required',
                // 'mobile_number' => 'sometimes',
                // 'telephone_number' => 'sometimes',
                // 'email_address' => 'required',
                'sr_type' => 'required',
                'severity' => 'required',
                'service_requirement' => 'required',
                'assigned_to' => 'required',
                'channel_sr_source' => 'required',
                'status' => 'required',
                'sr_description' => 'required',

                'attachments' => 'nullable',
                'attachments.*.attachment' => 'nullable|' . config('filesystems.validation_all')
            ];

            if ($request['account_type'] == 1) {
                $rules['primary_contact_person'] = 'required';
                $rules['company_business_name'] = 'sometimes';
            } else {
                $rules['primary_contact_person'] = 'sometimes';
                $rules['company_business_name'] = 'required';
            }

            // if ($request['sr_type'] > 1) {
            //     $rules['subcategory'] = 'required';

            //     if ($request['sr_type'] == 2 && $request['subcategory'] == 1) {
            //         $rules['booking_ref_no'] = 'required';
            //     } elseif ($request['sr_type'] == 2 && $request['subcategory'] == 2) {
            //         $rules['waybill_no'] = 'required';
            //     }
            // } else {
            //     $rules['subcategory'] = 'sometimes';
            //     $rules['booking_ref_no'] = 'sometimes';
            //     $rules['waybill_no'] = 'sometimes';
            // }

            if ($request['sr_type'] == 2 || $request['sr_type'] == 4) {
                $rules['subcategory'] = 'required';

                if ($request['sr_type'] == 2 && $request['subcategory'] == 1) {
                    $rules['booking_ref_no'] = 'required';
                } elseif ($request['sr_type'] == 2 && $request['subcategory'] == 2) {
                    $rules['waybill_no'] = 'required';
                }
            } elseif ($request['sr_type'] == 3) {
                $rules['service_requirement'] = 'sometimes';

                $rules['subcategory'] = 'sometimes';
                $rules['booking_ref_no'] = 'sometimes';
                $rules['waybill_no'] = 'sometimes';
            }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'sr_description.required' => 'The Service request description field is required.',
                    'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
                ]
            );

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $last_sr_no = CrmServiceRequest::select('id')->latest()->first();
            $last_inserted_id = $last_sr_no->id ?? 0 + 1;
            $sr_no = "SRN" . $last_inserted_id;


            $service_request = CrmServiceRequest::create([
                'sr_no' => $sr_no,
                'subject' => $validated['subject'],
                'customer' => $validated['company_business_name'],
                'account_id' => $validated['primary_contact_person'],
                // 'mobile' => $validated['mobile_number'],
                // 'telephone' => $validated['telephone_number'],
                // 'email' => $validated['email_address'],
                'service_request_type_id' => $validated['sr_type'],
                'severity_id' => $validated['severity'],
                'sub_category_id' => $request['subcategory'],
                'sub_category_reference' => $request['subcategory'] == 1 ? $request['booking_ref_no'] : $request['waybill_no'],
                'service_requirement_id' => $validated['service_requirement'],
                'assigned_to_id' => $validated['assigned_to'],
                'channel_id' => $validated['channel_sr_source'],
                'status_id' => $validated['status'],
                'life_stage_id' => 1,
                'description' => $validated['sr_description'],
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
            ]);



            $response = $this->attachments($service_request, $validated['attachments']);

            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            DB::commit();
            return $this->response(200, 'SR has been successfully created!', $service_request);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $sr_result = $response['result'];

            $sr_result->update([
                'service_request_type_id' => $request['sr_type'],
                'severity_id' => $request['severity'],
                'sub_category_id' => $request['subcategory'],
                'sub_category_reference' => $request['subcategory'] == 1 ? $request['booking_ref_no'] : $request['waybill_no'],
                'service_requirement_id' => $request['service_requirement'],
                'assigned_to_id' => $request['assigned_to'],
                'channel_id' => $request['channel_sr_source'],
                'updated_by' => Auth::user()->id,
            ]);

            $customer_data = CrmCustomerInformation::findOrFail($sr_result->account_id ?? $sr_result->customer);

            if ($customer_data) {

                if ($customer_data->account_type == 2) {
                    $customer_data->update([
                        'fullname' => $request['company_business_name'],
                        'company_name' => $request['company_business_name'],
                    ]);
                } else {
                    $customer_data->update([
                        'fullname' => $request['first_name'] . " " . $request['middle_name'] . " " . $request['last_name'],
                        'first_name' => $request['first_name'],
                        'middle_name' => $request['middle_name'],
                        'last_name' => $request['last_name'],
                    ]);
                }

                foreach ($customer_data->contactPersons as $i => $contact_person) {
                    if ($contact_person->is_primary == 1) {
                        $contact_person->update(
                            [
                                'first_name' => $request['first_name'],
                                'middle_name' => $request['middle_name'],
                                'last_name' => $request['last_name'],
                            ]
                        );
                    }
                }

                foreach ($customer_data->mobileNumbers as $i => $mobile_number) {
                    if ($mobile_number->is_primary == 1) {
                        $mobile_number->update([
                            'mobile' => $request['mobile_number'],
                        ]);
                    }
                }

                foreach ($customer_data->telephoneNumbers as $i => $telephone_number) {
                    if ($telephone_number->is_primary == 1) {
                        $telephone_number->update([
                            'telephone' => $request['telephone_number'],
                        ]);
                    }
                }

                foreach ($customer_data->emails as $i => $email_address) {
                    if ($email_address->is_primary == 1) {
                        $email_address->update([
                            'email' => $request['email_address'],
                        ]);
                    }
                }
            }

            DB::commit();

            return $this->response(200, 'SR has been successfully updated!', compact('sr_result', 'customer_data'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateSrDescription($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $sr_data = $response['result'];

            $sr_data->update([
                'description' => $request['sr_description'],
                'updated_by' => Auth::user()->id,
            ]);

            DB::commit();

            return $this->response(200, 'SR description has been successfully updated!', compact('request'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function attachments($service_request, $attachments)
    {
        DB::beginTransaction();
        try {
            $date = date('Y/m/d', strtotime($service_request->created_at));

            foreach ($attachments as $attachment) {

                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $service_request->attachments()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(" ", "_", 'crm/' . $date . '/' . 'service-request/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        Storage::disk('crm_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } else if (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('crm_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }
                    $service_request->attachments()->updateOrCreate(
                        [
                            'id' => $attachment['id']
                        ],
                        [
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                            'original_file_name' =>  $attachment['attachment']->getClientOriginalName(),
                        ]
                    );
                }
            }

            DB::commit();
            return $this->response(200, 'Service request attachment successfully attached', $service_request);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $service_request = CrmServiceRequest::with('attachments', 'srEmailDetails')->withCount(
                ['srEmailDetails as count_thread']
            )
                ->findOrFail($id);

            if (!$service_request) {
                return $this->response(404, 'Service Request', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Service Request', $service_request);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function leadValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'sr_id' => 'required',
                'sr_no' => 'required',
                'account_id' => 'required',
                'lead_name' => 'required',
                'shipment_type' => 'required',
                'service_requirement' => 'required',
                'description' => 'sometimes',
                'contact_owner' => 'required',
                'customer_type' => 'required',
                'channel_source' => 'required',
                'currency' => 'required',
            ];


            $validator = Validator::make(
                $request,
                $rules,
            );

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createLead($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->leadValidation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $lead = CrmLead::create([
                'sr_no' => $validated['sr_no'],
                'account_id' => $validated['account_id'],
                'lead_name' => $validated['lead_name'],
                'lead_status_id' => 1,
                'lead_classification_id' => 1,
                'shipment_type_id' => $validated['shipment_type'],
                'service_requirement_id' => $validated['service_requirement'],
                'description' => $validated['description'],
                'contact_owner_id' => $validated['contact_owner'],
                'customer_type_id' => $validated['customer_type'],
                'channel_source_id' => $validated['channel_source'],
                'currency' => $validated['currency'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'New lead has been successfully created!', $lead);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateSrStatus($id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $sr = $response['result'];

            $sr->update([
                'status_id' => 3,
            ]);

            DB::commit();
            return $this->response(200, 'Sr has been successfully resolved!', $sr);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
