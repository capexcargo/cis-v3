<?php

namespace App\Repositories\Crm\ServiceRequest\Urgency;

use App\Interfaces\Crm\ServiceRequest\Urgency\UrgencyInterface;
use App\Models\MileUrgency;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class UrgencyRepository implements UrgencyInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $m_urgencys = MileUrgency::
                paginate($request['paginate']);
            DB::commit();

            return $this->response(200, 'Urgency', compact('urgencys'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            foreach ($request['urgency_impacts'] as $i => $urgency_impact) {
                $rules['urgency_impacts.' . $i . '.severity'] = 'required';
                $rules['urgency_impacts.' . $i . '.high_priority_level'] = 'required';
                $rules['urgency_impacts.' . $i . '.medium_priority_level'] = 'required';
                $rules['urgency_impacts.' . $i . '.low_priority_level'] = 'required';
            }



            $validator = Validator::make($rules, [
                [
                    'urgency_impacts.*.severity.required' => 'The Urgency field is required.',
                    'urgency_impacts.*.high_priority_level.required' => 'The Impact field is required.',
                    'urgency_impacts.*.medium_priority_level.required' => 'The Impact field is required.',
                    'urgency_impacts.*.low_priority_level.required' => 'The Impact field is required.',

                ]

            ]);



            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            // dd($response);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            // dd($request['urgency_impacts']);

            foreach ($request['urgency_impacts'] as $i => $urgency_impact) {

                if ($request['urgency_impacts'][$i]['id'] && $request['urgency_impacts'][$i]['is_deleted']) 
                {
                    $urgency = MileUrgency::find($request['urgency_impacts'][$i]['id']); //query all in database

                    // dd('urgency');

                    if ($urgency) {
                        $urgency->delete();
                    }
                } else {
                    $urgency = MileUrgency::updateOrCreate(
                        [
                            'id' => $request['urgency_impacts'][$i]['id'],
                        ],
                        [
                            'severity_id' => $request['urgency_impacts'][$i]['severity'],
                            'high_priority_level' => $request['urgency_impacts'][$i]['high_priority_level'],
                            'medium_priority_level' => $request['urgency_impacts'][$i]['medium_priority_level'],
                            'low_priority_level' => $request['urgency_impacts'][$i]['low_priority_level'],
                            'status' => 1,
                        ]
                    );
                }
            }

            // foreach ($request['urgency_impacts'] as $i => $urgency_impact) {

            //     $urg = MileUrgency::create([
            //         'severity_id' => $request['urgency_impacts'][$i]['severity'],
            //         'high_priority_level' => $request['urgency_impacts'][$i]['high_priority_level'],
            //         'medium_priority_level' => $request['urgency_impacts'][$i]['medium_priority_level'],
            //         'low_priority_level' => $request['urgency_impacts'][$i]['low_priority_level'],
            //         'status' => 1,
            //     ]);
            // }


            // dd($urg);

            DB::commit();

            return $this->response(200, 'Urgency and Impact has been successfully saved!', $urgency);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show()
    {
        DB::beginTransaction();
        try {
            $urge = MileUrgency::all();
            if (!$urge) {
                return $this->response(404, 'Urgency and Impact', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Urgency and Impact', $urge);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // public function update($request, $id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $response = $this->show($id);
    //         if ($response['code'] != 200) {
    //             return $this->response($response['code'], $response['message'], $response['result']);
    //         }
    //         $sr_subs = $response['result'];

    //         $response = $this->validation($request, $id);
    //         if ($response['code'] != 200) {
    //             return $this->response($response['code'], $response['message'], $response['result']);
    //         }
    //         $validated = $response['result'];

    //         $sr_subs->update([
    //             'name' => $validated['srsub'],
    //             'sr_type_id' => $validated['category'],
    //         ]);

    //         DB::commit();

    //         return $this->response(200, 'SR Subcategory has been successfully updated!', $sr_subs);
    //     } catch (\Exception $e) {
    //         DB::rollback();

    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }




    // public function destroy($id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $srsc = SrTypeSubcategory::find($id);
    //         if (!$srsc) return $this->response(404, 'SR Subcategory', 'Not Found!');
    //         $srsc->delete();

    //         DB::commit();
    //         return $this->response(200, 'SR Subcategory Successfully Deleted!', $srsc);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }
}
