<?php

namespace App\Repositories\Crm\ServiceRequest\Knowledge;


use App\Interfaces\Crm\ServiceRequest\Knowledge\KnowledgeInterface;
use App\Models\CrmKnowledge;
use App\Models\CrmKnowledgeResponse;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class KnowledgeRepository implements KnowledgeInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {

            $knowledge_s = CrmKnowledge::with('respHasMany')
                ->when($request['stats'], function ($query) use ($request) {
                    if ($request['stats'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['stats']);
                    }
                })->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Knowledge', compact('knowledge_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getposition($request)
    {
        DB::beginTransaction();
        try {
            $knowledge_s = CrmKnowledge::where('faq_concern', $request)->get();

            DB::commit();

            return $this->response(200, 'Knowledge', compact('knowledge_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'faq_concern' => 'required',
            ];

            foreach ($request['positions'] as $i => $position) {
                $rules['positions.' . $i . '.id'] = 'sometimes';
                $rules['positions.' . $i . '.response_area'] = 'required';
            }

            $validator = Validator::make($rules, [
                [
                    'positions.*.response_area.required' => 'The Response field is required.',

                ],
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            // dd($response);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

                $s_knowledge = CrmKnowledge::create([
                'faq_concern' => $request['faq_concern'],
                'status' => 1,
            ]);

            foreach ($request['positions'] as $i => $position) {
                $s_knowledge_r = CrmKnowledgeResponse::create([
                    'knowledge_id' => $s_knowledge->id,
                    'response_area' => $request['positions'][$i]['response_area'],
                ]);
            }

            DB::commit();

            return $this->response(200, 'New Script has been successfully added!', compact($s_knowledge, $s_knowledge_r));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updatevalidation($request)
    {
        DB::beginTransaction();
        try {
    
            $rules = [
                'faq_concern' => 'required',
            ];

            foreach ($request['positions'] as $i => $position) {
                $rules['positions.' . $i . '.id'] = 'sometimes';
                $rules['positions.' . $i . '.response_area'] = 'required';
                $rules['positions.' . $i . '.is_deleted'] = 'sometimes';

            }

            $validator = Validator::make($rules, [
                [
                    'positions.*.response_area.required' => 'The Response field is required.',

                ],
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $know_result = CrmKnowledge::with('respHasMany')->findOrFail($id);
            if (!$know_result) {
                return $this->response(404, 'Knowledge', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Knowledge', $know_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $know_management = $response['result'];

            $response = $this->updatevalidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            
            $know_management->updateOrCreate([
                'id' => $id,
            ], [
                'faq_concern' => $request['faq_concern'],
                'status' => 1,
            ]);
           
            foreach ($request['positions'] as $a => $position) {
             
                if ($request['positions'][$a]['id'] && $request['positions'][$a]['is_deleted']) 
                {
                    $resps = CrmKnowledgeResponse::find($request['positions'][$a]['id']); //query all in database

                    if ($resps) {
                        $resps->delete();
                    }
                } else {
                    // dd($request['positions']);
                    $resps = CrmKnowledgeResponse::updateOrCreate(
                        [
                            'id' => $request['positions'][$a]['id'],
                        ],
                        [
                            'knowledge_id' => $know_management->id,
                            'response_area' => $request['positions'][$a]['response_area'],
                        ]
                    );
                }

            }

            DB::commit();

            return $this->response(200, 'Knowledge has been successfully updated!', compact($know_management, $resps));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }



}
