<?php

namespace App\Repositories\Crm\ServiceRequest\SRType;

use App\Interfaces\Crm\CustomerInformation\MarketingChannelInterface;
use App\Interfaces\Crm\ServiceRequest\SRType\SRTypeInterface;
use App\Models\MarketingChannel;
use App\Models\SrType;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class SRTypeRepository implements SRTypeInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $sr_types = SrType::paginate($request['paginate']);

            // dd($sr_typess);


            DB::commit();

            return $this->response(200, 'SR Type', compact('sr_types'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'srtype' => 'required|unique:crm_sr_type,name',

            ], [
                'srtype.required' => 'SR Type field is required',
                'srtype.unique' => 'SR Type field is already taken',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            // dd($response);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $sr_type = SrType::create([
                'name' => $validated['srtype'],
            ]);

            DB::commit();

            return $this->response(200, 'New SR Type has been successfully added!', $sr_type);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $sr_result = SrType::findOrFail($id);
            if (!$sr_result) {
                return $this->response(404, 'SR Type', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'SR Type', $sr_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validationUpdate($request, $id)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'srtype' => 'required|unique:crm_sr_type,name,' . $id,

            ], [
                'srtype.required' => 'SR Type field is required',
                'srtype.unique' => 'SR Type field is already taken',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $sr_result = $response['result'];

            $response = $this->validationUpdate($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            // dd($validated);

            $sr_result->update([
                'name' => $validated['srtype'],
            ]);

            DB::commit();

            return $this->response(200, 'SR Type has been successfully saved!', $sr_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $del = SrType::find($id);
            if (!$del) return $this->response(404, 'SR Type', 'Not Found!');
            $del->delete();

            DB::commit();
            return $this->response(200, 'SR Type successfully Deleted!', $del);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
