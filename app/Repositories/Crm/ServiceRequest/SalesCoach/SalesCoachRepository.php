<?php

namespace App\Repositories\Crm\ServiceRequest\SalesCoach;


use App\Interfaces\Crm\ServiceRequest\Hierarchy\HierarchyInterface;
use App\Interfaces\Crm\ServiceRequest\SalesCoach\SalesCoachInterface;
use App\Models\CrmSalesCoach;
use App\Models\CrmSalesCoachProcess;
use App\Models\CrmSalesCoachSubprocess;
use App\Models\Hierarchy;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class SalesCoachRepository implements SalesCoachInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $sales_coachs = CrmSalesCoach::with(['processHasMany' => function ($query) {
                $query->with('subprocess_many');
            }])->when($request['stats'], function ($query) use ($request) {
                if ($request['stats'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['stats']);
                }
            })->paginate($request['paginate']);

            // dd($sr_typess);


            DB::commit();

            return $this->response(200, 'Sales Coach', compact('sales_coachs'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    { 
        // dd($request);
        DB::beginTransaction();
        try {
            $rules = [
                'title' => 'required',
                'description' => 'sometimes',
            ];

            foreach ($request['pros'] as $i => $pro) {
                $rules['pros.' . $i . '.id'] = 'sometimes';
                $rules['pros.' . $i . '.process'] = 'required';
            }
            if (isset($request['subpros'][$i])) {
                foreach ($request['subpros'][$i] as $c => $subpro) {

                    $rules['subpros.' . $i . '.' . $c . '.id'] = 'sometimes';
                    $rules['subpros.' . $i . '.' . $c . '.sub_process'] = 'required';
                }
                $validator = Validator::make($rules, [
                    [
                        'pros.*.process.required' => 'The Process field is required.',
                        'subpros.*.*.sub_process.required' => 'The SUBProcess field is required.',

                    ],
                ]);
            } else {
                $validator = Validator::make($rules, [
                    [
                        'pros.*.process.required' => 'The Process field is required.',

                    ],
                ]);
            }


            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {

        DB::beginTransaction();
        try {


            $response = $this->validation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }


            $validated = $response['result'];
            // dd($response);
            $sales = CrmSalesCoach::create([
                'title' => $request['title'],
                'description' => $request['description'],
                'status' => 1,
                'created_by' => Auth::user()->id,
            ]);

            foreach ($request['pros'] as $a => $processes) {
                $process = CrmSalesCoachProcess::create([
                    'sales_coach_id' => $sales->id,
                    'process_content' => $request['pros'][$a][$a]['process'],
                ]);

                $b = 0;

                if (isset($request['subpros'][$a])) {
                    foreach ($request['subpros'][$a] as $c => $subpro) {

                        $subprocess = CrmSalesCoachSubprocess::create([
                            'sales_coach_process_id' => $process->id,
                            'process_content' => $request['subpros'][$a][$c]['sub_process'],
                        ]);



                        $b++;
                    }
                }

                // dd($request['subpros']);
            }


            DB::commit();
            return $this->response(200, 'New Sales Coach has been successfully added', compact($process, $sales));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $sales_coach = CrmSalesCoach::with('process_many')->findOrFail($id);

            if (!$sales_coach) {
                return $this->response(404, 'Sales Coach', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Sales Coach', $sales_coach);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function update($request, $id)
    {
        // dd(count($request['pros'][2][2]));
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $sales_coach = $response['result'];

            $response = $this->validation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];


            $sales_coach->updateOrCreate([
                'id' => $id,
            ], [
                'title' => $request['title'],
                'description' => $request['description'],
                'created_by' => Auth::user()->id,
            ]);

            foreach ($request['pros'] as $a => $processes) {
                if (count($request['pros'][$a][$a]) >= 3) {

                    if ($request['pros'][$a][$a]['id'] && $request['pros'][$a][$a]['is_deleted'] == true) {

                        $sales_coach_process_details = CrmSalesCoachProcess::find($request['pros'][$a][$a]['id']);
                        $sales_coach_process_details->delete();
                    } else {

                        $coachprocess = CrmSalesCoachProcess::updateOrCreate([
                            'id' => $request['pros'][$a][$a]['id'],
                        ], [
                            'process_content' => $request['pros'][$a][$a]['process'],
                            'sales_coach_id' => $id,
                        ]);
                    }
                } else {
                    if (count($request['pros'][$a][$a]) < 3) {
                        $test = CrmSalesCoachProcess::create([
                            'sales_coach_id' => $id,
                            'process_content' => $request['pros'][$a][$a]['process'],
                        ]);
                    }
                }


                $b = 0;
                if (isset($request['subpros'][$a])) {
                    foreach ($request['subpros'][$a] as $c => $subpro) {

                        // dd($request['subpros'][$a][$a]['id']);
                            if ($request['subpros'][$a][$c]['id'] && $request['subpros'][$a][$c]['is_deleted'] == true) {

                                $sales_coach_subprocess_details = CrmSalesCoachSubprocess::find($request['subpros'][$a][$c]['id']);
                                $sales_coach_subprocess_details->delete();
                            } else {


                                CrmSalesCoachSubprocess::updateOrCreate([
                                    'id' => $request['subpros'][$a][$c]['id'],
                                ], [
                                    'process_content' => $request['subpros'][$a][$c]['sub_process'],
                                    'sales_coach_process_id' => ($request['pros'][$a][$a]['id'] == "" ? $coachprocess->id : $request['pros'][$a][$a]['id']),
                                ]);
                            }


                        $b++;
                    }
                }
            }




            DB::commit();

            return $this->response(200, 'Sales Coach has been successfully updated!', $sales_coach);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
