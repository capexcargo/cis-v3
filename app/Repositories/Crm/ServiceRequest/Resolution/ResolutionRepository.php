<?php

namespace App\Repositories\Crm\ServiceRequest\Resolution;

use App\Interfaces\Crm\ServiceRequest\Resolution\ResolutionInterface;
use App\Models\MileResolution;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ResolutionRepository implements ResolutionInterface
{

use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $m_resolutions = MileResolution::paginate($request['paginate']);

            // dd($sr_typess);


            DB::commit();

            return $this->response(200, 'Resolution', compact('m_resolutions'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'priority_level' => 'required',
                'severity' => 'required',
                'target_response_time' => 'required',
            ],[
                'priority_level.required' => 'The Priority Level field is required',
                'severity.required' => 'The Severity field is required',
                'target_response_time.required' => 'The Target Response Time field is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validationUpdate($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'priority_level' => 'required',
                'severity' => 'required',
                'target_response_time' => 'required',
            ],[
                'priority_level.required' => 'The Priority Level field is required',
                'severity.required' => 'The Severity field is required',
                'target_response_time.required' => 'The Target Response Time field is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            // dd($response);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $res = MileResolution::create([
                'priority_level' => $validated['priority_level'],
                'severity' => $validated['severity'],
                'target_response_time' => $validated['target_response_time'],
            ]);

            DB::commit();

            return $this->response(200, 'New Resolution has been successfully added!', $res);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

         public function show($id)
        {
            DB::beginTransaction();
            try {
                $resp_res = MileResolution::findOrFail($id);
                if (!$resp_res) {
                    return $this->response(404, 'Resolution', 'Not Found!');
                }

                DB::commit();

                return $this->response(200, 'Resolution', $resp_res);
            } catch (\Exception $e) {
                DB::rollback();

                return $this->response(500, 'Something Went Wrong', $e->getMessage());
            }
        }

        public function update($request, $id)
        {
            DB::beginTransaction();
            try {
                $response = $this->show($id);
                if ($response['code'] != 200) {
                    return $this->response($response['code'], $response['message'], $response['result']);
                }
                $resp_res = $response['result'];

                $response = $this->validation($request, $id);
                if ($response['code'] != 200) {
                    return $this->response($response['code'], $response['message'], $response['result']);
                }
                $validated = $response['result'];

                $resp_res->update([
                    'priority_level' => $validated['priority_level'],
                    'severity' => $validated['severity'],
                    'target_response_time' => $validated['target_response_time'],
                ]);

                DB::commit();

                return $this->response(200, 'Resolution has been successfully saved!', $resp_res);
            } catch (\Exception $e) {
                DB::rollback();

                return $this->response(500, 'Something Went Wrong', $e->getMessage());
            }
        }


        public function destroy($id)
        {
            DB::beginTransaction();
            try {
                $del = MileResolution::find($id);
                if (!$del) return $this->response(404, 'Resolution', 'Not Found!');
                $del->delete();

                DB::commit();
                return $this->response(200, 'Resolution Successfully Deleted!', $del);
            } catch (\Exception $e) {
                DB::rollback();
                return $this->response(500, 'Something Went Wrong', $e->getMessage());
            }
        }
    }