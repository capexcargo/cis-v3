<?php

namespace App\Repositories\Crm\ServiceRequest\SrSub;

use App\Interfaces\Crm\ServiceRequest\SRSub\SrSubCategoryInterface;
use App\Interfaces\Hrim\RecruitmentAndHiring\EmploymentCategoryTypeInterface;
use App\Models\Hrim\EmploymentCategoryType;
use App\Models\SrTypeSubcategory;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class SrSubRepository implements SrSubCategoryInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $sr_subs = SrTypeSubcategory::with('SrSubCat')
                //     ->when($request['name'] ?? false, function ($query) use ($request) {
                //         $query->where('name', $request['name']);
                //     })
                //     ->when($request['division_id'] ?? false, function ($query) use ($request) {
                //         $query->where('division_id', $request['division_id']);
                //     })
                //     ->when(Auth::user()->level_id != 5, function ($query) {
                //         $query->where('division_id', Auth::user()->division_id);
                //     })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'List', compact('sr_subs'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'category' => 'required',
                'srsub' => 'required|unique:crm_sr_type_subcategory,name',
            ], [
                'srsub.required' => 'The SR Subcategory field is required',
                'srsub.unique' => 'The SR Subcategory field is already taken',
                'category.required' => 'The Tag field is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $Sr_subc = SrTypeSubcategory::create([
                'name' => $validated['srsub'],
                'sr_type_id' => $validated['category'],
            ]);

            DB::commit();

            return $this->response(200, 'New SR Subcategory has been Successfully added!', $Sr_subc);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $sr_subs = SrTypeSubcategory::findOrFail($id);
            if (!$sr_subs) {
                return $this->response(404, 'SR Subcategory', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'SR Subcategory', $sr_subs);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function validationUpdate($request, $id)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'category' => 'required',
                'srsub' => 'required|unique:crm_sr_type_subcategory,name,' . $id,

            ], [
                'srsub.required' => 'The SR Subcategory field is required',
                'srsub.unique' => 'The SR Subcategory field is already taken',
                'category.required' => 'The Tag field is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $sr_subs = $response['result'];

            $response = $this->validationUpdate($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $sr_subs->update([
                'name' => $validated['srsub'],
                'sr_type_id' => $validated['category'],
            ]);

            DB::commit();

            return $this->response(200, 'SR Subcategory has been successfully saved!', $sr_subs);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }




    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $srsc = SrTypeSubcategory::find($id);
            if (!$srsc) return $this->response(404, 'SR Subcategory', 'Not Found!');
            $srsc->delete();

            DB::commit();
            return $this->response(200, 'SR Subcategory successfully Deleted!', $srsc);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
