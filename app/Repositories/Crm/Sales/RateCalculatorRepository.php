<?php

namespace App\Repositories\Crm\Sales;

use App\Interfaces\Crm\Sales\RateCalculatorInterface;
use App\Models\BranchReference;
use App\Models\Crm\CrmAncillary;
use App\Models\CrmRateAirFreight;
use App\Models\CrmRateAirFreightDetails;
use App\Models\CrmRateSeaFreight;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class RateCalculatorRepository implements RateCalculatorInterface
{
    use ResponseTrait;

    public function compute($request)
    {
        DB::beginTransaction();
        try {
            $rate_cal = [];

            if ($request['transport_mode'] == 1) {
                if ($request['air_cargo']) {
                    $response = $this->computeAirCargo($request);

                    if ($response['code'] != 200) {
                        return $this->response($response['code'], $response['message'], $response['result']);
                    }

                    $rate_cal = $response['result'];
                }

                if ($request['air_pouch']) {
                    $response = $this->computeAirPouch($request);

                    if ($response['code'] != 200) {
                        return $this->response($response['code'], $response['message'], $response['result']);
                    }

                    $rate_cal = $response['result'];
                }

                if ($request['air_box']) {
                    $response = $this->computeAirBox($request);

                    if ($response['code'] != 200) {
                        return $this->response($response['code'], $response['message'], $response['result']);
                    }

                    $rate_cal = $response['result'];
                }
            } elseif ($request['transport_mode'] == 2) {
                if ($request['sea_cargo']) {
                    $response = $this->computeSeaCargo($request);

                    if ($response['code'] != 200) {
                        return $this->response($response['code'], $response['message'], $response['result']);
                    }

                    $rate_cal = $response['result'];
                }

                if ($request['sea_box']) {
                    $response = $this->computeSeaBox($request);

                    if ($response['code'] != 200) {
                        return $this->response($response['code'], $response['message'], $response['result']);
                    }

                    $rate_cal = $response['result'];
                }
            }
            // elseif ($request['transport_mode'] == 3) {
            //     if ($request['land']) {
            //         $response = $this->computeAirBox($request);

            //         if ($response['code'] != 200) {
            //             return $this->response($response['code'], $response['message'], $response['result']);
            //         }

            //         $rate_cal = $response['result'];
            //     }
            // }
            DB::commit();
            return $this->response(200, $response['message'], $rate_cal);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function computeAirCargo($request)
    {
        DB::beginTransaction();
        try {

            $rate_cal = [];
            $wtc = [];
            $weight_charge = 0;
            $ancillary_charge_id = [];

            $aWBFee = 0;
            $codCharge = 0;
            $insurance = 0;
            $valuation = 0;
            $evat = 0;
            $handling_fee = 0;

            $visayas_handling_fee = 0;
            $mindanao_handling_fee = 0;

            $weight1 = 0;
            $weight2 = 0;
            $weight3 = 0;
            $weight4 = 0;
            $weight5 = 0;

            $evat = 0;


            $rates = CrmRateAirFreight::with('getAncillaries', 'RateAirfreightHasMany')
                ->whereHas('RateAirfreightHasMany', function ($query) use ($request) {
                    $query->where('rate_id', $request['commodity_applicable_rate'])
                        ->where('origin_id', $request['origin'])
                        ->where('destination_id', $request['destination']);
                })
                ->where('commodity_type_id', $request['commodity_type'])
                ->where('service_mode_id', $request['service_mode'])
                ->first();

            $rate_details = $rates->RateAirfreightHasMany ?? [];
            $ancillaries = $rates->getAncillaries ?? [];

            foreach ($rate_details as $rate) {
                if ($rate->is_primary == 1) {
                    $weight1 = $rate->amount_weight_1;
                    $weight2 =  $rate->amount_weight_2;
                    $weight3 =  $rate->amount_weight_3;
                    $weight4 =  $rate->amount_weight_4;
                    $weight5 =  $rate->amount_weight_5;
                }
            }

            foreach ($request['air_cargos'] as $i => $ac) {
                $cwt = $request['air_cargos'][$i]['cwt'];

                if ($cwt >= 0 && $cwt <= 5) {
                    $wtc[$i] = $weight1 * $cwt;
                } elseif ($cwt >= 6 && $cwt <= 49) {
                    $wtc[$i] = $weight2 * $cwt;
                } elseif ($cwt >= 50 && $cwt <= 249) {
                    $wtc[$i] = $weight3 * $cwt;
                } elseif ($cwt >= 250 && $cwt <= 999) {
                    $wtc[$i] = $weight4 * $cwt;
                } elseif ($cwt >= 1000) {
                    $wtc[$i] = $weight5 * $cwt;
                }
            }

            $weight_charge = array_sum($wtc);

            foreach ($ancillaries as $i => $anci_charge) {
                $ancillary_charge_id[] =  $anci_charge->ancillary_charge_id;
            }

            $anci_charges = CrmAncillary::whereIn('id', $ancillary_charge_id)->get();

            foreach ($anci_charges as $acharge) {
                if ($acharge->name == "Waybill Fee") {
                    $aWBFee = $acharge->charges_amount;
                }
                if ($acharge->name == "Air Freight Valuation Fee") {
                    $valuation = $acharge->charges_amount * $request['declared_value'];
                }

                if ($acharge->name == "Air Freight Handling Fee 2") {
                    $visayas_handling_fee = $acharge->charges_amount;
                }

                if ($acharge->name == "Air Freight Handling Fee 3") {
                    $mindanao_handling_fee = $acharge->charges_amount;
                }
            }

            $insurance = 2 * $cwt;
            $major_island = BranchReference::where('id', $request['destination'])->first();

            if ($major_island->area_level_id == 90) {
                $handling_fee = $cwt * $visayas_handling_fee;
            } elseif ($major_island->area_level_id == 91) {
                $handling_fee = $cwt * $mindanao_handling_fee;
            }

            if ($request['paymode'] == 2) {
                $codCharge = 30;
            }

            $other_fees = array_sum(array(
                $request['opa_fee'] ?? 0,
                $request['oda_fee'] ?? 0,
                $request['equipment_rental'] ?? 0,
                $request['crating_fee'] ?? 0,
                $request['lashing_fee'] ?? 0,
                $request['manpower_fee'] ?? 0,
                $request['dangerous_goods_fee'] ?? 0,
                $request['trucking_fee'] ?? 0,
                $request['perishable_fee'] ?? 0,
                $request['packaging_fee'] ?? 0,
            ));

            $subtotal = array_sum(array($weight_charge, $aWBFee, $codCharge, $valuation, $insurance, $handling_fee, $other_fees));

            if ($request['evat']) {
                $evat = $subtotal * 0.12;
            }

            $discount_amount = $request['discount_amount'] ?? 0;;

            $discount_percentage = ($discount_amount / $subtotal) * 100;

            $rate_cal = [
                'cbm_charge' => number_format((float)$weight_charge, 2, '.', ''),
                'awb_fee' => number_format((float)$aWBFee, 2, '.', ''),
                'cod_charge' => number_format((float)$codCharge, 2, '.', ''),
                'valuation' => number_format((float)$valuation, 2, '.', ''),
                'insurance' => number_format((float)$insurance, 2, '.', ''),
                'evat' => number_format((float)$evat, 2, '.', ''),
                'handling_fee' => number_format((float)$handling_fee, 2, '.', ''),
                'discount_percentage' => number_format((float)$discount_percentage, 2, '.', ''),
                'subtotal' => number_format((float)$subtotal, 2, '.', ''),
                'grand_total' => number_format((((float)$subtotal + $evat) - (float)$discount_amount), 2),
            ];

            DB::commit();
            return $this->response(200, 'Rate Calculator - Air Cargo', $rate_cal);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function computeAirPouch($request)
    {
        DB::beginTransaction();
        try {
            $amounts = [];
            $subtotal = 0;
            $evat = 0;

            foreach ($request['air_pouches'] as $i => $ap) {
                $amounts[] = $ap['amount'];
            }

            if ($request['paymode'] == 2) {
                $codCharge = 30;
            }

            $other_fees = array_sum(array(
                $request['opa_fee'] ?? 0,
                $request['oda_fee'] ?? 0,
                $request['equipment_rental'] ?? 0,
                $request['crating_fee'] ?? 0,
                $request['lashing_fee'] ?? 0,
                $request['manpower_fee'] ?? 0,
                $request['dangerous_goods_fee'] ?? 0,
                $request['trucking_fee'] ?? 0,
                $request['perishable_fee'] ?? 0,
                $request['packaging_fee'] ?? 0,
            ));

            $subtotal = array_sum(array(array_sum($amounts), $codCharge, $other_fees));

            if ($request['evat']) {
                $evat = $subtotal * 0.12;
            }

            $discount_amount = $request['discount_amount'] ?? 0;;

            $discount_percentage = ($discount_amount / $subtotal) * 100;

            $rate_cal = [
                'evat' => number_format((float)$evat, 2, '.', ''),
                'cod_charge' => number_format((float)$codCharge, 2, '.', ''),
                'discount_percentage' => number_format((float)$discount_percentage, 2, '.', ''),
                'subtotal' => number_format((float)$subtotal, 2, '.', ''),
                'grand_total' => number_format((((float)$subtotal + $evat) - (float)$discount_amount), 2),
            ];

            DB::commit();
            return $this->response(200, 'Rate Calculator - Air Pouch', $rate_cal);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function computeAirBox($request)
    {
        DB::beginTransaction();
        try {

            $amounts = [];
            $subtotal = 0;

            $evat = 0;

            foreach ($request['air_boxes'] as $i => $ab) {
                $amounts[] = $ab['amount'];
            }

            if ($request['paymode'] == 2) {
                $codCharge = 30;
            }

            $other_fees = array_sum(array(
                $request['opa_fee'] ?? 0,
                $request['oda_fee'] ?? 0,
                $request['equipment_rental'] ?? 0,
                $request['crating_fee'] ?? 0,
                $request['lashing_fee'] ?? 0,
                $request['manpower_fee'] ?? 0,
                $request['dangerous_goods_fee'] ?? 0,
                $request['trucking_fee'] ?? 0,
                $request['perishable_fee'] ?? 0,
                $request['packaging_fee'] ?? 0,
            ));

            $subtotal = array_sum(array(array_sum($amounts), $codCharge, $other_fees));

            if ($request['evat']) {
                $evat = $subtotal * 0.12;
            }

            $discount_amount = $request['discount_amount'] ?? 0;;

            $discount_percentage = ($discount_amount / $subtotal) * 100;

            $rate_cal = [
                'evat' => number_format((float)$evat, 2, '.', ''),
                'cod_charge' => number_format((float)$codCharge, 2, '.', ''),
                'discount_percentage' => number_format((float)$discount_percentage, 2, '.', ''),
                'subtotal' => number_format((float)$subtotal, 2, '.', ''),
                'grand_total' => number_format((((float)$subtotal + $evat) - (float)$discount_amount), 2),
            ];

            DB::commit();
            return $this->response(200, 'Rate Calculator - Air Box', $rate_cal);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function computeSeaCargo($request)
    {
        DB::beginTransaction();
        try {

            $wtc = [];
            $amounts = [];

            $weight_charge = 0;
            $ancillary_charge_id = [];

            $aWBFee = 0;
            $codCharge = 0;
            $insurance = 0;
            $valuation = 0;
            $evat = 0;
            $handling_fee = 0;

            $visayas_handling_fee = 0;
            $mindanao_handling_fee = 0;

            $weight1 = 0;
            $weight2 = 0;
            $weight3 = 0;
            $weight4 = 0;
            $container_size = 0;

            $shipment_type = $request['shipment_type'];
            $evat = 0;

            $rates = CrmRateSeaFreight::with('getAncillaries', 'RateSeafreightHasMany')
                ->whereHas('RateSeafreightHasMany', function ($query) use ($request) {
                    $query->where('rate_id', $request['commodity_applicable_rate'])
                        ->where('origin_id', $request['origin'])
                        ->where('destination_id', $request['destination']);
                })
                ->where('commodity_type_id', $request['commodity_type'])
                // ->where('shipment_type_id', $shipment_type)
                ->first();


            $rate_details = $rates->RateSeafreightHasMany ?? [];
            $ancillaries = $rates->getAncillaries ?? [];

            foreach ($rate_details as $rate) {
                if ($rate->is_primary == 1) {
                    if ($shipment_type == 1) { // LCL
                        $weight1 = $rate->amount_weight_1;
                        $weight2 =  $rate->amount_weight_2;
                        $weight3 =  $rate->amount_weight_3;
                        $weight4 =  $rate->amount_weight_4;
                    } elseif ($shipment_type == 2) {  // FCL
                        $weight1 = $rate->amount_servicemode_1;
                        $weight2 =  $rate->amount_servicemode_2;
                        $weight3 =  $rate->amount_servicemode_3;
                        $weight4 =  $rate->amount_servicemode_4;

                        $container_size =  $rate->size;
                    }
                }
            }

            if ($request['paymode'] == 2) {
                $codCharge = 30;
            }

            $other_fees = array_sum(array(
                $request['opa_fee'] ?? 0,
                $request['oda_fee'] ?? 0,
                $request['equipment_rental'] ?? 0,
                $request['crating_fee'] ?? 0,
                $request['lashing_fee'] ?? 0,
                $request['manpower_fee'] ?? 0,
                $request['dangerous_goods_fee'] ?? 0,
                $request['trucking_fee'] ?? 0,
                $request['perishable_fee'] ?? 0,
                $request['packaging_fee'] ?? 0,
            ));

            if ($shipment_type == 1) { // LCL
                foreach ($request['sea_cargos_is_lcl'] as $i => $ac) {
                    $cbm = $request['sea_cargos_is_lcl'][$i]['cbm'];

                    $qty = $request['sea_cargos_is_lcl'][$i]['qty'];

                    if ($request['service_mode'] == 5) {
                        $wtc[$i] = $weight1 * ($cbm * $qty);
                    } elseif ($request['service_mode'] == 6) {
                        $wtc[$i] = $weight2 * ($cbm * $qty);
                    } elseif ($request['service_mode'] == 7) {
                        $wtc[$i] = $weight3 * ($cbm * $qty);
                    } elseif ($request['service_mode'] == 8) {
                        $wtc[$i] = $weight4 * ($cbm * $qty);
                    }
                }

                $weight_charge = array_sum($wtc);

                foreach ($ancillaries as $i => $anci_charge) {
                    $ancillary_charge_id[] =  $anci_charge->ancillary_charge_id;
                }

                $anci_charges = CrmAncillary::whereIn('id', $ancillary_charge_id)->get();

                foreach ($anci_charges as $acharge) {
                    if ($acharge->name == "Waybill Fee") {
                        $aWBFee = $acharge->charges_amount * $cbm;
                    }
                    if ($acharge->name == "Sea Freight Valuation Fee") {
                        $valuation = $request['declared_value'] * 0.005;
                    }

                    if ($acharge->name == "Sea Freight Handling Fee") {
                        $handling_fee = $acharge->charges_amount * $cbm;
                    }
                }

                $insurance = 2 * $cbm;


                $subtotal = array_sum(array($weight_charge, $aWBFee, $codCharge, $valuation, $insurance, $handling_fee, $other_fees));

                if ($request['evat']) {
                    $evat = $subtotal * 0.12;
                }

                $discount_amount = $request['discount_amount'] ?? 0;;

                $discount_percentage = ($discount_amount / $subtotal) * 100;

                $rate_cal = [
                    'cbm_charge' => number_format((float)$weight_charge, 2, '.', ''),
                    'awb_fee' => number_format((float)$aWBFee, 2, '.', ''),
                    'cod_charge' => number_format((float)$codCharge, 2, '.', ''),
                    'valuation' => number_format((float)$valuation, 2, '.', ''),
                    'insurance' => number_format((float)$insurance, 2, '.', ''),
                    'evat' => number_format((float)$evat, 2, '.', ''),
                    'handling_fee' => number_format((float)$handling_fee, 2, '.', ''),
                    'discount_percentage' => number_format((float)$discount_percentage, 2, '.', ''),
                    'subtotal' => number_format((float)$subtotal, 2, '.', ''),
                    'grand_total' => number_format((((float)$subtotal + $evat) - (float)$discount_amount), 2),
                ];
            } elseif ($shipment_type == 2) {  // FCL
                foreach ($request['sea_cargos_is_fcl'] as $i => $sc_fcl) {
                    $qty = $request['sea_cargos_is_fcl'][$i]['qty'];
                    $container = $request['sea_cargos_is_fcl'][$i]['container'];

                    if ($request['service_mode'] == 5 && $container == $container_size) {
                        $amounts[$i] = $weight1 * $qty;
                    } elseif ($request['service_mode'] == 6 && $container == $container_size) {
                        $amounts[$i] = $weight2 * $qty;
                    } elseif ($request['service_mode'] == 7 && $container == $container_size) {
                        $amounts[$i] = $weight3 * $qty;
                    } elseif ($request['service_mode'] == 8 && $container == $container_size) {
                        $amounts[$i] = $weight4 * $qty;
                    }
                }

                $subtotal = array_sum(array(array_sum($amounts), $codCharge, $other_fees));

                if ($request['evat']) {
                    $evat = $subtotal * 0.12;
                }

                $discount_amount = $request['discount_amount'] ?? 0;;

                $discount_percentage = ($discount_amount / $subtotal) * 100;

                $rate_cal = [
                    'evat' => number_format((float)$evat, 2, '.', ''),
                    'cod_charge' => number_format((float)$codCharge, 2, '.', ''),
                    'subtotal' => number_format((float)$subtotal, 2, '.', ''),
                    'grand_total' => number_format((((float)$subtotal + $evat) - (float)$discount_amount), 2),
                ];
            } elseif ($shipment_type == 3) {  // RCL
                foreach ($request['sea_cargos_is_rcl'] as $i => $ac) {
                    $cbm = $request['sea_cargos_is_rcl'][$i]['cbm'];
                    $amount = $request['sea_cargos_is_rcl'][$i]['amount'];

                    $wtc[$i] = $cbm * $amount;
                }

                $weight_charge = array_sum($wtc);

                $doc_fee = 5;

                $subtotal = array_sum(array($weight_charge, $doc_fee, $other_fees));

                if ($request['evat']) {
                    $evat = $subtotal * 0.12;
                }

                $discount_amount = $request['discount_amount'] ?? 0;;

                $discount_percentage = ($discount_amount / $subtotal) * 100;

                $rate_cal = [
                    'cbm_charge' => number_format((float)$weight_charge, 2, '.', ''),
                    'doc_fee' => number_format((float)$doc_fee, 2, '.', ''),
                    'evat' => number_format((float)$evat, 2, '.', ''),
                    'discount_percentage' => number_format((float)$discount_percentage, 2, '.', ''),
                    'subtotal' => number_format((float)$subtotal, 2, '.', ''),
                    'grand_total' => number_format((((float)$subtotal + $evat) - (float)$discount_amount), 2),
                ];
            }

            DB::commit();
            return $this->response(200, 'Rate Calculator - Sea Cargo $shipment_type', $rate_cal);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function computeSeaBox($request)
    {
        DB::beginTransaction();
        try {

            $amounts = [];
            $subtotal = 0;

            $evat = 0;

            foreach ($request['sea_boxes'] as $i => $ab) {
                $amounts[] = $ab['amount'];
            }

            if ($request['paymode'] == 2) {
                $codCharge = 30;
            }

            $other_fees = array_sum(array(
                $request['opa_fee'] ?? 0,
                $request['oda_fee'] ?? 0,
                $request['equipment_rental'] ?? 0,
                $request['crating_fee'] ?? 0,
                $request['lashing_fee'] ?? 0,
                $request['manpower_fee'] ?? 0,
                $request['dangerous_goods_fee'] ?? 0,
                $request['trucking_fee'] ?? 0,
                $request['perishable_fee'] ?? 0,
                $request['packaging_fee'] ?? 0,
            ));

            $subtotal = array_sum(array(array_sum($amounts), $codCharge, $other_fees));

            if ($request['evat']) {
                $evat = $subtotal * 0.12;
            }

            $discount_amount = $request['discount_amount'] ?? 0;;

            $discount_percentage = ($discount_amount / $subtotal) * 100;

            $rate_cal = [
                'evat' => number_format((float)$evat, 2, '.', ''),
                'cod_charge' => number_format((float)$codCharge, 2, '.', ''),
                'discount_percentage' => number_format((float)$discount_percentage, 2, '.', ''),
                'subtotal' => number_format((float)$subtotal, 2, '.', ''),
                'grand_total' => number_format(((float)$subtotal + $evat), 2),
            ];

            DB::commit();
            return $this->response(200, 'Rate Calculator - Air Box', $rate_cal);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function osCompute($request)
    {
        DB::beginTransaction();
        try {
            $rate_cal = [];

            if ($request['os_transport_mode'] == 1) {
                $response = $this->osComputeAirCargo($request);

                if ($response['code'] != 200) {
                    return $this->response($response['code'], $response['message'], $response['result']);
                }

                $rate_cal = $response['result'];
            }
            // elseif ($request['transport_mode'] == 2) {
            //     if ($request['sea_cargo']) {
            //         $response = $this->computeSeaCargo($request);

            //         if ($response['code'] != 200) {
            //             return $this->response($response['code'], $response['message'], $response['result']);
            //         }

            //         $rate_cal = $response['result'];
            //     }

            //     if ($request['sea_box']) {
            //         $response = $this->computeSeaBox($request);

            //         if ($response['code'] != 200) {
            //             return $this->response($response['code'], $response['message'], $response['result']);
            //         }

            //         $rate_cal = $response['result'];
            //     }
            // }
            DB::commit();
            return $this->response(200, $response['message'], $rate_cal);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function osComputeAirCargo($request)
    {
        DB::beginTransaction();
        try {

            $wtc = [];
            $weight_charge = 0;
            $ancillary_charge_id = [];

            $aWBFee = 0;
            $codCharge = 0;
            $insurance = 0;
            $valuation = 0;
            $evat = 0;
            $handling_fee = 0;

            $visayas_handling_fee = 0;
            $mindanao_handling_fee = 0;

            $weight1 = 0;
            $weight2 = 0;
            $weight3 = 0;
            $weight4 = 0;
            $weight5 = 0;

            $rates = CrmRateAirFreight::with('getAncillaries', 'RateAirfreightHasMany')
                ->whereHas('RateAirfreightHasMany', function ($query) use ($request) {
                    $query->where('rate_id', $request['os_commodity_applicable_rate'])
                        ->where('origin_id', $request['os_origin'])
                        ->where('destination_id', $request['os_destination']);
                })
                ->where('commodity_type_id', $request['os_commodity_type'])
                ->where('service_mode_id', $request['os_service_mode'])
                ->first();

            $rate_details = $rates->RateAirfreightHasMany ?? [];
            $ancillaries = $rates->getAncillaries ?? [];

            foreach ($rate_details as $rate) {
                if ($rate->is_primary == 1) {
                    $weight1 = $rate->amount_weight_1;
                    $weight2 =  $rate->amount_weight_2;
                    $weight3 =  $rate->amount_weight_3;
                    $weight4 =  $rate->amount_weight_4;
                    $weight5 =  $rate->amount_weight_5;
                }
            }

            foreach ($request['other_services_measurements'] as $i => $ac) {
                $cwt = $request['other_services_measurements'][$i]['cbm'];

                if ($cwt >= 0 && $cwt <= 5) {
                    $wtc[$i] = $weight1 * $cwt;
                } elseif ($cwt >= 6 && $cwt <= 49) {
                    $wtc[$i] = $weight2 * $cwt;
                } elseif ($cwt >= 50 && $cwt <= 249) {
                    $wtc[$i] = $weight3 * $cwt;
                } elseif ($cwt >= 250 && $cwt <= 999) {
                    $wtc[$i] = $weight4 * $cwt;
                } elseif ($cwt >= 1000) {
                    $wtc[$i] = $weight5 * $cwt;
                }
            }
            $weight_charge = array_sum($wtc);

            foreach ($ancillaries as $i => $anci_charge) {
                $ancillary_charge_id[] =  $anci_charge->ancillary_charge_id;
            }

            $anci_charges = CrmAncillary::whereIn('id', $ancillary_charge_id)->get();

            foreach ($anci_charges as $acharge) {
                if ($acharge->name == "Waybill Fee") {
                    $aWBFee = $acharge->charges_amount;
                }
                if ($acharge->name == "Air Freight Valuation Fee") {
                    $valuation = $acharge->charges_amount * $request['os_declared_value'];
                }

                if ($acharge->name == "Air Freight Handling Fee 2") {
                    $visayas_handling_fee = $acharge->charges_amount;
                }

                if ($acharge->name == "Air Freight Handling Fee 3") {
                    $mindanao_handling_fee = $acharge->charges_amount;
                }
            }

            $insurance = 2 * $cwt;
            $major_island = BranchReference::where('id', $request['os_destination'])->first();

            if ($major_island->area_level_id == 90) {
                $handling_fee = $cwt * $visayas_handling_fee;
            } elseif ($major_island->area_level_id == 91) {
                $handling_fee = $cwt * $mindanao_handling_fee;
            }

            if ($request['os_paymode'] == 2) {
                $codCharge = 30;
            }

            $other_fees = array_sum(array(
                $request['opa_fee'] ?? 0,
                $request['oda_fee'] ?? 0,
                $request['equipment_rental'] ?? 0,
                $request['crating_fee'] ?? 0,
                $request['lashing_fee'] ?? 0,
                $request['manpower_fee'] ?? 0,
                $request['dangerous_goods_fee'] ?? 0,
                $request['trucking_fee'] ?? 0,
                $request['perishable_fee'] ?? 0,
                $request['packaging_fee'] ?? 0,
            ));

            $subtotal = array_sum(array($weight_charge, $aWBFee, $codCharge, $valuation, $insurance, $handling_fee, $other_fees));

            if ($request['os_evat']) {
                $evat = $subtotal * 0.12;
            }

            $discount_amount = $request['discount_amount'] ?? 0;;

            $discount_percentage = ($discount_amount / $subtotal) * 100;

            $rate_cal = [
                'cbm_charge' => number_format((float)$weight_charge, 2, '.', ''),
                'awb_fee' => number_format((float)$aWBFee, 2, '.', ''),
                'cod_charge' => number_format((float)$codCharge, 2, '.', ''),
                'valuation' => number_format((float)$valuation, 2, '.', ''),
                'insurance' => number_format((float)$insurance, 2, '.', ''),
                'evat' => number_format((float)$evat, 2, '.', ''),
                'handling_fee' => number_format((float)$handling_fee, 2, '.', ''),
                'discount_percentage' => number_format((float)$discount_percentage, 2, '.', ''),
                'subtotal' => number_format((float)$subtotal, 2, '.', ''),
                'grand_total' => number_format(((float)$subtotal + $evat), 2),
            ];

            DB::commit();
            return $this->response(200, 'Rate Calculator - Air Cargo', $rate_cal);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
