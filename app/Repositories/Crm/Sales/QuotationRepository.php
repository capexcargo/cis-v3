<?php

namespace App\Repositories\Crm\Sales;

use App\Interfaces\Crm\Sales\QuotationInterface;
use App\Models\Crm\CrmQuotation;
use App\Models\Crm\CrmServiceRequest;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class QuotationRepository implements QuotationInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $quotations = CrmQuotation::with('customerDetails')
                ->whereHas('customerDetails', function ($query) use ($request) {
                    $query->whereHas('customerIndDetails', function ($query) use ($request) {
                        $query->when($request['customer_name_search'] ?? false, function ($query) use ($request) {
                            $query->where('fullname', 'like', '%' . $request['customer_name_search'] . '%');
                        });
                    });
                })
                ->when($request['quotation_ref_no_search'] ?? false, function ($query) use ($request) {
                    $query->where('reference_no', $request['quotation_ref_no_search']);
                })
                ->when($request['date_created_search'] ?? false, function ($query) use ($request) {
                    $query->whereDate('created_at', $request['date_created_search']);
                })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Quotations', compact('quotations'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id, $from_sr)
    {
        DB::beginTransaction();
        try {

            if ($from_sr == 'sr-create-quotation') {
                $service_request = CrmServiceRequest::with('attachments')
                    ->findOrFail($id);

                if (!$service_request) {
                    return $this->response(404, 'Service Request', 'Not Found!');
                }

                DB::commit();

                return $this->response(200, 'Service Request', $service_request);
            } else {
                $quotation = CrmQuotation::findOrFail($id);

                if (!$quotation) {
                    return $this->response(404, 'Quotation', 'Not Found!');
                }

                DB::commit();

                return $this->response(200, 'Quotation', $quotation);
            }
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'date' => 'required',
                'quotation_ref_no' => 'required',
                'shipment_type' => 'required',
                'sr_number' => 'required',
                'subject' => 'required',
                'account_id' => 'required',
                'contact_owner' => 'required',
                'assigned_to_id' => 'required',

                'transport_mode' => 'required',
                'origin' => 'required',
                'destination' => 'required',
                'exact_pickup_location' => 'required',
                'exact_dropoff_location' => 'required',

                'declared_value' => 'sometimes',
                'service_mode' => 'required',
                'commodity_type' => 'sometimes',
                'commodity_applicable_rate' => 'sometimes',
                'description' => 'sometimes',
                'paymode' => 'required',

                //Transport mode = 1
                'air_cargo' => 'sometimes',
                'air_cargos' => 'sometimes',

                'air_pouch' => 'sometimes',
                'air_pouches' => 'sometimes',

                'air_box' => 'sometimes',
                'air_boxes' => 'sometimes',


                //Transport mode = 2
                'sea_cargo' => 'sometimes',
                'sea_cargos_is_lcl' => 'sometimes',
                'sea_cargos_is_fcl' => 'sometimes',
                'sea_cargos_is_rcl' => 'sometimes',

                'sea_box' => 'sometimes',
                'sea_boxes' => 'sometimes',

                //Transport mode = 3
                'land' => 'sometimes',
                'lands_shippers_box' => 'sometimes',
                'lands_pouches' => 'sometimes',
                'lands_boxes' => 'sometimes',
                'lands_ftl' => 'sometimes',


                // BREAKDOWN OF FREIGHT CHARGES
                'weight_charge' => 'sometimes',
                'awb_fee' => 'sometimes',
                'valuation' => 'sometimes',
                'cod_charge' => 'sometimes',
                'insurance' => 'sometimes',
                'handling_fee' => 'sometimes',
                'other_fees' => 'sometimes',
                'opa_fee' => 'sometimes',
                'oda_fee' => 'sometimes',
                'equipment_rental' => 'sometimes',
                'crating_fee' => 'sometimes',
                'lashing_fee' => 'sometimes',
                'manpower_fee' => 'sometimes',
                'dangerous_goods_fee' => 'sometimes',
                'trucking_fee' => 'sometimes',
                'perishable_fee' => 'sometimes',
                'packaging_fee' => 'sometimes',
                'discount_amount' => 'sometimes',
                'discount_percentage' => 'sometimes',
                'evat_chkbox' => 'sometimes',
                'evat' => 'sometimes',
                'subtotal' => 'sometimes',
                'grand_total' => 'sometimes',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function saveSrCreateQuotation($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $service_type = 0;
            $sea_shipment_type = 0;
            $cbm = [];
            $total_cbm = 0;

            $quotation = CrmQuotation::updateOrCreate(
                [
                    'reference_no' => $validated['quotation_ref_no'],
                    'sr_no' => $validated['sr_number'],
                ],
                [
                    'reference_no' => $validated['quotation_ref_no'],
                    'account_id' => $validated['account_id'],
                    'sr_no' => $validated['sr_number'],
                    'subject' => $validated['subject'],
                    'shipment_type_id' => $validated['shipment_type'],
                    'assigned_to' => $validated['assigned_to_id'],
                    'is_save' => 1,
                ]
            );

            if ($validated['air_cargo']) {
                $service_type = 1;

                foreach ($validated['air_cargos'] as $i => $air_cargo) {
                    if ($air_cargo['qty'] != null) {
                        $cbm[] = $air_cargo['cwt'];
                    }
                    $quotation->shipmentDimensions()->updateOrCreate(
                        [
                            'quotation_id' => $quotation->id
                        ],
                        [
                            'quantity' => $air_cargo['qty'],
                            'weight' => $air_cargo['wt'],
                            'length' => $air_cargo['dimension_l'],
                            'width' => $air_cargo['dimension_w'],
                            'height' => $air_cargo['dimension_h'],
                            'unit_of_measurement_id' =>  $air_cargo['unit_of_measurement'],
                            'measurement_type_id' => $air_cargo['measurement_type'],
                            'type_of_packaging_id' => $air_cargo['type_of_packaging'],
                            'is_for_crating' => $air_cargo['for_crating'],
                            'crating_type_id' => $air_cargo['crating_type'],
                        ]
                    );
                }
            }

            if ($validated['air_pouch']) {
                $service_type = 2;

                foreach ($validated['air_pouches'] as $i => $air_pouch) {
                    $quotation->shipmentDimensions()->updateOrCreate(
                        [
                            'quotation_id' => $quotation->id
                        ],
                        [
                            'quantity' => $air_pouch['qty'],
                            'pouch_box_size' => $air_pouch['pouch_size'],
                            'pouch_box_amount' =>  $air_pouch['amount'],
                        ]
                    );
                }
            }

            if ($validated['air_box']) {
                $service_type = 3;

                foreach ($validated['air_boxes'] as $i => $air_box) {
                    $quotation->shipmentDimensions()->updateOrCreate(
                        [
                            'quotation_id' => $quotation->id
                        ],
                        [
                            'quantity' => $air_box['qty'],
                            'pouch_box_size' => $air_box['box_size'],
                            'weight' =>  $air_box['weight'],
                            'pouch_box_amount' =>  $air_box['amount'],
                        ]
                    );
                }
            }

            if ($validated['sea_cargo']) {
                foreach ($validated['sea_cargos_is_lcl'] as $i => $lcl) { // LCL
                    if ($lcl['qty'] != null) {
                        $sea_shipment_type = 1;
                        $cbm[] = $lcl['cbm'];
                    }

                    $quotation->shipmentDimensions()->updateOrCreate(
                        [
                            'quotation_id' => $quotation->id
                        ],
                        [
                            'quantity' => $lcl['qty'],
                            'weight' => $lcl['wt'],
                            'length' => $lcl['dimension_l'],
                            'width' => $lcl['dimension_w'],
                            'height' => $lcl['dimension_h'],
                            'unit_of_measurement_id' =>  $lcl['unit_of_measurement'],
                            'measurement_type_id' => $lcl['measurement_type'],
                            'type_of_packaging_id' => $lcl['type_of_packaging'],
                            'is_for_crating' => $lcl['for_crating'],
                            'crating_type_id' => $lcl['crating_type'],
                            // 'pouch_box_size' => $lcl['cbm'],
                            // 'pouch_box_amount' =>  $lcl['cbm'],
                            // 'container' => $lcl['container'],
                        ]
                    );
                }

                foreach ($validated['sea_cargos_is_fcl'] as $i => $fcl) { // FCL
                    if ($fcl['qty'] != null) {
                        $sea_shipment_type = 2;
                    }

                    $quotation->shipmentDimensions()->create([
                        'quantity' => $fcl['qty'],
                        'container' => $fcl['container'],
                    ]);
                }

                foreach ($validated['sea_cargos_is_rcl'] as $i => $rcl) { // RCL
                    if ($rcl['qty'] != null) {
                        $sea_shipment_type = 3;
                        $cbm[] = $rcl['cbm'];
                    }

                    $quotation->shipmentDimensions()->create([
                        'quantity' => $rcl['qty'],
                        'weight' => $rcl['wt'],
                        'length' => $rcl['dimension_l'],
                        'width' => $rcl['dimension_w'],
                        'height' => $rcl['dimension_h'],
                        'unit_of_measurement_id' =>  $rcl['unit_of_measurement'],
                        'measurement_type_id' => $rcl['measurement_type'],
                        'pouch_box_amount' => $rcl['amount'],
                        'type_of_packaging_id' => $rcl['type_of_packaging'],
                        'is_for_crating' => $rcl['for_crating'],
                        'crating_type_id' => $rcl['crating_type'],
                    ]);
                }
            }

            if ($validated['sea_box']) {
                foreach ($validated['sea_boxes'] as $i => $sea_box) {
                    $quotation->shipmentDimensions()->updateOrCreate(
                        [
                            'quotation_id' => $quotation->id
                        ],
                        [
                            'quantity' => $sea_box['qty'],
                            'pouch_box_size' => $sea_box['box_size'],
                            'weight' =>  $sea_box['weight'],
                            'pouch_box_amount' =>  $sea_box['amount'],
                        ]
                    );
                }
            }

            $total_cbm = array_sum($cbm);

            $quotation->shipmentDetails()->updateOrCreate(
                [
                    'quotation_id' => $quotation->id
                ],
                [
                    'transport_mode_id' => $validated['transport_mode'],
                    'origin_id' => $validated['origin'],
                    'destination_id' => $validated['destination'],
                    'origin_address' => $validated['exact_pickup_location'],
                    'destination_address' => $validated['exact_dropoff_location'],
                    'declared_value' => $validated['declared_value'],
                    'servicemode_id' => $validated['service_mode'],
                    // 'transhipment_id' => $validated['transhipment_id'],
                    'commodity_type_id' => $validated['commodity_type'],
                    'commodity_applicable_rate_id' => $validated['commodity_applicable_rate'],
                    'paymode_id' => $validated['paymode'],
                    'service_type_id' => $service_type,
                    'sea_shipment_type' => $sea_shipment_type,
                    'total_cbm' => $total_cbm,
                    'total_chgwt' => $validated['weight_charge'],
                    'total_amount' => (int)str_replace('', ',', $validated['grand_total']),
                ]
            );

            $quotation->shipmentCharges()->updateOrCreate(
                [
                    'quotation_id' => $quotation->id
                ],
                [
                    'weight_charge' => $validated['weight_charge'],
                    'awb_fee' => $validated['awb_fee'],
                    'valuation' => $validated['valuation'],
                    'cod_charge' => $validated['cod_charge'],
                    'insurance' => $validated['insurance'],
                    'handling_fee' => $validated['handling_fee'],
                    'doc_fee' => $validated['commodity_applicable_rate'],
                    'is_other_fee' => $validated['other_fees'],
                    // 'total_other_fee' => $validated['total_other_fee'],
                    'opa_fee' => $validated['opa_fee'],
                    'oda_fee' => $validated['oda_fee'],
                    'equipment_rental' => $validated['equipment_rental'],
                    'crating_fee' => $validated['crating_fee'],
                    'lashing' => $validated['lashing_fee'],
                    'manpower' => $validated['manpower_fee'],
                    'dangerous_goods_fee' => $validated['dangerous_goods_fee'],
                    'trucking' => $validated['trucking_fee'],
                    'perishable_fee' => $validated['perishable_fee'],
                    'packaging_fee' => $validated['packaging_fee'],
                    'discount_amount' => $validated['discount_amount'],
                    'discount_percentage' => $validated['discount_percentage'],
                    'subtotal' => $validated['subtotal'],
                    'evat' => $validated['evat'],
                    'is_evat' => $validated['evat_chkbox'],
                    'grandtotal' => (float)str_replace(',', '', $validated['grand_total']),
                ]
            );

            $quotation->activities()->updateOrCreate(
                [
                    'quotation_reference_no' => $quotation->reference_no,
                ],
                [
                    'employee_id' => $validated['assigned_to_id'],
                    'activity_type_id' => 6,
                    'account_id' => $validated['account_id'],
                    'description' => $validated['description'],
                    'sr_no' => $validated['sr_number'],
                    'quotation_reference_no' => $validated['quotation_ref_no'],
                    'status' => 1,
                    'datetime_completed' => date('Y-m-d H:i:s')
                ]
            );

            $quotation_id = $quotation->id;

            DB::commit();

            return $this->response(200, 'Quotation has been successfully saved!', compact('quotation', 'quotation_id'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function saveEditQuotation($request, $id, $from_sr)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id, $from_sr);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $quotation = $response['result'];

            $response = $this->validation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $service_type = 0;
            $sea_shipment_type = 0;
            $cbm = [];
            $total_cbm = 0;

            $quotation->update([
                'subject' => $validated['subject'],
                'shipment_type_id' => $validated['shipment_type'],
                // 'is_save' => 1,
            ]);

            if ($validated['air_cargo']) {
                $service_type = 1;

                foreach ($validated['air_cargos'] as $i => $air_cargo) {
                    if ($air_cargo['qty'] != null) {
                        $cbm[] = $air_cargo['cwt'];
                    }
                    $quotation->shipmentDimensions()->updateOrCreate(
                        [
                            'quotation_id' => $quotation->id
                        ],
                        [
                            'quantity' => $air_cargo['qty'],
                            'weight' => $air_cargo['wt'],
                            'length' => $air_cargo['dimension_l'],
                            'width' => $air_cargo['dimension_w'],
                            'height' => $air_cargo['dimension_h'],
                            'unit_of_measurement_id' =>  $air_cargo['unit_of_measurement'],
                            'measurement_type_id' => $air_cargo['measurement_type'],
                            'type_of_packaging_id' => $air_cargo['type_of_packaging'],
                            'is_for_crating' => $air_cargo['for_crating'],
                            'crating_type_id' => $air_cargo['crating_type'],
                        ]
                    );
                }
            }

            if ($validated['air_pouch']) {
                $service_type = 2;

                foreach ($validated['air_pouches'] as $i => $air_pouch) {
                    $quotation->shipmentDimensions()->updateOrCreate(
                        [
                            'quotation_id' => $quotation->id
                        ],
                        [
                            'quantity' => $air_pouch['qty'],
                            'pouch_box_size' => $air_pouch['pouch_size'],
                            'pouch_box_amount' =>  $air_pouch['amount'],
                        ]
                    );
                }
            }

            if ($validated['air_box']) {
                $service_type = 3;

                foreach ($validated['air_boxes'] as $i => $air_box) {
                    $quotation->shipmentDimensions()->updateOrCreate(
                        [
                            'quotation_id' => $quotation->id
                        ],
                        [
                            'quantity' => $air_box['qty'],
                            'pouch_box_size' => $air_box['box_size'],
                            'weight' =>  $air_box['weight'],
                            'pouch_box_amount' =>  $air_box['amount'],
                        ]
                    );
                }
            }

            if ($validated['sea_cargo']) {
                foreach ($validated['sea_cargos_is_lcl'] as $i => $lcl) { // LCL
                    if ($lcl['qty'] != null) {
                        $sea_shipment_type = 1;
                        $cbm[] = $lcl['cbm'];
                    }

                    $quotation->shipmentDimensions()->updateOrCreate(
                        [
                            'quotation_id' => $quotation->id
                        ],
                        [
                            'quantity' => $lcl['qty'],
                            'weight' => $lcl['wt'],
                            'length' => $lcl['dimension_l'],
                            'width' => $lcl['dimension_w'],
                            'height' => $lcl['dimension_h'],
                            'unit_of_measurement_id' =>  $lcl['unit_of_measurement'],
                            'measurement_type_id' => $lcl['measurement_type'],
                            'type_of_packaging_id' => $lcl['type_of_packaging'],
                            'is_for_crating' => $lcl['for_crating'],
                            'crating_type_id' => $lcl['crating_type'],
                            // 'pouch_box_size' => $lcl['cbm'],
                            // 'pouch_box_amount' =>  $lcl['cbm'],
                            // 'container' => $lcl['container'],
                        ]
                    );
                }

                foreach ($validated['sea_cargos_is_fcl'] as $i => $fcl) { // FCL
                    if ($fcl['qty'] != null) {
                        $sea_shipment_type = 2;
                    }

                    $quotation->shipmentDimensions()->create([
                        'quantity' => $fcl['qty'],
                        'container' => $fcl['container'],
                    ]);
                }

                foreach ($validated['sea_cargos_is_rcl'] as $i => $rcl) { // RCL
                    if ($rcl['qty'] != null) {
                        $sea_shipment_type = 3;
                        $cbm[] = $rcl['cbm'];
                    }

                    $quotation->shipmentDimensions()->create([
                        'quantity' => $rcl['qty'],
                        'weight' => $rcl['wt'],
                        'length' => $rcl['dimension_l'],
                        'width' => $rcl['dimension_w'],
                        'height' => $rcl['dimension_h'],
                        'unit_of_measurement_id' =>  $rcl['unit_of_measurement'],
                        'measurement_type_id' => $rcl['measurement_type'],
                        'pouch_box_amount' => $rcl['amount'],
                        'type_of_packaging_id' => $rcl['type_of_packaging'],
                        'is_for_crating' => $rcl['for_crating'],
                        'crating_type_id' => $rcl['crating_type'],
                    ]);
                }
            }

            if ($validated['sea_box']) {
                foreach ($validated['sea_boxes'] as $i => $sea_box) {
                    $quotation->shipmentDimensions()->updateOrCreate(
                        [
                            'quotation_id' => $quotation->id
                        ],
                        [
                            'quantity' => $sea_box['qty'],
                            'pouch_box_size' => $sea_box['box_size'],
                            'weight' =>  $sea_box['weight'],
                            'pouch_box_amount' =>  $sea_box['amount'],
                        ]
                    );
                }
            }

            $total_cbm = array_sum($cbm);

            $quotation->shipmentDetails()->update(
                [
                    'transport_mode_id' => $validated['transport_mode'],
                    'origin_id' => $validated['origin'],
                    'destination_id' => $validated['destination'],
                    'origin_address' => $validated['exact_pickup_location'],
                    'destination_address' => $validated['exact_dropoff_location'],
                    'declared_value' => $validated['declared_value'],
                    'servicemode_id' => $validated['service_mode'],
                    // 'transhipment_id' => $validated['transhipment_id'],
                    'commodity_type_id' => $validated['commodity_type'],
                    'commodity_applicable_rate_id' => $validated['commodity_applicable_rate'],
                    'paymode_id' => $validated['paymode'],
                    'service_type_id' => $service_type,
                    'sea_shipment_type' => $sea_shipment_type,
                    'total_cbm' => $total_cbm,
                    'total_chgwt' => $validated['weight_charge'],
                    'total_amount' => (int)str_replace('', ',', $validated['grand_total']),
                ]
            );

            $quotation->shipmentCharges()->update(
                [
                    'weight_charge' => $validated['weight_charge'],
                    'awb_fee' => $validated['awb_fee'],
                    'valuation' => $validated['valuation'],
                    'cod_charge' => $validated['cod_charge'],
                    'insurance' => $validated['insurance'],
                    'handling_fee' => $validated['handling_fee'],
                    'doc_fee' => $validated['commodity_applicable_rate'],
                    'is_other_fee' => $validated['other_fees'],
                    // 'total_other_fee' => $validated['total_other_fee'],
                    'opa_fee' => $validated['opa_fee'],
                    'oda_fee' => $validated['oda_fee'],
                    'equipment_rental' => $validated['equipment_rental'],
                    'crating_fee' => $validated['crating_fee'],
                    'lashing' => $validated['lashing_fee'],
                    'manpower' => $validated['manpower_fee'],
                    'dangerous_goods_fee' => $validated['dangerous_goods_fee'],
                    'trucking' => $validated['trucking_fee'],
                    'perishable_fee' => $validated['perishable_fee'],
                    'packaging_fee' => $validated['packaging_fee'],
                    'discount_amount' => $validated['discount_amount'],
                    'discount_percentage' => $validated['discount_percentage'],
                    'subtotal' => $validated['subtotal'],
                    'evat' => $validated['evat'],
                    'is_evat' => $validated['evat_chkbox'],
                    'grandtotal' => (float)str_replace(',', '', $validated['grand_total']),
                ]
            );

            DB::commit();

            return $this->response(200, 'Quotation has been successfully updated!', $quotation);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
