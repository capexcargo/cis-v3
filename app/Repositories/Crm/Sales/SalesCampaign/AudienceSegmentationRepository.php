<?php

namespace App\Repositories\Crm\Sales\SalesCampaign;

use App\Interfaces\Crm\Sales\SalesCampaign\AudienceSegmentationInterface;
use App\Models\ChannelSrSource;
use App\Models\Crm\CityReference;
use App\Models\Crm\CrmAccountTypeReference;
use App\Models\Crm\CrmAudienceSegmentManagement;
use App\Models\Crm\CrmAudienceSegmentManagementCriteria;
use App\Models\Crm\CrmCustomerStatusReferences;
use App\Models\Crm\CrmLifeStage;
use App\Models\Crm\CrmMarketingChannel;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AudienceSegmentationRepository implements AudienceSegmentationInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            // $segment = CrmAudienceSegmentManagementCriteria::with('audience')->paginate($request['paginate']);
            $segment = CrmAudienceSegmentManagement::with('criteria')->paginate($request['paginate']);

            // dd($segment);
            DB::commit();

            return $this->response(200, 'Segment', compact('segment'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function validation($request)
    {
        // dd($request['filter_id']);
        DB::beginTransaction();
        try {
            $rules =  [
                'name' => 'required',
                'filter_id' => 'required',
                'onboarded_date_from' => ($request['filter_id'] == 7 ? 'required' : 'sometimes'),
                'onboarded_date_to' => ($request['filter_id'] == 7 ? 'required' : 'sometimes'),
            ];
            foreach ($request['positions'] as $i => $positionss) {
                $rules['positions.' . $i . '.id'] = 'sometimes';
                $rules['positions.' . $i . '.criteria_id'] = ($request['filter_id'] != 7 ? 'required' : 'sometimes');
                $rules['positions.' . $i . '.is_deleted'] = 'sometimes';
            }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'This Segment Name field is required.',
                    'filter_id.required' => 'This Filter field is required.',
                    'onboarded_date_from.required' => 'This Date From field is required.',
                    'onboarded_date_to.required' => 'This Date To field is required.',
                    'positions.*.criteria_id.required' => 'The Criteria field is required.',
                ]

            );

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function create($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            // dd($response);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);     
            $segment = CrmAudienceSegmentManagement::create([
                'name' => $validated['name'],
                'filter_id' => $validated['filter_id'],
                'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                'status_id' => 1,
            ]);

            if ($validated['filter_id'] == 1) {
                foreach ($validated['positions'] as $i => $position) {
                    $criteria_name_life_stage = CrmLifeStage::where('id', $validated['positions'][$i]['criteria_id'])->first();
                    $criteria = CrmAudienceSegmentManagementCriteria::create([
                        'audience_id' => $segment->id,
                        'criteria_id' => $validated['positions'][$i]['criteria_id'],
                        'criteria_name' => $criteria_name_life_stage['name'],
                        'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                        'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                    ]);
                }
                // dd($criteria);
            }

            if ($validated['filter_id'] == 2) {
                foreach ($validated['positions'] as $i => $position) {
                    $criteria_name_customer_status = CrmCustomerStatusReferences::where('id', $validated['positions'][$i]['criteria_id'])->first();
                    $criteria = CrmAudienceSegmentManagementCriteria::create([
                        'audience_id' => $segment->id,
                        'criteria_id' => $validated['positions'][$i]['criteria_id'],
                        'criteria_name' => $criteria_name_customer_status['name'],
                        'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                        'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                    ]);
                }
                // dd($criteria);
            }

            if ($validated['filter_id'] == 3) {
                foreach ($validated['positions'] as $i => $position) {
                    $criteria_name_acc_type = CrmAccountTypeReference::where('id', $validated['positions'][$i]['criteria_id'])->first();
                    $criteria = CrmAudienceSegmentManagementCriteria::create([
                        'audience_id' => $segment->id,
                        'criteria_id' => $validated['positions'][$i]['criteria_id'],
                        'criteria_name' => $criteria_name_acc_type['name'],
                        'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                        'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                    ]);
                }
                // dd($criteria);
            }

            if ($validated['filter_id'] == 4) {
                foreach ($validated['positions'] as $i => $position) {
                    $criteria_name_channel_sr = ChannelSrSource::where('id', $validated['positions'][$i]['criteria_id'])->first();
                    $criteria = CrmAudienceSegmentManagementCriteria::create([
                        'audience_id' => $segment->id,
                        'criteria_id' => $validated['positions'][$i]['criteria_id'],
                        'criteria_name' => $criteria_name_channel_sr['name'],
                        'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                        'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                    ]);
                }
                // dd($criteria);
            }

            if ($validated['filter_id'] == 5) {
                foreach ($validated['positions'] as $i => $position) {
                    $criteria_name_marketing_channel = CrmMarketingChannel::where('id', $validated['positions'][$i]['criteria_id'])->first();
                    $criteria = CrmAudienceSegmentManagementCriteria::create([
                        'audience_id' => $segment->id,
                        'criteria_id' => $validated['positions'][$i]['criteria_id'],
                        'criteria_name' => $criteria_name_marketing_channel['name'],
                        'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                        'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                    ]);
                }
                // dd($criteria);
            }

            if ($validated['filter_id'] == 6) {
                foreach ($validated['positions'] as $i => $position) {
                    $criteria_name_city = CityReference::where('id', $validated['positions'][$i]['criteria_id'])->first();
                    $criteria = CrmAudienceSegmentManagementCriteria::create([
                        'audience_id' => $segment->id,
                        'criteria_id' => $validated['positions'][$i]['criteria_id'],
                        'criteria_name' => $criteria_name_city['name'],
                        'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                        'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                    ]);
                }
                // dd($criteria);
            }

            if ($validated['filter_id'] == 7) {
                foreach ($validated['positions'] as $i => $position) {
                    // $criteria_name_city = CityReference::where('id', $validated['positions'][$i]['criteria_id'])->first();
                    $criteria = CrmAudienceSegmentManagementCriteria::create([
                        'audience_id' => $segment->id,
                        'criteria_id' => null,
                        'criteria_name' => null,
                        'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                        'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                    ]);
                }
                // dd($criteria);
            }

            DB::commit();

            return $this->response(200, 'New audience segment has been successfully added!', compact($segment, $criteria));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function show($id)
    {
        DB::beginTransaction();
        try {
            // $segment = CrmAudienceSegmentManagementCriteria::with('audience')->findOrFail($id);
            $segment = CrmAudienceSegmentManagement::with('criteria')->findOrFail($id);
            if (!$segment) {
                return $this->response(404, 'Target Segment Not Found', $segment);
            }
            // dd($segment);
            DB::commit();
            return $this->response(200, 'Target Segment', $segment);
        } catch (\Exception $err) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $err->getMessage());
        }
    }

    public function update($request, $id)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            // dd($response);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $segment = $response['result'];
            // dd($items);
            $response = $this->validation($request);
            // dd($response);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];
            // dd($validated);
            $segment->updateOrCreate([
                'id' => $id,
            ], [
                'name' => $validated['name'],
                'filter_id' => $validated['filter_id'],
                'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
            ]);

            // dd($segment);

            if ($validated['filter_id'] == 1) {
                foreach ($validated['positions'] as $a => $position) {

                    if ($validated['positions'][$a]['id'] && $validated['positions'][$a]['is_deleted']) {
                        $criteria = CrmAudienceSegmentManagementCriteria::find($validated['positions'][$a]['id']); //query all in database

                        if ($criteria) {
                            $criteria->delete();
                        }
                    } else {
                        $criteria_name_life_stage = CrmLifeStage::where('id', $validated['positions'][$a]['criteria_id'])->first();
                        $criteria = CrmAudienceSegmentManagementCriteria::updateOrCreate(
                            [
                                'id' => $validated['positions'][$a]['id'],
                            ],
                            [
                                'audience_id' => $segment->id,
                                'criteria_id' => $validated['positions'][$a]['criteria_id'],
                                'criteria_name' => $criteria_name_life_stage['name'],
                                'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                                'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                            ]
                        );
                    }
                }
                // dd($criteria);
            }

            if ($validated['filter_id'] == 2) {
                foreach ($validated['positions'] as $a => $position) {

                    if ($validated['positions'][$a]['id'] && $validated['positions'][$a]['is_deleted']) {
                        $criteria = CrmAudienceSegmentManagementCriteria::find($validated['positions'][$a]['id']); //query all in database

                        if ($criteria) {
                            $criteria->delete();
                        }
                    } else {
                        $criteria_name_customer_status = CrmCustomerStatusReferences::where('id', $validated['positions'][$a]['criteria_id'])->first();
                        $criteria = CrmAudienceSegmentManagementCriteria::updateOrCreate(
                            [
                                'id' => $validated['positions'][$a]['id'],
                            ],
                            [
                                'audience_id' => $segment->id,
                                'criteria_id' => $validated['positions'][$a]['criteria_id'],
                                'criteria_name' => $criteria_name_customer_status['name'],
                                'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                                'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                            ]
                        );
                    }
                }
                // dd($criteria);
            }

            if ($validated['filter_id'] == 3) {
                foreach ($validated['positions'] as $a => $position) {

                    if ($validated['positions'][$a]['id'] && $validated['positions'][$a]['is_deleted']) {
                        $criteria = CrmAudienceSegmentManagementCriteria::find($validated['positions'][$a]['id']); //query all in database

                        if ($criteria) {
                            $criteria->delete();
                        }
                    } else {
                        $criteria_name_acc_type = CrmAccountTypeReference::where('id', $validated['positions'][$a]['criteria_id'])->first();
                        $criteria = CrmAudienceSegmentManagementCriteria::updateOrCreate(
                            [
                                'id' => $validated['positions'][$a]['id'],
                            ],
                            [
                                'audience_id' => $segment->id,
                                'criteria_id' => $validated['positions'][$a]['criteria_id'],
                                'criteria_name' => $criteria_name_acc_type['name'],
                                'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                                'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                            ]
                        );
                    }
                }
                // dd($criteria);
            }

            if ($validated['filter_id'] == 4) {
                foreach ($validated['positions'] as $a => $position) {

                    if ($validated['positions'][$a]['id'] && $validated['positions'][$a]['is_deleted']) {
                        $criteria = CrmAudienceSegmentManagementCriteria::find($validated['positions'][$a]['id']); //query all in database

                        if ($criteria) {
                            $criteria->delete();
                        }
                    } else {
                        $criteria_name_channel_sr = ChannelSrSource::where('id', $validated['positions'][$a]['criteria_id'])->first();
                        $criteria = CrmAudienceSegmentManagementCriteria::updateOrCreate(
                            [
                                'id' => $validated['positions'][$a]['id'],
                            ],
                            [
                                'audience_id' => $segment->id,
                                'criteria_id' => $validated['positions'][$a]['criteria_id'],
                                'criteria_name' => $criteria_name_channel_sr['name'],
                                'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                                'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                            ]
                        );
                    }
                }
                // dd($criteria);
            }

            if ($validated['filter_id'] == 5) {
                foreach ($validated['positions'] as $a => $position) {

                    if ($validated['positions'][$a]['id'] && $validated['positions'][$a]['is_deleted']) {
                        $criteria = CrmAudienceSegmentManagementCriteria::find($validated['positions'][$a]['id']); //query all in database

                        if ($criteria) {
                            $criteria->delete();
                        }
                    } else {
                        $criteria_name_marketing_channel = CrmMarketingChannel::where('id', $validated['positions'][$a]['criteria_id'])->first();
                        $criteria = CrmAudienceSegmentManagementCriteria::updateOrCreate(
                            [
                                'id' => $validated['positions'][$a]['id'],
                            ],
                            [
                                'audience_id' => $segment->id,
                                'criteria_id' => $validated['positions'][$a]['criteria_id'],
                                'criteria_name' => $criteria_name_marketing_channel['name'],
                                'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                                'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                            ]
                        );
                    }
                }
                // dd($criteria);
            }

            if ($validated['filter_id'] == 6) {
                foreach ($validated['positions'] as $a => $position) {

                    if ($validated['positions'][$a]['id'] && $validated['positions'][$a]['is_deleted']) {
                        $criteria = CrmAudienceSegmentManagementCriteria::find($validated['positions'][$a]['id']); //query all in database

                        if ($criteria) {
                            $criteria->delete();
                        }
                    } else {
                        $criteria_name_city = CityReference::where('id', $validated['positions'][$a]['criteria_id'])->first();
                        $criteria = CrmAudienceSegmentManagementCriteria::updateOrCreate(
                            [
                                'id' => $validated['positions'][$a]['id'],
                            ],
                            [
                                'audience_id' => $segment->id,
                                'criteria_id' => $validated['positions'][$a]['criteria_id'],
                                'criteria_name' => $criteria_name_city['name'],
                                'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                                'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                            ]
                        );
                    }
                }
                // dd($criteria);
            }

            if ($validated['filter_id'] == 7) {
                foreach ($validated['positions'] as $a => $position) {

                    if ($validated['positions'][$a]['id'] && $validated['positions'][$a]['is_deleted']) {
                        $criteria = CrmAudienceSegmentManagementCriteria::find($validated['positions'][$a]['id']); //query all in database

                        if ($criteria) {
                            $criteria->delete();
                        }
                    } else {
                        // $criteria_name_life_stage = CrmLifeStage::where('id', $validated['positions'][$a]['criteria_id'])->first();
                        $criteria = CrmAudienceSegmentManagementCriteria::updateOrCreate(
                            [
                                'id' => $validated['positions'][$a]['id'],
                            ],
                            [
                                'audience_id' => $segment->id,
                                'criteria_id' => null,
                                'criteria_name' => null,
                                'onboarded_date_from' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_from'] : null),
                                'onboarded_date_to' => ($validated['filter_id'] == 7 ? $validated['onboarded_date_to'] : null),
                            ]
                        );
                    }
                }
                // dd($criteria);
            }




            DB::commit();
            return $this->response(200, 'Audience Segment has been successfully updated!',  compact($segment, $criteria));
            // return $this->response(200, 'Segment has been successfully updated!', $segment);
        } catch (\Exception $err) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $err->getMessage());
        }
    }
}
