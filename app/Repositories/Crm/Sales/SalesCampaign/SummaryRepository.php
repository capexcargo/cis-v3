<?php

namespace App\Repositories\Crm\Sales\SalesCampaign;

use App\Interfaces\Crm\Sales\SalesCampaign\SummaryInterface;
use App\Models\Crm\CrmSalesCampaign;
use App\Models\Crm\CrmSalesCampaignAttachment;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class SummaryRepository implements SummaryInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $summary = CrmSalesCampaign::paginate($request['paginate']);

            $voucher_reference = uniqid();

            DB::commit();

            return $this->response(200, 'Summary', compact('summary'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $rules =  [
                'name' => 'required',
                'voucher_code' => 'required',
                'discount_type' => 'required',
                'discount_amount' => ($request['discount_type'] == 1 ? 'required' : 'sometimes'),
                'discount_percentage' => ($request['discount_type'] == 2 ? 'required' : 'sometimes'),
                'start_datetime' => 'required',
                'end_datetime' => 'required',
                'maximum_usage' => 'required',
                'terms_conditon' => 'required',
                'status' => 'nullable',

                'attachments' => 'nullable',
                'attachments.*.attachment' => 'nullable|' . config('filesystems.crm_sales_campaign_validation'),
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required' => 'The Campaign Name field is required.',
                    'voucher_code.required' => 'The Voucher Code field is required.',
                    'discount_type.required' => 'The Type of Discount field is required.',
                    'discount_amount.required' => 'The Discount Amount field is required.',
                    'discount_percentage.required' => 'The Discount Percentage field is required.',

                    'start_datetime.required' => 'The Start Date field is required.',
                    'end_datetime.required' => 'The End Date field is required.',
                    'maximum_usage.required' => 'The Maximum Usage field is required.',
                    'terms_conditon.required' => 'The Terms and Conditions field is required.',
                    'attachments.*.attachment.required' => 'The attachment field is required.',
                    'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
                ]

            );

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            // dd($response);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $summary = CrmSalesCampaign::create([
                'name' => $validated['name'],
                'voucher_code' => $validated['voucher_code'],
                'discount_type' => $validated['discount_type'],
                'discount_amount' => $validated['discount_amount'],
                'discount_percentage' => $validated['discount_percentage'],
                'start_datetime' => $validated['start_datetime'],
                'end_datetime' => $validated['end_datetime'],
                'maximum_usage' => $validated['maximum_usage'],
                'terms_conditon' => $validated['terms_conditon'],
                'status' => 1,
                'reference_no' => uniqid(),
            ]);
            $response = $this->attachments($summary, $validated['attachments']);
            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            DB::commit();

            return $this->response(200, 'New Promo Voucher has been successfully added!', $summary);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function attachments($summary, $attachments)
    {
        // dd($attachments);
        DB::beginTransaction();
        try {
            $date = date('Y/m/d', strtotime($summary->created_at));

            foreach ($attachments as $attachment) {

                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $summary->Salesattachments()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(" ", "_", 'sales/' . $date . '/' . 'sales-campaign/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        Storage::disk('crm_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } else if (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('crm_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }
                    $summary->Salesattachments()->updateOrCreate(
                        [
                            'id' => $attachment['id']
                        ],
                        [
                            'sales_campaign_id' => $summary->id,
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                        ]
                    );
                }
            }

            DB::commit();
            return $this->response(200, 'Promo Voucher attachment successfully attached', $summary);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $summary = CrmSalesCampaign::with('Salesattachments')->findOrFail($id);
            if (!$summary) {
                return $this->response(404, 'Promo Voucher Not Found', $summary);
            }
            // dd($summary);
            DB::commit();
            return $this->response(200, 'Promo Voucher', $summary);
        } catch (\Exception $err) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $err->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            // dd($response);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $summary = $response['result'];
            // dd($items);
            $response = $this->validation($request);
            // dd($response);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];
            // dd($validated);
            $summary->update([
                // 'name' => $validated['name'],
                // 'voucher_code' => $validated['voucher_code'],
                // 'discount_type' => $validated['discount_type'],
                // 'discount_amount' => $validated['discount_amount'],
                // 'discount_percentage' => $validated['discount_percentage'],
                // 'start_datetime' => $validated['start_datetime'],
                'end_datetime' => $validated['end_datetime'],
                // 'maximum_usage' => $validated['maximum_usage'],
                // 'terms_conditon' => $validated['terms_conditon'],
                // 'status' => $validated['status'],
            ]);
            $response = $this->attachments($summary, $validated['attachments']);
            // $items->itemsDetails()->update([
            //     'fname' => $validated['fname'],
            //     'mname' => $validated['mname'],
            //     'lname' => $validated['lname'],
            //     'mobile_number' => $validated['mobile_number'],
            // ]);

            DB::commit();
            return $this->response(200, 'Promo Voucher has been successfully updated!', $summary);
        } catch (\Exception $err) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $err->getMessage());
        }
    }

    public function delete($id)
    {

        DB::beginTransaction();
        try {

            $del = CrmSalesCampaign::find($id);

            if (!$del) return $this->response(404, 'Promo Voucher', 'Not Found!');
            // $del->delete();
            $del->Salesattachments()->delete();
            $del->delete();
            // dd($del);
            DB::commit();
            return $this->response(200, 'Promo Voucher Successfully Deleted!', $del);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
