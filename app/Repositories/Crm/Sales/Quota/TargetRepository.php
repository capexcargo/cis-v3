<?php

namespace App\Repositories\Crm\Sales\Quota;

use App\Interfaces\Crm\Sales\Quota\TargetInterface;
use App\Models\Crm\CrmQuotaManagementCorporate;
use App\Models\Crm\CrmQuotaManagementStakeholder;
use App\Models\MonthReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class TargetRepository implements TargetInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $quota_targets = [];

            if ($request['is_corporate_target'] == 1) {
                $corporate_targets = CrmQuotaManagementCorporate::with('month')
                    ->when($request['sortField'], function ($query) use ($request) {
                        $query->orderBy($request['sortField'], $request['sort_type'] ? 'desc' : 'asc');
                    })
                    ->when($request['month'] ?? false, function ($query) use ($request) {
                        $query->whereMonth('created_at', $request['month'] ?? date('M'));
                    })
                    ->when($request['year'] ?? false, function ($query) use ($request) {
                        $query->whereYear('created_at', $request['year'] ?? date('Y'));
                    })
                    ->groupBy('stakeholder_category_name', 'management_id')
                    ->paginate($request['paginate']);

                $corporate_target_subcategories = CrmQuotaManagementCorporate::with('month')
                    ->when($request['sortField'], function ($query) use ($request) {
                        $query->orderBy($request['sortField'], $request['sort_type'] ? 'desc' : 'asc');
                    })->paginate($request['paginate']);

                $quota_targets = compact('corporate_targets', 'corporate_target_subcategories');
                $response_message = "Corporate Target Distribution List";
            } else {
                if ($request['sh_header'] == '') {
                    $stakeholders_targets = CrmQuotaManagementStakeholder::with('accountType')
                        ->when($request['sortField'], function ($query) use ($request) {
                            $query->orderBy($request['sortField'], $request['sort_type'] ? 'asc' : 'desc');
                        })
                        ->when($request['month'] ?? false, function ($query) use ($request) {
                            $query->whereMonth('created_at', $request['month'] ?? date('M'));
                        })
                        ->when($request['year'] ?? false, function ($query) use ($request) {
                            $query->whereYear('created_at', $request['year'] ?? date('Y'));
                        })
                        ->groupBy('stakeholder_category_name', 'management_id')
                        // ->selectRaw('*, sum(distribution_pct) as percentage')
                        ->paginate($request['paginate']);
                } else {
                    $stakeholders_targets = CrmQuotaManagementStakeholder::with('accountType')
                        ->when($request['sortField'], function ($query) use ($request) {
                            $query->orderBy($request['sortField'], $request['sort_type'] ? 'asc' : 'desc');
                        })
                        ->when($request['month'] ?? false, function ($query) use ($request) {
                            $query->whereMonth('created_at', $request['month'] ?? date('M'));
                        })
                        ->when($request['year'] ?? false, function ($query) use ($request) {
                            $query->whereYear('created_at', $request['year'] ?? date('Y'));
                        })
                        ->where('management_id', $request['sh_header'])
                        ->paginate($request['paginate']);
                }

                $quota_targets = compact('stakeholders_targets');
                $response_message = "Stakeholders Target Distribution List";
            }

            DB::commit();

            return $this->response(200, $response_message, $quota_targets);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($type, $request)
    {
        DB::beginTransaction();
        try {
            if ($type == 1) {
                $rules =  [
                    'month' => 'sometimes',
                    'year' => 'sometimes',
                    'stakeholders' => 'sometimes',
                    'total_days_count' => 'sometimes',
                    'monthly_total_target_amount' => 'sometimes',
                ];
            } else {
                $rules =  [
                    'month' => 'sometimes',
                    'year' => 'sometimes',
                    'stakeholder_category' => 'sometimes',
                    'subcategories' => 'sometimes',
                ];
            }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    // 'subcategories.*.amount.required' => 'This amount field is required.',
                ]
            );

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function updateOrCreate($type, $request)
    {

        DB::beginTransaction();
        try {
            $response = $this->validation($type, $request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $month_referece = MonthReference::where('id', $validated['month'])->first();

            // dd($validated);

            if ($type == 1) {
                foreach ($validated['stakeholders'] as $i => $subcategory) {
                    if (count($subcategory['sh_subcategory']) > 0) {
                        foreach ($subcategory['sh_subcategory'] as $x => $sh_subcat) {
                            $quota_target = CrmQuotaManagementCorporate::updateOrCreate(
                                [
                                    'month' => $validated['month'],
                                    'year' => $validated['year'],
                                    'subcategory' => $sh_subcat['id'],
                                    'management_id' => $subcategory['no'],
                                ],
                                [
                                    'stakeholder_category_name' => $subcategory['stakeholder_category_name'],
                                    'subcategory' => $sh_subcat['id'],
                                    'management_id' => $subcategory['no'],
                                    'account_type' => $subcategory['account_type_id'],
                                    'month' => $validated['month'],
                                    'year' => $validated['year'],
                                    'monthly_tonnage' => $sh_subcat['monthly_tonnage'],
                                    'distribution_pct' => $sh_subcat['distribution_pct'],
                                    'monthly_cbm_sea' => $sh_subcat['monthly_cbm_sea'],
                                    'monthly_kg_air' => $sh_subcat['monthly_kg_air'],
                                    'monthly_cbm_land' => $sh_subcat['monthly_cbm_land'],
                                    'monthly_kg_int' => $sh_subcat['monthly_kg_int'],
                                    'total_days_count' => $validated['total_days_count'],
                                    'monthly_target_amount' => $validated['monthly_total_target_amount'],
                                ]
                            );
                        }
                    } else {
                        $quota_target = CrmQuotaManagementCorporate::updateOrCreate(
                            [
                                'month' => $validated['month'],
                                'year' => $validated['year'],
                                // 'subcategory' => "10",
                                'management_id' => $subcategory['no'],
                            ],
                            [
                                'stakeholder_category_name' => $subcategory['stakeholder_category_name'],
                                // 'subcategory' => "10",
                                'management_id' => $subcategory['no'],
                                'account_type' => $subcategory['account_type_id'],
                                'month' => $validated['month'],
                                'year' => $validated['year'],
                                'monthly_tonnage' => $subcategory['monthly_tonnage'],
                                'distribution_pct' => $subcategory['distribution_pct'],
                                'monthly_cbm_sea' => $subcategory['monthly_cbm_sea'],
                                'monthly_kg_air' => $subcategory['monthly_kg_air'],
                                'monthly_cbm_land' => $subcategory['monthly_cbm_land'],
                                'monthly_kg_int' => $subcategory['monthly_kg_int'],
                                'total_days_count' => $validated['total_days_count'],
                                'monthly_target_amount' => $validated['monthly_total_target_amount'],
                            ]
                        );
                    }
                }
                $response_message =  'Corporate Target for the month of ' . $month_referece->display . ' has been successfully saved!';
            } else {
                foreach ($validated['subcategories'] as $i => $subcategory) {
                    $quota_target = CrmQuotaManagementStakeholder::updateOrCreate(
                        [
                            'month' => $validated['month'],
                            'year' => $validated['year'],
                            'subcategory' => $subcategory['sh_subcategory'],
                            'management_id' => $validated['stakeholder_category'],
                        ],
                        [
                            'stakeholder_category_name' => $subcategory['stakeholder_category_name'],
                            'subcategory' => $subcategory['sh_subcategory'],
                            'management_id' => $validated['stakeholder_category'],
                            'account_type' => $subcategory['account_type'],
                            'month' => $validated['month'],
                            'year' => $validated['year'],
                            'distribution_pct' => $subcategory['distribution_pct'],
                            'amount' => $subcategory['amount'],
                        ]
                    );
                }

                $response_message =  'Stakeholder Target for the month of ' . $month_referece->display . ' has been successfully saved!';
            }

            DB::commit();

            return $this->response(200, $response_message, $quota_target);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {

        DB::beginTransaction();
        try {

            $stakeholders_targets = CrmQuotaManagementStakeholder::with('accountType')
                ->groupBy('stakeholder_category_name', 'management_id')
                ->where('management_id', $id)
                ->get();

            $response_message = "Stakeholder";

            DB::commit();

            return $this->response(200, $response_message, compact('stakeholders_targets'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
