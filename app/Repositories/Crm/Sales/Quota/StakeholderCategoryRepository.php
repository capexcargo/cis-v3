<?php

namespace App\Repositories\Crm\Sales\Quota;

use App\Interfaces\Crm\Sales\Quota\StakeholderCategoryInterface;
use App\Models\Crm\CrmQuotaManagementStakeholder;
use App\Models\Crm\CrmStakeholderSearchMgmtReferences;
use App\Models\Crm\StakeholderCategory;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class StakeholderCategoryRepository implements StakeholderCategoryInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $stakeholder_categories = StakeholderCategory::groupBy('name', 'management_id')
                ->paginate($request['paginate']);
            // $stakeholder_categories = CrmStakeholderSearchMgmtReferences::has('subcategories')
            //     // ->whereHas('subcategories', function ($query) use ($request) {
            //     //     $query->orderBy('created_at', 'desc');
            //     // })
            //     ->paginate($request['paginate']);


            $subcategories = StakeholderCategory::get();

            return $this->response(200, 'Stakeholder Categories List', compact('stakeholder_categories', 'subcategories'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $rules =  [
                'stakeholder_category_name' => 'required',
                'search_management_data' => 'required',
                'selected_subs' => 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'selected_subs.required' => 'Please, select at least one.',
                ]

            );

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            // dd($validated);
            foreach ($validated['selected_subs'] as $i => $selected) {
                $stakeholder_category = StakeholderCategory::create([
                    'stakeholder_category_name' => $validated['stakeholder_category_name'],
                    'name' => $validated['stakeholder_category_name'],
                    'subcategory' => $selected['id'],
                    'management_id' => $selected['ref_id'],
                    'account_type' => 2,
                ]);
            }
            // dd($segment);

            DB::commit();

            return $this->response(200, 'New Stakeholder Category has been successfully added!', compact("stakeholder_category"));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {

            $stakeholder_category = CrmStakeholderSearchMgmtReferences::with('subcategories')
                ->find($id);

            $response_message = "Stakeholder";

            DB::commit();

            return $this->response(200, $response_message, compact('stakeholder_category'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $response = $this->show($id);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $stakeholder_category = $response['result']['stakeholder_category'];

            foreach ($validated['selected_subs'] as $i => $selected) {
                $stakeholder_category->subcategories()->updateOrCreate(
                    [
                        'subcategory' => $selected['id'],
                        'management_id' => $selected['ref_id'],
                    ],
                    [
                        'stakeholder_category_name' => $validated['stakeholder_category_name'],
                        'name' => $validated['stakeholder_category_name'],
                        'subcategory' => $selected['id'],
                        'management_id' => $selected['ref_id'],
                        'account_type' => 2,
                    ]
                );
            }

            DB::commit();

            return $this->response(200, 'Stakeholder Category has been successfully updated!', compact('stakeholder_category'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function report($request)
    {
        DB::beginTransaction();
        try {

            $stakeholder_categories = CrmQuotaManagementStakeholder::with('accountType')
                ->when($request['sortField'], function ($query) use ($request) {
                    $query->orderBy($request['sortField'], $request['sort_type'] ? 'desc' : 'asc');
                })
                ->groupBy('stakeholder_category_name', 'management_id')
                // ->selectRaw('*, sum(distribution_pct) as percentage')
                // ->selectRaw('*, sum(amount)')
                ->selectRaw('*, sum(actual_amount) as act_amnt, sum(amount) as amnt')
                ->paginate($request['paginate']);
            return $this->response(200, 'Stakeholder Categories List', compact('stakeholder_categories'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
