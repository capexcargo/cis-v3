<?php

namespace App\Repositories\Crm\Sales\Opportunities\SalesStage;

use App\Interfaces\Crm\Sales\Opportunities\SalesStage\SalesStageInterface;
use App\Models\CrmSalesStage;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class SalesStageRepository implements SalesStageInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $sales_s = CrmSalesStage::with('OpportunityStatus')
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'List', compact('sales_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'sales_stage' => 'required',
                'opportunity_status_id' => 'required',
            ],[
                'sales_stage.required' => 'Sales Stage is required',
                'opportunity_status_id.required' => 'Opportunity Status is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $SalesStage = CrmSalesStage::create([
                'sales_stage' => $validated['sales_stage'],
                'opportunity_status_id' => $validated['opportunity_status_id'],
                'status' => 1,
            ]);

            DB::commit();

            return $this->response(200, 'Sales Stage has been successfully added!', $SalesStage);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $sales_s = CrmSalesStage::findOrFail($id);
            if (!$sales_s) {
                return $this->response(404, 'Sales Stage', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Sales Stage', $sales_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'sales_stage' => 'required',
                'opportunity_status_id' => 'required',
            ],[
                'sales_stage.required' => 'Sales Stage is required',
                'opportunity_status_id.required' => 'Opportunity Status is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $sales_s = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $sales_s->update([
                'sales_stage' => $validated['sales_stage'],
                'opportunity_status_id' => $validated['opportunity_status_id'],
            ]);

            DB::commit();

            return $this->response(200, 'Sales Stage has been successfully updated!', $sales_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }




    // public function destroy($id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $sales_s = CrmSalesStage::find($id);
    //         if (!$sales_s) return $this->response(404, 'Sales Stage', 'Not Found!');
    //         $sales_s->delete();

    //         DB::commit();
    //         return $this->response(200, 'Sales Stage successfully deactivated!', $sales_s);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }
}
