<?php

namespace App\Repositories\Crm\Sales\Opportunities;

use App\Interfaces\Crm\Sales\Opportunities\OpportunitiesInterface;
use App\Models\Crm\CrmOpportunity;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class OpportunitiesRepository implements OpportunitiesInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $opportunities = CrmOpportunity::with(
                'saleStage',
            )
                ->when($request['opportunity_name'] ?? false, function ($query) use ($request) {
                    $query->where('opportunity_name', 'like', '%' . $request['opportunity_name'] . '%');
                })
                ->when($request['sr_no'] ?? false, function ($query) use ($request) {
                    $query->where('sr_no', $request['sr_no']);
                })
                ->when($request['name'] ?? false, function ($query) use ($request) {
                    $query->where('fullname', 'like', '%' . $request['name'] . '%');
                    // $query->where('fullname', $request['name']);
                })
                ->when($request['date_created'] ?? false, function ($query) use ($request) {
                    $query->whereDate('created_at', $request['date_created']);
                })
                ->when($request['sales_stage'] ?? false, function ($query) use ($request) {
                    $query->where('sales_stage_id', $request['sales_stage']);
                })
                ->when($request['opportunity_status'] ?? false, function ($query) use ($request) {
                    $query->where('opportunity_status_id', $request['opportunity_status']);
                })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Opportunities List', compact('opportunities'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $opportunity = CrmOpportunity::findOrFail($id);

            if (!$opportunity) {
                return $this->response(404, 'Opportunity', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Opportunity', $opportunity);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make(
                $request,
                [
                    'opportunity_name' => 'required',
                    'company_industry' => 'required',
                    'sales_stage' => 'required',
                    'deal_size' => 'required',
                    'actual_amount' => 'required',
                    'lead_conversion_date' => 'required',
                    'close_date' => 'sometimes',
                    'attachments' => 'nullable',
                    'attachments.*.attachment' => 'nullable|' . config('filesystems.validation_all'),
                ],
                [
                    'attachments.*.attachment.required' => 'The attachment field is required.',
                    'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
                ]
            );

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $opportunity = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $opportunity->update([
                'sales_stage_id' => $validated['sales_stage'],
                'opportunity_status_id' => $validated['sales_stage'] == 4 ? 2 : ($validated['sales_stage'] == 5 ? 3 : 1),
                'industry_id' => $validated['company_industry'],
                'deal_size' => $validated['deal_size'],
                'close_date' => $validated['close_date'],
            ]);


            $response = $this->attachments($opportunity, $validated['attachments']);
            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            DB::commit();

            return $this->response(200, 'Opportunity has been successfully updated!', $opportunity);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function attachments($opportunity, $attachments)
    {
        DB::beginTransaction();
        try {
            $date = date('Y/m/d', strtotime($opportunity->created_at));

            foreach ($attachments as $attachment) {

                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $opportunity->attachments()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(" ", "_", 'crm/sales/' . $date . '/' . 'opportunity/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        Storage::disk('crm_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } else if (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('crm_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }
                    $opportunity->attachments()->updateOrCreate(
                        [
                            'id' => $attachment['id']
                        ],
                        [
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                        ]
                    );
                }
            }

            DB::commit();
            return $this->response(200, 'Ticket Attachment has been successfully attached', $opportunity);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
