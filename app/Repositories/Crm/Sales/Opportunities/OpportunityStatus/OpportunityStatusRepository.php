<?php

namespace App\Repositories\Crm\Sales\Opportunities\OpportunityStatus;

use App\Interfaces\Crm\Sales\Leads\Qualification\QualificationMgmtInterface;
use App\Interfaces\Crm\Sales\Opportunities\OpportunityStatus\OpportunityStatusInterface;
use App\Models\CrmOpportunityStatusMgmt;
use App\Models\CrmQualificationMgmt;
use App\Models\CrmRateLoa;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class OpportunityStatusRepository implements OpportunityStatusInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $status_s = CrmOpportunityStatusMgmt::
            
                paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'List', compact('status_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'name' => 'required',
            ],[
                'name.required' => 'Opportunity Status is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $status_s = CrmOpportunityStatusMgmt::create([
                'name' => $validated['name'],
                'status' => 1,
            ]);

            DB::commit();

            return $this->response(200, 'Opportunity Status has been successfully added!', $status_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $status_s = CrmOpportunityStatusMgmt::findOrFail($id);
            if (!$status_s) {
                return $this->response(404, 'Opportunity Status', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Opportunity Status', $status_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'name' => 'required',
            ],[
                'name.required' => 'Opportunity Status is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $status_s = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $status_s->update([
                'name' => $validated['name'],
            ]);

            DB::commit();

            return $this->response(200, 'Opportunity Status has been successfully updated!', $status_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
         
    // public function destroy($id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $status_s = CrmQualificationMgmt::find($id);
    //         if (!$status_s) return $this->response(404, 'Opportunity Status', 'Not Found!');
    //         $status_s->delete();

    //         DB::commit();
    //         return $this->response(200, 'Opportunity Status Successfully Deleted!', $status_s);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }
}
