<?php

namespace App\Repositories\Crm\Sales\BookingManagement\BookingMgmt;

use App\Interfaces\Crm\Sales\BookingManagement\BookingMgmt\BookingMgmtInterface;
use App\Models\BranchReference;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmCustomerInformationAddressList;
use App\Models\CrmBooking;
use App\Models\CrmBookingAttachment;
use App\Models\CrmBookingCargoDetails;
use App\Models\CrmBookingCargoDetailsDimensions;
use App\Models\CrmBookingConsignee;
use App\Models\CrmBookingHistory;
use App\Models\CrmBookingLogs;
use App\Models\CrmBookingRemarks;
use App\Models\CrmBookingShipper;
use App\Models\CrmBookingType;
use App\Models\UserDetails;
use App\Traits\ResponseTrait;
use CreateCrmBookingTypeReference;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Kreait\Firebase\Factory;
use App\Traits\InitializeFirestoreTrait;


class BookingMgmtRepository implements BookingMgmtInterface
{
    use ResponseTrait, InitializeFirestoreTrait;

    public function index($request, $search_request)
    {

        DB::beginTransaction();
        try {

            $book_s = CrmBooking::with(
                'BookingTypeReferenceBK',
                'VehicleTypeReferenceBK',
                'TimeslotReferenceBK',
                'WalkinReferenceBK',
                'ActivityReferenceBK',
                'ShipperReferenceBK',
                'FinalStatusReferenceBK',
                'BookingBranchReferenceBK',
                'MarketingChannelReferenceBK',
                'CreatedByBK',
                'BookingAttachmentHasManyBK',
                'BookingLogsHasManyBK',
                'BookingRemarksHasManyBK',
                'BookingConsigneeHasManyBK',
                'attachments',
                'BookingShipper',
                'BookingChannelReferenceBK',
            )->when($search_request['booking_reference_no'] ?? false, function ($query) use ($search_request) {
                $query->where('booking_reference_no', 'like', '%' . $search_request['booking_reference_no'] . '%');
            })
                ->when($search_request['created_at'] ?? false, function ($query) use ($search_request) {
                    $query->where('created_at', 'like', '%' . $search_request['created_at'] . '%');
                })
                ->when($search_request['pickup_date'] ?? false, function ($query) use ($search_request) {
                    $query->where('pickup_date', 'like', '%' . $search_request['pickup_date'] . '%');
                })
                ->whereHas('BookingShipper', function ($query) use ($search_request) {
                    $query->when($search_request['s_name'] ?? false, function ($query) use ($search_request) {
                        $query->where('name', 'like', '%' . $search_request['s_name'] . '%');
                    });
                })
                ->whereHas('BookingConsigneeHasManyBK', function ($query) use ($search_request) {
                    $query->when($search_request['c_name'] ?? false, function ($query) use ($search_request) {
                        $query->where('name', 'like', '%' . $search_request['c_name'] . '%');
                    });
                })
                ->when($search_request['final_status_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('final_status_id', $search_request['final_status_id']);
                })
                ->whereHas('BookingBranchReferenceBK', function ($query) use ($search_request) {
                    $query->when($search_request['booking_branch_id'] ?? false, function ($query) use ($search_request) {
                        $query->where('booking_branch_id', 'like', '%' . $search_request['booking_branch_id'] . '%');
                    });
                })
                ->when($request['stats'], function ($query) use ($request) {
                    if ($request['stats'] == false) {
                        $query->whereIn('final_status_id', [6, 7]);
                    } else {
                        $query->where('final_status_id', $request['stats']);
                    }
                })
                ->when($request['stats_1'], function ($query) use ($request) {
                    if ($request['stats_1'] == false) {
                        $query->whereIn('final_status_id', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
                    } else {
                        $query->where('final_status_id', $request['stats_1']);
                    }
                })
                ->when($request['bc_1'], function ($query) use ($request) {
                    if ($request['bc_1'] == false) {
                        $query->whereIs('booking_channel', [1, 2, 3, 4]);
                    } else {
                        $query->where('booking_channel', $request['bc_1']);
                    }
                    // dd($request['stats']);
                })
                // ->where('booking_reference_no','CRMB-6598CD2A7B9A1')
                // ->when($request['stats'], function ($query) use ($request) {
                //     if ($request['stats'] == false) {
                //         $query->whereIs('booking_type_id', [1, 2]);
                //     } else {
                //         $query->where('booking_type_id', $request['stats']);
                //     }
                //     // dd($request['stats']);
                // })
                ->paginate($request['paginate']);


            DB::commit();

            return $this->response(200, 'List', compact('book_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index2($request, $search_request)
    {
        DB::beginTransaction();
        try {

            $book_s2 = CrmBookingHistory::with(
                'BookingTypeReferenceBK',
                'VehicleTypeReferenceBK',
                'TimeslotReferenceBK',
                'WalkinReferenceBK',
                'ActivityReferenceBK',
                'ShipperReferenceBK',
                'FinalStatusReferenceBK',
                'BookingBranchReferenceBK',
                'MarketingChannelReferenceBK',
                'CreatedByBK',
                'BookingAttachmentHasManyBK',
                'BookingLogsHasManyBK',
                'BookingRemarksHasManyBK',
                'BookingConsigneeHasManyBK',
                'attachments',
                'BookingShipper',
                'BookingChannelReferenceBK',
            )
                ->when($search_request['created_at'] ?? false, function ($query) use ($search_request) {
                    $query->where('created_at', 'like', '%' . $search_request['created_at'] . '%');
                })
                ->when($search_request['pickup_date'] ?? false, function ($query) use ($search_request) {
                    $query->where('pickup_date', $search_request['pickup_date']);
                })
                ->when($search_request['final_status_id'] ?? false, function ($query) use ($search_request) {
                    $query->where('final_status_id', $search_request['final_status_id']);
                })
                ->whereHas('BookingBranchReferenceBK', function ($query) use ($search_request) {
                    $query->when($search_request['booking_branch_id'] ?? false, function ($query) use ($search_request) {
                        $query->where('booking_branch_id', 'like', '%' . $search_request['booking_branch_id'] . '%');
                    });
                })
                ->when($request['stats_1'], function ($query) use ($request) {
                    if ($request['stats_1'] == false) {
                        $query->whereIn('final_status_id', [5, 6, 7, 8,]);
                    } else {
                        $query->where('final_status_id', $request['stats_1']);
                    }
                })
                ->when($request['bc_1'], function ($query) use ($request) {
                    if ($request['bc_1'] == false) {
                        $query->whereIs('booking_channel', [1, 2, 3, 4]);
                    } else {
                        $query->where('booking_channel', $request['bc_1']);
                    }
                    // dd($request['stats']);
                })
                ->paginate($request['paginate']);
            // dd($book_s2);

            DB::commit();

            return $this->response(200, 'List', compact('book_s2'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab1($request)
    {
        // dd($request); 
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'booking_type_1' => 'required',
                'vehicle_type_1' => 'required',
                'pickup_date_1' => 'required',
                'time_slot_1' => 'required',
                'consignee_count_1' => ($request['consignee_category_1'] == 2 ? 'required' : 'sometimes'),
                'walk_in_branch_1' => ($request['booking_category_1'] == 2 ? 'required' : 'sometimes'),
                'activity_type_1' => 'required',
                'time_from_1' => ($request['time_slot_1'] == 1 ? 'required' : 'sometimes'),
                'time_to_1' => ($request['time_slot_1'] == 1 ? 'required' : 'sometimes'),
                // 'channel_1' => 'required',
            ], [
                'booking_type_1.required' => 'Booking Type is Required',
                'vehicle_type_1.required' => 'Vehicle Type is Required',
                'pickup_date_1.required' => 'Pickup Date is Required',
                'time_slot_1.required' => 'Time Slot is Required',
                'consignee_count_1.required' => 'Total of Consignee is Required',
                'walk_in_branch_1.required' => 'Branch is Required',
                'activity_type_1.required' => 'Activity Type is Required',
                'time_from_1.required' => 'Time is Required',
                'time_to_1.required' => 'Time is Required',
                // 'channel_1.required' => 'Marketing Channel Type is Required',
            ]);

            // dd($validator);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab2($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $validator = Validator::make($request, [
                'booking_reference_no_2' => 'required',
                'customer_no_2' => 'required',
                'company_name_2' => ($request['account_type_2'] == 2 ? 'required' : 'sometimes'),
                'first_name_2' => 'required',
                'middle_name_2' => 'sometimes',
                'last_name_2' => 'required',
                'mobile_number_2' => 'required',
                'email_address_2' => 'required',
                'address_2' => 'required',
                'state2' => 'required',
                'city2' => 'required',
                'barangay2' => 'required',
                'postal2' => 'required',
            ], [
                'booking_reference_no_2.required' => 'Booking Reference is Required',
                'customer_no_2.required' => 'Customer Number is Required',
                'company_name_2.required' => 'Company Name is Required',
                'first_name_2.required' => 'First Name is Required',
                'middle_name_2.sometimes' => '',
                'last_name_2.required' => 'Last Name is Required',
                'mobile_number_2.required' => 'Contact Number is Required',
                'email_address_2.required' => 'Email Address is Required',
                'address_2.required' => 'Address is Required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab3($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $rules = [
                'work_instruction_3' => 'sometimes',
                'remarks_3' => 'sometimes',
                'attachments' => 'sometimes',
                'attachments.*.attachment.sometimes' => 'sometimes|' . config('filesystems.crm_sales_booking_mgmt_validation'),
            ];
            foreach ($request['cons'] as $i => $con) {
                $rules['cons.' . $i . '.' . $i . '.id'] = 'sometimes';
                $rules['cons.' . $i . '.' . $i . '.customer_no_3'] = 'required';
                if ($request['cons'][$i][$i]['account_type_3'] == 2) {
                    $rules['cons.' . $i . '.' . $i . '.company_name_3'] = 'required';
                    $rules['cons.' . $i . '.' . $i . '.charge_to_3'] = 'required';
                    $rules['cons.' . $i . '.' . $i . '.charge_to_name_3'] = 'sometimes';
                } else {
                    $rules['cons.' . $i . '.' . $i . '.company_name_3'] = 'sometimes';
                    $rules['cons.' . $i . '.' . $i . '.charge_to_3'] = 'sometimes';
                    $rules['cons.' . $i . '.' . $i . '.charge_to_name_3'] = 'required';
                }
                $rules['cons.' . $i . '.' . $i . '.account_type_3'] = 'sometimes';
                $rules['cons.' . $i . '.' . $i . '.first_name_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.middle_name_3'] = 'sometimes';
                $rules['cons.' . $i . '.' . $i . '.last_name_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.mobile_number_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.email_address_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.address_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.state_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.city_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.barangay_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.postal_3'] = 'required';

                $rules['cons.' . $i . '.' . $i . '.declared_value_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.description_goods_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.transposrt_mode_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.service_mode_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.mode_of_payment_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.stype_3'] = 'sometimes';
                if (isset($request['cardets'][$i])) {
                    foreach ($request['cardets'][$i] as $c => $cardet) {
                        $rules['cardets.' . $i . '.' . $c . '.id'] = 'sometimes';
                        $rules['cardets.' . $i . '.' . $c . '.quantity_3'] = 'sometimes';
                        $rules['cardets.' . $i . '.' . $c . '.weight_3'] = 'sometimes';
                        $rules['cardets.' . $i . '.' . $c . '.length_3'] = 'sometimes';
                        $rules['cardets.' . $i . '.' . $c . '.width_3'] = 'sometimes';
                        $rules['cardets.' . $i . '.' . $c . '.height_3'] = 'sometimes';
                        $rules['cardets.' . $i . '.' . $c . '.size_3'] = 'sometimes';
                    }


                    $validator = Validator::make(
                        $request,
                        $rules,
                        [
                            'cons.*.*.customer_no_3.required' => 'Customer Numbers is required.',
                            'cons.*.*.company_name_3.required' => 'Company Name is required.',
                            'cons.*.*.first_name_3.required' => 'First Name is required.',
                            'cons.*.*.last_name_3.required' => 'Last Name is required.',
                            'cons.*.*.mobile_number_3.required' => 'Mobile Number is required.',
                            'cons.*.*.email_address_3.required' => 'Email Address is required.',
                            'cons.*.*.address_3.required' => 'Address is required.',
                            'cons.*.*.state_3.required' => 'Address is required.',
                            'cons.*.*.city_3.required' => 'Address is required.',
                            'cons.*.*.barangay_3.required' => 'Address is required.',
                            'cons.*.*.postal_3.required' => 'Address is required.',

                            'cons.*.*.declared_value_3.required' => 'Declared Value is required.',
                            'cons.*.*.description_goods_3.required' => 'Description of Goods is required.',
                            'cons.*.*.transposrt_mode_3.required' => 'Transport Mode is required.',
                            'cons.*.*.service_mode_3.required' => 'Service is required.',
                            'cons.*.*.mode_of_payment_3.required' => 'Mode of Payment is required.',
                            'cons.*.*.charge_to_3.required' => 'Charge to is required.',
                            'cons.*.*.charge_to_name_3.required' => 'Charge to is required.',
                            'cons.*.*.stype_3.sometimes' => 'Service Type is required.',

                            'cardets.*.*.quantity_3.sometimes' => 'Quantity is required.',
                            'cardets.*.*.weight_3.sometimes' => 'Weight is required.',
                            'cardets.*.*.length_3.sometimes' => 'Length is required.',
                            'cardets.*.*.width_3.sometimes' => 'Width is required.',
                            'cardets.*.*.height_3.sometimes' => 'Height is required.',
                            'attachments.*.attachment.sometimes' => 'This attachment field is required.',
                            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
                        ]
                    );
                        // dd($validator);

                } else {
                    $validator = Validator::make($request, $rules, [

                        'cons.*.*.customer_no_3.required' => 'Customer Number field is required.',
                        'cons.*.*.company_name_3.required' => 'Company Name is required.',
                        'cons.*.*.first_name_3.required' => 'First Name is required.',
                        'cons.*.*.last_name_3.required' => 'Last Name is required.',
                        'cons.*.*.mobile_number_3.required' => 'Mobile Number is required.',
                        'cons.*.*.email_address_3.required' => 'Email Address is required.',
                        'cons.*.*.address_3.required' => 'Address is required.',
                        'cons.*.*.state_3.required' => 'Address is required.',
                        'cons.*.*.city_3.required' => 'Address is required.',
                        'cons.*.*.barangay_3.required' => 'Address is required.',
                        'cons.*.*.postal_3.required' => 'Address is required.',

                        'cons.*.*.declared_value_3.required' => 'Declared Value is required.',
                        'cons.*.*.description_goods_3.required' => 'Description of Goods is required.',
                        'cons.*.*.transposrt_mode_3.required' => 'Transport Mode is required.',
                        'cons.*.*.service_mode_3.required' => 'Service is required.',
                        'cons.*.*.mode_of_payment_3.required' => 'Mode of Payment is required.',
                        'cons.*.*.charge_to_3.required' => 'Charge to is required.',
                        'cons.*.*.charge_to_name_3.required' => 'Charge to is required.',
                        'cons.*.*.stype_3.sometimes' => 'Service Type is required.',

                        'cardets.*.*.quantity_3.sometimes' => 'Quantity is required.',
                        'cardets.*.*.weight_3.sometimes' => 'Weight is required.',
                        'cardets.*.*.length_3.sometimes' => 'Length is required.',
                        'cardets.*.*.width_3.sometimes' => 'Width is required.',
                        'cardets.*.*.height_3.sometimes' => 'Height is required.',
                        'cardets.*.*.size_3.sometimes' => 'Size is required.',
                        'attachments.*.attachment.sometimes' => 'This attachment field is required.',
                        'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
                    ]);

                }
            }

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab4($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $validator = Validator::make($request, [], []);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd("entered", json_encode($request), $request);
        DB::beginTransaction();
        try {
            $response = $this->createValidationTab1($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated1 = $response['result'];

            $response = $this->createValidationTab2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated2 = $response['result'];
            // dd('asd123');

            $response = $this->createValidationTab3($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated3 = $response['result'];

            $validated = array_merge($validated1, $validated2, $validated3);
            $countconsignee = count($validated['cons']);
            // dd($validated);

            $ship_id = CrmCustomerInformation::where('account_no', $validated['customer_no_2'])->first();
            // $getbranch = UserDetails::where('user_id', Auth::user()->id)->first();

            if ($request['ismobile'] == 1) {
                $branchId = $request['branchids'];
            } else {
                $getbranch = UserDetails::where('user_id', Auth::user()->id)->first();
                $branchId = $getbranch->branch_id;
            }

            //  $book_branch_id = BranchReference::where('id', Auth::user()->id,)->first();

            $booking = CrmBooking::create([
                'booking_reference_no' => $validated['booking_reference_no_2'],
                'booking_type_id' => $validated['booking_type_1'],
                'vehicle_type_id' => $validated['vehicle_type_1'],
                'pickup_date' => $validated['pickup_date_1'],
                'time_slot_id' => $validated['time_slot_1'],
                'time_slot_from' => ($validated['time_slot_1'] == 1 ? $validated['time_from_1'] : null),
                'time_slot_to' => ($validated['time_slot_1'] == 1 ? $validated['time_to_1'] : null),
                'booking_category' => $request['booking_category_1'],
                'walk_in_branch_id' => $validated['walk_in_branch_1'],
                'activity_type' => $validated['activity_type_1'],
                'shipper_id' => $ship_id->id,
                'shipper_customer_no' => $validated['customer_no_2'],
                'consignee_category' => ($countconsignee > 1 ? 2 : $request['consignee_category_1']),
                'consignee_count' => ($countconsignee > 1 ? $countconsignee : 1),
                'work_instruction' => $request['work_instruction_3'],
                'remarks' => $request['remarks_3'],
                // 'assigned_team_id' => $validated['name'],
                'final_status_id' => ($validated['pickup_date_1'] == date('Y-m-d') ? 1 : 8),
                'booking_branch_id' => $branchId,
                // 'marketing_channel_id' => $validated['channel_1'],
                'booking_channel' => 1,
                'created_user' => Auth::user()->id,
            ]);


            $response = $this->attachments($booking, $validated['attachments']);
            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            // dd($booking);

            $shipper = CrmBookingShipper::create([
                'booking_id' => $booking->id,
                'customer_no' => $validated['customer_no_2'],
                'account_type_id' => $request['account_type_2'],
                'company_name' => ($request['account_type_2'] == 2 ? $validated['company_name_2'] : null),
                'name' => $validated['first_name_2'] . " " . $validated['middle_name_2'] . " " . $validated['last_name_2'],
                'first_name' => $validated['first_name_2'],
                'middle_name' => $validated['middle_name_2'],
                'last_name' => $validated['last_name_2'],
                'mobile_number' => $validated['mobile_number_2'],
                'email_address' => $validated['email_address_2'],
                'address' => $validated['address_2'],
                'state_id' => $validated['state2'],
                'city_id' => $validated['city2'],
                'barangay_id' => $validated['barangay2'],
                'postal_id' => $validated['postal2'],
            ]);

            $collectionShipperContact = $this->initializeFirestore()->collection('crm_booking_shipper');
            $documentShipperReference = $collectionShipperContact->add();
            $documentShipperReference->set([
                'id' => $shipper->id,
                'booking_id' => $booking->id,
                'customer_no' => $shipper->customer_no,
                'account_type_id' => $shipper->account_type_id,
                'company_name' => $shipper->company_name,
                'name' => $shipper->name,
                'first_name' => $shipper->first_name,
                'middle_name' => $shipper->middle_name,
                'last_name' => $shipper->last_name,
                'mobile_number' => $shipper->mobile_number,
                'email_address' => $shipper->email_address,
                'address' => $shipper->address,
                'state_id' => $shipper->state_id,
                'city_id' => $shipper->city_id,
                'barangay_id' => $shipper->barangay_id,
                'postal_id' => $shipper->postal_id,
            ]);


            // dd($shipper);

            $booklogs = CrmBookingLogs::create([
                'booking_id' => $booking->id,
                'booking_reference_no' => $validated['booking_reference_no_2'],
                'status_id' => 1,
                'user_id' => Auth::user()->id,
            ]);

            $bookremarks = CrmBookingRemarks::create([
                'booking_id' => $booking->id,
                'booking_reference_no' => $validated['booking_reference_no_2'],
                'remarks' => $request['remarks_3'],
                'status_id' => 1,
                'user_id' => Auth::user()->id,
            ]);


            $collectionConsigneeReference = $this->initializeFirestore()->collection('consignees');
            $documentConsigneeReference = $collectionConsigneeReference->add();

            $collectionCargoDeetsContact = $this->initializeFirestore()->collection('consignee_items');
            $documentDetailsReference = $collectionCargoDeetsContact->add();

            $collectionBookConContact = $this->initializeFirestore()->collection('crm_booking_consignee');

            foreach ($validated['cons'] as $a => $con) {
                $bookcons = CrmBookingConsignee::create([
                    'booking_id' => $booking->id,
                    'customer_no' => $validated['cons'][$a][$a]['customer_no_3'],
                    'account_type_id' => $validated['cons'][$a][$a]['account_type_3'],
                    'company_name' => ($validated['cons'][$a][$a]['account_type_3'] == 2 ? $validated['cons'][$a][$a]['company_name_3'] : null),
                    'name' => $validated['cons'][$a][$a]['first_name_3'] . " " . $validated['cons'][$a][$a]['middle_name_3'] . " " . $validated['cons'][$a][$a]['last_name_3'],
                    'first_name' => $validated['cons'][$a][$a]['first_name_3'],
                    'middle_name' => $validated['cons'][$a][$a]['middle_name_3'],
                    'last_name' => $validated['cons'][$a][$a]['last_name_3'],
                    'mobile_number' => $validated['cons'][$a][$a]['mobile_number_3'],
                    'email_address' => $validated['cons'][$a][$a]['email_address_3'],
                    'address' => $validated['cons'][$a][$a]['address_3'],
                    'declared_value' => preg_replace("/,/", '', $validated['cons'][$a][$a]['declared_value_3']),
                    'transposrt_mode_id' => $validated['cons'][$a][$a]['transposrt_mode_3'],
                    'service_mode_id' => $validated['cons'][$a][$a]['service_mode_3'],
                    'description_goods' => $validated['cons'][$a][$a]['description_goods_3'],
                    'mode_of_payment_id' => $validated['cons'][$a][$a]['mode_of_payment_3'],
                    // 'charge_to_id' => $validated['cons'][$a][$a]['charge_to_3'],
                    'charge_to_id' => ($validated['cons'][$a][$a]['account_type_3'] == 2 ? $validated['cons'][$a][$a]['charge_to_3'] : null),
                    // 'charge_to_name' => $validated['cons'][$a][$a]['charge_to_name_3'],
                    'charge_to_name' => ($validated['cons'][$a][$a]['account_type_3'] == 1 ? $validated['cons'][$a][$a]['charge_to_name_3'] : null),
                    'services_type_id' => $validated['cons'][$a][$a]['stype_3'],
                    'state_id' => $validated['cons'][$a][$a]['state_3'],
                    'city_id' => $validated['cons'][$a][$a]['city_3'],
                    'barangay_id' => $validated['cons'][$a][$a]['barangay_3'],
                    'postal_id' => $validated['cons'][$a][$a]['postal_3'],
                ], ['merge' => true]);

                // dd($validated['cons'][$a][$a]['stype_3']);

                //firestore add collecetion crm_booking_consignee
                $documentBookConReference = $collectionBookConContact->add([
                    'id' => $bookcons->id,
                    'booking_id' => $booking->id,
                    'customer_no' => $bookcons->customer_no,
                    'account_type_id' => $bookcons->account_type_id,
                    'company_name' => $bookcons->company_name,
                    'name' => $bookcons->name,
                    'first_name' => $bookcons->first_name,
                    'middle_name' => $bookcons->middle_name,
                    'last_name' => $bookcons->last_name,
                    'mobile_number' => $bookcons->mobile_number,
                    'email_address' => $bookcons->email_address,
                    'address' => $bookcons->address,
                    'declared_value' => $bookcons->declared_value,
                    'transposrt_mode_id' => $bookcons->transposrt_mode_id,
                    'service_mode_id' => $bookcons->service_mode_id,
                    'description_goods' => $bookcons->description_goods,
                    'mode_of_payment_id' => $bookcons->mode_of_payment_id,
                    'services_type_id' => $bookcons->services_type_id,
                    'charge_to_id' => $bookcons->charge_to_id,
                    'charge_to_name' => $bookcons->charge_to_name,
                    'state_id' => $bookcons->state_id,
                    'city_id' => $bookcons->city_id,
                    'barangay_id' => $bookcons->barangay_id,
                    'postal_id' => $bookcons->postal_id,
                ]);


                //firestore add collection consignee
                $consigneeList = [];
                $customerNo = $validated['cons'][$a][$a]['customer_no_3'];
                foreach ($validated['cons'] as $b => $con) {
                    $consigneeList[] = [
                        $validated['cons'][$b][$b]['customer_no_3'] => [
                            'account_type_3' => $validated['cons'][$b][$b]['account_type_3'],
                            'address_3' => $validated['cons'][$b][$b]['address_3'],
                            'barangay_3' => $validated['cons'][$b][$b]['barangay_3'],
                            'charge_to_3' => $validated['cons'][$b][$b]['charge_to_3'],
                            'charge_to_name_3' => $validated['cons'][$b][$b]['charge_to_name_3'],
                            'city_3' => $validated['cons'][$b][$b]['city_3'],
                            'company_name_3' => ($validated['cons'][$b][$b]['account_type_3'] == 2 ? $validated['cons'][$b][$b]['company_name_3'] : null),
                            'customer_no_3' => $validated['cons'][$b][$b]['customer_no_3'],
                            'declared_value_3' => intval(preg_replace("/,/", '', $validated['cons'][$b][$b]['declared_value_3'])),
                            'description_goods_3' => $validated['cons'][$b][$b]['description_goods_3'],
                            'email_address_3' => $validated['cons'][$b][$b]['email_address_3'],
                            'first_name_3' => $validated['cons'][$b][$b]['first_name_3'],
                            'is_deleted' => false,
                            'last_name_3' => $validated['cons'][$b][$b]['last_name_3'],
                            'middle_name_3' => $validated['cons'][$b][$b]['middle_name_3'],
                            'mobile_number_3' => $validated['cons'][$b][$b]['mobile_number_3'],
                            'mode_of_payment_3' => intval($validated['cons'][$b][$b]['mode_of_payment_3']),
                            'postal_3' => $validated['cons'][$b][$b]['postal_3'],
                            'service_mode_3' => intval($validated['cons'][$b][$b]['service_mode_3']),
                            'servmode_identify' => 1,
                            'state_3' => $validated['cons'][$b][$b]['state_3'],
                            'stype_3' => intval($validated['cons'][$b][$b]['stype_3']),
                            'transposrt_mode_3' => intval($validated['cons'][$b][$b]['transposrt_mode_3']),
                        ]
                    ];
                }

                $documentConsigneeReference->set([
                    'booking_reference_no_2' => $validated['booking_reference_no_2'],
                    'consignee_list' => $consigneeList,
                ]);

                if ($validated['cons'][$a][$a]['stype_3'] == 2) {
                    if (isset($validated['cardets'][$a])) {
                        foreach ($validated['cardets'][$a] as $c => $cardet) {
                            $bookcardet = CrmBookingCargoDetails::create([
                                'consignee_id' => $bookcons->id,
                                'quantity' => $validated['cardets'][$a][$c]['quantity_3'],
                                'size' => $validated['cardets'][$a][$c]['size_3'],
                            ]);
                        }
                    }
                } elseif ($validated['cons'][$a][$a]['stype_3'] == 3) {
                    if (isset($validated['cardets'][$a])) {
                        foreach ($validated['cardets'][$a] as $c => $cardet) {
                            $bookcardet = CrmBookingCargoDetails::create([
                                'consignee_id' => $bookcons->id,
                                'quantity' => $validated['cardets'][$a][$c]['quantity_3'],
                                'size' => $validated['cardets'][$a][$c]['size_3'],
                            ]);
                        }
                    }
                } else {
                    if (isset($validated['cardets'][$a])) {
                        foreach ($validated['cardets'][$a] as $c => $cardet) {
                            $bookcardet = CrmBookingCargoDetails::create([
                                'consignee_id' => $bookcons->id,
                                'quantity' => $validated['cardets'][$a][$c]['quantity_3'],
                                'weight' => $validated['cardets'][$a][$c]['weight_3'],
                                'length' => $validated['cardets'][$a][$c]['length_3'],
                                'width' => $validated['cardets'][$a][$c]['width_3'],
                                'height' => $validated['cardets'][$a][$c]['height_3'],
                            ]);
                        }
                    }
                }

                //firestore cargo details
                $customerDetails = [];
                if (isset($validated['cardets'][$a])) {
                    foreach ($validated['cardets'][$a] as $c => $cardet) {
                        $customerDetails[] = [
                            $c + 1 => [
                                'height_3' => intval($validated['cardets'][$a][$c]['height_3']) === 0 ? null : intval($validated['cardets'][$a][$c]['height_3']),
                                'id' => null,
                                'is_deleted' => false,
                                'length_3' => intval($validated['cardets'][$a][$c]['length_3']) === 0 ? null : intval($validated['cardets'][$a][$c]['length_3']),
                                'quantity_3' => intval($validated['cardets'][$a][$c]['quantity_3']) === 0 ? null : intval($validated['cardets'][$a][$c]['quantity_3']),
                                'size_3' => intval($validated['cardets'][$a][$c]['size_3']) === 0 ? null : intval($validated['cardets'][$a][$c]['size_3']),
                                'weight_3' => intval($validated['cardets'][$a][$c]['weight_3']) === 0 ? null : intval($validated['cardets'][$a][$c]['weight_3']),
                                'width_3' => intval($validated['cardets'][$a][$c]['width_3']) === 0 ? null : intval($validated['cardets'][$a][$c]['width_3']),
                            ],
                        ];
                    }
                    $cargoDeetsList[] = [
                        strval($customerNo) => $customerDetails,
                    ];
                }
            }
            $documentDetailsReference->set([
                'booking_reference_no_2' => $validated['booking_reference_no_2'],
                'cargoDetails_list' => $cargoDeetsList,
            ]);

            // dd($bookcons, $bookcardet);

            // dd($bookcons);

            //firestore Orders(booking and shipping data)
            $attachments = [];
            $collectionOrderReference = $this->initializeFirestore()->collection('orders');
            $query = $collectionOrderReference
                ->where('booking_reference_no_2', '=', $validated['booking_reference_no_2'])
                ->limit(1);
            $documents = $query->documents();
            if ($documents->isEmpty()) {
                $documentOrderReference = $collectionOrderReference->add(
                    [
                        'account_type_2' => $request['account_type_2'],
                        'activity_type_1' => intval($validated['activity_type_1']),
                        'address_2' => $validated['address_2'],
                        'attachments' =>  $attachments,
                        'barangay2' => $validated['barangay2'],
                        'booking_category_1' => $request['booking_category_1'],
                        'booking_reference_no_2' => $validated['booking_reference_no_2'],
                        'booking_type_1' => intval($validated['booking_type_1']),
                        'city2' => $validated['city2'],
                        'company_name_2' => ($request['account_type_2'] == 2 ? $validated['company_name_2'] : null),
                        'consignee_category_1' => ($countconsignee > 1 ? 2 : $request['consignee_category_1']),
                        'consignee_count_1' => ($countconsignee > 1 ? $countconsignee : 1),
                        'customer_no_2' => $validated['customer_no_2'],
                        'date_dispatched' => "",
                        'email_address_2' => $validated['email_address_2'],
                        'final_status' => ($validated['pickup_date_1'] == date('Y-m-d') ? 1 : 8),
                        'first_name_2' => $validated['first_name_2'],
                        'from' => 'web',
                        'last_name_2' => $validated['last_name_2'],
                        'middle_name_2' => $validated['middle_name_2'],
                        'mobile_number_2' => $validated['mobile_number_2'],
                        // 'order_type' => $validated['booking_category_1'],
                        'pickup_date_1' => $validated['pickup_date_1'],
                        'postal2' => $validated['postal2'],
                        'remarks_3' => $request['remarks_3'],
                        'state2' => $validated['state2'],
                        'team_route_id' => 0,
                        'time_1' => $validated['time_1'] ?? "",
                        'time_from_1' => ($validated['time_slot_1'] == 1 ? $validated['time_from_1'] : null),
                        'time_slot_1' => intval($validated['time_slot_1']),
                        'time_to_1' => ($validated['time_slot_1'] == 1 ? $validated['time_to_1'] : null),
                        'vehicle_type_1' => intval($validated['vehicle_type_1']),
                        'walk_in_branch_1' => $validated['walk_in_branch_1'],
                        'work_instruction_3' => $request['work_instruction_3'],
                    ]
                );
            }
            DB::commit();
            // dd($validated['booking_reference_no_2']);

            return $this->response(
                200,
                'Booking has been successfully submitted! Your Booking Reference Number is',
                $validated['booking_reference_no_2'],
                compact($booking, $shipper, $booklogs, $bookremarks, $bookcons, $bookcardet)
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function attachments($booking, $attachments)
    {
        // dd($attachments);
        DB::beginTransaction();
        try {
            $date = date('Y/m/d', strtotime($booking->created_at));

            foreach ($attachments as $attachment) {

                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $booking->BookingAttachmentHasManyBK()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(" ", "_", 'sales/' . $date . '/' . 'booking-mgmt/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        Storage::disk('crm_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } else if (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('crm_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }
                    $booking->BookingAttachmentHasManyBK()->updateOrCreate(
                        [
                            'id' => $attachment['id']
                        ],
                        [
                            'booking_id' => $booking->id,
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                        ]
                    );
                }
            }
            DB::commit();
            return $this->response(200, 'Booking Attachment Successfully Attached', $booking);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function editValidationTab1($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'booking_type_1' => 'required',
                'vehicle_type_1' => 'required',
                'pickup_date_1' => 'required',
                'time_slot_1' => 'required',
                'consignee_count_1' => ($request['consignee_category_1'] == 2 ? 'required' : 'sometimes'),
                'walk_in_branch_1' => ($request['booking_category_1'] == 2 ? 'required' : 'sometimes'),
                'activity_type_1' => 'required',
                'time_from_1' => ($request['time_slot_1'] == 1 ? 'required' : 'sometimes'),
                'time_to_1' => ($request['time_slot_1'] == 1 ? 'required' : 'sometimes'),
                // 'channel_1' => 'required',
            ], [
                'booking_type_1.required' => 'Booking Type is Required',
                'vehicle_type_1.required' => 'Vehicle Type is Required',
                'pickup_date_1.required' => 'Pickup Date is Required',
                'time_slot_1.required' => 'Time Slot is Required',
                'consignee_count_1.required' => 'Total of Consignee is Required',
                'walk_in_branch_1.required' => 'Branch is Required',
                'activity_type_1.required' => 'Activity Type is Required',
                'time_from_1.required' => 'Time is Required',
                'time_to_1.required' => 'Time is Required',
                // 'channel_1.required' => 'Marketing Channel Type is Required',
            ]);

            // dd($validator);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function editValidationTab2($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $validator = Validator::make($request, [
                'booking_reference_no_2' => 'required',
                'customer_no_2' => 'required',
                'company_name_2' => ($request['account_type_2'] == 2 ? 'required' : 'sometimes'),
                'first_name_2' => 'required',
                'middle_name_2' => 'sometimes',
                'last_name_2' => 'required',
                'mobile_number_2' => 'required',
                'email_address_2' => 'required',
                'address_2' => 'required',
                'state2' => 'required',
                'city2' => 'required',
                'barangay2' => 'required',
                'postal2' => 'required',
            ], [
                'booking_reference_no_2.required' => 'Booking Reference is Required',
                'customer_no_2.required' => 'Customer Number is Required',
                'company_name_2.required' => 'Company Name is Required',
                'first_name_2.required' => 'First Name is Required',
                'middle_name_2.sometimes' => '',
                'last_name_2.required' => 'Last Name is Required',
                'mobile_number_2.required' => 'Contact Number is Required',
                'email_address_2.required' => 'Email Address is Required',
                'address_2.required' => 'Address is Required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function editValidationTab3($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $rules = [
                'work_instruction_3' => 'sometimes',
                'remarks_3' => 'sometimes',
                'attachments' => 'sometimes',
                'attachments.*.attachment.sometimes' => 'sometimes|' . config('filesystems.crm_sales_booking_mgmt_validation'),
            ];
            // foreach ($request['cons'] as $i => $con) {
            //     $rules['cons.' . $i . '.' . $i . '.id'] = 'sometimes';
            //     $rules['cons.' . $i . '.' . $i . '.customer_no_3'] = 'required';
            //     if ($request['cons'][$i][$i]['account_type_3'] == 2) {
            //         $rules['cons.' . $i . '.' . $i . '.company_name_3'] = 'required';
            //     } else {
            //         $rules['cons.' . $i . '.' . $i . '.company_name_3'] = 'sometimes';
            //     }
            //     $rules['cons.' . $i . '.' . $i . '.account_type_3'] = 'sometimes';
            //     $rules['cons.' . $i . '.' . $i . '.first_name_3'] = 'required';
            //     $rules['cons.' . $i . '.' . $i . '.middle_name_3'] = 'sometimes';
            //     $rules['cons.' . $i . '.' . $i . '.last_name_3'] = 'required';
            //     $rules['cons.' . $i . '.' . $i . '.mobile_number_3'] = 'required';
            //     $rules['cons.' . $i . '.' . $i . '.email_address_3'] = 'required';
            //     $rules['cons.' . $i . '.' . $i . '.address_3'] = 'required';
            //     $rules['cons.' . $i . '.' . $i . '.state_3'] = 'required';
            //     $rules['cons.' . $i . '.' . $i . '.city_3'] = 'required';
            //     $rules['cons.' . $i . '.' . $i . '.barangay_3'] = 'required';
            //     $rules['cons.' . $i . '.' . $i . '.postal_3'] = 'required';

            //     $rules['cons.' . $i . '.' . $i . '.declared_value_3'] = 'required';
            //     $rules['cons.' . $i . '.' . $i . '.description_goods_3'] = 'required';
            //     $rules['cons.' . $i . '.' . $i . '.transposrt_mode_3'] = 'required';
            //     $rules['cons.' . $i . '.' . $i . '.service_mode_3'] = 'required';
            //     $rules['cons.' . $i . '.' . $i . '.mode_of_payment_3'] = 'required';
            //     $rules['cons.' . $i . '.' . $i . '.stype_3'] = 'sometimes';
            //     $rules['cons.' . $i . '.' . $i . '.is_deleted'] = 'sometimes';
            //     if (isset($request['cardets'][$i])) {
            //         foreach ($request['cardets'][$i] as $c => $cardet) {
            //             $rules['cardets.' . $i . '.' . $c . '.id'] = 'sometimes';
            //             $rules['cardets.' . $i . '.' . $c . '.quantity_3'] = 'sometimes';
            //             $rules['cardets.' . $i . '.' . $c . '.weight_3'] = 'sometimes';
            //             $rules['cardets.' . $i . '.' . $c . '.length_3'] = 'sometimes';
            //             $rules['cardets.' . $i . '.' . $c . '.width_3'] = 'sometimes';
            //             $rules['cardets.' . $i . '.' . $c . '.height_3'] = 'sometimes';
            //             $rules['cardets.' . $i . '.' . $c . '.is_deleted'] = 'sometimes';
            //         }
            foreach ($request['cons'] as $i => $con) {
                $rules['cons.' . $i . '.' . $i . '.id'] = 'sometimes';
                $rules['cons.' . $i . '.' . $i . '.customer_no_3'] = 'required';
                if ($request['cons'][$i][$i]['account_type_3'] == 2) {
                    $rules['cons.' . $i . '.' . $i . '.company_name_3'] = 'required';
                    $rules['cons.' . $i . '.' . $i . '.charge_to_3'] = 'required';
                    $rules['cons.' . $i . '.' . $i . '.charge_to_name_3'] = 'sometimes';
                } else {
                    $rules['cons.' . $i . '.' . $i . '.company_name_3'] = 'sometimes';
                    $rules['cons.' . $i . '.' . $i . '.charge_to_3'] = 'sometimes';
                    $rules['cons.' . $i . '.' . $i . '.charge_to_name_3'] = 'required';
                }
                $rules['cons.' . $i . '.' . $i . '.account_type_3'] = 'sometimes';
                $rules['cons.' . $i . '.' . $i . '.first_name_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.middle_name_3'] = 'sometimes';
                $rules['cons.' . $i . '.' . $i . '.last_name_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.mobile_number_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.email_address_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.address_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.state_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.city_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.barangay_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.postal_3'] = 'required';


                $rules['cons.' . $i . '.' . $i . '.declared_value_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.description_goods_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.transposrt_mode_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.service_mode_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.mode_of_payment_3'] = 'required';
                $rules['cons.' . $i . '.' . $i . '.stype_3'] = 'sometimes';
                $rules['cons.' . $i . '.' . $i . '.is_deleted'] = 'sometimes';

                if (isset($request['cardets'][$i])) {
                    foreach ($request['cardets'][$i] as $c => $cardet) {
                        $rules['cardets.' . $i . '.' . $c . '.id'] = 'sometimes';
                        $rules['cardets.' . $i . '.' . $c . '.quantity_3'] = 'sometimes';
                        $rules['cardets.' . $i . '.' . $c . '.weight_3'] = 'sometimes';
                        $rules['cardets.' . $i . '.' . $c . '.length_3'] = 'sometimes';
                        $rules['cardets.' . $i . '.' . $c . '.width_3'] = 'sometimes';
                        $rules['cardets.' . $i . '.' . $c . '.height_3'] = 'sometimes';
                        $rules['cardets.' . $i . '.' . $c . '.size_3'] = 'sometimes';
                        $rules['cardets.' . $i . '.' . $c . '.is_deleted'] = 'sometimes';
                    }


                    $validator = Validator::make(
                        $request,
                        $rules,
                        [
                            'cons.*.*.customer_no_3.required' => 'Customer Numbers is required.',
                            'cons.*.*.company_name_3.required' => 'Company Name is required.',
                            'cons.*.*.first_name_3.required' => 'First Name is required.',
                            'cons.*.*.last_name_3.required' => 'Last Name is required.',
                            'cons.*.*.mobile_number_3.required' => 'Mobile Number is required.',
                            'cons.*.*.email_address_3.required' => 'Email Address is required.',
                            'cons.*.*.address_3.required' => 'Address is required.',
                            'cons.*.*.state_3.required' => 'Address is required.',
                            'cons.*.*.city_3.required' => 'Address is required.',
                            'cons.*.*.barangay_3.required' => 'Address is required.',
                            'cons.*.*.postal_3.required' => 'Address is required.',

                            'cons.*.*.declared_value_3.required' => 'Declared Value is required.',
                            'cons.*.*.description_goods_3.required' => 'Description of Goods is required.',
                            'cons.*.*.transposrt_mode_3.required' => 'Transport Mode is required.',
                            'cons.*.*.service_mode_3.required' => 'Service is required.',
                            'cons.*.*.mode_of_payment_3.required' => 'Mode of Payment is required.',
                            'cons.*.*.charge_to_3.required' => 'Charge to is required.',
                            'cons.*.*.charge_to_name_3.required' => 'Charge to is required.',
                            'cons.*.*.stype_3.sometimes' => 'Service Type is required.',

                            'cardets.*.*.quantity_3.sometimes' => 'Quantity is required.',
                            'cardets.*.*.weight_3.sometimes' => 'Weight is required.',
                            'cardets.*.*.length_3.sometimes' => 'Length is required.',
                            'cardets.*.*.width_3.sometimes' => 'Width is required.',
                            'cardets.*.*.height_3.sometimes' => 'Height is required.',
                            'attachments.*.attachment.sometimes' => 'This attachment field is required.',
                            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
                        ]
                    );
                } else {
                    $validator = Validator::make($request, $rules, [

                        'cons.*.*.customer_no_3.required' => 'Customer Number field is required.',
                        'cons.*.*.company_name_3.required' => 'Company Name is required.',
                        'cons.*.*.first_name_3.required' => 'First Name is required.',
                        'cons.*.*.last_name_3.required' => 'Last Name is required.',
                        'cons.*.*.mobile_number_3.required' => 'Mobile Number is required.',
                        'cons.*.*.email_address_3.required' => 'Email Address is required.',
                        'cons.*.*.address_3.required' => 'Address is required.',
                        'cons.*.*.state_3.required' => 'Address is required.',
                        'cons.*.*.city_3.required' => 'Address is required.',
                        'cons.*.*.barangay_3.required' => 'Address is required.',
                        'cons.*.*.postal_3.required' => 'Address is required.',

                        'cons.*.*.declared_value_3.required' => 'Declared Value is required.',
                        'cons.*.*.description_goods_3.required' => 'Description of Goods is required.',
                        'cons.*.*.transposrt_mode_3.required' => 'Transport Mode is required.',
                        'cons.*.*.service_mode_3.required' => 'Service is required.',
                        'cons.*.*.mode_of_payment_3.required' => 'Mode of Payment is required.',
                        'cons.*.*.charge_to_3.required' => 'Charge to is required.',
                        'cons.*.*.charge_to_name_3.required' => 'Charge to is required.',
                        'cons.*.*.stype_3.sometimes' => 'Service Type is required.',

                        'cardets.*.*.quantity_3.sometimes' => 'Quantity is required.',
                        'cardets.*.*.weight_3.sometimes' => 'Weight is required.',
                        'cardets.*.*.length_3.sometimes' => 'Length is required.',
                        'cardets.*.*.width_3.sometimes' => 'Width is required.',
                        'cardets.*.*.height_3.sometimes' => 'Height is required.',
                        'cardets.*.*.size_3.sometimes' => 'Size is required.',
                        'attachments.*.attachment.sometimes' => 'This attachment field is required.',
                        'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
                    ]);
                }
            }
            // dd($request);
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function editValidationTab4($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $validator = Validator::make($request, [], []);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $book_result = CrmBooking::with(
                'BookingShipper',
                'ShipperReferenceBK',
                'attachments',
                'BookingRemarksHasManyBK',
                'BookingLogsHasManyBK',
                'CreatedByBK',
                'BookingConsigneeHasManyBK'
            )->findOrFail($id);
            // $book_result = CrmBooking::findOrFail($id);
            if (!$book_result) {
                return $this->response(404, 'Booking', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Booking', $book_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        // dd($request);
        // dd(json_encode($request));
        // dd("entered", json_encode($request), $request);
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $booking = $response['result'];

            $response = $this->editValidationTab1($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated1 = $response['result'];

            $response = $this->editValidationTab2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated2 = $response['result'];

            $response = $this->editValidationTab3($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated3 = $response['result'];
            $validated = array_merge($validated1, $validated2, $validated3);

            $countconsignee = 0;
            foreach ($validated['cons'] as $a => $con) {
                if ($validated['cons'][$a][$a]['is_deleted'] == false) {
                    $countconsignee = count($validated['cons']);
                }
            }
            $countconsignee = count($validated['cons']);

            $ship_id = CrmCustomerInformation::where('account_no', $validated['customer_no_2'])->first();
            $book_branch_id = BranchReference::where('id', Auth::user()->id,)->first();

            $booking->update([
                'booking_reference_no' => $validated['booking_reference_no_2'],
                'booking_type_id' => $validated['booking_type_1'],
                'vehicle_type_id' => $validated['vehicle_type_1'],
                'pickup_date' => $validated['pickup_date_1'],
                'time_slot_id' => $validated['time_slot_1'],
                'time_slot_from' => ($validated['time_slot_1'] == 1 ? $validated['time_from_1'] : null),
                'time_slot_to' => ($validated['time_slot_1'] == 1 ? $validated['time_to_1'] : null),
                'booking_category' => $request['booking_category_1'],
                'walk_in_branch_id' => $validated['walk_in_branch_1'],
                'activity_type' => $validated['activity_type_1'],
                'shipper_id' => $ship_id->id,
                'shipper_customer_no' => $validated['customer_no_2'],
                'consignee_category' => ($countconsignee > 1 ? 2 : $request['consignee_category_1']),
                'consignee_count' => ($countconsignee > 1 ? $countconsignee : 1),
                'work_instruction' => $request['work_instruction_3'],
                'remarks' => $request['remarks_3'],
                // 'assigned_team_id' => $validated['name'],
                'final_status_id' => ($validated['pickup_date_1'] == date('Y-m-d') ? 1 : 8),
                'booking_branch_id' => $book_branch_id->id,
                // 'marketing_channel_id' => $validated['channel_1'],
                // 'final_status_id' => 1,
                'created_user' => Auth::user()->id,
            ]);

            $response = $this->attachments($booking, $validated['attachments']);


            $booking->BookingShipper->update([
                'booking_id' => $id,
                'customer_no' => $validated['customer_no_2'],
                'account_type_id' => $request['account_type_2'],
                'company_name' => ($request['account_type_2'] == 2 ? $validated['company_name_2'] : null),
                'name' => $validated['first_name_2'] . " " . $validated['middle_name_2'] . " " . $validated['last_name_2'],
                'first_name' => $validated['first_name_2'],
                'middle_name' => $validated['middle_name_2'],
                'last_name' => $validated['last_name_2'],
                'mobile_number' => $validated['mobile_number_2'],
                'email_address' => $validated['email_address_2'],
                'address' => $validated['address_2'],
                'state_id' => $validated['state2'],
                'city_id' => $validated['city2'],
                'barangay_id' => $validated['barangay2'],
                'postal_id' => $validated['postal2'],

            ]);
            // dd($validated['cons'], $validated['cardets']);
            foreach ($validated['cons'] as $a => $con) {

                $b = 0;
                if (isset($validated['cardets'][$a])) {
                    foreach ($validated['cardets'][$a] as $c => $subpro) {

                        // dd($validated['subpros'][$a][$a]['id']);
                        if ($validated['cardets'][$a][$c]['id'] && $validated['cardets'][$a][$c]['is_deleted'] == true) {

                            $consignedetbook = CrmBookingCargoDetails::find($validated['cardets'][$a][$c]['id']);
                            $consignedetbook->delete();
                        }
                        $b++;
                    }
                }

                if ($validated['cons'][$a][$a]['is_deleted'] == true) {

                    if ($validated['cons'][$a][$a]['id']) {
                        $consignebook = CrmBookingConsignee::find($validated['cons'][$a][$a]['id']);
                        $consignebook->delete();
                    }
                } else {
                    $bookcons = CrmBookingConsignee::updateOrCreate([
                        'id' => $validated['cons'][$a][$a]['id'],
                    ], [
                        'booking_id' => $id,
                        'customer_no' => $validated['cons'][$a][$a]['customer_no_3'],
                        'account_type_id' => $validated['cons'][$a][$a]['account_type_3'],
                        'company_name' => ($validated['cons'][$a][$a]['account_type_3'] == 2 ? $validated['cons'][$a][$a]['company_name_3'] : null),
                        'name' => $validated['cons'][$a][$a]['first_name_3'] . " " . $validated['cons'][$a][$a]['middle_name_3'] . " " . $validated['cons'][$a][$a]['last_name_3'],
                        'first_name' => $validated['cons'][$a][$a]['first_name_3'],
                        'middle_name' => $validated['cons'][$a][$a]['middle_name_3'],
                        'last_name' => $validated['cons'][$a][$a]['last_name_3'],
                        'mobile_number' => $validated['cons'][$a][$a]['mobile_number_3'],
                        'email_address' => $validated['cons'][$a][$a]['email_address_3'],
                        'address' => $validated['cons'][$a][$a]['address_3'],
                        'declared_value' => preg_replace("/,/", '', $validated['cons'][$a][$a]['declared_value_3']),
                        'transposrt_mode_id' => $validated['cons'][$a][$a]['transposrt_mode_3'],
                        'service_mode_id' => $validated['cons'][$a][$a]['service_mode_3'],
                        'description_goods' => $validated['cons'][$a][$a]['description_goods_3'],
                        'mode_of_payment_id' => $validated['cons'][$a][$a]['mode_of_payment_3'],
                        'charge_to_id' => ($validated['cons'][$a][$a]['account_type_3'] == 2 ? $validated['cons'][$a][$a]['charge_to_3'] : null),
                        'charge_to_name' => ($validated['cons'][$a][$a]['account_type_3'] == 1 ? $validated['cons'][$a][$a]['charge_to_name_3'] : null),
                        'services_type_id' => $validated['cons'][$a][$a]['stype_3'],
                        'state_id' => $validated['cons'][$a][$a]['state_3'],
                        'city_id' => $validated['cons'][$a][$a]['city_3'],
                        'barangay_id' => $validated['cons'][$a][$a]['barangay_3'],
                        'postal_id' => $validated['cons'][$a][$a]['postal_3'],

                    ]);
                }


                if (isset($validated['cardets'][$a])) {
                    foreach ($validated['cardets'][$a] as $c => $subpro) {

                        // dd($validated['subpros'][$a][$a]['id']);
                        if ($validated['cardets'][$a][$c]['id'] && $validated['cardets'][$a][$c]['is_deleted'] == true) {
                        } else {

                            // dd($bookcons->id);
                            // CrmBookingCargoDetails::updateOrCreate([
                            //     'id' => $validated['cardets'][$a][$c]['id'],
                            // ], [
                            //     'consignee_id' => $bookcons->id,
                            //     'quantity' => $validated['cardets'][$a][$c]['quantity_3'],
                            //     'weight' => $validated['cardets'][$a][$c]['weight_3'],
                            //     'length' => $validated['cardets'][$a][$c]['length_3'],
                            //     'width' => $validated['cardets'][$a][$c]['width_3'],
                            //     'height' => $validated['cardets'][$a][$c]['height_3'],
                            // ]);
                            if ($validated['cons'][$a][$a]['stype_3'] == 2) {
                                CrmBookingCargoDetails::updateOrCreate([
                                    'id' => $validated['cardets'][$a][$c]['id'],
                                ], [
                                    'consignee_id' => $bookcons->id,
                                    'quantity' => $validated['cardets'][$a][$c]['quantity_3'],
                                    'size' => $validated['cardets'][$a][$c]['size_3'],
                                ]);
                            } elseif ($validated['cons'][$a][$a]['stype_3'] == 3) {
                                CrmBookingCargoDetails::updateOrCreate([
                                    'id' => $validated['cardets'][$a][$c]['id'],
                                ], [
                                    'consignee_id' => $bookcons->id,
                                    'quantity' => $validated['cardets'][$a][$c]['quantity_3'],
                                    'size' => $validated['cardets'][$a][$c]['size_3'],
                                ]);
                            } else {
                                CrmBookingCargoDetails::updateOrCreate([
                                    'id' => $validated['cardets'][$a][$c]['id'],
                                ], [
                                    'consignee_id' => $bookcons->id,
                                    'quantity' => $validated['cardets'][$a][$c]['quantity_3'],
                                    'weight' => $validated['cardets'][$a][$c]['weight_3'],
                                    'length' => $validated['cardets'][$a][$c]['length_3'],
                                    'width' => $validated['cardets'][$a][$c]['width_3'],
                                    'height' => $validated['cardets'][$a][$c]['height_3'],
                                ]);
                            }
                        }
                    }
                }
            }
            // dd('asdasd');
            $booking->BookingLogsHasManyBK[0]->update([
                'booking_id' => $id,
                'booking_reference_no' => $validated['booking_reference_no_2'],
                'status_id' => 1,
                'user_id' => Auth::user()->id,
            ]);




            $booking->BookingRemarksHasManyBK[0]->update([
                'booking_id' => $id,
                'booking_reference_no' => $validated['booking_reference_no_2'],
                'remarks' => $request['remarks_3'],
                'status_id' => 1,
                'user_id' => Auth::user()->id,
            ]);

            DB::commit();

            return $this->response(
                200,
                'Booking has been successfully submitted! Your Booking Reference Number is',
                $validated['booking_reference_no_2'],
                compact($booking)
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function cancelationValidation($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $validator = Validator::make($request, [
                'cancel_booking_idx' => 'required',

            ], [
                'cancel_booking_idx.required' => 'Select Atleast One',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function bookcancel($request, $id)
    {
        // dd($data['id']);
        DB::beginTransaction();
        try {
            $response = $this->cancelationValidation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $book = CrmBooking::where('id', $id)->first();

            $Cncl = CrmBookingLogs::create(
                [
                    'booking_id' => $book->id,
                    'booking_reference_no' => $book->booking_reference_no,
                    'status_id' => 9,
                    'cancel_reason' => $validated['cancel_booking_idx'],
                    'user_id' => Auth::user()->id,
                ]
            );
            $book->update([
                'final_status_id' => 9,
                'approval_status' => NULL,
            ]);


            DB::commit();

            return $this->response(200, 'Booking Has been successfully Cancelled!', $Cncl, $book);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function rescheduleValidation($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            $validator = Validator::make($request, [
                'reschedule_booking_idx' => 'required',

            ], [
                'reschedule_booking_idx.required' => 'Select Atleast One',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function bookresched($request, $id)
    {
        // dd($data['id']);
        DB::beginTransaction();
        try {
            $response = $this->rescheduleValidation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $bookres = CrmBooking::where('id', $id)->first();

            $Rschd = CrmBookingLogs::create(
                [
                    'booking_id' => $bookres->id,
                    'booking_reference_no' => $bookres->booking_reference_no,
                    'status_id' => 10,
                    'resched_reason' => $validated['reschedule_booking_idx'],
                    'user_id' => Auth::user()->id,
                ]
            );
            $bookres->update([
                'final_status_id' => 10,
                'approval_status' => NULL,
            ]);



            DB::commit();

            return $this->response(200, 'Booking Has been successfully Rescheduled!', $Rschd, $bookres);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function bookForConfirm($request, $id)
    {
        // dd($data['id']);
        DB::beginTransaction();
        try {

            $bookFC = CrmBooking::where('id', $id)->first();

            $bookFC->update([
                'final_status_id' => 2,
            ]);


            DB::commit();

            return $this->response(200, 'Booking Has been successfully Updated!', $bookFC);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function bookConfirmed($request, $id)
    {
        // dd($data['id']);
        DB::beginTransaction();
        try {

            $bookC = CrmBooking::where('id', $id)->first();

            $bookC->update([
                'final_status_id' => 3,
            ]);


            DB::commit();

            return $this->response(200, 'Booking Has been successfully Updated!', $bookC);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function bookOngoing($request, $id)
    {
        // dd($data['id']);
        DB::beginTransaction();
        try {

            $bookO = CrmBooking::where('id', $id)->first();

            $bookO->update([
                'final_status_id' => 4,
            ]);


            DB::commit();

            return $this->response(200, 'Booking Has been successfully Updated!', $bookO);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function bookCompleted($request, $id)
    {
        // dd($data['id']);
        DB::beginTransaction();
        try {

            $bookCD = CrmBooking::where('id', $id)->first();

            $bookCD->update([
                'final_status_id' => 5,
            ]);


            DB::commit();

            return $this->response(200, 'Booking Has been successfully Updated!', $bookCD);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
