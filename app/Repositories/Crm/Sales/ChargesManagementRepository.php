<?php

namespace App\Repositories\Crm\Sales;

use App\Interfaces\Crm\Sales\ChargesManagementInterface;
use App\Models\Crm\ChargesManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class ChargesManagementRepository implements ChargesManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $charges_list = ChargesManagement::paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Charges List', compact('charges_list'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'rate_calcu_charges' => 'required|unique:charges_management,rate_calcu_charges',
                'charges_category' => 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'rate_calcu_charges.required' => 'Rate calculator charges field is required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $charges_management = ChargesManagement::create([
                'rate_calcu_charges' => $validated['rate_calcu_charges'],
                'charges_category' => $validated['charges_category'],
                'status' => 1,
            ]);

            DB::commit();

            return $this->response(200, 'New rate calculator charges has been successfully added!', $charges_management);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $charges_management = ChargesManagement::findOrFail($id);
            if (!$charges_management) {
                return $this->response(404, 'Charge', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Charge', $charges_management);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'rate_calcu_charges' => 'required|unique:charges_management,rate_calcu_charges,' . $id . ',id',
                'charges_category' => 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'rate_calcu_charges.required' => 'Rate calculator charges field is required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $charges_management = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $charges_management->update([
                'rate_calcu_charges' => $validated['rate_calcu_charges'],
                'charges_category' => $validated['charges_category'],
            ]);

            DB::commit();

            return $this->response(200, 'Rate calculator charges has been successfully updated!', $charges_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update_status($id, $status)
    {
        DB::beginTransaction();
        try {
            $update_status = ChargesManagement::find($id);
            if (!$update_status) return $this->response(404, 'Charge', 'Not Found!');

            $update_status->update([
                'status' => $status
            ]);

            DB::commit();
            if ($status == 2) {
                return $this->response(200, 'Rate calculator charges has been successfully deactivated!', $update_status);
            } else {
                return $this->response(200, 'Rate calculator charges has been successfully activated!', $update_status);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
