<?php

namespace App\Repositories\Crm\Sales\Leads\Qualification;

use App\Interfaces\Crm\Sales\Leads\Qualification\QualificationMgmtInterface;
use App\Models\CrmQualificationMgmt;
use App\Models\CrmRateLoa;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class QualificationMgmtRepository implements QualificationMgmtInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $question_s = CrmQualificationMgmt::
            
                paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'List', compact('question_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'question' => 'required',
                'qualification_type' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $question_s = CrmQualificationMgmt::create([
                'question' => $validated['question'],
                'qualification_type' => $validated['qualification_type'],
            ]);

            DB::commit();

            return $this->response(200, 'Questionnaire has been Successfully Created!', $question_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $question_s = CrmQualificationMgmt::findOrFail($id);
            if (!$question_s) {
                return $this->response(404, 'Questionnaire', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Questionnaire', $question_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $question_s = $response['result'];

            $response = $this->validation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $question_s->update([
                'question' => $validated['question'],
                'qualification_type' => $validated['qualification_type'],
            ]);

            DB::commit();

            return $this->response(200, 'Questionnaire has been successfully updated!', $question_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
         
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $question_s = CrmQualificationMgmt::find($id);
            if (!$question_s) return $this->response(404, 'Questionnaire', 'Not Found!');
            $question_s->delete();

            DB::commit();
            return $this->response(200, 'Questionnaire Successfully Deleted!', $question_s);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
