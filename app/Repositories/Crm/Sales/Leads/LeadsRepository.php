<?php

namespace App\Repositories\Crm\Sales\Leads;

use App\Interfaces\Crm\Sales\Leads\LeadsInterface;
use App\Models\Crm\CrmLead;
use App\Models\Crm\CrmOpportunity;
use App\Models\Crm\CrmQualificationQuestionnaire;
use App\Models\Crm\CrmSrQualificationQuestion;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LeadsRepository implements LeadsInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $leads = CrmLead::with('account')->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'List', compact('leads'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function saveIfNotExisting()
    {
        DB::beginTransaction();
        try {
            $questionares = CrmQualificationQuestionnaire::get();
            $leads = CrmLead::get();
            $for_crm_sr_qualification_question = [];
            
            foreach ($leads as $i => $lead) {
                $lead_id = $lead->id;
                $sr_number = $lead->sr_no;

                foreach ($questionares as $i => $questionare) {
                    $for_crm_sr_qualification_question = CrmSrQualificationQuestion::updateOrCreate(
                        [
                            'lead_id' => $lead_id,
                            'sr_no' => $sr_number,
                            'question_id' =>  $questionare->id,
                        ],
                        [
                            'lead_id' => $lead_id,
                            'sr_no' => $sr_number,
                            'question_id' =>  $questionare->id,
                        ]
                    );
                }
            }

            DB::commit();

            return $this->response(200, 'List', compact('for_crm_sr_qualification_question'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $lead = CrmLead::findOrFail($id);

            if (!$lead) {
                return $this->response(404, 'Lead', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Lead', $lead);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateOrCreate($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            foreach ($request['qualification_questionnaires'] as $i => $qualification) {
                // if (!empty($qualification['response'])) {
                $qualifications = CrmSrQualificationQuestion::updateOrCreate([
                    'lead_id' => $qualification['lead_id'],
                    'question_id' => $qualification['question_id'],
                ], [
                    'lead_id' => $qualification['lead_id'],
                    'sr_no' => $qualification['sr_number'],
                    'question_id' => $qualification['question_id'],
                    'response' => $qualification['response'],
                    'remarks' => $qualification['remarks'],
                ]);
                // }
            }

            $qualifications->lead->update([
                'qualification_score' => $request['qualification_score'],
            ]);

            DB::commit();
            return $this->response(200, 'Lead has been successfully saved!', $qualifications);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function convert($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $lead_converted_to_opportunity = $response['result'];

            $convert_opportunity = CrmOpportunity::create([
                'sr_no' => $request['sr_no'],
                'account_id' => $request['account_id'],
                'opportunity_name' => $request['opportunity_name'],
                'industry_id' =>  $request['industry_id'],
                'sales_stage_id' => 1,
                'opportunity_status_id' => 1,
                'contact_owner_id' => $lead_converted_to_opportunity->contact_owner_id,
                'deal_size' => $request['deal_size'],
                'actual_amount' => $request['actual_amount'],
                'booking_reference' => $lead_converted_to_opportunity->serviceRequest->sub_category_reference,
                'lead_conversion_date' => date('Y-m-d'),
                'created_by' => Auth::user()->id,
            ]);

            $lead_converted_to_opportunity->update([
                'lead_status_id' => 3,
            ]);

            DB::commit();

            return $this->response(200, 'Lead has been converted to opportunity!', compact('convert_opportunity', 'lead_converted_to_opportunity'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function retireValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'retirement_reason' => 'required',
                'remarks' => 'sometimes',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function retire($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $lead = $response['result'];

            $response = $this->retireValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            // dd($lead, $validated);

            $lead->update([
                'lead_status_id' => 4,
                'retirement_reason_id' => $validated['retirement_reason'],
                'retirement_reason_remarks' => $validated['remarks'],
            ]);

            DB::commit();

            return $this->response(200, 'Lead has been successfully retired!', $lead);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
