<?php

namespace App\Repositories\Crm\Sales\BookingDropdownList\ActivityType;

use App\Interfaces\Crm\Sales\BookingDropdownList\ActivityType\ActivityTypeInterface;
use App\Models\CrmActivityType;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\InitializeFirestoreTrait;

class ActivityTypeRepository implements ActivityTypeInterface
{
    use ResponseTrait;
    use InitializeFirestoreTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $act_s = CrmActivityType::with('PickupReference', 'DeliveryReference')->when($request['stats'], function ($query) use ($request) {
                    if ($request['stats'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['stats']);
                    }
                })->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'List', compact('act_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'activity_type' => 'required',
                'pickup_execution_time_id' => 'required',
                'delivery_execution_time_id' => 'required',
            ],[
                'activity_type.required' => 'Activity Type is Required',
                'pickup_execution_time_id.required' => 'Pick Up Execution Time is Required',
                'delivery_execution_time_id.required' => 'Delivery Execution Time is Required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $act = CrmActivityType::create([
                'activity_type' => $validated['activity_type'],
                'pickup_execution_time_id' => $validated['pickup_execution_time_id'],
                'delivery_execution_time_id' => $validated['delivery_execution_time_id'],
                'status' => 1,
            ]);

            DB::commit();
            $actId = $act->id;

            $collectionReference = $this->initializeFirestore()->collection('crm_activity_type_management'); 
            $documentReference = $collectionReference->add();
            $documentReference->set([
                'id' => $actId,
                'activity_type' => $validated['activity_type'],
                'pickup_execution_time_id' => intval($validated['pickup_execution_time_id']),
                'delivery_execution_time_id' => intval($validated['delivery_execution_time_id']),
                'status' => 1,
            ]);

            return $this->response(200, 'Activity Type has been successfully added!', $act);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $act_s = CrmActivityType::findOrFail($id);
            if (!$act_s) {
                return $this->response(404, 'Activity Type', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Activity Type', $act_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'activity_type' => 'required',
                'pickup_execution_time_id' => 'required',
                'delivery_execution_time_id' => 'required',
            ],[
                'activity_type.required' => 'Activity Type is Required',
                'pickup_execution_time_id.required' => 'Pick Up Execution Time is Required',
                'delivery_execution_time_id.required' => 'Delivery Execution Time is Required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $act_s = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $act_s->update([
                'activity_type' => $validated['activity_type'],
                'pickup_execution_time_id' => $validated['pickup_execution_time_id'],
                'delivery_execution_time_id' => $validated['delivery_execution_time_id'],
            ]);

            $collectionReference = $this->initializeFirestore()->collection('crm_activity_type_management'); 
    
            $query = $collectionReference->where('id', '=', intval($id));
            $documents = $query->documents();
        
            foreach ($documents as $document) {
                if ($document->exists()) {
                    $documentId = $document->id();
                    $collectionReference->document($documentId)->update([
                        ['path' => 'activity_type', 'value' => $validated['activity_type']],
                        ['path' => 'pickup_execution_time_id', 'value' => intval($validated['pickup_execution_time_id'])],
                        ['path' => 'delivery_execution_time_id', 'value' => intval($validated['delivery_execution_time_id'])],
                    ]);
                }
            }

            DB::commit();

            return $this->response(200, 'Activity Type has been successfully updated!', $act_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // public function destroy($id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $acttype = CrmActivityType::find($id);
    //         if (!$acttype) return $this->response(404, 'Acitvity Type', 'Not Found!');
    //         $acttype->delete();

    //         DB::commit();
    //         return $this->response(200, 'Acitvity Type successfully deactivated!', $acttype);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }
}
