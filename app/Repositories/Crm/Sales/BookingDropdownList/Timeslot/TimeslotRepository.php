<?php

namespace App\Repositories\Crm\Sales\BookingDropdownList\Timeslot;

use App\Interfaces\Crm\Sales\BookingDropdownList\Timeslot\TimeslotInterface;
use App\Models\CrmTimeslot;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\InitializeFirestoreTrait;


class TimeslotRepository implements TimeslotInterface
{
    use ResponseTrait;
    use InitializeFirestoreTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $time_s = CrmTimeslot::paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'List', compact('time_s'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'name' => 'required',
            ],[
                'name.required' => 'Timeslot is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $time = CrmTimeslot::create([
                'name' => $validated['name'],
            ]);

            DB::commit();

            $timeId = $time->id;

            $collectionReference = $this->initializeFirestore()->collection('crm_timeslot'); 
            $documentReference = $collectionReference->add();
            $documentReference->set([
                'id' => $timeId,
                'name' => $validated['name'],
            ]);

            return $this->response(200, 'Timeslot has been successfully added!', $time);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $time_s = CrmTimeslot::findOrFail($id);
            if (!$time_s) {
                return $this->response(404, 'Timeslot', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Timeslot', $time_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'name' => 'required',
            ],[
                'name.required' => 'Timeslot is required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $time_s = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $time_s->update([
                'name' => $validated['name'],
            ]);

            $collectionReference = $this->initializeFirestore()->collection('crm_timeslot'); 
    
            $query = $collectionReference->where('id', '=', intval($id));
            $documents = $query->documents();
        
            foreach ($documents as $document) {
                if ($document->exists()) {
                    $documentId = $document->id();
                    $collectionReference->document($documentId)->update([
                        ['path' => 'name', 'value' => $validated['name']]
                    ]);
                }
            }

            DB::commit();

            return $this->response(200, 'Timeslot has been successfully updated!', $time_s);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $timeslot = CrmTimeslot::find($id);
            if (!$timeslot) return $this->response(404, 'Timeslot', 'Not Found!');
            $timeslot->delete();

            $collectionReference = $this->initializeFirestore()->collection('crm_timeslot'); 
            $query = $collectionReference->where('id', '=', intval($id));
            $documents = $query->documents();
    
            foreach ($documents as $document) {
                if ($document->exists()) {
                    $documentId = $document->id();
                    $collectionReference->document($documentId)->delete();
                }
            }

            DB::commit();
            return $this->response(200, 'Timeslot Successfully deactivated!', $timeslot);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
