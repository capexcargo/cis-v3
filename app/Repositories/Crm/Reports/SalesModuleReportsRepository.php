<?php

namespace App\Repositories\Crm\Reports;

use App\Interfaces\Crm\Reports\SalesModuleReportsInterface;
use App\Models\ChannelSrSource;
use App\Models\Crm\CrmIndustry;
use App\Models\Crm\CrmLead;
use App\Models\Crm\CrmOpportunity;
use App\Models\ServiceRequirements;
use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class SalesModuleReportsRepository implements SalesModuleReportsInterface
{
    use ResponseTrait;

    public function index($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {

            //LEADS
            $response = $this->getLeads($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $leads = $response['result']['leads'];

            //INDUSTRIES - LEADS
            $response = $this->getLeadsIndustry($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $industries = $response['result']['industries'];

            //SERVICE REQUIREMENT - LEADS
            $response = $this->getLeadsServiceRequirement($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $service_requirements = $response['result']['service_requirements'];

            //CHANNEL SOURCE - LEADS
            $response = $this->getLeadsChannelSrSource($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $channel_sources = $response['result']['channel_sources'];
            $channel_source_rankings = $response['result']['channel_source_rankings'];

            //OPPORTUNITIES
            $response = $this->getOpportunities($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $opportunities = $response['result']['opportunities'];


            DB::commit();

            return $this->response(200, 'Sales Module Reports', compact(
                'leads',
                'industries',
                'service_requirements',
                'channel_sources',
                'channel_source_rankings',
                'opportunities',
            ));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getLeads($request)
    {
        DB::beginTransaction();
        try {

            if ($request['date_range_lead'] == 1) {
                $request['date_range_search'] = $request['date_lead'];
            } elseif ($request['date_range_lead'] == 2) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date_lead'] . ' + 7 day'));
            } elseif ($request['date_range_lead'] == 3) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date_lead'] . ' + 30 day'));
            } else {
                $request['date_range_search'] = $request['date_lead'];
            }
            // dd($request['date_range_search']);
            $leads = User::with('leads')
                ->withCount(
                    [
                        'leads as no_of_leads'  => function ($query) {
                            $query->whereHas('account');
                        },
                        'leads as qualified_new_count'  => function ($query) {
                            $query->where('lead_status_id', 1);
                            $query->whereHas('account', function ($query) {
                                $query->where('customer_type', 1);
                            });
                        },
                        'leads as qualified_existing_count'  => function ($query) {
                            $query->where('lead_status_id', 1);
                            $query->whereHas('account', function ($query) {
                                $query->where('customer_type', 2);
                            });
                        },
                        'leads as unqualified_new_count'  => function ($query) {
                            $query->where('lead_status_id', 2);
                            $query->whereHas('account', function ($query) {
                                $query->where('customer_type', 1);
                            });
                        },
                        'leads as unqualified_existing_count'  => function ($query) {
                            $query->where('lead_status_id', 2);
                            $query->whereHas('account', function ($query) {
                                $query->where('customer_type', 2);
                            });
                        },
                        'leads as converted_new_count'  => function ($query) {
                            $query->where('lead_status_id', 3);
                            $query->whereHas('account', function ($query) {
                                $query->where('customer_type', 1);
                            });
                        },
                        'leads as converted_existing_count'  => function ($query) {
                            $query->where('lead_status_id', 3);
                            $query->whereHas('account', function ($query) {
                                $query->where('customer_type', 2);
                            });
                        },
                        'leads as retired_new_count'  => function ($query) {
                            $query->where('lead_status_id', 4);
                            $query->whereHas('account', function ($query) {
                                $query->where('customer_type', 1);
                            });
                        },
                        'leads as retired_existing_count'  => function ($query) {
                            $query->where('lead_status_id', 4);
                            $query->whereHas('account', function ($query) {
                                $query->where('customer_type', 2);
                            });
                        },
                    ]
                )
                ->withSum(['leads as qualified_new_qual_score' => function ($query) {
                    $query->where('lead_status_id', 1);
                    $query->whereHas('account', function ($query) {
                        $query->where('customer_type', 1);
                    });
                }], 'qualification_score')
                ->withSum(['leads as qualified_existing_qual_score' => function ($query) {
                    $query->where('lead_status_id', 1);
                    $query->whereHas('account', function ($query) {
                        $query->where('customer_type', 2);
                    });
                }], 'qualification_score')

                ->withSum(['leads as unqualified_new_qual_score' => function ($query) {
                    $query->where('lead_status_id', 2);
                    $query->whereHas('account', function ($query) {
                        $query->where('customer_type', 1);
                    });
                }], 'qualification_score')
                ->withSum(['leads as unqualified_existing_qual_score' => function ($query) {
                    $query->where('lead_status_id', 2);
                    $query->whereHas('account', function ($query) {
                        $query->where('customer_type', 2);
                    });
                }], 'qualification_score')

                ->withSum(['leads as converted_new_qual_score' => function ($query) {
                    $query->where('lead_status_id', 3);
                    $query->whereHas('account', function ($query) {
                        $query->where('customer_type', 1);
                    });
                }], 'qualification_score')
                ->withSum(['leads as converted_existing_qual_score' => function ($query) {
                    $query->where('lead_status_id', 3);
                    $query->whereHas('account', function ($query) {
                        $query->where('customer_type', 2);
                    });
                }], 'qualification_score')

                ->withSum(['leads as retired_new_qual_score' => function ($query) {
                    $query->where('lead_status_id', 4);
                    $query->whereHas('account', function ($query) {
                        $query->where('customer_type', 1);
                    });
                }], 'qualification_score')
                ->withSum(['leads as retired_existing_qual_score' => function ($query) {
                    $query->where('lead_status_id', 4);
                    $query->whereHas('account', function ($query) {
                        $query->where('customer_type', 2);
                    });
                }], 'qualification_score')

                ->whereHas('leads', function ($query) use ($request) {
                    $query->when($request['date_lead'], function ($query)  use ($request) {
                        $query->whereDate('created_at', '>=', $request['date_lead']);
                    });
                    $query->when($request['date_range_search'], function ($query)  use ($request) {
                        $query->whereDate('created_at', '<=', $request['date_range_search']);
                    });
                })
                ->whereHas('leads')
                ->get();

            DB::commit();

            return $this->response(200, 'Leads', compact('leads'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getLeadsIndustry($request)
    {
        DB::beginTransaction();
        try {
            $industries = CrmIndustry::with('customers')
                ->withCount(
                    [
                        'customers as food_industry' => function ($query) {
                            $query->where('industry_id', 1);
                            $query->whereHas('lead');
                        }, 'customers as food_industry_last_month' => function ($query) {
                            $query->where('industry_id', 1);
                            $query->whereDate('created_at', '<=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as food_industry_new' => function ($query) {
                            $query->where('industry_id', 1);
                            $query->whereDate('created_at', '>=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as agriculture' => function ($query) {
                            $query->where('industry_id', 2);
                            $query->whereHas('lead');
                        }, 'customers as agriculture_last_month' => function ($query) {
                            $query->where('industry_id', 2);
                            $query->whereDate('created_at', '<=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as agriculture_new' => function ($query) {
                            $query->where('industry_id', 2);
                            $query->whereDate('created_at', '>=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as construction' => function ($query) {
                            $query->where('industry_id', 3);
                            $query->whereHas('lead');
                        }, 'customers as construction_last_month' => function ($query) {
                            $query->where('industry_id', 3);
                            $query->whereDate('created_at', '<=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as construction_new' => function ($query) {
                            $query->where('industry_id', 3);
                            $query->whereDate('created_at', '>=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as automotive' => function ($query) {
                            $query->where('industry_id', 4);
                            $query->whereHas('lead');
                        }, 'customers as automotive_last_month' => function ($query) {
                            $query->where('industry_id', 4);
                            $query->whereDate('created_at', '<=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as automotive_new' => function ($query) {
                            $query->where('industry_id', 4);
                            $query->whereDate('created_at', '>=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as manufacturing' => function ($query) {
                            $query->where('industry_id', 5);
                            $query->whereHas('lead');
                        }, 'customers as manufacturing_last_month' => function ($query) {
                            $query->where('industry_id', 5);
                            $query->whereDate('created_at', '<=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as manufacturing_new' => function ($query) {
                            $query->where('industry_id', 5);
                            $query->whereDate('created_at', '>=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as financial_services' => function ($query) {
                            $query->where('industry_id', 6);
                            $query->whereHas('lead');
                        }, 'customers as financial_services_last_month' => function ($query) {
                            $query->where('industry_id', 6);
                            $query->whereDate('created_at', '<=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as financial_services_new' => function ($query) {
                            $query->where('industry_id', 6);
                            $query->whereDate('created_at', '>=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as transport' => function ($query) {
                            $query->where('industry_id', 7);
                            $query->whereHas('lead');
                        }, 'customers as transport_last_month' => function ($query) {
                            $query->where('industry_id', 7);
                            $query->whereDate('created_at', '<=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as transport_new' => function ($query) {
                            $query->where('industry_id', 7);
                            $query->whereDate('created_at', '>=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as logistics' => function ($query) {
                            $query->where('industry_id', 8);
                            $query->whereHas('lead');
                        }, 'customers as logistics_last_month' => function ($query) {
                            $query->where('industry_id', 8);
                            $query->whereDate('created_at', '<=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as logistics_new' => function ($query) {
                            $query->where('industry_id', 8);
                            $query->whereDate('created_at', '>=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as commerces' => function ($query) {
                            $query->where('industry_id', 9);
                            $query->whereHas('lead');
                        }, 'customers as commerces_last_month' => function ($query) {
                            $query->where('industry_id', 9);
                            $query->whereDate('created_at', '<=', now()->subDays(30));
                            $query->whereHas('lead');
                        }, 'customers as commerces_new' => function ($query) {
                            $query->where('industry_id', 9);
                            $query->whereDate('created_at', '>=', now()->subDays(30));
                            $query->whereHas('lead');
                        }
                    ]
                )
                ->get();

            DB::commit();

            return $this->response(200, 'Industries', compact('industries'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getLeadsServiceRequirement($request)
    {
        DB::beginTransaction();
        try {
            $service_requirements = ServiceRequirements::with('leads')
                ->withCount(
                    [
                        'leads as domestic_freight' => function ($query) {
                            $query->where('service_requirement_id', 1);
                        },
                        'leads as warehousing' => function ($query) {
                            $query->where('service_requirement_id', 2);
                        },
                        'leads as trucking' => function ($query) {
                            $query->where('service_requirement_id', 3);
                        },
                        'leads as packing_and_crating' => function ($query) {
                            $query->where('service_requirement_id', 4);
                        },
                        'leads as gsa' => function ($query) {
                            $query->where('service_requirement_id', 5);
                        },
                        'leads as international_freight' => function ($query) {
                            $query->where('service_requirement_id', 6);
                        },
                        'leads as nvocc' => function ($query) {
                            $query->where('service_requirement_id', 7);
                        },
                    ]
                )
                ->get();
            DB::commit();

            return $this->response(200, 'Service Requirement', compact('service_requirements'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getLeadsChannelSrSource($request)
    {
        DB::beginTransaction();
        try {
            $channel_sources = ChannelSrSource::with('leads')
                ->withCount(['leads'])
                ->withCount([
                    'leads as qualified_count'  => function ($query) {
                        $query->where('lead_status_id', 1);
                    }, 'leads as unqualified_count'  => function ($query) {
                        $query->where('lead_status_id', 2);
                    }, 'leads as converted_count'  => function ($query) {
                        $query->where('lead_status_id', 3);
                    }, 'leads as retired_count'  => function ($query) {
                        $query->where('lead_status_id', 4);
                    }
                ])
                ->whereHas('leads')
                ->get();


            $channel_source_rankings = ChannelSrSource::with('leads')
                ->withCount([
                    'leads as converted_to_opps_count'  => function ($query) {
                        $query->where('lead_status_id', 3);
                    }
                ])
                ->orderBy('converted_to_opps_count', 'desc')
                ->whereHas('leads')
                ->paginate(10);

            DB::commit();

            return $this->response(200, 'Channel Sources', compact('channel_sources', 'channel_source_rankings'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getOpportunities($request)
    {
        DB::beginTransaction();
        try {
            if ($request['date_range_opportunity'] == 1) {
                $request['date_range_search'] = $request['date_opportunity'];
            } elseif ($request['date_range_opportunity'] == 2) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date_opportunity'] . ' + 7 day'));
            } elseif ($request['date_range_opportunity'] == 3) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date_opportunity'] . ' + 30 day'));
            } else {
                $request['date_range_search'] = $request['date_opportunity'];
            }

            $opportunities = User::with('opportunities')
                ->withCount(
                    [
                        'opportunities as no_of_opportunities'  => function ($query) {
                            $query->whereHas('account');
                        },
                    ]
                )
                ->withSum(['opportunities as total_opportunity_amount'], 'deal_size')

                ->withCount(
                    [
                        'opportunities as opportunities_initiation'  => function ($query) {
                            $query->where('sales_stage_id', 1);
                        },
                    ]
                )
                ->withSum(['opportunities as deal_size_initiation' => function ($query) {
                    $query->where('sales_stage_id', 1);
                }], 'deal_size')

                ->withCount(
                    [
                        'opportunities as opportunities_quotation'  => function ($query) {
                            $query->where('sales_stage_id', 2);
                        },
                    ]
                )
                ->withSum(['opportunities as deal_size_quotation' => function ($query) {
                    $query->where('sales_stage_id', 2);
                }], 'deal_size')

                ->withCount(
                    [
                        'opportunities as opportunities_negotation'  => function ($query) {
                            $query->where('sales_stage_id', 3);
                        },
                    ]
                )
                ->withSum(['opportunities as deal_size_negotation' => function ($query) {
                    $query->where('sales_stage_id', 3);
                }], 'deal_size')

                ->withCount(
                    [
                        'opportunities as opportunities_loss'  => function ($query) {
                            $query->where('sales_stage_id', 4);
                        },
                    ]
                )
                ->withSum(['opportunities as deal_size_loss' => function ($query) {
                    $query->where('sales_stage_id', 4);
                }], 'deal_size')

                ->withCount(
                    [
                        'opportunities as opportunities_won'  => function ($query) {
                            $query->where('sales_stage_id', 5);
                        },
                    ]
                )
                ->withSum(['opportunities as deal_size_won' => function ($query) {
                    $query->where('sales_stage_id', 5);
                }], 'deal_size')

                ->whereHas('opportunities', function ($query) use ($request) {
                    $query->when($request['date_opportunity'], function ($query)  use ($request) {
                        $query->whereDate('lead_conversion_date', '>=', $request['date_opportunity']);
                    });
                    $query->when($request['date_range_search'], function ($query)  use ($request) {
                        $query->whereDate('lead_conversion_date', '<=', $request['date_range_search']);
                    });
                })
                ->whereHas('opportunities')
                ->get();

            DB::commit();

            return $this->response(200, 'Opportunities', compact('opportunities'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
