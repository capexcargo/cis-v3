<?php

namespace App\Repositories\Crm\Reports;

use App\Interfaces\Crm\Reports\CdmReportsInterface;
use App\Models\Crm\CrmAccountTypeReference;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmCustomerStatusReferences;
use App\Models\Crm\CrmIndustry;
use App\Models\Crm\CrmLifeStage;
use App\Models\MarketingChannel;
use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class CdmReportsRepository implements CdmReportsInterface
{
    use ResponseTrait;

    public function index($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            //CUSTOMER TYPES
            $response = $this->getCustomerTypeCount($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $customer_types = $response['result']['customer_types'];

            //ACCOUNT STATUS 
            $response = $this->getAccountStatusCount($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $account_status = $response['result']['account_status'];

            //CUSTOMER CATEGORIES 
            $response = $this->getCustomerCategoryCount($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $customer_categories = $response['result']['customer_categories'];
            //LIFE STAGES
            $response = $this->getLifeStageCount($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $life_stages = $response['result']['life_stages'];

            //MARKETING CHANNELS
            $response = $this->getMarketingChannelCount($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $marketing_channels = $response['result']['marketing_channels'];

            //INDUSTRIES
            $response = $this->getIndustryCount($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $industries = $response['result']['industries'];

            //CONTACT OWNERS
            $response = $this->getContactOwners($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $contact_owners = $response['result']['contact_owners'];

            //CONTACTS CREATED TODAY
            $response = $this->getContactsCreationReport($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $contacts_creation_report = $response['result']['contacts_creation_report'];

            DB::commit();

            return $this->response(200, 'CDM Reports', compact(
                'customer_types',
                'account_status',
                'customer_categories',
                'marketing_channels',
                'life_stages',
                'industries',
                'contact_owners',
                'contacts_creation_report'
            ));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getCustomerTypeCount($request)
    {
        DB::beginTransaction();
        try {
            if ($request['date_range'] == 1) {
                $request['date_range_search'] = $request['date'];
            } elseif ($request['date_range'] == 2) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date'] . ' + 7 day'));
            } elseif ($request['date_range'] == 3) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date'] . ' + 30 day'));
            } else {
                $request['date_range_search'] = $request['date'];
            }

            $customer_types = CrmAccountTypeReference::with('customers')
                ->withCount(['customers as individual' => function ($query) {
                    $query->where('account_type', 1);
                }, 'customers as corporate' => function ($query) {
                    $query->where('account_type', 2);
                }])
                ->when($request['date'], function ($query)  use ($request) {
                    $query->whereHas('customers', function ($query) use ($request) {
                        // $query->whereHas('lead', function ($query) use ($request) {
                        $query->whereDate('created_at', '>=', $request['date']);
                        // });
                    });
                })
                ->when($request['date_range_search'], function ($query)  use ($request) {
                    $query->whereHas('customers', function ($query) use ($request) {
                        // $query->whereHas('lead', function ($query) use ($request) {
                        $query->whereDate('created_at', '<=', $request['date_range_search']);
                        // });
                    });
                })
                ->get();

            DB::commit();

            return $this->response(200, 'Customer Types', compact('customer_types'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getAccountStatusCount($request)
    {
        DB::beginTransaction();
        try {
            if ($request['date_range'] == 1) {
                $request['date_range_search'] = $request['date'];
            } elseif ($request['date_range'] == 2) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date'] . ' + 7 day'));
            } elseif ($request['date_range'] == 3) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date'] . ' + 30 day'));
            } else {
                $request['date_range_search'] = $request['date'];
            }

            $account_status = CrmCustomerStatusReferences::with('customers')
                ->withCount(['customers as active' => function ($query) {
                    $query->where('status', 1);
                }, 'customers as inactive' => function ($query) {
                    $query->where('status', 2);
                }])
                ->when($request['date'], function ($query)  use ($request) {
                    $query->whereHas('customers', function ($query) use ($request) {
                        // $query->whereHas('lead', function ($query) use ($request) {
                        $query->whereDate('created_at', '>=', $request['date']);
                        // });
                    });
                })
                ->when($request['date_range_search'], function ($query)  use ($request) {
                    $query->whereHas('customers', function ($query) use ($request) {
                        // $query->whereHas('lead', function ($query) use ($request) {
                        $query->whereDate('created_at', '<=', $request['date_range_search']);
                        // });
                    });
                })
                ->get();

            DB::commit();

            return $this->response(200, 'Account Status', compact('account_status'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getCustomerCategoryCount($request)
    {
        DB::beginTransaction();
        try {
            if ($request['date_range'] == 1) {
                $request['date_range_search'] = $request['date'];
            } elseif ($request['date_range'] == 2) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date'] . ' + 7 day'));
            } elseif ($request['date_range'] == 3) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date'] . ' + 30 day'));
            } else {
                $request['date_range_search'] = $request['date'];
            }

            $customer_categories = CrmCustomerStatusReferences::with('customers')
                ->withCount(['customers as active' => function ($query) {
                    $query->where('status', 1);
                }, 'customers as inactive' => function ($query) {
                    $query->where('status', 2);
                }, 'customers as prospect' => function ($query) {
                    $query->where('status', 3);
                }])
                ->when($request['date'], function ($query)  use ($request) {
                    $query->whereHas('customers', function ($query) use ($request) {
                        // $query->whereHas('lead', function ($query) use ($request) {
                        $query->whereDate('created_at', '>=', $request['date']);
                        // });
                    });
                })
                ->when($request['date_range_search'], function ($query)  use ($request) {
                    $query->whereHas('customers', function ($query) use ($request) {
                        // $query->whereHas('lead', function ($query) use ($request) {
                        $query->whereDate('created_at', '<=', $request['date_range_search']);
                        // });
                    });
                })
                ->get();

            DB::commit();

            return $this->response(200, 'Customer Categories', compact('customer_categories'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getLifeStageCount($request)
    {
        DB::beginTransaction();
        try {
            if ($request['date_range'] == 1) {
                $request['date_range_search'] = $request['date'];
            } elseif ($request['date_range'] == 2) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date'] . ' + 7 day'));
            } elseif ($request['date_range'] == 3) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date'] . ' + 30 day'));
            } else {
                $request['date_range_search'] = $request['date'];
            }

            $life_stages = CrmLifeStage::with('customers')
                ->withCount(['customers as lead' => function ($query) {
                    $query->where('life_stage_id', 1);
                }, 'customers as customer' => function ($query) {
                    $query->where('life_stage_id', 2);
                }])
                ->when($request['date'], function ($query)  use ($request) {
                    $query->whereHas('customers', function ($query) use ($request) {
                        // $query->whereHas('lead', function ($query) use ($request) {
                        $query->whereDate('created_at', '>=', $request['date']);
                        // });
                    });
                })
                ->when($request['date_range_search'], function ($query)  use ($request) {
                    $query->whereHas('customers', function ($query) use ($request) {
                        // $query->whereHas('lead', function ($query) use ($request) {
                        $query->whereDate('created_at', '<=', $request['date_range_search']);
                        // });
                    });
                })
                ->get();

            DB::commit();

            return $this->response(200, 'Life Stages', compact('life_stages'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getMarketingChannelCount($request)
    {
        DB::beginTransaction();
        try {
            if ($request['date_range'] == 1) {
                $request['date_range_search'] = $request['date'];
            } elseif ($request['date_range'] == 2) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date'] . ' + 7 day'));
            } elseif ($request['date_range'] == 3) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date'] . ' + 30 day'));
            } else {
                $request['date_range_search'] = $request['date'];
            }

            $marketing_channels = MarketingChannel::with('customers')
                ->withCount(
                    ['customers as social_media' => function ($query) {
                        $query->where('marketing_channel_id', 1);
                    }, 'customers as google_search' => function ($query) {
                        $query->where('marketing_channel_id', 2);
                    }, 'customers as digital_ads' => function ($query) {
                        $query->where('marketing_channel_id', 3);
                    }, 'customers as email_marketing' => function ($query) {
                        $query->where('marketing_channel_id', 4);
                    }, 'customers as catalog_flyers' => function ($query) {
                        $query->where('marketing_channel_id', 5);
                    }, 'customers as outdoor_ads' => function ($query) {
                        $query->where('marketing_channel_id', 6);
                    }, 'customers as capex_truck' => function ($query) {
                        $query->where('marketing_channel_id', 7);
                    }, 'customers as events' => function ($query) {
                        $query->where('marketing_channel_id', 8);
                    }, 'customers as direct_selling' => function ($query) {
                        $query->where('marketing_channel_id', 9);
                    }]
                )
                ->whereHas('customers', function ($query) use ($request) {
                    $query->when($request['date'], function ($query)  use ($request) {
                        $query->whereDate('created_at', '>=', $request['date']);
                    });
                    $query->when($request['date_range_search'], function ($query)  use ($request) {
                        $query->whereDate('created_at', '<=', $request['date_range_search']);
                    });
                })
                ->get();

            DB::commit();

            return $this->response(200, 'Marketing Channels', compact('marketing_channels'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getIndustryCount($request)
    {
        DB::beginTransaction();
        try {
            if ($request['date_range'] == 1) {
                $request['date_range_search'] = $request['date'];
            } elseif ($request['date_range'] == 2) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date'] . ' + 7 day'));
            } elseif ($request['date_range'] == 3) {
                $request['date_range_search'] = date('Y-m-d', strtotime($request['date'] . ' + 30 day'));
            } else {
                $request['date_range_search'] = $request['date'];
            }

            $industries = CrmIndustry::with('customers')

                ->withCount(
                    [
                        'customers as food_industry' => function ($query) {
                            $query->where('industry_id', 1);
                        }, 'customers as food_industry_last_month' => function ($query) {
                            $query->where('industry_id', 1)
                                ->whereDate('created_at', '<=', now()->subDays(30));
                        }, 'customers as food_industry_new' => function ($query) {
                            $query->where('industry_id', 1)
                                ->whereDate('created_at', '>=', now()->subDays(30));
                        }, 'customers as agriculture' => function ($query) {
                            $query->where('industry_id', 2);
                        }, 'customers as agriculture_last_month' => function ($query) {
                            $query->where('industry_id', 2)
                                ->whereDate('created_at', '<=', now()->subDays(30));
                        }, 'customers as agriculture_new' => function ($query) {
                            $query->where('industry_id', 2)
                                ->whereDate('created_at', '>=', now()->subDays(30));
                        },
                        'customers as construction' => function ($query) {
                            $query->where('industry_id', 3);
                        }, 'customers as construction_last_month' => function ($query) {
                            $query->where('industry_id', 3)
                                ->whereDate('created_at', '<=', now()->subDays(30));
                        }, 'customers as construction_new' => function ($query) {
                            $query->where('industry_id', 3)
                                ->whereDate('created_at', '>=', now()->subDays(30));
                        }, 'customers as automotive' => function ($query) {
                            $query->where('industry_id', 4);
                        }, 'customers as automotive_last_month' => function ($query) {
                            $query->where('industry_id', 4)
                                ->whereDate('created_at', '<=', now()->subDays(30));
                        }, 'customers as automotive_new' => function ($query) {
                            $query->where('industry_id', 4)
                                ->whereDate('created_at', '>=', now()->subDays(30));
                        }, 'customers as manufacturing' => function ($query) {
                            $query->where('industry_id', 5);
                        }, 'customers as manufacturing_last_month' => function ($query) {
                            $query->where('industry_id', 5)
                                ->whereDate('created_at', '<=', now()->subDays(30));
                        }, 'customers as manufacturing_new' => function ($query) {
                            $query->where('industry_id', 5)
                                ->whereDate('created_at', '>=', now()->subDays(30));
                        }, 'customers as financial_services' => function ($query) {
                            $query->where('industry_id', 6);
                        }, 'customers as financial_services_last_month' => function ($query) {
                            $query->where('industry_id', 6)
                                ->whereDate('created_at', '<=', now()->subDays(30));
                        }, 'customers as financial_services_new' => function ($query) {
                            $query->where('industry_id', 6)
                                ->whereDate('created_at', '>=', now()->subDays(30));
                        }, 'customers as transport' => function ($query) {
                            $query->where('industry_id', 7);
                        }, 'customers as transport_last_month' => function ($query) {
                            $query->where('industry_id', 7)
                                ->whereDate('created_at', '<=', now()->subDays(30));
                        }, 'customers as transport_new' => function ($query) {
                            $query->where('industry_id', 7)
                                ->whereDate('created_at', '>=', now()->subDays(30));
                        }, 'customers as logistics' => function ($query) {
                            $query->where('industry_id', 8);
                        }, 'customers as logistics_last_month' => function ($query) {
                            $query->where('industry_id', 8)
                                ->whereDate('created_at', '<=', now()->subDays(30));
                        }, 'customers as logistics_new' => function ($query) {
                            $query->where('industry_id', 8)
                                ->whereDate('created_at', '>=', now()->subDays(30));
                        }, 'customers as commerces' => function ($query) {
                            $query->where('industry_id', 9);
                        }, 'customers as commerces_last_month' => function ($query) {
                            $query->where('industry_id', 9)
                                ->whereDate('created_at', '<=', now()->subDays(30));
                        }, 'customers as commerces_new' => function ($query) {
                            $query->where('industry_id', 9)
                                ->whereDate('created_at', '>=', now()->subDays(30));
                        }
                    ]
                )
                ->when($request['date'], function ($query)  use ($request) {
                    $query->whereHas('customers', function ($query) use ($request) {
                        $query->whereHas('lead', function ($query) use ($request) {
                            $query->whereDate('created_at', '>=', $request['date']);
                        });
                    });
                })
                ->when($request['date_range_search'], function ($query)  use ($request) {
                    $query->whereHas('customers', function ($query) use ($request) {
                        $query->whereHas('lead', function ($query) use ($request) {
                            $query->whereDate('created_at', '<=', $request['date_range_search']);
                        });
                    });
                })
                ->get();

            DB::commit();

            return $this->response(200, 'Industries', compact('industries'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getContactOwners($request)
    {
        DB::beginTransaction();
        try {
            $contact_owners = User::with('userDetails')->withCount(
                ['customers as count_contact_own' => function ($query) {
                    // $query->;
                }]
            )
                ->where('division_id', 2)
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Contact Owners', compact('contact_owners'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getContactsCreationReport($request)
    {
        DB::beginTransaction();
        try {
            $contacts_creation_report = CrmCustomerInformation::select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as count_contact_created'))
                ->when($request['date'], function ($query) use ($request) {
                    $query->whereDate('created_at', $request['date']);
                })
                ->groupBy('date')
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Contacts Creation Report', compact('contacts_creation_report'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
