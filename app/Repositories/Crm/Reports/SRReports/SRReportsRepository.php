<?php

namespace App\Repositories\Crm\Reports\SRReports;

use App\Interfaces\Crm\Reports\SRReports\SRReportsInterface;
use App\Models\Crm\CrmServiceRequest;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


// class SRReportsRepository implements SRReportsInterface
// {
//     use ResponseTrait;

//     public function index($request)
//     {
//         DB::beginTransaction();
//         try {
//             $service_requests = CrmServiceRequest::with('channelSource')->withCount(
//                 ['srEmailDetails as count_thread']
//             )
//                 ->withCount(
//                     ['srEmailDetails as read' => function ($query) {
//                         $query->where('is_read', 1);
//                     }]
//                 )
//                 ->paginate($request['paginate']);

//             DB::commit();

//             return $this->response(200, 'Service Request List', compact('service_requests'));
//         } catch (\Exception $e) {
//             DB::rollback();

//             return $this->response(500, 'Something Went Wrong', $e->getMessage());
//         }
//     }

   
// }
