<?php

namespace App\Repositories\Hrim\Csp;

use App\Interfaces\Hrim\Csp\CspManagementInterface;
use App\Models\Hrim\Agencies;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CspManagementRepository implements CspManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $csp_managements = Agencies::with('branch')
                ->paginate($request['paginate']);
            // ->when($request['name'] ?? false, function ($query) use ($request) {
            //     $query->where('name', $request['name']);
            // })
            // ->when($request['date'] ?? false, function ($query) use ($request) {
            //     $query->where('date', $request['date']);
            // })
            // ->when($request['branch_id'] ?? false, function ($query) use ($request) {
            //     $query->where('branch_id', $request['branch_id']);
            // })
            // ->when($request['type_id'] ?? false, function ($query) use ($request) {
            //     $query->where('type_id', $request['type_id']);
            // })

            DB::commit();
            return $this->response(200, 'CSP List', compact('csp_managements'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'name' => 'required',
                'branch_id' => 'required',
            ]);


            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'name' => 'required',
                'branch_id' => 'required',
            ]);


            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $cspManagement = Agencies::create([
                'name' => $validated['name'],
                'description' => NULL,
                'branch_id' => $validated['branch_id'],
            ]);

            DB::commit();
            return $this->response(200, 'CSP has been successfully created!', $cspManagement);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $csp_management = Agencies::findOrFail($id);
            if (!$csp_management) return $this->response(404, 'CSP', 'Not Found!');

            DB::commit();
            return $this->response(200, 'CSP', $csp_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $csp_management = $response['result'];

            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $csp_management->update([
                'name' => $validated['name'],
                'desription' => '',
                'branch_id' => $validated['branch_id'],
            ]);


            DB::commit();
            return $this->response(200, 'CSP has been successfully updated!', $csp_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateStatus($id, $status)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $csp_management = $response['result'];

            $csp_management->update([
                'is_active' => $status,
            ]);

            DB::commit();
            if ($status == 1) {
                return $this->response(200, 'CSP has been successfully activated!', $csp_management);
            } else {
                return $this->response(200, 'CSP has been successfully deactivated!', $csp_management);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $holiday = Agencies::find($id);
            if (!$holiday) return $this->response(404, 'CSP', 'Not Found!');
            $holiday->delete();

            DB::commit();
            return $this->response(200, 'CSP has been successfully deleted!', $holiday);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
