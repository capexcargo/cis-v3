<?php

namespace App\Repositories\Hrim\Csp;

use App\Interfaces\Hrim\Csp\CspUserManagementInterface;
use App\Models\Hrim\Agencies;
use App\Models\Hrim\CspUserManagement;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CspUserManagementRepository implements CspUserManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {

        DB::beginTransaction();
        try {
            // dd($request);
            $csp_user_managements = UserDetails::with([
                'agency', 'branch'
            ])
                ->when($request['agency_id'] ?? false, function ($query) use ($request) {
                    $query->where('agency_id', $request['agency_id']);
                })
                ->when($request['branch_id'] ?? false, function ($query) use ($request) {
                    $query->where('branch_id', $request['branch_id']);
                })
                ->when($request['agency_contact_person'] ?? false, function ($query) use ($request) {
                    $query->where('agency_contact_person', 'like', '%' . $request['agency_contact_person'] . '%');
                })
                ->where('agency_type', 1)
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })->paginate($request['paginate']);


            // ->when($request['date'] ?? false, function ($query) use ($request) {
            //     $query->where('date', $request['date']);
            // })
            // ->when($request['branch_id'] ?? false, function ($query) use ($request) {
            //     $query->where('branch_id', $request['branch_id']);
            // })
            // ->when($request['type_id'] ?? false, function ($query) use ($request) {
            //     $query->where('type_id', $request['type_id']);
            // })

            DB::commit();
            return $this->response(200, 'CSP User Admin List', compact('csp_user_managements'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'agency_id' => 'required',
                'branch_id' => 'required',
                'agency_contact_person' => 'required',
                'agency_contact_no' => 'required|numeric|digits:11',
                'agency_position' => 'required',
                'personal_email' => 'required|email:rfc,dns',
                'password' => 'required|confirmed|string|min:8',
                'password_confirmation' => 'required',
            ]);


            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }




    public function create($request)
    {

        DB::beginTransaction();
        try {

            $response = $this->validation($request);
            // dd($response['code']);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $user = User::create([
                'role_id' => 1,
                'division_id' => 5,
                'level_id' => 1,
                'name' => 'Agency',
                'email' => $validated['personal_email'],
                'password' => Hash::make($validated['password']),
            ]);

            $user->userDetails()->create([
                'first_name' => 'Agency',
                'agency_id' => $validated['agency_id'],
                'branch_id' => $validated['branch_id'],
                'agency_contact_person' => $validated['agency_contact_person'],
                'agency_contact_no' => $validated['agency_contact_no'],
                'agency_position' => $validated['agency_position'],
                'personal_email' => $validated['personal_email'],
                'company_email' => $validated['personal_email'],
                'agency_type' => 1,
            ]);

            DB::commit();
            return $this->response(200, 'CSP User Admin Successfully Created!', $user);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $csp_management_user = UserDetails::findOrFail($id);
            $csp_management = User::findOrFail($csp_management_user->user_id);

            if (!$csp_management) return $this->response(404, 'CSP', 'Not Found!');

            DB::commit();
            return $this->response(200, 'CSP', $csp_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($id, $request, $pass)
    {
        DB::beginTransaction();
        try {
            // dd($request);

            $user_details_ids = UserDetails::where('id', $id)->first();
            $user_ids = User::where('id', $user_details_ids->user_id)->first();

            // dd($user_ids->id);
            if ($pass) {
                $validator = Validator::make($request, [
                    'agency_id' => 'required',
                    'branch_id' => 'required',
                    'agency_contact_person' => 'required',
                    'agency_contact_no' => 'required|numeric|digits:11',
                    'agency_position' => 'required',
                    'personal_email' => 'required|email:rfc,dns|unique:users,email,' . $user_ids . ',id',
                    'password' => 'required|confirmed|string|min:8',
                    'password_confirmation' => 'required',
                ]);
            } else {
                $validator = Validator::make($request, [
                    'agency_id' => 'required',
                    'branch_id' => 'required',
                    'agency_contact_person' => 'required',
                    'agency_contact_no' => 'required|numeric|digits:11',
                    'agency_position' => 'required',
                    'personal_email' => 'required|email:rfc,dns|unique:users,email,' . $user_ids . ',id',
                ]);
            }

            dd($validator->validated());

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());
            // dd($validator);
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request, $pass)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $csp_management = $response['result'];


            // dd($validated);

            if ($pass) {
                $csp_management->update([
                    'password' => Hash::make($request['password']),
                ]);
            }

            $csp_management->update([
                // 'role_id' => 1,
                // 'division_id' => 5,
                // 'level_id' => 1,
                // 'name' => 'Agency',
                'email' => $request['personal_email'],
            ]);

            $csp_management->userDetails()->update([
                'first_name' => 'Agency',
                'agency_id' => $request['agency_id'],
                'branch_id' => $request['branch_id'],
                'agency_contact_person' => $request['agency_contact_person'],
                'agency_contact_no' => $request['agency_contact_no'],
                'agency_position' => $request['agency_position'],
                'personal_email' => $request['personal_email'],
                'company_email' => $request['personal_email'],
                'agency_type' => 1,
            ]);

            DB::commit();
            return $this->response(200, 'CSP User Admin has been successfully updated!', $csp_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateStatus($id, $status)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $csp_management = $response['result'];

            $csp_management->update([
                'status_id' => $status,
            ]);

            DB::commit();
            if ($status == 1) {
                return $this->response(200, 'CSP User Admin has been successfully activated!', $csp_management);
            } else {
                return $this->response(200, 'CSP User Admin has been successfully deactivated!', $csp_management);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $csp_management_user = UserDetails::findOrFail($id);
            $csp_management = User::findOrFail($csp_management_user->user_id);

            if (!$csp_management) return $this->response(404, 'CSP User Admin', 'Not Found!');
            $csp_management_user->delete();

            $csp_management->delete();

            DB::commit();
            return $this->response(200, 'Holiday Successfully Deleted!', $csp_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
