<?php

namespace App\Repositories\Hrim\Dashboard;

use App\Interfaces\Hrim\Dashboard\DashboardInterface;
use App\Models\BranchReference;
use App\Models\Hrim\Absent;
use App\Models\Hrim\EmployeeRequisition;
use App\Models\Hrim\EmploymentCategory;
use App\Models\Hrim\EmploymentStatus;
use App\Models\Hrim\GenderReference;
use App\Models\Hrim\SalaryGradeManagement;
use App\Models\Hrim\TimeLog;
use App\Models\LeaveDetails;
use App\Models\UserDetails;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class DashboardRepository implements DashboardInterface
{
    use ResponseTrait;

    public function countEmployeeByStatus()
    {
        DB::beginTransaction();
        try {

            $get_status = EmploymentStatus::withCount(['users as regular' => function ($query) {
                $query->where('employment_status_id', 1)
                    ->where('status_id', 1);
            }, 'users as probi' => function ($query) {
                $query->where('employment_status_id', 3)
                    ->where('status_id', 1);
            }, 'users as contractual' => function ($query) {
                $query->where('employment_status_id', 4)
                    ->where('status_id', 1);
            }])->get();
            // dd($get_status);
            DB::commit();
            return $this->response(200, 'Status Header', compact('get_status'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function countEmployeeStatusByBranch()
    {
        DB::beginTransaction();
        try {

            $get_branch_status = BranchReference::withCount(['userByBranchId as regular' => function ($query) {
                $query->where('employment_status_id', 1)
                    ->where('status_id', 1);
            }, 'userByBranchId as probi' => function ($query) {
                $query->where('employment_status_id', 3)
                    ->where('status_id', 1);
            }, 'userByBranchId as contractual' => function ($query) {
                $query->where('employment_status_id', 4)
                    ->where('status_id', 1);
            }])->get();
            // dd($get_branch_status);
            // echo "<pre>";
            // print_r($get_branch_status);
            // echo "</pre>";
            DB::commit();
            return $this->response(200, 'Status List', compact('get_branch_status'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function graphs()
    {
        DB::beginTransaction();
        try {

            $get_emp_status = EmploymentCategory::withCount(['users as direct' => function ($query) {
                $query->where('employment_category_id', 1)
                    ->where('status_id', 1);
            }, 'users as csp' => function ($query) {
                $query->where('employment_category_id', 2)
                    ->where('status_id', 1);
            }])->get();

            $get_emp_gender = GenderReference::withCount(['users as male' => function ($query) {
                $query->where('gender_id', 1)
                    ->where('status_id', 1);
            }, 'users as female' => function ($query) {
                $query->where('gender_id', 2)
                    ->where('status_id', 1);
            }])->get();

            $get_emp_age = UserDetails::select('age')->get();

            $get_salary_grade = SalaryGradeManagement::withCount([
                'salary_grade as a1' => function ($query) {
                    $query->where('salary_grade_id', 1);
                }, 'salary_grade as a2' => function ($query) {
                    $query->where('salary_grade_id', 2);
                }, 'salary_grade as a3' => function ($query) {
                    $query->where('salary_grade_id', 3);
                }, 'salary_grade as b1' => function ($query) {
                    $query->where('salary_grade_id', 4);
                }, 'salary_grade as b2' => function ($query) {
                    $query->where('salary_grade_id', 5);
                }, 'salary_grade as b3' => function ($query) {
                    $query->where('salary_grade_id', 6);
                }, 'salary_grade as c1' => function ($query) {
                    $query->where('salary_grade_id', 7);
                }, 'salary_grade as c2' => function ($query) {
                    $query->where('salary_grade_id', 8);
                }, 'salary_grade as c3' => function ($query) {
                    $query->where('salary_grade_id', 9);
                }, 'salary_grade as d1' => function ($query) {
                    $query->where('salary_grade_id', 10);
                }, 'salary_grade as d2' => function ($query) {
                    $query->where('salary_grade_id', 11);
                }
            ])->get();

            $get_emp_requisition = EmployeeRequisition::get();

            DB::commit();
            return $this->response(200, 'Graphs List', compact('get_emp_status', 'get_emp_gender', 'get_emp_age', 'get_salary_grade', 'get_emp_requisition'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function tableLists()
    {
        DB::beginTransaction();
        try {
            $get_emp_late = TimeLog::where('late', '>', '08:15:00')->get();

            $get_emp_on_leave = LeaveDetails::where('leave_date', date('Y-m-d'))->get();

            $get_emp_today_absent = Absent::where('absent_date', date('Y-m-d'))->get();

            DB::commit();
            return $this->response(200, 'Dashboard List', compact('get_emp_late', 'get_emp_on_leave', 'get_emp_today_absent'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
