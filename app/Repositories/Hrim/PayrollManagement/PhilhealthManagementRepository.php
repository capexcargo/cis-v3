<?php

namespace App\Repositories\Hrim\PayrollManagement;

use App\Interfaces\Hrim\PayrollManagement\PhilhealthManagementInterface;
use App\Models\Hrim\PhilhealthManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PhilhealthManagementRepository implements PhilhealthManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $philhealth_managements = PhilhealthManagement::get();
            DB::commit();
            return $this->response(200, 'Philhealth List', compact('philhealth_managements'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {

            $philhealthManagement = PhilhealthManagement::create([
                'year' => $request['year'],
                'premium_rate' => $request['premium_rate'],
            ]);

            DB::commit();
            return $this->response(200, 'Philhealth Successfully Created!', $philhealthManagement);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'year' => 'required',
                'premium_rate' => 'required',
            ]);


            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $philhealth_management = PhilhealthManagement::findOrFail($id);
            if (!$philhealth_management) return $this->response(404, 'Philhealth', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Leadership Competencies', $philhealth_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $philhealth_management = $response['result'];

            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $philhealth_management->update([
                'year' => $validated['year'],
                'premium_rate' => $validated['premium_rate'],
            ]);


            DB::commit();
            return $this->response(200, 'Philhealth has been successfully updated!', $philhealth_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
