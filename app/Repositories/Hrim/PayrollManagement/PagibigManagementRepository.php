<?php

namespace App\Repositories\Hrim\PayrollManagement;

use App\Interfaces\Hrim\PayrollManagement\PagibigManagementInterface;
use App\Models\Hrim\PagibigManagement;
use App\Models\Hrim\SssManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PagibigManagementRepository implements PagibigManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $pagibig_management = PagibigManagement::get();

            DB::commit();
            return $this->response(200, 'Pagibig List', compact('pagibig_management'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {

            $pagibigManagement = PagibigManagement::create([
                'year' => $request['year'],
                'share' => $request['share'],
            ]);

            DB::commit();
            return $this->response(200, 'Pagibig Share Created!', $pagibigManagement);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'year' => 'required|numeric',
                'share' => 'required|numeric',
            ]);


            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $pagibig_management = PagibigManagement::findOrFail($id);
            if (!$pagibig_management) return $this->response(404, 'Pagibig', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Leadership Competencies', $pagibig_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $pagibig_management = $response['result'];

            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $pagibig_management->update([
                'year' => $validated['year'],
                'share' => $validated['share'],
            ]);


            DB::commit();
            return $this->response(200, 'Pagibig has been successfully updated!', $pagibig_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
