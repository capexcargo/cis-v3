<?php

namespace App\Repositories\Hrim\PayrollManagement;

use App\Interfaces\Hrim\PayrollManagement\LoaAdjustmentInterface;
use App\Models\Hrim\LoaAdjustmentMgmt;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LoaAdjustmentRepository implements LoaAdjustmentInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $loa_adjustments = LoaAdjustmentMgmt::with('approver', 'category')
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);
            // dd($loa_adjustments);
            DB::commit();
            return $this->response(200, 'Loa Adjustment List', compact('loa_adjustments'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'min_amount' => 'required',
                'max_amount' => 'required',
                'category' => 'required',
                'approver' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'min_amount' => 'required',
                'max_amount' => 'required',
                'category' => 'required',
                'approver' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $loa_adjustment = LoaAdjustmentMgmt::create([
                'min_amount' => $validated['min_amount'],
                'max_amount' => $validated['max_amount'],
                'category_id' => $validated['category'],
                'approver_id' => $validated['approver'],
            ]);
            

            DB::commit();
            return $this->response(200, 'Loa Adjustment Successfully Created!', $loa_adjustment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $loa_adjustment = LoaAdjustmentMgmt::findOrFail($id);
            if (!$loa_adjustment) return $this->response(404, 'Loa Adjustment', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Loa Adjustment', $loa_adjustment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $loa_adjustment = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $loa_adjustment->update([
                'min_amount' => $validated['min_amount'],
                'max_amount' => $validated['max_amount'],
                'category_id' => $validated['category'],
                'approver_id' => $validated['approver'],
            ]);


            DB::commit();
            return $this->response(200, 'Load Adjustment has been successfully updated!', $loa_adjustment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $loa_adjustment = LoaAdjustmentMgmt::find($id);
            if (!$loa_adjustment) return $this->response(404, 'Loa Adjustment', 'Not Found!');
            $loa_adjustment->delete();

            DB::commit();
            return $this->response(200, 'Loa Adjustment Successfully Deleted!', $loa_adjustment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
