<?php

namespace App\Repositories\Hrim\PayrollManagement;

use App\Interfaces\Hrim\PayrollManagement\SssManagementInterface;
use App\Models\Hrim\SssManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class SssManagementRepository implements SssManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $sss_management = SssManagement::when($request['year'] ?? false, function ($query) use ($request) {
                $query->where('year', $request['year']);
            })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Sss Contributions List', compact('sss_management'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
