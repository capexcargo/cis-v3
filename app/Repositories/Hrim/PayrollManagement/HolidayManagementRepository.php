<?php

namespace App\Repositories\Hrim\PayrollManagement;

use App\Interfaces\Hrim\PayrollManagement\HolidayManagementInterface;
use App\Models\Hrim\HolidayManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class HolidayManagementRepository implements HolidayManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $holiday_managements = HolidayManagement::with('branch', 'holidayType')
                ->when($request['name'] ?? false, function ($query) use ($request) {
                    $query->where('name', $request['name']);
                })
                ->when($request['date'] ?? false, function ($query) use ($request) {
                    $query->where('date', $request['date']);
                })
                ->when($request['branch_id'] ?? false, function ($query) use ($request) {
                    $query->where('branch_id', $request['branch_id']);
                })
                ->when($request['type_id'] ?? false, function ($query) use ($request) {
                    $query->where('type_id', $request['type_id']);
                })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Holiday List', compact('holiday_managements'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'name' => 'required',
                'date' => 'required|unique:hrim_holiday,date,',
                'branch_id' => 'sometimes',
                'type_id' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'name' => 'required',
                'date' => 'required|unique:hrim_holiday,date,'.$id,
                'branch_id' => 'sometimes',
                'type_id' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $holidayManagement = HolidayManagement::create([
                'name' => $validated['name'],
                'date' => $validated['date'],
                'branch_id' => $validated['branch_id'],
                'type_id' => $validated['type_id'],
            ]);

            DB::commit();

            return $this->response(200, 'Holiday Successfully Created!', $holidayManagement);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $holiday_management = HolidayManagement::findOrFail($id);
            if (!$holiday_management) {
                return $this->response(404, 'Leadership Competencies', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Leadership Competencies', $holiday_management);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $holiday_management = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $holiday_management->update([
                'name' => $validated['name'],
                'date' => $validated['date'],
                'branch_id' => $validated['branch_id'],
                'type_id' => $validated['type_id'],
            ]);

            DB::commit();

            return $this->response(200, 'Holiday has been successfully updated!', $holiday_management);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $holiday = HolidayManagement::find($id);
            if (!$holiday) {
                return $this->response(404, 'Job Level', 'Not Found!');
            }
            $holiday->delete();

            DB::commit();

            return $this->response(200, 'Holiday Successfully Deleted!', $holiday);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
