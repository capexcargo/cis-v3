<?php

namespace App\Repositories\Hrim\PayrollManagement;

use App\Interfaces\Hrim\PayrollManagement\AdminAdjustmentInterface;
use App\Interfaces\Hrim\PayrollManagement\CutOffManagementInterface;
use App\Models\Hrim\CutOffManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CutOffMgmtReposirtory implements CutOffManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $cutoff_managements = CutOffManagement::when($request['sort_field'], function ($query) use ($request) {
                $query->orderBy($request['sort_field'], $request['sort_type']);
            })
                ->paginate($request['paginate']);
            DB::commit();
            return $this->response(200, 'Cut-off List', compact('cutoff_managements'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
    }

    public function updateValidation($request)
    {
    }

    public function create($request)
    {
    }

    public function show($request)
    {
    }

    public function update($request, $id)
    {
    }
}
