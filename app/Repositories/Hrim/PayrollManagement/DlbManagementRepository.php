<?php

namespace App\Repositories\Hrim\PayrollManagement;

use App\Interfaces\Hrim\PayrollManagement\DlbManagementInterface;
use App\Models\Hrim\DlbManagement;
use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DlbManagementRepository implements DlbManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $dlb_managements = User::withCount('dlbUsers')
                ->whereHas('dlbUsers', function ($query) {
                    $query->where('amount', '!=', 0);
                })
                ->whereHas('dlbUsers', function ($query) use ($request) {
                    $query->when($request['year'] ?? false, function ($query) use ($request) {
                        $query->where('year', $request['year']);
                    });
                    $query->when($request['month'] ?? false, function ($query) use ($request) {
                        $query->where('month', $request['month']);
                    });
                    $query->when($request['cut_off'] ?? false, function ($query) use ($request) {
                        $query->where('payout_cutoff', $request['cut_off']);
                    });
                })
                ->withSum('dlbUsers', 'amount')
                ->paginate($request['paginate']);
            // dd($dlb_managements);
            DB::commit();

            return $this->response(200, 'Dlb List', compact('dlb_managements'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $rules = [
                'user_id' => 'required',
            ];

            foreach ($request['dlbs'] as $i => $dlb) {
                $rules['dlbs.' . $i . '.id'] = 'sometimes';
                $rules['dlbs.' . $i . '.year'] = 'required';
                $rules['dlbs.' . $i . '.month'] = 'required';
                $rules['dlbs.' . $i . '.payout_cutoff'] = 'required';
                $rules['dlbs.' . $i . '.amount'] = 'required';
            }

            $validator = Validator::make($rules, [
                [
                    'dlbs.*.year.required' => 'The Year field is required.',
                    'dlbs.*.month.required' => 'The Month field is required.',
                    'dlbs.*.payou.requiredt_cutoff' => 'The Cutoff field is required.',
                    'dlbs.*.amount.required' => 'The Amount field is required.',
                ],
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            foreach ($request['dlbs'] as $i => $dlb) {
                $dlbManagement = DlbManagement::create([
                    'user_id' => $request['user_id'],
                    'year' => $request['dlbs'][$i]['year'],
                    'month' => $request['dlbs'][$i]['month'],
                    'payout_cutoff' => $request['dlbs'][$i]['payout_cutoff'],
                    'amount' => $request['dlbs'][$i]['amount'],
                ]);
            }

            DB::commit();

            return $this->response(200, 'DLB Successfully Created!', $dlbManagement);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $dlb_management = User::with('dlbUsers')->findOrFail($id);
            if (!$dlb_management) return $this->response(404, 'DLB Management', 'Not Found!');

            DB::commit();
            return $this->response(200, 'DLB Management', $dlb_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $dlb_management = $response['result'];

            // $response = $this->validation($request, $id);
            // if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);

            // $validated = $response['result'];

            // dd($request['dlbs']);


            foreach ($request['dlbs'] as $i => $dlb) {
                $dlb_management->dlbUsers()->updateOrCreate(
                    [
                        'id' => $request['dlbs'][$i]['id']
                    ],
                    [
                        'year' => $request['dlbs'][$i]['year'],
                        'month' => $request['dlbs'][$i]['month'],
                        'payout_cutoff' => $request['dlbs'][$i]['payout_cutoff'],
                        'amount' => $request['dlbs'][$i]['amount'],
                    ]
                );
            }

            DB::commit();
            return $this->response(200, 'DLB has been successfully updated!', $dlb_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // public function destroy($id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $holiday = HolidayManagement::find($id);
    //         if (!$holiday) return $this->response(404, 'Job Level', 'Not Found!');
    //         $holiday->delete();

    //         DB::commit();
    //         return $this->response(200, 'Holiday Successfully Deleted!', $holiday);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }
}
