<?php

namespace App\Repositories\Hrim\PayrollManagement;

use App\Interfaces\Hrim\PayrollManagement\AdminAdjustmentInterface;
use App\Models\Hrim\AdminAdjustment;
use App\Models\Hrim\LoaAdjustmentMgmt;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AdminAdjustmentRepository implements AdminAdjustmentInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $admin_adjustments = AdminAdjustment::with('user', 'categoryRef', 'approver', 'status', 'monthRef')
            ->when($request['sort_field'], function ($query) use ($request) {
                $query->orderBy($request['sort_field'], $request['sort_type']);
            })
                ->paginate($request['paginate']);
            // dd($admin_adjustments);
            DB::commit();
            return $this->response(200, 'Admin Adjustment List', compact('admin_adjustments'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'employee' => 'required',
                'category' => 'required',
                'description' => 'sometimes',
                'amount' => 'required',
                'year' => 'required',
                'month' => 'required',
                'cutoff' => 'required',
            ]);


            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'employee' => 'required',
                'category' => 'required',
                'description' => 'sometimes',
                'amount' => 'required',
                'year' => 'required',
                'month' => 'required',
                'cutoff' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $approver = LoaAdjustmentMgmt::select('min_amount', 'max_amount', 'category_id', 'approver_id')
                ->where('category_id', $validated['category'])
                ->first();

            $error_msg = "There's no approver yet for the amount";
            if ($approver) {
                if ($validated['amount'] >= $approver->min_amount && $validated['amount'] <= $approver->max_amount) {
                    $admin_adjustment = AdminAdjustment::create([
                        'user_id' => $validated['employee'],
                        'category' => $validated['category'],
                        'description' => $validated['description'],
                        'amount' => $validated['amount'],
                        'year' => $validated['year'],
                        'month' => $validated['month'],
                        'payout_cutoff' => $validated['cutoff'],
                        'first_approver' => $approver->approver_id,
                        'first_status' => 1,
                    ]);
                } else {
                    return $this->response(500, 'Something Went Wrong', $error_msg);
                }

                DB::commit();
                return $this->response(200, 'Adjustment Successfully Created!', $admin_adjustment);
            } else {
                return $this->response(500, 'Something Went Wrong', "There's no approver yet for the category selected");
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $admin_adjustment = AdminAdjustment::findOrFail($id);
            if (!$admin_adjustment) return $this->response(404, 'Admin Adjustment', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Admin Adjustment', $admin_adjustment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $admin_adjustment = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];
            $approver = LoaAdjustmentMgmt::select('min_amount', 'max_amount', 'category_id', 'approver_id')
                ->where('category_id', $validated['category'])
                ->first();

            $error_msg = "There's no approver yet for the amount";
            if ($approver) {
                if ($validated['amount'] >= $approver->min_amount && $validated['amount'] <= $approver->max_amount) {
                    $admin_adjustment->update([
                        'category' => $validated['category'],
                        'description' => $validated['description'],
                        'amount' => $validated['amount'],
                        'year' => $validated['year'],
                        'month' => $validated['month'],
                        'payout_cutoff' => $validated['cutoff'],
                    ]);
                } else {
                    return $this->response(500, 'Something Went Wrong', $error_msg);
                }

                DB::commit();
                return $this->response(200, 'Load Adjustment has been successfully updated!', $admin_adjustment);
            } else {
                return $this->response(500, 'Something Went Wrong', "There's no approver yet for the category selected");
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function approve($id)
    {
        DB::beginTransaction();
        try {
            $admin_adjustment = AdminAdjustment::find($id);

            if (!$admin_adjustment) return $this->response(404, 'Admin Adjustment', 'Not Found!');

            $admin_adjustment->update([
                'first_status' => 3,
                'first_date' => date("Y-m-d"),
            ]);

            DB::commit();
            return $this->response(200, 'Admin Adjustment has been successfully approved!', $admin_adjustment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $adjustment = AdminAdjustment::find($id);
            if (!$adjustment) return $this->response(404, 'Admin Adjustment', 'Not Found!');
            $adjustment->delete();

            DB::commit();
            return $this->response(200, 'Adjustment Successfully Deleted!', $adjustment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
