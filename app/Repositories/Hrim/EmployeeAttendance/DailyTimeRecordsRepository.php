<?php

namespace App\Repositories\Hrim\EmployeeAttendance;

use App\Interfaces\Hrim\EmployeeAttendance\DailyTimeRecordsInterface;
use App\Models\Hrim\Tar;
use App\Models\Hrim\TimeLog;
use App\Traits\ResponseTrait;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DailyTimeRecordsRepository implements DailyTimeRecordsInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            if ($request['cut_off'] == 'first') {
                $begin = new DateTime($request['year'] . '-' . ($request['month'] - 1) . '-26');
                $end = new DateTime($request['year'] . '-' . $request['month'] . '-11');
            } else {
                $begin = new DateTime($request['year'] . '-' . $request['month'] . '-11');
                $end = new DateTime($request['year'] . '-' . $request['month'] . '-26');
            }

            $time_logs = TimeLog::with(['user' => function ($query) {
                $query->with('userDetails');
            }, 'workSchedule' => function ($query) {
                $query->with('workMode');
            }])
                ->whereHas('user', function ($query) {
                    $query->where('user_id', Auth::user()->id);
                })
                ->whereDate('date', '>=', $begin)
                ->whereDate('date', '<=', $end)
                ->get();

            $tars = Tar::with('finalStatus')
                ->whereDate('date_from', '>=', $begin)
                ->whereDate('date_to', '<=', $end)
                ->where('user_id', Auth::user()->id)
                ->get();

            $interval = DateInterval::createFromDateString('1 day');
            $date_periods = new DatePeriod($begin, $interval, $end);
            $dates = [];
            foreach ($date_periods as $date_period) {
                $time_log = $time_logs->firstWhere('date', $date_period->format("Y-m-d"));
                $tar = $tars->firstWhere('date_from', $date_period->format("Y-m-d"));

                $dates[] = [
                    'date' => $date_period->format("Y-m-d"),
                    'date_formated' => $date_period->format("F d, Y"),
                    'day' => $date_period->format("l"),
                    'time_log' => $time_log,
                    'tar' => $tar,
                ];
            }

            $time_logs = collect($dates);
            DB::commit();
            return $this->response(200, 'Time Log List', $time_logs);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
