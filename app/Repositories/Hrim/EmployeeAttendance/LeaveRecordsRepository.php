<?php

namespace App\Repositories\Hrim\EmployeeAttendance;

use App\Interfaces\Hrim\EmployeeAttendance\LeaveRecordsInterface;
use App\Models\Hrim\Leave;
use App\Models\Hrim\LeaveDayTypeReference;
use App\Models\Hrim\LeaveTypeReference;
use App\Models\Hrim\LoaManagement;
use App\Traits\ResponseTrait;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class LeaveRecordsRepository implements LeaveRecordsInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->getUserAvailableLeaves();

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $user_leaves = $response['result']['user_leaves'];

            $leaves = Leave::with('leaveType', 'leaveDayType', 'relieverUser', 'firstApprover', 'secondApprover', 'thirdApprover', 'finalStatus')
                ->where('user_id', Auth::user()->id)
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Leaves List', compact('user_leaves', 'leaves'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        // dd($request['other']);
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'inclusive_date_from' => 'required|date|date_format:Y-m-d|before_or_equal:inclusive_date_to',
                'inclusive_date_to' => 'required|date|date_format:Y-m-d|after_or_equal:inclusive_date_from',
                'resume_of_work' => 'required',
                'type_of_leave' => 'required',
                'other' => $request['type_of_leave'] == 4 ? 'required' : 'nullable',
                'is_with_medical_certificate' => 'required_if:type_of_leave,1',
                'apply_for' => 'required',
                'is_with_pay' => 'required',
                'reason' => 'required',
                'reliever' => 'sometimes',
                'attachment' => 'sometimes|' . config('filesystems.validation_all'),
            ], [
                'other.required' => 'Other field is required, please specify the leave.',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $current_requested_points = 0;
            $current_total_days = 0;
            if ($validated['is_with_pay']) {
                $response = $this->getUserAvailableLeaves();
                if ($response['code'] != 200) {
                    return $this->response($response['code'], $response['message'], $response['result']);
                }
                $user_leaves = $response['result']['user_leaves'];
                $schedules = $response['result']['schedules'];

                $leave_day_type = LeaveDayTypeReference::find($validated['apply_for']);

                $current_leave_days_interval = DateInterval::createFromDateString('1 day');
                $current_date_periods = new DatePeriod(new DateTime($validated['inclusive_date_from'] . '- 2 days'), $current_leave_days_interval, new DateTime($validated['inclusive_date_to']));

                foreach ($current_date_periods as $current_date_period) {
                    if (in_array($current_date_period->format('l'), $schedules)) {
                        ++$current_total_days;
                    }
                }

                $current_requested_points = ($leave_day_type->points * $current_total_days);

                // dd($current_total_days);

                if ($validated['type_of_leave'] == 2) {
                    if ($user_leaves['vacation_leave'] < $current_requested_points) {
                        return $this->response(409, 'Leave', 'You don`t have enough Vacation leave');
                    }
                } else {
                    if ($user_leaves['sick_leave'] < $current_requested_points) {
                        return $this->response(409, 'Leave', 'You don`t have enough Sick leave');
                    }
                }
            }

            $approvers = LoaManagement::firstWhere([
                ['division_id', Auth::user()->division_id],
                ['branch_id', Auth::user()->userDetails->branch_id],
                ['department_id', Auth::user()->userDetails->department_id],
                ['loa_type_id', 4],
            ]);

            if (!$approvers) {
                return $this->response(500, 'Approver is required', 'Please create approver under your division, branch and Leave.');
            }

            $leave = Auth::user()->leaves()->create([
                'inclusive_date_from' => $validated['inclusive_date_from'],
                'inclusive_date_to' => $validated['inclusive_date_to'],
                'days' => $current_total_days,
                'points' => $current_requested_points,
                'resume_date' => $validated['resume_of_work'],
                'leave_type_id' => $validated['type_of_leave'],
                'other' => $validated['other'],
                'is_with_medical_certificate' => $validated['is_with_medical_certificate'],
                'leave_day_type_id' => $validated['apply_for'],
                'is_with_pay' => $validated['is_with_pay'],
                'reason' => $validated['reason'],
                'reliever' => $validated['reliever'],
                'first_approver' => $approvers->first_approver,
                'second_approver' => $approvers->second_approver,
                'third_approver' => $approvers->third_approver,
            ]);

            if ($validated['attachment']) {
                $response = $this->storeAttachment($validated['attachment']);
                if ($response['code'] == 500) {
                    return $this->response($response['code'], $response['message'], $response['result']);
                }
                $file = $response['result'];

                $leave->attachments()->create([
                    'original_name' => pathinfo($validated['attachment']->getClientOriginalName(), PATHINFO_FILENAME),
                    'path' => $file['file_path'],
                    'name' => $file['file_name'],
                    'extension' => $validated['attachment']->extension(),
                ]);
            }

            DB::commit();

            return $this->response(200, 'Leave has been successfully filed', $request);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id, $request = [])
    {
        DB::beginTransaction();
        try {
            $leave = Leave::with(
                'user',
                'leaveType',
                'leaveDayType',
                'relieverUser',
                'firstApprover',
                'secondApprover',
                'thirdApprover',
                'finalStatus',
                'attachments'
            )
                ->withCount('attachments')
                ->where('user_id', Auth::user()->id)
                ->find($id);

            if (!$leave) {
                return $this->response(404, 'Leave', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Leave', $leave);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $leave = $response['result'];

            $previous_requested_points = 0;
            $current_requested_points = 0;
            $previous_total_days = 0;
            $current_total_days = 0;

            if ($validated['is_with_pay']) {
                $response = $this->getUserAvailableLeaves();
                if ($response['code'] != 200) {
                    return $this->response($response['code'], $response['message'], $response['result']);
                }
                $user_leaves = $response['result']['user_leaves'];
                $schedules = $response['result']['schedules'];

                $leave_day_type = LeaveDayTypeReference::find($validated['apply_for']);
                $previous_leave_days_interval = DateInterval::createFromDateString('1 day');
                $previous_date_periods = new DatePeriod(new DateTime($leave->inclusive_date_from . '- 2 days'), $previous_leave_days_interval, new DateTime($leave->inclusive_date_to));

                foreach ($previous_date_periods as $previous_date_period) {
                    if (in_array($previous_date_period->format('l'), $schedules)) {
                        ++$previous_total_days;
                    }
                }

                $current_leave_days_interval = DateInterval::createFromDateString('1 day');
                $current_date_periods = new DatePeriod(new DateTime($validated['inclusive_date_from'] . '- 2 days'), $current_leave_days_interval, new DateTime($validated['inclusive_date_to']));

                foreach ($current_date_periods as $current_date_period) {
                    if (in_array($current_date_period->format('l'), $schedules)) {
                        ++$current_total_days;
                    }
                }

                $previous_requested_points = ($leave_day_type->points * $previous_total_days);
                $current_requested_points = ($leave_day_type->points * $current_total_days);

                $minus_points = 0;

                if ($validated['type_of_leave'] == $leave->leave_type_id) {
                    $minus_points = $previous_requested_points;
                }

                if ($validated['type_of_leave'] == 2) {
                    if (($user_leaves['vacation_leave'] + $minus_points) < $current_requested_points) {
                        return $this->response(409, 'Leave', 'You don`t have enough leave');
                    }
                } else {
                    if (($user_leaves['sick_leave'] + $minus_points) < $current_requested_points) {
                        return $this->response(409, 'Leave', 'You don`t have enough leave');
                    }
                }
            }

            $approvers = LoaManagement::firstWhere([
                ['division_id', Auth::user()->division_id],
                ['branch_id', Auth::user()->userDetails->branch_id],
                ['loa_type_id', 4],
            ]);

            if (!$approvers) {
                return $this->response(500, 'Approver is required', 'Please create approver under your division, branch and Leave.');
            }

            $leave->update([
                'inclusive_date_from' => $validated['inclusive_date_from'],
                'inclusive_date_to' => $validated['inclusive_date_to'],
                'days' => $current_total_days,
                'points' => $current_requested_points,
                'resume_date' => $validated['resume_of_work'],
                'leave_type_id' => $validated['type_of_leave'],
                'other' => $validated['other'],
                'is_with_medical_certificate' => $validated['is_with_medical_certificate'],
                'leave_day_type_id' => $validated['apply_for'],
                'is_with_pay' => $validated['is_with_pay'],
                'reason' => $validated['reason'],
                'reliever' => $validated['reliever'],
                'first_approver' => $approvers->first_approver,
                'second_approver' => $approvers->second_approver,
                'third_approver' => $approvers->third_approver,
            ]);

            if ($validated['attachment']) {
                $response = $this->storeAttachment($validated['attachment']);
                if ($response['code'] == 500) {
                    return $this->response($response['code'], $response['message'], $response['result']);
                }
                $file = $response['result'];

                $leave->attachments()->create([
                    'original_name' => pathinfo($validated['attachment']->getClientOriginalName(), PATHINFO_FILENAME),
                    'path' => $file['file_path'],
                    'name' => $file['file_name'],
                    'extension' => $validated['attachment']->extension(),
                ]);
            }

            DB::commit();

            return $this->response(200, 'Leave has been successfully updated', $request);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
    }

    public function restore($id)
    {
    }

    public function storeAttachment($file)
    {
        DB::beginTransaction();
        try {
            $year = date('Y', strtotime(now()));
            $file_path = strtolower(str_replace(' ', '_', 'employee_attendance/leave/' . $year . '/'));
            $file_name = Auth::user()->userDetails->employee_number . '-' . date('mdYHis') . uniqid() . '.' . $file->extension();

            if (in_array($file->extension(), config('filesystems.image_type'))) {
                $image_resize = Image::make($file->getRealPath())->resize(1024, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                Storage::disk('hrim_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
            } elseif (in_array($file->extension(), config('filesystems.file_type'))) {
                Storage::disk('hrim_gcs')->putFileAs($file_path, $file, $file_name);
            }

            DB::commit();

            return $this->response(200, 'Successfuly Save', compact('file_path', 'file_name'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getUserAvailableLeaves()
    {
        DB::beginTransaction();
        try {
            $accountdetails = Auth::user()->userDetails;
            $employment_category = Auth::user()->userDetails->employmentCategory;
            $work_schedule = Auth::user()->userDetails->workSchedule;
            $schedules = [];
            if ($work_schedule->monday) {
                $schedules[] = 'Monday';
            }
            if ($work_schedule->tuesday) {
                $schedules[] = 'Tuesday';
            }
            if ($work_schedule->wednesday) {
                $schedules[] = 'Wednesday';
            }
            if ($work_schedule->thursday) {
                $schedules[] = 'Thursday';
            }
            if ($work_schedule->friday) {
                $schedules[] = 'Friday';
            }
            if ($work_schedule->saturday) {
                $schedules[] = 'Saturday';
            }
            if ($work_schedule->sunday) {
                $schedules[] = 'Sunday';
            }

            $leave_types = LeaveTypeReference::with(['leaves' => function ($query) {
                $query->with('leaveDayType')->where([
                    ['user_id', Auth::user()->id],
                    ['final_status_id', '=', 3],
                    ['is_with_pay', 1],
                ]);
            }])->get();

            $date1 = new DateTime($accountdetails->hired_date);
            $date2 = new DateTime(date("Y-m-d"));
            $interval = $date1->diff($date2);
            // dd( "difference " . $interval->y . " years, " . $interval->m . " months, " . $interval->d . " days ");
            $vacation_leave = 0;
            if ($accountdetails->employment_status_id == 1) {

                if ($interval->y <= 5) {
                    $vacation_leave = 5;
                }
                if ($interval->y == 6) {
                    $vacation_leave = 6;
                }
                if ($interval->y == 7) {
                    $vacation_leave = 7;
                }
                if ($interval->y == 8) {
                    $vacation_leave = 8;
                }
                if ($interval->y == 9) {
                    $vacation_leave = 9;
                }
                if ($interval->y >= 10) {
                    $vacation_leave = 10;
                }
            }

            $sick_leave = 5;

            $user_leaves = [
                'sick_leave' => $sick_leave,
                'vacation_leave' => $vacation_leave,
            ];

            foreach ($leave_types as $leave_type) {
                foreach ($leave_type->leaves as $leave) {
                    if ($leave_type->code == 'sick_leave') {
                        $user_leaves['sick_leave'] -= ($leave->leaveDayType->points * $leave->days);
                    } else {
                        $user_leaves['vacation_leave'] -= ($leave->leaveDayType->points * $leave->days);
                    }
                }
            }
            DB::commit();

            return $this->response(200, 'User Available Leave List', compact('schedules', 'user_leaves'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
