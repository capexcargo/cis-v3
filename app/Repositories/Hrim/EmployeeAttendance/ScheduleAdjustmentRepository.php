<?php

namespace App\Repositories\Hrim\EmployeeAttendance;

use App\Interfaces\Hrim\EmployeeAttendance\ScheduleAdjustmentInterface;
use App\Models\Hrim\LoaManagement;
use App\Models\Hrim\ScheduleAdjustment;
use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ScheduleAdjustmentRepository implements ScheduleAdjustmentInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $schedule_adjustments = ScheduleAdjustment::with('user', 'currentWorkSchedule', 'newWorkSchedule', 'firstApprover', 'secondApprover', 'thirdApprover', 'finalStatus')
                ->whereHas('user', function ($query) use ($request) {
                    $query->where('division_id', Auth::user()->division_id)
                        ->when($request['employee_name'] ?? false, function ($query) use ($request) {
                            $query->where('name', 'like', '%' . $request['employee_name'] . '%');
                        });
                })
                ->when($request['date_filed'] ?? false, function ($query) use ($request) {
                    $query->whereDate('created_at', $request['date_filed']);
                })
                ->when($request['date_filed'] ?? false, function ($query) use ($request) {
                    $query->whereDate('created_at', $request['date_filed']);
                })
                ->when($request['status'] ?? false, function ($query) use ($request) {
                    $query->whereDate('final_status_id', $request['status']);
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Schedule Adjustment List', $schedule_adjustments);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'employee_name' => 'required',
                'employee_id' => 'required',

                'current_schedule' => 'required',

                'new_schedule' => 'required',
                'new_inclusive_date_start_date' => 'required',
                'new_inclusive_date_end_date' => 'required_if:set_an_end_date, 1',
                'set_an_end_date' => 'sometimes',
                'reason' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $employee_information = User::find($validated['employee_id']);

            $approvers = LoaManagement::firstWhere([
                ['division_id', $employee_information->division_id],
                ['branch_id', $employee_information->userDetails->branch_id],
                ['department_id', $employee_information->userDetails->department_id],
                ['loa_type_id', 2],
            ]);

            if (!$approvers) return $this->response(500, 'Approver is required', 'Please create approver under your division, branch and SAR.');


            $schedule_adjustment = ScheduleAdjustment::create([
                'user_id' => $validated['employee_id'],
                'current_work_schedule_id' => $validated['current_schedule'],
                'new_work_schedule_id' => $validated['new_schedule'],
                'inclusive_date_from' => $validated['new_inclusive_date_start_date'],
                'inclusive_date_to' => $validated['new_inclusive_date_end_date'],
                'reason' => $validated['reason'],
                'first_approver' => $approvers->first_approver,
                'second_approver' => $approvers->second_approver,
                'third_approver' => $approvers->third_approver,
            ]);

            DB::commit();
            return $this->response(200, 'Schedule adjustment has been successfully requested', $schedule_adjustment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id, $request = [])
    {
        DB::beginTransaction();
        try {
            $schedule_adjustment = ScheduleAdjustment::with(
                'user',
                'currentWorkSchedule',
                'newWorkSchedule',
                'firstApprover',
                'secondApprover',
                'thirdApprover',
                'finalStatus'
            )
                ->find($id);
            if (!$schedule_adjustment) return $this->response(404, 'Schedule Adjustment', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Schedule Adjustment', $schedule_adjustment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $response = $this->show($id);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $schedule_adjustment = $response['result'];

            $employee_information = User::find($validated['employee_id']);

            $approvers = LoaManagement::firstWhere([
                ['division_id', $employee_information->division_id],
                ['branch_id', $employee_information->userDetails->branch_id],
                ['loa_type_id', 2],
            ]);

            if (!$approvers) return $this->response(500, 'Approver is required', 'Please create approver under your division, branch and SAR.');


            $schedule_adjustment->update([
                'user_id' => $validated['employee_id'],
                'current_work_schedule_id' => $validated['current_schedule'],
                'new_work_schedule_id' => $validated['new_schedule'],
                'inclusive_date_from' => $validated['new_inclusive_date_start_date'],
                'inclusive_date_to' => $validated['new_inclusive_date_end_date'],
                'reason' => $validated['reason'],
                'first_approver' => $approvers->first_approver,
                'second_approver' => $approvers->second_approver,
                'third_approver' => $approvers->third_approver,
            ]);

            DB::commit();
            return $this->response(200, 'Schedule adjustment has been successfully updated', $schedule_adjustment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $schedule_adjustment = $response['result'];

            $schedule_adjustment->delete();

            DB::commit();
            return $this->response(200, 'Schedule Adjustment Request has been successfully deleted!', $schedule_adjustment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function restore($id)
    {
    }
}
