<?php

namespace App\Repositories\Hrim\EmployeeAttendance;

use App\Interfaces\Hrim\EmployeeAttendance\TarInterface;
use App\Models\Hrim\LoaManagement;
use App\Models\Hrim\Tar;
use App\Models\Hrim\TimeLog;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TarRepository implements TarInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $time_logs = TimeLog::with(['user' => function ($query) {
                $query->with('userDetails');
            }])
                ->whereHas('user', function ($query) {
                    $query->where('user_id', Auth::user()->id);
                })
                ->when($request['date'] ?? false, function ($query) use ($request) {
                    $query->whereDate('date_from', $request['date']);
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);
            DB::commit();
            return $this->response(200, 'Time Log List', $time_logs);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'time_log_id' => 'nullable',
                'date_from' => 'required',
                'date_to' => 'required',
                'actual_time_in' => 'required',
                'actual_time_out' => 'required',
                'reason_of_adjustment' => 'required',
                'time_log_id' => 'nullable',
                'time_log_id' => 'nullable',
            ]);
            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $approvers = LoaManagement::firstWhere([
                ['division_id', Auth::user()->division_id],
                ['branch_id', Auth::user()->userDetails->branch_id],
                ['department_id', Auth::user()->userDetails->department_id],
                ['loa_type_id', 1],
            ]);

            if (!$approvers) return $this->response(500, 'Approver is required', 'Please create approver under your division, branch and TAR.');

            $time_logs = TimeLog::find($validated['time_log_id']);

            $tar = Auth::user()->tars()->updateOrCreate([
                'date_from' => $validated['date_from'],
            ], [
                'time_log_id' => ($validated['time_log_id'] ? $validated['time_log_id'] : null),
                'actual_time_in' => $validated['actual_time_in'],
                'actual_time_out' => $validated['actual_time_out'],
                'current_time_in' => ($validated['time_log_id'] ? $time_logs->time_in : null),
                'current_time_out' => ($validated['time_log_id'] ? $time_logs->time_out : null),
                'date_from' => $validated['date_from'],
                'date_to' => $validated['date_to'],
                'tar_reason_id' => $validated['reason_of_adjustment'],
                'first_approver' => $approvers->first_approver,
                'second_approver' => $approvers->second_approver,
                'third_approver' => $approvers->third_approver,
            ]);

            DB::commit();
            return $this->response(200, ('Time adjustment has been successfully ' . ($tar->wasRecentlyCreated ? 'requested!' : 'updated!')), $tar);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($request)
    {
        DB::beginTransaction();
        try {
            $tar = Tar::with(
                'user',
                'tarReason',
                'firstApprover',
                'secondApprover',
                'thirdApprover',
                'adminApprover',
                'finalStatus'
            )
                ->firstWhere([
                    ['user_id', Auth::user()->id],
                    ['date_from', $request['date']]
                ]);
            if (!$tar) return $this->response(404, 'Tar', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Tar', $tar);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}

