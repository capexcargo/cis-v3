<?php

namespace App\Repositories\Hrim\EmployeeAttendance;

use App\Interfaces\Hrim\EmployeeAttendance\OvertimeRecordsInterface;
use App\Models\Hrim\LoaManagement;
use App\Models\Hrim\Overtime;
use App\Models\Hrim\TimeLog;
use App\Traits\ResponseTrait;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OvertimeRecordsRepository implements OvertimeRecordsInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $overtime_records = Overtime::with([
                'user',
                'dateCategory',
                'firstApprover',
                'secondApprover',
                'thirdApprover',
                'finalStatus',
                'timeLog' => function ($query) {
                    $query->with('workSchedule');
                },
            ])
                ->where('user_id', Auth::user()->id)
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Overtime List', $overtime_records);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'time_log' => 'required',
                'date_time_in' => 'required',
                'date_time_out' => 'required',
                'time_in' => 'required',
                'time_out' => 'required',
                'work_schedule' => 'required',
                'rendered_ot_hours' => 'required',

                'type_of_ot' => 'required',
                'is_have_ot_break' => 'sometimes',
                'actual_date_from' => 'required',
                'actual_date_to' => 'required|before_or_equal:date_time_out',
                'from' => 'required',
                'to' => 'required|before_or_equal:time_out',
                'overtime_request' => 'required',
                'reason' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $approvers = LoaManagement::firstWhere([
                ['division_id', Auth::user()->division_id],
                ['branch_id', Auth::user()->userDetails->branch_id],
                ['department_id', Auth::user()->userDetails->department_id],
                ['loa_type_id', 3],
            ]);

            if (!$approvers) {
                return $this->response(500, 'Approver is required', 'Please create approver under your division, branch and Overtime.');
            }

            $overtime = Overtime::create([
                'user_id' => Auth::user()->id,
                'time_log_id' => $validated['time_log']->id,
                'date_time_from' => $validated['actual_date_from'],
                'date_time_to' => $validated['actual_date_to'],
                'time_from' => $validated['from'],
                'time_to' => $validated['to'],
                'overtime_rendered' => $validated['rendered_ot_hours'],
                'overtime_request' => $validated['overtime_request'],
                'overtime_approved' => null,
                'date_category_id' => $validated['type_of_ot'],
                'reason' => $validated['reason'],
                'first_approver' => $approvers->first_approver,
                'second_approver' => $approvers->second_approver,
                'third_approver' => $approvers->third_approver,
            ]);

            DB::commit();

            return $this->response(200, 'Overtime successfully applied!', $overtime);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id, $request = [])
    {
        DB::beginTransaction();
        try {
            $overtime_record = Overtime::with([
                'user',
                'timeLog' => function ($query) {
                    $query->with('workSchedule');
                },
                'dateCategory',
                'firstApprover',
                'secondApprover',
                'thirdApprover',
                'adminApprover',
                'finalStatus'
            ])
                ->firstWhere([['user_id', Auth::user()->id], ['id', $id]]);
            if (!$overtime_record) {
                return $this->response(404, 'Overtime Record', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Overtime Record', $overtime_record);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function showDate($request)
    {
        DB::beginTransaction();
        try {
            $time_log = TimeLog::with(
                'user',
                'workSchedule',
                'dateCategory',
                'workMode',
            )
                ->firstWhere([
                    ['user_id', $request['user_id']],
                    ['date', $request['date']],
                ]);

            if (!$time_log) {
                return $this->response(404, 'Leave', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Time Log', $time_log);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $overtime = $response['result'];

            $approvers = LoaManagement::firstWhere([
                ['division_id', Auth::user()->division_id],
                ['branch_id', Auth::user()->userDetails->branch_id],
                ['loa_type_id', 3],
            ]);

            if (!$approvers) {
                return $this->response(500, 'Approver is required', 'Please create approver under your division, branch and Overtime.');
            }

            $overtime->update([
                'time_log_id' => $validated['time_log']->id,
                'date_time_from' => $validated['actual_date_from'],
                'date_time_to' => $validated['actual_date_to'],
                'time_from' => $validated['from'],
                'time_to' => $validated['to'],
                'overtime_rendered' => $validated['rendered_ot_hours'],
                'overtime_request' => $validated['overtime_request'],
                'overtime_approved' => null,
                'date_category_id' => $validated['type_of_ot'],
                'reason' => $validated['reason'],
                'first_approver' => $approvers->first_approver,
                'second_approver' => $approvers->second_approver,
                'third_approver' => $approvers->third_approver,
            ]);

            DB::commit();

            return $this->response(200, 'Overtime has been successfully updated', $request);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getOvertimed($request)
    {
        DB::beginTransaction();
        try {
            if ($request['cut_off'] == 'first') {
                $begin = new DateTime($request['year'] . '-' . ($request['month'] - 1) . '-26');
                $end = new DateTime($request['year'] . '-' . $request['month'] . '-10');
            } else {
                $begin = new DateTime($request['year'] . '-' . $request['month'] . '-11');
                $end = new DateTime($request['year'] . '-' . $request['month'] . '-26');
            }
          
            $overtime_records = TimeLog::with('user', 'workSchedule', 'dateCategory', 'workMode')
                ->where([['user_id', $request['user_id']], ['computed_ot', '>=', 1]])
                ->whereDate('date', '>=', $begin)
                ->whereDate('date', '<=', $end)
                ->orderBy('date', 'asc')
                ->get();

            DB::commit();

            return $this->response(200, 'Overtime List', $overtime_records);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getOvertimedate($request)
    {
        DB::beginTransaction();
        try {
            $begin = new DateTime($request['date_ot']);

            $overtime_records = TimeLog::with(['user', 'workSchedule', 'dateCategory', 'workMode', 'overtime' => function ($query) {
                $query->with('dateCategory');
            }])
                ->where([['user_id', $request['user_id']], ['computed_ot', '>=', 1]])
                ->whereDate('date', '=', $begin)
                ->orderBy('date', 'asc')
                ->get();

            DB::commit();

            return $this->response(200, 'Overtime List', $overtime_records);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getComputedOvertime($request)
    {
        DB::beginTransaction();
        try {
            if (!$request['time_log']) {
                return $this->response(409, 'Computed Overtime', 'Required time log');
            }



            $work_schedule_ot_break_time_from = date_create($request['time_log']->date . ' ' . $request['time_log']->workSchedule->ot_break_from);
            $work_schedule_ot_break_time_to = date_create($request['time_log']->date . ' ' . $request['time_log']->workSchedule->ot_break_to);

            if ($request['time_log']->workSchedule->shift == 'night_shift') {
                $work_schedule_ot_break_time_from = date_create(date("Y-m-d", strtotime('+1 day', strtotime($request['time_log']->date))) . ' ' . $request['time_log']->workSchedule->ot_break_from);
                $work_schedule_ot_break_time_to = date_create(date("Y-m-d", strtotime('+1 day', strtotime($request['time_log']->date))) . ' ' . $request['time_log']->workSchedule->ot_break_to);
            }

            $ot_time_from = date_create($request['time_log']->date . ' ' . $request['time_log']->workSchedule->time_to);
            $ot_time_to = date_create($request['actual_date_to'] . ' ' . $request['to']);
            $rendered_time = 0;
            if ($request['is_have_ot_break']) {
                if ($work_schedule_ot_break_time_from > $ot_time_to) {
                    $difference = date_diff($ot_time_from, $ot_time_to);
                    $rendered_time = $this->computeTime(($difference->format('%d') * 24) + ($difference->format('%H.%I')));
                } else {
                    $before = 0;
                    $after = 0;

                    if ($work_schedule_ot_break_time_from > $ot_time_from) {
                        $difference_before = date_diff($ot_time_from, $work_schedule_ot_break_time_from);
                        $before = $this->computeTime(($difference_before->format('%H.%I')));
                    }

                    if ($work_schedule_ot_break_time_to < $ot_time_to) {
                        $difference_after = date_diff($work_schedule_ot_break_time_to, $ot_time_to);
                        $after = $this->computeTime(($difference_after->format('%d') * 24) + ($difference_after->format('%H.%I')));
                    }

                    $rendered_time = ($before + $after);
                }
            } else {
                $difference = date_diff($ot_time_from, $ot_time_to);
                $rendered_time = $this->computeTime(($difference->format('%d') * 24) + ($difference->format('%H.%I')));
                //    dd($ot_time_from,$ot_time_to);
            }

            $rendered_time = ($rendered_time - $request['time_log']->late);
            if ($rendered_time  < 1) {
                $rendered_time = 0;
            }

            DB::commit();

            return $this->response(200, 'Computed Overtime', round($rendered_time, 2));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function computeTime($time)
    {
        $time = floatval($time);
        $h = intval($time);
        $m = ((($time - $h) * 100) / 60);

        return round(($h + $m), 2);
    }
}
