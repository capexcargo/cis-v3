<?php

namespace App\Repositories\Hrim\CompensationAndBenefits;

use App\Interfaces\Hrim\CompensationAndBenefits\SalaryGradeManagementInterface;
use App\Models\Hrim\PagibigManagement;
use App\Models\Hrim\SalaryGradeManagement;
use App\Models\Hrim\SssManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SalaryGradeManagementRepository implements SalaryGradeManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $salary_grade_managements = SalaryGradeManagement::get();
            // dd($salary_grade_managements);
            DB::commit();
            return $this->response(200, 'Salary Grade List', compact('salary_grade_managements'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {

            $salaryGradeManagement = SalaryGradeManagement::create([
                'grade' => $request['grade'],
                'minimun_amount' => $request['minimun_amount'],
                'maximum_amount' => $request['maximum_amount'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'Salary Grade Created!', $salaryGradeManagement);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'grade' => 'required',
                'minimun_amount' => 'required',
                'maximum_amount' => 'required',
            ]);


            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $salary_grade_management = SalaryGradeManagement::findOrFail($id);
            if (!$salary_grade_management) return $this->response(404, 'Salary Grade', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Salary Grade', $salary_grade_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $salary_grade_management = $response['result'];

            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $salary_grade_management->update([
                'grade' => $validated['grade'],
                'minimun_amount' => $validated['minimun_amount'],
                'maximum_amount' => $validated['maximum_amount'],
            ]);


            DB::commit();
            return $this->response(200, 'Salary Grade has been successfully updated!', $salary_grade_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
