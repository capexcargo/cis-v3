<?php

namespace App\Repositories\Hrim\CompensationAndBenefits;

use App\Interfaces\Hrim\CompensationAndBenefits\BenefitsManagementInterface;
use App\Models\Hrim\BenefitsManagement;
use App\Models\Hrim\PagibigManagement;
use App\Models\Hrim\SalaryGradeManagement;
use App\Models\Hrim\SssManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BenefitsManagementRepository implements BenefitsManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $benefits_managements = BenefitsManagement::with('benefitsUser')->get();
            // dd($salary_grade_managements);
            DB::commit();
            return $this->response(200, 'Benefits List', compact('benefits_managements'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {

            $benefitsManagement = BenefitsManagement::create([
                'benefit_type' => $request['benefit_type'],
                'description' => $request['description'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'New benefits has been successfully created!', $benefitsManagement);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'benefit_type' => 'required',
                'description' => 'required',
            ]);


            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $benefits_management = SalaryGradeManagement::findOrFail($id);
            if (!$benefits_management) return $this->response(404, 'Benefts', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Benefits', $benefits_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $benefits_management = $response['result'];

            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $benefits_management->update([
                'benefit_type' => $validated['benefit_type'],
                'description' => $validated['description'],
            ]);


            DB::commit();
            return $this->response(200, 'Benefits has been successfully updated!', $benefits_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $benefits = BenefitsManagement::find($id);
            if (!$benefits) return $this->response(404, 'Benefit', 'Not Found!');
            $benefits->delete();

            DB::commit();
            return $this->response(200, 'Benefits has been successfully deleted!', $benefits);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
