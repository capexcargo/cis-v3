<?php

namespace App\Repositories\Hrim\CompensationAndBenefits;

use App\Interfaces\Hrim\CompensationAndBenefits\StatutoryManagementInterface;
use App\Models\Hrim\PagibigManagement;
use App\Models\Hrim\SalaryGradeManagement;
use App\Models\Hrim\SssManagement;
use App\Models\Hrim\StatutoryManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class StatutoryManagementRepository implements StatutoryManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $statutory_managements = StatutoryManagement::with('StatutoryUser')->get();
            // dd($salary_grade_managements);
            DB::commit();
            return $this->response(200, 'Statutory List', compact('statutory_managements'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {

            $statutoryManagement = StatutoryManagement::create([
                'statutor_benefit_type' => $request['statutor_benefit_type'],
                'description' => $request['description'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'Statutory Benefits Created!', $statutoryManagement);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'statutor_benefit_type' => 'required',
                'description' => 'required',
            ]);


            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $statutory_management = StatutoryManagement::findOrFail($id);
            if (!$statutory_management) return $this->response(404, 'Statutory', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Statutory', $statutory_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $statutory_management = $response['result'];

            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $statutory_management->update([
                'statutor_benefit_type' => $validated['statutor_benefit_type'],
                'description' => $validated['description'],
            ]);


            DB::commit();
            return $this->response(200, 'Statutory Benefits has been successfully updated!', $statutory_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
