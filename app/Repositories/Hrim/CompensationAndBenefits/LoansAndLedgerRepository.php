<?php

namespace App\Repositories\Hrim\CompensationAndBenefits;

use App\Interfaces\Hrim\CompensationAndBenefits\LoansAndLedgerInterface;
use App\Models\Hrim\CutOffManagement;
use App\Models\Hrim\LoaManagement;
use App\Models\Hrim\LoanDetails;
use App\Models\Hrim\LoansAndLedger;
use App\Models\User;
use App\Traits\ResponseTrait;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class LoansAndLedgerRepository implements LoansAndLedgerInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $loans_and_ledger = LoansAndLedger::with('loanType', 'finalStatus', 'loanDetails')
                ->where('user_id', Auth::user()->id)
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);
            DB::commit();
            return $this->response(200, 'Loans And Ledger',  $loans_and_ledger);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index2($request)
    {
        DB::beginTransaction();
        try {
            $loans_and_ledger = User::with('userDetails', 'loans')
                ->where('name', 'like', '%' . $request['employee_name'] . '%')
                ->whereHas('userDetails', function ($query) use ($request) {
                    $query->when($request['employee_id'] ?? false, function ($query) use ($request) {
                        $query->where('employee_number', 'like', '%' . ($request['employee_id'] ?? false) . '%');
                    });
                    $query->when($request['branch'] ?? false, function ($query) use ($request) {
                        $query->where('branch_id', $request['branch'] ?? false);
                    });
                })
                ->whereHas('loans', function ($query) use ($request) {
                    $query->select('user_id', 'third_approver', 'second_status', 'third_status', 'final_status', 'end_date')->distinct();
                })
                ->when($request['employee_name'] ?? false, function ($query) use ($request) {
                    $query->where('name', 'like', '%' . ($request['employee_name'] ?? false) . '%');
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Loans And Ledger',  $loans_and_ledger);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {

        DB::beginTransaction();
        try {

            $validator = Validator::make($request, [
                'reference_number' => 'sometimes',
                'type' => 'required',
                'principal_amount' => 'required',
                'total_interest' => 'sometimes',
                'terms' => 'required',
                'term_type' => 'required',
                'payment_start_date' => 'required',
                'payment_end_date' => 'required',
                'purpose' => 'sometimes',
                'first_status' => 'sometimes',
            ]);
            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {

            $response = $this->createValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            // $loa = LoaManagement::where('division_id', Auth::user()->userDetails->division_id)
            //     ->where('loa_type_id', 6)->first();
            $approvers = LoaManagement::firstWhere([
                ['division_id', Auth::user()->division_id],
                ['branch_id', Auth::user()->userDetails->branch_id],
                ['department_id', Auth::user()->userDetails->department_id],
                ['loa_type_id', 6],
            ]);

            if (!$approvers) return $this->response(500, 'Approver is required.', 'Please create approver/s under your division, department and branch.');

            $loans_and_ledger = LoansAndLedger::create([
                'user_id' => Auth::user()->id,
                'reference_no' => $validated['reference_number'],
                'type' => $validated['type'],
                'principal_amount' => $validated['principal_amount'],
                'interest' => 0,
                'terms' => $validated['terms'],
                'term_type' => $validated['term_type'],
                'start_date' => $validated['payment_start_date'],
                'end_date' => $validated['payment_end_date'],
                'purpose' => $validated['purpose'],
                'first_approver' => $approvers->first_approver,
                'second_approver' => $approvers->second_approver,
                'third_approver' => $approvers->third_approver,
                'first_status' => 1,
                'second_status' => 1,
                'third_status' => 1,
                'final_status' => 1,
            ]);

            DB::commit();
            return $this->response(200, "Loan / Deduction request has been successfully filed! Please print the ATD for employee's Salary Deduction consent.", $loans_and_ledger);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $loans_and_ledger = LoansAndLedger::findOrFail($id);
            if (!$loans_and_ledger) return $this->response(404, 'Loans And Ledger', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Loans And Ledger', $loans_and_ledger);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'reference_number' => 'sometimes',
                'type' => 'required',
                'principal_amount' => 'sometimes',
                'total_interest' => 'sometimes',
                'terms' => 'required',
                'term_type' => 'required',
                'payment_start_date' => 'required',
                'payment_end_date' => 'required',
                'purpose' => 'sometimes',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $loans_and_ledger = $response['result'];

            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $loans_and_ledger->update([
                'reference_no' => $validated['reference_number'],
                'type' => $validated['type'],
                'principal_amount' => $validated['principal_amount'],
                'interest' => 0,
                'terms' => $validated['terms'],
                'term_type' => $validated['term_type'],
                'start_date' => $validated['payment_start_date'],
                'end_date' => $validated['payment_end_date'],
                'purpose' => $validated['purpose'],
            ]);

            DB::commit();
            return $this->response(200, 'Loans And Ledger has been successfully updated!', $loans_and_ledger);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // public function getBetweenDates($startDate, $endDate)
    // {
    //     $dateArray = [];

    //     $startDate = strtotime($startDate);
    //     $endDate = strtotime($endDate);

    //     for ($currentDate = $startDate; $currentDate <= $endDate; $currentDate += (86400)) {
    //         $date = date('Y-m-d', $currentDate);
    //         $dateArray[] = $date;
    //     }

    //     return $dateArray;
    // }
    // $dates = $this->getBetweenDates($start_date, $end_date);

    public function approve($id, $approved_amount, $is_bybass)
    {
        // dd($approved_amount);   
        $payment_dates = [];
        // $cutoff_year = [];
        // $cutoff_month = [];
        // $cutoff_day = [];
        DB::beginTransaction();
        try {
            $loans_and_ledger = LoansAndLedger::find($id);
            if (!$loans_and_ledger) return $this->response(404, 'Loans And Ledger', 'Not Found!');

            $start_date = $loans_and_ledger->start_date;
            $end_date = $loans_and_ledger->end_date;
            $term_type = $loans_and_ledger->term_type;
            $terms = $loans_and_ledger->terms;

            $begin = new DateTime($start_date);
            $end = new DateTime($end_date);

            if ($term_type == 1) {
                $add_date = ("+15 day");
            } else {
                $add_date = ("+1 month");
            }

            for ($j = $begin; $j <= $end; $j->modify($add_date)) {
                $payment_dates[] = $j->format('Y-m-d');
            }


            if ($is_bybass == "is_bypass") {
                $loans_and_ledger->update([
                    'overrule_approver' => Auth::user()->id,
                    'overrule_approval_date' => date("Y-m-d"),
                    'final_status' => 3,
                    'approved_amount' => $approved_amount,
                ]);

                foreach ($payment_dates as $i => $payment_date) {

                    $cutoff = CutOffManagement::where('cutoff_date', $payment_dates[$i])->first();
                    if (!$cutoff) return $this->response(404, 'Payroll Cutoff', 'Cutoff Not Found!');

                    $loans_and_ledger->loanDetails()->updateOrCreate(
                        [
                            'loan_id' =>  $loans_and_ledger->id,
                            'payment_date' =>  $payment_dates[$i],
                            'year' => $cutoff->payout_year,
                            'month' => $cutoff->payout_month,
                            'cutoff' => $cutoff->payout_cutoff,
                            'amount' => $loans_and_ledger->principal_amount / $terms,
                            'interest' => 0,
                            'status' => 1
                        ]
                    );
                }
            } else {

                if ($loans_and_ledger->first_approver != "" && $loans_and_ledger->first_status == 1 && $loans_and_ledger->second_approver == "") {
                    $first_status_approve = 3;
                    $second_status_approve = 1;
                    $third_status_approve = 1;
                    $final_status_approve = 3;
                } else if ($loans_and_ledger->second_approver != "" &&  $loans_and_ledger->second_status == 1 && $loans_and_ledger->first_status == 3) {
                    $first_status_approve = 3;
                    $second_status_approve = 3;
                    $third_status_approve = 1;
                    $final_status_approve = 3;
                } else if ($loans_and_ledger->third_approver != "" && $loans_and_ledger->third_status == 1 && $loans_and_ledger->second_status == 3) {
                    $first_status_approve = 3;
                    $second_status_approve = 3;
                    $third_status_approve = 3;
                    $final_status_approve = 3;
                }

                    foreach ($payment_dates as $i => $payment_date) {

                        $cutoff = CutOffManagement::where('cutoff_date', $payment_dates[$i])->first();
                        if (!$cutoff) return $this->response(404, 'Payroll Cutoff', 'Cutoff Not Found!');

                        $loans_and_ledger->update([
                            'first_status' => $first_status_approve,
                            'second_status' => $second_status_approve,
                            'third_status' => $third_status_approve,
                            'final_status' => $final_status_approve,
                            'approved_amount' => $approved_amount,
                        ]);

                        

                        if ($final_status_approve == 3) {
                            $loans_and_ledger->loanDetails()->updateOrCreate(
                                [
                                    'loan_id' =>  $loans_and_ledger->id,
                                    'payment_date' =>  $payment_dates[$i],
                                    'year' => $cutoff->payout_year,
                                    'month' => $cutoff->payout_month,
                                    'cutoff' => $cutoff->payout_cutoff,
                                    'amount' => $loans_and_ledger->principal_amount / $terms,
                                    'interest' => 0,
                                    'status' => 1
                                ]
                            );
                        }
                    }
               
            }

            DB::commit();
            return $this->response(200, 'Loan Request has been successfully approved!', $loans_and_ledger);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function reasonValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'decline_reason' => 'required',
            ], [
                'decline_reason.required' => 'The reason field is required.',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function decline($id, $request, $is_bybass)
    {
        DB::beginTransaction();
        try {

            $response = $this->reasonValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $loans_and_ledger = LoansAndLedger::find($id);
            if (!$loans_and_ledger) return $this->response(404, 'Loans And Ledger', 'Not Found!');

            if ($is_bybass == "is_bypass") {
                $loans_and_ledger->update([
                    'overrule_approver' => Auth::user()->id,
                    'overrule_approval_date' => date("Y-m-d"),
                    'final_status' => 4,
                    'decline_reason' => $validated['decline_reason'],
                ]);

                $loans_and_ledger->loanDetails()->forceDelete();
            } else {
                if ($loans_and_ledger->first_approver != "" && $loans_and_ledger->first_status == 1 && $loans_and_ledger->second_approver == "") {
                    $first_status_decline = 4;
                    $second_status_decline = 1;
                    $third_status_decline = 1;
                    $final_status_decline = 4;
                } else if ($loans_and_ledger->second_approver != "" && $loans_and_ledger->second_status == 1 && $loans_and_ledger->first_status == 3) {
                    $first_status_decline = 3;
                    $second_status_decline = 4;
                    $third_status_decline = 1;
                    $final_status_decline = 4;
                } else if ($loans_and_ledger->third_approver != "" && $loans_and_ledger->third_status == 1 && $loans_and_ledger->second_status == 3) {
                    $first_status_decline = 4;
                    $second_status_decline = 4;
                    $third_status_decline = 4;
                    $final_status_decline = 4;
                }

                $loans_and_ledger->update([
                    'first_status' => $first_status_decline,
                    'second_status' => $second_status_decline,
                    'third_status' => $third_status_decline,
                    'final_status' => $final_status_decline,
                    'decline_reason' => $validated['decline_reason'],
                ]);
            }

            DB::commit();
            return $this->response(200, 'Loan Request has been successfully declined!', $loans_and_ledger);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $loans_and_ledger = LoansAndLedger::find($id);
            if (!$loans_and_ledger) return $this->response(404, 'Loans And Ledger', 'Not Found!');
            $loans_and_ledger->delete();

            DB::commit();
            return $this->response(200, 'Loan / Deduction Request has been Successfully cancelled!', $loans_and_ledger);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show_schedule_payment($id)
    {
        DB::beginTransaction();
        try {
            $loans = LoansAndLedger::findOrFail($id);
            // $this->terms = $loans->terms;

            $loan_payments_references = LoanDetails::where('loan_id', $id)->get();

            $remaining = LoanDetails::where('loan_id', $id)->where('status', 1)->sum('status');


            DB::commit();
            return $this->response(200, 'Loan Sched', compact('loans', 'loan_payments_references', 'remaining'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function showForPrint($id, $emp_id)
    {
        DB::beginTransaction();
        try {
            $loans_and_ledger = LoansAndLedger::with('user', 'userDetails', 'loanType', 'loanDetails')->where('id', $id)->where('user_id', $emp_id)->first();
            if (!$loans_and_ledger) return $this->response(404, 'Loans And Ledger', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Loans And Ledger', $loans_and_ledger);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
