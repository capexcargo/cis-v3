<?php

namespace App\Repositories\Hrim\CompensationAndBenefits;

use App\Interfaces\Hrim\CompensationAndBenefits\PayrollInterface;
use App\Models\Hrim\PayrollHistory;
use App\Models\Hrim\PayrollStatuses;
use App\Models\UserDetails;
use App\Traits\ResponseTrait;
use Dotenv\Validator;
use Illuminate\Support\Facades\DB;

class PayrollRepository implements PayrollInterface
{
    use ResponseTrait;

    public function show($request)
    {
        DB::beginTransaction();
        try {
            $payrolls = UserDetails::with('user')->get();

            DB::commit();

            return $this->response(200, 'Payroll List', compact('payrolls'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index($request)
    {
        DB::beginTransaction();
        try {
            if ($request['cut_off'] == 1) {
                $begin = date('Y-m-d', strtotime($request['year'] . '-' . ($request['month'] - 1) . '-26'));
                $end = date('Y-m-d', strtotime($request['year'] . '-' . $request['month'] . '-10'));
            } else {
                $begin = date('Y-m-d', strtotime($request['year'] . '-' . ($request['month']) . '-11'));
                $end = date('Y-m-d', strtotime($request['year'] . '-' . $request['month'] . '-25'));
            }

            $where = '';
            if ($request['branch'] != '') {
                $where .= " AND branch_id = '" . $request['branch'] . "'";
            }
            if ($request['employee_id'] != '') {
                $where .= " AND employee_number = '" . $request['employee_id'] . "'";
            }
            if ($request['employee_category'] != '') {
                $where .= " AND employment_category_id = '" . $request['employee_category'] . "'";
            }
            if ($request['employee_name'] != '') {
                $where .= " AND (CONCAT(a.first_name,' ', a.middle_name, ' ', a.last_name) LIKE '%" . $request['employee_name'] . "%') OR 
                (CONCAT(a.first_name,' ', a.last_name) LIKE '%" . $request['employee_name'] . "%') OR(first_name like '%" . $request['employee_name'] . " %' 
                OR middle_name like '% " . $request['employee_name'] . " %' 
                OR last_name like '% " . $request['employee_name'] . " %') ";
            }
            if ($request['employee_id'] != '') {
                $where .= " AND employee_number = '" . $request['employee_id'] . "'";
            }
            if ($request['division'] != '') {
                $where .= " AND a.division_id = '" . $request['division'] . "'";
            }
            if ($request['job_rank'] != '') {
                $where .= " AND a.job_level_id = '" . $request['job_rank'] . "'";
            }

            if ($request['year'] == '' || $request['month'] == '' || $request['cut_off'] == '') {
                $payrolls = [];
            } else {
                // dd($where);
                $payrolls = DB::select('

            SELECT ' . $request['cut_off'] . ' as cut_off, a.user_id, b.photo_path, b.photo_name,h.description as divname, a.employee_number, a.first_name, a.last_name, a.hired_date, c.display AS emp_status, d.display AS job_rank, e.display AS position, f.display AS branch,
            g.basic_pay, g.cola, (g.basic_pay + g.cola) AS gross,ROUND((g.basic_pay / 26) / 8,2) as phour,
            
            (SELECT sum(ot.overtime_approved * (ROUND((g.basic_pay / 26) / 8,2) * bb.rate_percentage)) FROM hrim_overtime ot 
            LEFT JOIN hrim_date_category_reference bb ON bb.id = ot.date_category_id
            WHERE ot.user_id = a.user_id 
            AND ot.date_time_from >= "' . $begin . '" AND ot.date_time_from <= "' . $end . '") AS ot_pay,
            
            (SELECT round(SUM( tl.late * (ROUND((g.basic_pay / 26) / 8,2))), 2) FROM hrim_time_log tl WHERE tl.user_id = a.user_id  
            AND tl.date >= "' . $begin . '" AND tl.date <= "' . $end . '") AS tardiness,
            
            (SELECT SUM( tl.undertime * (ROUND((g.basic_pay / 26) / 8,2))) FROM hrim_time_log tl WHERE tl.user_id = a.user_id  
            AND tl.date >= "' . $begin . '" AND tl.date <= "' . $end . '") AS undertime,
            
            (SELECT SUM(bb.amount) FROM hrim_loan lns
            LEFT JOIN hrim_loan_details bb ON bb.loan_id = lns.id 
            where lns.user_id = a.user_id AND bb.month = "' . $request['month'] . '" AND bb.cutoff = "' . $request['cut_off'] . '" AND bb.year = "' . $request['year'] . '" ) AS loans,

            (SELECT CASE WHEN bb.cutoff = 2 THEN SUM(bb.amount) ELSE 0 END
            FROM hrim_loan lns
            LEFT JOIN hrim_loan_details bb ON bb.loan_id = lns.id 
            where lns.user_id = a.user_id AND bb.month = "' . $request['month'] . '" AND bb.cutoff = "' . $request['cut_off'] . '" AND bb.year = "' . $request['year'] . '" AND lns.`type` IN (2,3,4) AND lns.final_status = 3) as loansgovt,

            (SELECT COUNT(abs.id)  FROM hrim_absent abs WHERE abs.user_id = a.user_id AND 
            abs.absent_date >= "' . $begin . '" AND abs.absent_date <= "' . $end . '") AS absentcount,

            (SELECT round(sum(leaves.apply_for), 2) FROM hrim_leave_details leaves WHERE leaves.user_id = a.user_id AND leaves.leave_type = 0 AND leaves.leave_date >= "' . $begin . '" AND leaves.leave_date <= "' . $end . '") AS leaves,

            (SELECT round(sum(leaves.apply_for), 2) FROM hrim_leave_details leaves WHERE leaves.user_id = a.user_id AND leaves.leave_type = 1 AND leaves.leave_date >= "' . $begin . '" AND leaves.leave_date <= "' . $end . '") AS leaveswtpay,
            (SELECT CASE WHEN g.basic_pay <= 10000 THEN 350
            ELSE g.basic_pay * a.premium_rate END AS asd
            FROM hrim_philhealth a WHERE a.year = 2022) AS philhealth,
            
            (SELECT a.share
            FROM hrim_pagibig a WHERE a.year = 2022) AS pagibig,
           
            (SELECT SUM(htl.rendered_time) * (ROUND((g.basic_pay / 26) / 8,2))
            FROM hrim_rest_day rd 
            LEFT JOIN hrim_time_log htl on htl.user_id = rd.user_id where rd.date >= "' . $begin . '" and rd.date <= "' . $end . '"
            AND rd.user_id = a.user_id AND (case when rd.date = htl.date then 1 END) IS NOT null) as restdaypays,

            (SELECT 

            round(SUM(
            CONCAT(
            HOUR(
            TIMEDIFF( 
            (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) <= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT(htls.time_out_date, " ", htls.time_out)) 
            when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT((CURDATE()), " 06:00:00")) END), (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) then (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) END))) 
            ,".",
            MINUTE(
            TIMEDIFF( 
            (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) <= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT(htls.time_out_date, " ", htls.time_out)) 
            when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT((CURDATE()), " 06:00:00")) END), (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) then (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) END)))))
            * (ROUND((g.basic_pay / 26) / 8,2)), 2)
            
            FROM hrim_time_log htls WHERE htls.user_id = a.user_id AND htls.date >= "' . $begin . '" AND htls.date <= "' . $end . '") as ndval
            ,

            (SELECT 

            round(sum((g.basic_pay / 26) * otr.rate_percentage), 2) 
                        FROM hrim_holiday hl 
                        LEFT JOIN hrim_time_log htl on htl.date = hl.date 
                        LEFT JOIN user_details ud ON ud.user_id = htl.user_id
                        LEFT JOIN hrim_date_category_reference otr ON otr.id = hl.type_id
                     where htl.date >= "' . $begin . '" and htl.date <= "' . $end . '"
                     AND (hl.branch_id = a.branch_id OR hl.branch_id IS NULL) and htl.user_id = a.user_id) as holidayp,

            (SELECT SUM(dl.amount)
                     FROM hrim_dlb dl WHERE 
                     dl.month = "' . $request['month'] . '" 
                     AND dl.payout_cutoff = "' . $request['cut_off'] . '" 
                     AND dl.year = "' . $request['year'] . '"
                     AND dl.user_id = a.user_id) AS dlbpay,

            (SELECT SUM(aad.amount)
                     FROM hrim_admin_adjustment aad WHERE 
                     aad.month = "' . $request['month'] . '" 
                     AND aad.payout_cutoff = "' . $request['cut_off'] . '" 
                     AND aad.year = "' . $request['year'] . '"
                     AND aad.user_id = a.user_id 
                     AND aad.first_status = 3) AS aadpay
            
            
             
            
            FROM user_details a
            LEFT JOIN users b ON b.id = a.user_id
            LEFT JOIN hrim_employment_status c ON c.id = a.employment_status_id
            LEFT JOIN hrim_job_levels d ON d.id = a.job_level_id
            LEFT JOIN hrim_positions e ON e.id = a.position_id
            LEFT JOIN branch_reference f ON f.id = a.branch_id
            LEFT JOIN hrim_earnings g ON g.user_id = a.user_id
            LEFT JOIN division h ON h.id = a.division_id

            WHERE a.user_id is not null ' . $where . '
            and a.employment_category_id is not null
            
            
            order by a.last_name ASC
            ');
            }

            // $payroll = UserDetails::get();
            // dd($benefits_managements);

            DB::commit();

            return $this->response(200, 'Payroll List', compact('payrolls'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index2($id, $request)
    {

        // dd($request, $id);

        DB::beginTransaction();
        try {
            if ($request['cut_off'] == 1) {
                $begin = date('Y-m-d', strtotime($request['year'] . '-' . ($request['month'] - 1) . '-26'));
                $end = date('Y-m-d', strtotime($request['year'] . '-' . $request['month'] . '-10'));
            } else {
                $begin = date('Y-m-d', strtotime($request['year'] . '-' . ($request['month']) . '-11'));
                $end = date('Y-m-d', strtotime($request['year'] . '-' . $request['month'] . '-25'));
            }
            $monthday = date('F Y', strtotime($request['year'] . '-' . $request['month'] . '-25'));

            $where = '';
            if ($request['branch'] != '') {
                $where .= " AND branch_id = '" . $request['branch'] . "'";
            }
            if ($request['employee_id'] != '') {
                $where .= " AND employee_number = '" . $request['employee_id'] . "'";
            }
            if ($request['employee_category'] != '') {
                $where .= " AND employment_category_id = '" . $request['employee_category'] . "'";
            }
            if ($request['employee_name'] != '') {
                $where .= " AND (CONCAT(a.first_name,' ', a.middle_name, ' ', a.last_name) LIKE '%" . $request['employee_name'] . "%') OR 
                (CONCAT(a.first_name,' ', a.last_name) LIKE '%" . $request['employee_name'] . "%') OR(first_name like '%" . $request['employee_name'] . " %' 
                OR middle_name like '% " . $request['employee_name'] . " %' 
                OR last_name like '% " . $request['employee_name'] . " %') ";
            }
            if ($request['employee_id'] != '') {
                $where .= " AND employee_number = '" . $request['employee_id'] . "'";
            }
            if ($request['division'] != '') {
                $where .= " AND a.division_id = '" . $request['division'] . "'";
            }
            if ($request['job_rank'] != '') {
                $where .= " AND a.job_level_id = '" . $request['job_rank'] . "'";
            }

            if ($request['year'] == '' || $request['month'] == '' || $request['cut_off'] == '') {
                $payrolls = [];
            } else {
                // dd($where);
                $payrolls = DB::select('

            SELECT a.user_id, b.photo_path, b.photo_name, a.employee_number, a.first_name, a.last_name, a.hired_date, c.display AS emp_status, d.display AS job_rank, e.display AS position, f.display AS branch,
            g.basic_pay, g.cola, (g.basic_pay + g.cola) AS gross,ROUND((g.basic_pay / 26) / 8,2) as phour,
            
            (SELECT sum(ot.overtime_approved * (ROUND((g.basic_pay / 26) / 8,2) * bb.rate_percentage)) FROM hrim_overtime ot 
            LEFT JOIN hrim_date_category_reference bb ON bb.id = ot.date_category_id
            WHERE ot.user_id = a.user_id 
            AND ot.date_time_from >= "' . $begin . '" AND ot.date_time_from <= "' . $end . '") AS ot_pay,
            
            (SELECT round(SUM( tl.late * (ROUND((g.basic_pay / 26) / 8,2))), 2) FROM hrim_time_log tl WHERE tl.user_id = a.user_id  
            AND tl.date >= "' . $begin . '" AND tl.date <= "' . $end . '") AS tardiness,
            
            (SELECT SUM( tl.undertime * (ROUND((g.basic_pay / 26) / 8,2))) FROM hrim_time_log tl WHERE tl.user_id = a.user_id  
            AND tl.date >= "' . $begin . '" AND tl.date <= "' . $end . '") AS undertime,
            
            (SELECT SUM(bb.amount) FROM hrim_loan lns
            LEFT JOIN hrim_loan_details bb ON bb.loan_id = lns.id 
            where lns.user_id = a.user_id AND bb.month = "' . $request['month'] . '" AND bb.cutoff = "' . $request['cut_off'] . '" AND bb.year = "' . $request['year'] . '" ) AS loans,

            (SELECT CASE WHEN bb.cutoff = 2 THEN SUM(bb.amount) ELSE 0 END
            FROM hrim_loan lns
            LEFT JOIN hrim_loan_details bb ON bb.loan_id = lns.id 
            where lns.user_id = a.user_id AND bb.month = "' . $request['month'] . '" AND bb.cutoff = "' . $request['cut_off'] . '" AND bb.year = "' . $request['year'] . '" AND lns.`type` IN (2,3,4) AND lns.final_status = 3) as loansgovt,

            (SELECT COUNT(abs.id)  FROM hrim_absent abs WHERE abs.user_id = a.user_id AND 
            abs.absent_date >= "' . $begin . '" AND abs.absent_date <= "' . $end . '") AS absentcount,

            (SELECT round(sum(leaves.apply_for), 2) FROM hrim_leave_details leaves WHERE leaves.user_id = a.user_id AND leaves.leave_type = 0 AND leaves.leave_date >= "' . $begin . '" AND leaves.leave_date <= "' . $end . '") AS leaves,

            (SELECT round(sum(leaves.apply_for), 2) FROM hrim_leave_details leaves WHERE leaves.user_id = a.user_id AND leaves.leave_type = 1 AND leaves.leave_date >= "' . $begin . '" AND leaves.leave_date <= "' . $end . '") AS leaveswtpay,

            (SELECT CASE WHEN g.basic_pay <= 10000 THEN 350
            ELSE g.basic_pay * a.premium_rate END AS asd
            FROM hrim_philhealth a WHERE a.year = 2022) AS philhealth,
            
            (SELECT a.share
            FROM hrim_pagibig a WHERE a.year = 2022) AS pagibig,
           
            (SELECT SUM(htl.rendered_time) * (ROUND((g.basic_pay / 26) / 8,2))
            FROM hrim_rest_day rd 
            LEFT JOIN hrim_time_log htl on htl.user_id = rd.user_id where rd.date >= "' . $begin . '" and rd.date <= "' . $end . '"
            AND rd.user_id = a.user_id AND (case when rd.date = htl.date then 1 END) IS NOT null) as restdaypays,

            (SELECT 

            round(SUM(
            CONCAT(
            HOUR(
            TIMEDIFF( 
            (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) <= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT(htls.time_out_date, " ", htls.time_out)) 
            when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT((CURDATE()), " 06:00:00")) END), (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) then (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) END))) 
            ,".",
            MINUTE(
            TIMEDIFF( 
            (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) <= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT(htls.time_out_date, " ", htls.time_out)) 
            when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT((CURDATE()), " 06:00:00")) END), (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) then (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) END)))))
            * (ROUND((g.basic_pay / 26) / 8,2)), 2)
            
            FROM hrim_time_log htls WHERE htls.user_id = a.user_id AND htls.date >= "' . $begin . '" AND htls.date <= "' . $end . '") as ndval
            ,

            (SELECT 

            round(sum((g.basic_pay / 26) * otr.rate_percentage), 2) 
                        FROM hrim_holiday hl 
                        LEFT JOIN hrim_time_log htl on htl.date = hl.date 
                        LEFT JOIN user_details ud ON ud.user_id = htl.user_id
                        LEFT JOIN hrim_date_category_reference otr ON otr.id = hl.type_id
                     where htl.date >= "' . $begin . '" and htl.date <= "' . $end . '"
                     AND (hl.branch_id = a.branch_id OR hl.branch_id IS NULL) and htl.user_id = a.user_id) as holidayp,

            (SELECT SUM(dl.amount)
                     FROM hrim_dlb dl WHERE 
                     dl.month = "' . $request['month'] . '" 
                     AND dl.payout_cutoff = "' . $request['cut_off'] . '" 
                     AND dl.year = "' . $request['year'] . '"
                     AND dl.user_id = a.user_id) AS dlbpay,

            (SELECT SUM(aad.amount)
                     FROM hrim_admin_adjustment aad WHERE 
                     aad.month = "' . $request['month'] . '" 
                     AND aad.payout_cutoff = "' . $request['cut_off'] . '" 
                     AND aad.year = "' . $request['year'] . '"
                     AND aad.user_id = a.user_id 
                     AND aad.first_status = 3) AS aadpay,

                     ' . $request['cut_off'] . ' as cutoffidentify,


                     (SELECT MIN(a.cutoff_date) FROM hrim_cutoff_management a 
                     WHERE a.payout_year = "' . $request['year'] . '" 
                     AND a.payout_month = "' . $request['month'] . '" 
                     AND a.payout_cutoff = "' . $request['cut_off'] . '" AND a.category_id = 3)
                     AS paydatefrom,

                     (SELECT MAX(a.cutoff_date) FROM hrim_cutoff_management a 
                     WHERE a.payout_year = "' . $request['year'] . '" 
                     AND a.payout_month = "' . $request['month'] . '" 
                     AND a.payout_cutoff = "' . $request['cut_off'] . '" AND a.category_id = 3)
                     AS paydateto,

                     h.description as division,
                     "' . ($monthday) . '" as payoutdatemonth,
                     ' . $request['cut_off'] . ' as period,
                     ' . $request['month'] . ' as reqmonth,
                     ' . $request['year'] . ' as reqyear
            
            FROM user_details a
            LEFT JOIN users b ON b.id = a.user_id
            LEFT JOIN hrim_employment_status c ON c.id = a.employment_status_id
            LEFT JOIN hrim_job_levels d ON d.id = a.job_level_id
            LEFT JOIN hrim_positions e ON e.id = a.position_id
            LEFT JOIN branch_reference f ON f.id = a.branch_id
            LEFT JOIN hrim_earnings g ON g.user_id = a.user_id
            LEFT JOIN division h ON h.id = a.division_id

            WHERE a.user_id = "' . $id . '" ' . $where . '
            and a.employment_category_id is not null
            order by a.last_name ASC
            
            ');
            }

            DB::commit();

            return $this->response(200, 'Payroll List', compact('payrolls'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createHistory($selected)
    {
        DB::beginTransaction();
        try {


            $i = 0;
            foreach ($selected as $val) {

                $payroll_close = PayrollHistory::create([
                    'user_id' => 5,
                    'month' => $val,
                    'cutoff' => $val,
                    'year' => $val,
                    'employee_id' => $val,
                    'date_hired' => '2022-09-09',
                    'employment_status' => $val,
                    'job_rank' => $val,
                    'position' => $val,
                    'branch' => $val,
                    'year' => $val,
                ]);
            }

            DB::commit();
            return $this->response(200, "Payroll Successfully Closed", $payroll_close);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function generatePayslipInitial($selected)
    {
        DB::beginTransaction();
        try {

            $i = 0;
            foreach ($selected as $val) {

                $payslip_generate_initial = PayrollStatuses::create([
                    'user_id' => 5,
                    'month' => $val,
                    'cutoff' => $val,
                    'year' => $val,
                    'initial_payslip_status' => 1,
                    'initial_payslip_updated_at' => date("Y-m-d"),
                    'final_payslip_updated_at' => date("Y-m-d"),
                ]);
            }

            DB::commit();
            return $this->response(200, "Initial Payslip Generated", $payslip_generate_initial);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function generatePayslipFinal($selected)
    {
        DB::beginTransaction();
        try {

            $i = 0;
            foreach ($selected as $val) {

                $payslip_generate = PayrollStatuses::create([
                    'user_id' => 5,
                    'month' => $val,
                    'cutoff' => $val,
                    'year' => $val,
                    'final_payslip_status' => 1,
                    'initial_payslip_updated_at' => date("Y-m-d"),
                    'final_payslip_updated_at' => date("Y-m-d"),
                ]);
            }

            DB::commit();
            return $this->response(200, "Final Payslip Generated", $payslip_generate);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
