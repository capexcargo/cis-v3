<?php

namespace App\Repositories\Hrim\Reports;

use App\Interfaces\Hrim\Reports\LoansGovtDeductionInterface;
use App\Models\Hrim\LoanDetails;
use App\Models\Hrim\LoansAndLedger;
use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class LoansGovtDeductionRepository implements LoansGovtDeductionInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $loans_govt_deductions = User::with('userDetails', 'loans')
                ->when($request['employee_name'], function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request['employee_name'] . '%');
                })
                ->when($request['division'], function ($query) use ($request) {
                    $query->where('division_id', $request['division']);
                })
                ->whereHas('userDetails', function ($query) use ($request) {
                    $query->when($request['employee_id'] ?? false, function ($query) use ($request) {
                        $query->where('employee_number', 'like', '%' . ($request['employee_id'] ?? false) . '%');
                    })
                        ->when($request['department'], function ($query) use ($request) {
                            $query->where('department_id', $request['department']);
                        });
                })
                ->whereHas('loans', function ($query) {
                    $query->where('final_status', 3);
                })
                ->withCount([
                    'loans as paid' => function ($query) {
                        $query->whereHas('loanDetails', function ($query) {
                            $query->where('status', 2);
                        });
                    },
                    'loans as remaining' => function ($query) {
                        $query->whereHas('loanDetails', function ($query) {
                            $query->where('status', 1);
                        });
                    }
                ])
                ->withCount([
                    'loans as approved_loans' => function ($query) {
                        $query->where('final_status', 3);
                    }
                ])
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Loans Government Deductions', compact('loans_govt_deductions'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $loans_govt_deductions = LoanDetails::with('loan')
                ->whereHas('loan', function ($query) use ($id) {
                    $query->where('user_id', $id);
                })
                ->get();
            if (!$loans_govt_deductions) return $this->response(404, 'Loans/Government Deduction', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Loans/Government Deduction', $loans_govt_deductions);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
