<?php

namespace App\Repositories\Hrim;

use App\Interfaces\Hrim\TimeInAndOutInterface;
use App\Models\Hrim\Absent;
use App\Models\Hrim\HolidayManagement;
use App\Models\Hrim\ScheduleAdjustment;
use App\Models\Hrim\TimeLog;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class TimeInAndOutRepository implements TimeInAndOutInterface
{
    use ResponseTrait;

    public function getUserTimeLogs($request)
    {
        DB::beginTransaction();
        try {
            $time_logs = TimeLog::with('user')
                ->where('user_id', $request['user_id'])
                ->when($request['date'] ?? false, function ($query) use ($request) {
                    $query->whereMonth('created_at', date('m', strtotime($request['date'])))
                        ->whereYear('date', date('Y', strtotime($request['date'])));
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->get();

            DB::commit();
            return $this->response(200, 'Time Log List', $time_logs);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getWorkSchedule($request)
    {
        DB::beginTransaction();
        try {
            $time_logs = ScheduleAdjustment::with('currentWorkSchedule')
                ->where('user_id', $request['user_id'])
                ->where('final_status_id', 3)
                ->whereMonth('inclusive_date_from', date('m', strtotime($request['date'])))
                // ->WhereMonth('inclusive_date_to', date('m', strtotime($request['date'])))

                // ->when($request['sort_field'], function ($query) use ($request) {
                //     $query->orderBy($request['sort_field'], $request['sort_type']);
                // })
                ->get();



            DB::commit();
            return $this->response(200, 'Time Log List', $time_logs);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getWorkSchedule2($request)
    {
        DB::beginTransaction();
        try {
            $time_logs = userDetails::with('workSchedule')
                ->where('user_id', $request['user_id'])
                ->get();

            DB::commit();
            return $this->response(200, 'Time Log List', $time_logs);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function currentTimelog()
    {
        DB::beginTransaction();
        try {
            $previous_time_log = TimeLog::where('user_id', Auth::user()->id)
                ->where('time_out', null)
                ->orderBy('created_at', 'desc')
                ->first();

            $current_time_log = TimeLog::where('user_id', Auth::user()->id)
                ->when($previous_time_log, function ($query) use ($previous_time_log) {
                    $query->whereDate('date', $previous_time_log->date);
                })
                ->when(!$previous_time_log, function ($query) {
                    $query->whereDate('date', now());
                })
                ->first();

            DB::commit();

            return $this->response(200, 'Time Log', $current_time_log);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function currentTimelog2($id)
    {
        DB::beginTransaction();
        try {
            $previous_time_log = TimeLog::where('user_id', $id)
                ->where('time_out', null)
                ->orderBy('created_at', 'desc')
                ->first();

            $current_time_log = TimeLog::where('user_id', $id)
                ->when($previous_time_log, function ($query) use ($previous_time_log) {
                    $query->whereDate('date', $previous_time_log->date);
                })
                ->when(!$previous_time_log, function ($query) {
                    $query->whereDate('date', now());
                })
                ->first();

            DB::commit();

            return $this->response(200, 'Time Log', $current_time_log);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function timeIn($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'image' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            $validated = $validator->validated();

            $time_log = Auth::user()->timeLog()->where('time_in', '!=', null)->whereDate('date', now())->first();
            if ($time_log) {
                return $this->response(409, 'Duplicate Time In', $time_log);
            }

            $response = $this->storeImage('time_in', $validated['image']);
            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            if (!Auth::user()->userDetails->workSchedule) {
                return $this->response(500, 'Work Schedule', 'Your account is no work schedule yet');
            }
            // $time_logs = Auth::user()->timeLog()->whereDate('date', now())->first();
            $holiday = HolidayManagement::firstWhere('date', date('Y-m-d'));

            $date_category = 1;

            if ($holiday) {
                $date_category = $holiday->date_category_id;
            }

            $work_schedule = Auth::user()->userDetails->workSchedule;
            $work_schedule_time_in = date_create((date('Y-m-d', strtotime(now()))) . ' ' . date('H:i', strtotime('+15 minutes', strtotime($work_schedule->time_from))));
            $time_in_actual = date_create((date('Y-m-d', strtotime(now()))) . ' ' . date('H:i', strtotime(now())));

            $late = null;
            if ($work_schedule_time_in < $time_in_actual) {
                $difference_late = date_diff(date_create($work_schedule->time_from), $time_in_actual);
                $late = $this->computeTime($difference_late->format('%H.%I'));
            }
            $time_log = Auth::user()->timeLog()->create([
                'work_sched_id' => Auth::user()->userDetails->schedule_id,
                'date_category_id' => $date_category,
                'work_mode_id' => Auth::user()->userDetails->workSchedule->work_mode_id,
                'time_in_img_path' => $response['result']['file_path'],
                'time_in_img_name' => $response['result']['file_name'],
                'time_in' => now(),
                'date' => now(),
                'late' => $late,
            ]);

            $get_absence = Absent::where('user_id', Auth::user()->id)
                ->where('absent_date', date("Y-m-d"));

            if ($get_absence) {
                $get_absence->forceDelete();
            }


            DB::commit();

            $time_in_now = date('H:i:s A', strtotime(now()));

            $employee_name = Auth::user()->userDetails->last_name . ', ' . Auth::user()->userDetails->first_name;

            $time_in_message = 'TIME IN CONFIRMED!
            <p style="font-size:16px; margin-top: 0; margin-bottom: 5px;">'
                . $time_in_now
                . '</p><p style="font-size:16px; margin-top: 15px; margin-bottom: 5px;">'
                . $employee_name
                . '<br>ID No. '
                . Auth::user()->userDetails->employee_number
                . '</p>';

            return $this->response(200, $time_in_message, $time_log);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function CheckIfAlreadyTimeOut()
    {
        DB::beginTransaction();
        try {

            $time_log = TimeLog::where('user_id', Auth::user()->id)
                ->whereNotNull('time_out')
                ->whereDate('date', now())
                ->orderBy('created_at', 'desc')
                ->first();

            DB::commit();

            return $this->response(200, 'Time Log', $time_log);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function timeOut($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'image' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            $validated = $validator->validated();

            $response = $this->currentTimelog();
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $time_log = $response['result'];

            if (!$time_log) {
                return $this->response(409, 'You Need To Time In First', $time_log);
            }
            if ($time_log->time_out) {
                return $this->response(409, 'Duplicate Time Out', $time_log);
            }

            $work_schedule = Auth::user()->userDetails->workSchedule;
            $work_schedule_time_in = date_create($time_log->date . ' ' . date('H:i', strtotime('+15 minutes', strtotime($work_schedule->time_from))));
            $work_schedule_time_ins = date_create($time_log->date . ' ' . date('H:i', strtotime($work_schedule->time_from)));
            $work_schedule_time_out = date_create($time_log->date . ' ' . $work_schedule->time_to);

            if ($work_schedule->shift == 'night_shift') {
                $work_schedule_time_out = date_create(date('Y-m-d', strtotime('+1 day', strtotime($time_log->date))) . ' ' . $work_schedule->time_to);
            }

            $work_schedule_break_time_in = date_create($time_log->date . ' ' . $work_schedule->break_from);
            $work_schedule_break_time_out = date_create($time_log->date . ' ' . $work_schedule->break_to);

            $work_schedule_ot_break_time_from = date_create($time_log->date . ' ' . $work_schedule->ot_break_from);
            $work_schedule_ot_break_time_to = date_create($time_log->date . ' ' . $work_schedule->ot_break_to);

            if ($work_schedule->shift == 'night_shift') {
                $work_schedule_ot_break_time_from = date_create(date('Y-m-d', strtotime('+1 day', strtotime($time_log->date))) . ' ' . $work_schedule->ot_break_from);
                $work_schedule_ot_break_time_to = date_create(date('Y-m-d', strtotime('+1 day', strtotime($time_log->date))) . ' ' . $work_schedule->ot_break_to);
            }

            $time_log_time_in = date_create($time_log->date . ' ' . $time_log->time_in);
            $time_log_time_out = date_create(now());
            // $time_log_time_in = date_create(date("H:i", strtotime('18:34')));
            // $time_log_time_out = date_create(date("Y-m-d H:i", strtotime('2022-08-17 05:30')));
            $undertime = null;
            if ($work_schedule_time_out > $time_log_time_out) {
                $difference_undertime = date_diff($work_schedule_time_out, $time_log_time_out);
                $undertime = $this->computeTime(($difference_undertime->format('%H.%I')));
            }

            $late = null;
            if ($work_schedule_time_in < $time_log_time_in) {
                $difference_late = date_diff($work_schedule_time_ins, $time_log_time_in);
                $late = $this->computeTime($difference_late->format('%H.%I'));
            }

            // $ot = null;
            // if ($time_log_time_out > $work_schedule_time_out) {
            //     $before = 0;
            //     $after = 0;

            //     if ($work_schedule_ot_break_time_from > $work_schedule_time_out) {
            //         $difference_before = date_diff($work_schedule_time_out, $work_schedule_ot_break_time_from);
            //         $before = $this->computeTime(($difference_before->format('%H.%I')));
            //     }

            //     if ($work_schedule_ot_break_time_to < $time_log_time_out) {
            //         $difference_after = date_diff($work_schedule_ot_break_time_to, $time_log_time_out);
            //         $after = $this->computeTime(($difference_after->format('%d')* 24) + ($difference_after->format('%H.%I')));
            //     }

            //     $ot = ($before + $after);
            //     // $difference_ot = date_diff($work_schedule_time_out, $time_log_time_out);
            //     // $ot = $this->computeTime(($difference_ot->format('%d')* 24) + ($difference_ot->format('%H.%I')));
            // }

            $ot = null;
            if ($time_log_time_out > $work_schedule_time_out) {
                $before = 0;
                $after = 0;

                if ($work_schedule_ot_break_time_from > $work_schedule_time_out) {
                    if ($time_log_time_out > $work_schedule_ot_break_time_from) {
                        $difference_before = date_diff($work_schedule_time_out, $work_schedule_ot_break_time_from);
                        $before = $this->computeTime(($difference_before->format('%H.%I')));
                    } else {
                        $difference_before = date_diff($work_schedule_time_out, $time_log_time_out);
                        $before = $this->computeTime(($difference_before->format('%H.%I')));
                    }
                }

                if ($work_schedule_ot_break_time_from > $time_log_time_out) {
                    $difference_before = date_diff($work_schedule_ot_break_time_from, $work_schedule_ot_break_time_from);
                    $difference_before2 = date_diff($work_schedule_time_out, $time_log_time_out);
                    $before = $this->computeTime(($difference_before2->format('%H.%I')));
                }

                if ($work_schedule_ot_break_time_to < $time_log_time_out) {
                    $difference_after = date_diff($work_schedule_ot_break_time_to, $time_log_time_out);
                    $after = $this->computeTime(($difference_after->format('%d') * 24) + ($difference_after->format('%H.%I')));
                }

                $ot = ($before + $after);

                $ot = ($ot - $late);
                if ($ot < 0) {
                    $ot = 0;
                }

                // $difference_ot = date_diff($work_schedule_time_out, $time_log_time_out);
                // $ot = $this->computeTime(($difference_ot->format('%d')* 24) + ($difference_ot->format('%H.%I')));
            }

            $moring = 0;
            $afternoon = 0;
            $rendered_time = 0;

            if ($work_schedule_break_time_in > $time_log_time_in) {
                if ($work_schedule_break_time_in > $time_log_time_out) {
                    $difference_morning = date_diff($time_log_time_in, $time_log_time_out);
                    $moring = $this->computeTime(($difference_morning->format('%H.%I')));
                } else {
                    $difference_morning = date_diff($time_log_time_in, $work_schedule_break_time_in);
                    $moring = $this->computeTime(($difference_morning->format('%H.%I')));
                }
            }

            if ($work_schedule_break_time_out < $time_log_time_out) {
                $difference_afternoon = date_diff($work_schedule_break_time_out, $time_log_time_out);
                $afternoon = $this->computeTime(($difference_afternoon->format('%H.%I')));
            }

            $rendered_time = ($moring + $afternoon);

            $response = $this->storeImage('time_out', $validated['image']);
            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            // dd(($rendered_time > 8 ? 8.0 : $rendered_time));

            $time_log = $time_log->update([
                'time_out_img_path' => $response['result']['file_path'],
                'time_out_img_name' => $response['result']['file_name'],
                'time_out' => now(),
                'time_out_date' => now(),
                'rendered_time' => ($rendered_time > 8 ? 8.0 : $rendered_time),
                'late' => $late,
                'undertime' => $undertime,
                'computed_ot' => $ot,
            ]);

            $time_log = Auth::user()->timeLog()->whereDate('date', now())->first();

            DB::commit();

            $time_in_now = date('H:i:s A', strtotime(now()));

            $employee_name = Auth::user()->userDetails->last_name . ', ' . Auth::user()->userDetails->first_name;

            $time_in_message = 'TIME OUT CONFIRMED!
            <p style="font-size:16px; margin-top: 0; margin-bottom: 5px;">'
                . $time_in_now
                . '</p><p style="font-size:16px; margin-top: 15px; margin-bottom: 5px;">'
                . $employee_name
                . '<br>ID No. '
                . Auth::user()->userDetails->employee_number
                . '</p>';

            return $this->response(200, $time_in_message, $time_log);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function storeImage($type, $image)
    {
        DB::beginTransaction();
        try {
            $year = date('Y', strtotime(now()));
            $month = strtolower(date('F', strtotime(now())));
            $day = strtolower(date('d', strtotime(now())));

            $file_path = strtolower(str_replace(' ', '_', 'time_log/' . $type . '/' . $year . '/' . $month . '/' . $day . '/'));
            $file_name = Auth::user()->userDetails->employee_number . '-' . date('mdYHis') . uniqid() . '.jpeg';

            Storage::disk('hrim_gcs')->putFileAs($file_path, $image, $file_name);

            DB::commit();

            return $this->response(200, 'Successfully Save', compact('file_path', 'file_name'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function storeImage2($type, $image, $id)
    {
        DB::beginTransaction();
        try {
            $year = date('Y', strtotime(now()));
            $month = strtolower(date('F', strtotime(now())));
            $day = strtolower(date('d', strtotime(now())));

            $file_path = strtolower(str_replace(' ', '_', 'time_log/' . $type . '/' . $year . '/' . $month . '/' . $day . '/'));
            $file_name = $id . '-' . date('mdYHis') . uniqid() . '.jpeg';

            Storage::disk('hrim_gcs')->putFileAs($file_path, $image, $file_name);

            DB::commit();

            return $this->response(200, 'Successfully Save', compact('file_path', 'file_name'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function timeInAll($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'image' => 'required',
                'employee_id' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            $validated = $validator->validated();

            $user_employee_id = UserDetails::where('employee_number', $request['employee_id'])->first();
            if ($user_employee_id == '') {
                return $this->response(500, 'Error!', "Employee ID No doesn't exist");
            }

            $time_log = User::with('timeLog')
                ->whereHas('timeLog', function ($query) {
                    $query->where('time_in', '!=', null);
                })
                ->whereHas('timeLog', function ($query) {
                    $query->whereDate('date', now());
                })
                ->where('id', $user_employee_id['user_id'])->first();
            if ($time_log) {
                return $this->response(409, 'Duplicate Time In', $time_log);
            }

            $userdetails = UserDetails::with('workSchedule')->where('user_id', $user_employee_id['user_id'])->first();
            // dd($userdetails['schedule_id'],$userdetails['workSchedule']['work_mode_id']);

            $response = $this->storeImage2('time_in', $validated['image'], $request['employee_id']);

            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            if (!$userdetails['workSchedule']) {
                return $this->response(500, 'Work Schedule', 'Your account is no work schedule yet');
            }
            // $time_logs = Auth::user()->timeLog()->whereDate('date', now())->first();
            $holiday = HolidayManagement::firstWhere('date', date('Y-m-d'));

            $date_category = 1;

            if ($holiday) {
                $date_category = $holiday->date_category_id;
            }

            $time_log = TimeLog::create([
                'work_sched_id' => $userdetails['schedule_id'],
                'date_category_id' => $date_category,
                'work_mode_id' => $userdetails['workSchedule']['work_mode_id'],
                'time_in_img_path' => $response['result']['file_path'],
                'time_in_img_name' => $response['result']['file_name'],
                'time_in' => now(),
                'user_id' => $user_employee_id['user_id'],
                'date' => now(),
            ]);

            DB::commit();

            return $this->response(200, 'Successfully Time In', $time_log);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function timeOutAll($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'image' => 'required',
                'employee_id' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            $validated = $validator->validated();

            $user_employee_id = UserDetails::where('employee_number', $request['employee_id'])->first();
            if ($user_employee_id == '') {
                return $this->response(500, 'Error!', "Employee ID No doesn't exist");
            }

            $userdetails = UserDetails::with('workSchedule')->where('user_id', $user_employee_id['user_id'])->first();

            $response = $this->currentTimelog2($user_employee_id['user_id']);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $time_log = $response['result'];

            if (!$time_log) {
                return $this->response(409, 'You Need To Time In First', $time_log);
            }
            if ($time_log->time_out) {
                return $this->response(409, 'Duplicate Time Out', $time_log);
            }

            $work_schedule = $userdetails['workSchedule'];
            $work_schedule_time_in = date_create($time_log->date . ' ' . date('H:i', strtotime('+15 minutes', strtotime($work_schedule->time_from))));
            $work_schedule_time_out = date_create($time_log->date . ' ' . $work_schedule->time_to);

            if ($work_schedule->shift == 'night_shift') {
                $work_schedule_time_out = date_create(date('Y-m-d', strtotime('+1 day', strtotime($time_log->date))) . ' ' . $work_schedule->time_to);
            }

            $work_schedule_break_time_in = date_create($time_log->date . ' ' . $work_schedule->break_from);
            $work_schedule_break_time_out = date_create($time_log->date . ' ' . $work_schedule->break_to);

            $work_schedule_ot_break_time_from = date_create($time_log->date . ' ' . $work_schedule->ot_break_from);
            $work_schedule_ot_break_time_to = date_create($time_log->date . ' ' . $work_schedule->ot_break_to);

            if ($work_schedule->shift == 'night_shift') {
                $work_schedule_ot_break_time_from = date_create(date('Y-m-d', strtotime('+1 day', strtotime($time_log->date))) . ' ' . $work_schedule->ot_break_from);
                $work_schedule_ot_break_time_to = date_create(date('Y-m-d', strtotime('+1 day', strtotime($time_log->date))) . ' ' . $work_schedule->ot_break_to);
            }

            $time_log_time_in = date_create($time_log->date . ' ' . $time_log->time_in);
            $time_log_time_out = date_create(now());
            // $time_log_time_in = date_create(date("H:i", strtotime('18:34')));
            // $time_log_time_out = date_create(date("Y-m-d H:i", strtotime('2022-08-17 05:30')));
            $undertime = null;
            if ($work_schedule_time_out > $time_log_time_out) {
                $difference_undertime = date_diff($work_schedule_time_out, $time_log_time_out);
                $undertime = $this->computeTime(($difference_undertime->format('%H.%I')));
            }

            $late = null;
            if ($work_schedule_time_in < $time_log_time_in) {
                $difference_late = date_diff($work_schedule_time_in, $time_log_time_in);
                $late = $this->computeTime($difference_late->format('%H.%I'));
            }

            // $ot = null;
            // if ($time_log_time_out > $work_schedule_time_out) {
            //     $before = 0;
            //     $after = 0;

            //     if ($work_schedule_ot_break_time_from > $work_schedule_time_out) {
            //         $difference_before = date_diff($work_schedule_time_out, $work_schedule_ot_break_time_from);
            //         $before = $this->computeTime(($difference_before->format('%H.%I')));
            //     }

            //     if ($work_schedule_ot_break_time_to < $time_log_time_out) {
            //         $difference_after = date_diff($work_schedule_ot_break_time_to, $time_log_time_out);
            //         $after = $this->computeTime(($difference_after->format('%d')* 24) + ($difference_after->format('%H.%I')));
            //     }

            //     $ot = ($before + $after);
            //     // $difference_ot = date_diff($work_schedule_time_out, $time_log_time_out);
            //     // $ot = $this->computeTime(($difference_ot->format('%d')* 24) + ($difference_ot->format('%H.%I')));
            // }

            $ot = null;
            if ($time_log_time_out > $work_schedule_time_out) {
                $before = 0;
                $after = 0;

                if ($work_schedule_ot_break_time_from > $work_schedule_time_out) {
                    if ($time_log_time_out > $work_schedule_ot_break_time_from) {
                        $difference_before = date_diff($work_schedule_time_out, $work_schedule_ot_break_time_from);
                        $before = $this->computeTime(($difference_before->format('%H.%I')));
                    } else {
                        $difference_before = date_diff($work_schedule_time_out, $time_log_time_out);
                        $before = $this->computeTime(($difference_before->format('%H.%I')));
                    }
                }

                if ($work_schedule_ot_break_time_from > $time_log_time_out) {
                    $difference_before = date_diff($work_schedule_ot_break_time_from, $work_schedule_ot_break_time_from);
                    $difference_before2 = date_diff($work_schedule_time_out, $time_log_time_out);
                    $before = $this->computeTime(($difference_before2->format('%H.%I')));
                }

                if ($work_schedule_ot_break_time_to < $time_log_time_out) {
                    $difference_after = date_diff($work_schedule_ot_break_time_to, $time_log_time_out);
                    $after = $this->computeTime(($difference_after->format('%d') * 24) + ($difference_after->format('%H.%I')));
                }

                $ot = ($before + $after);

                $ot = ($ot - $late);
                if ($ot < 0) {
                    $ot = 0;
                }

                // $difference_ot = date_diff($work_schedule_time_out, $time_log_time_out);
                // $ot = $this->computeTime(($difference_ot->format('%d')* 24) + ($difference_ot->format('%H.%I')));
            }

            $moring = 0;
            $afternoon = 0;
            $rendered_time = 0;

            if ($work_schedule_break_time_in > $time_log_time_in) {
                $difference_morning = date_diff($time_log_time_in, $work_schedule_break_time_in);
                $moring = $this->computeTime(($difference_morning->format('%H.%I')));
            }

            if ($work_schedule_break_time_out < $time_log_time_out) {
                $difference_afternoon = date_diff($work_schedule_break_time_out, $time_log_time_out);
                $afternoon = $this->computeTime(($difference_afternoon->format('%H.%I')));
            }

            $rendered_time = ($moring + $afternoon);

            $response = $this->storeImage2('time_out', $validated['image'], $request['employee_id']);
            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $time_log = $time_log->update([
                'time_out_img_path' => $response['result']['file_path'],
                'time_out_img_name' => $response['result']['file_name'],
                'time_out' => now(),
                'time_out_date' => now(),
                'rendered_time' => ($rendered_time > 8 ? 8.0 : $rendered_time),
                'late' => $late,
                'undertime' => $undertime,
                'computed_ot' => $ot,
            ]);

            $time_log = User::with('timeLog')
                ->whereHas('timeLog', function ($query) {
                    $query->whereDate('date', now());
                })
                ->where('id', $user_employee_id['user_id'])->first();

            DB::commit();

            return $this->response(200, 'Successfully Time Out', $time_log);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function computeTime($time)
    {
        $time = floatval($time);
        $h = intval($time);
        $m = ((($time - $h) * 100) / 60);

        return round(($h + $m), 2);
    }
}
