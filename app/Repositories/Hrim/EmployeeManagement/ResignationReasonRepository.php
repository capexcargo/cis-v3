<?php

namespace App\Repositories\Hrim\EmployeeManagement;

use App\Interfaces\Hrim\EmployeeManagement\ResignationReasonInterface;
use App\Models\Hrim\ResignationReason;
use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ResignationReasonRepository implements ResignationReasonInterface
{
    use ResponseTrait;

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $user = User::with('resignationReason', 'attachments')
                ->find($id);
            if (!$user) return $this->response(404, 'User', 'Not Found!');

            DB::commit();
            return $this->response(200, 'User', $user);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'date_resigned' => 'required',
                'reason_for_severance' => 'required',
                'reason_for_resignation' => 'required',

                'attachments' => 'nullable',
                'attachments.*.attachment' => 'nullable|' . config('filesystems.validation_all'),
            ], [
                'attachments.*.attachment.mimes' => 'The file must be a file of type: jpg, jpeg, png.'
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateOrCreate($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $response = $this->show($id);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $user = $response['result'];

            $user->resignationReason()->updateOrCreate([], [
                'date_resignation' => $validated['date_resigned'],
                'reason_description' => $validated['reason_for_severance'],
                'reason_reference_id' => $validated['reason_for_resignation'],
            ]);

            $user->update([
                'status_id' => 2
            ]);

            $response = $this->attachments($user, $validated['attachments']);

            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            DB::commit();
            return $this->response(200, ($user->name . ' successfully resigned and deactivated'), $user);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function attachments($user, $attachments)
    {
        DB::beginTransaction();
        try {
            $date = date('Y/m/d', strtotime($user->created_at));

                // dd($attachments);
            foreach ($attachments as $attachment) {

                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $user->attachments()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(" ", "_", 'employee-management/' . $date . '/' . 'resignation/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        Storage::disk('hrim_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } else if (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('hrim_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }
                    $user->attachments()->updateOrCreate(
                        [
                            'id' => $attachment['id']
                        ],
                        [
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                        ]
                    );
                }
            }

            DB::commit();
            return $this->response(200, 'Resignation Attachment Successfully Attached', $user);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
