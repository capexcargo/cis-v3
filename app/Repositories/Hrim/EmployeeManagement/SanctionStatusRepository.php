<?php

namespace App\Repositories\Hrim\EmployeeManagement;

use App\Interfaces\Hrim\EmployeeManagement\SanctionStatusInterface;
use App\Models\Hrim\SanctionStatus;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class SanctionStatusRepository implements SanctionStatusInterface
{
    use ResponseTrait;

    
    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make(
                $request,
                [
                    'display' => 'required',
                ],
                [
                    'display.required' => 'This Sanction Status field is required.',
                ]
            );

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Sanction Status has been successfully added!', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {

            $response = $this->createValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];
            
            $sanction_status = SanctionStatus::create([
                'code' => $validated['display'],
                'display' => $validated['display'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'Sanction Status has been successfully added!', $sanction_status);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $sanction_status = SanctionStatus::findOrFail($id);
            if (!$sanction_status) return $this->response(404, 'Sanction Status', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Sanction Status', $sanction_status);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    
    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make(
                $request,
                [
                    'display' => 'required',
                ],
                [
                    'display.required' => 'This Sanction Status field is required.',
                ]
            );

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Sanction Status Successfully Updated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $sanction_status = $response['result'];
            
            
            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $sanction_status->update([
                'code' => $validated['display'],
                'display' => $validated['display']
            ]);
            DB::commit();
            return $this->response(200, 'Sanction Status Successfully Updated', $sanction_status);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $sanction_status = SanctionStatus::find($id);
            if (!$sanction_status) return $this->response(404, 'Sanction Status Status', 'Not Found!');
            $sanction_status->delete();

            DB::commit();
            return $this->response(200, 'Sanction Status status successfully deleted!', $sanction_status);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
