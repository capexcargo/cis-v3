<?php

namespace App\Repositories\Hrim\EmployeeManagement;

use App\Interfaces\Hrim\EmployeeManagement\DisciplinaryRecordInterface;
use App\Models\Hrim\DisciplinaryRecord;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class DisciplinaryRecordRepository implements DisciplinaryRecordInterface
{
    use ResponseTrait;

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make(
                $request,
                [
                    'display' => 'required',
                ],
                [
                    'display.required' => 'This Disciplinary Record field is required.',
                ]
            );

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Disciplinary Record has been successfully added!', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {

            $response = $this->createValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $disciplinaryRecord = DisciplinaryRecord::create([
                'code' => $validated['display'],
                'display' => $validated['display'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'Disciplinary Record has been successfully added!', $disciplinaryRecord);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $disciplinary_record = DisciplinaryRecord::findOrFail($id);
            if (!$disciplinary_record) return $this->response(404, 'Disciplinary Record', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Disciplinary Record', $disciplinary_record);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make(
                $request,
                [
                    'display' => 'required',
                ],
                [
                    'display.required' => 'This Disciplinary Record field is required.',
                ]
            );

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Disciplinary Record Successfully Updated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $disciplinary_record = $response['result'];
            
            
            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $disciplinary_record->update([
                'code' => $validated['display'],
                'display' => $validated['display']
            ]);
            DB::commit();
            return $this->response(200, 'Disciplinary Record Successfully Updated', $disciplinary_record);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $disciplinaryRecord = DisciplinaryRecord::find($id);
            if (!$disciplinaryRecord) return $this->response(404, 'Disciplinary record', 'Not Found!');
            $disciplinaryRecord->delete();

            DB::commit();
            return $this->response(200, 'Disciplinary record successfully deleted!', $disciplinaryRecord);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
