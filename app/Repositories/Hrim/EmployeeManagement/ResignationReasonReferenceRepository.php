<?php

namespace App\Repositories\Hrim\EmployeeManagement;

use App\Interfaces\Hrim\EmployeeManagement\ResignationReasonReferenceInterface;
use App\Models\Hrim\ResignationReasonReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class ResignationReasonReferenceRepository implements ResignationReasonReferenceInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $reason_reference1 = ResignationReasonReference::
            
                paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Category List', compact('reason_reference1'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                // 'category' => 'required',
                'reason' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            // dd($response);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $resignation_reason = ResignationReasonReference::create([
                'code' => $validated['reason'],
                'display' => $validated['reason'],
            ]);

            DB::commit();

            return $this->response(200, 'Resignation Reason Reference has been Successfully Created!', $resignation_reason);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $resignation_result = $response['result'];

            $response = $this->validation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $resignation_result->update([
                'code' => $validated['reason'],
                'display' => $validated['reason'],
                // 'employment_category_id' => $validated['category'],
            ]);

            DB::commit();

            return $this->response(200, 'Resignation Reason Reference has been successfully updated!', $resignation_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $resignation_result = ResignationReasonReference::findOrFail($id);
            if (!$resignation_result) {
                return $this->response(404, 'Leadership Competencies', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Status Management', $resignation_result);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

         
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $res_rea = ResignationReasonReference::find($id);
            if (!$res_rea) return $this->response(404, 'Resignation Reason Reference', 'Not Found!');
            $res_rea->delete();

            DB::commit();
            return $this->response(200, 'Resignation Reason Reference Successfully Deleted!', $res_rea);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    
}