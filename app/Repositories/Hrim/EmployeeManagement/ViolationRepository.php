<?php

namespace App\Repositories\Hrim\EmployeeManagement;

use App\Interfaces\Hrim\EmployeeManagement\ViolationInterface;
use App\Models\Hrim\Violation;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ViolationRepository implements ViolationInterface
{
    use ResponseTrait;

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make(
                $request,
                [
                    'display' => 'required',
                ],
                [
                    'display.required' => 'This violation field is required.',
                ]
            );

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Violation has been successfully added!', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {

            $response = $this->createValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $violation = Violation::create([
                'code' => $validated['display'],
                'display' => $validated['display'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'Violation has been successfully added!', $violation);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $violation = Violation::findOrFail($id);
            if (!$violation) return $this->response(404, 'Violation', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Violation', $violation);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make(
                $request,
                [
                    'display' => 'required',
                ],
                [
                    'display.required' => 'This Violation field is required.',
                ]
            );

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Violation Successfully Updated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $violation = $response['result'];


            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $violation->update([
                'code' => $validated['display'],
                'display' => $validated['display']
            ]);
            DB::commit();
            return $this->response(200, 'Violation Successfully Updated', $violation);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $violation = Violation::find($id);
            if (!$violation) return $this->response(404, 'Violation', 'Not Found!');
            $violation->delete();

            DB::commit();
            return $this->response(200, 'Violation Successfully Deleted!', $violation);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
