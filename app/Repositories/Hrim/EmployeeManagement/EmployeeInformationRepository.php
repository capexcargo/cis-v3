<?php

namespace App\Repositories\Hrim\EmployeeManagement;

use App\Interfaces\Hrim\EmployeeManagement\EmployeeInformationInterface;
use App\Models\AccountStatusReference;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class EmployeeInformationRepository implements EmployeeInformationInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $employee_information_statuses = AccountStatusReference::leftJoin('users as u', function ($join) use ($request) {
                $join->on('u.status_id', '=', 'account_status_reference.id')
                    ->leftJoin('user_details as ud', function ($join) use ($request) {
                        $join->on('u.id', '=', 'ud.user_id')
                            ->whereIn('ud.employment_category_id', $request['employment_categories']);
                    });
            })
                ->select('account_status_reference.*')
                ->selectRaw('IFNULL(COUNT(ud.user_id), 0) as users_count')
                ->groupBy('account_status_reference.id')
                ->get();

            $employee_informations = UserDetails::with([
                'user' => function ($query) use ($request) {
                    $query->with('status');
                },
                'department',
                'position',
                'employmentCategory',
            ])
                ->when($request['position'] ?? false, function ($query) use ($request) {
                    $query->where('position_id', $request['position']);
                })
                ->when($request['employment_categories'] ?? false, function ($query) use ($request) {
                    $query->whereIn('employment_category_id', $request['employment_categories']);
                })
                ->whereHas('user', function ($query) use ($request) {
                    $query->when($request['employee_name'] ?? false, function ($query) use ($request) {
                        $query->where('name', 'like', '%' . $request['employee_name'] . '%');
                    })
                        ->when($request['status'] ?? false, function ($query) use ($request) {
                            $query->where('status_id', $request['status']);
                        });
                })
                ->when(isset($request['sort_by_position']), function ($query) use ($request) {
                    $query->join('hrim_positions', 'user_details.position_id', '=', 'hrim_positions.id')
                        ->whereNull('hrim_positions.deleted_at')
                        ->orderBy('hrim_positions.display', $request['sort_by_position']);
                })
                ->when(isset($request['sort_by_name']), function ($query) use ($request) {
                    $query->orderBy('last_name', $request['sort_by_name']);
                    // $query->join('users', 'user_details.user_id', '=', 'users.id')
                    //     ->orderBy('users.name', $request['sort_by_name']);
                })
                ->when(isset($request['sort_by_status']), function ($query) use ($request) {
                    $query->join('users', 'user_details.user_id', '=', 'users.id')
                        ->orderBy('users.status_id', $request['sort_by_status']);
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Employee Information List', compact('employee_information_statuses', 'employee_informations'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab1($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'photo' => 'required|' . config('filesystems.validation_image'),
                'employee_id' => 'required|unique:user_details,employee_number',
                'erf_reference' => 'sometimes',
                'branch' => 'required',
                'first_name' => 'required',
                'middle_name' => 'sometimes',
                'last_name' => 'required',
                'position' => 'required',
                'job_level' => 'required',
                'department' => 'required',
                'division' => 'required',
                'employement_category' => 'required',
                'employement_status' => 'required',
                'gender' => 'required',
                'agency' => 'required_if:employement_category,3',
                'date_of_birth' => 'required',
                'date_hired' => 'required',
                'current_address' => 'required',
                'permanent_address' => 'required',
                'personal_mobile_number' => 'required',
                'personal_telephone_number' => 'nullable|numeric|digits_between:7, 10',
                'personal_email_address' => 'required|email:rfc,dns|unique:user_details,personal_email',

                'company_mobile_number' => 'nullable',
                'company_telephone_number' => 'nullable|numeric|digits_between:7, 10',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidationTab2($request)
    {
        DB::beginTransaction();
        try {
            $password_rules = [];
            if ($request['employement_category'] == 3) {
                $password_rules = [
                    'password' => 'required_if:employement_category,3|confirmed|string|min:8|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/',
                    'password_confirmation' => 'required_if:employement_category,3',
                ];
            }

            $validator = Validator::make(
                $request,
                array_merge([
                    'company_email_address' => 'required|email:rfc,dns|unique:users,email',

                    'work_schedule' => 'required',
                    'inclusive_days' => 'sometimes',
                    'time_schedule' => 'sometimes',

                    'sss' => 'sometimes',
                    'pagibig_number' => 'sometimes',
                    'philhealth_number' => 'sometimes',
                    'tin_number' => 'sometimes',

                    'educational_level' => 'required',
                    'educational_institution' => 'required',
                    'educational_inclusive_year_from' => 'required',
                    'educational_inclusive_year_to' => 'required',

                    'employment_employer' => 'sometimes',
                    'employment_position' => 'sometimes',
                    'employment_inclusive_year_from' => 'sometimes',
                    'employment_inclusive_year_to' => 'sometimes',

                    'character_reference_name' => 'sometimes',
                    'character_reference_position' => 'sometimes',
                    'character_reference_company' => 'sometimes',
                    'character_reference_mobile_number' => 'nullable',
                    'character_reference_telephone_number' => 'nullable|numeric|digits_between:7, 10',

                    'emergency_contact_name' => 'required',
                    'emergency_contact_relationship' => 'required',
                    'emergency_contact_address' => 'required',
                    'emergency_contact_mobile_number' => 'required',
                    'emergency_contact_telephone_number' => 'nullable|numeric|digits_between:7, 10',

                    'family_mother_maiden_name' => 'required',
                    'family_father_name' => 'required',
                    'family_siblings' => 'sometimes',
                    'family_siblings.*.name' => 'sometimes',
                    'family_spouse' => 'sometimes',
                    'family_childrens' => 'sometimes',
                    'family_childrens.*.name' => 'sometimes',

                    'earnings_basic_pay' => 'required',
                    'earnings_cola' => 'sometimes',
                    'earnings_gross_pay' => 'sometimes',
                ], $password_rules)
                // , [
                //     'password.regex' => 'Password must be contain at least one uppercase and lowercase letter, one number and one special character',
                // ]
            );

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidationTab1($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }


            $validated1 = $response['result'];
            $response = $this->createValidationTab2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated2 = $response['result'];
            $validated = array_merge($validated1, $validated2);

            $response = $this->storeImage($validated['photo']);
            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $photo = $response['result'];

            // dd($validated['date_of_birth']);

            $user = User::create([
                'role_id' => 1,
                'division_id' => 1,
                'level_id' => 1,
                'photo_path' => $photo['file_path'],
                'photo_name' => $photo['file_name'],
                'name' => $validated['first_name'] . ' ' . $validated['middle_name'] . ' ' . $validated['last_name'],
                'email' => $validated['company_email_address'],
                'email_verification_code' => (str_contains($validated['company_email_address'], 'default_email') ? uniqid() : null),
                'password' => Hash::make('123456'),
            ]);

            if ($validated['password'] ?? false) {
                $user->update([
                    'password' => Hash::make($validated['password']),
                ]);
            }

            $user->userDetails()->create([
                'erf_reference_no' => $validated['erf_reference'],
                'agency_id' => $validated['agency'],
                'branch_id' => $validated['branch'],
                'schedule_id' => $validated['work_schedule'],
                'employee_number' => $validated['employee_id'],
                'first_name' => $validated['first_name'],
                'middle_name' => $validated['middle_name'],
                'last_name' => $validated['last_name'],
                'position_id' => $validated['position'],
                'job_level_id' => $validated['job_level'],
                'department_id' => $validated['department'],
                'division_id' => $validated['division'],
                'employment_category_id' => $validated['employement_category'],
                'employment_status_id' => $validated['employement_status'],
                'gender_id' => $validated['gender'],
                'company_email' => $validated['company_email_address'],
                'age' => $this->getAge($validated['date_of_birth']),
                'birth_date' => $validated['date_of_birth'],
                'hired_date' => $validated['date_hired'],
                'personal_mobile_number' => $validated['personal_mobile_number'],
                'personal_telephone_number' => $validated['personal_telephone_number'],
                'personal_email' => $validated['personal_email_address'],
                'company_mobile_number' => $validated['company_mobile_number'],
                'company_telephone_number' => $validated['company_telephone_number'],
                // 'current_country_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'current_region_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'current_province_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'current_city_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'current_barangay_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'current_street' => $validated['aaaaaaaaaaaaaaaaaa'],
                'current_address' => $validated['current_address'],
                // 'permanent_country_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'permanent_region_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'permanent_province_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'permanent_city_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'permanent_barangay_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'permanent_street' => $validated['aaaaaaaaaaaaaaaaaa'],
                'permanent_address' => $validated['permanent_address'],
            ]);

            $user->governmentInformation()->create([
                'sss_no' => $validated['sss'],
                'pagibig_no' => $validated['pagibig_number'],
                'philhealth_no' => $validated['philhealth_number'],
                'tin_no' => $validated['tin_number'],
            ]);

            $user->edutcationalAttainment()->create([
                'ea_level_id' => $validated['educational_level'],
                'educational_institution' => $validated['educational_institution'],
                'inclusive_year_from' => $validated['educational_inclusive_year_from'],
                'inclusive_year_to' => $validated['educational_inclusive_year_to'],
            ]);

            $user->employmentRecord()->create([
                'employer' => $validated['employment_employer'],
                'position' => $validated['employment_position'],
                'inclusive_year_from' => $validated['employment_inclusive_year_from'],
                'inclusive_year_to' => $validated['employment_inclusive_year_to'],
            ]);

            $user->characterReference()->create([
                'name' => $validated['character_reference_name'],
                'position' => $validated['character_reference_position'],
                'company' => $validated['character_reference_company'],
                'mobile_number' => $validated['character_reference_mobile_number'],
                'telephone_number' => $validated['character_reference_telephone_number'],
            ]);

            $user->emergencyContactDetails()->create([
                'name' => $validated['emergency_contact_name'],
                'ecd_relationship_id' => $validated['emergency_contact_relationship'],
                'address' => $validated['emergency_contact_address'],
                'mobile_number' => $validated['emergency_contact_mobile_number'],
                'telephone_number' => $validated['emergency_contact_telephone_number'],
            ]);

            $family_information = $user->familyInformation()->create([
                'fathers_name' => $validated['family_father_name'],
                'mothers_maiden_name' => $validated['family_mother_maiden_name'],
                'spouse' => $validated['family_spouse'],
            ]);

            foreach ($validated['family_siblings'] as $family_sibling) {
                if (!empty($family_sibling['name'])) {
                    $family_information->siblings()->create([
                        'name' => $family_sibling['name'],
                    ]);
                }
            }

            foreach ($validated['family_childrens'] as $family_children) {
                if (!empty($family_children['name'])) {
                    $family_information->childrens()->create([
                        'name' => $family_children['name'],
                    ]);
                }
            }

            $user->earnings()->create([
                'basic_pay' => $validated['earnings_basic_pay'],
                'cola' => $validated['earnings_cola'],
                'gross_pay' => $validated['earnings_gross_pay'],
            ]);

            DB::commit();

            return $this->response(200, 'New employee has been successfully added', $user);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $user = User::with([
                'userDetails' => function ($query) {
                    $query->with(
                        'branch',
                        'workSchedule',
                        'position',
                        'jobLevel',
                        'department',
                        'division',
                        'employmentCategory',
                        'employmentStatus',
                        'gender',
                        'suffix',
                    );
                },
                'governmentInformation',
                'edutcationalAttainment' => function ($query) {
                    $query->with('level');
                },
                'employmentRecord',
                'characterReference',
                'emergencyContactDetails' => function ($query) {
                    $query->with('relationship');
                },
                'familyInformation' => function ($query) {
                    $query->with(
                        'siblings',
                        'childrens',
                    );
                },
                'earnings',
            ])
                ->find($id);
            if (!$user) {
                return $this->response(404, 'User', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'User', $user);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidationTab1($user, $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'photo' => 'nullable|' . config('filesystems.validation_image'),
                'employee_id' => 'required|unique:user_details,employee_number,' . $user->userDetails->id . ',id',
                'branch' => 'required',
                'erf_reference' => 'sometimes',
                'first_name' => 'required',
                'middle_name' => 'sometimes',
                'last_name' => 'required',
                'position' => 'required',
                'job_level' => 'required',
                'department' => 'required',
                'division' => 'required',
                'employement_category' => 'required',
                'employement_status' => 'required',
                'gender' => 'required',
                'agency' => 'required_if:employement_category,3',
                'date_of_birth' => 'required',
                'date_hired' => 'required',
                'current_address' => 'required',
                'permanent_address' => 'required',
                'personal_mobile_number' => 'required',
                'personal_telephone_number' => 'nullable|numeric|digits_between:7, 10',
                'personal_email_address' => 'required|email:rfc,dns|unique:user_details,personal_email,' . $user->userDetails->id . ',id',

                'company_mobile_number' => 'nullable',
                'company_telephone_number' => 'nullable|numeric|digits_between:7, 10',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidationTab2($user, $request)
    {
        DB::beginTransaction();
        try {
            $password_rules = [];
            if ($request['password'] && $request['employement_category'] == 3) {
                $password_rules = [
                    'password' => 'required_if:employement_category,3|confirmed|string|min:8|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/',
                    'password_confirmation' => 'required_if:employement_category,3',
                ];
            }

            $validator = Validator::make($request, array_merge([
                'company_email_address' => 'required|email:rfc,dns|unique:users,email,' . $user->id . ',id',

                'work_schedule' => 'required',
                'inclusive_days' => 'sometimes',
                'time_schedule' => 'sometimes',

                'sss' => 'sometimes',
                'pagibig_number' => 'sometimes',
                'philhealth_number' => 'sometimes',
                'tin_number' => 'sometimes',

                'educational_level' => 'required',
                'educational_institution' => 'required',
                'educational_inclusive_year_from' => 'required',
                'educational_inclusive_year_to' => 'required',

                'employment_employer' => 'sometimes',
                'employment_position' => 'sometimes',
                'employment_inclusive_year_from' => 'sometimes',
                'employment_inclusive_year_to' => 'sometimes',

                'character_reference_name' => 'sometimes',
                'character_reference_position' => 'sometimes',
                'character_reference_company' => 'sometimes',
                'character_reference_mobile_number' => 'nullable',
                'character_reference_telephone_number' => 'nullable|numeric|digits_between:7, 10',

                'emergency_contact_name' => 'required',
                'emergency_contact_relationship' => 'required',
                'emergency_contact_address' => 'required',
                'emergency_contact_mobile_number' => 'required',
                'emergency_contact_telephone_number' => 'nullable|numeric|digits_between:7, 10',

                'family_mother_maiden_name' => 'required',
                'family_father_name' => 'required',
                'family_siblings' => 'sometimes',
                'family_siblings.*.name' => 'sometimes',
                'family_spouse' => 'sometimes',
                'family_childrens' => 'sometimes',
                'family_childrens.*.name' => 'sometimes',

                'earnings_basic_pay' => 'required',
                'earnings_cola' => 'sometimes',
                'earnings_gross_pay' => 'sometimes',
            ], $password_rules));

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $user = $response['result'];

            $response = $this->updateValidationTab1($user, $request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated1 = $response['result'];
            $response = $this->updateValidationTab2($user, $request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated2 = $response['result'];
            $validated = array_merge($validated1, $validated2);



            if ($validated['photo']) {
                $response = $this->storeImage($validated['photo']);
                if ($response['code'] == 500) {
                    return $this->response($response['code'], $response['message'], $response['result']);
                }
                $photo = $response['result'];
                $user->update([
                    'photo_path' => $photo['file_path'],
                    'photo_name' => $photo['file_name'],
                ]);
            }

            $user->update([
                'role_id' => 1,
                'division_id' => 1,
                'level_id' => 1,
                'name' => $validated['first_name'] . ' ' . $validated['middle_name'] . ' ' . $validated['last_name'],
                'email' => $validated['company_email_address'],
            ]);

            if ($validated['password'] ?? false) {
                $user->update([
                    'password' => Hash::make($validated['password']),
                ]);
            }

            $user->userDetails()->update([
                'agency_id' => $validated['agency'],
                'branch_id' => $validated['branch'],
                'schedule_id' => $validated['work_schedule'],
                'erf_reference_no' => $validated['erf_reference'],
                'employee_number' => $validated['employee_id'],
                'first_name' => $validated['first_name'],
                'middle_name' => $validated['middle_name'],
                'last_name' => $validated['last_name'],
                'position_id' => $validated['position'],
                'job_level_id' => $validated['job_level'],
                'department_id' => $validated['department'],
                'division_id' => $validated['division'],
                'employment_category_id' => $validated['employement_category'],
                'employment_status_id' => $validated['employement_status'],
                'gender_id' => $validated['gender'],
                'company_email' => $validated['company_email_address'],
                'age' => $this->getAge($validated['date_of_birth']),
                'birth_date' => $validated['date_of_birth'],
                'hired_date' => $validated['date_hired'],
                'personal_mobile_number' => $validated['personal_mobile_number'],
                'personal_telephone_number' => $validated['personal_telephone_number'],
                'personal_email' => $validated['personal_email_address'],
                'company_mobile_number' => $validated['company_mobile_number'],
                'company_telephone_number' => $validated['company_telephone_number'],
                // 'current_country_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'current_region_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'current_province_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'current_city_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'current_barangay_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'current_street' => $validated['aaaaaaaaaaaaaaaaaa'],
                'current_address' => $validated['current_address'],
                // 'permanent_country_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'permanent_region_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'permanent_province_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'permanent_city_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'permanent_barangay_code' => $validated['aaaaaaaaaaaaaaaaaa'],
                // 'permanent_street' => $validated['aaaaaaaaaaaaaaaaaa'],
                'permanent_address' => $validated['permanent_address'],
            ]);

            $user->governmentInformation()->updateOrCreate(
                [
                    'user_id' => $user->id,
                ],
                [
                    'sss_no' => $validated['sss'],
                    'pagibig_no' => $validated['pagibig_number'],
                    'philhealth_no' => $validated['philhealth_number'],
                    'tin_no' => $validated['tin_number'],
                ]
            );

            $user->edutcationalAttainment()->updateOrCreate(
                [
                    'user_id' => $user->id,
                ],
                [
                    'ea_level_id' => $validated['educational_level'],
                    'educational_institution' => $validated['educational_institution'],
                    'inclusive_year_from' => $validated['educational_inclusive_year_from'],
                    'inclusive_year_to' => $validated['educational_inclusive_year_to'],
                ]
            );

            $user->employmentRecord()->updateOrCreate(
                [
                    'user_id' => $user->id,
                ],
                [
                    'employer' => $validated['employment_employer'],
                    'position' => $validated['employment_position'],
                    'inclusive_year_from' => $validated['employment_inclusive_year_from'],
                    'inclusive_year_to' => $validated['employment_inclusive_year_to'],
                ]
            );

            $user->characterReference()->updateOrCreate(
                [
                    'user_id' => $user->id,
                ],
                [
                    'name' => $validated['character_reference_name'],
                    'position' => $validated['character_reference_position'],
                    'company' => $validated['character_reference_company'],
                    'mobile_number' => $validated['character_reference_mobile_number'],
                    'telephone_number' => $validated['character_reference_telephone_number'],
                ]
            );

            $user->emergencyContactDetails()->updateOrCreate(
                [
                    'user_id' => $user->id,
                ],
                [
                    'name' => $validated['emergency_contact_name'],
                    'ecd_relationship_id' => $validated['emergency_contact_relationship'],
                    'address' => $validated['emergency_contact_address'],
                    'mobile_number' => $validated['emergency_contact_mobile_number'],
                    'telephone_number' => $validated['emergency_contact_telephone_number'],
                ]
            );

            $family_information = $user->familyInformation()->updateOrCreate(
                [
                    'user_id' => $user->id,
                ],
                [
                    'fathers_name' => $validated['family_father_name'],
                    'mothers_maiden_name' => $validated['family_mother_maiden_name'],
                    'spouse' => $validated['family_spouse'],
                ]
            );

            foreach ($validated['family_siblings'] as $family_sibling) {
                if ($family_sibling['id'] && $family_sibling['is_deleted']) {
                    $sibling = $family_information->siblings()->find($family_sibling['id']);
                    if ($sibling) {
                        $sibling->delete();
                    }
                } else {
                    if (!empty($family_sibling['name'])) {
                        $family_information->siblings()->updateOrCreate([
                            'id' => $family_sibling['id'],
                        ], [
                            'name' => $family_sibling['name'],
                        ]);
                    } else {
                        if (isset($family_sibling['id'])) {
                            $sibling = $family_information->siblings()->find($family_sibling['id']);
                            $sibling->delete();
                        }
                    }
                }
            }

            foreach ($validated['family_childrens'] as $family_children) {
                if ($family_children['id'] && $family_children['is_deleted']) {
                    $children = $family_information->childrens()->find($family_children['id']);
                    if ($children) {
                        $children->delete();
                    }
                } else {
                    if (!empty($family_children['name'])) {
                        $family_information->childrens()->updateOrCreate([
                            'id' => $family_children['id'],
                        ], [
                            'name' => $family_children['name'],
                        ]);
                    } else {
                        if (isset($family_children['id'])) {
                            $children = $family_information->childrens()->find($family_children['id']);
                            $children->delete();
                        }
                    }
                }
            }

            $user->earnings()->updateOrCreate(
                [
                    'user_id' => $user->id,
                ],
                [
                    'basic_pay' => $validated['earnings_basic_pay'],
                    'cola' => $validated['earnings_cola'],
                    'gross_pay' => $validated['earnings_gross_pay'],
                ]
            );

            DB::commit();

            return $this->response(200, 'Employee information has been successfully updated', $user);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateStatus($id, $status_id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $user = $response['result'];
            $user->update([
                'status_id' => $status_id,
            ]);

            DB::commit();

            return $this->response(200, 'Employee has been successfully updated status', $user);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getAge($date)
    {
        $age = date_diff(date_create($date), date_create('now'))->y;

        return $age;
    }

    public function storeImage($image)
    {
        DB::beginTransaction();
        try {
            $year = date('Y', strtotime(now()));

            $file_path = strtolower(str_replace(' ', '_', 'employee_management/employee_information/' . $year . '/'));
            $file_name = Auth::user()->userDetails->employee_number . '-' . date('mdYHis') . uniqid() . '.jpeg';

            $image_resize = Image::make($image->getRealPath())->resize(1024, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            Storage::disk('hrim_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
            DB::commit();

            return $this->response(200, 'Successfuly Save', compact('file_path', 'file_name'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
