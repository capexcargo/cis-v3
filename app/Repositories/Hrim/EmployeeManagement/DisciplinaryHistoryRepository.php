<?php

namespace App\Repositories\Hrim\EmployeeManagement;

use App\Interfaces\Hrim\EmployeeManagement\DisciplinaryHistoryInterface;
use App\Models\Hrim\DisciplinaryHistory;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class DisciplinaryHistoryRepository implements DisciplinaryHistoryInterface
{
    use ResponseTrait;

    public function show($id)
    {
        $disciplinaryHistory = DisciplinaryHistory::with('userDetails', 'attachments')->find($id);

        if (!$disciplinaryHistory) return $this->response(404, 'Disciplinary History', 'Not Found!');

        return $this->response(200, 'Disciplinary History', $disciplinaryHistory);
    }

    public function create($validated)
    {
        DB::beginTransaction();
        try {
            $disciplinaryHistory = DisciplinaryHistory::create([
                'incident_date' => $validated['incident_date'],
                'incident_time' => $validated['incident_time'],
                'description' => $validated['description'],
                'user_id' => $validated['employee_name'],
                'incident_branch' => $validated['incident_location'],
                'coc_section_id' => $validated['coc_section'],
                'violation_id' => $validated['violation'],
                'disciplinary_record_id' => $validated['disciplinary_record'],
                'sanction_id' => $validated['sanction'],
                'sanction_status_id' => $validated['sanction_status'],
                'created_by' => Auth::user()->id,
            ]);

            $response = $this->attachments($disciplinaryHistory, $validated['attachments']);

            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            DB::commit();
            return $this->response(200, 'Disciplinary history has been successfully filed!', $disciplinaryHistory);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update(DisciplinaryHistory $disciplinaryHistory, $validated)
    {
        DB::beginTransaction();
        try {

            $disciplinaryHistory->update([
                'incident_date' => $validated['incident_date'],
                'incident_time' => $validated['incident_time'],
                'description' => $validated['description'],
                'user_id' => $validated['employee_name'],
                'incident_branch' => $validated['incident_location'],
                'coc_section_id' => $validated['coc_section'],
                'violation_id' => $validated['violation'],
                'disciplinary_record_id' => $validated['disciplinary_record'],
                'sanction_id' => $validated['sanction'],
                'sanction_status_id' => $validated['sanction_status'],
                'created_by' => Auth::user()->id,
            ]);

            $response = $this->attachments($disciplinaryHistory, $validated['attachments']);

            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            DB::commit();
            return $this->response(200, 'Disciplinary Record Successfully Updated!', $disciplinaryHistory);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function attachments($disciplinaryHistory, $attachments)
    {
        DB::beginTransaction();
        try {
            $date = date('Y/m/d', strtotime($disciplinaryHistory->created_at));

            foreach ($attachments as $attachment) {

                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $disciplinaryHistory->attachments()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(" ", "_", 'employee-management/' . $date . '/' . 'disciplinary-history/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        Storage::disk('hrim_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } else if (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('hrim_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }
                    $disciplinaryHistory->attachments()->updateOrCreate(
                        [
                            'id' => $attachment['id']
                        ],
                        [
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                        ]
                    );
                }
            }

            DB::commit();
            return $this->response(200, 'Disciplinary History Attachment Successfully Attached', $disciplinaryHistory);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $disciplinaryHistory = DisciplinaryHistory::find($id);
            if (!$disciplinaryHistory) return $this->response(404, 'Disciplinary history', 'Not Found!');
            $disciplinaryHistory->delete();

            DB::commit();
            return $this->response(200, 'Disciplinary history successfully deleted!', $disciplinaryHistory);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
