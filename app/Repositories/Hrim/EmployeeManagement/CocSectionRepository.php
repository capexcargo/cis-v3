<?php

namespace App\Repositories\Hrim\EmployeeManagement;

use App\Interfaces\Hrim\EmployeeManagement\CocSectionInterface;
use App\Models\Hrim\CocSection;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class CocSectionRepository implements CocSectionInterface
{
    use ResponseTrait;

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make(
                $request,
                [
                    'display' => 'required',
                ],
                [
                    'display.required' => 'This COC Section field is required.',
                ]
            );

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'COC Section has been successfully added!', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {

            $response = $this->createValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $coc_section = CocSection::create([
                'code' => $validated['display'],
                'display' => $validated['display'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'COC Section has been successfully added!', $coc_section);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $coc_section = CocSection::findOrFail($id);
            if (!$coc_section) return $this->response(404, 'COC Section', 'Not Found!');

            DB::commit();
            return $this->response(200, 'COC Section', $coc_section);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make(
                $request,
                [
                    'display' => 'required',
                ],
                [
                    'display.required' => 'This COC Section field is required.',
                ]
            );

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'COC Section Successfully Updated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $coc_section = $response['result'];
            
            
            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $coc_section->update([
                'code' => $validated['display'],
                'display' => $validated['display']
            ]);
            DB::commit();
            return $this->response(200, 'COC Section Successfully Updated', $coc_section);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $coc_section = CocSection::find($id);
            if (!$coc_section) return $this->response(404, 'COC Section', 'Not Found!');
            $coc_section->delete();

            DB::commit();
            return $this->response(200, 'COC Section Successfully Deleted!', $coc_section);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
