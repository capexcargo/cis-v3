<?php

namespace App\Repositories\Hrim\EmployeeManagement;

use App\Interfaces\Hrim\EmployeeManagement\Hrim201FilesInterface;
use App\Models\Hrim\CocSection;
use App\Models\Hrim\Hrim201Files;
use App\Models\Hrim\Hrim201FilesAttachments;
use App\Models\Hrim\Hrim201FilesReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class Hrim201FilesRepository implements Hrim201FilesInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $hrim_201_files_references_model = Hrim201FilesReference::with(['hrim201Files' => function ($query) use ($request) {
                $query->with('attachments')->where('user_id', $request['user_id']);
            }])->where('is_visible', 1)->get();
            $hrim_201_files_references = [];
            foreach ($hrim_201_files_references_model as $hrim_201_files_reference) {
                $hrim_201_files_references[] = [
                    'id' => $hrim_201_files_reference->id,
                    'display' => $hrim_201_files_reference->display,
                    'date_updated' => ($hrim_201_files_reference->hrim201Files->isNotEmpty() ? date('M. d, Y', strtotime($hrim_201_files_reference->hrim201Files[0]->updated_at)) : ''),
                    'status' => ($hrim_201_files_reference->hrim201Files->isNotEmpty() ? ($hrim_201_files_reference->hrim201Files[0]->attachments->isNotEmpty() ? true : false) : false),
                    'file' => '',
                    'is_deleted' => false,
                ];
            }

            DB::commit();
            return $this->response(200, 'Hrim 201 Files List', $hrim_201_files_references);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function disciplinaryIndex($request)
    {
        DB::beginTransaction();
        try {
            $disciplinaries = CocSection::withCount([
                'disciplinaryHistories as violations_count' => function ($query) use ($request) {
                    $query->where('user_id', $request['user_id']);
                }
            ])->where('is_visible', 1)->get();

            DB::commit();
            return $this->response(200, 'Disciplinary List', $disciplinaries);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'user_id' => 'required',

                'hrim_201_files_references.*.id' => 'sometimes',
                'hrim_201_files_references.*.display' => 'sometimes',
                'hrim_201_files_references.*.date_updated' => 'sometimes',
                'hrim_201_files_references.*.status' => 'sometimes',
                'hrim_201_files_references.*.file' => 'sometimes|' .  config('filesystems.validation_image'),
                'hrim_201_files_references.*.is_deleted' => 'sometimes',
            ], [
                'hrim_201_files_references.*.file.mimes' => 'The file must be a file of type: jpg, jpeg, png.'
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {

            $response = $this->createValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];


            $hrim_201_files_references = array_filter($validated['hrim_201_files_references'], function ($hrim_201_files_reference) {
                return $hrim_201_files_reference['file'];
            });

            foreach ($hrim_201_files_references as $hrim_201_files_reference) {
                $hrim_201_files = Hrim201Files::updateOrCreate([
                    'user_id' => $validated['user_id'],
                    'hrim_201_reference_id' => $hrim_201_files_reference['id'],
                ], [
                    'created_by' => Auth::user()->id,
                ]);

                $response = $this->storeImage($hrim_201_files_reference['file']);
                if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
                $photo = $response['result'];

                $hrim_201_files->attachments()->create([
                    'path' => $photo['file_path'],
                    'name' => $photo['file_name'],
                    'extension' => $hrim_201_files_reference['file']->extension(),
                ]);
            }

            DB::commit();
            return $this->response(200, '201 Files Successfully Created', $hrim_201_files_references);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id, $user_id, $request = [])
    {
        DB::beginTransaction();
        try {
            $hrim_201_files = Hrim201Files::with(['hrim201Reference', 'attachments' => function ($query) {
                $query->when(Auth::user()->accesses()->contains('hrim_calendar_delete'), function ($query) {
                    $query->withTrashed();
                });
            }])
                ->where([['user_id', $user_id], ['hrim_201_reference_id', $id]])
                ->first();

            if (!$hrim_201_files) return $this->response(404, 'Hrim 201 Files', 'Not Found!');
            DB::commit();
            return $this->response(200, 'Hrim 201 Files', $hrim_201_files);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function showAttachment($id, $request = [])
    {
        DB::beginTransaction();
        try {
            $hrim_201_files_attachment = Hrim201FilesAttachments::with('hrim201Files')
                ->when(Auth::user()->accesses()->contains('hrim_calendar_delete'), function ($query) {
                    $query->withTrashed();
                })
                ->find($id);
            if (!$hrim_201_files_attachment) return $this->response(404, 'Hrim 201 Files', 'Not Found!');
            DB::commit();
            return $this->response(200, 'Hrim 201 Files Attachment', $hrim_201_files_attachment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id, $user_id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id, $user_id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $hrim_201_files = $response['result'];
            foreach ($hrim_201_files->attachments as $attachment) {
                $attachment->forceDelete();
            }

            DB::commit();
            return $this->response(200, 'Hrim 201 Files Successfully Deleted All Files', $hrim_201_files);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function restore($id, $user_id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id, $user_id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $hrim_201_files = $response['result'];
            foreach ($hrim_201_files->attachments as $attachment) {
                $attachment->restore();
            }

            DB::commit();
            return $this->response(200, 'Hrim 201 Files Successfully Restored All Files', $hrim_201_files);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroyAttachment($id)
    {
        DB::beginTransaction();
        try {
            $response = $this->showAttachment($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $hrim_201_files_attachment = $response['result'];
            $hrim_201_files_attachment->forceDelete();

            DB::commit();
            return $this->response(200, 'Hrim 201 Files Attachment Successfully Deleted', $hrim_201_files_attachment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function restoreAttachment($id)
    {
        DB::beginTransaction();
        try {
            $response = $this->showAttachment($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $hrim_201_files_attachment = $response['result'];
            $hrim_201_files_attachment->restore();

            DB::commit();
            return $this->response(200, 'Hrim 201 Files Attachment Successfully Restored', $hrim_201_files_attachment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function storeImage($image)
    {
        DB::beginTransaction();
        try {
            $year = date('Y', strtotime(now()));
            $file_path = strtolower(str_replace(" ", "_", 'employee_management/employee_information/hrim_201_files/' . $year . '/'));
            $file_name = date('mdYHis') . uniqid() . '.' . $image->extension();

            $image_resize = Image::make($image->getRealPath())->resize(1024, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            Storage::disk('hrim_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
            DB::commit();
            return $this->response(200, 'Successfuly Save', compact('file_path', 'file_name'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
