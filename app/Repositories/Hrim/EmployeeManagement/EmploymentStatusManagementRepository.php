<?php

namespace App\Repositories\Hrim\EmployeeManagement;

use App\Interfaces\Hrim\EmployeeManagement\EmploymentStatusManagementInterface;
use App\Models\Hrim\EmploymentStatus;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class EmploymentStatusManagementRepository implements EmploymentStatusManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $employment_status1 = EmploymentStatus::
            
                paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Category List', compact('employment_status1'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                // 'category' => 'required',
                'status' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            // dd($response);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $employment_status = EmploymentStatus::create([
                'code' => $validated['status'],
                'display' => $validated['status'],
            ]);

            DB::commit();

            return $this->response(200, 'Employment Status has been Successfully Created!', $employment_status);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $employment_status = $response['result'];

            $response = $this->validation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $employment_status->update([
                'code' => $validated['status'],
                'display' => $validated['status'],
                // 'employment_category_id' => $validated['category'],
            ]);

            DB::commit();

            return $this->response(200, 'Employment Status has been successfully updated!', $employment_status);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $employment_status = EmploymentStatus::findOrFail($id);
            if (!$employment_status) {
                return $this->response(404, 'Leadership Competencies', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Status Management', $employment_status);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

         
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $emp_stat = EmploymentStatus::find($id);
            if (!$emp_stat) return $this->response(404, 'Employment status', 'Not Found!');
            $emp_stat->delete();

            DB::commit();
            return $this->response(200, 'Employment status Successfully Deleted!', $emp_stat);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
