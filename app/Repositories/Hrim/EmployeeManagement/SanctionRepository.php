<?php

namespace App\Repositories\Hrim\EmployeeManagement;

use App\Interfaces\Hrim\EmployeeManagement\SanctionInterface;
use App\Models\Hrim\Sanction;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class SanctionRepository implements SanctionInterface
{
    use ResponseTrait;

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make(
                $request,
                [
                    'display' => 'required',
                ],
                [
                    'display.required' => 'This Sanction field is required.',
                ]
            );

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Sanction has been successfully added!', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {

            $response = $this->createValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];
            
            $sanction = Sanction::create([
                'code' => $validated['display'],
                'display' => $validated['display'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'Sanction has been successfully added!', $sanction);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $sanction = Sanction::findOrFail($id);
            if (!$sanction) return $this->response(404, 'Sanction', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Sanction', $sanction);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    
    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make(
                $request,
                [
                    'display' => 'required',
                ],
                [
                    'display.required' => 'This Sanction field is required.',
                ]
            );

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Sanction Successfully Updated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $sanction = $response['result'];
            
            
            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $sanction->update([
                'code' => $validated['display'],
                'display' => $validated['display']
            ]);
            DB::commit();
            return $this->response(200, 'Sanction Successfully Updated', $sanction);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $sanction = Sanction::find($id);
            if (!$sanction) return $this->response(404, 'Sanction', 'Not Found!');
            $sanction->delete();

            DB::commit();
            return $this->response(200, 'Sanction Successfully Deleted!', $sanction);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
