<?php

namespace App\Repositories\Hrim\RecruitmentAndHiring;

use App\Interfaces\Hrim\RecruitmentAndHiring\OnboardingStatusMgmtInterface;
use App\Models\Hrim\OnboardingStatusMgmt;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OnboardingStatusMgmtRepository implements OnboardingStatusMgmtInterface
{
    use ResponseTrait;

    public function create($validated)
    {
        DB::beginTransaction();
        try {
            $onboardingStatusMgmt = OnboardingStatusMgmt::create([
                'code' => $validated['onboarding_status'],
                'display' => $validated['onboarding_status'],
                'description' => $validated['description'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'Onboarding Status has been successfully added!', $onboardingStatusMgmt);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update(OnboardingStatusMgmt $onboardingStatusMgmt, $validated)
    {
        DB::beginTransaction();
        try {

            $onboardingStatusMgmt->update([
                'code' => $validated['onboarding_status'],
                'display' => $validated['onboarding_status'],                
                'description' => $validated['description'],
            ]);

            DB::commit();
            return $this->response(200, 'Onboarding Status successfully updated!', $onboardingStatusMgmt);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $onboardingStatusMgmt = OnboardingStatusMgmt::find($id);
            if (!$onboardingStatusMgmt) return $this->response(404, 'Onboarding Status', 'Not Found!');
            $onboardingStatusMgmt->delete();

            DB::commit();
            return $this->response(200, 'Onboarding Status Successfully Deleted!', $onboardingStatusMgmt);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
