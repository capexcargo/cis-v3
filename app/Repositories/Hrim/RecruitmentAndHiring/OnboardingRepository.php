<?php

namespace App\Repositories\Hrim\RecruitmentAndHiring;

use App\Interfaces\Hrim\RecruitmentAndHiring\OnboardingInterface;
use App\Models\Hrim\ApplicantTrackingLog;
use App\Models\Hrim\Onboarding;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OnboardingRepository implements OnboardingInterface
{
    use ResponseTrait;

    public function update(Onboarding $onboarding, $erf_reference_no_id, $final_remarks, $validated)
    {
        DB::beginTransaction();
        try {

            $onboarding->update([
                'status_id' => $validated['onboarding_status'],
            ]);

            $onboarding = ApplicantTrackingLog::create([
                'erf_reference_no_id' => $erf_reference_no_id,
                'date' => date("Y-m-d"),
                'status' => $validated['onboarding_status'],
                'remarks' => $final_remarks,
                'type' => 2,
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'Onboarding status has been successfully updated!', $onboarding);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
