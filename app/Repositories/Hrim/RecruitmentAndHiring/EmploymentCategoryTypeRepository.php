<?php

namespace App\Repositories\Hrim\RecruitmentAndHiring;

use App\Interfaces\Hrim\RecruitmentAndHiring\EmploymentCategoryTypeInterface;
use App\Models\Hrim\EmploymentCategoryType;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class EmploymentCategoryTypeRepository implements EmploymentCategoryTypeInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $employment_category_types = EmploymentCategoryType::
            with('emp_category')
            //     ->when($request['name'] ?? false, function ($query) use ($request) {
            //         $query->where('name', $request['name']);
            //     })
            //     ->when($request['division_id'] ?? false, function ($query) use ($request) {
            //         $query->where('division_id', $request['division_id']);
            //     })
            //     ->when(Auth::user()->level_id != 5, function ($query) {
            //         $query->where('division_id', Auth::user()->division_id);
            //     })
                ->
                paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Category List', compact('employment_category_types'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'category' => 'required',
                'category_type' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request);

        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $employment_category_type = EmploymentCategoryType::create([
                'code' => $validated['category_type'],
                'display' => $validated['category_type'],
                'employment_category_id' => $validated['category'],
            ]);

            DB::commit();

            return $this->response(200, 'Category has been Successfully Created!', $employment_category_type);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $employment_category_type = $response['result'];

            $response = $this->validation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $employment_category_type->update([
                'code' => $validated['category_type'],
                'display' => $validated['category_type'],
                'employment_category_id' => $validated['category'],
            ]);

            DB::commit();

            return $this->response(200, 'Employment Category Type has been successfully updated!', $employment_category_type);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $employment_category_type = EmploymentCategoryType::findOrFail($id);
            if (!$employment_category_type) {
                return $this->response(404, 'Leadership Competencies', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Category Management', $employment_category_type);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

         
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $emp_cat_type = EmploymentCategoryType::find($id);
            if (!$emp_cat_type) return $this->response(404, 'Employment Category Type', 'Not Found!');
            $emp_cat_type->delete();

            DB::commit();
            return $this->response(200, 'Employment Category Type Successfully Deleted!', $emp_cat_type);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
