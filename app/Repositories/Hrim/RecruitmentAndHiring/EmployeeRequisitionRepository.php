<?php

namespace App\Repositories\Hrim\RecruitmentAndHiring;

use App\Interfaces\Hrim\RecruitmentAndHiring\EmployeeRequisitionInterface;
use App\Models\Hrim\EmployeeRequisition;
use App\Models\Hrim\LoaManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class EmployeeRequisitionRepository implements EmployeeRequisitionInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $employee_requisitions = EmployeeRequisition::with(
                'branch',
                'division',
                'position',
                'job_level',
                'employment_category',
                'employment_category_type',
                'requisition_reason',
                'user',
                'firstApprover',
                'secondApprover',
                'status'
            )
                ->whereHas('position', function ($query) use ($request) {
                    $query->when($request['position_search'], function ($query) use ($request) {
                        $query->where('display', 'like', '%' . $request['position_search'] . '%');
                    });
                })
                ->when($request['final_status'], function ($query) use ($request) {
                    if ($request['final_status'] == 2) {
                        $query->whereIn('final_status', [1, 2]);
                    } else {
                        $query->where('final_status', $request['final_status']);
                    }
                })
                ->when($request['branch'], function ($query) use ($request) {
                    $query->where('branch_id', $request['branch']);
                })
                ->when($request['division'], function ($query) use ($request) {
                    $query->where('division_id', $request['division']);
                })
                ->when($request['request_reason'], function ($query) use ($request) {
                    $query->where('requisition_reason_id', $request['request_reason']);
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Employee Requisition List', compact('employee_requisitions'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($validated)
    {
        $approver1 = 0;
        $approver2 = 0;

        DB::beginTransaction();
        try {
            // $approvers = LoaManagement::where('loa_type_id', 5)->where('division_id', $validated['division'])->where('branch_id', $validated['branch'])->get();

            $approvers = LoaManagement::firstWhere([
                ['division_id', $validated['division']],
                ['branch_id', $validated['branch']],
                // ['department_id', $validated['department']],
                ['loa_type_id', 5],
            ]);

            if (!$approvers) return $this->response(500, 'Approver is required', 'Please create approver under your division and branch.');

            // if (count($approvers) > 0) {

            // foreach ($approvers as $approver) {
            //     $approver1 = $approver->first_approver;
            //     $approver2 = $approver->second_approver;
            // }

            if ($validated['employment_cat'] == 2) {
                $employeeRequisition = EmployeeRequisition::create([
                    'erf_reference_no' => $validated['erf_reference'],
                    'branch_id' => $validated['branch'],
                    'division_id' => $validated['division'],
                    'position_id' => $validated['position'],
                    'job_level_id' => $validated['job_level'],
                    'employment_category_id' => $validated['employment_cat'],
                    'employment_type_id' => $validated['emp_cat_type'],
                    'project_name' => $validated['project_name'],
                    'duration_from' => $validated['duration_from'],
                    'duration_to' => $validated['duration_to'],
                    'requisition_reason_id' => $validated['request_reason'],
                    'target_hire' => $validated['target_hire_date'],
                    'first_approver' => $approvers->first_approver,
                    'second_approver' => $approvers->second_approver,
                    'first_status' => 1,
                    'second_status' => 1,
                    'final_status' => 1,
                    'created_by' => Auth::user()->id,
                ]);
            } else {
                $employeeRequisition = EmployeeRequisition::create([
                    'erf_reference_no' => $validated['erf_reference'],
                    'branch_id' => $validated['branch'],
                    'division_id' => $validated['division'],
                    'position_id' => $validated['position'],
                    'job_level_id' => $validated['job_level'],
                    'employment_category_id' => $validated['employment_cat'],
                    'requisition_reason_id' => $validated['request_reason'],
                    'target_hire' => $validated['target_hire_date'],
                    'first_approver' => $approvers->first_approver,
                    'second_approver' => $approvers->second_approver,
                    'first_status' => 1,
                    'second_status' => 1,
                    'final_status' => 1,
                    'created_by' => Auth::user()->id,
                ]);
            }

            $response = $this->attachments($employeeRequisition, $validated['attachments']);

            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            DB::commit();

            return $this->response(200, 'Employee Request has been successfully created', $employeeRequisition);
            // } else {
            //     return $this->response(500, 'Error on Approver', 'You need to set an approver for this requested Division.');
            // }
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update(EmployeeRequisition $employeeRequisition, $validated)
    {
        DB::beginTransaction();
        try {
            if ($validated['employment_cat'] == 2) {
                $employeeRequisition->update([
                    'erf_reference_no' => $validated['erf_reference'],
                    'branch_id' => $validated['branch'],
                    'division_id' => $validated['division'],
                    'position_id' => $validated['position'],
                    'job_level_id' => $validated['job_level'],
                    'employment_category_id' => $validated['employment_cat'],
                    'employment_type_id' => $validated['emp_cat_type'],
                    'project_name' => $validated['project_name'],
                    'duration_from' => $validated['duration_from'],
                    'duration_to' => $validated['duration_to'],
                    'requisition_reason_id' => $validated['request_reason'],
                    'target_hire' => $validated['target_hire_date'],
                    'created_by' => Auth::user()->id,
                ]);
            } else {
                $employeeRequisition->update([
                    'erf_reference_no' => $validated['erf_reference'],
                    'branch_id' => $validated['branch'],
                    'division_id' => $validated['division'],
                    'position_id' => $validated['position'],
                    'job_level_id' => $validated['job_level'],
                    'employment_category_id' => $validated['employment_cat'],
                    'requisition_reason_id' => $validated['request_reason'],
                    'target_hire' => $validated['target_hire_date'],
                    'created_by' => Auth::user()->id,
                ]);
            }

            $response = $this->attachments($employeeRequisition, $validated['attachments']);

            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            DB::commit();

            return $this->response(200, 'Employee Request has been successfully updated', $employeeRequisition);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function attachments($employeeRequisition, $attachments)
    {
        DB::beginTransaction();
        try {
            $date = date('Y/m/d', strtotime($employeeRequisition->created_at));

            foreach ($attachments as $attachment) {
                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $employeeRequisition->attachments()->find($attachment['id']);
                    $attachment_model->delete();
                } elseif ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(' ', '_', 'recruitment_and_hiring/' . $date . '/' . 'employeeRequisition/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        Storage::disk('hrim_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } elseif (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('hrim_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }
                    $employeeRequisition->attachments()->updateOrCreate(
                        [
                            'id' => $attachment['id'],
                        ],
                        [
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                        ]
                    );
                }
            }

            DB::commit();

            return $this->response(200, 'Employee Requisition Attachment Successfully Attached', $employeeRequisition);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function cancel($id)
    {
        DB::beginTransaction();
        try {
            $employeeRequisition = EmployeeRequisition::find($id);
            if (!$employeeRequisition) {
                return $this->response(404, 'Employee Requisition', 'Not Found!');
            }
            $employeeRequisition->delete();

            DB::commit();

            return $this->response(200, 'Employee Request has been successfully cancelled', $employeeRequisition);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function approve($id)
    {
        DB::beginTransaction();
        try {
            $employeeRequisition = EmployeeRequisition::find($id);
            if (!$employeeRequisition) {
                return $this->response(404, 'Employee Requisition', 'Not Found!');
            }

            if ($employeeRequisition->first_approver != '' && $employeeRequisition->first_status == 1) {
                $first_status_approve = 3;
                $second_status_approve = 1;
                $final_status_approve = 2;
                $first_approval_date = date('Y-m-d');
                $second_approval_date = null;
            } elseif ($employeeRequisition->second_approver != '' && $employeeRequisition->second_status == 1 && $employeeRequisition->first_status == 3) {
                $first_status_approve = 3;
                $second_status_approve = 3;
                $final_status_approve = 3;
                $first_approval_date = $employeeRequisition->first_approval_date;
                $second_approval_date = date('Y-m-d');
            }

            $employeeRequisition->update([
                'first_status' => $first_status_approve,
                'second_status' => $second_status_approve,
                'final_status' => $final_status_approve,
                'first_approval_date' => $first_approval_date,
                'second_approval_date' => $second_approval_date,
            ]);

            DB::commit();

            return $this->response(200, 'Employee Request has been successfully approved', $employeeRequisition);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function decline($id)
    {
        DB::beginTransaction();
        try {
            $employeeRequisition = EmployeeRequisition::find($id);
            if (!$employeeRequisition) {
                return $this->response(404, 'Employee Requisition', 'Not Found!');
            }

            if ($employeeRequisition->first_approver != '' && $employeeRequisition->first_status == 1) {
                $first_status_approve = 4;
                $second_status_approve = 4;
                $final_status_approve = 4;
                $first_approval_date = date('Y-m-d');
                $second_approval_date = date('Y-m-d');
            }

            $employeeRequisition->update([
                'first_status' => $first_status_approve,
                'second_status' => $second_status_approve,
                'final_status' => $final_status_approve,
                'first_approval_date' => $first_approval_date,
                'second_approval_date' => $second_approval_date,
            ]);

            DB::commit();

            return $this->response(200, 'Employee Request has been successfully declined', $employeeRequisition);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
