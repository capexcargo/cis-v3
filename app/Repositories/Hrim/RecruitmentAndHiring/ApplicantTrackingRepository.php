<?php

namespace App\Repositories\Hrim\RecruitmentAndHiring;

use App\Interfaces\Hrim\RecruitmentAndHiring\ApplicantTrackingInterface;
use App\Models\Hrim\ApplicantTracking;
use App\Models\Hrim\ApplicantTrackingLog;
use App\Models\Hrim\Onboarding;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ApplicantTrackingRepository implements ApplicantTrackingInterface
{
    use ResponseTrait;

    public function create($validated)
    {
        DB::beginTransaction();
        try {
            $applicantTracking = ApplicantTracking::create([
                'erf_reference_no_id' => $validated['erf_reference'],
                'firstname' => $validated['first_name'],
                'middlename' => $validated['middle_name'],
                'lastname' => $validated['last_name'],
                'date_applied' => $validated['date_applied'],
                'hr_notes' => $validated['hr_notes'],
                'hiring_manager_notes' => $validated['hiring_manager_notes'],
                'gm_notes' => $validated['gm_segs_notes'],
                'status_id' => $validated['status_of_application'],
                'division_id' => $validated['requesting_division'],
                'remarks' => $validated['final_remarks'],
                '2nd_approval_date' => '2022-06-17',
            ]);

            $response = $this->attachments($applicantTracking, $validated['attachments']);
            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $applicantTracking = ApplicantTrackingLog::create([
                'erf_reference_no_id' => $validated['erf_reference'],
                'date' => date("Y-m-d"),
                'status' => $validated['status_of_application'],
                'remarks' => $validated['final_remarks'],
                'type' => 1,
                'created_by' => Auth::user()->id,
            ]);



            DB::commit();
            return $this->response(200, 'Applicant has been successfully added', $applicantTracking);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update(ApplicantTracking $applicantTracking, $validated)
    {
        DB::beginTransaction();
        try {

            $applicantTracking->update([
                'firstname' => $validated['first_name'],
                'middlename' => $validated['middle_name'],
                'lastname' => $validated['last_name'],
                'date_applied' => $validated['date_applied'],
                'hr_notes' => $validated['hr_notes'],
                'hiring_manager_notes' => $validated['hiring_manager_notes'],
                'gm_notes' => $validated['gm_segs_notes'],
                'status_id' => $validated['status_of_application'],
                'division_id' => $validated['requesting_division'],
                'remarks' => $validated['final_remarks'],
            ]);

            $response = $this->attachments($applicantTracking, $validated['attachments']);

            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $applicantTracking = ApplicantTrackingLog::create([
                'erf_reference_no_id' => $validated['erf_reference_no_id'],
                'date' => date("Y-m-d"),
                'status' => $validated['status_of_application'],
                'remarks' => $validated['final_remarks'],
                'type' => 1,
                'created_by' => Auth::user()->id,
            ]);

            if ($validated['status_of_application'] == 9) {
                $applicantTracking = Onboarding::create([
                    'erf_reference_no_id' => $validated['erf_reference_no_id'],
                    'firstname' => $validated['first_name'],
                    'middlename' => $validated['middle_name'],
                    'lastname' => $validated['last_name'],
                    'date_applied' => $validated['date_applied'],
                    'status_id' => 1,
                    '2nd_approval_date' => date("Y-m-d"),
                ]);
            }

            DB::commit();
            return $this->response(200, 'Applicant details has been successfully updated', $applicantTracking);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function attachments($applicantTracking, $attachments)
    {
        DB::beginTransaction();
        try {
            $date = date('Y/m/d', strtotime($applicantTracking->created_at));

            foreach ($attachments as $attachment) {

                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $applicantTracking->attachments()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(" ", "_", 'employee-management/' . $date . '/' . 'applicant-tracking/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        Storage::disk('hrim_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } else if (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('hrim_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }
                    $applicantTracking->attachments()->updateOrCreate(
                        [
                            'id' => $attachment['id']
                        ],
                        [
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                        ]
                    );
                }
            }

            DB::commit();
            return $this->response(200, 'Disciplinary History Attachment Successfully Attached', $applicantTracking);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
