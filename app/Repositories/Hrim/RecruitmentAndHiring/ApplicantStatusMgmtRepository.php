<?php

namespace App\Repositories\Hrim\RecruitmentAndHiring;

use App\Interfaces\Hrim\RecruitmentAndHiring\ApplicantStatusMgmtInterface;
use App\Models\Hrim\ApplicantStatusMgmt;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApplicantStatusMgmtRepository implements ApplicantStatusMgmtInterface
{
    use ResponseTrait;

    public function create($validated)
    {
        DB::beginTransaction();
        try {
            $applicantStatusMgmt = ApplicantStatusMgmt::create([
                'code' => $validated['applicant_status'],
                'display' => $validated['applicant_status'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'Applicant Status has been successfully added!', $applicantStatusMgmt);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function update(ApplicantStatusMgmt $applicantStatusMgmt, $validated)
    {
        DB::beginTransaction();
        try {

            $applicantStatusMgmt->update([
                'code' => $validated['applicant_status'],
                'display' => $validated['applicant_status']
            ]);

            DB::commit();
            return $this->response(200, 'Applicant Status successfully updated!', $applicantStatusMgmt);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    
    
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $applicantStatusMgmt = ApplicantStatusMgmt::find($id);
            if (!$applicantStatusMgmt) return $this->response(404, 'Applicant Status', 'Not Found!');
            $applicantStatusMgmt->delete();

            DB::commit();
            return $this->response(200, 'Applicant Status Successfully Deleted!', $applicantStatusMgmt);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
