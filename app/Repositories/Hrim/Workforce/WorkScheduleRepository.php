<?php

namespace App\Repositories\Hrim\Workforce;

use App\Interfaces\Hrim\Workforce\WorkScheduleInterface;
use App\Models\Hrim\WorkSchedule;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class WorkScheduleRepository implements WorkScheduleInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $work_schedules = WorkSchedule::when($request['work_schedule'] ?? false, function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request['work_schedule'] . '%');
            })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);
            DB::commit();
            return $this->response(200, 'Work Schedule List', $work_schedules);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'work_schedule' => 'required',
                'work_mode' => 'required',
                'shift' => 'required',
                'inclusive_days' => 'required',
                'time_from' => 'required',
                'time_to' => 'required',
                'break_from' => 'required',
                'break_to' => 'required',
                'ot_break_from' => 'required',
                'ot_break_to' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $work_schedule = WorkSchedule::create([
                'name' => $validated['work_schedule'],
                'work_mode_id' => $validated['work_mode'],
                'shift' => $validated['shift'],
                'monday' => in_array("monday", $validated['inclusive_days']),
                'tuesday' => in_array("tuesday", $validated['inclusive_days']),
                'wednesday' => in_array("wednesday", $validated['inclusive_days']),
                'thursday' => in_array("thursday", $validated['inclusive_days']),
                'friday' => in_array("friday", $validated['inclusive_days']),
                'saturday' => in_array("saturday", $validated['inclusive_days']),
                'sunday' => in_array("sunday", $validated['inclusive_days']),
                'time_from' => $validated['time_from'],
                'time_to' => $validated['time_to'],
                'break_from' => $validated['break_from'],
                'break_to' => $validated['break_to'],
                'ot_break_from' => $validated['ot_break_from'],
                'ot_break_to' => $validated['ot_break_to'],
                'created_by' => Auth::user()->id
            ]);

            DB::commit();
            return $this->response(200, 'Work schedule has been successfully created', $work_schedule);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $work_schedule = WorkSchedule::find($id);
            if (!$work_schedule) return $this->response(404, 'Work Schedule', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Work Schedule', $work_schedule);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);

            $validated = $response['result'];

            $response = $this->show($id);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);

            $work_schedule = $response['result'];
            $work_schedule->update([
                'name' => $validated['work_schedule'],
                'work_mode_id' => $validated['work_mode'],
                'shift' => $validated['shift'],
                'monday' => in_array("monday", $validated['inclusive_days']),
                'tuesday' => in_array("tuesday", $validated['inclusive_days']),
                'wednesday' => in_array("wednesday", $validated['inclusive_days']),
                'thursday' => in_array("thursday", $validated['inclusive_days']),
                'friday' => in_array("friday", $validated['inclusive_days']),
                'saturday' => in_array("saturday", $validated['inclusive_days']),
                'sunday' => in_array("sunday", $validated['inclusive_days']),
                'time_from' => $validated['time_from'],
                'time_to' => $validated['time_to'],
                'break_from' => $validated['break_from'],
                'break_to' => $validated['break_to'],
                'ot_break_from' => $validated['ot_break_from'],
                'ot_break_to' => $validated['ot_break_to'],
            ]);

            DB::commit();
            return $this->response(200, 'Work schedule has been successfully updated', $work_schedule);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
