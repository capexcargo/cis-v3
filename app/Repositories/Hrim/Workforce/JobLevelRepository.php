<?php

namespace App\Repositories\Hrim\Workforce;

use App\Interfaces\Hrim\Workforce\JobLevelInterface;
use App\Models\Hrim\JobLevel;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class JobLevelRepository implements JobLevelInterface
{
    use ResponseTrait;

    public function show($id)
    {
        $department = JobLevel::find($id);

        if (!$department) return $this->response(404, 'Job Level', 'Not Found!');

        return $this->response(200, 'JobLevel', $department);
    }

    public function createJobLevel($validated)
    {
        DB::beginTransaction();
        try {
            $department = JobLevel::create([
                'code' => $validated['display'],
                'display' => $validated['display'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'Job Level has been successfully created', $department);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateJobLevel(JobLevel $department, $validated)
    {
        DB::beginTransaction();
        try {

            $department->update([
                'code' => $validated['display'],
                'display' => $validated['display']
            ]);
            
            DB::commit();
            return $this->response(200, 'Job Level has been successfully updated!', $department);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $department = JobLevel::find($id);
            if (!$department) return $this->response(404, 'Job Level', 'Not Found!');
            $department->delete();

            DB::commit();
            return $this->response(200, 'Job Level has been successfully deleted!', $department);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
