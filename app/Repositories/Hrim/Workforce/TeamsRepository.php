<?php

namespace App\Repositories\Hrim\Workforce;

use App\Interfaces\Hrim\Workforce\TeamsInterface;
use App\Models\Division;
use App\Models\Hrim\TeamsAttachment;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class TeamsRepository implements TeamsInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $division = Division::with(['teams' => function ($query) {
                $query->with('division', 'position', 'reportsTo', 'department', 'section', 'createdBy');
            }, 'attachments'])
                ->orderBy('description', 'asc')
                ->get();
            DB::commit();
            return $this->response(200, 'Division List', $division);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        $division = Division::with(['teams' => function ($query) {
            $query->with('division', 'position', 'reportsTo', 'department', 'section', 'createdBy');
        }])
            ->find($id);
        if (!$division) return $this->response(404, 'Division', 'Not Found!');

        return $this->response(200, 'Division', $division);
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'division' => 'required',

                'teams' => 'required',
                'teams.*.id' => 'sometimes',
                'teams.*.role' => 'required',
                'teams.*.reports_to' => 'required',
                'teams.*.department' => 'required',
                'teams.*.section' => 'nullable',
                'teams.*.is_deleted' => 'sometimes',
            ], [
                'teams.*.role.required' => 'The role field is required.',
                'teams.*.reports_to.required' => 'The reports to field is required.',
                'teams.*.department.required' => 'The department field is required.',
                'teams.*.section.required' => 'The section field is required.',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateOrCreate($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $division = $response['result'];

            foreach ($validated['teams'] as $team) {
                if ($team['id'] && $team['is_deleted']) {
                    $team = $division->teams()->find($team['id']);
                    if ($team) {
                        $team->delete();
                    }
                } else {
                    $team = $division->teams()->updateOrCreate(
                        [
                            'id' => $team['id'],
                        ],
                        [
                            'division_id' => $validated['division'],
                            'position_id' => $team['role'],
                            'reports_to' => $team['reports_to'],
                            'department_id' => $team['department'],
                            'section_id' => $team['section'],
                            'created_by' => Auth::user()->id
                        ]
                    );
                }
            }

            DB::commit();
            return $this->response(200, ('Team has been successfully ' . ($team->wasRecentlyCreated ? 'created' : 'updated')), $team);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateStatus($id, $status)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $division = $response['result'];
            $division->update([
                'teams_status' => $status
            ]);

            DB::commit();
            return $this->response(200, ('Team has been successfully ' . ($status ? 'reactived!' : 'deactived!')), $division);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function showTeams($id)
    {
        $teams = Division::with('attachments')->find($id);

        if (!$teams) return $this->response(404, 'Teams', 'Not Found!');

        return $this->response(200, 'Teams for attachment', $teams);
    }

    public function validateAttachment($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'attachment' => 'nullable',
                'attachment' => 'nullable|' . config('filesystems.validation_all'),
            ], [
                'attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function addFile($request, $id)
    {
        DB::beginTransaction();
        try {

            $response = $this->validateAttachment($request);
            // dd($response);

            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            if ($validated['attachment']) {
                $response = $this->storeAttachment($validated['attachment']);
                if ($response['code'] == 500) {
                    return $this->response($response['code'], $response['message'], $response['result']);
                }
                $file = $response['result'];

                $teams = TeamsAttachment::updateOrCreate(
                    [
                        'division_id' => $id,
                    ],
                    [
                        'path' => $file['file_path'],
                        'name' => $file['file_name'],
                        'extension' => $validated['attachment']->extension(),
                    ]
                );
            }

            DB::commit();

            return $this->response(200, 'Teams organization chart has been successfully uploaded!', $teams);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function storeAttachment($file)
    {
        DB::beginTransaction();
        try {
            $year = date('Y', strtotime(now()));
            $file_path = strtolower(str_replace(' ', '_', 'workforce/teams/' . $year . '/'));
            $file_name = Auth::user()->userDetails->division_id . '-' . date('mdYHis') . uniqid() . '.' . $file->extension();

            if (in_array($file->extension(), config('filesystems.image_type'))) {
                $image_resize = Image::make($file->getRealPath())->resize(1024, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                Storage::disk('hrim_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
            } elseif (in_array($file->extension(), config('filesystems.file_type'))) {
                Storage::disk('hrim_gcs')->putFileAs($file_path, $file, $file_name);
            }

            DB::commit();

            return $this->response(200, 'Successfuly Save', compact('file_path', 'file_name'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
