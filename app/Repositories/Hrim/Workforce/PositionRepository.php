<?php

namespace App\Repositories\Hrim\Workforce;

use App\Interfaces\Hrim\Workforce\PositionInterface;
use App\Models\Hrim\Position;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PositionRepository implements PositionInterface
{
    use ResponseTrait;

    public function show($position_id)
    {
        $position = Position::find($position_id);

        if (!$position) return $this->response(404, 'Position', 'Not Found!');

        return $this->response(200, 'Position', $position);
    }

    public function create($validated)
    {
        DB::beginTransaction();
        try {

            $position = Position::create([
                'code' => $validated['position'],
                'display' => $validated['position'],
                'job_level_id' => $validated['job_level'],
                'department_level_id' => $validated['department'],
                'division_id' => $validated['division'],
                'job_overview' => $validated['job_overview'],
                'created_by' => Auth::user()->id,
            ]);

            foreach ($validated['responsibilities'] as $responsibility) {
                $position->responsibilities()->updateOrCreate(
                    [
                        'duties' => $responsibility['duties']
                    ],
                    [
                        'position_id' =>  $position->id,
                        'duties' => $responsibility['duties'],
                        'created_by' => Auth::user()->id,
                    ]
                );
            }

            foreach ($validated['requirements'] as $requirement) {
                $position->requirements()->updateOrCreate(
                    [
                        'code' => $requirement['code']
                    ],
                    [
                        'position_id' =>  $position->id,
                        'code' => $requirement['code'],
                        'created_by' => Auth::user()->id,
                    ]
                );
            }

            DB::commit();
            return $this->response(200, 'New position has been successfully added', $position);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    
    public function update(Position $position, $validated)
    {
        DB::beginTransaction();
        try {

            $position->update([
                'code' => $validated['display'],
                'display' => $validated['display'],
                'job_level_id' => $validated['job_level'],
                'department_level_id' => $validated['department'],
                'division_id' => $validated['division'],
                'job_overview' => $validated['job_overview'],
            ]);

            foreach ($validated['responsibilities'] as $responsibility) {
                if ($responsibility['id'] && $responsibility['is_deleted']) {
                    $responsibility_model = $position->responsibilities()->find($responsibility['id']);
                    if ($responsibility_model) {
                        $responsibility_model->delete();
                    }
                } else {

                    $position->responsibilities()->updateOrCreate(
                        [
                            'duties' => $responsibility['duties']
                        ],
                        [
                            'position_id' =>  $position->id,
                            'duties' => $responsibility['duties'],
                            'created_by' => Auth::user()->id,
                        ]
                    );
                }
            }


            foreach ($validated['requirements'] as $requirement) {
                if ($requirement['id'] && $requirement['is_deleted']) {
                    $requirement_model = $position->requirements()->find($requirement['id']);
                    if ($requirement_model) {
                        $requirement_model->delete();
                    }
                } else {

                    $position->requirements()->updateOrCreate(
                        [
                            'code' => $requirement['code']
                        ],
                        [
                            'position_id' =>  $position->id,
                            'code' => $requirement['code'],
                            'created_by' => Auth::user()->id,
                        ]
                    );
                }
            }

            DB::commit();
            return $this->response(200, 'Position Successfully Updated', $position);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $position = Position::find($id);
            if (!$position) return $this->response(404, 'Position', 'Not Found!');
            $position->delete();

            DB::commit();
            return $this->response(200, 'Position Successfully Deleted!', $position);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
