<?php

namespace App\Repositories\Hrim\Workforce;

use App\Interfaces\Hrim\Workforce\DepartmentInterface;
use App\Models\Hrim\Department;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DepartmentRepository implements DepartmentInterface
{
    use ResponseTrait;

    public function show($id)
    {
        $department = Department::find($id);

        if (!$department) return $this->response(404, 'Department', 'Not Found!');

        return $this->response(200, 'Department', $department);
    }

    public function createDepartment($validated)
    {
        DB::beginTransaction();
        try {
            $department = Department::create([
                'code' => $validated['department'],
                'display' => $validated['department'],
                'division_id' => $validated['division'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'Department has been successfully created', $department);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateDepartment(Department $department, $validated)
    {
        DB::beginTransaction();
        try {

            $department->update([
                'code' => $validated['department'],
                'display' => $validated['department'],
                'division_id' => $validated['division'],
            ]);
            
            DB::commit();
            return $this->response(200, 'Department has been successfully updated!', $department);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $department = Department::find($id);
            if (!$department) return $this->response(404, 'Department', 'Not Found!');
            $department->delete();

            DB::commit();
            return $this->response(200, 'Department has been successfully deleted!', $department);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
