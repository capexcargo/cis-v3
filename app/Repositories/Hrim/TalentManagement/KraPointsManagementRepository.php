<?php

namespace App\Repositories\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\KraPointsManagementInterface;
use App\Models\Hrim\KraPointsManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class KraPointsManagementRepository implements KraPointsManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $kra_points_managements = KraPointsManagement::when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);
            DB::commit();
            return $this->response(200, 'KRA Points List',  $kra_points_managements);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'points' => 'required',
                'description' => 'required',
                'quarter' => 'required',
                'year' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $kra_points = KraPointsManagement::create([
                'points' => $validated['points'],
                'description' => $validated['description'],
                'quarter' => $validated['quarter'],
                'year' => $validated['year'],
            ]);

            DB::commit();
            return $this->response(200, 'New KRA Points has been successfully added!', $kra_points);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $kra_points = KraPointsManagement::findOrFail($id);
            if (!$kra_points) return $this->response(404, 'KRA Points', 'Not Found!');

            DB::commit();
            return $this->response(200, 'KRA Points', $kra_points);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'points' => 'required',
                'description' => 'required',
                'quarter' => 'required',
                'year' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $kra_points = $response['result'];


            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $kra_points->update([
                'points' => $validated['points'],
                'description' => $validated['description'],
                'quarter' => $validated['quarter'],
                'year' => $validated['year'],
            ]);

            DB::commit();
            return $this->response(200, 'KRA Points has been successfully updated!', $kra_points);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $kra_points = KraPointsManagement::find($id);
            if (!$kra_points) return $this->response(404, 'KRA Points', 'Not Found!');
            $kra_points->delete();

            DB::commit();
            return $this->response(200, 'KRA Points successfully deleted!', $kra_points);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
