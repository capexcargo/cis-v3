<?php

namespace App\Repositories\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\KpiTaggingInterface;
use App\Models\Hrim\CoreValueManagement;
use App\Models\Hrim\KpiTagging;
use App\Models\Hrim\LeadershipCompetenciesMgmt;
use App\Models\Hrim\PerformanceEvaluation;
use App\Models\UserDetails;
use App\Traits\ResponseTrait;
use Database\Seeders\Hrim\TalentManagement\Performance\CoreValueManagementSeeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class KpiTaggingRepository implements KpiTaggingInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $kpi_tags = KpiTagging::with('position')
                ->whereHas('position', function ($query) use ($request) {
                    $query->when($request['position'] ?? false, function ($query) use ($request) {
                        $query->where('display', 'like', '%' . ($request['position'] ?? false) . '%');
                    });
                })
                ->when($request['quarter'] ?? false, function ($query) use ($request) {
                    $query->where('quarter', $request['quarter']);
                })
                ->when($request['year'] ?? false, function ($query) use ($request) {
                    $query->where('year', $request['year']);
                })
                // ->when($request['quarter'], function ($query) use ($request) {
                //     $query->orderBy('quarter', $request['quarter']);

                //     $query->orderBy($request['sort_field'], $request['sort_type']);
                // })
                ->select('quarter', 'position_id', 'year')->distinct()
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'KPI Tagged List',  $kpi_tags);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'position' => 'required',
                'quarter' => 'required',
                'year' => 'sometimes',
                'kpis' => 'sometimes',
                'kras' => 'sometimes',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];


            $users = UserDetails::where('position_id', $validated['position'])->get();
            $core_values = CoreValueManagement::get();
            $leadership_comps = LeadershipCompetenciesMgmt::get();


            if (count($users) > 0) {


                foreach ($users as $user) {

                    foreach ($validated['kpis'] as $i => $kpi) {

                        $kpi_tag = KpiTagging::updateOrCreate(
                            [
                                'position_id' => $validated['position'],
                                'quarter' => $validated['quarter'],
                                'year' => $validated['year'],

                                'kpi_id' => $kpi,
                                'kra_id' => $validated['kras'][$i],
                            ],
                        );


                        $kpi_tag->positionID()->updateOrCreate(
                            [
                                'user_id' =>  $user['user_id'],
                                'position_id' =>  $user['position_id'],
                                'kpi_id' =>  $kpi,
                                'kra_id' => $validated['kras'][$i],
                                'first_status' =>  1,
                                'second_status' =>  1,
                                'third_status' =>  1,

                                'quarter' => $validated['quarter'],
                                'year' => $validated['year'],
                            ]
                        );


                        $kpi_tag->positionID()->updateOrCreate(
                            [
                                'user_id' =>  $user['user_id'],
                                'position_id' =>  $user['position_id'],
                                'kpi_id' =>  $kpi,
                                'kra_id' => $validated['kras'][$i],
                                'first_status' =>  1,
                                'second_status' =>  1,
                                'third_status' =>  1,

                                'quarter' => $validated['quarter'],
                                'year' => $validated['year'],
                            ]
                        );

                        foreach ($core_values as $core_value) {
                            $kpi_tag = PerformanceEvaluation::updateOrCreate(
                                [
                                    'user_id' =>  $user['user_id'],
                                    'position_id' =>  $user['position_id'],
                                    'core_values_id' => $core_value->id,
                                    'first_status' =>  1,
                                    'second_status' =>  1,
                                    'third_status' =>  1,

                                    'quarter' => $validated['quarter'],
                                    'year' => $validated['year'],
                                ]
                            );
                        }

                        if ($user['job_level_id'] != 6) {
                            foreach ($leadership_comps as $leadership_comp) {
                                $kpi_tag = PerformanceEvaluation::updateOrCreate(
                                    [
                                        'user_id' =>  $user['user_id'],
                                        'position_id' =>  $user['position_id'],
                                        'leadership_comp_id' => $leadership_comp->id,
                                        'first_status' =>  1,
                                        'second_status' =>  1,
                                        'third_status' =>  1,

                                        'quarter' => $validated['quarter'],
                                        'year' => $validated['year'],
                                    ]
                                );
                            }
                        }
                    }
                }
            } else {
                return $this->response(500, "Position doesn't have any tagged user", '');
            }

            DB::commit();
            return $this->response(200, 'KPI has been successfully tagged!', $kpi_tag);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
