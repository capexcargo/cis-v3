<?php

namespace App\Repositories\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\PerformanceEvaluationInterface;
use App\Models\Hrim\PerformanceEvaluation;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PerformanceEvaluationRepository implements PerformanceEvaluationInterface
{
    use ResponseTrait;

    public function save1stJustification($request)
    {
        DB::beginTransaction();
        try {

            $first_justi = PerformanceEvaluation::where('id', $request['id'])->update(
                [
                    'first_justification' => $request['first_justification'],
                ]
            );

            DB::commit();
            return $this->response(200, 'First Justification Saved!', $first_justi);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function save2ndJustification($request)
    {
        DB::beginTransaction();
        try {

            $second_justi = PerformanceEvaluation::where('id', $request['id'])->update(
                [
                    'second_justification' => $request['second_justification'],
                ]
            );

            DB::commit();
            return $this->response(200, 'Second Justification Saved!', $second_justi);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function save3rdJustification($request)
    {
        DB::beginTransaction();
        try {

            $third_justi = PerformanceEvaluation::where('id', $request['id'])->update(
                [
                    'third_justification' => $request['third_justification'],
                ]
            );

            DB::commit();
            return $this->response(200, 'Third Justification Saved!', $third_justi);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function closeValidation($request)
    {
        DB::beginTransaction();
        try {
            if (count($request['leadership_competencies']) > 0) {

                if (
                    $request['performance_scorings'][0]['first_status'] == 1 &&
                    $request['core_value_assessments'][0]['first_status'] == 1 &&
                    $request['leadership_competencies'][0]['first_status'] == 1
                ) {

                    foreach ($request['performance_scorings'] as $i => $perf_scores) {
                        $rules['performance_scorings.' . $i . '.first_evaluator_points'] = 'required';
                    }

                    foreach ($request['core_value_assessments'] as $i => $core_vals) {
                        $first_eval_points[$i] = $core_vals['first_evaluator_points'];

                        if ($first_eval_points[$i] > 3) {
                            $rules['core_value_assessments.' . $i . '.evaluator_1st_critical_remarks'] = 'required';
                        }
                        $rules['core_value_assessments.' . $i . '.first_evaluator_points'] = 'required';
                    }

                    foreach ($request['leadership_competencies'] as $i => $core_vals) {
                        $first_eval_points[$i] = $core_vals['first_evaluator_points'];

                        if ($first_eval_points[$i] > 3) {
                            $rules['leadership_competencies.' . $i . '.evaluator_1st_critical_remarks'] = 'required';
                        }
                        $rules['leadership_competencies.' . $i . '.first_evaluator_points'] = 'required';
                    }

                    $validator = Validator::make(
                        $request,
                        $rules,
                        [
                            'performance_scorings.*.first_evaluator_points.required' => 'This field is required.',
                            'core_value_assessments.*.first_evaluator_points.required' => 'This field is required.',
                            'core_value_assessments.*.evaluator_1st_critical_remarks.required' => 'This field is required.',
                            'leadership_competencies.*.first_evaluator_points.required' => 'This field is required.',
                            'leadership_competencies.*.evaluator_1st_critical_remarks.required' => 'This field is required.',
                        ]
                    );
                } else if (
                    $request['performance_scorings'][0]['first_status'] == 2 && $request['core_value_assessments'][0]['first_status'] == 2 && $request['leadership_competencies'][0]['first_status'] == 2 &&
                    $request['performance_scorings'][0]['second_status'] == 1 && $request['core_value_assessments'][0]['second_status'] == 1 && $request['leadership_competencies'][0]['second_status'] == 1
                ) {

                    foreach ($request['performance_scorings'] as $i => $perf_scores) {
                        $rules['performance_scorings.' . $i . '.second_evaluator_points'] = 'required';
                    }

                    foreach ($request['core_value_assessments'] as $i => $core_vals) {
                        $first_eval_points[$i] = $core_vals['second_evaluator_points'];

                        if ($first_eval_points[$i] > 3) {
                            $rules['core_value_assessments.' . $i . '.evaluator_2nd_critical_remarks'] = 'required';
                        }
                        $rules['core_value_assessments.' . $i . '.second_evaluator_points'] = 'required';
                    }

                    foreach ($request['leadership_competencies'] as $i => $core_vals) {
                        $first_eval_points[$i] = $core_vals['second_evaluator_points'];

                        if ($first_eval_points[$i] > 3) {
                            $rules['leadership_competencies.' . $i . '.evaluator_2nd_critical_remarks'] = 'required';
                        }
                        $rules['leadership_competencies.' . $i . '.second_evaluator_points'] = 'required';
                    }

                    $validator = Validator::make(
                        $request,
                        $rules,
                        [
                            'performance_scorings.*.second_evaluator_points.required' => 'This field is required.',
                            'core_value_assessments.*.second_evaluator_points.required' => 'This field is required.',
                            'core_value_assessments.*.evaluator_2nd_critical_remarks.required' => 'This field is required.',
                            'leadership_competencies.*.second_evaluator_points.required' => 'This field is required.',
                            'leadership_competencies.*.evaluator_2nd_critical_remarks.required' => 'This field is required.',
                        ]
                    );
                } else if (
                    $request['performance_scorings'][0]['second_status'] == 2 && $request['core_value_assessments'][0]['second_status'] == 2 && $request['leadership_competencies'][0]['second_status'] == 2 &&
                    $request['performance_scorings'][0]['third_status'] == 1 && $request['core_value_assessments'][0]['third_status'] == 1 && $request['leadership_competencies'][0]['third_status'] == 1
                ) {

                    foreach ($request['performance_scorings'] as $i => $perf_scores) {
                        $rules['performance_scorings.' . $i . '.third_evaluator_points'] = 'required';
                    }

                    foreach ($request['core_value_assessments'] as $i => $core_vals) {
                        $first_eval_points[$i] = $core_vals['third_evaluator_points'];

                        if ($first_eval_points[$i] > 3) {
                            $rules['core_value_assessments.' . $i . '.evaluator_3rd_critical_remarks'] = 'required';
                        }
                        $rules['core_value_assessments.' . $i . '.third_evaluator_points'] = 'required';
                    }

                    foreach ($request['leadership_competencies'] as $i => $core_vals) {
                        $first_eval_points[$i] = $core_vals['third_evaluator_points'];

                        if ($first_eval_points[$i] > 3) {
                            $rules['leadership_competencies.' . $i . '.evaluator_3rd_critical_remarks'] = 'required';
                        }
                        $rules['leadership_competencies.' . $i . '.third_evaluator_points'] = 'required';
                    }

                    $validator = Validator::make(
                        $request,
                        $rules,
                        [
                            'performance_scorings.*.third_evaluator_points.required' => 'This field is required.',
                            'core_value_assessments.*.third_evaluator_points.required' => 'This field is required.',
                            'core_value_assessments.*.evaluator_3rd_critical_remarks.required' => 'This field is required.',
                            'leadership_competencies.*.third_evaluator_points.required' => 'This field is required.',
                            'leadership_competencies.*.evaluator_3rd_critical_remarks.required' => 'This field is required.',
                        ]
                    );
                }
            } else {
                if (
                    $request['performance_scorings'][0]['first_status'] == 1 &&
                    $request['core_value_assessments'][0]['first_status'] == 1
                ) {

                    foreach ($request['performance_scorings'] as $i => $perf_scores) {
                        $rules['performance_scorings.' . $i . '.first_evaluator_points'] = 'required';
                    }

                    foreach ($request['core_value_assessments'] as $i => $core_vals) {
                        $first_eval_points[$i] = $core_vals['first_evaluator_points'];

                        if ($first_eval_points[$i] > 3) {
                            $rules['core_value_assessments.' . $i . '.evaluator_1st_critical_remarks'] = 'required';
                        }
                        $rules['core_value_assessments.' . $i . '.first_evaluator_points'] = 'required';
                    }

                    $validator = Validator::make(
                        $request,
                        $rules,
                        [
                            'performance_scorings.*.first_evaluator_points.required' => 'This field is required.',
                            'core_value_assessments.*.first_evaluator_points.required' => 'This field is required.',
                            'core_value_assessments.*.evaluator_1st_critical_remarks.required' => 'This field is required.',
                        ]
                    );
                } else if (
                    $request['performance_scorings'][0]['first_status'] == 2 && $request['core_value_assessments'][0]['first_status'] == 2 &&
                    $request['performance_scorings'][0]['second_status'] == 1 && $request['core_value_assessments'][0]['second_status'] == 1
                ) {

                    foreach ($request['performance_scorings'] as $i => $perf_scores) {
                        $rules['performance_scorings.' . $i . '.second_evaluator_points'] = 'required';
                    }

                    foreach ($request['core_value_assessments'] as $i => $core_vals) {
                        $first_eval_points[$i] = $core_vals['second_evaluator_points'];

                        if ($first_eval_points[$i] > 3) {
                            $rules['core_value_assessments.' . $i . '.evaluator_2nd_critical_remarks'] = 'required';
                        }
                        $rules['core_value_assessments.' . $i . '.second_evaluator_points'] = 'required';
                    }

                    $validator = Validator::make(
                        $request,
                        $rules,
                        [
                            'performance_scorings.*.second_evaluator_points.required' => 'This field is required.',
                            'core_value_assessments.*.second_evaluator_points.required' => 'This field is required.',
                            'core_value_assessments.*.evaluator_2nd_critical_remarks.required' => 'This field is required.',
                        ]
                    );
                } else if (
                    $request['performance_scorings'][0]['second_status'] == 2 && $request['core_value_assessments'][0]['second_status'] == 2 &&
                    $request['performance_scorings'][0]['third_status'] == 1 && $request['core_value_assessments'][0]['third_status'] == 1
                ) {

                    foreach ($request['performance_scorings'] as $i => $perf_scores) {
                        $rules['performance_scorings.' . $i . '.third_evaluator_points'] = 'required';
                    }

                    foreach ($request['core_value_assessments'] as $i => $core_vals) {
                        $first_eval_points[$i] = $core_vals['third_evaluator_points'];

                        if ($first_eval_points[$i] > 3) {
                            $rules['core_value_assessments.' . $i . '.evaluator_3rd_critical_remarks'] = 'required';
                        }
                        $rules['core_value_assessments.' . $i . '.third_evaluator_points'] = 'required';
                    }

                    $validator = Validator::make(
                        $request,
                        $rules,
                        [
                            'performance_scorings.*.third_evaluator_points.required' => 'This field is required.',
                            'core_value_assessments.*.third_evaluator_points.required' => 'This field is required.',
                            'core_value_assessments.*.evaluator_3rd_critical_remarks.required' => 'This field is required.',
                        ]
                    );
                }
            }

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function save($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['performance_scorings'] as $i => $scoring) {
                if ($scoring['first_evaluator_points'] != "" && $scoring['first_status'] == 1) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $scoring['id'])->update(
                        [
                            'first_points' => $scoring['first_evaluator_points'],
                        ]
                    );
                } else if ($scoring['second_evaluator_points'] != "" && ($scoring['first_status'] == 2  && $scoring['second_status'] == 1)) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $scoring['id'])->update(
                        [
                            'second_points' => $scoring['second_evaluator_points'],
                        ]
                    );
                } else if ($scoring['third_evaluator_points'] != "" && ($scoring['second_status'] == 2  && $scoring['third_status'] == 1)) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $scoring['id'])->update(
                        [
                            'third_points' => $scoring['third_evaluator_points'],
                        ]
                    );
                }
            }

            foreach ($request['core_value_assessments'] as $i => $core_value) {
                if ($core_value['first_evaluator_points'] != "" && $scoring['first_status'] == 1) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $core_value['id'])->update(
                        [
                            'first_points' => $core_value['first_evaluator_points'],
                            'core_val_1st_critical_remarks' => $core_value['evaluator_1st_critical_remarks'],
                        ]
                    );
                } else if ($core_value['second_evaluator_points'] != "" && ($core_value['first_status'] == 2  && $core_value['second_status'] == 1)) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $core_value['id'])->update(
                        [
                            'second_points' => $core_value['second_evaluator_points'],
                            'core_val_2nd_critical_remarks' => $core_value['evaluator_2nd_critical_remarks'],
                        ]
                    );
                } else if ($core_value['third_evaluator_points'] != "" && ($core_value['second_status'] == 2  && $core_value['third_status'] == 1)) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $core_value['id'])->update(
                        [
                            'third_points' => $core_value['third_evaluator_points'],
                            'core_val_3rd_critical_remarks' => $core_value['evaluator_3rd_critical_remarks'],
                        ]
                    );
                }
            }

            foreach ($request['leadership_competencies'] as $i => $lead_comp) {
                if ($lead_comp['first_evaluator_points'] != "" && ($lead_comp['first_status'] == 1)) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $lead_comp['id'])->update(
                        [
                            'first_points' => $lead_comp['first_evaluator_points'],
                            'lead_com_1st_critical_remarks' => $lead_comp['evaluator_1st_critical_remarks'],
                        ]
                    );
                } else if ($lead_comp['second_evaluator_points'] != "" && ($lead_comp['first_status'] == 2  && $lead_comp['second_status'] == 1)) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $lead_comp['id'])->update(
                        [
                            'second_points' => $lead_comp['second_evaluator_points'],
                            'lead_com_2nd_critical_remarks' => $lead_comp['evaluator_2nd_critical_remarks'],
                        ]
                    );
                } else if ($lead_comp['third_evaluator_points'] != "" && ($lead_comp['second_status'] == 2  && $lead_comp['third_status'] == 1)) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $lead_comp['id'])->update(
                        [
                            'third_points' => $lead_comp['third_evaluator_points'],
                            'lead_com_3rd_critical_remarks' => $lead_comp['evaluator_3rd_critical_remarks'],
                        ]
                    );
                }
            }

            foreach ($request['performance_evaluators'] as $i => $lead_comp) {
                if ($lead_comp['first_remarks'] != "" && ($lead_comp['first_status'] == 1)) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $lead_comp['id'])->update(
                        [
                            'first_remarks' => $lead_comp['first_remarks'],
                        ]
                    );
                } else if ($lead_comp['second_remarks'] != "" && ($lead_comp['first_status'] == 2  && $lead_comp['second_status'] == 1)) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $lead_comp['id'])->update(
                        [
                            'second_remarks' => $lead_comp['second_remarks'],
                        ]
                    );
                } else if ($lead_comp['third_remarks'] != "" && ($lead_comp['second_status'] == 2  && $lead_comp['third_status'] == 1)) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $lead_comp['id'])->update(
                        [
                            'third_remarks' => $lead_comp['third_remarks'],
                        ]
                    );
                }
            }

            DB::commit();
            return $this->response(200, 'Evaluation Saved!', $performance_evaluation);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function close($request)
    {
        DB::beginTransaction();
        try {

            $response = $this->closeValidation($request);

            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $performance_evaluation = $response['result'];

            foreach ($request['performance_scorings'] as $i => $scoring) {

                if ($scoring['first_evaluator_points'] != "" && $scoring['first_status'] == 1) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $scoring['id'])->update(
                        [
                            'first_points' => $scoring['first_evaluator_points'],
                            'first_status' => 2,
                            'first_eval_date' => date("Y-m-d H:i:s"),
                            'first_remarks' => $request['performance_evaluators'][0]['first_remarks'],
                            'performance_total_score' => $request['first_scoring_total'],
                            'overall_total_score' => $request['first_scoring_total'] + $request['second_scoring_total'] + $request['third_scoring_total'],
                            'final_score' => ($request['first_scoring_total'] + $request['second_scoring_total'] + $request['third_scoring_total']) +
                                ($request['first_core_values_total'] + $request['second_core_values_total'] + $request['third_core_values_total']) +
                                ($request['first_lead_com_total'] + $request['second_lead_com_total'] + $request['third_lead_com_total']),
                        ]
                    );
                } else if ($scoring['second_evaluator_points'] != "" && ($scoring['first_status'] == 2  && $scoring['second_status'] == 1)) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $scoring['id'])->update(
                        [
                            'second_points' => $scoring['second_evaluator_points'],
                            'second_status' => 2,
                            'second_eval_date' => date("Y-m-d H:i:s"),
                            'second_remarks' => $request['performance_evaluators'][0]['second_remarks'],
                            'performance_total_score' => $request['second_scoring_total'],
                            'overall_total_score' => $request['first_scoring_total'] + $request['second_scoring_total'] + $request['third_scoring_total'],
                            'final_score' => ($request['first_scoring_total'] + $request['second_scoring_total'] + $request['third_scoring_total']) +
                                ($request['first_core_values_total'] + $request['second_core_values_total'] + $request['third_core_values_total']) +
                                ($request['first_lead_com_total'] + $request['second_lead_com_total'] + $request['third_lead_com_total']),
                        ]
                    );
                } else if ($scoring['third_evaluator_points'] != "" && ($scoring['second_status'] == 2  && $scoring['third_status'] == 1)) {

                    $performance_evaluation = PerformanceEvaluation::where('id', $scoring['id'])->update(
                        [
                            'third_points' => $scoring['third_evaluator_points'],
                            'third_status' => 2,
                            'third_eval_date' => date("Y-m-d H:i:s"),
                            'third_remarks' => $request['performance_evaluators'][0]['third_remarks'],
                            'performance_total_score' => $request['third_scoring_total'],
                            'overall_total_score' => $request['first_scoring_total'] + $request['second_scoring_total'] + $request['third_scoring_total'],
                            'final_score' => ($request['first_scoring_total'] + $request['second_scoring_total'] + $request['third_scoring_total']) +
                                ($request['first_core_values_total'] + $request['second_core_values_total'] + $request['third_core_values_total']) +
                                ($request['first_lead_com_total'] + $request['second_lead_com_total'] + $request['third_lead_com_total']),
                        ]
                    );
                }
            }

            foreach ($request['core_value_assessments'] as $i => $core_value) {
                if ($core_value['first_evaluator_points'] != ""  && ($core_value['first_status'] == 1)) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $core_value['id'])->update(
                        [
                            'first_points' => $core_value['first_evaluator_points'],
                            'core_val_1st_critical_remarks' => $core_value['evaluator_1st_critical_remarks'],
                            'first_status' => 2,
                            'first_eval_date' => date("Y-m-d H:i:s"),
                            'first_remarks' => $request['performance_evaluators'][0]['first_remarks'],
                            'core_value_total_score' => $request['first_core_values_total'],
                            'overall_total_score' => $request['first_core_values_total'] + $request['second_core_values_total'] + $request['third_core_values_total'],
                            'final_score' => ($request['first_scoring_total'] + $request['second_scoring_total'] + $request['third_scoring_total']) +
                                ($request['first_core_values_total'] + $request['second_core_values_total'] + $request['third_core_values_total']) +
                                ($request['first_lead_com_total'] + $request['second_lead_com_total'] + $request['third_lead_com_total']),
                        ]
                    );
                } else if ($core_value['second_evaluator_points'] != "" && ($core_value['first_status'] == 2  && $core_value['second_status'] == 1)) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $core_value['id'])->update(
                        [
                            'second_points' => $core_value['second_evaluator_points'],
                            'core_val_2nd_critical_remarks' => $core_value['evaluator_2nd_critical_remarks'],
                            'second_status' => 2,
                            'second_eval_date' => date("Y-m-d H:i:s"),
                            'second_remarks' => $request['performance_evaluators'][0]['second_remarks'],
                            'core_value_total_score' => $request['second_core_values_total'],
                            'overall_total_score' => $request['first_core_values_total'] + $request['second_core_values_total'] + $request['third_core_values_total'],
                            'final_score' => ($request['first_scoring_total'] + $request['second_scoring_total'] + $request['third_scoring_total']) +
                                ($request['first_core_values_total'] + $request['second_core_values_total'] + $request['third_core_values_total']) +
                                ($request['first_lead_com_total'] + $request['second_lead_com_total'] + $request['third_lead_com_total']),
                        ]
                    );
                } else if ($core_value['third_evaluator_points'] != "" && ($core_value['second_status'] == 2  && $core_value['third_status'] == 1)) {
                    $performance_evaluation = PerformanceEvaluation::where('id', $core_value['id'])->update(
                        [
                            'third_points' => $core_value['third_evaluator_points'],
                            'core_val_3rd_critical_remarks' => $core_value['evaluator_3rd_critical_remarks'],
                            'third_status' => 2,
                            'third_eval_date' => date("Y-m-d H:i:s"),
                            'third_remarks' => $request['performance_evaluators'][0]['third_remarks'],
                            'core_value_total_score' => $request['third_core_values_total'],
                            'overall_total_score' => $request['first_core_values_total'] + $request['second_core_values_total'] + $request['third_core_values_total'],
                            'final_score' => ($request['first_scoring_total'] + $request['second_scoring_total'] + $request['third_scoring_total']) +
                                ($request['first_core_values_total'] + $request['second_core_values_total'] + $request['third_core_values_total']) +
                                ($request['first_lead_com_total'] + $request['second_lead_com_total'] + $request['third_lead_com_total']),
                        ]
                    );
                }
            }

            if (count($request['leadership_competencies']) > 0) {
                foreach ($request['leadership_competencies'] as $i => $lead_comp) {
                    if ($lead_comp['first_evaluator_points'] != "" && ($lead_comp['first_status'] == 1)) {
                        $performance_evaluation = PerformanceEvaluation::where('id', $lead_comp['id'])->update(
                            [
                                'first_points' => $lead_comp['first_evaluator_points'],
                                'core_val_1st_critical_remarks' => $core_value['evaluator_1st_critical_remarks'],
                                'first_status' => 2,
                                'first_eval_date' => date("Y-m-d H:i:s"),
                                'first_remarks' => $request['performance_evaluators'][0]['first_remarks'],
                                'leadership_total_score' => $request['first_lead_com_total'],
                                'overall_total_score' => $request['first_lead_com_total'] + $request['second_lead_com_total'] + $request['third_lead_com_total'],
                                'final_score' => ($request['first_scoring_total'] + $request['second_scoring_total'] + $request['third_scoring_total']) +
                                    ($request['first_core_values_total'] + $request['second_core_values_total'] + $request['third_core_values_total']) +
                                    ($request['first_lead_com_total'] + $request['second_lead_com_total'] + $request['third_lead_com_total']),
                            ]
                        );
                    } else if ($lead_comp['second_evaluator_points'] != "" && ($lead_comp['first_status'] == 2  && $lead_comp['second_status'] == 1)) {

                        $performance_evaluation = PerformanceEvaluation::where('id', $lead_comp['id'])->update(
                            [
                                'second_points' => $lead_comp['second_evaluator_points'],
                                'core_val_2nd_critical_remarks' => $core_value['evaluator_2nd_critical_remarks'],
                                'second_status' => 2,
                                'second_eval_date' => date("Y-m-d H:i:s"),
                                'second_remarks' => $request['performance_evaluators'][0]['second_remarks'],
                                'leadership_total_score' => $request['second_lead_com_total'],
                                'overall_total_score' => $request['first_lead_com_total'] + $request['second_lead_com_total'] + $request['third_lead_com_total'],
                                'final_score' => ($request['first_scoring_total'] + $request['second_scoring_total'] + $request['third_scoring_total']) +
                                    ($request['first_core_values_total'] + $request['second_core_values_total'] + $request['third_core_values_total']) +
                                    ($request['first_lead_com_total'] + $request['second_lead_com_total'] + $request['third_lead_com_total']),
                            ]
                        );
                    } else if ($lead_comp['third_evaluator_points'] != "" && ($lead_comp['second_status'] == 2  && $lead_comp['third_status'] == 1)) {
                        $performance_evaluation = PerformanceEvaluation::where('id', $lead_comp['id'])->update(
                            [
                                'third_points' => $lead_comp['third_evaluator_points'],
                                'core_val_3rd_critical_remarks' => $core_value['evaluator_3rd_critical_remarks'],
                                'third_status' => 2,
                                'third_eval_date' => date("Y-m-d H:i:s"),
                                'third_remarks' => $request['performance_evaluators'][0]['third_remarks'],
                                'leadership_total_score' => $request['third_lead_com_total'],
                                'overall_total_score' => $request['first_lead_com_total'] + $request['second_lead_com_total'] + $request['third_lead_com_total'],
                                'final_score' => ($request['first_scoring_total'] + $request['second_scoring_total'] + $request['third_scoring_total']) +
                                    ($request['first_core_values_total'] + $request['second_core_values_total'] + $request['third_core_values_total']) +
                                    ($request['first_lead_com_total'] + $request['second_lead_com_total'] + $request['third_lead_com_total']),
                            ]
                        );
                    }
                }
            }

            DB::commit();
            return $this->response(200, 'Evaluation Closed!', $performance_evaluation);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function eIndex($request)
    {
        DB::beginTransaction();
        try {

            $core_values = [];
            $leadership_comps = [];
            $employee_evals = [];

            $employee_evaluation_status = PerformanceEvaluation::with('core_values')
                ->select('employee_eval_status', 'quarter', 'year', 'employee_remarks')->distinct()
                ->where('user_id', Auth::user()->id)
                ->where('quarter', $request['quarter'])
                ->where('year', $request['year'])
                ->where('core_values_id', '!=', null)
                ->get();

            $kpi_tagged_core_values = PerformanceEvaluation::with('core_values')
                ->where('user_id', Auth::user()->id)
                ->where('quarter', $request['quarter'])
                ->where('year', $request['year'])
                ->where('core_values_id', '!=', null)
                ->get();

            $kpi_tagged_leadership_comps = PerformanceEvaluation::with('leadership_comp')
                ->where('user_id', Auth::user()->id)
                ->where('quarter', $request['quarter'])
                ->where('year', $request['year'])
                ->where('leadership_comp_id', '!=', null)
                ->get();

            foreach ($employee_evaluation_status as $j => $employee_eval) {
                $employee_evals[] = [
                    'employee_eval_status' => $employee_eval->employee_eval_status == '' ? 1 : $employee_eval->employee_eval_status,
                    'quarter' => $employee_eval->quarter,
                    'year' => $employee_eval->year,
                    'employee_remarks' => $employee_eval->employee_remarks,
                ];
            }

            foreach ($kpi_tagged_core_values as $j => $core_val) {
                $core_values[] = [
                    'id' => $core_val->id,
                    'description' => $core_val->core_values->description,
                    'employee_eval_status' => $core_val->employee_eval_status,
                    'employee_self_eval_points' => $core_val->employee_self_points == '' ? null : $core_val->employee_self_points,
                    'employee_self_points' => $core_val->employee_self_points,
                    'employee_critical_remarks' => $core_val->critical_remarks == '' ? null : $core_val->critical_remarks,
                    'critical_remarks' => $core_val->critical_remarks,
                    'employee_eval_date' => $core_val->employee_eval_date,
                    'employee_remarks' => $core_val->employee_remarks,
                ];
            }


            foreach ($kpi_tagged_leadership_comps as $j => $lead_comp) {
                $leadership_comps[] = [
                    'id' => $lead_comp->id,
                    'description' => $lead_comp->leadership_comp->description,
                    'employee_eval_status' => $lead_comp->employee_eval_status,
                    'employee_self_eval_points' => $lead_comp->employee_self_points == '' ? null : $lead_comp->employee_self_points,
                    'employee_self_points' => $lead_comp->employee_self_points,
                    'employee_critical_remarks' => $lead_comp->critical_remarks == '' ? null : $lead_comp->critical_remarks,
                    'critical_remarks' => $lead_comp->critical_remarks,
                    'employee_eval_date' => $lead_comp->employee_eval_date,
                    'employee_remarks' => $lead_comp->employee_remarks,
                ];
            }


            DB::commit();
            return $this->response(200, 'Performance Evaluation', compact('core_values', 'leadership_comps', 'employee_evals'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function eSaveValidation($request)
    {
        DB::beginTransaction();
        try {
            if (count($request['leadership_comps']) > 0) {

                $rules = [
                    'employee_remarks' => 'sometimes',
                ];

                foreach ($request['core_values'] as $i => $core_vals) {
                    $first_eval_points[$i] = $core_vals['employee_self_eval_points'];

                    if ($first_eval_points[$i] > 3) {
                        $rules['core_values.' . $i . '.employee_critical_remarks'] = 'required';
                    }
                    $rules['core_values.' . $i . '.employee_self_eval_points'] = 'required';
                }

                foreach ($request['leadership_comps'] as $i => $core_vals) {
                    $first_eval_points[$i] = $core_vals['employee_self_eval_points'];

                    if ($first_eval_points[$i] > 3) {
                        $rules['leadership_comps.' . $i . '.employee_critical_remarks'] = 'required';
                    }
                    $rules['leadership_comps.' . $i . '.employee_self_eval_points'] = 'required';
                }

                // dd($rules);

                $validator = Validator::make(
                    $request,
                    $rules,
                    [
                        'core_values.*.employee_self_eval_points.required' => 'This field is required.',
                        'core_values.*.employee_critical_remarks.required' => 'This field is required.',
                        'leadership_comps.*.employee_self_eval_points.required' => 'This field is required.',
                        'leadership_comps.*.employee_critical_remarks.required' => 'This field is required.',
                    ]
                );
            } else {
                $rules = [
                    'employee_remarks' => 'sometimes',
                ];

                foreach ($request['core_values'] as $i => $core_vals) {
                    $first_eval_points[$i] = $core_vals['employee_self_eval_points'];

                    if ($first_eval_points[$i] > 3) {
                        $rules['core_values.' . $i . '.employee_critical_remarks'] = 'required';
                    }
                    $rules['core_values.' . $i . '.employee_self_eval_points'] = 'required';
                }

                $validator = Validator::make(
                    $request,
                    $rules,
                    [
                        'core_values.*.employee_self_eval_points.required' => 'This field is required.',
                        'core_values.*.employee_critical_remarks.required' => 'This field is required.',
                    ]
                );
            }

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function eSave($request)
    {
        DB::beginTransaction();
        try {

            $response = $this->eSaveValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $performance_evaluation = $response['result'];

            foreach ($request['core_values'] as $i => $core_value) {
                if ($core_value['employee_self_eval_points'] != "") {
                    $performance_evaluation = PerformanceEvaluation::where('id', $core_value['id'])->update(
                        [
                            'employee_self_points' => $core_value['employee_self_eval_points'],
                            'critical_remarks' => $core_value['employee_critical_remarks'],
                            'employee_remarks' => $request['employee_remarks'],
                            'employee_eval_date' => date("Y-m-d H:i:s"),
                            'employee_eval_status' => 2,
                        ]
                    );
                }
            }

            if (count($request['leadership_comps']) > 0) {
                foreach ($request['leadership_comps'] as $i => $lead_comp) {
                    if ($lead_comp['employee_self_eval_points'] != "") {
                        $performance_evaluation = PerformanceEvaluation::where('id', $lead_comp['id'])->update(
                            [
                                'employee_self_points' => $lead_comp['employee_self_eval_points'],
                                'critical_remarks' => $lead_comp['employee_critical_remarks'],
                                'employee_remarks' => $request['employee_remarks'],
                                'employee_eval_date' => date("Y-m-d H:i:s"),
                                'employee_eval_status' => 2,
                            ]
                        );
                    }
                }
            }

            DB::commit();
            return $this->response(200, 'Evaluation Saved!', $performance_evaluation);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function eIndexMobile($request)
    {
        DB::beginTransaction();
        try {
            $this->kpi_tagged_core_values = PerformanceEvaluation::with('core_values')
                ->where('user_id', Auth::user()->id)
                ->where('quarter', $request['quarter'])
                ->where('year', $request['year'])
                ->where('core_values_id', '!=', null)->get();

            $this->kpi_tagged_leadership_comps = PerformanceEvaluation::with('leadership_comp')
                ->where('user_id', Auth::user()->id)
                ->where('quarter', $request['quarter'])
                ->where('year', $request['year'])
                ->where('leadership_comp_id', '!=', null)->get();

            foreach ($this->kpi_tagged_core_values as $j => $core_val) {
                $core_value_assessments[] = [
                    'id' => $core_val->id,
                    'description' => $core_val->core_values->description,
                    'employee_eval_status' => $core_val->employee_eval_status,
                    'employee_self_eval_points' => $core_val->employee_self_points == '' ? null : $core_val->employee_self_points,
                    'employee_self_points' => $core_val->employee_self_points,
                    'employee_critical_remarks' => $core_val->critical_remarks == '' ? null : $core_val->critical_remarks,
                    'critical_remarks' => $core_val->critical_remarks,
                    'employee_eval_date' => $core_val->employee_eval_date,
                ];
            }

            foreach ($this->kpi_tagged_leadership_comps as $j => $lead_comp) {
                $leadership_competencies[] = [
                    'id' => $lead_comp->id,
                    'description' => $lead_comp->leadership_comp->description,
                    'employee_eval_status' => $lead_comp->employee_eval_status,
                    'employee_self_eval_points' => $lead_comp->employee_self_points == '' ? null : $lead_comp->employee_self_points,
                    'employee_self_points' => $lead_comp->employee_self_points,
                    'employee_critical_remarks' => $lead_comp->critical_remarks == '' ? null : $lead_comp->critical_remarks,
                    'critical_remarks' => $lead_comp->critical_remarks,
                    'employee_eval_date' => $lead_comp->employee_eval_date,
                ];
            }

            DB::commit();
            return $this->response(200, 'Performance Evaluation',  compact('core_value_assessments', 'leadership_competencies'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
