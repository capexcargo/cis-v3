<?php

namespace App\Repositories\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\EvaluatorManagementInterface;
use App\Models\Hrim\EvaluatorManagement;
use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class EvaluatorManagementRepository implements EvaluatorManagementInterface
{
    use ResponseTrait;

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $evaluator = User::with('userDetails', 'division')->findOrFail($id);
            if (!$evaluator) return $this->response(404, 'Evaluator', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Evaluator', $evaluator);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make(
                $request,
                [
                    'first_evaluator' => 'required',
                    'second_evaluator' => 'required',
                    'third_evaluator' => 'sometimes',
                ],
                [
                    'first_evaluator.required' => 'First Evaluator is required.',
                    'second_evaluator.required' => 'Second Evaluator is required.',
                ]
            );

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $evaluator = EvaluatorManagement::updateOrCreate([
                'user_id' => $id,
            ], [
                'user_id' => $id,
                'first_approver' => $validated['first_evaluator'],
                'second_approver' => $validated['second_evaluator'],
                'third_approver' => $validated['third_evaluator'],
            ]);

            $evaluator->evaluator()->update([
                'user_id' => $id,
                'first_evaluator' => $validated['first_evaluator'],
                'second_evaluator' => $validated['second_evaluator'],
                'third_evaluator' => $validated['third_evaluator'],
            ]);

            DB::commit();
            return $this->response(200, 'Evaluator has been successfully updated!', $evaluator);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
