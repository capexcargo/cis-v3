<?php

namespace App\Repositories\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\TrainingAndRefresherModulesInterface;
use App\Models\Hrim\TrainingAndRefreshers;
use App\Models\Hrim\TrainingAndRefreshersAttachment;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class TrainingAndRefresherModulesRepository implements TrainingAndRefresherModulesInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $training_and_refreshers = TrainingAndRefreshersAttachment::with('trainingRef')
                ->whereHas('trainingRef', function ($query) use ($request) {
                    $query->when($request['position'] ?? false, function ($query) use ($request) {
                        $query->where('position', $request['position']);
                    });
                    $query->when($request['division'] ?? false, function ($query) use ($request) {
                        $query->where('tagged_division', $request['division']);
                    });
                    $query->when($request['sort_field'], function ($query) use ($request) {
                        $query->orderBy($request['sort_field'], $request['sort_type']);
                    });
                })
                ->get();
            // ->paginate($request['paginate']);

            $get_tagged_position = TrainingAndRefreshers::get();

            DB::commit();

            return $this->response(200, 'Training And Refreshers', compact('training_and_refreshers', 'get_tagged_position'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request, $sub_type)
    {
        DB::beginTransaction();
        try {
            if (!($request['is_all_tagged'] || $request['division'])) {
                if ($sub_type == 'create') {
                    $rules = [
                        'title' => 'required',
                        'description' => 'required',
                        'tag_atleast_one' => 'required',
                        'attachments' => 'required',
                        'attachments.*.attachment.required' => 'required|mimes:mp4,mov',
                    ];
                } else {
                    $rules = [
                        'title' => 'required',
                        'description' => 'required',
                        'tag_atleast_one' => 'required',
                        'attachments' => 'required',
                        'attachments.*.attachment.sometimes' => 'sometimes|mimes:mp4,mov',
                    ];
                }
            } else {
                if ($request['division']) {
                    if (count($request['positions']) > 0) {
                        if ($sub_type == 'create') {
                            $rules = [
                                'title' => 'required',
                                'description' => 'required',
                                'is_all_tagged' => 'nullable',
                                'division' => 'nullable',
                                'positions' => 'required',
                                'positions.*.position' => 'required',
                                'attachments' => 'required',
                                'attachments.*.attachment.required' => 'required|mimes:mp4,mov',
                            ];
                        } else {
                            $rules = [
                                'title' => 'required',
                                'description' => 'required',
                                'is_all_tagged' => 'nullable',
                                'division' => 'nullable',
                                'positions' => 'required',
                                'positions.*.position' => 'required',
                                'attachments' => 'required',
                                'attachments.*.attachment.sometimes' => 'sometimes|mimes:mp4,mov',
                            ];
                        }
                    } else {
                        if ($sub_type == 'create') {
                            $rules = [
                                'title' => 'required',
                                'description' => 'required',
                                'is_all_tagged' => 'nullable',
                                'division' => 'nullable',
                                'positions' => 'required',
                                'positions.*.position' => 'required',
                                'attachments' => 'required',
                                'attachments.*.attachment.required' => 'required|mimes:mp4,mov',
                            ];
                        } else {
                            $rules = [
                                'title' => 'required',
                                'description' => 'required',
                                'is_all_tagged' => 'nullable',
                                'division' => 'nullable',
                                'positions' => 'required',
                                'positions.*.position' => 'required',
                                'attachments' => 'required',
                                'attachments.*.attachment.sometimes' => 'sometimes|mimes:mp4,mov',
                            ];
                        }
                    }
                } else {
                    if ($sub_type == 'create') {
                        $rules = [
                            'title' => 'required',
                            'description' => 'required',
                            'is_all_tagged' => 'nullable',
                            'division' => 'nullable',
                            'positions' => 'sometimes',
                            'positions.*.position' => 'sometimes',
                            'attachments' => 'required',
                            'attachments.*.attachment.required' => 'required|mimes:mp4,mov',
                        ];
                    } else {
                        $rules = [
                            'title' => 'required',
                            'description' => 'required',
                            'is_all_tagged' => 'nullable',
                            'division' => 'nullable',
                            'positions' => 'sometimes',
                            'positions.*.position' => 'sometimes',
                            'attachments' => 'required',
                            'attachments.*.attachment.sometimes' => 'sometimes|mimes:mp4,mov',
                        ];
                    }
                }
            }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'tag_atleast_one.required' => 'Tag to All Positions or Division.',
                    'positions.required' => 'Select at least one position',
                    'attachments.*.attachment.required' => 'Video file is required.',
                    'attachments.*.attachment.mimes' => 'Video file must be mp4 or mov.',
                ]
            );

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request, 'create');

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $last_video_ref = TrainingAndRefreshers::select('id')->latest()->first();
            $last_inserted_id = $last_video_ref->id ?? 0 + 1;
            $video_ref = "Video-" . str_pad(($last_inserted_id), 5, "0", STR_PAD_LEFT);

            if ($validated['positions']) {
                foreach ($validated['positions'] as $i => $val) {
                    $training_and_refresher = TrainingAndRefreshers::create([
                        'title' => $validated['title'],
                        'description' => $validated['description'],
                        'video_ref' => $video_ref,
                        'tagged_division' => $validated['division'],
                        'position' => $val['position'],
                    ]);
                }
            } else {
                $training_and_refresher = TrainingAndRefreshers::create([
                    'title' => $validated['title'],
                    'description' => $validated['description'],
                    'video_ref' => $video_ref,
                    'is_tag_all_position' => $validated['is_all_tagged'],
                ]);
            }

            $response = $this->attachment($training_and_refresher, $validated['attachments'], $video_ref, 'create');
            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $attachment = $response['result'];

            DB::commit();
            return $this->response(200, 'New Training and Refresher Module has been successfully uploaded!', $training_and_refresher);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $training_and_refresher = TrainingAndRefreshersAttachment::with('trainingRef')->findOrFail($id);
            if (!$training_and_refresher) return $this->response(404, 'Training And Refresher', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Training And Refresher', $training_and_refresher);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $training_and_refresher = $response['result'];

            $response = $this->validation($request, 'update');
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            if ($validated['positions']) {
                foreach ($validated['positions'] as $i => $val) {
                    $training_and_refresher->trainingRef()
                        ->where('is_tag_all_position', 1)
                        ->where('video_ref', $training_and_refresher->video_ref)
                        ->delete();

                    $training_and_refresher->trainingRef()->updateOrCreate(
                        [
                            'position' => $val['position'],
                            'video_ref' => $training_and_refresher->video_ref,
                        ],
                        [
                            'title' => $validated['title'],
                            'description' => $validated['description'],
                            'video_ref' => $training_and_refresher->video_ref,
                            'tagged_division' => $validated['division'],
                            'position' => $val['position'],
                        ]
                    );

                    $positionsToDelete = collect($validated['positions'])->pluck('position')->filter();

                    $training_and_refresher->trainingRef()
                        ->where('video_ref', $training_and_refresher->video_ref)
                        ->whereNotIn('position', $positionsToDelete)
                        ->delete();
                }
            } else {
                $training_and_refresher->trainingRef()
                    ->where('is_tag_all_position', 0)
                    ->where('video_ref', $training_and_refresher->video_ref)
                    ->delete();

                $training_and_refresher->trainingRef()->updateOrCreate(
                    [
                        'video_ref' => $training_and_refresher->video_ref,
                    ],
                    [
                        'title' => $validated['title'],
                        'description' => $validated['description'],
                        'video_ref' => $training_and_refresher->video_ref,
                        'is_tag_all_position' => $validated['is_all_tagged'],
                    ]
                );
            }

            $response = $this->attachment($training_and_refresher, $validated['attachments'], $training_and_refresher->video_ref, 'update');
            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $attachment = $response['result'];

            DB::commit();
            return $this->response(200, 'Training and Refresher has been successfully updated!', $validated);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function attachment($training_and_refresher, $attachments, $video_ref, $sub_type)
    {
        DB::beginTransaction();
        try {
            $date = date('Y/m/d', strtotime($training_and_refresher->created_at ?? null));

            foreach ($attachments as $attachment) {
                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $training_and_refresher->attachments()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(" ", "_", 'hrim/' . $date . '/' . 'training-and-refreshers/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.video_type'))) {
                        Storage::disk('hrim_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }

                    TrainingAndRefreshersAttachment::updateOrCreate(
                        [
                            'id' => $attachment['id'],
                        ],
                        [
                            'video_ref' => $video_ref,
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                            'original_filename' =>  $attachment['attachment']->getClientOriginalName(),
                        ]
                    );
                }
            }

            DB::commit();
            return $this->response(200, 'Training and Refresher attachment successfully attached', $attachments);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $training_and_refresher = $response['result'];

            $training_and_refresher->delete();
            $training_and_refresher->trainingRef()->delete();

            DB::commit();
            return $this->response(200, 'Module has been successfully deleted!', $training_and_refresher);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
