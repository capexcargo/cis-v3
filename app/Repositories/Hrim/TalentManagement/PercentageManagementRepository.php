<?php

namespace App\Repositories\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\PercentageManagementInterface;
use App\Models\Hrim\BreakdownPercentage;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PercentageManagementRepository implements PercentageManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {

            $percentage_managements = BreakdownPercentage::with('job_level')->when($request['sort_field'], function ($query) use ($request) {
                $query->orderBy($request['sort_field'], $request['sort_type']);
            })->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Percentage Management List', compact('percentage_managements'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'job_level' => 'required|unique:hrim_performance_breakdown_pct,job_level_id',
                'goal_percentage' => 'required',
                'core_value_percentage' => 'required',
                'lead_com_percentage' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'job_level' => 'required|unique:hrim_performance_breakdown_pct,job_level_id' . $id,
                'goal_percentage' => 'required',
                'core_value_percentage' => 'required',
                'lead_com_percentage' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    
    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $percentage = BreakdownPercentage::create([
                'job_level_id' => $validated['job_level'],
                'goals' => $validated['goal_percentage'],
                'core_values' => $validated['core_value_percentage'],
                'leadership_competencies' => $validated['lead_com_percentage'],
            ]);

            DB::commit();
            return $this->response(200, 'New Percentage Successfully Created!', $percentage);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $percentage = BreakdownPercentage::findOrFail($id);
            if (!$percentage) return $this->response(404, 'Percentage', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Percentage', $percentage);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {

        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $percentage = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $percentage->update([
                'job_level_id' => $validated['job_level'],
                'goals' => $validated['goal_percentage'],
                'core_values' => $validated['core_value_percentage'],
                'leadership_competencies' => $validated['lead_com_percentage'],
            ]);

            DB::commit();
            return $this->response(200, 'Percentage has been successfully updated!', $percentage);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $adjustment = BreakdownPercentage::find($id);
            if (!$adjustment) return $this->response(404, 'Percentage', 'Not Found!');
            $adjustment->delete();

            DB::commit();
            return $this->response(200, 'Percentage Successfully Deleted!', $adjustment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
