<?php

namespace App\Repositories\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\LeadershipCompetenciesMgmtInterface;
use App\Models\Hrim\LeadershipCompetenciesMgmt;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class LeadershipCompetenciesMgmtRepository implements LeadershipCompetenciesMgmtInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $leadership_competencies_management = LeadershipCompetenciesMgmt:: when($request['leadership_competencies'] ?? false, function ($query) use ($request) {
                    $query->where('description', 'like', '%' . $request['leadership_competencies'] . '%');
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);
            DB::commit();
            return $this->response(200, 'Leadership Competencies List',  $leadership_competencies_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'leadership_competencies' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $leadership_competencies = LeadershipCompetenciesMgmt::create([
                'description' => $validated['leadership_competencies'],
            ]);

            DB::commit();
            return $this->response(200, 'New Leadership Competency item has been successfully added!', $leadership_competencies);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $leadership_competencies = LeadershipCompetenciesMgmt::findOrFail($id);
            if (!$leadership_competencies) return $this->response(404, 'Leadership Competencies', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Leadership Competencies', $leadership_competencies);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'leadership_competencies' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $leadership_competencies = $response['result'];
            
            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $leadership_competencies->update([
                'description' => $validated['leadership_competencies'],
            ]);

            DB::commit();
            return $this->response(200, 'Leadership Competency item has been successfully updated!', $leadership_competencies);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $leadership_competencies = LeadershipCompetenciesMgmt::find($id);
            if (!$leadership_competencies) return $this->response(404, 'Leadership Competencies', 'Not Found!');
            $leadership_competencies->delete();

            DB::commit();
            return $this->response(200, 'Leadership Competencies successfully deleted!', $leadership_competencies);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
