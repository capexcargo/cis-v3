<?php

namespace App\Repositories\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\KraManagementInterface;
use App\Models\Hrim\KraManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class KraManagementRepository implements KraManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $kra_managements = KraManagement::with('job_levels')
                ->when($request['kra_name'] ?? false, function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request['kra_name'] . '%');
                })
                ->when($request['type'] ?? false, function ($query) use ($request) {
                    $query->where('type', $request['type']);
                })
                ->when($request['status'] ?? false, function ($query) use ($request) {
                    $query->where('status_id', $request['status']);
                })
                ->when($request['quarter'] ?? false, function ($query) use ($request) {
                    $query->where('quarter', $request['quarter']);
                })
                ->when($request['year'] ?? false, function ($query) use ($request) {
                    $query->where('year', $request['year']);
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);
            DB::commit();
            return $this->response(200, 'KRA List',  $kra_managements);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'kra_name' => 'required',
                'type' => 'required',
                'quarter' => 'required',
                'year' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $kra = KraManagement::create([
                'name' => $validated['kra_name'],
                'type' => $validated['type'],
                'quarter' => $validated['quarter'],
                'year' => $validated['year'],
            ]);

            DB::commit();
            return $this->response(200, 'New KRA has been successfully added!', $kra);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $kra = KraManagement::with('job_levels')->findOrFail($id);
            if (!$kra) return $this->response(404, 'KRA', 'Not Found!');

            DB::commit();
            return $this->response(200, 'KRA', $kra);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'kra_name' => 'required',
                'type' => 'required',
                'quarter' => 'required',
                'year' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $kra = $response['result'];


            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $kra->update([
                'name' => $validated['kra_name'],
                'type' => $validated['type'],
                'quarter' => $validated['quarter'],
                'year' => $validated['year'],
            ]);

            DB::commit();
            return $this->response(200, 'KRA has been successfully updated!', $kra);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $kra = KraManagement::find($id);
            if (!$kra) return $this->response(404, 'KRA', 'Not Found!');
            $kra->delete();

            DB::commit();
            return $this->response(200, 'KRA has been successfully deleted!', $kra);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
