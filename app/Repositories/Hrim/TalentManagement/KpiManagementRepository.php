<?php

namespace App\Repositories\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\KpiManagementInterface;
use App\Models\Hrim\KpiManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class KpiManagementRepository implements KpiManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $kpi_managements = KpiManagement::when($request['kpi_name'] ?? false, function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request['kpi_name'] . '%');
            })
                ->when($request['quarter'] ?? false, function ($query) use ($request) {
                    $query->where('quarter', $request['quarter']);
                })
                ->when($request['year'] ?? false, function ($query) use ($request) {
                    $query->where('year', $request['year']);
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);
            DB::commit();
            return $this->response(200, 'KPI List',  $kpi_managements);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'kpi_name' => 'required',
                'quarter' => 'required',
                'year' => 'required',
                'target' => 'sometimes',
                'points' => 'required',
                'remarks' => 'sometimes',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $kpi = KpiManagement::create([
                'name' => $validated['kpi_name'],
                'quarter' => $validated['quarter'],
                'year' => $validated['year'],
                'target' => $validated['target'],
                'points' => $validated['points'],
                'remarks' => $validated['remarks'],
            ]);

            DB::commit();
            return $this->response(200, 'New KPI has been successfully added!', $kpi);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $kpi = KpiManagement::findOrFail($id);
            if (!$kpi) return $this->response(404, 'KPI', 'Not Found!');

            DB::commit();
            return $this->response(200, 'KPI', $kpi);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'kpi_name' => 'required',
                'quarter' => 'required',
                'year' => 'required',
                'target' => 'sometimes',
                'points' => 'required',
                'remarks' => 'sometimes',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $kpi = $response['result'];


            $response = $this->updateValidation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $kpi->update([
                'name' => $validated['kpi_name'],
                'quarter' => $validated['quarter'],
                'year' => $validated['year'],
                'target' => $validated['target'],
                'points' => $validated['points'],
                'remarks' => $validated['remarks'],
            ]);

            DB::commit();
            return $this->response(200, 'KPI has been successfully updated!', $kpi);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $kpi = KpiManagement::find($id);
            if (!$kpi) return $this->response(404, 'KPI', 'Not Found!');
            $kpi->delete();

            DB::commit();
            return $this->response(200, 'KPI successfully deleted!', $kpi);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
