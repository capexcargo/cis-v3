<?php

namespace App\Repositories\Hrim\Attendance;

use App\Interfaces\Hrim\Attendance\ScheduleAdjustmentInterface;
use App\Models\Hrim\ScheduleAdjustment;
use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ScheduleAdjustmentRepository implements ScheduleAdjustmentInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $users = User::with(['userDetails' => function ($query) {
                $query->with(
                    'branch',
                    'workSchedule',
                    'position',
                    'jobLevel',
                    'department',
                    'division',
                    'employmentCategory',
                    'employmentStatus',
                    'gender',
                    'suffix'
                );
            }])
                ->withCount(['scheduleAdjustments as total_filed_sar' => function ($query) use ($request) {
                    $query->when($request['status'] ?? false, function ($query) use ($request) {
                        $query->when($request['status'] == 'sar_for_approval', function ($query) {
                            $query->whereIn('final_status_id', [1, 2, 4]);
                        });
                    })
                        ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                            $query->where('users.division_id', Auth::user()->division_id);
                        });
                }])
                ->whereHas('scheduleAdjustments', function ($query) use ($request) {
                    $query->when($request['status'] ?? false, function ($query) use ($request) {
                        $query->when($request['status'] == 'sar_for_approval', function ($query) {
                            $query->whereIn('final_status_id', [1, 2, 4]);
                        });
                    });
                })
                ->whereHas('userDetails', function ($query) use ($request) {
                    $query->when($request['branch'] ?? false, function ($query) use ($request) {
                        $query->where('branch_id', $request['branch']);
                    })
                        ->when($request['employment_category'] ?? false, function ($query) use ($request) {
                            $query->where('employment_category_id', $request['employment_category']);
                        });
                })
                ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                    $query->where('division_id', Auth::user()->division_id);
                })
                ->when($request['employee_id'] ?? false, function ($query) use ($request) {
                    $query->whereHas('userDetails', function ($query) use ($request) {
                        $query->where('employee_number', $request['employee_id']);
                    });
                })
                ->when($request['employee_name'] ?? false, function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request['employee_name'] . '%');
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Sar List', $users);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function showUserScheduleAdjustment($id, $request = [])
    {
        DB::beginTransaction();
        try {
            $user = User::find($id);
            $schedule_adjustments = ScheduleAdjustment::with(
                'user',
                'currentWorkSchedule',
                'newWorkSchedule',
                'firstApprover',
                'secondApprover',
                'thirdApprover',
                'finalStatus',
            )
                ->where('user_id', $id)
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'User Schedule Adjustment List', compact('user', 'schedule_adjustments'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id, $request = [])
    {
        DB::beginTransaction();
        try {
            $schedule_adjustment = ScheduleAdjustment::with(
                'user',
                'currentWorkSchedule',
                'newWorkSchedule',
                'firstApprover',
                'secondApprover',
                'thirdApprover',
                'finalStatus',
            )->firstWhere([['user_id', $request['user_id']], ['id', $id]]);
            if (!$schedule_adjustment) return $this->response(404, 'Schedule Adjustment', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Schedule Adjustment', $schedule_adjustment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'user_id' => 'required',
                'approver' => 'required',
                'remarks' => 'required_if:is_approved,0',
                'is_approved' => 'sometimes',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function approve($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $response = $this->show($id, ['user_id' => $validated['user_id']]);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $schedule_adjustment = $response['result'];
            if ($schedule_adjustment->first_approver && $validated['approver'] == 'first') {
                if ($schedule_adjustment->first_approver == Auth::user()->id || Auth::user()->level_id == 5 && $schedule_adjustment->first_approver) {
                    if (!$validated['is_approved'] && !$validated['remarks']) {
                        return $this->response(400, 'Bad Request', [
                            'remarks' => 'The first approver remarks field is required.'
                        ]);
                    }

                    if ($schedule_adjustment->second_status && !$validated['is_approved']) {
                        return $this->response(400, 'Bad Request', [
                            'approver' => 'The second approver must be disapproved.'
                        ]);
                    } else {
                        $schedule_adjustment->update([
                            'first_status' => $validated['is_approved'] ?? 0,
                            'first_approval_date' => date("Y-m-d"),
                            'first_approver_remarks' => $validated['remarks'],
                        ]);
                    }
                }
            }

            if ($schedule_adjustment->second_approver && $validated['approver'] == 'second') {
                if ($schedule_adjustment->first_status || !$validated['is_approved']) {
                    if ($schedule_adjustment->second_approver == Auth::user()->id || Auth::user()->level_id == 5 && $schedule_adjustment->second_approver) {
                        if (!$validated['is_approved'] && !$validated['remarks']) {
                            return $this->response(400, 'Bad Request', [
                                'remarks' => 'The approver remarks 2 field is required.'
                            ]);
                        }

                        if ($schedule_adjustment->third_status && !$validated['is_approved']) {
                            return $this->response(400, 'Bad Request', [
                                'approver' => 'The third approver must be disapproved.'
                            ]);
                        } else {
                            $schedule_adjustment->update([
                                'second_status' => $validated['is_approved'] ?? 0,
                                'second_approval_date' => date("Y-m-d"),
                                'second_approver_remarks' => $validated['remarks'],
                            ]);
                        }
                    }
                } else {
                    return $this->response(400, 'Bad Request', [
                        'approver' => 'The first approver must be approved.'
                    ]);
                }
            }


            if ($schedule_adjustment->third_approver && $validated['approver'] == 'third') {
                if ($schedule_adjustment->second_status || !$validated['is_approved']) {
                    if ($schedule_adjustment->third_approver == Auth::user()->id || Auth::user()->level_id == 5 && $schedule_adjustment->third_approver) {
                        if (!$validated['is_approved'] && !$validated['remarks']) {
                            return $this->response(400, 'Bad Request', [
                                'remarks' => 'The third approver remarks field is required.'
                            ]);
                        }

                        $schedule_adjustment->update([
                            'third_status' => $validated['is_approved'],
                            'third_approval_date' => date("Y-m-d"),
                            'third_approver_remarks' => $validated['remarks'],
                        ]);
                    }
                } else {
                    return $this->response(400, 'Bad Request', [
                        'approver' => 'The second approver must be approved.'
                    ]);
                }
            }




            $final_approved = true;
            if ($schedule_adjustment->first_approver && !$schedule_adjustment->first_status) {
                $final_approved = false;
            }
            if ($schedule_adjustment->second_approver && !$schedule_adjustment->second_status) {
                $final_approved = false;
            }
            if ($schedule_adjustment->third_approver && !$schedule_adjustment->third_status) {
                $final_approved = false;
            }

            if ($schedule_adjustment->third_approver && !$schedule_adjustment->third_status && $schedule_adjustment->third_approver_remarks) {
                $schedule_adjustment->update([
                    'final_status_id' => 4
                ]);
            } elseif ($schedule_adjustment->second_approver && !$schedule_adjustment->second_status && $schedule_adjustment->second_approver_remarks) {
                $schedule_adjustment->update([
                    'final_status_id' => 4
                ]);
            } elseif ($schedule_adjustment->first_approver && !$schedule_adjustment->first_status && $schedule_adjustment->first_approver_remarks) {
                $schedule_adjustment->update([
                    'final_status_id' => 4
                ]);
            } else {
                $schedule_adjustment->update([
                    'final_status_id' => ($final_approved ? 3 : 2)
                ]);
            }

            if ($final_approved) {
                if ($schedule_adjustment->inclusive_date_from == date('Y-m-d')) {
                    $user = User::find($validated['user_id']);
                    $user->userDetails()->update([
                        'schedule_id' => $schedule_adjustment->new_work_schedule_id,
                    ]);
                }
            }

            if ($validated['approver'] == "bypass") {
                if ($validated['is_approved']) {
                    $schedule_adjustment->update([
                        'admin_approver' => Auth::user()->id,
                        'admin_approval_date' => date("Y-m-d"),
                        'admin_approver_remarks' => $validated['remarks'],
                        'final_status_id' => 3
                    ]);

                    if ($schedule_adjustment->inclusive_date_from == date('Y-m-d')) {
                        $user = User::find($validated['user_id']);
                        $user->userDetails()->update([
                            'schedule_id' => $schedule_adjustment->new_work_schedule_id,
                        ]);
                    }
                } else {
                    $schedule_adjustment->update([
                        'admin_approver' => Auth::user()->id,
                        'admin_approval_date' => date("Y-m-d"),
                        'admin_approver_remarks' => $validated['remarks'],
                        'final_status_id' => 4
                    ]);


                    $user = User::find($validated['user_id']);

                    $user->userDetails()->update([
                        'schedule_id' => $schedule_adjustment->current_work_schedule_id,
                    ]);
                }
            }


            DB::commit();
            return $this->response(200, ('Schedule Adjustment Request has been successfully ' . ($validated['is_approved'] ? 'approved!' : 'declined!')), $request);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // public function approve($id, $request)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $response = $this->approveValidation($request);
    //         if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
    //         $validated = $response['result'];

    //         $response = $this->show($id, ['user_id' => $validated['user_id']]);
    //         if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
    //         $schedule_adjustment = $response['result'];

    //         $final_approved = false;

    //         if ($schedule_adjustment->first_approver) {
    //             if ($schedule_adjustment->first_approver == $validated['user_id'] || Auth::user()->level_id == 5 && $schedule_adjustment->first_approver) {
    //                 if (!$validated['is_first_approved'] && !$validated['first_approver_remarks']) {
    //                     return $this->response(400, 'Bad Request', [
    //                         'first_approver_remarks' => 'The first approver remarks field is required.'
    //                     ]);
    //                 }

    //                 if ($validated['is_second_approved'] && !$validated['is_first_approved']) {
    //                     return $this->response(400, 'Bad Request', [
    //                         'second_approver_id' => 'The second approver must be disapproved.'
    //                     ]);
    //                 } else {
    //                     $schedule_adjustment->update([
    //                         'first_status' => $validated['is_first_approved'] ?? 0,
    //                         'first_approval_date' => date("Y-m-d"),
    //                         'first_approver_remarks' => $validated['first_approver_remarks'],
    //                     ]);
    //                 }

    //                 if ($validated['is_first_approved']) {
    //                     $final_approved = true;
    //                 } else {
    //                     $final_approved = false;
    //                 }
    //             }
    //         }

    //         if ($schedule_adjustment->second_approver) {
    //             if ($validated['is_first_approved'] || !$validated['is_second_approved']) {
    //                 if ($schedule_adjustment->second_approver == $validated['user_id'] || Auth::user()->level_id == 5 && $schedule_adjustment->second_approver) {
    //                     if (!$validated['is_second_approved'] && !$validated['second_approver_remarks']) {
    //                         return $this->response(400, 'Bad Request', [
    //                             'second_approver_remarks' => 'The approver remarks 2 field is required.'
    //                         ]);
    //                     }

    //                     if ($validated['is_third_approved'] && !$validated['is_second_approved']) {
    //                         return $this->response(400, 'Bad Request', [
    //                             'third_approver_id' => 'The third approver must be disapproved.'
    //                         ]);
    //                     } else {
    //                         $schedule_adjustment->update([
    //                             'second_status' => $validated['is_second_approved'] ?? 0,
    //                             'second_approval_date' => date("Y-m-d"),
    //                             'second_approver_remarks' => $validated['second_approver_remarks'],
    //                         ]);
    //                     }
    //                 }

    //                 if ($validated['is_second_approved']) {
    //                     $final_approved = true;
    //                 } else {
    //                     $final_approved = false;
    //                 }
    //             } else {
    //                 return $this->response(400, 'Bad Request', [
    //                     'first_approver_id' => 'The first approver must be approved.'
    //                 ]);
    //             }
    //         }

    //         if ($schedule_adjustment->third_approver) {
    //             if ($validated['is_second_approved']  || !$validated['is_third_approved']) {
    //                 if ($schedule_adjustment->third_approver == $validated['user_id'] || Auth::user()->level_id == 5 && $schedule_adjustment->third_approver) {
    //                     if (!$validated['is_third_approved'] && !$validated['third_approver_remarks']) {
    //                         return $this->response(400, 'Bad Request', [
    //                             'third_approver_remarks' => 'The third approver remarks field is required.'
    //                         ]);
    //                     }

    //                     $schedule_adjustment->update([
    //                         'third_status' => $validated['is_third_approved'],
    //                         'third_approval_date' => date("Y-m-d"),
    //                         'third_approver_remarks' => $validated['third_approver_remarks'],
    //                     ]);
    //                 }

    //                 if ($validated['is_third_approved']) {
    //                     $final_approved = true;
    //                 } else {
    //                     $final_approved = false;
    //                 }
    //             } else {
    //                 return $this->response(400, 'Bad Request', [
    //                     'second_approver_id' => 'The second approver must be approved.'
    //                 ]);
    //             }
    //         }

    //         $message = 'Approved';
    //         if ($schedule_adjustment->third_approver && !$schedule_adjustment->third_status && $schedule_adjustment->third_approver_remarks) {
    //             $schedule_adjustment->update([
    //                 'final_status_id' => 4
    //             ]);
    //             $message = 'Rejected';
    //         } elseif ($schedule_adjustment->second_approver && !$schedule_adjustment->second_status && $schedule_adjustment->second_approver_remarks) {
    //             $schedule_adjustment->update([
    //                 'final_status_id' => 4
    //             ]);
    //             $message = 'Rejected';
    //         } elseif ($schedule_adjustment->first_approver && !$schedule_adjustment->first_status && $schedule_adjustment->first_approver_remarks) {
    //             $schedule_adjustment->update([
    //                 'final_status_id' => 4
    //             ]);
    //             $message = 'Rejected';
    //         } else {
    //             $schedule_adjustment->update([
    //                 'final_status_id' => ($final_approved ? 3 : 2)
    //             ]);
    //         }

    //         $user = User::find($validated['user_id']);
    //         if ($final_approved) {
    //             $user->userDetails()->update([
    //                 'schedule_id' => $schedule_adjustment->new_work_schedule_id,
    //             ]);
    //         }

    //         DB::commit();
    //         return $this->response(200, ('Schedule adjustment has been successfully ' . $message), $request);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }
}
