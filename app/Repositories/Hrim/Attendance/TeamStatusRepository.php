<?php

namespace App\Repositories\Hrim\Attendance;

use App\Interfaces\Hrim\Attendance\TeamStatusInterface;
use App\Models\Division;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class TeamStatusRepository implements TeamStatusInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $division = Division::with(['teams' => function ($query) {
                $query->with('division', 'position', 'reportsTo', 'department', 'section', 'createdBy');
            }])
                ->orderBy('description', 'asc')
                ->get();
            DB::commit();
            return $this->response(200, 'Division List', $division);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function showTeam($id, $request)
    {
        DB::beginTransaction();
        try {
            $team_records = UserDetails::with(['user', 'position', 'timeLog' => function ($query) {
                $query->whereDate('date', date("Y-m-d"));
            }])
                // ->whereHas('timeLog', function ($query) {
                //     $query->whereDate('date', date("Y-m-d"));
                // })
                ->where('division_id', $id)
                ->when($request['employee_id'] ?? false, function ($query) use ($request) {

                    $query->where('employee_number', $request['employee_id']);
                })
                ->when($request['employee_name'] ?? false, function ($query) use ($request) {
                    $query->whereHas('user', function ($query) use ($request) {
                        $query->where('name', 'like', '%' . $request['employee_name'] . '%');
                    });
                })
                ->get();

            // dd($team_records);
            DB::commit();
            return $this->response(200, 'Team List', $team_records);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
