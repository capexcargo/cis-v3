<?php

namespace App\Repositories\Hrim\Attendance;

use App\Interfaces\Hrim\Attendance\SchedulesInterface;
use App\Models\Hrim\StatusReference;
use App\Models\User;
use App\Traits\ResponseTrait;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SchedulesRepository implements SchedulesInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            if ($request['cut_off'] == 'first') {
                $begin = new DateTime($request['year'] . '-' . ($request['month'] - 1) . '-26');
                $end = new DateTime($request['year'] . '-' . $request['month'] . '-10');
            } else {
                $begin = new DateTime($request['year'] . '-' . $request['month'] . '-11');
                $end = new DateTime($request['year'] . '-' . $request['month'] . '-25');
            }

            $users_tar_all = User::whereHas('tars', function ($query) use ($begin, $end) {
                $query->whereDate('date_from', '>=', $begin)
                    ->whereDate('date_from', '<=', $end);
            })
                ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                    $query->where('division_id', Auth::user()->division_id);
                })->count();

            $users_tar_for_approval = User::whereHas('tars', function ($query) use ($begin, $end) {
                $query->whereDate('date_from', '>=', $begin)
                    ->whereDate('date_from', '<=', $end)
                    ->whereIn('final_status_id', [1, 2, 4]);
            })
                ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                    $query->where('division_id', Auth::user()->division_id);
                })->count();

            $users_sar_all = User::whereHas('scheduleAdjustments')
                ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                    $query->where('division_id', Auth::user()->division_id);
                })->count();

            $users_sar_for_approval = User::whereHas('scheduleAdjustments', function ($query) {
                $query->whereIn('final_status_id', [1, 2, 4]);
            })
                ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                    $query->where('division_id', Auth::user()->division_id);
                })->count();

            DB::commit();
            return $this->response(200, 'Schedule Tali', compact('users_tar_all', 'users_tar_for_approval', 'users_sar_all', 'users_sar_for_approval'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
