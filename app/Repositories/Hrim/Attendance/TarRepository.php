<?php

namespace App\Repositories\Hrim\Attendance;

use App\Interfaces\Hrim\Attendance\TarInterface;
use App\Models\Hrim\Absent;
use App\Models\Hrim\Tar;
use App\Models\Hrim\TimeLog;
use App\Models\User;
use App\Traits\ResponseTrait;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TarRepository implements TarInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            if ($request['cut_off'] == 'first') {
                $begin = new DateTime($request['year'] . '-' . ($request['month'] - 1) . '-26');
                $end = new DateTime($request['year'] . '-' . $request['month'] . '-10');
            } else {
                $begin = new DateTime($request['year'] . '-' . $request['month'] . '-11');
                $end = new DateTime($request['year'] . '-' . $request['month'] . '-25');
            }

            $users = User::with(['tars', 'userDetails' => function ($query) {
                $query->with(
                    'branch',
                    'workSchedule',
                    'position',
                    'jobLevel',
                    'department',
                    'division',
                    'employmentCategory',
                    'employmentStatus',
                    'gender',
                    'suffix',
                );
            }])
                ->withCount([
                    'tars as total_filed_tar' => function ($query) use ($begin, $end, $request) {
                        $query->whereDate('date_from', '>=', $begin)
                            ->whereDate('date_from', '<=', $end)
                            ->when($request['status'] ?? false, function ($query) use ($request) {
                                $query->when($request['status'] == 'tar_for_approval', function ($query) {
                                    $query->whereIn('final_status_id', [1, 2]);
                                });
                            })
                            ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                                $query->where('users.division_id', Auth::user()->division_id);
                            });
                    },
                ])
                ->whereHas('tars', function ($query) use ($begin, $end, $request) {
                    $query->whereDate('date_from', '>=', $begin)
                        ->whereDate('date_from', '<=', $end)
                        ->when($request['status'] ?? false, function ($query) use ($request) {
                            $query->when($request['status'] == 'tar_for_approval', function ($query) {
                                $query->whereIn('final_status_id', [1, 2]);
                            });
                        });
                })
                ->whereHas('userDetails', function ($query) use ($request) {
                    $query->when($request['branch'] ?? false, function ($query) use ($request) {
                        $query->where('branch_id', $request['branch']);
                    })
                        ->when($request['employment_category'] ?? false, function ($query) use ($request) {
                            $query->where('employment_category_id', $request['employment_category']);
                        });
                })
                ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                    $query->where('division_id', Auth::user()->division_id);
                })
                ->when($request['employee_id'] ?? false, function ($query) use ($request) {
                    $query->whereHas('userDetails', function ($query) use ($request) {
                        $query->where('employee_number', $request['employee_id']);
                    });
                })
                ->when($request['employee_name'] ?? false, function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request['employee_name'] . '%');
                })

                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Tar List', $users);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function showUserTar($id, $request = [])
    {
        DB::beginTransaction();
        try {
            $user = User::find($id);
            $tars = Tar::with(
                'user',
                'tarReason',
                'firstApprover',
                'secondApprover',
                'thirdApprover',
                'finalStatus',
                'timeLogs'
            )
                ->where('user_id', $id)
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();

            // dd($tars);

            return $this->response(200, 'User Tar List', compact('user', 'tars'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id, $request = [])
    {
        DB::beginTransaction();
        try {
            $tar = Tar::with(
                'user',
                'timeLog',
                'tarReason',
                'firstApprover',
                'secondApprover',
                'thirdApprover',
                'adminApprover',
                'finalStatus'
            )->firstWhere([['user_id', $request['user_id']], ['id', $id]]);
            if (!$tar) {
                return $this->response(404, 'Tar', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Tar', $tar);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function approveValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'user_id' => 'sometimes',
                'first_approver_id' => 'sometimes',
                'second_approver_id' => 'sometimes',
                'third_approver_id' => 'sometimes',
                'is_first_approved' => 'sometimes',
                'is_second_approved' => 'sometimes',
                'is_third_approved' => 'sometimes',
                'first_approver_remarks' => 'sometimes',
                'second_approver_remarks' => 'sometimes',
                'third_approver_remarks' => 'sometimes',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'user_id' => 'required',
                'approver' => 'required',
                'remarks' => 'required_if:is_approved,0',
                'is_approved' => 'sometimes',
            ], [
                'remarks.required_if' => 'The remarks field is required when is declined.',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function approve($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $response = $this->show($id, ['user_id' => $validated['user_id']]);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $tar = $response['result'];

            if ($tar->first_approver && $validated['approver'] == 'first') {
                if ($tar->first_approver == Auth::user()->id || Auth::user()->level_id == 5 && $tar->first_approver) {
                    if (!$validated['is_approved'] && !$validated['remarks']) {
                        return $this->response(400, 'Bad Request', [
                            'remarks' => 'The first approver remarks field is required.',
                        ]);
                    }

                    if ($tar->second_status && !$validated['is_approved']) {
                        return $this->response(400, 'Bad Request', [
                            'approver' => 'The second approver must be disapproved.',
                        ]);
                    } else {
                        $tar->update([
                            'first_status' => $validated['is_approved'] ?? 0,
                            'first_approval_date' => date('Y-m-d'),
                            'first_approver_remarks' => $validated['remarks'],
                        ]);
                    }
                }
            }

            if ($tar->second_approver && $validated['approver'] == 'second') {
                if ($tar->first_status || !$validated['is_approved']) {
                    if ($tar->second_approver == Auth::user()->id || Auth::user()->level_id == 5 && $tar->second_approver) {
                        if (!$validated['is_approved'] && !$validated['remarks']) {
                            return $this->response(400, 'Bad Request', [
                                'remarks' => 'The approver remarks 2 field is required.',
                            ]);
                        }

                        if ($tar->third_status && !$validated['is_approved']) {
                            return $this->response(400, 'Bad Request', [
                                'approver' => 'The third approver must be disapproved.',
                            ]);
                        } else {
                            $tar->update([
                                'second_status' => $validated['is_approved'] ?? 0,
                                'second_approval_date' => date('Y-m-d'),
                                'second_approver_remarks' => $validated['remarks'],
                            ]);
                        }
                    }
                } else {
                    return $this->response(400, 'Bad Request', [
                        'approver' => 'The first approver must be approved.',
                    ]);
                }
            }

            if ($tar->third_approver && $validated['approver'] == 'third') {
                if ($tar->second_status || !$validated['is_approved']) {
                    if ($tar->third_approver == Auth::user()->id || Auth::user()->level_id == 5 && $tar->third_approver) {
                        if (!$validated['is_approved'] && !$validated['remarks']) {
                            return $this->response(400, 'Bad Request', [
                                'remarks' => 'The third approver remarks field is required.',
                            ]);
                        }

                        $tar->update([
                            'third_status' => $validated['is_approved'],
                            'third_approval_date' => date('Y-m-d'),
                            'third_approver_remarks' => $validated['remarks'],
                        ]);
                    }
                } else {
                    return $this->response(400, 'Bad Request', [
                        'approver' => 'The second approver must be approved.',
                    ]);
                }
            }



            $final_approved = true;

            if ($tar->third_approver && !$tar->third_status) {
                $final_approved = false;
            }
            if ($tar->second_approver && !$tar->second_status) {
                $final_approved = false;
            }
            if ($tar->first_approver && !$tar->first_status) {
                $final_approved = false;
            }


            if ($tar->third_approver && !$tar->third_status && $tar->third_approver_remarks) {
                $tar->update([
                    'final_status_id' => 4,
                ]);

                $responses = TimeLog::where('date', $tar->date_from)
                    ->where('user_id', $validated['user_id'])
                    ->where('time_in', $tar->actual_time_in)
                    ->where('time_out', $tar->actual_time_out)
                    ->select('id')
                    ->first();

                if (!$tar->time_log_id) {
                    if ($responses) {
                        $usertimelog = TimeLog::find($responses->id);
                        $usertimelog->forceDelete(); //returns true/false
                    }

                    $getabsent = Absent::where('user_id', $validated['user_id'])
                        ->where('absent_date', date("Y-m-d"))->first();
                    if ($getabsent === null) {
                        Absent::create(['user_id' => $validated['user_id'], 'absent_date' => date("Y-m-d")]);
                    }
                } else {
                    $updatetimelog = TimeLog::find($tar->time_log_id);
                    $updatetimelog->update([
                        'time_in' => $tar->current_time_in,
                        'time_out' => $tar->current_time_out,
                        'time_out_date' => $tar->date_to
                    ]);
                }
            } elseif ($tar->second_approver && !$tar->second_status && $tar->second_approver_remarks) {
                $tar->update([
                    'final_status_id' => 4,
                ]);

                $responses = TimeLog::where('date', $tar->date_from)
                    ->where('user_id', $validated['user_id'])
                    ->where('time_in', $tar->actual_time_in)
                    ->where('time_out', $tar->actual_time_out)
                    ->select('id')
                    ->first();

                if (!$tar->time_log_id) {
                    if ($responses) {
                        $usertimelog = TimeLog::find($responses->id);
                        $usertimelog->forceDelete(); //returns true/false
                    }

                    $getabsent = Absent::where('user_id', $validated['user_id'])
                        ->where('absent_date', date("Y-m-d"))->first();
                    if ($getabsent === null) {
                        Absent::create(['user_id' => $validated['user_id'], 'absent_date' => date("Y-m-d")]);
                    }
                } else {
                    $updatetimelog = TimeLog::find($tar->time_log_id);
                    $updatetimelog->update([
                        'time_in' => $tar->current_time_in,
                        'time_out' => $tar->current_time_out,
                        'time_out_date' => $tar->date_to
                    ]);
                }
            } elseif ($tar->first_approver && !$tar->first_status && $tar->first_approver_remarks) {
                $tar->update([
                    'final_status_id' => 4,
                ]);

                $responses = TimeLog::where('date', $tar->date_from)
                    ->where('user_id', $validated['user_id'])
                    ->where('time_in', $tar->actual_time_in)
                    ->where('time_out', $tar->actual_time_out)
                    ->select('id')
                    ->first();


                if (!$tar->time_log_id) {
                    if ($responses) {
                        $usertimelog = TimeLog::find($responses->id);
                        $usertimelog->forceDelete(); //returns true/false
                    }

                    $getabsent = Absent::where('user_id', $validated['user_id'])
                        ->where('absent_date', date("Y-m-d"))->first();
                    if ($getabsent === null) {
                        Absent::create(['user_id' => $validated['user_id'], 'absent_date' => date("Y-m-d")]);
                    }
                } else {
                    $updatetimelog = TimeLog::find($tar->time_log_id);
                    $updatetimelog->update([
                        'time_in' => $tar->current_time_in,
                        'time_out' => $tar->current_time_out,
                        'time_out_date' => $tar->date_to
                    ]);
                }
            } else {
                $tar->update([
                    'final_status_id' => ($final_approved ? 3 : 2),
                ]);

                $get_absence = Absent::where('user_id', $validated['user_id'])
                    ->where('absent_date', date("Y-m-d"));

                if ($get_absence) {
                    $get_absence->forceDelete();
                }
            }

            if ($final_approved) {
                $user = User::find($validated['user_id']);

                $work_schedule = $user->userDetails->workSchedule;
                $work_schedule_time_in = date_create($tar->date_from . ' ' . date('H:i', strtotime('+15 minutes', strtotime($work_schedule->time_from))));
                $work_schedule_time_ins = date_create($tar->date_from . ' ' . date('H:i', strtotime($work_schedule->time_from)));
                $work_schedule_time_out = date_create($tar->date_from . ' ' . $work_schedule->time_to);

                if ($work_schedule->shift == 'night_shift') {
                    $work_schedule_time_out = date_create(date('Y-m-d', strtotime('+1 day', strtotime($tar->date_from))) . ' ' . $work_schedule->time_to);
                }

                $work_schedule_break_time_in = date_create($tar->date_from . ' ' . $work_schedule->break_from);
                $work_schedule_break_time_out = date_create($tar->date_from . ' ' . $work_schedule->break_to);

                $work_schedule_ot_break_time_from = date_create($tar->date_from . ' ' . $work_schedule->ot_break_from);
                $work_schedule_ot_break_time_to = date_create($tar->date_from . ' ' . $work_schedule->ot_break_to);

                if ($work_schedule->shift == 'night_shift') {
                    $work_schedule_ot_break_time_from = date_create(date('Y-m-d', strtotime('+1 day', strtotime($tar->date_from))) . ' ' . $work_schedule->ot_break_from);
                    $work_schedule_ot_break_time_to = date_create(date('Y-m-d', strtotime('+1 day', strtotime($tar->date_from))) . ' ' . $work_schedule->ot_break_to);
                }

                $time_log_time_in = date_create($tar->date_from . ' ' . $tar->actual_time_in);
                $time_log_time_out = date_create($tar->date_to . ' ' . $tar->actual_time_out);
                // $time_log_time_in = date_create(date("H:i", strtotime('18:34')));
                // $time_log_time_out = date_create(date("H:i", strtotime('11:34')));

                $undertime = null;
                if ($work_schedule_time_out > $time_log_time_out) {
                    $difference_undertime = date_diff($work_schedule_time_out, $time_log_time_out);
                    $undertime = $this->computeTime(($difference_undertime->format('%H.%I')));
                }

                $late = null;
                if ($work_schedule_time_in < $time_log_time_in) {
                    $difference_late = date_diff($work_schedule_time_ins, $time_log_time_in);
                    $late = $this->computeTime($difference_late->format('%H.%I'));
                }

                $ot = null;
                if ($time_log_time_out > $work_schedule_time_out) {
                    $before = 0;
                    $after = 0;

                    if ($work_schedule_ot_break_time_from > $work_schedule_time_out) {
                        if ($time_log_time_out > $work_schedule_ot_break_time_from) {
                            $difference_before = date_diff($work_schedule_time_out, $work_schedule_ot_break_time_from);
                            $before = $this->computeTime(($difference_before->format('%H.%I')));
                        } else {
                            $difference_before = date_diff($work_schedule_time_out, $time_log_time_out);
                            $before = $this->computeTime(($difference_before->format('%H.%I')));
                        }
                    }

                    if ($work_schedule_ot_break_time_from > $time_log_time_out) {
                        $difference_before = date_diff($work_schedule_ot_break_time_from, $work_schedule_ot_break_time_from);
                        $difference_before2 = date_diff($work_schedule_time_out, $time_log_time_out);
                        $before = $this->computeTime(($difference_before2->format('%H.%I')));
                    }

                    if ($work_schedule_ot_break_time_to < $time_log_time_out) {
                        $difference_after = date_diff($work_schedule_ot_break_time_to, $time_log_time_out);
                        $after = $this->computeTime(($difference_after->format('%d') * 24) + ($difference_after->format('%H.%I')));
                    }

                    $ot = ($before + $after);

                    $ot = ($ot - $late);
                    if ($ot < 0) {
                        $ot = 0;
                    }

                    // $difference_ot = date_diff($work_schedule_time_out, $time_log_time_out);
                    // $ot = $this->computeTime(($difference_ot->format('%d')* 24) + ($difference_ot->format('%H.%I')));
                }

                $moring = 0;
                $afternoon = 0;
                $rendered_time = 0;

                if ($work_schedule_break_time_in > $time_log_time_in) {
                    if ($work_schedule_break_time_in > $time_log_time_out) {
                        $difference_morning = date_diff($time_log_time_in, $time_log_time_out);
                        $moring = $this->computeTime(($difference_morning->format('%H.%I')));
                    } else {
                        $difference_morning = date_diff($time_log_time_in, $work_schedule_break_time_in);
                        $moring = $this->computeTime(($difference_morning->format('%H.%I')));
                    }

                    // $difference_morning = date_diff($time_log_time_in, $work_schedule_break_time_in);
                    // $moring = $this->computeTime(($difference_morning->format('%H.%I')));
                }

                if ($work_schedule_break_time_out < $time_log_time_out) {
                    $difference_afternoon = date_diff($work_schedule_break_time_out, $time_log_time_out);
                    $afternoon = $this->computeTime(($difference_afternoon->format('%H.%I')));
                }

                $rendered_time = ($moring + $afternoon);
                if ($validated['approver'] != "bypass") {
                    if ($validated['is_approved']) {
                        $user->timeLog()->updateOrCreate([
                            'id' => $tar->time_log_id,
                        ], [
                            'work_sched_id' => $user->userDetails->schedule_id,
                            'date_category_id' => 1,
                            'work_mode_id' => $user->userDetails->workSchedule->work_mode_id,
                            'time_in' => $tar->actual_time_in,
                            'time_out' => $tar->actual_time_out,
                            'time_out_date' => $tar->date_to,
                            'date' => $tar->date_from,
                            'rendered_time' => ($rendered_time > 8 ? 8.0 : $rendered_time),
                            'late' => $late,
                            'undertime' => $undertime,
                            'computed_ot' => $ot,
                        ]);
                    }
                }
            }

            if ($validated['approver'] == "bypass") {
                if ($validated['is_approved']) {


                    $user = User::find($validated['user_id']);

                    $work_schedule = $user->userDetails->workSchedule;
                    $work_schedule_time_in = date_create($tar->date_from . ' ' . date('H:i', strtotime('+15 minutes', strtotime($work_schedule->time_from))));
                    $work_schedule_time_ins = date_create($tar->date_from . ' ' . date('H:i', strtotime($work_schedule->time_from)));
                    $work_schedule_time_out = date_create($tar->date_from . ' ' . $work_schedule->time_to);

                    if ($work_schedule->shift == 'night_shift') {
                        $work_schedule_time_out = date_create(date('Y-m-d', strtotime('+1 day', strtotime($tar->date_from))) . ' ' . $work_schedule->time_to);
                    }

                    $work_schedule_break_time_in = date_create($tar->date_from . ' ' . $work_schedule->break_from);
                    $work_schedule_break_time_out = date_create($tar->date_from . ' ' . $work_schedule->break_to);

                    $work_schedule_ot_break_time_from = date_create($tar->date_from . ' ' . $work_schedule->ot_break_from);
                    $work_schedule_ot_break_time_to = date_create($tar->date_from . ' ' . $work_schedule->ot_break_to);

                    if ($work_schedule->shift == 'night_shift') {
                        $work_schedule_ot_break_time_from = date_create(date('Y-m-d', strtotime('+1 day', strtotime($tar->date_from))) . ' ' . $work_schedule->ot_break_from);
                        $work_schedule_ot_break_time_to = date_create(date('Y-m-d', strtotime('+1 day', strtotime($tar->date_from))) . ' ' . $work_schedule->ot_break_to);
                    }

                    $time_log_time_in = date_create($tar->date_from . ' ' . $tar->actual_time_in);
                    $time_log_time_out = date_create($tar->date_to . ' ' . $tar->actual_time_out);
                    // $time_log_time_in = date_create(date("H:i", strtotime('18:34')));
                    // $time_log_time_out = date_create(date("H:i", strtotime('11:34')));

                    $undertime = null;
                    if ($work_schedule_time_out > $time_log_time_out) {
                        $difference_undertime = date_diff($work_schedule_time_out, $time_log_time_out);
                        $undertime = $this->computeTime(($difference_undertime->format('%H.%I')));
                    }

                    $late = null;
                    if ($work_schedule_time_in < $time_log_time_in) {
                        $difference_late = date_diff($work_schedule_time_ins, $time_log_time_in);
                        $late = $this->computeTime($difference_late->format('%H.%I'));
                    }

                    $ot = null;
                    if ($time_log_time_out > $work_schedule_time_out) {
                        $before = 0;
                        $after = 0;

                        if ($work_schedule_ot_break_time_from > $work_schedule_time_out) {
                            if ($time_log_time_out > $work_schedule_ot_break_time_from) {
                                $difference_before = date_diff($work_schedule_time_out, $work_schedule_ot_break_time_from);
                                $before = $this->computeTime(($difference_before->format('%H.%I')));
                            } else {
                                $difference_before = date_diff($work_schedule_time_out, $time_log_time_out);
                                $before = $this->computeTime(($difference_before->format('%H.%I')));
                            }
                        }

                        if ($work_schedule_ot_break_time_from > $time_log_time_out) {
                            $difference_before = date_diff($work_schedule_ot_break_time_from, $work_schedule_ot_break_time_from);
                            $difference_before2 = date_diff($work_schedule_time_out, $time_log_time_out);
                            $before = $this->computeTime(($difference_before2->format('%H.%I')));
                        }

                        if ($work_schedule_ot_break_time_to < $time_log_time_out) {
                            $difference_after = date_diff($work_schedule_ot_break_time_to, $time_log_time_out);
                            $after = $this->computeTime(($difference_after->format('%d') * 24) + ($difference_after->format('%H.%I')));
                        }

                        $ot = ($before + $after);

                        $ot = ($ot - $late);
                        if ($ot < 0) {
                            $ot = 0;
                        }

                        // $difference_ot = date_diff($work_schedule_time_out, $time_log_time_out);
                        // $ot = $this->computeTime(($difference_ot->format('%d')* 24) + ($difference_ot->format('%H.%I')));
                    }

                    $moring = 0;
                    $afternoon = 0;
                    $rendered_time = 0;

                    if ($work_schedule_break_time_in > $time_log_time_in) {
                        if ($work_schedule_break_time_in > $time_log_time_out) {
                            $difference_morning = date_diff($time_log_time_in, $time_log_time_out);
                            $moring = $this->computeTime(($difference_morning->format('%H.%I')));
                        } else {
                            $difference_morning = date_diff($time_log_time_in, $work_schedule_break_time_in);
                            $moring = $this->computeTime(($difference_morning->format('%H.%I')));
                        }

                        // $difference_morning = date_diff($time_log_time_in, $work_schedule_break_time_in);
                        // $moring = $this->computeTime(($difference_morning->format('%H.%I')));
                    }

                    if ($work_schedule_break_time_out < $time_log_time_out) {
                        $difference_afternoon = date_diff($work_schedule_break_time_out, $time_log_time_out);
                        $afternoon = $this->computeTime(($difference_afternoon->format('%H.%I')));
                    }

                    $rendered_time = ($moring + $afternoon);





                    $user->timeLog()->updateOrCreate([
                        'id' => $tar->time_log_id,
                    ], [
                        'work_sched_id' => $user->userDetails->schedule_id,
                        'date_category_id' => 1,
                        'work_mode_id' => $user->userDetails->workSchedule->work_mode_id,
                        'time_in' => $tar->actual_time_in,
                        'time_out' => $tar->actual_time_out,
                        'time_out_date' => $tar->date_to,
                        'date' => $tar->date_from,
                        'rendered_time' => ($rendered_time > 8 ? 8.0 : $rendered_time),
                        'late' => $late,
                        'undertime' => $undertime,
                        'computed_ot' => $ot,
                    ]);


                    $tar->update([
                        'admin_approver' => Auth::user()->id,
                        'admin_approval_date' => date("Y-m-d"),
                        'admin_approver_remarks' => $validated['remarks'],
                        'final_status_id' => 3
                    ]);

                    $get_absence = Absent::where('user_id', $validated['user_id'])
                        ->where('absent_date', date("Y-m-d"));

                    $get_absence->forceDelete();
                } else { //decline bypass

                    $user = User::find($validated['user_id']);

                    $work_schedule = $user->userDetails->workSchedule;
                    $work_schedule_time_in = date_create($tar->date_from . ' ' . date('H:i', strtotime('+15 minutes', strtotime($work_schedule->time_from))));
                    $work_schedule_time_ins = date_create($tar->date_from . ' ' . date('H:i', strtotime($work_schedule->time_from)));
                    $work_schedule_time_out = date_create($tar->date_from . ' ' . $work_schedule->time_to);

                    if ($work_schedule->shift == 'night_shift') {
                        $work_schedule_time_out = date_create(date('Y-m-d', strtotime('+1 day', strtotime($tar->date_from))) . ' ' . $work_schedule->time_to);
                    }

                    $work_schedule_break_time_in = date_create($tar->date_from . ' ' . $work_schedule->break_from);
                    $work_schedule_break_time_out = date_create($tar->date_from . ' ' . $work_schedule->break_to);

                    $work_schedule_ot_break_time_from = date_create($tar->date_from . ' ' . $work_schedule->ot_break_from);
                    $work_schedule_ot_break_time_to = date_create($tar->date_from . ' ' . $work_schedule->ot_break_to);

                    if ($work_schedule->shift == 'night_shift') {
                        $work_schedule_ot_break_time_from = date_create(date('Y-m-d', strtotime('+1 day', strtotime($tar->date_from))) . ' ' . $work_schedule->ot_break_from);
                        $work_schedule_ot_break_time_to = date_create(date('Y-m-d', strtotime('+1 day', strtotime($tar->date_from))) . ' ' . $work_schedule->ot_break_to);
                    }

                    $time_log_time_in = date_create($tar->date_from . ' ' . $tar->actual_time_in);
                    $time_log_time_out = date_create($tar->date_to . ' ' . $tar->actual_time_out);
                    // $time_log_time_in = date_create(date("H:i", strtotime('18:34')));
                    // $time_log_time_out = date_create(date("H:i", strtotime('11:34')));

                    $undertime = null;
                    if ($work_schedule_time_out > $time_log_time_out) {
                        $difference_undertime = date_diff($work_schedule_time_out, $time_log_time_out);
                        $undertime = $this->computeTime(($difference_undertime->format('%H.%I')));
                    }

                    $late = null;
                    if ($work_schedule_time_in < $time_log_time_in) {
                        $difference_late = date_diff($work_schedule_time_ins, $time_log_time_in);
                        $late = $this->computeTime($difference_late->format('%H.%I'));
                    }

                    $ot = null;
                    if ($time_log_time_out > $work_schedule_time_out) {
                        $before = 0;
                        $after = 0;

                        if ($work_schedule_ot_break_time_from > $work_schedule_time_out) {
                            if ($time_log_time_out > $work_schedule_ot_break_time_from) {
                                $difference_before = date_diff($work_schedule_time_out, $work_schedule_ot_break_time_from);
                                $before = $this->computeTime(($difference_before->format('%H.%I')));
                            } else {
                                $difference_before = date_diff($work_schedule_time_out, $time_log_time_out);
                                $before = $this->computeTime(($difference_before->format('%H.%I')));
                            }
                        }

                        if ($work_schedule_ot_break_time_from > $time_log_time_out) {
                            $difference_before = date_diff($work_schedule_ot_break_time_from, $work_schedule_ot_break_time_from);
                            $difference_before2 = date_diff($work_schedule_time_out, $time_log_time_out);
                            $before = $this->computeTime(($difference_before2->format('%H.%I')));
                        }

                        if ($work_schedule_ot_break_time_to < $time_log_time_out) {
                            $difference_after = date_diff($work_schedule_ot_break_time_to, $time_log_time_out);
                            $after = $this->computeTime(($difference_after->format('%d') * 24) + ($difference_after->format('%H.%I')));
                        }

                        $ot = ($before + $after);

                        $ot = ($ot - $late);
                        if ($ot < 0) {
                            $ot = 0;
                        }

                        // $difference_ot = date_diff($work_schedule_time_out, $time_log_time_out);
                        // $ot = $this->computeTime(($difference_ot->format('%d')* 24) + ($difference_ot->format('%H.%I')));
                    }

                    $moring = 0;
                    $afternoon = 0;
                    $rendered_time = 0;

                    if ($work_schedule_break_time_in > $time_log_time_in) {
                        if ($work_schedule_break_time_in > $time_log_time_out) {
                            $difference_morning = date_diff($time_log_time_in, $time_log_time_out);
                            $moring = $this->computeTime(($difference_morning->format('%H.%I')));
                        } else {
                            $difference_morning = date_diff($time_log_time_in, $work_schedule_break_time_in);
                            $moring = $this->computeTime(($difference_morning->format('%H.%I')));
                        }

                        // $difference_morning = date_diff($time_log_time_in, $work_schedule_break_time_in);
                        // $moring = $this->computeTime(($difference_morning->format('%H.%I')));
                    }

                    if ($work_schedule_break_time_out < $time_log_time_out) {
                        $difference_afternoon = date_diff($work_schedule_break_time_out, $time_log_time_out);
                        $afternoon = $this->computeTime(($difference_afternoon->format('%H.%I')));
                    }

                    $rendered_time = ($moring + $afternoon);


                    $responses = TimeLog::where('date', $tar->date_from)
                        ->where('user_id', $validated['user_id'])
                        ->where('time_in', $tar->actual_time_in)
                        ->where('time_out', $tar->actual_time_out)
                        ->select('id')
                        ->first();

                    $tar->update([
                        'admin_approver' => Auth::user()->id,
                        'admin_approval_date' => date("Y-m-d"),
                        'admin_approver_remarks' => $validated['remarks'],
                        'final_status_id' => 4
                    ]);

                    if (!$tar->time_log_id) {

                        if ($responses) {


                            TimeLog::findOrfail($responses->id)->forceDelete();
                            // $usertimelog2 = TimeLog::withTrashed()->find($responses->id);
                            // $usertimelog->forceDelete(); //returns true/false
                            // $usertimelog2->forceDelete(); //returns true/false
                            // dd("absent log");

                        }

                        $getabsent = Absent::where('user_id', $validated['user_id'])
                            ->where('absent_date', date("Y-m-d"))->first();
                        if ($getabsent === null) {
                            Absent::create(['user_id' => $validated['user_id'], 'absent_date' => date("Y-m-d")]);
                        }
                    } else {
                        $updatetimelog = TimeLog::find($tar->time_log_id);
                        $updatetimelog->update([
                            'time_in' => $tar->current_time_in,
                            'time_out' => $tar->current_time_out,
                            'time_out_date' => $tar->date_to
                        ]);
                    }
                }
            }


            DB::commit();

            return $this->response(200, ('Time Adjustment Request has been successfully ' . ($validated['is_approved'] ? 'approved!' : 'declined!')), $request);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // public function approve($id, $request)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $response = $this->approveValidation($request);
    //         if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
    //         $validated = $response['result'];

    //         $response = $this->show($id, ['user_id' => $validated['user_id']]);
    //         if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
    //         $tar = $response['result'];

    //         $final_approved = false;

    //         if ($tar->first_approver) {
    //             if ($tar->first_approver == $validated['user_id'] || Auth::user()->level_id == 5 && $tar->first_approver) {
    //                 if (!$validated['is_first_approved'] && !$validated['first_approver_remarks']) {
    //                     return $this->response(400, 'Bad Request', [
    //                         'first_approver_remarks' => 'The first approver remarks field is required.'
    //                     ]);
    //                 }

    //                 if ($validated['is_second_approved'] && !$validated['is_first_approved']) {
    //                     return $this->response(400, 'Bad Request', [
    //                         'second_approver_id' => 'The second approver must be disapproved.'
    //                     ]);
    //                 } else {
    //                     $tar->update([
    //                         'first_status' => $validated['is_first_approved'] ?? 0,
    //                         'first_approval_date' => date("Y-m-d"),
    //                         'first_approver_remarks' => $validated['first_approver_remarks'],
    //                     ]);
    //                 }

    //                 if ($validated['is_first_approved']) {
    //                     $final_approved = true;
    //                 } else {
    //                     $final_approved = false;
    //                 }
    //             }
    //         }

    //         if ($tar->second_approver) {
    //             if ($validated['is_first_approved'] || !$validated['is_second_approved']) {
    //                 if ($tar->second_approver == $validated['user_id'] || Auth::user()->level_id == 5 && $tar->second_approver) {
    //                     if (!$validated['is_second_approved'] && !$validated['second_approver_remarks']) {
    //                         return $this->response(400, 'Bad Request', [
    //                             'second_approver_remarks' => 'The approver remarks 2 field is required.'
    //                         ]);
    //                     }

    //                     if ($validated['is_third_approved'] && !$validated['is_second_approved']) {
    //                         return $this->response(400, 'Bad Request', [
    //                             'third_approver_id' => 'The third approver must be disapproved.'
    //                         ]);
    //                     } else {
    //                         $tar->update([
    //                             'second_status' => $validated['is_second_approved'] ?? 0,
    //                             'second_approval_date' => date("Y-m-d"),
    //                             'second_approver_remarks' => $validated['second_approver_remarks'],
    //                         ]);
    //                     }
    //                 }

    //                 if ($validated['is_second_approved']) {
    //                     $final_approved = true;
    //                 } else {
    //                     $final_approved = false;
    //                 }
    //             } else {
    //                 return $this->response(400, 'Bad Request', [
    //                     'first_approver_id' => 'The first approver must be approved.'
    //                 ]);
    //             }
    //         }

    //         if ($tar->third_approver) {
    //             if ($validated['is_second_approved']  || !$validated['is_third_approved']) {
    //                 if ($tar->third_approver == $validated['user_id'] || Auth::user()->level_id == 5 && $tar->third_approver) {
    //                     if (!$validated['is_third_approved'] && !$validated['third_approver_remarks']) {
    //                         return $this->response(400, 'Bad Request', [
    //                             'third_approver_remarks' => 'The third approver remarks field is required.'
    //                         ]);
    //                     }

    //                     $tar->update([
    //                         'third_status' => $validated['is_third_approved'],
    //                         'third_approval_date' => date("Y-m-d"),
    //                         'third_approver_remarks' => $validated['third_approver_remarks'],
    //                     ]);
    //                 }

    //                 if ($validated['is_third_approved']) {
    //                     $final_approved = true;
    //                 } else {
    //                     $final_approved = false;
    //                 }
    //             } else {
    //                 return $this->response(400, 'Bad Request', [
    //                     'second_approver_id' => 'The second approver must be approved.'
    //                 ]);
    //             }
    //         }

    //         $message = 'Approved';
    //         if ($tar->third_approver && !$tar->third_status && $tar->third_approver_remarks) {
    //             $tar->update([
    //                 'final_status_id' => 4
    //             ]);
    //             $message = 'Rejected';
    //         } elseif ($tar->second_approver && !$tar->second_status && $tar->second_approver_remarks) {
    //             $tar->update([
    //                 'final_status_id' => 4
    //             ]);
    //             $message = 'Rejected';
    //         } elseif ($tar->first_approver && !$tar->first_status && $tar->first_approver_remarks) {
    //             $tar->update([
    //                 'final_status_id' => 4
    //             ]);
    //             $message = 'Rejected';
    //         } else {
    //             $tar->update([
    //                 'final_status_id' => ($final_approved ? 3 : 2)
    //             ]);
    //         }

    //         if ($final_approved) {
    //             $user = User::find($validated['user_id']);

    //             $work_schedule = Auth::user()->userDetails->workSchedule;
    //             $work_schedule_time_in = date_create($tar->date . ' ' . date("H:i", strtotime('+15 minutes', strtotime($work_schedule->time_from))));
    //             $work_schedule_time_out = date_create($tar->date . ' ' . $work_schedule->time_to);
    //             $work_schedule_break_time_in = date_create($tar->date . ' ' . $work_schedule->break_from);
    //             $work_schedule_break_time_out = date_create($tar->date . ' ' . $work_schedule->break_to);
    //             $work_schedule_ot_break_time_from = date_create($tar->date . ' ' . $work_schedule->ot_break_from);
    //             $work_schedule_ot_break_time_to = date_create($tar->date . ' ' . $work_schedule->ot_break_to);
    //             $time_log_time_in = date_create($tar->date . ' ' . $tar->actual_time_in);
    //             $time_log_time_out = date_create($tar->date . ' ' . $tar->actual_time_out);
    //             // $time_log_time_in = date_create(date("H:i", strtotime('18:34')));
    //             // $time_log_time_out = date_create(date("H:i", strtotime('11:34')));
    //             $undertime = null;
    //             if ($work_schedule_time_out > $time_log_time_out) {
    //                 $difference_undertime = date_diff($work_schedule_time_out, $time_log_time_out);
    //                 $undertime = $this->computeTime(($difference_undertime->format('%H.%I')));
    //             }

    //             $late = null;
    //             if ($work_schedule_time_in < $time_log_time_in) {
    //                 $difference_late = date_diff($work_schedule_time_in, $time_log_time_in);
    //                 $late = $this->computeTime($difference_late->format('%H.%I'));
    //             }

    //             $ot = null;
    //             if ($time_log_time_out > $work_schedule_time_out) {
    //                 $before = 0;
    //                 $after = 0;

    //                 if ($work_schedule_ot_break_time_from > $work_schedule_time_out) {
    //                     $difference_before = date_diff($work_schedule_time_out, $work_schedule_ot_break_time_from);
    //                     $before = $this->computeTime(($difference_before->format('%H.%I')));
    //                 }

    //                 if ($work_schedule_ot_break_time_to < $time_log_time_out) {
    //                     $difference_after = date_diff($work_schedule_ot_break_time_to, $time_log_time_out);
    //                     $after = $this->computeTime(($difference_after->format('%d') * 24) + ($difference_after->format('%H.%I')));
    //                 }

    //                 $ot = ($before + $after);
    //                 // $difference_ot = date_diff($work_schedule_time_out, $time_log_time_out);
    //                 // $ot = $this->computeTime(($difference_ot->format('%d')* 24) + ($difference_ot->format('%H.%I')));
    //             }

    //             $moring = 0;
    //             $afternoon = 0;
    //             $rendered_time = 0;

    //             if ($work_schedule_break_time_in > $time_log_time_in) {
    //                 $difference_morning = date_diff($time_log_time_in, $work_schedule_break_time_in);
    //                 $moring = $this->computeTime(($difference_morning->format('%H.%I')));
    //             }

    //             if ($work_schedule_break_time_out < $time_log_time_out) {
    //                 $difference_afternoon = date_diff($work_schedule_break_time_out, $time_log_time_out);
    //                 $afternoon = $this->computeTime(($difference_afternoon->format('%H.%I')));
    //             }

    //             $rendered_time = ($moring + $afternoon);

    //             $user->timeLog()->updateOrCreate([
    //                 'date' => $tar->date,
    //             ], [
    //                 'work_sched_id' => $user->userDetails->schedule_id,
    //                 'date_category_id' => 1,
    //                 'work_mode_id' => $user->userDetails->workSchedule->work_mode_id,
    //                 'time_in' => $tar->actual_time_in,
    //                 'time_out' => $tar->actual_time_out,
    //                 'time_out_date' => $tar->date,
    //                 'rendered_time' => ($rendered_time > 8 ? 8.0 : $rendered_time),
    //                 'late' => $late,
    //                 'undertime' => $undertime,
    //                 'computed_ot' => $ot,
    //             ]);
    //         }

    //         DB::commit();
    //         return $this->response(200, ('Tar has been successfully ' . $message), $request);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    public function computeTime($time)
    {
        $time = floatval($time);
        $h = intval($time);
        $m = ((($time - $h) * 100) / 60);

        return round(($h + $m), 2);
    }
}
