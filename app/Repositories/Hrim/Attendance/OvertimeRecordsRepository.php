<?php

namespace App\Repositories\Hrim\Attendance;

use App\Interfaces\Hrim\Attendance\OvertimeRecordsInterface;
use App\Models\Hrim\Overtime;
use App\Models\Hrim\StatusReference;
use App\Models\User;
use App\Traits\ResponseTrait;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OvertimeRecordsRepository implements OvertimeRecordsInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            if ($request['cut_off'] == 'first') {
                $begin = new DateTime($request['year'] . '-' . ($request['month'] - 1) . '-26');
                $end = new DateTime($request['year'] . '-' . $request['month'] . '-10');
            } else {
                $begin = new DateTime($request['year'] . '-' . $request['month'] . '-11');
                $end = new DateTime($request['year'] . '-' . $request['month'] . '-25');
            }

            // $overtime_records_statuses_tali = StatusReference::withCount(['overtimeRecords' => function ($query) use ($begin, $end, $request) {
            //     $query->whereDate('date_time_from', '>=', $begin)
            //         ->whereDate('date_time_from', '<=', $end);
            // }])->get();
            $users_overtime_records_all = User::whereHas('overtimes', function ($query) use ($begin, $end) {
                $query->whereDate('date_time_from', '>=', $begin)
                    ->whereDate('date_time_from', '<=', $end);
            })
                ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                    $query->where('division_id', Auth::user()->division_id);
                })
                ->count();

            $users_overtime_records_for_approval = User::whereHas('overtimes', function ($query) use ($begin, $end) {
                $query->whereDate('date_time_from', '>=', $begin)
                    ->whereDate('date_time_from', '<=', $end)
                    ->whereIn('final_status_id', [1, 2]);
            })
                ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                    $query->where('division_id', Auth::user()->division_id);
                })
                ->count();

            $users = User::with(['userDetails' => function ($query) {
                $query->with(
                    'branch',
                    'workSchedule',
                    'position',
                    'jobLevel',
                    'department',
                    'division',
                    'employmentCategory',
                    'employmentStatus',
                    'gender',
                    'suffix'
                );
            }])
                ->withSum([
                    'overtimes as total_filed_ot_hours' => function ($query) use ($begin, $end, $request) {
                        $query->whereDate('date_time_from', '>=', $begin)
                            ->whereDate('date_time_from', '<=', $end)
                            ->when($request['status'] ?? false, function ($query) use ($request) {
                                $query->when($request['status'] == 'for_approval', function ($query) {
                                    $query->whereIn('final_status_id', [1, 2]);
                                });
                            })
                            ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                                $query->whereHas('user', function ($query) {
                                    $query->where('division_id', Auth::user()->division_id);
                                });
                            });
                    }
                ], 'overtime_request')
                ->withSum([
                    'overtimes as total_approved_ot_hours' => function ($query) use ($begin, $end, $request) {
                        $query->whereDate('date_time_from', '>=', $begin)
                            ->whereDate('date_time_from', '<=', $end)
                            ->when($request['status'] ?? false, function ($query) use ($request) {
                                $query->when($request['status'] == 'for_approval', function ($query) {
                                    $query->whereIn('final_status_id', [1, 2]);
                                });
                            })
                            ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                                $query->whereHas('user', function ($query) {
                                    $query->where('division_id', Auth::user()->division_id);
                                });
                            });
                    }
                ], 'overtime_approved')
                ->whereHas('overtimes', function ($query) use ($begin, $end, $request) {
                    $query->whereDate('date_time_from', '>=', $begin)
                        ->whereDate('date_time_from', '<=', $end)
                        ->when($request['status'] ?? false, function ($query) use ($request) {
                            $query->when($request['status'] == 'for_approval', function ($query) {
                                $query->whereIn('final_status_id', [1, 2]);
                            });
                        });
                })
                ->whereHas('userDetails', function ($query) use ($request) {
                    $query->when($request['branch'] ?? false, function ($query) use ($request) {
                        $query->where('branch_id', $request['branch']);
                    })
                        ->when($request['employment_category'] ?? false, function ($query) use ($request) {
                            $query->where('employment_category_id', $request['employment_category']);
                                // ->whereHas('category_types', function ($query) use ($request) {
                                //     $query->when($request['employment_category_type'] ?? false, function ($query) use ($request) {
                                //         $query->where('id', $request['employment_category_type']);
                                //     });
                                // });
                        });
                })

                ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                    $query->where('division_id', Auth::user()->division_id);
                })
                ->when($request['employee_id'] ?? false, function ($query) use ($request) {
                    $query->whereHas('userDetails', function ($query) use ($request) {
                        $query->where('employee_number', $request['employee_id']);
                    });
                })
                ->when($request['employee_name'] ?? false, function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request['employee_name'] . '%');
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Overtime List', compact('users_overtime_records_all', 'users_overtime_records_for_approval', 'users'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function showUserOvertimeRecords($id, $request = [])
    {
        DB::beginTransaction();
        try {
            $user = User::find($id);
            $overtime_records = Overtime::with(
                'user',
                'timeLog',
                'dateCategory',
                'firstApprover',
                'secondApprover',
                'thirdApprover',
                'finalStatus',
            )
                ->where('user_id', $id)
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'User Overtime Record List', compact('user', 'overtime_records'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id, $request = [])
    {
        DB::beginTransaction();
        try {
            $overtime_record = Overtime::with(
                'user',
                'timeLog',
                'dateCategory',
                'firstApprover',
                'secondApprover',
                'thirdApprover',
                'adminApprover',
                'finalStatus'
            )->firstWhere([['user_id', $request['user_id']], ['id', $id]]);
            if (!$overtime_record) return $this->response(404, 'Overtime Record', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Overtime Record', $overtime_record);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'user_id' => 'required',
                'approver' => 'required',
                'approved_ot' => 'required_if:is_approved,1',
                'remarks' => 'required',
                'is_approved' => 'sometimes',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function approve($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $response = $this->show($id, ['user_id' => $validated['user_id']]);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $overtime_record = $response['result'];

            if ($overtime_record->first_approver && $validated['approver'] == 'first') {
                if ($overtime_record->first_approver == Auth::user()->id || Auth::user()->level_id == 5 && $overtime_record->first_approver) {
                    if (!$validated['is_approved'] && !$validated['remarks']) {
                        return $this->response(400, 'Bad Request', [
                            'remarks' => 'The first approver remarks field is required.'
                        ]);
                    }

                    if ($overtime_record->second_status && !$validated['is_approved']) {
                        return $this->response(400, 'Bad Request', [
                            'approver' => 'The second approver must be disapproved.'
                        ]);
                    } else {
                        $overtime_record->update([
                            'first_status' => $validated['is_approved'] ?? 0,
                            'first_approval_date' => date("Y-m-d"),
                            'first_approver_remarks' => $validated['remarks'],
                        ]);
                    }
                }
            }

            if ($overtime_record->second_approver && $validated['approver'] == 'second') {
                if ($overtime_record->first_status || !$validated['is_approved']) {
                    if ($overtime_record->second_approver == Auth::user()->id || Auth::user()->level_id == 5 && $overtime_record->second_approver) {
                        if (!$validated['is_approved'] && !$validated['remarks']) {
                            return $this->response(400, 'Bad Request', [
                                'remarks' => 'The approver remarks 2 field is required.'
                            ]);
                        }

                        if ($overtime_record->third_status && !$validated['is_approved']) {
                            return $this->response(400, 'Bad Request', [
                                'approver' => 'The third approver must be disapproved.'
                            ]);
                        } else {
                            $overtime_record->update([
                                'second_status' => $validated['is_approved'] ?? 0,
                                'second_approval_date' => date("Y-m-d"),
                                'second_approver_remarks' => $validated['remarks'],
                            ]);
                        }
                    }
                } else {
                    return $this->response(400, 'Bad Request', [
                        'approver' => 'The first approver must be approved.'
                    ]);
                }
            }


            if ($overtime_record->third_approver && $validated['approver'] == 'third') {
                if ($overtime_record->second_status || !$validated['is_approved']) {
                    if ($overtime_record->third_approver == Auth::user()->id || Auth::user()->level_id == 5 && $overtime_record->third_approver) {
                        if (!$validated['is_approved'] && !$validated['remarks']) {
                            return $this->response(400, 'Bad Request', [
                                'remarks' => 'The third approver remarks field is required.'
                            ]);
                        }

                        $overtime_record->update([
                            'third_status' => $validated['is_approved'],
                            'third_approval_date' => date("Y-m-d"),
                            'third_approver_remarks' => $validated['remarks'],
                        ]);
                    }
                } else {
                    return $this->response(400, 'Bad Request', [
                        'approver' => 'The second approver must be approved.'
                    ]);
                }
            }



            $final_approved = true;
            if ($overtime_record->first_approver && !$overtime_record->first_status) {
                $final_approved = false;
            }
            if ($overtime_record->second_approver && !$overtime_record->second_status) {
                $final_approved = false;
            }
            if ($overtime_record->third_approver && !$overtime_record->third_status) {
                $final_approved = false;
            }

            if ($overtime_record->third_approver && !$overtime_record->third_status && $overtime_record->third_approver_remarks) {
                $overtime_record->update([
                    'final_status_id' => 4
                ]);
            } elseif ($overtime_record->second_approver && !$overtime_record->second_status && $overtime_record->second_approver_remarks) {
                $overtime_record->update([
                    'final_status_id' => 4
                ]);
            } elseif ($overtime_record->first_approver && !$overtime_record->first_status && $overtime_record->first_approver_remarks) {
                $overtime_record->update([
                    'final_status_id' => 4
                ]);
            } else {
                $overtime_record->update([
                    'final_status_id' => ($final_approved ? 3 : 2)
                ]);
            }

            if ($final_approved) {
                $overtime_record->update([
                    'overtime_approved' => $validated['approved_ot']
                ]);
            }

            if ($validated['approver'] == "bypass") {
                if ($validated['is_approved']) {
                    $overtime_record->update([
                        'admin_approver' => Auth::user()->id,
                        'admin_approval_date' => date("Y-m-d"),
                        'admin_approver_remarks' => $validated['remarks'],
                        'final_status_id' => 3
                    ]);
                } else {
                    $overtime_record->update([
                        'admin_approver' => Auth::user()->id,
                        'admin_approval_date' => date("Y-m-d"),
                        'admin_approver_remarks' => $validated['remarks'],
                        'final_status_id' => 4
                    ]);
                }
            }

            DB::commit();
            return $this->response(200, ('Overtime has been successfully ' . ($validated['is_approved'] ? 'approved!' : 'declined!')), $request);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    // public function approve($id, $request)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $response = $this->approveValidation($request);
    //         if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
    //         $validated = $response['result'];

    //         $response = $this->show($id, ['user_id' => $validated['user_id']]);
    //         if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
    //         $overtime_record = $response['result'];

    //         $final_approved = false;

    //         if ($overtime_record->first_approver) {
    //             if ($overtime_record->first_approver == $validated['user_id'] || Auth::user()->level_id == 5 && $overtime_record->first_approver) {
    //                 if (!$validated['is_first_approved'] && !$validated['first_approver_remarks']) {
    //                     return $this->response(400, 'Bad Request', [
    //                         'first_approver_remarks' => 'The first approver remarks field is required.'
    //                     ]);
    //                 }

    //                 if ($validated['is_second_approved'] && !$validated['is_first_approved']) {
    //                     return $this->response(400, 'Bad Request', [
    //                         'second_approver_id' => 'The second approver must be disapproved.'
    //                     ]);
    //                 } else {
    //                     $overtime_record->update([
    //                         'first_status' => $validated['is_first_approved'] ?? 0,
    //                         'first_approval_date' => date("Y-m-d"),
    //                         'first_approver_remarks' => $validated['first_approver_remarks'],
    //                     ]);
    //                 }

    //                 if ($validated['is_first_approved']) {
    //                     $final_approved = true;
    //                 } else {
    //                     $final_approved = false;
    //                 }
    //             }
    //         }


    //         if ($overtime_record->second_approver) {
    //             if ($validated['is_first_approved'] || !$validated['is_second_approved']) {
    //                 if ($overtime_record->second_approver == $validated['user_id'] || Auth::user()->level_id == 5 && $overtime_record->second_approver) {
    //                     if (!$validated['is_second_approved'] && !$validated['second_approver_remarks']) {
    //                         return $this->response(400, 'Bad Request', [
    //                             'second_approver_remarks' => 'The approver remarks 2 field is required.'
    //                         ]);
    //                     }

    //                     if ($validated['is_third_approved'] && !$validated['is_second_approved']) {
    //                         return $this->response(400, 'Bad Request', [
    //                             'third_approver_id' => 'The third approver must be disapproved.'
    //                         ]);
    //                     } else {
    //                         $overtime_record->update([
    //                             'second_status' => $validated['is_second_approved'] ?? 0,
    //                             'second_approval_date' => date("Y-m-d"),
    //                             'second_approver_remarks' => $validated['second_approver_remarks'],
    //                         ]);
    //                     }
    //                 }

    //                 if ($validated['is_second_approved']) {
    //                     $final_approved = true;
    //                 } else {
    //                     $final_approved = false;
    //                 }
    //             } else {
    //                 return $this->response(400, 'Bad Request', [
    //                     'first_approver_id' => 'The first approver must be approved.'
    //                 ]);
    //             }
    //         }


    //         if ($overtime_record->third_approver) {
    //             if ($validated['is_second_approved']  || !$validated['is_third_approved']) {
    //                 if ($overtime_record->third_approver == $validated['user_id'] || Auth::user()->level_id == 5 && $overtime_record->third_approver) {
    //                     if (!$validated['is_third_approved'] && !$validated['third_approver_remarks']) {
    //                         return $this->response(400, 'Bad Request', [
    //                             'third_approver_remarks' => 'The third approver remarks field is required.'
    //                         ]);
    //                     }

    //                     $overtime_record->update([
    //                         'third_status' => $validated['is_third_approved'],
    //                         'third_approval_date' => date("Y-m-d"),
    //                         'third_approver_remarks' => $validated['third_approver_remarks'],
    //                     ]);
    //                 }

    //                 if ($validated['is_third_approved']) {
    //                     $final_approved = true;
    //                 } else {
    //                     $final_approved = false;
    //                 }
    //             } else {
    //                 return $this->response(400, 'Bad Request', [
    //                     'second_approver_id' => 'The second approver must be approved.'
    //                 ]);
    //             }
    //         }

    //         $message = 'Approved';
    //         if ($overtime_record->third_approver && !$overtime_record->third_status && $overtime_record->third_approver_remarks) {
    //             $overtime_record->update([
    //                 'final_status_id' => 4
    //             ]);
    //             $message = 'Rejected';
    //         } elseif ($overtime_record->second_approver && !$overtime_record->second_status && $overtime_record->second_approver_remarks) {
    //             $overtime_record->update([
    //                 'final_status_id' => 4
    //             ]);
    //             $message = 'Rejected';
    //         } elseif ($overtime_record->first_approver && !$overtime_record->first_status && $overtime_record->first_approver_remarks) {
    //             $overtime_record->update([
    //                 'final_status_id' => 4
    //             ]);
    //             $message = 'Rejected';
    //         } else {
    //             $overtime_record->update([
    //                 'final_status_id' => ($final_approved ? 3 : 2)
    //             ]);
    //         }

    //         if($final_approved){
    //             $overtime_record->update([
    //                 'overtime_approved' => $validated['approved_overtime_hours']
    //             ]);
    //         }

    //         DB::commit();
    //         return $this->response(200, ('Overtime has been successfully ' . $message), $request);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }
}
