<?php

namespace App\Repositories\Hrim\Attendance;

use App\Interfaces\Hrim\Attendance\DailyTimeRecordsInterface;
use App\Models\Hrim\TimeLog;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DailyTimeRecordsRepository implements DailyTimeRecordsInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $time_logs = TimeLog::with(['user' => function ($query) {
                $query->with(['userDetails' => function ($query) {
                    $query->with('employmentCategory');
                }]);
            }])
                ->when($request['branch'] ?? false, function ($query) use ($request) {
                    $query->whereHas('user', function ($query) use ($request) {
                        $query->whereHas('userDetails', function ($query) use ($request) {
                            $query->where('branch_id', $request['branch']);
                        });
                    });
                })
                ->when($request['employment_category'] ?? false, function ($query) use ($request) {
                    $query->whereHas('user', function ($query) use ($request) {
                        $query->whereHas('userDetails', function ($query) use ($request) {
                            $query->where('employment_category_id', $request['employment_category']);
                        });
                    });
                })
                ->when($request['employee_id'] ?? false, function ($query) use ($request) {
                    $query->whereHas('user', function ($query) use ($request) {
                        $query->whereHas('userDetails', function ($query) use ($request) {
                            $query->where('employee_number', $request['employee_id']);
                        });
                    });
                })
                ->when($request['employee_name'] ?? false, function ($query) use ($request) {
                    $query->whereHas('user', function ($query) use ($request) {
                        $query->where('name', 'like', '%' . $request['employee_name'] . '%');
                    });
                })
                ->when($request['date'] ?? false, function ($query) use ($request) {
                    $query->whereDate('date', $request['date']);
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                    $query->whereHas('user', function ($query) {
                        $query->where('division_id', Auth::user()->division_id);
                    });
                })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Time Log List', $time_logs);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
