<?php

namespace App\Repositories\Hrim\Attendance;

use App\Interfaces\Hrim\Attendance\LeaveRecordsInterface;
use App\Models\Hrim\Leave;
use App\Models\Hrim\StatusReference;
use App\Models\LeaveDetails;
use App\Models\User;
use App\Traits\ResponseTrait;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LeaveRecordsRepository implements LeaveRecordsInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            if ($request['cut_off'] == 'first') {
                $begin = new DateTime($request['year'] . '-' . ($request['month'] - 1) . '-26');
                $end = new DateTime($request['year'] . '-' . $request['month'] . '-10');
            } else {
                $begin = new DateTime($request['year'] . '-' . $request['month'] . '-11');
                $end = new DateTime($request['year'] . '-' . $request['month'] . '-25');
            }
            // $leave_records_statuses_tali = StatusReference::withCount(['leaveRecords' => function ($query) use ($request) {
            //     $query->when($request['year'] ?? false, function ($query) use ($request) {
            //         $query->whereYear('inclusive_date_from', '>=', $request['year'])
            //             ->whereYear('inclusive_date_to', '<=', $request['year']);
            //     })
            //         ->when($request['payout_date'] ?? false, function ($query) use ($request) {
            //             $query->whereDate('inclusive_date_from', $request['payout_date']);
            //         })
            //         ->when(Auth::user()->level_id != 5 || Auth::user()->division_id != 5 ?? false, function ($query) {
            //             $query->whereHas('user', function ($query) {
            //                 $query->where('division_id', Auth::user()->division_id);
            //             });
            //         });
            // }])
            //     ->get();

            // $users_leave_records_all = User::whereHas('leaves', function ($query) use ($request) {
            //     $query->when($request['year'] ?? false, function ($query) use ($request) {
            //     $query->whereYear('inclusive_date_from', '>=', $request['year'])
            //         ->whereYear('inclusive_date_from', '<=', $request['year']);
            //     })
            //         ->when($request['payout_date'] ?? false, function ($query) use ($request) {
            //             $query->whereDate('inclusive_date_from', $request['payout_date']);
            //         });
            // })
            $users_leave_records_all = User::whereHas('leaves', function ($query) use ($begin, $end) {
                $query->whereDate('inclusive_date_from', '>=', $begin)
                    ->whereDate('inclusive_date_to', '<=', $end);
            })
                ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                    $query->where('division_id', Auth::user()->division_id);
                })
                ->count();

            $users_leave_records_for_approval = User::whereHas('leaves', function ($query) use ($begin, $end) {
                $query->whereDate('inclusive_date_from', '>=', $begin)
                    ->whereDate('inclusive_date_to', '<=', $end);
                // ->whereIn('final_status_id', [1, 2]);
            })
                // $users_leave_records_for_approval = User::whereHas('leaves', function ($query) use ($request) {
                //     $query->when($request['year'] ?? false, function ($query) use ($request) {
                //         $query->whereYear('inclusive_date_from', '>=', $request['year'])
                //             ->whereYear('inclusive_date_to', '<=', $request['year']);
                //     })
                //         ->when($request['payout_date'] ?? false, function ($query) use ($request) {
                //             $query->whereDate('inclusive_date_from', $request['payout_date']);
                //         });
                // })
                ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                    $query->where('division_id', Auth::user()->division_id);
                })
                ->count();

            $users = User::with(['userDetails' => function ($query) {
                $query->with(
                    'branch',
                    'workSchedule',
                    'position',
                    'jobLevel',
                    'department',
                    'division',
                    'employmentCategory',
                    'employmentStatus',
                    'gender',
                    'suffix'
                );
            }])
                ->withSum([
                    'leaves as total_points' => function ($query) use ($request) {
                        $query->whereIn('is_with_pay', [1, 3])
                            ->where('final_status_id', 3)
                            ->when($request['status'] ?? false, function ($query) use ($request) {
                                $query->when($request['status'] == 'for_approval', function ($query) {
                                    $query->whereIn('final_status_id', [1, 2]);
                                });
                            })
                            ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                                $query->where('users.division_id', Auth::user()->division_id);
                            });
                    }
                ], 'points')
                ->withSum([
                    'leaves as total_filed_leave_with_pay' => function ($query) use ($request) {
                        $query->where('is_with_pay', 1)
                            ->where('final_status_id', 3)
                            ->when($request['status'] ?? false, function ($query) use ($request) {
                                $query->when($request['status'] == 'for_approval', function ($query) {
                                    $query->whereIn('final_status_id', [1, 2]);
                                });
                            });
                    }
                ], 'points')
                ->withCount(['leaves as total_filed_leave_without_pay' => function ($query) use ($request) {
                    $query->where('is_with_pay', 0)
                        ->when($request['status'] ?? false, function ($query) use ($request) {
                            $query->when($request['status'] == 'for_approval', function ($query) {
                                $query->whereIn('final_status_id', [1, 2]);
                            });
                        });
                }])
                ->withSum(['leaves as total_approved_leave_with_pay' => function ($query) {
                    $query->where([
                        ['is_with_pay', 1],
                        ['final_status_id', 3],
                    ]);
                }], 'points')
                ->withCount(['leaves as total_approved_leave_without_pay' => function ($query) {
                    $query->where([
                        ['is_with_pay', 0],
                        ['final_status_id', 3],
                    ]);
                }])
                // ->whereHas('leaves', function ($query) use ($request) {
                //     $query->when($request['year'] ?? false, function ($query) use ($request) {
                //         $query->whereYear('inclusive_date_from', '>=', $request['year'])
                //             ->whereYear('inclusive_date_to', '<=', $request['year']);
                //     })
                //         ->when($request['payout_date'] ?? false, function ($query) use ($request) {
                //             $query->whereDate('inclusive_date_from', $request['payout_date']);
                //         });
                // })
                ->whereHas('leaves', function ($query) use ($begin, $end, $request) {
                    $query->whereDate('inclusive_date_from', '>=', $begin)
                        ->whereDate('inclusive_date_to', '<=', $end);
                    // ->when($request['status'] ?? false, function ($query) use ($request) {
                    //     $query->when($request['status'] == 'for_approval', function ($query) {
                    //         $query->whereIn('final_status_id', [1, 2]);
                    //     });
                    // });
                })

                ->whereHas('userDetails', function ($query) use ($request) {
                    $query->when($request['branch'] ?? false, function ($query) use ($request) {
                        $query->where('branch_id', $request['branch']);
                    })
                        ->when($request['employment_category'] ?? false, function ($query) use ($request) {
                            $query->where('employment_category_id', $request['employment_category']);
                        });
                })
                ->when(Auth::user()->level_id != 5 && Auth::user()->division_id != 5, function ($query) {
                    $query->where('division_id', Auth::user()->division_id);
                })
                ->when($request['employee_id'] ?? false, function ($query) use ($request) {
                    $query->whereHas('userDetails', function ($query) use ($request) {
                        $query->where('employee_number', $request['employee_id']);
                    });
                })
                ->when($request['employee_name'] ?? false, function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request['employee_name'] . '%');
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Leave Summary', compact('users_leave_records_all', 'users_leave_records_for_approval', 'users'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function showUserLeaves($id, $request = [])
    {
        DB::beginTransaction();
        try {
            $user = User::find($id);
            $leaves = Leave::with(
                'user',
                'leaveType',
                'leaveDayType',
                'relieverUser',
                'firstApprover',
                'secondApprover',
                'thirdApprover',
                'finalStatus',
                'attachments'
            )
                ->where('user_id', $id)
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'User Leave List', compact('user', 'leaves'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id, $request = [])
    {
        DB::beginTransaction();
        try {
            $leave = Leave::with(
                'user',
                'leaveType',
                'leaveDayType',
                'relieverUser',
                'firstApprover',
                'secondApprover',
                'thirdApprover',
                'finalStatus',
                'attachments'
            )
                ->withCount('attachments')
                ->where('user_id', $request['user_id'])
                ->find($id);

            if (!$leave) return $this->response(404, 'Leave', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Leave', $leave);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'user_id' => 'required',
                'approver' => 'required',
                'remarks' => 'required_if:is_approved,0',
                'is_approved' => 'sometimes',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function approve($id, $request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $response = $this->show($id, ['user_id' => $validated['user_id']]);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $leave = $response['result'];

            $begin = new DateTime($leave->inclusive_date_from);
            $end   = new DateTime($leave->inclusive_date_to);

            for ($i = $begin; $i <= $end; $i->modify('+1 day')) {
                $leavedates[] = $i->format("Y-m-d");
            }

            if ($leave->first_approver && $validated['approver'] == 'first') {
                if ($leave->first_approver == Auth::user()->id || Auth::user()->level_id == 5 && $leave->first_approver) {
                    if (!$validated['is_approved'] && !$validated['remarks']) {
                        return $this->response(400, 'Bad Request', [
                            'remarks' => 'The first approver remarks field is required.'
                        ]);
                    }

                    if ($leave->second_status && !$validated['is_approved']) {
                        return $this->response(400, 'Bad Request', [
                            'approver' => 'The second approver must be disapproved.'
                        ]);
                    } else {
                        $leave->update([
                            'first_status' => $validated['is_approved'] ?? 0,
                            'first_approval_date' => date("Y-m-d"),
                            'first_approver_remarks' => $validated['remarks'],
                        ]);
                    }
                }
            }

            if ($leave->second_approver && $validated['approver'] == 'second') {
                if ($leave->first_status || !$validated['is_approved']) {
                    if ($leave->second_approver == Auth::user()->id || Auth::user()->level_id == 5 && $leave->second_approver) {
                        if (!$validated['is_approved'] && !$validated['remarks']) {
                            return $this->response(400, 'Bad Request', [
                                'remarks' => 'The approver remarks 2 field is required.'
                            ]);
                        }

                        if ($leave->third_status && !$validated['is_approved']) {
                            return $this->response(400, 'Bad Request', [
                                'approver' => 'The third approver must be disapproved.'
                            ]);
                        } else {
                            $leave->update([
                                'second_status' => $validated['is_approved'] ?? 0,
                                'second_approval_date' => date("Y-m-d"),
                                'second_approver_remarks' => $validated['remarks'],
                            ]);
                        }
                    }
                } else {
                    return $this->response(400, 'Bad Request', [
                        'approver' => 'The first approver must be approved.'
                    ]);
                }
            }


            if ($leave->third_approver && $validated['approver'] == 'third') {
                if ($leave->second_status || !$validated['is_approved']) {
                    if ($leave->third_approver == Auth::user()->id || Auth::user()->level_id == 5 && $leave->third_approver) {
                        if (!$validated['is_approved'] && !$validated['remarks']) {
                            return $this->response(400, 'Bad Request', [
                                'remarks' => 'The third approver remarks field is required.'
                            ]);
                        }

                        $leave->update([
                            'third_status' => $validated['is_approved'],
                            'third_approval_date' => date("Y-m-d"),
                            'third_approver_remarks' => $validated['remarks'],
                        ]);
                    }
                } else {
                    return $this->response(400, 'Bad Request', [
                        'approver' => 'The second approver must be approved.'
                    ]);
                }
            }


            $final_approved = true;
            if ($leave->first_approver && !$leave->first_status) {
                $final_approved = false;
            }
            if ($leave->second_approver && !$leave->second_status) {
                $final_approved = false;
            }
            if ($leave->third_approver && !$leave->third_status) {
                $final_approved = false;
            }

            if ($leave->third_approver && !$leave->third_status && $leave->third_approver_remarks) {
                $leave->update([
                    'final_status_id' => 4
                ]);

                $response = LeaveDetails::where('leave_id', $id)->get();
                $leavedetails = $response;

                foreach ($leavedetails as $leavedetail) {
                    $leavedetail->delete();
                }
            } elseif ($leave->second_approver && !$leave->second_status && $leave->second_approver_remarks) {
                $leave->update([
                    'final_status_id' => 4
                ]);

                $response = LeaveDetails::where('leave_id', $id)->get();
                $leavedetails = $response;

                foreach ($leavedetails as $leavedetail) {
                    $leavedetail->delete();
                }
            } elseif ($leave->first_approver && !$leave->first_status && $leave->first_approver_remarks) {
                $leave->update([
                    'final_status_id' => 4
                ]);

                $response = LeaveDetails::where('leave_id', $id)->get();
                $leavedetails = $response;

                foreach ($leavedetails as $leavedetail) {
                    $leavedetail->delete();
                }
            } else {
                $leave->update([
                    'final_status_id' => ($final_approved ? 3 : 2)
                ]);
            }

            if ($final_approved) {
                foreach ($leavedates as $i => $leavedate) {
                    $request = LeaveDetails::create([
                        'leave_id' => $id,
                        'user_id' => $validated['user_id'],
                        'leave_date' => $leavedates[$i],
                        'leave_type' => $leave->is_with_pay,
                        'apply_for' => $leave->leave_type_id == 2 ? 0.5 : 1,
                    ]);
                }
            }

            if ($validated['approver'] == "bypass") {
                if ($validated['is_approved']) {
                    $leave->update([
                        'admin_approver' => Auth::user()->id,
                        'admin_approval_date' => date("Y-m-d"),
                        'admin_approver_remarks' => $validated['remarks'],
                        'final_status_id' => 3
                    ]);

                    foreach ($leavedates as $i => $leavedate) {
                        $request = LeaveDetails::create([
                            'leave_id' => $id,
                            'user_id' => $validated['user_id'],
                            'leave_date' => $leavedates[$i],
                            'leave_type' => $leave->is_with_pay,
                            'apply_for' => $leave->leave_type_id == 2 ? 0.5 : 1,
                        ]);
                    }
                } else {
                    $leave->update([
                        'admin_approver' => Auth::user()->id,
                        'admin_approval_date' => date("Y-m-d"),
                        'admin_approver_remarks' => $validated['remarks'],
                        'final_status_id' => 4
                    ]);

                    $response = LeaveDetails::where('leave_id', $id)->get();
                    $leavedetails = $response;

                    foreach ($leavedetails as $leavedetail) {
                        $leavedetail->delete();
                    }
                }
            }



            DB::commit();
            return $this->response(200, ('Leave has been successfully ' . ($validated['is_approved'] ? 'approved!' : 'declined!')), $request);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // public function approve($id, $request)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $response = $this->approveValidation($request);
    //         if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
    //         $validated = $response['result'];

    //         $response = $this->show($id, ['user_id' => $validated['user_id']]);
    //         if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
    //         $leave = $response['result'];
    //         $leaverecord = Leave::find($id);
    //         if (!$leaverecord) return $this->response(404, 'Leave', 'Not Found!');

    //         $final_approved = false;

    //         $begin = new DateTime($leave->inclusive_date_from);
    //         $end   = new DateTime($leave->inclusive_date_to);

    //         for ($i = $begin; $i <= $end; $i->modify('+1 day')) {
    //             $leavedates[] = $i->format("Y-m-d");
    //         }






    //         if ($leave->first_approver) {
    //             if ($leave->first_approver == $validated['user_id'] || Auth::user()->level_id == 5 && $leave->first_approver) {
    //                 if (!$validated['is_first_approved'] && !$validated['first_approver_remarks']) {
    //                     return $this->response(400, 'Bad Request', [
    //                         'first_approver_remarks' => 'The first approver remarks field is required.'
    //                     ]);
    //                 }

    //                 if ($validated['is_second_approved'] && !$validated['is_first_approved']) {
    //                     return $this->response(400, 'Bad Request', [
    //                         'second_approver_id' => 'The second approver must be disapproved.'
    //                     ]);
    //                 } else {
    //                     $leave->update([
    //                         'first_status' => $validated['is_first_approved'] ?? 0,
    //                         'first_approval_date' => date("Y-m-d"),
    //                         'first_approver_remarks' => $validated['first_approver_remarks'],
    //                     ]);
    //                 }

    //                 if ($validated['is_first_approved']) {
    //                     $final_approved = true;
    //                 } else {
    //                     $final_approved = false;
    //                 }
    //             }
    //         }


    //         if ($leave->second_approver) {
    //             if ($validated['is_first_approved'] || !$validated['is_second_approved']) {
    //                 if ($leave->second_approver == $validated['user_id'] || Auth::user()->level_id == 5 && $leave->second_approver) {
    //                     if (!$validated['is_second_approved'] && !$validated['second_approver_remarks']) {
    //                         return $this->response(400, 'Bad Request', [
    //                             'second_approver_remarks' => 'The approver remarks 2 field is required.'
    //                         ]);
    //                     }

    //                     if ($validated['is_third_approved'] && !$validated['is_second_approved']) {
    //                         return $this->response(400, 'Bad Request', [
    //                             'third_approver_id' => 'The third approver must be disapproved.'
    //                         ]);
    //                     } else {
    //                         $leave->update([
    //                             'second_status' => $validated['is_second_approved'] ?? 0,
    //                             'second_approval_date' => date("Y-m-d"),
    //                             'second_approver_remarks' => $validated['second_approver_remarks'],
    //                         ]);
    //                     }
    //                 }

    //                 if ($validated['is_second_approved']) {
    //                     $final_approved = true;
    //                 } else {
    //                     $final_approved = false;
    //                 }
    //             } else {
    //                 return $this->response(400, 'Bad Request', [
    //                     'first_approver_id' => 'The first approver must be approved.'
    //                 ]);
    //             }
    //         }


    //         if ($leave->third_approver) {
    //             if ($validated['is_second_approved']  || !$validated['is_third_approved']) {
    //                 if ($leave->third_approver == $validated['user_id'] || Auth::user()->level_id == 5 && $leave->third_approver) {
    //                     if (!$validated['is_third_approved'] && !$validated['third_approver_remarks']) {
    //                         return $this->response(400, 'Bad Request', [
    //                             'third_approver_remarks' => 'The third approver remarks field is required.'
    //                         ]);
    //                     }

    //                     $leave->update([
    //                         'third_status' => $validated['is_third_approved'],
    //                         'third_approval_date' => date("Y-m-d"),
    //                         'third_approver_remarks' => $validated['third_approver_remarks'],
    //                     ]);
    //                 }

    //                 if ($validated['is_third_approved']) {
    //                     $final_approved = true;
    //                 } else {
    //                     $final_approved = false;
    //                 }
    //             } else {
    //                 return $this->response(400, 'Bad Request', [
    //                     'second_approver_id' => 'The second approver must be approved.'
    //                 ]);
    //             }
    //         }



    //         $message = 'Approved';
    //         if ($leave->third_approver && !$leave->third_status && $leave->third_approver_remarks) {
    //             $leave->update([
    //                 'final_status_id' => 4
    //             ]);
    //             $message = 'Rejected';
    //         } elseif ($leave->second_approver && !$leave->second_status && $leave->second_approver_remarks) {
    //             $leave->update([
    //                 'final_status_id' => 4
    //             ]);
    //             $message = 'Rejected';
    //         } elseif ($leave->first_approver && !$leave->first_status && $leave->first_approver_remarks) {
    //             $leave->update([
    //                 'final_status_id' => 4
    //             ]);
    //             $message = 'Rejected';
    //         } else {
    //             $leave->update([
    //                 'final_status_id' => ($final_approved ? 3 : 2)
    //             ]);

    //             foreach ($leavedates as $i => $leavedate) {
    //                 $request = LeaveDetails::create([
    //                     'user_id' => $validated['user_id'],
    //                     'leave_date' => $leavedates[$i],
    //                     'leave_type' => $leaverecord->is_with_pay,
    //                     'apply_for' => $leaverecord->leave_type_id == 2 ? 0.5 : 1,
    //                 ]);
    //             }

    //         }

    //         DB::commit();
    //         return $this->response(200, ('Leave has been successfully ' . $message), $request);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }
}
