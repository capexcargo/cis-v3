<?php

namespace App\Repositories\Hrim;

use App\Interfaces\Hrim\LoaManagementInterface;
use App\Models\Hrim\LoaManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LoaManagementRepository implements LoaManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $loa_managements = LoaManagement::with('division', 'departments', 'branch', 'loaType', 'firstApprover', 'secondApprover', 'thirdApprover')
                ->when($request['division'] ?? false, function ($query) use ($request) {
                    $query->where('division_id', $request['division']);
                })
                ->when($request['branch'] ?? false, function ($query) use ($request) {
                    $query->where('branch_id', $request['branch']);
                })
                ->when($request['type'] ?? false, function ($query) use ($request) {
                    $query->where('loa_type_id', $request['type']);
                })
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Loa List', $loa_managements);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'division' => 'required',
                'department_id' => 'required',
                'branch' => 'required',
                'type' => 'required',
                'first_approver' => 'required',
                'second_approver' => 'sometimes',
                'third_approver' => 'sometimes',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateOrCreate($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $loa_management = LoaManagement::updateOrCreate([
                'division_id' => $validated['division'],
                'branch_id' => $validated['branch'],
                'loa_type_id' => $validated['type'],
                'department_id' => $validated['department_id'],
            ], [
                'first_approver' => $validated['first_approver'],
                'second_approver' => ($validated['second_approver'] ? $validated['second_approver'] : null),
                'third_approver' => ($validated['third_approver'] ? $validated['third_approver'] : null),
            ]);

            DB::commit();
            return $this->response(200, ('Loa Successfully ' . ($loa_management->wasRecentlyCreated ? 'Created' : 'Updated')), $loa_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id, $request = [])
    {
        DB::beginTransaction();
        try {
            $loa_management = LoaManagement::find($id);
            if (!$loa_management) return $this->response(404, 'Loa', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Loa', $loa_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function checkLoaManagement($request)
    {
        DB::beginTransaction();
        try {
            $loa_management = LoaManagement::where([
                'division_id' => $request['division'],
                'department_id' => $request['department_id'],
                'branch_id' => $request['branch'],
                'loa_type_id' => $request['type'],
            ])
                ->first();

            if (!$loa_management) return $this->response(404, 'Loa', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Loa', $loa_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
