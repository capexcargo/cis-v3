<?php

namespace App\Repositories\Hrim\CompanyManagement;

use App\Interfaces\Hrim\CompanyManagement\EmployerComplianceInterface;
use App\Models\Hrim\EmployerCompliance;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EmployerComplianceRepository implements EmployerComplianceInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $employer_compliance = EmployerCompliance::when($request['with_trashed'] ?? false, function ($query) {
                $query->withTrashed();
            })->when($request['sort_field'], function ($query) use ($request) {
                $query->orderBy($request['sort_field'], $request['sort_type']);
            })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Employer Compliance List', $employer_compliance);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'image' => 'required|' .  config('filesystems.validation_image'),
                'title' => 'required',
                'description' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());
            $validated = $validator->validated();
            
            $response = $this->storeImage($validated['image']);
            if ($response['code'] == 500) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            $employer_compliance = EmployerCompliance::create([
                'path' => $response['result']['file_path'],
                'name' => $response['result']['file_name'],
                'extension' => $validated['image']->extension(),
                'title' => $validated['title'],
                'description' => $validated['description'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();
            return $this->response(200, 'Employer compliance has been successfully created!', $employer_compliance);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id, $request = [])
    {
        $employer_compliance = EmployerCompliance::when($request['with_trashed'] ?? false, function ($query) {
            $query->withTrashed();
        })
            ->find($id);
        if (!$employer_compliance) return $this->response(404, 'Employer Compliance', 'Not Found!');
        return $this->response(200, 'Employer Compliance', $employer_compliance);
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'image' => 'sometimes|' .  config('filesystems.validation_image'),
                'title' => 'required',
                'description' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());
            $validated = $validator->validated();

            $response = $this->show($id);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $employer_compliance = $response['result'];

            if ($validated['image']) {
                $response = $this->storeImage($validated['image']);
                if ($response['code'] == 500) return $this->response(400, 'Please Fill Required Field', $validator->errors());

                $employer_compliance->update([
                    'path' => $response['result']['file_path'],
                    'name' => $response['result']['file_name'],
                    'extension' => $validated['image']->extension(),
                ]);
            }

            $employer_compliance->update([
                'title' => $validated['title'],
                'description' => $validated['description'],
            ]);

            DB::commit();
            return $this->response(200, 'Employer compliance has been successfully updated!', $employer_compliance);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);

            $employer_compliance = $response['result'];
            $employer_compliance->delete();

            DB::commit();
            return $this->response(200, 'Employer compliance has been successfully deleted!', $employer_compliance);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function restore($id)
    {
        DB::beginTransaction();
        try {
            $request = [
                'with_trashed' => true
            ];

            $response = $this->show($id, $request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);

            $employer_compliance = $response['result'];
            $employer_compliance->restore();

            DB::commit();
            return $this->response(200, 'Employer compliance has been successfully restored!', $employer_compliance);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function storeImage($image)
    {
        DB::beginTransaction();
        try {

            $year = date('Y', strtotime(now()));
            $month = strtolower(date('F', strtotime(now())));

            $file_path = strtolower(str_replace(" ", "_", 'employer_compliance/' . $year . '/' . $month . '/'));
            $file_name = date('mdYHis') . uniqid() . '.' . $image->extension();

            Storage::disk('hrim_gcs')->putFileAs($file_path, $image, $file_name);

            DB::commit();
            return $this->response(200, 'Successfully Save', compact('file_path', 'file_name'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
