<?php

namespace App\Repositories\Hrim\CompanyManagement\Bulletin;

use App\Interfaces\Hrim\CompanyManagement\Bulletin\AnnouncementInterface;
use App\Models\Hrim\Announcements;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class AnnouncementRepository implements AnnouncementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $attachment = Announcements::with('postedBy', 'attachments')->when($request['sort_field'], function ($query) use($request) {
                $query->orderBy($request['sort_field'], $request['sort_type']);
            })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Attachment List', $attachment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        $announcement = Announcements::with('postedBy', 'attachments')->find($id);

        if (!$announcement) return $this->response(404, 'Announcement', 'Not Found!');

        return $this->response(200, 'Announcement', $announcement);
    }

    public function create($validated)
    {
        // dd($validated);
        DB::beginTransaction();
        try {
            $announcement = Announcements::create([
                'announcement_no' => $validated['announcement_no'],
                'date_posted' => $validated['date_posted'],
                'title' => $validated['title'],
                'details' => $validated['details'],
                'posted_by' => Auth::user()->id,
            ]);

            $response = $this->attachments($announcement, $validated['attachments']);

            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            DB::commit();
            return $this->response(200, 'Announcement has been successfully published!', $announcement);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update(Announcements $announcement, $validated)
    {
        DB::beginTransaction();
        try {

            $announcement->update([
                'title' => $validated['title'],
                'details' => $validated['details']
            ]);

            $response = $this->attachments($announcement, $validated['attachments']);

            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            DB::commit();
            return $this->response(200, 'Announcement has been successfully updated!', $announcement);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function attachments($announcement, $attachments)
    {

        DB::beginTransaction();
        try {
            // $month = strtolower(date('F', strtotime($announcement->created_at)));
            // $year = date('Y', strtotime($announcement->created_at));
            $date = date('Y/m/d', strtotime($announcement->created_at));

            foreach ($attachments as $attachment) {

                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $announcement->attachments()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(" ", "_", 'bulletin/' . $date . '/' . 'announcement/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        Storage::disk('hrim_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } else if (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('hrim_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }
                    $announcement->attachments()->updateOrCreate(
                        [
                            'id' => $attachment['id']
                        ],
                        [
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                        ]
                    );
                }
            }

            DB::commit();
            return $this->response(200, 'Announcement Attachment Successfully Attached', $announcement);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }



    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $announcement = Announcements::find($id);
            if (!$announcement) return $this->response(404, 'Announcement', 'Not Found!');
            $announcement->delete();

            DB::commit();
            return $this->response(200, 'Announcement has been successfully deleted!', $announcement);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
