<?php

namespace App\Repositories\Hrim\CompanyManagement\Bulletin;

use App\Interfaces\Hrim\CompanyManagement\Bulletin\MemoInterface;
use App\Models\Hrim\Memos;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class MemoRepository implements MemoInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $attachment = Memos::with('postedBy', 'attachments')->when($request['sort_field'], function ($query) use($request) {
                $query->orderBy($request['sort_field'], $request['sort_type']);
            })
                ->paginate($request['paginate']);

            DB::commit();
            return $this->response(200, 'Memos List', $attachment);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function show($id)
    {
        $memo = Memos::with('postedBy', 'attachments')->find($id);

        if (!$memo) return $this->response(404, 'Memo', 'Not Found!');

        return $this->response(200, 'Memo', $memo);
    }

    public function create($validated)
    {
        DB::beginTransaction();
        try {
            $memo = Memos::create([
                'memorandum_no' => $validated['memorandum_no'],
                'attention_to' => 1,
                'date_posted' => date('Y-m-d'),
                'title' => strtoupper($validated['title']),
                'details' => $validated['details'],
                'posted_by' => Auth::user()->id,
            ]);

            $response = $this->attachments($memo, $validated['attachments']);

            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            DB::commit();
            return $this->response(200, 'Memo has been successfully published!', $memo);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update(Memos $memo, $validated)
    {
        DB::beginTransaction();
        try {

            $memo->update([
                'title' => $validated['title'],
                'details' => $validated['details']
            ]);

            $response = $this->attachments($memo, $validated['attachments']);

            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            DB::commit();
            return $this->response(200, 'Memo has been successfully updated!', $memo);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function attachments($memo, $attachments)
    {

        DB::beginTransaction();
        try {
            // $month = strtolower(date('F', strtotime($memo->created_at)));
            // $year = date('Y', strtotime($memo->created_at));
            $date = date('Y/m/d', strtotime($memo->created_at));

            foreach ($attachments as $attachment) {

                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $memo->attachments()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(" ", "_", 'bulletin/' . $date . '/' . 'memo/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        Storage::disk('hrim_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } else if (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('hrim_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }
                    $memo->attachments()->updateOrCreate(
                        [
                            'id' => $attachment['id']
                        ],
                        [
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                        ]
                    );
                }
            }

            DB::commit();
            return $this->response(200, 'Memo Attachment Successfully Attached', $memo);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }



    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $memo = Memos::find($id);
            if (!$memo) return $this->response(404, 'Memo', 'Not Found!');
            $memo->delete();

            DB::commit();
            return $this->response(200, 'Memo has been successfully deleted!', $memo);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
