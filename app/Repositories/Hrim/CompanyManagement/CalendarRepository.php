<?php

namespace App\Repositories\Hrim\CompanyManagement;

use App\Interfaces\Hrim\CompanyManagement\CalendarInterface;
use App\Models\Hrim\Calendar;
use App\Models\Hrim\HolidayManagement;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CalendarRepository implements CalendarInterface
{
    use ResponseTrait;

    // public function rules()
    // {

    // }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'branch' => 'required',
                'event_type' => 'required',
                'date_and_time' => 'required',
                'event_title' => 'required',
                'description' => 'required',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $users = User::whereHas('userDetails', function ($query) use ($validated) {
                $query->where('branch_id', $validated['branch']);
            })
                ->where('status_id', 1)
                ->get();

            $calendar = Calendar::create([
                'branch_id' => $validated['branch'],
                'event_type_id' => $validated['event_type'],
                'event_date_time' => $validated['date_and_time'],
                'title' => $validated['event_title'],
                'description' => $validated['description'],
                'created_by' => Auth::user()->id,
            ]);

            foreach ($users as $user) {
                $user->calendars()->updateOrCreate([
                    'calendar_id' => $calendar->id
                ]);
            }

            DB::commit();
            return $this->response(200, 'Event has been successfully created!', $calendar);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id, $request = [])
    {
        DB::beginTransaction();
        try {
            $calendar = Calendar::with(['users' => function ($query) use ($request) {
                $query->when($request['with_trashed'] ?? false, function ($query) {
                    $query->withTrashed();
                });
            }])->when($request['with_trashed'] ?? false, function ($query) {
                $query->withTrashed();
            })
                ->find($id);
            if (!$calendar) return $this->response(404, 'Calendar Event', 'Not Found!');

            DB::commit();
            return $this->response(200, 'Calendar Event', $calendar);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $calendar = Calendar::find($id);
            if (!$calendar) return $this->response(404, 'Calendar Event', 'Not Found!');

            $response = $this->validation($request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);
            $validated = $response['result'];

            $users = User::whereHas('userDetails', function ($query) use ($validated) {
                $query->where('branch_id', $validated['branch']);
            })
                ->where('status_id', 1)
                ->get();

            $calendar->update([
                'branch_id' => $validated['branch'],
                'event_type_id' => $validated['event_type'],
                'event_date_time' => $validated['date_and_time'],
                'title' => $validated['event_title'],
                'description' => $validated['description'],
                'created_by' => Auth::user()->id,
            ]);

            foreach ($users as $user) {
                $user->calendars()->updateOrCreate([
                    'calendar_id' => $calendar->id
                ]);
            }

            DB::commit();
            return $this->response(200, 'Event has been successfully updated!', $calendar);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);

            $calendar = $response['result'];
            $calendar->delete();

            foreach ($calendar->users as $user) {
                $user->delete();
            }

            DB::commit();
            return $this->response(200, 'Event has been successfully deleted!', $calendar);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function restore($id)
    {
        DB::beginTransaction();
        try {
            $request = [
                'with_trashed' => true
            ];

            $response = $this->show($id, $request);
            if ($response['code'] != 200) return $this->response($response['code'], $response['message'], $response['result']);

            $calendar = $response['result'];
            $calendar->restore();

            foreach ($calendar->users as $user) {
                $user->restore();
            }

            DB::commit();
            return $this->response(200, 'Event has been successfully restored!', $calendar);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getCalendarEvents($request)
    {
        DB::beginTransaction();
        try {
            $calendars = Calendar::with(['branch', 'eventType', 'createdBy'])
                ->when($request['with_trashed'] ?? false, function ($query) {
                    $query->withTrashed();
                })
                ->when($request['type'] == 'date', function ($query) use ($request) {
                    $query->whereDate('event_date_time', date('Y-m-d', strtotime($request['date'])));
                })
                ->when($request['type'] == 'month', function ($query) use ($request) {
                    $query->whereMonth('event_date_time', date('m', strtotime($request['date'])))
                        ->whereYear('event_date_time', date('Y', strtotime($request['date'])));
                })
                ->orderByRaw('deleted_at IS NULL DESC, event_date_time DESC')
                ->when($request['sort_field'], function ($query) use ($request) {
                    $query->orderBy($request['sort_field'], $request['sort_type']);
                })
                ->get();

            $birth_months = UserDetails::whereMonth('birth_date', date('m', strtotime($request['date'])))
                // ->orWhere(function ($query) use ($month) {
                //     $query->whereMonth('created_at', $month);
                //     //    ->whereDay('created_at', ‘>=’, $date->day);
                // })
                ->orderByRaw("birth_date,last_name", 'ASC')
                ->get();


            $holiday_in_month = HolidayManagement::whereMonth('date', date('m', strtotime($request['date'])))
                ->orderByRaw("date,name", 'ASC')
                ->get();

            DB::commit();
            return $this->response(200, 'Calendar Event Lists', compact('calendars', 'birth_months', 'holiday_in_month'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
