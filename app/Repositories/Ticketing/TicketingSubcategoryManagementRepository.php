<?php

namespace App\Repositories\Ticketing;

use App\Interfaces\Ticketing\TicketingSubcategoryManagementInterface;
use App\Models\Ticketing\TicketSubcategoryMgmt;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TicketingSubcategoryManagementRepository implements TicketingSubcategoryManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $subcategory_managements = TicketSubcategoryMgmt::with('division', 'category', 'subcategories')
                // ->when($request['name'] ?? false, function ($query) use ($request) {
                //     $query->where('name', $request['name']);
                // })
                //     ->when($request['division_id'] ?? false, function ($query) use ($request) {
                //         $query->where('division_id', $request['division_id']);
                //     })
                //     ->when(Auth::user()->level_id != 5, function ($query) {
                //         $query->where('division_id', Auth::user()->division_id);
                //     })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Subcategory List', compact('subcategory_managements'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make(
                $request,
                [
                    'category' => 'required',
                    'subcategory' => 'required',
                    'sla' => 'required',
                    'severity' => 'required',
                    'category' => 'required',
                    'division' => 'required',
                    'department' => 'required',
                    'taskholders.*.task_holder' => 'required',
                ],
                [
                    'taskholders.*.task_holder.required' => 'The Task Holder field is required.',
                ]
            );


            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->validation($request);

            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $ticketing_subcategory_management = TicketSubcategoryMgmt::create([
                'name' => $validated['subcategory'],
                'division_id' => $validated['division'],
                'department_id' => $validated['department'],
                'category_id' => $validated['category'],
                'sla' => $validated['sla'],
                'severity' => $validated['severity'],
                'created_by' => Auth::user()->id,
            ]);

            foreach ($validated['taskholders'] as $i => $taskholder) {
                $ticketing_subcategory_management->subcategories()->updateOrCreate(
                    [
                        'user_id' => $taskholder['task_holder']
                    ],
                    [
                        'user_id' => $taskholder['task_holder'],
                        'category_id' => $validated['category'],
                        'subcategory_id' => $ticketing_subcategory_management->id,
                        'division_id' => $validated['division'],
                        'department_id' => $validated['department'],
                    ]
                );
            }

            DB::commit();

            return $this->response(200, 'Subcategory has been Successfully Created!', $ticketing_subcategory_management);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $ticketing_subcategory_management = $response['result'];


            $response = $this->validation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $ticketing_subcategory_management->update([
                'name' => $validated['subcategory'],
                'division_id' => $validated['division'],
                'department_id' => $validated['department'],
                'category_id' => $validated['category'],
                'sla' => $validated['sla'],
                'severity' => $validated['severity'],
                'created_by' => Auth::user()->id,
            ]);

            foreach ($validated['taskholders'] as $i => $taskholder) {
                $ticketing_subcategory_management->subcategories()->updateOrCreate(
                    [
                        'user_id' => $taskholder['task_holder']
                    ],
                    [
                        'user_id' => $taskholder['task_holder'],
                        'category_id' => $validated['category'],
                        'subcategory_id' => $ticketing_subcategory_management->id,
                        'division_id' => $validated['division'],
                        'department_id' => $validated['department'],
                    ]
                );
            }

            DB::commit();

            return $this->response(200, 'Ticket Subcategory has been successfully updated!', $ticketing_subcategory_management);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $ticketing_subcategory_management = TicketSubcategoryMgmt::findOrFail($id);
            if (!$ticketing_subcategory_management) {
                return $this->response(404, 'Ticket Subcategory', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Subcategory Management', $ticketing_subcategory_management);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
