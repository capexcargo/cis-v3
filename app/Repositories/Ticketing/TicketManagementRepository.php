<?php

namespace App\Repositories\Ticketing;

use App\Interfaces\Ticketing\TicketManagementInterface;
use App\Models\Ticketing\TaskHolder;
use App\Models\Ticketing\TicketCategoryManagement;
use App\Models\Ticketing\TicketManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;


class TicketManagementRepository implements TicketManagementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $tickets_management = TicketManagement::with('division', 'category', 'subcategory', 'taskholder', 'requestedBy')
                ->when($request['date_created'] ?? false, function ($query) use ($request) {
                    $query->whereDate('created_at', $request['date_created']);
                })
                ->when($request['category'] ?? false, function ($query) use ($request) {
                    $query->where('category_id', $request['category']);
                })
                ->when($request['task_holder'] ?? false, function ($query) use ($request) {
                    $query->where('task_holder', $request['task_holder']);
                })
                ->when(Auth::user()->level_id >= 2, function ($query) {
                    $query->where('division_id', Auth::user()->division_id);
                })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Ticket List', compact('tickets_management'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index_dashboard($request)
    {
        DB::beginTransaction();
        try {
            $total_tickets = TicketManagement::get();
            $open_tickets = TicketManagement::whereIn('final_status', [1, 2])->count();
            $closed_tickets = TicketManagement::where('final_status', 3)->get();

            $ticket_categories = TicketCategoryManagement::withCount([
                'tickets as tech_supp' => function ($query) use ($request) {
                    $query->where('category_id', 1);
                    $query->when($request['date_from'] ?? false, function ($query) use ($request) {
                        $query->whereDate('created_at', '>=', $request['date_from']);
                    })->when($request['date_to'] ?? false, function ($query) use ($request) {
                        $query->whereDate('created_at', '<=', $request['date_to']);
                    });
                },
                'tickets as help_desk' => function ($query) use ($request) {
                    $query->where('category_id', 2);
                    $query->when($request['date_from'] ?? false, function ($query) use ($request) {
                        $query->whereDate('created_at', '>=', $request['date_from']);
                    })->when($request['date_to'] ?? false, function ($query) use ($request) {
                        $query->whereDate('created_at', '<=', $request['date_to']);
                    });
                }
            ])->get();

            $tickets_sla_compliance = TicketManagement::when($request['date_from'] ?? false, function ($query) use ($request) {
                $query->whereDate('created_at', '>=', $request['date_from']);
            })->when($request['date_to'] ?? false, function ($query) use ($request) {
                $query->whereDate('created_at', '<=', $request['date_to']);
            })->get();


            $tech_supp = $ticket_categories[0]['tech_supp'] ?? 0;
            $help_desk = $ticket_categories[1]['help_desk'] ?? 0;

            $days_tickets = [];

            // if ($request['date_from'] == '' && $request['date_to'] == '') {
                $day1_tickets = date('M d', strtotime('-6 days'));
                $day2_tickets = date('M d', strtotime('-5 days'));
                $day3_tickets = date('M d', strtotime('-4 days'));
                $day4_tickets = date('M d', strtotime('-3 days'));
                $day5_tickets = date('M d', strtotime('-2 days'));
                $day6_tickets = date('M d', strtotime('-1 days'));
                $day7_tickets = date('M d');

                $days_tickets = [
                    'day1_tickets' => $day1_tickets,
                    'day2_tickets' => $day2_tickets,
                    'day3_tickets' => $day3_tickets,
                    'day4_tickets' => $day4_tickets,
                    'day5_tickets' => $day5_tickets,
                    'day6_tickets' => $day6_tickets,
                    'day7_tickets' => $day7_tickets,
                ];
            // }

            $weekly_closed1_tickets = TicketManagement::whereDate('final_status_date', date('Y-m-d', strtotime('-6 days')))->where('final_status', 3)->get();
            $weekly_closed2_tickets = TicketManagement::whereDate('final_status_date', date('Y-m-d', strtotime('-5 days')))->where('final_status', 3)->get();
            $weekly_closed3_tickets = TicketManagement::whereDate('final_status_date', date('Y-m-d', strtotime('-4 days')))->where('final_status', 3)->get();
            $weekly_closed4_tickets = TicketManagement::whereDate('final_status_date', date('Y-m-d', strtotime('-3 days')))->where('final_status', 3)->get();
            $weekly_closed5_tickets = TicketManagement::whereDate('final_status_date', date('Y-m-d', strtotime('-2 days')))->where('final_status', 3)->get();
            $weekly_closed6_tickets = TicketManagement::whereDate('final_status_date', date('Y-m-d', strtotime('-1 days')))->where('final_status', 3)->get();
            $weekly_closed7_tickets = TicketManagement::whereDate('final_status_date', date('Y-m-d'))->where('final_status', 3)->get();

            $weeklys_closed_tickets = [
                'weekly_closed1_tickets' => $weekly_closed1_tickets,
                'weekly_closed2_tickets' => $weekly_closed2_tickets,
                'weekly_closed3_tickets' => $weekly_closed3_tickets,
                'weekly_closed4_tickets' => $weekly_closed4_tickets,
                'weekly_closed5_tickets' => $weekly_closed5_tickets,
                'weekly_closed6_tickets' => $weekly_closed6_tickets,
                'weekly_closed7_tickets' => $weekly_closed7_tickets,
            ];

            $weekly_total1_tickets = TicketManagement::whereDate('created_at', date('Y-m-d', strtotime('-6 days')))->get();
            $weekly_total2_tickets = TicketManagement::whereDate('created_at', date('Y-m-d', strtotime('-5 days')))->get();
            $weekly_total3_tickets = TicketManagement::whereDate('created_at', date('Y-m-d', strtotime('-4 days')))->get();
            $weekly_total4_tickets = TicketManagement::whereDate('created_at', date('Y-m-d', strtotime('-3 days')))->get();
            $weekly_total5_tickets = TicketManagement::whereDate('created_at', date('Y-m-d', strtotime('-2 days')))->get();
            $weekly_total6_tickets = TicketManagement::whereDate('created_at', date('Y-m-d', strtotime('-1 days')))->get();
            $weekly_total7_tickets = TicketManagement::whereDate('created_at', date('Y-m-d'))->get();

            $weeklys_total_tickets = [
                'weekly_total1_tickets' => $weekly_total1_tickets,
                'weekly_total2_tickets' => $weekly_total2_tickets,
                'weekly_total3_tickets' => $weekly_total3_tickets,
                'weekly_total4_tickets' => $weekly_total4_tickets,
                'weekly_total5_tickets' => $weekly_total5_tickets,
                'weekly_total6_tickets' => $weekly_total6_tickets,
                'weekly_total7_tickets' => $weekly_total7_tickets,
            ];

            $overdue_tickets = TicketManagement::when(Auth::user()->level_id != 5, function ($query) {
                $query->where('division_id', Auth::user()->division_id);
            })->get();

            $count_overdue = [];
            $within_sla = [];
            $beyond_sla = [];

            foreach ($overdue_tickets as $i => $ticket) {
                if ($ticket['actual_end_date'] != null) {
                    if (date_diff(date_create($ticket['target_end_date']), date_create($ticket['actual_end_date']))->format('%R%a') > 0) {
                        $count_overdue[$i] = date_diff(date_create($ticket['target_end_date']), date_create($ticket['actual_end_date']))->format('%a Day/s');
                    }
                } else {
                    if ((date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%R%a') < 0 ? 0 : date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%a')) > 0) {
                        $count_overdue[$i] = (date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%R%a') < 0 ? 0 : date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%a Day/s'));
                    }
                }
            }

            foreach ($tickets_sla_compliance as $i => $ticket) {
                if ($ticket['actual_end_date'] != null) {
                    if ((date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%R%a') < 0 ? 0 : date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%a')) <= $ticket->subcategory->sla) {
                        $within_sla[$i] = $ticket;
                    }
                    if ((date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%R%a') < 0 ? 0 : date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%a')) >= $ticket->subcategory->sla) {
                        $beyond_sla[$i] = $ticket;
                    }
                } else {
                    if ((date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%R%a') < 0 ? 0 : date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%a')) <= $ticket->subcategory->sla) {
                        $within_sla[$i] = $ticket;
                    }
                    if ((date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%R%a') < 0 ? 0 : date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%a')) >= $ticket->subcategory->sla) {
                        $beyond_sla[$i] = $ticket;
                    }
                }
            }

            DB::commit();

            return $this->response(200, 'Ticket List', compact('total_tickets', 'open_tickets', 'closed_tickets', 'ticket_categories', 'tech_supp', 'help_desk', 'days_tickets', 'weeklys_closed_tickets', 'weeklys_total_tickets', 'count_overdue', 'within_sla', 'beyond_sla'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index_my_tickets($request)
    {
        DB::beginTransaction();
        try {
            $tickets_management = TicketManagement::with('division', 'category', 'subcategory', 'taskholder', 'requestedBy')
                ->when($request['ticket_ref_no'] ?? false, function ($query) use ($request) {
                    $query->where('ticket_reference_no', 'like', '%' . $request['ticket_ref_no'] . '%');
                })
                ->when($request['subject'] ?? false, function ($query) use ($request) {
                    $query->where('subject', 'like', '%' . $request['subject'] . '%');
                })
                ->when($request['date_created'] ?? false, function ($query) use ($request) {
                    $query->whereDate('created_at', $request['date_created']);
                })
                ->when($request['date_closed'] ?? false, function ($query) use ($request) {
                    $query->whereDate('final_status_date', $request['date_closed']);
                })
                ->when($request['category'] ?? false, function ($query) use ($request) {
                    $query->where('category_id', $request['category']);
                })
                ->when($request['ticket_status'] ?? false, function ($query) use ($request) {
                    $query->where('final_status', $request['ticket_status']);
                })
                // ->when(Auth::user()->level_id != 5, function ($query) {
                //     $query->where('requested_by', Auth::user()->id);
                // })
                ->where('requested_by', Auth::user()->id)
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Ticket List', compact('tickets_management'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index_my_tasks($request)
    {
        DB::beginTransaction();
        try {
            $tickets_management = TicketManagement::with('division', 'category', 'subcategory', 'taskholder', 'requestedBy')
                ->when($request['ticket_ref_no'] ?? false, function ($query) use ($request) {
                    $query->where('ticket_reference_no', 'like', '%' . $request['ticket_ref_no'] . '%');
                })
                ->when($request['subject'] ?? false, function ($query) use ($request) {
                    $query->where('subject', 'like', '%' . $request['subject'] . '%');
                })
                ->when($request['date_created'] ?? false, function ($query) use ($request) {
                    $query->whereDate('created_at', $request['date_created']);
                })
                ->when($request['date_closed'] ?? false, function ($query) use ($request) {
                    $query->whereDate('final_status_date', $request['date_closed']);
                })
                ->when($request['category'] ?? false, function ($query) use ($request) {
                    $query->where('category_id', $request['category']);
                })
                ->when($request['ticket_status'] ?? false, function ($query) use ($request) {
                    $query->where('final_status', $request['ticket_status']);
                })
                // ->when(Auth::user()->level_id != 5, function ($query) {
                //     $query->where('task_holder', Auth::user()->id);
                // })
                ->where('task_holder', Auth::user()->id)
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Ticket List', compact('tickets_management'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request, $division)
    {
        DB::beginTransaction();
        try {
            if ($division == 1) {
                $validator = Validator::make(
                    $request,
                    [
                        'ticket_ref_no' => 'required',
                        'division' => 'required',
                        'subject' => 'required',
                        'category' => 'required',
                        'subcategory' => 'required',
                        'message' => 'sometimes',
                        'concern_reference' => 'required',
                        'concern_ref_url' => 'required',
                        'attachments' => 'nullable',
                        'attachments.*.attachment' => 'nullable|' . config('filesystems.validation_all'),
                    ],
                    [
                        'concern_ref_url.required' => 'The Concern Reference URL field is required.',
                        'attachments.*.attachment.required' => 'The attachment field is required.',
                        'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
                    ]
                );
            } else {
                $validator = Validator::make(
                    $request,
                    [
                        'ticket_ref_no' => 'required',
                        'division' => 'required',
                        'subject' => 'required',
                        'category' => 'required',
                        'subcategory' => 'required',
                        'message' => 'sometimes',
                        'attachments' => 'nullable',
                        'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),
                    ],
                    [
                        'attachments.*.attachment.required' => 'The attachment field is required.',
                        'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
                    ]
                );
            }

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request, $division)
    {
        DB::beginTransaction();
        try {
            // $tholder = [];

            $response = $this->validation($request, $division);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $taskholders = TaskHolder::with('tickets')
                ->withCount(
                    [
                        'tickets as assigned' => function ($query) {
                            $query->where('task_holder_status', 1);
                        },
                        'tickets as pending' => function ($query) {
                            $query->where('task_holder_status', 2);
                        },
                        'tickets as closed' => function ($query) {
                            $query->where('task_holder_status', 3);
                        }
                    ]
                )
                ->where('category_id', $validated['category'])
                ->where('subcategory_id', $validated['subcategory'])
                ->where('division_id', $validated['division'])
                ->get();

            foreach ($taskholders as $i => $taskholder) {
                $task_holders[] = [
                    'id' => $taskholder->user_id,
                    'task_holders' => $taskholder->user->name,
                    'assigned' => $taskholder->assigned,
                    'pending' => $taskholder->pending,
                    'closed' => $taskholder->closed,
                ];

                foreach ($task_holders as $j => $task) {
                    $assigned[$i] = $task['assigned'];

                    $tholder[$j] = [
                        'assigned' => ($task['assigned'] + $task['pending']),
                        'id' => $task['id']
                    ];
                }
            }

            $ticket_management = TicketManagement::create([
                'ticket_reference_no' => $validated['ticket_ref_no'],
                'division_id' => $validated['division'],
                'subject' => $validated['subject'],
                'category_id' => $validated['category'],
                'subcategory_id' => $validated['subcategory'],
                'message' => $validated['message'],
                'task_holder' => min($tholder)['id'],
                'concern_reference' => $request['concern_reference'],
                'concern_reference_url' => $request['concern_ref_url'],
                'requested_by' => Auth::user()->id,
                'task_holder_status' => 1,
                'requester_status' => 1,
                'final_status' => 1,
            ]);

            $response = $this->attachments($ticket_management, $validated['attachments']);
            if ($response['code'] == 500) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }


            DB::commit();

            return $this->response(200, 'Ticket has been Successfully Created!', $ticket_management);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $ticket_management = TicketManagement::findOrFail($id);
            if (!$ticket_management) {
                return $this->response(404, 'Ticket', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Ticket Management', $ticket_management);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function assignTicketValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'task_holder' => 'required',
                'target_start_date' => 'required',
                'target_end_date' => 'required',
            ], [
                'task_holder.required' => 'The task holder field is required.',
                'target_start_date.required' => 'The target start date field is required.',
                'target_end_date.required' => 'The target end date field is required.',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateActualDateValidation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'actual_start_date' => 'required',
                'remarks' => 'required',
            ], [
                'actual_start_date.required' => 'The actual start date field is required.',
                'remarks.required' => 'The remarks field is required.',
            ]);

            if ($validator->fails()) return $this->response(400, 'Please Fill Required Field', $validator->errors());

            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $ticket_management = $response['result'];

            $response = $this->assignTicketValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            if ($ticket_management['assignment_date'] == null) {
                $assigned_date = date('Y-m-d');
            } else {
                $assigned_date = $ticket_management['assignment_date'];
            }

            $ticket_management->update([
                'task_holder' => $validated['task_holder'],
                'target_start_date' => $validated['target_start_date'],
                'target_end_date' => $validated['target_end_date'],
                'assignment_date' => $assigned_date,
            ]);

            DB::commit();

            return $this->response(200, 'Ticket has been successfully updated!', $ticket_management);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateActualDate($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $ticket_management = $response['result'];

            $response = $this->updateActualDateValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }

            $validated = $response['result'];

            $ticket_management->update([
                'actual_start_date' => $validated['actual_start_date'],
                'remarks' => $validated['remarks'],
            ]);

            DB::commit();

            return $this->response(200, 'Ticket has been successfully updated!', $ticket_management);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function close($id, $closer)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $ticket_management = $response['result'];

            if ($closer == "task_holder" && $ticket_management['requester_status'] == 3) {
                $ticket_management->update([
                    'task_holder_status' => 3,
                    'final_status' => 3,
                    'actual_end_date' => date("Y-m-d"),
                    'final_status_date' => date("Y-m-d"),
                    'task_holder_status_date' => date("Y-m-d")
                ]);
            } elseif ($closer == "task_holder" && ($ticket_management['requester_status'] == 1 || $ticket_management['requester_status'] == 2)) {
                $ticket_management->update([
                    'task_holder_status' => 3,
                    'final_status' => 2,
                    'task_holder_status_date' => date("Y-m-d")
                ]);
            } elseif ($closer == "requester" && $ticket_management['task_holder_status'] == 3) {
                $ticket_management->update([
                    'requester_status' => 3,
                    'final_status' => 3,
                    'actual_end_date' => date("Y-m-d"),
                    'final_status_date' => date("Y-m-d"),
                    'requester_status_date' => date("Y-m-d")
                ]);
            } elseif ($closer == "requester" && ($ticket_management['task_holder_status'] == 1 || $ticket_management['task_holder_status'] == 2)) {
                $ticket_management->update([
                    'requester_status' => 3,
                    'final_status' => 2,
                    'requester_status_date' => date("Y-m-d")
                ]);
            }


            DB::commit();

            return $this->response(200, 'Ticket has been successfully closed!', $ticket_management);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function attachments($ticket_management, $attachments)
    {
        DB::beginTransaction();
        try {
            $date = date('Y/m/d', strtotime($ticket_management->created_at));

            foreach ($attachments as $attachment) {

                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $ticket_management->attachments()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(" ", "_", 'ticketing/' . $date . '/' . 'ticket/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        Storage::disk('ticket_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } else if (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('ticket_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }
                    $ticket_management->attachments()->updateOrCreate(
                        [
                            'id' => $attachment['id']
                        ],
                        [
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                        ]
                    );
                }
            }

            DB::commit();
            return $this->response(200, 'Ticket Attachment has been successfully attached', $ticket_management);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
