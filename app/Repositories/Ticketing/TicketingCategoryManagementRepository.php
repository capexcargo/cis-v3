<?php

namespace App\Repositories\Ticketing;

use App\Interfaces\Ticketing\TicketingCategoryMangementInterface;
use App\Models\Ticketing\TicketCategoryManagement;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TicketingCategoryManagementRepository implements TicketingCategoryMangementInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $category_managements = TicketCategoryManagement::with('division')
            ->when($request['name'] ?? false, function ($query) use ($request) {
                $query->where('name', $request['name']);
            })
                ->when($request['division_id'] ?? false, function ($query) use ($request) {
                    $query->where('division_id', $request['division_id']);
                })
                ->when(Auth::user()->level_id != 5, function ($query) {
                    $query->where('division_id', Auth::user()->division_id);
                })
                ->paginate($request['paginate']);

            DB::commit();

            return $this->response(200, 'Category List', compact('category_managements'));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function validation($request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'name' => 'required',
                'division_id' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation($request, $id)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'name' => 'required',
                'division_id' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {

        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->validation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];


            $ticketCategoryManagement = TicketCategoryManagement::create([
                'name' => $validated['name'],
                'division_id' => $validated['division_id'],
                'created_by' => Auth::user()->id,
            ]);

            DB::commit();

            return $this->response(200, 'Category has been Successfully Created!', $ticketCategoryManagement);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->show($id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $ticketing_category_management = $response['result'];

            $response = $this->updateValidation($request, $id);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            $ticketing_category_management->update([
                'name' => $validated['name'],
                'division_id' => $validated['division_id'],
            ]);

            DB::commit();

            return $this->response(200, 'Ticket Category has been successfully updated!', $ticketing_category_management);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $ticketing_category_management = TicketCategoryManagement::findOrFail($id);
            if (!$ticketing_category_management) {
                return $this->response(404, 'Leadership Competencies', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, 'Category Management', $ticketing_category_management);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


}
