<?php

namespace App\Repositories\Globals;

use App\Interfaces\Globals\AccountManagementInterface;
use App\Interfaces\Globals\ForgotPasswordInterface;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class ForgotPasswordRepository implements ForgotPasswordInterface
{
    use ResponseTrait;

    public function show()
    {
        DB::beginTransaction();
        try {
            $user = User::findOrFail(Auth()->user()->id);
            if (!$user) {
                return $this->response(404, '', 'Not Found!');
            }

            DB::commit();

            return $this->response(200, '', $user);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
            // dd($user);

        }
    }

    public function updateValidation($request)
    {

        // dd($request);
        DB::beginTransaction();
        try {
            $validator = Validator::make($request, [
                'password' => 'required',
                'otp' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }

            DB::commit();

            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request)
    {
        DB::beginTransaction();
        try {


            $response = $this->show();
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $request_data = $response['result'];

            $response = $this->updateValidation($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];

            // return $this->response(200, '', $validated);
            if ($request_data['otp'] == $validated['otp']) {
                if ($request) {
                    $request_data->update([
                        'password' => Hash::make($validated['password']),
                        // 'password' => $validated['password'],
                    ]);
                }
            } else {
                return $this->response(200, 'OTP Failed', '');
            }


            DB::commit();

            return $this->response(200, 'Success!', $request_data);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
