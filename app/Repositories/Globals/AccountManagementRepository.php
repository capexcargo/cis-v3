<?php

namespace App\Repositories\Globals;

use App\Interfaces\Globals\AccountManagementInterface;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AccountManagementRepository implements AccountManagementInterface
{
    use ResponseTrait;

    public function show($id)
    {
        DB::beginTransaction();
        try {
            $user = UserDetails::with('division', 'user','position','employmentCategory','employmentStatus','gender','department','jobLevel','branch','agency')->find($id);

            DB::commit();
            return $this->response(200, 'Profile', $user);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($validated, $division)
    {
        DB::beginTransaction();
        try {
            $user = User::create([
                'role_id' => $validated['role'],
                'division_id' => $division,
                'level_id' => $validated['level'],
                'name' => $validated['first_name'] . ' ' . $validated['middle_name'] . ' ' . $validated['last_name'],
                'email' => $validated['email'],
                'password' => Hash::make($validated['password']),
            ]);

            $user->userDetails()->create([
                'employee_number' => $validated['employee_number'],
                'first_name' => $validated['first_name'],
                'middle_name' => $validated['middle_name'],
                'last_name' => $validated['last_name'],
                'company_mobile_number' => $validated['mobile_number'],
                'company_telephone_number' => $validated['telephone_number'],
                'suffix_id' => $validated['suffix'],
                'company_email' => $validated['email'],
                'age' => null,
                'birth_date' => null,
                'country' => null,
                'region' => null,
                'province' => null,
                'city' => null,
                'barangay' => null,
                'street' => null,
            ]);
            DB::commit();
            return $this->response(200, 'Account Successfully Created', $user);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($user, $validated, $validated_password, $division)
    {
        DB::beginTransaction();
        try {
            if ($validated_password) {
                $user->update([
                    'password' => Hash::make($validated_password['password']),
                ]);
            }

            $user->update([
                'role_id' => $validated['role'],
                'division_id' => $division,
                'level_id' => $validated['level'],
                'name' => $validated['first_name'] . ' ' . $validated['middle_name'] . ' ' . $validated['last_name'],
                'email' => $validated['email'],
            ]);

            $user->userDetails()->update([
                'employee_number' => $validated['employee_number'],
                'first_name' => $validated['first_name'],
                'middle_name' => $validated['middle_name'],
                'last_name' => $validated['last_name'],
                'company_mobile_number' => $validated['mobile_number'],
                'company_telephone_number' => $validated['telephone_number'],
                'suffix_id' => $validated['suffix'],
                'company_email' => $validated['email'],
                'age' => null,
                'birth_date' => null,
            ]);

            DB::commit();
            return $this->response(200, 'Account Successfully Updated', $user);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
