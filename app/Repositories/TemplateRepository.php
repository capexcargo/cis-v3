<?php

namespace App\Repositories;

use App\Interfaces\TemplateInterface;

class TemplateRepository implements TemplateInterface
{
    public function index($request)
    {
    }

    public function createValidation($request)
    {
    }

    public function create($request)
    {
    }

    public function show($id, $request = [])
    {
    }

    public function updateValidation($id, $request)
    {
    }

    public function update($id, $request)
    {
    }

    public function destroy($id)
    {
    }

    public function restore($id)
    {
    }
}
