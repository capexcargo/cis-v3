<?php

namespace App\Repositories\Fims\ServicePurchaseRequisition;

use App\Interfaces\Fims\ServicePurchaseRequisition\ServiceReqInterface;
use App\Models\FimsBudgetTransfer;
use App\Models\FimsManPowerReference;
use App\Models\FimsPurchaseRequestAttachment;
use App\Models\FimsPurchaseRequestDetails;
use App\Models\FimsPurchasing;
use App\Models\FimsPurchasingServiceReqDetails;
use App\Models\FimsPurpose;
use App\Models\FimsServiceCategory;
use App\Models\FimsServiceDescription;
use App\Models\OimsBranchReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

class ServiceReqRepository implements ServiceReqInterface
{
    use ResponseTrait;

    public function index2($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsPurchasing::get()->count();
            $getCI = FimsPurchasing::where('priority_level_id', 1)->get()->count();
            $getVI = FimsPurchasing::where('priority_level_id', 2)->get()->count();
            $getI = FimsPurchasing::where('priority_level_id', 3)->get()->count();
            $getLI = FimsPurchasing::where('priority_level_id', 4)->get()->count();

            $servReq = FimsPurchasing::with('ServReqDetails2', 'attachments','ServReqDetailsPriorityy')
                ->when($request['reference_no'] ?? false, function ($query) use ($request) {
                    $query->where('reference_no', $request['reference_no']);
                })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('priority_level_id', [1, 2, 3, 4]);
                    } else {
                        $query->where('priority_level_id', $request['status']);
                    }
                })
                ->where('purchasing_type_id', 1)
                ->paginate(10);
            DB::commit();



            $array = ["list" => $servReq, "count" => ['all' => $getAll, 'critically important' => $getCI, 'very important' => $getVI, 'important' => $getI, 'less important' => $getLI]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsPurchasing::get()->count();
            $getCI = FimsPurchasing::where('priority_level_id', 1)->get()->count();
            $getVI = FimsPurchasing::where('priority_level_id', 2)->get()->count();
            $getI = FimsPurchasing::where('priority_level_id', 3)->get()->count();
            $getLI = FimsPurchasing::where('priority_level_id', 4)->get()->count();

            $servReq = FimsPurchasing::with('details.serviceCategory', 'details.location', 'details.purpose', 'details.serviceDescription', 'attachments', 'user', 'user.division', 'user.userDetails.position', 'user.userDetails.branch', 'budget.coa', 'budget.opex', 'budget.budgetSource')
                ->when($request['reference_no'] ?? false, function ($query) use ($request) {
                    $query->where('reference_no', $request['reference_no']);
                })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('priority_level_id', [1, 2, 3, 4]);
                    } else {
                        $query->where('priority_level_id', $request['status']);
                    }
                })
                ->paginate(10);
                // dd($servReq);
            DB::commit();

            $array = ["list" => $servReq, "count" => ['all' => $getAll, 'critically important' => $getCI, 'very important' => $getVI, 'important' => $getI, 'less important' => $getLI]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getServiceCateg($request)
    {
        DB::beginTransaction();
        try {

            $ServCateg = FimsServiceCategory::with('ScsubAccs.budgetSource', 'ScsubAccs.opex', 'ScsubAccs.coa')->where('status', 1)->get();
            DB::commit();

            $array = ["list" => $ServCateg,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getServiceDesc($request)
    {
        DB::beginTransaction();
        try {
            if(isset($request['service_cat'])){
                $ServDesc = FimsServiceDescription::where('service_category_id', $request['service_cat'])->where('status', 1)->get();
            }else{
                $ServDesc = FimsServiceDescription::where('status', 1)->get();
            }
            
            DB::commit();


            $array = ["list" => $ServDesc,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getPrefWork($request)
    {
        DB::beginTransaction();
        try {

            $Manpow = FimsManPowerReference::where('status', 1)->get();
            DB::commit();

            $array = ["list" => $Manpow,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function createservice($request)
    {
        DB::beginTransaction();
        try {
        //  dd($request);

            $sreq = FimsPurchasing::create([
                'reference_no' => "SRn-". date("YmdHis") ."-". strtoupper(substr(uniqid(),9)),
                // 'reference_no' => 'FLS-SRN-101323-001',
                'priority_level_id' => 3,
                'expected_req_start_date' => $request['expected_start_date'],
                'service_category_id' => $request['service_category_id'],
                'expected_purchase_delivery_date' => NULL,
                'purchasing_type_id' => 1,
                'requisition_final_status' => 0,
                'canvassing_final_status' => 0,
                'request_final_status' => 0,
                'order_final_status' => 0,
                'rfp_final_status' => 0,
                'check_voucher_final_status' => 0,
                'requisition_status_update_date' => date("Y-m-d"),
                'canvassing_status_update_date' => date("Y-m-d"),
                'request_status_update_date' => date("Y-m-d"),
                'order_status_update_date' => date("Y-m-d"),
                'rfp_status_update_date' => date("Y-m-d"),
                'check_voucher_status_update_date' => date("Y-m-d"),
                'budget_id' => 21,
                'created_by' => Auth::user()->id,
            ]);

            // dd($sreq->id);

            $sreqdet = FimsPurchasingServiceReqDetails::create([
                // 'reference_no' => $request['reference_no'],
                'reference_no_id' => $sreq->id,
                'expected_start_date' => $request['expected_start_date'],
                'expected_end_date' => $request['expected_end_date'],
                'service_category_id' => $request['service_category_id'],
                'service_description_id' => $request['service_description_id'],
                'number_of_workers' =>  $request['number_of_workers'],
                'man_hours' =>  0,
                'estimated_rate' => $request['estimated_rate'],
                'preferred_worker' => $request['preferred_worker'],
                'purpose' =>  $request['purpose'],
                'location_id' => $request['location_id'],
                'comments' =>  $request['comments'],
                'budget_id' => 21,
                'approver1_id' => null,
                'approver2_id' =>  null,
                'approver3_id' => null,
                'approver1_status' =>  null,
                'approver2_status' => null,
                'approver3_status' => null,
                'approver1_date' =>  null,
                'approver2_date' => null,
                'approver3_date' =>  null,
                'approver1_remarks' =>  null,
                'approver2_remarks' => null,
                'approver3_remarks' =>  null,
            ]);

        DB::commit();

        return $this->response(200, 'Service Requisition has been successfully added!', compact($sreq, $sreqdet));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getPurp($request)
    {
        DB::beginTransaction();
        try {

            $Purp = FimsPurpose::where('status', 1)->get();
            DB::commit();

            $array = ["list" => $Purp,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getLoc($request)
    {
        DB::beginTransaction();
        try {

            $Loc = OimsBranchReference::where('status', 1)->get();
            DB::commit();

            $array = ["list" => $Loc,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        // dd($request['attachments']);
        DB::beginTransaction();
        try {
            $req['service'] = json_decode($request['service']);
            $sreq = FimsPurchasing::create([
                'reference_no' => $req['service']->reference_no,
                // 'reference_no' => 'FLS-SRN-101323-001',
                'priority_level_id' => $req['service']->priority_level_id,
                'expected_req_start_date' => $req['service']->expected_req_start_date,
                'service_category_id' => $req['service']->service_category_id,
                'expected_purchase_delivery_date' => $req['service']->expected_purchase_delivery_date,
                'purchasing_type_id' => $req['service']->purchasing_type_id,
                'requisition_final_status' => 0,
                'canvassing_final_status' => 0,
                'request_final_status' => 0,
                'order_final_status' => 0,
                'rfp_final_status' => 0,
                'check_voucher_final_status' => 0,
                'requisition_status_update_date' => isset($req['service']->requisition_status_update_date) ? $req['service']->requisition_status_update_date : date('y-m-d h:i:s'),
                'canvassing_status_update_date' => isset($req['service']->canvassing_status_update_date) ? $req['service']->canvassing_status_update_date : date('y-m-d h:i:s'),
                'request_status_update_date' => isset($req['service']->request_status_update_date) ? $req['service']->request_status_update_date : date('y-m-d h:i:s'),
                'order_status_update_date' => isset($req['service']->order_status_update_date) ? $req['service']->order_status_update_date : date('y-m-d h:i:s'),
                'rfp_status_update_date' => isset($req['service']->rfp_status_update_date) ? $req['service']->rfp_status_update_date : date('y-m-d h:i:s'),
                'check_voucher_status_update_date' => isset($req['service']->check_voucher_status_update_date) ? $req['service']->check_voucher_status_update_date : date('y-m-d h:i:s'),
                'budget_id' => $req['service']->budget_id,
                'created_by' => Auth::user()->id,
            ]);

            // dd($sreq->id);

            $det['details'] = json_decode($request['details']);

            $sreqdet = FimsPurchasingServiceReqDetails::create([
                // 'reference_no' => $request['reference_no'],
                'reference_no_id' => $sreq->id,
                'expected_start_date' => $det['details']->expected_start_date,
                'expected_end_date' => $det['details']->expected_end_date,
                'service_category_id' => $det['details']->service_category_id,
                'service_description_id' => $det['details']->service_description_id,
                'number_of_workers' => $det['details']->number_of_workers,
                'man_hours' => $det['details']->man_hours,
                'estimated_rate' => $det['details']->estimated_rate,
                'preferred_worker' => isset($det['details']->preferred_worker) ? $det['details']->preferred_worker : null,
                'purpose' => isset($det['details']->purpose) ? $det['details']->purpose : null,
                'location_id' => $det['details']->location_id,
                'comments' => isset($det['details']->comments) ? $det['details']->comments : null,
                'budget_id' => $det['details']->budget_id,
                'approver1_id' => isset($det['details']->approver1_id) ? $det['details']->approver1_id : null,
                'approver2_id' => isset($det['details']->approver2_id) ? $det['details']->approver2_id : null,
                'approver3_id' => isset($det['details']->approver3_id) ? $det['details']->approver3_id : null,
                'approver1_status' => isset($det['details']->approver1_status) ? $det['details']->approver1_status : null,
                'approver2_status' => isset($det['details']->approver2_status) ? $det['details']->approver2_status : null,
                'approver3_status' => isset($det['details']->approver3_status) ? $det['details']->approver3_status : null,
                'approver1_date' => isset($det['details']->approver1_date) ? $det['details']->approver1_date : null,
                'approver2_date' => isset($det['details']->approver2_date) ? $det['details']->approver2_date : null,
                'approver3_date' => isset($det['details']->approver3_date) ? $det['details']->approver3_date : null,
                'approver1_remarks' => isset($det['details']->approver1_remarks) ? $det['details']->approver1_remarks : null,
                'approver2_remarks' => isset($det['details']->approver2_remarks) ? $det['details']->approver2_remarks : null,
                'approver3_remarks' => isset($det['details']->approver3_remarks) ? $det['details']->approver3_remarks : null,
            ]);

            // $attachments123 = [];

            // $attachments123 = $request['attachments'];

            // dd($request['attachments']);

            // $request->file('attachments')->isValid();


            $attachments = [];
            if (array_key_exists('attachments', $request)) {
                foreach ($request['attachments'] as $attachment) {
                    $attachments[] = [
                        'id' => null,
                        'attachment' => $attachment,
                        'path' => null,
                        'name' => null,
                        'extension' => null,
                        'is_deleted' => false,
                    ];
                }
            }
            // dd($attachments);

            $response = $this->attachments($sreq, $sreqdet, $attachments);



            DB::commit();

            return $this->response(200, 'Service Requisition has been successfully added!', compact($sreq, $sreqdet));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function attachments($sreq, $sreqdet, $attachments)
    {
        DB::beginTransaction();
        try {
            $date = date('Y/m/d', strtotime($sreq->created_at));

            foreach ($attachments as $attachment) {
                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $sreq->attachments()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    $file_path = strtolower(str_replace(" ", "_", 'sales/' . $date . '/' . 'booking-mgmt/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                        Storage::disk('crm_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } else if (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('crm_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }
                    $sreq->attachments()->updateOrCreate(
                        [
                            'id' => $attachment['id']
                        ],
                        [
                            'reference_no_id' => $sreq->id,
                            'service_req_id' => $sreqdet->id,
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                        ]
                    );
                }
            }
            DB::commit();
            return $this->response(200, 'Attachment Successfully Attached', $sreq);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function reqstat($request, $id)
    {
        DB::beginTransaction();
        try {
            $reqfinal = FimsPurchasing::findOrFail($id);
            $reqfinalDet = FimsPurchasingServiceReqDetails::where('reference_no_id', $id)->first();
            if ($request['requisition_final_status'] == 1) {
                $reqfinal->update([
                    'requisition_final_status' => $request['requisition_final_status'],
                ]);
            } else {
                $reqfinal->update([
                    'requisition_final_status' => $request['requisition_final_status'],
                ]);
                $reqfinalDet->update([
                    'reason_id' => $request['reason_id'],
                ]);
            }

            DB::commit();
            $statusString = ($reqfinal['requisition_final_status'] == 1 ? "Approved" : ($reqfinal['requisition_final_status'] == 2 ? "Reject" : 'Reviewed'));
            if ($reqfinal['requisition_final_status'] == 1) {
                return $this->response(200, 'Service Requisition has been successfully ' . $statusString . '!', ['newStatus' => $reqfinal['requisition_final_status']]);
            } else {
                return $this->response(200, 'Service Requisition has been ' . $statusString . '!', ['newStatus' => $reqfinal['requisition_final_status'], $reqfinalDet['reason_id']]);
            }
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function approvestat($request, $id)
    {
        DB::beginTransaction();
        try {
            $approver = FimsPurchasing::findOrFail($id);
            $approver->update([
                'reason_id' => $request['reason_id'],
            ]);
            DB::commit();
            $statusString = $approver['requisition_final_status'] == 0 ? "Pending" : ($approver['requisition_final_status'] == 1 ? "Approved" : ($approver['requisition_final_status'] == 2 ? "Reject" : 'Reviewed'));

            return $this->response(200, 'Service Description has been ' . $statusString . ' successfully !', ['newStatus' => $approver['requisition_final_status']]);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update2($request, $id)
    {
        DB::beginTransaction();
        try {
      
        $servReq = FimsPurchasing::with('ServReqDetails2', 'attachments')->findOrFail($id);
        $servReq->ServReqDetails2->update([
            'expected_start_date' => $request['expected_start_date'],
            'expected_end_date' => $request['expected_end_date'],
            'service_category_id' => $request['service_category_id'],
            'service_description_id' => $request['service_description_id'],
            'number_of_workers' => $request['number_of_workers'],
            'estimated_rate' => $request['estimated_rate'],
            'preferred_worker' => $request['preferred_worker'],
            'purpose' => $request['purpose'],
            'location_id' => $request['location_id'],
            'comments' => $request['comments']
        ]);

        $servReq->update([
            'expected_req_start_date' => $request['expected_start_date'],
            'service_category_id' => $request['service_category_id']
        ]);


            DB::commit();

            return $this->response(200, 'Service Request has been successfully updated!', compact($servReq));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
