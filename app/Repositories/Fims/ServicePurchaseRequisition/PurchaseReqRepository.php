<?php

namespace App\Repositories\Fims\ServicePurchaseRequisition;

use App\Interfaces\Fims\ServicePurchaseRequisition\PurchaseReqInterface;
use App\Models\Accounting\Supplier;
use App\Models\BranchReference;
use App\Models\FimsItemCategory;
use App\Models\FimsItemDescription;
use App\Models\FimsPurchaseRequestDetails;
use App\Models\FimsPurchasing;
use App\Models\FimsPurpose;
use App\Models\FimsSubAccounts;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PurchaseReqRepository implements PurchaseReqInterface
{
    use ResponseTrait;


    public function create($request)
    {
        DB::beginTransaction();
        try {
            $req['purchase'] = json_decode($request['purchase']);
            $det['details'] = json_decode($request['details']);

            $getbudgetsss = 0;
            $getbudget = FimsItemCategory::where('id', $det['details'][0]->item_category_id)->first();
            $getsub = $getbudget['sub_accounts_id'];

            $getbudgetsss = FimsSubAccounts::where('id', $getsub)->first();
            $preq = FimsPurchasing::create([
                'reference_no' => $req['purchase']->reference_no,
                'priority_level_id' => 3,
                'expected_req_start_date' => null,
                'service_category_id' => null,
                'expected_purchase_delivery_date' => $req['purchase']->expected_purchase_delivery_date,
                'purchasing_type_id' => 2,
                'requisition_final_status' => 0,
                'canvassing_final_status' => 0,
                'request_final_status' => 0,
                'order_final_status' => 0,
                'rfp_final_status' => 0,
                'check_voucher_final_status' => 0,
                'requisition_status_update_date' => isset($req['purchase']->requisition_status_update_date) ? $req['purchase']->requisition_status_update_date : date('y-m-d h:i:s'),
                'canvassing_status_update_date' => isset($req['purchase']->requisition_status_update_date) ? $req['purchase']->requisition_status_update_date : date('y-m-d h:i:s'),
                'request_status_update_date' => isset($req['purchase']->requisition_status_update_date) ? $req['purchase']->requisition_status_update_date : date('y-m-d h:i:s'),
                'order_status_update_date' => isset($req['purchase']->requisition_status_update_date) ? $req['purchase']->requisition_status_update_date : date('y-m-d h:i:s'),
                'rfp_status_update_date' => isset($req['purchase']->requisition_status_update_date) ? $req['purchase']->requisition_status_update_date : date('y-m-d h:i:s'),
                'check_voucher_status_update_date' => isset($req['purchase']->requisition_status_update_date) ? $req['purchase']->requisition_status_update_date : date('y-m-d h:i:s'),
                'budget_id' => $getbudgetsss['id'],
                'created_by' => Auth::user()->id,
            ]);

            foreach ($det['details'] as $i => $details) {
                $getbudget = FimsItemCategory::where('id', $det['details'][$i]->item_category_id)->first();
                $getsub = $getbudget['sub_accounts_id'];
                $getbudget = FimsSubAccounts::where('id', $getsub)->first();

                $preqdet = FimsPurchaseRequestDetails::create([
                    'reference_no_id' => $preq->id,
                    'delivery_date' => $req['purchase']->expected_purchase_delivery_date,
                    'is_sample_product' => $det['details'][$i]->is_sample_product,
                    'item_category_id' => $det['details'][$i]->item_category_id,
                    'item_description_id' => $det['details'][$i]->item_description_id,
                    'qty' => $det['details'][$i]->qty,
                    'unit' => $det['details'][$i]->unit,
                    'estimated_rate' => $det['details'][$i]->estimated_rate,
                    'estimated_total' => $det['details'][$i]->estimated_total,
                    'preferred_supplier_id' => $det['details'][$i]->preferred_supplier_id,
                    'purpose' => $det['details'][$i]->purpose,
                    'beneficiary_branch_id' => $det['details'][$i]->beneficiary_branch_id,
                    'series_start_no' => $det['details'][$i]->series_start_no,
                    'series_end_no' => $det['details'][$i]->series_end_no,
                    'remarks' => $det['details'][$i]->remarks,
                    'budget_id' => $getbudget['id'],
                    'approver1_id' => $det['details'][$i]->approver1_id,
                    'approver2_id' => $det['details'][$i]->approver2_id,
                    'approver3_id' => $det['details'][$i]->approver3_id,
                    'approver1_status' => $det['details'][$i]->approver1_status,
                    'approver2_status' => $det['details'][$i]->approver2_status,
                    'approver3_status' => $det['details'][$i]->approver3_status,
                    'approver1_date' => $det['details'][$i]->approver1_date,
                    'approver2_date' => $det['details'][$i]->approver2_date,
                    'approver3_date' => $det['details'][$i]->approver3_date,
                    'approver1_remarks' => $det['details'][$i]->approver1_remarks,
                    'approver2_remarks' => $det['details'][$i]->approver2_remarks,
                    'approver3_remarks' => $det['details'][$i]->approver3_remarks,
                ]);

                $attachments = [];
                if (array_key_exists('attachments', $request)) {
                    foreach ($request['attachments'] as $attachment) {
                        $attachments[] = [
                            'id' => null,
                            'attachment' => $attachment,
                            'path' => null,
                            'name' => null,
                            'extension' => null,
                            'is_deleted' => false,
                        ];
                    }
                }
            }
            $response = $this->attachments($preq, $preqdet, $attachments);
            // dd($response);


            DB::commit();

            return $this->response(200, 'Purchase Requisition has been successfully added!', compact($preq, $preqdet));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function attachments($preq, $preqdet, $attachments)
    {
        DB::beginTransaction();
        try {
            // dd($attachments);
            // $request->file('attachments');

            $date = date('Y/m/d', strtotime($preq->created_at));

            foreach ($attachments as $attachment) {
                if ($attachment['id'] && $attachment['is_deleted']) {
                    $attachment_model = $preq->Purchaseattachments()->find($attachment['id']);
                    $attachment_model->delete();
                } else if ($attachment['attachment']) {
                    // dd('sad');
                    $file_path = strtolower(str_replace(" ", "_", 'sales/' . $date . '/' . 'booking-mgmt/'));
                    $file_name = date('mdYHis') . uniqid() . '.' . $attachment['attachment']->extension();

                    if (in_array($attachment['attachment']->extension(), config('filesystems.image_type'))) {
                        $image_resize = Image::make($attachment['attachment']->getRealPath())->resize(1024, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                        Storage::disk('crm_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                    } else if (in_array($attachment['attachment']->extension(), config('filesystems.file_type'))) {
                        Storage::disk('crm_gcs')->putFileAs($file_path, $attachment['attachment'], $file_name);
                    }
                    $preq->Purchaseattachments()->updateOrCreate(
                        [
                            'id' => $attachment['id']
                        ],
                        [
                            'reference_no_id' => $preq->id,
                            'purchase_req_id' => $preqdet->id,
                            'path' => $file_path,
                            'name' => $file_name,
                            'extension' => $attachment['attachment']->extension(),
                        ]
                    );
                }
            }
            DB::commit();
            return $this->response(200, 'Attachment Successfully Attached', $preq);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function reqstat($request, $id)
    {
        DB::beginTransaction();
        try {
            $reqfinal = FimsPurchasing::findOrFail($id);
            $reqfinalDet = FimsPurchaseRequestDetails::where('reference_no_id', $id)->get();
            // dd($reqfinalDet);
            if ($request['requisition_final_status'] == 1) {
                $reqfinal->update([
                    'requisition_final_status' => $request['requisition_final_status'],
                ]);
            } else {
                $reqfinal->update([
                    'requisition_final_status' => $request['requisition_final_status'],
                ]);
                foreach ($reqfinalDet as $i => $finalDet) {
                    $finalDet->update([
                        'reason_id' => $request['reason_id'],
                    ]);
                }
            }

            DB::commit();
            $statusString = ($reqfinal['requisition_final_status'] == 1 ? "Approved" : ($reqfinal['requisition_final_status'] == 2 ? "Reject" : 'Reviewed'));
            if ($reqfinal['requisition_final_status'] == 1) {
                return $this->response(200, 'Purchase Requisition has been successfully ' . $statusString . '!', ['newStatus' => $reqfinal['requisition_final_status']]);
            } else {
                return $this->response(200, 'Purchase Requisition has been ' . $statusString . '!', ['newStatus' => $reqfinal['requisition_final_status'], $finalDet['reason_id']]);
            }
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsPurchasing::get()->count();
            $getCI = FimsPurchasing::where('priority_level_id', 1)->get()->count();
            $getVI = FimsPurchasing::where('priority_level_id', 2)->get()->count();
            $getI = FimsPurchasing::where('priority_level_id', 3)->get()->count();
            $getLI = FimsPurchasing::where('priority_level_id', 4)->get()->count();

            $purchReq = FimsPurchasing::with('PurchReqDetails2', 'attachments', 'PurchReqDetailsPriorityy')
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('priority_level_id', [1, 2, 3, 4]);
                    } else {
                        $query->where('priority_level_id', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $purchReq, "count" => ['all' => $getAll, 'critically important' => $getCI, 'very important' => $getVI, 'important' => $getI, 'less important' => $getLI]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getItemCateg($request)
    {
        DB::beginTransaction();
        try {

            $ServCateg = FimsItemCategory::with('budgetSrc', 'opexCat', 'chartOfAccs', 'subAccs')->where('status', 1)->get();
            DB::commit();

            $array = ["list" => $ServCateg,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getItemDesc($request)
    {
        DB::beginTransaction();
        try {
            if(isset($request['item_cat'])){
                $ItemDesc = FimsItemDescription::where('item_category_id', $request['item_cat'])->where('status', 1)->get();
            }else{
                $ItemDesc = FimsItemDescription::where('status', 1)->get();
            }
            
            DB::commit();


            $array = ["list" => $ItemDesc,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getPurp($request)
    {
        DB::beginTransaction();
        try {

            $Purp = FimsPurpose::where('status', 1)->get();
            DB::commit();

            $array = ["list" => $Purp,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getBenBranch($request)
    {
        DB::beginTransaction();
        try {

            $Branch = BranchReference::get();
            DB::commit();

            $array = ["list" => $Branch,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getPrefSup($request)
    {
        DB::beginTransaction();
        try {
            $PrefSup = Supplier::get();
            DB::commit();

            $array = ["list" => $PrefSup,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // ==============================================================================================================================================================

    public function index2($request, $search_request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsPurchasing::get()->count();
            $getCI = FimsPurchasing::where('priority_level_id', 1)->get()->count();
            $getVI = FimsPurchasing::where('priority_level_id', 2)->get()->count();
            $getI = FimsPurchasing::where('priority_level_id', 3)->get()->count();
            $getLI = FimsPurchasing::where('priority_level_id', 4)->get()->count();

            $purchReq = FimsPurchasing::with('PurchReqDetails2', 'attachments', 'PurchReqDetailsPriorityy')
                ->when($search_request['reference_no'] ?? false, function ($query) use ($search_request) {
                    $query->where('reference_no', $search_request['reference_no']);
                })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('priority_level_id', [1, 2, 3, 4]);
                    } else {
                        $query->where('priority_level_id', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();



            $array = ["list" => $purchReq, "count" => ['all' => $getAll, 'critically important' => $getCI, 'very important' => $getVI, 'important' => $getI, 'less important' => $getLI]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function createValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'del_date' => 'required',
            ];
            foreach ($request['purchase_forms'] as $i => $purchase_form) {
                $rules['purchase_forms.' . $i . '.id'] = 'sometimes';
                $rules['purchase_forms.' . $i . '.item_category_id'] = 'required';
                $rules['purchase_forms.' . $i . '.item_description_id'] = 'required';
                $rules['purchase_forms.' . $i . '.qty'] = 'required';
                $rules['purchase_forms.' . $i . '.unit'] = 'required';
                $rules['purchase_forms.' . $i . '.estimated_price'] = 'required';
                $rules['purchase_forms.' . $i . '.estimated_total'] = 'required';
                $rules['purchase_forms.' . $i . '.purpose'] = 'required';
                $rules['purchase_forms.' . $i . '.branch_id'] = 'required';
                $rules['purchase_forms.' . $i . '.sfrom'] = 'sometimes';
                $rules['purchase_forms.' . $i . '.sto'] = 'sometimes';
                $rules['purchase_forms.' . $i . '.pref_supp'] = 'required';
                $rules['purchase_forms.' . $i . '.rmks'] = 'required';
                $rules['purchase_forms.' . $i . '.samp_prod'] = 'sometimes';
            }

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'del_date.required' => 'Date is Required.',
                    'purchase_forms.*.item_category_id.required' => 'Item category is required.',
                    'purchase_forms.*.item_description_id.required' => 'Item description is required.',
                    'purchase_forms.*.qty.required' => 'Qty is required.',
                    'purchase_forms.*.unit.required' => 'Unit is required.',
                    'purchase_forms.*.estimated_price.required' => 'Price is required.',
                    'purchase_forms.*.estimated_total.required' => 'Total is required.',
                    'purchase_forms.*.purpose.required' => 'Purpose 1 is required.',
                    'purchase_forms.*.branch_id.required' => 'Branch 1 is required.',
                    'purchase_forms.*.sfrom.required' => 'From 1 is required.',
                    'purchase_forms.*.sto.required' => 'To 1 is required.',
                    'purchase_forms.*.pref_supp.required' => 'Supplier 1 is required.',
                    'purchase_forms.*.rmks.required' => 'Remarks 1 is required.',
                    'purchase_forms.*.samp_prod.required' => 'Sample is required.',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function create2($request)
    {
        DB::beginTransaction();
        try {


            $getbudgetsss = 0;
            foreach ($request['purchase_forms'] as $i => $purchase_forms) {
                $getbudget = FimsItemCategory::where('id', $request['purchase_forms'][0]['item_category_id'])->first();
                $getsub = $getbudget['sub_accounts_id'];
                $getbudgetsss = FimsSubAccounts::where('id', $getsub)->first();
            }

            $preq = FimsPurchasing::create([
                'reference_no' => "FLS-PRN-" . date("YmdHis") . "-" . strtoupper(substr(uniqid(), 9)),
                'priority_level_id' => 3,
                'expected_req_start_date' => null,
                'service_category_id' => null,
                'expected_purchase_delivery_date' => $request['del_date'],
                'purchasing_type_id' => 2,
                'requisition_final_status' => 0,
                'canvassing_final_status' => 0,
                'request_final_status' => 0,
                'order_final_status' => 0,
                'rfp_final_status' => 0,
                'check_voucher_final_status' => 0,
                'requisition_status_update_date' => date('y-m-d h:i:s'),
                'canvassing_status_update_date' => date('y-m-d h:i:s'),
                'request_status_update_date' => date('y-m-d h:i:s'),
                'order_status_update_date' => date('y-m-d h:i:s'),
                'rfp_status_update_date' => date('y-m-d h:i:s'),
                'check_voucher_status_update_date' => date('y-m-d h:i:s'),
                'budget_id' => $getbudgetsss['id'],
                'created_by' => Auth::user()->id,
            ]);
            foreach ($request['purchase_forms'] as $i => $purchase_form) {

                $getbudget = FimsItemCategory::where('id', $request['purchase_forms'][$i]['item_category_id'])->first();
                $getsub = $getbudget['sub_accounts_id'];
                $getbudget = FimsSubAccounts::where('id', $getsub)->first();

                $preqdet = FimsPurchaseRequestDetails::create([
                    'reference_no_id' => $preq->id,
                    'delivery_date' => $request['del_date'],
                    'is_sample_product' => $request['purchase_forms'][$i]['samp_prod'],
                    'item_category_id' => $request['purchase_forms'][$i]['item_category_id'],
                    'item_description_id' => $request['purchase_forms'][$i]['item_description_id'],
                    'qty' => $request['purchase_forms'][$i]['qty'],
                    'unit' => $request['purchase_forms'][$i]['unit'],
                    'estimated_rate' => $request['purchase_forms'][$i]['estimated_price'],
                    'estimated_total' => $request['purchase_forms'][$i]['estimated_total'],
                    'preferred_supplier_id' => $request['purchase_forms'][$i]['estimated_total'],
                    'purpose' => $request['purchase_forms'][$i]['purpose'],
                    'beneficiary_branch_id' => $request['purchase_forms'][$i]['branch_id'],
                    'series_start_no' => $request['purchase_forms'][$i]['sfrom'],
                    'series_end_no' => $request['purchase_forms'][$i]['sto'],
                    'remarks' => $request['purchase_forms'][$i]['rmks'],
                    'budget_id' => $getbudget['id'],
                    // 'approver1_id' => null,
                    // 'approver2_id' => null,
                    // 'approver3_id' => null,
                    // 'approver1_status' => null,
                    // 'approver2_status' => null,
                    // 'approver3_status' => null,
                    // 'approver1_date' => null,
                    // 'approver2_date' => null,
                    // 'approver3_date' => null,
                    // 'approver1_remarks' => null,
                    // 'approver2_remarks' => null,
                    // 'approver3_remarks' => null,
                ]);
                // dd($);

            }


            DB::commit();

            return $this->response(200, 'Purchase Requisition has been successfully added!', compact($preq, $preqdet));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function update2($request, $id)
    {
        DB::beginTransaction();
        try {

            $purreq = FimsPurchasing::with('PurchReqDetails2', 'attachments')->findOrFail($id);

            // dd($purreq);
            $getbudgetsss = 0;
            foreach ($request['purchase_forms'] as $i => $purchase_forms) {
                $getbudget = FimsItemCategory::where('id', $request['purchase_forms'][0]['item_category_id'])->first();
                $getsub = $getbudget['sub_accounts_id'];
                $getbudgetsss = FimsSubAccounts::where('id', $getsub)->first();
            }

            $purreq->update([
                'expected_purchase_delivery_date' => $request['del_date'],
                'budget_id' => $getbudgetsss['id'],
            ]);

            foreach ($request['purchase_forms'] as $a => $purchase_form) {
                $getbudget = FimsItemCategory::where('id', $request['purchase_forms'][$i]['item_category_id'])->first();
                $getsub = $getbudget['sub_accounts_id'];
                $getbudget = FimsSubAccounts::where('id', $getsub)->first();


                if ($request['purchase_forms'][$a]['id'] && $request['purchase_forms'][$a]['is_deleted']) {
                    $purreqdet = FimsPurchaseRequestDetails::find($request['purchase_forms'][$a]['id']); //query all in database
                    if ($purreqdet) {
                        $purreqdet->delete();
                    }
                } else {
                    $purreqdet = FimsPurchaseRequestDetails::updateOrCreate(
                        [
                            'id' => $request['purchase_forms'][$a]['id'],
                        ],
                        [

                            'reference_no_id' => $purreq->id,
                            'delivery_date' => $request['del_date'],
                            'is_sample_product' => $request['purchase_forms'][$a]['samp_prod'],
                            'item_category_id' => $request['purchase_forms'][$a]['item_category_id'],
                            'item_description_id' => $request['purchase_forms'][$a]['item_description_id'],
                            'qty' => $request['purchase_forms'][$a]['qty'],
                            'unit' => $request['purchase_forms'][$a]['unit'],
                            'estimated_rate' => $request['purchase_forms'][$a]['estimated_price'],
                            'estimated_total' => $request['purchase_forms'][$a]['estimated_total'],
                            'preferred_supplier_id' => $request['purchase_forms'][$a]['estimated_total'],
                            'purpose' => $request['purchase_forms'][$a]['purpose'],
                            'beneficiary_branch_id' => $request['purchase_forms'][$a]['branch_id'],
                            'series_start_no' => $request['purchase_forms'][$a]['sfrom'],
                            'series_end_no' => $request['purchase_forms'][$a]['sto'],
                            'remarks' => $request['purchase_forms'][$a]['rmks'],
                            'budget_id' => $getbudget['id'],
                        ]
                    );
                }
            }


            DB::commit();

            return $this->response(200, 'Purchase Request has been successfully updated!', compact($purreq, $purreqdet));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
