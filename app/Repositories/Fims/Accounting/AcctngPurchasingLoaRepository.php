<?php

namespace App\Repositories\Fims\Accounting;

use App\Interfaces\Fims\Accounting\AcctngPurchasingLoaInterface;
use App\Models\FimsPurchasingLoaMgmt;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class AcctngPurchasingLoaRepository implements AcctngPurchasingLoaInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            // $getAll = FimsPurchasingLoaMgmt::get()->count();
            // $getActive = FimsPurchasingLoaMgmt::where('status', 1)->get()->count();
            // $getInactive = FimsPurchasingLoaMgmt::where('status', 2)->get()->count();

            $ploa = FimsPurchasingLoaMgmt::with('PurTypeRef', 'BudgetRef', 'App1Ref', 'App2Ref', 'App3Ref')
                // ->when($request['name'] ?? false, function ($query) use ($request) {
                //         $query->where('name', $request['name']);
                //     })
                //     ->when($request['status'], function ($query) use ($request) {
                //         if ($request['status'] == false) {
                //             $query->whereIn('status', [1, 2]);
                //         } else {
                //             $query->where('status', $request['status']);
                //         }
                //     })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $ploa];
            return $this->response(200, 'List', ($array));
            // return $this->response(200, 'Purchasing Loa', compact('ploa'));

        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function create($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['data'] as $data) {
                $ploa = FimsPurchasingLoaMgmt::create([
                    'purchasing_type_id' => $data['purchasing_type_id'],
                    'division_id' => $data['division_id'],
                    'min_amount' => $data['min_amount'],
                    'max_amount' => $data['max_amount'],
                    'approval1_id' => $data['approval1_id'],
                    'approval2_id' => $data['approval2_id'],
                    'approval3_id' => $data['approval3_id'],
                ]);
            }

            DB::commit();

            return $this->response(200, 'Purchasing Loa has been successfully added!', ($ploa));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $ploa = FimsPurchasingLoaMgmt::findOrFail($id);
            $ploa->update([
                'purchasing_type_id' => $request['purchasing_type_id'],
                'division_id' => $request['division_id'],
                'min_amount' => $request['min_amount'],
                'max_amount' => $request['max_amount'],
                'approval1_id' => $request['approval1_id'],
                'approval2_id' => $request['approval2_id'],
                'approval3_id' => $request['approval3_id'],
            ]);
            DB::commit();
            return $this->response(200, 'Purchasing Loa has been successfully updated!', ($ploa));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $timeslot = FimsPurchasingLoaMgmt::find($id);
            if (!$timeslot) return $this->response(404, 'Purchasing Loa', 'Not Found!');
            $timeslot->delete();

            DB::commit();
            return $this->response(200, 'Purchasing Loa Successfully Deleted!', $timeslot);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
