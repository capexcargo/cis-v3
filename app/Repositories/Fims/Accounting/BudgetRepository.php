<?php

namespace App\Repositories\Fims\Accounting;

use App\Interfaces\Fims\Accounting\BudgetInterface;
use App\Models\FimsBudgetManagement;
use App\Models\FimsBudgetTransfer;
use App\Models\FimsPurchasingLoaMgmt;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BudgetRepository implements BudgetInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            // $getAll = FimsPurchasingLoaMgmt::get()->count();
            // $getActive = FimsPurchasingLoaMgmt::where('status', 1)->get()->count();
            // $getInactive = FimsPurchasingLoaMgmt::where('status', 2)->get()->count();

            $BudgetMgmt = FimsBudgetManagement::with('budgetTrans')->where('year', key_exists('year', $request) ? $request['year'] : date("Y"))
                // ->when($request['name'] ?? false, function ($query) use ($request) {
                //         $query->where('name', $request['name']);
                //     })
                //     ->when($request['status'], function ($query) use ($request) {
                //         if ($request['status'] == false) {
                //             $query->whereIn('status', [1, 2]);
                //         } else {
                //             $query->where('status', $request['status']);
                //         }
                //     })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $BudgetMgmt];
            return $this->response(200, 'List', ($array));
            // return $this->response(200, 'Purchasing Loa', compact('ploa'));

        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function create($request)
    {
        
        DB::beginTransaction();
        try {

                $btrans = FimsBudgetTransfer::create([
                    'source_id_from' => $request['source_id_from'],
                    'source_id_to' => $request['source_id_to'],           
                    'coa_id' => $request['coa_id'],
                    'coa_from' => $request['coa_from'],
                    'coa_to' => $request['coa_to'],
                    'month_from' => $request['month_from'],
                    'month_to' => $request['month_to'],
                    'year' => $request['year'],
                    'transfer_type' => $request['transfer_type'],
                    'transfer_amount' => $request['transfer_amount'],
                    'transferred_by' => Auth::user()->id,
                ]);

            DB::commit();

            return $this->response(200, 'Budget Transfer has been successfully added!', ($btrans));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getCoa($request)
    {
        DB::beginTransaction();
        try {
           
            $get_coa = FimsBudgetManagement::with('budgetTrans')->where('budget_coa_id', $request['coa_id'])->where('type', 3)->first();

            $array = ["list" => $get_coa];
            return $this->response(200, 'List', ($array));
            // return $this->response(200, 'Purchasing Loa', compact('ploa'));

        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // public function update($request, $id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $ploa = FimsPurchasingLoaMgmt::findOrFail($id);
    //         $ploa->update([
    //             'purchasing_type_id' => $request['purchasing_type_id'],
    //             'division_id' => $request['division_id'],
    //             'min_amount' => $request['min_amount'],
    //             'max_amount' => $request['max_amount'],
    //             'approval1_id' => $request['approval1_id'],
    //             'approval2_id' => $request['approval2_id'],
    //             'approval3_id' => $request['approval3_id'],
    //         ]);
    //         DB::commit();
    //         return $this->response(200, 'Purchasing Loa has been successfully updated!', ($ploa));
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    // public function destroy($id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $timeslot = FimsPurchasingLoaMgmt::find($id);
    //         if (!$timeslot) return $this->response(404, 'Purchasing Loa', 'Not Found!');
    //         $timeslot->delete();

    //         DB::commit();
    //         return $this->response(200, 'Purchasing Loa Successfully Deleted!', $timeslot);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return $this->response(500, 'Something Went Wrong', $e->getMessage());
    //     }
    // }
}
