<?php

namespace App\Repositories\Fims\DataManagement;

use App\Interfaces\Fims\DataManagement\ServiceDescriptionInterface;
use App\Models\FimsServiceCategory;
use App\Models\FimsServiceDescription;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ServiceDescriptionRepository implements ServiceDescriptionInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsServiceDescription::get()->count();
            $getActive = FimsServiceDescription::where('status', 1)->get()->count();
            $getInactive = FimsServiceDescription::where('status', 2)->get()->count();

            $ServDesc = FimsServiceDescription::with('ServCat')->when($request['name'] ?? false, function ($query) use ($request) {
                $query->where('description', $request['name']);
            })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $ServDesc, "count" => ['all' => $getAll, 'active' => $getActive, 'inactive' => $getInactive]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getServiceCateg($request)
    {
        DB::beginTransaction();
        try {

            $ServCateg = FimsServiceCategory::where('status', 1)->get();
            DB::commit();

            $array = ["list" => $ServCateg,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['data'] as $data) {
                $ServDesc = FimsServiceDescription::create([
                    'service_category_id' => $data['service_category_id'],
                    'description' => $data['description'],
                    'price' => $data['price'],
                    'status' => 1,
                ]);
            }

            DB::commit();

            return $this->response(200, 'Service Description has been successfully added!', ($ServDesc));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $ServDesc = FimsServiceDescription::with('ServCat')->findOrFail($id);
            $ServDesc->update([
                'service_category_id' => $request['service_category_id'],
                'description' => $request['description'],
                'price' => $request['price'],
            ]);
            DB::commit();
            return $this->response(200, 'Service Description has been successfully updated!', ($ServDesc));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function toggleStatus($request, $id)
    {
        DB::beginTransaction();
        try {
            $ServDesc = FimsServiceDescription::findOrFail($id);
            $ServDesc->update([
                'status' => $ServDesc['status'] === 1 ? 2 : 1,
            ]);
            DB::commit();
            $status = $ServDesc['status'] === 1 ? "activated" : "deactivated";

            return $this->response(200, 'Service Description has been ' . $status . ' successfully !', ['newStatus' => $ServDesc['status']]);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // =======================================================================================================================================================

    public function createValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'service_category_id'=> 'required',
                'description'=> 'required',
                'price'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'service_category_id.required'=> 'Category is required',
                    'description.required'=> 'Description is required',
                    'price.required'=> 'Estimated Rate per hour is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create2($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->createValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);
                $service_ctg = FimsServiceDescription::create([
                    'service_category_id' => $validated['service_category_id'],
                    'description' => $validated['description'],
                    'price' => $validated['price'],
                    'status' => 1,
                ]);

            DB::commit();

            return $this->response(200, 'Service Description has been successfully added!', ($service_ctg));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'service_category_id'=> 'required',
                'description'=> 'required',
                'price'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'service_category_id.required'=> 'Service Category is required',
                    'description.required'=> 'Service Description is required',
                    'price.required'=> 'Estimated Rate per hour is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update2($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->updateValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);

            $ServiceCateg = FimsServiceDescription::findOrFail($id);
                $ServiceCateg->update([
                    'service_category_id' => $validated['service_category_id'],
                    'description' => $validated['description'],
                    'price' => $validated['price'],
                    'status' => 1,
                ]);

            DB::commit();
            return $this->response(200, 'Service Category has been successfully updated!', ($ServiceCateg));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index2($request, $search_request)
    {
        // dd($request['status']);
        DB::beginTransaction();
        try {
            $service_description_lists = FimsServiceDescription::with('ServCat')
            // ->when($search_request['name'] ?? false, function ($query) use ($search_request) {
            //     $query->where('name', 'like', '%' . $search_request['name'] . '%');
            // })
            ->when($request['status'], function ($query) use ($request) {
                if ($request['status'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['status']);
                }
            })
                ->whereHas('ServCat', function ($query) use ($search_request) {
                    $query->when($search_request['name'] ?? false, function ($query) use ($search_request) {
                        $query->where('name', 'like', '%' . $search_request['name'] . '%');
                    });
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $service_description_lists,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
