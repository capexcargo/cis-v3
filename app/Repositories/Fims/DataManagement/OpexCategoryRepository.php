<?php

namespace App\Repositories\Fims\DataManagement;

use App\Traits\ResponseTrait;
use App\Models\FimsBudgetSource;
use App\Models\FimsOpexCategory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Interfaces\Fims\DataManagement\OpexCategoryInterface;

class OpexCategoryRepository implements OpexCategoryInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsOpexCategory::get()->count();
            $getActive = FimsOpexCategory::where('status', 1)->get()->count();
            $getInactive = FimsOpexCategory::where('status', 2)->get()->count();

            $OpexC = FimsOpexCategory::when($request['opex_category'] ?? false, function ($query) use ($request) {
                    $query->where('name', $request['opex_category']);
                })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $OpexC, "count" => ['all' => $getAll, 'active' => $getActive, 'inactive' => $getInactive]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function create($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['data'] as $opx) {
                $OpexC = FimsOpexCategory::create([
                    'opex_name' => $opx['opex_category'],
                    'name' => $opx['opex_type'],
                    'status' => 1,
                ]);
        }

            DB::commit();

            return $this->response(200, 'Opex Category has been successfully added!', ($OpexC));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $OpexC = FimsOpexCategory::findOrFail($id);
            $OpexC->update([
                'opex_name' => $request['opex_category'],
                'name' => $request['opex_type'],
            ]);
            DB::commit();
            return $this->response(200, 'Opex Category has been successfully updated!', ($OpexC));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function toggleStatus($request, $id)
    {
        DB::beginTransaction();
        try {
            $OpexC = FimsOpexCategory::findOrFail($id);
            $OpexC->update([
                'status' => $OpexC['status'] === 1 ? 2 : 1,
            ]);
            DB::commit();
            $status = $OpexC['status'] === 1 ? "activated" : "deactivated";

            return $this->response(200, 'Opex Category has been ' . $status . ' successfully !', ['newStatus' => $OpexC['status']]);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // ============================================================================================================================
    public function createValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name'=> 'required',
                'opex_name'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required'=> 'OpEx Category is required',
                    'opex_name.required'=> 'OpEx Type is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create2($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);
                $service_ctg = FimsOpexCategory::create([
                    'name' => $validated['name'],
                    'opex_name' => $validated['opex_name'],
                    'status' => 1,
                ]);

            DB::commit();

            return $this->response(200, 'OpEx Category has been successfully added!', ($service_ctg));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name'=> 'required',
                'opex_name'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name.required'=> 'OpEx Category is required',
                    'opex_name.required'=> 'OpEx Type is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function update2($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->updateValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);

            $unitName = FimsOpexCategory::findOrFail($id);
                $unitName->update([
                    'name' => $validated['name'],
                    'opex_name' => $validated['opex_name'],
                    'status' => 1,
                ]);

            DB::commit();
            return $this->response(200, 'OpEx Category has been successfully updated!', ($unitName));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index2($request, $search_request)
    {
        DB::beginTransaction();
        try {
            $subAccounts_lists = FimsOpexCategory::when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', 'like', '%' . $search_request['name'] . '%');
            })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $subAccounts_lists,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
