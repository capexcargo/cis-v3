<?php

namespace App\Repositories\Fims\DataManagement;

use App\Interfaces\Fims\DataManagement\PurposeInterface;
use App\Models\FimsPurpose;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PurposeRepository implements PurposeInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsPurpose::get()->count();
            $getActive = FimsPurpose::where('status', 1)->get()->count();
            $getInactive = FimsPurpose::where('status', 2)->get()->count();

            $purp = FimsPurpose::when($request['name'] ?? false, function ($query) use ($request) {
                    $query->where('name', $request['name']);
                })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $purp, "count" => ['all' => $getAll, 'active' => $getActive, 'inactive' => $getInactive]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function create($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['data'] as $data) {
                $purp = FimsPurpose::create([
                    'name' => $data['name'],
                    'status' => 1,
                ]);
        }

            DB::commit();

            return $this->response(200, 'Purpose has been successfully added!', ($purp));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $purp = FimsPurpose::findOrFail($id);
            $purp->update([
                'name' => $request['name'],
            ]);
            DB::commit();
            return $this->response(200, 'Purpose has been successfully updated!', ($purp));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function toggleStatus($request, $id)
    {
        DB::beginTransaction();
        try {
            $purp = FimsPurpose::findOrFail($id);
            $purp->update([
                'status' => $purp['status'] === 1 ? 2 : 1,
            ]);
            DB::commit();
            $status = $purp['status'] === 1 ? "activated" : "deactivated";

            return $this->response(200, 'Purpose has been ' . $status . ' successfully !', ['newStatus' => $purp['status']]);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // =====================================================================================================================================

    public function createValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'purpose_name'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'purpose_name.required'=> 'Purpose is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create2($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);
                $service_ctg = FimsPurpose::create([
                    'name' => $validated['purpose_name'],
                    'status' => 1,
                ]);

            DB::commit();

            return $this->response(200, 'Purpose has been successfully added!', ($service_ctg));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index2($request, $search_request)
    {
        DB::beginTransaction();
        try {
            $purpose_lists = FimsPurpose::when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', 'like', '%' . $search_request['name'] . '%');
            })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $purpose_lists,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'purpose_name'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'purpose_name.required'=> 'Purpose is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update2($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->updateValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);

            $purposeName = FimsPurpose::findOrFail($id);
                $purposeName->update([
                    'name' => $validated['purpose_name'],
                    'status' => 1,
                ]);

            DB::commit();
            return $this->response(200, 'Purpose has been successfully updated!', ($purposeName));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
