<?php

namespace App\Repositories\Fims\DataManagement;

use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use App\Models\FimsManPowerReference;
use Illuminate\Support\Facades\Validator;
use App\Interfaces\Fims\DataManagement\ManpowerInterface;

class ManpowerRepository implements ManpowerInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsManPowerReference::get()->count();
            $getActive = FimsManPowerReference::where('status', 1)->get()->count();
            $getInactive = FimsManPowerReference::where('status', 2)->get()->count();

            $man = FimsManPowerReference::when($request['name'] ?? false, function ($query) use ($request) {
                    $query->where('name', $request['name']);
                })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $man, "count" => ['all' => $getAll, 'active' => $getActive, 'inactive' => $getInactive]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function create($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['data'] as $data) {
                $man = FimsManPowerReference::create([
                    'name' => $data['name'],
                    'email_address' => $data['email_address'],
                    'mobile_number' => $data['mobile_number'],
                    'tel_number' => $data['tel_number'],
                    'agency' => $data['agency'],
                    'tin' => $data['tin'],
                    'status' => 1,
                ]);
        }

            DB::commit();

            return $this->response(200, 'Manpower has been successfully added!', ($man));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $man = FimsManPowerReference::findOrFail($id);
            $man->update([
                'name' => $request['name'],
                'email_address' => $request['email_address'],
                'mobile_number' => $request['mobile_number'],
                'tel_number' => $request['tel_number'],
                'agency' => $request['agency'],
                'tin' => $request['tin'],
            ]);
            DB::commit();
            return $this->response(200, 'Manpower has been successfully updated!', ($man));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function toggleStatus($request, $id)
    {
        DB::beginTransaction();
        try {
            $man = FimsManPowerReference::findOrFail($id);
            $man->update([
                'status' => $man['status'] === 1 ? 2 : 1,
            ]);
            DB::commit();
            $status = $man['status'] === 1 ? "activated" : "deactivated";

            return $this->response(200, 'Manpower has been ' . $status . ' successfully !', ['newStatus' => $man['status']]);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // =================================================================================================================================

    public function createValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name'=> 'required',
                'email_address'=> 'required',
                'mobile_number'=> 'required',
                'tel_number'=> 'sometimes',
                'agency'=> 'required',
                'tin'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                'name'=> 'Name is required',
                'email_address'=> 'Email Address is required',
                'mobile_number'=> 'Mobile Number required',
                'agency'=> 'Company/Agency is required',
                'tin'=> 'Tin Number is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create2($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);
                $service_ctg = FimsManPowerReference::create([
                    'name' => $validated['name'],
                    'email_address' => $validated['email_address'],
                    'mobile_number' => $validated['mobile_number'],
                    'tel_number' => $validated['tel_number'],
                    'agency' => $validated['agency'],
                    'tin' => $validated['tin'],
                    'status' => 1,
                ]);

            DB::commit();

            return $this->response(200, 'Manpower has been successfully added!', ($service_ctg));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'name'=> 'required',
                'email_address'=> 'required',
                'mobile_number'=> 'required',
                'tel_number'=> 'sometimes',
                'agency'=> 'required',
                'tin'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'name'=> 'Name is required',
                    'email_address'=> 'Email Address is required',
                    'mobile_number'=> 'Mobile Number required',
                    'agency'=> 'Company/Agency is required',
                    'tin'=> 'Tin Number is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function update2($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->updateValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);

            $unitName = FimsManPowerReference::findOrFail($id);
                $unitName->update([
                    'name' => $validated['name'],
                    'email_address' => $validated['email_address'],
                    'mobile_number' => $validated['mobile_number'],
                    'tel_number' => $validated['tel_number'],
                    'agency' => $validated['agency'],
                    'tin' => $validated['tin'],
                    'status' => 1,
                ]);

            DB::commit();
            return $this->response(200, 'Manpower has been successfully updated!', ($unitName));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index2($request, $search_request)
    {
        DB::beginTransaction();
        try {
            $subAccounts_lists = FimsManPowerReference::when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', 'like', '%' . $search_request['name'] . '%');
            })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $subAccounts_lists,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
