<?php

namespace App\Repositories\Fims\DataManagement;

use App\Interfaces\Fims\DataManagement\ReasonForRejectionInterface;
use App\Models\FimsReasonForRejection;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class ReasonForRejectionRepository implements ReasonForRejectionInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsReasonForRejection::get()->count();
            $getActive = FimsReasonForRejection::where('status', 1)->get()->count();
            $getInactive = FimsReasonForRejection::where('status', 2)->get()->count();

            $rrej = FimsReasonForRejection::when($request['name'] ?? false, function ($query) use ($request) {
                    $query->where('name', $request['name']);
                })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $rrej, "count" => ['all' => $getAll, 'active' => $getActive, 'inactive' => $getInactive]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function create($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['data'] as $data) {
                $rrej = FimsReasonForRejection::create([
                    'name' => $data['name'],
                    'status' => 1,
                ]);
        }

            DB::commit();

            return $this->response(200, 'Reason for rejection has been successfully added!', ($rrej));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $rrej = FimsReasonForRejection::findOrFail($id);
            $rrej->update([
                'name' => $request['name'],
            ]);
            DB::commit();
            return $this->response(200, 'Reason for rejection has been successfully updated!', ($rrej));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function toggleStatus($request, $id)
    {
        DB::beginTransaction();
        try {
            $rrej = FimsReasonForRejection::findOrFail($id);
            $rrej->update([
                'status' => $rrej['status'] === 1 ? 2 : 1,
            ]);
            DB::commit();
            $status = $rrej['status'] === 1 ? "activated" : "deactivated";

            return $this->response(200, 'Reason for rejection has been ' . $status . ' successfully !', ['newStatus' => $rrej['status']]);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // ===============================================================================================================================================

    public function createValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'RFR_name'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'RFR_name.required'=> 'Reason for Rejection is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create2($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);
                $service_ctg = FimsReasonForRejection::create([
                    'name' => $validated['RFR_name'],
                    'status' => 1,
                ]);

            DB::commit();

            return $this->response(200, 'Reason for Rejection has been successfully added!', ($service_ctg));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'RFR_name'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'RFR_name.required'=> 'Reason for rejection is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function update2($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->updateValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);

            $unitName = FimsReasonForRejection::findOrFail($id);
                $unitName->update([
                    'name' => $validated['RFR_name'],
                    'status' => 1,
                ]);

            DB::commit();
            return $this->response(200, 'Reason for Rejection has been successfully updated!', ($unitName));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index2($request, $search_request)
    {
        DB::beginTransaction();
        try {
            $RFR_lists = FimsReasonForRejection::when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', 'like', '%' . $search_request['name'] . '%');
            })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $RFR_lists,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

}
