<?php

namespace App\Repositories\Fims\DataManagement;

use App\Interfaces\Fims\DataManagement\ItemCategoryInterface;
use App\Models\FimsBudgetManagement;
use App\Models\FimsBudgetSource;
use App\Models\FimsChartsOfAccounts;
use App\Models\FimsItemCategory;
use App\Models\FimsOpexCategory;
use App\Models\FimsSubAccounts;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ItemCategoryRepository implements ItemCategoryInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsItemCategory::get()->count();
            $getActive = FimsItemCategory::where('status', 1)->get()->count();
            $getInactive = FimsItemCategory::where('status', 2)->get()->count();

            $ItemCateg = FimsItemCategory::with('budgetSrc','opexCat','chartOfAccs','subAccs')->when($request['name'] ?? false, function ($query) use ($request) {
                $query->where('name', $request['name']);
            })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $ItemCateg, "count" => ['all' => $getAll, 'active' => $getActive, 'inactive' => $getInactive]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getBudget($request)
    {
        DB::beginTransaction();
        try {
            $Budget = FimsBudgetSource::where('status', 1)->get();
            DB::commit();

            $array = ["Budget" => $Budget,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getOpex($request)
    {
        DB::beginTransaction();
        try {
            $Opex = FimsOpexCategory::where('status', 1)->get();
            DB::commit();

            $array = ["Opex" => $Opex,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getCharts($request)
    {
        DB::beginTransaction();
        try {
            $ChartsofAccounts = FimsChartsOfAccounts::where('status', 1)->get();
            DB::commit();

            $array = ["ChartsofAccounts" => $ChartsofAccounts,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getSubAcc($request)
    {
        DB::beginTransaction();
        try {
            $Sub = FimsBudgetManagement::where('type', 4)->get();
            DB::commit();

            $array = ["SubAccounts" => $Sub,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function create($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['data'] as $data) {
                $ItemCateg = FimsItemCategory::create([
                    'name' => $data['name'],
                    'is_recurring' => $data['is_recurring'],
                    'budget_source_id' => $data['budget_source_id'],
                    'opex_category_id' => $data['opex_category_id'],
                    'chart_of_acccounts_id' => $data['chart_of_acccounts_id'],
                    'sub_accounts_id' => $data['sub_accounts_id'],
                    'status' => 1,
                ]);
            }

            DB::commit();

            return $this->response(200, 'Item Category has been successfully added!', ($ItemCateg));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $ItemCateg = FimsItemCategory::findOrFail($id);
            $ItemCateg->update([
                'name' => $request['name'],
                'is_recurring' => $request['is_recurring'],
                'budget_source_id' => $request['budget_source_id'],
                'opex_category_id' => $request['opex_category_id'],
                'chart_of_acccounts_id' => $request['chart_of_acccounts_id'],
                'sub_accounts_id' => $request['sub_accounts_id'],
            ]);
            DB::commit();
            return $this->response(200, 'Item Category has been successfully updated!', ($ItemCateg));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function toggleStatus($request, $id)
    {
        DB::beginTransaction();
        try {
            $ItemCateg = FimsItemCategory::findOrFail($id);
            $ItemCateg->update([
                'status' => $ItemCateg['status'] === 1 ? 2 : 1,
            ]);
            DB::commit();
            $status = $ItemCateg['status'] === 1 ? "activated" : "deactivated";

            return $this->response(200, 'Item Category has been ' . $status . ' successfully !', ['newStatus' => $ItemCateg['status']]);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // =========================================================================================================================

    public function createValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'item_category'=> 'required',
                'recurring'=> 'sometimes',
                'budget_source'=> 'required',
                'opex_category'=> 'required',
                'opex_type'=> 'required',
                'chart_of_accounts'=> 'required',
                'sub_accounts'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'item_category.required'=> 'Item Category is required',
                    'budget_source.required'=> 'Budget source is required',
                    'opex_category.required'=> 'Opex Category is required',
                    'opex_type.required'=> 'Opex type is required',
                    'chart_of_accounts.required'=> 'Chart of Accounts is required',
                    'sub_accounts.required'=> 'Sub Accounts is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create2($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);
            $ItemCateg = FimsItemCategory::create([
                'name' => $validated['item_category'],
                'is_recurring' => ($validated['recurring'] == 1 ? $validated['recurring'] : 0),
                'budget_source_id' => $validated['budget_source'],
                'opex_category_id' => $validated['opex_category'],
                'chart_of_acccounts_id' => $validated['chart_of_accounts'],
                'sub_accounts_id' => $validated['sub_accounts'],
                'status' => 1,
            ]);

            DB::commit();

            return $this->response(200, 'Item Category has been successfully added!', ($ItemCateg));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'item_category'=> 'required',
                'recurring'=> 'sometimes',
                'budget_source'=> 'required',
                'opex_category'=> 'required',
                'opex_type'=> 'required',
                'chart_of_accounts'=> 'required',
                'sub_accounts'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'item_category.required'=> 'Item Category is required',
                    'budget_source.required'=> 'Budget source is required',
                    'opex_category.required'=> 'Opex Category is required',
                    'opex_type.required'=> 'Opex type is required',
                    'chart_of_accounts.required'=> 'Chart of Accounts is required',
                    'sub_accounts.required'=> 'Sub Accounts is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update2($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->updateValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);

            $ItemCateg = FimsItemCategory::findOrFail($id);
                $ItemCateg->update([
                    'name' => $validated['item_category'],
                    'is_recurring' => ($validated['recurring'] == 1 ? $validated['recurring'] : 0),
                    'budget_source_id' => $validated['budget_source'],
                    'opex_category_id' => $validated['opex_category'],
                    'chart_of_acccounts_id' => $validated['chart_of_accounts'],
                    'sub_accounts_id' => $validated['sub_accounts'],
                    'status' => 1,
                ]);

            DB::commit();
            return $this->response(200, 'Item Category has been successfully updated!', ($ItemCateg));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index2($request, $search_request)
    {
        DB::beginTransaction();
        try {
            $item_category_lists = FimsItemCategory::with('budgetSrc','opexCat','chartOfAccs','subAccs')->when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', 'like', '%' . $search_request['name'] . '%');
            })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $item_category_lists,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
