<?php

namespace App\Repositories\Fims\DataManagement;

use App\Interfaces\Fims\DataManagement\BankNameInterface;
use App\Models\FimsBankNameReference;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class BankNameRepository implements BankNameInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsBankNameReference::get()->count();
            $getActive = FimsBankNameReference::where('status', 1)->get()->count();
            $getInactive = FimsBankNameReference::where('status', 2)->get()->count();

            $bank = FimsBankNameReference::when($request['name'] ?? false, function ($query) use ($request) {
                    $query->where('name', $request['name']);
                })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $bank, "count" => ['all' => $getAll, 'active' => $getActive, 'inactive' => $getInactive]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function create($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['data'] as $data) {
                $bank = FimsBankNameReference::create([
                    'name' => $data['name'],
                    'status' => 1,
                ]);
        }

            DB::commit();

            return $this->response(200, 'Bank Name has been successfully added!', ($bank));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $bank = FimsBankNameReference::findOrFail($id);
            $bank->update([
                'name' => $request['name'],
            ]);
            DB::commit();
            return $this->response(200, 'Bank Name has been successfully updated!', ($bank));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function toggleStatus($request, $id)
    {
        DB::beginTransaction();
        try {
            $bank = FimsBankNameReference::findOrFail($id);
            $bank->update([
                'status' => $bank['status'] === 1 ? 2 : 1,
            ]);
            DB::commit();
            $status = $bank['status'] === 1 ? "activated" : "deactivated";

            return $this->response(200, 'Bank Name has been ' . $status . ' successfully !', ['newStatus' => $bank['status']]);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
