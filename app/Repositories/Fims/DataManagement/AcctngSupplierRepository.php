<?php

namespace App\Repositories\Fims\DataManagement;

use App\Interfaces\Fims\DataManagement\AcctngSupplierInterface;
use App\Models\Crm\BarangayReference;
use App\Models\Crm\CityReference;
use App\Models\Crm\StateReference;
use App\Models\FimsIndustry;
use App\Models\FimsSupplier;
use App\Models\Region;
use App\Models\FimsBankNameReference;
use App\Traits\ResponseTrait;
use Google\Service\Dfareporting\City;
use Illuminate\Support\Facades\DB;

class AcctngSupplierRepository implements AcctngSupplierInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsSupplier::get()->count();
            $getActive = FimsSupplier::where('status', 1)->get()->count();
            $getInactive = FimsSupplier::where('status', 2)->get()->count();

            $sup = FimsSupplier::with('IndSup', 'RegSup', 'ProSup', 'MunSup', 'BarSup', 'BankSup')->when($request['name'] ?? false, function ($query) use ($request) {
                $query->where('name', $request['name']);
            })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $sup, "count" => ['all' => $getAll, 'active' => $getActive, 'inactive' => $getInactive]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getIndustry($request)
    {
        DB::beginTransaction();
        try {
            $Ind = FimsIndustry::where('status', 1)->get();
            DB::commit();

            $array = ["Ind" => $Ind,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getRegion($request)
    {
        DB::beginTransaction();
        try {
            $Reg = Region::get();
            DB::commit();

            $array = ["Reg" => $Reg,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getProvince($request)
    {
        DB::beginTransaction();
        try {
            $Prov = StateReference::where('status', 1)->get();
            DB::commit();

            $array = ["Prov" => $Prov,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getMunicipal($request)
    {
        DB::beginTransaction();
        try {
            $Muni = CityReference::where('status', 1)->get();
            DB::commit();

            $array = ["Muni" => $Muni,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getBarangay($request)
    {
        DB::beginTransaction();
        try {
            $Bar = BarangayReference::where('status', 1)->get();
            DB::commit();

            $array = ["Bar" => $Bar,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getBank($request)
    {
        DB::beginTransaction();
        try {
            $bank = FimsBankNameReference::where('status', 1)->get();
            DB::commit();

            $array = ["bank" => $bank,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['data'] as $data) {
                $middle_name = array_key_exists('middle_name', $data) ? $data['middle_name'] : null;
                $tel_number = array_key_exists('tel_number', $data) ? $data['tel_number'] : null;
                $sup = FimsSupplier::create([
                    'name' => $data['name'],
                    'trade_name' => $data['trade_name'],
                    'tin' => $data['tin'],
                    'industry_id' => $data['industry_id'],
                    'region_id' => $data['region_id'],
                    'province_id' => $data['province_id'],
                    'municipal_id' => $data['municipal_id'],
                    'barangay_id' => $data['barangay_id'],
                    'postal_code' => $data['postal_code'],
                    'address' => $data['address'],
                    'first_name' => $data['first_name'],
                    'middle_name' => $middle_name,
                    'last_name' => $data['last_name'],
                    'email_address' => $data['email_address'],
                    'mobile_number' => $data['mobile_number'],
                    'tel_number' => $tel_number,
                    'payee_name' => $data['payee_name'],
                    'payee_account_number' => $data['payee_account_number'],
                    'bank_account_number' => $data['bank_account_number'],
                    'bank_account_name' => $data['bank_account_name'],
                    'bank_id' => $data['bank_id'],
                    'status' => 1,
                ]);
            }

            DB::commit();

            return $this->response(200, 'Supplier has been successfully added!', ($sup));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $middle_name = array_key_exists('middle_name', $request) ? $request['middle_name'] : null;
            $tel_number = array_key_exists('tel_number', $request) ? $request['tel_number'] : null;
            $sup = FimsSupplier::findOrFail($id);
            $sup->update([
                'name' => $request['name'],
                'trade_name' => $request['trade_name'],
                'tin' => $request['tin'],
                'industry_id' => $request['industry_id'],
                'region_id' => $request['region_id'],
                'province_id' => $request['province_id'],
                'municipal_id' => $request['municipal_id'],
                'barangay_id' => $request['barangay_id'],
                'postal_code' => $request['postal_code'],
                'address' => $request['address'],
                'first_name' => $request['first_name'],
                'middle_name' => $middle_name,
                'last_name' => $request['last_name'],
                'email_address' => $request['email_address'],
                'mobile_number' => $request['mobile_number'],
                'tel_number' => $tel_number,
                'payee_name' => $request['payee_name'],
                'payee_account_number' => $request['payee_account_number'],
                'bank_account_number' => $request['bank_account_number'],
                'bank_account_name' => $request['bank_account_name'],
                'bank_id' => $request['bank_id'],
            ]);
            DB::commit();
            return $this->response(200, 'Supplier has been successfully updated!', ($sup));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function toggleStatus($request, $id)
    {
        DB::beginTransaction();
        try {
            $sup = FimsSupplier::findOrFail($id);
            $sup->update([
                'status' => $sup['status'] === 1 ? 2 : 1,
            ]);
            DB::commit();
            $status = $sup['status'] === 1 ? "activated" : "deactivated";

            return $this->response(200, 'Supplier has been ' . $status . ' successfully !', ['newStatus' => $sup['status']]);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
