<?php

namespace App\Repositories\Fims\DataManagement;

use App\Interfaces\Fims\DataManagement\ChartofAccountsInterface;
use App\Models\FimsChartsOfAccounts;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ChartofAccountsRepository implements ChartofAccountsInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsChartsOfAccounts::get()->count();
            $getActive = FimsChartsOfAccounts::where('status', 1)->get()->count();
            $getInactive = FimsChartsOfAccounts::where('status', 2)->get()->count();

            $Charts = FimsChartsOfAccounts::when($request['name'] ?? false, function ($query) use ($request) {
                    $query->where('name', $request['name']);
                })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $Charts, "count" => ['all' => $getAll, 'active' => $getActive, 'inactive' => $getInactive]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function create($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['data'] as $data) {
                $Charts = FimsChartsOfAccounts::create([
                    'name' => $data['name'],
                    'account_code' => $data['account_code'],
                    'status' => 1,
                ]);
        }

            DB::commit();

            return $this->response(200, 'Chart of Accounts has been successfully added!', ($Charts));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $Charts = FimsChartsOfAccounts::findOrFail($id);
            $Charts->update([
                'name' => $request['name'],
                'account_code' => $request['account_code'],
            ]);
            DB::commit();
            return $this->response(200, 'Chart of Accounts has been successfully updated!', ($Charts));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function toggleStatus($request, $id)
    {
        DB::beginTransaction();
        try {
            $Charts = FimsChartsOfAccounts::findOrFail($id);
            $Charts->update([
                'status' => $Charts['status'] === 1 ? 2 : 1,
            ]);
            DB::commit();
            $status = $Charts['status'] === 1 ? "activated" : "deactivated";

            return $this->response(200, 'Chart of Accounts has been ' . $status . ' successfully !', ['newStatus' => $Charts['status']]);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    //=======================================================================================================

    public function createValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'chartofAccounts_name'=> 'required',
                'acct_code'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'chartofAccounts_name.required'=> 'Chart of Account is required',
                    'acct_code.required'=> 'Account Code is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create2($request)
    {
        DB::beginTransaction();
        try {
            $response = $this->createValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);
                $service_ctg = FimsChartsOfAccounts::create([
                    'name' => $validated['chartofAccounts_name'],
                    'account_code' => $validated['acct_code'],
                    'status' => 1,
                ]);

            DB::commit();

            return $this->response(200, 'Chart of Account has been successfully added!', ($service_ctg));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'chartofAccounts_name'=> 'required',
                'acct_code'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'chartofAccounts_name.required'=> 'Chart of Account is required',
                    'acct_code.required'=> 'Account Code is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function update2($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->updateValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);

            $unitName = FimsChartsOfAccounts::findOrFail($id);
                $unitName->update([
                    'name' => $validated['chartofAccounts_name'],
                    'account_code' => $validated['acct_code'],
                    'status' => 1,
                ]);

            DB::commit();
            return $this->response(200, 'Chart of Account has been successfully updated!', ($unitName));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index2($request, $search_request)
    {
        DB::beginTransaction();
        try {
            $subAccounts_lists = FimsChartsOfAccounts::when($search_request['name'] ?? false, function ($query) use ($search_request) {
                $query->where('name', 'like', '%' . $search_request['name'] . '%');
            })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $subAccounts_lists,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
