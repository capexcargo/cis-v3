<?php

namespace App\Repositories\Fims\DataManagement;

use App\Interfaces\Accounting\BudgetManagement\BudgetSourceInterface;
use App\Interfaces\Fims\DataManagement\BudgetSRCInterface;
use App\Models\FimsBudgetSource;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class BudgetSRCRepository implements BudgetSRCInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsBudgetSource::get()->count();
            $getActive = FimsBudgetSource::where('status', 1)->get()->count();
            $getInactive = FimsBudgetSource::where('status', 2)->get()->count();

            $budgetSrc = FimsBudgetSource::when($request['name'] ?? false, function ($query) use ($request) {
                    $query->where('name', $request['name']);
                })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $budgetSrc, "count" => ['all' => $getAll, 'active' => $getActive, 'inactive' => $getInactive]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }


    public function create($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['names'] as $name) {

                $budgetSrc = FimsBudgetSource::create([
                    'name' => $name,
                    'status' => 1,
                ]);
            }
            DB::commit();

            return $this->response(200, 'Budget Source has been successfully added!', ($budgetSrc));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $budgetSrc = FimsBudgetSource::findOrFail($id);
            $budgetSrc->update([
                'name' => $request['name'],
            ]);

            DB::commit();

            return $this->response(200, 'Budget Source has been successfully updated!', ($budgetSrc));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function toggleStatus($request, $id)
    {
        DB::beginTransaction();
        try {
            $budgetSrc = FimsBudgetSource::findOrFail($id);
            $budgetSrc->update([
                'status' => $budgetSrc['status'] === 1 ? 2 : 1,
            ]);

            DB::commit();

            $status = $budgetSrc['status'] === 1 ? "activated" : "deactivated";

            return $this->response(200, 'Budget Source has been ' . $status . ' successfully !', ['newStatus' => $budgetSrc['status']]);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

}
