<?php

namespace App\Repositories\Fims\DataManagement;

use App\Interfaces\Fims\DataManagement\ItemDescriptionInterface;
use App\Models\FimsItemCategory;
use App\Models\FimsItemDescription;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ItemDescriptionRepository implements ItemDescriptionInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsItemDescription::get()->count();
            $getActive = FimsItemDescription::where('status', 1)->get()->count();
            $getInactive = FimsItemDescription::where('status', 2)->get()->count();

            $ItemDesc = FimsItemDescription::with('ItemCat')->when($request['name'] ?? false, function ($query) use ($request) {
                $query->where('description', $request['name']);
            })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $ItemDesc, "count" => ['all' => $getAll, 'active' => $getActive, 'inactive' => $getInactive]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function getItemCateg($request)
    {
        DB::beginTransaction();
        try {
            $ItemCateg = FimsItemCategory::where('status', 1)->get();
            DB::commit();

            $array = ["list" => $ItemCateg,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['data'] as $data) {
                $ItemDesc = FimsItemDescription::create([
                    'item_category_id' => $data['item_category_id'],
                    'description' => $data['description'],
                    'price' => $data['price'],
                    'status' => 1,
                ]);
            }

            DB::commit();

            return $this->response(200, 'Item Description has been successfully added!', ($ItemDesc));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $ItemDesc = FimsItemDescription::with('ItemCat')->findOrFail($id);
            $ItemDesc->update([
                'item_category_id' => $request['item_category_id'],
                'description' => $request['description'],
                'price' => $request['price'],
            ]);
            DB::commit();
            return $this->response(200, 'Item Description has been successfully updated!', ($ItemDesc));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function toggleStatus($request, $id)
    {
        DB::beginTransaction();
        try {
            $ItemDesc = FimsItemDescription::findOrFail($id);
            $ItemDesc->update([
                'status' => $ItemDesc['status'] === 1 ? 2 : 1,
            ]);
            DB::commit();
            $status = $ItemDesc['status'] === 1 ? "activated" : "deactivated";

            return $this->response(200, 'Item Description has been ' . $status . ' successfully !', ['newStatus' => $ItemDesc['status']]);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    // ========================================================================================================================================================

    public function createValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'item_category_id'=> 'required',
                'description'=> 'required',
                'price'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'item_category_id.required'=> 'Category is required',
                    'description.required'=> 'Description is required',
                    'price.required'=> 'Estimated Rate per hour is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function create2($request)
    {
        // dd($request);
        DB::beginTransaction();
        try {
            $response = $this->createValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);
                $item_ctg = FimsItemDescription::create([
                    'item_category_id' => $validated['item_category_id'],
                    'description' => $validated['description'],
                    'price' => $validated['price'],
                    'status' => 1,
                ]);

            DB::commit();

            return $this->response(200, 'Service Description has been successfully added!', ($item_ctg));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function updateValidation2($request)
    {
        DB::beginTransaction();
        try {

            $rules = [
                'item_category_id'=> 'required',
                'description'=> 'required',
                'price'=> 'required',
            ];

            $validator = Validator::make(
                $request,
                $rules,
                [
                    'item_category_id.required'=> 'Item Category is required',
                    'description.required'=> 'Description is required',
                    'price.required'=> 'Estimated Rate per hour is required',
                ]
            );
            if ($validator->fails()) {
                return $this->response(400, 'Please Fill Required Field', $validator->errors());
            }
            DB::commit();
            return $this->response(200, 'Successfully Validated', $validator->validated());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update2($request, $id)
    {
        DB::beginTransaction();
        try {
            $response = $this->updateValidation2($request);
            if ($response['code'] != 200) {
                return $this->response($response['code'], $response['message'], $response['result']);
            }
            $validated = $response['result'];
            // dd($validated);

            $ServiceCateg = FimsItemDescription::findOrFail($id);
                $ServiceCateg->update([
                    'item_category_id' => $validated['item_category_id'],
                    'description' => $validated['description'],
                    'price' => $validated['price'],
                    'status' => 1,
                ]);

            DB::commit();
            return $this->response(200, 'Item Description has been successfully updated!', ($ServiceCateg));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function index2($request, $search_request)
    {
        // dd($request['status']);
        DB::beginTransaction();
        try {
            $item_description_lists = FimsItemDescription::with('ItemCat')
            // ->when($search_request['name'] ?? false, function ($query) use ($search_request) {
            //     $query->where('name', 'like', '%' . $search_request['name'] . '%');
            // })
            ->when($request['status'], function ($query) use ($request) {
                if ($request['status'] == false) {
                    $query->whereIn('status', [1, 2]);
                } else {
                    $query->where('status', $request['status']);
                }
            })
                ->whereHas('ItemCat', function ($query) use ($search_request) {
                    $query->when($search_request['name'] ?? false, function ($query) use ($search_request) {
                        $query->where('name', 'like', '%' . $search_request['name'] . '%');
                    });
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $item_description_lists,];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
