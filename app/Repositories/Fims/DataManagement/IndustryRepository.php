<?php

namespace App\Repositories\Fims\DataManagement;

use App\Interfaces\Fims\DataManagement\IndustryInterface;
use App\Models\FimsIndustry;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class IndustryRepository implements IndustryInterface
{
    use ResponseTrait;

    public function index($request)
    {
        DB::beginTransaction();
        try {
            $getAll = FimsIndustry::get()->count();
            $getActive = FimsIndustry::where('status', 1)->get()->count();
            $getInactive = FimsIndustry::where('status', 2)->get()->count();

            $Ind = FimsIndustry::when($request['name'] ?? false, function ($query) use ($request) {
                    $query->where('name', $request['name']);
                })
                ->when($request['status'], function ($query) use ($request) {
                    if ($request['status'] == false) {
                        $query->whereIn('status', [1, 2]);
                    } else {
                        $query->where('status', $request['status']);
                    }
                })
                ->paginate(10);
            DB::commit();

            $array = ["list" => $Ind, "count" => ['all' => $getAll, 'active' => $getActive, 'inactive' => $getInactive]];
            return $this->response(200, 'List', ($array));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
    public function create($request)
    {
        DB::beginTransaction();
        try {
            foreach ($request['data'] as $data) {
                $Ind = FimsIndustry::create([
                    'name' => $data['name'],
                    'status' => 1,
                ]);
        }

            DB::commit();

            return $this->response(200, 'Industry has been successfully added!', ($Ind));
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $Ind = FimsIndustry::findOrFail($id);
            $Ind->update([
                'name' => $request['name'],
            ]);
            DB::commit();
            return $this->response(200, 'Industry has been successfully updated!', ($Ind));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }

    public function toggleStatus($request, $id)
    {
        DB::beginTransaction();
        try {
            $Ind = FimsIndustry::findOrFail($id);
            $Ind->update([
                'status' => $Ind['status'] === 1 ? 2 : 1,
            ]);
            DB::commit();
            $status = $Ind['status'] === 1 ? "activated" : "deactivated";

            return $this->response(200, 'Industry has been ' . $status . ' successfully !', ['newStatus' => $Ind['status']]);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}
