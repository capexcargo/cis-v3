<?php

namespace App\Http\Livewire\Oims\Reports\OrderManagementReports;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.oims.reports.order-management-reports.index');
    }
}
