<?php

namespace App\Http\Livewire\Oims\OrderManagement\VehicleUtilization;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.oims.order-management.vehicle-utilization.index');
    }
}
