<?php

namespace App\Http\Livewire\Oims\OrderManagement\TransactionEntry;

use App\Traits\Oims\TransactionEntry\TransactionEntryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class WaybillSummary extends Component
{
    use TransactionEntryTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['waybill_summary' => 'mount'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'print_sticker') {
            $this->print_sticker_modal = true;
        }elseif ($action_type == 'print_waybill') {
            $this->print_waybill_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'print_sticker') {
            $this->print_sticker_modal = false;
        }elseif ($action_type == 'print_waybill') {
            $this->print_waybill_modal = false;
        }
    }

    public function render()
    {
        return view('livewire.oims.order-management.transaction-entry.waybill-summary');
    }
}
