<?php

namespace App\Http\Livewire\Oims\OrderManagement\TransactionEntry;

use App\Models\BranchReference;
use App\Models\CrmBooking;
use App\Models\OimsTransactionEntry;
use App\Traits\Oims\TransactionEntry\TransactionEntryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use TransactionEntryTrait, WithPagination, PopUpMessagesTrait;


    protected $listeners = ['mount' => 'mount', 'index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public $book_id;
    public $bookings = [];
    public $cons;

    // public function mount($transid)
    // {
    // dd($transid);
    // $this->book_id = $id;

    // $this->bookings = CrmBooking::with('BookingShipper', 'BookingConsigneeHasManyBK', 'ShipperReferenceBK', 'ActivityReferenceBK', 'TimeslotReferenceBK', 'FinalStatusReferenceBK', 'BookingConsigneeHasManyBK')
    //     ->where('id', $this->book_id)->first();

    // if ($this->bookings['BookingConsigneeHasManyBK']->count() > 2) {

    //     $this->select_cons_modal = true;
    // }
    // }


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'scan') {
            $this->scan_modal = true;
        } elseif ($action_type == 'view_remarks') {
            $this->view_remarks_modal = true;
        } elseif ($action_type == 'add_remarks') {
            $this->add_remarks_modal = true;
        } elseif ($action_type == 'override_history') {
            $this->override_history_modal = true;
        } elseif ($action_type == 'waybill_summary') {
            // $this->waybill_summary_modal = true;
            return redirect()->route('oims.order-management.transaction-entry.waybill-summary');
            // return redirect()->route('oims.order-management.transaction-entry.waybill-summary', ['id' => $data['id']]);

        }
        // elseif ($action_type == 'print') {
        //     $this->print_modal = true;
        // } elseif ($action_type == 'share') {
        //     $this->share_modal = true;
        // } elseif ($action_type == 'edit') {
        //     $this->emitTo('oims.order-management.team-route-assignment.edit', 'edit', $data['id']);
        //     $this->team_id = $data['id'];
        //     $this->edit_modal = true;
        // }
        // else if ($action_type == 'update_status') {
        //     if ($data['status'] == 1) {
        //         $this->reactivate_modal = true;
        //         $this->team_id = $data['id'];
        //         return;
        //     }
        //     $this->deactivate_modal = true;
        //     $this->team_id = $data['id'];
        // }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'scan') {
            $this->scan_modal = false;
        } elseif ($action_type == 'view_remarks') {
            $this->view_remarks_modal = false;
        } elseif ($action_type == 'add_remarks') {
            $this->add_remarks_modal = false;
        } elseif ($action_type == 'override_history') {
            $this->override_history_modal = false;
        } elseif ($action_type == 'waybill_summary') {
            // $this->waybill_summary_modal = false;
        }
    }

    public function search()
    {
        $getTransactionEntry = OimsTransactionEntry::with('CrmBookingReference', 'OimsBranchReference', 'ShipperReference.contactPersons', 
        'ShipperReference.mobileNumbers', 'ShipperReference.addresses')->where('waybill', $this->waybill_no)->first();

        // $getBranch = BranchReference::where('id', $getTransactionEntry['branch_id'])->first();
        // dd($getTransactionEntry);
        if ($getTransactionEntry != null) {
            
            $this->trans_type = $getTransactionEntry['transaction_type'];
            $this->book_ref_no = $getTransactionEntry['CrmBookingReference']['booking_reference_no'];
            $this->book_type = $getTransactionEntry['booking_type_id'] ?? '';
            $this->origin = $getTransactionEntry['origin_id'];
            $this->trans_port = $getTransactionEntry['transhipment_id'] ?? '';
            $this->branch = $getTransactionEntry['OimsBranchReference']['name'];
            $this->trans_date = date('m/d/Y', strtotime($getTransactionEntry['transaction_date']));
            $this->transport_mode = $getTransactionEntry['transport_mode_id'];
            $this->destination = $getTransactionEntry['destination_id'];

            $this->destination = $getTransactionEntry['destination_id'];
            $this->scust_no = $getTransactionEntry['ShipperReference']['account_no'];
            $this->scname = $getTransactionEntry['ShipperReference']['company_name'] ?? '';
            $this->scont_person = $getTransactionEntry['ShipperReference']['contactPersons'][0]['first_name'] . ' ' . $getTransactionEntry['ShipperReference']['contactPersons'][0]['last_name'];
            $this->smobile = $getTransactionEntry['ShipperReference']['mobileNumbers'][0]['mobile'];
            $this->sadrs = $getTransactionEntry['shipper_address'];

            $this->ccust_no = $getTransactionEntry['ConsigneeReference']['account_no'];
            $this->ccname = $getTransactionEntry['ConsigneeReference']['company_name'] ?? '';
            $this->ccont_person = $getTransactionEntry['ConsigneeReference']['contactPersons'][0]['first_name'] . ' ' . $getTransactionEntry['ConsigneeReference']['contactPersons'][0]['last_name'];
            $this->cmobile = $getTransactionEntry['ConsigneeReference']['mobileNumbers'][0]['mobile'];
            $this->cadrs = $getTransactionEntry['consignee_address'];

            // $this->des_goods = $getTransactionEntry;
            $this->com_type = $getTransactionEntry['commodity_type'];
            // $this->dec_val = $getTransactionEntry;
            $this->types_goods = $getTransactionEntry['type_of_goods'] ?? null;
            $this->paymode = $getTransactionEntry['paymode'];
            $this->serv_mode = $getTransactionEntry['service_mode'];
            $this->charge_to = $getTransactionEntry['charge_to_id'] ?? null;
            $this->com_app_rate = $getTransactionEntry['commodity_app_rate'];
            if ($getTransactionEntry['transport_mode_id'] == 1) {
                // dd($getTransactionEntry);

                if ($getTransactionEntry['itemDetails'] != null) {
                    $this->air_cargo = true;
                }
                foreach ($getTransactionEntry['itemDetails'] as $a => $air_cargo) {

                    if ($this->air_cargo == true) {
                        $this->air_cargos[$a] = [
                            'id' => $air_cargo->id,
                            'waybill_id' => $air_cargo->waybill_id,
                            'ac_qty' => $air_cargo->qty,
                            'ac_wt' => $air_cargo->weight,
                            'ac_dl' => $air_cargo->length,
                            'ac_dw' => $air_cargo->width,
                            'ac_dh' => $air_cargo->height,
                            'ac_um' => $air_cargo->unit_of_measurement_id,
                            'ac_umt' => $air_cargo->measurement_type_id,
                            'ac_tp' => $air_cargo->type_of_packaging_id,
                            'ac_fc' => ($air_cargo->is_for_crating == 1 ? true : false),
                            // 'crating_status' => 0,
                            'ac_ct' => $air_cargo->crating_type_id ?? null,
                            'ac_cwt' => $air_cargo->cwt ?? null,
                            // 'cbm' => null,
                            // 'container' => null,
                            // 'rate_id_applied' => null,
                            // 'cargotype' => 1,
                            'is_deleted' => false,
                        ];
                    }
                    //     if ($request['air_pouch'] == true) {
                    //     }

                    //     if ($request['air_box'] == true) {
                    //     }
                }
                $countarray = 0;
                $getkeyarray = [];
                foreach ($this->air_cargos as $a => $air_cargo) {

                    $getkeyarray[] = $a;
                    $countarray += ($this->air_cargos[$a]['is_deleted'] == false ? 1 : 0);
                }
                $this->min = min($getkeyarray);
                $this->nearest = max($getkeyarray);
                $this->countarr = $countarray;
            }


            // if ($validated['transport_mode'] == 2) {
            //     if ($validated['sea_cargo'] == true) {
            //     }
            //     if ($validated['sea_box'] == true) {
            //     }
            // }

            // if ($validated['transport_mode'] == 3) {
            //     if ($validated['land'] == true) {
            //     }
            // }
        }
    }

    public function isForCreating($id, $trans_mode, $is_true)
    {
        if ($trans_mode == 1) {
            if ($is_true == true) {
                foreach ($this->air_cargos as $i => $ac) {
                    if ($i == $id) {
                        $this->air_cargos[$i]['ac_fc'] = true;
                    }
                }
            } else {
                $this->air_cargos[$id]['ac_fc'] = false;
                $this->air_cargos[$id]['ac_ct'] = null;
            }
        }

        if ($trans_mode == 2) {
            if ($is_true == true) {
                foreach ($this->sea_cargos_is_lcl as $i => $sc) {
                    if ($i == $id) {
                        $this->sea_cargos_is_lcl[$i]['for_crating'] = true;
                    }
                }
            } else {
                $this->sea_cargos_is_lcl[$id]['for_crating'] = false;
                $this->air_cargos[$id]['ac_ct'] = null;
            }
        }

        if ($trans_mode == 3) {
            if ($is_true == true) {
                foreach ($this->sea_cargos_is_rcl as $i => $sc) {
                    if ($i == $id) {
                        $this->sea_cargos_is_rcl[$i]['for_crating'] = true;
                    }
                }
            } else {
                $this->sea_cargos_is_rcl[$id]['for_crating'] = false;
                $this->air_cargos[$id]['ac_ct'] = null;
            }
        }
    }

    public function addair_cargos()
    {
        $this->air_cargos[] = [
            'id' => null,
            'ac_qty' => null,
            'ac_wt' => null,
            'ac_dl' => null,
            'ac_dw' => null,
            'ac_dh' => null,
            'ac_um' => null,
            'ac_umt' => null,
            'ac_tp' => null,
            'ac_fc' => null,
            'ac_ct' => null,
            'ac_cwt' => null,
            'is_deleted' => false,
        ];
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->air_cargos as $a => $air_cargo) {

            if ($this->air_cargos[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }
            $countarray += ($this->air_cargos[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    public function removeair_cargos(array $data)
    {
        if (count(collect($this->air_cargos)->where('is_deleted', false)) > 1) {
            if ($this->air_cargos[$data["a"]]['id']) {
                $this->air_cargos[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->air_cargos[$data["a"]]);
            }

            array_values($this->air_cargos);
>>>>>>> 42712ec72d0e1f642bdfe6d91b3f440d11140c3e
        }
    }


    public function render()
    {
        $this->search();
        return view('livewire.oims.order-management.transaction-entry.index');
    }
}
