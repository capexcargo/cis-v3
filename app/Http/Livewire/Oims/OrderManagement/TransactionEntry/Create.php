<?php

namespace App\Http\Livewire\Oims\OrderManagement\TransactionEntry;

use App\Interfaces\Oims\TransactionEntry\TransactionEntryInterface;
use App\Models\BranchReference;
use App\Models\Crm\CrmAncillaryDisplay;
use App\Models\CrmBooking;
use App\Models\CrmBookingConsignee;
use App\Models\CrmBookingShipper;
use App\Models\CrmCrateType;
use App\Models\CrmRateAirFreight;
use App\Models\UserDetails;
use App\Traits\Oims\TransactionEntry\TransactionEntryTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Create extends Component
{
    use TransactionEntryTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount',];


    public $book_id;
    public $bookings = [];
    public $cons;
    public $grandtotal = 0;
    public $total_quantity = 0;
    public $total_weight = 0;
    public $total_volume = 0;
    public $confirmation_message;
    public $delete_modal = false;
    public $cons_id;
    public $consacctype;
    public $ConsID;


    public function mount($id)
    {
        $this->book_id = $id;

        $this->bookings = CrmBooking::with('BookingShipper', 'BookingConsigneeHasManyBK', 'ShipperReferenceBK', 'ActivityReferenceBK', 'TimeslotReferenceBK', 'FinalStatusReferenceBK', 'BookingConsigneeHasManyBK')
            ->where('id', $this->book_id)->first();
        $this->select_cons_modal = true;
        $getUserBranch = UserDetails::where('user_id', Auth::user()->id)->first();
        $getBranch = BranchReference::where('id', $getUserBranch['branch_id'])->first();
        $this->branch = $getBranch['display'];
    }

    public $Cname;
    public function deleteConsignee(){
        $cons_name = CrmBookingConsignee::where('id', $this->getRequestSelected())->first();
        $this->Cname = $cons_name->name;
        $this->select_cons_modal = false;
        $this->confirmation_message = "Are you sure you want to delete this";
        $this->delete_modal = true;
        $this->cons_id = $this->getRequestSelected();
    }

    public function confirm(TransactionEntryInterface $transaction_entry_interface)
    {

            $response = $transaction_entry_interface->destroy($this->cons_id, '');
        if ($response['code'] == 200) {
            $this->sweetAlert('', $response['message']);
            $this->select_cons_modal = true;

        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'scan') {
            $this->scan_modal = true;
        } elseif ($action_type == 'view_remarks') {
            $this->view_remarks_modal = true;
        } elseif ($action_type == 'add_remarks') {
            $this->add_remarks_modal = true;
        } elseif ($action_type == 'override_history') {
            $this->override_history_modal = true;
        } elseif ($action_type == 'waybill_summary') {
            return redirect()->route('oims.order-management.transaction-entry.waybill-summary');
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'scan') {
            $this->scan_modal = false;
        } elseif ($action_type == 'view_remarks') {
            $this->view_remarks_modal = false;
        } elseif ($action_type == 'add_remarks') {
            $this->add_remarks_modal = false;
        } elseif ($action_type == 'override_history') {
            $this->override_history_modal = false;
        } elseif ($action_type == 'waybill_summary') {
        }
    }
   

    public function confirmationSubmitSelected(TransactionEntryInterface $transaction_entry_interface)
    {
        $response = $transaction_entry_interface->selectedValidation($this->getRequestSelected());

        if ($response['code'] == 200) {
            $getcons = CrmBookingConsignee::with('BookingTypeDetailsCS')->where('id', $response['result']['cons'])->first();
            $getship = CrmBookingShipper::where('booking_id', $getcons['booking_id'])->first();

            $this->ConsID = $getcons['id'];

            $this->consacctype = $getcons['account_type_id'];

            // $this->service = $getcons['account_type_id'];
            // $this->waybill_no = $getcons['account_type_id'];

            $this->trans_type = $getcons['BookingTypeDetailsCS']['booking_category'];
            $this->book_ref_no = $getcons['BookingTypeDetailsCS']['booking_reference_no'];
            $this->book_type = $getcons['BookingTypeDetailsCS']['booking_type_id'];
            // $this->origin = $getcons['account_type_id'];
            // $this->trans_port = $getcons['account_type_id'];

            // $this->branch = $getcons['BookingTypeDetailsCS']['booking_type_id'];
            $this->trans_date = date('m/d/Y');
            $this->transport_mode = $getcons['transposrt_mode_id'];
            // $this->destination = $getcons['BookingTypeDetailsCS']['booking_type_id'];

            $this->scust_no = $getship['customer_no'];
            $this->scname = $getship['name'];
            $this->scont_person = ($getship['account_type_id'] == 2 ? $getship['company_name'] : $getship['name']);
            $this->smobile = $getship['mobile_number'];
            $this->sadrs = $getship['address'];

            $this->ccust_no = $getcons['customer_no'];
            $this->ccname = $getcons['name'];
            $this->ccont_person = ($getcons['account_type_id'] == 2 ? $getcons['company_name'] : $getcons['name']);
            $this->cmobile = $getcons['mobile_number'];
            $this->cadrs = $getcons['address'];

            $this->des_goods = $getcons['description_goods'];
            // $this->types_goods = $getcons['description_goods'];
            // $this->com_type = $getcons['description_goods'];
            $this->dec_val = $getcons['declared_value'];
            $this->paymode = $getcons['mode_of_payment_id'];
            $this->serv_mode = $getcons['service_mode_id'];
            $this->charge_to = ($getcons['account_type_id'] == 2 ? $getcons['charge_to_id'] : $getcons['charge_to_name']);
            // $this->com_app_rate = $getcons['service_mode_id'];

            // $this->cadrs = $getcons['BookingCargoDetailsHasManyCD'][0][];

            $this->select_cons_modal = false;
        } else if ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function actionCompute()
    {
        if ($this->transport_mode == 1) {
            $chgwt = 0.0;
            $wt = 0.0;
            $qty = 0;
            $volume = 0;            
            $amountPerCbm = 0;
            $ancillaryCharge = 0;

            //BREAKDOWN CHARGES
            $awbFee = 0;
            $valuation = 0;
            $cod_charges = 0;
            $insurance = 0;
            $handling_fee = 0;
            $doc_fee = 0;
            $opa_fee = 0;
            $oda_fee = 0;
            $crating_fee = 0;
            $equipmentRental_fee = 0;
            $lashing_fee = 0;
            $manpower_fee = 0;
            $dangerousGoods_fee = 0;
            $trucking_fee = 0;
            $perishable_fee = 0;
            $packaging_fee = 0;

            $total_breakdown_charges = 0;
            $subtotal = 0;
            $grandtotal = 0;
            $hasOtherFees = false;
            $ancillary_display_id = 0;

            //AIR CARGOS
            if($this->air_cargo){

                if ($this->com_type == 1) {
                    $divisor = 3300;
                }
                if ($this->com_type == 2) {
                    $divisor = 2800;
                }
                
                foreach ($this->air_cargos as $i => $ac) {

                    $qty += $this->air_cargos[$i]['ac_qty'];

            if ($this->com_type == 1) {
                $divisor = 3300;
            }
            if ($this->com_type == 2) {
                $divisor = 2800;
            }

            foreach ($this->air_cargos as $i => $ac) {

                        $crateType = CrmRateCrating::where('crating_type_id', $this->air_cargos[$i]['ac_ct'])->first();
                        $amountPerCbm = $crateType->amount_per_cbm;
                        $ancillaryCharge = $crateType->ancillary_charge;
                        $crating_fee += $this->air_cargos[$i]['ac_cwt'] * $amountPerCbm;
                    }
                }
                $chgwtfinal = $chgwt > $wt ? $chgwt : $wt;

                $rate_air = CrmRateAirFreight::with('RateAirfreightHasMany')->where('id', $this->com_app_rate)->first();

                $rate_amt = 0.0;
                foreach ($rate_air->RateAirfreightHasMany as $i => $val) {
                    if ($val['origin_id'] == $this->origin && $val['destination_id'] == $this->destination) {

                        if ($chgwtfinal >= 0 && $chgwtfinal <= 5.99) {
                            $rate_amt = $val['amount_weight_1'];
                        }
                        if ($chgwtfinal >= 6 && $chgwtfinal <= 49.99) {
                            $rate_amt = $val['amount_weight_2'];
                        }
                        if ($chgwtfinal >= 50 && $chgwtfinal <= 249.99) {
                            $rate_amt = $val['amount_weight_3'];
                        }
                        if ($chgwtfinal >= 250 && $chgwtfinal <= 999.99) {
                            $rate_amt = $val['amount_weight_4'];
                        }
                        if ($chgwtfinal >= 1000) {
                            $rate_amt = $val['amount_weight_5'];
                        }
                    }
                }
                $weight_charge = number_format($chgwtfinal * $rate_amt, 2);
                $chgwt += ($this->air_cargos[$i]['ac_dl'] * $this->air_cargos[$i]['ac_dw'] * $this->air_cargos[$i]['ac_dh']) / $divisor;
                $wt += $this->air_cargos[$i]['ac_wt'];
                $this->air_cargos[$i]['ac_cwt'] = (($this->air_cargos[$i]['ac_dl'] * $this->air_cargos[$i]['ac_dw'] * $this->air_cargos[$i]['ac_dh']) / $divisor) >  $this->air_cargos[$i]['ac_wt'] ? ($this->air_cargos[$i]['ac_dl'] * $this->air_cargos[$i]['ac_dw'] * $this->air_cargos[$i]['ac_dh']) / $divisor : $this->air_cargos[$i]['ac_wt'];
            }
            // ac_cwt
            $chgwtfinal = $chgwt > $wt ? $chgwt : $wt;

            $rate_air = CrmRateAirFreight::with('RateAirfreightHasMany')->where('id', $this->com_app_rate)->first();

            // $this->weight_charge = $rate_air->

            $rate_amt = 0.0;
            foreach ($rate_air->RateAirfreightHasMany as $i => $val) {
                if ($val['origin_id'] == $this->origins && $val['destination_id'] == $this->destination) {

            }

                    if ($chgwtfinal >= 0 && $chgwtfinal <= 5.99) {
                        $rate_amt = $val['amount_weight_1'];
                    }
                    if ($chgwtfinal >= 6 && $chgwtfinal <= 49.99) {
                        $rate_amt = $val['amount_weight_2'];
                    }
                    if ($chgwtfinal >= 50 && $chgwtfinal <= 249.99) {
                        $rate_amt = $val['amount_weight_3'];
                    }
                    if ($chgwtfinal >= 250 && $chgwtfinal <= 999.99) {
                        $rate_amt = $val['amount_weight_4'];
                    }
                    if ($chgwtfinal >= 1000) {
                        $rate_amt = $val['amount_weight_5'];
                    }
                }
            }
            $this->weight_charge = number_format($chgwtfinal * $rate_amt);
            // $ancillary = CrmAncillaryDisplay::with('AncillaryDisplayDetails')->where('id', $rate_air->ancillary_charge_id)->get();
            // dd($ancillary);
            // dd( $chgwtfinal, $this->com_app_rate, $rate_air, $rate_air->RateAirfreightHasMany, $rate_amt, $this->origin);    


            $this->total_quantity = $qty;
            $this->total_weight = number_format($wt, 2);
            $this->total_volume = number_format($chgwt, 2);
            
            $this->weight_charge = $weight_charge;
            $this->awb_fee = number_format($awbFee, 2);
            $this->valuation = number_format($valuation, 2);
            $this->cod_charge = number_format($cod_charges, 2);
            $this->insurance = number_format($insurance, 2);
            $this->handling_fee = number_format($handling_fee, 2);
            $this->doc_fee = number_format($doc_fee, 2);
            $this->opa_fee = number_format($opa_fee, 2);
            $this->oda_fee = number_format($oda_fee, 2);
            $this->crating_fee = number_format($crating_fee, 2);
            $this->equipment_rental = number_format($equipmentRental_fee, 2);
            $this->lashing_fee = number_format($lashing_fee, 2);
            $this->manpower_fee = number_format($manpower_fee, 2);
            $this->dangerous_goods_fee = number_format($dangerousGoods_fee, 2);
            $this->trucking_fee = number_format($trucking_fee, 2);
            $this->perishable_fee = number_format($perishable_fee, 2);
            $this->packaging_fee = number_format($packaging_fee, 2);
            $this->other_fees = $hasOtherFees;

            
            $total_breakdown_charges = $awbFee + $valuation + $cod_charges + $insurance + $handling_fee + $doc_fee + $opa_fee + $oda_fee 
                + $crating_fee + $equipmentRental_fee + $lashing_fee + $manpower_fee + $dangerousGoods_fee + $trucking_fee + $perishable_fee + $packaging_fee;

            $subtotal = ($chgwtfinal * $rate_amt) + $total_breakdown_charges;
            $this->subtotal = number_format($subtotal, 2);
            $this->grandtotal = number_format($subtotal, 2);
       

        }
    }

    public function triggerair()
    {

        // foreach ($this->air_cargos as $i => $ac) {
        //     dd($ac['ac_wt'], $ac['ac_dl']);
        // }

    }



    public function isForCreating($id, $trans_mode, $is_true)
    {
        if ($trans_mode == 1) {
            if ($is_true == true) {
                foreach ($this->air_cargos as $i => $ac) {
                    if ($i == $id) {
                        $this->air_cargos[$i]['ac_fc'] = true;
                    }
                }
            } else {
                $this->air_cargos[$id]['ac_fc'] = false;
                $this->air_cargos[$id]['ac_ct'] = null;
            }
        }

        if ($trans_mode == 2) {
            if ($is_true == true) {
                foreach ($this->sea_cargos_is_lcl as $i => $sc) {
                    if ($i == $id) {
                        $this->sea_cargos_is_lcl[$i]['for_crating'] = true;
                    }
                }
            } else {
                $this->sea_cargos_is_lcl[$id]['for_crating'] = false;
                $this->air_cargos[$id]['ac_ct'] = null;
            }
        }

        if ($trans_mode == 3) {
            if ($is_true == true) {
                foreach ($this->sea_cargos_is_rcl as $i => $sc) {
                    if ($i == $id) {
                        $this->sea_cargos_is_rcl[$i]['for_crating'] = true;
                    }
                }
            } else {
                $this->sea_cargos_is_rcl[$id]['for_crating'] = false;
                $this->air_cargos[$id]['ac_ct'] = null;
            }
        }
    }

    public function addair_cargos()
    {
        $this->air_cargos[] = [
            'id' => null,
            'ac_qty' => null,
            'ac_wt' => null,
            'ac_dl' => null,
            'ac_dw' => null,
            'ac_dh' => null,
            'ac_um' => null,
            'ac_umt' => null,
            'ac_tp' => null,
            'ac_fc' => null,
            'ac_ct' => null,
            'ac_cwt' => null,
            'is_deleted' => false,
        ];
    }

    public function removeair_cargos(array $data)
    {
        if (count(collect($this->air_cargos)->where('is_deleted', false)) > 1) {
            if ($this->air_cargos[$data["a"]]['id']) {
                $this->air_cargos[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->air_cargos[$data["a"]]);
            }

            array_values($this->air_cargos);
        }
    }

    public function addair_pouches()
    {
        $this->air_pouches[] = [
            'id' => null,
            'ap_qty' => null,
            'ap_ps' => null,
            'ap_amount' => null,
            'is_deleted' => false,
        ];
    }

    public function removeair_pouches(array $data)
    {
        if (count(collect($this->air_pouches)->where('is_deleted', false)) > 1) {
            if ($this->air_pouches[$data["a"]]['id']) {
                $this->air_pouches[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->air_pouches[$data["a"]]);
            }

            array_values($this->air_pouches);
        }
    }

    public function addair_boxes()
    {
        $this->air_boxes[] = [
            'id' => null,
            'ab_qty' => null,
            'ab_bs' => null,
            'ab_weight' => null,
            'ab_amount' => null,
            'is_deleted' => false,
        ];
    }

    public function removeair_boxes(array $data)
    {
        if (count(collect($this->air_boxes)->where('is_deleted', false)) > 1) {
            if ($this->air_boxes[$data["a"]]['id']) {
                $this->air_boxes[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->air_boxes[$data["a"]]);
            }

            array_values($this->air_boxes);
        }
    }

    public function addSeaCargosLcl()
    {
        $this->sea_cargos_is_lcl[] = [
            'id' => null,
            'qty' => null,
            'wt' => null,
            'dimension_l' => null,
            'dimension_w' => null,
            'dimension_h' => null,
            'unit_of_measurement' => null,
            'measurement_type' => null,
            'type_of_packaging' => null,
            'for_crating' => null,
            'crating_type' => null,
            'cbm' => null,
            'is_deleted' => false,
        ];
    }

    public function removeSeaCargos(array $data)
    {
        if (count(collect($this->sea_cargos_is_lcl)->where('is_deleted', false)) > 1) {
            if ($this->sea_cargos_is_lcl[$data["i"]]['id']) {
                $this->sea_cargos_is_lcl[$data["i"]]['is_deleted'] = true;
            } else {
                unset($this->sea_cargos_is_lcl[$data["i"]]);
            }

            array_values($this->sea_cargos_is_lcl);
        }
    }

    public function addSeaCargosFcl()
    {
        $this->sea_cargos_is_fcl[] = [
            'id' => null,
            'qty' => null,
            'container' => null,
            'is_deleted' => false,
        ];
    }

    public function removeSeaCargosFcl(array $data)
    {
        if (count(collect($this->sea_cargos_is_fcl)->where('is_deleted', false)) > 1) {
            if ($this->sea_cargos_is_fcl[$data["i"]]['id']) {
                $this->sea_cargos_is_fcl[$data["i"]]['is_deleted'] = true;
            } else {
                unset($this->sea_cargos_is_fcl[$data["i"]]);
            }

            array_values($this->sea_cargos_is_fcl);
        }
    }

    public function addSeaCargosRcl()
    {
        $this->sea_cargos_is_rcl[] = [
            'id' => null,
            'qty' => null,
            'wt' => null,
            'dimension_l' => null,
            'dimension_w' => null,
            'dimension_h' => null,
            'unit_of_measurement' => null,
            'measurement_type' => null,
            'amount' => null,
            'for_crating' => null,
            'crating_type' => null,
            'cbm' => null,
            'is_deleted' => false,
        ];
    }

    public function removeSeaCargosRcl(array $data)
    {
        if (count(collect($this->sea_cargos_is_rcl)->where('is_deleted', false)) > 1) {
            if ($this->sea_cargos_is_rcl[$data["i"]]['id']) {
                $this->sea_cargos_is_rcl[$data["i"]]['is_deleted'] = true;
            } else {
                unset($this->sea_cargos_is_rcl[$data["i"]]);
            }

            array_values($this->sea_cargos_is_rcl);
        }
    }

    public function addSeaBoxes()
    {
        $this->sea_boxes[] = [
            'id' => null,
            'qty' => null,
            'box_size' => null,
            'weight' => null,
            'amount' => null,
            'is_deleted' => false,
        ];
    }

    public function removeSeaBoxes(array $data)
    {
        if (count(collect($this->sea_boxes)->where('is_deleted', false)) > 1) {
            if ($this->sea_boxes[$data["i"]]['id']) {
                $this->sea_boxes[$data["i"]]['is_deleted'] = true;
            } else {
                unset($this->sea_boxes[$data["i"]]);
            }

            array_values($this->sea_boxes);
        }
    }

    public function addLandsShippersBox()
    {
        $this->lands_shippers_box[] = [
            'id' => null,
            'total_qty' => null,
            'total_weight' => null,
            'dimension_l' => null,
            'dimension_w' => null,
            'dimension_h' => null,
            'cbm' => null,
            'cwt' => null,
            'land_rate' => null,
            'rate_amount' => null,
            'is_deleted' => false,
        ];
    }

    public function removeLandsShippersBox(array $data)
    {
        if (count(collect($this->lands_shippers_box)->where('is_deleted', false)) > 1) {
            if ($this->lands_shippers_box[$data["i"]]['id']) {
                $this->lands_shippers_box[$data["i"]]['is_deleted'] = true;
            } else {
                unset($this->lands_shippers_box[$data["i"]]);
            }

            array_values($this->lands_shippers_box);
        }
    }

    public function addLandsPouches()
    {
        $this->lands_pouches[] = [
            'id' => null,
            'size' => null,
            'weight_per_pouch' => null,
            'cbm' => null,
            'total_amount' => null,
            'is_deleted' => false,
        ];
    }

    public function removeLandsPouches(array $data)
    {
        if (count(collect($this->lands_pouches)->where('is_deleted', false)) > 1) {
            if ($this->lands_pouches[$data["i"]]['id']) {
                $this->lands_pouches[$data["i"]]['is_deleted'] = true;
            } else {
                unset($this->lands_pouches[$data["i"]]);
            }

            array_values($this->lands_pouches);
        }
    }

    public function addLandsBoxes()
    {
        $this->lands_boxes[] = [
            'id' => null,
            'size' => null,
            'weight_per_box' => null,
            'cbm' => null,
            'total_amount' => null,
            'is_deleted' => false,
        ];
    }

    public function removeLandsBoxes(array $data)
    {
        if (count(collect($this->lands_boxes)->where('is_deleted', false)) > 1) {
            if ($this->lands_boxes[$data["i"]]['id']) {
                $this->lands_boxes[$data["i"]]['is_deleted'] = true;
            } else {
                unset($this->lands_boxes[$data["i"]]);
            }

            array_values($this->lands_boxes);
        }
    }

    public function addLandsFtl()
    {
        $this->lands_ftl[] = [
            'id' => null,
            'quantity' => null,
            'truck_type' => null,
            'unit_price' => null,
            'amount' => null,
            'is_deleted' => false,
        ];
    }

    public function removeLandsFtl(array $data)
    {
        if (count(collect($this->lands_ftl)->where('is_deleted', false)) > 1) {
            if ($this->lands_ftl[$data["i"]]['id']) {
                $this->lands_ftl[$data["i"]]['is_deleted'] = true;
            } else {
                unset($this->lands_ftl[$data["i"]]);
            }

            array_values($this->lands_ftl);
        }
    }
    public $confirmation_modal;


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.transaction-entry.create', 'create');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(TransactionEntryInterface $transaction_entry_interface)
    {
        $response = $transaction_entry_interface->createValidation($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(TransactionEntryInterface $transaction_entry_interface)
    {
        $response = $transaction_entry_interface->create($this->getRequest(), $this->ConsID);
        if ($response['code'] == 200) {

            $this->resetForm();
            // $this->emitTo('oims.order-management.transaction-entry.create', 'close_modal', 'create');
            $this->emitTo('oims.order-management.transaction-entry.create', 'create');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        if (!$this->air_cargos) {
            $this->air_cargos[] = [
                'id' => null,
                'ac_qty' => null,
                'ac_wt' => null,
                'ac_dl' => null,
                'ac_dw' => null,
                'ac_dh' => null,
                'ac_um' => null,
                'ac_umt' => null,
                'ac_tp' => null,
                'ac_fc' => null,
                'ac_ct' => null,
                'ac_cwt' => null,
                'is_deleted' => false,
            ];
        }

        if (!$this->air_pouches) {
            $this->air_pouches[] = [
                'id' => null,
                'ap_qty' => null,
                'ap_ps' => null,
                'ap_amount' => null,
                'is_deleted' => false,
            ];
        }

        if (!$this->air_boxes) {
            $this->air_boxes[] = [
                'id' => null,
                'ab_qty' => null,
                'ab_bs' => null,
                'ab_weight' => null,
                'ab_amount' => null,
                'is_deleted' => false,
            ];
        }

        if (!$this->sea_cargos_is_lcl) {
            $this->sea_cargos_is_lcl[] = [
                'id' => null,
                'qty' => null,
                'wt' => null,
                'dimension_l' => null,
                'dimension_w' => null,
                'dimension_h' => null,
                'unit_of_measurement' => null,
                'measurement_type' => null,
                'type_of_packaging' => null,
                'for_crating' => null,
                'crating_type' => null,
                'cbm' => null,
                'is_deleted' => false,
            ];
        }

        if (!$this->sea_cargos_is_fcl) {
            $this->sea_cargos_is_fcl[] = [
                'id' => null,
                'qty' => null,
                'container' => null,
                'is_deleted' => false,
            ];
        }

        if (!$this->sea_cargos_is_rcl) {
            $this->sea_cargos_is_rcl[] = [
                'id' => null,
                'qty' => null,
                'wt' => null,
                'dimension_l' => null,
                'dimension_w' => null,
                'dimension_h' => null,
                'unit_of_measurement' => null,
                'measurement_type' => null,
                'amount' => null,
                'for_crating' => null,
                'crating_type' => null,
                'cbm' => null,
                'is_deleted' => false,
            ];
        }

        if (!$this->sea_boxes) {
            $this->sea_boxes[] = [
                'id' => null,
                'qty' => null,
                'box_size' => null,
                'weight' => null,
                'amount' => null,
                'is_deleted' => false,
            ];
        }

        if (!$this->lands_shippers_box) {
            $this->lands_shippers_box[] = [
                'id' => null,
                'total_qty' => null,
                'total_weight' => null,
                'dimension_l' => null,
                'dimension_w' => null,
                'dimension_h' => null,
                'cbm' => null,
                'cwt' => null,
                'land_rate' => null,
                'rate_amount' => null,
                'is_deleted' => false,
            ];
        }

        if (!$this->lands_pouches) {
            $this->lands_pouches[] = [
                'id' => null,
                'size' => null,
                'weight_per_pouch' => null,
                'cbm' => null,
                'total_amount' => null,
                'is_deleted' => false,
            ];
        }

        if (!$this->lands_boxes) {
            $this->lands_boxes[] = [
                'id' => null,
                'size' => null,
                'weight_per_box' => null,
                'cbm' => null,
                'total_amount' => null,
                'is_deleted' => false,
            ];
        }

        if (!$this->lands_ftl) {
            $this->lands_ftl[] = [
                'id' => null,
                'quantity' => null,
                'truck_type' => null,
                'unit_price' => null,
                'amount' => null,
                'is_deleted' => false,
            ];
        }


        return view('livewire.oims.order-management.transaction-entry.create');
    }
}
