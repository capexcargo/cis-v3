<?php

namespace App\Http\Livewire\Oims\OrderManagement\TransactionEntry;

use App\Traits\Oims\TransactionEntry\TransactionEntryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class ViewRemarks extends Component
{

    use TransactionEntryTrait, WithPagination, PopUpMessagesTrait;

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add_remarks') {
            $this->add_remarks_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'add_remarks') {
            $this->add_remarks_modal = false;
        } 
    }
    public function render()
    {
        return view('livewire.oims.order-management.transaction-entry.view-remarks');
    }
}
