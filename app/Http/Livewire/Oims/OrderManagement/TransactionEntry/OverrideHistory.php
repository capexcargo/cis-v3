<?php

namespace App\Http\Livewire\Oims\OrderManagement\TransactionEntry;

use App\Traits\Oims\TransactionEntry\TransactionEntryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class OverrideHistory extends Component
{
    use TransactionEntryTrait, WithPagination, PopUpMessagesTrait;

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'commodity_applicable_rate') {
            $this->commodity_applicable_rate_modal = true;
        }elseif ($action_type == 'service_mode') {
            $this->service_mode_modal = true;
        }elseif ($action_type == 'shipper_contact_person') {
            $this->shipper_contact_person_modal = true;
        }elseif ($action_type == 'consignee_mobile_number') {
            $this->consignee_mobile_number_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'commodity_applicable_rate') {
            $this->commodity_applicable_rate_modal = false;
        }if ($action_type == 'service_mode') {
            $this->service_mode_modal = false;
        }if ($action_type == 'shipper_contact_person') {
            $this->shipper_contact_person_modal = false;
        }if ($action_type == 'consignee_mobile_number') {
            $this->consignee_mobile_number_modal = false;
        } 
    }
    public function render()
    {
        return view('livewire.oims.order-management.transaction-entry.override-history');
    }
}
