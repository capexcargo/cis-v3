<?php

namespace App\Http\Livewire\Oims\OrderManagement\TransactionEntry;

use Livewire\Component;

class Scan extends Component
{
    public function render()
    {
        return view('livewire.oims.order-management.transaction-entry.scan');
    }
}
