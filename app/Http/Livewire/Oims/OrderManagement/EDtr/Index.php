<?php

namespace App\Http\Livewire\Oims\OrderManagement\EDtr;

use App\Interfaces\Oims\EDtr\EDtrInterface;
use App\Models\OimsEdtr;
use App\Models\OimsEdtrDetails;
use App\Traits\Oims\OrderManagement\EDtr\EDtrTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use EDtrTrait, WithPagination, PopUpMessagesTrait;

    public $status_header_cards = [];
    public $stats;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public function action(array $data, $action_type)
    {
        // $this->action_type = $action_type;
        // if ($action_type == 'add') {
        //     $this->create_modal = true;
        // }elseif ($action_type == 'edit') {
        //     $this->emitTo('oims.order-management.team-route-assignment.area-mgmt.edit', 'edit', $data['id']);
        //     $this->area_id = $data['id'];
        //     $this->edit_modal = true;
        // }else if ($action_type == 'update_status') {
        //     if ($data['status'] == 1) {
        //         $this->reactivate_modal = true;
        //         $this->area_id = $data['id'];
        //         return;
        //     }
        //     $this->deactivate_modal = true;
        //     $this->area_id = $data['id'];
        // }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'edtrr') {
            $this->edtrr_modal = false;
        } else if ($action_type == 'pua') {
            $this->pua_modal = false;
        } else if ($action_type == 'da') {
            $this->da_modal = false;
        } else if ($action_type == 'ac') {
            $this->ac_modal = false;
        }
        // elseif ($action_type == 'update_status') {
        //     $this->reactivate_modal = false;
        //     $this->deactivate_modal = false;
        // } 
    }

    public $getEDtrs = [];
    public $Edtrid;

    public function actionedtrr(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'edtrr') {
            $this->edtrr_modal = true;
        }

        $this->Edtrid = $data['id'];

        $getedtrRef = OimsEdtr::where('id', $this->Edtrid)->first();

        $this->getEDtrs = OimsEdtr::with('Traref', 'cbref', 'edtrBranch')->with(['Traref' => function ($query) {
            $query->with('teamRouteAssignmentReference', 'teamIdReference', 'teamPlateReference', 'teamDriverReference', 'teamChecker1Reference', 'teamChecker2Reference');
        }])
            ->with(['edtrDetails' => function ($query) {
                // $query->where('edtr_item_type', 1);
                $query->with(['edtrbook' => function ($query) {
                    $query->with('BookShipper', 'FinalStatusReferenceBK');
                }]);
                $query->with(['edtrtransaction' => function ($query) {
                    $query->with('CrmBookingReference', 'OriginReference', 'DestinationReference', 'OimsBranchReference', 'ShipperReference', 'ConsigneeReference');
                }]);
            }])->where('reference_no', $getedtrRef->reference_no)->get();

        // dd($this->getEDtrs);

        $this->rmrefno = $this->getEDtrs[0]['reference_no'];
        $this->rmcreated = date('m/d/Y', strtotime($this->getEDtrs[0]['dispatch_date']));
        $this->rmdisp = date('m/d/Y', strtotime($this->getEDtrs[0]['dispatch_date']));
        $this->rmbrnch = $this->getEDtrs[0]['edtrBranch']['name'];
        $this->rmplate = $this->getEDtrs[0]['Traref']['teamPlateReference']['plate_no'];

        $this->rmteam = $this->getEDtrs[0]['Traref']['teamIdReference']['name'];
        $this->rmdrv = $this->getEDtrs[0]['Traref']['teamDriverReference']['name'];
        $this->rmchk1 = $this->getEDtrs[0]['Traref']['teamChecker1Reference']['name'] ?? '-';
        $this->rmchk2 = $this->getEDtrs[0]['Traref']['teamChecker2Reference']['name'] ?? '-';

        // $this->getbookid = $getEDtr['reference_no']

        // $getBooking[] = Oims;
    }

    public $pickups;

    public function actionpua(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'pua') {
            $this->pua_modal = true;
        }
    }

    public function actionda(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'da') {
            $this->da_modal = true;
        }
    }

    public function actionac(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'ac') {
            $this->ac_modal = true;
        }
    }

    public $latestDelivery;

    public function actionpv(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'pv') {
            $this->pv_modal = true;
        }
        $this->latestPickup = '';
        $this->latestDelivery = '';
        $this->Edtrid = $data['id'];

        $getedtrRef = OimsEdtr::where('id', $this->Edtrid)->first();

        // $getEdtrDet = OimsEdtrDetails::where('edtr_id', $this->Edtrid)->where('edtr_item_type', 1)->latest('created_at')->first();
        // if ($getEdtrDet != null) {
        //     $this->latestPickup = date('m/d/Y H:i:s', strtotime($getEdtrDet['created_at']));
        // }
        // $getEdtrDetD = OimsEdtrDetails::where('edtr_id', $this->Edtrid)->where('edtr_item_type', 2)->latest('created_at')->first();
        // if ($getEdtrDetD != null) {
        //     $this->latestDelivery = date('m/d/Y H:i:s', strtotime($getEdtrDetD['created_at']));
        // }

        $this->getEDtrs = OimsEdtr::with('Traref', 'cbref', 'edtrBranch')->with(['Traref' => function ($query) {
            $query->with('teamRouteAssignmentReference', 'teamIdReference', 'teamPlateReference', 'teamDriverReference', 'teamChecker1Reference', 'teamChecker2Reference');
        }])
            ->with(['edtrDetails' => function ($query) {
                $query->with(['edtrbook' => function ($query) {
                    $query->with('BookShipper', 'FinalStatusReferenceBK', 'ActivityReferenceBK');
                }]);
                $query->with(['edtrtransaction' => function ($query) {
                    $query->with('CrmBookingReference', 'OriginReference', 'DestinationReference', 'OimsBranchReference', 'ShipperReference', 'ConsigneeReference');
                }]);
            }])->where('reference_no', $getedtrRef->reference_no)->get();

        $this->rmrefno = $this->getEDtrs[0]['reference_no'];
        $this->rmcreated = date('m/d/Y', strtotime($this->getEDtrs[0]['dispatch_date']));
        $this->rmdisp = date('m/d/Y', strtotime($this->getEDtrs[0]['dispatch_date']));
        $this->rmbrnch = $this->getEDtrs[0]['edtrBranch']['name'];
        $this->rmplate = $this->getEDtrs[0]['Traref']['teamPlateReference']['plate_no'];

        $this->rmteam = $this->getEDtrs[0]['Traref']['teamIdReference']['name'];
        $this->rmdrv = $this->getEDtrs[0]['Traref']['teamDriverReference']['name'];
        $this->rmchk1 = $this->getEDtrs[0]['Traref']['teamChecker1Reference']['name'] ?? '-';
        $this->rmchk2 = $this->getEDtrs[0]['Traref']['teamChecker2Reference']['name'] ?? '-';

        foreach ($this->getEDtrs as $i => $getEDtr) {
            foreach ($this->getEDtrs[$i]['edtrDetails'] as $o => $getEDtrDet) {
                if ($this->getEDtrs[$i]['edtrDetails'][$o]['edtr_item_type'] == 1) {
                    $getEdtrDet = OimsEdtrDetails::where('edtr_id', $this->getEDtrs[$i]['edtrDetails'][$o]['edtr_id'])->latest('created_at')->first();
                    // dd($getEdtrDet);
                    if ($getEdtrDet != null) {
                        $this->latestPickup = date('m/d/Y H:i:s', strtotime($getEdtrDet['created_at']));
                    }
                } elseif ($this->getEDtrs[$i]['edtrDetails'][$o]['edtr_item_type'] == 2) {
                    $getEdtrDetD = OimsEdtrDetails::where('edtr_id', $this->getEDtrs[$i]['edtrDetails'][$o]['edtr_id'])->latest('created_at')->first();
                    // dd($getEdtrDetD);
                    if ($getEdtrDetD != null) {
                        $this->latestDelivery = date('m/d/Y H:i:s', strtotime($getEdtrDetD['created_at']));
                    }
                }
            }
        }

    }

    // public function actionpvs(array $data, $action_type)
    // {
    //     $this->action_type = $action_type;
    //     if ($action_type == 'pvs') {


    //     $this->emitTo('oims.order-management.e-dtr.edtr-print', 'pvs', ['id' => $data['id']]);


    //     $this->Edtrid = $data['id'];
    //     }

    // }
    public $refno;
    public $disdate;
    public $brnch;
    public $team;
    public $driver;
    public $chk;
    public $plate;

    public function search(EDtrInterface $EDtr_interface)
    {
        $this->search_request = [
            'refno' => $this->refno,
            // 'waybill' => $this->waybill,
            'disdate' => $this->disdate,
            'brnch' => $this->brnch,
            'team' => $this->team,
            'driver' => $this->driver,
            'chk' => $this->chk,
            'plate' => $this->plate,
        ];
    }
    public function render(EDtrInterface $EDtr_interface)
    {
        // $this->mount();
        // $this->loadStatusHeaderCards();



        // if(isset($this->selecteds)){
        //     $this->selCount = count($this->selecteds);
        // }

        $request = [
            'paginate' => $this->paginate,
            // 'branch_id' => $this->branch_id,
            // 'brnch' => $this->brnch,
            // 'stats' => $this->stats,
        ];

        $response = $EDtr_interface->index($request, $this->search_request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'edtr_lists' => [],
                ];
        }
        return view('livewire.oims.order-management.e-dtr.index', [
            'edtr_lists' => $response['result']['edtr_lists']
        ]);
    }
}
