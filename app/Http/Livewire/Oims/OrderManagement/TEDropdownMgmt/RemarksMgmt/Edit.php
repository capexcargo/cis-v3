<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\RemarksMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\RemarksMgmt\RemarksMgmtInterface;
use App\Models\OimsRemarks;
use App\Traits\Oims\TEDropdownMgmt\RemarksMgmt\RemarksMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use RemarksMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $remarks = [];

    public function mount($id)
    {
        $this->resetForm();
        $this->remarks = OimsRemarks::findOrFail($id);

        $this->name = $this->remarks->name;
        $this->t_remarks = $this->remarks->type_of_remarks;
        $this->transact = $this->remarks->transaction_stage_id;
        $this->remarks_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.remarks-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(RemarksMgmtInterface $remarks_mgmt_interface)
    {

        $response = $remarks_mgmt_interface->updateValidation($this->getRequest(),$this->remarks_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(RemarksMgmtInterface $remarks_mgmt_interface)
    {
        $response = $remarks_mgmt_interface->update($this->getRequest(), $this->remarks_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.remarks-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.remarks-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.remarks-mgmt.edit');
    }
}
