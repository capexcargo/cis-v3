<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\OdaopaMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\OdaopaMgmt\OdaopaMgmtInterface;
use App\Models\OimsOdaOpaReference;
use App\Traits\Oims\TEDropdownMgmt\OdaopaMgmt\OdaopaMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use OdaopaMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $odaopas = [];

    public function mount($id)
    {
        $this->resetForm();
        $this->odaopas = OimsOdaOpaReference::findOrFail($id);

        $this->name = $this->odaopas->name;
        $this->oda_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.odaopa-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(OdaopaMgmtInterface $odaopa_mgmt_interface)
    {

        $response = $odaopa_mgmt_interface->updateValidation($this->getRequest(),$this->oda_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(OdaopaMgmtInterface $odaopa_mgmt_interface)
    {
        $response = $odaopa_mgmt_interface->update($this->getRequest(), $this->oda_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.odaopa-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.odaopa-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.odaopa-mgmt.edit');
    }
}
