<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\BranchMgmt;

use App\Models\OimsBranchReference;
use App\Traits\Oims\TEDropdownMgmt\BranchMgmt\BranchMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class View extends Component
{
    use BranchMgmtTrait, WithPagination, PopUpMessagesTrait;
    public $branch = [];
    public $map;
    public $latitude;
    public $longitude;


    public function mount($id)
    {
        $this->branch = OimsBranchReference::findOrFail($id);

        $this->map = $this->branch->name;
        $this->latitude = $this->branch->latitude;
        $this->longitude = $this->branch->longitude;
        // dd($this->map);

    }

    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.branch-mgmt.view');
    }
}
