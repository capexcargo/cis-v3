<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\BranchMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\BranchMgmt\BranchMgmtInterface;
use App\Traits\Oims\TEDropdownMgmt\BranchMgmt\BranchMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use BranchMgmtTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $tellnos = [];
    public $contacts = [];

    protected $listeners = ['get_address_name' => 'getAddressName', 'get_lat' => 'getLat', 'get_long' => 'getLong'];


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.branch-mgmt.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(BranchMgmtInterface $branch_mgmt_interface)
    {
        $response = $branch_mgmt_interface->createValidation($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
            $this->resetErrorBag();
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    // dd($response['result']);
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(BranchMgmtInterface $branch_mgmt_interface)
    {
        $response = $branch_mgmt_interface->create($this->getRequest());
        if ($response['code'] == 200) {

            $this->resetForm();
            $this->resetErrorBag();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.branch-mgmt.index', 'close_modal', 'create');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.branch-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function addTellNo()
    {
        $this->tellnos[] = [
            'id' => null,
            'tell_no' => null,
            'is_deleted' => false,

        ];
    }

    public function removeTellNo(array $data)
    {
        if (count(collect($this->tellnos)->where('is_deleted', false)) > 1) {
            if ($this->tellnos[$data["a"]]['id']) {
                $this->tellnos[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->tellnos[$data["a"]]);
            }

            array_values($this->tellnos);
        }
    }

    public function addContactNo()
    {
        $this->contacts[] = [
            'id' => null,
            'cont' => null,
            'is_deleted' => false,

        ];
    }

    public function removeContactNo(array $data)
    {
        if (count(collect($this->contacts)->where('is_deleted', false)) > 1) {
            if ($this->contacts[$data["a"]]['id']) {
                $this->contacts[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->contacts[$data["a"]]);
            }

            array_values($this->contacts);
        }
    }

    public function render()
    {
        if (!$this->tellnos) {
            $this->tellnos[] = [
                'id' => null,
                'tell_no' => null,
                'is_deleted' => false,

            ];
        }

        if (!$this->contacts) {
            $this->contacts[] = [
                'id' => null,
                'cont' => null,
                'is_deleted' => false,

            ];
        }

        return view('livewire.oims.order-management.t-e-dropdown-mgmt.branch-mgmt.create');
    }
}
