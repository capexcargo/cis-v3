<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\BranchMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\BranchMgmt\BranchMgmtInterface;
use App\Models\BranchReference;
use App\Models\OimsBranchReference;
use App\Traits\Oims\TEDropdownMgmt\BranchMgmt\BranchMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use BranchMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount','get_address_name' => 'getAddressName', 'get_lat' => 'getLat', 'get_long' => 'getLong'];

    public $branch = [];
    public $branch_id;
    public $tellnos = [];
    public $contacts = [];

    public $countarr;
    public $nearest;
    public $min;

    public $countarr2;
    public $nearest2;
    public $min2;


    public function mount($id)
    {
        $this->resetForm();
        $this->branch = OimsBranchReference::with('OBRTelephoneHasMany', 'OBRContactHasMany')->findOrFail($id);

        $this->r_name = $this->branch->region;
        $this->b_name = $this->branch->name;
        $this->adrs = $this->branch->address;
        $this->lat = $this->branch->latitude;
        $this->long = $this->branch->longitude;
        $this->orig = $this->branch->origin_destination_id;
        // $this->r_name = $this->branch->OBRTelephoneHasMany->telephone;
        // $this->r_name = $this->branch->OBRContactHasMany->contact_no;
        $this->branch_id = $id;

        foreach ($this->branch->OBRTelephoneHasMany as $a => $tels) {
            $this->tellnos[$a] = [
                'id' => $tels->id,
                'tell_no' => $tels->telephone,
                'is_deleted' => false,
            ];
        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->tellnos as $a => $tellno) {

            $getkeyarray[] = $a;
            $countarray += ($this->tellnos[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;

        // ------------------------------------------------------------------------------------------------------

        foreach ($this->branch->OBRContactHasMany as $b => $conts) {
            $this->contacts[$b] = [
                'id' => $conts->id,
                'cont' => $conts->contact_no,
                'is_deleted' => false,
            ];
        }
        $countarray2 = 0;
        $getkeyarray2 = [];
        foreach ($this->contacts as $b => $tellno) {

            $getkeyarray2[] = $b;
            $countarray2 += ($this->contacts[$b]['is_deleted'] == false ? 1 : 0);
        }
        $this->min2 = min($getkeyarray2);
        $this->nearest2 = max($getkeyarray2);
        $this->countarr2 = $countarray2;
    }

    public function addTellNo()
    {
        $this->tellnos[] = [
            'id' => null,
            'tell_no' => null,
            'is_deleted' => false,
        ];
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->tellnos as $a => $position) {

            if ($this->tellnos[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }
            $countarray += ($this->tellnos[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    public function removeTellNo(array $data)
    {
        if (count(collect($this->tellnos)->where('is_deleted', false)) > 1) {
            if ($this->tellnos[$data["a"]]['id']) {
                $this->tellnos[$data["a"]]['is_deleted'] = true;
                // unset($this->tellnos[$data["a"]]);
            } else {
                unset($this->tellnos[$data["a"]]);
            }
            array_values($this->tellnos);
        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->tellnos as $a => $position) {

            if ($this->tellnos[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }
            $countarray += ($this->tellnos[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    public function addContactNo()
    {
        $this->contacts[] = [
            'id' => null,
            'cont' => null,
            'is_deleted' => false,
        ];
        $countarray2 = 0;
        $getkeyarray2 = [];
        foreach ($this->contacts as $b => $position) {

            if ($this->contacts[$b]['is_deleted'] == false) {
                $getkeyarray2[] = $b;
            }
            $countarray2 += ($this->contacts[$b]['is_deleted'] == false ? 1 : 0);
        }
        $this->min2 = min($getkeyarray2);
        $this->nearest2 = max($getkeyarray2);
        $this->countarr2 = $countarray2;
    }


    public function removeContactNo(array $data)
    {
        if (count(collect($this->contacts)->where('is_deleted', false)) > 1) {
            if ($this->contacts[$data["a"]]['id']) {
                $this->contacts[$data["a"]]['is_deleted'] = true;
                // unset($this->contacts[$data["a"]]);
            } else {
                unset($this->contacts[$data["a"]]);
            }
            array_values($this->contacts);
        }
        $countarray2 = 0;
        $getkeyarray2 = [];
        foreach ($this->contacts as $b => $contact) {

            if ($this->contacts[$b]['is_deleted'] == false) {
                $getkeyarray2[] = $b;
            }
            $countarray2 += ($this->contacts[$b]['is_deleted'] == false ? 1 : 0);
        }
        $this->min2 = min($getkeyarray2);
        $this->nearest2 = max($getkeyarray2);
        $this->countarr2 = $countarray2;
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.branch-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(BranchMgmtInterface $branch_mgmt_interface)
    {

        $response = $branch_mgmt_interface->updateValidation($this->getRequest(), $this->branch_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(BranchMgmtInterface $branch_mgmt_interface)
    {
        $response = $branch_mgmt_interface->update($this->getRequest(), $this->branch_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.branch-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.branch-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.branch-mgmt.edit');
    }
}
