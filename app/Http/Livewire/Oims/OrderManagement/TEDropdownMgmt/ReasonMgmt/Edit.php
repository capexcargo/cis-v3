<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\ReasonMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ReasonMgmt\ReasonMgmtInterface;
use App\Models\OimsReasonReference;
use App\Traits\Oims\TEDropdownMgmt\ReasonMgmt\ReasonMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use ReasonMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $reasons = [];

    public function mount($id)
    {
        $this->resetForm();
        $this->reasons = OimsReasonReference::findOrFail($id);

        $this->name = $this->reasons->name;
        $this->t_reason = $this->reasons->type_of_reason;
        $this->app = $this->reasons->application_id;
        $this->reason_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.reason-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(ReasonMgmtInterface $reason_mgmt_interface)
    {

        $response = $reason_mgmt_interface->updateValidation($this->getRequest(),$this->reason_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(ReasonMgmtInterface $reason_mgmt_interface)
    {
        $response = $reason_mgmt_interface->update($this->getRequest(), $this->reason_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.reason-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.reason-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.reason-mgmt.edit');
    }
}
