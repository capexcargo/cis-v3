<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\ServiceMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ServiceMgmt\ServiceMgmtInterface;
use App\Models\OimsServiceModeReference;
use App\Traits\Oims\TEDropdownMgmt\ServiceMgmt\ServiceMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use ServiceMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $services = [];

    public function mount($id)
    {
        $this->resetForm();
        $this->services = OimsServiceModeReference::findOrFail($id);

        $this->name = $this->services->name;
        $this->code = $this->services->code;
        $this->t_mode = $this->services->transport_mode_id;
        $this->service_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.service-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(ServiceMgmtInterface $service_mgmt_interface)
    {

        $response = $service_mgmt_interface->updateValidation($this->getRequest(),$this->service_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(ServiceMgmtInterface $service_mgmt_interface)
    {
        $response = $service_mgmt_interface->update($this->getRequest(), $this->service_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.service-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.service-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.service-mgmt.edit');
    }
}
