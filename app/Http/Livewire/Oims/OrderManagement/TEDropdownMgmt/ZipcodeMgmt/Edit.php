<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\ZipcodeMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ZipcodeMgmt\ZipcodeMgmtInterface;
use App\Models\OimsZipcodeReference;
use App\Traits\Oims\TEDropdownMgmt\ZipcodeMgmt\ZipcodeMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use ZipcodeMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $zipcodes = [];

    public function mount($id)
    {
        $this->resetForm();
        $this->zipcodes = OimsZipcodeReference::findOrFail($id);

        $this->name = $this->zipcodes->name;
        $this->zipcode_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.zipcode-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(ZipcodeMgmtInterface $zipcode_mgmt_interface)
    {

        $response = $zipcode_mgmt_interface->updateValidation($this->getRequest(),$this->zipcode_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(ZipcodeMgmtInterface $zipcode_mgmt_interface)
    {
        $response = $zipcode_mgmt_interface->update($this->getRequest(), $this->zipcode_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.zipcode-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.zipcode-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.zipcode-mgmt.edit');
    }
}
