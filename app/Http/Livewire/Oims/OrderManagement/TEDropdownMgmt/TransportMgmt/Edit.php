<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\TransportMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\TransportMgmt\TransportMgmtInterface;
use App\Models\OimsTransportReference;
use App\Traits\Oims\TEDropdownMgmt\TransportMgmt\TransportMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use TransportMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $transports = [];

    public function mount($id)
    {
        $this->resetForm();
        $this->transports = OimsTransportReference::findOrFail($id);

        $this->name = $this->transports->name;
        $this->transport_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.transport-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(TransportMgmtInterface $transport_mgmt_interface)
    {

        $response = $transport_mgmt_interface->updateValidation($this->getRequest(),$this->transport_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(TransportMgmtInterface $transport_mgmt_interface)
    {
        $response = $transport_mgmt_interface->update($this->getRequest(), $this->transport_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.transport-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.transport-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.transport-mgmt.edit');
    }
}
