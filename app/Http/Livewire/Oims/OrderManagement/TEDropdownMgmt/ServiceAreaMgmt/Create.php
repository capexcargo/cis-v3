<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\ServiceAreaMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ServiceAreaMgmt\ServiceAreaMgmtInterface;
use App\Traits\Oims\TEDropdownMgmt\ServiceAreaMgmt\ServiceAreaMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use ServiceAreaMgmtTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(ServiceAreaMgmtInterface $servicearea_interface)
    {
        $response = $servicearea_interface->createValidation($this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    // dd($response['result']);
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(ServiceAreaMgmtInterface $servicearea_interface)
    {
        $response = $servicearea_interface->create($this->getRequest());
        if ($response['code'] == 200) {

            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index', 'close_modal', 'create');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.create');
    }
}
