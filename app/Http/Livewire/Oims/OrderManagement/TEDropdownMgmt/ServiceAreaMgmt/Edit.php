<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\ServiceAreaMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ServiceAreaMgmt\ServiceAreaMgmtInterface;
use App\Models\OimsServiceAreaCoverage;
use App\Traits\Oims\TEDropdownMgmt\ServiceAreaMgmt\ServiceAreaMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use ServiceAreaMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $servareas = [];

    public function mount($id)
    {
        $this->resetForm();
        $this->servareas = OimsServiceAreaCoverage::with('Ports')->findOrFail($id);

        // $this->name = $this->servareas->name;

        $this->isl = $this->servareas->island_group_id;
        $this->reg = $this->servareas->region_id;
        $this->prov = $this->servareas->state_id;
        $this->mun = $this->servareas->city_id;
        $this->bar = $this->servareas->barangay_id;
        $this->zip = $this->servareas->zipcode;
        $this->ptc = $this->servareas->Ports->name;
        $this->serv = $this->servareas->serviceability_id;

        $this->service_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(ServiceAreaMgmtInterface $service_area_mgmt_interface)
    {

        $response = $service_area_mgmt_interface->updateValidation($this->getRequest(),$this->service_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(ServiceAreaMgmtInterface $service_area_mgmt_interface)
    {
        $response = $service_area_mgmt_interface->update($this->getRequest(), $this->service_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.edit');
    }
}
