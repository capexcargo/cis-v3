<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\ServiceAreaMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ServiceAreaMgmt\ServiceAreaMgmtInterface;
use App\Models\OimsServiceAreaCoverage;
use App\Traits\Oims\TEDropdownMgmt\ServiceAreaMgmt\ServiceAreaMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use ServiceAreaMgmtTrait, WithPagination, PopUpMessagesTrait;

    public $status_header_cards = [];
    public $stats;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal = true;
        }elseif ($action_type == 'edit') {
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.edit', 'edit', $data['id']);
            $this->service_id = $data['id'];
            $this->edit_modal = true;
        }else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->service_id = $data['id'];
                return;
            }
            $this->deactivate_modal = true;
            $this->service_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        } 
    }

    public function load()
    {
        $this->loadStatusHeaderCards();
        $this->stats = '1';
    }

    public function loadStatusHeaderCards()
    {
        $activestatus = OimsServiceAreaCoverage::where('status', 1)->get();
        $inactivestatus = OimsServiceAreaCoverage::where('status', 2)->get();
        // dd($activestatus);

        $statusalls = OimsServiceAreaCoverage::get();
        // dd($forap);


        $statusall = $statusalls->count();
        $statusactive = $activestatus->count();
        $statusinactive = $inactivestatus->count();

        // $this->countall = $statusall;

        // dd($foraped);

        $this->status_header_cards = [
            // [
            //     'title' => 'All',
            //     'value' => '',
            //     'class' => 'bg-white  border border-gray-400 shadow-sm text-sm',
            //     'color' => 'text-blue',
            //     'action' => 'stats',
            //     'id' => false
            // ],
            [
                'title' => 'ACTIVE',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 1,
            ],
            [
                'title' => 'DEACTIVATED',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 2
            ],
            // [
            //     'title' => 'Declined',
            //     'value' => '',
            //     'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
            //     'color' => 'text-blue',
            //     'action' => 'stats',
            //     'id' => 2
            // ],
        ];
    }

    public function updateStatus($id, $value)
    {
        $areas = OimsServiceAreaCoverage::findOrFail($id);

        $areas->update([
            'status' => $value,
        ]);


        if ($value == 1) {
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index', 'close_modal', 'update_status');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index', 'index');
            $this->sweetAlert('', 'Service Area has been Successfully Reactivated!');
            return;
        }

        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index', 'close_modal', 'update_status');
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index', 'index');
        $this->sweetAlert('', 'Service Area has been Successfully Deactivated!');
    }


    public function render(ServiceAreaMgmtInterface $service_area_mgmt_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'stats' => $this->stats,

        ];

        $response = $service_area_mgmt_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'service_area_lists' => [],
                ];
        }
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index', [
            'service_area_lists' => $response['result']['service_area_lists']
        ]);
    }
    // public function render()
    // {
    //     return view('livewire.oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index');
    // }
}
