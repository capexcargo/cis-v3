<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\MunicipalityMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\MunicipalityMgmt\MunicipalityMgmtInterface;
use App\Traits\Oims\TEDropdownMgmt\MunicipalityMgmt\MunicipalityMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use MunicipalityMgmtTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public function addBrgyZip()
    {
        $this->brgyzips[] = [
            'id' => null,
            'brgy' => null,
            'zip' => null,
            'is_deleted' => false,

        ];
    }

    public function removeBrgyZip(array $data)
    {
        if (count(collect($this->brgyzips)->where('is_deleted', false)) > 1) {
            if ($this->brgyzips[$data["a"]]['id']) {
                $this->brgyzips[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->brgyzips[$data["a"]]);
            }

            array_values($this->brgyzips);
        }
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.municipality-mgmt.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(MunicipalityMgmtInterface $municipality_mgmt_interface)
    {
        $response = $municipality_mgmt_interface->createValidation($this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    // dd($response['result']);
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(MunicipalityMgmtInterface $municipality_mgmt_interface)
    {
        $response = $municipality_mgmt_interface->create($this->getRequest());
        if ($response['code'] == 200) {

            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.municipality-mgmt.index', 'close_modal', 'create');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.municipality-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        if (!$this->brgyzips) {
            $this->brgyzips[] = [
                'id' => null,
                'brgy' => null,
                'zip' => null,
                'is_deleted' => false,

            ];
        }

        return view('livewire.oims.order-management.t-e-dropdown-mgmt.municipality-mgmt.create');
    }
}
