<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\MunicipalityMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\MunicipalityMgmt\MunicipalityMgmtInterface;
use App\Models\Crm\CityReference;
use App\Traits\Oims\TEDropdownMgmt\MunicipalityMgmt\MunicipalityMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use MunicipalityMgmtTrait, WithPagination, PopUpMessagesTrait;
    

    protected $listeners = ['edit' => 'mount'];

    public $municipals = [];

    // public $municipals = [];
    // public $municipals_id;
    // public $brgyzips = [];
    // public $contacts = [];

    public $countarr;
    public $nearest;
    public $min;

    public function mount($id)
    {
        $this->resetForm();
        // $this->municipals = CityReference::findOrFail($id);

        // $this->name = $this->municipals->name;
        // $this->municipality_id = $id;

        // foreach ($this->municipals->BarangayCity as $a => $brgys) {
        //     $this->brgyzips[$a] = [
        //         'id' => $brgys->id,
        //         'brgy' => $brgys->BarangayCity->id,
        //         'is_deleted' => false,
        //     ];
        // }
        $municipals = CityReference::with(['BarangayCity' => function ($query) {
            $query->with('ZipcodeReference');
        }])->where('id', $id)->first();
        $this->municipality_id = $id;
        // dd($municipals->AncillaryDisplayDetails);
        foreach ($municipals->BarangayCity as $a => $municipal) {
            // dd($municipal->id);
            $this->brgyzips[$a] = [
                'id' => $municipal->id,
                'brgy' => $municipal->id,
                'zip' => $municipal->ZipcodeReference->name,
                'is_deleted' => false,
            ];
        }

        $this->name = $municipals->name;
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->brgyzips as $a => $brgyzip) {

            $getkeyarray[] = $a;
            $countarray += ($this->brgyzips[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }    

// =================================================================================================================================

    public function addBrgyZip()
    {
        $this->brgyzips[] = [
            'id' => null,
            'brgy' => null,
            'zip' => null,
            'is_deleted' => false,
        ];
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->brgyzips as $a => $brgyzip) {

            if ($this->brgyzips[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }
            $countarray += ($this->brgyzips[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

// =============================================================================================================================================

    public function removeBrgyZip(array $data)
    {
        if (count(collect($this->brgyzips)->where('is_deleted', false)) > 1) {
            if ($this->brgyzips[$data["a"]]['id']) {
                $this->brgyzips[$data["a"]]['is_deleted'] = true;
                // unset($this->brgyzips[$data["a"]]);
            } else {
                unset($this->brgyzips[$data["a"]]);
            }
            array_values($this->brgyzips);
        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->brgyzips as $a => $brgyzip) {

            if ($this->brgyzips[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }
            $countarray += ($this->brgyzips[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

// =============================================================================================================================================

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.municipality-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(MunicipalityMgmtInterface $municipality_mgmt_interface)
    {

        $response = $municipality_mgmt_interface->updateValidation($this->getRequest(),$this->municipality_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(MunicipalityMgmtInterface $municipality_mgmt_interface)
    {
        $response = $municipality_mgmt_interface->update($this->getRequest(), $this->municipality_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.municipality-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.municipality-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.municipality-mgmt.edit');
    }
}
