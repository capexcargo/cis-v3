<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\CargoMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\CargoMgmt\CargoMgmtInterface;
use App\Traits\Oims\TEDropdownMgmt\CargoMgmt\CargoMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use CargoMgmtTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.cargo-mgmt.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(CargoMgmtInterface $cargo_interface)
    {
        $response = $cargo_interface->createValidation($this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    // dd($response['result']);
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(CargoMgmtInterface $cargo_interface)
    {
        $response = $cargo_interface->create($this->getRequest());
        if ($response['code'] == 200) {

            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.cargo-mgmt.index', 'close_modal', 'create');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.cargo-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.cargo-mgmt.create');
    }
}
