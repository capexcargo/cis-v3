<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\CargoMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\CargoMgmt\CargoMgmtInterface;
use App\Models\OimsCargoStatusReference;
use App\Traits\Oims\TEDropdownMgmt\CargoMgmt\CargoMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use CargoMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $cargos = [];

    public function mount($id)
    {
        $this->resetForm();
        $this->cargos = OimsCargoStatusReference::findOrFail($id);

        $this->name = $this->cargos->name;
        $this->cargo_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.cargo-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(CargoMgmtInterface $cargo_mgmt_interface)
    {

        $response = $cargo_mgmt_interface->updateValidation($this->getRequest(),$this->cargo_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(CargoMgmtInterface $cargo_mgmt_interface)
    {
        $response = $cargo_mgmt_interface->update($this->getRequest(), $this->cargo_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.cargo-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.cargo-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.cargo-mgmt.edit');
    }
}
