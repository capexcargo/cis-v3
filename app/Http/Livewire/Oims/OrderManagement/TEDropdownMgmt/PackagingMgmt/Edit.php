<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\PackagingMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\PackagingMgmt\PackagingMgmtInterface;
use App\Models\OimsPackagingReference;
use App\Traits\Oims\TEDropdownMgmt\PackagingMgmt\PackagingMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use PackagingMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $packages = [];

    public function mount($id)
    {
        $this->resetForm();
        $this->packages = OimsPackagingReference::findOrFail($id);

        $this->name = $this->packages->name;
        $this->packaging_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.packaging-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(PackagingMgmtInterface $paackaging_mgmt_interface)
    {

        $response = $paackaging_mgmt_interface->updateValidation($this->getRequest(),$this->packaging_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(PackagingMgmtInterface $paackaging_mgmt_interface)
    {
        $response = $paackaging_mgmt_interface->update($this->getRequest(), $this->packaging_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.packaging-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.packaging-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.packaging-mgmt.edit');
    }
}
