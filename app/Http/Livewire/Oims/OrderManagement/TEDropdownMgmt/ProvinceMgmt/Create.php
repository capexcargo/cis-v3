<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\ProvinceMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ProvinceMgmt\ProvinceMgmtInterface;
use App\Traits\Oims\TEDropdownMgmt\ProvinceMgmt\ProvinceMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use ProvinceMgmtTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public function addProv()
    {
        $this->provs[] = [
            'id' => null,
            'mun' => null,
            'is_deleted' => false,

        ];
    }

    public function removeProv(array $data)
    {
        if (count(collect($this->provs)->where('is_deleted', false)) > 1) {
            if ($this->provs[$data["a"]]['id']) {
                $this->provs[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->provs[$data["a"]]);
            }

            array_values($this->provs);
        }
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.province-mgmt.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(ProvinceMgmtInterface $province_mgmt_interface)
    {
        $response = $province_mgmt_interface->createValidation($this->getRequest());
        // dd($response);


        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    // dd($response['result']);
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(ProvinceMgmtInterface $province_mgmt_interface)
    {
        $response = $province_mgmt_interface->create($this->getRequest());
        // dd($response);
        if ($response['code'] == 200) {

            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.province-mgmt.index', 'close_modal', 'create');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.province-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        if (!$this->provs) {
            $this->provs[] = [
                'id' => null,
                'mun' => null,
                'is_deleted' => false,

            ];
        }

        return view('livewire.oims.order-management.t-e-dropdown-mgmt.province-mgmt.create');
    }
}
