<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\ProvinceMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ProvinceMgmt\ProvinceMgmtInterface;
use App\Models\Crm\StateReference;
use App\Traits\Oims\TEDropdownMgmt\ProvinceMgmt\ProvinceMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use ProvinceMgmtTrait, WithPagination, PopUpMessagesTrait;


    protected $listeners = ['edit' => 'mount'];

    public $provinces = [];

    // public $provinces = [];
    // public $provinces_id;
    // public $provs = [];
    // public $contacts = [];

    public $countarr;
    public $nearest;
    public $min;

    public function mount($id)
    {
        $this->resetForm();
        // $this->provinces = CityReference::findOrFail($id);

        // $this->name = $this->provinces->name;
        // $this->province_id = $id;

        // foreach ($this->provinces->BarangayCity as $a => $brgys) {
        //     $this->provs[$a] = [
        //         'id' => $brgys->id,
        //         'brgy' => $brgys->BarangayCity->id,
        //         'is_deleted' => false,
        //     ];
        // }
        $provinces = StateReference::with('cityProvince', 'regionProvince', 'islandProvince')->where('id', $id)->first();
        // dd($provinces);

        $this->province_id = $id;

        foreach ($provinces->cityProvince as $a => $province) {
            // dd($provinces->cityProvince);
            $this->provs[$a] = [
                'id' => $province->id,
                'mun' => $province->id,
                'is_deleted' => false,
            ];
        }

        
        $this->name = $provinces->name;
        $this->reg = $provinces->regionProvince->id;
        $this->isl = $provinces->islandProvince->id;
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->provs as $a => $brgyzip) {

            $getkeyarray[] = $a;
            $countarray += ($this->provs[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    // =================================================================================================================================

    public function addProv()
    {
        $this->provs[] = [
            'id' => null,
            'mun' => null,
            'is_deleted' => false,
        ];
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->provs as $a => $brgyzip) {

            if ($this->provs[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }
            $countarray += ($this->provs[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    // =============================================================================================================================================

    public function removeProv(array $data)
    {
        if (count(collect($this->provs)->where('is_deleted', false)) > 1) {
            if ($this->provs[$data["a"]]['id']) {
                $this->provs[$data["a"]]['is_deleted'] = true;
                // unset($this->provs[$data["a"]]);
            } else {
                unset($this->provs[$data["a"]]);
            }
            array_values($this->provs);
        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->provs as $a => $brgyzip) {

            if ($this->provs[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }
            $countarray += ($this->provs[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    // =============================================================================================================================================

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.province-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(ProvinceMgmtInterface $province_mgmt_interface)
    {

        $response = $province_mgmt_interface->updateValidation($this->getRequest(), $this->province_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(ProvinceMgmtInterface $province_mgmt_interface)
    {
        $response = $province_mgmt_interface->update($this->getRequest(), $this->province_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.province-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.province-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.province-mgmt.edit');
    }
}
