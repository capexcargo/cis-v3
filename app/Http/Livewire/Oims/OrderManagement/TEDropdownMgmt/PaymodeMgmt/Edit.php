<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\PaymodeMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\PaymodeMgmt\PaymodeMgmtInterface;
use App\Models\OimsPaymodeReference;
use App\Traits\Oims\TEDropdownMgmt\PaymodeMgmt\PaymodeMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use PaymodeMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $paymodes = [];

    public function mount($id)
    {
        $this->resetForm();
        $this->paymodes = OimsPaymodeReference::findOrFail($id);

        $this->name = $this->paymodes->name;
        $this->description = $this->paymodes->description;
        $this->paymode_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.paymode-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(PaymodeMgmtInterface $paymode_mgmt_interface)
    {

        $response = $paymode_mgmt_interface->updateValidation($this->getRequest(),$this->paymode_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(PaymodeMgmtInterface $paymode_mgmt_interface)
    {
        $response = $paymode_mgmt_interface->update($this->getRequest(), $this->paymode_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.paymode-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.paymode-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.paymode-mgmt.edit');
    }
}
