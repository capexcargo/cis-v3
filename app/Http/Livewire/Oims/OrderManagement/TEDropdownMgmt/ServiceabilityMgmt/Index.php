<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\ServiceabilityMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\ServiceabilityMgmt\ServiceabilityMgmtInterface;
use App\Models\OimsServiceabilityReference;
use App\Traits\Oims\TEDropdownMgmt\ServiceabilityMgmt\ServiceabilityMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use ServiceabilityMgmtTrait, WithPagination, PopUpMessagesTrait;

    public $status_header_cards = [];
    public $stats;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal = true;
        }elseif ($action_type == 'edit') {
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.serviceability-mgmt.edit', 'edit', $data['id']);
            $this->serviceability_id = $data['id'];
            $this->edit_modal = true;
        }else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->serviceability_id = $data['id'];
                return;
            }
            $this->deactivate_modal = true;
            $this->serviceability_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        } 
    }

    public function load()
    {
        $this->loadStatusHeaderCards();
        $this->stats = '1';
    }

    public function loadStatusHeaderCards()
    {
        $activestatus = OimsServiceabilityReference::where('status', 1)->get();
        $inactivestatus = OimsServiceabilityReference::where('status', 2)->get();

        $statusalls = OimsServiceabilityReference::get();
        // dd($forap);


        $statusall = $statusalls->count();
        $statusactive = $activestatus->count();
        $statusinactive = $inactivestatus->count();

        // $this->countall = $statusall;

        // dd($foraped);

        $this->status_header_cards = [
            // [
            //     'title' => 'All',
            //     'value' => '',
            //     'class' => 'bg-white  border border-gray-400 shadow-sm text-sm',
            //     'color' => 'text-blue',
            //     'action' => 'stats',
            //     'id' => false
            // ],
            [
                'title' => 'ACTIVE',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 1,
            ],
            [
                'title' => 'DEACTIVATED',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 2
            ],
            // [
            //     'title' => 'Declined',
            //     'value' => '',
            //     'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
            //     'color' => 'text-blue',
            //     'action' => 'stats',
            //     'id' => 2
            // ],
        ];
    }

    public function updateStatus($id, $value)
    {
        $areas = OimsServiceabilityReference::findOrFail($id);

        $areas->update([
            'status' => $value,
        ]);


        if ($value == 1) {
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.serviceability-mgmt.index', 'close_modal', 'update_status');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.serviceability-mgmt.index', 'index');
            $this->sweetAlert('', 'Serviceability has been Successfully Reactivated!');
            return;
        }

        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.serviceability-mgmt.index', 'close_modal', 'update_status');
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.serviceability-mgmt.index', 'index');
        $this->sweetAlert('', 'Serviceability has been Successfully Deactivated!');
    }

    public function search(ServiceabilityMgmtInterface $serviceability_mgmt_interface)
    {
        $this->search_request = [
            'name' => $this->name,
        ];
    }


    public function render(ServiceabilityMgmtInterface $serviceability_mgmt_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'stats' => $this->stats,

        ];

        $response = $serviceability_mgmt_interface->index($request , $this->search_request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'serviceability_lists' => [],
                ];
        }
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.serviceability-mgmt.index', [
            'serviceability_lists' => $response['result']['serviceability_lists']
        ]);
    }
}
