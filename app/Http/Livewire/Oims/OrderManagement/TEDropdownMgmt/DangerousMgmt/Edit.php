<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\DangerousMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\DangerousMgmt\DangerousMgmtInterface;
use App\Models\OimsDangerousGoodsReference;
use App\Traits\Oims\TEDropdownMgmt\DangerousMgmt\DangerousMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use DangerousMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $dangers = [];

    public function mount($id)
    {
        $this->resetForm();
        $this->dangers = OimsDangerousGoodsReference::findOrFail($id);

        $this->name = $this->dangers->name;
        $this->danger_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.dangerous-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(DangerousMgmtInterface $dangerous_mgmt_interface)
    {

        $response = $dangerous_mgmt_interface->updateValidation($this->getRequest(),$this->danger_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(DangerousMgmtInterface $dangerous_mgmt_interface)
    {
        $response = $dangerous_mgmt_interface->update($this->getRequest(), $this->danger_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.dangerous-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.dangerous-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.dangerous-mgmt.edit');
    }
}
