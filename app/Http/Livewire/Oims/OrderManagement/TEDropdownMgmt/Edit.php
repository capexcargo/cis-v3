<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\TEDropdownMgmtInterface;
use App\Models\OriginDestinationPortReference;
use App\Traits\Oims\TEDropdownMgmt\TEDropdownMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use TEDropdownMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $areas = [];
    public $rid;

    public function mount($id)
    {
        $this->resetForm();
        $this->areas = OriginDestinationPortReference::findOrFail($id);

        $this->name = $this->areas->name;
        $this->area_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(TEDropdownMgmtInterface $t_e_dropdown_mgmt_interface)
    {

        $response = $t_e_dropdown_mgmt_interface->updateValidation($this->getRequest(),$this->area_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(TEDropdownMgmtInterface $t_e_dropdown_mgmt_interface)
    {
        $response = $t_e_dropdown_mgmt_interface->update($this->getRequest(), $this->area_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.edit');
    }
}
