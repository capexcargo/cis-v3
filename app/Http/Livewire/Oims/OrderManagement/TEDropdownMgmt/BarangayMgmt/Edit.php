<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\BarangayMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\BarangayMgmt\BarangayMgmtInterface;
use App\Models\Crm\BarangayReference;
use App\Traits\Oims\TEDropdownMgmt\BarangayMgmt\BarangayMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use BarangayMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $barangays = [];

    public function mount($id)
    {
        $this->resetForm();
        $this->barangays = BarangayReference::findOrFail($id);

        $this->name = $this->barangays->name;
        $this->zip = $this->barangays->zipcode_id;
        $this->barangay_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.barangay-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(BarangayMgmtInterface $barangay_mgmt_interface)
    {

        $response = $barangay_mgmt_interface->updateValidation($this->getRequest(),$this->barangay_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(BarangayMgmtInterface $barangay_mgmt_interface)
    {
        $response = $barangay_mgmt_interface->update($this->getRequest(), $this->barangay_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.barangay-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.barangay-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.barangay-mgmt.edit');
    }
}
