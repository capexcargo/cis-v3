<?php

namespace App\Http\Livewire\Oims\OrderManagement\TEDropdownMgmt\BarangayMgmt;

use App\Interfaces\Oims\TEDropdownMgmt\BarangayMgmt\BarangayMgmtInterface;
use App\Models\Crm\BarangayReference;
use App\Traits\InitializeFirestoreTrait;
use App\Traits\Oims\TEDropdownMgmt\BarangayMgmt\BarangayMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use BarangayMgmtTrait, WithPagination, PopUpMessagesTrait, InitializeFirestoreTrait;

    public $status_header_cards = [];
    public $stats;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal = true;
        }elseif ($action_type == 'edit') {
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.barangay-mgmt.edit', 'edit', $data['id']);
            $this->barangay_id = $data['id'];
            $this->edit_modal = true;
        }else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->barangay_id = $data['id'];
                return;
            }
            $this->deactivate_modal = true;
            $this->barangay_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        } 
    }

    public function load()
    {
        $this->loadStatusHeaderCards();
        $this->stats = '1';
    }

    public function loadStatusHeaderCards()
    {
        $activestatus = BarangayReference::where('status', 1)->get();
        $inactivestatus = BarangayReference::where('status', 2)->get();

        $statusalls = BarangayReference::get();

        $statusall = $statusalls->count();
        $statusactive = $activestatus->count();
        $statusinactive = $inactivestatus->count();

        $this->status_header_cards = [
           
            [
                'title' => 'ACTIVE',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 1,
            ],
            [
                'title' => 'DEACTIVATED',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 2
            ],
          
        ];
    }

    public function updateStatus($id, $value)
    {
        $areas = BarangayReference::findOrFail($id);

        $areas->update([
            'status' => $value,
        ]);

        $collectionReference = $this->initializeFirestore()->collection('barangay'); 
    
        $query = $collectionReference->where('id', '=', intval($id));
        $documents = $query->documents();
    
        foreach ($documents as $document) {
            if ($document->exists()) {
                $documentId = $document->id();
                $collectionReference->document($documentId)->update([
                    ['path' => 'status', 'value' => $value]
                ]);
            }
        }

        if ($value == 1) {
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.barangay-mgmt.index', 'close_modal', 'update_status');
            $this->emitTo('oims.order-management.t-e-dropdown-mgmt.barangay-mgmt.index', 'index');
            $this->sweetAlert('', 'Barangay has been Successfully Reactivated!');
            return;
        }

        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.barangay-mgmt.index', 'close_modal', 'update_status');
        $this->emitTo('oims.order-management.t-e-dropdown-mgmt.barangay-mgmt.index', 'index');
        $this->sweetAlert('', 'Barangay has been Successfully Deactivated!');
    }

    public function search(BarangayMgmtInterface $barangay_mgmt_interface)
    {
        $this->search_request = [
            'name' => $this->name,
            'zip' => $this->zip,
        ];
    }


    public function render(BarangayMgmtInterface $barangay_mgmt_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'stats' => $this->stats,

        ];

        $response = $barangay_mgmt_interface->index($request , $this->search_request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'barangay_lists' => [],
                ];
        }
        return view('livewire.oims.order-management.t-e-dropdown-mgmt.barangay-mgmt.index', [
            'barangay_lists' => $response['result']['barangay_lists']
        ]);
    }
}
