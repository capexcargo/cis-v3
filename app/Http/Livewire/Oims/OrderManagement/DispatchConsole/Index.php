<?php

namespace App\Http\Livewire\Oims\OrderManagement\DispatchConsole;

use App\Interfaces\Oims\DispatchControlHub\DispatchControlHubInterface;
use App\Models\CrmBooking;
use App\Models\CrmBookingConsignee;
use App\Models\CrmBookingLogs;
use App\Models\CrmBookingShipper;
use App\Models\OimsBranchReference;
use App\Models\OimsEdtr;
use App\Models\OimsEdtrDetails;
use App\Models\OimsTeamReference;
use App\Models\OimsTeamRouteAssignment;
use App\Models\OimsTeamRouteAssignmentDetails;
use App\Models\OimsTransactionEntry;
use App\Traits\Oims\OrderManagement\DispatchConsole\DispatchConsoleTrait;
use App\Traits\PopUpMessagesTrait;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use DispatchConsoleTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount', 'index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public $transactionCount;

    public function mount()
    {
        if ($this->actStatusHead == 2) {
            $this->getbranches = OimsBranchReference::
                withCount(['TransactionHasMany' => function ($query) {
                }])
                ->where('name', 'like', '%' . $this->brnch . '%')->get();
        } elseif ($this->actStatusHead == 1 || $this->actStatusHead == null) {
            $this->getbranches = OimsBranchReference::with(['TeamRouteAssignRef'
            => function ($query1) {
                $query1->whereDate('dispatch_date', Carbon::today())
                    ->with(['teamDetails'
                    => function ($query2) {
                        $query2->with('teamIdReference');
                    }]);
            }])->
                withCount(['BookingHasMany' => function ($query) {
                    $query->whereIn('final_status_id', [6, 1, 8]);
                }])
                ->where('name', 'like', '%' . $this->brnch . '%')->get();
        }
        // dd($this->getbranches);
        $booking = CrmBooking::whereIn('final_status_id', [6, 1, 8])->get();
        $this->bookingCount = count($booking);

        $transaction = OimsTransactionEntry::
            // whereIn('final_status_id', [6, 1, 8])->
            get();
        $this->transactionCount = count($transaction);

    }

    public $getSweepers;
    public $idBranch;
    public $getSweepersDetails = [];

    public function actionbookingref(array $data, $action_type)
    {
        $viewbookref = CrmBooking::with('BookingShipper', 'ShipperReferenceBK', 'ActivityReferenceBK', 'TimeslotReferenceBK', 'FinalStatusReferenceBK', 'BookingConsigneeHasManyBK')
            ->where('id', $data['id'])->first();
        // dd($viewbookref->id);
        $this->customer_no = $viewbookref->BookingShipper->customer_no;
        // dd($this->customer_no);
        $shipdet = CrmBookingShipper::with('AccountTypeReferenceSP')->where('customer_no', $this->customer_no)->first();
        // dd($shipdet);

        $this->IDXcbId = $viewbookref->id;

        $this->IDXpickupwalk = $viewbookref->booking_category;
        $this->IDXsinglemulti = $viewbookref->consignee_category;

        $this->IDXbookingrefno = $viewbookref->booking_reference_no;
        $this->IDXbookingstat = $viewbookref->FinalStatusReferenceBK->name;
        $this->IDXfullname = $viewbookref->BookingShipper->name;
        $this->IDXfname = $viewbookref->BookingShipper->name;

        $this->IDXcompany = $viewbookref->BookingShipper->company_name;
        $this->IDXmobile_nos = $viewbookref->BookingShipper->mobile_number;
        $this->IDXemail_address = $viewbookref->BookingShipper->email_address;
        $this->IDXaddress = $viewbookref->BookingShipper->address;

        $this->IDXcustomertype = $shipdet->AccountTypeReferenceSP->name;
        $this->IDXacttype = $viewbookref->ActivityReferenceBK->activity_type;

        $this->IDXtimeslot = $viewbookref->TimeslotReferenceBK->name;

        $vbdconsignees = CrmBooking::with('ShipperReferenceBK', 'ActivityReferenceBK', 'TimeslotReferenceBK', 'FinalStatusReferenceBK', 'BookingConsigneeHasManyBK')
            ->where('id', $data['id'])->get();
        // dd($vbdconsignees);

        $this->vconsignee = [];
        foreach ($vbdconsignees as $i => $vbdconsigneess) {
            $this->vconsignee[0] = $vbdconsigneess->BookingConsigneeHasManyBK;
        }
        foreach ($this->vconsignee[0] as $i => $viewcons2) {
            $this->IDXconsid = $this->vconsignee[0][$i]['id'];
        }

        $this->action_type = $action_type;
        if ($action_type == 'bref') {
            $this->bookref_modal = true;
        }
    }

    public function actionBSLogs(array $data, $action_type)
    {
        // dd($data['id']);
        $viewbookingstatuslogs = CrmBookingLogs::with('StatusReferenceLG')
            ->where('booking_id', $data['id'])->first();

        // dd($viewbookingstatuslogs);
        $this->bstatrefno = $viewbookingstatuslogs->booking_reference_no;
        $this->bstatstat = $viewbookingstatuslogs->StatusReferenceLG->name;
        $this->bstatcreated = $viewbookingstatuslogs->created_at;

        $this->bsl_modal = true;
    }

    public function actionvcdet(array $data, $action_type)
    {
        $VBconsdetails = CrmBookingConsignee::with('BookingCargoDetailsHasManyCD', 'TransportReferenceCD', 'ServiceModeReferenceCD', 'ModeOfPaymenReferenceCD')
            ->where('id', $data['id'])->get();

        $this->VBCD = [];
        $this->bookcons = [];

        foreach ($VBconsdetails as $i => $VBconsdetailss) {
            $this->VBCD = $VBconsdetailss->BookingCargoDetailsHasManyCD;
            $this->bookcons = $VBconsdetailss;
        }
        $this->IDXdecval = $this->bookcons->declared_value;
        $this->IDXtransportmode = $this->bookcons->TransportReferenceCD->name;
        $this->IDXservmode = $this->bookcons->ServiceModeReferenceCD->name;
        $this->IDXdescgoods = $this->bookcons->description_goods;
        $this->IDXmodeofp = $this->bookcons->ModeOfPaymenReferenceCD->name;

        $this->IDXref = $this->bookcons->booking_id;
        $vcdets = CrmBooking::with('BookingShipper', 'ShipperReferenceBK', 'ActivityReferenceBK', 'TimeslotReferenceBK', 'FinalStatusReferenceBK', 'BookingConsigneeHasManyBK')
            ->where('id', $this->IDXref)->first();

        $this->IDXwcount = $vcdets->consignee_count;
        $this->IDXworkins = $vcdets->work_instruction;
        $this->IDXintrem = $vcdets->remarks;
        // foreach ($this->VBCD[0] as $i => $viewcons) {
        // }
        $this->action_type = $action_type;
        if ($action_type == 'c_det') {
            $this->consigneedet_modal = true;
        }
    }

    public function load()
    {
        // $this->loadStatusHeaderCards();
        $this->stats = '';
    }

    public function loadStatusHeaderCards()
    {

        if ($this->actStatusHead == 1 || $this->actStatusHead == null) {

            $reschedstatus = CrmBooking::where('final_status_id', 6)->where('booking_branch_id', $this->branch_id)->get();
            $fdispatchstatus = CrmBooking::where('final_status_id', 1)
                ->where('booking_branch_id', $this->branch_id)
                ->get();
            $advancestatus = CrmBooking::where('final_status_id', 8)->where('booking_branch_id', $this->branch_id)->get();

            $statusalls = CrmBooking::where('booking_branch_id', $this->branch_id)->whereIn('final_status_id', [6, 1, 8])->get();

            $statusall = count($statusalls);
            $statusresched = count($reschedstatus);
            $statusfdispatch = count($fdispatchstatus);
            $statusadvance = count($advancestatus);
            // }


            $this->status_header_cards = [
                [
                    'title' => 'All',
                    'value' => $statusall,
                    'class' => 'bg-white  border border-gray-400 shadow-sm text-sm',
                    'color' => 'text-blue',
                    'action' => 'stats',
                    'id' => false
                ],
                [
                    'title' => 'Rescheduled',
                    'value' => $statusresched,
                    'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                    'color' => 'text-blue',
                    'action' => 'stats',
                    'id' => 6
                ],
                [
                    'title' => 'For Dispatch',
                    'value' => $statusfdispatch,
                    'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                    'color' => 'text-blue',
                    'action' => 'stats',
                    'id' => 1
                ],
                [
                    'title' => 'Advance',
                    'value' => $statusadvance,
                    'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                    'color' => 'text-blue',
                    'action' => 'stats',
                    'id' => 8
                ],
            ];
        } elseif ($this->actStatusHead == 2) {

            $forDeliverystatus = OimsTransactionEntry::where('transType', 1)->where('destination_id', $this->branch_id)->get();
            $failedDeliverytatus = OimsTransactionEntry::where('transType', 2)
                ->where('branch_id', $this->branch_id)
                ->get();
            // dd($failedDeliverytatus);
            $statusalls = OimsTransactionEntry::where('destination_id', $this->branch_id)->whereIn('transType', [1, 2])->get();

            $statusall = count($statusalls);
            $statuford = count($forDeliverystatus);
            $statusfaild = count($failedDeliverytatus);
            // $statusadvance = count($advancestatus);

            $this->status_header_cards = [
                [
                    'title' => 'All',
                    'value' => $statusall,
                    'class' => 'bg-white  border border-gray-400 shadow-sm text-sm',
                    'color' => 'text-blue',
                    'action' => 'stats',
                    'id' => false
                ],
                [
                    'title' => 'For Delivery',
                    'value' => $statuford,
                    'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                    'color' => 'text-blue',
                    'action' => 'stats',
                    'id' => 1
                ],
                [
                    'title' => 'Failed Delivery',
                    'value' => $statusfaild,
                    'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                    'color' => 'text-blue',
                    'action' => 'stats',
                    'id' => 2
                ],
            ];
        }
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == "activities") {
            $this->isShow = 1;
            if ($this->actStatusHead == 1) {
                $this->actStatusHead = 1;
            }
            if ($this->actStatusHead == 2) {
                $this->actStatusHead = 2;
            }
            $this->branch_id = $data['id'];
        } elseif ($action_type == "pa") {
            $this->isShow = 1;
            $this->reset([
                'selecteds',
            ]);
            $this->actStatusHead = 1;
            $this->branch_id = $data['id'];
        } elseif ($action_type == "da") {
            $this->isShow = 1;
            $this->reset([
                'selecteds',
            ]);
            $this->actStatusHead = 2;
            $this->branch_id = $data['id'];
        } elseif ($action_type == "teams") {
            $this->isShow = 2;
            $this->branch_id = $data['id'];

            $conditon = OimsTeamRouteAssignment::whereDate('dispatch_date', Carbon::today())
                ->where('branch_id',  $data['id'])->first();

            if ($conditon != null) {
                    $this->teamidentifier = 1;
                    $this->getSweepersDetails = OimsTeamRouteAssignmentDetails::withCount(['EDTRReference'
                    => function ($query0) {
                        $query0->with(['edtrDetails' => function ($query02) {
                            $query02->with('edtrbook', 'edtrtransaction');
                        }]);
                        $query0->whereDate('dispatch_date', Carbon::today());
                    }])->with(['EDTRReference'
                    => function ($query1) {
                        $query1->with(['edtrDetails' => function ($query12) {
                            $query12->with('edtrbook', 'edtrtransaction');
                        }]);
                        $query1->whereDate('dispatch_date', Carbon::today());
                    }])
                    ->where('team_route_id', $conditon->id)->get();
            }else{
                $this->teamidentifier = 2;
            }
        } elseif ($action_type == 'dispatch') {
            $this->emitTo('oims.order-management.dispatch-console.dispatch', 'dispatch', [$this->selecteds], $this->branch_id, $this->actStatusHead);
            $this->sel = $this->selecteds;
            $this->bid = $this->branch_id;
            $this->actstat = $this->actStatusHead;
            $this->dispatch_modal = true;
        } elseif ($action_type == "sweeper") {
            $this->getSweeperId = OimsTeamRouteAssignmentDetails::with('EDTRReference.edtrDetails.edtrbook', 'EDTRReference.edtrDetails.edtrtransaction', 'teamRouteAssignmentReference', 'teamIdReference', 
            'teamRouteReference', 'teamPlateReference', 'vehicleReference','teamDriverReference', 'teamChecker1Reference', 'teamChecker2Reference')->where('id', $data['id'])->first();
            $this->isShow = 3;
            // dd($this->getSweeperId);
            $this->sweeperName = $this->getSweeperId->teamIdReference->name;
            $this->start1 = $this->getSweeperId->EDTRReference[0]['edtrDetails'][0]['actual_start_travel_time'] ?? '';
            $this->end1 = $this->getSweeperId->EDTRReference[0]['edtrDetails'][0]['actual_end_travel_time'] ?? '';
            $this->bookRef = $this->getSweeperId->EDTRReference[0]['edtrDetails'][0]['edtrbook']['booking_reference_no'] ?? '';
            $this->shipperName = $this->getSweeperId->EDTRReference[0]['edtrDetails'][0]['edtrbook']['BookingShipper']['name'] ?? '';
            $this->pickAddress = $this->getSweeperId->EDTRReference[0]['edtrDetails'][0]['edtrbook']['BookingShipper']['address'] ?? '';
            $this->bookStart = $this->getSweeperId->EDTRReference[0]['edtrDetails'][0]['pickup_start_time'] ?? '';
            $this->bookEnd = $this->getSweeperId->EDTRReference[0]['edtrDetails'][0]['pickup_end_time'] ?? '';
            $this->start2 = $this->getSweeperId->EDTRReference[1]['edtrDetails'][0]['actual_start_travel_time'] ?? '';
            $this->end2 = $this->getSweeperId->EDTRReference[1]['edtrDetails'][0]['actual_end_travel_time'] ?? '';
            $this->waybillStart = $this->getSweeperId->EDTRReference[1]['edtrDetails'][0]['delivery_start_time'] ?? '';
            $this->waybillEnd = $this->getSweeperId->EDTRReference[1]['edtrDetails'][0]['delivery_end_time'] ?? '';
            $this->waybillNo = $this->getSweeperId->EDTRReference[1]['edtrDetails'][0]['edtrtransaction']['waybill'] ?? '';
            $this->shipperRefName = $this->getSweeperId->EDTRReference[1]['edtrDetails'][0]['edtrtransaction']['ShipperReference']['fullname'] ?? '';
            $this->consignee = $this->getSweeperId->EDTRReference[1]['edtrDetails'][0]['edtrtransaction']['ConsigneeReference']['fullname'] ?? '';
            $this->consigneeAdd = $this->getSweeperId->EDTRReference[1]['edtrDetails'][0]['edtrtransaction']['consignee_address'] ?? '';
            // $this->breakStart = $this->getSweeperId->EDTRReference[0]['actual_start_break'];
            // $this->breakEnd = $this->getSweeperId->EDTRReference[0]['actual_end_break'];

            // dd($this->getSweeperId->id);
        } elseif ($action_type == "showBranchteam") {
            $conditon = OimsTeamRouteAssignment::whereDate('dispatch_date', Carbon::today())
                ->where('branch_id',  $data['id'])->first();

            if ($conditon != null) {
                    $this->teamidentifier = 1;
                    $this->isShow = 2;
                    $this->getSweepersDetails = OimsTeamRouteAssignmentDetails::withCount(['EDTRReference'
                    => function ($query0) {
                        $query0->with(['edtrDetails' => function ($query02) {
                            $query02->with('edtrbook', 'edtrtransaction');
                        }]);
                        $query0->whereDate('dispatch_date', Carbon::today());
                    }])->with(['EDTRReference'
                    => function ($query1) {
                        $query1->with(['edtrDetails' => function ($query12) {
                            $query12->with('edtrbook', 'edtrtransaction');
                        }]);
                        $query1->whereDate('dispatch_date', Carbon::today());
                    }])
                    ->where('team_route_id', $conditon->id)->get();
                    // dd($this->getSweepersDetails[2]['EDTRReference'][0]['actual_start_break']);
            }else{
                $this->teamidentifier = 2;
            }
        }
    }
    public $teamidentifier;
    public $getSweeperId;
    public $sweeperName;
    public $start1;
    public $end1;
    public $bookRef;
    public $shipperName;
    public $pickAddress;
    public $bookStart;
    public $bookEnd;
    public $start2;
    public $end2;
    public $waybillEnd;
    public $waybillStart;
    public $waybillNo;
    public $shipperRefName;
    public $consignee;
    public $consigneeAdd;
    public $breakStart;
    public $breakEnd;


    public function closeModal($action_type)
    {
        if ($action_type == 'dispatch') {
            $this->dispatch_modal = false;
        } elseif ($action_type == 'bref') {
            $this->bookref_modal = false;
        } elseif ($action_type == 'c_det') {
            $this->consigneedet_modal = false;
        }
    }

    public function updatedSelectAll($value)
    {
        // dd($value);
        if ($value) {

            $request = [
                'booking_reference_no' => $this->booking_reference_no,
                'shipper_id' => $this->shipper_id,
                'booking_branch_id' => $this->booking_branch_id,
                'branch_id' => $this->branch_id,
            ];
            // dd($request ,$this->actStatusHead);
            if ($this->actStatusHead == 2) {
                $this->lists = OimsTransactionEntry::with(
                    'CrmBookingReference',
                    'OimsBranchReference',
                    'ShipperReference',
                    'OriginReference',
                    'DestinationReference',
                )->where('destination_id', $request['branch_id'])
                    ->pluck('id');
            } else {
                $this->lists = CrmBooking::with(
                    'BookingTypeReferenceBK',
                    'VehicleTypeReferenceBK',
                    'TimeslotReferenceBK',
                    'WalkinReferenceBK',
                    'ActivityReferenceBK',
                    'ShipperReferenceBK',
                    'FinalStatusReferenceBK',
                    'BookingBranchReferenceBK',
                    'MarketingChannelReferenceBK',
                    'CreatedByBK',
                    'BookingAttachmentHasManyBK',
                    'BookingLogsHasManyBK',
                    'BookingRemarksHasManyBK',
                    'BookingConsigneeHasManyBK',
                    'attachments',
                    'BookingShipper',
                    'BookingChannelReferenceBK',
                )
                    ->where('booking_branch_id', $request['branch_id'])
                    ->pluck('id');
            }


            $this->selecteds = $this->lists;
        } else {
            $this->selecteds = [];
        }
    }


    public function actionviewwork(array $data, $action_type)
    {
        $this->bookworkins = CrmBooking::with('BookingAttachmentHasManyBK')->where('id', $data['id'])->first();
        $this->work_ins = $this->bookworkins->work_instruction;
        $this->work_ins_id = $this->bookworkins->id;

        $this->action_type = $action_type;
        if ($action_type == 'workins') {
            $this->workinst_modal = true;
        } elseif ($action_type == 'no_workins') {
            $this->no_workinst_modal = true;
        }
    }

    public $showSweeper = false;

    public function showDetails()
    {
        $this->showSweeper = true;
    }

    public function hideDetails()
    {
        $this->showSweeper = false;
    }

    public function render(DispatchControlHubInterface $dispatch_interface)
    {
        $this->mount();
        $this->loadStatusHeaderCards();

        if (isset($this->selecteds)) {
            $this->selCount = count($this->selecteds);
        }

        $request = [
            'paginate' => $this->paginate,
            'branch_id' => $this->branch_id,
            'brnch' => $this->brnch,
            'stats' => $this->stats,
        ];

        $response = $dispatch_interface->index($request, $this->actStatusHead);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'pickup_lists' => [],
                ];
        }
        return view('livewire.oims.order-management.dispatch-console.index', [
            'pickup_lists' => $response['result']['pickup_lists']
        ]);
    }
}
