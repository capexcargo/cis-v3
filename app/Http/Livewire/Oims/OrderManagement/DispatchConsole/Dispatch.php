<?php

namespace App\Http\Livewire\Oims\OrderManagement\DispatchConsole;

use App\Interfaces\Oims\DispatchControlHub\DispatchControlHubInterface;
use App\Models\CrmBooking;
use App\Models\OimsTeamRouteAssignment;
use App\Models\OimsTeamRouteAssignmentDetails;
use App\Models\OimsTransactionEntry;
use App\Traits\Oims\OrderManagement\DispatchConsole\DispatchConsoleTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Dispatch extends Component
{
    use DispatchConsoleTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['dispatch' => 'mount', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public function mount($selecteds, $branch_id, $actStatusHead)
    {
        // dd($selecteds, $branch_id, $actStatusHead);
        $this->sel = $selecteds;
        $this->bid = $branch_id;
        $this->actstat = $actStatusHead;
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.dispatch-console.index', 'close_modal', 'dispatch');
    }

    public function confirmationSubmit(DispatchControlHubInterface $dispatch_interface)
    {
        $response = $dispatch_interface->createValidation($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function actionc(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'ba_close') {
            $this->resetForm();
            $this->reset([
                'selecteds',
            ]);
            $this->emitTo('oims.order-management.dispatch-console.index', 'close_modal', 'dispatch');
            $this->emitTo('oims.order-management.dispatch-console.index', 'index');
            // $this->emitTo('crm.sales.booking-mgmt.index', 'close_modal', 'create');
            // $this->emitTo('crm.sales.booking-mgmt.booking-history.index2', 'close_modal', 'create');
            // $this->emitTo('crm.sales.booking-mgmt.index', 'index');
            // $this->emitTo('crm.sales.booking-mgmt.booking-history.index2', 'index2');
            // $this->current_tab = 1;
            // $this->mount();
        }
    }

    public $Getbookings = [];
    public $Gettransactions = [];
    public function submit(DispatchControlHubInterface $dispatch_interface)
    {

        $getTeams = OimsTeamRouteAssignmentDetails::with('teamIdReference', 'teamRouteReference', 'teamPlateReference', 'teamDriverReference', 'teamChecker1Reference', 'teamChecker2Reference')->where('id', $this->rdis)->first();

        $this->teamname = $getTeams['teamIdReference']['name'];
        $this->routec = $getTeams['teamRouteReference']['name'];
        $this->plate = $getTeams['teamPlateReference']['plate_no'];
        $this->driver = $getTeams['teamDriverReference']['name'];
        $this->checker1 = $getTeams['teamChecker1Reference']['name'];
        $this->checker2 = $getTeams['teamChecker2Reference']['name'] ?? '-';

        $aa = implode(',', $this->sel[0]);
        $bb = explode(',', $aa);

        $i = 0;
        foreach ($bb as $v) {
            if ($this->actstat == 1 || $this->actstat == null) {
                $Getbookings = CrmBooking::with('BookingShipper')->where('id', intval($v))->first();
                $this->Getbookings[$i]['reference'] = $Getbookings->booking_reference_no;
                $this->Getbookings[$i]['shippername'] = $Getbookings->BookingShipper->name;
                $this->Getbookings[$i]['address'] = $Getbookings->BookingShipper->address;
                $i++;
            } elseif ($this->actstat == 2) {
                $Getbookings = OimsTransactionEntry::with('CrmBookingReference','ConsigneeReference.addresses')->where('id', intval($v))->first();
                // $Getbookings = CrmBooking::with('BookingShipper')->where('id', intval($v))->first();
                // dd($Getbookings['CrmBookingReference']['BookingShipper']);
                // dd($Getbookings);
                $this->Getbookings[$i]['reference'] = $Getbookings['CrmBookingReference']['booking_reference_no'];
                $this->Getbookings[$i]['shippername'] = $Getbookings['ConsigneeReference']['fullname'];
                $this->Getbookings[$i]['address'] = $Getbookings['consignee_address'];
                $i++;
            }
        }


        $response = $dispatch_interface->create($this->getRequest(), $this->sel, $this->bid, $this->actstat);
        if ($response['code'] == 200) {
            $this->confirmation_modal = false;
            $this->dispatch_modal = false;
            $this->confirmsub_modal = true;
            // $this->resetForm();
            // $this->emitTo('oims.order-management.dispatch-console.index', 'close_modal', 'dispatch');
            // $this->emitTo('oims.order-management.dispatch-console.index', 'index');
            // $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(DispatchControlHubInterface $dispatch_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'tn' => $this->tn,
        ];
        $response = $dispatch_interface->dispatch($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'team_lists' => [],
                ];
        }
        return view('livewire.oims.order-management.dispatch-console.dispatch', [
            'team_lists' => $response['result']['team_lists']
        ]);
    }
}
