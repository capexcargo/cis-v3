<?php

namespace App\Http\Livewire\Oims\OrderManagement\WaybillRegistry;

use App\Interfaces\Oims\WaybillRegistry\WaybillRegistryInterface;
use App\Models\OimsWaybillRegistry;
use App\Models\OimsWaybillRegistryDetails;
use App\Traits\Oims\OrderManagement\WaybillRegistry\WaybillRegistryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WaybillRegistryTrait, WithPagination, PopUpMessagesTrait;

    public $status_header_cards = [];
    public $stats;

    public $pfiCount;
    public $ipCount;
    public $usedCount;
    public $unusedCount;
    public $cancelledCount;


    protected $listeners = ['mount' => 'mount', 'index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public function mount()
    {
        $getpfissueance = OimsWaybillRegistry::whereNull('issued_to')->get();
        // dd($getpfissueance);
        $this->pfiCount = $getpfissueance->count();

        $getissuedpads = OimsWaybillRegistry::whereNotNull('issued_to')->get();
        $this->ipCount = $getissuedpads->count();

        $getusedwb = OimsWaybillRegistryDetails::where('status', 5)->get();
        $this->usedCount = $getusedwb->count();

        $getunusedwb = OimsWaybillRegistryDetails::whereNotIn('status', [5, 6])->get();
        $this->unusedCount = $getunusedwb->count();

        $cancelledwb = OimsWaybillRegistryDetails::where('status', 5)->get();
        $this->cancelledCount = $cancelledwb->count();
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('oims.order-management.waybill-registry.edit', 'edit', $data['id']);
            $this->waybill_id = $data['id'];
            $this->edit_modal = true;
        } elseif ($action_type == 'issue') {
            $this->emitTo('oims.order-management.waybill-registry.issue', 'issue', $data['id']);
            $this->issue_id = $data['id'];
            $this->issue_modal = true;
        } elseif ($action_type == 'view') {
            $this->emitTo('oims.order-management.waybill-registry.view', 'view', $data['id']);
            $this->view_id = $data['id'];
            $this->view_modal = true;
        }
        // else if ($action_type == 'update_status') {
        //     if ($data['status'] == 1) {
        //         $this->reactivate_modal = true;
        //         $this->area_id = $data['id'];
        //         return;
        //     }
        //     $this->deactivate_modal = true;
        //     $this->area_id = $data['id'];
        // }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } else if ($action_type == 'issue') {
            $this->issue_modal = false;
        }
    }

    public function loadheaders()
    {
        $this->loadStatusHeaderCards();
        $this->stats = '';
    }

    public function loadStatusHeaderCards()
    {
        $activestatus = OimsWaybillRegistry::whereNotNull('issued_to')->get();
        $inactivestatus = OimsWaybillRegistry::whereNull('issued_to')->get();

        $statusalls = OimsWaybillRegistry::get();

        $statusall = $statusalls->count();
        $statusactive = $activestatus->count();
        $statusinactive = $inactivestatus->count();

        $this->status_header_cards = [
            [
                'title' => 'ALL',
                'value' => '',
                'class' => 'bg-white  border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => false
            ],
            [
                'title' => 'ISSUED',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 1,
            ],
            [
                'title' => 'FOR ISSUANCE',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 2
            ],
        ];
    }

    public $isto;
    public $istoname;
    public $idate;
    public $status;

    public function search(WaybillRegistryInterface $waybill_registry_interface)
    {
        $this->search_request = [
            'from' => $this->from,
            'to' => $this->to,
            'isto' => $this->isto,
            'istoname' => $this->istoname,
            'idate' => $this->idate,
            'status' => $this->status,
        ];
    }

    public function render(WaybillRegistryInterface $waybill_registry_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'stats' => $this->stats,

        ];

        $response = $waybill_registry_interface->index($request, $this->search_request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'waybill_lists' => [],
                ];
        }
        return view('livewire.oims.order-management.waybill-registry.index', [
            'waybill_lists' => $response['result']['waybill_lists']
        ]);
    }
}
