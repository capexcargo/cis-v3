<?php

namespace App\Http\Livewire\Oims\OrderManagement\WaybillRegistry;

use App\Interfaces\Oims\WaybillRegistry\WaybillRegistryInterface;
use App\Models\OimsWaybillRegistry;
use App\Models\OimsWaybillRegistryDetails;
use App\Traits\Oims\OrderManagement\WaybillRegistry\WaybillRegistryTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class Issue extends Component
{
    use WaybillRegistryTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['issue' => 'mount'];

    public function mount($id)
    {
        $this->waybills = OimsWaybillRegistry::where('id', $id)->get();

        $this->from = $this->waybills[0]['waybill_start'];
        $this->to = $this->waybills[0]['waybill_end'];

        $this->issue_id = $id;

        $max = DB::select("SELECT MAX(status) AS status  from oims_waybill_registry_details where status not in (7,8) and waybill between '$this->from' and '$this->to' ");
        $compactmax  = compact('max');
        $maxstat = $compactmax['max'][0]->status;

        $this->getStatus1 = OimsWaybillRegistryDetails::select(DB::raw('*, max(status) as status'))
            ->where('status', $maxstat)
            ->first();

        $this->name_1 = $this->waybills[0]['issued_to'];
        $this->branch = $this->waybills[0]['branch_id'];

        if ($this->getStatus1->status == 2) {
            $this->name_2 = $this->waybills[0]['issued_to_fls'];
        } elseif ($this->getStatus1->status == 3) {
            $this->name_2 = $this->waybills[0]['issued_to_om'];
        } elseif ($this->getStatus1->status == 4) {
            $this->name_2 = $this->waybills[0]['issued_to_emp_id'];
        }

        // dd($this->name_2);
        // $this->waybilldetails = OimsWaybillRegistryDetails::where('waybill_registry_id', $this->issue_id)->get();

    }





    // =============================================================================================================================================

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.waybill-registry.index', 'close_modal', 'issue');
        $this->confirmation_modal = false;
        // $this->mount($id);
    }

    public function actionc(array $data, $action_type)
    {
        // dd("asd");
        $this->action_type = $action_type;
        if ($action_type == 'ba_close') {
            // $this->issue_modal = false;
            // $this->resetForm();
            // $this->emitTo('crm.sales.booking-mgmt.index', 'close_modal', 'create');
            // $this->emitTo('crm.sales.booking-mgmt.booking-history.index2', 'close_modal', 'create');
            // $this->emitTo('crm.sales.booking-mgmt.index', 'index');
            // $this->emitTo('crm.sales.booking-mgmt.booking-history.index2', 'index2');
            // $this->current_tab = 1;
            // $this->mount();
        }
    }

    public $getReqws;
    public $frm;
    public $t;

    public function confirmationSubmit(WaybillRegistryInterface $waybill_interface)
    {

        $response = $waybill_interface->updateValidation($this->getRequestEdit(), $this->issue_id,);

        // dd($response);

        $this->getReqws = $this->getRequestEdit();
        
                $this->frm = $this->getReqws['from'];
                $this->t = $this->getReqws['to'];
          

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;

        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public $getReq;

    public function submit(WaybillRegistryInterface $waybill_interface)
    {
        $response = $waybill_interface->update($this->getRequestEdit(), $this->issue_id,);
        // dd($this->getRequestEdit());
        // dd($response);

        $this->getReq = $this->getRequestEdit();

        if ($response['code'] == 200) {
            // $this->resetForm();
            // $this->emitTo('oims.order-management.waybill-registry.index', 'close_modal', 'issue');
            // $this->emitTo('oims.order-management.waybill-registry.index', 'index');
            // dd($this->getReq['name_1']);
            if ($this->getReq['name_1'] == 4) {
                $waybill_interface->issueToOMSkipEdit();
            } elseif ($this->getReq['name_1'] == 1) {
                $waybill_interface->issueToCheckerSkipEdit();
            }
            // $this->sweetAlert('', $response['message']);
            $this->confirmation_modal = false;
            $this->confirmsubissue_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.waybill-registry.issue');
    }
}
