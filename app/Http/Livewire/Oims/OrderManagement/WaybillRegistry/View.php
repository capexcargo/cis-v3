<?php

namespace App\Http\Livewire\Oims\OrderManagement\WaybillRegistry;

use App\Interfaces\Oims\WaybillRegistry\WaybillRegistryInterface;
use App\Models\OimsWaybillRegistry;
use App\Models\OimsWaybillRegistryDetails;
use App\Traits\Oims\OrderManagement\WaybillRegistry\WaybillRegistryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class View extends Component
{
    use WaybillRegistryTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['issue' => 'mount', 'index' => 'render', 'load_view_header_cards' => 'loadview'];

    public $wstart;
    public $wend;
    public $isto;
    public $branch;
    public $isdate;
    public $stat;
    public $cldate;

    // public $waybill_views = [];

    public $status_view_header_cards = [];
    public $viewstats;

    public $wid;

    public function mount($id)
    {

        // dd($request);
        $this->wid = $id;

        $this->waybills = OimsWaybillRegistry::with('waybillDetails', 'waybillBranch', 'waybillEmpUser', 'waybillCustInfo', 'waybillAgent', 'waybillOmUser', 'waybillFlsUser', 'waybillCbUser')->where('id', $id)->get();
        $this->wstart = $this->waybills[0]['waybill_start'];
        $this->wend = $this->waybills[0]['waybill_end'];

        if ($this->waybills[0]['issued_to'] == 1) {
            $this->isto = $this->waybills[0]['waybillEmpUser']['first_name'] . ' ' . $this->waybills[0]['waybillEmpUser']['last_name'];
        } elseif ($this->waybills[0]['issued_to'] == 2) {
            $this->isto = $this->waybills[0]['waybillCustInfo']['fullname'];
        } elseif ($this->waybills[0]['issued_to'] == 3) {
            $this->isto = $this->waybills[0]['waybillAgent']['name'];
        } elseif ($this->waybills[0]['issued_to'] == 4) {
            $this->isto = $this->waybills[0]['waybillOmUser']['first_name'] . ' ' . $this->waybills[0]['waybillOmUser']['last_name'];
        } elseif ($this->waybills[0]['issued_to'] == 5) {
            $this->isto = $this->waybills[0]['waybillFlsUser']['first_name'] . ' ' . $this->waybills[0]['waybillFlsUser']['last_name'];
        }

        $this->branch = $this->waybills[0]['waybillBranch']['name'];
        $this->isdate = $this->waybills[0]['issuance_date'];
        $this->stat = $this->waybills[0]['status'];
        $this->cldate = $this->waybills[0]['closed_date'];

        // $this->waybill_views = OimsWaybillRegistryDetails::where('waybill_registry_id', $this->waybills[0]['id'])->get();
    }

    public $wbsearch;


    public function loadviewheaders()
    {
        $this->loadStatusHeaderCards();
        $this->viewstats = '';
    }

    public function loadStatusHeaderCards()
    {
        $unusedstatus = OimsWaybillRegistryDetails::whereIn('status', [1, 2, 3, 4])->get();
        $usedstatus = OimsWaybillRegistryDetails::where('status', 5)->get();
        $cancelledstatus = OimsWaybillRegistryDetails::where('status', 6)->get();

        $statusalls = OimsWaybillRegistryDetails::whereIn('status', [1, 2, 3, 4, 5, 6])->get();

        $statusall = $statusalls->count();
        $statusunused = $unusedstatus->count();
        $statusused = $usedstatus->count();
        $statuscancelled = $cancelledstatus->count();

        $this->status_view_header_cards = [
            [
                'title' => 'ALL',
                'value' => $statusall,
                'class' => 'bg-white  border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'viewstats',
                'id' => false
            ],
            [
                'title' => 'UNUSED',
                'value' => $statusunused,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'viewstats',
                'id' => 1,
            ],
            [
                'title' => 'USED',
                'value' => $statusused,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'viewstats',
                'id' => 2
            ],
            [
                'title' => 'CANCELLED',
                'value' => $statuscancelled,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'viewstats',
                'id' => 3
            ],
        ];
    }

    public $wb;
    // public function search(WaybillRegistryInterface $waybill_registry_interface)
    // {
    //     $this->search_request = [
    //         'wb' => $this->wb,
    //     ];
    // }

    public function render(WaybillRegistryInterface $waybill_registry_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'viewstats' => $this->viewstats,
            'wb' => $this->wb,
            'wid' => $this->wid,

        ];

        $response = $waybill_registry_interface->view($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'waybill_views' => [],
                ];
        }
        return view('livewire.oims.order-management.waybill-registry.view', [
            'waybill_views' => $response['result']['waybill_views']
        ]);
    }
}
