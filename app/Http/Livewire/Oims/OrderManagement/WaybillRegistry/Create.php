<?php

namespace App\Http\Livewire\Oims\OrderManagement\WaybillRegistry;

use App\Interfaces\Oims\WaybillRegistry\WaybillRegistryInterface;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\OimsAgent;
use App\Models\OimsBranchReference;
use App\Models\OimsWaybillRegistry;
use App\Models\OimsWaybillRegistryDetails;
use App\Models\UserDetails;
use App\Traits\Oims\OrderManagement\WaybillRegistry\WaybillRegistryTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Create extends Component
{

    use WaybillRegistryTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public $getwaybills;
    // public function mount()
    // {
    //     $getWaybillRegistry = OimsWaybillRegistry::with('waybillDetails')->where('')->get();

    //     dd($getWaybillRegistry);
    // }

    public function addawb($a)
    {
        $this->awbs[$a][] = [
            'id' => null,
            'from' => null,
            'to' => null,
            'is_deleted' => false,
        ];
    }

    public function addsubawb($a)
    {
        if (isset($this->awbs[$a][$a])) {

            $getFrom = $this->awbs[$a][$a]['from'];
            $getTo = $this->awbs[$a][$a]['to'];

            $max = DB::select("SELECT MAX(status) AS status  from oims_waybill_registry_details where status not in (7,8) and waybill between '$getFrom' and '$getTo' ");
            $compactmax  = compact('max');
            $maxstat = $compactmax['max'][0]->status;

            $this->getStatus1 = OimsWaybillRegistryDetails::select(DB::raw('*, max(status) as status'))
                ->where('status', $maxstat)
                ->first();
                // dd($this->getStatus1);
        }



        $this->subawbs[$a][] = [
            'id' => null,
            'name_1' => null,
            'name_2' => null,
            'branch' => null,
            'is_deleted' => false,
        ];
    }

    public function removeawb(array $data)
    {
        if (count(collect($this->awbs)->where('is_deleted', false)) > 1) {
            if ($this->awbs[$data['a']][0]['id']) {

                $this->awbs[$data['a']][0]['is_deleted'] = true;
                $b = 0;
                if (isset($this->subawbs[$data['a']])) {
                    foreach ($this->subawbs[$data['a']] as $c => $awbs) {
                        $this->subawbs[$data['a']][$c]['is_deleted'] = true;
                        $b++;
                    }
                }


                // unset($this->awbs[$data['a']]);
            } else {

                unset($this->awbs[$data['a']]);
                unset($this->subawbs[$data['a']]);
            }
            array_values($this->awbs);
        }
    }

    public function removesubawb(array $data)
    {
        if ($this->subawbs[$data['a']][0]['id']) {

            $this->subawbs[$data['a']][0]['is_deleted'] = true;
        } else {

            unset($this->subawbs[$data['a']]);
        }
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.waybill-registry.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }
    public $getReqws = [];
    public $ws;

    public function confirmationSubmit(WaybillRegistryInterface $waybill_interface)
    {
        $response = $waybill_interface->createValidation($this->getRequest());

        $this->getReqws = $this->getRequest();
        // dd($this->getReqws[][]);
        if (isset($this->getReqws)) {
            foreach ($this->getReqws['awbs'] as $a => $getws) {
                $this->ws = $getws[$a]['from'];
            }
        }
        ($response);
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    // dd($response['result']);
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public $getReq = [];

    public function submit(WaybillRegistryInterface $waybill_interface)
    {
        $response = $waybill_interface->create($this->getRequest());

        $this->getReq = $this->getRequest();
        // $response = $this->getRequest();
        // dd($this->getReq['subawbs']);

        if ($response['code'] == 200) {

            // $this->resetForm();
            // $this->emitTo('oims.order-management.waybill-registry.index', 'close_modal', 'create');
            // $this->emitTo('oims.order-management.waybill-registry.index', 'index');
            if (isset($this->getReq)) {
                foreach ($this->getReq['subawbs'] as $a => $get) {
                    // dd($this->ws);
                    if ($get[$a]['name_1'] == 4) {
                        $waybill_interface->issueToOMSkip();
                    } elseif ($get[$a]['name_1'] == 1) {
                        $waybill_interface->issueToCheckerSkip();
                    }
                }
            }
            $this->confirmation_modal = false;
            $this->confirmsubedit_modal = true;
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {


        if (!$this->awbs) {
            $this->awbs[][] = [
                'id' => null,
                'from' => null,
                'to' => null,
                'is_deleted' => false,
            ];
        }
        return view('livewire.oims.order-management.waybill-registry.create');
    }
}
