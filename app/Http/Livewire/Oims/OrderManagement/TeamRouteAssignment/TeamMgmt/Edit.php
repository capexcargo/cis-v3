<?php

namespace App\Http\Livewire\Oims\OrderManagement\TeamRouteAssignment\TeamMgmt;

use App\Interfaces\Oims\TeamMgmt\TeamMgmtInterface;
use App\Models\OimsAreaReference;
use App\Models\OimsTeamReference;
use App\Traits\Oims\TeamRouteAssignment\TeamMgmt\TeamMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use TeamMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $teams = [];
    public $getArease = [];

    public function mount($id)
    {
        $this->resetForm();
        $this->teams = OimsTeamReference::with(['quadrantReference' => function ($query) {
            $query->with('AreaReference');
        }])->findOrFail($id);
        $this->name = $this->teams->name;
        $this->quad = $this->teams->quadrant_id;

        $this->getArease = OimsAreaReference::where('quadrant_id', $this->quad)->get();

        $this->team_id = $id;
    }

    public function updatedQuad()
    {
        $this->getArease = OimsAreaReference::where('quadrant_id', $this->quad)->get();
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.team-route-assignment.team-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(TeamMgmtInterface $team_mgmt_interface)
    {
        $response = $team_mgmt_interface->updateValidation($this->getRequest(), $this->team_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(TeamMgmtInterface $team_mgmt_interface)
    {
        $response = $team_mgmt_interface->update($this->getRequest(), $this->team_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.team-route-assignment.team-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.team-route-assignment.team-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.team-route-assignment.team-mgmt.edit');
    }
}
