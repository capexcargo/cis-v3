<?php

namespace App\Http\Livewire\Oims\OrderManagement\TeamRouteAssignment;

use App\Interfaces\Oims\TeamRouteAssignment\TeamRouteAssignmentInterface;
use App\Models\OimsBranchReference;
use App\Models\OimsTeamRouteAssignment;
use App\Models\UserDetails;
use App\Traits\Oims\TeamRouteAssignment\TeamRouteAssignmentTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Create extends Component
{
    use TeamRouteAssignmentTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public $confirmation_modal;
    public $getBranch = [];

    public function mount()
    {
        $teamCount = OimsTeamRouteAssignment::get();

        $this->tra = 'MNLTRA' . sprintf("%05d", count($teamCount) + 1);

        $this->getBranch = UserDetails::where('user_id', Auth::user()->id)->get();
 
        foreach ($this->getBranch as $a => $getb) {
            $branchid = $getb->branch_id;
        }

        $getName = OimsBranchReference::where('origin_destination_id', $branchid)->where('default', 1)->first();

        if (isset($getName->name)) {
            $this->branch = $getName->name;
        } else {
            $this->branch = "";
        }
    }

    public function addteam()
    {
        $this->teams[] = [
            'id' => null,
            'team_name' => null,
            'plate' => null,
            'route' => null,
            'driver' => null,
            'check_1' => null,
            'check_2' => null,
            'remarks' => null,
            'is_deleted' => false,

        ];
    }

    public function removeteam(array $data)
    {
        if (count(collect($this->teams)->where('is_deleted', false)) > 1) {
            if ($this->teams[$data["a"]]['id']) {
                $this->teams[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->teams[$data["a"]]);
            }

            array_values($this->teams);
        }
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.team-route-assignment.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
        $this->mount();
    }

    public function confirmationSubmit(TeamRouteAssignmentInterface $team_route_interface)
    {
        $response = $team_route_interface->createValidation($this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    // dd($response['result']);
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(TeamRouteAssignmentInterface $team_route_interface)
    {
        $response = $team_route_interface->create($this->getRequest());
        if ($response['code'] == 200) {

            $this->resetForm();
            $this->emitTo('oims.order-management.team-route-assignment.index', 'close_modal', 'create');
            $this->emitTo('oims.order-management.team-route-assignment.index', 'index');
            $this->sweetAlert('', $response['message']);
            $this->mount();
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        if (!$this->teams) {
            $this->teams[] = [
                'id' => null,
                'team_name' => null,
                'plate' => null,
                'route' => null,
                'driver' => null,
                'check_1' => null,
                'check_2' => null,
                'remarks' => null,
                'is_deleted' => false,

            ];
        }
        return view('livewire.oims.order-management.team-route-assignment.create');
    }
}
