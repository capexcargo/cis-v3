<?php

namespace App\Http\Livewire\Oims\OrderManagement\TeamRouteAssignment;

use App\Interfaces\Oims\TeamRouteAssignment\TeamRouteAssignmentInterface;
use App\Models\OimsBranchReference;
use App\Models\OimsTeamRouteAssignment;
use App\Traits\Oims\TeamRouteAssignment\TeamRouteAssignmentTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use TeamRouteAssignmentTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount',];

    public $team_data = [];

    public $countarr;
    public $nearest;
    public $min;

    public function mount($id)
    {
        $this->resetForm();
        $team_data = OimsTeamRouteAssignment::with('teamDetails', 'teamBranch')->where('id', $id)->first();

        $branchid = $team_data->branch_id;

        $getName = OimsBranchReference::where('origin_destination_id', $branchid)->where('default', 1)->first();


        $this->tra = $team_data->tra_no;
        $this->dis_date = $team_data->dispatch_date;
        $this->branch = $getName->name;

        $this->team_id = $id;

        // dd($this->team_id);

        foreach ($team_data->teamDetails as $a => $teamdet) {
            $this->teams[$a] = [
                'id' => $teamdet->id,
                'team_name' => $teamdet->team_id,
                'plate' => $teamdet->plate_no_id,
                'route' => $teamdet->route_category_id,
                'driver' => $teamdet->driver_id,
                'check_1' => $teamdet->checker1_id,
                'check_2' => $teamdet->checker2_id,
                'remarks' => $teamdet->remarks,
                'is_deleted' => false,
            ];
        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->teams as $a => $areatag) {

            $getkeyarray[] = $a;
            $countarray += ($this->teams[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    // =================================================================================================================================

    public function addteam()
    {
        $this->teams[] = [
            'id' => null,
            'team_name' => null,
            'plate' => null,
            'route' => null,
            'driver' => null,
            'check_1' => null,
            'check_2' => null,
            'remarks' => null,
            'is_deleted' => false,
        ];
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->teams as $a => $areatag) {

            if ($this->teams[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }
            $countarray += ($this->teams[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    // =============================================================================================================================================

    public function removeteam(array $data)
    {
        if (count(collect($this->teams)->where('is_deleted', false)) > 1) {
            if ($this->teams[$data["a"]]['id']) {
                $this->teams[$data["a"]]['is_deleted'] = true;
                // unset($this->teams[$data["a"]]);
            } else {
                unset($this->teams[$data["a"]]);
            }
            array_values($this->teams);
        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->teams as $a => $areatag) {

            if ($this->teams[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }
            $countarray += ($this->teams[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    // =============================================================================================================================================

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.team-route-assignment.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
        // $this->mount($id);
    }

    public function confirmationSubmit(TeamRouteAssignmentInterface $team_interface)
    {

        $response = $team_interface->updateValidation($this->getRequest(), $this->team_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(TeamRouteAssignmentInterface $team_interface)
    {
        $response = $team_interface->update($this->getRequest(), $this->team_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.team-route-assignment.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.team-route-assignment.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.team-route-assignment.edit');
    }
}
