<?php

namespace App\Http\Livewire\Oims\OrderManagement\TeamRouteAssignment\QuadrantMgmt;

use App\Interfaces\Oims\QuadrantMgmt\QuadrantMgmtInterface;
use App\Models\Crm\BranchReferencesQuadrant;
use App\Traits\Oims\TeamRouteAssignment\QuadrantMgmt\QuadrantMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use QuadrantMgmtTrait, WithPagination, PopUpMessagesTrait;

    public $status_header_cards = [];
    public $stats;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal = true;
        }elseif ($action_type == 'edit') {
            $this->emitTo('oims.order-management.team-route-assignment.quadrant-mgmt.edit', 'edit', $data['id']);
            $this->quadrant_id = $data['id'];
            $this->edit_modal = true;
        }else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->quadrant_id = $data['id'];
                return;
            }
            $this->deactivate_modal = true;
            $this->quadrant_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        } 
    }

    public function load()
    {
        $this->loadStatusHeaderCards();
        $this->stats = '1';
    }

    public function loadStatusHeaderCards()
    {
        $activestatus = BranchReferencesQuadrant::where('status', 1)->get();
        $inactivestatus = BranchReferencesQuadrant::where('status', 2)->get();

        $statusalls = BranchReferencesQuadrant::get();

        $statusall = $statusalls->count();
        $statusactive = $activestatus->count();
        $statusinactive = $inactivestatus->count();

        $this->status_header_cards = [
           
            [
                'title' => 'ACTIVE',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 1,
            ],
            [
                'title' => 'DEACTIVATED',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 2
            ],
          
        ];
    }

    public function updateStatus($id, $value)
    {
        $quads = BranchReferencesQuadrant::findOrFail($id);

        $quads->update([
            'status' => $value,
        ]);


        if ($value == 1) {
            $this->emitTo('oims.order-management.team-route-assignment.quadrant-mgmt.index', 'close_modal', 'update_status');
            $this->emitTo('oims.order-management.team-route-assignment.quadrant-mgmt.index', 'index');
            $this->sweetAlert('', 'Quadrant has been Successfully Reactivated!');
            return;
        }

        $this->emitTo('oims.order-management.team-route-assignment.quadrant-mgmt.index', 'close_modal', 'update_status');
        $this->emitTo('oims.order-management.team-route-assignment.quadrant-mgmt.index', 'index');
        $this->sweetAlert('', 'Quadrant has been Successfully Deactivated!');
    }

    // public function search(QuadrantMgmtInterface $quadrant_mgmt_interface)
    // {
    //     $this->search_request = [
    //         'name' => $this->name,
    //         'mun' => $this->mun,
    //         'brgy' => $this->brgy,
    //     ];
    // }


    public function render(QuadrantMgmtInterface $quadrant_mgmt_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'stats' => $this->stats,

        ];

        $response = $quadrant_mgmt_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'quadrant_lists' => [],
                ];
        }
        return view('livewire.oims.order-management.team-route-assignment.quadrant-mgmt.index', [
            'quadrant_lists' => $response['result']['quadrant_lists']
        ]);
    }

    //  public function render()
    // {
    //     return view('livewire.oims.order-management.team-route-assignment.quadrant-mgmt.index');
    // }

}
