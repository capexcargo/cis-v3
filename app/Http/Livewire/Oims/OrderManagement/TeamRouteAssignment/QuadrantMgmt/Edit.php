<?php

namespace App\Http\Livewire\Oims\OrderManagement\TeamRouteAssignment\QuadrantMgmt;

use App\Interfaces\Oims\QuadrantMgmt\QuadrantMgmtInterface;
use App\Models\Crm\BranchReferencesQuadrant;
use App\Traits\Oims\TeamRouteAssignment\QuadrantMgmt\QuadrantMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use QuadrantMgmtTrait, WithPagination, PopUpMessagesTrait;


    protected $listeners = ['edit' => 'mount'];

    public $quadrants = [];

    public $countarr;
    public $nearest;
    public $min;

    public function mount($id)
    {
        $this->resetForm();
        $quadrants = BranchReferencesQuadrant::with('AreaReference')->where('id', $id)->first();

        $this->name = $quadrants->name;
  
        $this->quadrant_id = $id;

        foreach ($quadrants->AreaReference as $a => $areat) {
            $this->areatags[$a] = [
                'id' => $areat->id,
                'area' => $areat->id,
                'is_deleted' => false,
            ];
        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->areatags as $a => $areatag) {

            $getkeyarray[] = $a;
            $countarray += ($this->areatags[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    // =================================================================================================================================

    public function addareatag()
    {
        $this->areatags[] = [
            'id' => null,
            'area' => null,
            'is_deleted' => false,
        ];
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->areatags as $a => $areatag) {

            if ($this->areatags[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }
            $countarray += ($this->areatags[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    // =============================================================================================================================================

    public function removeareatag(array $data)
    {
        if (count(collect($this->areatags)->where('is_deleted', false)) > 1) {
            if ($this->areatags[$data["a"]]['id']) {
                $this->areatags[$data["a"]]['is_deleted'] = true;
                // unset($this->areatags[$data["a"]]);
            } else {
                unset($this->areatags[$data["a"]]);
            }
            array_values($this->areatags);
        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->areatags as $a => $areatag) {

            if ($this->areatags[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }
            $countarray += ($this->areatags[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    // =============================================================================================================================================

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.team-route-assignment.quadrant-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(QuadrantMgmtInterface $quadrant_mgmt_interface)
    {

        $response = $quadrant_mgmt_interface->updateValidation($this->getRequest(), $this->quadrant_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(QuadrantMgmtInterface $quadrant_mgmt_interface)
    {
        $response = $quadrant_mgmt_interface->update($this->getRequest(), $this->quadrant_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.team-route-assignment.quadrant-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.team-route-assignment.quadrant-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.team-route-assignment.quadrant-mgmt.edit');
    }
}
