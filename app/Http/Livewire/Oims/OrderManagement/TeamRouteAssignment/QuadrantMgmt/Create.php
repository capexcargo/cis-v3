<?php

namespace App\Http\Livewire\Oims\OrderManagement\TeamRouteAssignment\QuadrantMgmt;

use App\Interfaces\Oims\QuadrantMgmt\QuadrantMgmtInterface;
use App\Traits\Oims\TeamRouteAssignment\QuadrantMgmt\QuadrantMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use QuadrantMgmtTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public function addareatag()
    {
        $this->areatags[] = [
            'id' => null,
            'area' => null,
            'is_deleted' => false,

        ];
    }

    public function removeareatag(array $data)
    {
        if (count(collect($this->areatags)->where('is_deleted', false)) > 1) {
            if ($this->areatags[$data["a"]]['id']) {
                $this->areatags[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->areatags[$data["a"]]);
            }

            array_values($this->areatags);
        }
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.team-route-assignment.quadrant-mgmt.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(QuadrantMgmtInterface $quadrant_mgmt_interface)
    {
        $response = $quadrant_mgmt_interface->createValidation($this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    // dd($response['result']);
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(QuadrantMgmtInterface $quadrant_mgmt_interface)
    {
        $response = $quadrant_mgmt_interface->create($this->getRequest());
        if ($response['code'] == 200) {

            $this->resetForm();
            $this->emitTo('oims.order-management.team-route-assignment.quadrant-mgmt.index', 'close_modal', 'create');
            $this->emitTo('oims.order-management.team-route-assignment.quadrant-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        if (!$this->areatags) {
            $this->areatags[] = [
                'id' => null,
                'area' => null,
                'is_deleted' => false,

            ];
        }
        return view('livewire.oims.order-management.team-route-assignment.quadrant-mgmt.create');
    }
}
