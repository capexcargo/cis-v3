<?php

namespace App\Http\Livewire\Oims\OrderManagement\TeamRouteAssignment\RouteCategory;

use App\Interfaces\Oims\RouteCategory\RouteCategoryInterface;
use App\Models\RouteCategoryReference;
use App\Traits\Oims\TeamRouteAssignment\RouteCategoryMgmt\RouteCategoryMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use RouteCategoryMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $route = [];
    public $rid;

    public function mount($id)
    {
        $this->resetForm();
        $this->route = RouteCategoryReference::findOrFail($id);

        $this->name = $this->route->name;
        $this->rcm_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.team-route-assignment.route-category.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(RouteCategoryInterface $route_category_interface)
    {

        $response = $route_category_interface->updateValidation($this->getRequest(),$this->rcm_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(RouteCategoryInterface $route_category_interface)
    {
        $response = $route_category_interface->update($this->getRequest(), $this->rcm_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.team-route-assignment.route-category.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.team-route-assignment.route-category.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.oims.order-management.team-route-assignment.route-category.edit');
    }
}
