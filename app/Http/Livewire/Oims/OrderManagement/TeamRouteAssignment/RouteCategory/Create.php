<?php

namespace App\Http\Livewire\Oims\OrderManagement\TeamRouteAssignment\RouteCategory;

use App\Interfaces\Oims\RouteCategory\RouteCategoryInterface;
use App\Traits\Oims\TeamRouteAssignment\RouteCategoryMgmt\RouteCategoryMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use RouteCategoryMgmtTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.team-route-assignment.route-category.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(RouteCategoryInterface $route_category_interface)
    {
        $response = $route_category_interface->createValidation($this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    // dd($response['result']);
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(RouteCategoryInterface $route_category_interface)
    {
        $response = $route_category_interface->create($this->getRequest());
        if ($response['code'] == 200) {

            $this->resetForm();
            $this->emitTo('oims.order-management.team-route-assignment.route-category.index', 'close_modal', 'create');
            $this->emitTo('oims.order-management.team-route-assignment.route-category.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.oims.order-management.team-route-assignment.route-category.create');
    }
}
