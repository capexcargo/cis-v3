<?php

namespace App\Http\Livewire\Oims\OrderManagement\TeamRouteAssignment;

use App\Interfaces\Oims\TeamRouteAssignment\TeamRouteAssignmentInterface;
use App\Models\OimsTeamRouteAssignment;
use App\Models\OimsTeamRouteAssignmentDetails;
use App\Models\OimsVehicle;
use App\Models\RouteCategoryReference;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\Oims\TeamRouteAssignment\TeamRouteAssignmentTrait;
use App\Traits\PopUpMessagesTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use TeamRouteAssignmentTrait, WithPagination, PopUpMessagesTrait;

    public $status_header_cards = [];
    public $stats;

    public $current;
    public $bid;
    public $totalTRK;
    public $totalTRKActive;
    public $totalTRKInactive;
    public $totalMT;
    public $getTotalMTActive;
    public $totalMTActive;
    public $totalMTInactive;
    public $totalFTE;
    public $totalFTEActive;
    public $totalFTEActiveD;
    public $totalFTEActiveC1;
    public $totalFTEActiveC2;
    public $totalFTEInactive;

    public $getRoutes = [];

    public $getCurrent = [];
    public $plate_id;
    public $did;
    public $cid1;
    public $cid2;

    public $rid;

    public $vcdd;

    public $getuserbranch;

    protected $listeners = ['mount' => 'mount', 'index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public $userid;
    
    public function mount()
    {
        $getub = UserDetails::where('user_id', Auth::user()->id)->first();
        $this->userid = $getub->user_id;
        $this->getuserbranch = $getub->branch_id;

        $this->getCurrent = OimsTeamRouteAssignment::with('teamDetails')->where('branch_id', $this->getuserbranch)->whereDate('dispatch_date', Carbon::today())->first();
        // dd($this->getCurrent->teamDetails['plate_no_id']);
        if ($this->getCurrent != null) {
            $this->current = $this->getCurrent->tra_no;
            $this->bid = $this->getCurrent->branch_id;
            $this->vcdd = $this->getCurrent->dispatch_date;
            // dd($this->cid);

            foreach ($this->getCurrent->teamDetails as $c => $getCurrents) {
                $this->plate_id[$c] = $getCurrents->plate_no_id;
                $this->did[$c] = $getCurrents->driver_id;
                $this->cid1[$c] = $getCurrents->checker1_id;
                $this->cid2[$c] = $getCurrents->checker2_id;

                $this->rid[$c] = $getCurrents->route_category_id;
            }
            // dd($this->did);
            // $this->plate_id = $this->getCurrent->branch_id;
        } else {
            $this->current = '-';
        }

        $getTotalTRK = OimsVehicle::with('teamDetails')->where('vehicle_type', 1)->get();
        $this->totalTRK = count($getTotalTRK);

        // dd($getTotalTRK);
        // $getTotalTRK = OimsVehicle::where('vehicle_type', 1)->get();
        if ($this->getCurrent != null) {
            $getTotalTRKActive = OimsVehicle::where('branch_id', $this->bid)->where('vehicle_type', 1)->whereIn('id', $this->plate_id)->get();
            $this->totalTRKActive = count($getTotalTRKActive);
            $this->totalTRKInactive = $this->totalTRK - $this->totalTRKActive;
        } else {
            $this->totalTRKActive = 0;
            $this->totalTRKInactive = 0;
        }


        $getTotalMT = OimsVehicle::with('teamDetails')->where('vehicle_type', 2)->get();
        $this->totalMT = count($getTotalMT);

        if ($this->getCurrent != null) {
            $getTotalMTActive = OimsVehicle::where('branch_id', $this->bid)->where('vehicle_type', 2)->whereIn('id', $this->plate_id)->get();

            $this->totalMTActive = count($getTotalMTActive);
            $this->totalMTInactive = ($this->totalMT - $this->totalMTActive);
        } else {
            $this->totalMTActive = 0;
            $this->totalMTInactive = 0;
        }


        $getTotalFTE = UserDetails::whereIn('position_id', [25, 26])->get();
        $this->totalFTE = count($getTotalFTE);

        if ($this->getCurrent != null) {
            $getTotalFTEActiveD = UserDetails::where('branch_id', $this->bid)->whereIn('user_id', $this->did)->get();
            $getTotalFTEActiveC1 = UserDetails::where('branch_id', $this->bid)->whereIn('user_id', $this->cid1)->get();
            $getTotalFTEActiveC2 = UserDetails::where('branch_id', $this->bid)->whereIn('user_id', $this->cid2)->get();

            $this->totalFTEActiveD = count($getTotalFTEActiveD);
            $this->totalFTEActiveC1 = count($getTotalFTEActiveC1);
            $this->totalFTEActiveC2 = count($getTotalFTEActiveC2);

            $this->totalFTEActive = $this->totalFTEActiveD + $this->totalFTEActiveC1 + $this->totalFTEActiveC2;
            $this->totalFTEInactive = $this->totalFTE - $this->totalFTEActive;
        } else {
            $this->totalFTEActive = 0;
            $this->totalFTEInactive = 0;
        }

        // dd($getTotalFTE);
        // if ($this->getCurrent != null) {
        $this->getRoutes = RouteCategoryReference::with(['teamDetails' => function ($query) {
            $query->withCount('teamPlateReference');
            $query->withCount('teamDriverReference');
            $query->withCount('teamChecker1Reference');
            $query->withCount('teamChecker2Reference');
            $query->with(['teamPlateReference' => function ($query2) {
                $query2->withCount('teamDetails');
            }]);
            $query->whereDate('dispatch_date', Carbon::today());
        }])->withCount(['teamDetails as truck' => function ($query) {
            $query->whereDate('dispatch_date', Carbon::today())
                ->where('vehicle_type', 1);
        }, 'teamDetails as motorcycle' => function ($query) {
            $query->whereDate('dispatch_date', Carbon::today())
                ->where('vehicle_type', 2);
        }])
            ->orderBy('name', 'ASC')->get();
        // dd($this->getRoutes);
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'print') {
            $this->print_modal = true;
        } elseif ($action_type == 'share') {
            $this->share_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('oims.order-management.team-route-assignment.edit', 'edit', $data['id']);
            $this->team_id = $data['id'];
            $this->edit_modal = true;
        }
        // else if ($action_type == 'update_status') {
        //     if ($data['status'] == 1) {
        //         $this->reactivate_modal = true;
        //         $this->team_id = $data['id'];
        //         return;
        //     }
        //     $this->deactivate_modal = true;
        //     $this->team_id = $data['id'];
        // }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'print') {
            $this->print_modal = false;
        } else if ($action_type == 'share') {
            $this->share_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } else if ($action_type == 'closeprint') {
            $this->print_modal = false;
        }
        // elseif ($action_type == 'update_status') {
        //     $this->reactivate_modal = false;
        //     $this->deactivate_modal = false;
        // }
    }

    public function actioncp($action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'closeprint') {
            $this->print_modal = false;
        }
    }

    public function refreshComponent()
    {
        $this->emit('$refresh');
    }

    public function load()
    {
        $this->loadStatusHeaderCards();
        $this->stats = 1;
    }

    public function loadStatusHeaderCards()
    {
        // $activestatus = OimsTeamRouteAssignmentDetails::whereDate('dispatch_date', Carbon::today())->get();
        // $inactivestatus = OimsTeamRouteAssignmentDetails::whereDate('dispatch_date', Carbon::today())->get();

        // $statusalls = OimsTeamRouteAssignmentDetails::get();

        $activestatus = OimsVehicle::with(['teamDetails' => function ($query) {
            $query->whereDate('dispatch_date', Carbon::today());
        }])->get();

        $activedata = [];
        $inactivedata = [];

        foreach ($activestatus as $val) {
            if (isset($val['teamDetails']['id'])) {
                $activedata[] = $val['id'];
            } else {
                $inactivedata[] = $val['id'];
            }
        }
        // dd(count($inactivedata));
        // $inactivestatus = OimsTeamRouteAssignmentDetails::whereDate('dispatch_date', Carbon::today())->get();

        $statusalls = OimsTeamRouteAssignmentDetails::get();

        $statusall = count($activedata) + count($inactivedata);
        $statusactive = count($activedata);
        $statusinactive = count($inactivedata);

        $this->status_header_cards = [

            [
                'title' => 'Active',
                'value' => ($this->totalTRKActive + $this->totalMTActive),
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 1,
            ],

            [
                'title' => 'Inactive',
                'value' => $statusinactive,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 2
            ],
            [
                'title' => 'All',
                'value' => $statusall,
                'class' => 'bg-white  border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => false
            ],
        ];
    }

    public $dispatch_date;
    public $branch_id;
    public $route_category_id;
    public $plate_no_id;
    public $driver_id;
    public $checker1_id;

    public function search(TeamRouteAssignmentInterface $team_interface)
    {
        $this->search_request = [
            'dispatch_date' => $this->dispatch_date,
            'branch_id' => $this->branch_id,
            'route_category_id' => $this->route_category_id,
            'plate_no_id' => $this->plate_no_id,
            'driver_id' => $this->driver_id,
            'checker1_id' => $this->checker1_id,
        ];
    }

    public function render(TeamRouteAssignmentInterface $team_interface)
    {
        $this->mount();

        // dd($this->search_request);

        $request = [
            'paginate' => $this->paginate,
            'stats' => $this->stats,
            'getuserbranch' => $this->getuserbranch,

        ];

        $response = $team_interface->index($request, $this->search_request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'team_routes' => [],
                ];
        }
        return view('livewire.oims.order-management.team-route-assignment.index', [
            'team_routes' => $response['result']['team_routes']
        ]);
    }
}
