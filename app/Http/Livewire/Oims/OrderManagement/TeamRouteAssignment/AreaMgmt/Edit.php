<?php

namespace App\Http\Livewire\Oims\OrderManagement\TeamRouteAssignment\AreaMgmt;

use App\Interfaces\Oims\AreaMgmt\AreaMgmtInterface;
use App\Models\OimsAreaReference;
use App\Traits\Oims\TeamRouteAssignment\AreaMgmt\AreaMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use AreaMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $areas = [];
    public $rid;

    public function mount($id)
    {
        $this->resetForm();
        $this->areas = OimsAreaReference::findOrFail($id);

        $this->name = $this->areas->name;
        $this->area_id = $id;

    }    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('oims.order-management.team-route-assignment.area-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(AreaMgmtInterface $area_mgmt_interface)
    {

        $response = $area_mgmt_interface->updateValidation($this->getRequest(),$this->area_id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(AreaMgmtInterface $area_mgmt_interface)
    {
        $response = $area_mgmt_interface->update($this->getRequest(), $this->area_id,);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('oims.order-management.team-route-assignment.area-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('oims.order-management.team-route-assignment.area-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.oims.order-management.team-route-assignment.area-mgmt.edit');
    }
}
