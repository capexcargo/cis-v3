<?php

namespace App\Http\Livewire\Oims\OrderManagement\TeamRouteAssignment\AreaMgmt;

use App\Interfaces\Oims\AreaMgmt\AreaMgmtInterface;
use App\Models\OimsAreaReference;
use App\Traits\Oims\TeamRouteAssignment\AreaMgmt\AreaMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use AreaMgmtTrait, WithPagination, PopUpMessagesTrait;

    public $status_header_cards = [];
    public $stats;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal = true;
        }elseif ($action_type == 'edit') {
            $this->emitTo('oims.order-management.team-route-assignment.area-mgmt.edit', 'edit', $data['id']);
            $this->area_id = $data['id'];
            $this->edit_modal = true;
        }else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->area_id = $data['id'];
                return;
            }
            $this->deactivate_modal = true;
            $this->area_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        } 
    }

    public function load()
    {
        $this->loadStatusHeaderCards();
        $this->stats = '';
    }

    public function loadStatusHeaderCards()
    {
        $activestatus = OimsAreaReference::where('status', 1)->get();
        $inactivestatus = OimsAreaReference::where('status', 2)->get();

        $statusalls = OimsAreaReference::get();
        // dd($forap);


        $statusall = $statusalls->count();
        $statusactive = $activestatus->count();
        $statusinactive = $inactivestatus->count();

        // $this->countall = $statusall;

        // dd($foraped);

        $this->status_header_cards = [
            [
                'title' => 'All',
                'value' => '',
                'class' => 'bg-white  border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => false
            ],
            [
                'title' => 'Acitive',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 1,
            ],
            [
                'title' => 'Inactive',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 2
            ],
            // [
            //     'title' => 'Declined',
            //     'value' => '',
            //     'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
            //     'color' => 'text-blue',
            //     'action' => 'stats',
            //     'id' => 2
            // ],
        ];
    }

    public function updateStatus($id, $value)
    {
        $areas = OimsAreaReference::findOrFail($id);

        $areas->update([
            'status' => $value,
        ]);


        if ($value == 1) {
            $this->emitTo('oims.order-management.team-route-assignment.area-mgmt.index', 'close_modal', 'update_status');
            $this->emitTo('oims.order-management.team-route-assignment.area-mgmt.index', 'index');
            $this->sweetAlert('', 'Area Successfully Reactivated!');
            return;
        }

        $this->emitTo('oims.order-management.team-route-assignment.area-mgmt.index', 'close_modal', 'update_status');
        $this->emitTo('oims.order-management.team-route-assignment.area-mgmt.index', 'index');
        $this->sweetAlert('', 'Area Successfully Deactivated!');
    }


    public function render(AreaMgmtInterface $area_mgmt_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'stats' => $this->stats,

        ];

        $response = $area_mgmt_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'area_names' => [],
                ];
        }
        return view('livewire.oims.order-management.team-route-assignment.area-mgmt.index', [
            'area_names' => $response['result']['area_names']
        ]);
    }
}
