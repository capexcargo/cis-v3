<?php

namespace App\Http\Livewire\Ticketing\SubcategoryManagement;

use App\Interfaces\Ticketing\TicketingSubcategoryManagementInterface;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Ticketing\TicketingSubcategoryManagementTrait;
use Livewire\Component;

class Edit extends Component
{
    use TicketingSubcategoryManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $ticket_subcategory;

    public function mount(TicketingSubcategoryManagementInterface $ticketing_subcategory_management_interface, $id)
    {
        $this->resetForm();

        $response = $ticketing_subcategory_management_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->ticket_subcategory = $response['result'];

        $this->category = $this->ticket_subcategory->category_id;
        $this->subcategory = $this->ticket_subcategory->name;
        $this->sla = $this->ticket_subcategory->sla;
        $this->severity = $this->ticket_subcategory->severity;
        $this->division = $this->ticket_subcategory->division_id;
        $this->department = $this->ticket_subcategory->department_id;

        foreach ($this->ticket_subcategory->subcategories as $requirement) {
            $this->taskholders[] = [
                'id' => $requirement->id,
                'task_holder' => $requirement->user_id,
                'is_deleted' => false,
            ];
        }
    }

    public function submit(TicketingSubcategoryManagementInterface $ticketing_subcategory_management_interface)
    {
        $response = $ticketing_subcategory_management_interface->update($this->getRequest(), $this->ticket_subcategory->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('ticketing.subcategory-management.index', 'close_modal', 'edit');
            $this->emitTo('ticketing.subcategory-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.ticketing.subcategory-management.edit');
    }
}
