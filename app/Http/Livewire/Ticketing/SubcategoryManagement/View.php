<?php

namespace App\Http\Livewire\Ticketing\SubcategoryManagement;

use App\Interfaces\Ticketing\TicketingSubcategoryManagementInterface;
use App\Traits\Ticketing\TicketingSubcategoryManagementTrait;
use Livewire\Component;

class View extends Component
{
    use TicketingSubcategoryManagementTrait;

    protected $listeners = ['view' => 'mount'];

    public function mount(TicketingSubcategoryManagementInterface $ticketing_subcategory_management_interface, $id)
    {
        $response = $ticketing_subcategory_management_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->ticket_subcategory = $response['result'];

        $this->category = $this->ticket_subcategory->category->name;
        $this->subcategory = $this->ticket_subcategory->name;
        $this->sla = $this->ticket_subcategory->sla;
        $this->severity = $this->ticket_subcategory->severity;
        $this->division = $this->ticket_subcategory->division->name;
        $this->department = $this->ticket_subcategory->department->display;

        foreach ($this->ticket_subcategory->subcategories as $i => $requirement) {
            $this->taskholders[$i] = [
                'id' => $requirement->id,
                'task_holder' => $requirement->user->name,
                'is_deleted' => false,
            ];
        }
    }

    public function render()
    {
        return view('livewire.ticketing.subcategory-management.view');
    }
}
