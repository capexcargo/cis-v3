<?php

namespace App\Http\Livewire\Ticketing\SubcategoryManagement;

use App\Interfaces\Ticketing\TicketingSubcategoryManagementInterface;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Ticketing\TicketingSubcategoryManagementTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use TicketingSubcategoryManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('ticketing.subcategory-management.edit', 'edit', $data['id']);
            $this->subcategory_id = $data['id'];
            $this->edit_modal = true;
        } elseif ($action_type == 'view') {
            $this->emitTo('ticketing.subcategory-management.view', 'view', $data['id']);
            $this->subcategory_id = $data['id'];
            $this->view_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function render(TicketingSubcategoryManagementInterface $ticketing_subcategory_management_interface)
    {
        $request = [
            // 'name' => $this->name,
            // 'division_id' => $this->division_id,
            'paginate' => $this->paginate,
        ];

        $response = $ticketing_subcategory_management_interface->index($request);
        // dd($response);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'subcategory_managements' => [],
                ];
        }

        return view('livewire.ticketing.subcategory-management.index', [
            'subcategory_managements' => $response['result']['subcategory_managements']
        ]);
    }
}
