<?php

namespace App\Http\Livewire\Ticketing\CategoryManagement;

use App\Interfaces\Ticketing\TicketingCategoryMangementInterface;
use App\Models\Ticketing\TicketCategoryManagement;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Ticketing\TicketingCategoryManagementTrait;
use Livewire\Component;

class Edit extends Component
{
    use TicketingCategoryManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['ticketing_category_management_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->ticketing_category_management = TicketCategoryManagement::findOrFail($id);

        $this->name = $this->ticketing_category_management->name;
        $this->division_id = $this->ticketing_category_management->division_id;
    }

    public function submit(TicketingCategoryMangementInterface $ticketing_category_management_interface)
    {
        $response = $ticketing_category_management_interface->update($this->getRequest(), $this->ticketing_category_management->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('ticketing.category-management.index', 'close_modal', 'edit');
            $this->emitTo('ticketing.category-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render()
    {
        return view('livewire.ticketing.category-management.edit');
    }
}
