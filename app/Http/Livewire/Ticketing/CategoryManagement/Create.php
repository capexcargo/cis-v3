<?php

namespace App\Http\Livewire\Ticketing\CategoryManagement;

use App\Interfaces\Ticketing\TicketingCategoryMangementInterface;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Ticketing\TicketingCategoryManagementTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Create extends Component
{
    use TicketingCategoryManagementTrait, PopUpMessagesTrait;

    protected $rules = [
        'name' => 'required',
        'division_id' => 'required',
    ];

    public function confirmationSubmit()
    {
        $this->validate();
        $this->confirmation_modal = true;
    }

    public function mount(){

        $this->divisionid = Auth::user()->division_id;
        $this->levelid = Auth::user()->level_id;

        if($this->levelid != 5){
            $this->division_id = Auth::user()->division_id;
        }

    }

    public function submit(TicketingCategoryMangementInterface $ticketing_category_management_interface)
    {
        $response = $ticketing_category_management_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('ticketing.category-management.index', 'close_modal', 'create');
            $this->emitTo('ticketing.category-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.ticketing.category-management.create');
    }
}
