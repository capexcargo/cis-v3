<?php

namespace App\Http\Livewire\Ticketing\CategoryManagement;

use App\Interfaces\Ticketing\TicketingCategoryMangementInterface;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Ticketing\TicketingCategoryManagementTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use TicketingCategoryManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('ticketing_category_management_mount', 'edit', $data['id']);
            $this->category_id = $data['id'];
            $this->edit_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }


    public function render(TicketingCategoryMangementInterface $ticketing_category_management_interface)
    {
        $request = [
            'name' => $this->name,
            'division_id' => $this->division_id,
            'paginate' => $this->paginate,
        ];

        $response = $ticketing_category_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'category_managements' => [],
                ];
        }
        return view('livewire.ticketing.category-management.index', [
            'category_managements' => $response['result']['category_managements']
        ]);
    }
}
