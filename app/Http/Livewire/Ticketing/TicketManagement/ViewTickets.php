<?php

namespace App\Http\Livewire\Ticketing\TicketManagement;

use App\Models\Ticketing\TicketManagement;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Ticketing\TicketManagementTrait;
use Livewire\Component;
use Livewire\WithPagination;

class ViewTickets extends Component
{
    use TicketManagementTrait, WithPagination, PopUpMessagesTrait;

    public $tickets_management;

    protected $listeners = ['view' => 'mount'];

    public function mount()
    {
        $this->tickets_management = TicketManagement::get();
    }

    public function render()
    {
        return view('livewire.ticketing.ticket-management.view-tickets');
    }
}
