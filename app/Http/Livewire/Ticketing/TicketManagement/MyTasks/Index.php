<?php

namespace App\Http\Livewire\Ticketing\TicketManagement\MyTasks;


use App\Interfaces\Ticketing\TicketManagementInterface;
use App\Models\Ticketing\TicketManagement;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Ticketing\TicketManagementTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use TicketManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'refresh_header_cards' => 'mount'];

    public $division_id;
    public $closer;

    public $count_in_progress_tickets;
    public $overdue_tickets_task_holder;
    public $count_overdue = [];

    public function mount()
    {
        $this->in_progress_tickets = TicketManagement::whereIn('final_status', [1, 2])
            // ->when(Auth::user()->level_id != 5, function ($query) {
            //     $query->where('task_holder', Auth::user()->id);
            // })
            ->where('task_holder', Auth::user()->id)
            ->get();

        $this->overdue_tickets_task_holder = TicketManagement::
            // when(Auth::user()->level_id != 5, function ($query) {
            //     $query->where('task_holder', Auth::user()->id);
            // })

            where('task_holder', Auth::user()->id)->get();

        foreach ($this->overdue_tickets_task_holder as $i => $ticket) {

            if ($ticket['actual_end_date'] != null) {
                if (date_diff(date_create($ticket['target_end_date']), date_create($ticket['actual_end_date']))->format('%R%a') > 0) {
                    $this->count_overdue[$i] = date_diff(date_create($ticket['target_end_date']), date_create($ticket['actual_end_date']))->format('%a Day/s');
                }
            } else {
                if ((date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%R%a') < 0 ? 0 : date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%a')) > 0) {
                    $this->count_overdue[$i] = (date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%R%a') < 0 ? 0 : date_diff(date_create($ticket['target_end_date']), date_create(date('Y-m-d')))->format('%a Day/s'));
                    // $this->count_overdue[$i] = "else";
                }
            }
        }
    }

    public function load()
    {
        $this->categories();
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create') {
            $this->emit('generate_ticket_reference');
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('ticketing.ticket-management.my-tasks.edit', 'edit', $data['id']);
            $this->ticket_id = $data['id'];
            $this->edit_modal = true;
        } elseif ($action_type == 'close') {
            $this->confirmation_modal = true;
            $this->confirmation_message = "Are you sure you want to close this ticket?";
            $this->ticket_id = $data['id'];
            $this->closer = $data['closer'];
        } elseif ($action_type == 'view_remarks') {
            $this->view_modal = true;
            $this->emitTo('ticketing.ticket-management.my-tasks.view', 'view', $data['id']);
            $this->ticket_id = $data['id'];
        }
    }

    public function submit(TicketManagementInterface $ticketing_management_interface)
    {
        $response = $ticketing_management_interface->close($this->ticket_id, $this->closer);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('ticketing.ticket-management.my-tasks.index', 'close_modal', 'close');
            $this->emitTo('ticketing.ticket-management.my-tasks', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'close') {
            $this->confirmation_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render(TicketManagementInterface $ticket_management_interface)
    {
        $request = [
            'division_id' => $this->division_id,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'ticket_ref_no' => $this->ticket_ref_no,
            'subject' => $this->subject,
            'date_created' => $this->date_created,
            'date_closed' => $this->date_closed,
            'ticket_status' => $this->ticket_status,
            'category' => $this->category,
            'task_holder' => $this->task_holder,
            'paginate' => $this->paginate,
        ];

        $response = $ticket_management_interface->index_my_tasks($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'tickets_management' => [],
                ];
        }

        return view('livewire.ticketing.ticket-management.my-tasks.index', [
            'tickets_management' => $response['result']['tickets_management']
        ]);
    }
}
