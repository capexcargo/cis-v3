<?php

namespace App\Http\Livewire\Ticketing\TicketManagement\MyTasks;

use App\Interfaces\Ticketing\TicketManagementInterface;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Ticketing\TicketManagementTrait;
use Livewire\Component;

class View extends Component
{
    use TicketManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['view' => 'mount'];

    public function mount(TicketManagementInterface $ticketing_management_interface, $id)
    {
        $response = $ticketing_management_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->tickets = $response['result'];

        $this->remarks = $this->tickets->remarks;
    }

    public function render()
    {
        return view('livewire.ticketing.ticket-management.my-tasks.view');
    }
}
