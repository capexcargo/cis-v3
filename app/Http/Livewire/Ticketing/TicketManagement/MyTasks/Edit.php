<?php

namespace App\Http\Livewire\Ticketing\TicketManagement\MyTasks;

use App\Interfaces\Ticketing\TicketManagementInterface;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Ticketing\TicketManagementTrait;
use Livewire\Component;

class Edit extends Component
{
    use TicketManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public function mount(TicketManagementInterface $ticketing_management_interface, $id)
    {
        $response = $ticketing_management_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->tickets = $response['result'];

        $this->actual_start_date = $this->tickets->actual_start_date;
        $this->remarks = $this->tickets->remarks;
    }

    public function submit(TicketManagementInterface $ticketing_management_interface)
    {
        $response = $ticketing_management_interface->updateActualDate($this->getActualDates(), $this->tickets->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('ticketing.ticket-management.my-tasks.index', 'close_modal', 'edit');
            $this->emitTo('ticketing.ticket-management.my-tasks.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.ticketing.ticket-management.my-tasks.edit');
    }
}
