<?php

namespace App\Http\Livewire\Ticketing\TicketManagement\MyTasks;

use Livewire\Component;

class Create extends Component
{
    public function render()
    {
        return view('livewire.ticketing.ticket-management.my-tasks.create');
    }
}
