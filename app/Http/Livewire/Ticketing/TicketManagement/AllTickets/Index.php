<?php

namespace App\Http\Livewire\Ticketing\TicketManagement\AllTickets;

use App\Interfaces\Ticketing\TicketManagementInterface;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Ticketing\TicketManagementTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use TicketManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public $division_id;

    public function load()
    {
        $this->categories();
        $this->task_holders();
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create') {
            $this->emit('generate_ticket_reference');
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('ticketing.ticket-management.all-tickets.edit', 'edit', $data['id']);
            $this->ticket_id = $data['id'];
            $this->edit_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function render(TicketManagementInterface $ticket_management_interface)
    {
        $request = [
            'date_created' => $this->date_created,
            'division_id' => $this->division_id,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'category' => $this->category,
            'task_holder' => $this->task_holder,
            'paginate' => $this->paginate,
        ];

        $response = $ticket_management_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'tickets_management' => [],
                ];
        }

        return view('livewire.ticketing.ticket-management.all-tickets.index', [
            'tickets_management' => $response['result']['tickets_management']
        ]);
    }
}
