<?php

namespace App\Http\Livewire\Ticketing\TicketManagement\AllTickets;

use App\Interfaces\Ticketing\TicketManagementInterface;
use App\Models\Ticketing\TaskHolder;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Ticketing\TicketManagementTrait;
use Livewire\Component;

class Edit extends Component
{
    use TicketManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $tickets;

    public function mount(TicketManagementInterface $ticketing_management_interface, $id)
    {
        $this->resetForm();

        $response = $ticketing_management_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->tickets = $response['result'];

        $this->task_holder = $this->tickets->task_holder;
        $this->division = $this->tickets->division_id;
        $this->category = $this->tickets->category_id;
        $this->subcategory = $this->tickets->subcategory_id;

        $this->target_start_date = $this->tickets->target_start_date;
        $this->target_end_date = $this->tickets->target_end_date;
        // dd($this->task_holder);
        $this->taskholder_references = TaskHolder::where('division_id', $this->division)
            ->where('category_id', $this->category)
            ->where('subcategory_id', $this->subcategory)
            ->get();
    }

    public function submit(TicketManagementInterface $ticketing_management_interface)
    {
        // dd($this->getTaskholder());

        $response = $ticketing_management_interface->update($this->getTaskholder(), $this->tickets->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('ticketing.ticket-management.all-tickets.index', 'close_modal', 'edit');
            $this->emitTo('ticketing.ticket-management.all-tickets.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.ticketing.ticket-management.all-tickets.edit');
    }
}
