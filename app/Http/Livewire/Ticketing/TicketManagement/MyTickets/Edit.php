<?php

namespace App\Http\Livewire\Ticketing\TicketManagement\MyTickets;

use App\Interfaces\Ticketing\TicketManagementInterface;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Ticketing\TicketManagementTrait;
use Livewire\Component;

class Edit extends Component
{
    use TicketManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public function mount(TicketManagementInterface $ticketing_management_interface, $id)
    {
        $response = $ticketing_management_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->tickets = $response['result'];

        $this->actual_start_date = $this->tickets->actual_start_date;
        $this->remarks = $this->tickets->remarks;
    }

    public function render()
    {
        return view('livewire.ticketing.ticket-management.my-tickets.edit');
    }
}
