<?php

namespace App\Http\Livewire\Ticketing\TicketManagement\MyTickets;

use App\Interfaces\Ticketing\TicketManagementInterface;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Ticketing\TicketManagementTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use TicketManagementTrait, WithFileUploads, PopUpMessagesTrait;

    protected $listeners = ['generate_ticket_reference' => 'generateTicketReference'];

    public function confirmationSubmit()
    {
        if ($this->division == 1) {
            $rules = [
                'ticket_ref_no' => 'required',
                'division' => 'required',
                'subject' => 'required',
                'category' => 'required',
                'subcategory' => 'required',
                'message' => 'sometimes',
                'concern_reference' => 'required',
                'concern_ref_url' => 'required',
                'attachments' => 'nullable',
                'attachments.*.attachment' => 'nullable|' . config('filesystems.validation_all'),
            ];

            $validated = $this->validate(
                $rules,
                [
                    'concern_ref_url.required' => 'The Concern Reference URL field is required.',
                    'attachments.*.attachment.required' => 'The attachment field is required.',
                    'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
                ]
            );
        } else {
            $rules = [
                'ticket_ref_no' => 'required',
                'division' => 'required',
                'subject' => 'required',
                'category' => 'required',
                'subcategory' => 'required',
                'message' => 'sometimes',
                'attachments' => 'nullable',
                'attachments.*.attachment' => 'nullable|' . config('filesystems.validation_all'),
            ];

            $validated = $this->validate(
                $rules,
                [
                    'attachments.*.attachment.required' => 'The attachment field is required.',
                    'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
                ]
            );
        }
        $this->confirmation_modal = true;
    }

    public function submit(TicketManagementInterface $ticket_management_interface)
    {
        $response = $ticket_management_interface->create($this->getRequest(), $this->division);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('ticketing.ticket-management.my-tickets.index', 'close_modal', 'create');
            $this->emitTo('ticketing.ticket-management.my-tickets.index', 'index');

            $this->emitTo('ticketing.ticket-management.index', 'close_modal', 'create');
            $this->emitTo('ticketing.ticket-management.index', 'index');
            $this->emitTo('ticketing.ticket-management.index', 'refresh_dashboard');
            $this->emit('reload_index');

            $this->emitTo('ticketing.ticket-management.all-tickets.index', 'close_modal', 'create');
            $this->emitTo('ticketing.ticket-management.all-tickets.index', 'index');

            $this->emitTo('ticketing.ticket-management.my-tasks.index', 'close_modal', 'create');
            $this->emitTo('ticketing.ticket-management.my-tasks.index', 'index');
            $this->emitTo('ticketing.ticket-management.my-tasks.index', 'refresh_header_cards');

            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.ticketing.ticket-management.my-tickets.create');
    }
}
