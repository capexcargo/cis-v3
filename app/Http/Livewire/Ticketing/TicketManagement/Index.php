<?php

namespace App\Http\Livewire\Ticketing\TicketManagement;

use App\Interfaces\Ticketing\TicketManagementInterface;
use App\Repositories\Ticketing\TicketManagementRepository;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Ticketing\TicketManagementTrait;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use TicketManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'refresh_dashboard' => 'mount'];

    public $chartLine;
    public $chartPie;
    public $chartPie2;

    public $date_from;
    public $date_to;

    public $ticket_category_data = [];
    public $sla_compliance_data = [];

    public $days_in_tickets = [];
    public $total_weekly_tickets = [];
    public $total_closed_weekly_tickets = [];

    public function mount(TicketManagementInterface $ticket_management_interface)
    {
        $filter = [
            'date_from' => $this->date_from,
            'date_to' => $this->date_to,
        ];

        $response = $ticket_management_interface->index_dashboard($filter);
        // dd($response['result']['ticket_categories']);

        $this->total_tickets = $response['result']['total_tickets'];
        $this->open_tickets = $response['result']['open_tickets'];
        $this->closed_tickets = $response['result']['closed_tickets'];
        $this->ticket_categories = $response['result']['ticket_categories'];
        $this->tech_supp = $response['result']['tech_supp'];
        $this->help_desk = $response['result']['help_desk'];


        $this->day1_tickets = $response['result']['days_tickets']['day1_tickets'];
        $this->day2_tickets = $response['result']['days_tickets']['day2_tickets'];
        $this->day3_tickets = $response['result']['days_tickets']['day3_tickets'];
        $this->day4_tickets = $response['result']['days_tickets']['day4_tickets'];
        $this->day5_tickets = $response['result']['days_tickets']['day5_tickets'];
        $this->day6_tickets = $response['result']['days_tickets']['day6_tickets'];
        $this->day7_tickets = $response['result']['days_tickets']['day7_tickets'];

        $this->weekly_total1_tickets = count($response['result']['weeklys_total_tickets']['weekly_total1_tickets']);
        $this->weekly_total2_tickets = count($response['result']['weeklys_total_tickets']['weekly_total2_tickets']);
        $this->weekly_total3_tickets = count($response['result']['weeklys_total_tickets']['weekly_total3_tickets']);
        $this->weekly_total4_tickets = count($response['result']['weeklys_total_tickets']['weekly_total4_tickets']);
        $this->weekly_total5_tickets = count($response['result']['weeklys_total_tickets']['weekly_total5_tickets']);
        $this->weekly_total6_tickets = count($response['result']['weeklys_total_tickets']['weekly_total6_tickets']);
        $this->weekly_total7_tickets = count($response['result']['weeklys_total_tickets']['weekly_total7_tickets']);

        $this->weekly_closed1_tickets = count($response['result']['weeklys_closed_tickets']['weekly_closed1_tickets']);
        $this->weekly_closed2_tickets = count($response['result']['weeklys_closed_tickets']['weekly_closed2_tickets']);
        $this->weekly_closed3_tickets = count($response['result']['weeklys_closed_tickets']['weekly_closed3_tickets']);
        $this->weekly_closed4_tickets = count($response['result']['weeklys_closed_tickets']['weekly_closed4_tickets']);
        $this->weekly_closed5_tickets = count($response['result']['weeklys_closed_tickets']['weekly_closed5_tickets']);
        $this->weekly_closed6_tickets = count($response['result']['weeklys_closed_tickets']['weekly_closed6_tickets']);
        $this->weekly_closed7_tickets = count($response['result']['weeklys_closed_tickets']['weekly_closed7_tickets']);

        $this->count_overdue = $response['result']['count_overdue'];
        $this->within_sla = count($response['result']['within_sla']);
        $this->beyond_sla = count($response['result']['beyond_sla']);

        $this->ticket_category_data = [
            $this->tech_supp,
            $this->help_desk
        ];

        $this->sla_compliance_data = [
            $this->within_sla,
            $this->beyond_sla
        ];

        $this->days_in_tickets = [
            $this->day1_tickets,
            $this->day2_tickets,
            $this->day3_tickets,
            $this->day4_tickets,
            $this->day5_tickets,
            $this->day6_tickets,
            $this->day7_tickets,
        ];

        $this->total_weekly_tickets = [
            $this->weekly_total1_tickets,
            $this->weekly_total2_tickets,
            $this->weekly_total3_tickets,
            $this->weekly_total4_tickets,
            $this->weekly_total5_tickets,
            $this->weekly_total6_tickets,
            $this->weekly_total7_tickets,
        ];

        $this->total_closed_weekly_tickets = [
            $this->weekly_closed1_tickets,
            $this->weekly_closed2_tickets,
            $this->weekly_closed3_tickets,
            $this->weekly_closed4_tickets,
            $this->weekly_closed5_tickets,
            $this->weekly_closed6_tickets,
            $this->weekly_closed7_tickets,
        ];
    }

    public function load()
    {
        $this->render();
    }

    public function searchDateRange()
    {
        $ticket_management_interface = new TicketManagementRepository;

        $filter = [
            'date_from' => $this->date_from,
            'date_to' => $this->date_to,
        ];

        // $start = Carbon::parse($this->date_from);
        // $end = Carbon::parse($this->date_to);
        // $dates = [];
        // while ($start->lte($end)) {
        //     $dates[] = $start->toDateString();
        //     $start->addDay();
        // }

        $response = $ticket_management_interface->index_dashboard($filter);

        // dd($response);

        $this->ticket_category_data = [
            $this->tech_supp = $response['result']['tech_supp'],
            $this->help_desk = $response['result']['help_desk'],
        ];

        $this->sla_compliance_data = [
            $this->within_sla = count($response['result']['within_sla']),
            $this->beyond_sla = count($response['result']['beyond_sla']),
        ];

        $this->days_in_tickets = [
            $this->day1_tickets,
            $this->day2_tickets,
            $this->day3_tickets,
            $this->day4_tickets,
            $this->day5_tickets,
            $this->day6_tickets,
            $this->day7_tickets,
        ];

        $this->total_weekly_tickets = [
            $this->weekly_total1_tickets = count($response['result']['weeklys_total_tickets']['weekly_total1_tickets']),
            $this->weekly_total2_tickets = count($response['result']['weeklys_total_tickets']['weekly_total2_tickets']),
            $this->weekly_total3_tickets = count($response['result']['weeklys_total_tickets']['weekly_total3_tickets']),
            $this->weekly_total4_tickets = count($response['result']['weeklys_total_tickets']['weekly_total4_tickets']),
            $this->weekly_total5_tickets = count($response['result']['weeklys_total_tickets']['weekly_total5_tickets']),
            $this->weekly_total6_tickets = count($response['result']['weeklys_total_tickets']['weekly_total6_tickets']),
            $this->weekly_total7_tickets = count($response['result']['weeklys_total_tickets']['weekly_total7_tickets']),
        ];

        $this->total_closed_weekly_tickets = [
            $this->weekly_closed1_tickets = count($response['result']['weeklys_closed_tickets']['weekly_closed1_tickets']),
            $this->weekly_closed2_tickets = count($response['result']['weeklys_closed_tickets']['weekly_closed2_tickets']),
            $this->weekly_closed3_tickets = count($response['result']['weeklys_closed_tickets']['weekly_closed3_tickets']),
            $this->weekly_closed4_tickets = count($response['result']['weeklys_closed_tickets']['weekly_closed4_tickets']),
            $this->weekly_closed5_tickets = count($response['result']['weeklys_closed_tickets']['weekly_closed5_tickets']),
            $this->weekly_closed6_tickets = count($response['result']['weeklys_closed_tickets']['weekly_closed6_tickets']),
            $this->weekly_closed7_tickets = count($response['result']['weeklys_closed_tickets']['weekly_closed7_tickets']),
        ];

        $datas = [
            'ticket_category_data' => $this->ticket_category_data,
            'sla_compliance_data' => $this->sla_compliance_data,
            'days_in_tickets' => $this->days_in_tickets,
            'total_weekly_tickets' => $this->total_weekly_tickets,
            'total_closed_weekly_tickets' => $this->total_closed_weekly_tickets,
        ];

        $this->emit('updatedCategories', $datas);
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'view_total_tickets') {
            $this->view_modal = true;
        } else if ($action_type == 'create') {
            $this->emit('generate_ticket_reference');
            $this->create_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }
    }

    public function render()
    {
        return view('livewire.ticketing.ticket-management.index', [
            'days_in_tickets' => $this->days_in_tickets,
            'total_weekly_tickets' => $this->total_weekly_tickets,
            'total_closed_weekly_tickets' => $this->total_closed_weekly_tickets,

            'day1_tickets' => $this->day1_tickets,
            'day2_tickets' => $this->day2_tickets,
            'day3_tickets' => $this->day3_tickets,
            'day4_tickets' => $this->day4_tickets,
            'day5_tickets' => $this->day5_tickets,
            'day6_tickets' => $this->day6_tickets,
            'day7_tickets' => $this->day7_tickets,

            'weekly_total1_tickets' => $this->weekly_total1_tickets,
            'weekly_total2_tickets' => $this->weekly_total2_tickets,
            'weekly_total3_tickets' => $this->weekly_total3_tickets,
            'weekly_total4_tickets' => $this->weekly_total4_tickets,
            'weekly_total5_tickets' => $this->weekly_total5_tickets,
            'weekly_total6_tickets' => $this->weekly_total6_tickets,
            'weekly_total7_tickets' => $this->weekly_total7_tickets,

            'weekly_closed1_tickets' => $this->weekly_closed1_tickets,
            'weekly_closed2_tickets' => $this->weekly_closed2_tickets,
            'weekly_closed3_tickets' => $this->weekly_closed3_tickets,
            'weekly_closed4_tickets' => $this->weekly_closed4_tickets,
            'weekly_closed5_tickets' => $this->weekly_closed5_tickets,
            'weekly_closed6_tickets' => $this->weekly_closed6_tickets,
            'weekly_closed7_tickets' => $this->weekly_closed7_tickets,

            'ticket_category_data' => $this->ticket_category_data,
            'tech_supp' => $this->tech_supp,
            'help_desk' => $this->help_desk,

            'sla_compliance_data' => $this->sla_compliance_data,
            'within_sla' => $this->within_sla,
            'beyond_sla' => $this->beyond_sla,
        ]);
    }
}
