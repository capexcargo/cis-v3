<?php

namespace App\Http\Livewire\Crm\Sales\SalesCampaign\Summary;

use App\Interfaces\Crm\Sales\SalesCampaign\SummaryInterface;
use App\Models\Crm\CrmSalesCampaign;
use App\Traits\Crm\Sales\SalesCampaign\SummaryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Index extends Component
{
    use SummaryTrait, WithPagination, PopUpMessagesTrait, WithFileUploads;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal','mount'=>'mount'];

    // public function mount()
    // {
    //     $this->voucher_reference = uniqid();
    //     // dd($this->voucher_reference);
    // }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('crm.sales.sales-campaign.summary.edit', 'edit', $data['id']);
            $this->summary_id = $data['id'];
            $this->edit_modal = true;
        } else if ($this->action_type == 'delete') {
            $this->delete_modal = true;
            $this->emitTo('crm.sales.sales-campaign.summary.index', 'delete', $data['id']);
            $this->summary_id = $data['id'];
        } else if ($action_type == 'terms_view') {
            $this->emitTo('crm.sales.sales-campaign.summary.terms-view', 'terms_view', $data['id']);
            $this->summary_id = $data['id'];
            $this->terms_modal = true;
        } else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->summary_id = $data['id'];
            return;
            }

            $this->deactivate_modal = true;
            $this->summary_id = $data['id'];
        }
        else if ($action_type == 'audiencesegmentation') {
            return redirect()->route('crm.sales.audience-segmentation.index');
        }
        // dd($action_type);
    }

    public function confirmDelete(SummaryInterface $summary_interface)
    {
        // dd($this->summary_id);
        $response = $summary_interface->delete($this->summary_id);
        if ($response['code'] == 200) {
            // $this->resetForm();
            $this->emitTo('crm.sales.sales-campaign.summary.index', 'close_modal', 'delete');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
        // dd($response);
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        } elseif ($action_type == 'terms_view') {
            $this->terms_modal = false;
        } elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        }
    }

    public function updateStatus($id, $value)
    {
        // dd($value);
        $know = CrmSalesCampaign::findOrFail($id);
        $know->update([
            'status' => $value,
        ]);



        if ($value == 1) {

            $this->emitTo('crm.sales.sales-campaign.summary.index', 'close_modal', 'update_status');
            $this->emitTo('crm.sales.sales-campaign.summary.index', 'index');
            $this->sweetAlert('', 'Promo Voucher Successfully Reactivated!');
            return;

            // return redirect()->to(route('crm.sales.sales-campaign.summary.index'));

        }

        $this->emitTo('crm.sales.sales-campaign.summary.index', 'close_modal', 'update_status');
        $this->emitTo('crm.sales.sales-campaign.summary.index', 'index');
        $this->sweetAlert('', 'Promo Voucher Successfully Deactivated!');
    }
    public function render(SummaryInterface $summary_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $summary_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'summary' => [],
                ];
        }
        // else if ($response['code'] == 200) {

        //     if ($response['result']['summary']) {
        //         $this->status = $response['result']['summary']['status'];
        //     } else {
        //         $this->status = "";
        //     }
        //     dd($this->status);
        // }


        return view('livewire.crm.sales.sales-campaign.summary.index', [
            'summary' => $response['result']['summary']
        ]);
    }
}
