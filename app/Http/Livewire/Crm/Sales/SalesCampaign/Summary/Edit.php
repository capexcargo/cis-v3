<?php

namespace App\Http\Livewire\Crm\Sales\SalesCampaign\Summary;

use App\Interfaces\Crm\Sales\SalesCampaign\SummaryInterface;
use App\Models\Crm\CrmSalesCampaign;
use App\Traits\Crm\Sales\SalesCampaign\SummaryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use SummaryTrait, PopUpMessagesTrait, WithFileUploads;

    public function mount($id, SummaryInterface $summary_interface)
    {
        $response = $summary_interface->show($id);
        // dd($response);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $summary = $response['result'];

        $this->summary_id = $id;
        $this->name = $summary->name ?? null;
        $this->voucher_code = $summary->voucher_code ?? null;
        $this->discount_type = $summary->discount_type ?? null;
        (double)$this->discount_amount = number_format((double)$summary->discount_amount, 2,'.','') ?? null;
        $this->discount_percentage = $summary->discount_percentage ?? null;
        // $this->start_datetime = date('m/d/Y g:i:A', strtotime($summary->start_datetime));
        // $this->end_datetime = date('m/d/Y g:i:A', strtotime($summary->end_datetime));
        $this->start_datetime = $summary->start_datetime;
        $this->end_datetime = $summary->end_datetime;
        $this->maximum_usage = $summary->maximum_usage ?? null;
        $this->terms_conditon = $summary->terms_conditon ?? null;
        $this->status = $summary->status ?? null;

        $this->summary_attachment = CrmSalesCampaign::with('Salesattachments')->findOrFail($id);

        foreach ($this->summary_attachment->Salesattachments as $attachment) {
            // dd($attachment);
            $this->attachments[] = [
                'id' => $attachment->id,
                'attachment' => '',
                'sales_campaign_id'=>$attachment->sales_campaign_id,
                'path' => $attachment->path,
                'name' => $attachment->name,
                'extension' => $attachment->extension,
                'is_deleted' => false,
            ];
        }
        // dd($this->summary_attachment);
    }

    public function validateSubmit(SummaryInterface $summary_interface)
    {
        $response = $summary_interface->update($this->getRequest(), $this->summary_id);
        // dd($response);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.sales-campaign.summary.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } 
         elseif ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.crm.sales.sales-campaign.summary.edit');
    }
}
