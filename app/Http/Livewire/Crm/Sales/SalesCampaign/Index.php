<?php

namespace App\Http\Livewire\Crm\Sales\SalesCampaign;

use App\Interfaces\Crm\Sales\SalesCampaign\SummaryInterface;
use App\Traits\Crm\Sales\SalesCampaign\SummaryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use SummaryTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];
    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } else if ($action_type == 'terms_view') {
            $this->emitTo('crm.sales.sales-campaign.terms-view', 'terms', $data['id']);
            $this->summary_id = $data['id'];
            $this->terms_modal = true;
        } else if ($action_type == 'audiencesegmentation') {
            return redirect()->route('crm.sales.audience-segmentation.index');
        }
        // dd($action_type);
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'terms_view') {
            $this->terms_modal = false;
        }
    }

    public function render(SummaryInterface $summary_interface)

    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $summary_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'summary' => [],
                ];
        }
        // else if ($response['code'] == 200) {

        //     if ($response['result']['summary']) {
        //         $this->status = $response['result']['summary']['status'];
        //     } else {
        //         $this->status = "";
        //     }
        //     dd($this->status);
        // }


        return view('livewire.crm.sales.sales-campaign.index', [
            'summary' => $response['result']['summary']
        ]);
    }
}
