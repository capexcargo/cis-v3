<?php

namespace App\Http\Livewire\Crm\Sales\SalesCampaign;

use App\Interfaces\Crm\Sales\SalesCampaign\SummaryInterface;
use App\Traits\Crm\Sales\SalesCampaign\SummaryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use SummaryTrait, PopUpMessagesTrait, WithFileUploads;

    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function validateSubmit()
    {
        // dd($this->discount_type);
        $rules = [
            'name' => 'required',
            'voucher_code' => 'required',
            'discount_type' => 'required',
            'discount_amount' => ($this->discount_type == 1 ? 'required' : 'sometimes'),
            'discount_percentage' => ($this->discount_type == 2 ? 'required' : 'sometimes'),
            // 'discount_amount' => 'sometimes',
            // 'discount_percentage' => 'sometimes',
            'start_datetime' => 'required',
            'end_datetime' => 'required',
            'maximum_usage' => 'required',
            'terms_conditon' => 'required',

            'attachments' => 'required',
            'attachments.*.attachment' => 'required|' . config('filesystems.crm_sales_campaign_validation'),
        ];

        $validated = $this->validate(
            $rules,
            [
                'name.required' => 'The Campaign Name field is required.',
                'voucher_code.required' => 'The Voucher Code field is required.',
                'discount_type.required' => 'The Type of Discount field is required.',
                'discount_amount.required' => 'The Discount Amount field is required.',
                'discount_percentage.required' => 'The Discount Percentage field is required.',

                'start_datetime.required' => 'The Start Date field is required.',
                'end_datetime.required' => 'The End Date field is required.',
                'maximum_usage.required' => 'The Maximum Usage field is required.',
                'terms_conditon.required' => 'The Terms and Conditions field is required.',
                'attachments.*.attachment.required' => 'The attachment field is required.',
                'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
            ]
        );
        // dd($validated);
        $this->confirmation_modal = true;
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.sales.sales-campaign.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function submit(SummaryInterface $summary_interface)
    {
        // dd($this->getRequest());
        $response = $summary_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.sales-campaign.index', 'close_modal', 'create');
            $this->emitTo('crm.sales.sales-campaign.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        if (!$this->attachments) {
            $this->attachments[] = [
                'id' => null,
                'attachment' => null,
                'path' => null,
                'name' => null,
                'extension' => null,
                'is_deleted' => false,
            ];
        }
        return view('livewire.crm.sales.sales-campaign.create');
    }
}
