<?php

namespace App\Http\Livewire\Crm\Sales\SalesCampaign;

use App\Interfaces\Crm\Sales\SalesCampaign\SummaryInterface;
use App\Traits\Crm\Sales\SalesCampaign\SummaryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class TermsView extends Component
{
    use SummaryTrait, PopUpMessagesTrait;

    public function mount($id, SummaryInterface $terms_interface)
    {
        $response = $terms_interface->show($id);
        // dd($response);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $terms = $response['result'];
        // dd($terms);
        $this->summary_id = $id;
        $this->terms_conditon = $terms->terms_conditon ?? null;
    }
    public function render()
    {
        return view('livewire.crm.sales.sales-campaign.terms-view');
    }
}
