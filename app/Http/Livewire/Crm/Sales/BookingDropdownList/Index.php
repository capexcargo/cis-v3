<?php

namespace App\Http\Livewire\Crm\Sales\BookingDropdownList;

use App\Interfaces\Crm\Sales\BookingDropdownList\ActivityType\ActivityTypeInterface;
use App\Models\CrmActivityType;
use App\Traits\Crm\Sales\BookingDropdownList\ActivityType\ActivityTypeTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;
use App\Traits\InitializeFirestoreTrait;

class Index extends Component
{
    use ActivityTypeTrait, WithPagination, PopUpMessagesTrait, InitializeFirestoreTrait;

    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $reactivate_modal = false;
    public $deactivate_modal = false;
    public $stats;
    public $confirmation_message;
    
    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } 
        elseif ($action_type == 'edit') {
            $this->emitTo('crm.sales.booking-dropdown-list.edit', 'edit', $data['id']);
            $this->act_id = $data['id'];
            $this->edit_modal = true;
        } 
        // else if ($action_type == 'delete') {
        //     $this->confirmation_message = "Are you sure you want to delete this Acitvity Type?";
        //     $this->delete_modal = true;
        //     $this->act_id = $data['id'];
        // }
        else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->act_id = $data['id'];
                return;
            }

            $this->deactivate_modal = true;
            $this->act_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } 
        else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
        // elseif ($action_type == 'delete') {
        //     $this->delete_modal = false;
        // }
        elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        }
    }

    public function updated()
    {
        $this->resetPage();

    }

    public function updateStatus($id, $value)
    {
        // dd($value);
        $know = CrmActivityType::findOrFail($id);
        $know->update([
            'status' => $value,
        ]);

        $collectionReference = $this->initializeFirestore()->collection('crm_activity_type_management'); 
    
        $query = $collectionReference->where('id', '=', intval($id));
        $documents = $query->documents();
    
        foreach ($documents as $document) {
            if ($document->exists()) {
                $documentId = $document->id();
                $collectionReference->document($documentId)->update([
                    ['path' => 'status', 'value' => $value]
                ]);
            }
        }

        if ($value == 1) {

            $this->emitTo('crm.sales.booking-dropdown-list.index', 'close_modal', 'update_status');
            $this->emitTo('crm.sales.booking-dropdown-list.index', 'index');
            $this->sweetAlert('', 'Acitvity type has been successfully reactivated!');
            return;

            // return redirect()->to(route('crm.sales.booking-dropdown-list.index'));

        }

        $this->emitTo('crm.sales.booking-dropdown-list.index', 'close_modal', 'update_status');
        $this->emitTo('crm.sales.booking-dropdown-list.index', 'index');
        $this->sweetAlert('', 'Acitvity type has been successfully deactivated!');


    }

    // public function confirm(ActivityTypeInterface $activity_interface)
    // {
    //     if ($this->action_type == "delete") {
    //         $response = $activity_interface->destroy($this->act_id);
    //         $this->emitTo('crm.sales.booking-dropdown-list.index', 'close_modal', 'delete');
    //         $this->emitTo('crm.sales.booking-dropdown-list.index', 'index');
    //     }

    //     if ($response['code'] == 200) {
    //         $this->emitTo('crm.sales.booking-dropdown-list.index', 'close_modal', 'delete');
    //         $this->emitTo('crm.sales.booking-dropdown-list.index', 'index');
    //         $this->sweetAlert('', $response['message']);
    //     } else {
    //         $this->sweetAlertError('error', $response['message'], $response['result']);
    //     }
    // }

    public function render(ActivityTypeInterface $activity_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'stats' => $this->stats,
        ];

        $response = $activity_interface->index($request, $this->getRequest());

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'act_s' => [],
                ];
        }

        return view('livewire.crm.sales.booking-dropdown-list.index', [
            'act_s' => $response['result']['act_s']
        ]);
    }

}
