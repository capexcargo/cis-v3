<?php

namespace App\Http\Livewire\Crm\Sales\BookingDropdownList;

use App\Interfaces\Crm\Sales\BookingDropdownList\ActivityType\ActivityTypeInterface;
use App\Traits\Crm\Sales\BookingDropdownList\ActivityType\ActivityTypeTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use ActivityTypeTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    // protected $rules = [
    //     'activity_type' => 'required',
    //     'pickup_execution_time_id' => 'required',
    //     'delivery_execution_time_id' => 'required',
    // ];

    // public function confirmationSubmit()
    // {
    //     $this->validate();

    //     $this->confirmation_modal = true;
    // }


    public function action(ActivityTypeInterface $activity_type_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'submit2') {
            $response = $activity_type_interface->createValidation($this->getRequest());
            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.sales.booking-dropdown-list.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }


    
    public function submit(ActivityTypeInterface $activity_interface)
    {
        // dd($this->getRequest());
        
        $response = $activity_interface->create($this->getRequest());
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.booking-dropdown-list.index', 'close_modal', 'create');
            $this->emitTo('crm.sales.booking-dropdown-list.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.sales.booking-dropdown-list.create');
    }
}
