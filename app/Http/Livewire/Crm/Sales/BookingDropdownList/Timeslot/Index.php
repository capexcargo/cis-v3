<?php

namespace App\Http\Livewire\Crm\Sales\BookingDropdownList\Timeslot;

use App\Interfaces\Crm\Sales\BookingDropdownList\Timeslot\TimeslotInterface;
use App\Traits\Crm\Sales\BookingDropdownList\Timeslot\TimeslotTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use TimeslotTrait, WithPagination, PopUpMessagesTrait;

    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $confirmation_message;

    public $name;
    public $times_id;


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } 
        elseif ($action_type == 'edit') {
            $this->emitTo('crm.commercials.commercial-mgmt.loa-mgmt.edit', 'edit', $data['id']);
            $this->times_id = $data['id'];
            $this->edit_modal = true;
        } 
        else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this Timeslot?";
            $this->delete_modal = true;
            $this->times_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } 
        else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
        elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }

    public function confirm(TimeslotInterface $time_interface)
    {
        if ($this->action_type == "delete") {
            $response = $time_interface->destroy($this->times_id);
            $this->emitTo('crm.sales.booking-dropdown-list.timeslot.index', 'close_modal', 'delete');
            $this->emitTo('crm.sales.booking-dropdown-list.timeslot.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('crm.sales.booking-dropdown-list.timeslot.index', 'close_modal', 'delete');
            $this->emitTo('crm.sales.booking-dropdown-list.timeslot.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(TimeslotInterface $time_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $time_interface->index($request, $this->getRequest());

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'time_s' => [],
                ];
        }

        return view('livewire.crm.sales.booking-dropdown-list.timeslot.index', [
            'time_s' => $response['result']['time_s']
        ]);
    }
}
