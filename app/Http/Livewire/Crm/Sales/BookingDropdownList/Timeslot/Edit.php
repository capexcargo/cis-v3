<?php

namespace App\Http\Livewire\Crm\Sales\BookingDropdownList\Timeslot;

use App\Interfaces\Crm\Sales\BookingDropdownList\Timeslot\TimeslotInterface;
use App\Models\CrmTimeslot;
use App\Traits\Crm\Sales\BookingDropdownList\Timeslot\TimeslotTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use TimeslotTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $name;
    public $times_s;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->times_s = CrmTimeslot::findOrFail($id);

        $this->name = $this->times_s->name;
    }

    public function action(TimeslotInterface $time_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'submit2') {
            $response = $time_interface->updateValidation($this->getRequest());
            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function submit(TimeslotInterface $time_interface)
    {
        $response = $time_interface->update($this->getRequest(), $this->times_s->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.booking-dropdown-list.timeslot.index', 'close_modal', 'edit');
            $this->emitTo('crm.sales.booking-dropdown-list.timeslot.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.sales.booking-dropdown-list.timeslot.edit');
    }
}
