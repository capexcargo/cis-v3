<?php

namespace App\Http\Livewire\Crm\Sales\BookingDropdownList;

use App\Interfaces\Crm\Sales\BookingDropdownList\ActivityType\ActivityTypeInterface;
use App\Models\CrmActivityType;
use App\Traits\Crm\Sales\BookingDropdownList\ActivityType\ActivityTypeTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use ActivityTypeTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $act_s;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->act_s = CrmActivityType::findOrFail($id);

        $this->activity_type = $this->act_s->activity_type;
        $this->pickup_execution_time_id = $this->act_s->pickup_execution_time_id;
        $this->delivery_execution_time_id = $this->act_s->delivery_execution_time_id;
    }

    // public function confirmationSubmit(ActivityTypeInterface $act_interface)
    // {
    //     $response = $act_interface->updateValidation($this->getRequest());

    //     if ($response['code'] == 200) {
    //         $this->confirmation_modal = true;
    //     } else if ($response['code'] == 400) {
    //         $this->resetErrorBag();
    //         foreach ($response['result']->getMessages() as $a => $messages) {
    //             if (is_array($messages)) {
    //                 foreach ($messages as $message) :
    //                     $this->addError($a, $message);
    //                 endforeach;
    //             } else {
    //                 $this->addError($a, $messages);
    //             }
    //         }
    //         return;
    //     } else {
    //         $this->sweetAlertError('error', $response['message'], $response['result']);
    //     }
    // }

    public function action(ActivityTypeInterface $act_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'submit2') {
            $response = $act_interface->updateValidation($this->getRequest());
            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function submit(ActivityTypeInterface $act_interface)
    {
        $response = $act_interface->update($this->getRequest(), $this->act_s->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.booking-dropdown-list.index', 'close_modal', 'edit');
            $this->emitTo('crm.sales.booking-dropdown-list.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.crm.sales.booking-dropdown-list.edit');
    }
}
