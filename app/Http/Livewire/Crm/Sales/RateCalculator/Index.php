<?php

namespace App\Http\Livewire\Crm\Sales\RateCalculator;

use App\Interfaces\Crm\Sales\RateCalculatorInterface;
use App\Models\CrmRateBoxDetails;
use App\Models\CrmRatePouchDetails;
use App\Traits\Crm\Sales\RateCalculatorTrait;
use Livewire\Component;

class Index extends Component
{
    use RateCalculatorTrait;

    public $action_type;

    public $type;
    public $type_header_cards = [];

    public $rate_calculator = [];

    public $is_compute = false;

    protected $rules = [
        'transport_mode' => 'required',
        'origin' => 'required',
        'destination' => 'required',
        'paymode' => 'required',
        'service_mode' => 'required',
    ];

    public function mount()
    {
        $this->loadTypeHeaderCards();
        $this->type = 1;

        $this->transportModeReferences();

        $this->addAirCargos();
        $this->addAirPouches();
        $this->addAirBoxes();

        $this->addSeaCargosLcl();
        $this->addSeaCargosFcl();
        $this->addSeaCargosRcl();
        $this->addSeaBoxes();

        $this->addLandsShippersBox();
        $this->addLandsPouches();
        $this->addLandsBoxes();
        $this->addLandsFtl();

        $this->movementSfTypeReferences();

        $this->addOtherServicesMeasurement();

        $this->loadServiceType();
    }

    public function action($data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToBooking') {
            return redirect()->route('crm.sales.booking-mgmt.index');
        }
    }

    public function ActiveType($type)
    {
        if ($type == 1) {
            $this->type = 1;
        } elseif ($type == 2) {
            $this->type = 2;
        }

        $this->loadTypeHeaderCards();
    }

    public function loadTypeHeaderCards()
    {
        $this->type = $this->type ?? false;

        $this->type_header_cards = [
            [
                'title' => 'Domestic',
                'class' => 'w-32 text-center bg-white text-gray-500 border border-gray-600 shadow-sm rounded-l-md',
                'color' => 'text-blue',
                'action' => 'types',
                'id' => 1
            ],
            [
                'title' => 'Other Services',
                'class' => 'w-32 text-center bg-white text-gray-500 border border-gray-600 shadow-sm rounded-r-md',
                'color' => 'text-blue',
                'action' => 'types',
                'id' => 2
            ],
        ];
    }

    public function loadServiceType()
    {
        $this->service_type_references = [
            [
                'name' => 'Domestic Freight',
                'id' => 1,
                'wire_model' => 'domestic_freight',
            ],
            [
                'name' => 'International Freight Forwarding (IFF)',
                'id' => 2,
                'wire_model' => 'iff',
            ],
            [
                'name' => 'Project Shipment',
                'id' => 3,
                'wire_model' => 'project_shipment',
            ],
            [
                'name' => 'Brokerage',
                'id' => 4,
                'wire_model' => 'brokerage',
            ],
            [
                'name' => 'FTL',
                'id' => 5,
                'wire_model' => 'ftl',
            ],
        ];
    }

    public function forCWT_CBM($trans_mode = 0)
    {
        // dd($this->movement_sf_type);
        $wt = 0;
        $cbm = 0;
        $dimension = 0;
        $amount = 0;

        if ($this->commodity_type == 1) {
            $divisor = 3300;
        } else {
            $divisor = 2800;
        }

        if ($trans_mode == 1) {
            foreach ($this->air_cargos as $i => $ac) {
                $wt = $this->air_cargos[$i]['wt'];
                $dimension = $this->air_cargos[$i]['dimension_l'] *  $this->air_cargos[$i]['dimension_w'] *  $this->air_cargos[$i]['dimension_h'] / $divisor;

                $this->air_cargos[$i]['cwt'] =  $wt > $dimension ? $wt : number_format($dimension, 2);
            }
        } elseif ($trans_mode == 2) {
            if ($this->movement_sf_type == 1) { // for LCL
                foreach ($this->sea_cargos_is_lcl as $i => $ac) {
                    $wt = $this->sea_cargos_is_lcl[$i]['wt'];
                    $cbm = $this->sea_cargos_is_lcl[$i]['dimension_l'] *  $this->sea_cargos_is_lcl[$i]['dimension_w'] *  $this->sea_cargos_is_lcl[$i]['dimension_h'] / 1000000;

                    $this->sea_cargos_is_lcl[$i]['cbm'] =  $wt > $cbm ? $wt : number_format($cbm, 2);
                }
            } elseif ($this->movement_sf_type == 3) { // for RCL
                foreach ($this->sea_cargos_is_rcl as $i => $ac) {
                    $wt = $this->sea_cargos_is_rcl[$i]['wt'];
                    $cbm = $this->sea_cargos_is_rcl[$i]['dimension_l'] *  $this->sea_cargos_is_rcl[$i]['dimension_w'] *  $this->sea_cargos_is_rcl[$i]['dimension_h'] / 1000000;

                    $this->sea_cargos_is_rcl[$i]['cbm'] =  $wt > $cbm ? $wt : number_format(($cbm), 2);
                }
            }
        }


        foreach ($this->other_services_measurements as $i => $osm) {
            $wt = $this->other_services_measurements[$i]['wt'];
            $dimension = $this->other_services_measurements[$i]['dimension_l'] *  $this->other_services_measurements[$i]['dimension_w'] *  $this->other_services_measurements[$i]['dimension_h'] / $divisor;

            $this->other_services_measurements[$i]['cbm'] =  $wt > $dimension ? $wt : number_format($dimension, 2);
        }
    }

    public function forAirPouchSize()
    {
        $rates = CrmRatePouchDetails::where('origin_id', $this->origin)
            ->where('destination_id', $this->destination)
            ->first();

        $amount_small = $rates->amount_small;
        $amount_medium = $rates->amount_medium;
        $amount_large = $rates->amount_large;

        foreach ($this->air_pouches as $i => $ap) {
            $qty = $this->air_pouches[$i]['qty'];

            if ($this->air_pouches[$i]['pouch_size'] == 1) {
                $this->air_pouches[$i]['amount'] =  $qty * $amount_small;
            } else if ($this->air_pouches[$i]['pouch_size'] == 2) {
                $this->air_pouches[$i]['amount'] =  $qty * $amount_medium;
            } else if ($this->air_pouches[$i]['pouch_size'] == 3) {
                $this->air_pouches[$i]['amount'] =  $qty * $amount_large;
            }
        }
    }

    public function forBoxWeight($trans_mode)
    {
        $this->validate();

        $rates = CrmRateBoxDetails::where('origin_id', $this->origin)
            ->where('destination_id', $this->destination)
            ->first();

        $amount_small = (int)$rates->amount_small;
        $amount_medium = (int)$rates->amount_medium;
        $amount_large = (int)$rates->amount_large;

        $exceeding_amount_small = $amount_small / 5;
        $exceeding_amount_medium = $amount_medium / 10;
        $exceeding_amount_large = $amount_large / 20;

        $exceeded_small_amount = 0;
        $exceeded_medium_amount = 0;
        $exceeded_large_amount = 0;
        $exceed_weight = 0;

        $boxes = [];

        if ($trans_mode == 1) {
            foreach ($this->air_boxes as $i => $ab) {
                $qty = (int)$this->air_boxes[$i]['qty'];
                $weight = (int)$this->air_boxes[$i]['weight'];

                if (!empty($this->air_boxes[$i]['weight'])) {
                    if ($this->air_boxes[$i]['box_size'] == 1) {
                        if ($weight > 5) {
                            $exceed_weight = $weight - 5;
                            $exceeded_small_amount = $exceed_weight * $exceeding_amount_small;
                        }
                        $this->air_boxes[$i]['amount'] =  ($qty * $amount_small) + $exceeded_small_amount;
                    } else if ($this->air_boxes[$i]['box_size'] == 2) {
                        if ($weight > 10) {
                            $exceed_weight = $weight - 10;
                            $exceeded_medium_amount = $exceed_weight * $exceeding_amount_medium;
                        }
                        $this->air_boxes[$i]['amount'] =  ($qty * $amount_medium) + $exceeded_medium_amount;
                    } else if ($this->air_boxes[$i]['box_size'] == 3) {
                        if ($weight > 20) {
                            $exceed_weight = $weight - 20;
                            $exceeded_large_amount = $exceed_weight * $exceeding_amount_large;
                        }
                        $this->air_boxes[$i]['amount'] =  ($qty * $amount_large) + $exceeded_large_amount;
                    }
                }
            }
        } elseif ($trans_mode == 2) {
            foreach ($this->sea_boxes as $i => $sb) {
                $qty = (int)$this->sea_boxes[$i]['qty'];
                $weight = (int)$this->sea_boxes[$i]['weight'];

                if (!empty($this->sea_boxes[$i]['weight'])) {
                    if ($this->sea_boxes[$i]['box_size'] == 1) {
                        if ($weight > 5) {
                            $exceed_weight = $weight - 5;
                            $exceeded_small_amount = $exceed_weight * $exceeding_amount_small;
                        }
                        $this->sea_boxes[$i]['amount'] =  ($qty * $amount_small) + $exceeded_small_amount;
                    } else if ($this->sea_boxes[$i]['box_size'] == 2) {
                        if ($weight > 10) {
                            $exceed_weight = $weight - 10;
                            $exceeded_medium_amount = $exceed_weight * $exceeding_amount_medium;
                        }
                        $this->sea_boxes[$i]['amount'] =  ($qty * $amount_medium) + $exceeded_medium_amount;
                    } else if ($this->sea_boxes[$i]['box_size'] == 3) {
                        if ($weight > 20) {
                            $exceed_weight = $weight - 20;
                            $exceeded_large_amount = $exceed_weight * $exceeding_amount_large;
                        }
                        $this->sea_boxes[$i]['amount'] =  ($qty * $amount_large) + $exceeded_large_amount;
                    }
                }
            }
        }
    }

    public function compute(RateCalculatorInterface $rate_calculator_interface)
    {
        $this->validate();

        $response = $rate_calculator_interface->compute($this->getRequest());

        $this->rate_calculator = $response['result'];

        foreach ($this->rate_calculator as $i => $rc) {
            $this->weight_charge = $this->rate_calculator['cbm_charge'] ?? null;
            $this->awb_fee = $this->rate_calculator['awb_fee'] ?? null;
            $this->cod_charge = $this->rate_calculator['cod_charge'] ?? null;
            $this->valuation = $this->rate_calculator['valuation'] ?? null;
            $this->insurance = $this->rate_calculator['insurance'] ?? null;
            $this->doc_fee = $this->rate_calculator['doc_fee'] ?? null;
            $this->handling_fee = $this->rate_calculator['handling_fee'] ?? null;
            $this->subtotal = $this->rate_calculator['subtotal'] ?? null;
            $this->grand_total = $this->rate_calculator['grand_total'] ?? null;
        }

        $this->is_compute = true;
    }

    public function osCompute(RateCalculatorInterface $rate_calculator_interface)
    {
        $response = $rate_calculator_interface->osCompute($this->osGetRequest());

        $this->rate_calculator = $response['result'];

        foreach ($this->rate_calculator as $i => $rc) {
            $this->os_weight_charge = $this->rate_calculator['cbm_charge'] ?? null;
            $this->os_awb_fee = $this->rate_calculator['awb_fee'] ?? null;
            $this->os_cod_charge = $this->rate_calculator['cod_charge'] ?? null;
            $this->os_valuation = $this->rate_calculator['valuation'] ?? null;
            $this->os_insurance = $this->rate_calculator['insurance'] ?? null;
            $this->os_doc_fee = $this->rate_calculator['doc_fee'] ?? null;
            $this->os_handling_fee = $this->rate_calculator['handling_fee'] ?? null;
            $this->os_subtotal = $this->rate_calculator['subtotal'] ?? null;
            $this->os_grand_total = $this->rate_calculator['grand_total'] ?? null;
        }

        $this->is_compute = true;
    }

    public function render()
    {
        return view('livewire.crm.sales.rate-calculator.index');
    }
}
