<?php

namespace App\Http\Livewire\Crm\Sales\RateCalculator\ChargesManagement;

use App\Interfaces\Crm\Sales\ChargesManagementInterface;
use App\Traits\Crm\Sales\ChargesManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use ChargesManagementTrait, PopUpMessagesTrait;

    public function confirmationSubmit(ChargesManagementInterface $charges_management_interface)
    {
        $response = $charges_management_interface->createValidation($this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeCreateModal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.sales.rate-calculator.charges-management.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function submit(ChargesManagementInterface $charges_management_interface)
    {
        $response = $charges_management_interface->create($this->getRequest());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.rate-calculator.charges-management.index', 'close_modal', 'create');
            $this->emitTo('crm.sales.rate-calculator.charges-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.sales.rate-calculator.charges-management.create');
    }
}
