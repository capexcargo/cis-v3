<?php

namespace App\Http\Livewire\Crm\Sales\RateCalculator\ChargesManagement;

use App\Interfaces\Crm\Sales\ChargesManagementInterface;
use App\Models\Crm\ChargesManagement;
use App\Traits\Crm\Sales\ChargesManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use ChargesManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id, ChargesManagementInterface $charges_management_interface)
    {
        $this->resetForm();
        $response = $charges_management_interface->show($id);
        $this->charges_management = $response['result'];

        $this->charge_id = $id;
        $this->rate_calcu_charges = $this->charges_management->rate_calcu_charges;
        $this->charges_category = $this->charges_management->charges_category;
    }

    public function submit(ChargesManagementInterface $charges_management_interface)
    {
        $response = $charges_management_interface->update($this->getRequest(), $this->charge_id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.rate-calculator.charges-management.index', 'close_modal', 'edit');
            $this->emitTo('crm.sales.rate-calculator.charges-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.sales.rate-calculator.charges-management.edit');
    }
}
