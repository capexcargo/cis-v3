<?php

namespace App\Http\Livewire\Crm\Sales\RateCalculator\ChargesManagement;

use App\Interfaces\Crm\Sales\ChargesManagementInterface;
use App\Models\Crm\ChargesManagement;
use App\Traits\Crm\Sales\ChargesManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use ChargesManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('crm.sales.rate-calculator.charges-management.edit', 'edit', $data['id']);
            $this->charge_id = $data['id'];
            $this->edit_modal = true;
            //
        } else if ($action_type == 'update_status') {
            if ($data['status'] == 2) {
                $this->confirmation_message = "Are you sure you want to activate this rate calculator charges?";
                $this->status = 1;
            } else {
                $this->confirmation_message = "Are you sure you want to deactivate this rate calculator charges?";
                $this->status = 2;
            }

            $this->update_status_modal = true;
            $this->charge_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'update_status') {
            $this->update_status_modal = false;
        }
    }

    public function confirm(ChargesManagementInterface $charges_management_interface)
    {
        if ($this->action_type == "update_status") {
            $response = $charges_management_interface->update_status($this->charge_id, $this->status);
            $this->emitTo('crm.sales.rate-calculator.charges-management.index', 'close_modal', 'update_status');
            $this->emitTo('crm.sales.rate-calculator.charges-management.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('crm.sales.rate-calculator.charges-management.index', 'close_modal', 'update_status');
            $this->emitTo('crm.sales.rate-calculator.charges-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(ChargesManagementInterface $charges_management_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $charges_management_interface->index($request);

        // dd($response);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'charges_list' => [],
                ];
        }

        return view('livewire.crm.sales.rate-calculator.charges-management.index', [
            'charges_list' => $response['result']['charges_list']
        ]);
    }
}
