<?php

namespace App\Http\Livewire\Crm\Sales\Quota;

use App\Interfaces\Crm\Sales\Quota\TargetInterface;
use App\Models\Crm\StakeholderCategory;
use App\Models\MonthReference;
use Livewire\Component;

class Index extends Component
{
    public $months_references = [];

    public $header_card;
    public $stake_holder;
    public $stake_holder_header_cards = [];

    public $action_type;
    public $corporate_target_modal = false;
    public $stakeholder_target_modal = false;
    public $edit_modal = false;

    public $stakeholder_id;

    public $stakeholder;
    public $month;
    public $year;

    public $total_days_count;
    public $monthly_target_amount;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function mount()
    {
        $this->loadTypeHeaderCards();
        $this->stake_holder = "";

        $this->header_card = 2;

        $this->months_references = MonthReference::get();
    }

    public function headerCard($header)
    {
        if ($header == 1) {
            $this->header_card = 1;
        } elseif ($header == 2) {
            $this->header_card = 2;
        }
    }

    public function ActiveType($stake_holder)
    {

        $this->stake_holder = $stake_holder;

        // if ($stake_holder == "") {
        //     $this->stake_holder = "";
        // } elseif ($stake_holder == 1) {
        //     $this->stake_holder = 1;
        // } elseif ($stake_holder == 2) {
        //     $this->stake_holder = 2;
        // } elseif ($stake_holder == 3) {
        //     $this->stake_holder = 3;
        // } elseif ($stake_holder == 4) {
        //     $this->stake_holder = 4;
        // } elseif ($stake_holder == 5) {
        //     $this->stake_holder = 5;
        // } elseif ($stake_holder == 6) {
        //     $this->stake_holder = 6;
        // }

        $this->loadTypeHeaderCards();
    }

    public function loadTypeHeaderCards()
    {
        $this->stake_holder = $this->stake_holder ?? false;

        // $stakeholders = StakeholderCategory::groupBy('name')->get();

        $this->stake_holder_header_cards = [
            [
                'title' => 'All',
                'class' => 'w-24 flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm rounded-l-md',
                'color' => 'text-blue',
                'action' => 'stake_holder',
                'id' => false
            ],
            [
                'title' => 'CSM',
                'class' => 'w-24 flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm',
                'color' => 'text-blue',
                'action' => 'stake_holder',
                'id' => 1
            ],
            [
                'title' => 'NL / SL',
                'class' => 'w-24 flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm',
                'color' => 'text-blue',
                'action' => 'stake_holder',
                'id' => 2
            ],
            [
                'title' => 'Metro Manila - Field',
                'class' => 'flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm',
                'color' => 'text-blue',
                'action' => 'stake_holder',
                'id' => 3
            ],
            [
                'title' => 'Metro Manila - Walk In',
                'class' => 'flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm',
                'color' => 'text-blue',
                'action' => 'stake_holder',
                'id' => 4
            ],
            [
                'title' => 'Provincial Branches',
                'class' => 'flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm',
                'color' => 'text-blue',
                'action' => 'stake_holder',
                'id' => 5
            ],
            [
                'title' => 'Online Channel',
                'class' => 'flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm rounded-r-md',
                'color' => 'text-blue',
                'action' => 'stake_holder',
                'id' => 6
            ],
        ];


        // $count = count($stakeholders);

        // foreach ($stakeholders as $index => $stakeholder) {

        //     $isLastElement = $index === ($count - 1);

        //     $class = 'px-8 text-gray-500 bg-white border border-gray-600 shadow-sm';

        //     if ($isLastElement) {
        //         $class .= ' rounded-r-md';
        //     }

        //     $this->stake_holder_header_cards[] = [
        //         'title' => $stakeholder->name,
        //         'class' => $class,
        //         'color' => 'text-blue',
        //         'action' => 'division',
        //         'id' => $stakeholder->id
        //     ];
        // }
    }

    public function action($data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'stake_holder_category') {
            return redirect()->route('crm.sales.quota.stakeholder-category.index');
        } elseif ($action_type == 'corporate_target') {
            $this->corporate_target_modal = true;
        } elseif ($action_type == 'stakeholder_target') {
            $this->stakeholder_target_modal = true;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = true;
            $this->stakeholder_id = $data['id'];
            $this->emitTo('crm.sales.quota.edit', 'mount', $data['id']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'corporate_target') {
            $this->corporate_target_modal = false;
        } else if ($action_type == 'stakeholder_target') {
            $this->stakeholder_target_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function search()
    {
        return [
            'is_corporate_target' => $this->header_card,
            'stakeholder' => $this->stakeholder,
            'sh_header' => $this->stake_holder,
            'month' => $this->month,
            'year' => $this->year,
            'paginate' => $this->paginate,
            'sortField' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
        ];
    }

    public function render(TargetInterface $target_interface)
    {
        $response = $target_interface->index($this->search());
        // dd($response['result']['stakeholders_targets']);
        $corporate_target_subcategories = $response['result']['corporate_target_subcategories'] ?? [];
        $total_days_sum = [];
        $monthly_target_amount_sum = [];

        foreach ($corporate_target_subcategories as $i => $corporate_target_subcat) {
            $total_days_sum[] = $corporate_target_subcat->total_days_count;
            $monthly_target_amount_sum[] = $corporate_target_subcat->monthly_target_amount;
        }

        $this->total_days_count = array_sum($total_days_sum);
        $this->monthly_target_amount = number_format(array_sum($monthly_target_amount_sum), 2);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'corporate_targets' => [],
                    'corporate_target_subcategories' => [],
                    'stakeholders_targets' => [],
                    'stakeholders_target_subcategories' => [],
                ];
        }
        return view('livewire.crm.sales.quota.index', [
            'corporate_targets' => $response['result']['corporate_targets'] ?? [],
            'corporate_target_subcategories' => $response['result']['corporate_target_subcategories'] ?? [],

            'stakeholders_targets' => $response['result']['stakeholders_targets'] ?? [],
        ]);
    }
}
