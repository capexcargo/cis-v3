<?php

namespace App\Http\Livewire\Crm\Sales\Quota;

use App\Traits\Crm\Sales\Quota\StakeholderCategoryTrait;
use App\Traits\Crm\Sales\Quota\TargetTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use TargetTrait, StakeholderCategoryTrait, WithPagination, PopUpMessagesTrait;

    public $stakeholder_id;

    protected $listeners = ['mount' => 'mount'];

    public function mount($id)
    {
        $this->stakeholder_id = $id;
    }

    public function render()
    {
        return view('livewire.crm.sales.quota.edit');
    }
}
