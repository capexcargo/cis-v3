<?php

namespace App\Http\Livewire\Crm\Sales\Quota\StakeholderCategory;

use App\Interfaces\Crm\Sales\Quota\StakeholderCategoryInterface;
use App\Traits\Crm\Sales\Quota\StakeholderCategoryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use StakeholderCategoryTrait, PopUpMessagesTrait;

    public $stakeholder_category;

    public function mount(StakeholderCategoryInterface $stakeholder_category_interface, $id)
    {
        $this->stakeholder_category_id = $id;


        $response = $stakeholder_category_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->stakeholder_category = $response['result']['stakeholder_category'];

        $this->stakeholder_category_name = $this->stakeholder_category->name;
        $this->search_management_data = $this->stakeholder_category->id;


        $this->load();
        foreach ($this->stakeholder_category->subcategories as $i => $scsc) {
            if ($this->search_management_data == 1) {
                $filtered_search = array_merge($this->account_type);
            } elseif ($this->search_management_data == 2) {
                $filtered_search = array_merge($this->nl_sl);
            } elseif ($this->search_management_data == 3) {
                $filtered_search = array_merge($this->quadrants);
            } elseif ($this->search_management_data == 4) {
                $filtered_search = array_merge($this->walk_ins);
            } elseif ($this->search_management_data == 5) {
                $filtered_search = array_merge($this->provincial_branches);
            } elseif ($this->search_management_data == 6) {
                $filtered_search = array_merge($this->online_channels);
            } else {
                $filtered_search = array_merge($this->account_type, $this->nl_sl, $this->quadrants, $this->walk_ins, $this->provincial_branches, $this->online_channels);
            }
            $this->stakeholder_mgmt_data_refs = $filtered_search;

            foreach ($this->stakeholder_mgmt_data_refs as $a => $data_ref) {
                if ($data_ref['id'] == $scsc->subcategory && $data_ref['reference_id'] == $scsc->management_id) {
                    $this->selected_subs[$a] = [
                        'id' => $scsc->subcategory,
                        'ref_id' => $this->stakeholder_category->id,
                    ];
                }
            }
        }
    }

    public function confirmationSubmit()
    {
        // dd($this->selected_subs);
        foreach ($this->selected_subs as $sel) {
            if (is_array($sel)) {
                $this->selected[] = [
                    'id' => $sel['id'],
                    'ref_id' => $sel['ref_id'],
                    'explode' => false,
                ];
            } else {
                $array = explode(',', $sel);

                $this->selected[] = [
                    'id' => $array[0],
                    'ref_id' => $array[0],
                    'explode' => true,
                ];
            }
        }


        $validated = $this->validate(
            [
                'stakeholder_category_name' => 'required',
                'selected_subs' => 'required',
            ],
            [
                'selected_subs.required' => 'Please, select at least one.',
            ]
        );

        $this->confirmation_modal = true;
    }

    public function submit(StakeholderCategoryInterface $stakeholder_interface)
    {
        $response = $stakeholder_interface->update($this->getRequest(), $this->stakeholder_category_id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.quota.stakeholder-category.index', 'close_modal', 'edit');
            $this->emitTo('crm.sales.quota.stakeholder-category.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.sales.quota.stakeholder-category.edit');
    }
}
