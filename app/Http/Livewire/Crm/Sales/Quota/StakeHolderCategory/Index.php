<?php

namespace App\Http\Livewire\Crm\Sales\Quota\StakeholderCategory;

use App\Interfaces\Crm\Sales\Quota\StakeholderCategoryInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $action_type;
    public $create_modal = false;
    public $edit_modal = false;
    public $confirmation_modal = false;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;

    public $stakeholder_category_id;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action($data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'back') {
            return redirect()->route('crm.sales.quota.index');
        } elseif ($action_type == 'create') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('crm.sales.quota.stakeholder-category.edit', 'edit', $data['id']);
            $this->stakeholder_category_id = $data['id'];
            $this->edit_modal = true;
        } elseif ($action_type == 'update_status') {
            $this->confirmation_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'update_status') {
            $this->confirmation_modal = false;
        }
    }

    public function search()
    {
        return [
            'paginate' => $this->paginate,
            'sortField' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
        ];
    }

    public function render(StakeholderCategoryInterface $stakeholder_category_interface)
    {
        $response = $stakeholder_category_interface->index($this->search());

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'stakeholder_categories' => [],
                    'subcategories' => [],
                ];
        }

        return view('livewire.crm.sales.quota.stakeholder-category.index', [
            'stakeholder_categories' => $response['result']['stakeholder_categories'],
            'subcategories' => $response['result']['subcategories'],
        ]);
    }
}
