<?php

namespace App\Http\Livewire\Crm\Sales\Quota\StakeholderCategory;

use App\Interfaces\Crm\Sales\Quota\StakeholderCategoryInterface;
use App\Traits\Crm\Sales\Quota\StakeholderCategoryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use StakeholderCategoryTrait, PopUpMessagesTrait;

    public function confirmationSubmit()
    {
        foreach ($this->selected_subs as $sel) {
            $array = explode(',', $sel);

            $this->selected[] = [
                'id' => $array[0],
                'ref_id' => $array[1],
            ];
        }

        $validated = $this->validate(
            [
                'stakeholder_category_name' => 'required',
                'search_management_data' => 'required',
                'selected_subs' => 'required',
            ],
            [
                'selected_subs.required' => 'Please, select at least one.',
            ]
        );

        $this->confirmation_modal = true;
    }

    public function submit(StakeholderCategoryInterface $stakeholder_interface)
    {
        $response = $stakeholder_interface->create($this->getRequest());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.quota.stakeholder-category.index', 'close_modal', 'create');
            $this->emitTo('crm.sales.quota.stakeholder-category.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.sales.quota.stakeholder-category.create');
    }
}
