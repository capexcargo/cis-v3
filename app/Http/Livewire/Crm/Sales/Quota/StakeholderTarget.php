<?php

namespace App\Http\Livewire\Crm\Sales\Quota;

use App\Interfaces\Crm\Sales\Quota\TargetInterface;
use App\Traits\Crm\Sales\Quota\StakeholderCategoryTrait;
use App\Traits\Crm\Sales\Quota\TargetTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class StakeholderTarget extends Component
{
    use TargetTrait, StakeholderCategoryTrait, WithPagination, PopUpMessagesTrait;

    public $action_type;

    public function action($data, $action_type, TargetInterface $target_interface)
    {
        $this->action_type = $action_type;

        if ($action_type == 'save') {
            $this->stakeholder_category = $data['id'];

            $response = $target_interface->updateOrCreate(2, $this->getRequestStakeholder());
            if ($response['code'] == 200) {
                $this->emitTo('crm.sales.quota.index', 'close_modal', 'stakeholder_target');
                $this->emitTo('crm.sales.quota.index', 'index');
                $this->sweetAlert('', $response['message']);
            } elseif ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function render()
    {
        return view('livewire.crm.sales.quota.stakeholder-target');
    }
}
