<?php

namespace App\Http\Livewire\Crm\Sales\Quota;

use App\Interfaces\Crm\Sales\Quota\TargetInterface;
use App\Traits\Crm\Sales\Quota\TargetTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class CorporateTarget extends Component
{
    use TargetTrait, WithPagination, PopUpMessagesTrait;

    public $action_type;

    public function save(TargetInterface $target_interface)
    {
        $response = $target_interface->updateOrCreate(1, $this->getRequestCorporate());
        if ($response['code'] == 200) {
            $this->emitTo('crm.sales.quota.index', 'close_modal', 'corporate_target');
            $this->emitTo('crm.sales.quota.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.sales.quota.corporate-target');
    }
}
