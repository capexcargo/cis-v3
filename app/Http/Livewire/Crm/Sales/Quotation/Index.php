<?php

namespace App\Http\Livewire\Crm\Sales\Quotation;

use App\Interfaces\Crm\Sales\QuotationInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $action_type;

    public $customer_name_search;
    public $sr_name_search;
    public $quotation_ref_no_search;
    public $date_created_search;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;

    protected $listeners = ['index' => 'render'];

    public function action($data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'quotation') {
            return redirect()->route('crm.sales.quotation.index-quotation', ['id' => $data['id'], 'from_sr' => $data['from_sr']]);
        }
    }

    public function search()
    {
        return [
            'customer_name_search' => $this->customer_name_search,
            'sr_name_search' => $this->sr_name_search,
            'quotation_ref_no_search' => $this->quotation_ref_no_search,
            'date_created_search' => $this->date_created_search,
            'paginate' => $this->paginate,
            'sortField' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
        ];
    }

    public function render(QuotationInterface $quotation_interface)
    {
        $response = $quotation_interface->index($this->search());

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'quotations' => [],
                ];
        }

        return view('livewire.crm.sales.quotation.index', [
            'quotations' => $response['result']['quotations']
        ]);
    }
}
