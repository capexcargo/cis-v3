<?php

namespace App\Http\Livewire\Crm\Sales\Quotation;

use App\Interfaces\Crm\Sales\QuotationInterface;
use App\Interfaces\Crm\Sales\RateCalculatorInterface;
use App\Models\BranchReference;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmMovementSfTypeReferences;
use App\Models\Crm\CrmQuotation;
use App\Models\CrmCommodityType;
use App\Models\CrmCrateType;
use App\Models\CrmModeOfPaymentReference;
use App\Models\CrmRateAirFreight;
use App\Models\CrmRateBoxDetails;
use App\Models\CrmRatePouchDetails;
use App\Models\CrmServiceMode;
use App\Models\CrmShipmentType;
use App\Repositories\Crm\Sales\RateCalculatorRepository;
use App\Traits\Crm\Sales\QuotationTrait;
use App\Traits\Crm\Sales\RateCalculatorTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class IndexQuotation extends Component
{
    use QuotationTrait, RateCalculatorTrait, PopUpMessagesTrait;

    public $is_from_sr;
    public $sr_id;
    public $quotation_id;
    public $quot_id;

    protected $listeners = ['index_quotation' => 'render', 'mount' => 'mount', 'close_modal' => 'closeModal', 'generate_quotation_ref_no' => 'generateQuotationRefNo'];

    protected $rules = [
        'date' => 'required',
        'quotation_ref_no' => 'required',
        'shipment_type' => 'required',
        'sr_number' => 'required',
        'subject' => 'required',
        'account_id' => 'required',
        'contact_owner' => 'required',
        'assigned_to_id' => 'required',

        'transport_mode' => 'required',
        'origin' => 'required',
        'destination' => 'required',
        'exact_pickup_location' => 'required',
        'exact_dropoff_location' => 'required',

        'declared_value' => 'sometimes',
        'service_mode' => 'required',
        'commodity_type' => 'sometimes',
        'commodity_applicable_rate' => 'sometimes',
        'description' => 'sometimes',
        'paymode' => 'required',

        //Transport mode = 1
        'air_cargo' => 'sometimes',
        'air_cargos' => 'sometimes',

        'air_pouch' => 'sometimes',
        'air_pouches' => 'sometimes',

        'air_box' => 'sometimes',
        'air_boxes' => 'sometimes',


        //Transport mode = 2
        'sea_cargo' => 'sometimes',
        'sea_cargos_is_lcl' => 'sometimes',
        'sea_cargos_is_fcl' => 'sometimes',
        'sea_cargos_is_rcl' => 'sometimes',

        'sea_box' => 'sometimes',
        'sea_boxes' => 'sometimes',

        //Transport mode = 3
        'land' => 'sometimes',
        'lands_shippers_box' => 'sometimes',
        'lands_pouches' => 'sometimes',
        'lands_boxes' => 'sometimes',
        'lands_ftl' => 'sometimes',


        // BREAKDOWN OF FREIGHT CHARGES
        'weight_charge' => 'sometimes',
        'awb_fee' => 'sometimes',
        'valuation' => 'sometimes',
        'cod_charge' => 'sometimes',
        'insurance' => 'sometimes',
        'handling_fee' => 'sometimes',
        'other_fees' => 'sometimes',
        'equipment_rental' => 'sometimes',
        'oda_fee' => 'sometimes',
        'discount_amount' => 'sometimes',
        'discount_percentage' => 'sometimes',
        'subtotal' => 'sometimes',
        'grand_total' => 'sometimes',
    ];

    public function mount(QuotationInterface $quotation_interface, $id, $from_sr)
    {
        $this->generateQuotationRefNo();

        $this->evat = null;
        $this->transportModeReferences();

        $this->date = date('Y-m-d');

        // $this->quotation_ref_no = "CPX-Q-0001";

        $this->is_from_sr = $from_sr;
        $this->sr_id = $id;

        $response = $quotation_interface->show($id, $from_sr);

        abort_if($response['code'] != 200, $response['code'], $response['message']);
        if ($from_sr == 'sr-create-quotation') {

            $this->addAirCargos();
            $this->addAirPouches();
            $this->addAirBoxes();

            $this->addSeaCargosLcl();
            $this->addSeaCargosFcl();
            $this->addSeaCargosRcl();
            $this->addSeaBoxes();

            $this->addLandsShippersBox();
            $this->addLandsPouches();
            $this->addLandsBoxes();
            $this->addLandsFtl();

            $this->movementSfTypeReferences();

            $this->quotation_id = $id;

            $this->service_request = $response['result'];

            $this->sr_number = $this->service_request->sr_no;
            $this->subject = $this->service_request->subject;
            $this->account_id = $this->service_request->customer ?? $this->service_request->account_id;
            $this->contact_owner = $this->service_request->assignedTo->name;
            $this->assigned_to_id = $this->service_request->assigned_to_id;

            $customer_data = CrmCustomerInformation::with('contactPersons', 'mobileNumbers', 'emails', 'addresses')->find($this->account_id);

            if ($customer_data) {

                $this->company_business_name = $customer_data->account_type ?? null == 2 ? $customer_data->company_name : '';

                foreach ($customer_data->contactPersons as $i => $contact_person) {
                    if ($contact_person->is_primary == 1) {
                        $this->primary_contact_person = $contact_person->first_name . " " . $contact_person->middle_name . " " . $contact_person->last_name;
                        $this->fname = $contact_person->first_name;
                        $this->mname = $contact_person->middle_name;
                        $this->lname = $contact_person->last_name;
                        $this->position = $contact_person->position;
                    }
                }

                foreach ($customer_data->mobileNumbers as $i => $mobile_number) {
                    if ($mobile_number->is_primary == 1) {
                        $this->mobile_number = $mobile_number->mobile;
                    }
                }

                foreach ($customer_data->telephoneNumbers as $i => $telephone_number) {
                    if ($telephone_number->is_primary == 1) {
                        $this->telephone_number = $telephone_number->telephone;
                    }
                }

                foreach ($customer_data->emails as $i => $email_address) {
                    if ($email_address->is_primary == 1) {
                        $this->email_address = $email_address->email;
                    }
                }

                foreach ($customer_data->addresses as $i => $address) {
                    if ($address->is_primary == 1) {
                        $this->address_line_1 = $address->address_line_1;
                        $this->address_line_2 = $address->address_line_2;
                        $this->address_type = $address->address_type;

                        $this->postal = $address->postal_id;
                        $this->state_province = '';
                        $this->city_municipal = '';
                        $this->barangay = '';
                    }
                }
            }
        } else {

            $this->quotation_id = $id;

            $this->quotation = $response['result'];

            $this->sr_number = $this->quotation->sr_no;
            $this->shipment_type = $this->quotation->shipment_type_id;
            $this->subject = $this->quotation->subject;
            $this->quotation_ref_no = $this->quotation->reference_no;
            $this->account_id = $this->quotation->customer->customer ?? $this->quotation->customer->account_id;
            $this->contact_owner = $this->quotation->customer->assignedTo->name;
            $this->assigned_to_id = $this->quotation->customer->assigned_to_id;

            $customer_data = CrmCustomerInformation::with('contactPersons', 'mobileNumbers', 'emails', 'addresses')->find($this->account_id);

            if ($customer_data) {

                $this->company_business_name = $customer_data->account_type ?? null == 2 ? $customer_data->company_name : '';

                foreach ($customer_data->contactPersons as $i => $contact_person) {
                    if ($contact_person->is_primary == 1) {
                        $this->primary_contact_person = $contact_person->first_name . " " . $contact_person->middle_name . " " . $contact_person->last_name;
                        $this->fname = $contact_person->first_name;
                        $this->mname = $contact_person->middle_name;
                        $this->lname = $contact_person->last_name;
                        $this->position = $contact_person->position;
                    }
                }

                foreach ($customer_data->mobileNumbers as $i => $mobile_number) {
                    if ($mobile_number->is_primary == 1) {
                        $this->mobile_number = $mobile_number->mobile;
                    }
                }

                foreach ($customer_data->telephoneNumbers as $i => $telephone_number) {
                    if ($telephone_number->is_primary == 1) {
                        $this->telephone_number = $telephone_number->telephone;
                    }
                }

                foreach ($customer_data->emails as $i => $email_address) {
                    if ($email_address->is_primary == 1) {
                        $this->email_address = $email_address->email;
                    }
                }

                foreach ($customer_data->addresses as $i => $address) {
                    if ($address->is_primary == 1) {
                        $this->address_line_1 = $address->address_line_1;
                        $this->address_line_2 = $address->address_line_2;
                        $this->address_type = $address->address_type;

                        $this->postal = $address->postal_id;
                        $this->state_province = '';
                        $this->city_municipal = '';
                        $this->barangay = '';
                    }
                }
            }

            //Shipment Details;
            $this->origin_references = BranchReference::get();
            $this->destination_references = BranchReference::get();
            $this->transhipment_references = CrmShipmentType::get();
            $this->commodity_type_references = CrmCommodityType::get();
            $this->paymode_references = CrmModeOfPaymentReference::get();

            $this->transport_mode = $this->quotation->shipmentDetails['transport_mode_id'];

            if ($this->transport_mode == 1) {
                $this->commodity_applicable_rate_references = CrmRateAirFreight::where('transport_mode_id', $this->transport_mode)->get();
                $this->service_mode_references = CrmServiceMode::whereIn('id', [1, 2, 3, 4])->get();
            } elseif ($this->transport_mode == 2) {
                $this->updatedTranshipment();
                $this->service_mode_references = CrmServiceMode::whereIn('id', [5, 6, 7, 8])->get();
            } elseif ($this->transport_mode == 3) {
                $this->commodity_applicable_rate_references = [];
                $this->service_mode_references = CrmServiceMode::get();
            } else {
                $this->commodity_applicable_rate_references = [];
            }

            $this->origin = $this->quotation->shipmentDetails['origin_id'];
            $this->destination = $this->quotation->shipmentDetails['destination_id'];
            $this->exact_pickup_location = $this->quotation->shipmentDetails['origin_address'];
            $this->exact_dropoff_location = $this->quotation->shipmentDetails['destination_address'];
            $this->declared_value = $this->quotation->shipmentDetails['declared_value'];
            $this->commodity_type = $this->quotation->shipmentDetails['commodity_type_id'];
            $this->commodity_applicable_rate = $this->quotation->shipmentDetails['commodity_applicable_rate_id'];
            $this->paymode = $this->quotation->shipmentDetails['paymode_id'];
            $this->service_mode = $this->quotation->shipmentDetails['servicemode_id'];


            $this->movement_sf_type = 1;
            $this->movement_sf_type_references = CrmMovementSfTypeReferences::get();
            $this->crating_type_references = CrmCrateType::get();

            if ($this->quotation->shipmentDetails['service_type_id'] == 1) {
                $this->air_cargo = true;
                $this->air_cargos[] = [
                    'id' => $this->quotation->shipmentDimensions['id'],
                    'qty' => $this->quotation->shipmentDimensions['quantity'],
                    'wt' => $this->quotation->shipmentDimensions['weight'],
                    'dimension_l' => $this->quotation->shipmentDimensions['length'],
                    'dimension_w' => $this->quotation->shipmentDimensions['width'],
                    'dimension_h' => $this->quotation->shipmentDimensions['height'],
                    'unit_of_measurement' => $this->quotation->shipmentDimensions['unit_of_measurement_id'],
                    'measurement_type' => $this->quotation->shipmentDimensions['measurement_type_id'],
                    'type_of_packaging' => $this->quotation->shipmentDimensions['type_of_packaging_id'],
                    'for_crating' => $this->quotation->shipmentDimensions['is_for_crating'],
                    'crating_type' => $this->quotation->shipmentDimensions['crating_type_id'],
                    'cwt' => $this->quotation->shipmentDetails['total_cbm'],
                    'is_deleted' => false,
                ];
            }

            if ($this->quotation->shipmentDetails['service_type_id'] == 2) {
                $this->air_pouch = true;
                $this->air_pouches[] = [
                    'id' => $this->quotation->shipmentDimensions['id'],
                    'qty' => $this->quotation->shipmentDimensions['quantity'],
                    'pouch_size' => $this->quotation->shipmentDimensions['pouch_box_size'],
                    'amount' => $this->quotation->shipmentDimensions['pouch_box_amount'],
                    'is_deleted' => false,
                ];
            }

            if ($this->quotation->shipmentDetails['service_type_id'] == 3) {
                if ($this->transport_mode == 1) {
                    $this->air_box = true;
                    $this->air_boxes[] = [
                        'id' => $this->quotation->shipmentDimensions['id'],
                        'qty' => $this->quotation->shipmentDimensions['quantity'],
                        'box_size' => $this->quotation->shipmentDimensions['pouch_box_size'],
                        'weight' => $this->quotation->shipmentDimensions['weight'],
                        'amount' => $this->quotation->shipmentDimensions['pouch_box_amount'],
                        'is_deleted' => false,
                    ];
                } elseif ($this->transport_mode == 2) {
                    $this->sea_box = true;
                    $this->sea_boxes[] = [
                        'id' => $this->quotation->shipmentDimensions['id'],
                        'qty' => $this->quotation->shipmentDimensions['quantity'],
                        'box_size' => $this->quotation->shipmentDimensions['pouch_box_size'],
                        'weight' => $this->quotation->shipmentDimensions['weight'],
                        'amount' => $this->quotation->shipmentDimensions['pouch_box_amount'],
                        'is_deleted' => false,
                    ];
                }
            }

            if ($this->quotation->shipmentDetails['sea_shipment_type'] == 1) {
                $this->sea_cargo = true;
                $this->sea_cargos_is_lcl[] = [
                    'id' => $this->quotation->shipmentDimensions['id'],
                    'qty' => $this->quotation->shipmentDimensions['quantity'],
                    'wt' => $this->quotation->shipmentDimensions['weight'],
                    'dimension_l' => $this->quotation->shipmentDimensions['length'],
                    'dimension_w' => $this->quotation->shipmentDimensions['width'],
                    'dimension_h' => $this->quotation->shipmentDimensions['height'],
                    'unit_of_measurement' => $this->quotation->shipmentDimensions['unit_of_measurement_id'],
                    'measurement_type' => $this->quotation->shipmentDimensions['measurement_type_id'],
                    'type_of_packaging' => $this->quotation->shipmentDimensions['type_of_packaging_id'],
                    'for_crating' => $this->quotation->shipmentDimensions['is_for_crating'],
                    'crating_type' => $this->quotation->shipmentDimensions['crating_type_id'],
                    'cbm' => $this->quotation->shipmentDetails['total_cbm'],
                    'is_deleted' => false,
                ];
            }

            if ($this->quotation->shipmentDetails['sea_shipment_type'] == 2) {
                $this->sea_cargo = true;
                $this->sea_cargos_is_fcl[] = [
                    'id' => $this->quotation->shipmentDimensions['id'],
                    'qty' => $this->quotation->shipmentDimensions['quantity'],
                    'container' => $this->quotation->shipmentDimensions['container'],
                    'is_deleted' => false,
                ];
            }

            if ($this->quotation->shipmentDetails['sea_shipment_type'] == 3) {
                $this->sea_cargo = true;
                $this->sea_cargos_is_rcl[] = [
                    'id' => $this->quotation->shipmentDimensions['id'],
                    'qty' => $this->quotation->shipmentDimensions['quantity'],
                    'wt' => $this->quotation->shipmentDimensions['weight'],
                    'dimension_l' => $this->quotation->shipmentDimensions['length'],
                    'dimension_w' => $this->quotation->shipmentDimensions['width'],
                    'dimension_h' => $this->quotation->shipmentDimensions['height'],
                    'unit_of_measurement' => $this->quotation->shipmentDimensions['unit_of_measurement_id'],
                    'measurement_type' => $this->quotation->shipmentDimensions['measurement_type_id'],
                    'amount' =>  $this->quotation->shipmentDimensions['pouch_box_amount'],
                    'for_crating' => $this->quotation->shipmentDimensions['is_for_crating'],
                    'crating_type' => $this->quotation->shipmentDimensions['crating_type_id'],
                    'cbm' => $this->quotation->shipmentDetails['total_cbm'],
                    'is_deleted' => false,
                ];
            }

            //Breakdown Charges;
            $this->weight_charge = number_format($this->quotation->shipmentCharges['weight_charge'], 2, '.', '');
            $this->awb_fee = number_format($this->quotation->shipmentCharges['awb_fee'], 2, '.', '');
            $this->valuation = number_format($this->quotation->shipmentCharges['valuation'], 2, '.', '');
            // $this->awb_fee = number_format($this->quotation->shipmentCharges['awb_fee'], 2, '.', '');
            $this->cod_charge = number_format($this->quotation->shipmentCharges['cod_charge'], 2, '.', '');
            $this->insurance = number_format($this->quotation->shipmentCharges['insurance'], 2, '.', '');
            $this->handling_fee = number_format($this->quotation->shipmentCharges['handling_fee'], 2, '.', '');

            $this->evat_chkbox = $this->quotation->shipmentCharges['is_evat'];
            $this->evat =  number_format($this->quotation->shipmentCharges['evat'], 2, '.', '');

            $this->other_fees = $this->quotation->shipmentCharges['is_other_fee'];
            if ($this->other_fees == 1) {
                $this->opa_fee = $this->quotation->shipmentCharges['opa_fee'];
                $this->oda_fee = $this->quotation->shipmentCharges['oda_fee'];
                $this->equipment_rental = $this->quotation->shipmentCharges['equipment_rental'];
                $this->crating_fee = $this->quotation->shipmentCharges['crating_fee'];
                $this->lashing_fee = $this->quotation->shipmentCharges['lashing'];
                $this->manpower_fee = $this->quotation->shipmentCharges['manpower'];
                $this->dangerous_goods_fee = $this->quotation->shipmentCharges['dangerous_goods_fee'];
                $this->trucking_fee = $this->quotation->shipmentCharges['trucking'];
                $this->perishable_fee = $this->quotation->shipmentCharges['perishable_fee'];
                $this->packaging_fee = $this->quotation->shipmentCharges['packaging_fee'];
            }

            $this->discount_amount =  number_format($this->quotation->shipmentCharges['discount_amount'], 2, '.', '');
            $this->discount_percentage =  number_format($this->quotation->shipmentCharges['discount_percentage'], 2, '.', '');
            $this->subtotal =  number_format($this->quotation->shipmentCharges['subtotal'], 2, '.', '');
            $this->grand_total = number_format($this->quotation->shipmentCharges['grandtotal'], 2);
        }
    }

    protected function updatedOtherFees()
    {
        if (!$this->other_fees) {
            $this->opa_fee = null;
            $this->oda_fee =  null;
            $this->equipment_rental =  null;
            $this->manpower_fee =  null;
            $this->dangerous_goods_fee = null;
            $this->trucking_fee = null;
            $this->perishable_fee = null;
            $this->packaging_fee =  null;
        }
    }

    public function action($data, $action_type, QuotationInterface $quotation_interface)
    {
        $this->action_type = $action_type;

        if ($action_type == 'generate') {

            $response = $quotation_interface->saveSrCreateQuotation($this->getQuotationRequests());
            // dd($data['id'],  $this->is_from_sr, $response['result']['quotation_id']);

            if ($response['code'] == 200) {

                if ($this->is_from_sr == "sr-create-quotation") {
                    $this->sweetAlert('', $response['message']);
                    $this->emitTo('crm.sales.quotation.quotation-modal', 'quotation', $response['result']['quotation_id'], 'show-quotation');
                    $quot_id = $response['result']['quotation_id'];
                } else {
                    $this->emitTo('crm.sales.quotation.quotation-modal', 'quotation', $data['id'], 'show-quotation');
                    $quot_id = $data['id'];
                }

                $this->quot_id = $quot_id;
                $this->from_sr = 'show-quotation';
                $this->generate_quotation_modal = true;
            } elseif ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($action_type == 'save_sr_create_quotation') {
            $response = $quotation_interface->saveSrCreateQuotation($this->getQuotationRequests());
            if ($response['code'] == 200) {
                $this->sweetAlert('', $response['message']);
                // return redirect()->route('crm.sales.quotation.index-quotation', ['id' => $this->sr_id, 'from_sr' => $this->is_from_sr]);
            } elseif ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($action_type == 'save_edit_quotation') {
            $response = $quotation_interface->saveEditQuotation($this->getQuotationRequests(), $this->quotation_id, $this->from_sr);

            if ($response['code'] == 200) {
                $this->sweetAlert('', $response['message']);
            } elseif ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($action_type == 'back') {
            return redirect()->route('crm.sales.quotation.index');
        }
    }

    public function forCWT_CBM($trans_mode = 0)
    {
        // dd($this->movement_sf_type);
        $wt = 0;
        $cbm = 0;
        $dimension = 0;
        $amount = 0;

        if ($this->commodity_type == 1) {
            $divisor = 3300;
        } else {
            $divisor = 2800;
        }

        if ($trans_mode == 1) {
            foreach ($this->air_cargos as $i => $ac) {
                $wt = $this->air_cargos[$i]['wt'];
                $dimension = $this->air_cargos[$i]['dimension_l'] *  $this->air_cargos[$i]['dimension_w'] *  $this->air_cargos[$i]['dimension_h'] / $divisor;

                $this->air_cargos[$i]['cwt'] =  $wt > $dimension ? $wt : number_format($dimension, 2);
            }
        } elseif ($trans_mode == 2) {
            if ($this->movement_sf_type == 1) { // for LCL
                foreach ($this->sea_cargos_is_lcl as $i => $ac) {
                    $wt = $this->sea_cargos_is_lcl[$i]['wt'];
                    $cbm = $this->sea_cargos_is_lcl[$i]['dimension_l'] *  $this->sea_cargos_is_lcl[$i]['dimension_w'] *  $this->sea_cargos_is_lcl[$i]['dimension_h'] / 1000000;

                    $this->sea_cargos_is_lcl[$i]['cbm'] =  $wt > $cbm ? $wt : number_format($cbm, 2);
                }
            } elseif ($this->movement_sf_type == 3) { // for RCL
                foreach ($this->sea_cargos_is_rcl as $i => $ac) {
                    $wt = $this->sea_cargos_is_rcl[$i]['wt'];
                    $cbm = $this->sea_cargos_is_rcl[$i]['dimension_l'] *  $this->sea_cargos_is_rcl[$i]['dimension_w'] *  $this->sea_cargos_is_rcl[$i]['dimension_h'] / 1000000;

                    $this->sea_cargos_is_rcl[$i]['cbm'] =  $wt > $cbm ? $wt : number_format(($cbm), 2);
                }
            }
        }


        foreach ($this->other_services_measurements as $i => $osm) {
            $wt = $this->other_services_measurements[$i]['wt'];
            $dimension = $this->other_services_measurements[$i]['dimension_l'] *  $this->other_services_measurements[$i]['dimension_w'] *  $this->other_services_measurements[$i]['dimension_h'] / $divisor;

            $this->other_services_measurements[$i]['cbm'] =  $wt > $dimension ? $wt : number_format($dimension, 2);
        }
    }

    public function forAirPouchSize()
    {
        $rates = CrmRatePouchDetails::where('origin_id', $this->origin)
            ->where('destination_id', $this->destination)
            ->first();

        $amount_small = $rates->amount_small;
        $amount_medium = $rates->amount_medium;
        $amount_large = $rates->amount_large;

        foreach ($this->air_pouches as $i => $ap) {
            $qty = $this->air_pouches[$i]['qty'];

            if ($this->air_pouches[$i]['pouch_size'] == 1) {
                $this->air_pouches[$i]['amount'] =  $qty * $amount_small;
            } else if ($this->air_pouches[$i]['pouch_size'] == 2) {
                $this->air_pouches[$i]['amount'] =  $qty * $amount_medium;
            } else if ($this->air_pouches[$i]['pouch_size'] == 3) {
                $this->air_pouches[$i]['amount'] =  $qty * $amount_large;
            }
        }
    }

    public function forBoxWeight($trans_mode)
    {
        $this->validate();

        $rates = CrmRateBoxDetails::where('origin_id', $this->origin)
            ->where('destination_id', $this->destination)
            ->first();

        $amount_small = (int)$rates->amount_small;
        $amount_medium = (int)$rates->amount_medium;
        $amount_large = (int)$rates->amount_large;

        $exceeding_amount_small = $amount_small / 5;
        $exceeding_amount_medium = $amount_medium / 10;
        $exceeding_amount_large = $amount_large / 20;

        $exceeded_small_amount = 0;
        $exceeded_medium_amount = 0;
        $exceeded_large_amount = 0;
        $exceed_weight = 0;

        $boxes = [];

        if ($trans_mode == 1) {
            foreach ($this->air_boxes as $i => $ab) {
                $qty = (int)$this->air_boxes[$i]['qty'];
                $weight = (int)$this->air_boxes[$i]['weight'];

                if (!empty($this->air_boxes[$i]['weight'])) {
                    if ($this->air_boxes[$i]['box_size'] == 1) {
                        if ($weight > 5) {
                            $exceed_weight = $weight - 5;
                            $exceeded_small_amount = $exceed_weight * $exceeding_amount_small;
                        }
                        $this->air_boxes[$i]['amount'] =  ($qty * $amount_small) + $exceeded_small_amount;
                    } else if ($this->air_boxes[$i]['box_size'] == 2) {
                        if ($weight > 10) {
                            $exceed_weight = $weight - 10;
                            $exceeded_medium_amount = $exceed_weight * $exceeding_amount_medium;
                        }
                        $this->air_boxes[$i]['amount'] =  ($qty * $amount_medium) + $exceeded_medium_amount;
                    } else if ($this->air_boxes[$i]['box_size'] == 3) {
                        if ($weight > 20) {
                            $exceed_weight = $weight - 20;
                            $exceeded_large_amount = $exceed_weight * $exceeding_amount_large;
                        }
                        $this->air_boxes[$i]['amount'] =  ($qty * $amount_large) + $exceeded_large_amount;
                    }
                }
            }
        } elseif ($trans_mode == 2) {
            foreach ($this->sea_boxes as $i => $sb) {
                $qty = (int)$this->sea_boxes[$i]['qty'];
                $weight = (int)$this->sea_boxes[$i]['weight'];

                if (!empty($this->sea_boxes[$i]['weight'])) {
                    if ($this->sea_boxes[$i]['box_size'] == 1) {
                        if ($weight > 5) {
                            $exceed_weight = $weight - 5;
                            $exceeded_small_amount = $exceed_weight * $exceeding_amount_small;
                        }
                        $this->sea_boxes[$i]['amount'] =  ($qty * $amount_small) + $exceeded_small_amount;
                    } else if ($this->sea_boxes[$i]['box_size'] == 2) {
                        if ($weight > 10) {
                            $exceed_weight = $weight - 10;
                            $exceeded_medium_amount = $exceed_weight * $exceeding_amount_medium;
                        }
                        $this->sea_boxes[$i]['amount'] =  ($qty * $amount_medium) + $exceeded_medium_amount;
                    } else if ($this->sea_boxes[$i]['box_size'] == 3) {
                        if ($weight > 20) {
                            $exceed_weight = $weight - 20;
                            $exceeded_large_amount = $exceed_weight * $exceeding_amount_large;
                        }
                        $this->sea_boxes[$i]['amount'] =  ($qty * $amount_large) + $exceeded_large_amount;
                    }
                }
            }
        }
    }

    public function compute()
    {
        $this->validate();

        $rate_calculator_repository = new RateCalculatorRepository();

        $response = $rate_calculator_repository->compute($this->getQuotationRequests());

        $this->rate_calculator = $response['result'];
        foreach ($this->rate_calculator as $i => $rc) {
            $this->weight_charge = $this->rate_calculator['cbm_charge'] ?? null;
            $this->awb_fee = $this->rate_calculator['awb_fee'] ?? null;
            $this->cod_charge = $this->rate_calculator['cod_charge'] ?? null;
            $this->valuation = $this->rate_calculator['valuation'] ?? null;
            $this->insurance = $this->rate_calculator['insurance'] ?? null;
            $this->handling_fee = $this->rate_calculator['handling_fee'] ?? null;
            $this->evat = $this->rate_calculator['evat'] ?? null;
            $this->discount_percentage = $this->rate_calculator['discount_percentage'] ?? null;
            $this->subtotal = $this->rate_calculator['subtotal'] ?? null;
            $this->grand_total = $this->rate_calculator['grand_total'] ?? null;
        }
    }

    public function render()
    {
        return view('livewire.crm.sales.quotation.index-quotation');
    }
}
