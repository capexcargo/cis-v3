<?php

namespace App\Http\Livewire\Crm\Sales\BookingMgmt;

use App\Interfaces\Crm\Sales\BookingManagement\BookingMgmt\BookingMgmtInterface;
use App\Models\CrmBooking;
use App\Models\CrmBookingAttachment;
use App\Models\CrmBookingConsignee;
use App\Models\CrmBookingFailedReasonReference;
use App\Models\CrmBookingLogs;
use App\Models\CrmBookingRemarks;
use App\Models\CrmBookingShipper;
use App\Models\CrmRateApplyFor;
use App\Traits\Crm\Sales\BookingMgmt\BookingMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    public $customer_no;
    public $IDXpickupwalk;
    public $IDXsinglemulti;

    public $IDXbookingrefno;
    public $IDXbookingstat;
    public $IDXfullname;
    public $IDXfname;

    public $IDXcompany;
    public $IDXmobile_nos;
    public $IDXemail_address;
    public $IDXaddress;

    public $IDXcustomertype;
    public $IDXacttype;

    public $IDXdecval;
    public $IDXtransportmode;
    public $IDXservmode;
    public $IDXdescgoods;
    public $IDXmodeofp;
    public $IDXref;
    public $IDXwcount;
    public $IDXworkins;
    public $IDXintrem;

    public $IDXcbId;

    public $IDXtimeslot;

    public $vconsignee = [];
    public $IDXconsid = [];

    public $VBCD = [];
    public $bookcons = [];
    public $CDref;

    public $work_ins;
    public $int_rem;
    public $brn;
    public $cancelID;
    public $cancel_booking_idx;
    public $rescheduleID;
    public $reschedule_booking_idx;

    public $book_id;

    public $booking_reference_no;
    public $created_at;
    public $pickup_date;
    public $s_name;
    public $c_name;
    public $final_status_id;
    public $booking_branch_id;

    public $activestatus;
    public $status_header_cards = [];
    public $stats;

    public $statusall;
    public $rescheduled;
    public $cancelled;

    public $header_cards = [];
    public $stats_1;

    public $cfordisp;
    public $crescheduled;
    public $ccancelled;
    public $cforconf;
    public $cconfirmed;
    public $congoing;
    public $ccompleted;
    public $cadvanced;
    public $creqcancel;
    public $creqresched;

    public $bookingc_header_cards = [];
    public $bc_1;

    public $total_pickup;
    public $total_booking;

    public $approve_cancel_reason;
    public $decline_cancel_reason;

    public $approve_resched_reason;
    public $decline_resched_reason;

    public $days;
    public $pudate;
    public $date_now;

    public $work_ins_atch;
    public $bookworkins = [];


    use BookingMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = [
        'mount' => 'mount', 'index' => 'render',
        // 'open_create' => 'openModal', 
        'close_modal' => 'closeModal', 'load_header_cards' => 'load'
    ];


    public function mount()
    {
        $totalbooking = CrmBooking::where('pickup_date', date("Y-m-d"))->orWhereIn('final_status_id', [6, 8])->get();
        $totalbookingcompleted = CrmBooking::where('pickup_date', date("Y-m-d"))->where('final_status_id', 5)->get();
        // dd($totalbooking);
        $this->total_pickup = count($totalbookingcompleted);
        $this->total_booking = count($totalbooking);
        // $this->pudate = $totalbooking->pickup_date;
        // dd($totalbooking);
        // $this->date_now = (date('Y-m-d'));
        // $this->days = now()->diffInDays(Carbon::parse($this->pudate));
    }
    public function load()
    {
        $this->loadHeadCards();
        $this->loadStatusHeaderCards();
        $this->loadBookingHeaderCards();

        $this->stats = '';
        // $this->bc_1 = '';

    }

    public function loadStatusHeaderCards()
    {
        $activestatus = CrmBooking::get();

        $statusalls = CrmBooking::get();
        $reschedule = CrmBooking::where('final_status_id', 6)->get();
        $cancel = CrmBooking::where('final_status_id', 7)->get();

        $statusall = $statusalls->count();
        $rescheduled = $reschedule->count();
        $cancelled = $cancel->count();

        $this->status_header_cards = [
            [
                'title' => 'All',
                'value' => '',
                'class' => 'bg-white  border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => false
            ],
            [
                'title' => 'Rescheduled',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 6
            ],
            [
                'title' => 'Cancelled',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 7
            ],
        ];
    }

    public function loadHeadCards()
    {
        $activestatus = CrmBooking::get();
        // dd($this->search_request['booking_reference_no']);
        // dd($request);
        $fordisp = CrmBooking::where('final_status_id', 1)->with(
            'BookingTypeReferenceBK',
            'VehicleTypeReferenceBK',
            'TimeslotReferenceBK',
            'WalkinReferenceBK',
            'ActivityReferenceBK',
            'ShipperReferenceBK',
            'FinalStatusReferenceBK',
            'BookingBranchReferenceBK',
            'MarketingChannelReferenceBK',
            'CreatedByBK',
            'BookingAttachmentHasManyBK',
            'BookingLogsHasManyBK',
            'BookingRemarksHasManyBK',
            'BookingConsigneeHasManyBK',
            'attachments',
            'BookingShipper',
            'BookingChannelReferenceBK',
        )
        ->when($this->search_request['booking_reference_no'] ?? false, function ($query) {
            $query->where('booking_reference_no', $this->search_request['booking_reference_no']);
        })
            ->when($this->search_request['created_at'] ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->search_request['created_at'] . '%');
            })
            ->when($this->search_request['pickup_date'] ?? false, function ($query) {
                $query->where('pickup_date', 'like', '%' . $this->search_request['pickup_date'] . '%');
            })
            ->whereHas('BookingShipper', function ($query) {
                $query->when($this->search_request['s_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['s_name'] . '%');
                });
            })
            ->whereHas('BookingConsigneeHasManyBK', function ($query) {
                $query->when($this->search_request['c_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['c_name'] . '%');
                });
            })
            ->when($this->search_request['final_status_id'] ?? false, function ($query) {
                $query->where('final_status_id', $this->search_request['final_status_id']);
            })
            ->whereHas('BookingBranchReferenceBK', function ($query) {
                $query->when($this->search_request['booking_branch_id'] ?? false, function ($query) {
                    $query->where('booking_branch_id', 'like', '%' . $this->search_request['booking_branch_id'] . '%');
                });
            })
            ->get();
            // dd($fordisp);
        $forconf = CrmBooking::where('final_status_id', 2)->with(
            'BookingTypeReferenceBK',
            'VehicleTypeReferenceBK',
            'TimeslotReferenceBK',
            'WalkinReferenceBK',
            'ActivityReferenceBK',
            'ShipperReferenceBK',
            'FinalStatusReferenceBK',
            'BookingBranchReferenceBK',
            'MarketingChannelReferenceBK',
            'CreatedByBK',
            'BookingAttachmentHasManyBK',
            'BookingLogsHasManyBK',
            'BookingRemarksHasManyBK',
            'BookingConsigneeHasManyBK',
            'attachments',
            'BookingShipper',
            'BookingChannelReferenceBK',
        )
        ->when($this->search_request['booking_reference_no'] ?? false, function ($query) {
            $query->where('booking_reference_no', $this->search_request['booking_reference_no']);
        })
            ->when($this->search_request['created_at'] ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->search_request['created_at'] . '%');
            })
            ->when($this->search_request['pickup_date'] ?? false, function ($query) {
                $query->where('pickup_date', 'like', '%' . $this->search_request['pickup_date'] . '%');
            })
            ->whereHas('BookingShipper', function ($query) {
                $query->when($this->search_request['s_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['s_name'] . '%');
                });
            })
            ->whereHas('BookingConsigneeHasManyBK', function ($query) {
                $query->when($this->search_request['c_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['c_name'] . '%');
                });
            })
            ->when($this->search_request['final_status_id'] ?? false, function ($query) {
                $query->where('final_status_id', $this->search_request['final_status_id']);
            })
            ->whereHas('BookingBranchReferenceBK', function ($query) {
                $query->when($this->search_request['booking_branch_id'] ?? false, function ($query) {
                    $query->where('booking_branch_id', 'like', '%' . $this->search_request['booking_branch_id'] . '%');
                });
            })->get();
        $confirmed = CrmBooking::where('final_status_id', 3)->with(
            'BookingTypeReferenceBK',
            'VehicleTypeReferenceBK',
            'TimeslotReferenceBK',
            'WalkinReferenceBK',
            'ActivityReferenceBK',
            'ShipperReferenceBK',
            'FinalStatusReferenceBK',
            'BookingBranchReferenceBK',
            'MarketingChannelReferenceBK',
            'CreatedByBK',
            'BookingAttachmentHasManyBK',
            'BookingLogsHasManyBK',
            'BookingRemarksHasManyBK',
            'BookingConsigneeHasManyBK',
            'attachments',
            'BookingShipper',
            'BookingChannelReferenceBK',
        )
        ->when($this->search_request['booking_reference_no'] ?? false, function ($query) {
            $query->where('booking_reference_no', $this->search_request['booking_reference_no']);
        })
            ->when($this->search_request['created_at'] ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->search_request['created_at'] . '%');
            })
            ->when($this->search_request['pickup_date'] ?? false, function ($query) {
                $query->where('pickup_date', 'like', '%' . $this->search_request['pickup_date'] . '%');
            })
            ->whereHas('BookingShipper', function ($query) {
                $query->when($this->search_request['s_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['s_name'] . '%');
                });
            })
            ->whereHas('BookingConsigneeHasManyBK', function ($query) {
                $query->when($this->search_request['c_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['c_name'] . '%');
                });
            })
            ->when($this->search_request['final_status_id'] ?? false, function ($query) {
                $query->where('final_status_id', $this->search_request['final_status_id']);
            })
            ->whereHas('BookingBranchReferenceBK', function ($query) {
                $query->when($this->search_request['booking_branch_id'] ?? false, function ($query) {
                    $query->where('booking_branch_id', 'like', '%' . $this->search_request['booking_branch_id'] . '%');
                });
            })->get();
        $ongoing = CrmBooking::where('final_status_id', 4)->with(
            'BookingTypeReferenceBK',
            'VehicleTypeReferenceBK',
            'TimeslotReferenceBK',
            'WalkinReferenceBK',
            'ActivityReferenceBK',
            'ShipperReferenceBK',
            'FinalStatusReferenceBK',
            'BookingBranchReferenceBK',
            'MarketingChannelReferenceBK',
            'CreatedByBK',
            'BookingAttachmentHasManyBK',
            'BookingLogsHasManyBK',
            'BookingRemarksHasManyBK',
            'BookingConsigneeHasManyBK',
            'attachments',
            'BookingShipper',
            'BookingChannelReferenceBK',
        )
        ->when($this->search_request['booking_reference_no'] ?? false, function ($query) {
            $query->where('booking_reference_no', $this->search_request['booking_reference_no']);
        })
            ->when($this->search_request['created_at'] ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->search_request['created_at'] . '%');
            })
            ->when($this->search_request['pickup_date'] ?? false, function ($query) {
                $query->where('pickup_date', 'like', '%' . $this->search_request['pickup_date'] . '%');
            })
            ->whereHas('BookingShipper', function ($query) {
                $query->when($this->search_request['s_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['s_name'] . '%');
                });
            })
            ->whereHas('BookingConsigneeHasManyBK', function ($query) {
                $query->when($this->search_request['c_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['c_name'] . '%');
                });
            })
            ->when($this->search_request['final_status_id'] ?? false, function ($query) {
                $query->where('final_status_id', $this->search_request['final_status_id']);
            })
            ->whereHas('BookingBranchReferenceBK', function ($query) {
                $query->when($this->search_request['booking_branch_id'] ?? false, function ($query) {
                    $query->where('booking_branch_id', 'like', '%' . $this->search_request['booking_branch_id'] . '%');
                });
            })->get();
        $completed = CrmBooking::where('final_status_id', 5)->with(
            'BookingTypeReferenceBK',
            'VehicleTypeReferenceBK',
            'TimeslotReferenceBK',
            'WalkinReferenceBK',
            'ActivityReferenceBK',
            'ShipperReferenceBK',
            'FinalStatusReferenceBK',
            'BookingBranchReferenceBK',
            'MarketingChannelReferenceBK',
            'CreatedByBK',
            'BookingAttachmentHasManyBK',
            'BookingLogsHasManyBK',
            'BookingRemarksHasManyBK',
            'BookingConsigneeHasManyBK',
            'attachments',
            'BookingShipper',
            'BookingChannelReferenceBK',
        )
        ->when($this->search_request['booking_reference_no'] ?? false, function ($query) {
            $query->where('booking_reference_no', $this->search_request['booking_reference_no']);
        })
            ->when($this->search_request['created_at'] ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->search_request['created_at'] . '%');
            })
            ->when($this->search_request['pickup_date'] ?? false, function ($query) {
                $query->where('pickup_date', 'like', '%' . $this->search_request['pickup_date'] . '%');
            })
            ->whereHas('BookingShipper', function ($query) {
                $query->when($this->search_request['s_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['s_name'] . '%');
                });
            })
            ->whereHas('BookingConsigneeHasManyBK', function ($query) {
                $query->when($this->search_request['c_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['c_name'] . '%');
                });
            })
            ->when($this->search_request['final_status_id'] ?? false, function ($query) {
                $query->where('final_status_id', $this->search_request['final_status_id']);
            })
            ->whereHas('BookingBranchReferenceBK', function ($query) {
                $query->when($this->search_request['booking_branch_id'] ?? false, function ($query) {
                    $query->where('booking_branch_id', 'like', '%' . $this->search_request['booking_branch_id'] . '%');
                });
            })->get();
        $forreschedule = CrmBooking::where('final_status_id', 6)->with(
            'BookingTypeReferenceBK',
            'VehicleTypeReferenceBK',
            'TimeslotReferenceBK',
            'WalkinReferenceBK',
            'ActivityReferenceBK',
            'ShipperReferenceBK',
            'FinalStatusReferenceBK',
            'BookingBranchReferenceBK',
            'MarketingChannelReferenceBK',
            'CreatedByBK',
            'BookingAttachmentHasManyBK',
            'BookingLogsHasManyBK',
            'BookingRemarksHasManyBK',
            'BookingConsigneeHasManyBK',
            'attachments',
            'BookingShipper',
            'BookingChannelReferenceBK',
        )
        ->when($this->search_request['booking_reference_no'] ?? false, function ($query) {
            $query->where('booking_reference_no', $this->search_request['booking_reference_no']);
        })
            ->when($this->search_request['created_at'] ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->search_request['created_at'] . '%');
            })
            ->when($this->search_request['pickup_date'] ?? false, function ($query) {
                $query->where('pickup_date', 'like', '%' . $this->search_request['pickup_date'] . '%');
            })
            ->whereHas('BookingShipper', function ($query) {
                $query->when($this->search_request['s_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['s_name'] . '%');
                });
            })
            ->whereHas('BookingConsigneeHasManyBK', function ($query) {
                $query->when($this->search_request['c_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['c_name'] . '%');
                });
            })
            ->when($this->search_request['final_status_id'] ?? false, function ($query) {
                $query->where('final_status_id', $this->search_request['final_status_id']);
            })
            ->whereHas('BookingBranchReferenceBK', function ($query) {
                $query->when($this->search_request['booking_branch_id'] ?? false, function ($query) {
                    $query->where('booking_branch_id', 'like', '%' . $this->search_request['booking_branch_id'] . '%');
                });
            })->get();
        $forcancel = CrmBooking::where('final_status_id', 7)->with(
            'BookingTypeReferenceBK',
            'VehicleTypeReferenceBK',
            'TimeslotReferenceBK',
            'WalkinReferenceBK',
            'ActivityReferenceBK',
            'ShipperReferenceBK',
            'FinalStatusReferenceBK',
            'BookingBranchReferenceBK',
            'MarketingChannelReferenceBK',
            'CreatedByBK',
            'BookingAttachmentHasManyBK',
            'BookingLogsHasManyBK',
            'BookingRemarksHasManyBK',
            'BookingConsigneeHasManyBK',
            'attachments',
            'BookingShipper',
            'BookingChannelReferenceBK',
        )
        ->when($this->search_request['booking_reference_no'] ?? false, function ($query) {
            $query->where('booking_reference_no', $this->search_request['booking_reference_no']);
        })
            ->when($this->search_request['created_at'] ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->search_request['created_at'] . '%');
            })
            ->when($this->search_request['pickup_date'] ?? false, function ($query) {
                $query->where('pickup_date', 'like', '%' . $this->search_request['pickup_date'] . '%');
            })
            ->whereHas('BookingShipper', function ($query) {
                $query->when($this->search_request['s_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['s_name'] . '%');
                });
            })
            ->whereHas('BookingConsigneeHasManyBK', function ($query) {
                $query->when($this->search_request['c_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['c_name'] . '%');
                });
            })
            ->when($this->search_request['final_status_id'] ?? false, function ($query) {
                $query->where('final_status_id', $this->search_request['final_status_id']);
            })
            ->whereHas('BookingBranchReferenceBK', function ($query) {
                $query->when($this->search_request['booking_branch_id'] ?? false, function ($query) {
                    $query->where('booking_branch_id', 'like', '%' . $this->search_request['booking_branch_id'] . '%');
                });
            })->get();
        $advanced = CrmBooking::where('final_status_id', 8)->with(
            'BookingTypeReferenceBK',
            'VehicleTypeReferenceBK',
            'TimeslotReferenceBK',
            'WalkinReferenceBK',
            'ActivityReferenceBK',
            'ShipperReferenceBK',
            'FinalStatusReferenceBK',
            'BookingBranchReferenceBK',
            'MarketingChannelReferenceBK',
            'CreatedByBK',
            'BookingAttachmentHasManyBK',
            'BookingLogsHasManyBK',
            'BookingRemarksHasManyBK',
            'BookingConsigneeHasManyBK',
            'attachments',
            'BookingShipper',
            'BookingChannelReferenceBK',
        )
        ->when($this->search_request['booking_reference_no'] ?? false, function ($query) {
            $query->where('booking_reference_no', $this->search_request['booking_reference_no']);
        })
            ->when($this->search_request['created_at'] ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->search_request['created_at'] . '%');
            })
            ->when($this->search_request['pickup_date'] ?? false, function ($query) {
                $query->where('pickup_date', 'like', '%' . $this->search_request['pickup_date'] . '%');
            })
            ->whereHas('BookingShipper', function ($query) {
                $query->when($this->search_request['s_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['s_name'] . '%');
                });
            })
            ->whereHas('BookingConsigneeHasManyBK', function ($query) {
                $query->when($this->search_request['c_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['c_name'] . '%');
                });
            })
            ->when($this->search_request['final_status_id'] ?? false, function ($query) {
                $query->where('final_status_id', $this->search_request['final_status_id']);
            })
            ->whereHas('BookingBranchReferenceBK', function ($query) {
                $query->when($this->search_request['booking_branch_id'] ?? false, function ($query) {
                    $query->where('booking_branch_id', 'like', '%' . $this->search_request['booking_branch_id'] . '%');
                });
            })->get();
        $reqcancel = CrmBooking::where('final_status_id', 9)->with(
            'BookingTypeReferenceBK',
            'VehicleTypeReferenceBK',
            'TimeslotReferenceBK',
            'WalkinReferenceBK',
            'ActivityReferenceBK',
            'ShipperReferenceBK',
            'FinalStatusReferenceBK',
            'BookingBranchReferenceBK',
            'MarketingChannelReferenceBK',
            'CreatedByBK',
            'BookingAttachmentHasManyBK',
            'BookingLogsHasManyBK',
            'BookingRemarksHasManyBK',
            'BookingConsigneeHasManyBK',
            'attachments',
            'BookingShipper',
            'BookingChannelReferenceBK',
        )
        ->when($this->search_request['booking_reference_no'] ?? false, function ($query) {
            $query->where('booking_reference_no', $this->search_request['booking_reference_no']);
        })
            ->when($this->search_request['created_at'] ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->search_request['created_at'] . '%');
            })
            ->when($this->search_request['pickup_date'] ?? false, function ($query) {
                $query->where('pickup_date', 'like', '%' . $this->search_request['pickup_date'] . '%');
            })
            ->whereHas('BookingShipper', function ($query) {
                $query->when($this->search_request['s_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['s_name'] . '%');
                });
            })
            ->whereHas('BookingConsigneeHasManyBK', function ($query) {
                $query->when($this->search_request['c_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['c_name'] . '%');
                });
            })
            ->when($this->search_request['final_status_id'] ?? false, function ($query) {
                $query->where('final_status_id', $this->search_request['final_status_id']);
            })
            ->whereHas('BookingBranchReferenceBK', function ($query) {
                $query->when($this->search_request['booking_branch_id'] ?? false, function ($query) {
                    $query->where('booking_branch_id', 'like', '%' . $this->search_request['booking_branch_id'] . '%');
                });
            })->get();
        $reqresched = CrmBooking::where('final_status_id', 10)->with(
            'BookingTypeReferenceBK',
            'VehicleTypeReferenceBK',
            'TimeslotReferenceBK',
            'WalkinReferenceBK',
            'ActivityReferenceBK',
            'ShipperReferenceBK',
            'FinalStatusReferenceBK',
            'BookingBranchReferenceBK',
            'MarketingChannelReferenceBK',
            'CreatedByBK',
            'BookingAttachmentHasManyBK',
            'BookingLogsHasManyBK',
            'BookingRemarksHasManyBK',
            'BookingConsigneeHasManyBK',
            'attachments',
            'BookingShipper',
            'BookingChannelReferenceBK',
        )
        ->when($this->search_request['booking_reference_no'] ?? false, function ($query) {
            $query->where('booking_reference_no', $this->search_request['booking_reference_no']);
        })
            ->when($this->search_request['created_at'] ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->search_request['created_at'] . '%');
            })
            ->when($this->search_request['pickup_date'] ?? false, function ($query) {
                $query->where('pickup_date', 'like', '%' . $this->search_request['pickup_date'] . '%');
            })
            ->whereHas('BookingShipper', function ($query) {
                $query->when($this->search_request['s_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['s_name'] . '%');
                });
            })
            ->whereHas('BookingConsigneeHasManyBK', function ($query) {
                $query->when($this->search_request['c_name'] ?? false, function ($query) {
                    $query->where('name', 'like', '%' . $this->search_request['c_name'] . '%');
                });
            })
            ->when($this->search_request['final_status_id'] ?? false, function ($query) {
                $query->where('final_status_id', $this->search_request['final_status_id']);
            })
            ->whereHas('BookingBranchReferenceBK', function ($query) {
                $query->when($this->search_request['booking_branch_id'] ?? false, function ($query) {
                    $query->where('booking_branch_id', 'like', '%' . $this->search_request['booking_branch_id'] . '%');
                });
            })->get();

        $cfordisp = $fordisp->count();
        $crescheduled = $forreschedule->count();
        $ccancelled = $forcancel->count();
        $cforconf = $forconf->count();
        $cconfirmed = $confirmed->count();
        $congoing = $ongoing->count();
        $ccompleted = $completed->count();
        $cadvanced = $advanced->count();
        $creqcancel = $reqcancel->count();
        $creqresched = $reqresched->count();


        $this->header_cards = [
            [
                'title' => 'For Dispatch',
                'value' => $cfordisp,
                'class' => 'bg-white border border-gray-400 shadow-sm text-lg',
                'icon' => '<svg class="w-10 mt-5 h-9" style="color:#e4d5ff;" aria-hidden="true" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M640 0V400c0 61.9-50.1 112-112 112c-61 0-110.5-48.7-112-109.3L48.4 502.9c-17.1 4.6-34.6-5.4-39.3-22.5s5.4-34.6 22.5-39.3L352 353.8V64c0-35.3 28.7-64 64-64H640zM576 400a48 48 0 1 0 -96 0 48 48 0 1 0 96 0zM23.1 207.7c-4.6-17.1 5.6-34.6 22.6-39.2l46.4-12.4 20.7 77.3c2.3 8.5 11.1 13.6 19.6 11.3l30.9-8.3c8.5-2.3 13.6-11.1 11.3-19.6l-20.7-77.3 46.4-12.4c17.1-4.6 34.6 5.6 39.2 22.6l41.4 154.5c4.6 17.1-5.6 34.6-22.6 39.2L103.7 384.9c-17.1 4.6-34.6-5.6-39.2-22.6L23.1 207.7z">
                            </path>
                            </svg>',
                'color' => 'text-blue',
                'action' => 'stats_1',
                'id' => 1
            ],
            [
                'title' => 'For Confirmation',
                'value' => $cforconf,
                'class' => 'bg-white border border-gray-400 shadow-sm text-xs',
                'icon' => '<svg class="mt-5 ml-2 w-9 h-9" style="color:#d9e6ff;" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z">
                            </path>
                            </svg>',
                'color' => 'text-blue',
                'action' => 'stats_1',
                'id' => 2
            ],
            [
                'title' => 'Confirmed',
                'value' => $cconfirmed,
                'class' => 'bg-white border border-gray-400 shadow-sm text-lg',
                'icon' => '<svg class="mt-5 ml-3 w-9 h-9" style="color:#b7cfff;" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M192 0c-41.8 0-77.4 26.7-90.5 64H64C28.7 64 0 92.7 0 128V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V128c0-35.3-28.7-64-64-64H282.5C269.4 26.7 233.8 0 192 0zm0 64a32 32 0 1 1 0 64 32 32 0 1 1 0-64zM305 273L177 401c-9.4 9.4-24.6 9.4-33.9 0L79 337c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L271 239c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                            </path>
                            </svg>',
                'color' => 'text-blue',
                'action' => 'stats_1',
                'id' => 3
            ],
            [
                'title' => 'Ongoing',
                'value' => $congoing,
                'class' => 'bg-white border border-gray-400 shadow-sm text-lg',
                'icon' => '<svg class="mt-5 ml-2 w-9 h-9" style="color:#ffcf99;" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M464 256A208 208 0 1 1 48 256a208 208 0 1 1 416 0zM0 256a256 256 0 1 0 512 0A256 256 0 1 0 0 256zM232 120V256c0 8 4 15.5 10.7 20l96 64c11 7.4 25.9 4.4 33.3-6.7s4.4-25.9-6.7-33.3L280 243.2V120c0-13.3-10.7-24-24-24s-24 10.7-24 24z">
                            </path>
                            </svg>',
                'color' => 'text-blue',
                'action' => 'stats_1',
                'id' => 4
            ],
            [
                'title' => 'Completed',
                'value' => $ccompleted,
                'class' => 'bg-white border border-gray-400 shadow-sm text-lg',
                'icon' => '<svg class="mt-5 ml-2 w-9 h-9" style="color:#99add6;" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                            </path>
                            </svg>',
                'color' => 'text-blue',
                'action' => 'stats_1',
                'id' => 5
            ],
            [
                'title' => 'Rescheduled',
                'value' => $crescheduled,
                'class' => 'bg-white border border-gray-400 shadow-sm text-lg',
                'icon' => '<svg class="mt-5 ml-2 w-9 h-9" style="color:#ffd5cf;" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M128 0c17.7 0 32 14.3 32 32V64H288V32c0-17.7 14.3-32 32-32s32 14.3 32 32V64h48c26.5 0 48 21.5 48 48v48H0V112C0 85.5 21.5 64 48 64H96V32c0-17.7 14.3-32 32-32zM0 192H448V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V192zm64 80v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H80c-8.8 0-16 7.2-16 16zm128 0v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H208c-8.8 0-16 7.2-16 16zm144-16c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H336zM64 400v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H80c-8.8 0-16 7.2-16 16zm144-16c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H208zm112 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H336c-8.8 0-16 7.2-16 16z">
                            </path>
                            </svg>',
                'color' => 'text-blue',
                'action' => 'stats_1',
                'id' => 6
            ],
            [
                'title' => 'Cancelled',
                'value' => $ccancelled,
                'class' => 'bg-white border border-gray-400 shadow-sm text-lg ',
                'icon' => '<svg class="mt-5 ml-2 w-9 h-9" style="color:#ff9999;" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M175 175C184.4 165.7 199.6 165.7 208.1 175L255.1 222.1L303 175C312.4 165.7 327.6 165.7 336.1 175C346.3 184.4 346.3 199.6 336.1 208.1L289.9 255.1L336.1 303C346.3 312.4 346.3 327.6 336.1 336.1C327.6 346.3 312.4 346.3 303 336.1L255.1 289.9L208.1 336.1C199.6 346.3 184.4 346.3 175 336.1C165.7 327.6 165.7 312.4 175 303L222.1 255.1L175 208.1C165.7 199.6 165.7 184.4 175 175V175zM512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256zM256 48C141.1 48 48 141.1 48 256C48 370.9 141.1 464 256 464C370.9 464 464 370.9 464 256C464 141.1 370.9 48 256 48z">
                            </path>
                            </svg>',
                'color' => 'text-blue',
                'action' => 'stats_1',
                'id' => 7
            ],
            [
                'title' => 'Advance',
                'value' => $cadvanced,
                'class' => 'bg-white border border-gray-400 shadow-sm text-lg ',
                'icon' => '<svg class="mt-5 ml-2 w-9 h-9" style="color:#99cc99;" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M0 256a256 256 0 1 0 512 0A256 256 0 1 0 0 256zM241 377c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l87-87-87-87c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0L345 239c9.4 9.4 9.4 24.6 0 33.9L241 377z">
                            </path>
                            </svg>',
                'color' => 'text-blue',
                'action' => 'stats_1',
                'id' => 8
            ],
            [
                'title' => 'Request for Cancellation',
                'value' => $creqcancel,
                'class' => 'bg-white border border-gray-400 shadow-sm text-lg ',
                'icon' => '<svg class="mt-5 ml-2 w-9 h-9" style="color:#ffecb8;" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M456 32h-304C121.1 32 96 57.13 96 88v320c0 13.22-10.77 24-24 24S48 421.2 48 408V112c0-13.25-10.75-24-24-24S0 98.75 0 112v296C0 447.7 32.3 480 72 480h352c48.53 0 88-39.47 88-88v-304C512 57.13 486.9 32 456 32zM464 392c0 22.06-17.94 40-40 40H139.9C142.5 424.5 144 416.4 144 408v-320c0-4.406 3.594-8 8-8h304c4.406 0 8 3.594 8 8V392zM264 272h-64C186.8 272 176 282.8 176 296S186.8 320 200 320h64C277.3 320 288 309.3 288 296S277.3 272 264 272zM408 272h-64C330.8 272 320 282.8 320 296S330.8 320 344 320h64c13.25 0 24-10.75 24-24S421.3 272 408 272zM264 352h-64c-13.25 0-24 10.75-24 24s10.75 24 24 24h64c13.25 0 24-10.75 24-24S277.3 352 264 352zM408 352h-64C330.8 352 320 362.8 320 376s10.75 24 24 24h64c13.25 0 24-10.75 24-24S421.3 352 408 352zM400 112h-192c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h192c17.67 0 32-14.33 32-32v-64C432 126.3 417.7 112 400 112z">
                            </path>
                            </svg>',
                'color' => 'text-blue',
                'action' => 'stats_1',
                'id' => 9
            ],
            [
                'title' => 'Request for Reschedule',
                'value' => $creqresched,
                'class' => 'bg-white border border-gray-400 shadow-sm text-lg ',
                'icon' => '<svg class="mt-5 ml-2 w-9 h-9" style="color:#ffecb8;" aria-hidden="true" focusable="false" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M456 32h-304C121.1 32 96 57.13 96 88v320c0 13.22-10.77 24-24 24S48 421.2 48 408V112c0-13.25-10.75-24-24-24S0 98.75 0 112v296C0 447.7 32.3 480 72 480h352c48.53 0 88-39.47 88-88v-304C512 57.13 486.9 32 456 32zM464 392c0 22.06-17.94 40-40 40H139.9C142.5 424.5 144 416.4 144 408v-320c0-4.406 3.594-8 8-8h304c4.406 0 8 3.594 8 8V392zM264 272h-64C186.8 272 176 282.8 176 296S186.8 320 200 320h64C277.3 320 288 309.3 288 296S277.3 272 264 272zM408 272h-64C330.8 272 320 282.8 320 296S330.8 320 344 320h64c13.25 0 24-10.75 24-24S421.3 272 408 272zM264 352h-64c-13.25 0-24 10.75-24 24s10.75 24 24 24h64c13.25 0 24-10.75 24-24S277.3 352 264 352zM408 352h-64C330.8 352 320 362.8 320 376s10.75 24 24 24h64c13.25 0 24-10.75 24-24S421.3 352 408 352zM400 112h-192c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h192c17.67 0 32-14.33 32-32v-64C432 126.3 417.7 112 400 112z">
                            </path>
                            </svg>',
                'color' => 'text-blue',
                'action' => 'stats_1',
                'id' => 10
            ],
        ];
    }

    public function loadBookingHeaderCards()
    {
        $bookchan = CrmRateApplyFor::withCount('BookingChannel')->get();
        
        $bookchan = CrmRateApplyFor::withCount(['BookingChannel' => function ($query) {
            $query->when($this->search_request['booking_reference_no'] ?? false, function ($query) {
                $query->where('booking_reference_no', $this->search_request['booking_reference_no']);
            })
                ->when($this->search_request['created_at'] ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->search_request['created_at'] . '%');
                })
                ->when($this->search_request['pickup_date'] ?? false, function ($query) {
                    $query->where('pickup_date', 'like', '%' . $this->search_request['pickup_date'] . '%');
                })
                ->whereHas('BookingShipper', function ($query) {
                    $query->when($this->search_request['s_name'] ?? false, function ($query) {
                        $query->where('name', 'like', '%' . $this->search_request['s_name'] . '%');
                    });
                })
                ->whereHas('BookingConsigneeHasManyBK', function ($query) {
                    $query->when($this->search_request['c_name'] ?? false, function ($query) {
                        $query->where('name', 'like', '%' . $this->search_request['c_name'] . '%');
                    });
                })
                ->when($this->search_request['final_status_id'] ?? false, function ($query) {
                    $query->where('final_status_id', $this->search_request['final_status_id']);
                })
                ->whereHas('BookingBranchReferenceBK', function ($query) {
                    $query->when($this->search_request['booking_branch_id'] ?? false, function ($query) {
                        $query->where('booking_branch_id', 'like', '%' . $this->search_request['booking_branch_id'] . '%');
                    });
                });
            }])
        
        ->get();

        // $bookchan = CrmRateApplyFor::withCount('BookingChannel')
        // ->with(
        //         'BookingTypeReferenceBK',
        //         'VehicleTypeReferenceBK',
        //         'TimeslotReferenceBK',
        //         'WalkinReferenceBK',
        //         'ActivityReferenceBK',
        //         'ShipperReferenceBK',
        //         'FinalStatusReferenceBK',
        //         'BookingBranchReferenceBK',
        //         'MarketingChannelReferenceBK',
        //         'CreatedByBK',
        //         'BookingAttachmentHasManyBK',
        //         'BookingLogsHasManyBK',
        //         'BookingRemarksHasManyBK',
        //         'BookingConsigneeHasManyBK',
        //         'attachments',
        //         'BookingShipper',
        //         'BookingChannelReferenceBK',
        //     )
        //     ->when($this->search_request['booking_reference_no'] ?? false, function ($query) {
        //         $query->where('booking_reference_no', $this->search_request['booking_reference_no']);
        //     })
        //         ->when($this->search_request['created_at'] ?? false, function ($query) {
        //             $query->where('created_at', 'like', '%' . $this->search_request['created_at'] . '%');
        //         })
        //         ->when($this->search_request['pickup_date'] ?? false, function ($query) {
        //             $query->where('pickup_date', 'like', '%' . $this->search_request['pickup_date'] . '%');
        //         })
        //         ->whereHas('BookingShipper', function ($query) {
        //             $query->when($this->search_request['s_name'] ?? false, function ($query) {
        //                 $query->where('name', 'like', '%' . $this->search_request['s_name'] . '%');
        //             });
        //         })
        //         ->whereHas('BookingConsigneeHasManyBK', function ($query) {
        //             $query->when($this->search_request['c_name'] ?? false, function ($query) {
        //                 $query->where('name', 'like', '%' . $this->search_request['c_name'] . '%');
        //             });
        //         })
        //         ->when($this->search_request['final_status_id'] ?? false, function ($query) {
        //             $query->where('final_status_id', $this->search_request['final_status_id']);
        //         })
        //         ->whereHas('BookingBranchReferenceBK', function ($query) {
        //             $query->when($this->search_request['booking_branch_id'] ?? false, function ($query) {
        //                 $query->where('booking_branch_id', 'like', '%' . $this->search_request['booking_branch_id'] . '%');
        //             });
        //         })->get();
      

        if (isset($bookchan)) {
            $this->bookingc_header_cards = [
                [
                    'title' => 'All Booking',
                    'value' => $bookchan->sum('booking_channel_count'),
                    'class' => 'bg-white border border-gray-400 shadow-sm text-lg ',
                    'icon' => '<svg class="mt-5 w-9 h-9" style="color:#99add6;" aria-hidden="true" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M192 0c-41.8 0-77.4 26.7-90.5 64H64C28.7 64 0 92.7 0 128V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V128c0-35.3-28.7-64-64-64H282.5C269.4 26.7 233.8 0 192 0zm0 64a32 32 0 1 1 0 64 32 32 0 1 1 0-64zM72 272a24 24 0 1 1 48 0 24 24 0 1 1 -48 0zm104-16H304c8.8 0 16 7.2 16 16s-7.2 16-16 16H176c-8.8 0-16-7.2-16-16s7.2-16 16-16zM72 368a24 24 0 1 1 48 0 24 24 0 1 1 -48 0zm88 0c0-8.8 7.2-16 16-16H304c8.8 0 16 7.2 16 16s-7.2 16-16 16H176c-8.8 0-16-7.2-16-16z">
                            </path>
                            </svg>',
                    'color' => 'text-blue',
                    'action' => 'bc_1',
                    'id' => false
                ],
                [
                    'title' => 'CIS 3',
                    'value' => $bookchan[0]->booking_channel_count,
                    'class' => 'bg-white border border-gray-400 shadow-sm text-lg',
                    'icon' => '<svg class="mt-5 -ml-4 w-9 h-9" style="color:#99add6;" aria-hidden="true" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M305.5 135.3c7.1-6.3 9.9-16.2 6.2-25c-2.3-5.3-4.8-10.5-7.6-15.5L301 89.4c-3-5-6.3-9.9-9.8-14.6c-5.7-7.6-15.7-10.1-24.7-7.1l-28.2 9.3c-10.7-8.8-23-16-36.2-20.9L196 27.1c-1.9-9.3-9.1-16.7-18.5-17.8C170.9 8.4 164.2 8 157.4 8h-.7c-6.8 0-13.5 .4-20.1 1.2c-9.4 1.1-16.6 8.6-18.5 17.8L112 56.1c-13.3 5-25.5 12.1-36.2 20.9L47.5 67.8c-9-3-19-.5-24.7 7.1c-3.5 4.7-6.8 9.6-9.9 14.6l-3 5.3c-2.8 5-5.3 10.2-7.6 15.6c-3.7 8.7-.9 18.6 6.2 25l22.2 19.8C29.6 161.9 29 168.9 29 176s.6 14.1 1.7 20.9L8.5 216.7c-7.1 6.3-9.9 16.2-6.2 25c2.3 5.3 4.8 10.5 7.6 15.6l3 5.2c3 5.1 6.3 9.9 9.9 14.6c5.7 7.6 15.7 10.1 24.7 7.1l28.2-9.3c10.7 8.8 23 16 36.2 20.9l6.1 29.1c1.9 9.3 9.1 16.7 18.5 17.8c6.7 .8 13.5 1.2 20.4 1.2s13.7-.4 20.4-1.2c9.4-1.1 16.6-8.6 18.5-17.8l6.1-29.1c13.3-5 25.5-12.1 36.2-20.9l28.2 9.3c9 3 19 .5 24.7-7.1c3.5-4.7 6.8-9.5 9.8-14.6l3.1-5.4c2.8-5 5.3-10.2 7.6-15.5c3.7-8.7 .9-18.6-6.2-25l-22.2-19.8c1.1-6.8 1.7-13.8 1.7-20.9s-.6-14.1-1.7-20.9l22.2-19.8zM109 176a48 48 0 1 1 96 0 48 48 0 1 1 -96 0zM501.7 500.5c6.3 7.1 16.2 9.9 25 6.2c5.3-2.3 10.5-4.8 15.5-7.6l5.4-3.1c5-3 9.9-6.3 14.6-9.8c7.6-5.7 10.1-15.7 7.1-24.7l-9.3-28.2c8.8-10.7 16-23 20.9-36.2l29.1-6.1c9.3-1.9 16.7-9.1 17.8-18.5c.8-6.7 1.2-13.5 1.2-20.4s-.4-13.7-1.2-20.4c-1.1-9.4-8.6-16.6-17.8-18.5L580.9 307c-5-13.3-12.1-25.5-20.9-36.2l9.3-28.2c3-9 .5-19-7.1-24.7c-4.7-3.5-9.6-6.8-14.6-9.9l-5.3-3c-5-2.8-10.2-5.3-15.6-7.6c-8.7-3.7-18.6-.9-25 6.2l-19.8 22.2c-6.8-1.1-13.8-1.7-20.9-1.7s-14.1 .6-20.9 1.7l-19.8-22.2c-6.3-7.1-16.2-9.9-25-6.2c-5.3 2.3-10.5 4.8-15.6 7.6l-5.2 3c-5.1 3-9.9 6.3-14.6 9.9c-7.6 5.7-10.1 15.7-7.1 24.7l9.3 28.2c-8.8 10.7-16 23-20.9 36.2L312.1 313c-9.3 1.9-16.7 9.1-17.8 18.5c-.8 6.7-1.2 13.5-1.2 20.4s.4 13.7 1.2 20.4c1.1 9.4 8.6 16.6 17.8 18.5l29.1 6.1c5 13.3 12.1 25.5 20.9 36.2l-9.3 28.2c-3 9-.5 19 7.1 24.7c4.7 3.5 9.5 6.8 14.6 9.8l5.4 3.1c5 2.8 10.2 5.3 15.5 7.6c8.7 3.7 18.6 .9 25-6.2l19.8-22.2c6.8 1.1 13.8 1.7 20.9 1.7s14.1-.6 20.9-1.7l19.8 22.2zM461 304a48 48 0 1 1 0 96 48 48 0 1 1 0-96z">
                            </path>
                            </svg>',
                    'color' => 'text-blue',
                    'action' => 'bc_1',
                    'id' => 1
                ],
                [
                    'title' => 'Website Booking',
                    'value' => $bookchan[1]->booking_channel_count,
                    'class' => 'bg-white border border-gray-400 shadow-sm text-lg',
                    'icon' => '<svg class="mt-5 w-9 h-9" style="color:#99add6;" aria-hidden="true" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M0 128C0 92.7 28.7 64 64 64H448c35.3 0 64 28.7 64 64V384c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V128zm64 32v64c0 17.7 14.3 32 32 32H416c17.7 0 32-14.3 32-32V160c0-17.7-14.3-32-32-32H96c-17.7 0-32 14.3-32 32zM80 320c-13.3 0-24 10.7-24 24s10.7 24 24 24h56c13.3 0 24-10.7 24-24s-10.7-24-24-24H80zm136 0c-13.3 0-24 10.7-24 24s10.7 24 24 24h48c13.3 0 24-10.7 24-24s-10.7-24-24-24H216z">
                            </path>
                            </svg>',
                    'color' => 'text-blue',
                    'action' => 'bc_1',
                    'id' => 2
                ],
                [
                    'title' => 'CaPEx Mobile Application',
                    'value' => $bookchan[2]->booking_channel_count,
                    'class' => 'bg-white border border-gray-400 shadow-sm text-lg',
                    'icon' => '<svg class="mt-5 ml-2 w-9 h-9" style="color:#99add6;" aria-hidden="true" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M0 64C0 28.7 28.7 0 64 0H288c35.3 0 64 28.7 64 64V448c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V64zM208 448a32 32 0 1 0 -64 0 32 32 0 1 0 64 0zM288 64H64V384H288V64z">
                            </path>
                            </svg>',
                    'color' => 'text-blue',
                    'action' => 'bc_1',
                    'id' => 3
                ],
                [
                    'title' => 'VIP Portal',
                    'value' => $bookchan[3]->booking_channel_count,
                    'class' => 'bg-white border border-gray-400 shadow-sm text-lg',
                    'icon' => '<svg class="mt-5 w-9 h-9" style="color:#99add6;" aria-hidden="true" data-prefix="fas"
                            data-icon="users-cog" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M0 128C0 92.7 28.7 64 64 64H448c35.3 0 64 28.7 64 64V384c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V128zm64 32v64c0 17.7 14.3 32 32 32H416c17.7 0 32-14.3 32-32V160c0-17.7-14.3-32-32-32H96c-17.7 0-32 14.3-32 32zM80 320c-13.3 0-24 10.7-24 24s10.7 24 24 24h56c13.3 0 24-10.7 24-24s-10.7-24-24-24H80zm136 0c-13.3 0-24 10.7-24 24s10.7 24 24 24h48c13.3 0 24-10.7 24-24s-10.7-24-24-24H216z">
                            </path>
                            </svg>',
                    'color' => 'text-blue',
                    'action' => 'bc_1',
                    'id' => 4
                ],
            ];
        }
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('crm.sales.booking-mgmt.edit', 'edit', $data['id']);
            $this->book_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'update_app1') {

            if ($data['approval_status'] == 1) {
                $this->approve_cancel_modal = true;
                $this->book_id = $data['id'];
                $cancelreasonaps = CrmBookingLogs::with('CancelReasonReferenceLG')->where('booking_id', $this->book_id)->where('status_id', 9)->latest()->first();
                $this->approve_cancel_reason = $cancelreasonaps->CancelReasonReferenceLG->name;
                return;
            }
            $this->decline_cancel_modal = true;
            $this->book_id = $data['id'];
            $cancelreasondecs = CrmBookingLogs::with('CancelReasonReferenceLG')->where('booking_id', $this->book_id)->where('status_id', 9)->latest()->first();
            $this->decline_cancel_reason = $cancelreasondecs->CancelReasonReferenceLG->name;
        } else if ($action_type == 'update_app2') {
            if ($data['approval_status']) {
                if ($data['approval_status'] == 1) {
                    $this->approve_resched_modal = true;
                    $this->book_id = $data['id'];
                    $reschedreasonaps = CrmBookingLogs::with('ReschedReasonReferenceLG')->where('booking_id', $this->book_id)->where('status_id', 10)->latest()->first();
                    $this->approve_resched_reason = $reschedreasonaps->ReschedReasonReferenceLG->name;
                    return;
                }
                $this->decline_resched_modal = true;
                $this->book_id = $data['id'];
                $reschedreasondecs = CrmBookingLogs::with('ReschedReasonReferenceLG')->where('booking_id', $this->book_id)->where('status_id', 10)->latest()->first();
                $this->decline_resched_reason = $reschedreasondecs->ReschedReasonReferenceLG->name;
            }
        }
        // elseif ($action_type == 'redirect') {
        //     $this->emitTo('oims.order-management.transaction-entry.index', 'redirect', $this->transid);
        //     $this->redirect_modal = true;
        //     $this->transid = $data['id'];
        // }
    }
    // public $transid;
    // public $redirect_modal = false;
    // public function openModal()
    // {

    //     $this->create_modal = true;

    // }

    public function closeModal($action_type)
    {
        // if ($action_type == 'redirect') {
        //     $this->redirect_modal = false;
        // }
        if ($action_type == 'create') {
            $this->create_modal = false;
        }
        if ($action_type == 'bref') {
            $this->bookref_modal = false;
        }
        if ($action_type == 'c_det') {
            $this->consigneedet_modal = false;
        }
        if ($action_type == 'workins') {
            $this->workinst_modal = false;
        }
        if ($action_type == 'no_workins') {
            $this->no_workinst_modal = false;
        }
        if ($action_type == 'vimg') {
            $this->vimg_modal = false;
        }
        if ($action_type == 'vpdf') {
            $this->vpdf_modal = false;
        }
        if ($action_type == 'intrem') {
            $this->internalrem_modal = false;
        }
        if ($action_type == 'cancelbk') {
            $this->cancel_modal = false;
        }
        if ($action_type == 'reschedulebk') {
            $this->reschedule_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'update_app1') {
            $this->approve_cancel_modal = false;
            $this->decline_cancel_modal = false;
        } elseif ($action_type == 'update_app2') {
            $this->approve_resched_modal = false;
            $this->decline_resched_modal = false;
        }
    }

    public function updateApp2($id, $value1)
    {
        $app2 = CrmBooking::findOrFail($id);
        $applogs2 = CrmBookingLogs::where('booking_id', $id)->where('status_id', 10)->latest()->first();

        // dd($applogs);
        $app2->update([
            'approval_status' => $value1,
        ]);

        if ($app2->approval_status == 1) {
            $app2->update([
                'approval_status' => $value1,
                'final_status_id' => 6,
            ]);
            $applogs2->update([
                'approve_user_id' => Auth::user()->id,
                'approve_date' => date("Y-m-d G:i:s"),
                'status_id' => 6,
            ]);
        }

        if ($value1 == 1) {

            $this->emitTo('crm.sales.booking-mgmt.index', 'close_modal', 'update_app2');
            $this->emitTo('crm.sales.booking-mgmt.index', 'index');
            $this->sweetAlert('', 'Resched Request Successfully Approved!');
            return;
        } elseif ($app2->approval_status == 2) {
            $app2->update([
                'approval_status' => $value1,
            ]);
            $applogs2->update([
                'approve_user_id' => Auth::user()->id,
                'approve_date' => date("Y-m-d G:i:s"),
            ]);
        }

        $this->emitTo('crm.sales.booking-mgmt.index', 'close_modal', 'update_app2');
        $this->emitTo('crm.sales.booking-mgmt.index', 'index');
        $this->sweetAlert('', 'Resched Request Successfully Declined!');
    }

    public function updateApp1($id, $value1)
    {
        $app1 = CrmBooking::findOrFail($id);
        $applogs = CrmBookingLogs::where('booking_id', $id)->where('status_id', 9)->latest()->first();
        // dd($applogs);
        $app1->update([
            'approval_status' => $value1,
        ]);

        if ($app1->approval_status == 1) {
            $app1->update([
                'approval_status' => $value1,
                'final_status_id' => 7,
            ]);
            $applogs->update([
                'approve_user_id' => Auth::user()->id,
                'approve_date' => date("Y-m-d G:i:s"),
                'status_id' => 7,
            ]);
        }

        if ($value1 == 1) {

            $this->emitTo('crm.sales.booking-mgmt.index', 'close_modal', 'update_app1');
            $this->emitTo('crm.sales.booking-mgmt.index', 'index');
            $this->sweetAlert('', 'Cancellation has been successfully approved!');
            return;
        } elseif ($app1->approval_status == 2) {
            $app1->update([
                'approval_status' => $value1,
            ]);
            $applogs->update([
                'approve_user_id' => Auth::user()->id,
                'approve_date' => date("Y-m-d G:i:s"),
            ]);
        }

        $this->emitTo('crm.sales.booking-mgmt.index', 'close_modal', 'update_app1');
        $this->emitTo('crm.sales.booking-mgmt.index', 'index');
        $this->sweetAlert('', 'Cancellation has been successfully declined!');
    }

    public function actionbookingref(array $data, $action_type)
    {
        $viewbookref = CrmBooking::with('BookingShipper', 'ShipperReferenceBK', 'ActivityReferenceBK', 'TimeslotReferenceBK', 'FinalStatusReferenceBK', 'BookingConsigneeHasManyBK')
            ->where('id', $data['id'])->first();
        // dd($viewbookref->id);
        $this->customer_no = $viewbookref->BookingShipper->customer_no;
        // dd($this->customer_no);
        $shipdet = CrmBookingShipper::with('AccountTypeReferenceSP')->where('customer_no', $this->customer_no)->first();
        // dd($shipdet);

        $this->IDXcbId = $viewbookref->id;

        $this->IDXpickupwalk = $viewbookref->booking_category;
        $this->IDXsinglemulti = $viewbookref->consignee_category;

        $this->IDXbookingrefno = $viewbookref->booking_reference_no;
        $this->IDXbookingstat = $viewbookref->FinalStatusReferenceBK->name;
        $this->IDXfullname = $viewbookref->BookingShipper->name;
        $this->IDXfname = $viewbookref->BookingShipper->name;

        $this->IDXcompany = $viewbookref->BookingShipper->company_name;
        $this->IDXmobile_nos = $viewbookref->BookingShipper->mobile_number;
        $this->IDXemail_address = $viewbookref->BookingShipper->email_address;
        $this->IDXaddress = $viewbookref->BookingShipper->address;

        $this->IDXcustomertype = $shipdet->AccountTypeReferenceSP->name;
        $this->IDXacttype = $viewbookref->ActivityReferenceBK->activity_type;

        $this->IDXtimeslot = $viewbookref->TimeslotReferenceBK->name;

        $vbdconsignees = CrmBooking::with('ShipperReferenceBK', 'ActivityReferenceBK', 'TimeslotReferenceBK', 'FinalStatusReferenceBK', 'BookingConsigneeHasManyBK')
            ->where('id', $data['id'])->get();
        // dd($vbdconsignees);

        $this->vconsignee = [];
        foreach ($vbdconsignees as $i => $vbdconsigneess) {
            $this->vconsignee[0] = $vbdconsigneess->BookingConsigneeHasManyBK;
        }
        foreach ($this->vconsignee[0] as $i => $viewcons2) {
            $this->IDXconsid = $this->vconsignee[0][$i]['id'];
        }

        $this->action_type = $action_type;
        if ($action_type == 'bref') {
            $this->bookref_modal = true;
        }
    }

    public function actionvcdet(array $data, $action_type)
    {
        $VBconsdetails = CrmBookingConsignee::with('BookingCargoDetailsHasManyCD', 'TransportReferenceCD', 'ServiceModeReferenceCD', 'ModeOfPaymenReferenceCD')
            ->where('id', $data['id'])->get();

        $this->VBCD = [];
        $this->bookcons = [];

        foreach ($VBconsdetails as $i => $VBconsdetailss) {
            $this->VBCD = $VBconsdetailss->BookingCargoDetailsHasManyCD;
            $this->bookcons = $VBconsdetailss;
        }
        $this->IDXdecval = $this->bookcons->declared_value;
        $this->IDXtransportmode = $this->bookcons->TransportReferenceCD->name;
        $this->IDXservmode = $this->bookcons->ServiceModeReferenceCD->name;
        $this->IDXdescgoods = $this->bookcons->description_goods;
        $this->IDXmodeofp = $this->bookcons->ModeOfPaymenReferenceCD->name;

        $this->IDXref = $this->bookcons->booking_id;
        $vcdets = CrmBooking::with('BookingShipper', 'ShipperReferenceBK', 'ActivityReferenceBK', 'TimeslotReferenceBK', 'FinalStatusReferenceBK', 'BookingConsigneeHasManyBK')
            ->where('id', $this->IDXref)->first();

        $this->IDXwcount = $vcdets->consignee_count;
        $this->IDXworkins = $vcdets->work_instruction;
        $this->IDXintrem = $vcdets->remarks;
        // foreach ($this->VBCD[0] as $i => $viewcons) {
        // }
        $this->action_type = $action_type;
        if ($action_type == 'c_det') {
            $this->consigneedet_modal = true;
        }
    }

    public function actionBSLogs(array $data, $action_type)
    {
        // dd($data['id']);
        $viewbookingstatuslogs = CrmBookingLogs::with('StatusReferenceLG')
            ->where('booking_id', $data['id'])->first();

        // dd($viewbookingstatuslogs);
        $this->bstatrefno = $viewbookingstatuslogs->booking_reference_no;
        $this->bstatstat = $viewbookingstatuslogs->StatusReferenceLG->name;
        $this->bstatcreated = $viewbookingstatuslogs->created_at;

        $this->bsl_modal = true;
    }


    public function actionviewwork(array $data, $action_type)
    {
        $this->bookworkins = CrmBooking::with('BookingAttachmentHasManyBK')->where('id', $data['id'])->first();
        $this->work_ins = $this->bookworkins->work_instruction;
        $this->work_ins_id = $this->bookworkins->id;

        $this->action_type = $action_type;
        if ($action_type == 'workins') {
            $this->workinst_modal = true;
        } elseif ($action_type == 'no_workins') {
            $this->no_workinst_modal = true;
        }
    }
    public $img_id;
    public $attchm = [];

    public function actionvimg(array $data, $action_type)
    {
        if (isset($data['id'])) {

            $this->attchm = CrmBookingAttachment::where('id', $data['id'])->get();
            // dd($this->attchm);

            $this->img_id = $data['id'];
            if ($action_type == 'vimg') {
                $this->vimg_modal = true;
            }
        }
    }

    public function actionvpdf(array $data, $action_type)
    {
        if (isset($data['id'])) {

            $this->attchm = CrmBookingAttachment::where('id', $data['id'])->get();
            // dd($this->attchm);

            if ($action_type == 'vpdf') {
                $this->vpdf_modal = true;
            }
        }
    }


    public function actionviewedit(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'viewedit') {
            $this->workinst_modal = false;
            $this->no_workinst_modal = false;
            $this->emitTo('crm.sales.booking-mgmt.edit', 'edit', $data['id']);
            $this->book_id = $data['id'];
            $this->edit_modal = true;
        }
    }

    public function actionviewinternrem(array $data, $action_type)
    {
        $bookintremrks = CrmBookingRemarks::where('booking_id', $data['id'])->first();
        $this->int_rem = $bookintremrks->remarks;

        $this->action_type = $action_type;
        if ($action_type == 'intrem') {
            $this->internalrem_modal = true;
        }
    }

    public function actionCancelBk(array $data, $action_type)
    {
        $cancelbook = CrmBooking::where('id', $data['id'])->first();
        // dd($cancelbook);


        $this->brn = $cancelbook->booking_reference_no;

        $this->cancelID = $cancelbook->id;

        $this->action_type = $action_type;
        if ($action_type == 'cancelbk') {
            $this->cancel_modal = true;
        }
    }

    public function actioncancelation(BookingMgmtInterface $booking_interface, $id, $action_type)
    {
        // dd($id);
        $this->action_type = $action_type;
        if ($action_type == 'validatecancelation') {
            $response = $booking_interface->cancelationValidation($this->getRequestcancel());
            // dd($response);
            if ($response['code'] == 200) {
                $this->cancelconfirm_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function submitcancel(BookingMgmtInterface $booking_interface, $id)
    {
        // dd($id);
        $response = $booking_interface->bookcancel($this->getRequestcancel(), $id);
        if ($response['code'] == 200) {
            $this->resetCancel();
            $this->cancel_modal = false;
            $this->cancelconfirm_modal = false;
            $this->emitTo('crm.sales.booking-mgmt.index', 'closeModal', 'cancel_modal');
            $this->emitTo('crm.sales.booking-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function actionrescheduleBk(array $data, $action_type)
    {
        $reschedulebook = CrmBooking::where('id', $data['id'])->first();
        // dd($reschedulebook);

        $this->brn = $reschedulebook->booking_reference_no;

        $this->rescheduleID = $reschedulebook->id;

        $this->action_type = $action_type;
        if ($action_type == 'reschedbk') {
            $this->reschedule_modal = true;
        }
    }

    public function actionresched(BookingMgmtInterface $booking_interface, $id, $action_type)
    {
        // dd($id);
        $this->action_type = $action_type;
        if ($action_type == 'validatereschedule') {
            $response = $booking_interface->rescheduleValidation($this->getRequestresched());
            // dd($response);
            if ($response['code'] == 200) {
                $this->rescheduleconfirm_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function submitreschedule(BookingMgmtInterface $booking_interface, $id)
    {
        // dd($id);

        $response = $booking_interface->bookresched($this->getRequestresched(), $id);
        if ($response['code'] == 200) {
            $this->resetResched();
            $this->reschedule_modal = false;
            $this->rescheduleconfirm_modal = false;
            $this->emitTo('crm.sales.booking-mgmt.index', 'close_modal', 'reschedulebk');
            $this->emitTo('crm.sales.booking-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
            // $this->render();
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function search(BookingMgmtInterface $booking_interface)
    {
        $this->search_request = [
            'booking_reference_no' => $this->booking_reference_no,
            'created_at' => $this->created_at,
            'pickup_date' => $this->pickup_date,
            's_name' => $this->s_name,
            'c_name' => $this->c_name,
            // // 'name' => $this->final_status_id,
            'final_status_id' => $this->final_status_id,
            'booking_branch_id' => $this->booking_branch_id,
        ];
        // dd($this->search_request);

    }

    public function render(BookingMgmtInterface $booking_interface)
    {
        // dd("asdasd");
        $this->load();
            // $this->livewire('oims.order-management.transaction-entry.index');


        $request = [
            'paginate' => $this->paginate,
            'stats' => $this->stats,
            'stats_1' => $this->stats_1,
            'bc_1' => $this->bc_1,
        ];

        $response = $booking_interface->index($request,  $this->search_request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'book_s' => [],
                ];
        }
        // ['pages' => (new Collection($this->commercialmgmts))->paginate(5)]

        return view('livewire.crm.sales.booking-mgmt.index', [
            'book_s' => $response['result']['book_s']
            // 'book_s' => (new Collection($response))->paginate(10)
        ]);
    }
}
