<?php

namespace App\Http\Livewire\Crm\Sales\BookingMgmt;

use App\Interfaces\Crm\Sales\BookingManagement\BookingMgmt\BookingMgmtInterface;
use App\Models\Crm\BarangayReference;
use App\Models\Crm\CityReference;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmCustomerInformationAddressList;
use App\Models\Crm\CrmCustomerInformationContactPerson;
use App\Models\Crm\StateReference;
use App\Models\CrmBooking;
use App\Models\CrmBookingCargoDetails;
use App\Models\CrmBookingConsignee;
use App\Models\CrmBookingLogs;
use App\Models\CrmBookingShipper;
use App\Models\CrmModeOfPaymentReference;
use App\Models\CrmTransportMode;
use App\Traits\Crm\Sales\BookingMgmt\BookingMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\WithFileUploads;
use Livewire\Component;

class Create extends Component
{

    use BookingMgmtTrait, PopUpMessagesTrait, WithFileUploads;

    public $idcons;

    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function mount()
    {
        $this->booking_reference_no_2 = 'CRMB-' . strtoupper(uniqid());
        $this->consignee_category_1 = 1;
        $this->booking_category_1 = 1;
        $this->account_type_2 = 1;
        // $this->stype_3 = 1;
        // dd($this->cons);
        // $this->cons[$data['a']][$data['a']]['declared_value_3'] = number_format($this->price, 0, '', '');    

        // if($this->state != ''){
        //     $this->loadShipperCityMunicipality();
        // }
        // if($this->city != ''){
        //     $this->loadShipperBarangay();
        // }
    }



    public function actionview(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'view_cargo_det') {
            $this->cargo_details_modal = true;
            // dd($this->cons[$data['a']][$data['a']]);

            foreach ($this->cons[$data['a']][$data['a']] as $condet) {
                // dd($this->cons[1][1]['declared_value_3']);
                $transportmodename = CrmTransportMode::find($this->cons[$data['a']][$data['a']]['transposrt_mode_3']);
                $servicemodename = $this->cons[$data['a']][$data['a']]['service_mode_3'];
                $modeofpayment = CrmModeOfPaymentReference::find($this->cons[$data['a']][$data['a']]['mode_of_payment_3']);
                // dd($servicemodename);

                $this->decval_summary = $this->cons[$data['a']][$data['a']]['declared_value_3'];
                $this->transposrt_mode_3_summary = $transportmodename->name;
                $this->service_mode_3_summary = $servicemodename;
                $this->mode_of_payment_3_summary = $modeofpayment->name;
                $this->description_goods_3_summary = $this->cons[$data['a']][$data['a']]['description_goods_3'];
                // dd($this->service_mode_3_summary);

            }

            $this->asd = $data['a'];
            // dd($this->cardets[$data['a']]);
            $this->cardetsss = $this->cardets[$data['a']];
        }
    }

    public function actionchangeadd(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'change_addrs') {
            $this->change_address_modal = true;
        }

        $customerno = CrmCustomerInformation::where('account_no', $this->customer_no_2)->first();

        $cust_id = $customerno->id;

        $this->shipperaddresses = CrmCustomerInformationAddressList::with('stateRef', 'cityRef', 'barangayRef')->where('account_id', $cust_id)->get();
        // dd($this->shipperaddresses);
        $shipperaddressesgetprimary = CrmCustomerInformationAddressList::with('stateRef', 'cityRef', 'barangayRef')->where('account_id', $cust_id)->where('is_primary', 1)->first();

        $this->shipperAddressList = $shipperaddressesgetprimary->id;
        // dd($this->shipperAddressList);
    }

    public function actionchangeaddcons(array $data, $action_type)
    {
        // dd($this->cons[$data['a']][$data['a']]['customer_no_3']);
        $this->action_type = $action_type;
        if ($action_type == 'change_addrs_cons') {
            $this->change_address_cons_modal = true;
        }
        $customerno_cons = CrmCustomerInformation::where('account_no', $this->cons[$data['a']][$data['a']]['customer_no_3'])->first();
        // dd($customerno_cons);

        if (isset($customerno_cons->id)) {
            $cust_id_cons = $customerno_cons->id;
            // dd($cust_id_cons);

            $this->consigneeaddresses = CrmCustomerInformationAddressList::with('stateRef', 'cityRef', 'barangayRef')->where('account_id', $cust_id_cons)->get();
            $consigneeaddressesgetprimary = CrmCustomerInformationAddressList::with('stateRef', 'cityRef', 'barangayRef')->where('account_id', $cust_id_cons)->where('is_primary', 1)->first();

            $this->idcons = $data['a'];

            $this->consigneeAddressList = $consigneeaddressesgetprimary->id;
        } else {
            // $cust_id_cons = $customerno_cons->id;
            // dd($cust_id_cons);

            $this->consigneeaddresses = [];
            $consigneeaddressesgetprimary = "";

            $this->idcons = $data['a'];

            $this->consigneeAddressList = " ";
        }





        // dd($this->consigneeaddresses[0]['id']);
    }

    public function actionchangeaddSubmit(array $data)
    {
        $rules = [
            'shipperAddressList' => 'required',
        ];

        $validated = $this->validate(
            $rules,
            [
                'shipperAddressList.required' => 'You Must Select Atleast 1.',
            ]
        );

        $addlists = CrmCustomerInformationAddressList::with('stateRef', 'cityRef', 'barangayRef')->where('id', $this->shipperAddressList)->get();

        foreach ($addlists as $p => $addlist) {
            // dd($addlist);
            $addlineadd1 = $addlist->address_line_1;
            $addlineadd2 = $addlist->address_line_2;
            $stateadd2 = $addlist->stateRef->name;
            $cityadd2 = $addlist->cityRef->name;
            $barangayadd2 = $addlist->barangayRef->name;

            $statehidden = $addlist->state_id;
            $cityhidden = $addlist->city_id;
            $barangayhidden = $addlist->city_id;
            $postalhidden = $addlist->postal_id;
        }

        $this->address_2 = $addlineadd1 . ', ' . $addlineadd2 . ', ' . $stateadd2 . ', ' . $cityadd2 . ', ' . $barangayadd2;

        $this->state2 = $statehidden;
        $this->city2 = $cityhidden;
        $this->barangay2 = $barangayhidden;
        $this->postal2 = $postalhidden;

        $this->change_address_modal = false;
        $this->resetErrorBag();
    }

    public function actionchangeaddSubmitCons(array $data)
    {
        // dd($data);
        // if (count($this->shipperAddressList) <= 1) {
        $rules = [
            'consigneeAddressList' => 'required',
        ];

        $validated = $this->validate(
            $rules,
            [
                'consigneeAddressList.required' => 'You Must Select Atleast 1.',
            ]
        );
        // }
        $consaddlists = CrmCustomerInformationAddressList::with('stateRef', 'cityRef', 'barangayRef')->where('id', $this->consigneeAddressList)->get();

        foreach ($consaddlists as $p => $consaddlist) {
            // dd($addlist);
            $consaddlineadd1 = $consaddlist->address_line_1;
            $consaddlineadd2 = $consaddlist->address_line_2;
            $consstateadd2 = $consaddlist->stateRef->name;
            $conscityadd2 = $consaddlist->cityRef->name;
            $consbarangayadd2 = $consaddlist->barangayRef->name;

            $consstatehidden = $consaddlist->state_id;
            $conscityhidden = $consaddlist->city_id;
            $consbarangayhidden = $consaddlist->city_id;
            $conspostalhidden = $consaddlist->postal_id;
            // dd($consaddlineadd1);
        }

        $this->cons[$this->idcons][$this->idcons]['address_3'] = $consaddlineadd1 . ', ' . $consaddlineadd2 . ', ' . $consstateadd2 . ', ' . $conscityadd2 . ', ' . $consbarangayadd2;
        // dd($this->cons[0][0]['address_3']);
        $this->cons[$this->idcons][$this->idcons]['state_3'] = $consstatehidden;
        $this->cons[$this->idcons][$this->idcons]['city_3'] = $conscityhidden;
        $this->cons[$this->idcons][$this->idcons]['barangay_3'] = $consbarangayhidden;
        $this->cons[$this->idcons][$this->idcons]['postal_3'] = $conspostalhidden;


        $this->change_address_cons_modal = false;
        $this->resetErrorBag();
        // $this->address_2 = $this->shipperAddressList;
        // $this->change_address_modal = false;
        // $this->resetErrorBag();
    }

    public function actioncreateaddrs(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'create_address') {
            $this->create_addrs_modal = true;
        }

        $customerinfos = CrmCustomerInformation::with('mobileNumbers')->where('account_no', $this->customer_no_2)->first();

        // dd($customerinfos->mobileNumbers);

        if ($customerinfos) {
            foreach ($customerinfos->mobileNumbers as $i => $mobileNumber) {
                if ($mobileNumber->is_primary == 1) {
                    $this->numbers = $mobileNumber->mobile;
                }
            }
        }

        $this->mobile_nos = $this->numbers;
        // dd($this->mobile_nos);
        $this->custno = $customerinfos->account_no;
        $this->custshpr = $customerinfos->fullname;
        $this->custcpny = $customerinfos->company_name;
    }

    public function actioncreateaddrscons(array $data, $action_type)
    {
        // dd($this->cons[$this->idcons][$this->idcons]['customer_no_3']);
        $this->action_type = $action_type;
        if ($action_type == 'create_address_cons') {
            $this->create_addrs_cons_modal = true;
        }

        $customerno_conss = CrmCustomerInformation::where('account_no', $this->cons[$this->idcons][$this->idcons]['customer_no_3'])->first();

        if (isset($customerno_conss)) {
            $customerinfoscons = CrmCustomerInformation::with('mobileNumbers')->where('account_no', $this->cons[$this->idcons][$this->idcons]['customer_no_3'])->first();
            // dd($customerinfoscons);
            // dd($customerinfos->mobileNumbers);

            if ($customerinfoscons) {
                foreach ($customerinfoscons->mobileNumbers as $i => $mobileNumber) {
                    if ($mobileNumber->is_primary == 1) {
                        $this->numbers = $mobileNumber->mobile;
                    }
                }
            }

            $this->mobile_nos_cons = $this->numbers;
            // dd($this->mobile_nos);
            $this->custnocons = $customerinfoscons->account_no;
            $this->custshprcons = $customerinfoscons->fullname;
            $this->custcpnycons = $customerinfoscons->company_name;
        } else {
            $this->numbers = "";

            $this->mobile_nos_cons = "";
            // dd($this->mobile_nos);
            $this->custnocons = "";
            $this->custshprcons = "";
            $this->custcpnycons = $this->cons[$this->idcons][$this->idcons]['company_name_3'];
        }
    }

    public function actionCreateaddSubmit(array $data)
    {

        $rules = [
            'addresslabel' => 'required',
            'add1' => 'required',
            'add2' => 'sometimes',
            'state' => 'required',
            'city' => 'required',
            'barangay' => 'required',
            'postal' => 'required',
        ];

        $validated = $this->validate(
            $rules,
            [
                'addresslabel.required' => 'Address Label is Required.',
                'add1.required' => 'Address Line 1 is Required.',
                // 'add2.required' => 'Address Line 2 is Required.',
                'state.required' => 'State/Province is Required.',
                'city.required' => 'City/Municipality is Required.',
                'barangay.required' => 'Barangay is Required.',
                'postal.required' => 'Postal Code is Required.',
            ]
        );

        $getcustomerids = CrmCustomerInformation::where('account_no', $this->customer_no_2)->get();
        foreach ($getcustomerids as $x => $getcustomerid) {
            // dd($getcustomerid);
            $this->custid2 = $getcustomerid->id;
            $this->acctype2 = $getcustomerid->account_type;
        }

        // $addcustaddress = CrmCustomerInformationAddressList::create([
        //    'account_type' => $this->acctype2,
        //    'account_id' => $this->custid2,
        //    'address_line_1' => $validated['add1'],
        //    'address_line_2' => $validated['add2'],
        //    'address_type' => 1,
        //    'address_label' => $validated['addresslabel'],
        //    'state_id' => $validated['state'],
        //    'city_id' => $validated['city'],
        //    'barangay_id' => $validated['barangay'],
        //    'postal_id' => $this->postal,
        //    'is_primary' => 0,
        // ]);

        $this->statenames = StateReference::where('id', $this->state)->get();

        foreach ($this->statenames as $a => $statename) {
            $this->state_name = $this->statenames[$a]['name'];
            $stateid = $this->statenames[$a]['id'];
        }

        $this->citynames = CityReference::where('id', $this->city)->get();

        foreach ($this->citynames as $a => $cityname) {
            $this->city_name = $this->citynames[$a]['name'];
            $cityid = $this->citynames[$a]['id'];
        }

        $this->barangaynames = BarangayReference::where('id', $this->barangay)->get();

        foreach ($this->barangaynames as $a => $barangayname) {
            $this->barangay_name = $this->barangaynames[$a]['name'];
            $barangayid = $this->barangaynames[$a]['id'];
        }


        $this->address_2 = $this->add1 . ', ' . $this->add2 . ', ' . $this->state_name . ', ' . $this->city_name . ', ' . $this->barangay_name;

        $this->state2 = $stateid;
        $this->city2 = $cityid;
        $this->barangay2 = $barangayid;
        $this->postal2 = $this->postal;

        $this->create_addrs_modal = false;
        $this->change_address_modal = false;

        $this->reset([
            'addresslabel',
            'add1',
            'add2',
            'state',
            'city',
            'barangay',
            'postal',
        ]);
        $this->resetErrorBag();
    }

    public function actionCreateaddconsSubmit(array $data)
    {
        // dd($this->cons[$this->idcons][$this->idcons]['address_3']. 'sad');

        $rules = [
            'addresslabel' => 'required',
            'add1' => 'required',
            'add2' => 'sometimes',
            'state' => 'required',
            'city' => 'required',
            'barangay' => 'required',
            'postal' => 'required',

        ];

        $validated = $this->validate(
            $rules,
            [
                'addresslabel.required' => 'Address Label is Required.',
                'add1.required' => 'Address Line 1 is Required.',
                // 'add2.required' => 'Address Line 2 is Required.',
                'state.required' => 'State/Province is Required.',
                'city.required' => 'City/Municipality is Required.',
                'barangay.required' => 'Barangay is Required.',
                'postal.required' => 'Postal Code is Required.',
            ]
        );
        // dd($this->cons[$this->idcons][$this->idcons]['customer_no_3']);


        $getcustomerconsids = CrmCustomerInformation::where('account_no', $this->cons[$this->idcons][$this->idcons]['customer_no_3'])->get();
        // dd($getcustomerconsids);
        foreach ($getcustomerconsids as $x => $getcustomerid) {
            // dd($getcustomerid);
            $this->custid2 = $getcustomerid->id;
            $this->acctype2 = $getcustomerid->account_type;
        }

        // $addcustaddress = CrmCustomerInformationAddressList::create([
        //     'account_type' => $this->acctype2,
        //     'account_id' => $this->custid2,
        //     'address_line_1' => $validated['add1'],
        //     'address_line_2' => $validated['add2'],
        //     'address_type' => 1,
        //     'address_label' => $validated['addresslabel'],
        //     'state_id' => $validated['state'],
        //     'city_id' => $validated['city'],
        //     'barangay_id' => $validated['barangay'],
        //     'postal_id' => $this->postal,
        //     'is_primary' => 0,
        // ]);

        $this->statenames = StateReference::where('id', $this->state)->get();

        foreach ($this->statenames as $a => $statename) {
            $this->state_name = $this->statenames[$a]['name'];
            $stateid = $this->statenames[$a]['id'];
        }

        $this->citynames = CityReference::where('id', $this->city)->get();

        foreach ($this->citynames as $a => $cityname) {
            $this->city_name = $this->citynames[$a]['name'];
            $cityid = $this->citynames[$a]['id'];
        }

        $this->barangaynames = BarangayReference::where('id', $this->barangay)->get();

        foreach ($this->barangaynames as $a => $barangayname) {
            $this->barangay_name = $this->barangaynames[$a]['name'];
            $barangayid = $this->barangaynames[$a]['id'];
        }

        $this->cons[$this->idcons][$this->idcons]['address_3'] = $this->add1 . ', ' . $this->add2 . ', ' . $this->state_name . ', ' . $this->city_name . ', ' . $this->barangay_name;
        $this->cons[$this->idcons][$this->idcons]['state_3'] = $stateid;
        $this->cons[$this->idcons][$this->idcons]['city_3'] = $cityid;
        $this->cons[$this->idcons][$this->idcons]['barangay_3'] = $barangayid;
        $this->cons[$this->idcons][$this->idcons]['postal_3'] = $this->postal;

        $this->create_addrs_cons_modal = false;
        $this->change_address_cons_modal = false;

        $this->reset([
            'addresslabel',
            'add1',
            'add2',
            'state',
            'city',
            'barangay',
            'postal',
        ]);
        $this->resetErrorBag();
    }

    public function actionba(array $data, $action_type)
    {
        // dd($this->cons[$data['a']][$data['a']]['customer_no_3']);
        // $validated['booking_reference_no_2'];

        $this->action_type = $action_type;
        if ($action_type == 'book_again') {
            // $this->resetForm();
            // dd("asd");
            // $this->emitTo('crm.sales.booking-mgmt.create','');
            // $this->emitTo('crm.sales.booking-mgmt.index', 'create');
            // $this->emitTo('crm.sales.booking-mgmt.index', 'close_modal', 'create');
            // $this->confirmsub_modal = false;
            // $this->emitTo('crm.sales.booking-mgmt.booking-history.index2', 'close_modal', 'create');
            // $this->emitTo('crm.sales.booking-mgmt.index', 'index');
            // $this->emitTo('crm.sales.booking-mgmt.booking-history.index2', 'index2');

            // $this->emitTo('crm.sales.booking-mgmt.index', 'open_create');


            // $this->current_tab = 2;
            // $this->mount();
            // return redirect()->route('crm.sales.booking-mgmt.create');
            $this->confirmsub_modal = false;
            $this->resetForm();
            $this->current_tab = 1;
            $this->mount();
        }
    }

    public function actionc(array $data, $action_type)
    {
        // dd("asd");
        $this->action_type = $action_type;
        if ($action_type == 'ba_close') {
            // $this->resetForm();
            // $this->emitTo('crm.sales.booking-mgmt.index', 'close_modal', 'create');
            // $this->emitTo('crm.sales.booking-mgmt.booking-history.index2', 'close_modal', 'create');
            // $this->emitTo('crm.sales.booking-mgmt.index', 'index');
            // $this->emitTo('crm.sales.booking-mgmt.booking-history.index2', 'index2');
            // $this->current_tab = 1;
            // $this->mount();
        }
    }




    public function closeModal($action_type)
    {
        if ($action_type == 'view_cargo_det') {
            $this->cargo_details_modal = false;
        } elseif ($action_type == 'change_addrs') {
            $this->change_address_modal = false;
        } elseif ($action_type == 'change_addrs_cons') {
            $this->change_address_cons_modal = false;
        } elseif ($action_type == 'create_address') {
            $this->create_addrs_modal = false;
        } elseif ($action_type == 'create_address_cons') {
            $this->create_addrs_cons_modal = false;
        } elseif ($action_type == 'c_details') {
            $this->consigneedetails_modal = false;
        }  elseif ($action_type == 'book_again') {
            // $this->confirmsub_modal = false;
        }
    }

    public function actionviewcdet(array $data, $action_type)
    {
        $viewbookingconsigneesdetails = CrmBookingConsignee::with('BookingCargoDetailsHasManyCD', 'TransportReferenceCD', 'ServiceModeReferenceCD', 'ModeOfPaymenReferenceCD')
            ->where('id', $data['id'])->get();

        $this->viewbookingconsigneesdetail = [];
        $this->viewbookingconsignees = [];

        foreach ($viewbookingconsigneesdetails as $i => $viewbookingconsigneesdetailss) {
            $this->viewbookingconsigneesdetail = $viewbookingconsigneesdetailss->BookingCargoDetailsHasManyCD;
            $this->viewbookingconsignees = $viewbookingconsigneesdetailss;
        }
        $this->CDdecval = $this->viewbookingconsignees->declared_value;
        $this->CDtransportmode = $this->viewbookingconsignees->TransportReferenceCD->name;
        $this->CDservmode = $this->viewbookingconsignees->ServiceModeReferenceCD->name;
        $this->CDdescgoods = $this->viewbookingconsignees->description_goods;
        $this->CDmodeofp = $this->viewbookingconsignees->ModeOfPaymenReferenceCD->name;

        $viewconsdetails = CrmBooking::with('BookingShipper', 'ShipperReferenceBK', 'ActivityReferenceBK', 'TimeslotReferenceBK', 'FinalStatusReferenceBK', 'BookingConsigneeHasManyBK')
            ->where('booking_reference_no', $this->booking_ref_no)->first();

        $this->CDwcount = $viewconsdetails->consignee_count;

        $this->CDworkins = $viewconsdetails->work_instruction;
        $this->CDintrem = $viewconsdetails->remarks;

        foreach ($this->viewbookingdetailsconsignee[0] as $i => $viewcons) {
        }
        $this->action_type = $action_type;
        if ($action_type == 'c_details') {
            $this->consigneedetails_modal = true;
        }
    }

    public function actionSubmitNewBooking(array $data)
    {
        $this->current_tab = 3;
        $this->confirmshipper_modal = false;
    }

    public function actionViewExisting(array $data)
    {
        $viewbookingdetails = CrmBooking::with('BookingShipper', 'ShipperReferenceBK', 'ActivityReferenceBK', 'TimeslotReferenceBK', 'FinalStatusReferenceBK', 'BookingConsigneeHasManyBK')
            ->where('booking_reference_no', $this->booking_ref_no)->first();
        // dd($viewbookingdetails);

        $shipdetails = CrmBookingShipper::with('AccountTypeReferenceSP')->where('customer_no', $this->customer_no_2)->first();

        $this->pickupwalk = $viewbookingdetails->booking_category;
        $this->singlemulti = $viewbookingdetails->consignee_category;

        $this->SDbookingrefno = $viewbookingdetails->booking_reference_no;
        $this->SDbookingstat = $viewbookingdetails->FinalStatusReferenceBK->name;
        $this->SDfullname = $viewbookingdetails->BookingShipper->name;
        $this->SDfname = $viewbookingdetails->BookingShipper->name;

        $this->SDcompany = $viewbookingdetails->BookingShipper->company_name;
        $this->SDmobile_nos = $viewbookingdetails->BookingShipper->mobile_number;
        $this->SDemail_address = $viewbookingdetails->BookingShipper->email_address;
        $this->SDaddress = $viewbookingdetails->BookingShipper->address;

        $this->SDcustomertype = $shipdetails->AccountTypeReferenceSP->name;
        $this->SDacttype = $viewbookingdetails->ActivityReferenceBK->activity_type;

        $this->SDtimeslot = $viewbookingdetails->TimeslotReferenceBK->name;

        $viewbookingdetailsconsignees = CrmBooking::with('ShipperReferenceBK', 'ActivityReferenceBK', 'TimeslotReferenceBK', 'FinalStatusReferenceBK', 'BookingConsigneeHasManyBK')
            ->where('booking_reference_no', $this->booking_ref_no)->get();

        $this->viewbookingdetailsconsignee = [];
        foreach ($viewbookingdetailsconsignees as $i => $viewbookingdetailsconsigneess) {
            $this->viewbookingdetailsconsignee[0] = $viewbookingdetailsconsigneess->BookingConsigneeHasManyBK;
        }
        foreach ($this->viewbookingdetailsconsignee[0] as $i => $viewcons2) {
            $this->consid = $this->viewbookingdetailsconsignee[0][$i]['id'];
        }
        $this->viewExisting_modal = true;
    }
    // actionBookingStatLogs
    // bookstatlog_modal
    public function actionBookingStatLogs(array $data, $action_type)
    {
        $viewbookingstatuslogs = CrmBookingLogs::with('StatusReferenceLG')
            ->where('booking_reference_no', $this->booking_ref_no)->first();

        // dd($viewbookingstatuslogs);
        $this->bstatrefno = $viewbookingstatuslogs->booking_reference_no;
        $this->bstatstat = $viewbookingstatuslogs->StatusReferenceLG->name;
        $this->bstatcreated = $viewbookingstatuslogs->created_at;

        $this->bookstatlog_modal = true;
    }

    public function single_consignee_1()
    {
        $this->consignee_category_1 = 1;
    }
    public function multiple_consignee_1()
    {
        $this->consignee_category_1 = 2;
    }
    public function pickup_category_1()
    {
        $this->booking_category_1 = 1;
    }
    public function walkin_category_1()
    {
        $this->booking_category_1 = 2;
    }

    public function individual_type_2()
    {
        $this->account_type_2 = 1;
    }
    public function Corporate_type_2()
    {
        $this->account_type_2 = 2;
    }
    public function individual_type_3(array $data)
    {
        $this->cons[$data['a']][$data['a']]['account_type_3'] = 1;
    }
    public function Corporate_type_3(array $data)
    {
        $this->cons[$data['a']][$data['a']]['account_type_3'] = 2;
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.sales.booking-mgmt.index', 'close_modal', 'create');
        $this->emitTo('crm.sales.booking-mgmt.booking-history.index2', 'close_modal', 'create');
        $this->confirmation_modal = false;
        $this->current_tab = 1;
        $this->mount();
        // $this->create_modal = false;
    }


    public function action(BookingMgmtInterface $booking_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'create_next') {
            $response = $booking_interface->createValidationTab1($this->getRequest());
            // dd($response);
            if ($response['code'] == 200) {
                $this->current_tab = 2;
                $this->resetErrorBag();
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($action_type == 'create_next_2') {
            $response = $booking_interface->createValidationTab2($this->getRequest());
            $customerinfosconfirm = CrmBooking::where('shipper_customer_no', $this->customer_no_2)->where('pickup_date', $this->pickup_date_1)->first();
            // dd($customerinfosconfirm);
            if ($customerinfosconfirm) {
                $this->booking_ref_no = $customerinfosconfirm->booking_reference_no;
            }
            // dd($this->booking_ref_no, $customerinfosconfirm, $this->customer_no_2, $this->pickup_date_1);
            if ($response['code'] == 200) {
                if ($customerinfosconfirm) {
                    if ($this->customer_no_2 == $customerinfosconfirm->shipper_customer_no && $customerinfosconfirm->pickup_date == $this->pickup_date_1) {
                        $this->confirmshipper_modal = true;
                    } else {
                        $this->current_tab = 3;
                        $this->resetErrorBag();
                    }
                } else {
                    $this->current_tab = 3;
                    $this->resetErrorBag();
                }
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($action_type == 'create_next_3') {
            $response = $booking_interface->createValidationTab3($this->getRequest());
            // dd($response);
            if ($response['code'] == 200) {
                $this->current_tab = 4;
                $this->resetErrorBag();
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($action_type == 'create_next_4') {
            $response = $booking_interface->createValidationTab4($this->getRequest());
            // dd($response);
            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
                $this->current_tab = 4;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function submit(BookingMgmtInterface $booking_interface)
    {
        $response = $booking_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = false;
            $this->confirmsub_modal = true;


            // $this->resetForm();
            // $this->emitTo('crm.sales.booking-mgmt.index', 'close_modal', 'create');
            // $this->emitTo('crm.sales.booking-mgmt.booking-history.index2', 'close_modal', 'create');
            // $this->emitTo('crm.sales.booking-mgmt.index', 'index');
            // $this->emitTo('crm.sales.booking-mgmt.booking-history.index2', 'index2');
            // $this->sweetAlert('', $response['message'], $response['result']);
            // $this->current_tab = 1;
            // $this->mount();

        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        $this->shipperAddressList;


        if (!$this->cons) {
            $this->cons[][] = [
                'id' => null,
                'account_type_3' => 1,
                'customer_no_3' => null,
                'company_name_3' => null,
                'first_name_3' => null,
                'middle_name_3' => null,
                'last_name_3' => null,
                'mobile_number_3' => null,
                'email_address_3' => null,
                'address_3' => null,
                'state_3' => null,
                'city_3' => null,
                'barangay_3' => null,
                'postal_3' => null,

                'stype_3' => null,
                'declared_value_3' => null,
                'description_goods_3' => null,
                'transposrt_mode_3' => null,
                'service_mode_3' => null,
                'mode_of_payment_3' => null,
                'servmode_identify' => false,
                'charge_to_3' => null,
                'charge_to_name_3' => null,
                'is_deleted' => false,
            ];
        }

        if (!$this->cardets) {
            $this->cardets[][] = [
                'id' => null,
                'quantity_3' => null,
                'weight_3' => null,
                'length_3' => null,
                'width_3' => null,
                'height_3' => null,
                'size_3' => null,
                'is_deleted' => false,
            ];
        }

        if (!$this->attachments) {
            $this->attachments[] = [
                'id' => null,
                'attachment' => null,
                'path' => null,
                'name' => null,
                'extension' => null,
                'is_deleted' => false,
            ];
        }
        return view('livewire.crm.sales.booking-mgmt.create');
    }
}
