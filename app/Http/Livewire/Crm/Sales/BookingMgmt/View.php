<?php

namespace App\Http\Livewire\Crm\Sales\BookingMgmt;

use App\Traits\Crm\Sales\BookingMgmt\BookingMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class View extends Component
{
    use BookingMgmtTrait, PopUpMessagesTrait;


    
    public function render()
    {
        return view('livewire.crm.sales.booking-mgmt.view');
    }
}
