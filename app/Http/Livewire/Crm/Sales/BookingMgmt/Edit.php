<?php

namespace App\Http\Livewire\Crm\Sales\BookingMgmt;

use App\Interfaces\Crm\Sales\BookingManagement\BookingMgmt\BookingMgmtInterface;
use App\Models\Crm\BarangayReference;
use App\Models\Crm\CityReference;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmCustomerInformationAddressList;
use App\Models\Crm\StateReference;
use App\Models\CrmBooking;
use App\Models\CrmBookingCargoDetails;
use App\Models\CrmBookingConsignee;
use App\Models\CrmModeOfPaymentReference;
use App\Models\CrmTransportMode;
use App\Traits\Crm\Sales\BookingMgmt\BookingMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use BookingMgmtTrait, PopUpMessagesTrait, WithFileUploads;

    public $booking_s;
    public $consignees = [];
    public $consignee;
    public $cargo_details = [];
    public $cargo_detail;
    public $booking_id;
    public $book_id;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->booking_s = CrmBooking::with('BookingShipper', 'ShipperReferenceBK', 'attachments', 'BookingRemarksHasManyBK', 'BookingLogsHasManyBK', 'CreatedByBK', 'BookingConsigneeHasManyBK')->findOrFail($id);
        
        $this->book_id = $id;

         /////////////////////////////////////////-----------ATTACHMENT----//////////////////////////////////////////////  
        foreach ($this->booking_s->attachments as $attachment) {
            $this->attachments[] = [
                'id' => $attachment->id,
                'attachment' => '',
                'path' => $attachment->path,
                'name' => $attachment->name,
                'extension' => $attachment->extension,
                'is_deleted' => false,
            ];
        }
        // dd($this->booking_s);
        /////////////////////////////////////////-----------CURRENT PAGE 1----//////////////////////////////////////////////   
        $this->consignee_category_1 = $this->booking_s->consignee_category;
        $this->booking_category_1 = $this->booking_s->booking_category;

        $this->booking_type_1 = $this->booking_s->booking_type_id;
        $this->vehicle_type_1 = $this->booking_s->vehicle_type_id;
        $this->pickup_date_1 = $this->booking_s->pickup_date;
        $this->time_slot_1 = $this->booking_s->time_slot_id;
        $this->time_from_1 = $this->booking_s->time_slot_from;
        $this->time_to_1 = $this->booking_s->time_slot_to;
        $this->consignee_count_1 = $this->booking_s->consignee_count;
        $this->walk_in_branch_1 = $this->booking_s->walk_in_branch_id;
        $this->activity_type_1 = $this->booking_s->activity_type;
        // $this->channel_1 = $this->booking_s->marketing_channel_id;
        if($this->time_from_1 == "13:00:00"){
            $this->time_1 = 1;
        }else if($this->time_from_1 == "15:00:00"){
            $this->time_1 = 2;
        }else if($this->time_from_1 == "17:00:00"){
            $this->time_1 = 3;
        }else if($this->time_from_1 == "19:00:00"){
            $this->time_1 = 4;
        }else if($this->time_from_1 == "21:00:00"){
            $this->time_1 = 5;
        }

        /////////////////////////////////////////-----------CURRENT PAGE 2---------------/////////////////////////////////////
        $this->account_type_2 = $this->booking_s->BookingShipper->account_type_id;

        $this->booking_reference_no_2 = $this->booking_s->booking_reference_no;
        $this->customer_no_2 = $this->booking_s->BookingShipper->customer_no;
        $this->company_name_2 = $this->booking_s->BookingShipper->company_name;
        $this->first_name_2 = $this->booking_s->BookingShipper->first_name;
        $this->middle_name_2 = $this->booking_s->BookingShipper->middle_name;
        $this->last_name_2 = $this->booking_s->BookingShipper->last_name;
        $this->mobile_number_2 = $this->booking_s->BookingShipper->mobile_number;
        $this->email_address_2 = $this->booking_s->BookingShipper->email_address;
        $this->address_2 = $this->booking_s->BookingShipper->address;
        $this->state2 = $this->booking_s->BookingShipper->state_id;
        $this->city2 = $this->booking_s->BookingShipper->city_id;
        $this->barangay2 = $this->booking_s->BookingShipper->barangay_id;
        $this->postal2 = $this->booking_s->BookingShipper->postal_id;


        /////////////////////////////////////////-----------CURRENT PAGE 3---------------///////////////////////////////////// 

        $this->consignees = CrmBookingConsignee::where('booking_id', $id)->get();
        // dd($this->cargo_details);
        // dd($this->consignees);

        //  dd($this->consignees->customer_no);

        // $this->account_type_3 = $this->consignees->
        $this->booking_id = $id;

        foreach ($this->consignees as $a => $consignee) {
            $this->cargo_details = CrmBookingCargoDetails::where('consignee_id', $consignee->id)->get();


            $this->cons[$a][$a] = [
                'id' => $consignee->id,
                'account_type_3' => $consignee->account_type_id,
                'customer_no_3' => $consignee->customer_no,
                'company_name_3' => $consignee->company_name,
                'first_name_3' => $consignee->first_name,
                'middle_name_3' => $consignee->middle_name,
                'last_name_3' => $consignee->last_name,
                'mobile_number_3' => $consignee->mobile_number,
                'email_address_3' => $consignee->email_address,
                'address_3' => $consignee->address,
                'state_3' => $consignee->state_id,
                'city_3' => $consignee->city_id,
                'barangay_3' => $consignee->barangay_id,
                'postal_3' => $consignee->postal_id,
                'stype_3' => $consignee->services_type_id,
                'declared_value_3' => $consignee->declared_value,
                'description_goods_3' => $consignee->description_goods,
                'transposrt_mode_3' => $consignee->transposrt_mode_id,
                'service_mode_3' => $consignee->service_mode_id,
                'mode_of_payment_3' => $consignee->mode_of_payment_id,
                'servmode_identify' => $consignee->transposrt_mode_id,
                'charge_to_3' => $consignee->charge_to_id,
                'charge_to_name_3' => $consignee->charge_to_name,
                'created_by' => null,
                'is_deleted' => false,
            ];




            $b = 0;
            foreach ($this->cargo_details as $c => $cargo_detail) {
                $this->cardets[$a][$c] = [
                    'id' => $cargo_detail->id,
                    'quantity_3' => $cargo_detail->quantity,
                    'weight_3' => $cargo_detail->weight,
                    'length_3' => $cargo_detail->length,
                    'width_3' => $cargo_detail->width,
                    'height_3' => $cargo_detail->height,
                    'size_3' => $cargo_detail->size,
                    'is_deleted' => false,
                ];

                $b++;
            }

        }


        // dd($this->pros);

        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->cons as $a => $con) {

            if ($this->cons[$a][$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->cons[$a][$a]['is_deleted'] == false ? 1 : 0);
        }
         $this->countarr = $countarray;
         $this->min = min($getkeyarray);
         $this->nearest = max($getkeyarray);

        //  dd($this->booking_s->BookingConsigneeHasManyBK->account_type_id);  
        //    $this->account_type_3 = $this->booking_s->BookingConsigneeHasManyBK->account_type_id;

        $this->work_instruction_3 = $this->booking_s->work_instruction;
        $this->remarks_3 = $this->booking_s->remarks;


        //  'work_instruction_3' => 'sometimes',
        //  'remarks_3' => 'sometimes',
        //  'attachments' => 'required',
        //  'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),


        /////////////////////////////////////////-----------CURRENT PAGE 4---------------/////////////////////////////////////   



    }

    public function actionview(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'view_cargo_det') {
            $this->cargo_details_modal = true;
            // dd($this->cons[$data['a']][$data['a']]);

            foreach ($this->cons[$data['a']][$data['a']] as $condet) {
                // dd($this->cons[1][1]['declared_value_3']);
                $transportmodename = CrmTransportMode::find($this->cons[$data['a']][$data['a']]['transposrt_mode_3']);
                $servicemodename = $this->cons[$data['a']][$data['a']]['service_mode_3'];
                $modeofpayment = CrmModeOfPaymentReference::find($this->cons[$data['a']][$data['a']]['mode_of_payment_3']);
                // dd($servicemodename);

                $this->decval_summary = $this->cons[$data['a']][$data['a']]['declared_value_3'];
                $this->transposrt_mode_3_summary = $transportmodename->name;
                $this->service_mode_3_summary = $servicemodename;
                $this->mode_of_payment_3_summary = $modeofpayment->name;
                $this->description_goods_3_summary = $this->cons[$data['a']][$data['a']]['description_goods_3'];
                // dd($this->service_mode_3_summary);

            }

            $this->asd = $data['a'];
            // dd($this->cardets[$data['a']]);
            $this->cardetsss = $this->cardets[$data['a']];
        }
    }

    public function actionviewcdet(array $data, $action_type)
    {
        $viewbookingconsigneesdetails = CrmBookingConsignee::with('BookingCargoDetailsHasManyCD', 'TransportReferenceCD', 'ServiceModeReferenceCD', 'ModeOfPaymenReferenceCD')
            ->where('id', $data['id'])->get();

        $this->viewbookingconsigneesdetail = [];
        $this->viewbookingconsignees = [];

        foreach ($viewbookingconsigneesdetails as $i => $viewbookingconsigneesdetailss) {
            $this->viewbookingconsigneesdetail = $viewbookingconsigneesdetailss->BookingCargoDetailsHasManyCD;
            $this->viewbookingconsignees = $viewbookingconsigneesdetailss;
        }
        $this->CDdecval = $this->viewbookingconsignees->declared_value;
        $this->CDtransportmode = $this->viewbookingconsignees->TransportReferenceCD->name;
        $this->CDservmode = $this->viewbookingconsignees->ServiceModeReferenceCD->name;
        $this->CDdescgoods = $this->viewbookingconsignees->description_goods;
        $this->CDmodeofp = $this->viewbookingconsignees->ModeOfPaymenReferenceCD->name;

        $viewconsdetails = CrmBooking::with('BookingShipper', 'ShipperReferenceBK', 'ActivityReferenceBK', 'TimeslotReferenceBK', 'FinalStatusReferenceBK', 'BookingConsigneeHasManyBK')
            ->where('booking_reference_no', $this->booking_ref_no)->first();

        $this->CDwcount = $viewconsdetails->consignee_count;

        $this->CDworkins = $viewconsdetails->work_instruction;
        $this->CDintrem = $viewconsdetails->remarks;

        foreach ($this->viewbookingdetailsconsignee[0] as $i => $viewcons) {
        }
        $this->action_type = $action_type;
        if ($action_type == 'c_details') {
            $this->consigneedetails_modal = true;
        }
    }

    public function single_consignee_1()
    {
        $this->consignee_category_1 = 1;
    }
    public function multiple_consignee_1()
    {
        $this->consignee_category_1 = 2;
    }
    public function pickup_category_1()
    {
        $this->booking_category_1 = 1;
    }
    public function walkin_category_1()
    {
        $this->booking_category_1 = 2;
    }
    public function individual_type_2()
    {
        $this->account_type_2 = 1;
    }
    public function Corporate_type_2()
    {
        $this->account_type_2 = 2;
    }
    public function individual_type_3(array $data)
    {
        $this->cons[$data['a']][$data['a']]['account_type_3'] = 1;
    }
    public function Corporate_type_3(array $data)
    {
        $this->cons[$data['a']][$data['a']]['account_type_3'] = 2;
    }

    public function actionchangeadd(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'change_addrs') {
            $this->change_address_modal = true;
        }

        $customerno = CrmCustomerInformation::where('account_no', $this->customer_no_2)->first();

        $cust_id = $customerno->id;

        // $this->shipperaddresses = CrmCustomerInformationAddressList::where('account_id', $cust_id)->get();
        $this->shipperaddresses = CrmCustomerInformationAddressList::with('stateRef','cityRef','barangayRef')->where('account_id', $cust_id)->get();
        $shipperaddressesgetprimary = CrmCustomerInformationAddressList::with('stateRef', 'cityRef', 'barangayRef')->where('account_id', $cust_id)->where('is_primary', 1)->first();

        $this->shipperAddressList = $shipperaddressesgetprimary->id;
    }

    public function actionchangeaddcons(array $data, $action_type)
    {
        // dd($this->cons[$data['a']][$data['a']]['customer_no_3']);
        $this->action_type = $action_type;
        if ($action_type == 'change_addrs_cons') {
            $this->change_address_cons_modal = true;
        }

        $customerno_cons = CrmCustomerInformation::where('account_no', $this->cons[$data['a']][$data['a']]['customer_no_3'])->first();
        // dd($customerno_cons);



        $cust_id_cons = $customerno_cons->id;
        // dd($cust_id_cons);

        $this->consigneeaddresses = CrmCustomerInformationAddressList::with('stateRef', 'cityRef', 'barangayRef')->where('account_id', $cust_id_cons)->get();
        $consigneeaddressesgetprimary = CrmCustomerInformationAddressList::with('stateRef', 'cityRef', 'barangayRef')->where('account_id', $cust_id_cons)->where('is_primary', 1)->first();

        $this->idcons = $data['a'];

        $this->consigneeAddressList = $consigneeaddressesgetprimary->id;


        // dd($this->consigneeaddresses[0]['id']);
    }

    // public function actionchangeaddSubmit(array $data)
    // {
    //     // if (count($this->shipperAddressList) <= 1) {
    //     $rules = [
    //         'shipperAddressList' => 'required',
    //     ];

    //     $validated = $this->validate(
    //         $rules,
    //         [
    //             'shipperAddressList.required' => 'You Must Select Atleast 1.',
    //         ]
    //     );
    //     // }
    //     $this->address_2 = $this->shipperAddressList;
    //     $this->change_address_modal = false;
    //     $this->resetErrorBag();
    // }
    public function actionchangeaddSubmit(array $data)
    {
        $rules = [
            'shipperAddressList' => 'required',
        ];

        $validated = $this->validate(
            $rules,
            [
                'shipperAddressList.required' => 'You Must Select Atleast 1.',
            ]
        );

        $addlists = CrmCustomerInformationAddressList::with('stateRef','cityRef','barangayRef')->where('id', $this->shipperAddressList)->get();

        foreach($addlists as $p => $addlist){
        // dd($addlist);
            $addlineadd1 = $addlist->address_line_1;
            $addlineadd2 = $addlist->address_line_2;
            $stateadd2 = $addlist->stateRef->name;
            $cityadd2 = $addlist->cityRef->name;
            $barangayadd2 = $addlist->barangayRef->name;

            $statehidden = $addlist->state_id;
            $cityhidden = $addlist->city_id;
            $barangayhidden = $addlist->city_id;
            $postalhidden = $addlist->postal_id;
        }

        $this->address_2 = $addlineadd1.' '.$addlineadd2.' '.$stateadd2.', '.$cityadd2.', '.$barangayadd2;

        $this->state2 = $statehidden;
        $this->city2 = $cityhidden;
        $this->barangay2 = $barangayhidden;
        $this->postal2 = $postalhidden;

        $this->change_address_modal = false;
        $this->resetErrorBag();
    }

    public function actionchangeaddSubmitCons(array $data)
    {
        // dd($data);
        // if (count($this->shipperAddressList) <= 1) {
        $rules = [
            'consigneeAddressList' => 'required',
        ];

        $validated = $this->validate(
            $rules,
            [
                'consigneeAddressList.required' => 'You Must Select Atleast 1.',
            ] 
        );
        // }
        $consaddlists = CrmCustomerInformationAddressList::with('stateRef', 'cityRef', 'barangayRef')->where('id', $this->consigneeAddressList)->get();

        foreach ($consaddlists as $p => $consaddlist) {
            // dd($addlist);
            $consaddlineadd1 = $consaddlist->address_line_1;
            $consaddlineadd2 = $consaddlist->address_line_2;
            $consstateadd2 = $consaddlist->stateRef->name;
            $conscityadd2 = $consaddlist->cityRef->name;
            $consbarangayadd2 = $consaddlist->barangayRef->name;

            $consstatehidden = $consaddlist->state_id;
            $conscityhidden = $consaddlist->city_id;
            $consbarangayhidden = $consaddlist->city_id;
            $conspostalhidden = $consaddlist->postal_id;
            // dd($consaddlineadd1);
        }

        $this->cons[$this->idcons][$this->idcons]['address_3'] = $consaddlineadd1 . ' ' . $consaddlineadd2 . ' ' . $consstateadd2 . ', ' . $conscityadd2 . ', ' . $consbarangayadd2;
        // dd($this->cons[0][0]['address_3']);
        $this->cons[$this->idcons][$this->idcons]['state_3'] = $consstatehidden;
        $this->cons[$this->idcons][$this->idcons]['city_3'] = $conscityhidden;
        $this->cons[$this->idcons][$this->idcons]['barangay_3'] = $consbarangayhidden;
        $this->cons[$this->idcons][$this->idcons]['postal_3'] = $conspostalhidden;


        $this->change_address_cons_modal = false;
        $this->resetErrorBag();
        // $this->address_2 = $this->shipperAddressList;
        // $this->change_address_modal = false;
        // $this->resetErrorBag();
    }

    public function actioncreateaddrs(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'create_address') {
            $this->create_addrs_modal = true;
        }

        $customerinfos = CrmCustomerInformation::with('mobileNumbers')->where('account_no', $this->customer_no_2)->first();

        // dd($customerinfos->mobileNumbers);

        if ($customerinfos) {
            foreach ($customerinfos->mobileNumbers as $i => $mobileNumber) {
                if ($mobileNumber->is_primary == 1) {
                    $this->numbers = $mobileNumber->mobile;
                }
            }
        }

        $this->mobile_nos = $this->numbers;
        // dd($this->mobile_nos);
        $this->custno = $customerinfos->account_no;
        $this->custshpr = $customerinfos->fullname;
        $this->custcpny = $customerinfos->company_name;
    }

    public function actioncreateaddrscons(array $data, $action_type)
    {
        // dd($this->cons[$this->idcons][$this->idcons]['customer_no_3']);
        $this->action_type = $action_type;
        if ($action_type == 'create_address_cons') {
            $this->create_addrs_cons_modal = true;
        }

        $customerinfoscons = CrmCustomerInformation::with('mobileNumbers')->where('account_no', $this->cons[$this->idcons][$this->idcons]['customer_no_3'])->first();
        // dd($customerinfoscons);
        // dd($customerinfos->mobileNumbers);

        if ($customerinfoscons) {
            foreach ($customerinfoscons->mobileNumbers as $i => $mobileNumber) {
                if ($mobileNumber->is_primary == 1) {
                    $this->numbers = $mobileNumber->mobile;
                }
            }
        }

        $this->mobile_nos_cons = $this->numbers;
        // dd($this->mobile_nos);
        $this->custnocons = $customerinfoscons->account_no;
        $this->custshprcons = $customerinfoscons->fullname;
        $this->custcpnycons = $customerinfoscons->company_name;
    }

    // public function actionCreateaddSubmit(array $data)
    // {
    //     $rules = [
    //         'add1' => 'required',
    //     ];

    //     $validated = $this->validate(
    //         $rules,
    //         [
    //             'add1.required' => 'Address Line 1 is Required.',
    //         ]
    //     );
    //     $this->address_2 = $this->add1;
    //     $this->create_addrs_modal = false;
    //     $this->change_address_modal = false;
    //     $this->resetErrorBag();
    // }

    public function actionCreateaddSubmit(array $data)
    {
        
        $rules = [
            'addresslabel' => 'required',
            'add1' => 'required',
            'add2' => 'sometimes',
            'state' => 'required',
            'city' => 'required',
            'barangay' => 'required',
            'postal' => 'required',
            
        ];

        $validated = $this->validate(
            $rules,
            [
                'addresslabel.required' => 'Address Label is Required.',
                'add1.required' => 'Address Line 1 is Required.',
                'add2.sometimes' => 'Address Line 2 is Required.',
                'state.required' => 'State/Province is Required.',
                'city.required' => 'City/Municipality is Required.',
                'barangay.required' => 'Barangay is Required.',
                'postal.required' => 'Postal Code is Required.',
            ]
        );

        $getcustomerids = CrmCustomerInformation::where('account_no', $this->customer_no_2)->get();
        foreach($getcustomerids as $x => $getcustomerid){
        // dd($getcustomerid);
        $this->custid2 = $getcustomerid->id;
        $this->acctype2 = $getcustomerid->account_type;
        }

        // $addcustaddress = CrmCustomerInformationAddressList::create([
        //    'account_type' => $this->acctype2,
        //    'account_id' => $this->custid2,
        //    'address_line_1' => $validated['add1'],
        //    'address_line_2' => $validated['add2'],
        //    'address_type' => 1,
        //    'address_label' => $validated['addresslabel'],
        //    'state_id' => $validated['state'],
        //    'city_id' => $validated['city'],
        //    'barangay_id' => $validated['barangay'],
        //    'postal_id' => $this->postal,
        //    'is_primary' => 0,
        // ]);

        $this->statenames = StateReference::where('id', $this->state)->get();

        foreach($this->statenames as $a => $statename){
            $this->state_name = $this->statenames[$a]['name'];
            $stateid = $this->statenames[$a]['id'];
        }

        $this->citynames = CityReference::where('id', $this->city)->get();
        
        foreach($this->citynames as $a => $cityname){
            $this->city_name = $this->citynames[$a]['name'];
            $cityid = $this->citynames[$a]['id'];
        }

        $this->barangaynames = BarangayReference::where('id', $this->barangay)->get();
        
        foreach($this->barangaynames as $a => $barangayname){
            $this->barangay_name = $this->barangaynames[$a]['name'];
            $barangayid = $this->barangaynames[$a]['id'];
        }


        $this->address_2 = $this->add1.', '.$this->add2.', '.$this->state_name.', '.$this->city_name.', '.$this->barangay_name;

        $this->state2 = $stateid;
        $this->city2 = $cityid;
        $this->barangay2 = $barangayid;
        $this->postal2 = $this->postal;

        $this->create_addrs_modal = false;
        $this->change_address_modal = false;
        
        $this->reset([
            'addresslabel',
            'add1',
            'add2',
            'state',
            'city',
            'barangay',
            'postal',
        ]);
        $this->resetErrorBag();
    }

    public function actionCreateaddconsSubmit(array $data)
    {
        // dd($this->cons[$this->idcons][$this->idcons]['address_3']. 'sad');

        $rules = [
            'addresslabel' => 'required',
            'add1' => 'required',
            'add2' => 'sometimes',
            'state' => 'required',
            'city' => 'required',
            'barangay' => 'required',
            'postal' => 'required',

        ];

        $validated = $this->validate(
            $rules,
            [
                'addresslabel.required' => 'Address Label is Required.',
                'add1.required' => 'Address Line 1 is Required.',
                'add2.sometimes' => 'Address Line 2 is Required.',
                'state.required' => 'State/Province is Required.',
                'city.required' => 'City/Municipality is Required.',
                'barangay.required' => 'Barangay is Required.',
                'postal.required' => 'Postal Code is Required.',
            ]
        );
        // dd($this->cons[$this->idcons][$this->idcons]['customer_no_3']);


        $getcustomerconsids = CrmCustomerInformation::where('account_no', $this->cons[$this->idcons][$this->idcons]['customer_no_3'])->get();
        // dd($getcustomerconsids);
        foreach ($getcustomerconsids as $x => $getcustomerid) {
            // dd($getcustomerid);
            $this->custid2 = $getcustomerid->id;
            $this->acctype2 = $getcustomerid->account_type;
        }

        // $addcustaddress = CrmCustomerInformationAddressList::create([
        //     'account_type' => $this->acctype2,
        //     'account_id' => $this->custid2,
        //     'address_line_1' => $validated['add1'],
        //     'address_line_2' => $validated['add2'],
        //     'address_type' => 1,
        //     'address_label' => $validated['addresslabel'],
        //     'state_id' => $validated['state'],
        //     'city_id' => $validated['city'],
        //     'barangay_id' => $validated['barangay'],
        //     'postal_id' => $this->postal,
        //     'is_primary' => 0,
        // ]);

        $this->statenames = StateReference::where('id', $this->state)->get();

        foreach ($this->statenames as $a => $statename) {
            $this->state_name = $this->statenames[$a]['name'];
            $stateid = $this->statenames[$a]['id'];
        }

        $this->citynames = CityReference::where('id', $this->city)->get();

        foreach ($this->citynames as $a => $cityname) {
            $this->city_name = $this->citynames[$a]['name'];
            $cityid = $this->citynames[$a]['id'];
        }

        $this->barangaynames = BarangayReference::where('id', $this->barangay)->get();

        foreach ($this->barangaynames as $a => $barangayname) {
            $this->barangay_name = $this->barangaynames[$a]['name'];
            $barangayid = $this->barangaynames[$a]['id'];
        }

        $this->cons[$this->idcons][$this->idcons]['address_3'] = $this->add1 . ', ' . $this->add2 . ', ' . $this->state_name . ', ' . $this->city_name . ', ' . $this->barangay_name;
        $this->cons[$this->idcons][$this->idcons]['state_3'] = $stateid;
        $this->cons[$this->idcons][$this->idcons]['city_3'] = $cityid;
        $this->cons[$this->idcons][$this->idcons]['barangay_3'] = $barangayid;
        $this->cons[$this->idcons][$this->idcons]['postal_3'] = $this->postal;

        $this->create_addrs_cons_modal = false;
        $this->change_address_cons_modal = false;

        $this->reset([
            'addresslabel',
            'add1',
            'add2',
            'state',
            'city',
            'barangay',
            'postal',
        ]);
        $this->resetErrorBag();
    }


    public function closeeditmodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.sales.booking-mgmt.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
        $this->current_tab = 1;
    }

    public function action(BookingMgmtInterface $booking_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'create_next') {
            $response = $booking_interface->editValidationTab1($this->getRequest());
            // dd($response);
            if ($response['code'] == 200) {
                $this->current_tab = 2;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($action_type == 'create_next_2') {
            $response = $booking_interface->editValidationTab2($this->getRequest());
            // dd($response);
            if ($response['code'] == 200) {
                $this->current_tab = 3;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($action_type == 'create_next_3') {
            $response = $booking_interface->editValidationTab3($this->getRequest());
            if ($response['code'] == 200) {
                $this->current_tab = 4;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
        elseif ($action_type == 'create_next_4') {
            $response = $booking_interface->editValidationTab4($this->getRequest());
            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
                $this->current_tab = 4;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
        
    }
    public function submit(BookingMgmtInterface $booking_interface)
    {
        $response = $booking_interface->update($this->getRequest(),$this->book_id,);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.booking-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('crm.sales.booking-mgmt.index', 'index');
            $this->sweetAlert('', $response['message'], $response['result']);
            $this->current_tab = 1;
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    

    public function render()
    {
        $this->shipperAddressList;
        
        return view('livewire.crm.sales.booking-mgmt.edit');
    }
}
