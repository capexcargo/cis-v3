<?php

namespace App\Http\Livewire\Crm\Sales\Leads;

use App\Interfaces\Crm\Sales\Leads\LeadsInterface;
use App\Models\Crm\CrmQualificationQuestionnaire;
use App\Models\Crm\CrmSrQualificationQuestion;
use App\Models\CrmQualificationMgmt;
use App\Traits\Crm\Sales\Leads\LeadsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use LeadsTrait, WithPagination, PopUpMessagesTrait;

    public $header_type;
    public $type_header_cards = [];

    public $is_open = false;

    public $message_modal = false;
    public $retire_modal = false;
    public $action_type;

    // public $last_updated;

    public $l_res_qualification_score;
    public $l_diff_percent;

    // public $count_response = [];
    public $count_yes = [];
    public $count_no = [];

    protected $listeners = ['edit' => 'mount', 'close_modal' => 'closeModal', 'render' => 'render'];

    public function mount(LeadsInterface $leads_interface, $id)
    {
        $this->loadTypeHeaderCards();
        $this->header_type = 1;

        $this->lead_id = $id;
        $response = $leads_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->lead = $response['result'];

        $this->lead_name = $this->lead->lead_name;
        $this->sr_number = $this->lead->sr_no;
        $this->account_id = $this->lead->account_id;
        $this->industry_id = $this->lead->industry_id;
        $this->service_requirement = $this->lead->service_requirement_id;
        $this->lead_status = $this->lead->leadStatus->name;
        $this->qualification_score = $this->lead->qualification_score;
        $this->lead_classification = $this->lead->lead_classification_id;
        $this->deal_size = $this->lead->deal_size;
        $this->actual_amount_converted = 0;

        foreach ($this->lead->account->contactPersons as $i => $contact) {
            if ($contact->is_primary == 1) {
                $this->contact_person = $contact->first_name . " " . $contact->middle_name . " " . $contact->last_name;
            }
        }

        foreach ($this->lead->account->mobileNumbers as $i => $contact) {
            if ($contact->is_primary == 1) {
                $this->contact_mobile_no = $contact->mobile;
            }
        }

        foreach ($this->lead->account->emails as $i => $contact) {
            if ($contact->is_primary == 1) {
                $this->contact_email_add = $contact->email;
            }
        }
        // dd($this->lead->account);
        $this->description = $this->lead->description ?? null;
        $this->contact_owner = $this->lead->contactOwner->name ?? null;
        $this->customer_type = $this->lead->customerType->name ?? null;
        $this->channel_source = $this->lead->channel_source_id ?? null;
        $this->currency = $this->lead->currency ?? null;

        foreach ($this->lead->account->addresses as $i => $contact) {
            if ($contact->is_primary == 1) {
                $this->address_type = $contact->address_type;
                $this->address_line1 = $contact->address_line_1;
                $this->address_line2 = $contact->address_line_2;
                $this->state_province = $contact->state_id;
                $this->city_municipality = $contact->city_id;
                $this->barangay = $contact->barangay_id;
                $this->postal_code = $contact->postal_id;
            }
        }

        $for_countings = CrmQualificationQuestionnaire::with('qualifications')
            ->withCount([
                'qualifications as count_response' => function ($query) {
                    $query->where('response', '<>', "");
                    $query->where('lead_id', $this->lead_id);
                },
                'qualifications as count_yes' => function ($query) {
                    $query->where('response', 1);
                    $query->where('lead_id', $this->lead_id);
                },
                'qualifications as count_no' => function ($query) {
                    $query->where('response', 2);
                    $query->where('lead_id', $this->lead_id);
                }
            ])
            ->get();

        foreach ($for_countings as $i => $count) {
            // if ($count['count_response'] == 1) {
            //     $this->count_response[] = $for_countings[$i]['count_response'];
            // }

            if ($for_countings[$i]['count_yes'] == 1) {
                $this->count_yes[] = $for_countings[$i]['count_yes'];
            }

            if ($for_countings[$i]['count_no'] == 2) {
                $this->count_no[] = $for_countings[$i]['count_no'];
            }
        }

        $questionnaires = CrmSrQualificationQuestion::with('questions')->where('lead_id', $this->lead_id)->get();

        foreach ($questionnaires as $i => $questionnaire) {

            $this->qualifications_questionnaires[] = [
                'question_id' => $questionnaire->questions->id,
                'lead_id' => $this->lead_id,
                'sr_number' => $this->sr_number,
                'question' => $questionnaire->questions->question,
                'response' => $questionnaire->response,
                'remarks' => $questionnaire->remarks,
            ];
        }

        // $qualificationQuestion = CrmSrQualificationQuestion::where('lead_id', $this->lead_id)->latest()->first();

        // $updated_at = $qualificationQuestion->updated_at ?? null;

        // $this->last_updated = $updated_at != null ? date('m/d/Y H:i A', strtotime($updated_at)) : "";

        // $this->qualification_score = ((count($this->count_yes) / count($this->qualifications_questionnaires)) * 100);
        // $this->diff_percent = (100 - $this->qualification_score);
    }

    public function ActiveType($type)
    {
        if ($type == 1) {
            $this->header_type = 1;
        } elseif ($type == 2) {
            $this->header_type = 2;
        }

        $this->loadTypeHeaderCards();
    }

    public function loadTypeHeaderCards()
    {
        $this->header_type = $this->header_type ?? false;

        $this->type_header_cards = [
            [
                'title' => 'Summary',
                'class' => 'w-32 text-center bg-white text-gray-500 border border-gray-600 shadow-sm rounded-l-md',
                'color' => 'text-blue',
                'action' => 'type',
                'id' => 1
            ],
            [
                'title' => 'Qualifications',
                'class' => 'w-32 text-center bg-white text-gray-500 border border-gray-600 shadow-sm rounded-r-md',
                'color' => 'text-blue',
                'action' => 'type',
                'id' => 2
            ],
        ];
    }

    public function action($data, $action_type, LeadsInterface $leads_interface)
    {
        $this->action_type = $action_type;

        if ($action_type == 'retire') {
            $this->emitTo('crm.sales.leads.retire', 'retire', $data['id']);
            $this->lead_id = $data['id'];
            $this->retire_modal = true;
        } else if ($action_type == 'back') {
            return redirect()->route('crm.sales.leads.index');
        } elseif ($action_type == 'save_qualifications') {
            $response = $leads_interface->updateOrCreate($this->qualificationsGetRequest());

            if ($response['code'] == 200) {
                // $this->sweetAlert('', $response['message']);
                $this->message_modal = true;
                $this->emitTo('crm.sales.leads.edit', $this->lead_id, 'edit');
                // return redirect()->route('crm.sales.leads.edit', ['id' => $this->lead_id]);
            } elseif ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } else if ($action_type == 'convert') {
            $response = $leads_interface->convert($this->convertGetRequest(), $this->lead_id);
            if ($response['code'] == 200) {
                $this->sweetAlert('', $response['message']);
                return redirect()->route('crm.sales.opportunities.index');
            } elseif ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } else if ($action_type == 'management') {
            return redirect()->route('crm.sales.leads.qualification-mgmt.index');
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'retire') {
            $this->retire_modal = false;
        }
    }

    public function okAction()
    {
        $this->emit('render');
        $this->header_type = 2;
        $this->message_modal = false;

        $this->emit('dataLoaded', $this->l_res_qualification_score, $this->l_diff_percent);
    }

    public function render(LeadsInterface $leads_interface)
    {
        $for_countings = CrmQualificationQuestionnaire::with('qualifications')
            ->withCount([
                'qualifications as count_response' => function ($query) {
                    $query->where('response', '<>', "");
                    $query->where('lead_id', $this->lead_id);
                },
                'qualifications as count_yes' => function ($query) {
                    $query->where('response', 1);
                    $query->where('lead_id', $this->lead_id);
                },
                'qualifications as count_no' => function ($query) {
                    $query->where('response', 2);
                    $query->where('lead_id', $this->lead_id);
                }
            ])
            ->get();

        $count_yes = [];
        $count_response = [];

        foreach ($for_countings as $i => $count) {
            if ($count['count_response'] == 1) {
                $count_response[] = $for_countings[$i]['count_response'];
            }

            if ($for_countings[$i]['count_yes'] == 1) {
                $count_yes[] = $for_countings[$i]['count_yes'];
            }
        }

        $qualificationQuestion = CrmSrQualificationQuestion::where('lead_id', $this->lead_id)->latest()->first();

        $updated_at = $qualificationQuestion->updated_at ?? null;

        $last_updated = $updated_at != null ? date('m/d/Y H:i A', strtotime($updated_at)) : "";

        $this->l_res_qualification_score = ((count($count_yes) / count($this->qualifications_questionnaires)) * 100);
        $this->l_diff_percent = (100 - $this->l_res_qualification_score);

        return view('livewire.crm.sales.leads.edit', [
            'res_qualification_score' => $this->l_res_qualification_score,
            'count_response' => $count_response,
            'diff_percent' => $this->l_diff_percent,
            'last_updated' => $last_updated,
        ]);
    }
}
