<?php

namespace App\Http\Livewire\Crm\Sales\Leads;

use App\Interfaces\Crm\Sales\Leads\LeadsInterface;
use App\Models\Crm\CrmLeadRetirementReasons;
use App\Traits\Crm\Sales\Leads\LeadsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Retire extends Component
{
    use LeadsTrait, PopUpMessagesTrait;

    public $confirmation_modal = false;
    public $retirement_reason_references = [];

    public $retirement_reason;
    public $remarks;

    protected $listeners = ['retire' => 'mount'];

    public function mount(LeadsInterface $leads_interface, $id)
    {
        $this->lead_id = $id;
    }

    public function retirementReasonReferences()
    {
        $this->retirement_reason_references = CrmLeadRetirementReasons::get();
    }

    public function confirmationSubmit()
    {
        $rules = [
            'retirement_reason' => 'required',
            'remarks' => 'sometimes',
        ];

        $validated = $this->validate(
            $rules,
        );

        $this->confirmation_modal = true;
    }

    public function submit(LeadsInterface $leads_interface)
    {
        $response = $leads_interface->retire($this->retirementGetRequest(), $this->lead_id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.leads.edit', 'close_modal', 'retire');
            $this->emitTo('crm.sales.leads.edit', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.sales.leads.retire');
    }
}
