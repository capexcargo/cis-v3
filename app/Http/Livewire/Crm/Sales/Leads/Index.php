<?php

namespace App\Http\Livewire\Crm\Sales\Leads;

use App\Interfaces\Crm\Sales\Leads\LeadsInterface;
use App\Models\Crm\CrmLead;
use App\Models\Crm\CrmOpportunity;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $status;
    public $status_header_cards = [];

    public $severity_search;
    public $sr_no_search;
    public $date_created_search;
    public $status_search;
    public $action_type;

    public $lead_id;

    public $paginate = 10;

    public function mount()
    {
        $this->loadTypeHeaderCards();
        $this->status = "";
    }

    public function ActiveType($status)
    {
        if ($status == "") {
            $this->status = "";
        } elseif ($status == 1) {
            $this->status = 1;
        } elseif ($status == 2) {
            $this->status = 2;
        }

        $this->loadTypeHeaderCards();
    }

    public function loadTypeHeaderCards()
    {
        $this->status = $this->status ?? false;

        $this->status_header_cards = [
            [
                'title' => 'All',
                'value' => '25',
                'class' => 'w-28 flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm rounded-l-md',
                'color' => 'text-blue',
                'action' => 'status',
                'id' => false
            ],
            [
                'title' => 'Total Qualified',
                'value' => '25',
                'class' => 'w-40 flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm',
                'color' => 'text-blue',
                'action' => 'status',
                'id' => 1
            ],
            [
                'title' => 'Total Unqualified',
                'value' => '25',
                'class' => 'w-40 flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm rounded-r-md',
                'color' => 'text-blue',
                'action' => 'status',
                'id' => 2
            ],
        ];
    }

    public function action($data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'edit') {
            $this->lead_id = $data['id'];

            return redirect()->route('crm.sales.leads.edit', ['id' => $data['id']]);
        } else if ($action_type == 'redirectToOpportunity') {
            $lead = CrmLead::where('id', $data['id'])->first();

            $opportunity_id = $lead->opportunity->id;

            return redirect()->route('crm.sales.opportunities.edit', ['id' => $opportunity_id]);
        }
    }

    public function search()
    {
        return [
            'paginate' => $this->paginate,
            'severity' => $this->severity_search,
            'sr_no' => $this->sr_no_search,
            'date_created' => $this->date_created_search,
            'status' => $this->status_search,
        ];
    }

    public function render(LeadsInterface $leads_interface)
    {

        $response = $leads_interface->index($this->search());

        $all_leads = count($response['result']['leads']) ?? 0;
        $my_open_leads = 0;
        $my_teams_leads = 0;


        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'leads' => [],
                ];
        }

        $response_2 = $leads_interface->saveIfNotExisting();

        if ($response_2['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response_2['message'], $response_2['result']);
            $response_2['result'] =
                [
                    'for_crm_sr_qualification_question' => [],
                ];
        }

        return view('livewire.crm.sales.leads.index', [
            'all_leads' => $all_leads,
            'my_open_leads' => $my_open_leads,
            'my_teams_leads' => $my_teams_leads,
            'leads' => $response['result']['leads'],
        ]);
    }
}
