<?php

namespace App\Http\Livewire\Crm\Sales\Leads\QualificationMgmt;

use App\Interfaces\Crm\Sales\Leads\Qualification\QualificationMgmtInterface;
use App\Models\CrmQualificationMgmt;
use App\Traits\Crm\Sales\Leads\QualificationMgmt\QualificationMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use QualificationMgmtTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $question;
    public $qualification_type;
    public $question_s;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->question_s = CrmQualificationMgmt::findOrFail($id);

        $this->question = $this->question_s->question;
        $this->qualification_type = $this->question_s->qualification_type;
    }

    public function submit(QualificationMgmtInterface $qualification_interface)
    {
        $response = $qualification_interface->update($this->getRequest(), $this->question_s->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.leads.qualification-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('crm.sales.leads.qualification-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.crm.sales.leads.qualification-mgmt.edit');
    }
}
