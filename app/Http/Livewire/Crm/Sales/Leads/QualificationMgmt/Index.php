<?php

namespace App\Http\Livewire\Crm\Sales\Leads\QualificationMgmt;

use App\Interfaces\Crm\Sales\Leads\Qualification\QualificationMgmtInterface;
use App\Traits\Crm\Sales\Leads\QualificationMgmt\QualificationMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use QualificationMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $confirmation_message;
    
    public $question;
    public $qualification_type;
    public $qqm_id;

    public $action_type;
    public $search_request;


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } 
        elseif ($action_type == 'edit') {
            $this->emitTo('crm.sales.leads.qualification-mgmt.edit', 'edit', $data['id']);
            $this->qqm_id = $data['id'];
            $this->edit_modal = true;
        } 
        else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this Questionnaire?";
            $this->delete_modal = true;
            $this->qqm_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } 
        else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
        elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }

    public function confirm(QualificationMgmtInterface $sr_sub_interfaces)
    {
        if ($this->action_type == "delete") {
            $response = $sr_sub_interfaces->destroy($this->qqm_id);
            $this->emitTo('crm.sales.leads.qualification-mgmt.index', 'close_modal', 'delete');
            $this->emitTo('crm.sales.leads.qualification-mgmt.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('crm.sales.leads.qualification-mgmt.index', 'close_modal', 'delete');
            $this->emitTo('crm.sales.leads.qualification-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render(QualificationMgmtInterface $qualification_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $qualification_interface->index($request, $this->getRequest());

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'question_s' => [],
                ];
        }

        return view('livewire.crm.sales.leads.qualification-mgmt.index', [
            'question_s' => $response['result']['question_s']
        ]);
    }
    
}
