<?php

namespace App\Http\Livewire\Crm\Sales\AudienceSegmentation;

use App\Interfaces\Crm\Sales\SalesCampaign\AudienceSegmentationInterface;
use App\Traits\Crm\Sales\SalesCampaign\AudienceSegmentationTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use AudienceSegmentationTrait, PopUpMessagesTrait;
    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function validateSubmit()
    {
        $rules = [
            'name' => 'required',
            'filter_id' => 'required',
            'onboarded_date_from' => ($this->filter_id == 7 ? 'required' : 'sometimes'),
            'onboarded_date_to' => ($this->filter_id == 7 ? 'required' : 'sometimes'),
        ];
        foreach ($this->positions as $i => $position) {
            $rules['positions.' . $i . '.id'] = 'sometimes';
            $rules['positions.' . $i . '.criteria_id'] = ($this->filter_id != 7 ? 'required' : 'sometimes');
        }

        $validated = $this->validate(
            $rules,
            [
                'name.required' => 'This Segment Name field is required.',
                'filter_id.required' => 'This Filter field is required.',
                'onboarded_date_from.required' => 'This Date From field is required.',
                'onboarded_date_to.required' => 'This Date To field is required.',
                'positions.*.criteria_id.required' => 'The Criteria field is required.',
            ]
        );
        $this->confirmation_modal = true;
    }
   
    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.sales.audience-segmentation.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }
    
    public function submit(AudienceSegmentationInterface $audience_Segmentation_interface)
    {
        // dd($this->getRequest());
        $response = $audience_Segmentation_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.audience-segmentation.index', 'close_modal', 'create');
            $this->emitTo('crm.sales.audience-segmentation.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        if (!$this->positions) {
            $this->positions[] = [
                'id' => null,
                'criteria_id' => null,
                'is_deleted' => false,
            ];
        }
        return view('livewire.crm.sales.audience-segmentation.create');
    }
}
