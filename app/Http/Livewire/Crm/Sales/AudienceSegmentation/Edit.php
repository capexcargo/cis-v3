<?php

namespace App\Http\Livewire\Crm\Sales\AudienceSegmentation;

use App\Interfaces\Crm\Sales\SalesCampaign\AudienceSegmentationInterface;
use App\Models\Crm\CrmAudienceSegmentManagement;
use App\Models\Crm\CrmAudienceSegmentManagementCriteria;
use App\Traits\Crm\Sales\SalesCampaign\AudienceSegmentationTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use AudienceSegmentationTrait, PopUpMessagesTrait;

    public function mount($id, AudienceSegmentationInterface $audience_Segmentation_interface)
    {
        $response = $audience_Segmentation_interface->show($id);
        // dd($response);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $segment = $response['result'];

        $this->audience_segmentation_id = $id;
        $this->name = $segment->name ?? null;
        $this->filter_id = $segment->filter_id ?? null;
        $this->criteria_id = $segment->criteria_id ?? null; 
        // $this->criteria_name = $segment->criteria_name ?? null;
        $this->onboarded_date_from = $segment->onboarded_date_from ?? null;
        $this->onboarded_date_to = $segment->onboarded_date_to ?? null;
        $this->criteria = CrmAudienceSegmentManagement::with('criteria')->findOrFail($id);
        // $this->criteria = $audience_Segmentation_interface->show($id);

        foreach ($this->criteria->criteria as $a => $criteria_ids) {

            $this->positions[$a] =  [
                'id' => $criteria_ids->id,
                'criteria_id' => $criteria_ids->criteria_id,
                'is_deleted' => false,
            ];
        }

        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->positions as $a => $position) {

            $getkeyarray[] = $a;
            $countarray += ($this->positions[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    public function addPosition()
    {
        $this->positions[] = [
            'id' => null,
            'criteria_id' => null,
            'is_deleted' => false,
        ];
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->positions as $a => $position) {

            if ($this->positions[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->positions[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }
    
    public function removePosition(array $data)
    {
        if (count(collect($this->positions)->where('is_deleted', false)) > 1) {
            if ($this->positions[$data["a"]]['id']) {
                $this->positions[$data["a"]]['is_deleted'] = true;
                // unset($this->positions[$data["a"]]);
            } else {
                unset($this->positions[$data["a"]]);
            }

            array_values($this->positions);
        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->positions as $a => $position) {

            if ($this->positions[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->positions[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    public function validateSubmit(AudienceSegmentationInterface $audience_Segmentation_interface)
    {
        $rules = [
            'name' => 'required',
            'filter_id' => 'required',
            'onboarded_date_from' => ($this->filter_id == 7 ? 'required' : 'sometimes'),
            'onboarded_date_to' => ($this->filter_id == 7 ? 'required' : 'sometimes'),
        ];

        foreach ($this->positions as $i => $position) {
            $rules['positions.' . $i . '.id'] = 'sometimes';
            $rules['positions.' . $i . '.criteria_id'] = ($this->filter_id != 7 ? 'required' : 'sometimes');
            $rules['positions.' . $i . '.is_deleted'] = 'sometimes';
        }

        $validated = $this->validate(
            $rules,
            [
                'name.required' => 'This Segment Name field is required.',
                'filter_id.required' => 'This Filter field is required.',
                'onboarded_date_from.required' => 'This Date From field is required.',
                'onboarded_date_to.required' => 'This Date To field is required.',
                'positions.*.criteria_id.required' => 'The Criteria field is required.',

            ]
        );
        $response = $audience_Segmentation_interface->update($this->getRequest(), $this->audience_segmentation_id);
        // dd($response);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.audience-segmentation.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.crm.sales.audience-segmentation.edit');
    }
}
