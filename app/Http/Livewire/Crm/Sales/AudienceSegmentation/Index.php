<?php

namespace App\Http\Livewire\Crm\Sales\AudienceSegmentation;

use App\Interfaces\Crm\Sales\SalesCampaign\AudienceSegmentationInterface;
use App\Models\Crm\CrmAudienceSegmentManagement;
use App\Models\Crm\CrmAudienceSegmentManagementCriteria;
use App\Traits\Crm\Sales\SalesCampaign\AudienceSegmentationTrait;
use App\Traits\PopUpMessagesTrait;
use CreateCrmAudienceSegmentManagementCriteria;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use AudienceSegmentationTrait, WithPagination, PopUpMessagesTrait;
    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal','mount'=>'mount'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } 
        elseif ($action_type == 'edit') {
            $this->emitTo('crm.sales.audience-segmentation.edit', 'edit', $data['id']);
            $this->audience_segmentation_id = $data['id'];
            $this->edit_modal = true;
        } 
        // else if ($this->action_type == 'delete') {
        //     $this->delete_modal = true;
        //     $this->emitTo('crm.sales.sales-campaign.summary.index', 'delete', $data['id']);
        //     $this->summary_id = $data['id'];
        // } else if ($action_type == 'terms') {
        //     $this->emitTo('crm.sales.sales-campaign.summary.terms-view', 'terms', $data['id']);
        //     $this->summary_id = $data['id'];
        //     $this->terms_modal = true;
        // } 
        else if ($action_type == 'update_status') {
            if ($data['status_id'] == 1) {
                $this->reactivate_modal = true;
                $this->audience_segmentation_id = $data['id'];
                return;
            }

            $this->deactivate_modal = true;
            $this->audience_segmentation_id = $data['id'];
        }
        // dd($action_type);
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } 
        else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
        // elseif ($action_type == 'delete') {
        //     $this->delete_modal = false;
        // } elseif ($action_type == 'terms') {
        //     $this->terms_modal = false;
        // } 
        elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        }
    }
    public function updateStatus($id, $value)
    {
        // dd($value);
        $segment = CrmAudienceSegmentManagement::findOrFail($id);
        // dd($segment);

        $segment->update([
            'status_id' => $value,
        ]);



        if ($value == 1) {

            $this->emitTo('crm.sales.audience-segmentation.index', 'close_modal', 'update_status');
            $this->emitTo('crm.sales.audience-segmentation.index', 'index');
            $this->sweetAlert('', 'Audience Segment Successfully Reactivated!');
            return;

            // return redirect()->to(route('crm.sales.audience-segmentation.index'));

        }

        $this->emitTo('crm.sales.audience-segmentation.index', 'close_modal', 'update_status');
        $this->emitTo('crm.sales.audience-segmentation.index', 'index');
        $this->sweetAlert('', 'Audience Segment Successfully Deactivated!');
    }
    public function render(AudienceSegmentationInterface $audience_Segmentation_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $audience_Segmentation_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'segment' => [],
                ];
        }
        // dd($response);
        
        return view('livewire.crm.sales.audience-segmentation.index', [
            'segment' => $response['result']['segment']
        ]);
    }
    
}
