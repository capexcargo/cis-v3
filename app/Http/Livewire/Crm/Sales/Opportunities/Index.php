<?php

namespace App\Http\Livewire\Crm\Sales\Opportunities;

use App\Interfaces\Crm\Sales\Opportunities\OpportunitiesInterface;
use App\Models\Crm\CrmOpportunity;
use App\Models\CrmSalesStage;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $all_opportunities = 0;
    public $my_open_opportunities = 0;
    public $my_teams_opportunities = 0;

    public $sales_stage;
    public $sales_stage_header_cards = [];

    public $opportunity_status = 0;

    public $opportunity_name_search;
    public $sr_no_search;
    public $date_created_search;
    public $sales_stage_search;
    public $action_type;

    public $paginate = 10;

    public $view_modal = false;
    public $opportunity_id;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function mount()
    {
        $this->loadTypeHeaderCards();
        $this->sales_stage = "";

        $this->all_opportunities = CrmOpportunity::count();
        $this->my_open_opportunities = CrmOpportunity::where('opportunity_status_id', 1)->count();
    }

    public function opportunitiesStatus($status)
    {
        if ($status == 0) {
            $this->opportunity_status = false;
        } elseif ($status == 1) {
            $this->opportunity_status = 1;
        }
    }

    public function ActiveType($sales_stage)
    {

        if ($sales_stage == "") {
            $this->sales_stage = "";
        } elseif ($sales_stage == 1) {
            $this->sales_stage = 1;
        } elseif ($sales_stage == 2) {
            $this->sales_stage = 2;
        } elseif ($sales_stage == 3) {
            $this->sales_stage = 3;
        } elseif ($sales_stage == 4) {
            $this->sales_stage = 4;
        } elseif ($sales_stage == 5) {
            $this->sales_stage = 5;
        }

        $this->loadTypeHeaderCards();
    }

    public function loadTypeHeaderCards()
    {
        $this->sales_stage = $this->sales_stage ?? false;

        $sales_stage = CrmSalesStage::withCount('opportunities')->get();

        $this->sales_stage_header_cards = [
            [
                'title' => 'All',
                'value' =>  $sales_stage->sum('opportunities_count'),
                'class' => 'w-24 flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm rounded-l-md',
                'color' => 'text-blue',
                'action' => 'sales_stage',
                'id' => false
            ],
            [
                'title' => 'Initiation',
                'value' =>  $sales_stage[0]->opportunities_count ?? 0,
                'class' => 'w-28 flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm',
                'color' => 'text-blue',
                'action' => 'sales_stage',
                'id' => 1
            ],
            [
                'title' => 'Quotation',
                'value' =>  $sales_stage[1]->opportunities_count ?? 0,
                'class' => 'w-28 flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm',
                'color' => 'text-blue',
                'action' => 'sales_stage',
                'id' => 2
            ],
            [
                'title' => 'Negotiation',
                'value' =>  $sales_stage[2]->opportunities_count ?? 0,
                'class' => 'w-28 flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm',
                'color' => 'text-blue',
                'action' => 'sales_stage',
                'id' => 3
            ],
            [
                'title' => 'Loss',
                'value' =>  $sales_stage[3]->opportunities_count ?? 0,
                'class' => 'w-24 flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm',
                'color' => 'text-blue',
                'action' => 'sales_stage',
                'id' => 4
            ],
            [
                'title' => 'Won',
                'value' =>  $sales_stage[4]->opportunities_count ?? 0,
                'class' => 'w-24 flex justify-between bg-white text-gray-500 border border-gray-600 shadow-sm rounded-r-md',
                'color' => 'text-blue',
                'action' => 'sales_stage',
                'id' => 5
            ],
        ];
    }

    public function action($data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'edit') {
            $this->opportunity_id = $data['id'];
            // dd($this->opportunity_id);
            return redirect()->route('crm.sales.opportunities.edit', ['id' => $data['id']]);
        } elseif ($action_type == 'view') {
            $this->opportunity_id = $data['id'];
            $this->view_modal = true;
            // dd($this->opportunity_id);
        } elseif ($action_type == 'sales_stage') {
            return redirect()->route('crm.sales.opportunities.sales-stage.index');
        } elseif ($action_type == 'opportunity_status') {
            return redirect()->route('crm.sales.opportunities.opportunity-status.index');
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'view') {
            $this->view_modal = false;
        }
        // elseif ($action_type == 'delete') {
        //     $this->delete_modal = false;
        // }
    }

    public function search()
    {
        return [
            'paginate' => $this->paginate,
            'opportunity_name' => $this->opportunity_name_search,
            'sr_no' => $this->sr_no_search,
            'date_created' => $this->date_created_search,
            'sales_stage' => $this->sales_stage,
            'opportunity_status' => $this->opportunity_status,
        ];
    }

    public function render(OpportunitiesInterface $opportunities_interface)
    {
        $response = $opportunities_interface->index($this->search());

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'opportunities' => [],
                ];
        }

        return view('livewire.crm.sales.opportunities.index', [
            'opportunities' => $response['result']['opportunities'],
        ]);
    }
}
