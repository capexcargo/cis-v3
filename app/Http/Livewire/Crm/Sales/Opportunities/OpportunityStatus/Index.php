<?php

namespace App\Http\Livewire\Crm\Sales\Opportunities\OpportunityStatus;

use App\Interfaces\Crm\Sales\Opportunities\OpportunityStatus\OpportunityStatusInterface;
use App\Models\CrmOpportunityStatusMgmt;
use App\Traits\Crm\Sales\Opportunities\OpportunityStatus\OpportunityStatusTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use OpportunityStatusTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $reactivate_modal = false;
    public $deactivate_modal = false;
    public $confirmation_message;

    public $name;
    public $osm_id;

    public $action_type;
    public $search_request;


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('crm.sales.opportunities.opportunity-status.edit', 'edit', $data['id']);
            $this->osm_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->osm_id = $data['id'];
                return;
            }

            $this->deactivate_modal = true;
            $this->osm_id = $data['id'];
        }
        // else if ($action_type == 'delete') {
        //     $this->confirmation_message = "Are you sure you want to delete this Questionnaire?";
        //     $this->delete_modal = true;
        //     $this->osm_id = $data['id'];
        // }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        }
        // elseif ($action_type == 'delete') {
        //     $this->delete_modal = false;
        // }
    }

    public function updated()
    {
        $this->resetPage();

    }

    public function updateStatus($id, $value)
    {
        // dd($value);
        $deactivate_a = CrmOpportunityStatusMgmt::findOrFail($id);
        $deactivate_a->update([
            'status' => $value,
        ]);



        if ($value == 1) {

            $this->emitTo('crm.sales.opportunities.opportunity-status.index', 'close_modal', 'update_status');
            $this->emitTo('crm.sales.opportunities.opportunity-status.index', 'index');
            $this->sweetAlert('', 'Opportunity Status Successfully Reactivated!');
            return;

            // return redirect()->to(route('crm.sales.opportunities.opportunity-status.index'));

        }

        $this->emitTo('crm.sales.opportunities.opportunity-status.index', 'close_modal', 'update_status');
        $this->emitTo('crm.sales.opportunities.opportunity-status.index', 'index');
        $this->sweetAlert('', 'Opportunity Status Successfully Deactivated!');


    }

    public function render(OpportunityStatusInterface $status_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $status_interface->index($request, $this->getRequest());

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'status_s' => [],
                ];
        }

        return view('livewire.crm.sales.opportunities.opportunity-status.index', [
            'status_s' => $response['result']['status_s']
        ]);
    }
}
