<?php

namespace App\Http\Livewire\Crm\Sales\Opportunities\OpportunityStatus;

use App\Interfaces\Crm\Sales\Opportunities\OpportunityStatus\OpportunityStatusInterface;
use App\Models\CrmOpportunityStatusMgmt;
use App\Traits\Crm\Sales\Opportunities\OpportunityStatus\OpportunityStatusTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use OpportunityStatusTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $name;
    public $status_s;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->status_s = CrmOpportunityStatusMgmt::findOrFail($id);

        $this->name = $this->status_s->name;
    }

    public function action(OpportunityStatusInterface $status_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'submit2') {
            $response = $status_interface->updateValidation($this->getRequest());
            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function submit(OpportunityStatusInterface $status_interface)
    {
        $response = $status_interface->update($this->getRequest(), $this->status_s->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.opportunities.opportunity-status.index', 'close_modal', 'edit');
            $this->emitTo('crm.sales.opportunities.opportunity-status.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.sales.opportunities.opportunity-status.edit');
    }
}
