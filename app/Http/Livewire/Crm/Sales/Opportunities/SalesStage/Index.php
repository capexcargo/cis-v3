<?php

namespace App\Http\Livewire\Crm\Sales\Opportunities\SalesStage;

use App\Interfaces\Crm\Sales\Opportunities\SalesStage\SalesStageInterface;
use App\Models\CrmSalesStage;
use App\Traits\Crm\Sales\Opportunities\SalesStage\SalesStageTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use SalesStageTrait, WithPagination, PopUpMessagesTrait;

    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $confirmation_message;

    public $sales_stage;
    public $opportunity_status_id;
    public $sales_id;

    public $action_type;
    public $search_request;

    public $reactivate_modal = false;
    public $deactivate_modal = false;


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('crm.sales.opportunities.sales-stage.edit', 'edit', $data['id']);
            $this->sales_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->sales_id = $data['id'];
                return;
            }

            $this->deactivate_modal = true;
            $this->sales_id = $data['id'];
        }
        // else if ($action_type == 'delete') {
        //     $this->confirmation_message = "Are you sure you want to delete this Sales Stage?";
        //     $this->delete_modal = true;
        //     $this->sales_id = $data['id'];
        // }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        }
    }

    public function updated()
    {
        $this->resetPage();

    }

    public function updateStatus($id, $value)
    {
        // dd($value);
        $deactivate_a = CrmSalesStage::findOrFail($id);
        // dd($deactivate_a);
        $deactivate_a->update([
            'status' => $value,
        ]);



        if ($value == 1) {

            $this->emitTo('crm.sales.opportunities.sales-stage.index', 'close_modal', 'update_status');
            $this->emitTo('crm.sales.opportunities.sales-stage.index', 'index');
            $this->sweetAlert('', 'Sales Stage Successfully Reactivated!');
            return;

            // return redirect()->to(route('crm.sales.opportunities.sales-stage.index'));

        }

        $this->emitTo('crm.sales.opportunities.sales-stage.index', 'close_modal', 'update_status');
        $this->emitTo('crm.sales.opportunities.sales-stage.index', 'index');
        $this->sweetAlert('', 'Sales Stage Successfully Deactivated!');


    }

    // public function confirm(SalesStageInterface $SalesStageInterface)
    // {
    //     if ($this->action_type == "delete") {
    //         $response = $SalesStageInterface->destroy($this->sales_id);
    //         $this->emitTo('crm.sales.opportunities.sales-stage.index', 'close_modal', 'delete');
    //         $this->emitTo('crm.sales.opportunities.sales-stage.index', 'index');
    //     }

    //     if ($response['code'] == 200) {
    //         $this->emitTo('crm.sales.opportunities.sales-stage.index', 'close_modal', 'delete');
    //         $this->emitTo('crm.sales.opportunities.sales-stage.index', 'index');
    //         $this->sweetAlert('', $response['message']);
    //     } else {
    //         $this->sweetAlertError('error', $response['message'], $response['result']);
    //     }
    // }

    public function render(SalesStageInterface $SalesStageInterfaces)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $SalesStageInterfaces->index($request, $this->getRequest());

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'sales_s' => [],
                ];
        }

        return view('livewire.crm.sales.opportunities.sales-stage.index', [
            'sales_s' => $response['result']['sales_s']
        ]);
    }
}
