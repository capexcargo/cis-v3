<?php

namespace App\Http\Livewire\Crm\Sales\Opportunities\SalesStage;

use App\Interfaces\Crm\Sales\Opportunities\SalesStage\SalesStageInterface;
use App\Models\CrmSalesStage;
use App\Traits\Crm\Sales\Opportunities\SalesStage\SalesStageTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{

    use SalesStageTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $sales_stage;
    public $opportunity_status_id;
    public $status_s;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->status_s = CrmSalesStage::findOrFail($id);

        $this->sales_stage = $this->status_s->sales_stage;
        $this->opportunity_status_id = $this->status_s->opportunity_status_id;
    }

    public function action(SalesStageInterface $status_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'submit2') {
            $response = $status_interface->updateValidation($this->getRequest());
            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function submit(SalesStageInterface $status_interface)
    {
        $response = $status_interface->update($this->getRequest(), $this->status_s->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.opportunities.sales-stage.index', 'close_modal', 'edit');
            $this->emitTo('crm.sales.opportunities.sales-stage.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.sales.opportunities.sales-stage.edit');
    }
}
