<?php

namespace App\Http\Livewire\Crm\Sales\Opportunities\SalesStage;

use App\Interfaces\Crm\Sales\Opportunities\SalesStage\SalesStageInterface;
use App\Traits\Crm\Sales\Opportunities\SalesStage\SalesStageTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use SalesStageTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $sales_stage;
    public $opportunity_status_id;


    // protected $rules = [
    //     'sales_stage' => 'required',
    //     'opportunity_status_id' => 'required',
    // ];

    // public function confirmationSubmit()
    // {
    //     $this->validate();

    //     $this->confirmation_modal = true;
    // }

    

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.sales.opportunities.sales-stage.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function action(SalesStageInterface $sales_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'submit2') {
            $response = $sales_interface->createValidation($this->getRequest());
            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }


    
    public function submit(SalesStageInterface $sales_interface)
    {
        // dd($this->getRequest());
        
        $response = $sales_interface->create($this->getRequest());
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.sales.opportunities.sales-stage.index', 'close_modal', 'create');
            $this->emitTo('crm.sales.opportunities.sales-stage.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.sales.opportunities.sales-stage.create');
    }
}
