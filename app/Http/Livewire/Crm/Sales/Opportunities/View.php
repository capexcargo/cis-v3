<?php

namespace App\Http\Livewire\Crm\Sales\Opportunities;

use Livewire\Component;

class View extends Component
{
    protected $listeners = ['view' => 'render'];

    public function render()
    {
        return view('livewire.crm.sales.opportunities.view');
    }
}
