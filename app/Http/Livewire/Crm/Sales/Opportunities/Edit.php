<?php

namespace App\Http\Livewire\Crm\Sales\Opportunities;

use App\Interfaces\Crm\Sales\Opportunities\OpportunitiesInterface;
use App\Models\Crm\CrmIndustry;
use App\Models\CrmSalesStage;
use App\Traits\Crm\Sales\Opportunities\OpportunitiesTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Edit extends Component
{
    use OpportunitiesTrait, WithFileUploads, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount', 'close_modal' => 'closeModal'];

    public function mount(OpportunitiesInterface $opportunity_interface, $id)
    {
        $this->opportunity_id = $id;
        $response = $opportunity_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->opportunity = $response['result'];

        $this->opportunity_name = $this->opportunity->opportunity_name;
        $this->sr_number = $this->opportunity->sr_no;
        $this->account_id = $this->opportunity->account_id;

        $this->company_industry = $this->opportunity->industry_id;
        $this->company_industry_references = CrmIndustry::get();

        $this->sales_stage = $this->opportunity->sales_stage_id;
        $this->sales_stage_references = CrmSalesStage::get();

        $this->opportunity_status = $this->opportunity->opportunityStatus->name;
        $this->deal_size = $this->opportunity->deal_size;
        $this->actual_amount_converted = $this->opportunity->actual_amount;
        $this->booking_reference = $this->opportunity->booking_reference;


        foreach ($this->opportunity->account->contactPersons as $i => $contact) {
            if ($contact->is_primary == 1) {
                $this->contact_person = $contact->first_name . " " . $contact->middle_name . " " . $contact->last_name;
            }
        }

        foreach ($this->opportunity->account->mobileNumbers as $i => $contact) {
            if ($contact->is_primary == 1) {
                $this->contact_mobile_no = $contact->mobile;
            }
        }

        foreach ($this->opportunity->account->emails as $i => $contact) {
            if ($contact->is_primary == 1) {
                $this->contact_email_add = $contact->email;
            }
        }

        $this->contact_owner = $this->opportunity->account->contactOwner->name ?? null;
        $this->customer_type = $this->opportunity->lead->customerType->name ?? null;
        $this->lead_conversion_date = $this->opportunity->lead_conversion_date ?? null;
        $this->close_date = $this->opportunity->close_date ?? null;

        $this->addAttachments();
    }

    public function action($data, $action_type, OpportunitiesInterface $opportunities_interface)
    {
        if ($action_type == 'back') {
            return redirect()->route('crm.sales.opportunities.index');
        } elseif ($action_type == 'save') {
            $response = $opportunities_interface->update($this->getRequest(), $this->opportunity_id);

            if ($response['code'] == 200) {
                $this->sweetAlert('', $response['message']);
                $this->emitTo('crm.sales.opportunities.edit', $this->opportunity_id, 'edit');
            } elseif ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($action_type == 'view') {
            $this->opportunity_id = $data['id'];
            $this->view_modal = true;
        }
    }
    public function render()
    {
        return view('livewire.crm.sales.opportunities.edit');
    }
}
