<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\LandFreight;

use App\Models\CrmRateLandFreight;
use App\Traits\Crm\Commercials\LandFreight\LandFreightTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class View extends Component
{

    use LandFreightTrait, PopUpMessagesTrait;

    public $rate_type_id;
    public $transport_mode_id;
    public $ancillary_charge_id;
    public $commodity_type_id;
    public $service_mode_id;
    public $apply_for_id;
    public $box_type_id;
    public $dates;
    public $transport;
    public $vice_versa;
    public $base_rate_id;

    public $rate_id;
    public $land_s;
    public $transportmode;
    public $boxes = [];
    public $origin_id;
    public $vice_versa_status;


    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function mount($id)
    {
        // dd($id);

        $this->resetForm();
        $this->land_s = CrmRateLandFreight::with(['RateLandfreightHasMany' => function ($query) {
            $query->with('OriginDetails', 'DestinationDetails')->where('is_primary', 1);
        }])->find($id);
        
        // dd($this->land_s);

        if ($this->land_s->is_vice_versa == 1) {
            $this->vice_versa_status = 'Yes';
        } else {
            $this->vice_versa_status = 'No';
        }

        if ($this->land_s->is_vice_versa == 1) {
            $this->vice_versa_display = 'VV';
        } else {
            $this->vice_versa_display = '';
        }

    }

    public function render()
    {
        return view('livewire.crm.commercials.commercial-mgmt.land-freight.view');
    }
}
