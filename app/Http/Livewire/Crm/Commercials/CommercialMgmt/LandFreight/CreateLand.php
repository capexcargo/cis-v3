<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\LandFreight;

use App\Interfaces\Crm\Commercials\LandFreight\LandFreightInterface;
use App\Models\CrmRateLandFreight;
use App\Models\CrmTransportMode;
use App\Traits\Crm\Commercials\LandFreight\LandFreightTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class CreateLand extends Component
{

    use LandFreightTrait, PopUpMessagesTrait;

    public $apply_for_id;
    public $dates;
    public $transport;
    public $vice_versa;
    public $base_rate_id;

    public $amount_weight_1;
    public $amount_weight_2;
    public $amount_weight_3;
    public $amount_weight_4;

    public $commodity_type_id;
    public $transport_mode_id;
    public $transportmode;
    public $ancillary_charge_id;
    public $service_mode_id;

    public $bases = [];




    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];


    public function mount()
    {
        // dd($rate);
        // $this->rate = $rate;
        $this->bases = CrmRateLandFreight::where('final_status_id','2')->get();
        // $this->pouch_s = CrmRatePouch::with('RatePouchHasMany')->get();

        $this->transportmode = CrmTransportMode::where('id', 3)->first();
        $this->transport_mode_id = $this->transportmode->name;




        $this->created_by = Auth::user()->name;
        $this->dates = date('m/d/Y');
        // $this->transport = ('rate_type_id');
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.commercials.commercial-mgmt.land-freight.index', 'close_modal', 'create_land');
        $this->confirmation_modal = false;
    }

    public function action(LandFreightInterface $landfreight_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'create_next') {
            $response = $landfreight_interface->createValidationTab1of1($this->getRequestLand());
            if ($response['code'] == 200) {
                $this->current_tab = 2;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
        elseif ($action_type == 'create_next_perview') {

            $response = $landfreight_interface->createValidationTab2of2($this->getRequestLand());
            // dd($response);

            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

     


    public function submit(LandFreightInterface $land_interface)
    {
        // dd($this->getRequestland());
        $response = $land_interface->createLand($this->getRequestLand());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.commercials.commercial-mgmt.land-freight.index', 'close_modal', 'create_land');
            $this->emitTo('crm.commercials.commercial-mgmt.land-freight.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.commercials.commercial-mgmt.land-freight.create-land');
    }
}
