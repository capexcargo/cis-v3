<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\Box;

use App\Models\CrmRateBox;
use App\Traits\Crm\Commercials\Box\BoxTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class View extends Component
{
    use BoxTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public $rate_type_id;
    public $transport_mode_id;
    public $booking_type_id;
    public $apply_for_id;
    public $box_type_id;
    public $dates;
    public $transport;
    public $vice_versa;
    public $base_rate_id;

    public $rate_id;
    public $box_s;
    public $transportmode;
    public $boxes = [];
    public $origin_id;
    public $vice_versa_status;


    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function mount($id)
    {
        // dd($id);

        $this->resetForm();
        $this->box_s = CrmRateBox::with(['RateBoxHasMany' => function ($query) {
            $query->with('OriginDetails', 'DestinationDetails')->where('is_primary', 1);
        }])->find($id);
        
        // dd($this->box_s);

        if ($this->box_s->is_vice_versa == 1) {
            $this->vice_versa_status = 'Yes';
        } else {
            $this->vice_versa_status = 'No';
        }

        if ($this->box_s->is_vice_versa == 1) {
            $this->vice_versa_display = 'VV';
        } else {
            $this->vice_versa_display = '';
        }



    }
    public function render()
    {
        return view('livewire.crm.commercials.commercial-mgmt.box.view');
    }
}
