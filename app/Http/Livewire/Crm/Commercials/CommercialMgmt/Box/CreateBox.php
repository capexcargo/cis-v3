<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\Box;

use App\Interfaces\Crm\Commercials\Box\BoxInterface;
use App\Models\CrmRateBox;
use App\Models\CrmRatePouch;
use App\Traits\Crm\Commercials\Box\BoxTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class CreateBox extends Component
{

    use BoxTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public $rate_type_id;
    public $transport_mode_id;
    public $booking_type_id;
    public $apply_for_id;
    public $pouch_type_id;
    public $dates;
    public $transport;
    public $vice_versa;
    public $base_rate_id;
    public $selectDestination = [];

    public $small;
    public $medium;
    public $large;
    public $pouch_s;

    public $bases = [];




    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];


    public function mount()
    {
        // dd($rate);
        // $this->rate = $rate;
        $this->bases = CrmRateBox::where('final_status_id','2')->get();
        // $this->pouch_s = CrmRatePouch::with('RatePouchHasMany')->get();

        // $this->origin_id = $this->pouch_s->origin_id;

        // dd($this->origin_id);




        $this->created_by = Auth::user()->name;
        $this->dates = date('m/d/Y');
        // $this->transport = ('rate_type_id');
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.commercials.commercial-mgmt.box.index', 'close_modal', 'create_box');
        $this->confirmation_modal = false;
    }

    public function action(BoxInterface $box_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'create_next') {
            $response = $box_interface->createValidationTab1of1($this->getRequestBox());
            if ($response['code'] == 200) {
                $this->current_tab = 2;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
        elseif ($action_type == 'create_next_perview') {

            $response = $box_interface->createValidationTab2of2($this->getRequestBox());
            // dd($response);

            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

     


    public function submit(BoxInterface $box_interface)
    {
        // dd($this->getRequestbox());
        $response = $box_interface->createBox($this->getRequestBox());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.commercials.commercial-mgmt.box.index', 'close_modal', 'create_box');
            $this->emitTo('crm.commercials.commercial-mgmt.box.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.commercials.commercial-mgmt.box.create-box');
    }
}
