<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\Pouch;

use App\Models\CrmRatePouch;
use App\Models\CrmTransportMode;
use App\Traits\Crm\Commercials\Pouch\PouchTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class View extends Component
{
    use PouchTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public $rate_type_id;
    public $transport_mode_id;
    public $booking_type_id;
    public $apply_for_id;
    public $pouch_type_id;
    public $dates;
    public $transport;
    public $vice_versa;
    public $base_rate_id;

    public $pouch_id;
    public $pouch_s;
    public $transportmode;
    public $pouches = [];
    public $origin_id;
    public $vice_versa_status;


    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function mount($id)
    {
        // dd($id);

        $this->resetForm();
        $this->pouch_s = CrmRatePouch::with(['RatePouchHasMany' => function ($query) {
            $query->with('OriginDetails', 'DestinationDetails')->where('is_primary', 1);
        },'PouchTypeReference'])->find($id);

        if ($this->pouch_s->is_vice_versa == 1) {
            $this->vice_versa_status = 'Yes';
        } else {
            $this->vice_versa_status = 'No';
        }

        if ($this->pouch_s->is_vice_versa == 1) {
            $this->vice_versa_display = 'VV';
        } else {
            $this->vice_versa_display = '';
        }

        // dd($this->pouch_s->vice_versa);
        // foreach ($this->pouch_s as $pouch) {
        //     $this->pouches[] = $pouch->RatePouchHasMany;
        // }


        // dd($this->pouch_s);


    }

    public function render()
    {
        return view('livewire.crm.commercials.commercial-mgmt.pouch.view');
    }
}
