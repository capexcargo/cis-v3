<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\Pouch;

use App\Interfaces\Crm\Commercials\Pouch\PouchInterface;
use App\Models\CrmRatePouch;
use App\Traits\Crm\Commercials\Pouch\PouchTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class CreatePouch extends Component
{
    use PouchTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public $rate_type_id;
    public $transport_mode_id;
    public $booking_type_id;
    public $apply_for_id;
    public $pouch_type_id;
    public $dates;
    public $transport;
    public $vice_versa;
    public $base_rate_id;
    public $selectDestination = [];

    public $small;
    public $medium;
    public $large;

    public $bases = [];




    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];


    public function mount()
    {
        // dd($rate);
        // $this->rate = $rate;
        $this->bases = CrmRatePouch::where('final_status_id','2')->get();

        $this->created_by = Auth::user()->name;
        $this->dates = date('m/d/Y');
        // $this->transport = ('rate_type_id');
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.commercials.commercial-mgmt.pouch.index', 'close_modal', 'create_pouch');
        $this->confirmation_modal = false;
    }

    public function action(PouchInterface $pouch_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'create_next') {
            $response = $pouch_interface->createValidationTab1of1($this->getRequestPouch());
            if ($response['code'] == 200) {
                $this->current_tab = 2;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
        elseif ($action_type == 'create_next_perview') {

            $response = $pouch_interface->createValidationTab2of2($this->getRequestPouch());
            // dd($response);

            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

     


    public function submit(PouchInterface $pouch_interface)
    {
        // dd($this->getRequestPouch());
        $response = $pouch_interface->createPouch($this->getRequestPouch());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.commercials.commercial-mgmt.pouch.index', 'close_modal', 'create_pouch');
            $this->emitTo('crm.commercials.commercial-mgmt.pouch.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    
    public function render()
    {
        return view('livewire.crm.commercials.commercial-mgmt.pouch.create-pouch');
    }
}
