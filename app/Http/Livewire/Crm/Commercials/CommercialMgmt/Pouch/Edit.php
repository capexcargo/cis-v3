<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\Pouch;

use App\Interfaces\Crm\Commercials\Pouch\PouchInterface;
use App\Models\CrmRatePouch;
use App\Models\CrmTransportMode;
use App\Traits\Crm\Commercials\Pouch\PouchTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Edit extends Component
{

    use PouchTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public $rate_type_id;
    public $transport_mode_id;
    public $booking_type_id;
    public $apply_for_id;
    public $pouch_type_id;
    public $dates;
    public $transport;
    public $vice_versa;
    public $base_rate_id;


    public $pouch_id;
    public $pouch_s;
    public $transportmode;



    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function mount($id)
    {
        // dd($id);
        
        $this->resetForm();
        $this->dates = date('m/d/Y');

        $this->pouch_s = CrmRatePouch::with('RatePouchHasMany')->findOrFail($id);
        $this->transportmode = CrmTransportMode::where('id',$this->pouch_s->transport_mode_id)->first();

        // dd($this->transportmode);

        $this->pouch_id = $id;
        $this->created_by = Auth::user()->name;
        $this->name = $this->pouch_s->name;
        $this->effectivity_date = $this->pouch_s->effectivity_date;
        $this->description = $this->pouch_s->description;
        $this->rate_type_id = $this->pouch_s->rate_type_id;
        $this->transport_mode_id = $this->transportmode->name;
        $this->booking_type_id = $this->pouch_s->booking_type_id;
        $this->apply_for_id = $this->pouch_s->apply_for_id;
        $this->pouch_type_id = $this->pouch_s->pouch_type_id;
        $this->rates = $this->pouch_s->rates;


        // dd(count($this->hie->tagHasMany()));
        foreach ($this->pouch_s->RatePouchHasMany as $a => $tags) {

            $this->rates[$a] = [
                'id' => $tags->id,
                'origin_id' => $tags->origin_id,
                'destination_id' => $tags->destination_id,
                'amount_small' => $tags->amount_small,
                'amount_medium' => $tags->amount_medium,
                'amount_large' => $tags->amount_large,
                'is_deleted' => false,
            ];

        }
        // dd($this->rates);

    }

    public function action(PouchInterface $pouch_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'update_next') {
            $response = $pouch_interface->updateValidationTab1($this->getRequest());
            if ($response['code'] == 200) {
                $this->current_tab = 2;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }elseif ($action_type == 'update_next_perview') {

            $response = $pouch_interface->updateValidationTab2($this->getRequest());
            // dd($response);

            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
                // $this->create_next_perview();
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }

    }


    public function submit(PouchInterface $pouch_interface)
    {
        // dd($this->pouch_id->);
        // $response = $pouch_interface->update($validated, $this->pouch_id);
        $response = $pouch_interface->update($this->getRequest(),$this->pouch_id,);

        // dd($response);
        // $response = $pouch_interface->create($this->getRequest());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.commercials.commercial-mgmt.pouch.index', 'close_modal', 'edit');
            $this->emitTo('crm.commercials.commercial-mgmt.pouch.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    
    public function render()
    {
        return view('livewire.crm.commercials.commercial-mgmt.pouch.edit');
    }
}
