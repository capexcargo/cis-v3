<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt;

use App\Interfaces\Crm\Commercials\AirFreight\AirFreightInterface;
use App\Models\CrmCommodityType;
use App\Models\CrmRateAirFreight;
use App\Traits\Crm\Commercials\AirFreight\AirFreightTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use AirFreightTrait, WithPagination, PopUpMessagesTrait;

    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public $create_modal = false;
    public $create_air_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $view_modal = false;
    public $reactivate_modal = false;
    public $deactivate_modal = false;
    public $approve1_modal = false;
    public $decline1_modal = false;
    public $approve2_modal = false;
    public $decline2_modal = false;
    public $air_id;
    public $final_status_id;
    public $confirmation_message;
    public $action_type;
    public $search_request;

    public $status_header_cards = [];
    public $stats;


    public function load()
    {
        $this->loadStatusHeaderCards();
    }

    public function loadStatusHeaderCards()
    {
        $status = CrmCommodityType::withCount('AirCommodity')->get();

        // dd($status);
        if (isset($status)){
            $this->status_header_cards = [
                [
                    'title' => 'Air Freight Gen.Cargo',
                    'value' => $status[0]->air_commodity_count,
                    'class' => 'bg-white border border-gray-600 shadow-sm text-gray-500',
                    'color' => 'text-blue',
                    'action' => 'stats',
                    'id' => 1
                ],
                [
                    'title' => 'Air Freight V-Cargo',
                    'value' => $status[1]->air_commodity_count,
                    'class' => 'bg-white border border-gray-600 shadow-sm text-gray-500 ',
                    'color' => 'text-blue',
                    'action' => 'stats',
                    'id' => 2
                ],
    
            ];
        }
        // dd($this->status_header_cards );
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add_rate') {
            $this->create_modal = true;
        }
        if ($action_type == 'add_air') {
            $this->create_air_modal = true;
        } 
        // elseif ($action_type == 'edit') {
        //     $this->emitTo('crm.commercials.commercial-mgmt.pouch.edit', 'edit', $data['id']);
        //     $this->air_id = $data['id'];
        //     $this->edit_modal = true;
        // } 
        elseif ($action_type == 'view') {
            $this->emitTo('crm.commercials.commercial-mgmt.view', 'view', $data['id']);
            $this->air_id = $data['id'];
            $this->view_modal = true;
        } 
        else if ($action_type == 'update_status') {
            if ($data['final_status_id'] == 2) {
                $this->reactivate_modal = true;
                $this->air_id = $data['id'];
                return;
            }
            $this->deactivate_modal = true;
            $this->air_id = $data['id'];
        } else if ($action_type == 'update_app1') {
            if ($data['final_status_id'] == 2) {
                $this->approve1_modal = true;
                $this->air_id = $data['id'];
                return;
            }
            $this->decline1_modal = true;
            $this->air_id = $data['id'];
        } else if ($action_type == 'update_app2') {
            if ($data['final_status_id'] == 2) {
                $this->approve2_modal = true;
                $this->air_id = $data['id'];
                return;
            }
            $this->decline2_modal = true;
            $this->air_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }
        if ($action_type == 'create_air') {
            $this->create_air_modal = false;
        } 
        // else if ($action_type == 'edit') {
        //     $this->edit_modal = false;
        // } 
        else if ($action_type == 'view') {
            $this->view_modal = false;
        } 
        elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        } elseif ($action_type == 'update_app1') {
            $this->approve1_modal = false;
            $this->decline1_modal = false;
        } elseif ($action_type == 'update_app2') {
            $this->approve2_modal = false;
            $this->decline2_modal = false;
        }
    }

    public function updated()
    {
        $this->resetPage();
    }

    public function updateStatus($id, $value)
    {
        // dd($value);
        $know = CrmRateAirFreight::findOrFail($id);
        $know->update([
            'final_status_id' => $value,
            'approver1_status_id' => $value,
            'approver2_status_id' => $value,
        ]);

        if ($value == 2) {

            $this->emitTo('crm.commercials.commercial-mgmt.index', 'close_modal', 'update_status');
            $this->emitTo('crm.commercials.commercial-mgmt.index', 'index');
            $this->sweetAlert('', 'Rate has been successfully approved!');
            return;

            // return redirect()->to(route('crm.commercials.commercial-mgmt.index'));
        }

        $this->emitTo('crm.commercials.commercial-mgmt.index', 'close_modal', 'update_status');
        $this->emitTo('crm.commercials.commercial-mgmt.index', 'index');
        $this->sweetAlert('', 'Rate has been successfully deactivated!');
    }

    public function updateApp1($id, $value1)
    {
        // dd($value);
        $app1 = CrmRateAirFreight::findOrFail($id);
        $app1->update([
            'approver1_status_id' => $value1,
        ]);
        
        if ($app1->approver2_status_id == 2) {
            if ($value1 != 3) {
                $app1->update([
                    'final_status_id' => $value1,
                ]);
            }
        }

        if ($value1 == 2) {

            $this->emitTo('crm.commercials.commercial-mgmt.index', 'close_modal', 'update_app1');
            $this->emitTo('crm.commercials.commercial-mgmt.index', 'index');
            $this->sweetAlert('', 'Rate Successfully Approved!');
            return;

        }elseif($app1->approver2_status_id == 3){
            $app1->update([
                'final_status_id' => $value1,
            ]);
        }

        $this->emitTo('crm.commercials.commercial-mgmt.index', 'close_modal', 'update_app1');
        $this->emitTo('crm.commercials.commercial-mgmt.index', 'index');
        $this->sweetAlert('', 'Rate Successfully Declined!');
    }

    public function updateApp2($id, $value2)
    {
        // dd($value);
        $app2 = CrmRateAirFreight::findOrFail($id);
        $app2->update([
            'approver2_status_id' => $value2,
        ]);

        if ($app2->approver1_status_id == 2) {
            // if ($value2 != 3) {
                $app2->update([
                    'final_status_id' => $value2,
                ]);
            // }
        }
        

        if ($value2 == 2) {

            $this->emitTo('crm.commercials.commercial-mgmt.index', 'close_modal', 'update_app2');
            $this->emitTo('crm.commercials.commercial-mgmt.index', 'index');
            $this->sweetAlert('', 'Rate Successfully Approved!');
            return;

        }elseif($app2->approver1_status_id == 3){
            $app2->update([
                'final_status_id' => $value2,
            ]);
        }

        $this->emitTo('crm.commercials.commercial-mgmt.index', 'close_modal', 'update_app2');
        $this->emitTo('crm.commercials.commercial-mgmt.index', 'index');
        $this->sweetAlert('', 'Rate Successfully Declined!');
    }

    public function search(AirFreightInterface $airfreight_interface)
    {
        $this->search_request = [
            'name' => $this->name,
            'description' => $this->description,
            'final_status_id' => $this->final_status_id,
        ];
    }

    public function render(AirFreightInterface $airfreight_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'stats' => $this->stats,
        ];

        $response = $airfreight_interface->index($request, $this->search_request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'air_s' => [],
                ];
        }

        return view('livewire.crm.commercials.commercial-mgmt.index', [
            'air_s' => $response['result']['air_s']
        ]);

    }
}
