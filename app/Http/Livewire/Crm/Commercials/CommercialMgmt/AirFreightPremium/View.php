<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\AirFreightPremium;

use App\Models\CrmRateAirFreightPremium;
use App\Traits\Crm\Commercials\AirFreightPremium\AirFreightPremiumTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class View extends Component
{

    use AirFreightPremiumTrait, PopUpMessagesTrait;

    public $rate_type_id;
    public $transport_mode_id;
    public $ancillary_charge_id;
    public $commodity_type_id;
    public $service_mode_id;
    public $apply_for_id;
    public $box_type_id;
    public $dates;
    public $transport;
    public $vice_versa;
    public $base_rate_id;

    public $rate_id;
    public $airp_s;
    public $transportmode;
    public $boxes = [];
    public $origin_id;
    public $vice_versa_status;


    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function mount($id)
    {
        // dd($id);

        $this->resetForm();
        $this->airp_s = CrmRateAirFreightPremium::with(['RateAirfreightPremiumHasMany' => function ($query) {
            $query->with('OriginDetails', 'DestinationDetails', 'ServiceModeReference')->where('is_primary', 1);
        }])->find($id);
        
        // dd($this->airp_s);

        if ($this->airp_s->is_vice_versa == 1) {
            $this->vice_versa_status = 'Yes';
        } else {
            $this->vice_versa_status = 'No';
        }

        if ($this->airp_s->is_vice_versa == 1) {
            $this->vice_versa_display = 'VV';
        } else {
            $this->vice_versa_display = '';
        }

    }
    public function render()
    {
        return view('livewire.crm.commercials.commercial-mgmt.air-freight-premium.view');
    }
}
