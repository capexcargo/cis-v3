<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\AirFreightPremium;

use App\Interfaces\Crm\Commercials\AirFreightPremium\AirFreightPremiumInterface;
use App\Models\CrmRateAirFreightPremium;
use App\Models\CrmTransportMode;
use App\Traits\Crm\Commercials\AirFreightPremium\AirFreightPremiumTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class CreateAirPremium extends Component
{

    use AirFreightPremiumTrait, PopUpMessagesTrait;

    public $apply_for_id;
    public $dates;
    public $transport;
    public $vice_versa;
    public $base_rate_id;

    public $amount_weight_1;
    public $amount_weight_2;
    public $amount_weight_3;
    public $amount_weight_4;

    public $commodity_type_id;
    public $transport_mode_id;
    public $transportmode;
    public $ancillary_charge_id;
    public $service_mode_id;

    public $bases = [];




    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];


    public function mount()
    {
        // dd($rate);
        // $this->rate = $rate;
        $this->bases = CrmRateAirFreightPremium::where('final_status_id','2')->get();
        // $this->pouch_s = CrmRatePouch::with('RatePouchHasMany')->get();

        $this->transportmode = CrmTransportMode::where('id', 1)->first();
        $this->transport_mode_id = $this->transportmode->name;




        $this->created_by = Auth::user()->name;
        $this->dates = date('m/d/Y');
        // $this->transport = ('rate_type_id');
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.commercials.commercial-mgmt.air-freight-premium.index', 'close_modal', 'create_air_premium');
        $this->confirmation_modal = false;
    }

    public function action(AirFreightPremiumInterface $airfreightp_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'create_next') {
            $response = $airfreightp_interface->createValidationTab1of1($this->getRequestAirP());
            if ($response['code'] == 200) {
                $this->current_tab = 2;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
        elseif ($action_type == 'create_next_perview') {

            $response = $airfreightp_interface->createValidationTab2of2($this->getRequestAirP());
            // dd($response);

            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

     


    public function submit(AirFreightPremiumInterface $air_interface)
    {
        // dd($this->getRequestbox());
        $response = $air_interface->createAirPremium($this->getRequestAirP());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.commercials.commercial-mgmt.air-freight-premium.index', 'close_modal', 'create_air_premium');
            $this->emitTo('crm.commercials.commercial-mgmt.air-freight-premium.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.crm.commercials.commercial-mgmt.air-freight-premium.create-air-premium');
    }
}
