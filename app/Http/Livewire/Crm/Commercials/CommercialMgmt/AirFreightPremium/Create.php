<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\AirFreightPremium;

use App\Interfaces\Crm\Commercials\AirFreightPremium\AirFreightPremiumInterface;
use App\Models\CrmTransportMode;
use App\Traits\Crm\Commercials\AirFreightPremium\AirFreightPremiumTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Create extends Component
{
    use AirFreightPremiumTrait, PopUpMessagesTrait;

    public $rate_type_id;
    public $apply_for_id;
    public $dates;
    public $transport;
    public $vice_versa;
    public $base_rate_id;

    public $commodity_type_id;
    public $transport_mode_id;
    public $transportmode;
    public $ancillary_charge_id;
    // public $service_mode_id;

    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];


    public function mount()
    {
        // dd($rate);
        // $this->rate = $rate;
        $this->created_by = Auth::user()->name;
        $this->dates = date('m/d/Y');
        // $this->transport = ('rate_type_id');
        $this->transportmode = CrmTransportMode::where('id', 1)->first();
        $this->transport_mode_id = $this->transportmode->name;
        // dd($this->transportmode);
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.commercials.commercial-mgmt.air-freight-premium.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function action(AirFreightPremiumInterface $airfreightP_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'create_next') {
            $response = $airfreightP_interface->createValidationTab1($this->getRequest());
            if ($response['code'] == 200) {
                $this->current_tab = 2;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
        elseif ($action_type == 'create_next_perview') {

            $response = $airfreightP_interface->createValidationTab2($this->getRequest());
            // dd($response);

            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
                // $this->create_next_perview();
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }


    public function submit(AirFreightPremiumInterface $airfreightp_interface)
    {
        // dd($this->getRequest());
        $response = $airfreightp_interface->create($this->getRequest());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.commercials.commercial-mgmt.air-freight-premium.index', 'close_modal', 'create');
            $this->emitTo('crm.commercials.commercial-mgmt.air-freight-premium.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        if (!$this->rates) {
            $this->rates[] = [
                'id' => null,
                'origin_id' => null,
                'destination_id' => null,
                'amount_weight_1' => null,
                'amount_weight_2' => null,
                'service_mode_id' => null,
                'is_deleted' => false,
            ];
        }
        return view('livewire.crm.commercials.commercial-mgmt.air-freight-premium.create');
    }
}
