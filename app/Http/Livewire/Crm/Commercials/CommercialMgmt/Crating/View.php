<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\Crating;

use App\Models\CrmRateCrating;
use App\Traits\Crm\Commercials\Crating\CratingTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class View extends Component
{
    use CratingTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public $crating_type_id;
    public $transport_mode_id;
    public $booking_type_id;
    public $apply_for_id;
    public $box_type_id;
    public $dates;
    public $transport;
    public $vice_versa;
    public $base_rate_id;

    public $crate_id;
    public $crate_s = [];
    public $crate;
    public $transportmode;
    public $boxes = [];
    public $origin_id;


    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function mount($id)
    {
        // dd($id);

        $this->resetForm();
        $this->crate_s = CrmRateCrating::find($id);

        // $this->crate_s->cratecount + 1;

        // dd($this->crate_s);
    }

    public function render()
    {
        return view('livewire.crm.commercials.commercial-mgmt.crating.view');
    }
}
