<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\Warehousing;

use App\Models\CrmRateWarehousing;
use App\Traits\Crm\Commercials\Warehousing\WarehousingTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class View extends Component
{
    use WarehousingTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public $warehousing_s;
    public $options;
    public $optionl;
    public $short;
    public $long;

    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function mount($id)
    {
        // dd($id);

        $this->resetForm();
        $this->warehousing_s = CrmRateWarehousing::with(['RateWarehousingHasMany' => function ($query) {
            $query->with('WarehousingOptionDetails');
        }])->find($id);
        
        // dd($this->warehousing_s);


    }

    public function render()
    {
        return view('livewire.crm.commercials.commercial-mgmt.warehousing.view');
    }
}
