<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\Warehousing;

use App\Interfaces\Crm\Commercials\Crating\CratingInterface;
use App\Interfaces\Crm\Commercials\Warehousing\WarehousingInterface;
use App\Models\CrmRateWarehousing;
use App\Models\CrmRateWarehousingDetails;
use App\Models\CrmWarehousingOption;
use App\Traits\Crm\Commercials\Warehousing\WarehousingTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Create extends Component
{
    use WarehousingTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $ancillary_charge_id;
    public $apply_for_id;
    public $dates;
    public $base_rate_id;

    public $rate_id;
    public $short;
    public $long;


    public $warehouse_s;
    public $options = [];
    public $optionl = [];
    public $optionss = [];



    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];


    public function mount()
    {
        // dd($rate);
        // $this->rate = $rate;
        $this->created_by = Auth::user()->name;
        $this->dates = date('m/d/Y');
        // $this->transport = ('rate_type_id');

        $this->warehouse_s = CrmWarehousingOption::get();

        foreach ($this->warehouse_s as $a => $warehouse) {
            $this->options[$a] = [
                'warehousing_option_id' => $warehouse->id,
                'option' => $warehouse->option,
                'rate_per_unit' => $warehouse->rate_per_unit,
                'amount_short_term' => null,
                'amount_long_term' => null,

            ];
        }
        // dd($this->options);
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.commercials.commercial-mgmt.warehousing.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function action(WarehousingInterface $warehousing_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'create_next') {
            $response = $warehousing_interface->createValidationTab1($this->getRequest());
            if ($response['code'] == 200) {
                $this->current_tab = 2;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($action_type == 'create_next_perview') {

            $response = $warehousing_interface->createValidationTab2($this->getRequest());
            // dd($response);

            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
                // $this->create_next_perview();
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }


    public function submit(WarehousingInterface $warehousing_interface)
    {
        // dd($this->getRequest());
        $response = $warehousing_interface->create($this->getRequest());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.commercials.commercial-mgmt.warehousing.index', 'close_modal', 'create');
            $this->emitTo('crm.commercials.commercial-mgmt.warehousing.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.crm.commercials.commercial-mgmt.warehousing.create');
    }
}
