<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\LoaMgmt;

use App\Interfaces\Crm\Commercials\Loa\LoaMgmtInterface;
use App\Traits\Crm\Commercials\LoaMgmt\LoaMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{

    use LoaMgmtTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $rate_category;
    public $approver1_id;
    public $approver2_id;


    // protected $rules = [
    //     'rate_category' => 'required',
    //     'approver1_id' => 'required',
    //     'approver2_id' => 'required',
    // ];

    // public function confirmationSubmit()
    // {
    //     $this->confirmation_modal = true;
    //     $this->validate();
    // }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.commercials.commercial-mgmt.loa-mgmt.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function action(LoaMgmtInterface $loa_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'submit2') {
            $response = $loa_interface->createValidation($this->getRequest());
            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }



    public function submit(LoaMgmtInterface $loa_mgmt_interface)
    {
        // dd($this->getRequest());

        $response = $loa_mgmt_interface->create($this->getRequest());
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.commercials.commercial-mgmt.loa-mgmt.index', 'close_modal', 'create');
            $this->emitTo('crm.commercials.commercial-mgmt.loa-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.commercials.commercial-mgmt.loa-mgmt.create');
    }
}
