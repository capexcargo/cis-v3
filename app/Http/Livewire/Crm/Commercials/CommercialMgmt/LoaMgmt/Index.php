<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\LoaMgmt;

use App\Interfaces\Crm\Commercials\Loa\LoaMgmtInterface;
use App\Traits\Crm\Commercials\LoaMgmt\LoaMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use LoaMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $confirmation_message;
    
    public $rate_category;
    public $approver1_id;
    public $approver2_id;
    public $loa_id;

    public $action_type;
    public $search_request;


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } 
        elseif ($action_type == 'edit') {
            $this->emitTo('crm.commercials.commercial-mgmt.loa-mgmt.edit', 'edit', $data['id']);
            $this->loa_id = $data['id'];
            $this->edit_modal = true;
        } 
        // else if ($action_type == 'delete') {
        //     $this->confirmation_message = "Are you sure you want to delete this SR Subcategory?";
        //     $this->delete_modal = true;
        //     $this->sr_id = $data['id'];
        // }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } 
        else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
        // elseif ($action_type == 'delete') {
        //     $this->delete_modal = false;
        // }
    }

    public function search(LoaMgmtInterface $loa_interface)
    {
        $this->search_request = [
            'rate_category' => $this->rate_category,
            'approver1_id' => $this->approver1_id,
            'approver2_id' => $this->approver2_id,
        ];
    }

    public function render(LoaMgmtInterface $loa_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $loa_interface->index($request, $this->search_request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'loa_s' => [],
                ];
        }

        return view('livewire.crm.commercials.commercial-mgmt.loa-mgmt.index', [
            'loa_s' => $response['result']['loa_s']
        ]);
    }

   
}
