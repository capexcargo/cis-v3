<?php

namespace App\Http\Livewire\Crm\Commercials\CommercialMgmt\LoaMgmt;

use App\Interfaces\Crm\Commercials\Loa\LoaMgmtInterface;
use App\Models\CrmRateLoa;
use App\Traits\Crm\Commercials\LoaMgmt\LoaMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use LoaMgmtTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $rate_category;
    public $approver1_id;
    public $approver2_id;
    public $loa_s;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->loa_s = CrmRateLoa::findOrFail($id);

        $this->rate_category = $this->loa_s->rate_category;
        $this->approver1_id = $this->loa_s->approver1_id;
        $this->approver2_id = $this->loa_s->approver2_id;
    }

    public function action(LoaMgmtInterface $loa_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'submit2') {
            $response = $loa_interface->updateValidation($this->getRequest());
            if ($response['code'] == 200) {
                $this->confirmation_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function submit(LoaMgmtInterface $loa_interface)
    {
        $response = $loa_interface->update($this->getRequest(), $this->loa_s->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.commercials.commercial-mgmt.loa-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('crm.commercials.commercial-mgmt.loa-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.crm.commercials.commercial-mgmt.loa-mgmt.edit');
    }
}
