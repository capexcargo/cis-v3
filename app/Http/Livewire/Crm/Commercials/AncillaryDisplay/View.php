<?php

namespace App\Http\Livewire\Crm\Commercials\AncillaryDisplay;

use App\Models\Crm\CrmAncillaryDisplay;
use App\Traits\Crm\Commercials\AncillaryDisplay\AncillaryDisplayTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class View extends Component
{
    use AncillaryDisplayTrait, WithPagination, PopUpMessagesTrait;

    public function mount($id)
    {
        $this->resetForm();
        $this->ancillarydetails = CrmAncillaryDisplay::with(['AncillaryDisplayDetails' => function ($query) {
            $query->with('AncillaryCharge');
        }])->find($id);

        // foreach ($ancillarydetails->AncillaryDisplayDetails as $a => $ancillarydetail) {
        //     // dd($ancillarydetail->id);
        //     $this->ancillarycharges[$a] = [
        //         'id' => $ancillarydetail->id,
        //         'ancillary_charge_id' => $ancillarydetail->ancillary_charge_id,
        //         'description' => $ancillarydetail->AncillaryCharge->description,
        //         'is_deleted' => false,
        //     ];
        // }

        $this->name = $this->ancillarydetails->name;
    }

    public function render()
    {
        return view('livewire.crm.commercials.ancillary-display.view');
    }
}
