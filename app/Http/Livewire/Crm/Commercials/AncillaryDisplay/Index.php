<?php

namespace App\Http\Livewire\Crm\Commercials\AncillaryDisplay;

use App\Interfaces\Crm\Commercials\AncillaryDisplay\AncillaryDisplayMgmtInterface;
use App\Models\Crm\CrmAncillaryDisplay;
use App\Traits\Crm\Commercials\AncillaryDisplay\AncillaryDisplayTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use AncillaryDisplayTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'create') {
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('ancillarydisplay_mgmt_mount', $data['id']);
            $this->edit_modal = true;
            $this->ancillarydisplay_mgmt_id = $data['id'];
        } else if ($action_type == 'deactivate') {

            if ($data['status'] == 2) {
                // dd($data['status']);
                $this->deactivate_modal = true;
                $this->ancillarydisplay_mgmt_id = $data['id'];
                return;
            }
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->ancillarydisplay_mgmt_id = $data['id'];
                return;
            }
        } elseif ($action_type == 'view') {
            $this->emitTo('crm.commercials.ancillary-display.view', 'view', $data['id']);
            $this->ancillarydisplay_mgmt_id = $data['id'];
            $this->view_modal = true;
        }
    }

    public function reactivate_stat($id, $value)
    {
        // dd($value);
        $know = CrmAncillaryDisplay::findOrFail($id);
        $know->update([
            'status' => $value,
        ]);

        if ($value == 1) {

            $this->emitTo('crm.commercials.ancillary-display.index', 'close_modal', 'deactivate');
            $this->emitTo('crm.commercials.ancillary-display.index', 'index');
            $this->sweetAlert('', 'Ancillary Charge item has been Successfully Reactivated!');
            return;
        }

        if ($value == 2) {
            $this->emitTo('crm.commercials.ancillary-display.index', 'close_modal', 'deactivate');
            $this->emitTo('crm.commercials.ancillary-display.index', 'index');
            $this->sweetAlert('', 'Ancillary Charge item has been successfully deactivated!');
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'deactivate') {
            $this->deactivate_modal = false;
            $this->reactivate_modal = false;
        }
    }

    public function render(AncillaryDisplayMgmtInterface $ancillary_display_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $ancillary_display_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'ancillary_charges_displays' => [],
                ];
        }

        // dd($response['result']['ancillary_charges_displays']);
        return view('livewire.crm.commercials.ancillary-display.index', [
            'ancillary_charges_displayss' => $response['result']['ancillary_charges_displays']
        ]);
    }
}
