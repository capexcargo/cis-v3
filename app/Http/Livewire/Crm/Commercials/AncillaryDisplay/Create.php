<?php

namespace App\Http\Livewire\Crm\Commercials\AncillaryDisplay;

use App\Interfaces\Crm\Commercials\AncillaryDisplay\AncillaryDisplayMgmtInterface;
use App\Traits\Crm\Commercials\AncillaryDisplay\AncillaryDisplayTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Create extends Component
{
    use AncillaryDisplayTrait, WithPagination, PopUpMessagesTrait;

    public function confirmationSubmit(AncillaryDisplayMgmtInterface $ancillary_display_interface)
    {
        $response = $ancillary_display_interface->createValidation($this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    // dd($response['result']);
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.commercials.ancillary-display.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function submit(AncillaryDisplayMgmtInterface $ancillary_display_interface)
    {
        // dd($this->getRequest());
        $response = $ancillary_display_interface->create($this->getRequest());
        if ($response['code'] == 200) {

            $this->resetForm();
            $this->emitTo('crm.commercials.ancillary-display.index', 'close_modal', 'create');
            $this->emitTo('crm.commercials.ancillary-display.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        if (!$this->ancillarycharges) {
            $this->ancillarycharges[] = [
                'id' => null,
                'ancillary_charge_id' => null,
                'description' => null,
                'is_deleted' => false,
            ];
        }
        return view('livewire.crm.commercials.ancillary-display.create');
    }
}
