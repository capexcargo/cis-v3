<?php

namespace App\Http\Livewire\Crm\Commercials\AncillaryDisplay;

use App\Interfaces\Crm\Commercials\AncillaryDisplay\AncillaryDisplayMgmtInterface;
use App\Models\Crm\CrmAncillaryDisplay;
use App\Models\Crm\CrmAncillaryDisplayDetails;
use App\Traits\Crm\Commercials\AncillaryDisplay\AncillaryDisplayTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use AncillaryDisplayTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['ancillarydisplay_mgmt_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $ancillarydetails = CrmAncillaryDisplay::with(['AncillaryDisplayDetails' => function ($query) {
            $query->with('AncillaryCharge');
        }])->where('id', $id)->first();
        $this->ancillarydisplay_mgmt_id = $id;
        // dd($ancillarydetails->AncillaryDisplayDetails);
        foreach ($ancillarydetails->AncillaryDisplayDetails as $a => $ancillarydetail) {
            // dd($ancillarydetail->id);
            $this->ancillarycharges[$a] = [
                'id' => $ancillarydetail->id,
                'ancillary_charge_id' => $ancillarydetail->ancillary_charge_id,
                'description' => $ancillarydetail->AncillaryCharge->description,
                'is_deleted' => false,
            ];
        }

        $this->name = $ancillarydetails->name;

        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->ancillarycharges as $a => $ancillarycharge) {

            if ($this->ancillarycharges[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->ancillarycharges[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->max = max($getkeyarray);
        $this->countarr = $countarray;
    }

    public function confirmationSubmit(AncillaryDisplayMgmtInterface $ancillary_display_interface)
    {

        $response = $ancillary_display_interface->updateValidation($this->getRequest(), $this->ancillarydisplay_mgmt_id);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.commercials.ancillary-display.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function submit(AncillaryDisplayMgmtInterface $ancillary_display_interface)
    {
        $response = $ancillary_display_interface->update($this->getRequest(), $this->ancillarydisplay_mgmt_id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.commercials.ancillary-display.index', 'close_modal', 'edit');
            $this->emitTo('crm.commercials.ancillary-display.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.commercials.ancillary-display.edit');
    }
}
