<?php

namespace App\Http\Livewire\Crm\Commercials\Ancillary;

use App\Interfaces\Crm\Commercials\Ancillary\AncillaryMgmtInterface;
use App\Traits\Crm\Commercials\Ancillary\AncillaryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Create extends Component
{

    use AncillaryTrait, WithPagination, PopUpMessagesTrait;

    public function confirmationSubmit(AncillaryMgmtInterface $ancillary_interface)
    {
        $response = $ancillary_interface->createValidation($this->getRequest());
        // dd($response);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.commercials.ancillary.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function submit(AncillaryMgmtInterface $ancillary_interface)
    {
        // dd($this->getRequest());
        $response = $ancillary_interface->create($this->getRequest());
        if ($response['code'] == 200) {

            $this->resetForm();
            $this->emitTo('crm.commercials.ancillary.index', 'close_modal', 'create');
            $this->emitTo('crm.commercials.ancillary.index', 'index');
            $this->sweetAlert('', $response['message']);
            
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render()
    {
        if (!$this->ancillarycharges) {
            $this->ancillarycharges[] = [
                'id' => null,
                'name' => null,
                'charges_amount' => null,
                'charges_rate' => null,
                'description' => null,
                'is_deleted' => false,
            ];
        }

        return view('livewire.crm.commercials.ancillary.create');
    }
}
