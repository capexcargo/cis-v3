<?php

namespace App\Http\Livewire\Crm\Commercials\Ancillary;

use App\Interfaces\Crm\Commercials\Ancillary\AncillaryMgmtInterface;
use App\Models\Crm\CrmAncillary;
use App\Traits\Crm\Commercials\Ancillary\AncillaryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use AncillaryTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'create') {
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('ancillary_mgmt_mount', $data['id']);
            $this->edit_modal = true;
            $this->ancillary_mgmt_id = $data['id'];
        } else if ($action_type == 'deactivate') {
            if ($data['status'] == 2) {
                $this->deactivate_modal = true;
                $this->ancillary_mgmt_id = $data['id'];
                return;
            }
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->ancillary_mgmt_id = $data['id'];
                return;
            }
        }
    }

    public function reactivate_stat($id, $value)
    {
        // dd($value);
        $know = CrmAncillary::findOrFail($id);
        $know->update([
            'status' => $value,
        ]);

        if ($value == 1) {

            $this->emitTo('crm.commercials.ancillary.index', 'close_modal', 'deactivate');
            $this->emitTo('crm.commercials.ancillary.index', 'index');
            $this->sweetAlert('', 'Ancillary Charge Successfully Reactivated!');
            return;
        }

        if ($value == 2) {
            $this->emitTo('crm.commercials.ancillary.index', 'close_modal', 'deactivate');
            $this->emitTo('crm.commercials.ancillary.index', 'index');
            $this->sweetAlert('', 'Ancillary Charge Successfully Deactivated!');
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'deactivate') {
            $this->deactivate_modal = false;
            $this->reactivate_modal = false;
        }
    }

    public function render(AncillaryMgmtInterface $ancillary_interface)
    {


        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $ancillary_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'ancillary_charges' => [],
                ];
        }
        return view('livewire.crm.commercials.ancillary.index', [
            'ancillary_charges' => $response['result']['ancillary_charges']
        ]);
    }
}
