<?php

namespace App\Http\Livewire\Crm\Commercials\Ancillary;

use App\Interfaces\Crm\Commercials\Ancillary\AncillaryMgmtInterface;
use App\Models\Crm\CrmAncillary;
use App\Traits\Crm\Commercials\Ancillary\AncillaryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use AncillaryTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['ancillary_mgmt_mount' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->ancillary = CrmAncillary::findOrFail($id);

        $this->name = $this->ancillary->name;
        $this->charges_amount = $this->ancillary->charges_amount;
        $this->charges_rate = $this->ancillary->charges_rate;
        $this->description = $this->ancillary->description;
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.commercials.ancillary.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(AncillaryMgmtInterface $ancillary_interface)
    {
        $response = $ancillary_interface->updateValidation($this->getRequestUpdate(), $this->ancillary->id);
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function submit(AncillaryMgmtInterface $ancillary_interface)
    {
        $response = $ancillary_interface->update($this->getRequestUpdate(), $this->ancillary->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.commercials.ancillary.index', 'close_modal', 'edit');
            $this->emitTo('crm.commercials.ancillary.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.commercials.ancillary.edit');
    }
}
