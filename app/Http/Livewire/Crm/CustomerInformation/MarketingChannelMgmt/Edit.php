<?php

namespace App\Http\Livewire\Crm\CustomerInformation\MarketingChannelMgmt;

use App\Interfaces\Crm\CustomerInformation\MarketingChannelInterface;
use App\Models\MarketingChannel;
use App\Traits\Crm\CustomerInformation\MarketingChannelTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    public $confirmation_modal;
    public $marketing;
    public $marketing_channel;
    
    use MarketingChannelTrait, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->marketing_channel = MarketingChannel::findOrFail($id);

        $this->marketing = $this->marketing_channel->name;
        // $this->category = $this->employment_category_type->employment_category_id;
    }

    public function confirmationSubmit(MarketingChannelInterface $marketing_channel)
    {

        $response = $marketing_channel->updateValidation($this->getRequest(),$this->marketing_channel->id,);

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(MarketingChannelInterface $marketing_channel)
    {
        $response = $marketing_channel->update($this->getRequest(), $this->marketing_channel->id);
        
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.customer-information.marketing-channel-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('crm.customer-information.marketing-channel-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.customer-information.marketing-channel-mgmt.edit');
    }
}
