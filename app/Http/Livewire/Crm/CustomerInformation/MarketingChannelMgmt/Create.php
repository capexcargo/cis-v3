<?php

namespace App\Http\Livewire\Crm\CustomerInformation\MarketingChannelMgmt;

use App\Interfaces\Crm\CustomerInformation\MarketingChannelInterface;
use App\Traits\Crm\CustomerInformation\MarketingChannelTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use MarketingChannelTrait, PopUpMessagesTrait;
    
    public $confirmation_modal;
    public $marketing;


    public function confirmationSubmit(MarketingChannelInterface $Marketing_Channel_interface)
    {
        $response = $Marketing_Channel_interface->createValidation($this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {

            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {

                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    // dd($response['result']);
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.customer-information.marketing-channel-mgmt.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function submit(MarketingChannelInterface $Marketing_Channel_interface)
    {
        // dd($this->getRequest());
        
        $response = $Marketing_Channel_interface->create($this->getRequest());
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.customer-information.marketing-channel-mgmt.index', 'close_modal', 'create');
            $this->emitTo('crm.customer-information.marketing-channel-mgmt.index', 'index');
            $this->sweetAlert('',$response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.customer-information.marketing-channel-mgmt.create');
    }
}
