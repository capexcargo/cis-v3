<?php

namespace App\Http\Livewire\Crm\CustomerInformation\MarketingChannelMgmt;

use App\Interfaces\Crm\CustomerInformation\MarketingChannelInterface;
use App\Traits\Crm\CustomerInformation\MarketingChannelTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use MarketingChannelTrait, WithPagination, PopUpMessagesTrait;

    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $marketing_id;
    public $confirmation_message;
    public $action_type;


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('crm.customer-information.marketing-channel-mgmt.edit', 'edit', $data['id']);
            $this->marketing_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this Marketing Channel?";
            $this->delete_modal = true;
            $this->marketing_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }

    public function confirm(MarketingChannelInterface $marketing_channel_mgmt_interface)
    {
        if ($this->action_type == "delete") {
            $response = $marketing_channel_mgmt_interface->destroy($this->marketing_id);
            $this->emitTo('crm.customer-information.marketing-channel-mgmt.index', 'close_modal', 'delete');
            $this->emitTo('crm.customer-information.marketing-channel-mgmt.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('crm.customer-information.marketing-channel-mgmt.index', 'close_modal', 'delete');
            $this->emitTo('crm.customer-information.marketing-channel-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(MarketingChannelInterface $Marketing_Channel_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $Marketing_Channel_interface->index($request);

        // dd($response);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'marketing_channels' => [],
                ];
        }
        // dd($response['result']['marketing_channelss']);

        return view('livewire.crm.customer-information.marketing-channel-mgmt.index', [
            'marketing_channels' => $response['result']['marketing_channels']
        ]);
    }
}
