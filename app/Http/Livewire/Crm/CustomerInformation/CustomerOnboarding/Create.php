<?php

namespace App\Http\Livewire\Crm\CustomerInformation\CustomerOnboarding;

use App\Interfaces\Crm\CustomerInformation\CustomerOnboardingInterface;
use App\Models\Checkmobitest;
use App\Models\Crm\CustomerInformation\CrmCusInformationInitiatedCall;
use App\Traits\Crm\CustomerInformation\CustomerOnboardingTrait;
use App\Traits\PopUpMessagesTrait;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Log;

class Create extends Component
{
    use CustomerOnboardingTrait, PopUpMessagesTrait, ResponseTrait;

    public $customer_onboarding_id;
    public $message_modal = false;

    protected $listeners = [
        'generate_customer_no' => 'generateCustomerNo',
    ];

    public function mount()
    {
        $this->addEmails();
        $this->addTelephoneNumbers();
        $this->addMobileNumbers();
        $this->addAddresses();

        $this->addContactPersons();

        $this->account_type = 1;

        $this->contact_owner = Auth::user()->id;
    }

    public function accountType($type)
    {
        $this->account_type = $type;
    }

    public $buttonText = 'Verify Now';
    
    // public function verifyNumber($i, CustomerOnboardingInterface $customer_onboarding_interface)
    // {
    //     if (empty($this->mobile_numbers[$i]['mobile'])) {
    //         $errors = $this->addError('mobile_numbers.' . $i . '.mobile', 'Please enter a mobile number');
    //         $mobileNumberError = $errors->first('mobile_numbers.' . $i . '.mobile');
    //         $mobileNumberError;
    //         return;
    //     } else {
    //         $this->resetErrorBag('mobile_numbers.' . $i . '.mobile');
    //     }

    //     $this->buttonText = ($this->buttonText == 'Verify Now') ? 'Check' : 'Verify Now';
    //     $found = false;
    
    //     foreach ($this->mobile_numbers as $a => $item) {
    //         if (isset($item['mobile']) && $item['mobile'] === $this->mobile_numbers[$i]['mobile'] && $item['status'] != null) {
    //             $found = true;
    //             break;
    //         }
    //     }
    
    //     if ($found) {
    //         if ($this->mobile_numbers[$i]['status'] == null) {
    //             $errors = $this->addError('mobile_numbers.' . $i . '.mobile', 'Mobile number is already verified');
    //             $mobileNumberError = $errors->first('mobile_numbers.' . $i . '.mobile');
    //             $mobileNumberError;
    //             return;
    //         }
    //     } else {
    //         try {
    //             $number = '+63' . substr(str_replace('-', '', preg_replace('/[^0-9-]/', '', $this->mobile_numbers[$i]['mobile'])), 1);
    
    //             $request_data = [
    //                 'number' => $number,
    //                 'customer_number' => $this->customer_number,
    //             ];
    
    //             $existingRecord = CrmCusInformationInitiatedCall::where([
    //                 'customer_number' => $request_data['customer_number'],
    //                 'mobile_number' => $request_data['number'],
    //             ])->first();
    
    //             if(!$existingRecord) {
    //                 CrmCusInformationInitiatedCall::updateOrCreate(
    //                     [
    //                         'customer_number' => $request_data['customer_number'],
    //                         'mobile_number' => $request_data['number'],
    //                     ],
    //                     [
    //                         'customer_number' => $request_data['customer_number'],
    //                         'mobile_number' => $request_data['number'],
    //                         'CallStatus' => "",
    //                         'CallSid' => "",
    //                     ]
    //                 );
    
<<<<<<< HEAD
    //                 $response = $customer_onboarding_interface->missedcall($request_data);
    //                 sleep(7);
    //                 return $this->responseAPI($response['code'], $response['message'], $response['result']);
    //             } else {
    //                 $updatedStatus = CrmCusInformationInitiatedCall::where('mobile_number', $request_data['number'])->value('CallStatus');
    //                 $this->mobile_numbers[$i]['status'] = $updatedStatus;

    //                 if ($existingRecord->CallStatus !== 'hangup' && $existingRecord->CallStatus !== 'ringing' && $existingRecord->CallStatus !== 'answer') {
    //                     $errors = $this->addError('mobile_numbers.' . $i . '.mobile', 'Mobile number not found.');
    //                     $mobileNumberError = $errors->first('mobile_numbers.' . $i . '.mobile');
    //                     $mobileNumberError;
    //                     return;
    //                 } else{
    //                     $this->resetErrorBag('mobile_numbers.' . $i . '.mobile');
    //                 }
    //             }
    //         } catch (\Exception $e) {
    //             return $this->responseAPI(500, 'Error processing webhook: ', $e->getMessage());
    //         }
    //     }
    // }
=======
                    $response = $customer_onboarding_interface->missedcall($request_data);
                    sleep(3);
                    return $this->responseAPI($response['code'], $response['message'], $response['result']);
                } else {
                    $updatedStatus = CrmCusInformationInitiatedCall::where('mobile_number', $request_data['number'])->value('CallStatus');
                    $this->mobile_numbers[$i]['status'] = $updatedStatus;

                    if ($existingRecord->CallStatus !== 'hangup' && $existingRecord->CallStatus !== 'ringing' && $existingRecord->CallStatus !== 'answer') {
                        $errors = $this->addError('mobile_numbers.' . $i . '.mobile', 'Mobile number not found.');
                        $mobileNumberError = $errors->first('mobile_numbers.' . $i . '.mobile');
                        $mobileNumberError;
                        return;
                    } else{
                        $this->resetErrorBag('mobile_numbers.' . $i . '.mobile');
                    }
                }
            } catch (\Exception $e) {
                return $this->responseAPI(500, 'Error processing webhook: ', $e->getMessage());
            }
        }
    }
>>>>>>> 42712ec72d0e1f642bdfe6d91b3f440d11140c3e

    public $buttonTextCont = 'Verify Now';

    // public function verifyContactPersonNumber($i, $x, CustomerOnboardingInterface $customer_onboarding_interface)
    // {
    //     if (empty($this->contact_persons[$i]['mobile_numbers'][$x]['mobile'])) {
    //         $errors = $this->addError('contact_persons.' . $i . '.mobile_numbers.' . $x . '.mobile', 'Please enter a mobile number');
    //         $mobileNumberError = $errors->first('contact_persons.' . $i . '.mobile_numbers.' . $x . '.mobile');
    //         return;
    //     } else {
    //         $this->resetErrorBag('contact_persons.' . $i . '.mobile_numbers.' . $x . '.mobile');
    //     }

    //     $this->buttonTextCont = ($this->buttonTextCont == 'Verify Now') ? 'Check' : 'Verify Now';
    //     $found_mn = false;
    //     $found_cp_mn = false;
        
    //     $str_replace_num = str_replace('-', '', $this->contact_persons[$i]['mobile_numbers'][$x]['mobile']);
        
    //     foreach ($this->mobile_numbers as $a => $item) {
    //         if (isset($item['mobile']) && ($item['mobile'] === $this->contact_persons[$i]['mobile_numbers'][$x]['mobile'] || $item['mobile'] === $str_replace_num) && $item['status'] != null) {
    //             $found_mn = true;
    //             break;
    //         }
    //     }
        
    //     foreach ($this->contact_persons[$i]['mobile_numbers'] as $a => $item) {
    //         if (isset($item['mobile']) && ($item['mobile'] === $this->contact_persons[$i]['mobile_numbers'][$x]['mobile'] || $item['mobile'] === $str_replace_num) && $item['status'] != null) {
    //             $found_cp_mn = true;
    //             break;
    //         }
    //     }
        
    //     if ($found_mn || $found_cp_mn) {
    //         if ($this->contact_persons[$i]['mobile_numbers'][$x]['status'] == null) {
    //             $errors = $this->addError('contact_persons.' . $i . '.mobile_numbers.' . $x . '.mobile', 'Mobile number is already verified');
    //             $mobileNumberError = $errors->first('contact_persons.' . $i . '.mobile_numbers.' . $x . '.mobile');
    //             return;
    //         }
    //     } else {

    //         try {
    //             $number = '+63' . substr(str_replace('-', '', preg_replace('/[^0-9-]/', '', $this->contact_persons[$i]['mobile_numbers'][$x]['mobile'])), 1);
            
    //             $request_data = [
    //                 'number' => $number,
    //                 'customer_number' => $this->customer_number,
    //             ];
            
    //             $existingContactRecord = CrmCusInformationInitiatedCall::where([
    //                 'customer_number' => $request_data['customer_number'],
    //                 'mobile_number' => $request_data['number'],
    //             ])->first();
            
    //             if (!$existingContactRecord) {
    //                 CrmCusInformationInitiatedCall::updateOrCreate(
    //                     [
    //                         'customer_number' => $request_data['customer_number'],
    //                         'mobile_number' => $request_data['number'],
    //                     ],
    //                     [
    //                         'customer_number' => $request_data['customer_number'],
    //                         'mobile_number' => $request_data['number'],
    //                         'CallStatus' => "",
    //                         'CallSid' => "",
    //                     ]
    //                 );
            
<<<<<<< HEAD
    //                 $response = $customer_onboarding_interface->missedcall($request_data);
    //                 sleep(7);
    //                 return $this->responseAPI($response['code'], $response['message'], $response['result']);
    //             } else {
    //                 $updatedStatus = CrmCusInformationInitiatedCall::where('mobile_number', $request_data['number'])->value('CallStatus');
    //                 $this->contact_persons[$i]['mobile_numbers'][$x]['status'] =  $updatedStatus;
=======
                    $response = $customer_onboarding_interface->missedcall($request_data);
                    sleep(3);
                    return $this->responseAPI($response['code'], $response['message'], $response['result']);
                } else {
                    $updatedStatus = CrmCusInformationInitiatedCall::where('mobile_number', $request_data['number'])->value('CallStatus');
                    $this->contact_persons[$i]['mobile_numbers'][$x]['status'] =  $updatedStatus;
>>>>>>> 42712ec72d0e1f642bdfe6d91b3f440d11140c3e
                    
    //                 if ($existingContactRecord->CallStatus !== 'hangup' && $existingContactRecord->CallStatus !== 'ringing' && $existingContactRecord->CallStatus !== 'answer') {
    //                     $errors = $this->addError('contact_persons.' . $i . '.mobile_numbers.' . $x . '.mobile', 'Mobile number not found.');
    //                     $mobileNumberError = $errors->first('contact_persons.' . $i . '.mobile_numbers.' . $x . '.mobile');
    //                     $mobileNumberError;
    //                     return;
    //                 } else{
    //                     $this->resetErrorBag('contact_persons.' . $i . '.mobile_numbers.' . $x . '.mobile');
    //                 }
    //             }
    //         } catch (\Exception $e) {
    //             return $this->responseAPI(500, 'Error processing webhook: ', $e->getMessage());
    //         }
    //     }
    // }

    public function retrieveStatus()
    {
    }

    public function validateEmail()
    {
        $rules['emails.*.email'] = 'required|unique:crm_email_address_list,email|email|regex:/\./';

        $validated = $this->validate(
            $rules,
            [
                'emails.*.email.required' => 'The email address field is required.',
                'emails.*.email.email' => 'Please enter a valid email address.',
                'emails.*.email.regex' => 'Please enter a valid email address.',
                'emails.*.email.unique' => 'This email address is already exists in our CDM.',
            ]
        );
    }

    public function confirmationSubmit()
    {
        $this->account_type = $this->account_type ?? 1;

        if ($this->account_type == 1) {
            $rules = [
                'account_type' => 'required',
                'customer_number' => 'required',
                'first_name' => 'required',
                'middle_name' => 'nullable',
                'last_name' => 'required',

                'life_stage' => 'required',
                'contact_owner' => 'required',
                'marketing_channel' => 'required',
                'notes' => 'nullable',
            ];

            foreach ($this->emails as $i => $email) {
                $rules['emails.' . $i . '.email'] = 'nullable|email|regex:/\./';
                $rules['emails.' . $i . '.default'] = 'nullable';
            }
        } else {
            $rules = [
                'account_type' => 'required',
                'customer_number' => 'required',
                'company_name' => 'required',
                'company_industry' => 'required',
                'company_website_soc_med' => 'nullable',
                'rate_name' => 'nullable',
                'is_mother_account' => 'required',
                // 'tin' => 'required',
                'tin' => 'required|unique:crm_customer_information,tin',
                'company_anniversary' => 'required',
            ];

            if ($this->is_mother_account == 1 || $this->is_mother_account == null) {
                $rules['associate_with'] = 'nullable';
            } else {
                $rules['associate_with'] = 'required';
            }

            foreach ($this->emails as $i => $email) {
                // $rules['emails.' . $i . '.email'] = 'required|email|regex:/\./|ends_with:gmail.com,yahoo.com';
                $rules['emails.' . $i . '.email'] = 'required|unique:crm_email_address_list,email|email|regex:/\./';
            }
        }

        foreach ($this->telephone_numbers as $i => $telephone) {
            $rules['telephone_numbers.' . $i . '.telephone'] = 'nullable';
        }

        foreach ($this->mobile_numbers as $i => $mobile) {
            $rules['mobile_numbers.' . $i . '.mobile'] = 'required|unique:crm_mobile_list,mobile';

            $this->mobile_numbers[$i]['mobile'] = preg_replace("/[^0-9]/", '', $this->mobile_numbers[$i]['mobile']);
        }

        foreach ($this->addresses as $i => $address) {
            $rules['addresses.' . $i . '.address_type'] = 'required';
            if ($this->addresses[$i]['address_type'] == 2) {
                $rules['addresses.' . $i . '.country'] = 'required';
            }
            $rules['addresses.' . $i . '.address_line1'] = 'required';
            $rules['addresses.' . $i . '.address_line2'] = 'nullable';
            $rules['addresses.' . $i . '.state_province'] = 'required';
            $rules['addresses.' . $i . '.city_municipality'] = 'required';
            if ($this->addresses[$i]['address_type'] == 1) {
                $rules['addresses.' . $i . '.barangay'] = 'required';
            }
            $rules['addresses.' . $i . '.postal_code'] = 'required';
            $rules['addresses.' . $i . '.address_label'] = 'required';
        }

        $validated = $this->validate(
            $rules,
            [
                'is_mother_account.required' => 'Please choose account.',
                'tin.unique' => 'This TIN (Tax Identification Number) already exists in our CDM.',

                'emails.*.email.required' => 'The company email address field is required.',
                'emails.*.email.email' => 'Please enter a valid email address.',
                'emails.*.email.regex' => 'Please enter a valid email address.',
                'emails.*.email.unique' => 'This email address is already exists in our CDM.',
                // 'emails.*.email.ends_with' => 'Please enter a valid email address.',
                // 'telephone_numbers.*.telephone.required' => 'The Telephone Number field is required.',
                'mobile_numbers.*.mobile.required' => 'The Mobile Number field is required.',
                'mobile_numbers.*.mobile.unique' => 'This mobile number is already exists in our CDM.',

                'addresses.*.address_type.required' => 'The Address Type is required.',
                'addresses.*.country.required' => 'The Country field is required.',
                'addresses.*.address_line1.required' => 'The Address Line1 field is required.',
                // 'addresses.*.address_line2.required' => 'The Address Line2 field is required.',
                'addresses.*.state_province.required' => 'The State/Province field is required.',
                'addresses.*.city_municipality.required' => 'The City/Municipality field is required.',
                'addresses.*.barangay.required' => 'The Barangay field is required.',
                'addresses.*.postal_code.required' => 'The Postal Code field is required.',
                'addresses.*.address_label.required' => 'Select Address Label.',
            ]
        );
        if ($this->account_type == 1) {
            $this->confirmation_modal = true;
        } else {
            $this->current_tab = 2;
        }
    }

    public function submitCorpContactPerson()
    {
        $rules = [
            'con_per_life_stage' => 'required',
            'con_per_contact_owner' => 'required',
            'con_per_marketing_channel' => 'required',
            'con_per_notes' => 'nullable',
        ];

        foreach ($this->contact_persons as $i => $position) {
            $rules['contact_persons.' . $i . '.first_name'] = 'required';
            $rules['contact_persons.' . $i . '.middle_name'] = 'nullable';
            $rules['contact_persons.' . $i . '.last_name'] = 'required';
            $rules['contact_persons.' . $i . '.position'] = 'required';

            foreach ($this->contact_persons[$i]['emails'] as $x => $email) {
                $rules['contact_persons.' . $i . '.emails.' . $x . '.email'] = 'required|unique:crm_email_address_list,email|email|regex:/\./';
            }

            foreach ($this->contact_persons[$i]['telephone_numbers'] as $x => $email) {
                $rules['contact_persons.' . $i . '.telephone_numbers.' . $x . '.telephone'] = 'nullable'; //|regex:/^[0-9]{10}$/';
            }

            foreach ($this->contact_persons[$i]['mobile_numbers'] as $x => $email) {
                $rules['contact_persons.' . $i . '.mobile_numbers.' . $x . '.mobile'] = 'required|unique:crm_mobile_list,mobile';

                $this->contact_persons[$i]['mobile_numbers'][$x]['mobile'] = preg_replace("/[^0-9]/", '', $this->contact_persons[$i]['mobile_numbers'][$x]['mobile']);
            }


            $rules['contact_persons.' . $i . '.default'] = 'required';
        }

        $validated = $this->validate(
            $rules,
            [
                'con_per_life_stage.required' => 'The Life Stage field is required.',
                'con_per_contact_owner.required' => 'The Contact Owner field is required.',
                'con_per_marketing_channel.required' => 'The Marketing Channel field is required.',

                'contact_persons.*.first_name.required' => 'The First Name field is required.',
                'contact_persons.*.last_name.required' => 'The Last Name field is required.',
                'contact_persons.*.position.required' => 'The Position field is required.',

                'contact_persons.*.emails.*.email.required' => 'The Email field is required.',
                'contact_persons.*.emails.*.email.email' => 'Please enter a valid email address.',
                'contact_persons.*.emails.*.email.regex' => 'Please enter a valid email address.',
                'contact_persons.*.emails.*.email.unique' => 'This email address is already exists in our CDM.',

                'contact_persons.*.mobile_numbers.*.mobile.required' => 'The Mobile Number field is required.',
                'contact_persons.*.mobile_numbers.*.mobile.unique' => 'This mobile number is already exists in our CDM.',
                // 'contact_persons.*.telephone_numbers.*.telephone.regex' => 'The Telephone Number format is invalid.',
            ]
        );

        $this->current_tab = 2;
        $this->confirmation_modal = true;
    }


    public function submit(CustomerOnboardingInterface $customer_onboarding_interface)
    {

        $response = $customer_onboarding_interface->create($this->getRequest(), false);

        if ($response['code'] == 200) {
            $this->resetForm();

            $this->addEmails();
            $this->addTelephoneNumbers();
            $this->addMobileNumbers();
            $this->addAddresses();

            $this->addContactPersons();

            foreach ($this->emails as $i => $email) {
                if (count($this->emails) <= 1) {
                    $this->emails[$i]['default'] = true;
                }
            }

            foreach ($this->mobile_numbers as $i => $mobile_number) {
                if (count($this->mobile_numbers) <= 1) {
                    $this->mobile_numbers[$i]['default'] = true;
                }
            }

            foreach ($this->telephone_numbers as $i => $telephone_number) {
                if (count($this->telephone_numbers) <= 1) {
                    $this->telephone_numbers[$i]['default'] = true;
                }
            }

            foreach ($this->addresses as $i => $address) {
                if (count($this->addresses) <= 1) {
                    $this->addresses[$i]['default'] = true;
                }
            }

            foreach ($this->contact_persons as $i => $position) {
                if (count($this->contact_persons) <= 1) {
                    $this->contact_persons[$i]['default'] = true;
                }
            }

            // $this->sweetAlert('', $response['message']);
            $this->message_modal = true;
            $this->customer_onboarding_id = $response['result']['customer_onboarding_id'];

            // return redirect()->route('crm.customer-information.customer-data-mgmt.index', ['id' => $this->customer_onboarding_id, 'type' => $this->account_type]);
        } elseif ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function okAction($id)
    {
        $this->emitTo('crm.customer-information.customer-onboarding.index', 'close_modal', 'create');
        $this->emitTo('crm.customer-information.customer-onboarding.index', 'index');

        return redirect()->route('crm.customer-information.customer-data-mgmt.index', ['id' => $id, 'type' => $this->account_type]);
    }

    public function cancel()
    {
        $this->resetForm();
        $this->resetErrorBag();

        $this->addEmails();
        $this->addTelephoneNumbers();
        $this->addMobileNumbers();
        $this->addAddresses();

        $this->addContactPersons();

        $this->emit('generate_customer_no');
        $this->emitTo('crm.customer-information.customer-onboarding.index', 'close_modal', 'create');
    }

    public function render()
    {
        return view('livewire.crm.customer-information.customer-onboarding.create');
    }
}
