<?php

namespace App\Http\Livewire\Crm\CustomerInformation\CustomerOnboarding;

use App\Interfaces\Crm\CustomerInformation\CustomerOnboardingInterface;
use App\Traits\Crm\CustomerInformation\CustomerOnboardingTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Index extends Component
{
    use CustomerOnboardingTrait, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->emit('generate_customer_no');
            $this->create_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }
    }

    public function search(CustomerOnboardingInterface $customer_onboarding_interface)
    {
        $this->search_request = [
            'name_search' => $this->name_search,
            'email_search' => $this->email_search,
            'mobile_number_search' => $this->mobile_number_search,
            'tin_search' => $this->tin_search,
        ];

        $response = $customer_onboarding_interface->search($this->search_request);

        $this->search_result = $response['result'];

        if (count($this->search_result) < 1 && $this->search_result != null) {
            $this->search_result = $response['result'];
        } else if (count($this->search_result) > 0 && $this->search_request['name_search'] == '' && $this->search_request['email_search']  == '' && $this->search_request['mobile_number_search']  == '' && $this->search_request['tin_search'] == '') {
            $this->search_result = $response['result'];
        } else {
            foreach ($this->search_result as $result) {
                // return redirect()->route('crm.customer-information.customer-data-mgmt.search-result', ['id' => $result->id]);
                return redirect()->route('crm.customer-information.customer-data-mgmt.index', ['id' => $result->id, 'type' => $result->account_type]);
            }
        }
    }

    public function render()
    {
        return view('livewire.crm.customer-information.customer-onboarding.index');
    }
}
