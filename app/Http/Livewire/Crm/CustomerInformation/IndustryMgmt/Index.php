<?php

namespace App\Http\Livewire\Crm\CustomerInformation\IndustryMgmt;

use App\Interfaces\Crm\CustomerInformation\IndustryMgmtInterface;
use App\Traits\Crm\CustomerInformation\IndustryMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use IndustryMgmtTrait, WithPagination, PopUpMessagesTrait;

    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $industry_id;
    public $confirmation_message;

    public $action_type;

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('crm.customer-information.industry-mgmt.edit', 'edit', $data['id']);
            $this->industry_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this Industry?";
            $this->delete_modal = true;
            $this->industry_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }

    public function confirm(IndustryMgmtInterface $industry_mgmts_interface)
    {
        if ($this->action_type == "delete") {
            $response = $industry_mgmts_interface->destroy($this->industry_id);
            $this->emitTo('crm.customer-information.industry-mgmt.index', 'close_modal', 'delete');
            $this->emitTo('crm.customer-information.industry-mgmt.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('crm.customer-information.industry-mgmt.index', 'close_modal', 'delete');
            $this->emitTo('crm.customer-information.industry-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(IndustryMgmtInterface $Industry_Mgmt_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $Industry_Mgmt_interface->index($request);

        // dd($response);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'industry_mgmts' => [],
                ];
        }
        // dd($response['result']['industry_mgmtss']);

        return view('livewire.crm.customer-information.industry-mgmt.index', [
            'industry_mgmts' => $response['result']['industry_mgmts']
        ]);
    }
    
}
