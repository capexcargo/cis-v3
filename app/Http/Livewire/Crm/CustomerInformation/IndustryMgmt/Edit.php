<?php

namespace App\Http\Livewire\Crm\CustomerInformation\IndustryMgmt;

use App\Interfaces\Crm\CustomerInformation\IndustryMgmtInterface;
use App\Models\Industry;
use App\Traits\Crm\CustomerInformation\IndustryMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{

    public $confirmation_modal;
    public $industry;
    public $industry_mgmt;
    
    use IndustryMgmtTrait, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->industry_mgmt = Industry::findOrFail($id);

        $this->industry = $this->industry_mgmt->name;
        // $this->category = $this->employment_category_type->employment_category_id;
    }

    public function submit(IndustryMgmtInterface $industry_mgmt)
    {
        $response = $industry_mgmt->update($this->getRequest(), $this->industry_mgmt->id);
        
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.customer-information.industry-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('crm.customer-information.industry-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.customer-information.industry-mgmt.edit');
    }
}
