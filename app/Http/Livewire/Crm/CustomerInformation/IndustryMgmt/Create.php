<?php

namespace App\Http\Livewire\Crm\CustomerInformation\IndustryMgmt;

use App\Interfaces\Crm\CustomerInformation\IndustryMgmtInterface;
use App\Traits\Crm\CustomerInformation\IndustryMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use IndustryMgmtTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $industry;


    protected $rules = [
        'industry' => 'required|unique:crm_industry,name',
    ];

    public function confirmationSubmit()
    {
        $this->validate();

        $this->confirmation_modal = true;
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.customer-information.industry-mgmt.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function submit(IndustryMgmtInterface $Industry_Mgmt_interface)
    {
        // dd($this->getRequest());

        $response = $Industry_Mgmt_interface->create($this->getRequest());
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.customer-information.industry-mgmt.index', 'close_modal', 'create');
            $this->emitTo('crm.customer-information.industry-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.customer-information.industry-mgmt.create');
    }
}
