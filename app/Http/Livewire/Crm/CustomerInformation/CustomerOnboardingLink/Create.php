<?php

namespace App\Http\Livewire\Crm\CustomerInformation\CustomerOnboardingLink;

use App\Traits\Crm\CustomerInformation\CustomerOnboardingTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Create extends Component
{
    use CustomerOnboardingTrait, PopUpMessagesTrait;

    public $customer_onboarding_id;

    public $pageTitle;
    public $perview_summary_modal = false;

    protected $listeners = ['create' => 'render'];

    public function mount()
    {
        $this->addEmails();
        $this->addTelephoneNumbers();
        $this->addMobileNumbers();
        $this->addAddresses();

        $this->addContactPersons();

        if ($_GET["type"] ?? null) {
            if ($_GET['type'] == 'individual%1%') {
                $this->account_type = 1;
            } else if ($_GET['type'] == 'corporate%2%') {
                $this->account_type = 2;
            }
        } else {
            $this->account_type = false;
        }

        $this->pageTitle = $this->account_type ? 'Customer Onboarding' : '404 Not Found';

        $this->emit('pageTitleSet', $this->pageTitle);

        $this->generateCustomerNo();
    }

    public function confirmationSubmit()
    {
        // dd($this->getRequest());

        $this->account_type = $this->account_type ?? 1;

        if ($this->account_type == 1) {
            $rules = [
                'account_type' => 'required',
                'customer_number' => 'required',
                'first_name' => 'required',
                'middle_name' => 'nullable',
                'last_name' => 'required',

                'life_stage' => 'required',
                'contact_owner' => 'required',
                'marketing_channel' => 'required',
                'notes' => 'nullable',
            ];

            foreach ($this->emails as $i => $email) {
                $rules['emails.' . $i . '.email'] = 'nullable|email|regex:/\./';
                $rules['emails.' . $i . '.default'] = 'nullable';
            }
        } else {
            $rules = [
                'account_type' => 'required',
                'customer_number' => 'required',
                'company_name' => 'required',
                'company_industry' => 'required',
                'company_website_soc_med' => 'nullable',
                'is_mother_account' => 'required',
                // 'tin' => 'required',
                'tin' => 'required|unique:crm_customer_information,tin',
                'company_anniversary' => 'required',
            ];

            if ($this->is_mother_account == 1 || $this->is_mother_account == null) {
                $rules['associate_with'] = 'nullable';
            } else {
                $rules['associate_with'] = 'required';
            }

            foreach ($this->emails as $i => $email) {
                // $rules['emails.' . $i . '.email'] = 'required|email|regex:/\./|ends_with:gmail.com,yahoo.com';
                $rules['emails.' . $i . '.email'] = 'required|unique:crm_email_address_list,email|email|regex:/\./';
            }
        }

        foreach ($this->telephone_numbers as $i => $telephone) {
            $rules['telephone_numbers.' . $i . '.telephone'] = 'nullable';
        }

        foreach ($this->mobile_numbers as $i => $mobile) {
            $rules['mobile_numbers.' . $i . '.mobile'] = 'required|unique:crm_mobile_list,mobile';

            $this->mobile_numbers[$i]['mobile'] = preg_replace("/[^0-9]/", '', $this->mobile_numbers[$i]['mobile']);
        }

        foreach ($this->addresses as $i => $address) {
            $rules['addresses.' . $i . '.address_type'] = 'required';
            if ($this->addresses[$i]['address_type'] == 2) {
                $rules['addresses.' . $i . '.country'] = 'required';
            }
            $rules['addresses.' . $i . '.address_line1'] = 'required';
            $rules['addresses.' . $i . '.address_line2'] = 'nullable';
            $rules['addresses.' . $i . '.state_province'] = 'required';
            $rules['addresses.' . $i . '.city_municipality'] = 'required';
            if ($this->addresses[$i]['address_type'] == 1) {
                $rules['addresses.' . $i . '.barangay'] = 'required';
            }
            $rules['addresses.' . $i . '.postal_code'] = 'required';
            $rules['addresses.' . $i . '.address_label'] = 'required';
        }

        $validated = $this->validate(
            $rules,
            [
                'is_mother_account.required' => 'Please choose account.',
                'tin.unique' => 'The provided TIN (Tax Identification Number) already exists.',

                'emails.*.email.required' => 'The company email address field is required.',
                'emails.*.email.email' => 'Please enter a valid email address.',
                'emails.*.email.regex' => 'Please enter a valid email address.',
                'emails.*.email.unique' => 'The provided email address is already exists.',
                // 'emails.*.email.ends_with' => 'Please enter a valid email address.',
                // 'telephone_numbers.*.telephone.required' => 'The Telephone Number field is required.',
                'mobile_numbers.*.mobile.required' => 'The Mobile Number field is required.',
                'mobile_numbers.*.mobile.unique' => 'The provided mobile number is already exists.',

                'addresses.*.address_type.required' => 'The Address Type is required.',
                'addresses.*.country.required' => 'The Country field is required.',
                'addresses.*.address_line1.required' => 'The Address Line1 field is required.',
                // 'addresses.*.address_line2.required' => 'The Address Line2 field is required.',
                'addresses.*.state_province.required' => 'The State/Province field is required.',
                'addresses.*.city_municipality.required' => 'The City/Municipality field is required.',
                'addresses.*.barangay.required' => 'The Barangay field is required.',
                'addresses.*.postal_code.required' => 'The Postal Code field is required.',
                'addresses.*.address_label.required' => 'Select Address Label.',
            ]
        );

        if ($this->account_type == 1) {
            $this->perview_summary_modal = true;
        } else {
            $this->current_tab = 2;
        }
    }
    


    public function submitCorpContactPerson()
    {
        // dd($this->getRequest());

        $rules = [
            'con_per_life_stage' => 'required',
            'con_per_contact_owner' => 'required',
            'con_per_marketing_channel' => 'required',
            'con_per_notes' => 'nullable',
        ];

        foreach ($this->contact_persons as $i => $position) {
            $rules['contact_persons.' . $i . '.first_name'] = 'required';
            $rules['contact_persons.' . $i . '.middle_name'] = 'nullable';
            $rules['contact_persons.' . $i . '.last_name'] = 'required';
            $rules['contact_persons.' . $i . '.position'] = 'required';

            foreach ($this->contact_persons[$i]['emails'] as $x => $email) {
                $rules['contact_persons.' . $i . '.emails.' . $x . '.email'] = 'required|unique:crm_email_address_list,email|email|regex:/\./';
            }

            foreach ($this->contact_persons[$i]['telephone_numbers'] as $x => $email) {
                $rules['contact_persons.' . $i . '.telephone_numbers.' . $x . '.telephone'] = 'nullable'; //|regex:/^[0-9]{10}$/';
            }

            foreach ($this->contact_persons[$i]['mobile_numbers'] as $x => $email) {
                $rules['contact_persons.' . $i . '.mobile_numbers.' . $x . '.mobile'] = 'required|unique:crm_mobile_list,mobile';

                $this->contact_persons[$i]['mobile_numbers'][$x]['mobile'] = preg_replace("/[^0-9]/", '', $this->contact_persons[$i]['mobile_numbers'][$x]['mobile']);
            }


            $rules['contact_persons.' . $i . '.default'] = 'required';
        }

        $validated = $this->validate(
            $rules,
            [
                'con_per_life_stage.required' => 'The Life Stage field is required.',
                'con_per_contact_owner.required' => 'The Contact Owner field is required.',
                'con_per_marketing_channel.required' => 'The Marketing Channel field is required.',

                'contact_persons.*.first_name.required' => 'The First Name field is required.',
                'contact_persons.*.last_name.required' => 'The Last Name field is required.',
                'contact_persons.*.position.required' => 'The Position field is required.',

                'contact_persons.*.emails.*.email.required' => 'The Email field is required.',
                'contact_persons.*.emails.*.email.email' => 'Please enter a valid email address.',
                'contact_persons.*.emails.*.email.regex' => 'Please enter a valid email address.',
                'contact_persons.*.emails.*.email.unique' => 'The provided email address is already exists.',

                'contact_persons.*.mobile_numbers.*.mobile.required' => 'The Mobile Number field is required.',
                'contact_persons.*.mobile_numbers.*.mobile.unique' => 'The provided mobile number is already exists.',
                // 'contact_persons.*.telephone_numbers.*.telephone.regex' => 'The Telephone Number format is invalid.',
            ]
        );

        $this->current_tab = 2;
        $this->perview_summary_modal = true;
    }

    public function render()
    {

        return view('livewire.crm.customer-information.customer-onboarding-link.create');
    }
}
