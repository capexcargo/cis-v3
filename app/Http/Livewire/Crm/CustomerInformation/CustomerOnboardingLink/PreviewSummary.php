<?php

namespace App\Http\Livewire\Crm\CustomerInformation\CustomerOnboardingLink;

use App\Interfaces\Crm\CustomerInformation\CustomerOnboardingInterface;
use App\Models\Crm\BarangayReference;
use App\Models\Crm\CityReference;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\StateReference;
use App\Models\Industry;
use App\Models\MarketingChannel;
use App\Models\User;
use App\Traits\Crm\CustomerInformation\CustomerOnboardingTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Redirect;
use Livewire\Component;

class PreviewSummary extends Component
{
    use CustomerOnboardingTrait, PopUpMessagesTrait;

    public $action_type;
    public $data;
    public $state = [];
    public $city = [];
    public $barangay = [];

    public $associate_with;
    public $associate_with_accnt_no;
    public $industry;
    public $contact_owner;
    public $marketing_channel;

    public $perview_summary_modal = false;

    public $customer_onboarding_id;

    public function mount($data)
    {
        $this->data = $data;

        $this->associate_with = CrmCustomerInformation::where('id', $data['associate_with'])->value('fullname');
        $this->associate_with_accnt_no = CrmCustomerInformation::where('id', $data['associate_with'])->value('account_no');
        $this->industry = Industry::where('id', $data['company_industry'])->value('name');

        foreach ($this->data['addresses'] as $i => $address) {
            $this->state[$i] = StateReference::where('id', $address['state_province'])->value('name');

            $this->city[$i] = CityReference::where('id', $address['city_municipality'])->value('name');

            $this->barangay[$i] = BarangayReference::where('id', $address['barangay'])->value('name');
        }

        $this->contact_owner = User::where('division_id', 2)->where('id', $data['contact_owner'] ?? $data['con_per_contact_owner'])->value('name');
        $this->marketing_channel = MarketingChannel::where('id', $data['marketing_channel'] ?? $data['con_per_marketing_channel'])->value('name');
    }

    public function submit(CustomerOnboardingInterface $customer_onboarding_interface)
    {
        // dd($this->data['account_type']);
        $response = $customer_onboarding_interface->create($this->data, true);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->addEmails();
            $this->addTelephoneNumbers();
            $this->addMobileNumbers();
            $this->addAddresses();

            $this->addContactPersons();

            foreach ($this->emails as $i => $email) {
                if (count($this->emails) <= 1) {
                    $this->emails[$i]['default'] = true;
                }
            }

            foreach ($this->mobile_numbers as $i => $mobile_number) {
                if (count($this->mobile_numbers) <= 1) {
                    $this->mobile_numbers[$i]['default'] = true;
                }
            }

            foreach ($this->telephone_numbers as $i => $telephone_number) {
                if (count($this->telephone_numbers) <= 1) {
                    $this->telephone_numbers[$i]['default'] = true;
                }
            }

            foreach ($this->addresses as $i => $address) {
                if (count($this->addresses) <= 1) {
                    $this->addresses[$i]['default'] = true;
                }
            }

            foreach ($this->contact_persons as $i => $position) {
                if (count($this->contact_persons) <= 1) {
                    $this->contact_persons[$i]['default'] = true;
                }
            }

            $this->sweetAlert('', $response['message']);
            // $this->emitTo('crm.customer-information.customer-onboarding-link', 'create');
// dd('asdasd');

            if($this->data['account_type'] == 1){
                return Redirect::to('https://cisv3-stag.capex.com.ph/customer-onboarding-link?type=individual%1%');
            }

            if($this->data['account_type'] == 2){
                return Redirect::to('https://cisv3-stag.capex.com.ph/customer-onboarding-link?type=corporate%2%');
            }
            // $this->emit('redirectTo');

            $this->customer_onboarding_id = $response['result']['customer_onboarding_id'];

            // return redirect()->route('crm.customer-information.customer-onboarding-link.create');
        } elseif ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render()
    {
        return view('livewire.crm.customer-information.customer-onboarding-link.preview-summary');
    }
}
