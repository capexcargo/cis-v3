<?php

namespace App\Http\Livewire\Crm\CustomerInformation\CustomerDataMgmt;

use App\Models\Crm\CrmAccountTypeReference;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmCustomerInformationAddressList;
use App\Models\Crm\CrmCustomerInformationEmailAddressList;
use App\Models\Crm\CrmCustomerInformationMobileList;
use App\Models\Crm\CrmCustomerInformationTelephoneList;
use App\Models\Crm\CrmCustomerStatusReferences;
use Livewire\Component;
use Livewire\WithPagination;

class SearchResult extends Component
{
    use WithPagination;

    public $customer_informations = [];
    public $email;
    public $mobile_no;
    public $telephone_no;

    public $header_cards = [];
    public $status_header_cards = [];
    
    public $account_type;
    public $name_search;
    public $email_search;
    public $mobile_number_search;
    public $status;


    public function mount($id)
    {
        $this->customer_informations = CrmCustomerInformation::with(
            'accountType',
            'industry',
            'lifeStage',
            'contactOwner',
            'marketingChannel',
            'statusRef',
            'emails',
            'mobileNumbers',
            'telephoneNumbers',
            'addresses'
        )
            ->where('id', $id)
            ->get();

        $emails = CrmCustomerInformationEmailAddressList::where('account_id', $id)->where('is_primary', 1)->first();
        $mobile_no = CrmCustomerInformationMobileList::where('account_id', $id)->where('is_primary', 1)->first();
        $telephone_no = CrmCustomerInformationTelephoneList::where('account_id', $id)->where('is_primary', 1)->first();

        $this->email = $emails->email;
        $this->mobile_no = $mobile_no->mobile;
        $this->telephone_no = $telephone_no->telephone;
    }

    public function load()
    {
        $this->loadHeadCards();
        $this->loadStatusHeaderCards();
    }

    public function loadHeadCards()
    {
        $crm_account_type = CrmAccountTypeReference::get();

        $this->header_cards = [
            [
                'title' => 'Individual',
                'value' => $crm_account_type[0]['id'],
                'color' => 'text-blue',
                'action' => 'account_type',
                'id' => 1
            ],
            [
                'title' => 'Corporate',
                'value' => $crm_account_type[1]['id'],
                'color' => 'text-blue',
                'action' => 'account_type',
                'id' => 2
            ],
        ];
    }

    public function loadStatusHeaderCards()
    {
        $status = CrmCustomerStatusReferences::withCount('customers')->get();

        $this->status_header_cards = [
            [
                'title' => 'All Customer',
                'value' => $status->sum('customers_count'),
                'class' => 'bg-white text-gray-500 border border-gray-600 shadow-sm rounded-l-md',
                'color' => 'text-blue',
                'action' => 'status',
                'id' => false
            ],
            [
                'title' => 'Active',
                'value' => 1,
                // 'value' => $status[0]->customers_count + $status[1]->customers_count,
                'class' => 'bg-white border border-gray-600 shadow-sm text-gray-500 ',
                'color' => 'text-blue',
                'action' => 'status',
                'id' => 2
            ],
            [
                'title' => 'Prospect',
                'value' => 1,
                // 'value' => $status[2]->customers_count,
                'class' => 'bg-white border border-gray-600 shadow-sm text-gray-500',
                'color' => 'text-blue',
                'action' => 'status',
                'id' => 3
            ],
            [
                'title' => 'Inactive',
                'value' => 1,
                // 'value' => $status[3]->customers_count,
                'class' => 'bg-white border border-gray-600 shadow-sm text-gray-500 rounded-r-md',
                'color' => 'text-blue',
                'action' => 'status',
                'id' => 4
            ],

        ];
    }

    public function render()
    {
        return view('livewire.crm.customer-information.customer-data-mgmt.search-result');
    }
}
