<?php

namespace App\Http\Livewire\Crm\CustomerInformation\CustomerDataMgmt;

use App\Interfaces\Crm\CustomerInformation\CustomerOnboardingInterface;
use App\Models\Crm\CrmAccountTypeReference;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmCustomerStatusReferences;
use App\Traits\Crm\CustomerInformation\CustomerDataMgmtTrait;
use App\Traits\Crm\CustomerInformation\CustomerOnboardingTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use CustomerOnboardingTrait, WithPagination, PopUpMessagesTrait;

    public $name_search;
    public $email_search;
    public $mobile_number_search;
    public $status;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 50;

    public $customer_id;
    public $customer_param_id;
    public $customer_param_account_type;

    public $cat_status;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'mount' => 'mount'];

    public function mount()
    {
        $this->customer_param_id = $_GET['id'] ?? null;
        $this->customer_param_account_type = $_GET['type'] ?? null;
        $this->account_type =  $_GET['type'] ?? null;
        // $this->search();
        $this->load();
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'view') {
            $this->emitTo('crm.customer-information.customer-data-mgmt.view', 'view', $data['id']);
            $this->customer_id = $data['id'];
            $this->view_modal = true;
        } else if ($action_type == 'edit') {
            $this->emitTo('crm.customer-information.customer-data-mgmt.edit', 'edit', $data['id']);
            $this->customer_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'update_status') {
            $this->customer_id = $data['id'];
            $this->cat_status = $data['status'];
            $this->confirmation_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'view') {
            $this->view_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function update(CustomerOnboardingInterface $customer_onboarding_interface)
    {
        $response = $customer_onboarding_interface->updateStatus($this->customer_id,  $this->cat_status);

        if ($response['code'] == 200) {
            // $this->emitTo('crm.customer-information.customer-data-mgmt.index', 'index');
            // $this->emitTo('crm.customer-information.customer-data-mgmt.index', 'mount');
            $this->sweetAlert('', $response['message']);
            $this->status = 2;
            $this->confirmation_modal = false;
        } else {
            $this->emitTo('crm.customer-information.customer-data-mgmt.index', 'index');
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function load()
    {
        $this->loadStatusHeaderCards();

        // $this->account_type = 1;

        $this->status = '';
    }

    public function accountType($type)
    {
        $this->customer_param_id = null;
        $this->customer_param_account_type = null;

        // if ($type == 1) {
        //     $this->account_type = 1;
        // } elseif ($type == 2) {
        //     $this->account_type = 2;
        // }
        $this->account_type = $type;

        $this->loadStatusHeaderCards();

        $this->status = '';
    }

    public function checkStatus($status)
    {
        $this->customer_param_id = null;
        $this->customer_param_account_type = null;
        if ($status == "") {
            $this->status = '';
        } elseif ($status == 1) {
            $this->status = 1;
        } elseif ($status == 2) {
            $this->status = 2;
        } elseif ($status == 3) {
            $this->status = 3;
        }

        $this->loadStatusHeaderCards();
    }

    public function loadStatusHeaderCards()
    {
        // for the filter like mount
        $this->account_type = $this->account_type ?? 1;
        $this->status = $this->status ?? false;

        // $status = CrmCustomerStatusReferences::withCount('customers')->get();
        $count_active = CrmCustomerInformation::where('status', 1)->where('account_type', $this->account_type)->count();
        $count_inactive = CrmCustomerInformation::where('status', 2)->where('account_type', $this->account_type)->count();
        $count_prospect = CrmCustomerInformation::where('status', 3)->where('account_type', $this->account_type)->count();
        $count_all = $count_active + $count_inactive + $count_prospect;

        $this->status_header_cards = [
            [
                'title' => 'All Customer',
                // 'value' => $status->sum('customers_count'),
                'value' => $count_all,
                'class' => 'bg-white text-gray-500 border border-gray-600 shadow-sm rounded-l-md',
                'color' => 'text-blue',
                'action' => 'status',
                'id' => false
            ],
            [
                'title' => 'Active',
                // 'value' => $status[0]->customers_count,
                'value' => $count_active,
                'class' => 'bg-white text-gray-500 border border-gray-600 shadow-sm ',
                'color' => 'text-blue',
                'action' => 'status',
                'id' => 1
            ],
            [
                'title' => 'Prospect',
                // 'value' => $status[2]->customers_count,
                'value' => $count_prospect,
                'class' => 'bg-white text-gray-500 border border-gray-600 shadow-sm',
                'color' => 'text-blue',
                'action' => 'status',
                'id' => 3
            ],
            [
                'title' => 'Inactive',
                // 'value' => $status[1]->customers_count,
                'value' => $count_inactive,
                'class' => 'bg-white text-gray-500 border border-gray-600 shadow-sm rounded-r-md',
                'color' => 'text-blue',
                'action' => 'status',
                'id' => 2
            ],
        ];
    }

    public function search()
    {
        return [
            'paginate' => $this->paginate,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'account_type' => $this->customer_param_account_type ?? $this->account_type ?? 1,
            'customer_param_id' => $this->customer_param_id,
            'name' => $this->name_search,
            'email' => $this->email_search,
            'mobile' => preg_replace("/[^0-9]/", '', $this->mobile_number_search),
            'status' => $this->status,
        ];
    }

    public function render(CustomerOnboardingInterface $customer_onboarding_interface)
    {
        $response = $customer_onboarding_interface->index($this->search());

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'customer_informations' => [],
                ];
        }

        return view('livewire.crm.customer-information.customer-data-mgmt.index', [
            'customer_informations' => $response['result']['customer_informations']
        ]);
    }
}
