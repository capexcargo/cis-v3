<?php

namespace App\Http\Livewire\Crm\CustomerInformation\CustomerDataMgmt;

use App\Interfaces\Crm\CustomerInformation\CustomerOnboardingInterface;
use App\Models\Crm\BarangayReference;
use App\Models\Crm\CityReference;
use App\Traits\Crm\CustomerInformation\CustomerOnboardingTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use CustomerOnboardingTrait, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    // public $customer_informations;
    // public $customer;

    public function mount(CustomerOnboardingInterface $customer_onboarding_interface, $id)
    {
        $this->resetForm();

        $response = $customer_onboarding_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $customer = $response['result'];

        $this->customer_id = $customer->id;

        $this->account_type = $customer->account_type;
        $this->customer_number = $customer->account_no;
        $this->company_name = $customer->company_name;
        $this->first_name = $customer->first_name;
        $this->middle_name = $customer->middle_name;
        $this->last_name = $customer->last_name;
        $this->rate_name = $customer->rate_name;

        $this->company_industry = $customer->industry_id;
        $this->company_website_soc_med = $customer->company_link;
        $this->is_mother_account = $customer->is_mother_account;
        $this->associate_with = $customer->child_account_id;
        $this->tin = $customer->tin;
        $this->company_anniversary = $customer->company_anniversary;

        $this->life_stage = $customer->life_stage_id == 1 ? "Lead" : '';
        $this->contact_owner = $customer->contact_owner_id;
        $this->marketing_channel = $customer->marketing_channel_id;
        $this->notes = $customer->notes;


        foreach ($customer->emails as $email) {
            if ($email->account_type != 3) {
                $this->emails[] = [
                    'id' => $email->id,
                    'email' => $email->email,
                    'default' => $email->is_primary == 1 ? true : false,
                    'is_deleted' => false,
                ];
            }
        }

        foreach ($customer->telephoneNumbers as $telephone_number) {
            if ($telephone_number->account_type != 3) {
                $this->telephone_numbers[] = [
                    'id' => $telephone_number->id,
                    'telephone' => $telephone_number->telephone,
                    'default' => $telephone_number->is_primary == 1 ? true : false,
                    'is_deleted' => false,
                ];
            }
        }

        foreach ($customer->mobileNumbers as $mobile_number) {
            if ($mobile_number->account_type != 3) {
                $this->mobile_numbers[] = [
                    'id' => $mobile_number->id,
                    'mobile' => $mobile_number->mobile,
                    'default' => $mobile_number->is_primary == 1 ? true : false,
                    'is_deleted' => false,
                ];
            }
        }

        foreach ($customer->addresses as $address) {
            if ($address->account_type != 3) {
                $this->addresses[] = [
                    'id' => $address->id,
                    'address_type' => $address->address_type,
                    'country' => $address->country_id,
                    'address_line1' => $address->address_line_1,
                    'address_line2' => $address->address_line_2,
                    'state_province' => $address->state_id,
                    'city_municipality' => $address->city_id,
                    'barangay' => $address->barangay_id,
                    'postal_code' => $address->postal_id,
                    'address_label' => $address->address_label,
                    'city_municipal_references' => CityReference::where('state_id', $address->state_id)->get(),
                    'barangay_references' => BarangayReference::where('state_id', $address->state_id)
                        ->where('city_id', $address->city_id)->get(),
                    'default' => $address->is_primary == 1 ? true : false,
                    'is_deleted' => false,
                ];
            }
        }

        // CONTACT PERSON UPDATE
        $contact_persons = [];
        foreach ($customer->contactPersons as $i => $con_per) {
            $contact_persons[$i] = [
                'id' => $con_per->id,
                'first_name' => $con_per->first_name,
                'middle_name' => $con_per->middle_name,
                'last_name' => $con_per->last_name,
                'position' => $con_per->position,
                'emails' => $con_per->conPerEmails,
                'telephone_numbers' => $con_per->conPerTelephoneNumbers,
                'mobile_numbers' => $con_per->conPerMobileNumbers,
                'default' => $con_per->is_primary == 1 ? true : false,
                'is_deleted' => false,
            ];
        }

        $this->contact_persons = $contact_persons;

        foreach ($this->contact_persons as $i => $contact_person) {
            foreach ($this->contact_persons[$i]['emails'] as $k => $email) {
                $this->contact_persons[$i]['emails'][$k]['default'] = $email->is_primary == 1 ? true : false;
            }
        }

        foreach ($this->contact_persons as $i => $contact_person) {
            foreach ($this->contact_persons[$i]['telephone_numbers'] as $k => $telephone_number) {
                $this->contact_persons[$i]['telephone_numbers'][$k]['default'] =  $telephone_number->is_primary == 1 ? true : false;
            }
        }

        foreach ($this->contact_persons as $i => $contact_person) {
            foreach ($this->contact_persons[$i]['mobile_numbers'] as $k => $mobile_number) {
                $this->contact_persons[$i]['mobile_numbers'][$k]['default'] =  $mobile_number->is_primary == 1 ? true : false;
            }
        }

        $this->con_per_life_stage = $customer->life_stage_id == 1 ? "Lead" : '';
        $this->con_per_contact_owner = $customer->contact_owner_id;
        $this->con_per_marketing_channel = $customer->marketing_channel_id;
        $this->con_per_notes = $customer->notes;
    }

    public function confirmationSubmit()
    {
        $this->account_type = $this->account_type ?? 1;

        if ($this->account_type == 1) {
            $rules = [
                'account_type' => 'required',
                'customer_number' => 'required',
                'first_name' => 'required',
                'middle_name' => 'nullable',
                'last_name' => 'required',

                'life_stage' => 'required',
                'contact_owner' => 'required',
                'marketing_channel' => 'required',
                'notes' => 'nullable',
            ];

            foreach ($this->emails as $i => $email) {
                $rules['emails.' . $i . '.email'] = 'nullable|email|regex:/\./';
                $rules['emails.' . $i . '.default'] = 'nullable';
            }
        } else {
            $rules = [
                'account_type' => 'required',
                'customer_number' => 'required',
                'company_name' => 'required',
                'company_industry' => 'required',
                'company_website_soc_med' => 'nullable',
                'rate_name' => 'nullable',
                'is_mother_account' => 'required',
                'tin' => 'required',
                'company_anniversary' => 'required',
            ];

            if ($this->is_mother_account == 1 || $this->is_mother_account == null) {
                $rules['associate_with'] = 'nullable';
            } else {
                $rules['associate_with'] = 'required';
            }

            foreach ($this->emails as $i => $email) {
                // $rules['emails.' . $i . '.email'] = 'required|email|regex:/\./|ends_with:gmail.com,yahoo.com';
                $rules['emails.' . $i . '.email'] = 'required|email|regex:/\./';
            }
        }

        foreach ($this->telephone_numbers as $i => $telephone) {
            $rules['telephone_numbers.' . $i . '.telephone'] = 'nullable';
        }

        foreach ($this->mobile_numbers as $i => $mobile) {
            $rules['mobile_numbers.' . $i . '.mobile'] = 'required';

            $this->mobile_numbers[$i]['mobile'] = preg_replace("/[^0-9]/", '', $this->mobile_numbers[$i]['mobile']);
        }

        foreach ($this->addresses as $i => $address) {
            $rules['addresses.' . $i . '.address_type'] = 'required';
            if ($this->addresses[$i]['address_type'] == 2) {
                $rules['addresses.' . $i . '.country'] = 'required';
            }
            $rules['addresses.' . $i . '.address_line1'] = 'required';
            $rules['addresses.' . $i . '.address_line2'] = 'nullable';
            $rules['addresses.' . $i . '.state_province'] = 'required';
            $rules['addresses.' . $i . '.city_municipality'] = 'required';
            if ($this->addresses[$i]['address_type'] == 1) {
                $rules['addresses.' . $i . '.barangay'] = 'required';
            }
            $rules['addresses.' . $i . '.postal_code'] = 'required';
            $rules['addresses.' . $i . '.address_label'] = 'required';
        }

        $validated = $this->validate(
            $rules,
            [
                'is_mother_account.required' => 'Please choose account.',

                'emails.*.email.required' => 'The company email address field is required.',
                'emails.*.email.email' => 'Please enter a valid email address.',
                'emails.*.email.regex' => 'Please enter a valid email address.',
                // 'emails.*.email.ends_with' => 'Please enter a valid email address.',
                // 'telephone_numbers.*.telephone.required' => 'The Telephone Number field is required.',
                'mobile_numbers.*.mobile.required' => 'The Mobile Number field is required.',

                'addresses.*.address_type.required' => 'The Address Type is required.',
                'addresses.*.country.required' => 'The Country field is required.',
                'addresses.*.address_line1.required' => 'The Address Line1 field is required.',
                // 'addresses.*.address_line2.required' => 'The Address Line2 field is required.',
                'addresses.*.state_province.required' => 'The State/Province field is required.',
                'addresses.*.city_municipality.required' => 'The City/Municipality field is required.',
                'addresses.*.barangay.required' => 'The Barangay field is required.',
                'addresses.*.postal_code.required' => 'The Postal Code field is required.',
                'addresses.*.address_label.required' => 'Select Address Label.',
            ]
        );

        if ($this->account_type == 1) {
            $this->confirmation_modal = true;
        } else {
            $this->current_tab = 2;
        }
    }

    public function submitCorpContactPerson()
    {
        $rules = [
            'con_per_life_stage' => 'required',
            'con_per_contact_owner' => 'required',
            'con_per_marketing_channel' => 'required',
            'con_per_notes' => 'nullable',
        ];

        foreach ($this->contact_persons as $i => $position) {
            $rules['contact_persons.' . $i . '.first_name'] = 'required';
            $rules['contact_persons.' . $i . '.middle_name'] = 'nullable';
            $rules['contact_persons.' . $i . '.last_name'] = 'required';
            $rules['contact_persons.' . $i . '.position'] = 'required';

            foreach ($this->contact_persons[$i]['emails'] as $x => $email) {
                $rules['contact_persons.' . $i . '.emails.' . $x . '.email'] = 'required|email|regex:/\./';
            }

            foreach ($this->contact_persons[$i]['telephone_numbers'] as $x => $email) {
                $rules['contact_persons.' . $i . '.telephone_numbers.' . $x . '.telephone'] = 'nullable'; //|regex:/^[0-9]{10}$/';
            }

            foreach ($this->contact_persons[$i]['mobile_numbers'] as $x => $email) {
                $rules['contact_persons.' . $i . '.mobile_numbers.' . $x . '.mobile'] = 'required';
            }


            $rules['contact_persons.' . $i . '.default'] = 'required';
        }

        $validated = $this->validate(
            $rules,
            [
                'con_per_life_stage.required' => 'The Life Stage field is required.',
                'con_per_contact_owner.required' => 'The Contact Owner field is required.',
                'con_per_marketing_channel.required' => 'The Marketing Channel field is required.',

                'contact_persons.*.first_name.required' => 'The First Name field is required.',
                'contact_persons.*.last_name.required' => 'The Last Name field is required.',
                'contact_persons.*.position.required' => 'The Position field is required.',
                'contact_persons.*.emails.*.email.required' => 'The Email field is required.',
                'contact_persons.*.emails.*.email.email' => 'Please enter a valid email address.',
                'contact_persons.*.emails.*.email.regex' => 'Please enter a valid email address.',
                'contact_persons.*.mobile_numbers.*.mobile.required' => 'The Mobile Number field is required.',
                // 'contact_persons.*.telephone_numbers.*.telephone.regex' => 'The Telephone Number format is invalid.',
            ]
        );

        $this->current_tab = 2;
        $this->confirmation_modal = true;
    }

    public function submit(CustomerOnboardingInterface $customer_onboarding_interface)
    {
        // dd($this->getRequest());
        $response = $customer_onboarding_interface->update($this->getRequest(), $this->customer_id);

        if ($response['code'] == 200) {
            $this->emitTo('crm.customer-information.customer-data-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('crm.customer-information.customer-data-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);

            $this->resetForm();

            $this->addEmails();
            $this->addTelephoneNumbers();
            $this->addMobileNumbers();
            $this->addAddresses();

            $this->addContactPersons();

            foreach ($this->emails as $i => $email) {
                if (count($this->emails) <= 1) {
                    $this->emails[$i]['default'] = true;
                }
            }
            foreach ($this->mobile_numbers as $i => $mobile_number) {
                if (count($this->mobile_numbers) <= 1) {
                    $this->mobile_numbers[$i]['default'] = true;
                }
            }
            foreach ($this->telephone_numbers as $i => $telephone_number) {
                if (count($this->telephone_numbers) <= 1) {
                    $this->telephone_numbers[$i]['default'] = true;
                }
            }
            foreach ($this->addresses as $i => $address) {
                if (count($this->addresses) <= 1) {
                    $this->addresses[$i]['default'] = true;
                }
            }

            foreach ($this->contact_persons as $i => $position) {
                if (count($this->contact_persons) <= 1) {
                    $this->contact_persons[$i]['default'] = true;
                }
            }
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.customer-information.customer-data-mgmt.edit');
    }
}
