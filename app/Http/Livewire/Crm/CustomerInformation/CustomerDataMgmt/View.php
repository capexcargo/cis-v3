<?php

namespace App\Http\Livewire\Crm\CustomerInformation\CustomerDataMgmt;

use App\Interfaces\Crm\CustomerInformation\CustomerOnboardingInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class View extends Component
{
    use PopUpMessagesTrait;

    protected $listeners = ['view' => 'mount'];

    public $customer_informations;

    public $fullname;
    public $customer_number;
    public $is_mother_account;
    public $sub_account;

    public $sub_account_name;
    public $sub_customer_number;

    public $emails = [];
    public $contact_persons = [];
    public $contact_person_mobiles = [];
    public $telephone_numbers = [];
    public $mobile_numbers = [];
    public $addresses = [];


    public $tin;
    public $category;
    public $life_stage;
    public $account_type;
    public $account_type_id;
    public $contact_owner;
    public $onboarding_channel;
    public $marketing_channel;
    public $date_onboarded;
    public $note;
    public $final_status;
    public $industry;
    public $rate_name;

    public function mount(CustomerOnboardingInterface $customer_onboarding_interface, $id)
    {

        $response = $customer_onboarding_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->customer_informations = $response['result'];

        $this->fullname = $this->customer_informations->fullname;
        $this->customer_number = $this->customer_informations->account_no;
        $this->is_mother_account = $this->customer_informations->is_mother_account;
        $this->sub_account = $this->customer_informations->child_account_id;
        $this->rate_name = $this->customer_informations->rate_name;

        $this->sub_account_name = $this->customer_informations->subAccount->fullname ?? null;
        $this->sub_customer_number = $this->customer_informations->subAccount->account_no ?? null;

        $this->emails = $this->customer_informations->emails;
        $this->contact_persons = $this->customer_informations->contactPersons;
        // $this->contact_person_mobiles = $this->customer_informations->contactPersons->conPerMobileNumbers ?? null;
        $this->telephone_numbers = $this->customer_informations->telephoneNumbers;
        $this->mobile_numbers = $this->customer_informations->mobileNumbers;
        $this->addresses = $this->customer_informations->addresses;

        $this->industry = $this->customer_informations->industry->name ?? null;

        $this->tin = $this->customer_informations->tin;
        $this->category = $this->customer_informations->statusRef->name;
        $this->life_stage = $this->customer_informations->lifeStage->name ?? null;
        // $this->account_type = $this->customer_informations->accountType->name;
        $this->account_type = $this->customer_informations->account_type;
        $this->account_type_id = $this->customer_informations->account_type;
        $this->contact_owner = $this->customer_informations->contactOwner->name ?? null;
        // $this->onboarding_channel = $this->customer_informations->accountType->name;
        $this->marketing_channel = $this->customer_informations->marketingChannel->name ?? null;
        $this->date_onboarded = date('M. d, Y', strtotime($this->customer_informations->created_at));
        $this->note = $this->customer_informations->notes;
        $this->final_status = $this->customer_informations->final_status;
    }

    public function render()
    {
        return view('livewire.crm.customer-information.customer-data-mgmt.view');
    }
}
