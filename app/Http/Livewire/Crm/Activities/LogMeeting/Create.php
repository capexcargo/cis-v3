<?php

namespace App\Http\Livewire\Crm\Activities\LogMeeting;

use App\Interfaces\Crm\Activities\LogMeetingInterface;
use App\Traits\Crm\Activities\LogMeetingTraits;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Create extends Component
{
    use LogMeetingTraits, WithPagination, PopUpMessagesTrait;

    public function closecreatemodal()
    {
        $this->resetFormMeeting();
        $this->resetErrorBag();
        $this->emitTo('crm.activities.index', 'close_modal', 'meeting');
        $this->confirmation_modal_meeting = false;
    }

    // public function confirmationSubmit(LogMeetingInterface $meeting_interface)
    // {
    //     $response = $meeting_interface->createValidation($this->getRequestMeeting());

    //     if ($response['code'] == 200) {
    //         $this->confirmation_modal_meeting = true;
    //     } else if ($response['code'] == 400) {

    //         $this->resetErrorBag();
    //         foreach ($response['result']->getMessages() as $a => $messages) {
    //             if (is_array($messages)) {

    //                 foreach ($messages as $message) :
    //                     $this->addError($a, $message);
    //                 endforeach;
    //             } else {
    //                 $this->addError($a, $messages);
    //             }
    //         }
    //         return;
    //     } else {
    //         $this->sweetAlertError('error', $response['message'], $response['result']);
    //     }
    // }

    public function submit(LogMeetingInterface $meeting_interface)
    {
        // dd($this->getRequest());
        $response = $meeting_interface->create($this->getRequestMeeting());
        if ($response['code'] == 200) {

            $this->resetFormMeeting();
            $this->confirmation_modal_meeting = false;
            $this->emitTo('crm.activities.index', 'close_modal', 'meeting');
            $this->emitTo('crm.activities.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.activities.log-meeting.create');
    }
}
