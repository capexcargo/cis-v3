<?php

namespace App\Http\Livewire\Crm\Activities\ScheduleMeeting;

use App\Traits\Crm\Activities\ScheduleMeeting\ScheduleMeetingTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use ScheduleMeetingTrait, PopUpMessagesTrait;

    public $confirmation_modal;

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.activities.schedule-meeting.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    // public function confirmationSubmit(OdaopaMgmtInterface $odaopa_interface)
    // {
    //     $response = $odaopa_interface->createValidation($this->getRequest());

    //     if ($response['code'] == 200) {
    //         $this->confirmation_modal = true;
    //     } else if ($response['code'] == 400) {

    //         $this->resetErrorBag();
    //         foreach ($response['result']->getMessages() as $a => $messages) {
    //             if (is_array($messages)) {

    //                 foreach ($messages as $message) :
    //                     $this->addError($a, $message);
    //                 endforeach;
    //             } else {
    //                 // dd($response['result']);
    //                 $this->addError($a, $messages);
    //             }
    //         }
    //         return;
    //     } else {
    //         $this->sweetAlertError('error', $response['message'], $response['result']);
    //     }
    // }

    // public function submit(OdaopaMgmtInterface $odaopa_interface)
    // {
    //     $response = $odaopa_interface->create($this->getRequest());
    //     if ($response['code'] == 200) {

    //         $this->resetForm();
    //         $this->emitTo('oims.order-management.t-e-dropdown-mgmt.odaopa-mgmt.index', 'close_modal', 'create');
    //         $this->emitTo('oims.order-management.t-e-dropdown-mgmt.odaopa-mgmt.index', 'index');
    //         $this->sweetAlert('', $response['message']);
    //     } elseif ($response['code'] == 400) {
    //         $this->resetErrorBag();
    //         foreach ($response['result']->getMessages() as $a => $messages) {
    //             if (is_array($messages)) {
    //                 foreach ($messages as $message) {
    //                     $this->addError($a, $message);
    //                 }
    //             } else {
    //                 $this->addError($a, $messages);
    //             }
    //         }

    //         return;
    //     } else {
    //         $this->sweetAlertError('error', $response['message'], $response['result']);
    //     }
    // }

    public function render()
    {
        return view('livewire.crm.activities.schedule-meeting.create');
    }
}
