<?php

namespace App\Http\Livewire\Crm\Activities\ScheduleMeeting;

use App\Traits\Crm\Activities\ScheduleMeeting\ScheduleMeetingTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use ScheduleMeetingTrait, WithPagination, PopUpMessagesTrait;

    public $status_header_cards = [];
    public $stats;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal = true;
        }
        // elseif ($action_type == 'edit') {
        //     $this->emitTo('oims.order-management.t-e-dropdown-mgmt.odaopa-mgmt.edit', 'edit', $data['id']);
        //     $this->oda_id = $data['id'];
        //     $this->edit_modal = true;
        // }else if ($action_type == 'update_status') {
        //     if ($data['status'] == 1) {
        //         $this->reactivate_modal = true;
        //         $this->oda_id = $data['id'];
        //         return;
        //     }
        //     $this->deactivate_modal = true;
        //     $this->oda_id = $data['id'];
        // }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }
        // else if ($action_type == 'edit') {
        //     $this->edit_modal = false;
        // } elseif ($action_type == 'update_status') {
        //     $this->reactivate_modal = false;
        //     $this->deactivate_modal = false;
        // } 
    }

    public function render()
    {
        return view('livewire.crm.activities.schedule-meeting.index');
    }
}
