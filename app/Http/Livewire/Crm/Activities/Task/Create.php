<?php

namespace App\Http\Livewire\Crm\Activities\Task;

use App\Interfaces\Crm\Activities\TaskInterface;
use App\Traits\Crm\Activities\TaskTraits;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Create extends Component
{
    use TaskTraits, WithPagination, PopUpMessagesTrait;

    // public function confirmationSubmit(TaskInterface $task_interface)
    // {
    //     $response = $task_interface->createValidation($this->getRequest());

    //     if ($response['code'] == 200) {
    //         $this->confirmation_modal_task = true;
    //     } else if ($response['code'] == 400) {

    //         $this->resetErrorBag();
    //         foreach ($response['result']->getMessages() as $a => $messages) {
    //             if (is_array($messages)) {

    //                 foreach ($messages as $message) :
    //                     $this->addError($a, $message);
    //                 endforeach;
    //             } else {
    //                 $this->addError($a, $messages);
    //             }
    //         }
    //         return;
    //     } else {
    //         $this->sweetAlertError('error', $response['message'], $response['result']);
    //     }
    // }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.activities.index', 'close_modal', 'add');
        $this->confirmation_modal_task = false;
    }

    public function submit(TaskInterface $task_interface)
    {
        // dd($this->getRequest());
        $response = $task_interface->create($this->getRequest());
        if ($response['code'] == 200) {

            $this->resetForm();
            $this->confirmation_modal_task = false;
            $this->emitTo('crm.activities.index', 'close_modal', 'add');
            $this->emitTo('crm.activities.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.activities.task.create');
    }
}
