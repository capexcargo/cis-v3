<?php

namespace App\Http\Livewire\Crm\Activities;

use App\Traits\Crm\Activities\LogMeetingTraits;
use App\Traits\Crm\Activities\NotesTraits;
use App\Traits\Crm\Activities\TaskTraits;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use TaskTraits, NotesTraits, LogMeetingTraits, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal_task = true;
        } else if ($action_type == 'add_note') {
            $this->create_modal_note = true;
        } else if ($action_type == 'add_meeting') {
            $this->create_modal_log_meeting = true;
        }else if ($action_type == 'schedule_meeting') {
            // return view('livewire.crm.activities.schedule-meeting.index');
            return redirect()->to(route('crm.activities.schedule-meeting.index'));
            // $this->emitTo('crm.activities.schedule-meeting.index', 'index');
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'add') {
            $this->create_modal_task = false;
            $this->confirmation_modal_task = false;
        } else if ($action_type == 'notes') {
            $this->create_modal_note = false;
            $this->confirmation_modal_note = false;
        } else if ($action_type == 'meeting') {
            $this->create_modal_log_meeting = false;
            $this->confirmation_modal_meeting = false;
        }
    }

    public function render()
    {
        return view('livewire.crm.activities.index');
    }
}
