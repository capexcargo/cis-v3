<?php

namespace App\Http\Livewire\Crm\Activities\Notes;

use App\Interfaces\Crm\Activities\NotesInterface;
use App\Traits\Crm\Activities\NotesTraits;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;
use Mockery\Matcher\Not;

class Create extends Component
{
    use NotesTraits, WithPagination, PopUpMessagesTrait;

    // public function confirmationSubmit(NotesInterface $notes_interface)
    // {
    //     // dd($this->getRequestNotes());
    //     $response = $notes_interface->createValidation($this->getRequestNotes());

    //     if ($response['code'] == 200) {
    //         $this->confirmation_modal_note = true;
    //     } else if ($response['code'] == 400) {

    //         $this->resetErrorBag();
    //         foreach ($response['result']->getMessages() as $a => $messages) {
    //             if (is_array($messages)) {

    //                 foreach ($messages as $message) :
    //                     $this->addError($a, $message);
    //                 endforeach;
    //             } else {
    //                 $this->addError($a, $messages);
    //             }
    //         }
    //         return;
    //     } else {
    //         $this->sweetAlertError('error', $response['message'], $response['result']);
    //     }
    // }

    public function closecreatemodal()
    {
        $this->resetFormNotes();
        $this->resetErrorBag();
        $this->emitTo('crm.activities.index', 'close_modal', 'notes');
        $this->confirmation_modal_note = false;
    }

    public function submit(NotesInterface $notes_interface)
    {
        // dd($this->getRequest());
        $response = $notes_interface->create($this->getRequestNotes());
        if ($response['code'] == 200) {

            $this->resetFormNotes();
            $this->confirmation_modal_note = false;
            $this->emitTo('crm.activities.index', 'close_modal', 'notes');
            $this->emitTo('crm.activities.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.crm.activities.notes.create');
    }
}
