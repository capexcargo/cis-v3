<?php

namespace App\Http\Livewire\Crm\Reports\CdmReports;

use App\Interfaces\Crm\Reports\CdmReportsInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $paginate = 10;
    public $date_created_search;
    public $date_range_search;

    public $action_type;

    protected $listeners = ['index' => 'render'];

    public function search()
    {
        return [
            'paginate' => $this->paginate,
            'date' => $this->date_created_search,
            'date_range' => $this->date_range_search,
        ];
    }

    public function action($data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'see_customer_type') {
            return redirect()->route('crm.customer-information.customer-data-mgmt.index');
        } elseif ($action_type == 'see_account_status') {
            return redirect()->route('crm.customer-information.customer-data-mgmt.index');
        } elseif ($action_type == 'see_customer_category') {
            return redirect()->route('crm.customer-information.customer-data-mgmt.index');
        } elseif ($action_type == 'see_contact_lifestage') {
            return redirect()->route('crm.customer-information.customer-data-mgmt.index');
        } elseif ($action_type == 'see_marketing_channel') {
            return redirect()->route('crm.customer-information.marketing-channel-mgmt.index');
        } elseif ($action_type == 'see_onboarding_channel') {
            return redirect()->route('crm.customer-information.customer-data-mgmt.index');
        } elseif ($action_type == 'see_industry') {
            return redirect()->route('crm.customer-information.industry-mgmt.index');
        }
    }

    public function render(CdmReportsInterface $cdm_reports_interface)
    {
        $response = $cdm_reports_interface->index($this->search());
        // dd($response);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'count_individual' => [],
                    'count_corporate' => [],

                    'count_as_active' => [],
                    'count_as_inactive' => [],

                    'count_ls_lead' => [],
                    'count_ls_customer' => [],

                    'count_mc_social_media' => [],
                    'count_mc_google_search' => [],
                    'count_mc_digital_ads' => [],
                    'count_mc_email_marketing' => [],
                    'count_mc_catalog_flyers' => [],
                    'count_mc_outdoor_ads' => [],
                    'count_mc_capex_truck' => [],
                    'count_mc_events' => [],
                    'count_mc_direct_selling' => [],

                    'count_ind_food_industry' => [],
                    'count_ind_agriculture' => [],
                    'count_ind_construction' => [],
                    'count_ind_automotive' => [],
                    'count_ind_manufacturing' => [],
                    'count_ind_financial_services' => [],
                    'count_ind_transport' => [],
                    'count_ind_logistics' => [],
                    'count_ind_commerces' => [],

                    'contact_owners' => [],
                    'contacts_creation_report' => [],
                ];
        }

        $food_industry_new = $response['result']['industries'][0]['food_industry_new'] ?? 0;
        $food_industry_last_month = $response['result']['industries'][0]['food_industry_last_month'] ?? 0;
        if (($food_industry_new - $food_industry_last_month) > 0) {
            $food_industry_total_percentage = $food_industry_last_month ==  0 ? 100 : (int)round((($food_industry_new - $food_industry_last_month) / $food_industry_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        } else {
            $food_industry_total_percentage = ($food_industry_new - $food_industry_last_month) ==  0 ? 0 : (int)round((($food_industry_new - $food_industry_last_month) / $food_industry_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        }

        $agriculture_new = $response['result']['industries'][1]['agriculture_new'] ?? 0;
        $agriculture_last_month = $response['result']['industries'][1]['agriculture_last_month'] ?? 0;
        if (($agriculture_new - $agriculture_last_month) > 0) {
            $agriculture_total_percentage = $agriculture_last_month ==  0 ? 100 : (int)round((($agriculture_new - $agriculture_last_month) / $agriculture_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        } else {
            $agriculture_total_percentage = ($agriculture_new - $agriculture_last_month) ==  0 ? 0 : (int)round((($agriculture_new - $agriculture_last_month) / $agriculture_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        }

        $construction_new = $response['result']['industries'][2]['construction_new'] ?? 0;
        $construction_last_month = $response['result']['industries'][2]['construction_last_month'] ?? 0;
        if (($construction_new - $construction_last_month) > 0) {
            $construction_total_percentage = $construction_last_month ==  0 ? 100 : (int)round((($construction_new - $construction_last_month) / $construction_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        } else {
            $construction_total_percentage = ($construction_new - $construction_last_month) ==  0 ? 0 : (int)round((($construction_new - $construction_last_month) / $construction_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        }

        $automotive_new = $response['result']['industries'][3]['automotive_new'] ?? 0;
        $automotive_last_month = $response['result']['industries'][3]['automotive_last_month'] ?? 0;
        if (($automotive_new - $automotive_last_month) > 0) {
            $automotive_total_percentage = $automotive_last_month ==  0 ? 100 : (int)round((($automotive_new - $automotive_last_month) / $automotive_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        } else {
            $automotive_total_percentage = ($automotive_new - $automotive_last_month) ==  0 ? 0 : (int)round((($automotive_new - $automotive_last_month) / $automotive_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        }

        $manufacturing_new = $response['result']['industries'][4]['manufacturing_new'] ?? 0;
        $manufacturing_last_month = $response['result']['industries'][4]['manufacturing_last_month'] ?? 0;
        if (($manufacturing_new - $manufacturing_last_month) > 0) {
            $manufacturing_total_percentage = $manufacturing_last_month ==  0 ? 100 : (int)round((($manufacturing_new - $manufacturing_last_month) / $manufacturing_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        } else {
            $manufacturing_total_percentage = ($manufacturing_new - $manufacturing_last_month) ==  0 ? 0 : (int)round((($manufacturing_new - $manufacturing_last_month) / $manufacturing_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        }

        $financial_services_new = $response['result']['industries'][5]['financial_services_new'] ?? 0;
        $financial_services_last_month = $response['result']['industries'][5]['financial_services_last_month'] ?? 0;
        if (($financial_services_new - $financial_services_last_month) > 0) {
            $financial_services_total_percentage = $financial_services_last_month ==  0 ? 100 : (int)round((($financial_services_new - $financial_services_last_month) / $financial_services_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        } else {
            $financial_services_total_percentage = ($financial_services_new - $financial_services_last_month) ==  0 ? 0 : (int)round((($financial_services_new - $financial_services_last_month) / $financial_services_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        }

        $transport_new = $response['result']['industries'][6]['transport_new'] ?? 0;
        $transport_last_month = $response['result']['industries'][6]['transport_last_month'] ?? 0;
        if (($transport_new - $transport_last_month) > 0) {
            $transport_total_percentage = $transport_last_month ==  0 ? 100 : (int)round((($transport_new / $transport_last_month) / $transport_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        } else {
            $transport_total_percentage = ($transport_new - $transport_last_month) ==  0 ? 0 : (int)round((($transport_new - $transport_last_month) / $transport_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        }

        $logistics_new = $response['result']['industries'][7]['logistics_new'] ?? 0;
        $logistics_last_month = $response['result']['industries'][7]['logistics_last_month'] ?? 0;
        if (($logistics_new - $logistics_last_month) > 0) {
            $logistics_total_percentage = $logistics_last_month ==  0 ? 100 : (int)round((($logistics_new / $logistics_last_month) / $logistics_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        } else {
            $logistics_total_percentage = ($logistics_new - $logistics_last_month) ==  0 ? 0 : (int)round((($logistics_new - $logistics_last_month) / $logistics_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        }

        $commerces_new = $response['result']['industries'][8]['commerces_new'] ?? 0;
        $commerces_last_month = $response['result']['industries'][8]['commerces_last_month'] ?? 0;
        if (($commerces_new - $commerces_last_month) > 0) {
            $commerces_total_percentage = $commerces_last_month ==  0 ? 100 : (int)round((($commerces_new / $commerces_last_month) / $commerces_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        } else {
            $commerces_total_percentage = ($commerces_new - $commerces_last_month) ==  0 ? 0 : (int)round((($commerces_new - $commerces_last_month) / $commerces_last_month) * 100 / ($food_industry_new + $food_industry_last_month));
        }

        $food_industry = [];
        $agriculture = [];
        $construction = [];
        $automotive = [];
        $manufacturing = [];
        $financial_services = [];
        $transport = [];
        $logistics = [];
        $commerces = [];

        foreach ($response['result']['industries'] as $industry) {
            $food_industry[] = $industry->food_industry;
            $agriculture[] = $industry->agriculture;
            $construction[] = $industry->construction;
            $automotive[] = $industry->automotive;
            $manufacturing[] = $industry->manufacturing;
            $financial_services[] = $industry->financial_services;
            $transport[] = $industry->transport;
            $logistics[] = $industry->logistics;
            $commerces[] = $industry->commerces;
        }

        $total_customers_w_industries = array_sum(
            array_merge(
                $food_industry,
                $agriculture,
                $construction,
                $automotive,
                $manufacturing,
                $financial_services,
                $transport,
                $logistics,
                $commerces,
            )
        );

        $food_industry_percentage = array_sum($food_industry) != 0 ? array_sum($food_industry) / $total_customers_w_industries : 0;
        $agriculture_percentage = array_sum($agriculture) != 0 ?  array_sum($agriculture) / $total_customers_w_industries : 0;
        $construction_percentage = array_sum($construction) != 0 ?  array_sum($construction) / $total_customers_w_industries : 0;
        $automotive_percentage = array_sum($automotive) != 0 ?  array_sum($automotive) / $total_customers_w_industries : 0;
        $manufacturing_percentage = array_sum($manufacturing) != 0 ?  array_sum($manufacturing) / $total_customers_w_industries : 0;
        $financial_services_percentage = array_sum($financial_services) != 0 ?  array_sum($financial_services) / $total_customers_w_industries : 0;
        $transport_percentage = array_sum($transport) != 0 ?  array_sum($transport) / $total_customers_w_industries : 0;
        $logistics_percentage = array_sum($logistics) != 0 ?  array_sum($logistics) / $total_customers_w_industries : 0;
        $commerces_percentage = array_sum($commerces) != 0 ?  array_sum($commerces) / $total_customers_w_industries : 0;

        // dd($agriculture_percentage);
        $this->emit('updateChart', [
            'count_individual' => $response['result']['customer_types'][0]['individual'] ?? 0,
            'count_corporate' => $response['result']['customer_types'][1]['corporate'] ?? 0,
        ]);

        return view('livewire.crm.reports.cdm-reports.index', [
            'count_individual' => $response['result']['customer_types'][0]['individual'] ?? 0,
            'count_corporate' => $response['result']['customer_types'][1]['corporate'] ?? 0,

            'count_as_active' => $response['result']['account_status'][0]['active'] ?? 0,
            'count_as_inactive' => $response['result']['account_status'][1]['inactive'] ?? 0,

            'count_cc_active' => $response['result']['customer_categories'][0]['active'] ?? 0,
            'count_cc_inactive' => $response['result']['customer_categories'][1]['inactive'] ?? 0,
            'count_cc_prospect' => $response['result']['customer_categories'][1]['prospect'] ?? 0,

            'count_ls_lead' => $response['result']['life_stages'][0]['lead'] ?? 0,
            'count_ls_customer' => $response['result']['life_stages'][1]['customer'] ?? 0,

            'count_mc_social_media' => $response['result']['marketing_channels'][0]['social_media'] ?? 0,
            'count_mc_google_search' => $response['result']['marketing_channels'][1]['google_search'] ?? 0,
            'count_mc_digital_ads' => $response['result']['marketing_channels'][2]['digital_ads'] ?? 0,
            'count_mc_email_marketing' => $response['result']['marketing_channels'][3]['email_marketing'] ?? 0,
            'count_mc_catalog_flyers' => $response['result']['marketing_channels'][4]['catalog_flyers'] ?? 0,
            'count_mc_outdoor_ads' => $response['result']['marketing_channels'][5]['outdoor_ads'] ?? 0,
            'count_mc_capex_truck' => $response['result']['marketing_channels'][6]['capex_truck'] ?? 0,
            'count_mc_events' => $response['result']['marketing_channels'][7]['events'] ?? 0,
            'count_mc_direct_selling' => $response['result']['marketing_channels'][8]['direct_selling'] ?? 0,

            'count_ind_food_industry' => array_sum($food_industry),
            'count_ind_agriculture' => array_sum($agriculture),
            'count_ind_construction' => array_sum($construction),
            'count_ind_automotive' => array_sum($automotive),
            'count_ind_manufacturing' => array_sum($manufacturing),
            'count_ind_financial_services' => array_sum($financial_services),
            'count_ind_transport' => array_sum($transport),
            'count_ind_logistics' => array_sum($logistics),
            'count_ind_commerces' => array_sum($commerces),

            'prcntg_ind_food_industry' => round($food_industry_percentage * 100, 2),
            'prcntg_ind_agriculture' =>  round($agriculture_percentage * 100, 2),
            'prcntg_ind_construction' =>  round($construction_percentage * 100, 2),
            'prcntg_ind_automotive' =>  round($automotive_percentage * 100, 2),
            'prcntg_ind_manufacturing' =>  round($manufacturing_percentage * 100, 2),
            'prcntg_ind_financial_services' =>  round($financial_services_percentage * 100, 2),
            'prcntg_ind_transport' =>  round($transport_percentage * 100, 2),
            'prcntg_ind_logistics' => round($logistics_percentage * 100, 2),
            'prcntg_ind_commerces' =>  round($commerces_percentage * 100, 2),

            'food_industry_total_percentage' =>  $food_industry_total_percentage,
            'agriculture_total_percentage' =>  $agriculture_total_percentage,
            'construction_total_percentage' =>  $construction_total_percentage,
            'automotive_total_percentage' =>  $automotive_total_percentage,
            'manufacturing_total_percentage' =>  $manufacturing_total_percentage,
            'financial_services_total_percentage' =>  $financial_services_total_percentage,
            'transport_total_percentage' =>  $transport_total_percentage,
            'logistics_total_percentage' =>  $logistics_total_percentage,
            'commerces_total_percentage' =>  $commerces_total_percentage,

            'contact_owners' => $response['result']['contact_owners'] ?? [],
            'contacts_creation_report' => $response['result']['contacts_creation_report'] ?? 0,
        ]);
    }
}
