<?php

namespace App\Http\Livewire\Crm\Reports\QuotaReports;

use App\Interfaces\Crm\Sales\Quota\StakeholderCategoryInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $date;
    public $date_type;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;

    public $total_target_volume;
    public $total_target_amount;
    public $total_volume;
    public $actual_total_amount;

    public function search()
    {
        return [
            'date' => $this->date,
            'date_type' => $this->date_type,
            'paginate' => $this->paginate,
            'sortField' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
        ];
    }

    public function render(StakeholderCategoryInterface $stakeholder_category_interface)
    {
        $response = $stakeholder_category_interface->report($this->search());
        $target_amount = [];
        $actual_amount = [];
        
        foreach ($response['result']['stakeholder_categories'] as $i => $stakeholder_c) {
            $target_amount[] = $stakeholder_c->amnt;
            $actual_amount[] = $stakeholder_c->act_amnt;
        }
        $this->total_target_amount = number_format(array_sum($target_amount), 2);
        $this->actual_total_amount = number_format(array_sum($actual_amount), 2);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'stakeholder_categories' => [],
                ];
        }

        return view('livewire.crm.reports.quota-reports.index', [
            'stakeholder_categories' => $response['result']['stakeholder_categories'] ?? [],
        ]);
    }
}
