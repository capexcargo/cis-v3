<?php

namespace App\Http\Livewire\Crm\Reports\ActivityReports;

// use App\Traits\Crm\ActivityReports\ActivityReportsTrait;

use App\Models\Crm\CrmServiceRequest;
use App\Models\User;
use App\Traits\Crm\Reports\ActivityReports\ActivityReportsTrait;
use App\Traits\PopUpMessagesTrait;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use ActivityReportsTrait, WithPagination, PopUpMessagesTrait;

    public $agents = [];
    public $agent = [];
    public $agentss;
    public $agents_id;

    public $dataindex = [];
    public $dataindexs = [];

    public $AR_searchs = 1;
    public $date;

    protected $listeners = ['mount' => 'mount', 'index' => 'render'];

    public $view_modal = false;

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;


        if ($action_type == 'view') {
            $this->emitTo('crm.reports.activity-reports.view', 'view', $data);
            $this->dataindexs = ['date' => $this->date_created_search, 'id' => $data['id']];
            $this->agents_id = $data['id'];
            $this->view_modal = true;
        }
    }

    public function search()
    {

        // $this->agents  = User::with('ServiceReq')->whereHas('ServiceReq', function ($query) {
        //     $query->whereIn('status_idssss', [1,2,3])->when($this->date_created_search ?? false, function ($query) {
        //         $query->where('created_ats', '>=', '"' . $this->date_created_search . ' 00:00:00"')->where('created_at', '<=', '"' . $this->date_created_search . ' 23:59:59"');
        //     });
        // })->where('division_id', 2)->get();

        // dd($this->agents, $this->date_created_search);
        $this->mount();
        // dd($this->mount());
    }

    public function mount()
    {
        // dd($this->date_created_search);

        // $this->datefrom = '"' . $this->date_created_search . ' 00:00:00"';
        // $this->dateto = '"' . $this->date_created_search . ' 23:59:59"';

        // $this->agents  = User::withCount(['ServiceReq as open' => function ($query) {
        //     $query->whereIn('status_id', [1, 2])->when($this->date_created_search ?? false, function ($query) {
        //         $query->whereBetween("created_at", [$this->datefrom, $this->dateto]);
        //     });
        // }, 'ServiceReq as close' => function ($query) {
        //     $query->where('status_id', 3)->when($this->date_created_search ?? false, function ($query) {
        //         $query->whereBetween("created_at", [$this->datefrom, $this->dateto]);
        //     });
        // }, 'ServiceReq as total' => function ($query) {
        //     $query->whereIn('status_id', [1, 2, 3])->when($this->date_created_search ?? false, function ($query) {
        //         $query->whereBetween("created_at", [$this->datefrom, $this->dateto]);
        //     });
        // }])->where('division_id', 2)->get();
        
        // if($this->AR_searchs == 1) {

        if ($this->date_created_search) {
            $this->datefrom = ' and `created_at` >= "' . $this->date_created_search . ' 00:00:00" 
            and `created_at` <= "' . $this->date_created_search . ' 23:59:59"';
        } else {
            $this->datefrom = "";
        }

        $this->agentss = DB::select('select `users`.*, 
        (select count(*) from `crm_service_request`
        where `users`.`id` = `crm_service_request`.`assigned_to_id` 
        and `status_id` in (1, 2) ' . $this->datefrom . ') as `open`, 
        (select count(*) from `crm_service_request` 
        where `users`.`id` = `crm_service_request`.`assigned_to_id` 
        and `status_id` = 3 ' . $this->datefrom . ') as `close`, (
        select count(*) from `crm_service_request` 
        where `users`.`id` = `crm_service_request`.`assigned_to_id` 
        and `status_id` in (1, 2, 3) ' . $this->datefrom . ') as `total` from `users` where `division_id` = 2');

        $this->agents = collect($this->agentss);

        // }else if($this->AR_searchs == 2){
        //     $date1 = new DateTime(date($this->date_created_search));
        //     $date1->modify('+7 day');
        //     $this->date1 = $date1->format('Y-m-d');
        // }
    }



    public function render()
    {
        $this->mount();
        // $this->search();

        return view('livewire.crm.reports.activity-reports.index');
    }
}
