<?php

namespace App\Http\Livewire\Crm\Reports\ActivityReports;

use App\Models\Crm\CrmActivities;
use App\Models\Crm\CrmActivitiesReference;
use App\Models\User;
use App\Traits\Crm\Reports\ActivityReports\ActivityReportsTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class View extends Component
{
    use ActivityReportsTrait, WithPagination, PopUpMessagesTrait;

    public $tasks = [];
    public $taskss;
    public $actnames = [];
    public $name;
    public $datefiltered;

    protected $listeners = ['mount' => 'mount', 'index' => 'render'];


    public function mount($data)
    {
        // dd($data);
        $this->actnames = User::where('id', $data['id'])->get();
        // dd($this->actnames);

        foreach ($this->actnames as $names) {
            $this->name = $names->name;
        }
        // dd($this->name, $data['id'], $data);

        $id = $data['id'];

        $this->datefiltered = $data['date'];
        // dd($id);

        if ($data['date']) {
            $this->datefrom = ' and `crm_service_request`.`created_at` >= "' . $data['date'] . ' 00:00:00" 
            and `crm_service_request`.`created_at` <= "' . $data['date'] . ' 23:59:59"';
        } else {
            $this->datefrom = "";
        }

        // $this->tasks  = CrmActivitiesReference::withCount(['ServiceRequestget as opened' => function ($query) use ($id) {
        //     $query->whereIn('status_id', [1, 2])->where('assigned_to_id', $id);
        // }, 'ServiceRequestget as closed' => function ($query) use ($id) {
        //     $query->where('status_id', 3)->where('assigned_to_id', $id);
        // }, 'ServiceRequestget as totals' => function ($query) use ($id) {
        //     $query->whereIn('status_id', [1, 2, 3])->where('assigned_to_ids', $id);
        // }])->get();



        $this->taskss = DB::select('select `crm_activities_references`.*, 
        (select count(*) from `crm_service_request` 
        inner join `crm_activities` on `crm_activities`.`sr_no` = `crm_service_request`.`sr_no` 
        where `crm_activities_references`.`id` = `crm_activities`.`activity_type_id` 
        and `status_id` in (1, 2) and `assigned_to_id` = ' . $id . ' 
        
        ' . $this->datefrom . '
        
        ) as `opened`, 
        (select count(*) from `crm_service_request` inner join `crm_activities` 
        on `crm_activities`.`sr_no` = `crm_service_request`.`sr_no` 
        where `crm_activities_references`.`id` = `crm_activities`.`activity_type_id` 
        and `status_id` = 3 and `assigned_to_id` = ' . $id . '  
        
        ' . $this->datefrom . '
        
        ) as `closed`, 
        (select count(*) from `crm_service_request` inner join `crm_activities` 
        on `crm_activities`.`sr_no` = `crm_service_request`.`sr_no` 
        where `crm_activities_references`.`id` = `crm_activities`.`activity_type_id` 
        and `status_id` in (1, 2, 3) and `assigned_to_id` = ' . $id . '  
        
        ' . $this->datefrom . '
        
        ) as `totals` 
        from `crm_activities_references`');

        $this->tasks = collect($this->taskss);

        // dd($this->tasks, $id);
    }

    public function render()
    {
        return view('livewire.crm.reports.activity-reports.view');
    }
}
