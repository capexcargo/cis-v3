<?php

namespace App\Http\Livewire\Crm\Reports\BookingReports;

use App\Models\CrmBooking;
use App\Models\CrmBookingLogs;
use App\Models\CrmBookingType;
use App\Traits\Crm\Reports\BookingReports\BookingReportsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class ViewTotalResched extends Component
{
    use BookingReportsTrait, PopUpMessagesTrait;

    public $logs = [];

    protected $listeners = ['mount' => 'mount', 'index' => 'render'];

    public function mount($data){

        // dd($data['id']);

        $this->logs = CrmBookingLogs::with('ReschedReasonReferenceLG')->where('status_id', 6)->where('booking_id', $data)->get();

        // dd($this->logs[0]['ReschedReasonReferenceLG']);


    }

    public function render()
    {
        return view('livewire.crm.reports.booking-reports.view-total-resched');
    }
}
