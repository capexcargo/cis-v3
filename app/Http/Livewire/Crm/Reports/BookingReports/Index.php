<?php

namespace App\Http\Livewire\Crm\Reports\BookingReports;

use App\Models\BranchReference;
use App\Models\Crm\CityReference;
use App\Models\CrmBooking;
use App\Models\CrmBookingFailedReasonReference;
use App\Models\CrmBookingHistory;
use App\Models\CrmBookingLogs;
use App\Models\CrmBookingShipper;
use App\Models\CrmBookingStatusReference;
use App\Models\CrmRateApplyFor;
use App\Models\User;
use App\Traits\Crm\Reports\BookingReports\BookingReportsTrait;
use App\Traits\PopUpMessagesTrait;
use CreateCrmBookingFailedReasonReference;
use DateTime;
use Livewire\Component;

class Index extends Component
{
    use BookingReportsTrait, PopUpMessagesTrait;

    public $view_total_resched_modal = false;

    public $total;
    public $labelcis3;
    public $totalcis3;
    public $avgcis3;

    public $bookinghistorycis3 = [];
    public $bookinghistoryweb = [];
    public $bookinghistorymobile = [];
    public $bookinghistoryvip = [];

    public $labelweb;
    public $totalweb;
    public $avgweb;

    public $labelmobile;
    public $totalmobile;
    public $avgmob;

    public $labelvip;
    public $totalvip;
    public $avgvip;

    public $totalresched;
    public $rescheduled = [];
    public $resch_id;

    public $totalcancelled;
    public $cancelledr1s = [];
    public $reason1;
    public $reason1count;
    public $cancelledr2s = [];
    public $reason2;
    public $reason2count;
    public $cancelledr3s = [];
    public $reason3;
    public $reason3count;
    public $cancelledr4s = [];
    public $reason4;
    public $reason4count;
    public $cancelledr5s = [];
    public $reason5;
    public $reason5count;
    public $cancelledr6s = [];
    public $reason6;
    public $reason6count;
    public $cancelledr7s = [];
    public $reason7;
    public $reason7count;
    public $cancelledr8s = [];
    public $reason8;
    public $reason8count;
    public $cancelledr9s = [];
    public $reason9;
    public $reason9count;
    public $cancelledr10s = [];
    public $reason10;
    public $reason10count;
    public $cancelledr11s = [];
    public $reason11;
    public $reason11count;

    public $totalpickup;
    public $totaljan;
    public $totalfeb;
    public $totalmar;
    public $totalapr;
    public $totalmay;
    public $totaljun;
    public $totaljul;
    public $totalaug;
    public $totalsept;
    public $totaloct;
    public $totalnov;
    public $totaldec;

    public $bookingperagents = [];
    public $totalbook;

    public $bookingperbranches = [];
    public $totalbookb;

    public $totalindividual;
    public $totalcorporate;
    public $totalctd;
    public $individual;
    public $corporate;

    public $cdate = [];
    public $completeddates = [];
    public $completeddates2 = [];
    public $completed;
    public $advanced;
    public $resched;
    public $cancel;

    public $bookingvolumeperchannels = [];
    public $totalBVPC;

    public $BH_search;
    public $BH_searchs = 1;
    public $RR_search;
    public $CR_search;
    public $CR_searchs = 1;
    public $PACR_search;
    public $BPA_search;
    public $BPB_search;
    public $BVPC_search;
    public $CTD_search;

    public $date1;
    public $date2;
    public $date4;
    public $color;

    public $totalheatmap;
    public $heatmaps = [];

    public $totalBookingHeatmap = [];
    public $postalcodes = [];
    public $latlngs = [];

    public $HM_search;
    public $HM_searchs = 1;

    public $totalsarray = [];
    public $totalsarray2 = [];
    public $totalsarray3 = [];



    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'mount' => 'mount', 'index' => 'render'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'view_total_resched') {
            $this->resch_id = $data['id'];
            $this->view_total_resched_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'view_total_resched') {
            $this->view_total_resched_modal = false;
        }
    }

    // public function mount()
    // {
    // }




    public function searchBH()
    {
        //////////////////////////////////////////////////////////// BOOKING HISTORY ////////////////////////////////////////////////////////
        if ($this->BH_searchs == 1) {
            $this->completeddates = CrmBookingHistory::whereIn('final_status_id', [5, 8, 6, 7])
                ->groupByRaw('date(created_at)')
                ->selectRaw('SUM(if(final_status_id = 5, 1, 0)) as completed,
                SUM(if(final_status_id = 8, 1, 0)) as advanced,
                SUM(if(final_status_id = 6, 1, 0)) as resched,
                SUM(if(final_status_id = 7, 1, 0)) as cancel,
                created_at')->when($this->BH_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->BH_search . '%');
                })->get();

            $bookinghistory = CrmBookingHistory::when($this->BH_search ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->BH_search . '%');
            })->get();

            $this->bookinghistorycis3 = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 1)->when($this->BH_search ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->BH_search . '%');
            })->get();

            $bookinghistorycis3av = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 1)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as cis3ave')->when($this->BH_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->BH_search . '%');
                })->first();

            $this->bookinghistoryweb = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 2)->when($this->BH_search ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->BH_search . '%');
            })->get();

            $bookinghistorywebav = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 2)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as webave')->when($this->BH_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->BH_search . '%');
                })->first();

            $this->bookinghistorymobile = CrmBookingHistory::where('booking_channel', 3)->when($this->BH_search ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->BH_search . '%');
            })->get();

            $bookinghistorymobav = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 3)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as mobave')->when($this->BH_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->BH_search . '%');
                })->first();

            $this->bookinghistoryvip = CrmBookingHistory::where('booking_channel', 4)->when($this->BH_search ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->BH_search . '%');
            })->get();

            $bookinghistoryvipav = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 4)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as vipave')->when($this->BH_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->BH_search . '%');
                })->first();
        } else if ($this->BH_searchs == 2) {
            $date1 = new DateTime(date($this->BH_search));
            $date1->modify('+7 day');
            $this->date1 = $date1->format('Y-m-d');

            $this->completeddates = CrmBookingHistory::whereIn('final_status_id', [5, 8, 6, 7])
                ->groupByRaw('date(created_at)')
                ->selectRaw('SUM(if(final_status_id = 5, 1, 0)) as completed,
            SUM(if(final_status_id = 8, 1, 0)) as advanced,
            SUM(if(final_status_id = 6, 1, 0)) as resched,
            SUM(if(final_status_id = 7, 1, 0)) as cancel,
            created_at')->when($this->BH_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
                })->get();

            $bookinghistory = CrmBookingHistory::when($this->BH_search ?? false, function ($query) {
                $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
            })->get();

            $this->bookinghistorycis3 = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 1)->when($this->BH_search ?? false, function ($query) {
                $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
            })->get();

            $bookinghistorycis3av = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 1)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as cis3ave')->when($this->BH_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
                })->first();

            $this->bookinghistoryweb = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 2)->when($this->BH_search ?? false, function ($query) {
                $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
            })->get();

            $bookinghistorywebav = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 2)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as webave')->when($this->BH_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
                })->first();

            $this->bookinghistorymobile = CrmBookingHistory::where('booking_channel', 3)->when($this->BH_search ?? false, function ($query) {
                $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
            })->get();

            $bookinghistorymobav = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 3)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as mobave')->when($this->BH_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
                })->first();

            $this->bookinghistoryvip = CrmBookingHistory::where('booking_channel', 4)->when($this->BH_search ?? false, function ($query) {
                $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
            })->get();

            $bookinghistoryvipav = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 4)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as vipave')->when($this->BH_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
                })->first();
        } else if ($this->BH_searchs == 3) {
            $date1 = new DateTime(date($this->BH_search));
            $date1->modify('+30 day');
            $this->date1 = $date1->format('Y-m-d');

            $this->completeddates = CrmBookingHistory::whereIn('final_status_id', [5, 8, 6, 7])
                ->groupByRaw('date(created_at)')
                ->selectRaw('SUM(if(final_status_id = 5, 1, 0)) as completed,
            SUM(if(final_status_id = 8, 1, 0)) as advanced,
            SUM(if(final_status_id = 6, 1, 0)) as resched,
            SUM(if(final_status_id = 7, 1, 0)) as cancel,
            created_at')->when($this->BH_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
                })->get();

            $bookinghistory = CrmBookingHistory::when($this->BH_search ?? false, function ($query) {
                $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
            })->get();

            $this->bookinghistorycis3 = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 1)->when($this->BH_search ?? false, function ($query) {
                $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
            })->get();

            $bookinghistorycis3av = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 1)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as cis3ave')->when($this->BH_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
                })->first();

            $this->bookinghistoryweb = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 2)->when($this->BH_search ?? false, function ($query) {
                $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
            })->get();

            $bookinghistorywebav = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 2)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as webave')->when($this->BH_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
                })->first();

            $this->bookinghistorymobile = CrmBookingHistory::where('booking_channel', 3)->when($this->BH_search ?? false, function ($query) {
                $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
            })->get();

            $bookinghistorymobav = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 3)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as mobave')->when($this->BH_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
                })->first();

            $this->bookinghistoryvip = CrmBookingHistory::where('booking_channel', 4)->when($this->BH_search ?? false, function ($query) {
                $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
            })->get();

            $bookinghistoryvipav = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 4)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as vipave')->when($this->BH_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->BH_search, $this->date1]);
                })->first();
        } else {
            $this->completeddates = CrmBookingHistory::whereIn('final_status_id', [5, 8, 6, 7])
                ->groupByRaw('date(created_at)')
                ->selectRaw('SUM(if(final_status_id = 5, 1, 0)) as completed,
            SUM(if(final_status_id = 8, 1, 0)) as advanced,
            SUM(if(final_status_id = 6, 1, 0)) as resched,
            SUM(if(final_status_id = 7, 1, 0)) as cancel,
            created_at')->get();

            $bookinghistory = CrmBookingHistory::get();

            $this->bookinghistorycis3 = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 1)->get();

            $bookinghistorycis3av = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 1)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as cis3ave')->first();

            $this->bookinghistoryweb = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 2)->get();

            $bookinghistorywebav = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 2)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as webave')->first();

            $this->bookinghistorymobile = CrmBookingHistory::where('booking_channel', 3)->get();

            $bookinghistorymobav = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 3)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as mobave')->first();

            $this->bookinghistoryvip = CrmBookingHistory::where('booking_channel', 4)->get();

            $bookinghistoryvipav = CrmBookingHistory::with('BookingChannelReferenceBK')->where('booking_channel', 4)
                ->selectRaw('SUM(TIMESTAMPDIFF(MINUTE, pickup_date, completed_date)) / COUNT(*) as vipave')->first();
        }

        $getdata = [];
        foreach ($this->completeddates as $i => $completeddate) {
            $asdasd = date('m/d/Y l', strtotime($completeddate->created_at));

            $getdata[] = ["'$asdasd'", $completeddate->completed, $completeddate->advanced, $completeddate->resched, $completeddate->cancel];
        }
        $this->completeddates2 = $getdata;

        //////////////////////////////////////////////////////////// BOOKING CHANNEL ////////////////////////////////////////////////////////
        $this->total = count($bookinghistory);

        foreach ($this->bookinghistorycis3 as $p => $bookinghistorycis) {
            $this->labelcis3 = $bookinghistorycis->BookingChannelReferenceBK->name;
        }
        $this->totalcis3 = count($this->bookinghistorycis3);

        $this->avgcis3 = $bookinghistorycis3av->cis3ave;

        foreach ($this->bookinghistoryweb as $p => $bookinghistorywe) {
            $this->labelweb = $bookinghistorywe->BookingChannelReferenceBK->name;
        }
        $this->totalweb = count($this->bookinghistoryweb);

        $this->avgweb = $bookinghistorywebav->webave;

        foreach ($this->bookinghistorymobile as $p => $bookinghistorymobil) {
            $this->labelmobile = $bookinghistorymobil->BookingChannelReferenceBK->name;
        }
        $this->totalmobile = count($this->bookinghistorymobile);

        $this->avgmob = $bookinghistorymobav->mobave;

        foreach ($this->bookinghistoryvip as $p => $bookinghistoryvi) {
            $this->labelvip = $bookinghistoryvi->BookingChannelReferenceBK->name;
        }
        $this->totalvip = count($this->bookinghistoryvip);

        $this->avgvip = $bookinghistoryvipav->vipave;


        $this->totalsarray = [$this->totalcis3, $this->totalweb, $this->totalmobile, $this->totalvip];
        $this->emit('totalsarray',  $this->totalsarray);
    }

    public function searchCR()
    {
        //////////////////////////////////////////////////////////// RESHCEDULE REPORT ////////////////////////////////////////////////////////

        $this->rescheduled = CrmBooking::withCount(['BookingLogsHasManyBK' => function ($query) {
            $query->with('ReschedReasonReferenceLG')->groupBy('booking_id')->where('status_id', 6);
        }])->with(['BookingLogsHasManyBK' => function ($query) {
            $query->with('ReschedReasonReferenceLG')->groupBy('booking_id')->where('status_id', 6);
        }])->where('final_status_id', 6)->when($this->RR_search ?? false, function ($query) {
            $query->where('booking_reference_no', 'like', '%' . $this->RR_search . '%');
        })->get();

        // dd($this->rescheduled);


        //////////////////////////////////////////////////////////// CANCELLATION REPORT ////////////////////////////////////////////////////////

        if ($this->CR_searchs == 1) {
            $totalrescheds = CrmBookingLogs::with('ReschedReasonReferenceLG')->where('status_id', 6)->when($this->CR_search ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->CR_search . '%');
            })->get();

            // dd($totalrescheds);

            $totalcancels = CrmBookingLogs::with('CancelReasonReferenceLG')->where('status_id', 7)
                ->when($this->CR_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->CR_search . '%');
                })->get();

            $this->cancelledr1s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->CR_search . '%');
                });
            }])->where('id', 1)->get();

            $this->cancelledr2s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->CR_search . '%');
                });
            }])->where('id', 2)->get();

            $this->cancelledr3s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->CR_search . '%');
                });
            }])->where('id', 3)->get();

            $this->cancelledr4s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->CR_search . '%');
                });
            }])->where('id', 4)->get();

            $this->cancelledr5s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->CR_search . '%');
                });
            }])->where('id', 5)->get();

            $this->cancelledr6s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->CR_search . '%');
                });
            }])->where('id', 6)->get();

            $this->cancelledr7s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->CR_search . '%');
                });
            }])->where('id', 7)->get();

            $this->cancelledr8s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->CR_search . '%');
                });
            }])->where('id', 8)->get();

            $this->cancelledr9s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->CR_search . '%');
                });
            }])->where('id', 9)->get();

            $this->cancelledr10s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->CR_search . '%');
                });
            }])->where('id', 10)->get();

            $this->cancelledr11s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->CR_search . '%');
                });
            }])->where('id', 11)->get();
        } else if ($this->CR_searchs == 2) {
            $date2 = new DateTime(date($this->CR_search));
            $date2->modify('+7 day');
            $this->date2 = $date2->format('Y-m-d');

            $totalrescheds = CrmBookingLogs::with('ReschedReasonReferenceLG')->where('status_id', 6)->when($this->CR_search ?? false, function ($query) {
                $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
            })->get();

            $totalcancels = CrmBookingLogs::with('CancelReasonReferenceLG')->where('status_id', 7)
                ->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                })->get();

            $this->cancelledr1s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 1)->get();

            $this->cancelledr2s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 2)->get();

            $this->cancelledr3s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 3)->get();

            $this->cancelledr4s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 4)->get();

            $this->cancelledr5s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 5)->get();

            $this->cancelledr6s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 6)->get();

            $this->cancelledr7s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 7)->get();

            $this->cancelledr8s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 8)->get();

            $this->cancelledr9s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 9)->get();

            $this->cancelledr10s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 10)->get();

            $this->cancelledr11s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 11)->get();
        } else if ($this->CR_searchs == 3) {
            $date2 = new DateTime(date($this->CR_search));
            $date2->modify('+30 day');
            $this->date2 = $date2->format('Y-m-d');

            $totalrescheds = CrmBookingLogs::with('ReschedReasonReferenceLG')->where('status_id', 6)->when($this->CR_search ?? false, function ($query) {
                $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
            })->get();

            $totalcancels = CrmBookingLogs::with('CancelReasonReferenceLG')->where('status_id', 7)
                ->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                })->get();

            $this->cancelledr1s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 1)->get();

            $this->cancelledr2s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 2)->get();

            $this->cancelledr3s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 3)->get();

            $this->cancelledr4s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 4)->get();

            $this->cancelledr5s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 5)->get();

            $this->cancelledr6s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 6)->get();

            $this->cancelledr7s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 7)->get();

            $this->cancelledr8s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 8)->get();

            $this->cancelledr9s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 9)->get();

            $this->cancelledr10s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 10)->get();

            $this->cancelledr11s = CrmBookingFailedReasonReference::withCount(['CancelledReason' => function ($query) {
                $query->when($this->CR_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->CR_search, $this->date2]);
                });
            }])->where('id', 11)->get();
        } else {
            $totalrescheds = CrmBookingLogs::with('ReschedReasonReferenceLG')->where('status_id', 6)->get();
            $totalcancels = CrmBookingLogs::with('CancelReasonReferenceLG')->where('status_id', 7)->get();
            $this->cancelledr1s = CrmBookingFailedReasonReference::withCount('CancelledReason')->where('id', 1)->get();
            $this->cancelledr2s = CrmBookingFailedReasonReference::withCount('CancelledReason')->where('id', 2)->get();
            $this->cancelledr3s = CrmBookingFailedReasonReference::withCount('CancelledReason')->where('id', 3)->get();
            $this->cancelledr4s = CrmBookingFailedReasonReference::withCount('CancelledReason')->where('id', 4)->get();
            $this->cancelledr5s = CrmBookingFailedReasonReference::withCount('CancelledReason')->where('id', 5)->get();
            $this->cancelledr6s = CrmBookingFailedReasonReference::withCount('CancelledReason')->where('id', 6)->get();
            $this->cancelledr7s = CrmBookingFailedReasonReference::withCount('CancelledReason')->where('id', 7)->get();
            $this->cancelledr8s = CrmBookingFailedReasonReference::withCount('CancelledReason')->where('id', 8)->get();
            $this->cancelledr9s = CrmBookingFailedReasonReference::withCount('CancelledReason')->where('id', 9)->get();
            $this->cancelledr10s = CrmBookingFailedReasonReference::withCount('CancelledReason')->where('id', 10)->get();
            $this->cancelledr11s = CrmBookingFailedReasonReference::withCount('CancelledReason')->where('id', 11)->get();
        }
        $this->totalresched = count($totalrescheds);
        $this->totalcancelled = count($totalcancels);

        foreach ($this->cancelledr1s as $q => $cancelledr1) {
            $this->reason1 = $cancelledr1->name;
            $this->reason1count = $cancelledr1->cancelled_reason_count;
        }

        foreach ($this->cancelledr2s as $q => $cancelledr2) {
            $this->reason2 = $cancelledr2->name;
            $this->reason2count = $cancelledr2->cancelled_reason_count;
        }

        foreach ($this->cancelledr3s as $q => $cancelledr3) {
            $this->reason3 = $cancelledr3->name;
            $this->reason3count = $cancelledr3->cancelled_reason_count;
        }

        foreach ($this->cancelledr4s as $q => $cancelledr4) {
            $this->reason4 = $cancelledr4->name;
            $this->reason4count = $cancelledr4->cancelled_reason_count;
        }

        foreach ($this->cancelledr5s as $q => $cancelledr5) {
            $this->reason5 = $cancelledr5->name;
            $this->reason5count = $cancelledr5->cancelled_reason_count;
        }

        foreach ($this->cancelledr6s as $q => $cancelledr6) {
            $this->reason6 = $cancelledr6->name;
            $this->reason6count = $cancelledr6->cancelled_reason_count;
        }

        foreach ($this->cancelledr7s as $q => $cancelledr7) {
            $this->reason7 = $cancelledr7->name;
            $this->reason7count = $cancelledr7->cancelled_reason_count;
        }

        foreach ($this->cancelledr8s as $q => $cancelledr8) {
            $this->reason8 = $cancelledr8->name;
            $this->reason8count = $cancelledr8->cancelled_reason_count;
        }

        foreach ($this->cancelledr9s as $q => $cancelledr9) {
            $this->reason9 = $cancelledr9->name;
            $this->reason9count = $cancelledr9->cancelled_reason_count;
        }

        foreach ($this->cancelledr10s as $q => $cancelledr10) {
            $this->reason10 = $cancelledr10->name;
            $this->reason10count = $cancelledr10->cancelled_reason_count;
        }

        foreach ($this->cancelledr11s as $q => $cancelledr11) {
            $this->reason11 = $cancelledr11->name;
            $this->reason11count = $cancelledr11->cancelled_reason_count;
        }

        $this->totalsarray2 = [
            $this->reason1count, $this->reason2count, $this->reason3count, $this->reason4count, $this->reason5count,
            $this->reason6count, $this->reason7count, $this->reason8count, $this->reason9count, $this->reason10count, $this->reason11count
        ];
        $this->emit('totalsarray2',  $this->totalsarray2);
    }

    public function searchPACR()
    {
        //////////////////////////////////////////////////////////// Pick up Activity Completion Rate ////////////////////////////////////////////////////////
        $pickupdate = CrmBooking::when($this->PACR_search ?? false, function ($query) {
            $query->where('pickup_date', 'like', '%' . $this->PACR_search . '%');
        })->get();
        $this->totalpickup = count($pickupdate);

        $jan = CrmBooking::whereMonth('pickup_date', '=', 1)->when($this->PACR_search ?? false, function ($query) {
            $query->where('pickup_date', 'like', '%' . $this->PACR_search . '%');
        })->get();
        $this->totaljan = count($jan);

        $feb = CrmBooking::whereMonth('pickup_date', '=', 2)->when($this->PACR_search ?? false, function ($query) {
            $query->where('pickup_date', 'like', '%' . $this->PACR_search . '%');
        })->get();
        $this->totalfeb = count($feb);

        $mar = CrmBooking::whereMonth('pickup_date', '=', 3)->when($this->PACR_search ?? false, function ($query) {
            $query->where('pickup_date', 'like', '%' . $this->PACR_search . '%');
        })->get();
        $this->totalmar = count($mar);

        $apr = CrmBooking::whereMonth('pickup_date', '=', 4)->when($this->PACR_search ?? false, function ($query) {
            $query->where('pickup_date', 'like', '%' . $this->PACR_search . '%');
        })->get();
        $this->totalapr = count($apr);

        $may = CrmBooking::whereMonth('pickup_date', '=', 5)->when($this->PACR_search ?? false, function ($query) {
            $query->where('pickup_date', 'like', '%' . $this->PACR_search . '%');
        })->get();
        $this->totalmay = count($may);

        $jun = CrmBooking::whereMonth('pickup_date', '=', 6)->when($this->PACR_search ?? false, function ($query) {
            $query->where('pickup_date', 'like', '%' . $this->PACR_search . '%');
        })->get();
        $this->totaljun = count($jun);

        $jul = CrmBooking::whereMonth('pickup_date', '=', 7)->when($this->PACR_search ?? false, function ($query) {
            $query->where('pickup_date', 'like', '%' . $this->PACR_search . '%');
        })->get();
        $this->totaljul = count($jul);

        $aug = CrmBooking::whereMonth('pickup_date', '=', 8)->when($this->PACR_search ?? false, function ($query) {
            $query->where('pickup_date', 'like', '%' . $this->PACR_search . '%');
        })->get();
        $this->totalaug = count($aug);

        $sept = CrmBooking::whereMonth('pickup_date', '=', 9)->when($this->PACR_search ?? false, function ($query) {
            $query->where('pickup_date', 'like', '%' . $this->PACR_search . '%');
        })->get();
        $this->totalsept = count($sept);

        $oct = CrmBooking::whereMonth('pickup_date', '=', 10)->when($this->PACR_search ?? false, function ($query) {
            $query->where('pickup_date', 'like', '%' . $this->PACR_search . '%');
        })->get();
        $this->totaloct = count($oct);

        $nov = CrmBooking::whereMonth('pickup_date', '=', 11)->when($this->PACR_search ?? false, function ($query) {
            $query->where('pickup_date', 'like', '%' . $this->PACR_search . '%');
        })->get();
        $this->totalnov = count($nov);

        $dec = CrmBooking::whereMonth('pickup_date', '=', 12)->when($this->PACR_search ?? false, function ($query) {
            $query->where('pickup_date', 'like', '%' . $this->PACR_search . '%');
        })->get();
        $this->totaldec = count($dec);
    }

    public function searchBPA()
    {
        //////////////////////////////////////////////////////////// BOOKING PER AGENT ////////////////////////////////////////////////////////
        $bookingperagent = CrmBooking::with('CreatedByBK')->when($this->BPA_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->BPA_search . '%');
        })->get();
        $this->totalbook = count($bookingperagent);

        $this->bookingperagents = User::withCount(['bookingscreated' => function ($query) {
            $query->when($this->BPA_search ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->BPA_search . '%');
            });
        }])->get();
    }

    public function searchBPB()
    {
        //////////////////////////////////////////////////////////// BOOKING PER BRANCH ////////////////////////////////////////////////////////
        $bookingperbranch = CrmBooking::with('BookingBranchReferenceBK')->when($this->BPB_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->BPB_search . '%');
        })->get();
        $this->totalbookb = count($bookingperbranch);

        $this->bookingperbranches = BranchReference::withCount(['bookings' => function ($query) {
            $query->when($this->BPB_search ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->BPB_search . '%');
            });
        }])->get();
    }

    public function searchBVPC()
    {
        //////////////////////////////////////////////////////////// BOOKING VOLUME PER CHANNEL ////////////////////////////////////////////////////////
        $this->bookingvolumeperchannels = CrmRateApplyFor::withCount(['BookingChannel' => function ($query) {
            $query->when($this->BVPC_search ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->BVPC_search . '%');
            });
        }])->get();

        $bookingvolume = CrmBooking::when($this->BVPC_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->BVPC_search . '%');
        })->get();
        $this->totalBVPC = count($bookingvolume);
    }

    public $ctdindividuals = [];
    public $ctdcorporates = [];

    public function searchCTD()
    {
        //////////////////////////////////////////////////////////// CUSTOMER TYPE DISTRIBUTION ////////////////////////////////////////////////////////
        $ctd = CrmBookingShipper::when($this->CTD_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->CTD_search . '%');
        })->get();
        $this->totalctd = count($ctd);

        $this->ctdindividuals = CrmBookingShipper::with('AccountTypeReferenceSP')->where('account_type_id', 1)->when($this->CTD_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->CTD_search . '%');
        })->get();

        foreach ($this->ctdindividuals as $z => $ctdindividual) {
            $this->individual = $ctdindividual->AccountTypeReferenceSP->name;
        }

        // $this->individual = $this->ctdindividuals[0]['AccountTypeReferenceSP']['name'];
        $this->totalindividual = count($this->ctdindividuals);

        $this->ctdcorporates = CrmBookingShipper::with('AccountTypeReferenceSP')->where('account_type_id', 2)->when($this->CTD_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->CTD_search . '%');
        })->get();

        foreach ($this->ctdcorporates as $z => $ctdcorporate) {
            $this->corporate = $ctdcorporate->AccountTypeReferenceSP->name;
        }

        // $this->corporate = $this->ctdcorporates[0]['AccountTypeReferenceSP']['name'];
        $this->totalcorporate = count($this->ctdcorporates);

        $this->totalsarray3 = [
            $this->totalindividual, $this->totalcorporate,
        ];
        $this->emit('totalsarray3',  $this->totalsarray3);
    }

    public function searchHM()
    {

        if ($this->HM_searchs == 1) {

            $totalBookingHeatmaps = CrmBookingShipper::when($this->HM_search ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->HM_search . '%');
            })->get();
            $this->totalheatmap = count($totalBookingHeatmaps);

            $this->heatmaps = CityReference::with('BookingCity')->withCount(['BookingCity' => function ($query) {
                $query->when($this->HM_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->HM_search . '%');
                });
            }])->get();
        } else if ($this->HM_searchs == 2) {

            $date4 = new DateTime(date($this->HM_search));
            $date4->modify('+7 day');
            $this->date4 = $date4->format('Y-m-d');

            $totalBookingHeatmaps = CrmBookingShipper::when($this->HM_search ?? false, function ($query) {
                $query->whereBetween('created_at', [$this->HM_search, $this->date4]);
            })->get();
            $this->totalheatmap = count($totalBookingHeatmaps);

            $this->heatmaps = CityReference::with('BookingCity')->withCount(['BookingCity' => function ($query) {
                $query->when($this->HM_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->HM_search, $this->date4]);
                });
            }])->get();
        } else if ($this->HM_searchs == 3) {

            $date4 = new DateTime(date($this->HM_search));
            $date4->modify('+30 day');
            $this->date4 = $date4->format('Y-m-d');

            $totalBookingHeatmaps = CrmBookingShipper::when($this->HM_search ?? false, function ($query) {
                $query->whereBetween('created_at', [$this->HM_search, $this->date4]);
            })->get();
            $this->totalheatmap = count($totalBookingHeatmaps);

            $this->heatmaps = CityReference::with('BookingCity')->withCount(['BookingCity' => function ($query) {
                $query->when($this->HM_search ?? false, function ($query) {
                    $query->whereBetween('created_at', [$this->HM_search, $this->date4]);
                });
            }])->get();
        }

        ////////////////////////////////////////////////// Heat Map ///////////////////////////////////////////////////////////////////////////
        $this->totalBookingHeatmap = CrmBookingShipper::with('ShipperCity')->withCount(['ShipperCity' => function ($query) {
            $query->with('BookingCity');
        }])->get();

        foreach ($this->totalBookingHeatmap as $x => $postal) {
            if (isset($postal->ShipperCity->name)) {
                array_push($this->postalcodes, $postal->ShipperCity->name . $postal->ShipperCity->city_postal);
            }
        }
        foreach ($this->postalcodes as $postal) {
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($postal) . "&sensor=false&key=" . env('GOOGLE_API_KEY');
            $result_string = file_get_contents($url);
            $result = json_decode($result_string, true);
            $status = $result['status'];
            if ($status != "ZERO_RESULTS") {
                $location = $result['results'][0]['geometry']['location'];

                array_push($this->latlngs, [$location['lat'], $location['lng']]);
            }
        }
    }

    public function render()
    {
        // $this->mount();
        $this->searchBH();
        $this->searchCR();
        $this->searchPACR();
        $this->searchBPA();
        $this->searchBPB();
        $this->searchBVPC();
        $this->searchCTD();
        $this->searchHM();
        return view('livewire.crm.reports.booking-reports.index');
    }
}
