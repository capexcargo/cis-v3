<?php

namespace App\Http\Livewire\Crm\Reports\SalesModuleReports;

use App\Interfaces\Crm\Reports\SalesModuleReportsInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $paginate = 10;
    public $date_created_search_lead;
    public $date_range_search_lead;
    public $quarter_search;
    public $year_search;
    public $month_search;

    public $date_created_search_opportunity;
    public $date_range_search_opportunity;

    public $action_type;

    protected $listeners = ['index' => 'render'];

    public function search()
    {
        return [
            'paginate' => $this->paginate,
            'date_lead' => $this->date_created_search_lead,
            'date_range_lead' => $this->date_range_search_lead,
            'quarter' => $this->quarter_search,
            'year' => $this->year_search,
            'month' => $this->month_search,
            'date_opportunity' => $this->date_created_search_opportunity,
            'date_range_opportunity' => $this->date_range_search_opportunity,
        ];
    }

    public function action($data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'see_sales_campaign') {
            return redirect()->route('crm.sales.sales-campaign.index');
        } elseif ($action_type == 'see_leads') {
            return redirect()->route('crm.sales.leads.index');
        } elseif ($action_type == 'see_industry') {
            return redirect()->route('crm.customer-information.industry-mgmt.index');
        } elseif ($action_type == 'see_service_requirement') {
            return redirect()->route('crm.sales.leads.index');
        } elseif ($action_type == 'see_opportunities') {
            return redirect()->route('crm.sales.opportunities.index');
        }
    }

    public function render(SalesModuleReportsInterface $sales_module_reports_interface)
    {
        $response = $sales_module_reports_interface->index($this->search());
        $all_leads = [];
        $all_converted = [];
        $no_of_leads = [];

        $retired_new_leads = [];
        $retired_existing_leads = [];
        $retired_new_leads_qual_score = [];
        $retired_existing_leads_qual_score = [];

        $unqualified_new_leads = [];
        $unqualified_existing_leads = [];
        $unqualified_new_leads_qual_score = [];
        $unqualified_existing_leads_qual_score = [];

        $qualified_new_leads = [];
        $qualified_existing_leads = [];
        $qualified_new_leads_qual_score = [];
        $qualified_existing_leads_qual_score = [];

        $converted_new_leads = [];
        $converted_existing_leads = [];
        $converted_new_leads_qual_score = [];
        $converted_existing_leads_qual_score = [];

        // dd($response['result']);

        $industry_total_leads = array_sum(array(
            $response['result']['industries'][0]['food_industry'] ?? 0,
            $response['result']['industries'][1]['agriculture'] ?? 0,
            $response['result']['industries'][2]['construction'] ?? 0,
            $response['result']['industries'][3]['automotive'] ?? 0,
            $response['result']['industries'][4]['manufacturing'] ?? 0,
            $response['result']['industries'][5]['financial_services'] ?? 0,
            $response['result']['industries'][6]['transport'] ?? 0,
            $response['result']['industries'][7]['logistics'] ?? 0,
            $response['result']['industries'][8]['commerces'] ?? 0,
        ));

        $service_requirement_total_leads = array_sum(array(
            $response['result']['service_requirements'][0]['domestic_freight'] ?? 0,
            $response['result']['service_requirements'][5]['international_freight'] ?? 0,
        ));

        foreach ($response['result']['channel_sources'] as $i => $cs) {
            $all_leads[] = $cs['leads_count'];
        }
        $total_leads = array_sum($all_leads);

        foreach ($response['result']['channel_source_rankings'] as $i => $csr) {
            $all_converted[] = $csr['converted_to_opps_count'];
        }
        $total_converted = array_sum($all_converted);

        foreach ($response['result']['leads'] as $i => $csr) {
            $no_of_leads[] = $csr['no_of_leads'];
        }
        $total_no_of_leads = array_sum($no_of_leads);

        foreach ($response['result']['leads'] as $i => $csr) {
            $retired_new_leads[] = $csr['retired_new_count'];
            $retired_existing_leads[] = $csr['retired_existing_count'];
        }
        $total_retired_leads = array_sum(array_merge($retired_new_leads, $retired_existing_leads));

        foreach ($response['result']['leads'] as $i => $csr) {
            $retired_new_leads_qual_score[] = $csr['retired_new_qual_score'];
            $retired_existing_leads_qual_score[] = $csr['retired_existing_qual_score'];
        }
        $total_retired_leads_qual_score = array_sum(array_merge($retired_new_leads_qual_score, $retired_existing_leads_qual_score));

        foreach ($response['result']['leads'] as $i => $csr) {
            $unqualified_new_leads[] = $csr['unqualified_new_count'];
            $unqualified_existing_leads[] = $csr['unqualified_existing_count'];
        }
        $total_unqualified_leads = array_sum(array_merge($unqualified_new_leads, $unqualified_existing_leads));

        foreach ($response['result']['leads'] as $i => $csr) {
            $unqualified_new_leads_qual_score[] = $csr['unqualified_new_qual_score'];
            $unqualified_existing_leads_qual_score[] = $csr['unqualified_existing_qual_score'];
        }
        $total_unqualified_leads_qual_score = array_sum(array_merge($unqualified_new_leads_qual_score, $unqualified_existing_leads_qual_score));

        foreach ($response['result']['leads'] as $i => $csr) {
            $qualified_new_leads[] = $csr['qualified_new_count'];
            $qualified_existing_leads[] = $csr['qualified_existing_count'];
        }
        $total_qualified_leads = array_sum(array_merge($qualified_new_leads, $qualified_existing_leads));

        foreach ($response['result']['leads'] as $i => $csr) {
            $qualified_new_leads_qual_score[] = $csr['qualified_new_qual_score'];
            $qualified_existing_leads_qual_score[] = $csr['qualified_existing_qual_score'];
        }
        $total_qualified_leads_qual_score = array_sum(array_merge($qualified_new_leads_qual_score, $qualified_existing_leads_qual_score));

        foreach ($response['result']['leads'] as $i => $csr) {
            $converted_new_leads[] = $csr['converted_new_count'];
            $converted_existing_leads[] = $csr['converted_existing_count'];
        }
        $total_converted_leads = array_sum(array_merge($converted_new_leads, $converted_existing_leads));

        foreach ($response['result']['leads'] as $i => $csr) {
            $converted_new_leads_qual_score[] = $csr['converted_new_qual_score'];
            $converted_existing_leads_qual_score[] = $csr['converted_existing_qual_score'];
        }
        $total_converted_leads_qual_score = array_sum(array_merge($converted_new_leads_qual_score, $converted_existing_leads_qual_score));

        return view('livewire.crm.reports.sales-module-reports.index', [
            'leads' =>  $response['result']['leads'] ?? [],
            'total_no_of_leads' =>  $total_no_of_leads,
            'total_retired_leads' =>  $total_retired_leads,
            'total_retired_leads_qual_score' =>  $total_retired_leads_qual_score,
            'total_unqualified_leads' =>  $total_unqualified_leads,
            'total_unqualified_leads_qual_score' =>  $total_unqualified_leads_qual_score,
            'total_qualified_leads' =>  $total_qualified_leads,
            'total_qualified_leads_qual_score' =>  $total_qualified_leads_qual_score,
            'total_converted_leads' =>  $total_converted_leads,
            'total_converted_leads_qual_score' =>  $total_converted_leads_qual_score,

            'opportunities' =>  $response['result']['opportunities'] ?? [],

            'count_ind_food_industry' =>  $response['result']['industries'][0]['food_industry'] ?? 0,
            'count_ind_agriculture' =>  $response['result']['industries'][1]['agriculture'] ?? 0,
            'count_ind_construction' =>  $response['result']['industries'][2]['construction'] ?? 0,
            'count_ind_automotive' =>  $response['result']['industries'][3]['automotive'] ?? 0,
            'count_ind_manufacturing' =>  $response['result']['industries'][4]['manufacturing'] ?? 0,
            'count_ind_financial_services' =>  $response['result']['industries'][5]['financial_services'] ?? 0,
            'count_ind_transport' =>  $response['result']['industries'][6]['transport'] ?? 0,
            'count_ind_logistics' =>  $response['result']['industries'][7]['logistics'] ?? 0,
            'count_ind_commerces' =>  $response['result']['industries'][8]['commerces'] ?? 0,
            'industry_total_leads' =>  $industry_total_leads,

            'count_domestic_freight' =>  $response['result']['service_requirements'][0]['domestic_freight'] ?? 0,
            'count_international_freight' =>  $response['result']['service_requirements'][5]['international_freight'] ?? 0,
            'service_requirement_total_leads' =>  $service_requirement_total_leads,

            'channel_sources' =>  $response['result']['channel_sources'] ?? [],
            'total_leads' =>  $total_leads,
            'channel_source_rankings' =>  $response['result']['channel_source_rankings'] ?? [],
            'total_converted' =>  $total_converted,
        ]);
    }
}
