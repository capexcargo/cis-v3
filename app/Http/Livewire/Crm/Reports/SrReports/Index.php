<?php

namespace App\Http\Livewire\Crm\Reports\SrReports;

use App\Interfaces\Crm\Reports\SRReports\SRReportsInterface;
use App\Models\ChannelSrSource;
use App\Models\Crm\CrmServiceRequest;
use App\Models\MileResolution;
use App\Models\ServiceRequirements;
use App\Models\SrType;
use App\Models\SrTypeSubcategory;
use App\Models\User;
use App\Traits\Crm\Reports\SRReports\SRReportsTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Broadcasting\Channel;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use SRReportsTrait, WithPagination, PopUpMessagesTrait;

    public $totalopen;
    public $totalclose;
    public $totalsrr;

    public $totalhigh;
    public $totalmedium;
    public $totallow;
    public $totalsev;

    public $numbers = [];

    public $totalchannels = [];
    public $totalchannel = [];
    public $getdatachannels;

    public $totalrequirements = [];
    public $totalrequirement = [];
    public $getdatarequirements;

    public $search_request = [];
    public $search_requests;

    public $agents = [];
    public $agent = [];
    public $getdataagents;

    public $totalagents;

    public $serviceRTD = [];
    public $serviceRTDsub = [];

    public $sr_sub_categ_count;

    public $statuses = [];

    public $totalsarray = [];
    public $totalsarray2 = [];

    protected $listeners = ['mount' => 'mount', 'index' => 'render'];

    public function search()
    {
            $this->mount();
    }

    public function mount()
    {
        /////////////////////////////////////////////// SERVICE REQUEST REPORT /////////////////////////////////////////////
        $newandinprogress = CrmServiceRequest::whereIn('status_id', [1, 2])->when($this->date_created_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->date_created_search . '%');
        })->get();
        $resolved = CrmServiceRequest::where('status_id', 3)->when($this->date_created_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->date_created_search . '%');
        })->get();
        $totalRequestrep = CrmServiceRequest::whereIn('status_id', [1, 2, 3])->when($this->date_created_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->date_created_search . '%');
        })->get();

        // dd($totalRequestrep);

        $this->totalopen = count($newandinprogress);
        $this->totalclose = count($resolved);
        $this->totalsrr = count($totalRequestrep);

        $this->totalsarray = [$this->totalclose,$this->totalopen];

        $this->emit('totalsarray',  $this->totalsarray);



        // dd($this->totalopen);
        /////////////////////////////////////////////// SERVICE REQUEST REPORT /////////////////////////////////////////////


        /////////////////////////////////////////////// SEVERITY /////////////////////////////////////////////
        $high = CrmServiceRequest::where('severity_id', 1)->when($this->date_created_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->date_created_search . '%');
        })->get();
        $medium = CrmServiceRequest::where('severity_id', 2)->when($this->date_created_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->date_created_search . '%');
        })->get();
        $low = CrmServiceRequest::where('severity_id', 3)->when($this->date_created_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->date_created_search . '%');
        })->get();

        $totalseverity = CrmServiceRequest::whereIn('severity_id', [1, 2, 3])->when($this->date_created_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->date_created_search . '%');
        })->get();

        $this->totalhigh = count($high);
        $this->totalmedium = count($medium);
        $this->totallow = count($low);
        $this->totalsev = count($totalseverity);

        $this->totalsarray2 = [$this->totallow,$this->totalmedium,$this->totalhigh];


        $this->emit('totalsarray2',  $this->totalsarray2);
        // dd($this->totalsarray2);

        /////////////////////////////////////////////// SEVERITY /////////////////////////////////////////////


        /////////////////////////////////////////////// CHANNEL/ SR SOURCE /////////////////////////////////////////////
        $this->totalchannels = ChannelSrSource::withCount('ServiceChannel')->when($this->date_created_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->date_created_search . '%');
        })->get();

        $getdata = [];
        foreach ($this->totalchannels as $i => $totalchannel) {
            $getdata[] = ["$totalchannel->name", $totalchannel->service_channel_count, "#82abff"];
        }

        $this->getdatachannels = $getdata;
        /////////////////////////////////////////////// CHANNEL/ SR SOURCE /////////////////////////////////////////////


        /////////////////////////////////////////////// SERVICE REQUIREMENTS /////////////////////////////////////////////
        $this->totalrequirements = ServiceRequirements::withCount('ServRequirements')->when($this->date_created_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->date_created_search . '%');
        })->get();

        $getdata2 = [];
        foreach ($this->totalrequirements as $i => $totalrequirement) {
            $getdata2[] = ["$totalrequirement->name", $totalrequirement->serv_requirements_count, "#82abff"];
        }

        $this->getdatarequirements = $getdata2;
        /////////////////////////////////////////////// SERVICE REQUIREMENTS /////////////////////////////////////////////


        /////////////////////////////////////////////// AGENTS TABLE /////////////////////////////////////////////
        $this->agents  = User::where('division_id', 2)->when($this->date_created_search ?? false, function ($query) {
            $query->where('created_at', 'like', '%' . $this->date_created_search . '%');
        })->get();

        $this->totalagents = count($this->agents);
        /////////////////////////////////////////////// AGENTS TABLE /////////////////////////////////////////////


        /////////////////////////////////////////////// SERVICE REQUEST TYPE DISTRIBUTION /////////////////////////////////////////////

        $this->serviceRTD = SrType::with(['SrSubCateg' => function ($query) {
            $query->withCount(['ServiceRequest' => function ($query) {
                $query->when($this->date_created_search ?? false, function ($query) {
                    $query->where('created_at', 'like', '%' . $this->date_created_search . '%');
                });
            }
        ]);
        }])->withCount(['ServiceRequestsr' => function ($query){
            $query->when($this->date_created_search ?? false, function ($query) {
                $query->where('created_at', 'like', '%' . $this->date_created_search . '%');
            });
        }])->get();

        /////////////////////////////////////////////// SERVICE REQUEST TYPE DISTRIBUTION /////////////////////////////////////////////


    }

    public function render()
    {
        return view('livewire.crm.reports.sr-reports.index');
    }
}
