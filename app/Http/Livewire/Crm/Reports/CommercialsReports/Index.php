<?php

namespace App\Http\Livewire\Crm\Reports\CommercialsReports;

use App\Models\CrmRateAirFreight;
use App\Models\CrmRateAirFreightPremium;
use App\Models\CrmRateBox;
use App\Models\CrmRateCrating;
use App\Models\CrmRateLandFreight;
use App\Models\CrmRatePouch;
use App\Models\CrmRateSeaFreight;
use App\Models\CrmRateWarehousing;
use App\Traits\Crm\Reports\CommercialReports\CommercialReportsTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use CommercialReportsTrait;
    use PopUpMessagesTrait;
    use WithPagination;

    public $commercialmgmts = [];
    public $commercialmgmtair = [];
    public $commercialmgmtsea = [];
    public $commercialmgmtland = [];
    public $commercialmgmtairpremium = [];
    public $commercialmgmtpouch = [];
    public $commercialmgmtbox = [];
    public $commercialmgmtcrating = [];
    public $commercialmgmtwarehouse = [];

    public function mount()
    {
        $airfreights = CrmRateAirFreight::with('CommodityTypeReference', 'ServiceModeReference')->get();
        foreach ($airfreights as $i => $commercial) {
            $this->commercialmgmtair[$i]['name'] = $commercial['name'];
            $this->commercialmgmtair[$i]['description'] = $commercial['description'].' '.$commercial->CommodityTypeReference['name']
                .' - '.$commercial->ServiceModeReference['name'].' '.($commercial['is_vice_versa'] == 1 ? 'VV' : '');
        }
        // dd($this->commercialmgmtair);

        $seafreights = CrmRateSeaFreight::with('CommodityTypeReference', 'ShipmentTypeReference')->get();
        foreach ($seafreights as $i => $commercial) {
            $this->commercialmgmtsea[$i]['name'] = $commercial['name'];
            $this->commercialmgmtsea[$i]['description'] = $commercial['description'].' '.$commercial->CommodityTypeReference['name']
                .' - '.$commercial->ShipmentTypeReference['name'].' '.($commercial['is_vice_versa'] == 1 ? 'VV' : '');
        }
        // dd($this->commercialmgmtsea);

        $landfreights = CrmRateLandFreight::with('CommodityTypeReference')->get();
        foreach ($landfreights as $i => $commercial) {
            $this->commercialmgmtland[$i]['name'] = $commercial['name'];
            $this->commercialmgmtland[$i]['description'] = $commercial['description'].' '.$commercial->CommodityTypeReference['name']
                .' - '.($commercial['is_vice_versa'] == 1 ? 'VV' : '');
        }
        // dd($this->commercialmgmtland);

        $airpremiumfreights = CrmRateAirFreightPremium::with('CommodityTypeReference')->get();
        foreach ($airpremiumfreights as $i => $commercial) {
            $this->commercialmgmtairpremium[$i]['name'] = $commercial['name'];
            $this->commercialmgmtairpremium[$i]['description'] = $commercial['description'].' '.$commercial->CommodityTypeReference['name']
                .' - '.($commercial['is_vice_versa'] == 1 ? 'VV' : '');
        }
        // dd($this->commercialmgmtairpremium);

        $pouchs = CrmRatePouch::with('BookingTypeReference', 'RatePouchReference')->get();
        foreach ($pouchs as $i => $commercial) {
            $this->commercialmgmtpouch[$i]['name'] = $commercial['name'];
            $this->commercialmgmtpouch[$i]['description'] = $commercial['description'].' '.$commercial->BookingTypeReference['name']
                .' - '.$commercial->RatePouchReference['name'].' '.($commercial['is_vice_versa'] == 1 ? 'VV' : '');
        }
        // dd($this->commercialmgmtpouch);

        $boxs = CrmRateBox::with('BookingTypeReference')->get();
        foreach ($boxs as $i => $commercial) {
            $this->commercialmgmtbox[$i]['name'] = $commercial['name'];
            $this->commercialmgmtbox[$i]['description'] = $commercial['description'].' '.$commercial->BookingTypeReference['name']
                .' - '.($commercial['is_vice_versa'] == 1 ? 'VV' : '');
        }
        // dd($this->commercialmgmtbox);

        $cratings = CrmRateCrating::with('CrateTypeReference')->get();
        foreach ($cratings as $i => $commercial) {
            $this->commercialmgmtcrating[$i]['name'] = $commercial['name'];
            $this->commercialmgmtcrating[$i]['description'] = $commercial['description'].' - '.$commercial->CrateTypeReference['name'];
        }
        // dd($this->commercialmgmtcrating);

        $warehousings = CrmRateWarehousing::get();
        foreach ($warehousings as $i => $commercial) {
            $this->commercialmgmtwarehouse[$i]['name'] = $commercial['name'];
            $this->commercialmgmtwarehouse[$i]['description'] = $commercial['description'];
        }

        
    }

    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function render()
    {
        $this->commercialmgmts = array_merge(
            $this->commercialmgmtair,
            $this->commercialmgmtsea,
            $this->commercialmgmtland,
            $this->commercialmgmtairpremium,
            $this->commercialmgmtpouch,
            $this->commercialmgmtbox,
            $this->commercialmgmtcrating,
            $this->commercialmgmtwarehouse
        );

        return view('livewire.crm.reports.commercials-reports.index', ['pages' => $this->paginate($this->commercialmgmts)]);
    }
}
