<?php

namespace App\Http\Livewire\Crm\Reports\ContractReports;

use App\Models\Crm\CrmCustomerInformation;
use App\Traits\Crm\Reports\ContractsReports\ContractsReportsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use ContractsReportsTrait, WithPagination, PopUpMessagesTrait;

    public $totalapps;
    public $totalapproved;
    public $totaldeclined;
    public $totalforapps;
    public $totalsarray = [];

    protected $listeners = ['mount' => 'mount', 'index' => 'render'];

    public function search()
    {
        $this->mount();
    }

    public function mount()
    {
        $totalapp = CrmCustomerInformation::where('account_type', 2)->when($this->date_created_search ?? false, function ($query) {
            $query->where('approval_date', 'like', '%' . $this->date_created_search . '%');
        })->get();

        $totalapprove = CrmCustomerInformation::where('account_type', 2)->where('final_status', 1)->when($this->date_created_search ?? false, function ($query) {
            $query->where('approval_date', 'like', '%' . $this->date_created_search . '%');
        })->get();

        $totaldecline = CrmCustomerInformation::where('account_type', 2)->where('final_status', 2)->when($this->date_created_search ?? false, function ($query) {
            $query->where('approval_date', 'like', '%' . $this->date_created_search . '%');
        })->get();

        $totalforapp = CrmCustomerInformation::where('account_type', 2)->whereNull('final_status')->when($this->date_created_search ?? false, function ($query) {
            $query->where('approval_date', 'like', '%' . $this->date_created_search . '%');
        })->get();

        $this->totalapps = count($totalapp);
        $this->totalapproved = count($totalapprove);
        $this->totaldeclined = count($totaldecline);
        $this->totalforapps = count($totalforapp);

        $this->totalsarray = [$this->totalapproved,$this->totalforapps,$this->totaldeclined];

        $this->emit('totalsarray',  $this->totalsarray);
        
       

        // dd($this->totalforapps);
    }

    public function render()
    {
        return view('livewire.crm.reports.contract-reports.index');
    }
}
