<?php

namespace App\Http\Livewire\Crm\ServiceRequest\ServiceRequestMgmt;

use App\Interfaces\Crm\ServiceRequest\ServiceRequestMgmtInterface;
use App\Models\Crm\CrmServiceRequest;
use App\Models\Crm\CrmSrEmailDetails;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Reply extends Component
{
    use PopUpMessagesTrait;

    public $sr_number;
    public $to;
    public $body;
    public $sr_id;
    public $message_id;

    protected $listeners = ['reply' => 'render', 'mount' => 'mount'];

    public function mount($sr_number)
    {
        $this->sr_number = $sr_number;

        $email = CrmSrEmailDetails::where('sr_no', $sr_number)->where('type', 1)->first();
        $sr = CrmServiceRequest::where('sr_no', $sr_number)->first();

        $this->to = $email->email_external;
        $this->message_id = $email->message_id;
        $this->sr_id = $sr->id;
    }

    public function SendReply(ServiceRequestMgmtInterface $service_request_mgmt_interface)
    {
        // dd($this->message_id);
        $response = $service_request_mgmt_interface->sendReply($this->sr_number, $this->body, $this->message_id);
        // dd($response);

        if ($response['code'] == 200) {
            $this->emitTo('crm.service-request.service-request-mgmt.edit', 'edit_close_modal', 'reply');
            $this->emitTo('crm.service-request.service-request-mgmt.edit', 'edit_mount', $this->sr_id);
            $this->emitTo('crm.service-request.service-request-mgmt.edit', 'edit', $this->sr_id);
            $this->emitTo('crm.service-request.service-request-mgmt.edit', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.service-request.service-request-mgmt.reply');
    }
}
