<?php

namespace App\Http\Livewire\Crm\ServiceRequest\ServiceRequestMgmt;

use App\Models\Crm\CrmSrAttachments;
use Livewire\Component;

class ViewAttachment extends Component
{
    protected $listeners = ['view_attachment' => 'mount'];
    public $requests = array();

    public function mount($id)
    {
        $this->reset([
            'requests',
        ]);

        $this->requests = CrmSrAttachments::where('id', $id)->get();
    }

    public function render()
    {
        return view('livewire.crm.service-request.service-request-mgmt.view-attachment');
    }
}
