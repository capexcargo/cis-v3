<?php

namespace App\Http\Livewire\Crm\ServiceRequest\ServiceRequestMgmt;

use App\Interfaces\Crm\ServiceRequest\ServiceRequestMgmtInterface;
use App\Traits\Crm\ServiceRequest\ServiceRequestMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use ServiceRequestMgmtTrait, PopUpMessagesTrait, WithFileUploads;

    public function mount()
    {
        $this->account_type = 1;

        $this->assigned_to = Auth::user()->id;
    }

    protected function updatedSubcategory()
    {
        if ($this->sr_type == 3 && $this->subcategory == 4 || $this->sr_type == 2 && $this->subcategory == 1) {
            $this->severity = 1;
        } else if ($this->sr_type == 2 && $this->subcategory == 2) {
            $this->severity = 2;
        } else if ($this->sr_type == 3 && $this->subcategory == 3) {
            $this->severity = 3;
        }
    }

    public function accountType($type)
    {
        $this->account_type = $type;
    }

    public function confirmationSubmit()
    {

        $this->account_type = $this->account_type ?? 1;

        $rules = [
            'subject' => 'required',
            'sr_type' => 'required',
            'severity' => 'required',
            'service_requirement' => 'required',
            'assigned_to' => 'required',
            'channel_sr_source' => 'required',
            'status' => 'required',
            'sr_description' => 'required',

            'attachments' => 'nullable',
            'attachments.*.attachment' => 'nullable|' . config('filesystems.validation_all')
        ];

        if ($this->account_type == 1) {
            $rules['primary_contact_person'] = 'required';
            $rules['company_business_name'] = 'sometimes';
        } else {
            $rules['primary_contact_person'] = 'sometimes';
            $rules['company_business_name'] = 'required';
        }

        if ($this->sr_type == 2 || $this->sr_type == 4) {
            $rules['subcategory'] = 'required';

            if ($this->sr_type == 2 && $this->subcategory == 1) {
                $rules['booking_ref_no'] = 'required';
            } elseif ($this->sr_type == 2 && $this->subcategory == 2) {
                $rules['waybill_no'] = 'required';
            }
        } elseif ($this->sr_type == 3) {
            $rules['service_requirement'] = 'sometimes';

            $rules['subcategory'] = 'sometimes';
            $rules['booking_ref_no'] = 'sometimes';
            $rules['waybill_no'] = 'sometimes';
        }

        $validated = $this->validate(
            $rules,
            [
                'sr_description.required' => 'The Service request description field is required.',
                'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
            ]
            
        );

    }

    public function submit(ServiceRequestMgmtInterface $service_request_interface)
    {
        $this->confirmationSubmit();

        $response = $service_request_interface->create($this->getRequest());
        
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->load();
            $this->emitTo('crm.service-request.service-request-mgmt.index', 'close_modal', 'add');
            $this->emitTo('crm.service-request.service-request-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        }
    }

    public function render()
    {
        return view('livewire.crm.service-request.service-request-mgmt.create');
    }
}
