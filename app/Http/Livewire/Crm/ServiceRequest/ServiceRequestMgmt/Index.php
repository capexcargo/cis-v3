<?php

namespace App\Http\Livewire\Crm\ServiceRequest\ServiceRequestMgmt;

use App\Interfaces\Crm\ServiceRequest\ServiceRequestMgmtInterface;
use App\Models\Crm\CrmSrEmailDetails;
use App\Models\User;
use App\Traits\Crm\ServiceRequest\ServiceRequestMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use ServiceRequestMgmtTrait, PopUpMessagesTrait, WithPagination;

    public $severity_search;
    public $sr_no_search;
    public $date_created_search;
    public $status_search;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;
    public $sort_by = false;
    public $sort_clicked = false;

    public $count_new;

    public $read;
    public $card_header;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function mount()
    {
        $this->search();

        $this->card_header = 1;
    }

    public function cardHeader($card)
    {
        $this->card_header = $card;
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            return redirect()->route('crm.service-request.service-request-mgmt.edit', ['id' => $data['id']]);
        } else if ($action_type == 'resolve') {
            if ($this->sr_status == 3) {
                $this->confirmation_modal = true;
                $this->sr_id = $data['id'];
            }
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'add') {
            $this->create_modal = false;
        } else if ($action_type == 'resolve') {
            $this->confirmation_modal = false;
        }
    }

    public function resolveSr(ServiceRequestMgmtInterface $service_request_mgmt_interface)
    {
        $response = $service_request_mgmt_interface->updateSrStatus($this->sr_id);

        if ($response['code'] == 200) {
            // $this->resetForm();
            $this->emitTo('crm.service-request.service-request-mgmt.index', 'close_modal', 'resolve');
            $this->emitTo('crm.service-request.service-request-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        }
    }

    public function search()
    {
        return [
            'card_header' => $this->card_header,
            'paginate' => $this->paginate,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'sort_by' => null,
            'severity' => $this->severity_search,
            'sr_no' => $this->sr_no_search,
            'date_created' => $this->date_created_search,
            'status' => $this->status_search,
        ];
    }


    public function sortBy($field)
    {
        $this->sort_by = $field;
        
        $this->sort_clicked = true;

        return [
            'card_header' => $this->card_header,
            'paginate' => $this->paginate,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'sort_by' => ($field  ? 'asc' : 'desc'),
            'severity' => $this->severity_search,
            'sr_no' => $this->sr_no_search,
            'date_created' => $this->date_created_search,
            'status' => $this->status_search,
        ];
    }

    public function render(ServiceRequestMgmtInterface $service_request_mgmt_interface)
    {
        $retrieved = $service_request_mgmt_interface->retrieveEmail();
        // dd($retrieved);

        $response = $service_request_mgmt_interface->index($this->sort_clicked ? $this->sortBy($this->sort_by) : $this->search());

        // $response = $service_request_mgmt_interface->index($this->sortBy($this->sort_by));
        // dd($response['result']['service_requests']);

        $all_sr = $response['result']['all_sr'] ?? null;
        $closed_sr = $response['result']['closed_sr'] ?? null;
        $teams_sr = $response['result']['teams_sr'] ?? null;
        $my_open_sr = $response['result']['my_open_sr'] ?? null;


        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'service_requests' => [],
                ];
        }

        return view('livewire.crm.service-request.service-request-mgmt.index', [
            'all_sr' => $all_sr,
            'teams_sr' => $teams_sr,
            'closed_sr' => $closed_sr,
            'my_open_sr' => $my_open_sr,
            'service_requests' => $response['result']['service_requests'],
        ]);
    }
}
