<?php

namespace App\Http\Livewire\Crm\ServiceRequest\ServiceRequestMgmt;

use App\Models\Crm\CrmServiceRequest;
use Livewire\Component;

class GeneratedReply extends Component
{
    public $sr_no;

    protected $listeners = ['index' => 'render', 'mount' => 'mount'];

    public function mount()
    {
        $last_sr_no = CrmServiceRequest::select('sr_no')->latest()->first();
        $last_inserted_sr_no = (int)substr($last_sr_no->sr_no, 3);
        // $count_sr = $last_inserted_sr_no += 1;
        $count_sr = $last_inserted_sr_no++;
        $sr_no = "SRN" . $count_sr;

        $this->sr_no = $sr_no;
    }

    public function render()
    {
        return view('livewire.crm.service-request.service-request-mgmt.generated-reply');
    }
}
