<?php

namespace App\Http\Livewire\Crm\ServiceRequest\ServiceRequestMgmt;

use Livewire\Component;

class EmailSignature extends Component
{
    public function render()
    {
        return view('livewire.crm.service-request.service-request-mgmt.email-signature');
    }
}
