<?php

namespace App\Http\Livewire\Crm\ServiceRequest\ServiceRequestMgmt;

use App\Interfaces\Crm\ServiceRequest\ServiceRequestMgmtInterface;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmSrEmailDetails;
use App\Models\MileResponse;
use App\Traits\Crm\ServiceRequest\ServiceRequestMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Webklex\IMAP\Facades\Client;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Carbon\Carbon;
use CreateCrmMilestoneResponse;
use DateTime;

class Edit extends Component
{
    use ServiceRequestMgmtTrait, PopUpMessagesTrait, WithFileUploads, WithPagination;

    public $open = false;
    public $isOpen = false;

    public $service_request;
    public $sr_id;
    public $sr_no;
    public $created_by;
    public $created_at;
    public $updated_at;
    public $updated_by;
    public $resolved_date;

    public $compose_modal = false;
    public $reply_modal = false;
    public $sr_number;

    public $attachment_modal = false;
    public $attachment_id;

    public $emails = [];
    public $email_from;
    public $email_to;
    public $email_subject;
    public $email_body;

    public $email;
    public $latest_email;

    public $count_thread;

    public $account_type;
    public $customer_data;

    public $time_remaining;
    public $next_milestone;
    public $due_date;
    public $starts_when;
    public $completes_when;
    public $milestone_response;

    public $clicked_edit_sr_description = false;

    public $with_lead = false;

    protected $listeners = ['edit' => 'render', 'edit_mount' => 'mount', 'edit_close_modal' => 'closeModal'];

    public function mount(ServiceRequestMgmtInterface $service_request_mgmt_interface, $id)
    {
        $response = $service_request_mgmt_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->service_request = $response['result'];
        $this->count_thread = $this->service_request->count_thread;
        $this->sr_id = $this->service_request->id;
        $this->sr_no = $this->service_request->sr_no;
        $this->subject = $this->service_request->subject;

        $this->customer_data = CrmCustomerInformation::with('contactPersons', 'mobileNumbers', 'emails')->find($this->service_request->customer ?? $this->service_request->account_id);

        // dd($this->service_request->customer ?? $this->service_request->account_id, $this->customer_data->contactPersons);
        if ($this->customer_data) {

            $this->account_type = $this->customer_data->account_type;

            $this->company_business_name = $this->customer_data->account_type ?? null == 2 ? $this->customer_data->company_name : '';

            foreach ($this->customer_data->contactPersons as $i => $contact_person) {
                if ($contact_person->is_primary == 1) {
                    $this->primary_contact_person = $contact_person->first_name . " " . $contact_person->middle_name . " " . $contact_person->last_name;
                    $this->fname = $contact_person->first_name;
                    $this->mname = $contact_person->middle_name;
                    $this->lname = $contact_person->last_name;
                }
            }


            foreach ($this->customer_data->mobileNumbers as $i => $mobile_number) {
                if ($mobile_number->is_primary == 1) {
                    $this->mobile_number = $mobile_number->mobile;
                }
            }

            foreach ($this->customer_data->telephoneNumbers as $i => $telephone_number) {
                if ($telephone_number->is_primary == 1) {
                    $this->telephone_number = $telephone_number->telephone;
                }
            }

            foreach ($this->customer_data->emails as $i => $email_address) {
                if ($email_address->is_primary == 1) {
                    $this->email_address = $email_address->email;
                }
            }
        }

        if ($this->customer_data->lead ?? null != null) {
            $this->with_lead = true;
        }

        $this->sr_type = $this->service_request->service_request_type_id;
        $this->subcategory = $this->service_request->sub_category_id;
        $this->booking_ref_no = $this->subcategory == 1 ? $this->service_request->sub_category_reference : '';
        $this->waybill_no = $this->subcategory == 2 ? $this->service_request->sub_category_reference : '';
        $this->severity = $this->service_request->severity_id;
        $this->service_requirement = $this->service_request->service_requirement_id;
        $this->assigned_to = $this->service_request->assigned_to_id;
        $this->channel_sr_source = $this->service_request->channel_id;
        $this->status = $this->service_request->status_id;
        $this->sr_description = $this->service_request->description;
        $this->created_by = $this->service_request->createdBy->name;
        $this->created_at = $this->service_request->created_at;
        $this->updated_at = $this->service_request->updated_at;
        $this->updated_by = $this->service_request->updatedBy->name ?? null;
        $this->resolved_date =  $this->status == 3 ? date('m/d/Y h:i A', strtotime($this->service_request->resolved_date)) : '-';

        $this->emails = CrmSrEmailDetails::where('sr_no', $this->sr_no)->orderBy('created_at', 'desc')->get();

        $this->next_milestone;
        $this->starts_when = date('m/d/Y h:i A', strtotime($this->created_at));
        $this->completes_when;
        $this->milestone_response = '';

        // if (count($this->emails) > 0) {
        //     foreach ($this->emails as $email) {
        //         $this->milestone_response = MileResponse::where('email', $email->email_internal)->first();
        //     }
        // } else {
            $this->milestone_response = MileResponse::where('email', 'yarnsupt1.capex@gmail.com')
                ->orWhere('email', 'sales@capex.com.ph')
                ->first();
        // }
        $current_time = new DateTime(); // Current time
        $this->due_date = date('m/d/Y h:i A', strtotime($this->created_at . '+' . ($this->milestone_response->target_response_time ?? 0) . ' hours'));
        $due_date_time = DateTime::createFromFormat('m/d/Y h:i A', $this->due_date);

        $interval = $current_time->diff($due_date_time);

        if ($interval->invert) {
            $this->time_remaining = '0 hours 0 minutes';
        } else {
            if ($interval->h < 1 && $interval->i < 1) {
                $this->time_remaining = '0 hours 0 minutes';
            } else {
                $this->time_remaining = $interval->format('%h hours %i minutes');
            }
        }

        $retrieved = $service_request_mgmt_interface->retrieveReplyEmail($this->sr_no);
    }

    public function action(array $data, $action_type, ServiceRequestMgmtInterface $service_request_mgmt_interface)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal = true;
            $this->emitTo('crm.service-request.service-request-mgmt.lead.create', 'create', $data['id']);
            //
        } else if ($action_type == 'back') {
            return redirect()->route('crm.service-request.service-request-mgmt.index');
            //
        } else if ($action_type == 'compose-email') {
            $this->compose_modal = true;
            $this->sr_number = $data['sr_number'];
            $this->emitTo('crm.service-request.service-request-mgmt.compose', 'compose', $data['sr_number']);
            //
        } else if ($action_type == 'reply') {
            $this->reply_modal = true;
            $this->sr_number = $data['sr_number'];
            $this->emitTo('crm.service-request.service-request-mgmt.reply', 'reply', $data['sr_number']);
            //
        } else if ($action_type == 'create_quotation') {
            return redirect()->route('crm.sales.quotation.index-quotation', ['id' => $data['id'], 'from_sr' => $data['from_sr']]);
            //
        } else if ($action_type == 'click_edit_name') {
            $this->clicked_edit = 1;
            //
        } else if ($action_type == 'cancel_edit_name') {
            $this->clicked_edit = 0;
            $this->emitTo('crm.service-request.service-request-mgmt.edit', 'edit_mount', $data['id']);
            //
        } else if ($action_type == 'view-attachment') {
            $this->attachment_modal = true;
            $this->attachment_id = $data['id'];
            $this->emitTo('crm.service-request.service-request-mgmt.view-attachment', 'view_attachment', $data['id']);
            //
        } else if ($action_type == 'edit_sr_description') {
            $this->clicked_edit_sr_description = true;
            //
        } else if ($action_type == 'save_sr_description') {
            $request = [
                'sr_description' => $this->sr_description,
            ];

            $response = $service_request_mgmt_interface->updateSrDescription($request, $this->sr_id);

            if ($response['code'] == 200) {
                $this->emitTo('crm.service-request.service-request-mgmt.edit', 'edit');
                $this->sweetAlert('', $response['message']);
                $this->clicked_edit_sr_description = false;
                $this->emitTo('crm.service-request.service-request-mgmt.edit', 'edit_mount', $this->sr_id);
            }
            //
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'add') {
            $this->create_modal = false;
        } else if ($action_type == 'reply') {
            $this->reply_modal = false;
        } else if ($action_type == 'compose-email') {
            $this->compose_modal = false;
        }
    }

    public function submit(ServiceRequestMgmtInterface $service_request_mgmt_interface)
    {
        $response = $service_request_mgmt_interface->update($this->getRequest(), $this->sr_id);

        if ($response['code'] == 200) {
            $this->emitTo('crm.service-request.service-request-mgmt.edit', 'edit');
            $this->sweetAlert('', $response['message']);
            $this->clicked_edit = 0;
            $this->emitTo('crm.service-request.service-request-mgmt.edit', 'edit_mount', $this->sr_id);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.service-request.service-request-mgmt.edit');
    }
}
