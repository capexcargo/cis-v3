<?php

namespace App\Http\Livewire\Crm\ServiceRequest\ServiceRequestMgmt;

use App\Interfaces\Crm\ServiceRequest\ServiceRequestMgmtInterface;
use App\Models\Crm\CrmCustomerInformationEmailAddressList;
use App\Models\Crm\CrmServiceRequest;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Compose extends Component
{
    use PopUpMessagesTrait;

    public $sr_number;
    public $to;
    public $subject;
    public $body;
    public $sr_id;

    public $compose = [];

    protected $listeners = ['compose' => 'render', 'mount' => 'mount'];

    public function mount($sr_number)
    {
        $this->sr_number = $sr_number;

        $sr = CrmServiceRequest::where('sr_no', $sr_number)->first();
        $customer_email = CrmCustomerInformationEmailAddressList::where('account_id', $sr->account_id ?? $sr->customer)->first();

        $this->to = $customer_email->email;
        $this->subject = $sr->subject;

        $this->sr_id = $sr->id;
    }

    public function send(ServiceRequestMgmtInterface $service_request_mgmt_interface)
    {
        $this->compose = [
            'sr_number' => $this->sr_number,
            'to' => $this->to,
            'subject' => $this->subject,
            'body' => $this->body,
        ];

        $response = $service_request_mgmt_interface->send($this->compose);

        // dd($response);

        if ($response['code'] == 200) {
            $this->emitTo('crm.service-request.service-request-mgmt.edit', 'edit_close_modal', 'compose-email');
            $this->emitTo('crm.service-request.service-request-mgmt.edit', 'edit_mount', $this->sr_id);
            $this->emitTo('crm.service-request.service-request-mgmt.edit', 'compose', $this->sr_id);
            $this->emitTo('crm.service-request.service-request-mgmt.edit', 'compose');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.service-request.service-request-mgmt.compose');
    }
}
