<?php

namespace App\Http\Livewire\Crm\ServiceRequest\ServiceRequestMgmt\Lead;

use App\Interfaces\Crm\ServiceRequest\ServiceRequestMgmtInterface;
use App\Models\Crm\CrmCustomerInformation;
use App\Traits\Crm\ServiceRequest\LeadTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use LeadTrait, PopUpMessagesTrait, WithFileUploads;

    protected $listeners = ['create' => 'render', 'mount' => 'mount'];

    public $assigned_to_id;
    public $customer_type_id;

    public $address_type;
    public $country;
    public $address_line1;
    public $address_line2;
    public $state_province;
    public $city_municipality;
    public $barangay;
    public $postal_code;
    public $address_label;

    public function mount(ServiceRequestMgmtInterface $service_request_mgmt_interface, $id)
    {
        $this->address_type = 1;

        $response = $service_request_mgmt_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->service_request = $response['result'];

        $this->sr_id = $this->service_request->id;
        $this->sr_no = $this->service_request->sr_no;
        $this->lead_name = $this->service_request->subject;
        $this->account_id = $this->service_request->customer ?? $this->service_request->account_id;
        $this->contact_owner = $this->service_request->assignedTo->name;
        $this->assigned_to_id = $this->service_request->assigned_to_id;
        $this->customer_type = $this->service_request->statusRef->name;
        $this->customer_type_id = $this->service_request->status_id;

        $customer_data = CrmCustomerInformation::with('contactPersons', 'mobileNumbers', 'emails')->find($this->service_request->customer ?? $this->service_request->account_id);

        if ($customer_data) {

            foreach ($customer_data->contactPersons as $i => $contact_person) {
                if ($contact_person->is_primary == 1) {
                    $this->contact_person = $contact_person->first_name . " " . $contact_person->middle_name . " " . $contact_person->last_name;
                }
            }

            foreach ($customer_data->mobileNumbers as $i => $mobile_number) {
                if ($mobile_number->is_primary == 1) {
                    $this->contact_mobile_no = $mobile_number->mobile;
                }
            }

            foreach ($customer_data->emails as $i => $email_address) {
                if ($email_address->is_primary == 1) {
                    $this->contact_email_address = $email_address->email;
                }
            }

            foreach ($customer_data->addresses as $i => $address) {
                if ($address->is_primary == 1) {
                    $this->address_type = $address->address_type;
                    $this->country = $address->country_id;
                    $this->address_line1 = $address->address_line_1;
                    $this->address_line2 = $address->address_line_2;
                    $this->state_province = $address->state_id;
                    $this->city_municipality = $address->city_id;
                    $this->barangay = $address->barangay_id;
                    $this->postal_code = $address->postal_id;
                }
            }
        }
    }

    public function confirmationSubmit()
    {
        $rules = [
            'sr_id' => 'required',
            'sr_no' => 'required',
            'account_id' => 'required',
            'lead_name' => 'required',
            'shipment_type' => 'required',
            'service_requirement' => 'required',
            'description' => 'sometimes',
            'contact_owner' => 'required',
            'customer_type' => 'required',
            'channel_source' => 'required',
            'currency' => 'required',
        ];


        $validated = $this->validate(
            $rules,
        );

        $this->confirmation_modal = true;
    }

    public function submit(ServiceRequestMgmtInterface $service_request_mgmt_interface)
    {
        $response = $service_request_mgmt_interface->createLead($this->getRequest());

        if ($response['code'] == 200) {
            // $this->resetForm();
            $this->emitTo('crm.service-request.service-request-mgmt.edit', 'edit_close_modal', 'add');
            $this->emitTo('crm.service-request.service-request-mgmt.edit', 'edit');
            $this->sweetAlert('', $response['message']);
        }
    }

    public function render()
    {
        return view('livewire.crm.service-request.service-request-mgmt.lead.create');
    }
}
