<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\Response;

use App\Interfaces\Crm\ServiceRequest\Response\ResponseMInterface;
use App\Traits\Crm\ServiceRequest\Response\ResponseMTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use ResponseMTrait, PopUpMessagesTrait;
    
    public $confirmation_modal;
    public $email;
    public $target_response_time;


    // protected $rules = [
    //     'email' => 'required',
    //     'target_response_time' => 'required',

    // ];

    // public function confirmationSubmit()
    // {
    //     $this->validate();

    //     $this->confirmation_modal = true;
    // }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.service-request.sr-related-mgmt.response.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function submit(ResponseMInterface $res_interface)
    {
        // dd($this->getRequest());
        
        $response = $res_interface->create($this->getRequest());
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.service-request.sr-related-mgmt.response.index', 'close_modal', 'create');
            $this->emitTo('crm.service-request.sr-related-mgmt.response.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.service-request.sr-related-mgmt.response.create');
    }
}
