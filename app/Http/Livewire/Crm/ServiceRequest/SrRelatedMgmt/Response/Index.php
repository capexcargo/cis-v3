<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\Response;

use App\Interfaces\Crm\ServiceRequest\Resolution\ResolutionInterface;
use App\Interfaces\Crm\ServiceRequest\Response\ResponseMInterface;
use App\Traits\Crm\ServiceRequest\Response\ResponseMTrait;
use App\Traits\Crm\ServiceRequest\Response\ResponseTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use ResponseMTrait, WithPagination, PopUpMessagesTrait;

    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $res_id;
    public $confirmation_message;


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } 
        elseif ($action_type == 'edit') {
            $this->emitTo('crm.service-request.sr-related-mgmt.response.edit', 'edit', $data['id']);
            $this->res_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this Response?";
            $this->delete_modal = true;
            $this->res_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } 
        else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
        elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }

    public function confirm(ResponseMInterface $response_interface)
    {
        if ($this->action_type == "delete") {
            $response = $response_interface->destroy($this->res_id);
            $this->emitTo('crm.service-request.sr-related-mgmt.response.index', 'close_modal', 'delete');
            $this->emitTo('crm.service-request.sr-related-mgmt.response.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('crm.service-request.sr-related-mgmt.response.index', 'close_modal', 'delete');
            $this->emitTo('crm.service-request.sr-related-mgmt.response.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(ResponseMInterface $res_interface, ResolutionInterface $reso_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $res_interface->index($request);
        $response_reso = $reso_interface->index($request);

        // dd($response);

        if ($response_reso['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response_reso['message'], $response_reso['result']);
            $response_reso['result'] =
                [
                    'm_resolutions' => [],
                ];
        }

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'm_responses' => [],
                ];
        }

        return view('livewire.crm.service-request.sr-related-mgmt.response.index', [
            'm_responses' => $response['result']['m_responses'],'m_resolutions' => $response_reso['result']['m_resolutions']
        ]);
    }

}
