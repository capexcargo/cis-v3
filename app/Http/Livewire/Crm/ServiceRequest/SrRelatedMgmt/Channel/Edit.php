<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\Channel;

use App\Interfaces\Crm\ServiceRequest\Channel\ChannelInterface;
use App\Models\ChannelSrSource;
use App\Traits\Crm\ServiceRequest\Channel\ChannelTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    
    use ChannelTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $channel;
    public $channels;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->channels = ChannelSrSource::findOrFail($id);

        $this->channel = $this->channels->name;
        // $this->category = $this->employment_category_type->employment_category_id;
    }

    public function submit(ChannelInterface $channels)
    {
        $response = $channels->update($this->getRequest(), $this->channels->id);
        
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.service-request.sr-related-mgmt.channel.index', 'close_modal', 'edit');
            $this->emitTo('crm.service-request.sr-related-mgmt.channel.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.service-request.sr-related-mgmt.channel.edit');
    }
}
