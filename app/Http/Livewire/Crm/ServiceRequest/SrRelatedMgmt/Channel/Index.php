<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\Channel;

use App\Interfaces\Crm\ServiceRequest\Channel\ChannelInterface;
use App\Traits\Crm\ServiceRequest\Channel\ChannelTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use ChannelTrait, WithPagination, PopUpMessagesTrait;

    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $chan_id;
    public $confirmation_message;


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('crm.service-request.sr-related-mgmt.channel.edit', 'edit', $data['id']);
            $this->chan_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this Channel Source?";
            $this->delete_modal = true;
            $this->chan_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } 
        else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
        elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }

    public function confirm(ChannelInterface $channels_interface)
    {
        if ($this->action_type == "delete") {
            $response = $channels_interface->destroy($this->chan_id);
            $this->emitTo('crm.service-request.sr-related-mgmt.channel.index', 'close_modal', 'delete');
            $this->emitTo('crm.service-request.sr-related-mgmt.channel.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('crm.service-request.sr-related-mgmt.channel.index', 'close_modal', 'delete');
            $this->emitTo('crm.service-request.sr-related-mgmt.channel.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(ChannelInterface $channel_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $channel_interface->index($request);

        // dd($response);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'm_channels' => [],
                ];
        }
        // dd($response['result']['marketing_channelss']);

        return view('livewire.crm.service-request.sr-related-mgmt.channel.index', [
            'm_channels' => $response['result']['m_channels']
        ]);
    }

}
