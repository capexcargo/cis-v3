<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\Resolution;

use App\Interfaces\Crm\ServiceRequest\Resolution\ResolutionInterface;
use App\Models\MileResolution;
use App\Traits\Crm\ServiceRequest\Resolution\ResolutionTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{

    public $confirmation_modal;
    public $priority_level;
    public $severity;
    public $target_response_time;
    public $res;
    
    use ResolutionTrait, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->res = MileResolution::findOrFail($id);

        $this->priority_level = $this->res->priority_level;
        $this->severity = $this->res->severity;
        $this->target_response_time = $this->res->target_response_time;
        // $this->category = $this->employment_category_type->employment_category_id;
    }

    public function submit(ResolutionInterface $res)
    {
        $response = $res->update($this->getRequest(), $this->res->id);
        
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.service-request.sr-related-mgmt.resolution.index', 'close_modal', 'edit');
            $this->emitTo('crm.service-request.sr-related-mgmt.resolution.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render()
    {
        return view('livewire.crm.service-request.sr-related-mgmt.resolution.edit');
    }
}
