<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\Resolution;

use App\Interfaces\Crm\ServiceRequest\Resolution\ResolutionInterface;
use App\Interfaces\Crm\ServiceRequest\Response\ResponseMInterface;
use App\Traits\Crm\ServiceRequest\Resolution\ResolutionTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use ResolutionTrait, WithPagination, PopUpMessagesTrait;

    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $reso_id;
    public $confirmation_message;


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } 
        elseif ($action_type == 'edit') {
            $this->emitTo('crm.service-request.sr-related-mgmt.resolution.edit', 'edit', $data['id']);
            $this->reso_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this Resolution?";
            $this->delete_modal = true;
            $this->reso_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } 
        else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
        elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }

    public function confirm(ResolutionInterface $resolution_interface)
    {
        if ($this->action_type == "delete") {
            $response = $resolution_interface->destroy($this->reso_id);
            $this->emitTo('crm.service-request.sr-related-mgmt.resolution.index', 'close_modal', 'delete');
            $this->emitTo('crm.service-request.sr-related-mgmt.resolution.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('crm.service-request.sr-related-mgmt.resolution.index', 'close_modal', 'delete');
            $this->emitTo('crm.service-request.sr-related-mgmt.resolution.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(ResolutionInterface $reso_interface, ResponseMInterface $res_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $reso_interface->index($request);
        $response_res = $res_interface->index($request);

        // dd($response);

        if ($response_res['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response_res['message'], $response_res['result']);
            $response_res['result'] =
                [
                    'm_responses' => [],
                ];
        }
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'm_resolutions' => [],
                ];
        }

        return view('livewire.crm.service-request.sr-related-mgmt.resolution.index', [
            'm_resolutions' => $response['result']['m_resolutions'],'m_responses' => $response_res['result']['m_responses']
        ]);
    }

}
