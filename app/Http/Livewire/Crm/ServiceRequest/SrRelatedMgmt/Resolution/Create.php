<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\Resolution;

use App\Interfaces\Crm\ServiceRequest\Resolution\ResolutionInterface;
use App\Traits\Crm\ServiceRequest\Resolution\ResolutionTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{

    use ResolutionTrait, PopUpMessagesTrait;
    
    public $confirmation_modal;
    public $priority_level;
    public $severity;
    public $target_response_time;


    // protected $rules = [
    //     'priority_level' => 'required',
    //     'severity' => 'required',
    //     'target_response_time' => 'required',

    // ];

    // public function confirmationSubmit()
    // {
    //     $this->validate();

    //     $this->confirmation_modal = true;
    // }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.service-request.sr-related-mgmt.resolution.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function submit(ResolutionInterface $res_interface)
    {
        // dd($this->getRequest());
        
        $response = $res_interface->create($this->getRequest());
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.service-request.sr-related-mgmt.resolution.index', 'close_modal', 'create');
            $this->emitTo('crm.service-request.sr-related-mgmt.resolution.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.service-request.sr-related-mgmt.resolution.create');
    }
}
