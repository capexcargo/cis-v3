<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt;

use App\Interfaces\Crm\ServiceRequest\SRSub\SrSubCategoryInterface;
use App\Interfaces\Crm\ServiceRequest\SRType\SRTypeInterface;
use App\Traits\Crm\ServiceRequest\SRType\SRTypeTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use SRTypeTrait, WithPagination, PopUpMessagesTrait;

    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $type_id;
    public $confirmation_message;
    public $action_type;


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('crm.service-request.sr-related-mgmt.edit', 'edit', $data['id']);
            $this->type_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this SR Type?";
            $this->delete_modal = true;
            $this->type_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } 
        else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
        elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }

    public function confirm(SRTypeInterface $sr_types_interface)
    {
        if ($this->action_type == "delete") {
            $response = $sr_types_interface->destroy($this->type_id);
            $this->emitTo('crm.service-request.sr-related-mgmt.index', 'close_modal', 'delete');
            $this->emitTo('crm.service-request.sr-related-mgmt.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('crm.service-request.sr-related-mgmt.index', 'close_modal', 'delete');
            $this->emitTo('crm.service-request.sr-related-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(SRTypeInterface $sr_type_interface,SrSubCategoryInterface $sr_sub_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $sr_type_interface->index($request);
        $response_sub = $sr_sub_interface->index($request);

        // dd($response);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'sr_types' => [],
                ];
        }

        if ($response_sub['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response_sub['message'], $response_sub['result']);
            $response_sub['result'] =
                [
                    'sr_subs' => [],
                ];
        }
        // dd($response['result']['marketing_channelss']);

        return view('livewire.crm.service-request.sr-related-mgmt.index', [
            'sr_types' => $response['result']['sr_types'],'sr_subs' => $response_sub['result']['sr_subs']
        ]);
    }

}
