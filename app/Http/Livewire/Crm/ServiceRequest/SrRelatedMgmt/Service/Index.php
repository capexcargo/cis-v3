<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\Service;

use App\Interfaces\Crm\ServiceRequest\Service\ServiceInterface;
use App\Traits\Crm\ServiceRequest\Service\ServiceTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use ServiceTrait, WithPagination, PopUpMessagesTrait;

    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $serv_id;
    public $confirmation_message;


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('crm.service-request.sr-related-mgmt.service.edit', 'edit', $data['id']);
            $this->serv_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this Service Requirement?";
            $this->delete_modal = true;
            $this->serv_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } 
        else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
        elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }

    public function confirm(ServiceInterface $services_interface)
    {
        if ($this->action_type == "delete") {
            $response = $services_interface->destroy($this->serv_id);
            $this->emitTo('crm.service-request.sr-related-mgmt.service.index', 'close_modal', 'delete');
            $this->emitTo('crm.service-request.sr-related-mgmt.service.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('crm.service-request.sr-related-mgmt.service.index', 'close_modal', 'delete');
            $this->emitTo('crm.service-request.sr-related-mgmt.service.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(ServiceInterface $service_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $service_interface->index($request);

        // dd($response);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'm_services' => [],
                ];
        }
        // dd($response['result']['marketing_servicess']);

        return view('livewire.crm.service-request.sr-related-mgmt.service.index', [
            'm_services' => $response['result']['m_services']
        ]);
    }


}
