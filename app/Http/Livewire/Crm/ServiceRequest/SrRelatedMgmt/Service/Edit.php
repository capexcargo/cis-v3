<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\Service;

use App\Interfaces\Crm\ServiceRequest\Service\ServiceInterface;
use App\Models\ServiceRequirements;
use App\Traits\Crm\ServiceRequest\Service\ServiceTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{

    use ServiceTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $service;
    public $services;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->services = ServiceRequirements::findOrFail($id);

        $this->service = $this->services->name;
        // $this->category = $this->employment_category_type->employment_category_id;
    }

    public function submit(ServiceInterface $services)
    {
        $response = $services->update($this->getRequest(), $this->services->id);
        
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.service-request.sr-related-mgmt.service.index', 'close_modal', 'edit');
            $this->emitTo('crm.service-request.sr-related-mgmt.service.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render()
    {
        return view('livewire.crm.service-request.sr-related-mgmt.service.edit');
    }
}
