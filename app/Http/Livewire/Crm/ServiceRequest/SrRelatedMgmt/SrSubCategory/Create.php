<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\SrSubCategory;

use App\Interfaces\Crm\ServiceRequest\SRSub\SrSubCategoryInterface;
use App\Traits\Crm\ServiceRequest\SrSubCategory\SrSubCategoryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{

    use SrSubCategoryTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $srsub;
    public $category;

    // protected $rules = [
    //     'category' => 'required',
    //     'srsub' => 'required',
    // ];

    // public function confirmationSubmit()
    // {
    //     $this->validate();

    //     $this->confirmation_modal = true;
    // }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.service-request.sr-related-mgmt.sr-sub-category.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }


    
    public function submit(SrSubCategoryInterface $sub_category_interface)
    {
        // dd($this->getRequest());
        
        $response = $sub_category_interface->create($this->getRequest());
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.service-request.sr-related-mgmt.sr-sub-category.index', 'close_modal', 'create');
            $this->emitTo('crm.service-request.sr-related-mgmt.sr-sub-category.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.service-request.sr-related-mgmt.sr-sub-category.create');
    }
}
