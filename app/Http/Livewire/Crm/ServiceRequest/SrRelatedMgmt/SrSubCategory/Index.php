<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\SrSubCategory;

use App\Interfaces\Crm\ServiceRequest\SRSub\SrSubCategoryInterface;
use App\Interfaces\Crm\ServiceRequest\SRType\SRTypeInterface;
use App\Traits\Crm\ServiceRequest\SrSubCategory\SrSubCategoryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use SrSubCategoryTrait, WithPagination, PopUpMessagesTrait;

    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $sr_id;
    public $confirmation_message;


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('crm.service-request.sr-related-mgmt.sr-sub-category.edit', 'edit', $data['id']);
            $this->sr_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this SR Subcategory?";
            $this->delete_modal = true;
            $this->sr_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } 
        else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
        elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }

    public function confirm(SrSubCategoryInterface $sr_sub_interfaces)
    {
        if ($this->action_type == "delete") {
            $response = $sr_sub_interfaces->destroy($this->sr_id);
            $this->emitTo('crm.service-request.sr-related-mgmt.sr-sub-category.index', 'close_modal', 'delete');
            $this->emitTo('crm.service-request.sr-related-mgmt.sr-sub-category.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('crm.service-request.sr-related-mgmt.sr-sub-category.index', 'close_modal', 'delete');
            $this->emitTo('crm.service-request.sr-related-mgmt.sr-sub-category.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(SrSubCategoryInterface $sr_sub_interface, SRTypeInterface $sr_types_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $sr_sub_interface->index($request);
        $response_type = $sr_types_interface->index($request);

        // dd($response_type);
        // // dd($response_type['result']['sr_type']);

        if ($response_type['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response_type['message'], $response_type['result']);
            $response_type['result'] =
                [
                    'sr_types' => [],
                ];
        }


        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'sr_subs' => [],
                ];
        }

        return view('livewire.crm.service-request.sr-related-mgmt.sr-sub-category.index', [
            'sr_subs' => $response['result']['sr_subs'],'sr_types' => $response_type['result']['sr_types']
        ]);
    }

    // public function render()
    // {
    //     return view('livewire.crm.service-request.sr-related-mgmt.sr-sub-category.index');
    // }
}
