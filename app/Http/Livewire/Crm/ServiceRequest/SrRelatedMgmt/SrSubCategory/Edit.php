<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\SrSubCategory;

use App\Interfaces\Crm\ServiceRequest\SRSub\SrSubCategoryInterface;
use App\Models\SrTypeSubcategory;
use App\Traits\Crm\ServiceRequest\SrSubCategory\SrSubCategoryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use SrSubCategoryTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $srsub;
    public $category;
    public $sr_sub;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->sr_sub = SrTypeSubcategory::findOrFail($id);

        $this->srsub = $this->sr_sub->name;
        $this->category = $this->sr_sub->sr_type_id;
    }

    public function submit(SrSubCategoryInterface $sr_interface)
    {
        $response = $sr_interface->update($this->getRequest(), $this->sr_sub->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.service-request.sr-related-mgmt.sr-sub-category.index', 'close_modal', 'edit');
            $this->emitTo('crm.service-request.sr-related-mgmt.sr-sub-category.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.service-request.sr-related-mgmt.sr-sub-category.edit');
    }
}
