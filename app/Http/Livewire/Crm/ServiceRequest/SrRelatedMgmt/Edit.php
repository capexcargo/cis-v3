<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt;

use App\Interfaces\Crm\ServiceRequest\SRType\SRTypeInterface;
use App\Models\SrType;
use App\Traits\Crm\ServiceRequest\SRType\SRTypeTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{

    public $confirmation_modal;
    public $srtype;
    public $sr_type;
    
    use SRTypeTrait, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->sr_type = SrType::findOrFail($id);

        $this->srtype = $this->sr_type->name;
        // $this->category = $this->employment_category_type->employment_category_id;
    }

    public function submit(SRTypeInterface $sr_type)
    {
        $response = $sr_type->update($this->getRequest(), $this->sr_type->id);
        
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.service-request.sr-related-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('crm.service-request.sr-related-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.service-request.sr-related-mgmt.edit');
    }
}
