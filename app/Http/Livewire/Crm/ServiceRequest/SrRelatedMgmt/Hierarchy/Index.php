<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\Hierarchy;

use App\Interfaces\Crm\ServiceRequest\Hierarchy\HierarchyInterface;
use App\Traits\Crm\ServiceRequest\Hierarchy\HierarchyTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use HierarchyTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $email;
    public $hie_id;
    public $confirmation_message;


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } 
        elseif ($action_type == 'edit') {
            $this->emitTo('crm.service-request.sr-related-mgmt.hierarchy.edit', 'edit', $data['id']);
            $this->hie_id = $data['id'];
            $this->edit_modal = true;
        } 
        // else if ($action_type == 'delete') {
        //     $this->confirmation_message = "Are you sure you want to delete this Channel SR Source?";
        //     $this->delete_modal = true;
        //     $this->chan_id = $data['id'];
        // }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } 
        else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
        // elseif ($action_type == 'delete') {
        //     $this->delete_modal = false;
        // }
    }

    // public function confirm(ChannelInterface $channels_interface)
    // {
    //     if ($this->action_type == "delete") {
    //         $response = $channels_interface->destroy($this->chan_id);
    //         $this->emitTo('crm.service-request.sr-related-mgmt.channel.index', 'close_modal', 'delete');
    //         $this->emitTo('crm.service-request.sr-related-mgmt.channel.index', 'index');
    //     }

    //     if ($response['code'] == 200) {
    //         $this->emitTo('crm.service-request.sr-related-mgmt.channel.index', 'close_modal', 'delete');
    //         $this->emitTo('crm.service-request.sr-related-mgmt.channel.index', 'index');
    //         $this->sweetAlert('', $response['message']);
    //     } else {
    //         $this->sweetAlertError('error', $response['message'], $response['result']);
    //     }
    // }

    public function render(HierarchyInterface $hierarchy_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $hierarchy_interface->index($request);
        // $asd = 0;
        // foreach($response['result']['hierarchy_s'] as $val){
        //     $response2[] = $hierarchy_interface->getposition($response['result']['hierarchy_s'][$asd]['email']);
        //     $asd++;
        // }
  
        // dd($response2);
       

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'hierarchy_s' => [],
                ];
        }
   

        return view('livewire.crm.service-request.sr-related-mgmt.hierarchy.index', [
            'hierarchy_s' => $response['result']['hierarchy_s']
        ]);
    }

    
    
    // public function render()
    // {
    //     return view('livewire.crm.service-request.sr-related-mgmt.hierarchy.index');
    // }
}
