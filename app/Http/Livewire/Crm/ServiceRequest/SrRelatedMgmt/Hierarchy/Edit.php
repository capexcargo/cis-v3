<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\Hierarchy;

use App\Interfaces\Crm\ServiceRequest\Hierarchy\HierarchyInterface;
use App\Models\CrmHeirarchyDetails;
use App\Models\Hierarchy;
use App\Traits\Crm\ServiceRequest\Hierarchy\HierarchyTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use HierarchyTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $email;
    public $tagged_position_id;
    public $heirarchy_id;
    public $positions = [];
    public $hie;
    public $countarr;
    public $nearest;
    public $min;


    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->hie = Hierarchy::with('tagHasMany')->findOrFail($id);

        $this->heirarchy_id = $id;
        $this->email = $this->hie->email;


        // dd(count($this->hie->tagHasMany()));
        foreach ($this->hie->tagHasMany as $a => $tags) {

            $this->positions[$a] = [
                'id' => $tags->id,
                'tagged_position_id' => $tags->tagged_position_id,
                'is_deleted' => false,
            ];
        }

        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->positions as $a => $position) {

            $getkeyarray[] = $a;
            $countarray += ($this->positions[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    public function addPosition()
    {
        $this->positions[] = [
            'id' => null,
            'tagged_position_id' => null,
            'is_deleted' => false,
        ];

        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->positions as $a => $position) {

            if ($this->positions[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->positions[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    public function removePosition(array $data)
    {
        if (count(collect($this->positions)->where('is_deleted', false)) > 1) {
            if ($this->positions[$data["a"]]['id']) {
                $this->positions[$data["a"]]['is_deleted'] = true;
                // unset($this->positions[$data["a"]]);
            } else {
                unset($this->positions[$data["a"]]);
            }

            array_values($this->positions);
        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->positions as $a => $position) {

            if ($this->positions[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->positions[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    public function removePositionEdit(array $data)
    {
        if (count(collect($this->positions)->where('is_deleted', false)) > 1) {
            if ($this->positions[$data["a"]]['id']) {
                $this->positions[$data["a"]]['is_deleted'] = true;
                // unset($this->positions[$data["a"]]);
            } else {
                unset($this->positions[$data["a"]]);
            }

            array_values($this->positions);
        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->positions as $a => $position) {

            if($this->positions[$a]['is_deleted'] == false){
                $getkeyarray[] = $a;
            }

            $countarray += ($this->positions[$a]['is_deleted'] == false ? 1 : 0);

        }
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }



    public function submit(HierarchyInterface $hie_interface)
    {

        $rules = [
            'email' => 'required|unique:crm_hierarchy,email,' . $this->heirarchy_id . ',id',
        ];

        foreach ($this->positions as $i => $position) {
            $rules['positions.' . $i . '.id'] = 'sometimes';
            $rules['positions.' . $i . '.tagged_position_id'] = 'required';
            $rules['positions.' . $i . '.is_deleted'] = 'sometimes';
        }

        $validated = $this->validate(
            $rules,
            [
                // 'positions.*.unique:tagged_position_id.required' => 'The position field is required.',
                'email.required' => 'The Email Address field is required.',
                'positions.*.tagged_position_id.required' => 'The Tag field is required.',

            ]
        );

        // $response = $hie_interface->update($this->getRequest(), $this->hie->id);
        $response = $hie_interface->update($validated, $this->heirarchy_id);


        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.service-request.sr-related-mgmt.hierarchy.index', 'close_modal', 'edit');
            $this->emitTo('crm.service-request.sr-related-mgmt.hierarchy.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {

        return view('livewire.crm.service-request.sr-related-mgmt.hierarchy.edit');
    }
}
