<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\Hierarchy;

use App\Interfaces\Crm\ServiceRequest\Hierarchy\HierarchyInterface;
use App\Models\Hierarchy;
use App\Traits\Crm\ServiceRequest\Hierarchy\HierarchyTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{

    use HierarchyTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $email;
    public $tagged_position_id;
    public $positions = [];

    public function confirmationSubmit()
    {
        // $this->validate();

        $rules = [
            'email' => 'required|unique:crm_hierarchy,email',
        ];

        foreach ($this->positions as $i => $position) {
            $rules['positions.' . $i . '.id'] = 'sometimes';
            $rules['positions.' . $i . '.tagged_position_id'] = 'required';
        }

        $validated = $this->validate(
            $rules,
            [
                'positions.*.tagged_position_id.required' => 'The tagged position id field is required.',
            ]
        );

        $this->confirmation_modal = true;
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.service-request.sr-related-mgmt.hierarchy.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function addPosition()
    {
        $this->positions[] = [
            'id' => null,
            'tagged_position_id' => null,
            'is_deleted' => false,
        ];
    }

    public function removePosition(array $data)
    {
        if (count(collect($this->positions)->where('is_deleted', false)) > 1) {
            if ($this->positions[$data["a"]]['id']) {
                $this->positions[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->positions[$data["a"]]);
            }

            array_values($this->positions);
        }
    }


    public function submit(HierarchyInterface $hierarchy_interface)
    {

        $rules = [
            'email' => 'required|unique:crm_hierarchy,email',
        ];

        foreach ($this->positions as $i => $position) {
            $rules['positions.' . $i . '.id'] = 'sometimes';
            $rules['positions.' . $i . '.tagged_position_id'] = 'required';
        }

        $validated = $this->validate(
            $rules,
            [
                'email.required' => 'The Email Address field is required.',
                'positions.*.tagged_position_id.required' => 'The Tag field is required.',

            ]
        );

        // dd($validated);


        $response = $hierarchy_interface->create($this->getRequest());
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.service-request.sr-related-mgmt.hierarchy.index', 'close_modal', 'create');
            $this->emitTo('crm.service-request.sr-related-mgmt.hierarchy.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {

        if (!$this->positions) {
            $this->positions[] = [
                'id' => null,
                'tagged_position_id' => null,
                'is_deleted' => false,
            ];
        }

        return view('livewire.crm.service-request.sr-related-mgmt.hierarchy.create');
    }
}
