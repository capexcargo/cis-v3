<?php

namespace App\Http\Livewire\Crm\ServiceRequest\SrRelatedMgmt\Urgency;

use App\Interfaces\Crm\ServiceRequest\Resolution\ResolutionInterface;
use App\Interfaces\Crm\ServiceRequest\Response\ResponseMInterface;
use App\Interfaces\Crm\ServiceRequest\SRSub\SrSubCategoryInterface;
use App\Interfaces\Crm\ServiceRequest\SRType\SRTypeInterface;
use App\Interfaces\Crm\ServiceRequest\Urgency\UrgencyInterface;
use App\Traits\Crm\ServiceRequest\Urgency\UrgencyTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use UrgencyTrait, WithPagination, PopUpMessagesTrait;

    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $confirmation_message;
    public $counturgency;

    public function mount()
    {
        $this->loadUrgency();
    }

    public function submit(UrgencyInterface $urgency_interface)
    {

        // dd($this->urgency_impacts);
        foreach ($this->urgency_impacts as $i => $urgency_impact) {
            $rules['urgency_impacts.' . $i . '.severity'] = 'required';
            $rules['urgency_impacts.' . $i . '.high_priority_level'] = 'required';
            $rules['urgency_impacts.' . $i . '.medium_priority_level'] = 'required';
            $rules['urgency_impacts.' . $i . '.low_priority_level'] = 'required';
        }
        // dd($this->validate);

        $validated = $this->validate(
            $rules,
            [
                'urgency_impacts.*.severity.required' => 'The Urgency field is required.',
                'urgency_impacts.*.high_priority_level.required' => 'The Impact field is required.',
                'urgency_impacts.*.medium_priority_level.required' => 'The Impact field is required.',
                'urgency_impacts.*.low_priority_level.required' => 'The Impact field is required.',

            ]
        );


        $response = $urgency_interface->create($this->getRequest());

        if ($response['code'] == 200) {

            $this->emitTo('livewire.crm.service-request.sr-related-mgmt.urgency.index', 'index');
            $this->sweetAlert('', $response['message']);
            return redirect(request()->header('Referer'));
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    // public function action(array $data, $action_type)
    // {
    //     $this->action_type = $action_type;

    //     if ($action_type == 'add') {
    //         $this->create_modal = true;
    //     } 
    //     elseif ($action_type == 'edit') {
    //         $this->emitTo('crm.service-request.sr-related-mgmt.channel.edit', 'edit', $data['id']);
    //         $this->chan_id = $data['id'];
    //         $this->edit_modal = true;
    //     } else if ($action_type == 'delete') {
    //         $this->confirmation_message = "Are you sure you want to delete this Channel SR Source?";
    //         $this->delete_modal = true;
    //         $this->chan_id = $data['id'];
    //     }
    // }

    // public function closeModal($action_type)
    // {
    //     if ($action_type == 'create') {
    //         $this->create_modal = false;
    //     } 
    //     else if ($action_type == 'edit') {
    //         $this->edit_modal = false;
    //     } 
    //     elseif ($action_type == 'delete') {
    //         $this->delete_modal = false;
    //     }
    // }

    // public function confirm(ChannelInterface $channels_interface)
    // {
    //     if ($this->action_type == "delete") {
    //         $response = $channels_interface->destroy($this->chan_id);
    //         $this->emitTo('crm.service-request.sr-related-mgmt.channel.index', 'close_modal', 'delete');
    //         $this->emitTo('crm.service-request.sr-related-mgmt.channel.index', 'index');
    //     }

    //     if ($response['code'] == 200) {
    //         $this->emitTo('crm.service-request.sr-related-mgmt.channel.index', 'close_modal', 'delete');
    //         $this->emitTo('crm.service-request.sr-related-mgmt.channel.index', 'index');
    //         $this->sweetAlert('', $response['message']);
    //     } else {
    //         $this->sweetAlertError('error', $response['message'], $response['result']);
    //     }
    // }

    // public function render(UrgencyInterface $urgency_interface, ResolutionInterface $reso_interface, ResponseMInterface $res_interface)
    // {
    //     $request = [
    //         'paginate' => $this->paginate,
    //     ];

    //     $response = $urgency_interface->index($request);
    //     $response1 = $reso_interface->index($request);
    //     $response2 = $res_interface->index($request);

    //     // dd($response);

    //     if ($response2['code'] != 200) {
    //         $this->sweetAlertDefaultError('error', $response2['message'], $response2['result']);
    //         $response2['result'] =
    //             [
    //                 'm_responses' => [],
    //             ];
    //     }


    //     if ($response1['code'] != 200) {
    //         $this->sweetAlertDefaultError('error', $response1['message'], $response1['result']);
    //         $response1['result'] =
    //             [
    //                 'm_resolutions' => [],
    //             ];
    //     }

    //     if ($response['code'] != 200) {
    //         $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
    //         $response['result'] =
    //             [
    //                 'm_urgency' => [],
    //             ];
    //     }
    //     // dd($response['result']['marketing_urgencys']);

    //     return view('livewire.crm.service-request.sr-related-mgmt.urgency.index', [
    //         'm_urgency' => $response['result']['m_urgency'],'m_resolutions' => $response1['result']['m_resolutions'],'m_responses' => $response2['result']['m_responses']
    //     ]);
    // }


    // public function render(ResolutionInterface $reso_interface, ResponseMInterface $res_interface)
    // {
    //     $request = [
    //         'paginate' => $this->paginate,
    //     ];

    //     $response = $reso_interface->index($request);
    //     $response_res = $res_interface->index($request);

    //     // dd($response);

    //     if ($response_res['code'] != 200) {
    //         $this->sweetAlertDefaultError('error', $response_res['message'], $response_res['result']);
    //         $response_res['result'] =
    //             [
    //                 'm_responses' => [],
    //             ];
    //     }
    //     if ($response['code'] != 200) {
    //         $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
    //         $response['result'] =
    //             [
    //                 'm_resolutions' => [],
    //             ];
    //     }

    //     return view('livewire.crm.service-request.sr-related-mgmt.resolution.index', [
    //         'm_resolutions' => $response['result']['m_resolutions'],'m_responses' => $response_res['result']['m_responses']
    //     ]);
    // }

    public function render(UrgencyInterface $ugency_interface, ResolutionInterface $reso_interface, ResponseMInterface $res_interface)
    {
        // dd(count($this->urgency_impacts));
        $this->counturgency = count($this->urgency_impacts);
        $request = [
            'paginate' => $this->paginate,
        ];
        $response = $reso_interface->index($request);
        $response_res = $res_interface->index($request);

        // dd($response);

        if ($response_res['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response_res['message'], $response_res['result']);
            $response_res['result'] =
                [
                    'm_responses' => [],
                ];
        }
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'm_resolutions' => [],
                ];
        }
        return view('livewire.crm.service-request.sr-related-mgmt.urgency.index', [
            'm_resolutions' => $response['result']['m_resolutions'], 'm_responses' => $response_res['result']['m_responses']
        ]);
    }
}
