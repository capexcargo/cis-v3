<?php

namespace App\Http\Livewire\Crm\ServiceRequest\Knowledge;

use App\Interfaces\Crm\ServiceRequest\Knowledge\KnowledgeInterface;
use App\Traits\Crm\ServiceRequest\Knowledge\KnowledgeTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use KnowledgeTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $faq_concern;
    public $response_area;
    public $positions = [];
    

    public function confirmationSubmit()
    {
        // $this->validate();

        $rules = [
            'faq_concern' => 'required',
        ];

        foreach ($this->positions as $i => $position) {
            $rules['positions.' . $i . '.id'] = 'sometimes';
            $rules['positions.' . $i . '.response_area'] = 'required';
        }

        $validated = $this->validate(
            $rules,
            [
                'positions.*.response_area.required' => 'The Response field is required.',
            ]
        );

        $this->confirmation_modal = true;
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.service-request.knowledge.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function addPosition()
    {
        $this->positions[] = [
            'id' => null,
            'response_area' => null,
            'is_deleted' => false,

        ];
    }

    public function removePosition(array $data)
    {
        if (count(collect($this->positions)->where('is_deleted', false)) > 1) {
            if ($this->positions[$data["a"]]['id']) {
                $this->positions[$data["a"]]['is_deleted'] = true;
                
            } else {
                unset($this->positions[$data["a"]]);

            }

            array_values($this->positions);
        }
    }


    public function submit(KnowledgeInterface $knowledge_interface)
    {

        $rules = [
            'faq_concern' => 'required',
        ];

        foreach ($this->positions as $i => $position) {
            $rules['positions.' . $i . '.id'] = 'sometimes';
            $rules['positions.' . $i . '.response_area'] = 'required';
        }

        $validated = $this->validate(
            $rules,
            [
                'positions.*.response_area.required' => 'The Response field is required.',
                
            ]
        );

        // dd($validated);


        $response = $knowledge_interface->create($this->getRequest());
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.service-request.knowledge.index', 'close_modal', 'create');
            $this->emitTo('crm.service-request.knowledge.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {

        if(!$this->positions){
            $this->positions[] = [
                'id' => null,
                'response_area' => null,
                'is_deleted' => false,

            ];
        }
       
        return view('livewire.crm.service-request.knowledge.create');
    }
      
}
