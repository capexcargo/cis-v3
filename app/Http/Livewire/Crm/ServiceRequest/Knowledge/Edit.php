<?php

namespace App\Http\Livewire\Crm\ServiceRequest\Knowledge;

use App\Interfaces\Crm\ServiceRequest\Knowledge\KnowledgeInterface;
use App\Models\CrmKnowledge;
use App\Models\CrmKnowledgeResponse;
use App\Traits\Crm\ServiceRequest\Knowledge\KnowledgeTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{

    use KnowledgeTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $faq_concern;
    public $response_area;
    public $knowledge_id;
    public $response;
    public $positions = [];

    public $countarr;
    public $nearest;
    public $min;



    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->response = CrmKnowledge::with('respHasMany')->findOrFail($id);

        $this->knowledge_id = $id;
        $this->faq_concern = $this->response->faq_concern;


        // dd(count($this->response->tagHasMany()));
        foreach ($this->response->respHasMany as $a => $tags) {
            $this->positions[$a] = [
                'id' => $tags->id,
                'response_area' => $tags->response_area,
                'is_deleted' => false,
            ];
        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->positions as $a => $position) {

            $getkeyarray[] = $a;
            $countarray += ($this->positions[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    public function addPosition()
    {
        $this->positions[] = [
            'id' => null,
            'response_area' => null,
            'is_deleted' => false,
        ];
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->positions as $a => $position) {

            if ($this->positions[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->positions[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }


    public function removePosition(array $data)
    {
        if (count(collect($this->positions)->where('is_deleted', false)) > 1) {
            if ($this->positions[$data["a"]]['id']) {
                $this->positions[$data["a"]]['is_deleted'] = true;
                // unset($this->positions[$data["a"]]);
            } else {
                unset($this->positions[$data["a"]]);
            }

            array_values($this->positions);
        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->positions as $a => $position) {

            if ($this->positions[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->positions[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }


    public function submit(KnowledgeInterface $know_interface)
    {

        $rules = [
            'faq_concern' => 'required',
        ];

        foreach ($this->positions as $i => $position) {
            $rules['positions.' . $i . '.id'] = 'sometimes';
            $rules['positions.' . $i . '.response_area'] = 'required';
            $rules['positions.' . $i . '.is_deleted'] = 'sometimes';
        }

        $validated = $this->validate(
            $rules,
            [
                'positions.*.response_area.required' => 'The Response field is required.',

            ]
        );

        // $response = $hie_interface->update($this->getRequest(), $this->hie->id);
        $response = $know_interface->update($validated, $this->knowledge_id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.service-request.knowledge.index', 'close_modal', 'edit');
            $this->emitTo('crm.service-request.knowledge.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.crm.service-request.knowledge.edit');
    }
}
