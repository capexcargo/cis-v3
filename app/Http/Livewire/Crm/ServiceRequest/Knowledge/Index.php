<?php

namespace App\Http\Livewire\Crm\ServiceRequest\Knowledge;

use App\Interfaces\Crm\ServiceRequest\Knowledge\KnowledgeInterface;
use App\Models\CrmKnowledge;
use App\Traits\Crm\ServiceRequest\Knowledge\KnowledgeTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use KnowledgeTrait, WithPagination, PopUpMessagesTrait;

    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public $create_modal = false;
    public $edit_modal = false;
    public $reactivate_modal = false;
    public $deactivate_modal = false;
    public $faq_concern;
    public $status;
    public $response_area;
    public $res_id;
    public $positions = [];
    public $confirmation_message;

    public $activestatus;
    public $status_header_cards = [];
    public $stats;

    public $statusall;
    public $statusactives;
    public $statusinactives;




    public function load()
    {
        $this->loadStatusHeaderCards();
    }



    public function loadStatusHeaderCards()
    {
        // dd($this->status_header_cards);


        $activestatus = CrmKnowledge::get();

        $statusalls = CrmKnowledge::get();
        $statusactive = CrmKnowledge::where('status', 1)->get();
        $statusinactive = CrmKnowledge::where('status', 2)->get();

        $statusall = $statusalls->count();
        $statusactives = $statusactive->count();
        $statusinactives = $statusinactive->count();

        // dd($statusall,$statusactives,$statusinactives);

        // dd($activestatus);
        // dd($this->status_header_cards);



        $this->status_header_cards = [
            [
                'title' => 'All',
                'value' => $statusall,
                'class' => 'bg-white  border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => false
            ],
            [
                'title' => 'Active',
                'value' => $statusactives,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 1
            ],
            [
                'title' => 'Deactivated',
                'value' => $statusinactives,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 2
            ],


        ];
        // dd($this->status_header_cards);

    }


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('crm.service-request.knowledge.edit', 'edit', $data['id']);
            $this->res_id = $data['id'];
            $this->edit_modal = true;
        }
        // else if ($action_type == 'delete') {
        //     $this->confirmation_message = "Are you sure you want to Deactivate this Knowledge?";
        //     $this->delete_modal = true;
        //     $this->res_id = $data['id'];
        // }
        else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->res_id = $data['id'];
                return;
            }

            $this->deactivate_modal = true;
            $this->res_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        }
    }
    public function copied(){
        $this->sweetAlert('', 'The script has been successfully copied!');
    }

    public function notcopied(){
        $this->sweetAlert('', 'The script is Deactivated');
    }


    public function updated()
    {
        $this->resetPage();

    }

    public function updateStatus($id, $value)
    {
        // dd($value);
        $know = CrmKnowledge::findOrFail($id);
        $know->update([
            'status' => $value,
        ]);



        if ($value == 1) {

            $this->emitTo('crm.service-request.knowledge.index', 'close_modal', 'update_status');
            $this->emitTo('crm.service-request.knowledge.index', 'index');
            $this->sweetAlert('', 'Script has been Successfully Reactivated!');
            return;

            // return redirect()->to(route('crm.service-request.knowledge.index'));

        }

        $this->emitTo('crm.service-request.knowledge.index', 'close_modal', 'update_status');
        $this->emitTo('crm.service-request.knowledge.index', 'index');
        $this->sweetAlert('', 'Script has been Successfully Deactivated!');


    }

    // public function confirm(KnowledgeInterface $knowledge_interface)
    // {
    //     if ($this->action_type == "delete") {
    //         $response = $knowledge_interface->update($this->getRequest(),$this->res_id);
    //         $this->emitTo('crm.service-request.knowledge.index', 'close_modal', 'delete');
    //         $this->emitTo('crm.service-request.knowledge.index', 'index');
    //     }

    //     if ($response['code'] == 200) {
    //         $this->emitTo('crm.service-request.knowledge.index', 'close_modal', 'delete');
    //         $this->emitTo('crm.service-request.knowledge.index', 'index');
    //         $this->sweetAlert('', $response['message']);
    //     } else {
    //         $this->sweetAlertError('error', $response['message'], $response['result']);
    //     }
    // }

    public function render(KnowledgeInterface $knowledge_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'stats' => $this->stats,

        ];

        $response = $knowledge_interface->index($request);
        // $asd = 0;
        // foreach($response['result']['hierarchy_s'] as $val){
        //     $response2[] = $hierarchy_interface->getposition($response['result']['hierarchy_s'][$asd]['email']);
        //     $asd++;
        // }

        // dd($response2);


        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'knowledge_s' => [],
                ];
        }


        return view('livewire.crm.service-request.knowledge.index', [
            'knowledge_s' => $response['result']['knowledge_s']
        ]);
    }

    // public function render()
    // {
    //     return view('livewire.crm.service-request.knowledge.index');
    // }
}
