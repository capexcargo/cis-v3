<?php

namespace App\Http\Livewire\Crm\ServiceRequest\Knowledge\SalesCoach;

use App\Interfaces\Crm\ServiceRequest\SalesCoach\SalesCoachInterface;
use App\Models\CrmSalesCoach;
use App\Models\CrmSalesCoachProcess;
use App\Models\CrmSalesCoachSubprocess;
use App\Traits\Crm\ServiceRequest\SalesCoach\SalesCoachTrait;
use App\Traits\PopUpMessagesTrait;
use CreateCrmSalesCoachProcess;
use Livewire\Component;

class Edit extends Component
{


    use SalesCoachTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $faq_concern;
    public $res;
    public $positions = [];
    public $sales_coach_id;
    public $title;
    public $description;
    public $process;
    public $sub_process;

    public $sales_coaches;
    public $sales_coach_process;
    public $sales_coach_subprocess;





    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // $this->resetForm();
        $this->sales_coaches = CrmSalesCoach::findOrFail($id);
        $this->sales_coach_process = CrmSalesCoachProcess::where('sales_coach_id', $id)->get();


        $this->sales_coach_id = $id;
        $this->title = $this->sales_coaches->title;
        $this->description = $this->sales_coaches->description;


        foreach ($this->sales_coach_process as $a => $sales_coach) {


            $this->sales_coach_subprocess = CrmSalesCoachSubprocess::where('sales_coach_process_id', $sales_coach->id)->get();

            $this->pros[$a][$a] = [
                'id' => $sales_coach->id,
                'title' => $this->sales_coaches->title,
                'description' => $this->sales_coaches->description,
                'process' => $sales_coach->process_content,
                'created_by' => null,
                'is_deleted' => false,
            ];


            $b = 0;
            foreach ($this->sales_coach_subprocess as $c => $subprocess) {
                $this->subpros[$a][$c] = [
                    'id' => $subprocess->id,
                    'sub_process' => $subprocess->process_content,
                    'is_deleted' => false,
                ];

                $b++;
            }
        }

    
        // dd($this->pros);
        
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->pros as $a => $pro) {

            if ($this->pros[$a][$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->pros[$a][$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->countarr = $countarray;
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);

    }

    public function submit(SalesCoachInterface $sales_coach_interface)
    {

        $rules = [
            'title' => 'required',
            'description' => 'sometimes',
        ];

        foreach ($this->pros as $i => $position) {
            // dd($rules['pros.' . $i . '.' . $i . '.id']);
            $rules['pros.' . $i . '.' . $i . '.id'] = 'sometimes';
            $rules['pros.' . $i . '.' . $i . '.process'] = 'required';
            $rules['pros.' . $i . '.' . $i . '.is_deleted'] = 'sometimes';
            $b = 0;
            if (isset($this->subpros[$i])) {
                foreach ($this->subpros[$i] as $c => $subpro) {

                    $rules['subpros.' . $i . '.' . $c . '.id'] = 'sometimes';
                    $rules['subpros.' . $i . '.' . $c . '.sub_process'] = 'required';
                    $rules['subpros.' . $i . '.' . $c . '.is_deleted'] = 'sometimes';

                    $b++;
                }
            }
        }

        $validated = $this->validate(
            $rules,
            [
                'pros.*.*.process.required' => 'The Process field is required.',
                'subpros.*.*.sub_process.required' => 'The SUBProcess field is required.',
            ]
        );
        // dd($validated);
        $response = $sales_coach_interface->update($validated, $this->sales_coach_id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.service-request.knowledge.sales-coach.index', 'close_modal', 'edit');
            $this->emitTo('crm.service-request.knowledge.sales-coach.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render()
    {
        return view('livewire.crm.service-request.knowledge.sales-coach.edit');
    }
}
