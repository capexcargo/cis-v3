<?php

namespace App\Http\Livewire\Crm\ServiceRequest\Knowledge\SalesCoach;

use App\Interfaces\Crm\ServiceRequest\SalesCoach\SalesCoachInterface;
use App\Models\CrmSalesCoach;
use App\Traits\Crm\ServiceRequest\SalesCoach\SalesCoachTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use SalesCoachTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $reactivate_modal = false;
    public $deactivate_modal = false;
    public $action_type;
    public $sales_coach_id;

    public $activestatus;
    public $status_header_cards = [];
    public $stats;
    public $status;

    public $statusall;
    public $statusactives;
    public $statusinactives;

    public function load()
    {
        $this->loadStatusHeaderCards();
    }

    public function copied(){
        $this->sweetAlert('', 'The script has been successfully copied!');
    }

    public function notcopied(){
        $this->sweetAlert('', 'The script is Deactivated');
    }

    public function loadStatusHeaderCards()
    {
        // dd($this->status_header_cards);


        $activestatus = CrmSalesCoach::get();

        $statusalls = CrmSalesCoach::get();
        $statusactive = CrmSalesCoach::where('status', 1)->get();
        $statusinactive = CrmSalesCoach::where('status', 2)->get();

        $statusall = $statusalls->count();
        $statusactives = $statusactive->count();
        $statusinactives = $statusinactive->count();

        // dd($statusall,$statusactives,$statusinactives);

        // dd($activestatus);
        // dd($this->status_header_cards);



        $this->status_header_cards = [
            [
                'title' => 'All',
                'value' => $statusall,
                'class' => 'bg-white  border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => false
            ],
            [
                'title' => 'Active',
                'value' => $statusactives,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 1
            ],
            [
                'title' => 'Deactivated',
                'value' => $statusinactives,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 2
            ],


        ];
        // dd($this->status_header_cards);

    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('crm.service-request.knowledge.sales-coach.edit', 'edit', $data['id']);
            $this->sales_coach_id = $data['id'];

            // dd($this->sales_coach_id);
            $this->edit_modal = true;
        }
        else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->sales_coach_id = $data['id'];
                return;
            }

            $this->deactivate_modal = true;
            $this->sales_coach_id = $data['id'];
        }
        // else if ($action_type == 'delete') {
        //     $this->confirmation_message = "Are you sure you want to delete this Knowledge?";
        //     $this->delete_modal = true;
        //     $this->res_id = $data['id'];
        // }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        }
        // elseif ($action_type == 'delete') {
        //     $this->delete_modal = false;
        // }
        
    }

    public function updated()
    {
        $this->resetPage();
    }

    public function updateStatus($id, $value)
    {
        // dd($value);
        $know = CrmSalesCoach::findOrFail($id);
        $know->update([
            'status' => $value,
        ]);



        if ($value == 1) {

            $this->emitTo('crm.service-request.knowledge.sales-coach.index', 'close_modal', 'update_status');
            $this->emitTo('crm.service-request.knowledge.sales-coach.index', 'index');
            $this->sweetAlert('', 'Sales Coach has been Successfully Reactivated!');
            return;

            // return redirect()->to(route('crm.service-request.knowledge.sales-coach.index'));

        }

        $this->emitTo('crm.service-request.knowledge.sales-coach.index', 'close_modal', 'update_status');
        $this->emitTo('crm.service-request.knowledge.sales-coach.index', 'index');
        $this->sweetAlert('', 'Sales Coach has been Successfully Deactivated!');
    }

    public function render(SalesCoachInterface $sales_coach_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'stats' => $this->stats,
        ];

        $response = $sales_coach_interface->index($request);
        // $asd = 0;
        // foreach($response['result']['hierarchy_s'] as $val){
        //     $response2[] = $hierarchy_interface->getposition($response['result']['hierarchy_s'][$asd]['email']);
        //     $asd++;
        // }

        // dd($response2);


        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'sales_coachs' => [],
                ];
        }


        return view('livewire.crm.service-request.knowledge.sales-coach.index', [
            'sales_coachs' => $response['result']['sales_coachs']
        ]);
    }
}
