<?php

namespace App\Http\Livewire\Crm\ServiceRequest\Knowledge\SalesCoach;

use App\Interfaces\Crm\ServiceRequest\SalesCoach\SalesCoachInterface;
use App\Traits\Crm\ServiceRequest\SalesCoach\SalesCoachTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{

    use SalesCoachTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $title;
    public $description;
    public $process;


    public function confirmationSubmit()
    {

        $rules = [
            'title' => 'required',
            'description' => 'sometimes',
        ];

        foreach ($this->pros as $i => $position) {
            // dd($rules['pros.' . $i . '.' . $i . '.id']);
            $rules['pros.' . $i . '.' . $i . '.id'] = 'sometimes';
            $rules['pros.' . $i . '.' . $i . '.process'] = 'required';
            $b = 0;
            if (isset($this->subpros[$i])) {
                foreach ($this->subpros[$i] as $c => $subpro) {

                    $rules['subpros.' . $i . '.' . $c . '.id'] = 'sometimes';
                    $rules['subpros.' . $i . '.' . $c . '.sub_process'] = 'required';

                    $b++;
                }
            }
        }

        $validated = $this->validate(
            $rules,
            [
                'pros.*.*.process.required' => 'The Process field is required.',
                'subpros.*.*.sub_process.required' => 'The SUBProcess field is required.',
            ]
        );

        $this->confirmation_modal = true;
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.service-request.knowledge.sales-coach.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }


    public function submit(SalesCoachInterface $sales_coach_interface)
    {

        $rules = [
            'title' => 'required',
            'description' => 'sometimes',
        ];

        foreach ($this->pros as $i => $position) {
            // dd($rules['pros.' . $i . '.' . $i . '.id']);
            $rules['pros.' . $i . '.' . $i . '.id'] = 'sometimes';
            $rules['pros.' . $i . '.' . $i . '.process'] = 'required';
            $b = 0;
            if (isset($this->subpros[$i])) {
                foreach ($this->subpros[$i] as $c => $subpro) {

                    $rules['subpros.' . $i . '.' . $c . '.id'] = 'sometimes';
                    $rules['subpros.' . $i . '.' . $c . '.sub_process'] = 'required';

                    $b++;
                }
            }
        }

        $validated = $this->validate(
            $rules,
            [
                'pros.*.*.process.required' => 'The Processasd field is required.',
                'subpros.*.*.sub_process.required' => 'The SUBProcessasd field is required.',
            ]
        );


        // dd($validated);


        $response = $sales_coach_interface->create($validated);
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.service-request.knowledge.sales-coach.index', 'close_modal', 'create');
            $this->emitTo('crm.service-request.knowledge.sales-coach.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {

        if (!$this->pros) {
            $this->pros[][] = [
                'id' => null,
                'process' => null,
            ];
        }
        // if (!$this->subpros) {
        //     $this->subpros[][] = [
        //         'id' => null,
        //         'process_content' => null,
        //         'is_deleted' => false,
        //     ];
        // }


        return view('livewire.crm.service-request.knowledge.sales-coach.create');
    }
}
