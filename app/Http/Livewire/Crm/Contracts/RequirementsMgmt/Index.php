<?php

namespace App\Http\Livewire\Crm\Contracts\RequirementsMgmt;

use App\Interfaces\Crm\Contracts\RequirementsMgmt\RequirementsMgmtInterface;
use App\Models\CrmApplicationRequirements;
use App\Traits\Crm\Contracts\RequirementsMgmt\RequirementsMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use RequirementsMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $create_modal = false;
    public $edit_modal = false;
    // public $delete_modal = false;
    public $reactivate_modal = false;
    public $deactivate_modal = false;
    public $confirmation_message;

    public $name;
    public $req_id;

    public $action_type;
    public $search_request;


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'add') {
            $this->create_modal = true;
        } 
        elseif ($action_type == 'edit') {
            $this->emitTo('crm.contracts.requirements-mgmt.edit', 'edit', $data['id']);
            $this->req_id = $data['id'];
            $this->edit_modal = true;
        } 
        // else if ($action_type == 'delete') {
        //     $this->confirmation_message = "Are you sure you want to delete this Accounts Application Requirements?";
        //     $this->delete_modal = true;
        //     $this->req_id = $data['id'];
        // }
        else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->req_id = $data['id'];
                return;
            }

            $this->deactivate_modal = true;
            $this->req_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } 
        else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
        // elseif ($action_type == 'delete') {
        //     $this->delete_modal = false;
        // }
        elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        }
    }

    public function updateStatus($id, $value)
    {
        // dd($value);
        $reqmgmt = CrmApplicationRequirements::findOrFail($id);
        // dd($reqmgmt);

        $reqmgmt->update([
            'status' => $value,
        ]);



        if ($value == 1) {

            $this->emitTo('crm.contracts.requirements-mgmt.index', 'close_modal', 'update_status');
            $this->emitTo('crm.contracts.requirements-mgmt.index', 'index');
            $this->sweetAlert('', 'Application Requirement Successfully Reactivated!');
            return;


        }

        $this->emitTo('crm.contracts.requirements-mgmt.index', 'close_modal', 'update_status');
        $this->emitTo('crm.contracts.requirements-mgmt.index', 'index');
        $this->sweetAlert('', 'Application Requirement Successfully Deactivated!');
    }

    // public function confirm(RequirementsMgmtInterface $req_interface)
    // {
    //     if ($this->action_type == "delete") {
    //         $response = $req_interface->destroy($this->req_id);
    //         $this->emitTo('crm.contracts.requirements-mgmt.index', 'close_modal', 'delete');
    //         $this->emitTo('crm.contracts.requirements-mgmt.index', 'index');
    //     }

    //     if ($response['code'] == 200) {
    //         $this->emitTo('crm.contracts.requirements-mgmt.index', 'close_modal', 'delete');
    //         $this->emitTo('crm.contracts.requirements-mgmt.index', 'index');
    //         $this->sweetAlert('', $response['message']);
    //     } else {
    //         $this->sweetAlertError('error', $response['message'], $response['result']);
    //     }
    // }


    public function render(RequirementsMgmtInterface $req_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $req_interface->index($request, $this->getRequest());

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'req_s' => [],
                ];
        }

        return view('livewire.crm.contracts.requirements-mgmt.index', [
            'req_s' => $response['result']['req_s']
        ]);
    }
}
