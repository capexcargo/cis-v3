<?php

namespace App\Http\Livewire\Crm\Contracts\RequirementsMgmt;

use App\Interfaces\Crm\Contracts\RequirementsMgmt\RequirementsMgmtInterface;
use App\Models\CrmApplicationRequirements;
use App\Traits\Crm\Contracts\RequirementsMgmt\RequirementsMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use RequirementsMgmtTrait, PopUpMessagesTrait;

    public $confirmation_modal;
    public $name;
    public $req_s;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->req_s = CrmApplicationRequirements::findOrFail($id);

        $this->name = $this->req_s->name;
    }

    public function submit(RequirementsMgmtInterface $req_interface)
    {
        $response = $req_interface->update($this->getRequest(), $this->req_s->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.contracts.requirements-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('crm.contracts.requirements-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.crm.contracts.requirements-mgmt.edit');
    }
}
