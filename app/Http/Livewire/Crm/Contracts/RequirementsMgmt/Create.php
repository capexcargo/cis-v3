<?php

namespace App\Http\Livewire\Crm\Contracts\RequirementsMgmt;

use App\Interfaces\Crm\Contracts\RequirementsMgmt\RequirementsMgmtInterface;
use App\Traits\Crm\Contracts\RequirementsMgmt\RequirementsMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use RequirementsMgmtTrait, PopUpMessagesTrait;

    public $confirmation_modal;


    // protected $rules = [
    //     'name' => 'required',
    // ];

    // public function confirmationSubmit()
    // {
    //     $this->validate();

    //     $this->confirmation_modal = true;
    // }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('crm.contracts.requirements-mgmt.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }
    
    public function submit(RequirementsMgmtInterface $req_interface)
    {
        // dd($this->getRequest());
        
        $response = $req_interface->create($this->getRequest());
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('crm.contracts.requirements-mgmt.index', 'close_modal', 'create');
            $this->emitTo('crm.contracts.requirements-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.crm.contracts.requirements-mgmt.create');
    }
}
