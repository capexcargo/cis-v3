<?php

namespace App\Http\Livewire\Crm\Contracts\AccountsApplication;

use App\Interfaces\Crm\Contracts\AccountsApplication\AccountsApplicationInterface;
use App\Models\Crm\CrmAccountApplicationAttachment;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\CrmApplicationRequirements;
use App\Repositories\Crm\Contracts\AccountsApplication\AccountsApplicationRepository;
use App\Traits\Crm\Contracts\AccountsApplication\AccountsApplicationTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class ViewAttachments extends Component
{

    use AccountsApplicationTrait, WithFileUploads, PopUpMessagesTrait;

    public $confirmation_modal = false;
    public $delete_modal = false;
    public $view_image_modal = false;
    public $confirmation_message;
    public $fullname;
    public $accno;
    public $industry;
    public $cfname;
    public $cmname;
    public $clname;
    public $cpnum;
    public $tin;
    public $createdat;
    public $set_requirement_id;
    public $set_account_info_id;
    public $reqmgmts = [];

    public $applications = [];
    public $validitems = [];

    public $img_id;

    protected $listeners = ['view_attachments' => 'mount', 'close_modal' => 'closeModal',];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'view-image') {
            $this->emitTo('crm.contracts.accounts-application.view-image', 'view-image', $data['id']);
            $this->img_id = $data['id'];
            $this->view_image_modal = true;
        }
         elseif ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this Accounts Application?";
            $this->delete_modal = true;
            $this->img_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'view-image') {
            $this->view_image_modal = false;
        } elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }

    public function mount($id)
    {
        $viewapp = CrmCustomerInformation::with('industry', 'contactPersons', 'mobileNumbers', 'appAttachment')->where('id', $id)->first();
        //    dd($viewapp->mobileNumbers[0]['mobile']);

        $this->fullname = $viewapp->fullname;
        $this->accno = $viewapp->account_no;
        $this->industry = $viewapp->industry->name;
        $this->cfname = $viewapp->contactPersons[0]['first_name'];
        $this->cmname = $viewapp->contactPersons[0]['middle_name'];
        $this->clname = $viewapp->contactPersons[0]['last_name'];
        $this->cpnum = $viewapp->mobileNumbers[0]['mobile'];
        $this->tin = $viewapp->tin;
        $this->createdat = $viewapp->created_at;
        $this->set_account_info_id = $id;



        //    $this->attach = $viewapp->appAttachment[0]['mobile'];

        $this->reqmgmts = CrmApplicationRequirements::with('appRequirements')->get();

        // dd($this->reqmgmts[0]['appRequirements']['application_requirement_id']);
        
       

        $this->validitems = CrmAccountApplicationAttachment::get();

        // dd($this->validitem[1]['application_requirement_id']);

        // dd($this->reqmgmt);
    }

    public function setid($id)
    {
        $this->set_requirement_id = $id;
    }
    public function updatedapplications()
    {
        // dd($this->applications, $this->set_requirement_id, $this->set_account_info_id);


        $accounts_applications_repository = new AccountsApplicationRepository();
        $response = $accounts_applications_repository->create($this->getRequest());

        // dd($response);

        if ($response['code'] == 200) {
            $this->resetErrorBag();
            $this->emitTo('crm.contracts.accounts-application.view-attachments', $this->set_requirement_id);
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'applications' => $this->applications,
            'set_requirement_id' => $this->set_requirement_id,
            'set_account_info_id' => $this->set_account_info_id,
        ];
    }

    public function confirm(AccountsApplicationInterface $accapp_interface)
    {
        if ($this->action_type == "delete") {
            $response = $accapp_interface->destroy($this->img_id);
            $this->emitTo('crm.contracts.accounts-application.view-attachments', 'close_modal', 'delete');
            $this->emitTo('crm.contracts.accounts-application.view-attachments', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('crm.contracts.accounts-application.view-attachments', 'close_modal', 'delete');
            $this->emitTo('crm.contracts.accounts-application.view-attachments', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        // dd($this->updatedaccount());
        return view('livewire.crm.contracts.accounts-application.view-attachments');
    }
}
