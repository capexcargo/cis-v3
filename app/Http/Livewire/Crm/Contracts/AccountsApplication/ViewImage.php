<?php

namespace App\Http\Livewire\Crm\Contracts\AccountsApplication;

use App\Models\CrmApplicationRequirements;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;


class ViewImage extends Component
{

    use PopUpMessagesTrait, WithFileUploads;

    public  $reqmgmts = [];
    public  $appRequirements = [];
    // public  $appRequirementss = [];

    public function mount($id)
    {
        $this->reqmgmts = CrmApplicationRequirements::with('appRequirements')->findOrFail($id);

        if ($id == $this->reqmgmts->appRequirements['application_requirement_id']) {
            $this->appRequirements[] = [
                'id' => $this->reqmgmts->appRequirements['id'],
                'account_info_id' => $this->reqmgmts->appRequirements['account_info_id'],
                'application_requirement_id' => $this->reqmgmts->appRequirements['application_requirement_id'],
                'path' => $this->reqmgmts->appRequirements['path'],
                'name' => $this->reqmgmts->appRequirements['name'],
                'extension' => $this->reqmgmts->appRequirements['extension'],
                'is_deleted' => false,
            ];
        }
    }



    public function render()
    {
        return view('livewire.crm.contracts.accounts-application.view-image');
    }
}
