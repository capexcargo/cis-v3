<?php

namespace App\Http\Livewire\Crm\Contracts\AccountsApplication;

use App\Interfaces\Crm\Contracts\AccountsApplication\AccountsApplicationInterface;
use App\Models\Crm\CrmCustomerInformation;
use App\Traits\Crm\Contracts\AccountsApplication\AccountsApplicationTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use AccountsApplicationTrait, WithPagination, PopUpMessagesTrait;

    public $view_modal = false;
    public $view_attachments_modal = false;
    public $reactivate_modal = false;
    public $deactivate_modal = false;
    public $acc_id;

    public $approve1_modal = false;
    public $decline1_modal = false;
    public $approve2_modal = false;
    public $decline2_modal = false;
    public $approve3_modal = false;
    public $decline3_modal = false;
    public $approve4_modal = false;
    public $decline4_modal = false;

    public $status_header_cards = [];
    public $stats;

    public $countall;

    public $search_request;
    public $created_at;
    public $account_no;
    public $company_name;



    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        // if ($action_type == 'add') {
        //     $this->create_modal = true;
        // } elseif ($action_type == 'edit') {
        //     $this->emitTo('crm.service-request.knowledge.edit', 'edit', $data['id']);
        //     $this->res_id = $data['id'];
        //     $this->edit_modal = true;
        // }
        if ($action_type == 'view') {
            $this->emitTo('crm.contracts.accounts-application.view', 'view', $data['id']);
            $this->acc_id = $data['id'];
            $this->view_modal = true;
        }
        if ($action_type == 'view_attachments') {
            $this->emitTo('crm.contracts.accounts-application.view-attachments', 'view-attachments', $data['id']);
            $this->acc_id = $data['id'];
            $this->view_attachments_modal = true;
        }
         else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->acc_id = $data['id'];
                return;
            }
            $this->deactivate_modal = true;
            $this->acc_id = $data['id'];
        } else if ($action_type == 'update_app1') {
            if ($data['approver1_status'] == 1) {
                $this->approve1_modal = true;
                $this->acc_id = $data['id'];
                return;
            }
            $this->decline1_modal = true;
            $this->acc_id = $data['id'];
        } else if ($action_type == 'update_app2') {
            if ($data['approver2_status'] == 1) {
                $this->approve2_modal = true;
                $this->acc_id = $data['id'];
                return;
            }
            $this->decline2_modal = true;
            $this->acc_id = $data['id'];
        } else if ($action_type == 'update_app3') {
            if ($data['approver3_status'] == 1) {
                $this->approve3_modal = true;
                $this->acc_id = $data['id'];
                return;
            }
            $this->decline3_modal = true;
            $this->acc_id = $data['id'];
        } else if ($action_type == 'update_app4') {
            if ($data['approver4_status'] == 1) {
                $this->approve4_modal = true;
                $this->acc_id = $data['id'];
                return;
            }
            $this->decline4_modal = true;
            $this->acc_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'view') {
            $this->view_modal = false;
        }
        elseif ($action_type == 'view_attachments') {
            $this->view_attachments_modal = false;
        }
         elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        } elseif ($action_type == 'update_app1') {
            $this->approve1_modal = false;
            $this->decline1_modal = false;
        } elseif ($action_type == 'update_app2') {
            $this->approve2_modal = false;
            $this->decline2_modal = false;
        } elseif ($action_type == 'update_app3') {
            $this->approve3_modal = false;
            $this->decline3_modal = false;
        } elseif ($action_type == 'update_app4') {
            $this->approve4_modal = false;
            $this->decline4_modal = false;
        }
    }

    public function load()
    {
        $this->loadStatusHeaderCards();
        $this->stats = '';
    }

    public function loadStatusHeaderCards()
    {
        $activestatus = CrmCustomerInformation::where('account_type', 2)->get();

        $statusalls = CrmCustomerInformation::where('account_type', 2)->get();
        $forap = CrmCustomerInformation::whereNull('final_status')->where('account_type', 2)->get();
        $approve = CrmCustomerInformation::where('final_status', 1)->where('account_type', 2)->get();
        $decline = CrmCustomerInformation::where('final_status', 2)->where('account_type', 2)->get();

        // dd($forap);


        $statusall = $statusalls->count();
        $foraped = $forap->count();
        $approved = $approve->count();
        $declined = $decline->count();

        $this->countall = $statusall;

        // dd($foraped);

        $this->status_header_cards = [
            [
                'title' => 'All',
                'value' => '',
                'class' => 'bg-white  border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => false
            ],
            [
                'title' => 'For Approval',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 3,
            ],
            [
                'title' => 'Approved',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 1
            ],
            [
                'title' => 'Declined',
                'value' => '',
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 2
            ],
        ];
    }

    public function updated()
    {
        $this->resetPage();
    }

    public function updateStatus($id, $value)
    {
        // dd($value);
        $know = CrmCustomerInformation::findOrFail($id);
        $know->update([
            'status' => $value,
        ]);



        if ($value == 1) {
            $this->emitTo('crm.contracts.accounts-application.index', 'close_modal', 'update_status');
            $this->emitTo('crm.contracts.accounts-application.index', 'index');
            $this->sweetAlert('', 'Accounts Successfully Reactivated!');
            return;
            // return redirect()->to(route('crm.contracts.accounts-application.index'));
        }

        $this->emitTo('crm.contracts.accounts-application.index', 'close_modal', 'update_status');
        $this->emitTo('crm.contracts.accounts-application.index', 'index');
        $this->sweetAlert('', 'Accounts Successfully Deactivated!');
    }

    public function updateApp1($id, $value1)
    {
        $app1 = CrmCustomerInformation::findOrFail($id);
        $app1->update([
            'approver1_id' => Auth::user()->id,
            'approver1_status' => $value1,
            'approver1_date' => date("Y-m-d"),
        ]);

        // if ($app1->approver2_status == 1 && $app1->approver3_status == 1 && $app1->approver4_status == 1) {
        //     // if ($value1 != 2) {
        //     $app1->update([
        //         'final_status' => $value1,
        //     ]);
        //     // }
        // }

        if ($value1 == 1) {

            $this->emitTo('crm.contracts.accounts-application.index', 'close_modal', 'update_app1');
            $this->emitTo('crm.contracts.accounts-application.index', 'index');
            $this->sweetAlert('', 'Account Successfully Approved!');
            return;

            // }elseif($app1->approver2_status_id == 3){
            //     $app1->update([
            //         'final_status_id' => $value1,
            //     ]);
            // }
        } else {
            $app1->update([
                'final_status' => $value1,
                'approval_date' => date("Y-m-d"),
            ]);
        }

        $this->emitTo('crm.contracts.accounts-application.index', 'close_modal', 'update_app1');
        $this->emitTo('crm.contracts.accounts-application.index', 'index');
        $this->sweetAlert('', 'Account Successfully Declined!');
    }

    public function updateApp2($id, $value2)
    {
        $app2 = CrmCustomerInformation::findOrFail($id);
        $app2->update([
            'approver2_id' => Auth::user()->id,
            'approver2_status' => $value2,
            'approver2_date' => date("Y-m-d"),
        ]);

        if ($value2 == 1) {

            $this->emitTo('crm.contracts.accounts-application.index', 'close_modal', 'update_app2');
            $this->emitTo('crm.contracts.accounts-application.index', 'index');
            $this->sweetAlert('', 'Account Successfully Approved!');
            return;
        } else {
            $app2->update([
                'final_status' => $value2,
                'approval_date' => date("Y-m-d"),
            ]);
        }

        $this->emitTo('crm.contracts.accounts-application.index', 'close_modal', 'update_app2');
        $this->emitTo('crm.contracts.accounts-application.index', 'index');
        $this->sweetAlert('', 'Account Successfully Declined!');
    }

    public function updateApp3($id, $value3)
    {
        $app3 = CrmCustomerInformation::findOrFail($id);
        $app3->update([
            'approver3_id' => Auth::user()->id,
            'approver3_status' => $value3,
            'approver3_date' => date("Y-m-d"),
        ]);

        if ($value3 == 1) {

            $this->emitTo('crm.contracts.accounts-application.index', 'close_modal', 'update_app3');
            $this->emitTo('crm.contracts.accounts-application.index', 'index');
            $this->sweetAlert('', 'Account Successfully Approved!');
            return;
        } else {
            $app3->update([
                'final_status' => $value3,
                'approval_date' => date("Y-m-d"),
            ]);
        }

        $this->emitTo('crm.contracts.accounts-application.index', 'close_modal', 'update_app3');
        $this->emitTo('crm.contracts.accounts-application.index', 'index');
        $this->sweetAlert('', 'Account Successfully Declined!');
    }

    public function updateApp4($id, $value4)
    {
        $app4 = CrmCustomerInformation::findOrFail($id);
        $app4->update([
            'approver4_id' => Auth::user()->id,
            'approver4_status' => $value4,
            'approver4_date' => date("Y-m-d"),
        ]);

        if ($app4->approver1_status == 1 && $app4->approver2_status == 1 && $app4->approver4_status == 1) {
            $app4->update([
                'final_status' => $value4,
                'approval_date' => date("Y-m-d"),
            ]);
        }

        if ($value4 == 1) {

            $this->emitTo('crm.contracts.accounts-application.index', 'close_modal', 'update_app4');
            $this->emitTo('crm.contracts.accounts-application.index', 'index');
            $this->sweetAlert('', 'Account Successfully Approved!');
            return;
        } else {
            $app4->update([
                'final_status' => $value4,
                'approval_date' => date("Y-m-d"),
            ]);
        }

        $this->emitTo('crm.contracts.accounts-application.index', 'close_modal', 'update_app4');
        $this->emitTo('crm.contracts.accounts-application.index', 'index');
        $this->sweetAlert('', 'Account Successfully Declined!');
    }

    public function search(AccountsApplicationInterface $accapp_interface)
    {
        $this->search_request = [
            'created_at' => $this->created_at,
            'account_no' => $this->account_no,
            'company_name' => $this->company_name,
        ];
    }

    public function render(AccountsApplicationInterface $accapp_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'stats' => $this->stats,
        ];

        $response = $accapp_interface->index($request, $this->search_request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'acc_s' => [],
                ];
        }

        return view('livewire.crm.contracts.accounts-application.index', [
            'acc_s' => $response['result']['acc_s']
        ]);
    }
}
