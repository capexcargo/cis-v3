<?php

namespace App\Http\Livewire\Crm\Contracts\AccountsApplication;

use App\Interfaces\Crm\Contracts\AccountsApplication\AccountsApplicationInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class View extends Component
{
    use PopUpMessagesTrait;

    protected $listeners = ['view' => 'mount'];

    public $acc_s;
    public $fullname;
    public $customer_number;

    public $emails = [];
    public $telephone_numbers = [];
    public $mobile_numbers = [];
    public $addresses = [];

    public $category;
    public $life_stage;
    public $account_type;
    public $contact_owner;
    public $onboarding_channel;
    public $marketing_channel;
    public $date_onboarded;
    public $note;

    public function mount(AccountsApplicationInterface $acc_app_interface, $id)
    {

        $response = $acc_app_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->acc_s = $response['result'];

        $this->fullname = $this->acc_s->fullname;
        $this->customer_number = $this->acc_s->account_no;
        
        $this->emails = $this->acc_s->emails;
        $this->telephone_numbers = $this->acc_s->telephoneNumbers;
        $this->mobile_numbers = $this->acc_s->mobileNumbers;
        $this->addresses = $this->acc_s->addresses;

        $this->category = $this->acc_s->statusRef->name;
        $this->life_stage = $this->acc_s->lifeStage->name ?? null;
        $this->account_type = $this->acc_s->accountType->name;
        // $this->contact_owner = $this->acc_s->accountType->name;
        // $this->onboarding_channel = $this->acc_s->accountType->name;
        $this->marketing_channel = $this->acc_s->marketingChannel->name ?? null;
        $this->date_onboarded = date('M. d, Y', strtotime($this->acc_s->created_at));
        $this->note = $this->acc_s->notes;
    }
    
    public function render()
    {
        return view('livewire.crm.contracts.accounts-application.view');
    }
}
