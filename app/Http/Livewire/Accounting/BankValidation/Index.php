<?php

namespace App\Http\Livewire\Accounting\BankValidation;

use App\Imports\BankValidationImport;
use App\Models\Accounting\BankValidations;
use App\Models\MonthReference;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Maatwebsite\Excel\Facades\Excel;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait, WithFileUploads;

    public $import_modal = false;
    public $import_file;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $transaction_date_from;
    public $transaction_date_to;
    public $month;
    public $year;
    public $account_no;

    public $month_references = [];

    public $header_cards = [];

    protected $listeners = ['index' => 'render', 'load_head_cards' => 'loadHeadCards'];

    public function load()
    {
        $this->loadHeadCards();
        $this->loadMonthReference();
    }

    public function loadHeadCards()
    {
        $total_debit_amount = BankValidations::sum('debit_amount');
        $total_credit_amount = BankValidations::sum('credit_amount');
        $this->header_cards = [
            [
                'title' => 'Total Debit Amount',
                'value' => number_format($total_debit_amount, 2),
                'action' => 'bank',
                'id' => null
            ],
            [
                'title' => 'Total Credit Amount',
                'value' => number_format($total_credit_amount, 2),
                'action' => 'bank',
                'id' => null
            ]
        ];
    }

    public function loadMonthReference()
    {
        $this->month_references = MonthReference::get();
    }

    public function import()
    {
        try {
            $validated = $this->validate([
                'import_file' => 'required'
            ]);

            Excel::import(new BankValidationImport, $this->import_file, null, \Maatwebsite\Excel\Excel::CSV);
            $this->reset('import_file');
            $this->loadHeadCards();
            $this->sweetAlert('success', 'Bank Validation Successfully Imported!');
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            foreach ($failures as $failure) {
                $this->sweetAlertError('error', 'Something Went Wrong', "Row: " . $failure->row() .
                    "\n Column: " . $failure->attribute() .
                    "\n Message: " . $failure->errors()[0] .
                    "\n Value: " . $failure->values()[$failure->attribute()]);
            }
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.bank-validation.index', [
            'bank_validations' => BankValidations::with('paymentType')
                ->when($this->transaction_date_from && !$this->month && !$this->year, function ($query) {
                    $query->whereDate('transaction_date', '>=', $this->transaction_date_from);
                })
                ->when($this->transaction_date_to && !$this->month && !$this->year, function ($query) {
                    $query->whereDate('transaction_date', '<=', $this->transaction_date_to);
                })
                ->when($this->month, function ($query) {
                    $query->whereMonth('transaction_date', $this->month);
                })
                ->when($this->year, function ($query) {
                    $query->whereYear('transaction_date', $this->year);
                })
                ->when($this->account_no, function ($query) {
                    $query->where('account_no', 'like', '%' . $this->account_no . '%');
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
