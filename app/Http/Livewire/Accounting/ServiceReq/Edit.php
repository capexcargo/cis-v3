<?php

namespace App\Http\Livewire\Accounting\ServiceReq;

use App\Interfaces\Fims\ServicePurchaseRequisition\ServiceReqInterface;
use App\Models\BranchReference;
use App\Models\FimsManPowerReference;
use App\Models\FimsPurchasing;
use App\Models\FimsPurpose;
use App\Models\FimsServiceCategory;
use App\Models\FimsServiceDescription;
use App\Traits\InitializeFirestoreTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{

    use WithPagination, PopUpMessagesTrait, InitializeFirestoreTrait;

    public $servid;
    public $confirmation_modal;
    public $create_modal;
    public $edit_modal;
    public $service_category_id;
    public $service_description_id;
    public $number_of_workers;
    public $estimated_rate;
    public $preferred_worker;
    public $purpose;
    public $location_id;
    public $comments;
    public $expected_start_date;
    public $expected_end_date;

    public $service_category_references = [];
    public $service_description_references = [];
    public $manpower_references = [];
    public $purpose_references = [];
    public $branch_references = [];


    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        $this->servid = $id;
        $servReq = FimsPurchasing::with('ServReqDetails2', 'attachments')->findOrFail($id);
        $this->service_category_id = $servReq->ServReqDetails2->service_category_id;
        $this->service_description_id = $servReq->ServReqDetails2->service_description_id;
        $this->number_of_workers = $servReq->ServReqDetails2->number_of_workers;
        $this->estimated_rate = $servReq->ServReqDetails2->estimated_rate;
        $this->preferred_worker = $servReq->ServReqDetails2->preferred_worker;
        $this->purpose = $servReq->ServReqDetails2->purpose;
        $this->location_id = $servReq->ServReqDetails2->location_id;
        $this->comments = $servReq->ServReqDetails2->comments;
        $this->expected_start_date = $servReq->ServReqDetails2->expected_start_date;
        $this->expected_end_date = $servReq->ServReqDetails2->expected_end_date;
        // dd($servReq->ServReqDetails2);


        $this->updatedServiceCategoryId();
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
    }

    public function confirmationSubmit()
    {

            $this->confirmation_modal = true;
      
    }

    public function getRequest()
    {
        return [
            'expected_start_date' => $this->expected_start_date,
            'expected_end_date' => $this->expected_end_date,
            'service_category_id' => $this->service_category_id,
            'service_description_id' => $this->service_description_id,
            'number_of_workers' => $this->number_of_workers,
            'estimated_rate' => $this->estimated_rate,
            'preferred_worker' => $this->preferred_worker,
            'purpose' => $this->purpose,
            'location_id' => $this->location_id,
            'comments' => $this->comments,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'expected_start_date',
            'expected_end_date',
            'service_category_id',
            'service_description_id',
            'number_of_workers',
            'estimated_rate',
            'purpose',
            'location_id',
            'comments',
        ]);

        $this->confirmation_modal = false;
    }

    public function submit(ServiceReqInterface $service_req_interface)
    {
        $response = $service_req_interface->update2($this->getRequest(), $this->servid);

        if ($response['code'] == 200) {
            $this->emitTo('accounting.service-req.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeeditmodal()
    {
        $this->edit_modal = false;
    }

    public function ServiceCategoryReference()
    {
        $this->service_category_references = FimsServiceCategory::get();
    }

    public function updatedServiceCategoryId()
    {
        $this->service_description_references = FimsServiceDescription::where('service_category_id', $this->service_category_id)->get();
        // dd($this->service_description_references);
    }


    public function ManpowerReference()
    {
        $this->manpower_references = FimsManPowerReference::get();
    }


    public function PurposeReference()
    {
        $this->purpose_references = FimsPurpose::get();
    }

    
    public function loadBranchReference()
    {
        $this->branch_references = BranchReference::get();
    }


    
    public function render()
    {
        return view('livewire.accounting.service-req.edit');
    }
}
