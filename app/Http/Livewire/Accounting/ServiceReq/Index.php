<?php

namespace App\Http\Livewire\Accounting\ServiceReq;

use App\Interfaces\Fims\ServicePurchaseRequisition\ServiceReqInterface;
use App\Traits\InitializeFirestoreTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use WithPagination, PopUpMessagesTrait, InitializeFirestoreTrait;

    public $paginate = 50;
    public $stats;
    public $reference_no;

    public $create_modal = false;
    public $edit_modal = false;
    public $action_type;
    public $reqid;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal = true;
        }elseif ($action_type == 'edit') {
            // $this->emitTo('oims.order-management.t-e-dropdown-mgmt.transport-mgmt.edit', 'edit', $data['id']);
            // dd("asdasd");
            $this->reqid = $data['id'];
            // dd($this->reqid );
            $this->edit_modal = true;
        }
    }

    public function load()
    {
        $this->stats = '1';
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
    }

    public function render(ServiceReqInterface $service_req_interface)
    {

        $request = [
            'paginate' => $this->paginate,
            'status' => $this->stats,
            'reference_no' => $this->reference_no,
        ];

        $response = $service_req_interface->index2($request);

        // dd($response['result']);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'service_req_lists' => [],
                ];
        }
        return view('livewire.accounting.service-req.index', [
            'service_req_lists' => $response['result']['list']
        ]);

    }
}
