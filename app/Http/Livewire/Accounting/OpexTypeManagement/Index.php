<?php

namespace App\Http\Livewire\Accounting\OpexTypeManagement;

use App\Models\Accounting\OpexType;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $create_modal = false;
    public $edit_modal = false;
    public $opex_type_id;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $name;
    public $description;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emit('opex_type_management_edit_mount', $data['id']);
            $this->edit_modal = true;
        }

        $this->opex_type_id = $data['id'];
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.opex-type-management.index', [
            'opex_types' => OpexType::when($this->name, function ($query) {
                $query->where('name', 'like', '%' . $this->name . '%');
            })
                ->when($this->description, function ($query) {
                    $query->where('description', 'like', '%' . $this->description . '%');
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)

        ]);
    }
}
