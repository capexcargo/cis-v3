<?php

namespace App\Http\Livewire\Accounting\OpexTypeManagement;

use App\Interfaces\Accounting\OpexTypeManagementInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use PopUpMessagesTrait;

    public $name;
    public $description;

    public function submit(OpexTypeManagementInterface $OpexTypeManagementInterface)
    {
        $validated = $this->validate([
            'name' => 'required|unique:accounting_opex_type,name',
            'description' => 'required',
        ]);

        $response = $OpexTypeManagementInterface->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.opex-type-management.index', 'close_modal', 'create');
            $this->emitTo('accounting.opex-type-management.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            "name"
        ]);
    }

    public function render()
    {
        return view('livewire.accounting.opex-type-management.create');
    }
}
