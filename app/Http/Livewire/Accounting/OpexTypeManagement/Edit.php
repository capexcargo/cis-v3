<?php

namespace App\Http\Livewire\Accounting\OpexTypeManagement;

use App\Interfaces\Accounting\OpexTypeManagementInterface;
use App\Models\Accounting\OpexType;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use PopUpMessagesTrait;

    public $name;
    public $description;

    public $opex_type;

    protected $listeners = ['opex_type_management_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->opex_type = OpexType::findOrFail($id);

        $this->name = $this->opex_type->name;
        $this->description = $this->opex_type->description;
    }

    public function submit(OpexTypeManagementInterface $OpexTypeManagementInterface)
    {
        $validated = $this->validate([
            'name' => 'required|unique:accounting_opex_type,name,' . $this->opex_type->id . ',id',
            'description' => 'required',
        ]);

        $response = $OpexTypeManagementInterface->update($this->opex_type, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.opex-type-management.index', 'close_modal', 'edit');
            $this->emitTo('accounting.opex-type-management.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            "name"
        ]);
    }

    public function render()
    {
        return view('livewire.accounting.opex-type-management.edit');
    }
}
