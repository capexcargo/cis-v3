<?php

namespace App\Http\Livewire\Accounting\CaReferenceManagement;

use App\Interfaces\Accounting\CaReferenceManagementInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use PopUpMessagesTrait;

    public $display;

    public function submit(CaReferenceManagementInterface $CaReferenceManagementInterface)
    {
        $validated = $this->validate([
            'display' => 'required|unique:accounting_ca_reference_types,display',
        ]);

        $response = $CaReferenceManagementInterface->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.ca-reference-management.index', 'close_modal', 'create');
            $this->emitTo('accounting.ca-reference-management.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            "display"
        ]);
    }


    public function render()
    {
        return view('livewire.accounting.ca-reference-management.create');
    }
}
