<?php

namespace App\Http\Livewire\Accounting\CaReferenceManagement;


use App\Models\Accounting\CAReferenceTypes;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    use WithPagination, PopUpMessagesTrait;

    public $create_modal = false;
    public $edit_modal = false;
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $ca_reference_id;

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emit('ca_reference_edit_mount', $data['id']);
            $this->edit_modal = true;
        }

        $this->ca_reference_id = $data['id'];
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.ca-reference-management.index', [
            'ca_references' => CAReferenceTypes::when($this->display, function ($query) {
                $query->where('display', 'like', '%' . $this->display . '%');
            })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
