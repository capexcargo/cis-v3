<?php

namespace App\Http\Livewire\Accounting\CaReferenceManagement;

use Livewire\Component;
use App\Interfaces\Accounting\CaReferenceManagementInterface;
use App\Models\Accounting\CAReferenceTypes;
use App\Traits\PopUpMessagesTrait;

class Edit extends Component
{

    use PopUpMessagesTrait;

    protected $listeners = ['ca_reference_edit_mount' => 'mount'];

    public $display;

    public function mount($id){

        $this->resetForm();
        $this->ca_reference = CAReferenceTypes::findOrFail($id);
        $this->display = $this->ca_reference->display;

    }

    public function submit(CaReferenceManagementInterface $CaReferenceManagementInterface)
    {

        $validated = $this->validate([
            'display' => 'required|unique:accounting_ca_reference_types,display,' . $this->ca_reference->id . ',id',
        ]);

        $response = $CaReferenceManagementInterface->update($this->ca_reference, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.ca-reference-management.index', 'close_modal', 'edit');
            $this->emitTo('accounting.ca-reference-management.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            "display"
        ]);
    }

    public function render()
    {
        return view('livewire.accounting.ca-reference-management.edit');
    }
}
