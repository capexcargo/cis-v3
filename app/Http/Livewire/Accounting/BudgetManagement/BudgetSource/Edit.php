<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetSource;

use App\Interfaces\Accounting\BudgetManagement\BudgetSourceInterface;
use App\Models\Accounting\BudgetSource;
use App\Traits\Accounting\BudgetManagement\BudgetSourceTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use BudgetSourceTrait, PopUpMessagesTrait;

    public $budget_source;

    protected $listeners = ['budget_source_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->budget_source = BudgetSource::findOrFail($id);

        $this->division = $this->budget_source->division_id;
        $this->name = $this->budget_source->name;
    }

    public function submit(BudgetSourceInterface $BudgetSourceInterface)
    {
        $validated = $this->validate([
            'division' => 'required',
            'name' => 'required'
        ]);

        $response = $BudgetSourceInterface->update($this->budget_source, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.budget-management.budget-source.index', 'close_modal', 'edit');
            $this->emitTo('accounting.budget-management.budget-source.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.budget-management.budget-source.edit');
    }
}
