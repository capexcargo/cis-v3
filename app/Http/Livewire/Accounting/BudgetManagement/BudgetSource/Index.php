<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetSource;

use App\Models\Accounting\BudgetSource;
use App\Models\Division;
use Livewire\Component;

class Index extends Component
{
    public $create_modal = false;
    public $edit_modal = false;
    public $budget_source_id;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $division = false;
    public $name;

    public $header_cards = [];
    public $division_references = [];
    public $budget_source_references = [];

    protected $listeners = ['index' => 'render', 'load_head_cards' => 'loadHeadCards', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadHeadCards();
        $this->loadDivisionReference();
    }

    public function loadHeadCards()
    {
        $divisions = Division::withCount('budgetSource')->get();

        $this->header_cards[] = [
            'title' => 'All',
            'value' => $divisions->sum('budget_source_count'),
            'action' => 'division',
            'id' => false
        ];

        foreach ($divisions as $division) {
            $this->header_cards[] = [
                'title' => $division->name,
                'value' => $division->budget_source_count,
                'action' => 'division',
                'id' => $division->id
            ];
        }
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emit('budget_source_edit_mount', $data['id']);
            $this->edit_modal = true;
        }

        $this->budget_source_id = $data['id'];
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.budget-management.budget-source.index', [
            'budget_sources' => BudgetSource::with('division')
                ->where('name', 'like', '%' . $this->name . '%')
                ->when($this->division, function ($query) {
                    $query->where('division_id', $this->division);
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
