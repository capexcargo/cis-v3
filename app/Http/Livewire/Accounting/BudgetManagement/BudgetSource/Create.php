<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetSource;

use App\Interfaces\Accounting\BudgetManagement\BudgetSourceInterface;
use App\Models\BudgetSource;
use App\Traits\Accounting\BudgetManagement\BudgetSourceTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use BudgetSourceTrait, PopUpMessagesTrait;

    public function submit(BudgetSourceInterface $BudgetSourceInterface)
    {
        $validated = $this->validate([
            'division' => 'required',
            'name' => 'required'
        ]);

        $response = $BudgetSourceInterface->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.budget-management.budget-source.index', 'close_modal', 'create');
            $this->emitTo('accounting.budget-management.budget-source.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.budget-management.budget-source.create');
    }
}
