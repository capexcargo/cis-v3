<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetPlanSummary;

use App\Models\Division;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Index extends Component
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $year;
    public $seleted = [];

    public $header_cards = [];

    protected $listeners = ['index' => 'render', 'load_head_cards' => 'loadHeadCards'];

    public function load()
    {
        $this->loadHeadCards();
    }

    public function loadHeadCards()
    {
        $divisions = Division::withSum(['budgetPlan' => function ($query) {
            $query->where('year', $this->year);
        }], 'total_amount')
            ->withSum(['transferBudgetFrom' => function ($query) {
                $query->where('year', $this->year);
            }], 'amount')
            ->withSum(['transferBudgetTo' => function ($query) {
                $query->where('year', $this->year);
            }], 'amount')
            ->withSum(['availment' => function ($query) {
                $query->where('year', $this->year);
            }], 'amount')
            ->when(Auth::user()->level_id <= 4, function ($query) {
                $query->where('id', Auth::user()->division_id);
            })
            ->get();

        $this->header_cards = [];

        foreach ($divisions as $division) {
            $total_add_balance = ($division->budget_plan_sum_total_amount + $division->transfer_budget_to_sum_amount);
            $total_minus_balance = ($division->transfer_budget_from_sum_amount + $division->availment_sum_amount);
            $total_amount = ($total_add_balance - $total_minus_balance);
            $this->header_cards[] = [
                'title' => $division->name,
                'current_value' => number_format($total_amount, 2),
                'availed_value' => number_format($total_minus_balance, 2),
                'approved_value' => number_format($total_add_balance, 2),
                'color' => 'text-blue',
                'action' => 'division',
                'id' => $division->id
            ];
        }
    }

    public function checkbox($division_id)
    {
        if (($key = array_search($division_id, $this->seleted)) !== false) {
            unset($this->seleted[$key]);
        } else {
            $this->seleted[] = $division_id;
        }
    }

    public function updatedYear()
    {
        $this->loadHeadCards();
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.budget-management.budget-plan-summary.index', [
            'divisions' => Division::withSum(['budgetPlan' => function ($query) {
                $query->where('year', $this->year);
            }], 'total_amount')
                ->withSum(['transferBudgetFrom' => function ($query) {
                    $query->where('year', $this->year);
                }], 'amount')
                ->withSum(['transferBudgetTo' => function ($query) {
                    $query->where('year', $this->year);
                }], 'amount')
                ->withSum(['availment' => function ($query) {
                    $query->where('year', $this->year);
                }], 'amount')
                ->when(Auth::user()->level_id <= 4, function ($query) {
                    $query->where('id', Auth::user()->division_id);
                })
                ->when(Auth::user()->level_id >= 5, function ($query) {
                    $query->whereIn('id', $this->seleted);
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
