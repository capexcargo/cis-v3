<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetPlan;

use App\Interfaces\Accounting\BudgetManagement\BudgetPlanInterface;
use App\Models\Accounting\BudgetPlan;
use App\Traits\Accounting\BudgetManagement\BudgetPlanTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use BudgetPlanTrait, PopUpMessagesTrait;

    public $budget_plan;

    protected $listeners = ['budget_plan_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->budget_plan = BudgetPlan::with('source', 'chart', 'division', 'opexType', 'transferBudgetFrom', 'transferBudgetTo')->findOrFail($id);
        $this->source = $this->budget_plan->budget_source_id;
        $this->chart = $this->budget_plan->budget_chart_id;
        $this->item = $this->budget_plan->item;
        $this->division = $this->budget_plan->division_id;
        $this->opex_type = $this->budget_plan->opex_type_id;
        $this->year = $this->budget_plan->year;
        $this->request_type = $this->budget_plan->rfp_type_id;

        $this->january = ($this->budget_plan->january + $this->getTranferedBudgetTotal('january')) - $this->getTranferedBudgetTotalFrom('january');
        $this->february = ($this->budget_plan->february + $this->getTranferedBudgetTotal('february')) - $this->getTranferedBudgetTotalFrom('february');
        $this->march = ($this->budget_plan->march + $this->getTranferedBudgetTotal('march')) - $this->getTranferedBudgetTotalFrom('march');
        $this->april = ($this->budget_plan->april + $this->getTranferedBudgetTotal('april')) - $this->getTranferedBudgetTotalFrom('april');
        $this->may = ($this->budget_plan->may + $this->getTranferedBudgetTotal('may')) - $this->getTranferedBudgetTotalFrom('may');
        $this->june = ($this->budget_plan->june + $this->getTranferedBudgetTotal('june')) - $this->getTranferedBudgetTotalFrom('june');
        $this->july = ($this->budget_plan->july + $this->getTranferedBudgetTotal('july')) - $this->getTranferedBudgetTotalFrom('july');
        $this->august = ($this->budget_plan->august + $this->getTranferedBudgetTotal('august')) - $this->getTranferedBudgetTotalFrom('august');
        $this->september = ($this->budget_plan->september + $this->getTranferedBudgetTotal('september')) - $this->getTranferedBudgetTotalFrom('september');
        $this->october = ($this->budget_plan->october + $this->getTranferedBudgetTotal('october')) - $this->getTranferedBudgetTotalFrom('october');
        $this->november = ($this->budget_plan->november + $this->getTranferedBudgetTotal('november')) - $this->getTranferedBudgetTotalFrom('november');
        $this->december = ($this->budget_plan->december + $this->getTranferedBudgetTotal('december')) - $this->getTranferedBudgetTotalFrom('december');

        $this->loadSourceReference();
        $this->loadChartReference();
    }

    public function getTranferedBudgetTotal($month)
    {
        $total_amount = 0;
        if ($this->budget_plan->transferBudgetTo) {
            $total_amount = $this->budget_plan->transferBudgetTo()->where('month_to', $month)->sum('amount');
        }

        return $total_amount;
    }

    public function getTranferedBudgetTotalFrom($month)
    {
        $total_amount = 0;
        if ($this->budget_plan->transferBudgetFrom) {
            $total_amount = $this->budget_plan->transferBudgetFrom()->where('month_from', $month)->sum('amount');
        }

        return $total_amount;
    }

    public function submit(BudgetPlanInterface $BudgetPlanInterface)
    {
        $validated = $this->validate([
            'division' => 'required',
            'source' => 'required',
            'chart' => 'required',
            'item' => 'required',
            'opex_type' => 'required',
            'year' => 'required',
            'request_type' => 'required',

            'january' => 'required',
            'february' => 'required',
            'march' => 'required',
            'april' => 'required',
            'may' => 'required',
            'june' => 'required',
            'july' => 'required',
            'august' => 'required',
            'september' => 'required',
            'october' => 'required',
            'november' => 'required',
            'december' => 'required',
        ]);

        $response = $BudgetPlanInterface->update($this->budget_plan, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.budget-management.budget-plan.index', 'close_modal', 'edit');
            $this->emitTo('accounting.budget-management.budget-plan.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.budget-management.budget-plan.edit');
    }
}
