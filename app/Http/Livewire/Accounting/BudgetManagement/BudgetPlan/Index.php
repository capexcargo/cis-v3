<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetPlan;

use App\Exports\Accounting\BudgetPlan\BudgetPlanExport as BudgetPlanExports;
use App\Imports\BudgetPlanImport;
use App\Models\Accounting\BudgetChart;
use App\Models\Accounting\BudgetPlan;
use App\Models\Accounting\BudgetSource;
use App\Models\Division;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Maatwebsite\Excel\Facades\Excel;

class Index extends Component
{
    use WithPagination;
    use WithFileUploads;
    use PopUpMessagesTrait;

    public $create_modal = false;
    public $edit_modal = false;
    public $details_modal = false;
    public $import_modal = false;
    public $transfer_budget = false;
    public $budget_plan_id;
    public $month;

    public $division;
    public $budget_source;
    public $budget_chart;
    public $budget_plan;
    public $year;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $division_references = [];
    public $budget_source_references = [];
    public $budget_chart_references = [];
    public $budget_plan_references = [];

    public $import;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function mount()
    {
        $this->division = Auth::user()->division_id;
        $this->year = date('Y');

        $this->updatedDivision();
    }

    public function load()
    {
        $this->loadDivisionReference();
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function loadBudgetSourceReference()
    {
        $this->budget_source_references = BudgetSource::where('division_id', $this->division)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function loadBudgetChartReference()
    {
        $this->budget_chart_references = BudgetChart::where([
            ['division_id', $this->division],
            ['budget_source_id', $this->budget_source],
        ])
            ->orderBy('name', 'asc')
            ->get();
    }

    public function loadBudgetPlanReference()
    {
        $this->budget_plan_references = BudgetPlan::where([
            ['division_id', $this->division],
            ['budget_source_id', $this->budget_source],
            ['budget_chart_id', $this->budget_chart],
        ])
            ->orderBy('item', 'asc')
            ->get();
    }

    protected function updatedDivision()
    {
        $this->loadBudgetSourceReference();

        $this->reset([
            'budget_source',
            'budget_chart',
            'budget_plan',
        ]);

        $this->budget_chart_references = [];
        $this->budget_plan_references = [];
    }

    protected function updatedBudgetSource()
    {
        $this->loadBudgetChartReference();

        $this->reset([
            'budget_chart',
            'budget_plan',
        ]);

        $this->budget_plan_references = [];
    }

    protected function updatedBudgetChart()
    {
        $this->loadBudgetPlanReference();

        $this->reset([
            'budget_plan',
        ]);
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emit('budget_plan_edit_mount', $data['id']);
            $this->edit_modal = true;
        } elseif ($action_type == 'details') {
            $this->emit('budget_plan_details_mount', $data['id'], $data['month']);
            $this->details_modal = true;
        } elseif ($action_type == 'transfer_budget') {
            $this->emit('budget_plan_transfer_budget_mount', $data['id']);
            $this->transfer_budget = true;
        }

        $this->budget_plan_id = $data['id'] ?? null;
        $this->month = $data['month'] ?? null;
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'details') {
            $this->details_modal = false;
        } elseif ($action_type == 'transfer_budget') {
            $this->transfer_budget = false;
        }
    }

    public function import()
    {
        try {
            $validated = $this->validate([
                'import' => 'mimes:xlsx,csv',
            ]);

            Excel::import(new BudgetPlanImport(), $this->import);
            $this->reset('import');
            $this->sweetAlert('success', 'Budget Plan Successfully Imported!');
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            foreach ($failures as $failure) {
                $this->sweetAlertError('error', 'Something Went Wrong', 'Row: ' . $failure->row() .
                    "\n Column: " . $failure->attribute() .
                    "\n Message: " . $failure->errors()[0] .
                    "\n Value: " . $failure->values()[$failure->attribute()]);
            }
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function export()
    {
        $export = [
            'divisions' => Division::withSum('budgetPlan', 'january')
                ->withSum('budgetPlan', 'february')
                ->withSum('budgetPlan', 'march')
                ->withSum('budgetPlan', 'april')
                ->withSum('budgetPlan', 'may')
                ->withSum('budgetPlan', 'june')
                ->withSum('budgetPlan', 'july')
                ->withSum('budgetPlan', 'august')
                ->withSum('budgetPlan', 'september')
                ->withSum('budgetPlan', 'october')
                ->withSum('budgetPlan', 'november')
                ->withSum('budgetPlan', 'december')
                ->withSum('budgetPlan', 'total_amount')
                ->withSum(['transferBudgetFrom' => function ($query) {
                    $query->where('year', $this->year);
                }], 'amount')
                ->withSum(['transferBudgetTo' => function ($query) {
                    $query->where('year', $this->year);
                }], 'amount')
                ->withSum(['availment' => function ($query) {
                    $query->where('year', $this->year);
                }], 'amount')
                ->when($this->year, function ($query) {
                    $query->whereHas('budgetPlan', function ($query) {
                        $query->where('year', $this->year);
                    });
                })
                ->with(['transferBudgetFrom' => function ($query) {
                    $query->where('year', $this->year);
                }, 'transferBudgetTo' => function ($query) {
                    $query->where('year', $this->year);
                }, 'budgetSource' => function ($query) {
                    $query->withSum('budgetPlan', 'january')
                        ->withSum('budgetPlan', 'february')
                        ->withSum('budgetPlan', 'march')
                        ->withSum('budgetPlan', 'april')
                        ->withSum('budgetPlan', 'may')
                        ->withSum('budgetPlan', 'june')
                        ->withSum('budgetPlan', 'july')
                        ->withSum('budgetPlan', 'august')
                        ->withSum('budgetPlan', 'september')
                        ->withSum('budgetPlan', 'october')
                        ->withSum('budgetPlan', 'november')
                        ->withSum('budgetPlan', 'december')
                        ->withSum('budgetPlan', 'total_amount')
                        ->withSum(['transferBudgetFrom' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'amount')
                        ->withSum(['transferBudgetTo' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'amount')
                        ->withSum(['availment' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'amount')
                        ->when($this->year, function ($query) {
                            $query->whereHas('budgetPlan', function ($query) {
                                $query->where('year', $this->year);
                            });
                        })
                        ->with([
                            'transferBudgetFrom' => function ($query) {
                                $query->where('year', $this->year);
                            }, 'transferBudgetTo' => function ($query) {
                                $query->where('year', $this->year);
                            },
                            'budgetChart' => function ($query) {
                                $query->withSum('budgetPlan', 'january')
                                    ->withSum('budgetPlan', 'february')
                                    ->withSum('budgetPlan', 'march')
                                    ->withSum('budgetPlan', 'april')
                                    ->withSum('budgetPlan', 'may')
                                    ->withSum('budgetPlan', 'june')
                                    ->withSum('budgetPlan', 'july')
                                    ->withSum('budgetPlan', 'august')
                                    ->withSum('budgetPlan', 'september')
                                    ->withSum('budgetPlan', 'october')
                                    ->withSum('budgetPlan', 'november')
                                    ->withSum('budgetPlan', 'december')
                                    ->withSum('budgetPlan', 'total_amount')
                                    ->withSum(['transferBudgetFrom' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'amount')
                                    ->withSum(['transferBudgetTo' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'amount')
                                    ->withSum(['availment' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'amount')
                                    ->when($this->year, function ($query) {
                                        $query->whereHas('budgetPlan', function ($query) {
                                            $query->where('year', $this->year);
                                        });
                                    })
                                    ->with(['transferBudgetFrom' => function ($query) {
                                        $query->where('year', $this->year);
                                    }, 'transferBudgetTo' => function ($query) {
                                        $query->where('year', $this->year);
                                    }, 'availment' => function ($query) {
                                        $query->where('year', $this->year);
                                    }, 'budgetPlan' => function ($query) {
                                        $query->withSum(['transferBudgetFrom' => function ($query) {
                                            $query->where('year', $this->year);
                                        }], 'amount')
                                            ->withSum(['transferBudgetTo' => function ($query) {
                                                $query->where('year', $this->year);
                                            }], 'amount')
                                            ->withSum(['availment' => function ($query) {
                                                $query->where('year', $this->year);
                                            }], 'amount')
                                            ->with(['source', 'chart', 'division', 'opexType', 'transferBudgetFrom' => function ($query) {
                                                $query->where('year', $this->year);
                                            }, 'transferBudgetTo' => function ($query) {
                                                $query->where('year', $this->year);
                                            }])
                                            ->when($this->budget_plan, function ($query) {
                                                $query->where('id', $this->budget_plan);
                                            });
                                    }])
                                    ->when($this->budget_chart, function ($query) {
                                        $query->where('id', $this->budget_chart);
                                    });
                            },
                        ])
                        ->when($this->budget_source, function ($query) {
                            $query->where('id', $this->budget_source);
                        });
                }])
                ->where('id', $this->division)
                ->get(),
        ];

        return Excel::download(new BudgetPlanExports($export), 'budgetplanexport.xlsx');
    }

    public function render()
    {

        $asdasd = Division::withSum(['budgetPlan' => function ($query) {
            $query->where('year', $this->year);
        }], 'january')
            ->withSum(['budgetPlan' => function ($query) {
                $query->where('year', $this->year);
            }], 'february')
            ->withSum(['budgetPlan' => function ($query) {
                $query->where('year', $this->year);
            }], 'march')
            ->withSum(['budgetPlan' => function ($query) {
                $query->where('year', $this->year);
            }], 'april')
            ->withSum(['budgetPlan' => function ($query) {
                $query->where('year', $this->year);
            }], 'may')
            ->withSum(['budgetPlan' => function ($query) {
                $query->where('year', $this->year);
            }], 'june')
            ->withSum(['budgetPlan' => function ($query) {
                $query->where('year', $this->year);
            }], 'july')
            ->withSum(['budgetPlan' => function ($query) {
                $query->where('year', $this->year);
            }], 'august')
            ->withSum(['budgetPlan' => function ($query) {
                $query->where('year', $this->year);
            }], 'september')
            ->withSum(['budgetPlan' => function ($query) {
                $query->where('year', $this->year);
            }], 'october')
            ->withSum(['budgetPlan' => function ($query) {
                $query->where('year', $this->year);
            }], 'november')
            ->withSum(['budgetPlan' => function ($query) {
                $query->where('year', $this->year);
            }], 'december')
            ->withSum(['budgetPlan' => function ($query) {
                $query->where('year', $this->year);
            }], 'total_amount')

            ->withSum(['transferBudgetFrom' => function ($query) {
                $query->where('year', $this->year);
            }], 'amount')
            ->withSum(['transferBudgetTo' => function ($query) {
                $query->where('year', $this->year);
            }], 'amount')
            ->withSum(['availment' => function ($query) {
                $query->where('year', $this->year);
            }], 'amount')
            ->when($this->year, function ($query) {
                $query->whereHas('budgetPlan', function ($query) {
                    $query->where('year', $this->year);
                });
            })
            ->with(['transferBudgetFrom' => function ($query) {
                $query->where('year', $this->year);
            }, 'transferBudgetTo' => function ($query) {
                $query->where('year', $this->year);
            }, 'budgetSource' => function ($query) {
                $query->withSum(['budgetPlan' => function ($query) {
                    $query->where('year', $this->year);
                }], 'january')
                    ->withSum(['budgetPlan' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'february')
                    ->withSum(['budgetPlan' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'march')
                    ->withSum(['budgetPlan' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'april')
                    ->withSum(['budgetPlan' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'may')
                    ->withSum(['budgetPlan' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'june')
                    ->withSum(['budgetPlan' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'july')
                    ->withSum(['budgetPlan' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'august')
                    ->withSum(['budgetPlan' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'september')
                    ->withSum(['budgetPlan' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'october')
                    ->withSum(['budgetPlan' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'november')
                    ->withSum(['budgetPlan' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'december')
                    ->withSum(['budgetPlan' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'total_amount')
                    ->withSum(['transferBudgetFrom' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'amount')
                    ->withSum(['transferBudgetTo' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'amount')
                    ->withSum(['availment' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'amount')
                    ->when($this->year, function ($query) {
                        $query->whereHas('budgetPlan', function ($query) {
                            $query->where('year', $this->year);
                        });
                    })
                    ->with([
                        'transferBudgetFrom' => function ($query) {
                            $query->where('year', $this->year);
                        }, 'transferBudgetTo' => function ($query) {
                            $query->where('year', $this->year);
                        },
                        'budgetChart' => function ($query) {
                            $query->withSum(['budgetPlan' => function ($query) {
                                $query->where('year', $this->year);
                            }], 'january')
                                ->withSum(['budgetPlan' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'february')
                                ->withSum(['budgetPlan' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'march')
                                ->withSum(['budgetPlan' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'april')
                                ->withSum(['budgetPlan' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'may')
                                ->withSum(['budgetPlan' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'june')
                                ->withSum(['budgetPlan' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'july')
                                ->withSum(['budgetPlan' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'august')
                                ->withSum(['budgetPlan' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'september')
                                ->withSum(['budgetPlan' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'october')
                                ->withSum(['budgetPlan' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'november')
                                ->withSum(['budgetPlan' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'december')
                                ->withSum(['budgetPlan' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'total_amount')
                                ->withSum(['transferBudgetFrom' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'amount')
                                ->withSum(['transferBudgetTo' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'amount')
                                ->withSum(['availment' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'amount')
                                ->when($this->year, function ($query) {
                                    $query->whereHas('budgetPlan', function ($query) {
                                        $query->where('year', $this->year);
                                    });
                                })
                                ->with(['transferBudgetFrom' => function ($query) {
                                    $query->where('year', $this->year);
                                }, 'transferBudgetTo' => function ($query) {
                                    $query->where('year', $this->year);
                                }, 'availment' => function ($query) {
                                    $query->where('year', $this->year);
                                }, 'budgetPlan' => function ($query) {
                                    $query->withSum(['transferBudgetFrom' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'amount')
                                        ->withSum(['transferBudgetTo' => function ($query) {
                                            $query->where('year', $this->year);
                                        }], 'amount')
                                        ->withSum(['availment' => function ($query) {
                                            $query->where('year', $this->year);
                                        }], 'amount')
                                        ->with(['source', 'chart', 'division', 'opexType', 'transferBudgetFrom' => function ($query) {
                                            $query->where('year', $this->year);
                                        }, 'transferBudgetTo' => function ($query) {
                                            $query->where('year', $this->year);
                                        }])
                                        ->when($this->budget_plan, function ($query) {
                                            $query->where('id', $this->budget_plan);
                                        });
                                }])
                                ->when($this->budget_chart, function ($query) {
                                    $query->where('id', $this->budget_chart);
                                });
                        },
                    ])
                    ->when($this->budget_source, function ($query) {
                        $query->where('id', $this->budget_source);
                    });
            }])
            ->where('id', $this->division)
            ->get();

            // dd($asdasd);
        return view('livewire.accounting.budget-management.budget-plan.index', [
            'divisions' => Division::withSum(['budgetPlan' => function ($query) {
                $query->where('year', $this->year);
            }], 'january')
                ->withSum(['budgetPlan' => function ($query) {
                    $query->where('year', $this->year);
                }], 'february')
                ->withSum(['budgetPlan' => function ($query) {
                    $query->where('year', $this->year);
                }], 'march')
                ->withSum(['budgetPlan' => function ($query) {
                    $query->where('year', $this->year);
                }], 'april')
                ->withSum(['budgetPlan' => function ($query) {
                    $query->where('year', $this->year);
                }], 'may')
                ->withSum(['budgetPlan' => function ($query) {
                    $query->where('year', $this->year);
                }], 'june')
                ->withSum(['budgetPlan' => function ($query) {
                    $query->where('year', $this->year);
                }], 'july')
                ->withSum(['budgetPlan' => function ($query) {
                    $query->where('year', $this->year);
                }], 'august')
                ->withSum(['budgetPlan' => function ($query) {
                    $query->where('year', $this->year);
                }], 'september')
                ->withSum(['budgetPlan' => function ($query) {
                    $query->where('year', $this->year);
                }], 'october')
                ->withSum(['budgetPlan' => function ($query) {
                    $query->where('year', $this->year);
                }], 'november')
                ->withSum(['budgetPlan' => function ($query) {
                    $query->where('year', $this->year);
                }], 'december')
                ->withSum(['budgetPlan' => function ($query) {
                    $query->where('year', $this->year);
                }], 'total_amount')
    
                ->withSum(['transferBudgetFrom' => function ($query) {
                    $query->where('year', $this->year);
                }], 'amount')
                ->withSum(['transferBudgetTo' => function ($query) {
                    $query->where('year', $this->year);
                }], 'amount')
                ->withSum(['availment' => function ($query) {
                    $query->where('year', $this->year);
                }], 'amount')
                ->when($this->year, function ($query) {
                    $query->whereHas('budgetPlan', function ($query) {
                        $query->where('year', $this->year);
                    });
                })
                ->with(['transferBudgetFrom' => function ($query) {
                    $query->where('year', $this->year);
                }, 'transferBudgetTo' => function ($query) {
                    $query->where('year', $this->year);
                }, 'budgetSource' => function ($query) {
                    $query->withSum(['budgetPlan' => function ($query) {
                        $query->where('year', $this->year);
                    }], 'january')
                        ->withSum(['budgetPlan' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'february')
                        ->withSum(['budgetPlan' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'march')
                        ->withSum(['budgetPlan' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'april')
                        ->withSum(['budgetPlan' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'may')
                        ->withSum(['budgetPlan' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'june')
                        ->withSum(['budgetPlan' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'july')
                        ->withSum(['budgetPlan' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'august')
                        ->withSum(['budgetPlan' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'september')
                        ->withSum(['budgetPlan' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'october')
                        ->withSum(['budgetPlan' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'november')
                        ->withSum(['budgetPlan' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'december')
                        ->withSum(['budgetPlan' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'total_amount')
                        ->withSum(['transferBudgetFrom' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'amount')
                        ->withSum(['transferBudgetTo' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'amount')
                        ->withSum(['availment' => function ($query) {
                            $query->where('year', $this->year);
                        }], 'amount')
                        ->when($this->year, function ($query) {
                            $query->whereHas('budgetPlan', function ($query) {
                                $query->where('year', $this->year);
                            });
                        })
                        ->with([
                            'transferBudgetFrom' => function ($query) {
                                $query->where('year', $this->year);
                            }, 'transferBudgetTo' => function ($query) {
                                $query->where('year', $this->year);
                            },
                            'budgetChart' => function ($query) {
                                $query->withSum(['budgetPlan' => function ($query) {
                                    $query->where('year', $this->year);
                                }], 'january')
                                    ->withSum(['budgetPlan' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'february')
                                    ->withSum(['budgetPlan' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'march')
                                    ->withSum(['budgetPlan' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'april')
                                    ->withSum(['budgetPlan' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'may')
                                    ->withSum(['budgetPlan' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'june')
                                    ->withSum(['budgetPlan' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'july')
                                    ->withSum(['budgetPlan' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'august')
                                    ->withSum(['budgetPlan' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'september')
                                    ->withSum(['budgetPlan' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'october')
                                    ->withSum(['budgetPlan' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'november')
                                    ->withSum(['budgetPlan' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'december')
                                    ->withSum(['budgetPlan' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'total_amount')
                                    ->withSum(['transferBudgetFrom' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'amount')
                                    ->withSum(['transferBudgetTo' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'amount')
                                    ->withSum(['availment' => function ($query) {
                                        $query->where('year', $this->year);
                                    }], 'amount')
                                    ->when($this->year, function ($query) {
                                        $query->whereHas('budgetPlan', function ($query) {
                                            $query->where('year', $this->year);
                                        });
                                    })
                                    ->with(['transferBudgetFrom' => function ($query) {
                                        $query->where('year', $this->year);
                                    }, 'transferBudgetTo' => function ($query) {
                                        $query->where('year', $this->year);
                                    }, 'availment' => function ($query) {
                                        $query->where('year', $this->year);
                                    }, 'budgetPlan' => function ($query) {
                                        $query->withSum(['transferBudgetFrom' => function ($query) {
                                            $query->where('year', $this->year);
                                        }], 'amount')
                                            ->withSum(['transferBudgetTo' => function ($query) {
                                                $query->where('year', $this->year);
                                            }], 'amount')
                                            ->withSum(['availment' => function ($query) {
                                                $query->where('year', $this->year);
                                            }], 'amount')
                                            ->with(['source', 'chart', 'division', 'opexType', 'transferBudgetFrom' => function ($query) {
                                                $query->where('year', $this->year);
                                            }, 'transferBudgetTo' => function ($query) {
                                                $query->where('year', $this->year);
                                            }])
                                            ->when($this->budget_plan, function ($query) {
                                                $query->where('id', $this->budget_plan);
                                            });
                                    }])
                                    ->when($this->budget_chart, function ($query) {
                                        $query->where('id', $this->budget_chart);
                                    });
                            },
                        ])
                        ->when($this->budget_source, function ($query) {
                            $query->where('id', $this->budget_source);
                        });
                }])
                ->where('id', $this->division)
                ->get(),
        ]);
    }
}
