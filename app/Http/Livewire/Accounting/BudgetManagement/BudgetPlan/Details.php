<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetPlan;

use App\Models\Accounting\BudgetAvailment;
use Livewire\Component;
use Livewire\WithPagination;

class Details extends Component
{
    use WithPagination;

    public $view_modal = false;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $budget_plan_id;
    public $month;
    public $parent_reference_no;

    protected $listeners = ['budget_plan_details_mount' => 'mount', 'close_modal' => 'closeModal'];

    public function mount($id, $month)
    {
        $this->budget_plan_id = $id;
        $this->month = $month;
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'view') {
            $this->emit('request_management_view_mount', $data['parent_reference_no']);
            $this->view_modal = true;
        }

        $this->parent_reference_no = $data['parent_reference_no'] ?? null;
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'view') {
            $this->view_modal = false;
        }
    }

    public function render()
    {
        return view('livewire.accounting.budget-management.budget-plan.details', [
            'budget_availments' => BudgetAvailment::with('requestForPayment', 'division', 'source', 'chart')->where([
                ['budget_plan_id', $this->budget_plan_id],
                ['month', $this->month]
            ])
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
