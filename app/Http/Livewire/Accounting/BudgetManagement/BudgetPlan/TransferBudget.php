<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetPlan;

use App\Interfaces\Accounting\BudgetManagement\BudgetPlanInterface;
use App\Models\Accounting\BudgetChart;
use App\Models\Accounting\BudgetPlan;
use App\Models\Accounting\BudgetSource;
use App\Models\Accounting\TransferTypeReference;
use App\Models\Division;
use App\Models\MonthReference;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class TransferBudget extends Component
{
    use PopUpMessagesTrait;

    public $transfer_type;

    public $year;

    public $division_from;
    public $source_from;
    public $chart_from;
    public $budget_plan_from;
    public $month_from;
    public $available_amount;

    public $division_to;
    public $source_to;
    public $chart_to;
    public $budget_plan_to;
    public $month_to;
    public $amount;

    public $transfer_type_references = [];
    public $division_references = [];
    public $budget_source_references = [];
    public $budget_chart_references = [];
    public $budget_plan_references = [];
    public $month_references = [];

    public $budget_plan_1;
    public $budget_plan_2;

    public $approvedbudgetplan;
    public $availedbudgetplan;
    public $balancebudgetplan;

    protected $listeners = ['budget_plan_transfer_budget_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetFrom();
        $this->budget_plan_1 = BudgetPlan::with('source', 'chart', 'division')->findOrFail($id);

        $this->division_to = $this->budget_plan_1->division ? $this->budget_plan_1->division->name : '';
        $this->source_to = $this->budget_plan_1->source ? $this->budget_plan_1->source->name : '';
        $this->chart_to = $this->budget_plan_1->chart ? $this->budget_plan_1->chart->name : '';
        $this->budget_plan_to = $this->budget_plan_1->item;
        $this->year = $this->budget_plan_1->year;
    }

    public function load()
    {
        $this->loadMonthReference();
    }

    public function loadTransferTypeReference()
    {
        $this->transfer_type_references = TransferTypeReference::get();
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function loadSourceReference()
    {
        $this->budget_source_references = BudgetSource::where('division_id', $this->division_from)->get();
    }

    public function loadChartReference()
    {
        $this->budget_chart_references = BudgetChart::where([
            ['division_id', $this->division_from],
            ['budget_source_id', $this->source_from]
        ])->get();
    }

    public function loadBudgetPlan()
    {
        $this->budget_plan_references = BudgetPlan::where([
            ['division_id', $this->division_from],
            ['budget_source_id', $this->source_from],
            ['budget_chart_id', $this->chart_from],
            ['year', $this->year]
        ])->get();
    }

    public function loadMonthReference()
    {
        $this->month_references = MonthReference::get();
    }

    public function loadBudgetPlan2()
    {
        $monfrom = $this->month_from;

        $this->budget_plan_2 = BudgetPlan::with('transferBudgetFrom', 'transferBudgetTo', 'availment')->find($this->budget_plan_from);

        if ($this->month_from) {

            $budgetbalance = BudgetPlan::with(['chart' => function ($query) use ($monfrom) {
                $query->withSum(['transferBudgetFrom' => function ($query) use ($monfrom) {
                    $query->where('month_from', $monfrom)->where('year', date('Y'))
                        ->where('year', date('Y', strtotime($monfrom)));
                }], 'amount')
                    ->withSum(['transferBudgetTo' => function ($query) use ($monfrom) {
                        $query->where('month_to', $monfrom)->where('year', date('Y'))
                            ->where('year', date('Y', strtotime($monfrom)));
                    }], 'amount')
                    ->withSum(['availment' => function ($query) use ($monfrom) {
                        $query->where('month', $monfrom)->where('year', date('Y'));
                    }], 'amount')
                    ->withSum(['budgetPlan' => function ($query) use ($monfrom) {
                        $query->where('year', date('Y'));
                    }], $monfrom);
                // ->withSum('budgetPlan', strtolower(date('F', strtotime($transaction_date))));
            }, 'budgetLoa' => function ($query) {
                $query->with('firstApprover', 'secondApprover', 'thirdApprover');
            }, 'division'])
                ->withSum(['transferBudgetFrom' => function ($query) use ($monfrom) {
                    $query->where('month_from', $monfrom)->where('year', date('Y'));
                }], 'amount')
                ->withSum(['transferBudgetTo' => function ($query) use ($monfrom) {
                    $query->where('month_to', $monfrom)->where('year', date('Y'));
                }], 'amount')
                ->withSum(['availment' => function ($query) use ($monfrom) {
                    $query->where('month', $monfrom)->where('year', date('Y'));
                }], 'amount')
                ->findOrFail($this->budget_plan_from);

            $balancebudplan = ($budgetbalance->chart['budget_plan_sum_' . $monfrom] - $budgetbalance->chart->transfer_budget_from_sum_amount) + $budgetbalance->chart->transfer_budget_to_sum_amount;
            $balancebudplanpctamt = $balancebudplan * 0.083;
            $balancebudplanavailment = $budgetbalance->chart->availment_sum_amount;
            $balancebudplanf = ($balancebudplan - $balancebudplanpctamt) - $balancebudplanavailment;

            $this->approvedbudgetplan = $balancebudplan;
            $this->availedbudgetplan = $balancebudplanavailment;
            $this->balancebudgetplan = $balancebudplanf;

            // dd($budgetbalance->chart, 
            // $budgetbalance->chart['budget_plan_sum_' . $monfrom], 
            // $balancebudplan,
            // $balancebudplanpctamt, 
            // $balancebudplanavailment, 
            // $balancebudplanf);
        }


        $add_amount = 0;
        $minus_amount = 0;
        $availed = 0;

        if ($this->budget_plan_2 && $this->month_from) {
            if ($this->budget_plan_2->transferBudgetTo) {
                $add_amount = $this->budget_plan_2->transferBudgetTo()->where('month_to', $this->month_from)->sum('amount');
            }

            if ($this->budget_plan_2->transferBudgetFrom) {
                $minus_amount = $this->budget_plan_2->transferBudgetFrom()->where('month_from', $this->month_from)->sum('amount');
            }

            if ($this->budget_plan_2->availment) {
                $availed = $this->budget_plan_2->availment()->where('month', $this->month_from)->sum('amount');
            }

            $current_amount = $this->budget_plan_2[$this->month_from];
            if ($this->month_from) {
                if ($balancebudplanf < 0) {
                    $this->available_amount = 0;
                } else {
                    $this->available_amount = ($current_amount + $add_amount) - ($minus_amount + $availed);
                }
            } else {
                $this->available_amount = 0;
            }
        }
    }

    protected function updatedTransferType()
    {
        $this->reset([
            'division_from',
            'source_from',
            'chart_from',
            'budget_plan_from',
            'month_from',
            'available_amount',
        ]);

        if ($this->transfer_type == 2) {

            $this->division_from = $this->budget_plan_1->division_id;
            $this->source_from = $this->budget_plan_1->budget_source_id;
            $this->chart_from = $this->budget_plan_1->budget_chart_id;
            $this->loadSourceReference();
            $this->loadChartReference();
            $this->loadBudgetPlan();
        }
    }

    protected function updatedDivisionFrom()
    {
        $this->loadSourceReference();
        $this->loadChartReference();
        $this->loadBudgetPlan();
        $this->reset([
            'source_from',
            'chart_from',
            'budget_plan_from',
        ]);

        $this->budget_chart_references = [];
        $this->budget_plan_references = [];
    }

    protected function updatedSourceFrom()
    {
        $this->loadChartReference();
        $this->loadBudgetPlan();
        $this->reset([
            'chart_from',
            'budget_plan_from',
        ]);

        $this->budget_plan_references = [];
    }

    protected function updatedChartFrom()
    {
        $this->loadBudgetPlan();
    }

    protected function updatedBudgetPlanFrom()
    {
        $this->loadBudgetPlan2();
    }

    protected function updatedMonthFrom()
    {
        $this->loadBudgetPlan2();
    }

    public function submit(BudgetPlanInterface $BudgetPlanInterface)
    {
        $validated = $this->validate([
            'transfer_type' => 'required',
            'source_from' => 'sometimes',
            'chart_from' => 'sometimes',
            'division_from' => 'sometimes',
            'budget_plan_from' => 'required_if:transfer_type,2',
            'month_from' => 'required_if:transfer_type,2',
            'available_amount' => 'required_if:transfer_type,2',

            'month_to' => 'required',
            'amount' => 'required',
        ]);
        $validated_amount = [];
        if ($this->transfer_type == 2) {
            $validated_amount = $this->validate([
                'amount' => 'required|gt:0|lte:available_amount',
            ]);
        }

        $response = $BudgetPlanInterface->transferBudget($this->budget_plan_2, $this->budget_plan_1, $validated, $validated_amount);

        if ($response['code'] == 200) {
            // $this->reset();
            $this->emitTo('accounting.budget-management.budget-plan.index', 'close_modal', 'transfer_budget');
            $this->emitTo('accounting.budget-management.budget-plan.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetFrom()
    {
        $this->reset([
            'transfer_type',

            'year',

            'division_from',
            'source_from',
            'chart_from',
            'budget_plan_from',
            'month_from',
            'available_amount',

            'division_to',
            'source_to',
            'chart_to',
            'budget_plan_to',
            'month_to',
            'amount',
        ]);
    }

    public function render()
    {
        return view('livewire.accounting.budget-management.budget-plan.transfer-budget');
    }
}
