<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetPlan;

use App\Interfaces\Accounting\BudgetManagement\BudgetPlanInterface;
use App\Traits\Accounting\BudgetManagement\BudgetPlanTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use BudgetPlanTrait, PopUpMessagesTrait;

    public function submit(BudgetPlanInterface $BudgetPlanInterface)
    {
        $validated = $this->validate([
            'division' => 'required',
            'source' => 'required',
            'chart' => 'required',
            'item' => 'required',
            'opex_type' => 'required',
            'year' => 'required',
            'request_type' => 'required',

            'january' => 'required',
            'february' => 'required',
            'march' => 'required',
            'april' => 'required',
            'may' => 'required',
            'june' => 'required',
            'july' => 'required',
            'august' => 'required',
            'september' => 'required',
            'october' => 'required',
            'november' => 'required',
            'december' => 'required',
        ]);

        $response = $BudgetPlanInterface->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.budget-management.budget-plan.index', 'close_modal', 'create');
            $this->emitTo('accounting.budget-management.budget-plan.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.budget-management.budget-plan.create');
    }
}
