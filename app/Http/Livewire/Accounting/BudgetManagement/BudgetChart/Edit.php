<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetChart;

use App\Interfaces\Accounting\BudgetManagement\BudgetChartInterface;
use App\Models\Accounting\BudgetChart;
use App\Traits\Accounting\BudgetManagement\BudgetChartTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use BudgetChartTrait, PopUpMessagesTrait;

    public $budget_chart;

    protected $listeners = ['budget_chart_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->budget_chart = BudgetChart::findOrFail($id);
        $this->division = $this->budget_chart->division_id;
        $this->source = $this->budget_chart->budget_source_id;
        $this->name = $this->budget_chart->name;

        $this->loadBudgetSourceReference();
    }

    public function submit(BudgetChartInterface $BudgetChartInterface)
    {
        $validated = $this->validate([
            'division' => 'required',
            'source' => 'required',
            'name' => 'required',
        ]);

        $response = $BudgetChartInterface->update($this->budget_chart, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.budget-management.budget-chart.index', 'close_modal', 'edit');
            $this->emitTo('accounting.budget-management.budget-chart.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.budget-management.budget-chart.edit');
    }
}
