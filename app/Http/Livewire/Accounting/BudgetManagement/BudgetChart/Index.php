<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetChart;

use App\Models\Accounting\BudgetChart;
use App\Models\Accounting\BudgetSource;
use App\Models\Division;
use Livewire\Component;

class Index extends Component
{
    public $create_modal = false;
    public $edit_modal = false;
    public $budget_chart_id;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $division = false;
    public $source;
    public $name;

    public $header_cards = [];
    public $division_references = [];
    public $budget_source_references = [];

    protected $listeners = ['index' => 'render', 'load_head_cards' => 'loadHeadCards', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadHeadCards();
        $this->loadDivisionReference();
    }

    public function loadHeadCards()
    {
        $divisions = Division::withCount('budgetChart')->get();

        $this->header_cards[] = [
            'title' => 'All',
            'value' => $divisions->sum('budget_chart_count'),
            'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
            'color' => 'text-blue',
            'action' => 'division',
            'id' => false
        ];

        foreach ($divisions as $division) {
            $this->header_cards[] = [
                'title' => $division->name,
                'value' => $division->budget_chart_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-blue',
                'action' => 'division',
                'id' => $division->id
            ];
        }
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function loadBudgetSourceReference()
    {
        $this->budget_source_references = BudgetSource::where('division_id', $this->division)->get();
    }

    protected function updatedDivision()
    {
        $this->loadBudgetSourceReference();
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emit('budget_chart_edit_mount', $data['id']);
            $this->edit_modal = true;
        }

        $this->budget_chart_id = $data['id'];
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.budget-management.budget-chart.index', [
            'budget_charts' => BudgetChart::with('division', 'budgetSource')
                ->where('name', 'like', '%' . $this->name . '%')
                ->when($this->division, function ($query) {
                    $query->where('division_id', $this->division);
                })
                ->when($this->source, function ($query) {
                    $query->where('budget_source_id', $this->source);
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
