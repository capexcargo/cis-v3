<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetChart;

use App\Interfaces\Accounting\BudgetManagement\BudgetChartInterface;
use App\Traits\Accounting\BudgetManagement\BudgetChartTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use BudgetChartTrait, PopUpMessagesTrait;

    public function submit(BudgetChartInterface $BudgetChartInterface)
    {
        $validated = $this->validate([
            'division' => 'required',
            'source' => 'required',
            'name' => 'required',
        ]);

        $response = $BudgetChartInterface->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.budget-management.budget-chart.index', 'close_modal', 'create');
            $this->emitTo('accounting.budget-management.budget-chart.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.budget-management.budget-chart.create');
    }
}
