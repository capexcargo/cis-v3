<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetLoa;

use App\Models\Accounting\BudgetChart;
use App\Models\Accounting\BudgetLoa;
use App\Models\Accounting\BudgetPlan;
use App\Models\Accounting\BudgetSource;
use App\Models\Division;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $create_modal = false;
    public $edit_modal = false;
    public $budget_loa_id;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $division;
    public $budget_source;
    public $budget_chart;
    public $budget_plan;

    public $division_references = [];
    public $budget_source_references = [];
    public $budget_chart_references = [];
    public $budget_plan_references = [];

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadDivisionReference();
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function loadBudgetSourceReference()
    {
        $this->budget_source_references = BudgetSource::where('division_id', $this->division)->get();
    }

    public function loadBudgetChartReference()
    {
        $this->budget_chart_references = BudgetChart::where([
            ['division_id', $this->division],
            ['budget_source_id', $this->budget_source],
        ])->get();
    }

    public function loadBudgetPlanReference()
    {
        $this->budget_plan_references = BudgetPlan::withCount('budgetLoa')->where([
            ['division_id', $this->division],
            ['budget_source_id', $this->budget_source],
            ['budget_chart_id', $this->budget_chart],
        ])->get();
    }

    protected function updated()
    {
        $this->resetPage();
    }

    protected function updatedDivision()
    {
        $this->loadBudgetPlanReference();
        $this->loadBudgetSourceReference();
        $this->budget_chart_references = [];
        $this->budget_plan_references = [];
        $this->reset([
            'budget_source',
            'budget_chart',
            'budget_plan',
        ]);
    }

    protected function updatedBudgetSource()
    {
        $this->loadBudgetPlanReference();
        $this->loadBudgetChartReference();
        $this->budget_plan_references = [];
        $this->reset([
            'budget_chart',
            'budget_plan',
        ]);
    }

    protected function updatedBudgetChart()
    {
        $this->loadBudgetPlanReference();
        $this->reset([
            'budget_plan',
        ]);
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emit('budget_loa_edit_mount', $data['id']);
            $this->edit_modal = true;
        }

        $this->budget_loa_id = $data['id'];
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.budget-management.budget-loa.index', [
            'budget_loas' => BudgetLoa::with('budgetPlan', 'firstApprover', 'secondApprover', 'thirdApprover')
                ->when($this->budget_plan, function ($query) {
                    $query->where('budget_plan_id', $this->budget_plan);
                })->whereHas('budgetPlan', function ($query) {
                    $query->when($this->division, function ($query) {
                        $query->where('division_id', $this->division);
                    })->when($this->budget_source, function ($query) {
                        $query->where('budget_source_id', $this->budget_source);
                    })->when($this->budget_chart, function ($query) {
                        $query->where('budget_chart_id', $this->budget_chart);
                    });
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)

        ]);
    }
}
