<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetLoa;

use App\Interfaces\Accounting\BudgetManagement\BudgetLoaInterface;
use App\Traits\Accounting\BudgetManagement\BudgetLoaTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use BudgetLoaTrait, PopUpMessagesTrait;

    public function submit(BudgetLoaInterface $BudgetLoaInterface)
    {
        $validated = $this->validate([
            'division' => 'required',
            'budget_source' => 'required',
            'budget_chart' => 'required',
            'budget_plan' => 'required',
            'limit_min_amount' => 'required',
            'limit_max_amount' => 'required|gt:limit_min_amount',
            'first_approver' => 'required',
            'second_approver' => 'sometimes',
            'third_approver' => 'sometimes',
        ]);

        $response = $BudgetLoaInterface->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.budget-management.budget-loa.index', 'close_modal', 'create');
            $this->emitTo('accounting.budget-management.budget-loa.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.budget-management.budget-loa.create');
    }
}
