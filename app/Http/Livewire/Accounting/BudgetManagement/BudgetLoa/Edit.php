<?php

namespace App\Http\Livewire\Accounting\BudgetManagement\BudgetLoa;

use App\Interfaces\Accounting\BudgetManagement\BudgetLoaInterface;
use App\Models\Accounting\BudgetLoa;
use App\Traits\Accounting\BudgetManagement\BudgetLoaTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use BudgetLoaTrait, PopUpMessagesTrait;

    public $budget_loa;

    protected $listeners = ['budget_loa_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->budget_loa = BudgetLoa::with('budgetPlan')->findOrFail($id);

        $this->division = $this->budget_loa->budgetPlan->division_id;
        $this->budget_source = $this->budget_loa->budgetPlan->budget_source_id;
        $this->budget_chart = $this->budget_loa->budgetPlan->budget_chart_id;
        $this->budget_plan = $this->budget_loa->budget_plan_id;
        $this->limit_min_amount = $this->budget_loa->limit_min_amount;
        $this->limit_max_amount = $this->budget_loa->limit_max_amount;

        $this->loadFirstApproverReferences();
        $this->loadSecondApproverReferences();
        $this->loadThirdApproverReferences();

        $this->loadDivisionReference();
        $this->loadBudgetSourceReference();
        $this->loadBudgetChartReference();
        $this->loadBudgetPlanReference();

        $this->getApproverName($this->budget_loa->first_approver_id, 1);
        $this->getApproverName($this->budget_loa->second_approver_id, 2);
        $this->getApproverName($this->budget_loa->third_approver_id, 3);
    }

    public function submit(BudgetLoaInterface $BudgetLoaInterface)
    {
        $validated = $this->validate([
            'division' => 'required',
            'budget_source' => 'required',
            'budget_chart' => 'required',
            'budget_plan' => 'required',
            'limit_min_amount' => 'required',
            'limit_max_amount' => 'required|gt:limit_min_amount',
            'first_approver' => 'required',
            'second_approver' => 'sometimes',
            'third_approver' => 'sometimes',
        ]);

        $response = $BudgetLoaInterface->update($this->budget_loa, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.budget-management.budget-loa.index', 'close_modal', 'edit');
            $this->emitTo('accounting.budget-management.budget-loa.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.budget-management.budget-loa.edit');
    }
}
