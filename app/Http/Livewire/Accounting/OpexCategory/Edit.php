<?php

namespace App\Http\Livewire\Accounting\OpexCategory;

use App\Interfaces\Fims\DataManagement\OpexCategoryInterface;
use App\Models\FimsOpexCategory;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $confirmation_modal;
    
    public $subAccountId;

    public $name;
    public $opex_name;

    public function mount($id)
    {
        $this->subAccountId = $id;
        $name_lists = FimsOpexCategory::findOrFail($id);
        
        $this->name = $name_lists['name'];
        $this->opex_name = $name_lists['opex_name'];
    }

    public function load(){
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.opex-category.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(OpexCategoryInterface $opexCat_interface)
    {
        $response = $opexCat_interface->updateValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(OpexCategoryInterface $opexCat_interface)
    {
        $response = $opexCat_interface->update2($this->getRequest(), $this->subAccountId);

        if ($response['code'] == 200) {
            $this->emitTo('accounting.opex-category.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'name' => $this->name,
            'opex_name' => $this->opex_name,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'name',
            'opex_name',
        ]);
    }
    public function render()
    {
        return view('livewire.accounting.opex-category.edit');
    }
}
