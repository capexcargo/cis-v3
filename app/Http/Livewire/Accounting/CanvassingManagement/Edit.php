<?php

namespace App\Http\Livewire\Accounting\CanvassingManagement;

use App\Interfaces\Accounting\CanvassingManagementInterface;
use App\Models\Accounting\Canvassing;
use App\Traits\Accounting\CanvassingManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use CanvassingManagementTrait, PopUpMessagesTrait;

    public $canvassing;

    protected $listeners = ['canvassing_management_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->canvassing = Canvassing::with(['createdBy', 'canvassingSupplier' => function ($query) {
            $query->with(['supplier', 'requestForPayment', 'canvassingSupplierItem' => function ($query) {
                $query->with('supplierItem');
            }]);
        }])->findOrFail($id);

        $this->reference_number = $this->canvassing->reference_id;

        foreach ($this->canvassing->canvassingSupplier as $canvassing_supplier) {

            $items = [];
            foreach ($canvassing_supplier->canvassingSupplierItem as $canvassing_supplier_item) {
                $items[] = [
                    'id' => $canvassing_supplier_item->id,
                    'name' => $canvassing_supplier_item->supplierItem->name,
                    'unit_cost' => $canvassing_supplier_item->unit_cost,
                    'quantity' => $canvassing_supplier_item->quantity,
                    'total_amount' => ($canvassing_supplier_item->unit_cost * $canvassing_supplier_item->quantity),
                    'recommended_status' => $canvassing_supplier_item->recommended_status,
                    'final_status' => $canvassing_supplier_item->final_status,
                    'is_deleted' => false,
                ];
            }

            $this->suppliers[] = [
                'id' => $canvassing_supplier->id,
                'supplier_id' => $canvassing_supplier->supplier->id,
                'company' => $canvassing_supplier->supplier->company,
                'industry' => $canvassing_supplier->supplier->industry_id,
                'type' => $canvassing_supplier->supplier->type_id,
                'branch' => $canvassing_supplier->supplier->branch_id,
                'first_name' => $canvassing_supplier->supplier->first_name,
                'middle_name' => $canvassing_supplier->supplier->middle_name,
                'last_name' => $canvassing_supplier->supplier->last_name,
                'email' => $canvassing_supplier->supplier->email,
                'mobile_number' => $canvassing_supplier->supplier->mobile_number,
                'telephone_number' => $canvassing_supplier->supplier->telephone_number,
                'address' => $canvassing_supplier->supplier->address,
                'terms' => $canvassing_supplier->supplier->terms,
                'items' => $items,
                'total_amount' => $canvassing_supplier->canvassingSupplierItem->sum('total_amount'),
                'is_deleted' => false,
            ];
        }
    }

    public function updateItemStatus($a, $b, $status, $type)
    {
        if ($type == 'recommended') {
            $this->suppliers[$a]['items'][$b]['recommended_status'] = $status;
        } else if ($type == 'final') {
            $this->suppliers[$a]['items'][$b]['final_status'] = $status;
        }
        array_values($this->suppliers[$a]['items']);
    }

    public function submit(CanvassingManagementInterface $canvassing_management_interface)
    {
        $validated = $this->validate([
            'suppliers.*.id' => 'sometimes',
            'suppliers.*.supplier_id' => 'sometimes',
            'suppliers.*.company' => 'required_if:suppliers.*.is_deleted,false',
            'suppliers.*.industry' => 'required_if:suppliers.*.is_deleted,false',
            'suppliers.*.type' => 'required_if:suppliers.*.is_deleted,false',
            'suppliers.*.branch' => 'required_if:suppliers.*.is_deleted,false',
            'suppliers.*.first_name' => 'required_if:suppliers.*.is_deleted,false',
            'suppliers.*.middle_name' => 'required_if:suppliers.*.is_deleted,false',
            'suppliers.*.last_name' => 'required_if:suppliers.*.is_deleted,false',
            'suppliers.*.email' => 'required_if:suppliers.*.is_deleted,false',
            'suppliers.*.mobile_number' => 'required_if:suppliers.*.is_deleted,false|numeric|digits:11',
            'suppliers.*.telephone_number' => 'required_if:suppliers.*.is_deleted,false|numeric|digits_between:8, 10',
            'suppliers.*.address' => 'required_if:suppliers.*.is_deleted,false',
            'suppliers.*.terms' => 'required_if:suppliers.*.is_deleted,false',
            'suppliers.*.items' => 'required_if:suppliers.*.is_deleted,false',
            'suppliers.*.is_deleted' => 'sometimes',

            'suppliers.*.items.*.id' => 'sometimes',
            'suppliers.*.items.*.name' => 'required_if:suppliers.*.items.*.is_deleted,false',
            'suppliers.*.items.*.unit_cost' => 'required_if:suppliers.*.items.*.is_deleted,false',
            'suppliers.*.items.*.quantity' => 'required_if:suppliers.*.items.*.is_deleted,false',
            'suppliers.*.items.*.is_deleted' => 'sometimes',
        ], [
            'suppliers.*.company.required_if' => 'The company field is required.',
            'suppliers.*.industry.required_if' => 'The industry field is required.',
            'suppliers.*.type.required_if' => 'The type field is required.',
            'suppliers.*.branch.required_if' => 'The branch field is required.',
            'suppliers.*.first_name.required_if' => 'The first name field is required.',
            'suppliers.*.middle_name.required_if' => 'The middle name field is required.',
            'suppliers.*.last_name.required_if' => 'The last name field is required.',
            'suppliers.*.email.required_if' => 'The email field is required.',
            'suppliers.*.mobile_number.required_if' => 'The mobile number field is required.',
            'suppliers.*.mobile_number.digits' => 'The mobile number must be 11 digits.',
            'suppliers.*.telephone_number.required_if' => 'The telephone number field is required.',
            'suppliers.*.telephone_number.digits_between' => 'The telephone number must be between 8 and 10 digits.',
            'suppliers.*.address.required_if' => 'The address field is required.',
            'suppliers.*.terms.required_if' => 'The terms field is required.',
            'suppliers.*.items.required_if' => 'The items field is required.',

            'suppliers.*.items.*.name.required_if' => 'The name field is required.',
            'suppliers.*.items.*.unit_cost.required_if' => 'The unit cost field is required.',
            'suppliers.*.items.*.quantity.required_if' => 'The quantity field is required.',
        ]);

        $response = $canvassing_management_interface->update($this->canvassing, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.canvassing-management.index', 'close_modal', 'edit');
            $this->emitTo('accounting.canvassing-management.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.canvassing-management.edit');
    }
}
