<?php

namespace App\Http\Livewire\Accounting\CanvassingManagement;

use App\Interfaces\Accounting\CanvassingManagementInterface;
use App\Traits\Accounting\CanvassingManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Create extends Component
{
    use CanvassingManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['generate_reference_number' => 'generateReferenceNumber'];

    public function mount()
    {
        $this->addSupplier();
    }

    public function submit(CanvassingManagementInterface $canvassing_management_interface)
    {
        $validated = $this->validate([
            'reference_number' => 'required',
            'suppliers.*.supplier_id' => 'sometimes',
            'suppliers.*.company' => 'required',
            'suppliers.*.industry' => 'required',
            'suppliers.*.type' => 'required',
            'suppliers.*.branch' => 'required',
            'suppliers.*.first_name' => 'required',
            'suppliers.*.middle_name' => 'required',
            'suppliers.*.last_name' => 'required',
            'suppliers.*.email' => 'required',
            'suppliers.*.mobile_number' => 'required|numeric|digits:11',
            'suppliers.*.telephone_number' => 'required|numeric|digits_between:8, 10',
            'suppliers.*.address' => 'required',
            'suppliers.*.terms' => 'required',
            'suppliers.*.items' => 'required',

            'suppliers.*.items.*.name' => 'required',
            'suppliers.*.items.*.unit_cost' => 'required',
            'suppliers.*.items.*.quantity' => 'required',
        ], [
            'suppliers.*.company.required' => 'The company field is required.',
            'suppliers.*.industry.required' => 'The industry field is required.',
            'suppliers.*.type.required' => 'The type field is required.',
            'suppliers.*.branch.required' => 'The branch field is required.',
            'suppliers.*.first_name.required' => 'The first name field is required.',
            'suppliers.*.middle_name.required' => 'The middle name field is required.',
            'suppliers.*.last_name.required' => 'The last name field is required.',
            'suppliers.*.email.required' => 'The email field is required.',
            'suppliers.*.mobile_number.required' => 'The mobile number field is required.',
            'suppliers.*.mobile_number.digits' => 'The mobile number must be 11 digits.',
            'suppliers.*.telephone_number.required' => 'The telephone number field is required.',
            'suppliers.*.telephone_number.digits_between' => 'The telephone number must be between 8 and 10 digits.',
            'suppliers.*.address.required' => 'The address field is required.',
            'suppliers.*.terms.required' => 'The terms field is required.',
            'suppliers.*.items.required' => 'The items field is required.',

            'suppliers.*.items.*.name.required' => 'The name field is required.',
            'suppliers.*.items.*.unit_cost.required' => 'The unit cost field is required.',
            'suppliers.*.items.*.quantity.required' => 'The quantity field is required.',
        ]);
        

        $response = $canvassing_management_interface->create($validated, Auth::user()->id);
       
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.canvassing-management.index', 'close_modal', 'create');
            $this->emitTo('accounting.canvassing-management.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.canvassing-management.create');
    }
}
