<?php

namespace App\Http\Livewire\Accounting\CanvassingManagement;

use App\Models\Accounting\Canvassing;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $create_modal = false;
    public $edit_modal = false;
    public $canvassing_id;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $reference_number;
    public $company;
    public $item_name;
    public $assigned;
    public $first_name;
    public $middle_name;
    public $last_name;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        if ($action_type == 'create') {
            $this->emit('generate_reference_number');
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('canvassing_management_edit_mount', $data['id']);
            $this->edit_modal = true;
        }

        $this->canvassing_id = $data['id'] ?? null;
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.canvassing-management.index', [
            'canvassings' => Canvassing::with(['createdBy', 'canvassingSupplier' => function ($query) {
                $query->with(['supplier', 'requestForPayment', 'canvassingSupplierItem' => function ($query) {
                    $query->with('supplierItem');
                }])->whereHas('supplier', function ($query) {
                    $query->when($this->company, function ($query) {
                        $query->where('company', 'like', '%' . $this->company . '%');
                    })->when($this->first_name, function ($query) {
                        $query->where('first_name', 'like', '%' . $this->first_name . '%');
                    })->when($this->middle_name, function ($query) {
                        $query->where('middle_name', 'like', '%' . $this->middle_name . '%');
                    })->when($this->last_name, function ($query) {
                        $query->where('last_name', 'like', '%' . $this->last_name . '%');
                    });
                })->when($this->item_name, function ($query) {
                    $query->whereHas('canvassingSupplierItem', function ($query) {
                        $query->whereHas('supplierItem', function ($query) {
                            $query->where('name', 'like', '%' . $this->item_name . '%');
                        });
                    });
                });
            }])
                ->when($this->reference_number, function ($query) {
                    $query->where('reference_id', 'like', '%' . $this->reference_number . '%');
                })
                ->whereHas('canvassingSupplier', function ($query) {
                    $query->whereHas('supplier', function ($query) {
                        $query->when($this->company, function ($query) {
                            $query->where('company', 'like', '%' . $this->company . '%');
                        })->when($this->first_name, function ($query) {
                            $query->where('first_name', 'like', '%' . $this->first_name . '%');
                        })->when($this->middle_name, function ($query) {
                            $query->where('middle_name', 'like', '%' . $this->middle_name . '%');
                        })->when($this->last_name, function ($query) {
                            $query->where('last_name', 'like', '%' . $this->last_name . '%');
                        });
                    })
                        ->when($this->item_name, function ($query) {
                            $query->whereHas('canvassingSupplierItem', function ($query) {
                                $query->whereHas('supplierItem', function ($query) {
                                    $query->where('name', 'like', '%' . $this->item_name . '%');
                                });
                            });
                        });
                })
                ->when(Auth::user()->level_id < 3, function ($query) {
                    $query->where('created_by', Auth::user()->id);
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
