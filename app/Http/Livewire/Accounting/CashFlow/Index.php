<?php

namespace App\Http\Livewire\Accounting\CashFlow;

use App\Interfaces\Accounting\CheckVoucherInterface;
use App\Models\Accounting\CashFlowDetails;
use App\Models\Accounting\CashFlowSnapshot;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $confirmation_modal = false;
    public $view_modal = false;
    public $snapshot_modal = false;
    public $cash_flow_details_id;
    public $parent_reference_no;
    public $action_type;
    public $status_id;
    public $confirmation_message;

    public $current_balance = 0;
    public $floating_balance = 0;
    public $year = 2022;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $cv_date_from;
    public $cv_date_to;
    public $encashment_date_from;
    public $encashment_date_to;
    public $check_no;

    public $cash_flow_snapshot;

    public $header_cards = [];

    protected $listeners = ['index' => 'render', 'load_head_cards' => 'loadHeadCards', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadHeadCards();
    }

    public function loadHeadCards()
    {
        $this->cash_flow_snapshot = CashFlowSnapshot::where('year', $this->year)->first();

        $this->header_cards = [
            [
                'title' => 'Current Balance',
                'value' => number_format($this->cash_flow_snapshot->current_balance, 2),
                'color' => 'text-blue',
                'action' => 'cash',
                'id' => null
            ],
            [
                'title' => 'Floating Balance',
                'value' => number_format($this->cash_flow_snapshot->floating_balance, 2),
                'color' => 'text-blue',
                'action' => 'cash',
                'id' => null
            ]
        ];
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'view') {
            $this->emit('request_management_view_mount', $data['parent_reference_no']);
            $this->view_modal = true;
        } else if ($action_type == 'status') {
            $this->confirmation_modal = true;
            $this->confirmation_message = 'Are you sure to change status to ' . ($data['status_id'] == 1 ? 'Unbudgeted' : 'Budgeted');
            $this->status_id = $data['status_id'] ?? null;
            $this->cash_flow_details_id = $data['cash_flow_details_id'] ?? null;
        } else if ($action_type == 'snapshot') {
            $this->snapshot_modal = true;
            if ($this->cash_flow_snapshot) {
                $this->current_balance = $this->cash_flow_snapshot->current_balance;
                $this->floating_balance = $this->cash_flow_snapshot->floating_balance;
            }
        }

        $this->parent_reference_no = $data['parent_reference_no'] ?? null;
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'view') {
            $this->view_modal = false;
        } elseif ($action_type == 'confirmation') {
            $this->confirmation_modal = false;
        }
    }

    public function remove(CheckVoucherInterface $check_voucher_interface)
    {
        if ($this->action_type == 'status') {
            $cash_flow_details = CashFlowDetails::find($this->cash_flow_details_id);
            $response = $check_voucher_interface->updateStatusCashFlow($cash_flow_details, $this->status_id);

            if ($response['code'] == 200) {
                $this->emitTo('accounting.cash-flow.index', 'close_modal', 'confirmation');
                $this->emitTo('accounting.cash-flow.index', 'index');
                $this->sweetAlert('success', $response['message']);
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
            return;
        }
    }

    public function submitSnapshot(CheckVoucherInterface $check_voucher_interface)
    {
        $validated = $this->validate([
            'current_balance' => 'required',
            'floating_balance' => 'required',
            'year' => 'required',
        ]);

        $response = $check_voucher_interface->createOrUpdateCashFlowSnapshot($validated);

        if ($response['code'] == 200) {
            $this->emitTo('accounting.cash-flow.index', 'index');
            $this->emitTo('accounting.cash-flow.index', 'load_head_cards');
            $this->sweetAlert('success', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.cash-flow.index', [
            'cash_flow_details' => CashFlowDetails::with(['checkVoucherDetails' => function ($query) {
                $query->with(['checkVoucher' => function ($query) {
                    $query->with('requestForPayment');
                }]);
            }, 'payee', 'status'])
                ->when($this->cv_date_from, function ($query) {
                    $query->whereDate('cv_date', '>=', $this->cv_date_from);
                })
                ->when($this->cv_date_to, function ($query) {
                    $query->whereDate('cv_date', '<=', $this->cv_date_to);
                })
                ->when($this->encashment_date_from, function ($query) {
                    $query->whereDate('encashment_date', '>=', $this->encashment_date_from);
                })
                ->when($this->encashment_date_to, function ($query) {
                    $query->whereDate('encashment_date', '<=', $this->encashment_date_to);
                })
                ->when($this->check_no, function ($query) {
                    $query->where('check_no', 'like', '%' . $this->check_no . '%');
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
