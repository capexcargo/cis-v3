<?php

namespace App\Http\Livewire\Accounting\ReasonForRejection;

use Livewire\Component;
use App\Interfaces\Fims\DataManagement\ReasonForRejectionInterface;
use App\Models\FimsReasonForRejection;
use App\Traits\PopUpMessagesTrait;
use Livewire\WithPagination;

class Edit extends Component
{
    use WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $confirmation_modal;
    
    public $unitId;

    public $RFR_name;

    public function mount($id)
    {
        $this->unitId = $id;
        $RFR_name_lists = FimsReasonForRejection::findOrFail($id);
        
        $this->RFR_name = $RFR_name_lists['name'];
    }

    public function load(){
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.reason-for-rejection.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(ReasonForRejectionInterface $RFR_interface)
    {
        $response = $RFR_interface->updateValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(ReasonForRejectionInterface $RFR_interface)
    {
        $response = $RFR_interface->update2($this->getRequest(), $this->unitId);

        if ($response['code'] == 200) {
            $this->emitTo('accounting.reason-for-rejection.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'RFR_name' => $this->RFR_name,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'RFR_name',
        ]);
    }
    public function render()
    {
        return view('livewire.accounting.reason-for-rejection.edit');
    }
}
