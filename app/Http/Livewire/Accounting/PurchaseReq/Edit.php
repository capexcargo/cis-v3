<?php

namespace App\Http\Livewire\Accounting\PurchaseReq;

use App\Interfaces\Fims\ServicePurchaseRequisition\PurchaseReqInterface;
use App\Models\Accounting\Supplier;
use App\Models\BranchReference;
use App\Models\FimsItemCategory;
use App\Models\FimsItemDescription;
use App\Models\FimsPurchasing;
use App\Models\FimsPurpose;
use App\Models\FimsSupplier;
use App\Traits\InitializeFirestoreTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use WithPagination, PopUpMessagesTrait, InitializeFirestoreTrait;

    protected $listeners = ['edit' => 'mount'];

    public $reference_no;
    public $confirmation_modal;
    public $del_date;
    public $purchase_forms = [];

    public $item_category_references = [];
    public $item_description_references = [];
    public $branch_references = [];
    public $purpose_references = [];
    public $pref_references = [];

    public $countarr;
    public $nearest;
    public $min;

    public $purchid;
    public $purchReq = [];

    public function mount($id)
    {
        $this->purchid = $id;
        $purchReq = FimsPurchasing::with('PurchReqDetails2', 'attachments')->findOrFail($id);
        // dd($purchReq);

        $this->del_date = $purchReq['expected_purchase_delivery_date'];


        // $this->updatedServiceCategoryId();
        foreach ($purchReq->PurchReqDetails2 as $a => $purchRe) {
            // dd($purchRe);
            $this->purchase_forms[$a] = [
                'id' => $purchRe->id,
                'item_category_id' => $purchRe->item_category_id,
                'item_description_id' => $purchRe->item_description_id,
                'qty' => $purchRe->qty,
                'unit' => $purchRe->unit,
                'estimated_price' => $purchRe->estimated_rate,
                'estimated_total' => $purchRe->estimated_total,
                'purpose' => $purchRe->purpose,
                'branch_id' => $purchRe->beneficiary_branch_id,
                'sfrom' => $purchRe->series_start_no,
                'sto' => $purchRe->series_end_no,
                'pref_supp' => $purchRe->preferred_supplier_id,
                'rmks' => $purchRe->remarks,
                'samp_prod' => $purchRe->is_sample_product,
                'is_deleted' => false,
            ];
            // dd($this->purchase_forms[$a]);

        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->purchase_forms as $a => $areatag) {

            $getkeyarray[] = $a;
            $countarray += ($this->purchase_forms[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    public function confirmationSubmit()
    {
            $this->confirmation_modal = true;
    }

    public function ItemCategoryReference()
    {
        $this->item_category_references = FimsItemCategory::get();
    }

    // public function ItemCategoryReference()
    // {
    //     $this->item_category_references = FimsItemCategory::get();
    // }


    protected function updatedPurchaseForms()
    {
        // $this->loadItemCategoryId();


        foreach ($this->purchase_forms as $i => $purchase_form) {

            // if($this->purchase_forms[$i]['item_category_id'] != ""){
                $this->purchase_forms[$i]['item_category_id'] = $this->purchase_forms[$i]['item_category_id'];
                $this->item_description_references[$i] = FimsItemDescription::where([
                    ['item_category_id', $this->purchase_forms[$i]['item_category_id']]
                ])->get();
            // }else{
            //     $this->item_description_references[$i] = FimsItemDescription::where([
            //         ['item_category_id', 1]
            //     ])->get();
            // }
         

        //  dd($this->item_description_references);

        }
    }


    // public function updatedItemCategoryId(array $data, $i)
    // {
    //     // dd($data['a']);
    //     dd($i);
    //     foreach ($this->purchase_forms[$data['i']] as $purchase_form) {

    //     $this->item_description_references = FimsItemDescription::where('item_category_id', $this->purchase_forms[$data['i']]['item_category_id'])->get();
    //     // dd($this->service_description_references);
    //     }
    // }
    public function ItemDescriptionReference()
    {
        $this->item_description_references = FimsItemDescription::get();
    }

    public function loadBranchReference()
    {
        $this->branch_references = BranchReference::get();
    }

    public function PurposeReference()
    {
        $this->purpose_references = FimsPurpose::get();
    }

    public function PrefReference()
    {
        $this->pref_references = Supplier::get();
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.purchase-req.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    // =================================================================================================================================

    public function addpurchase_forms()
    {
        $this->purchase_forms[] = [
            'id' => null,
            'item_category_id' => null,
            'item_description_id' => null,
            'qty' => null,
            'unit' => null,
            'estimated_price' => null,
            'estimated_total' => null,
            'purpose' => null,
            'branch_id' => null,
            'sfrom' => null,
            'sto' => null,
            'pref_supp' => null,
            'rmks' => null,
            'samp_prod' => null,
            'is_deleted' => false,
        ];
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->purchase_forms as $a => $areatag) {

            if ($this->purchase_forms[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }
            $countarray += ($this->purchase_forms[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    // =============================================================================================================================================

    public function removepurchase_forms(array $data)
    {
        if (count(collect($this->purchase_forms)->where('is_deleted', false)) > 1) {
            if ($this->purchase_forms[$data["a"]]['id']) {
                $this->purchase_forms[$data["a"]]['is_deleted'] = true;
                // unset($this->purchase_forms[$data["a"]]);
            } else {
                unset($this->purchase_forms[$data["a"]]);
            }
            array_values($this->purchase_forms);
        }
        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->purchase_forms as $a => $areatag) {

            if ($this->purchase_forms[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }
            $countarray += ($this->purchase_forms[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        $this->countarr = $countarray;
    }

    public function submit(PurchaseReqInterface $purchasereq_interface)
    {
        $response = $purchasereq_interface->update2($this->getRequest(), $this->purchid);

        if ($response['code'] == 200) {
            $this->emitTo('accounting.purchase-req.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'reference_no' => $this->reference_no,
            'del_date' => $this->del_date,
            'purchase_forms' => $this->purchase_forms,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'reference_no',
            'del_date',
            'purchase_forms',
        ]);
    }

    public function render()
    {
        return view('livewire.accounting.purchase-req.edit');
    }
}
