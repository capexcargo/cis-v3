<?php

namespace App\Http\Livewire\Accounting\PurchaseReq;

use App\Interfaces\Fims\ServicePurchaseRequisition\PurchaseReqInterface;
use App\Models\FimsPurchasing;
use App\Traits\InitializeFirestoreTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait, InitializeFirestoreTrait;

    public $paginate = 50;
    public $stats;

    public $create_modal = false;
    public $edit_modal = false;
    public $purref_modal = false;
    public $action_type;
    public $reqid;
    

    public $search_request;
    public $status_header_cards = [];

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal = true;
        }elseif ($action_type == 'edit') {
            $this->reqid = $data['id'];
            $this->edit_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }else if ($action_type == 'pref') {
            $this->purref_modal = false;
        }
    }

    public function load()
    {
        $this->loadStatusHeaderCards();
        $this->stats = false;
    }

    public $reference_no;
    public $del_date;
    public $purchase_forms = [];

    public $countarr;
    public $nearest;
    public $min;

    public $purchid;
    public $purchReq = [];

    public function actionpurchref(array $data, $action_type)
    {
        $this->purchid = $data['id'];
        $purchReq = FimsPurchasing::with('PurchReqDetails2', 'attachments')->findOrFail($data['id']);
        // dd($purchReq);

        $this->del_date = $purchReq['expected_purchase_delivery_date'];


        // $this->updatedServiceCategoryId();
        foreach ($purchReq->PurchReqDetails2 as $a => $purchRe) {
            // dd($purchRe);
            $this->purchase_forms[$a] = [
                'id' => $purchRe->id,
                'item_category_id' => $purchRe->item_category_id,
                'item_description_id' => $purchRe->item_description_id,
                'qty' => $purchRe->qty,
                'unit' => $purchRe->unit,
                'estimated_price' => $purchRe->estimated_rate,
                'estimated_total' => $purchRe->estimated_total,
                'purpose' => $purchRe->purpose,
                'branch_id' => $purchRe->beneficiary_branch_id,
                'sfrom' => $purchRe->series_start_no,
                'sto' => $purchRe->series_end_no,
                'pref_supp' => $purchRe->preferred_supplier_id,
                'rmks' => $purchRe->remarks,
                'samp_prod' => $purchRe->is_sample_product,
                'is_deleted' => false,
            ];
        }


        $this->action_type = $action_type;
        if ($action_type == 'pref') {
            $this->purref_modal = true;
        }
    }

    public function loadStatusHeaderCards()
    {
        // $activestatus = BarangayReference::where('status', 1)->get();
        // $inactivestatus = BarangayReference::where('status', 2)->get();

        // $statusalls = BarangayReference::get();

            $getAll = FimsPurchasing::get();
            $getCI = FimsPurchasing::where('priority_level_id', 1)->get();
            $getVI = FimsPurchasing::where('priority_level_id', 2)->get();
            $getI = FimsPurchasing::where('priority_level_id', 3)->get();
            $getLI = FimsPurchasing::where('priority_level_id', 4)->get();

        $statusall = $getAll->count();
        $statusci = $getCI->count();
        $statusvi = $getVI->count();
        $statusi = $getI->count();
        $statusli = $getLI->count();

        $this->status_header_cards = [
           
            [
                'title' => 'ALL',
                'value' => $statusall,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => false,
            ],
            [
                'title' => 'CRITICALLY IMPORTANT',
                'value' => $statusci,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 1,
            ],
            [
                'title' => 'VERY IMPORTANT',
                'value' => $statusvi,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 2
            ],
            [
                'title' => 'IMPORTANT',
                'value' => $statusi,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 3
            ],
            [
                'title' => 'LESS IMPORTANT',
                'value' => $statusli,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 4
            ],
          
        ];
    }

    
    

    

    public function search(PurchaseReqInterface $purchase_req_interface)
    {
        $this->search_request = [
            'reference_no' => $this->reference_no,
        ];
    }

    public function render(PurchaseReqInterface $purchase_req_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'status' => $this->stats,
        ];

        $response = $purchase_req_interface->index2($request, $this->search_request);

        // dd($response['result']);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'purchase_req_lists' => [],
                ];
        }
        return view('livewire.accounting.purchase-req.index', [
            'purchase_req_lists' => $response['result']['list']
        ]);
    }
}
