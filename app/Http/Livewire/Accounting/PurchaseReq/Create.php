<?php

namespace App\Http\Livewire\Accounting\PurchaseReq;

use App\Interfaces\Fims\ServicePurchaseRequisition\PurchaseReqInterface;
use App\Models\Accounting\Supplier;
use App\Models\BranchReference;
use App\Models\FimsItemCategory;
use App\Models\FimsItemDescription;
use App\Models\FimsPurpose;
use App\Models\FimsServiceCategory;
use App\Models\FimsSupplier;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function mount()
    {
        // $this->reference_no = 'FLS-PRN-' . strtoupper(uniqid());

    }

    public $reference_no;
    public $confirmation_modal;
    public $del_date;
    public $purchase_forms = [];

    public $item_category_references = [];
    public $item_description_references = [];
    public $branch_references = [];
    public $purpose_references = [];
    public $pref_references = [];

    public function ItemCategoryReference()
    {
        $this->item_category_references = FimsItemCategory::get();
    }


    protected function updatedPurchaseForms()
    {
        // $this->loadItemCategoryId();


        foreach ($this->purchase_forms as $i => $purchase_form) {

            // if($this->purchase_forms[$i]['item_category_id'] != ""){
                $this->purchase_forms[$i]['item_category_id'] = $this->purchase_forms[$i]['item_category_id'];
                $this->item_description_references[$i] = FimsItemDescription::where([
                    ['item_category_id', $this->purchase_forms[$i]['item_category_id']]
                ])->get();
            // }else{
            //     $this->item_description_references[$i] = FimsItemDescription::where([
            //         ['item_category_id', 1]
            //     ])->get();
            // }
         

        //  dd($this->item_description_references);

        }
    }

    // public function loadItemCategoryId()
    // {
    //     foreach ($this->purchase_forms as $i => $purchase_form) {
    //         $this->item_description_references = FimsItemDescription::where([
    //             ['item_category_id', $this->purchase_forms[$i]['item_category_id']]
    //         ])->get();
    //     }
    // }
    // public function ItemDescriptionReference()
    // {
    //     $this->item_description_references = FimsItemDescription::get();
    // }

    public function loadBranchReference()
    {
        $this->branch_references = BranchReference::get();
    }

    public function PurposeReference()
    {
        $this->purpose_references = FimsPurpose::get();
    }

    public function PrefReference()
    {
        $this->pref_references = Supplier::get();
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.purchase-req.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function addpurchase_forms()
    {
        $this->purchase_forms[] = [
            'id' => null,
            'item_category_id' => null,
            'item_description_id' => null,
            'qty' => null,
            'unit' => null,
            'estimated_price' => null,
            'estimated_total' => null,
            'purpose' => null,
            'branch_id' => null,
            'sfrom' => null,
            'sto' => null,
            'pref_supp' => null,
            'rmks' => null,
            'samp_prod' => null,
            'is_deleted' => false,
        ];
    }

    public function removepurchase_forms(array $data)
    {
        if (count(collect($this->purchase_forms)->where('is_deleted', false)) > 1) {
            if ($this->purchase_forms[$data["a"]]['id']) {
                $this->purchase_forms[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->purchase_forms[$data["a"]]);
            }

            array_values($this->purchase_forms);
        }
    }

    public function confirmationSubmit(PurchaseReqInterface $purchasereq_interface)
    {
        $response = $purchasereq_interface->createValidation2($this->getRequest());
        // dd($response);
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function submit(PurchaseReqInterface $purchasereq_interface)
    {
        $response = $purchasereq_interface->create2($this->getRequest());
        // dd($response);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.purchase-req.index', 'close_modal', 'create');
            $this->emitTo('accounting.purchase-req.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'reference_no' => $this->reference_no,
            'del_date' => $this->del_date,
            'purchase_forms' => $this->purchase_forms,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'reference_no',
            'del_date',
            'purchase_forms',
        ]);
    }
    public function render()
    {
        if (!$this->purchase_forms) {
            $this->purchase_forms[] = [
                'id' => null,
                'item_category_id' => null,
                'item_description_id' => null,
                'qty' => null,
                'unit' => null,
                'estimated_price' => null,
                'estimated_total' => null,
                'purpose' => null,
                'branch_id' => null,
                'sfrom' => null,
                'sto' => null,
                'pref_supp' => null,
                'rmks' => null,
                'samp_prod' => null,
                'is_deleted' => false,
            ];
        }
        return view('livewire.accounting.purchase-req.create');
    }
}
