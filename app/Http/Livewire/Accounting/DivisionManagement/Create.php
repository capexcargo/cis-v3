<?php

namespace App\Http\Livewire\Accounting\DivisionManagement;

use App\Interfaces\Accounting\DivisionManagementInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use PopUpMessagesTrait;

    public $name;
    public $description;

    public function submit(DivisionManagementInterface $DivisionManagementInterface)
    {
        $validated = $this->validate([
            'name' => 'required|unique:division,name',
            'description' => 'required',
        ]);

        $response = $DivisionManagementInterface->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.division-management.index', 'close_modal', 'create');
            $this->emitTo('accounting.division-management.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            "name",
            "description"
        ]);
    }

    public function render()
    {
        return view('livewire.accounting.division-management.create');
    }
}
