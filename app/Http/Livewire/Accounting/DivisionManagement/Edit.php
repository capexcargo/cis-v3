<?php

namespace App\Http\Livewire\Accounting\DivisionManagement;

use App\Interfaces\Accounting\DivisionManagementInterface;
use App\Models\Division;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use PopUpMessagesTrait;

    public $name;
    public $description;

    public $division;

    protected $listeners = ['division_management_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->division = Division::findOrFail($id);

        $this->name = $this->division->name;
        $this->description = $this->division->description;
    }

    public function submit(DivisionManagementInterface $DivisionManagementInterface)
    {
        $validated = $this->validate([
            'name' => 'required|unique:division,name,' . $this->division->id . ',id',
            'description' => 'required',
        ]);

        $response = $DivisionManagementInterface->update($this->division, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.division-management.index', 'close_modal', 'edit');
            $this->emitTo('accounting.division-management.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            "name",
            "description"
        ]);
    }

    public function render()
    {
        return view('livewire.accounting.division-management.edit');
    }
}
