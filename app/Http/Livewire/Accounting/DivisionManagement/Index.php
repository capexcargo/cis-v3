<?php

namespace App\Http\Livewire\Accounting\DivisionManagement;

use App\Models\Division;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $create_modal = false;
    public $edit_modal = false;
    public $division_id;

    public $division;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $name;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emit('division_management_edit_mount', $data['id']);
            $this->edit_modal = true;
        } else if ($action_type == 'redirectToPosition') {
            return redirect()->to(route('hrim.workforce.position.index'));
        } else if ($action_type == 'redirectToJobLevel') {
            return redirect()->to(route('hrim.workforce.job-level.index'));
        } else if ($action_type == 'redirectToDepartment') {
            return redirect()->to(route('hrim.workforce.department.index'));
        } else if ($action_type == 'redirectToDivision') {
            return redirect()->to(route('accounting.division-management.index'));
        }

        $this->division_id = $data['id'];
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.division-management.index', [
            'divisions' => Division::where('name', 'like', '%' . $this->division . '%')
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'desc' : 'asc');
                })->paginate($this->paginate)

        ]);
    }
}
