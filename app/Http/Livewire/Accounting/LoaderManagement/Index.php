<?php

namespace App\Http\Livewire\Accounting\LoaderManagement;

use App\Models\Accounting\Loaders;
use App\Models\Accounting\LoadersTypeReference;
use Livewire\Component;

class Index extends Component
{
    public $create_modal = false;
    public $edit_modal = false;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $name;
    public $loader_types = [];
    public $loader_id;
    public $loader_type_references = [];
    public $loaders_type_id;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loaderTypeReference();
    }

    public function loaderTypeReference()
    {
        $this->loader_type_references = LoadersTypeReference::get();
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emit('loader_edit_mount', $data['id']);
            $this->edit_modal = true;
        }

        $this->loader_id = $data['id'];
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;

        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.loader-management.index', [
            'loaders' => Loaders::with('loaderTypeReference')
                ->when($this->name, function ($query) {
                    $query->where('name', 'like', '%' . $this->name . '%');
                })
                ->when($this->loaders_type_id, function ($query) {
                    $query->where('loaders_type_id', $this->loaders_type_id);
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
