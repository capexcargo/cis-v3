<?php

namespace App\Http\Livewire\Accounting\LoaderManagement;

use App\Interfaces\Accounting\LoaderManagementInterface;
use App\Models\Accounting\Loaders;
use App\Models\Accounting\LoadersTypeReference;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use PopUpMessagesTrait;

    protected $listeners = ['loader_edit_mount' => 'mount'];

    public $name;
    public $loaders_type_id;
    public $loader_type_references = [];

    public function mount($id)
    {
        $this->resetForm();
        $this->loaders = Loaders::findOrFail($id);
        $this->loaders_type_id = $this->loaders->loaders_type_id;
        $this->name = $this->loaders->name;
    }

    public function submit(LoaderManagementInterface $LoaderManagementInterface)
    {
        $request = [
            'name' => $this->name,
            'loaders_type_id' => $this->loaders_type_id,
        ];

        $response = $LoaderManagementInterface->update($this->loaders->id, $request);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.loader-management.index', 'close_modal', 'edit');
            $this->emitTo('accounting.loader-management.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            "name",
            "loaders_type_id"
        ]);
    }


    public function loaderTypeReference()
    {
        $this->loader_type_references = LoadersTypeReference::get();
    }

    public function render()
    {
        return view('livewire.accounting.loader-management.edit');
    }
}
