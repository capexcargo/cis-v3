<?php

namespace App\Http\Livewire\Accounting\ItemDescription;

use App\Interfaces\Fims\DataManagement\ItemDescriptionInterface;
use App\Models\FimsItemCategory;
use App\Models\FimsItemDescription;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{

    use WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $confirmation_modal;
    
    public $itemDesId;

    public $item_category_id;
    public $description;
    public $price;

    public $item_category_id_references = [];

    public function mount($id)
    {
        $this->itemDesId = $id;
        $item_category_id_lists = FimsItemDescription::with('ItemCat')->findOrFail($id);
        
        $this->item_category_id = $item_category_id_lists['ItemCat']['id'];
        $this->description = $item_category_id_lists->description;
        $this->price = $item_category_id_lists->price;
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.item-description.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

        public function load(){
        $this->ItemCategoryIdReference();
    }

    public function ItemCategoryIdReference()
    {
        $this->item_category_id_references = FimsItemCategory::get();
    }

    public function confirmationSubmit(ItemDescriptionInterface $itemdes_interface)
    {
        $response = $itemdes_interface->updateValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(ItemDescriptionInterface $itemdes_interface)
    {
        $response = $itemdes_interface->update2($this->getRequest(), $this->itemDesId);

        if ($response['code'] == 200) {
            $this->emitTo('accounting.item-description.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'item_category_id' => $this->item_category_id,
            'description' => $this->description,
            'price' => $this->price,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'item_category_id',
            'description',
            'price',
        ]);
    }
    public function render()
    {
        return view('livewire.accounting.item-description.edit');
    }
}
