<?php

namespace App\Http\Livewire\Accounting\ItemDescription;

use App\Interfaces\Fims\DataManagement\ItemDescriptionInterface;
use App\Models\FimsItemCategory;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function mount()
    {
    }
    public $confirmation_modal;
    public $item_category_id;
    public $description;
    public $price;

    public $item_category_id_references = [];

    public function ItemCategoryIdReference()
    {
        $this->item_category_id_references = FimsItemCategory::get();
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.item-description.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(ItemDescriptionInterface $itemdes_interface)
    {
        $response = $itemdes_interface->createValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function submit(ItemDescriptionInterface $itemdes_interface)
    {
        $response = $itemdes_interface->create2($this->getRequest());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.item-description.index', 'close_modal', 'create');
            $this->emitTo('accounting.item-description.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'item_category_id' => $this->item_category_id,
            'description' => $this->description,
            'price' => $this->price,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'item_category_id',
            'description',
            'price',
        ]);
    }

    public function render()
    {
        return view('livewire.accounting.item-description.create');
    }
}
