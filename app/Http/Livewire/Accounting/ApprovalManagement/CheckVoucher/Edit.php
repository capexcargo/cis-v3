<?php

namespace App\Http\Livewire\Accounting\ApprovalManagement\CheckVoucher;

use App\Interfaces\Accounting\CheckVoucherInterface;
use App\Models\Accounting\CheckVoucher;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Edit extends Component
{
    use PopUpMessagesTrait;

    public $check_voucher;

    public $approver_1_id;
    public $approver_2_id;
    public $approver_1_name;
    public $approver_2_name;
    public $is_approved_1 = false;
    public $is_approved_2 = false;
    public $approver_remarks_1;
    public $approver_remarks_2;

    protected $listeners = ['check_voucher_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->check_voucher = CheckVoucher::with(['requestForPayment' => function ($query) {
            $query->with('payee', 'opex');
        }, 'status', 'checkVoucherDetails'])
            ->find($id);
        abort_if(!$this->check_voucher, 404);

        $this->approver_1_id = $this->check_voucher->approver_1_id;
        $this->approver_2_id = $this->check_voucher->approver_2_id;
        $this->approver_1_name = $this->check_voucher->firstApprover ? $this->check_voucher->firstApprover->name : '';
        $this->approver_2_name = $this->check_voucher->secondApprover ? $this->check_voucher->secondApprover->name : '';
        $this->is_approved_1 = $this->check_voucher->is_approved_1;
        $this->is_approved_2 = $this->check_voucher->is_approved_2;
        $this->approver_remarks_1 = $this->check_voucher->approver_1_remarks;
        $this->approver_remarks_2 = $this->check_voucher->approver_2_remarks;
    }

    public function submit(CheckVoucherInterface $check_voucher_interface)
    {
        $validated = $this->validate([
            'approver_1_id' => 'sometimes',
            'approver_2_id' => 'sometimes',
            'is_approved_1' => 'sometimes',
            'is_approved_2' => 'sometimes',
            'approver_remarks_1' => 'sometimes',
            'approver_remarks_2' => 'sometimes',
        ]);

        if ($this->approver_1_id) {
            if ($this->approver_1_id == Auth::user()->id && !$this->approver_remarks_1 || Auth::user()->level_id == 5 && $this->check_voucher->approver_1_id) {
                if (!$this->is_approved_1 && !$this->approver_remarks_1) {
                    $this->addError('approver_remarks_1', 'The approver remarks 1 field is required.');
                }

                if ($this->check_voucher->is_approved_2 && !$validated['is_approved_1']) {
                    $this->addError('approver_2_id', 'The approver 2 must be disapproved.');
                }
            }
        }

        if ($this->approver_2_id) {
            if ($this->is_approved_1 || !$this->is_approved_2) {
                if ($this->approver_2_id == Auth::user()->id && !$this->approver_remarks_2 || Auth::user()->level_id == 5 && $this->check_voucher->approver_1_id) {
                    if (!$this->is_approved_2 && !$this->approver_remarks_2) {
                        $this->addError('approver_remarks_2', 'The approver remarks 2 field is required.');
                    }
                }
            } else {
                $this->addError('approver_1_id', 'The approver 1 must be approved.');
            }
        }

        $errors = $this->getErrorBag();
        if ($errors->any()) {
            return;
        }

        $response = $check_voucher_interface->approved($this->check_voucher, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.approval-management.check-voucher.index', 'close_modal', 'edit');
            $this->emitTo('accounting.approval-management.check-voucher.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            'approver_1_id',
            'approver_2_id',
            'approver_1_name',
            'approver_2_name',
            'is_approved_1',
            'is_approved_2',
            'approver_remarks_1',
            'approver_remarks_2',
        ]);
    }

    public function render()
    {
        return view('livewire.accounting.approval-management.check-voucher.edit');
    }
}
