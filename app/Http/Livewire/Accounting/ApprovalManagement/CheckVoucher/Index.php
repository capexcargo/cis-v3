<?php

namespace App\Http\Livewire\Accounting\ApprovalManagement\CheckVoucher;

use App\Interfaces\Accounting\CheckVoucherInterface;
use App\Models\Accounting\StatusReference;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $edit_modal = false;
    public $view_modal = false;
    public $check_voucher_id;
    public $parent_reference_no;

    public $rfp_reference_no;
    public $check_reference_no;
    public $cv_no;
    public $check_no;
    public $created_by;
    public $received_by;
    public $received_date_from;
    public $received_date_to;
    public $created_date_from;
    public $created_date_to;
    public $released_date_from;
    public $released_date_to;
    public $status = false;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $status_references = [];

    public $header_cards = [];

    protected $queryString = ['edit_modal', 'check_voucher_id'];
    protected $listeners = ['index' => 'render', 'load_head_cards' => 'loadHeadCards', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadHeadCards();
        $this->loadStatusReference();
    }

    public function loadHeadCards()
    {
        $check_vouchers = StatusReference::withCount('checkVouchers')->checkVoucher()->get();

        $this->header_cards = [];

        $this->header_cards[] = [
            'title' => 'All',
            'value' => $check_vouchers->sum('check_vouchers_count'),
            'action' => 'status',
            'id' => false
        ];
        foreach ($check_vouchers as $check_voucher) {
            $this->header_cards[] = [
                'title' => $check_voucher->display,
                'value' => $check_voucher->check_vouchers_count,
                'action' => 'status',
                'id' => $check_voucher->id
            ];
        }
    }

    public function loadStatusReference()
    {
        $this->status_references = StatusReference::checkVoucher()->get();
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emit('check_voucher_edit_mount', $data['id']);
            $this->edit_modal = true;
        } else if ($action_type == 'view') {
            $this->emit('request_management_view_mount', $data['parent_reference_no']);
            $this->view_modal = true;
        }

        $this->parent_reference_no = $data['parent_reference_no'] ?? null;
        $this->check_voucher_id = $data['id'] ?? null;
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'edit') {
            $this->edit_modal = false;
        } else if ($action_type == 'view') {
            $this->view_modal = false;
        }
    }

    public function render(CheckVoucherInterface $check_voucher_interface)
    {
        $request = [
            'rfp_reference_no' => $this->rfp_reference_no,
            'check_reference_no' => $this->check_reference_no,
            'cv_no' => $this->cv_no,
            'check_no' => $this->check_no,
            'created_by' => $this->created_by,
            'received_by' => $this->received_by,
            'received_date_from' => $this->received_date_from,
            'received_date_to' => $this->received_date_to,
            'created_date_from' => $this->created_date_from,
            'created_date_to' => $this->created_date_to,
            'released_date_from' => $this->released_date_from,
            'released_date_to' => $this->released_date_to,
            'have_check_voucher' => 1,
            'status' => $this->status,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $check_voucher_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.accounting.approval-management.check-voucher.index', [
            'request_for_payments' => $response['result']
        ]);
    }
}
