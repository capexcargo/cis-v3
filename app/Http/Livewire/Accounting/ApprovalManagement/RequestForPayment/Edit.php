<?php

namespace App\Http\Livewire\Accounting\ApprovalManagement\RequestForPayment;

use App\Interfaces\Accounting\RequestManagementInterface;
use App\Models\Accounting\AccountTypeReference;
use App\Models\Accounting\CanvassingSupplier;
use App\Models\Accounting\CAReferenceTypes;
use App\Models\Accounting\RequestForPayment;
use App\Traits\Accounting\RequestManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use RequestManagementTrait, PopUpMessagesTrait, WithFileUploads;

    public $edit_modal = false;

    protected $listeners = ['request_management_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->request = RequestForPayment::with(['budget' => function ($query) {
            $query->with('division', 'source', 'chart');
        }, 'requestForPaymentDetails' => function ($query) {
            $query->with('loaders', 'paymentType', 'terms');
        }, 'priority', 'payee', 'branch', 'opex', 'canvassingSupplier', 'accountType', 'CAReferenceType', 'attachments'])
            ->when(Auth::user()->level_id <= 3, function ($query) {
                $query->whereRaw("(approver_1_id = " . Auth::user()->id . " OR approver_2_id = " . Auth::user()->id . " OR approver_3_id = " . Auth::user()->id . ")");
            })
            ->find($id);

        abort_if(!$this->request, 404);

        if (Auth::user()->level_id == 4) {
            $allow_to_view = false;
            if ($this->request->approver1) {
                if ($this->request->approver1->division_id == Auth::user()->division_id) {
                    $allow_to_view = true;
                }
            } elseif ($this->request->approver2) {
                if ($this->request->approver2->division_id == Auth::user()->division_id) {
                    $allow_to_view = true;
                }
            } elseif ($this->request->approver3) {
                if ($this->request->approver3->division_id == Auth::user()->division_id) {
                    $allow_to_view = true;
                }
            }

            abort_if(!$allow_to_view, 404);
        }


        if (!$this->request->budget_id) {
            return $this->sweetAlertError('info', "Budget Plan", 'This request will be available for tagging budget and approving for next year.');
        }

        $this->parent_reference_number = $this->request->parent_reference_no;
        $this->reference_number = $this->request->reference_id;
        $this->rfp_type = $this->request->type_id;

        $this->is_payee_contact_person = $this->request->is_payee_contact_person;
        $this->payee = $this->request->payee_id;
        $this->remarks = $this->request->remarks;
        $this->description = $this->request->description;
        $this->amount = $this->request->amount;
        $this->multiple_budget = $this->request->multiple_budget_id;
        $this->branch = $this->request->branch_id;
        $this->coa_category = $this->request->budget_id;
        $this->date_needed = $this->request->date_needed;
        $this->date_of_transaction_from = $this->request->date_of_transaction_from;
        $this->date_of_transaction_to = $this->request->date_of_transaction_to;
        $this->priority = $this->request->priority_id;
        $this->canvasser = $this->request->canvasser;
        $this->opex_type = $this->request->opex_type_id;

        $this->division = $this->request->budget->division_id;
        $this->budget_source = $this->request->budget->budget_source_id;
        $this->chart_of_accounts_search = $this->request->budget->chart->name;
        $this->chart_of_accounts = $this->request->budget->budget_chart_id;


        if ($this->rfp_type == 1) {
            if ($this->request->canvassingSupplier) {
                $this->canvassing_supplier = CanvassingSupplier::find($this->request->canvassing_supplier_id);
                $this->po_reference_no = $this->canvassing_supplier->po_reference_no;
                $this->canvassing_supplier_id = $this->canvassing_supplier->id;
            }

            foreach ($this->request->requestForPaymentDetails as $request_for_payment_details) {
                $this->request_for_payments[] = [
                    'id' => $request_for_payment_details->id,
                    'particulars' => $request_for_payment_details->particulars,
                    'plate_no' => $request_for_payment_details->plate_no,
                    'labor_cost' => $request_for_payment_details->labor_cost,
                    'unit_cost' => $request_for_payment_details->unit_cost,
                    'quantity' => $request_for_payment_details->quantity,
                    'amount' => $request_for_payment_details->amount,
                    'is_deleted' => false,
                ];
            }
        } else if ($this->rfp_type == 2) {
            $this->freight_account_type = AccountTypeReference::find($this->request->account_type_id);
            $this->accounting_account_type = $this->request->account_type_id;
            if ($this->request->requestForPaymentDetails[0]->loaders) {
                $this->loader = $this->request->requestForPaymentDetails[0]->loaders_id;
                $this->loader_search = $this->request->requestForPaymentDetails[0]->loaders->name;
            }

            foreach ($this->request->requestForPaymentDetails as $request_for_payment_details) {
                $month = strtolower(date("F", strtotime($request_for_payment_details->transaction_date)));
                // $budget_plan = $this->getBudgetPlanBalance($month, $request_for_payment_details->transaction_date);
                $budget_plan = $this->getBudgetPlanBalanceFreight($month, $request_for_payment_details->transaction_date);
                $total_amountssss = ($budget_plan['budget_plan_sum_' . strtolower(date('F', strtotime($request_for_payment_details->transaction_date)))] +
                    $budget_plan->transfer_budget_to_sum_amount) - $budget_plan->transfer_budget_from_sum_amount;
                // dd($budget_plan);

                // $plan_approved =
                //     $budget_plan->chart['budget_plan_sum_' . $month] +
                //     ($budget_plan->chart->transferBudgetTo
                //         ? $budget_plan->chart
                //         ->transferBudgetTo()
                //         ->where('month_to', $month)
                //         ->sum('amount')
                //         : 0) -
                //     ($budget_plan->chart->transferBudgetFrom
                //         ? $budget_plan->chart
                //         ->transferBudgetFrom()
                //         ->where('month_from', $month)
                //         ->sum('amount')
                //         : 0);
                // $plan_availed = $budget_plan->chart->availment_sum_amount
                //     ? $budget_plan->chart
                //     ->availment()
                //     ->where('month', $month)
                //     ->sum('amount')
                //     : 0;
                // dd( $budget_plan,
                //     $total_amountssss, $budget_plan->availment_sum_amount, $request_for_payment_details->amount
                // );
                $plan_balance = round((($budget_plan->availment_sum_amount + $request_for_payment_details->amount) / $total_amountssss) * 100, 2) < 98.3 ? round($total_amountssss - $budget_plan->availment_sum_amount, 2) : 0;
                // dd($plan_approved,$request_for_payment_details->amount, $plan_availed, $month, $plan_balance, round((($plan_availed + $request_for_payment_details->amount) / $plan_approved) * 100, 2));

                $this->freights[] = [
                    'id' => $request_for_payment_details->id,
                    'freight_reference_no' => $request_for_payment_details->freight_reference_no,
                    'freight_reference_type' => $request_for_payment_details->freight_reference_type_id,
                    'soa_no' => $request_for_payment_details->soa_no,
                    'trucking_type' => $request_for_payment_details->trucking_type_id,
                    'trucking_amount' => $request_for_payment_details->trucking_amount,
                    'freight_amount' => $request_for_payment_details->freight_amount,
                    'freight_usage' => $request_for_payment_details->freight_usage_id,
                    'transaction_date' => $request_for_payment_details->transaction_date,
                    'allowance' => $request_for_payment_details->allowance,
                    'amount' => $request_for_payment_details->amount,
                    'balance_budget' => $plan_balance,
                    'is_deleted' => false,
                ];
            }
            $this->loadFreightReferenceTypeReference();
            $this->loadFreightUsageReference();
        } else if ($this->rfp_type == 3) {
            $this->account_no = $this->request->requestForPaymentDetails[0]->account_no;
            $this->payment_type = $this->request->requestForPaymentDetails[0]->payment_type_id;
            $this->terms = $this->request->requestForPaymentDetails[0]->terms_id;
            $this->pdc_from = $this->request->requestForPaymentDetails[0]->pdc_from;
            $this->pdc_to = $this->request->requestForPaymentDetails[0]->pdc_to;
            foreach ($this->request->requestForPaymentDetails as $request_for_payment_details) {
                $this->payables[] = [
                    'id' => $request_for_payment_details->id,
                    'invoice_no' => $request_for_payment_details->invoice,
                    'invoice_amount' => $request_for_payment_details->invoice_amount,
                    'date_of_transaction' => $request_for_payment_details->transaction_date,
                    'is_deleted' => false,
                ];
            }
        } else if ($this->rfp_type == 4) {
            $this->ca_no = $this->request->ca_no;
            if ($this->request->ca_reference_type_id) {
                $this->ca_reference_type = $this->request->ca_reference_type_id;
                $this->ca_reference_type_display = (CAReferenceTypes::find($this->request->ca_reference_type_id))->display;
            }
            foreach ($this->request->requestForPaymentDetails as $request_for_payment_details) {
                $this->cash_advances[] = [
                    'id' => $request_for_payment_details->id,
                    'particulars' => $request_for_payment_details->particulars,
                    'ca_reference_no' => $request_for_payment_details->ca_reference_no,
                    'unit_cost' => $request_for_payment_details->unit_cost,
                    'quantity' => $request_for_payment_details->quantity,
                    'amount' => $request_for_payment_details->amount,
                    'is_deleted' => false,
                ];
            }
        }

        $this->loadBudgetSourceReference();
        $this->loadBudgetChartReference();
        $this->loadBudgetPlanReference();
        $this->loadApprovers();
        $this->updatedCoaCategory();

        $this->total_amount = $this->request->amount;


        $this->is_set_approver_1 = $this->request->is_set_approver_1;
        $this->approver_1_id = $this->request->approver_1_id;
        $this->approver_2_id = $this->request->approver_2_id;
        $this->approver_3_id = $this->request->approver_3_id;
        $this->is_approved_1 = $this->request->is_approved_1;
        $this->is_approved_2 = $this->request->is_approved_2;
        $this->is_approved_3 = $this->request->is_approved_3;
        $this->approver_remarks_1 = $this->request->approver_remarks_1;
        $this->approver_remarks_2 = $this->request->approver_remarks_2;
        $this->approver_remarks_3 = $this->request->approver_remarks_3;

        $this->approver_1_name = $this->request->approver1 ? $this->request->approver1->name : '';
        $this->approver_2_name = $this->request->approver2 ? $this->request->approver2->name : '';
        $this->approver_3_name = $this->request->approver3 ? $this->request->approver3->name : '';
    }

    public function submit(RequestManagementInterface $request_management_interface)
    {
        $validated = $this->validate([
            'approver_1_id' => 'sometimes',
            'approver_2_id' => 'sometimes',
            'approver_3_id' => 'sometimes',
            'is_approved_1' => 'sometimes',
            'is_approved_2' => 'sometimes',
            'is_approved_3' => 'sometimes',
            'approver_remarks_1' => 'sometimes',
            'approver_remarks_2' => 'sometimes',
            'approver_remarks_3' => 'sometimes',
        ]);

        if ($this->approver_1_id) {
            if ($this->approver_1_id == Auth::user()->id && !$this->approver_remarks_1 || Auth::user()->level_id == 5 && $this->request->approver_1_id) {
                if (!$this->is_approved_1 && !$this->approver_remarks_1) {
                    $this->addError('approver_remarks_1', 'The approver remarks 1 field is required.');
                }

                if ($this->request->is_approved_2 && !$validated['is_approved_1']) {
                    $this->addError('approver_2_id', 'The approver 2 must be disapproved.');
                }
            }
        }

        if ($this->approver_2_id) {
            if ($this->is_approved_1 || !$this->is_approved_2) {
                if ($this->approver_2_id == Auth::user()->id && !$this->approver_remarks_2 || Auth::user()->level_id == 5 && $this->request->approver_2_id) {
                    if (!$this->is_approved_2 && !$this->approver_remarks_2) {
                        $this->addError('approver_remarks_2', 'The approver remarks 2 field is required.');
                    }
                }

                if ($this->request->is_approved_3 && !$validated['is_approved_2']) {
                    $this->addError('approver_3_id', 'The approver 3 must be disapproved.');
                }
            } else {
                $this->addError('approver_1_id', 'The approver 1 must be approved.');
            }
        }

        if ($this->approver_3_id) {
            if ($this->is_approved_2 || !$this->is_approved_3) {
                if ($this->approver_3_id == Auth::user()->id && !$this->approver_remarks_3 || Auth::user()->level_id == 5 && $this->request->approver_3_id) {
                    if (!$this->is_approved_3 && !$this->approver_remarks_3) {
                        $this->addError('approver_remarks_3', 'The approver remarks 3 field is required.');
                    }
                }
            } else {
                $this->addError('approver_2_id', 'The approver 2 must be approved.');
            }
        }

        $errors = $this->getErrorBag();
        if ($errors->any()) {
            return;
        }
        $response = $request_management_interface->approved($this->request, $validated, Auth::user()->id,  Auth::user()->level_id);
        // dd($response);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.approval-management.request-for-payment.index', 'close_modal', 'edit');
            $this->emitTo('accounting.approval-management.request-for-payment.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
            // $this->emitTo('accounting.approval-management.request-for-payment.index', 'close_modal', 'edit');
            $this->emit('request_management_edit_mount', $this->request['id']);
            $this->edit_modal = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.approval-management.request-for-payment.edit');
    }
}
