<?php

namespace App\Http\Livewire\Accounting\ApprovalManagement\RequestForPayment;

use App\Models\Accounting\OpexType;
use App\Models\Accounting\PriorityReference;
use App\Models\Accounting\RequestForPayment;
use App\Models\Accounting\RequestForPaymentTypeReference;
use App\Models\Accounting\StatusReference;
use App\Models\Division;
use App\Models\User;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $edit_modal = false;
    public $view_modal = false;
    public $check_voucher_view_modal = false;
    public $request_id;
    public $parent_reference_no;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $division;
    public $reference_no;
    public $request_type;
    public $payee;
    public $opex_type;
    public $requester;
    public $description;
    public $date_created;
    public $date_created_to;
    public $date_needed;
    public $date_needed_to;
    public $status = 1;
    public $approver;
    public $priority = false;
    public $user_id;

    public $header_cards = [];
    public $users = [];

    public $division_references = [];
    public $request_for_payment_type_references = [];
    public $opex_type_references = [];
    public $status_references = [];
    public $priority_references = [];
    public $check_voucher_id;

    protected $queryString = ['edit_modal', 'request_id'];
    protected $listeners = ['index' => 'render', 'load_head_cards' => 'loadHeadCards', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadHeadCards();
        $this->loadUserApprovers();
        $this->loadDivisionReferences();
        $this->loadRequestForPaymentTypeReference();
        $this->loadOpexTypeReference();
        $this->loadStatusReference();
        $this->loadPriorityReference();
    }

    public function loadHeadCards()
    {
        $priorities = PriorityReference::withCount(['requestForPayment' => function ($query) {
            $query->when(Auth::user()->level_id <= 3, function ($query) {
                $query->whereRaw("(approver_1_id = " . Auth::user()->id . " OR approver_2_id = " . Auth::user()->id . " OR approver_3_id = " . Auth::user()->id . ")");
            })
                ->when(Auth::user()->level_id == 4, function ($query) {
                    $query->when(!$this->approver, function ($query) {
                        $query->whereRaw("(approver_1_id = " . Auth::user()->id . " OR approver_2_id = " . Auth::user()->id . " OR approver_3_id = " . Auth::user()->id . ")");
                    })
                        ->when($this->approver == 1, function ($query) {
                            $query->whereHas('approver1', function ($query) {
                                $query->where('division_id', Auth::user()->division_id);
                            });
                        })
                        ->when($this->approver == 2, function ($query) {
                            $query->whereHas('approver2', function ($query) {
                                $query->where('division_id', Auth::user()->division_id);
                            });
                        })
                        ->when($this->approver == 3, function ($query) {
                            $query->whereHas('approver3', function ($query) {
                                $query->where('division_id', Auth::user()->division_id);
                            });
                        });
                })
                ->when($this->status, function ($query) {
                    $query->where('status_id', $this->status);
                })
                ->whereHas('budget');
        }])->get();

        $this->header_cards = [];

        $this->header_cards[] = [
            'title' => 'All',
            'value' => $priorities->sum('request_for_payment_count'),
            'action' => 'priority',
            'id' => false
        ];
        foreach ($priorities as $priority) {
            $this->header_cards[] = [
                'title' => $priority->display,
                'value' => $priority->request_for_payment_count,
                'action' => 'priority',
                'id' => $priority->id
            ];
        }
    }

    public function loadUserApprovers()
    {
        $users = User::withCount(['rfpFirstApprover' => function ($query) {
            $query->whereHas('budget')
                ->where('is_approved_1', null);
        }, 'rfpSecondApprover' => function ($query) {
            $query->whereHas('budget')
                ->where('is_approved_2', null);
        }, 'rfpThirdApprover' => function ($query) {
            $query->whereHas('budget')
                ->where('is_approved_3', null);
        }])
            ->whereHas('rfpFirstApprover', function ($query) {
                $query->where('is_approved_1', null);
            })
            ->orWhereHas('rfpSecondApprover', function ($query) {
                $query->where('is_approved_2', null);
            })
            ->orWhereHas('rfpThirdApprover', function ($query) {
                $query->where('is_approved_3', null);
            })
            ->orderBy('name', 'asc')
            ->get();

        foreach ($users as $user) {
            if (Auth::user()->level_id <= 3) {
                if ($user->id == Auth::user()->id) {
                    $this->users[] = [
                        'id' => $user->id,
                        'rfp_first_approver_count' => $user->rfp_first_approver_count,
                        'rfp_second_approver_count' => $user->rfp_second_approver_count,
                        'rfp_third_approver_count' => $user->rfp_third_approver_count,
                        'name' => $user->name,
                    ];
                }
            } elseif (Auth::user()->level_id == 4) {
                if ($user->division_id == Auth::user()->division_id) {
                    $this->users[] = [
                        'id' => $user->id,
                        'rfp_first_approver_count' => $user->rfp_first_approver_count,
                        'rfp_second_approver_count' => $user->rfp_second_approver_count,
                        'rfp_third_approver_count' => $user->rfp_third_approver_count,
                        'name' => $user->name,
                    ];
                }
            } else {
                $this->users[] = [
                    'id' => $user->id,
                    'rfp_first_approver_count' => $user->rfp_first_approver_count,
                    'rfp_second_approver_count' => $user->rfp_second_approver_count,
                    'rfp_third_approver_count' => $user->rfp_third_approver_count,
                    'name' => $user->name,
                ];
            }
        }
    }

    public function loadDivisionReferences()
    {
        $this->division_references = Division::get();
    }

    public function loadRequestForPaymentTypeReference()
    {
        $this->request_for_payment_type_references = RequestForPaymentTypeReference::active()->get();
    }

    public function loadOpexTypeReference()
    {
        $this->opex_type_references = OpexType::get();
    }

    public function loadStatusReference()
    {
        $this->status_references = StatusReference::requestForPayment()->get();
    }

    public function loadPriorityReference()
    {
        $this->priority_references = PriorityReference::active()->get();
    }

    protected function updatedApprover()
    {
        $this->loadHeadCards();
    }

    public function checkbox($previous_user_id, $current_user_id)
    {
        if ($current_user_id == $previous_user_id) {
            $this->user_id = null;
        } else {
            $this->user_id = $current_user_id;
        }
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'create') {
            $this->emit('generate_reference_number');
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('request_management_edit_mount', $data['id']);
            $this->edit_modal = true;
        } else if ($action_type == 'view') {
            $this->emit('request_management_view_mount', $data['parent_reference_no']);
            $this->view_modal = true;
        } else if ($action_type == 'view_voucher') {
            $this->emit('check_voucher_view_mount', $data['check_voucher_id']);
            $this->check_voucher_view_modal = true;
        }


        $this->parent_reference_no = $data['parent_reference_no'] ?? null;
        $this->request_id = $data['id'] ?? null;
        $this->check_voucher_id = $data['check_voucher_id'] ?? null;

        // dd($this->check_voucher_id);
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } else if ($action_type == 'view') {
            $this->view_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    // public function createAvailmendAll()
    // {
    //     DB::beginTransaction();
    //     try {
    //         $request_for_payments = RequestForPayment::where('status_id', 2)->get();
    //         foreach ($request_for_payments as $request_for_payment) {
    //             if ($request_for_payment->budgetAvailments) {
    //                 foreach ($request_for_payment->budgetAvailments as $budget_availment) {
    //                     $budget_availment->delete();
    //                 }

    //                 $request_for_payment_currents = RequestForPayment::with('requestForPaymentDetails')
    //                     ->where('reference_id', $request_for_payment->reference_id)
    //                     ->whereYear('created_at', date('Y', strtotime(now())))->get();
    //                 foreach ($request_for_payment_currents as $i => $request_for_payment_current) {
    //                     if ($i) {
    //                         foreach ($request_for_payment_current->requestForPaymentDetails as $request_for_payment_details_current) {
    //                             $request_for_payment_details_current->delete();
    //                         }

    //                         $request_for_payment_current->delete();
    //                     }
    //                 }
    //             }

    //             if ($request_for_payment->status_id == 2) {
    //                 if ($request_for_payment->type_id == 2 && $request_for_payment->account_type_id == 1) {
    //                     foreach ($request_for_payment->requestForPaymentDetails as $request_for_payment_details) {
    //                         $request_for_payment->budgetAvailments()->create([
    //                             'division_id' => $request_for_payment->budget->division_id,
    //                             'budget_source_id' => $request_for_payment->budget->budget_source_id,
    //                             'budget_chart_id' => $request_for_payment->budget->budget_chart_id,
    //                             'budget_plan_id' => $request_for_payment->budget_id,
    //                             'amount' => $request_for_payment_details->amount,
    //                             'month' => strtolower(date('F', strtotime($request_for_payment_details->transaction_date))),
    //                             'year' => date('Y', strtotime($request_for_payment_details->transaction_date)),
    //                         ]);
    //                     }
    //                 } elseif ($request_for_payment->type_id == 3) {

    //                     if ($request_for_payment->requestForPaymentDetails[0]->payment_type_id == 2) {
    //                         $current_year = $request_for_payment->requestForPaymentDetails()->whereYear('transaction_date', date('Y', strtotime(now())))->get();
    //                         $next_year = $request_for_payment->requestForPaymentDetails()->whereYear('transaction_date', '>', date('Y', strtotime(now())))->get();

    //                         if ($current_year) {
    //                             foreach ($current_year as $request_for_payment_details) {
    //                                 $request_for_payment->budgetAvailments()->create([
    //                                     'division_id' => $request_for_payment->budget->division_id,
    //                                     'budget_source_id' => $request_for_payment->budget->budget_source_id,
    //                                     'budget_chart_id' => $request_for_payment->budget->budget_chart_id,
    //                                     'budget_plan_id' => $request_for_payment->budget_id,
    //                                     'amount' => $request_for_payment_details->amount,
    //                                     'month' => strtolower(date('F', strtotime($request_for_payment_details->transaction_date))),
    //                                     'year' => date('Y', strtotime($request_for_payment_details->transaction_date)),
    //                                 ]);
    //                             }
    //                         }

    //                         if ($next_year) {
    //                             if ($next_year[0]->payment_type_id == 2) {
    //                                 $request_for_payment_new = (object)array();
    //                                 foreach ($next_year as $i => $request_for_payment_details) {
    //                                     if ($i == 0) {
    //                                         $request_for_payment_new = RequestForPayment::create([
    //                                             'parent_reference_no' => "PRNT-" . date("ymdhis") . "-" . rand(111, 999),
    //                                             'reference_id' => $request_for_payment_details->requestForPayment->reference_id,
    //                                             'type_id' => $request_for_payment_details->requestForPayment->type_id,
    //                                             'payee_id' => $request_for_payment_details->requestForPayment->payee_id,
    //                                             'description' => $request_for_payment_details->requestForPayment->description,
    //                                             'amount' => $next_year->sum("amount"),
    //                                             'remarks' => $request_for_payment_details->requestForPayment->remarks,
    //                                             'multiple_budget_id' => $request_for_payment_details->requestForPayment->multiple_budget_id,
    //                                             'user_id' => $request_for_payment_details->requestForPayment->user_id,
    //                                             'branch_id' => $request_for_payment_details->requestForPayment->branch_id,
    //                                             'priority_id' => $request_for_payment_details->requestForPayment->priority_id,
    //                                             'canvasser' => $request_for_payment_details->requestForPayment->canvasser,
    //                                             'canvassing_supplier_id' => $request_for_payment_details->requestForPayment->canvassing_supplier_id,
    //                                             'ca_no' => $request_for_payment_details->requestForPayment->ca_no,
    //                                             'ca_reference_type_id' => $request_for_payment_details->requestForPayment->ca_reference_type_id,
    //                                             'account_type_id' => $request_for_payment_details->requestForPayment->account_type_id,
    //                                             'date_needed' => $request_for_payment_details->requestForPayment->date_needed,
    //                                             'date_of_transaction_from' => $request_for_payment_details->requestForPayment->date_of_transaction_from,
    //                                             'date_of_transaction_to' => $request_for_payment_details->requestForPayment->date_of_transaction_to,
    //                                             'opex_type_id' => $request_for_payment_details->requestForPayment->opex_type_id,
    //                                             'status_id' => 1,
    //                                         ]);
    //                                     }

    //                                     $request_for_payment_new->requestForPaymentDetails()->create([
    //                                         'reference_id' => $request_for_payment_details->reference_id,

    //                                         'account_no' => $request_for_payment_details->account_no,
    //                                         'payment_type_id' => $request_for_payment_details->payment_type_id,
    //                                         'terms_id' => $request_for_payment_details->terms_id,
    //                                         'pdc_from' => $request_for_payment_details->pdc_from,
    //                                         'pdc_to' => $request_for_payment_details->pdc_to,

    //                                         'invoice' => $request_for_payment_details->invoice,
    //                                         'invoice_amount' => $request_for_payment_details->invoice_amount,
    //                                         'transaction_date' =>  $request_for_payment_details->transaction_date,
    //                                         'amount' => $request_for_payment_details->amount,
    //                                     ]);
    //                                 }
    //                             }
    //                         }
    //                     } else {
    //                         $request_for_payment->budgetAvailments()->create([
    //                             'division_id' => $request_for_payment->budget->division_id,
    //                             'budget_source_id' => $request_for_payment->budget->budget_source_id,
    //                             'budget_chart_id' => $request_for_payment->budget->budget_chart_id,
    //                             'budget_plan_id' => $request_for_payment->budget_id,
    //                             'amount' => $request_for_payment->amount,
    //                             'month' => strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))),
    //                             'year' => date('Y', strtotime($request_for_payment->date_of_transaction_from)),
    //                         ]);
    //                     }
    //                 } else {
    //                     $request_for_payment->budgetAvailments()->create([
    //                         'division_id' => $request_for_payment->budget->division_id,
    //                         'budget_source_id' => $request_for_payment->budget->budget_source_id,
    //                         'budget_chart_id' => $request_for_payment->budget->budget_chart_id,
    //                         'budget_plan_id' => $request_for_payment->budget_id,
    //                         'amount' => $request_for_payment->amount,
    //                         'month' => strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))),
    //                         'year' => date('Y', strtotime($request_for_payment->date_of_transaction_from)),
    //                     ]);
    //                 }
    //             }
    //         }
    //         DB::commit();
    //         return $this->sweetAlert('success', 'Request For Payment Successfully Created', $request_for_payment);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         return $this->sweetAlertError('error', 'Something Went Wrong', $e->getMessage());
    //     }
    // }

    public function render()
    {

        $request_for_payments = RequestForPayment::with('requestForPaymentDetails', 'type', 'user', 'approver1', 'approver2', 'approver3', 'status', 'checkVouchers')
            ->whereHas('budget', function ($query) {
                $query->when($this->division, function ($query) {
                    $query->where('division_id', $this->division);
                });
            })
            ->whereHas('user', function ($query) {
                $query->when($this->requester, function ($query) {
                    $query->where('name', 'like', '%' . $this->requester . '%');
                });
            })
            ->when($this->reference_no, function ($query) {
                $query->where('reference_id', 'like', '%' . $this->reference_no . '%');
            })
            ->when($this->request_type, function ($query) {
                $query->where('type_id', $this->request_type);
            })
            ->when($this->payee, function ($query) {
                $query->whereHas('payee', function ($query) {
                    $query->where('company', 'like', '%' . $this->payee . '%');
                });
            })
            ->when($this->opex_type, function ($query) {
                $query->where('opex_type_id', 'like', '%' . $this->opex_type . '%');
            })
            ->when($this->status, function ($query) {
                $query->where('status_id', $this->status);
            })
            ->when($this->date_created, function ($query) {
                $query->whereDate('created_at', $this->date_created);
            })
            ->when($this->description, function ($query) {
                $query->where('description', 'like', '%' . $this->description . '%');
            })
            ->when($this->priority, function ($query) {
                $query->where('priority_id', $this->priority);
            })
            ->when(Auth::user()->level_id <= 3, function ($query) {
                $query->when(!$this->approver, function ($query) {
                    $query->whereRaw("(approver_1_id = " . Auth::user()->id . " OR approver_2_id = " . Auth::user()->id . " OR approver_3_id = " . Auth::user()->id . ")");
                })
                    ->when($this->approver == 1, function ($query) {
                        $query->where('approver_1_id', Auth::user()->id);
                    })
                    ->when($this->approver == 2, function ($query) {
                        $query->where('approver_2_id', Auth::user()->id);
                    })
                    ->when($this->approver == 3, function ($query) {
                        $query->where('approver_3_id', Auth::user()->id);
                    });
            })
            ->when(Auth::user()->level_id == 4, function ($query) {
                $query->when(!$this->approver, function ($query) {
                    $query->whereRaw("(approver_1_id = " . Auth::user()->id . " OR approver_2_id = " . Auth::user()->id . " OR approver_3_id = " . Auth::user()->id . ")");
                })
                    ->when($this->approver == 1, function ($query) {
                        $query->whereHas('approver1', function ($query) {
                            $query->where('division_id', Auth::user()->division_id);
                        })
                            ->when($this->user_id, function ($query) {
                                $query->where([
                                    ['is_approved_1', null],
                                    ['approver_1_id', $this->user_id]
                                ]);
                            });;
                    })
                    ->when($this->approver == 2, function ($query) {
                        $query->whereHas('approver2', function ($query) {
                            $query->where('division_id', Auth::user()->division_id);
                        })
                            ->when($this->user_id, function ($query) {
                                $query->where([
                                    ['is_approved_2', null],
                                    ['approver_2_id', $this->user_id]
                                ]);
                            });
                    })
                    ->when($this->approver == 3, function ($query) {
                        $query->whereHas('approver3', function ($query) {
                            $query->where('division_id', Auth::user()->division_id);
                        })
                            ->when($this->user_id, function ($query) {
                                $query->where([
                                    ['is_approved_3', null],
                                    ['approver_3_id', $this->user_id]
                                ]);
                            });;
                    });
            })
            ->when(Auth::user()->level_id == 5, function ($query) {
                $query->when(!$this->approver, function ($query) {
                    $query->when($this->user_id, function ($query) {
                        $query->whereRaw("(`is_approved_1` is null and `approver_1_id` = " . $this->user_id . " 
                            or `is_approved_2` is null and `approver_2_id` = " . $this->user_id . " 
                            or `is_approved_3` is null and `approver_3_id` = " . $this->user_id . ")");
                    });
                })
                    ->when($this->approver == 1, function ($query) {
                        $query->where([
                            ['is_approved_1', null],
                            ['approver_1_id', $this->user_id]
                        ]);
                    })
                    ->when($this->approver == 2, function ($query) {
                        $query->where([
                            ['is_approved_2', null],
                            ['approver_2_id', $this->user_id]
                        ]);
                    })
                    ->when($this->approver == 3, function ($query) {
                        $query->where([
                            ['is_approved_3', null],
                            ['approver_3_id', $this->user_id]
                        ]);
                    });
            })
            ->when($this->sortField, function ($query) {
                $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
            })->paginate($this->paginate);



        // dd($request_for_payments);


        return view('livewire.accounting.approval-management.request-for-payment.index', [
            'request_for_payments' => RequestForPayment::with('requestForPaymentDetails', 'type', 'user', 'approver1', 'approver2', 'approver3', 'status', 'checkVouchers')
                ->whereHas('budget', function ($query) {
                    $query->when($this->division, function ($query) {
                        $query->where('division_id', $this->division);
                    });
                })
                ->whereHas('user', function ($query) {
                    $query->when($this->requester, function ($query) {
                        $query->where('name', 'like', '%' . $this->requester . '%');
                    });
                })
                ->when($this->reference_no, function ($query) {
                    $query->where('reference_id', 'like', '%' . $this->reference_no . '%');
                })
                ->when($this->request_type, function ($query) {
                    $query->where('type_id', $this->request_type);
                })
                ->when($this->payee, function ($query) {
                    $query->whereHas('payee', function ($query) {
                        $query->where('company', 'like', '%' . $this->payee . '%');
                    });
                })
                ->when($this->opex_type, function ($query) {
                    $query->where('opex_type_id', 'like', '%' . $this->opex_type . '%');
                })
                ->when($this->status, function ($query) {
                    $query->where('status_id', $this->status);
                })
                ->when($this->date_created, function ($query) {
                    $query->whereDate('created_at', '>=', $this->date_created);
                })
                ->when($this->date_created_to, function ($query) {
                    $query->whereDate('created_at', '<=', $this->date_created_to);
                })
                ->when($this->date_needed, function ($query) {
                    $query->whereDate('date_needed', '>=', $this->date_needed);
                })
                ->when($this->date_needed_to, function ($query) {
                    $query->whereDate('date_needed', '<=', $this->date_needed_to);
                })
                ->when($this->description, function ($query) {
                    $query->where('description', 'like', '%' . $this->description . '%');
                })
                ->when($this->priority, function ($query) {
                    $query->where('priority_id', $this->priority);
                })
                ->when(Auth::user()->level_id <= 3, function ($query) {
                    $query->when(!$this->approver, function ($query) {
                        $query->whereRaw("(approver_1_id = " . Auth::user()->id . " OR approver_2_id = " . Auth::user()->id . " OR approver_3_id = " . Auth::user()->id . ")");
                    })
                        ->when($this->approver == 1, function ($query) {
                            $query->where('approver_1_id', Auth::user()->id);
                        })
                        ->when($this->approver == 2, function ($query) {
                            $query->where('approver_2_id', Auth::user()->id);
                        })
                        ->when($this->approver == 3, function ($query) {
                            $query->where('approver_3_id', Auth::user()->id);
                        });
                })
                ->when(Auth::user()->level_id == 4, function ($query) {
                    $query->when(!$this->approver, function ($query) {
                        $query->whereRaw("(approver_1_id = " . Auth::user()->id . " OR approver_2_id = " . Auth::user()->id . " OR approver_3_id = " . Auth::user()->id . ")");
                    })
                        ->when($this->approver == 1, function ($query) {
                            $query->whereHas('approver1', function ($query) {
                                $query->where('division_id', Auth::user()->division_id);
                            })
                                ->when($this->user_id, function ($query) {
                                    $query->where([
                                        ['is_approved_1', null],
                                        ['approver_1_id', $this->user_id]
                                    ]);
                                });;
                        })
                        ->when($this->approver == 2, function ($query) {
                            $query->whereHas('approver2', function ($query) {
                                $query->where('division_id', Auth::user()->division_id);
                            })
                                ->when($this->user_id, function ($query) {
                                    $query->where([
                                        ['is_approved_2', null],
                                        ['approver_2_id', $this->user_id]
                                    ]);
                                });
                        })
                        ->when($this->approver == 3, function ($query) {
                            $query->whereHas('approver3', function ($query) {
                                $query->where('division_id', Auth::user()->division_id);
                            })
                                ->when($this->user_id, function ($query) {
                                    $query->where([
                                        ['is_approved_3', null],
                                        ['approver_3_id', $this->user_id]
                                    ]);
                                });;
                        });
                })
                ->when(Auth::user()->level_id == 5, function ($query) {
                    $query->when(!$this->approver, function ($query) {
                        $query->when($this->user_id, function ($query) {
                            $query->whereRaw("(`is_approved_1` is null and `approver_1_id` = " . $this->user_id . " 
                            or `is_approved_2` is null and `approver_2_id` = " . $this->user_id . " 
                            or `is_approved_3` is null and `approver_3_id` = " . $this->user_id . ")");
                        });
                    })
                        ->when($this->approver == 1, function ($query) {
                            $query->where([
                                ['is_approved_1', null],
                                ['approver_1_id', $this->user_id]
                            ]);
                        })
                        ->when($this->approver == 2, function ($query) {
                            $query->where([
                                ['is_approved_2', null],
                                ['approver_2_id', $this->user_id]
                            ]);
                        })
                        ->when($this->approver == 3, function ($query) {
                            $query->where([
                                ['is_approved_3', null],
                                ['approver_3_id', $this->user_id]
                            ]);
                        });
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)

        ]);
    }
}
