<?php

namespace App\Http\Livewire\Accounting\LiquidationManagement;

use App\Interfaces\Accounting\LiquidationManagementInterface;
use App\Traits\Accounting\LiquidationManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use LiquidationManagementTrait, WithFileUploads, PopUpMessagesTrait;

    protected $listeners = ['generate_liquidation_reference_number' => 'generateLiquidationReferenceNumber'];

    public function mount()
    {
        $this->addLiquidationDetail();
    }

    public function submit(LiquidationManagementInterface $liquidation_management_interface)
    {
        $request = [
            'liquidation_reference_no' => $this->liquidation_reference_no,
            'status' => $this->status,
            'request_for_payments' => $this->request_for_payments,
            'liquidation_details' => $this->liquidation_details,
        ];

        $response = $liquidation_management_interface->create($request);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.liquidation-management.index', 'close_modal', 'create');
            $this->emitTo('accounting.liquidation-management.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.liquidation-management.create');
    }
}
