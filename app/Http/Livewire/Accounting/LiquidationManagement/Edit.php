<?php

namespace App\Http\Livewire\Accounting\LiquidationManagement;

use App\Interfaces\Accounting\LiquidationManagementInterface;
use App\Models\Accounting\Liquidation;
use App\Traits\Accounting\LiquidationManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use LiquidationManagementTrait, WithFileUploads, PopUpMessagesTrait;

    protected $listeners = ['liquidation_management_edit_mount' => 'mount'];

    public function mount(LiquidationManagementInterface $liquidation_management_interface, $id)
    {
        $this->resetForm();
        $response = $liquidation_management_interface->show($id);
        if ($response['code'] != 200) return $this->sweetAlertError('error', $response['message'], $response['result']);

        $this->liquidation = $response['result'];
        $this->liquidation_reference_no = $this->liquidation->liquidation_reference_no;
        $this->liquidation_total_amount = $this->liquidation->liquidation_total_amount;

        $this->status = $this->liquidation->status_id;

        foreach ($this->liquidation->requestForPayments as $request_for_payment) {
            $this->request_for_payments[] = [
                'id' => $request_for_payment->id,
                'request_for_payment_id' => $request_for_payment->requestForPayment->id,
                'reference_id' => $request_for_payment->requestForPayment->reference_id,
                'pay_to' => $request_for_payment->requestForPayment->payee->company,
                'opex_type' => $request_for_payment->requestForPayment->opex->name,
                'requested_by' => $request_for_payment->requestForPayment->user->name,
                'amount' => $request_for_payment->requestForPayment->amount,
                'is_deleted' => false,
            ];
        }

        $this->loadRequestForPaymentReference();

        foreach ($this->liquidation->liquidationDetails as $a => $liquidation_detail) {
            $this->liquidation_details[] = [
                'id' => $liquidation_detail->id,
                'or_no' => $liquidation_detail->or_no,
                'transaction_date' => $liquidation_detail->transaction_date,
                'amount' => $liquidation_detail->amount,
                'cash_on_hand' => $liquidation_detail->cash_on_hand,
                'description' => $liquidation_detail->description,
                'remarks' => $liquidation_detail->remarks,
                'total_amount' => 0,
                'is_deleted' => false,

                'attachments' => [],
                'request_for_payments' => []
            ];

            foreach ($liquidation_detail->attachments as $attachment) {
                $this->liquidation_details[$a]['attachments'][] = [
                    'id' => $attachment->id,
                    'attachment' => '',
                    'path' => $attachment->path,
                    'name' => $attachment->name,
                    'extension' => $attachment->extension,
                    'is_deleted' => false,
                ];
            }

            foreach ($liquidation_detail->requestForPayments as $request_for_payment) {
                $this->liquidation_details[$a]['request_for_payments'][] = [
                    'id' => $request_for_payment->id,
                    'request_for_payment_id' => $request_for_payment->requestForPayment->id,
                    'reference_id' => $request_for_payment->requestForPayment->reference_id,
                    'pay_to' => $request_for_payment->requestForPayment->payee->company,
                    'opex_type' => $request_for_payment->requestForPayment->opex->name,
                    'requested_by' => $request_for_payment->requestForPayment->user->name,
                    'amount' => $request_for_payment->requestForPayment->amount,
                    'is_deleted' => false,
                ];
            }
        }
    }

    public function submit(LiquidationManagementInterface $liquidation_management_interface)
    {
        $request = [
            'liquidation_reference_no' => $this->liquidation_reference_no,
            'status' => $this->status,
            'request_for_payments' => $this->request_for_payments,
            'liquidation_details' => $this->liquidation_details,
        ];

        $response = $liquidation_management_interface->update($this->liquidation->id, $request);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.liquidation-management.index', 'close_modal', 'edit');
            $this->emitTo('accounting.liquidation-management.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.liquidation-management.edit');
    }
}
