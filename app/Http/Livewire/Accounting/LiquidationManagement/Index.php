<?php

namespace App\Http\Livewire\Accounting\LiquidationManagement;

use App\Interfaces\Accounting\LiquidationManagementInterface;
use App\Models\Accounting\LiquidationStatusReference;
use App\Models\Accounting\OpexType;
use App\Models\Division;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $confirmation_modal = false;
    public $confirmation_message;

    public $create_modal = false;
    public $edit_modal = false;
    public $liquidation_id;
    public $status_id;

    public $view_modal = false;
    public $parent_reference_no;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $division;
    public $rfp_reference_no;
    public $cv_no;
    public $liquidation_reference_no;
    public $payee;
    public $opex_type;
    public $rfp_requester;
    public $liquidated_by;
    public $status;

    public $division_references = [];
    public $opex_type_references = [];
    public $liquidation_status_references = [];

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadDivisionReference();
        $this->loadOpexTypeReference();
        $this->loadLiquidationStatusReference();
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function loadOpexTypeReference()
    {
        $this->opex_type_references = OpexType::get();
    }

    public function loadLiquidationStatusReference()
    {
        $this->liquidation_status_references = LiquidationStatusReference::active()->get();
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'create') {
            $this->emit('generate_liquidation_reference_number');
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('liquidation_management_edit_mount', $data['id']);
            $this->edit_modal = true;
        } else if ($action_type == 'update_liquidation_status') {
            if ($data['status_id'] == 3) {
                $this->confirmation_message = "Change the status to CLOSED this liquidation?";
            } else if ($data['status_id'] == 2) {
                $this->confirmation_message = "Change the status to EQUALED this liquidation?";
            }
            $this->confirmation_modal = true;
            $this->status_id = $data['status_id'];
        } else if ($action_type == 'view') {
            $this->emit('request_management_view_mount', $data['parent_reference_no']);
            $this->view_modal = true;
            $this->parent_reference_no = $data['parent_reference_no'] ?? null;
        }

        $this->liquidation_id = $data['id'] ?? null;
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } else if ($action_type == 'view') {
            $this->view_modal = false;
        }
    }

    public function updateLiquidationStatus(LiquidationManagementInterface $liquidation_management_interface)
    {
        $response = $liquidation_management_interface->updateStatus($this->liquidation_id, $this->status_id);

        if ($response['code'] == 200) {
            $this->emit('liquidation_management_index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render(LiquidationManagementInterface $liquidation_management_interface)
    {
        $request = [
            'division' => $this->division,
            'rfp_reference_no' => $this->rfp_reference_no,
            'cv_no' => $this->cv_no,
            'liquidation_reference_no' => $this->liquidation_reference_no,
            'payee' => $this->payee,
            'opex_type' => $this->opex_type,
            'rfp_requester' => $this->rfp_requester,
            'liquidated_by' => $this->liquidated_by,
            'status' => $this->status,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $liquidation_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.accounting.liquidation-management.index', [
            'liquidations' => $response['result']
        ]);
    }
}
