<?php

namespace App\Http\Livewire\Accounting\SubAccounts;

use App\Interfaces\Fims\DataManagement\SubAccountsInterface;
use App\Models\FimsSubAccounts;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $confirmation_modal;
    
    public $subAccountId;

    public $subAccounts_name;
    public $acct_code;

    public function mount($id)
    {
        $this->subAccountId = $id;
        $subAccounts_name_lists = FimsSubAccounts::findOrFail($id);
        
        $this->subAccounts_name = $subAccounts_name_lists['name'];
        $this->acct_code = $subAccounts_name_lists['account_code'];
    }

    public function load(){
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.sub-accounts.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(SubAccountsInterface $subAccounts_interface)
    {
        $response = $subAccounts_interface->updateValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(SubAccountsInterface $subAccounts_interface)
    {
        $response = $subAccounts_interface->update2($this->getRequest(), $this->subAccountId);

        if ($response['code'] == 200) {
            $this->emitTo('accounting.sub-accounts.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'subAccounts_name' => $this->subAccounts_name,
            'acct_code' => $this->acct_code,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'subAccounts_name',
            'acct_code',
        ]);
    }
    public function render()
    {
        return view('livewire.accounting.sub-accounts.edit');
    }
}
