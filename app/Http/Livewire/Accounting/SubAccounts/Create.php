<?php

namespace App\Http\Livewire\Accounting\SubAccounts;

use App\Interfaces\Fims\DataManagement\SubAccountsInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{

    use PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function mount()
    {
    }
    public $confirmation_modal;
    public $subAccounts_name;
    public $acct_code;

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.sub-accounts.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(SubAccountsInterface $subAccounts_interface)
    {
        $response = $subAccounts_interface->createValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function submit(SubAccountsInterface $subAccounts_interface)
    {
        $response = $subAccounts_interface->create2($this->getRequest());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.sub-accounts.index', 'close_modal', 'create');
            $this->emitTo('accounting.sub-accounts.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'subAccounts_name' => $this->subAccounts_name,
            'acct_code' => $this->acct_code,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'subAccounts_name',
            'acct_code',
        ]);
    }
    public function render()
    {
        return view('livewire.accounting.sub-accounts.create');
    }
}
