<?php

namespace App\Http\Livewire\Accounting\SubAccounts;

use App\Interfaces\Fims\DataManagement\SubAccountsInterface;
use App\Models\FimsSubAccounts;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $paginate = 50;
    public $stats;

    public $create_modal = false;
    public $edit_modal = false;
    public $reactivate_modal  = false;
    public $deactivate_modal  = false;
    public $action_type;

    public $subAccountsId;

    public $search_request;
    public $status_header_cards = [];

    public $name;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'add') {
            $this->create_modal = true;
        } 
        elseif ($action_type == 'edit') {
            $this->subAccountsId = $data['id'];
            $this->edit_modal = true;
        } 
        else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->reactivate_modal = true;
                $this->subAccountsId = $data['id'];
                return;
            }
            $this->deactivate_modal = true;
            $this->subAccountsId = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } 
        else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } 
        elseif ($action_type == 'update_status') {
            $this->reactivate_modal = false;
            $this->deactivate_modal = false;
        }
    }

    public function load()
    {
        $this->loadStatusHeaderCards();
        $this->stats = false;
    }

    public function loadStatusHeaderCards()
    {

        $getAll = FimsSubAccounts::get();
        $getActive = FimsSubAccounts::where('status', 1)->get();
        $getDeactivated = FimsSubAccounts::where('status', 2)->get();

        $statusall = $getAll->count();
        $statusactive = $getActive->count();
        $statusdeactivated = $getDeactivated->count();

        $this->status_header_cards = [

            [
                'title' => 'ALL',
                'value' => $statusall,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => false,
            ],
            [
                'title' => 'Active',
                'value' => $statusactive,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 1,
            ],
            [
                'title' => 'Deactivated',
                'value' => $statusdeactivated,
                'class' => 'bg-white border border-gray-400 shadow-sm text-sm ',
                'color' => 'text-blue',
                'action' => 'stats',
                'id' => 2
            ],
        ];
    }

    public function updateStatus($id, $value)
    {
        $unit = FimsSubAccounts::findOrFail($id);

        $unit->update([
            'status' => $value,
        ]);


        if ($value == 1) {
            $this->emitTo('accounting.sub-accounts.index', 'close_modal', 'update_status');
            $this->emitTo('accounting.sub-accounts.index', 'index');
            $this->sweetAlert('', 'Unit has been Successfully Reactivated!');
            return;
        }

        $this->emitTo('accounting.sub-accounts.index', 'close_modal', 'update_status');
        $this->emitTo('accounting.sub-accounts.index', 'index');
        $this->sweetAlert('', 'Unit has been Successfully Deactivated!');
    }

    public function search(SubAccountsInterface $subAccounts_list)
    {
        $this->search_request = [
            'name' => $this->name,
        ];
    }

    public function render(SubAccountsInterface $subAccounts_list)
    {
        $this->loadStatusHeaderCards();
        $request = [
            'paginate' => $this->paginate,
            'status' => $this->stats,
        ];

        $response = $subAccounts_list->index2($request, $this->search_request);

        // dd($response['result']);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'subAccounts_lists' => [],
                ];
        }
        return view('livewire.accounting.sub-accounts.index', [
            'subAccounts_lists' => $response['result']['list']
        ]);
    }
    // public function render()
    // {
    //     return view('livewire.accounting.sub-accounts.index');
    // }
}
