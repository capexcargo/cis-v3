<?php

namespace App\Http\Livewire\Accounting\SupplierManagement\Supplier;

use App\Interfaces\Accounting\SupplierManagement\SupplierInterface;
use App\Traits\Accounting\SupplierManagement\SupplierTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use SupplierTrait, PopUpMessagesTrait;

    public function submit(SupplierInterface $SupplierInterface)
    {
        $validated = $this->validate([
            'company' => 'required|unique:accounting_supplier,company',
            'industry' => 'required',
            'type' => 'required',
            'branch' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email:rfc,dns',
            'mobile_number' => 'required|numeric|digits:11',
            'telephone_number' => 'required|numeric|digits_between:8, 10',
            'address' => 'required',
            'terms' => 'required',
        ]);

        $response = $SupplierInterface->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.supplier-management.supplier.index', 'close_modal', 'create');
            $this->emitTo('accounting.supplier-management.supplier.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.supplier-management.supplier.create');
    }
}
