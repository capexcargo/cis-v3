<?php

namespace App\Http\Livewire\Accounting\SupplierManagement\Supplier;

use App\Models\Accounting\Supplier;
use App\Models\Accounting\SupplierIndustryReference;
use App\Models\Accounting\SupplierTypeReference;
use App\Models\BranchReference;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $create_modal = false;
    public $edit_modal = false;
    public $supplier_id;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $industry;
    public $type;
    public $branch;
    public $company;
    public $email;

    public $supplier_industry_references = [];
    public $supplier_type_references = [];
    public $branch_references = [];

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadSupplierIndustryReference();
        $this->loadSupplierTypeReference();
        $this->loadBranchReference();
    }

    public function loadSupplierIndustryReference()
    {
        $this->supplier_industry_references = SupplierIndustryReference::get();
    }

    public function loadSupplierTypeReference()
    {
        $this->supplier_type_references = SupplierTypeReference::get();
    }

    public function loadBranchReference()
    {
        $this->branch_references = BranchReference::get();
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emit('suppplier_edit_mount', $data['id']);
            $this->edit_modal = true;
        }

        $this->supplier_id = $data['id'];
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.supplier-management.supplier.index', [
            'suppliers' => Supplier::with('industry', 'type', 'branch', 'items')
                ->where('company', 'like', '%' . $this->company . '%')
                ->where('email', 'like', '%' . $this->email . '%')
                ->when($this->industry, function ($query) {
                    $query->where('industry_id', $this->industry);
                })
                ->when($this->type, function ($query) {
                    $query->where('type_id', $this->type);
                })
                ->when($this->branch, function ($query) {
                    $query->where('branch_id', $this->branch);
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)

        ]);
    }
}
