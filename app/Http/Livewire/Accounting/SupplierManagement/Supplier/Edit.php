<?php

namespace App\Http\Livewire\Accounting\SupplierManagement\Supplier;

use App\Interfaces\Accounting\SupplierManagement\SupplierInterface;
use App\Models\Accounting\Supplier;
use App\Models\Accounting\SupplierItem;
use App\Traits\Accounting\SupplierManagement\SupplierTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use WithPagination, SupplierTrait, PopUpMessagesTrait;

    public $create_modal = false;
    public $edit_modal = false;

    public $name;

    public $supplier;
    public $supplier_item;

    public $paginate = 3;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $search;

    protected $listeners = ['suppplier_item_edit_index' => 'render', 'suppplier_edit_mount' => 'mount', 'close_modal' => 'closeModal'];

    public function mount($id)
    {
        $this->resetForm();
        $this->supplier  = Supplier::with('items')->findOrFail($id);

        $this->company = $this->supplier->company;
        $this->industry = $this->supplier->industry_id;
        $this->type = $this->supplier->type_id;
        $this->branch = $this->supplier->branch_id;
        $this->first_name = $this->supplier->first_name;
        $this->middle_name = $this->supplier->middle_name;
        $this->last_name = $this->supplier->last_name;
        $this->email = $this->supplier->email;
        $this->mobile_number = $this->supplier->mobile_number;
        $this->telephone_number = $this->supplier->telephone_number;
        $this->address = $this->supplier->address;
        $this->terms = $this->supplier->terms;
    }

    public function action(array $data, $action_type)
    {
        $this->reset('name');
        if ($action_type == 'edit') {
            $this->supplier_item = $this->supplier->items()->find($data['id']);
            if (!$this->supplier_item) {
                abort(404);
            }
            $this->name = $this->supplier_item->name;
            $this->edit_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function submit(SupplierInterface $SupplierInterface)
    {
        $validated = $this->validate([
            'company' => 'required|unique:accounting_supplier,company,' . $this->supplier->id . ',id',
            'industry' => 'required',
            'type' => 'required',
            'branch' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email:rfc,dns',
            'mobile_number' => 'required|numeric|digits:11',
            'telephone_number' => 'required|numeric|digits_between:8, 10',
            'address' => 'required',
            'terms' => 'required',
        ]);

        $response = $SupplierInterface->update($this->supplier, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.supplier-management.supplier.index', 'close_modal', 'edit');
            $this->emitTo('accounting.supplier-management.supplier.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function createItem(SupplierInterface $SupplierInterface)
    {
        $validated = $this->validate([
            'name' => 'required'
        ]);

        $response = $SupplierInterface->createItem($this->supplier, $validated);

        if ($response['code'] == 200) {
            $this->reset([
                'name'
            ]);
            $this->emitSelf('suppplier_item_edit_index');
            session()->flash('message', 'Item Successfully Created.');
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }

        $this->closeModal('create');
    }

    public function editItem(SupplierInterface $SupplierInterface)
    {
        $validated = $this->validate([
            'name' => 'required'
        ]);

        $response = $SupplierInterface->updateItem($this->supplier, $this->supplier_item, $validated);

        if ($response['code'] == 200) {
            $this->reset([
                'name'
            ]);
            $this->emitSelf('suppplier_item_edit_index');
            session()->flash('message', 'Item Successfully Updated.');
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }

        $this->closeModal('edit');
    }


    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.supplier-management.supplier.edit', [
            'items' => SupplierItem::where('supplier_id', $this->supplier->id)
                ->where('name', 'like', '%' . $this->search . '%')
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)

        ]);
    }
}
