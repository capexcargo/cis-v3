<?php

namespace App\Http\Livewire\Accounting\SupplierManagement\SupplierIndustry;

use App\Interfaces\Accounting\SupplierManagement\SupplierInterface;
use App\Models\Accounting\SupplierIndustryReference;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use PopUpMessagesTrait;

    public $name;

    public $industry;

    protected $listeners = ['suppplier_industry_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->industry = SupplierIndustryReference::findOrFail($id);

        $this->name = $this->industry->display;
    }

    public function submit(SupplierInterface $SupplierInterface)
    {
        $validated = $this->validate([
            'name' => 'required|unique:accounting_supplier_industry_reference,display,' . $this->industry->id . ',id',
        ]);

        $response = $SupplierInterface->updateIndustry($this->industry, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.supplier-management.supplier-industry.index', 'close_modal', 'edit');
            $this->emitTo('accounting.supplier-management.supplier-industry.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            "name"
        ]);
    }

    public function render()
    {
        return view('livewire.accounting.supplier-management.supplier-industry.edit');
    }
}
