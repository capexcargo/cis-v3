<?php

namespace App\Http\Livewire\Accounting\SupplierManagement\SupplierIndustry;

use App\Interfaces\Accounting\SupplierManagement\SupplierInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use PopUpMessagesTrait;

    public $name;

    public function submit(SupplierInterface $SupplierInterface)
    {
        $validated = $this->validate([
            'name' => 'required|unique:accounting_supplier_industry_reference,display',
        ]);

        $response = $SupplierInterface->createIndustry($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.supplier-management.supplier-industry.index', 'close_modal', 'create');
            $this->emitTo('accounting.supplier-management.supplier-industry.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            "name"
        ]);
    }

    public function render()
    {
        return view('livewire.accounting.supplier-management.supplier-industry.create');
    }
}
