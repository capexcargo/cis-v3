<?php

namespace App\Http\Livewire\Accounting\ServiceCategory;

use App\Interfaces\Fims\DataManagement\ServiceCategoryInterface;
use App\Models\FimsBudgetManagement;
use App\Models\FimsBudgetSource;
use App\Models\FimsChartsOfAccounts;
use App\Models\FimsOpexCategory;
use App\Models\FimsSubAccounts;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function mount()
    {
    }
    public $confirmation_modal;
    public $service_category;
    public $recurring;
    public $budget_source;
    public $opex_category;
    public $opex_type;
    public $chart_of_accounts;
    public $sub_accounts;

    public $budget_source_references = [];
    public $opex_category_references = [];
    public $opex_type_references = [];
    public $chart_of_accounts_references = [];
    public $sub_accounts_references = [];

    public function BudgetSourceReference()
    {
        $this->budget_source_references = FimsBudgetSource::get();
    }

    public function OpexCategoryReference()
    {
        $this->opex_category_references = FimsOpexCategory::get();
    }

    public function UpdatedOpexCategory()
    {
        $this->opex_type_references = FimsOpexCategory::where('id', $this->opex_category)->get();
        foreach ($this->opex_type_references as $opex_type_reference){
        $this->opex_type = $opex_type_reference->id;
        }
    }

    public function ChartOfAccountsReference()
    {
        $this->chart_of_accounts_references = FimsChartsOfAccounts::get();
    }

    public function SubAccountsReference()
    {
        $this->sub_accounts_references = FimsSubAccounts::get();
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.service-category.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(ServiceCategoryInterface $servicecat_interface)
    {
        $response = $servicecat_interface->createValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function submit(ServiceCategoryInterface $servicecat_interface)
    {
        $response = $servicecat_interface->create2($this->getRequest());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.service-category.index', 'close_modal', 'create');
            $this->emitTo('accounting.service-category.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'service_category' => $this->service_category,
            'recurring' => $this->recurring,
            'budget_source' => $this->budget_source,
            'opex_category' => $this->opex_category,
            'opex_type' => $this->opex_type,
            'chart_of_accounts' => $this->chart_of_accounts,
            'sub_accounts' => $this->sub_accounts,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'service_category',
            'recurring',
            'budget_source',
            'opex_category',
            'opex_type',
            'chart_of_accounts',
            'sub_accounts',
        ]);
    }
    public function render()
    {
        return view('livewire.accounting.service-category.create');
    }
}
