<?php

namespace App\Http\Livewire\Accounting\ServiceCategory;

use App\Interfaces\Fims\DataManagement\ServiceCategoryInterface;
use App\Models\FimsBudgetManagement;
use App\Models\FimsBudgetSource;
use App\Models\FimsChartsOfAccounts;
use App\Models\FimsOpexCategory;
use App\Models\FimsServiceCategory;
use App\Models\FimsSubAccounts;
use App\Traits\InitializeFirestoreTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $confirmation_modal;
    
    public $servCatId;

    public $service_category;
    public $recurring;
    public $budget_source;
    public $opex_category;
    public $opex_type;
    public $chart_of_accounts;
    public $sub_accounts;

    public $budget_source_references = [];
    public $opex_category_references = [];
    public $opex_type_references = [];
    public $chart_of_accounts_references = [];
    public $sub_accounts_references = [];

    public function mount($id)
    {
        $this->servCatId = $id;
        $service_category_lists = FimsServiceCategory::with('ScbudgetSrc','ScopexCat','ScchartOfAccs','ScsubAccs')->findOrFail($id);
        
        $this->service_category = $service_category_lists['name'];
        $this->recurring = $service_category_lists['is_recurring'];
        $this->budget_source = $service_category_lists['ScbudgetSrc']['id'];
        $this->opex_category = $service_category_lists['ScopexCat']['id'];
        $this->opex_type = $service_category_lists['ScopexCat']['id'];
        $this->chart_of_accounts = $service_category_lists['ScchartOfAccs']['id'];
        $this->sub_accounts = $service_category_lists['ScsubAccs']['id'];
    }

    public function load(){
        $this->BudgetSourceReference();
        $this->OpexCategoryReference();
        $this->UpdatedOpexCategory();
        $this->ChartOfAccountsReference();
        $this->SubAccountsReference();
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.service-category.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function BudgetSourceReference()
    {
        $this->budget_source_references = FimsBudgetSource::get();
    }

    public function OpexCategoryReference()
    {
        $this->opex_category_references = FimsOpexCategory::get();
    }

    public function UpdatedOpexCategory()
    {
        $this->opex_type_references = FimsOpexCategory::where('id', $this->opex_category)->get();
        foreach ($this->opex_type_references as $opex_type_reference){
        $this->opex_type = $opex_type_reference->id;
        }
    }

    public function ChartOfAccountsReference()
    {
        $this->chart_of_accounts_references = FimsChartsOfAccounts::get();
    }

    public function SubAccountsReference()
    {
        $this->sub_accounts_references = FimsSubAccounts::get();
    }

    public function confirmationSubmit(ServiceCategoryInterface $servicecat_interface)
    {
        $response = $servicecat_interface->updateValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(ServiceCategoryInterface $servicecat_interface)
    {
        $response = $servicecat_interface->update2($this->getRequest(), $this->servCatId);

        if ($response['code'] == 200) {
            $this->emitTo('accounting.service-category.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'service_category' => $this->service_category,
            'recurring' => $this->recurring,
            'budget_source' => $this->budget_source,
            'opex_category' => $this->opex_category,
            'opex_type' => $this->opex_type,
            'chart_of_accounts' => $this->chart_of_accounts,
            'sub_accounts' => $this->sub_accounts,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'service_category',
            'recurring',
            'budget_source',
            'opex_category',
            'opex_type',
            'chart_of_accounts',
            'sub_accounts',
        ]);
    }

    public function render()
    {
        return view('livewire.accounting.service-category.edit');
    }
}
