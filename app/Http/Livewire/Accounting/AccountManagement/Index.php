<?php

namespace App\Http\Livewire\Accounting\AccountManagement;

use App\Models\AccountStatusReference;
use App\Models\Division;
use App\Models\DivisionRolesReference;
use App\Models\User;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $create_modal = false;
    public $edit_modal = false;
    public $reactivate_modal = false;
    public $deactivate_modal = false;
    public $user_id;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $name;
    public $role = false;
    public $status;

    public $account_status_references = [];
    public $division_roles_references = [];
    public $header_cards = [];

    protected $listeners = ['index' => 'render', 'load_head_cards' => 'loadHeadCards', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadHeadCards();
        $this->loadDivisionRolesReferece();
        $this->loadAccountStatusReference();
    }

    public function loadHeadCards()
    {
        $division_roles = Division::with(['roles' => function ($query) {
            $query->withCount('users');
        }])->find(6);

        $this->header_cards = [
            [
                'title' => 'All',
                'value' => $division_roles->roles->sum('users_count'),
                'action' => 'role',
                'id' => false
            ],
        ];

        foreach ($division_roles->roles as $role) {
            $this->header_cards[] = [
                'title' => $role->display,
                'value' => $role->users_count,
                'action' => 'role',
                'id' => $role->id
            ];
        }
    }

    public function loadDivisionRolesReferece()
    {
        $this->division_roles_references = DivisionRolesReference::where([
            ['is_visible', 1],
            ['division_id', 6]
        ])->get();
    }

    public function loadAccountStatusReference()
    {
        $this->account_status_references = AccountStatusReference::where('is_visible', 1)->get();
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emit('account_management_edit_mount', $data['id']);
            $this->edit_modal = true;
        } else if ($action_type == 'update_status') {
            if ($data['status_id'] == 1) {
                $this->reactivate_modal = true;
                return;
            }

            $this->deactivate_modal = true;
        }

        $this->user_id = $data['id'];
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function updated()
    {
        $this->resetPage();
    }

    public function updateStatus($id, $value)
    {
        $user = User::findOrFail($id);
        $user->update([
            'status_id' => $value
        ]);

        $this->emit('load_head_cards');

        if ($value == 1) {
            $this->sweetAlert('success', 'Account Successfully Reactivated!');
            return;
        }

        $this->sweetAlert('success', 'Account Successfully Deactivated!');
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.account-management.index', [
            'users' => User::with('division', 'status', 'role', 'userDetails')
                ->where('name', 'like', '%' . $this->name . '%')
                ->where('division_id', 6)
                ->when($this->status, function ($query) {
                    $query->where('status_id', $this->status);
                })
                ->when($this->role, function ($query) {
                    $query->where('role_id', $this->role);
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)

        ]);
    }
}
