<?php

namespace App\Http\Livewire\Accounting\AccountManagement;

use App\Interfaces\Globals\AccountManagementInterface;
use App\Models\User;
use App\Traits\Accounting\AccountManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use AccountManagementTrait, PopUpMessagesTrait;

    public $user;

    protected $listeners = ['account_management_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->user = User::findOrFail($id);
        $this->employee_number = $this->user->userDetails->employee_number;
        $this->first_name = $this->user->userDetails->first_name;
        $this->middle_name = $this->user->userDetails->middle_name;
        $this->last_name = $this->user->userDetails->last_name;
        $this->mobile_number = $this->user->userDetails->company_mobile_number;
        $this->telephone_number = $this->user->userDetails->company_telephone_number;
        $this->suffix = $this->user->userDetails->suffix_id;
        $this->role = $this->user->role_id;
        $this->level = $this->user->level_id;
        $this->email = $this->user->email;

        $this->loadRoles();
    }

    public function submit(AccountManagementInterface $AccountManagementInterface)
    {
        $validated = $this->validate([
            'employee_number' => 'required',
            'first_name' => 'required',
            'middle_name' => 'sometimes',
            'last_name' => 'required',
            'mobile_number' => 'required|numeric|digits:11',
            'telephone_number' => 'nullable|numeric|digits_between:8, 10',
            'suffix' => 'sometimes',
            'level' => 'required',
            'role' => 'required',
            'email' => 'required|email:rfc,dns|unique:users,email,' . $this->user->id . ',id',
        ]);

        $validated_password = [];

        if ($this->password) {
            $validated_password = $this->validate([
                'password' => 'required|confirmed|string|min:8|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/',
                'password_confirmation' => 'required',
            ], [
                'password.regex' => 'Password must be contain at least one uppercase and lowercase letter, one number and one special character'
            ]);
        }

        $response = $AccountManagementInterface->update($this->user, $validated, $validated_password, 6);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.account-management.index', 'close_modal', 'edit');
            $this->emitTo('accounting.account-management.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.account-management.edit');
    }
}
