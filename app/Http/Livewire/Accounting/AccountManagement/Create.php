<?php

namespace App\Http\Livewire\Accounting\AccountManagement;

use App\Interfaces\Globals\AccountManagementInterface;
use App\Traits\Accounting\AccountManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use AccountManagementTrait, PopUpMessagesTrait;

    public function submit(AccountManagementInterface $AccountManagementInterface)
    {
        $validated = $this->validate([
            'employee_number' => 'required',
            'first_name' => 'required',
            'middle_name' => 'sometimes',
            'last_name' => 'required',
            'mobile_number' => 'required|numeric|digits:11',
            'telephone_number' => 'nullable|numeric|digits_between:8, 10',
            'suffix' => 'sometimes',
            'level' => 'required',
            'role' => 'required',
            'email' => 'required|email:rfc,dns|unique:users,email',
            'password' => 'required|confirmed|string|min:8|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/',
            'password_confirmation' => 'required',
        ], [
            'password.regex' => 'Password must be contain at least one uppercase and lowercase letter, one number and one special character'
        ]);

        $response = $AccountManagementInterface->create($validated, 6);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.account-management.index', 'close_modal', 'create');
            $this->emitTo('accounting.account-management.index','index');
            $this->sweetAlert('success', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.account-management.create');
    }
}
