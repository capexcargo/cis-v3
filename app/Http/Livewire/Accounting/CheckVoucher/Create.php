<?php

namespace App\Http\Livewire\Accounting\CheckVoucher;

use App\Interfaces\Accounting\CheckVoucherInterface;
use App\Traits\Accounting\CheckVoucherTrait;
use App\Models\Accounting\RequestForPayment;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use CheckVoucherTrait, PopUpMessagesTrait;

    protected $listeners = ['check_voucher_create_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->request_for_payment = RequestForPayment::with('budget', 'payee', 'opex', 'requestForPaymentDetails', 'attachments', 'checkVoucher')->find($id);
        abort_if(!$this->request_for_payment, 404);
        $this->total_amount = $this->request_for_payment->amount;

        if ($this->request_for_payment->checkVoucher) {
            $this->reference_no = $this->request_for_payment->checkVoucher->reference_no;
            $this->cv_no = $this->request_for_payment->checkVoucher->cv_no;
            $this->description = $this->request_for_payment->checkVoucher->description;
            foreach ($this->request_for_payment->checkVoucher->checkVoucherDetails as $check_voucher_detail) {
                $this->vouchers[] = [
                    'id' => $check_voucher_detail->id,
                    'voucher_no' => $check_voucher_detail->voucher_no,
                    'description' => $check_voucher_detail->description,
                    'date' => $check_voucher_detail->date,
                    'amount' => $check_voucher_detail->amount,
                    'is_deleted' => false,
                ];
            }
            $this->updatedVouchers();
        } else {
            $this->generateReferenceNumber();
            $this->generateCVNumber();
            $this->description = $this->request_for_payment->description;
        }

        $this->getSupplier($this->request_for_payment->payee_id);
    }

    public function submit(CheckVoucherInterface $check_voucher_interface)
    {
        $this->updatedVouchers();
        $rules = [
            'reference_no' => 'required',
            'cv_no' => 'required',
            'pay_to' => 'required',
            'description' => 'required',

            'vouchers' => 'required',
        ];

        foreach ($this->vouchers as $i => $voucher) {
            $rules['vouchers.' . $i . '.id'] = 'sometimes';
            $rules['vouchers.' . $i . '.date'] = 'required';
            $rules['vouchers.' . $i . '.voucher_no'] = 'required|unique:accounting_check_voucher_details,voucher_no,' . $voucher['id'] . ',id';
            $rules['vouchers.' . $i . '.description'] = 'required';
            $rules['vouchers.' . $i . '.amount'] = 'required';
            $rules['vouchers.' . $i . '.is_deleted'] = 'sometimes';
        }

        $validated = $this->validate(
            $rules,
            [
                'vouchers.*.date.required' => 'Check Date is required',
                'vouchers.*.voucher_no.required' => 'Check No is required',
                'vouchers.*.description.required' => 'Description is required',
                'vouchers.*.amount.required' => 'Check Amount is required',
                'vouchers.*.voucher_no.unique' => 'Check No is already taken',
            ]
        );

        if (number_format($this->total_voucher_amount, 2) != number_format($this->total_amount, 2)) {
            return $this->addError('total_voucher_amount', 'Must be equal to total amount');
        }

        $response = $check_voucher_interface->createOrUpdate($this->request_for_payment, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.check-voucher.index', 'close_modal', 'create');
            $this->emitTo('accounting.check-voucher.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.accounting.check-voucher.create');
    }
}
