<?php

namespace App\Http\Livewire\Accounting\CheckVoucher;

use App\Interfaces\Accounting\CheckVoucherInterface;
use App\Models\Accounting\CheckVoucher;
use App\Models\Accounting\Supplier;
use App\Models\User;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Confirmation extends Component
{
    use PopUpMessagesTrait;



    public $received_by_search;
    public $received_by;
    public $received_date;
    public $released_date;
    public $schedule_release_date;
    public $remarks;
    public $attachments = [];

    public $received_by_references = [];

    public $check_voucher;

    protected $listeners = ['check_voucher_confirmation_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->check_voucher = CheckVoucher::with(['firstApprover', 'secondApprover', 'requestForPayment' => function ($query) {
            $query->with('payee', 'opex');
        }, 'status', 'checkVoucherDetails'])
            ->find($id);
        abort_if(!$this->check_voucher, 404);

        $this->getReceivedBy($this->check_voucher->received_by);
        $this->received_date = $this->check_voucher->received_date;
        $this->released_date = $this->check_voucher->release_date;
        $this->schedule_release_date = $this->check_voucher->schedule_release_date;
        $this->remarks = $this->check_voucher->received_remarks;
    }

    public function load()
    {
        $this->loadReceivedByReferences();
    }

    public function loadReceivedByReferences()
    {
        $this->received_by_references = Supplier::where('company', 'like', '%' . $this->received_by_search . '%')->orderBy('company', 'asc')->take(10)->get();
    }

    protected function updatedReceivedBySearch()
    {
        $this->loadReceivedByReferences();
        $this->reset([
            'received_by'
        ]);
    }

    public function getReceivedBy($id)
    {
        $supplier = Supplier::find($id);

        if ($supplier) {
            $this->received_by_search = $supplier->company;
            $this->received_by = $supplier->id;
        }
    }

    public function submit(CheckVoucherInterface $check_voucher_interface)
    {
        $validated = $this->validate([
            'received_by' => 'sometimes',
            'received_date' => 'sometimes',
            'released_date' => 'sometimes',
            'remarks' => 'sometimes',
            'attachments' => 'sometimes',
            'schedule_release_date' => 'sometimes',
        ]);

        $response = $check_voucher_interface->confirmation($this->check_voucher, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.check-voucher.index', 'close_modal', 'confirmation');
            $this->emitTo('accounting.check-voucher.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            'received_by_search',
            'received_by',
            'received_date',
            'released_date',
            'remarks',
        ]);
    }

    public function render()
    {
        return view('livewire.accounting.check-voucher.confirmation');
    }
}
