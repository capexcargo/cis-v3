<?php

namespace App\Http\Livewire\Accounting\CheckVoucher;

use App\Interfaces\Accounting\CheckVoucherInterface;
use App\Models\Accounting\CheckVoucher;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class View extends Component
{
    use PopUpMessagesTrait;

    public $check_voucher;

    protected $listeners = ['check_voucher_view_mount' => 'mount'];

    public function mount($id)
    {
        $this->check_voucher = CheckVoucher::with(['firstApprover', 'secondApprover', 'requestForPayment' => function ($query) {
            $query->with('payee', 'opex');
        }, 'status', 'checkVoucherDetails'])
            ->find($id);
        abort_if(!$this->check_voucher, 404);
    }

    public function render()
    {
        return view('livewire.accounting.check-voucher.view');
    }
}
