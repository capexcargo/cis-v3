<?php

namespace App\Http\Livewire\Accounting\CheckVoucher;

use App\Interfaces\Accounting\CheckVoucherInterface;
use App\Models\Accounting\StatusReference;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $create_modal = false;
    public $confirmation_modal = false;
    public $view_modal = false;
    public $check_voucher_view_modal = false;
    public $request_for_payment_id;
    public $parent_reference_no;
    public $check_voucher_id;

    public $rfp_reference_no;
    public $check_reference_no;
    public $cv_no;
    public $check_no;
    public $created_by;
    public $received_by;
    public $received_date_from;
    public $received_date_to;
    public $created_date_from;
    public $created_date_to;
    public $released_date_from;
    public $released_date_to;
    public $status;
    public $have_check_voucher;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $selected = [];
    public $status_references = [];

    protected $queryString = ['create_modal', 'confirmation_modal', 'request_for_payment_id', 'check_voucher_id'];
    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadStatusReference();
    }

    public function loadStatusReference()
    {
        $this->status_references = StatusReference::checkVoucher()->get();
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = true;
            $this->emit('check_voucher_create_mount', $data['id']);
        } else if ($action_type == 'confirmation') {
            $this->emit('check_voucher_confirmation_mount', $data['check_voucher_id']);
            $this->confirmation_modal = true;
        } else if ($action_type == 'view') {
            $this->emit('request_management_view_mount', $data['parent_reference_no']);
            $this->view_modal = true;
        } else if ($action_type == 'check_voucher_view') {
            $this->check_voucher_view_modal = true;
            $this->emit('check_voucher_view_mount', $data['check_voucher_id']);
        }

        $this->parent_reference_no = $data['parent_reference_no'] ?? null;
        $this->request_for_payment_id = $data['id'] ?? null;
        $this->check_voucher_id = $data['check_voucher_id'] ?? null;
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'confirmation') {
            $this->confirmation_modal = false;
        } else if ($action_type == 'view') {
            $this->view_modal = false;
        } else if ($action_type == 'check_voucher_view') {
            $this->check_voucher_view_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render(CheckVoucherInterface $check_voucher_interface)
    {
        $request = [
            'rfp_reference_no' => $this->rfp_reference_no,
            'check_reference_no' => $this->check_reference_no,
            'cv_no' => $this->cv_no,
            'check_no' => $this->check_no,
            'created_by' => $this->created_by,
            'received_by' => $this->received_by,
            'received_date_from' => $this->received_date_from,
            'received_date_to' => $this->received_date_to,
            'created_date_from' => $this->created_date_from,
            'created_date_to' => $this->created_date_to,
            'released_date_from' => $this->released_date_from,
            'released_date_to' => $this->released_date_to,
            'status' => $this->status,
            'have_check_voucher' => $this->have_check_voucher,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $check_voucher_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.accounting.check-voucher.index', [
            'request_for_payments' => $response['result']
        ]);
    }
}
