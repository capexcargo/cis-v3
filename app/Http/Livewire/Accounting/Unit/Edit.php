<?php

namespace App\Http\Livewire\Accounting\Unit;

use App\Interfaces\Fims\DataManagement\UnitInterface;
use App\Models\FimsUnit;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $confirmation_modal;
    
    public $unitId;

    public $unit_name;

    public function mount($id)
    {
        $this->unitId = $id;
        $unit_name_lists = FimsUnit::findOrFail($id);
        
        $this->unit_name = $unit_name_lists['name'];
    }

    public function load(){
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.unit.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(UnitInterface $unit_interface)
    {
        $response = $unit_interface->updateValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(UnitInterface $unit_interface)
    {
        $response = $unit_interface->update2($this->getRequest(), $this->unitId);

        if ($response['code'] == 200) {
            $this->emitTo('accounting.unit.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'unit_name' => $this->unit_name,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'unit_name',
        ]);
    }
    public function render()
    {
        return view('livewire.accounting.unit.edit');
    }
}
