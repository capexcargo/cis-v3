<?php

namespace App\Http\Livewire\Accounting\BudgetSource;

use App\Interfaces\Accounting\BudgetManagement\BudgetSourceInterface;
use App\Models\FimsBudgetSource;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $confirmation_modal;
    
    public $budgetSourceId;

    public $name;

    public function mount($id)
    {
        $this->budgetSourceId = $id;
        $name_lists = FimsBudgetSource::findOrFail($id);
        
        $this->name = $name_lists['name'];
    }

    public function load(){
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.budget-source.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(BudgetSourceInterface $budgetSource_interface)
    {
        $response = $budgetSource_interface->updateValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(BudgetSourceInterface $budgetSource_interface)
    {
        $response = $budgetSource_interface->update2($this->getRequest(), $this->budgetSourceId);

        if ($response['code'] == 200) {
            $this->emitTo('accounting.budget-source.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'name' => $this->name,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'name',
        ]);
    }
    public function render()
    {
        return view('livewire.accounting.budget-source.edit');
    }
}
