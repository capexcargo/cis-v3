<?php

namespace App\Http\Livewire\Accounting\RequestManagement;

use App\Models\Accounting\RequestForPayment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class View extends Component
{
    public $requests;

    protected $listeners = ['request_management_view_mount' => 'mount'];

    public function mount($parent_reference_no)
    {
        $this->reset([
            'requests',
        ]);

        $this->requests = RequestForPayment::withTrashed()->with(['budget' => function ($query) {
            $query->with('division', 'source', 'chart');
        }, 'requestForPaymentDetails' => function ($query) {
            $query->with('loaders', 'freightReferenceType', 'truckingType', 'freightUsage', 'paymentType', 'terms');
        }, 'priority', 'payee', 'branch', 'opex', 'canvassingSupplier', 'accountType', 'CAReferenceType', 'approver1', 'approver2', 'approver3', 'attachments'])
            ->where('parent_reference_no', $parent_reference_no)
            ->get();
    }

    public function download($request_for_payment_id, $request_for_payment_attachment_id)
    {
        $request = RequestForPayment::with('attachments')->findOrFail($request_for_payment_id);
        if ($request) {
            $document = $request->attachments()->find($request_for_payment_attachment_id);
            if ($document) {
                if (Storage::disk('accounting_gcs')->exists($document->path . $document->name)) {
                    return Storage::disk('accounting_gcs')->download($document->path . $document->name, $document->id . '-' . $request->reference_id . '.' . $document->extension);
                } else {
                    $this->sweetAlert('error', 'File doesn`t exist in cloud.');
                }
            }
        }
    }

    public function render()
    {
        return view('livewire.accounting.request-management.view');
    }
}
