<?php

namespace App\Http\Livewire\Accounting\RequestManagement;

use App\Interfaces\Accounting\RequestManagementInterface;
use App\Models\Accounting\CanvassingSupplier;
use App\Models\Accounting\CAReferenceTypes;
use App\Models\Accounting\RequestForPayment;
use App\Traits\Accounting\RequestManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use RequestManagementTrait, PopUpMessagesTrait, WithFileUploads;

    protected $listeners = ['request_management_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->request = RequestForPayment::withTrashed()->with(['budget' => function ($query) {
            $query->with('division', 'source', 'chart');
        }, 'requestForPaymentDetails' => function ($query) {
            $query->with('loaders');
        }, 'attachments', 'canvassingSupplier', 'approver1', 'approver2', 'approver3'])
            ->when(Auth::user()->level_id < 5, function ($query) {
                $query->where('user_id', Auth::user()->id);
            })
            ->findOrFail($id);

        if (!$this->request->budget_id) {
            return $this->sweetAlertError('info', "Budget Plan", 'This request will be available for tagging budget and approving for next year.');
        }

        $this->parent_reference_number = $this->request->parent_reference_no;
        $this->reference_number = $this->request->reference_id;
        $this->rfp_type = $this->request->type_id;

        $this->is_payee_contact_person = $this->request->is_payee_contact_person;
        $this->getSupplier($this->request->payee_id);
        $this->subpayee = $this->request->subpayee;
        $this->remarks = $this->request->remarks;
        $this->description = $this->request->description;
        $this->amount = $this->request->amount;
        $this->multiple_budget = $this->request->multiple_budget_id;
        $this->branch = $this->request->branch_id;
        $this->coa_category = $this->request->budget_id;
        $this->date_needed = $this->request->date_needed;
        $this->date_of_transaction_from = $this->request->date_of_transaction_from;
        $this->date_of_transaction_to = $this->request->date_of_transaction_to;
        $this->priority = $this->request->priority_id;
        $this->canvasser = $this->request->canvasser;

        $this->division = $this->request->budget->division_id;
        $this->budget_source = $this->request->budget->budget_source_id;
        $this->chart_of_accounts_search = $this->request->budget->chart->name;
        $this->chart_of_accounts = $this->request->budget->budget_chart_id;

        if ($this->rfp_type == 1) {
            if ($this->request->canvassingSupplier) {
                $this->canvassing_supplier = CanvassingSupplier::find($this->request->canvassing_supplier_id);
                $this->po_reference_no = $this->canvassing_supplier->po_reference_no;
                $this->canvassing_supplier_id = $this->canvassing_supplier->id;
            }

            foreach ($this->request->requestForPaymentDetails as $request_for_payment_details) {
                $this->request_for_payments[] = [
                    'id' => $request_for_payment_details->id,
                    'particulars' => $request_for_payment_details->particulars,
                    'plate_no' => $request_for_payment_details->plate_no,
                    'labor_cost' => $request_for_payment_details->labor_cost,
                    'unit_cost' => $request_for_payment_details->unit_cost,
                    'quantity' => $request_for_payment_details->quantity,
                    'amount' => $request_for_payment_details->amount,
                    'is_deleted' => false,
                ];
            }
        } else if ($this->rfp_type == 2) {
            $this->accounting_account_type = $this->request->account_type_id;
            if ($this->request->requestForPaymentDetails[0]->loaders) {
                $this->loader = $this->request->requestForPaymentDetails[0]->loaders_id;
                $this->loader_search = $this->request->requestForPaymentDetails[0]->loaders->name;
            }
            foreach ($this->request->requestForPaymentDetails as $request_for_payment_details) {
                $this->freights[] = [
                    'id' => $request_for_payment_details->id,
                    'freight_reference_no' => $request_for_payment_details->freight_reference_no,
                    'freight_reference_type' => $request_for_payment_details->freight_reference_type_id,
                    'soa_no' => $request_for_payment_details->soa_no,
                    'trucking_type' => $request_for_payment_details->trucking_type_id,
                    'trucking_amount' => $request_for_payment_details->trucking_amount,
                    'freight_amount' => $request_for_payment_details->freight_amount,
                    'freight_usage' => $request_for_payment_details->freight_usage_id,
                    'transaction_date' => $request_for_payment_details->transaction_date,
                    'allowance' => $request_for_payment_details->allowance,
                    'amount' => $request_for_payment_details->amount,
                    'is_deleted' => false,
                ];
            }

            $this->loadFreightReferenceTypeReference();
            $this->loadFreightUsageReference();
        } else if ($this->rfp_type == 3) {
            $this->account_no = $this->request->requestForPaymentDetails[0]->account_no;
            $this->payment_type = $this->request->requestForPaymentDetails[0]->payment_type_id;
            $this->terms = $this->request->requestForPaymentDetails[0]->terms_id;
            $this->pdc_from = $this->request->requestForPaymentDetails[0]->pdc_from;
            $this->pdc_to = $this->request->requestForPaymentDetails[0]->pdc_to;
            foreach ($this->request->requestForPaymentDetails as $request_for_payment_details) {
                $this->payables[] = [
                    'id' => $request_for_payment_details->id,
                    'invoice_no' => $request_for_payment_details->invoice,
                    'invoice_amount' => $request_for_payment_details->invoice_amount,
                    'date_of_transaction' => $request_for_payment_details->transaction_date,
                    'is_deleted' => false,
                ];
            }
        } else if ($this->rfp_type == 4) {
            $this->ca_no = $this->request->ca_no;
            if ($this->request->ca_reference_type_id) {
                $this->ca_reference_type = $this->request->ca_reference_type_id;
                $this->ca_reference_type_display = (CAReferenceTypes::find($this->request->ca_reference_type_id))->display;
            }
            foreach ($this->request->requestForPaymentDetails as $request_for_payment_details) {
                $this->cash_advances[] = [
                    'id' => $request_for_payment_details->id,
                    'particulars' => $request_for_payment_details->particulars,
                    'ca_reference_no' => $request_for_payment_details->ca_reference_no,
                    'unit_cost' => $request_for_payment_details->unit_cost,
                    'quantity' => $request_for_payment_details->quantity,
                    'amount' => $request_for_payment_details->amount,
                    'is_deleted' => false,
                ];
            }
        }

        foreach ($this->request->attachments as $attachment) {
            $this->attachments[] = [
                'id' => $attachment->id,
                'attachment' => '',
                'path' => $attachment->path,
                'name' => $attachment->name,
                'extension' => $attachment->extension,
                'is_deleted' => false,
            ];
        }

        $this->loadBudgetSourceReference();
        $this->loadBudgetChartReference();
        $this->loadBudgetPlanReference();
        $this->loadApprovers();

        $this->total_amount = $this->request->amount;
        $this->updatedCoaCategory();
        $this->opex_type = $this->request->opex_type_id;

        $this->is_set_approver_1 = $this->request->is_set_approver_1;
        $this->approver_1_id = $this->request->approver_1_id;
        $this->approver_2_id = $this->request->approver_2_id;
        $this->approver_3_id = $this->request->approver_3_id;

        $this->approver_1_name = $this->request->approver1 ? $this->request->approver1->name : '';
        $this->approver_2_name = $this->request->approver2 ? $this->request->approver2->name : '';
        $this->approver_3_name = $this->request->approver3 ? $this->request->approver3->name : '';
    }

    public function submit(RequestManagementInterface $RequestManagementInterface)
    {
        if ($this->rfp_type == 2 && $this->accounting_account_type == 1 || $this->payment_type == 2) {
            $this->date_of_transaction_from = now();
            $this->date_of_transaction_to = now();
        }

        if ($this->rfp_type == 1) {
            $this->updatedRequestForPayments();
        } elseif ($this->rfp_type == 2) {
            $this->updatedFreights();
        } elseif ($this->rfp_type == 3) {
            $this->updatedPayables();
        } elseif ($this->rfp_type == 4) {
            $this->updatedCashAdvances();
        }

        $validated = $this->validate([
            'parent_reference_number' => 'required',
            'reference_number' => 'required',
            'rfp_type' => 'required',
            'is_payee_contact_person' => 'sometimes',
            'payee' => 'required',
            'subpayee' => 'sometimes',
            'is_subpayee' => 'sometimes',
            'description' => 'required',
            'remarks' => 'sometimes',
            'branch' => 'required',
            'coa_category' => 'required',
            'date_needed' => 'required',
            'date_of_transaction_from' => 'required',
            'date_of_transaction_to' => 'required',
            'is_set_approver_1' => 'sometimes',
            'approver_1_id' => 'required',
            'approver_2_id' => 'required',
            'approver_3_id' => 'sometimes',
            'opex_type' => 'required',

            'attachments' => 'required',
            'attachments.*.attachment' => 'required_if:attachments.*.id,null|' . config('filesystems.validation_all'),

            'priority' => 'required',
            'canvasser' => 'sometimes',

            'division' => 'required',
            'budget_source' => 'required',
            'chart_of_accounts_search' => 'required',
            'chart_of_accounts' => 'required',

            'canvassing_supplier' => 'required_if:rfp_type,1',
            'canvassing_supplier_id' => 'required_if:rfp_type,1',
            'po_reference_no' => 'required_if:rfp_type,1',

            'accounting_account_type' => 'required_if:rfp_type,2',
            'loader' => 'required_if:rfp_type,2',

            'ca_no' => 'sometimes',
            'ca_reference_type' => 'sometimes',

            'request_for_payments' => 'required_if:rfp_type,1',
            'freights' => 'required_if:rfp_type,2',
            'payables' => 'required_if:rfp_type,3',
            'cash_advances' => 'required_if:rfp_type,4',
        ], [
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png,xlsx,doc,docx.',
        ]);

        if ($validated['rfp_type'] == 1) {
            $validated_details = $this->validate([
                'request_for_payments.*.id' => 'nullable',
                'request_for_payments.*.particulars' => 'required',
                'request_for_payments.*.plate_no' => 'nullable',
                'request_for_payments.*.labor_cost' => 'nullable',
                'request_for_payments.*.unit_cost' => 'required',
                'request_for_payments.*.quantity' => 'required',
                'request_for_payments.*.amount' => 'required',
                'request_for_payments.*.is_deleted' => 'nullable',
            ], [
                'request_for_payments.*.particulars.required' => 'The particulars field is required.',
                'request_for_payments.*.plate_no.required' => 'The plate number field is required.',
                'request_for_payments.*.labor_cost.required' => 'The labor cost field is required.',
                'request_for_payments.*.unit_cost.required' => 'The unit cost field is required.',
                'request_for_payments.*.quantity.required' => 'The quantity field is required.',
                'request_for_payments.*.amount.required' => 'The amount field is required.',
            ]);

            $response = $RequestManagementInterface->updateRequestForPayments($this->request, $validated, $validated_details, Auth::user()->id);

            if ($response['code'] == 200) {
                $this->resetForm();
                $this->emitTo('accounting.request-management.index', 'close_modal', 'edit');
                $this->emitTo('accounting.request-management.index', 'index');
                $this->sweetAlert('success', $response['message']);
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($validated['rfp_type'] == 2) {
            $validated_details = $this->validate([
                'freights.*.id' => 'nullable',
                'freights.*.freight_reference_no' => 'required',
                'freights.*.freight_reference_type' => 'required',
                'freights.*.soa_no' => 'required',
                'freights.*.trucking_type' => 'required',
                'freights.*.trucking_amount' => 'required',
                'freights.*.freight_amount' => 'required',
                'freights.*.freight_usage' => 'required',
                'freights.*.transaction_date' => 'required',
                'freights.*.allowance' => 'required',
                'freights.*.is_deleted' => 'nullable',
            ], [
                'freights.*.freight_reference_no.required' => 'The freight reference no field is required.',
                'freights.*.freight_reference_type.required' => 'The freight reference type field is required.',
                'freights.*.soa_no.required' => 'The soa no field is required.',
                'freights.*.trucking_type.required' => 'The trucking category field is required.',
                'freights.*.trucking_amount.required' => 'The trucking amount field is required.',
                'freights.*.freight_amount.required' => 'The freight amount field is required.',
                'freights.*.freight_usage.required' => 'The freight usage field is required.',
                'freights.*.transaction_date.required' => 'The trucking date field is required.',
                'freights.*.allowance.required' => 'The allowance field is required.',
            ]);

            $response = $RequestManagementInterface->updateFreights($this->request, $validated, $validated_details, Auth::user()->id);

            if ($response['code'] == 200) {
                $this->resetForm();
                $this->emitTo('accounting.request-management.index', 'close_modal', 'edit');
                $this->emitTo('accounting.request-management.index', 'index');
                $this->sweetAlert('success', $response['message']);
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($validated['rfp_type'] == 3) {
            $validated_details = $this->validate([
                'account_no' => 'required',
                'account_no' => 'required',
                'payment_type' => 'required',
                'terms' => 'required',
                'pdc_from' => 'required',
                'pdc_to' => 'required',
                'payables.*.id' => 'nullable',
                'payables.*.invoice_no' => 'required',
                'payables.*.invoice_amount' => 'required',
                'payables.*.date_of_transaction' => 'required_if:payment_type,2',
                'payables.*.is_deleted' => 'nullable',
            ], [
                'payables.*.invoice_no.required' => 'The invoice number field is required.',
                'payables.*.invoice_amount.required' => 'The invoice amount field is required.',
                'payables.*.date_of_transaction.required_if' => 'The date of transaction field is required.',
            ]);

            $response = $RequestManagementInterface->updatePayables($this->request, $validated, $validated_details, Auth::user()->id);

            if ($response['code'] == 200) {
                $this->resetForm();
                $this->emitTo('accounting.request-management.index', 'close_modal', 'edit');
                $this->emitTo('accounting.request-management.index', 'index');
                $this->sweetAlert('success', $response['message']);
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($validated['rfp_type'] == 4) {
            $validated_details = $this->validate([
                'cash_advances.*.id' => 'nullable',
                'cash_advances.*.particulars' => 'required',
                'cash_advances.*.ca_reference_no' => 'sometimes',
                'cash_advances.*.unit_cost' => 'required',
                'cash_advances.*.quantity' => 'required',
                'cash_advances.*.amount' => 'required',
                'cash_advances.*.is_deleted' => 'nullable',
            ], [
                'cash_advances.*.particulars.required' => 'The particulars field is required.',
                'cash_advances.*.ca_reference_no.required' => 'The ' . ($this->ca_reference_type_display ?? 'CA Reference No') . ' field is required.',
                'cash_advances.*.unit_cost.required' => 'The unit cost field is required.',
                'cash_advances.*.quantity.required' => 'The quantity field is required.',
                'cash_advances.*.amount.required' => 'The amount field is required.',
            ]);

            $response = $RequestManagementInterface->updateCashAdvances($this->request, $validated, $validated_details, Auth::user()->id);

            if ($response['code'] == 200) {
                $this->resetForm();
                $this->emitTo('accounting.request-management.index', 'close_modal', 'edit');
                $this->emitTo('accounting.request-management.index', 'index');
                $this->sweetAlert('success', $response['message']);
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function render()
    {
        return view('livewire.accounting.request-management.edit');
    }
}
