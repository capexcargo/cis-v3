<?php

namespace App\Http\Livewire\Accounting\RequestManagement;

use App\Imports\ReqeustForPaymentImport;
use App\Interfaces\Accounting\RequestManagementInterface;
use App\Models\Accounting\OpexType;
use App\Models\Accounting\RequestForPayment;
use App\Models\Accounting\RequestForPaymentTypeReference;
use App\Models\Accounting\StatusReference;
use App\Models\Division;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Maatwebsite\Excel\Facades\Excel;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait, WithFileUploads;

    public $confirmation_modal = false;
    public $action_type;
    public $confirmation_message;

    public $create_modal = false;
    public $edit_modal = false;
    public $view_modal = false;
    public $import_modal = false;

    public $request_id;
    public $parent_reference_no;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $division;
    public $reference_no;
    public $request_type;
    public $payee;
    public $opex_type;
    public $requester;
    public $description;
    public $date_created;
    public $status;
    public $trashed;

    public $is_subpayee;
    public $subpayee;
    
    public $selected = [];
    public $import;

    public $division_references = [];
    public $request_for_payment_type_references = [];
    public $opex_type_references = [];
    public $status_references = [];

    protected $queryString = ['view_modal' => ['except' => false], 'parent_reference_no' => ['except' => '']];
    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadDivisionReferences();
        $this->loadRequestForPaymentTypeReference();
        $this->loadOpexTypeReference();
        $this->loadStatusReference();
    }

    public function loadDivisionReferences()
    {
        $this->division_references = Division::get();
    }

    public function loadRequestForPaymentTypeReference()
    {
        $this->request_for_payment_type_references = RequestForPaymentTypeReference::active()->get();
    }

    public function loadOpexTypeReference()
    {
        $this->opex_type_references = OpexType::get();
    }

    public function loadStatusReference()
    {
        $this->status_references = StatusReference::requestForPayment()->get();
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'create') {
            $this->emit('generate_parent_reference_number');
            $this->emit('generate_reference_number');
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('request_management_edit_mount', $data['id']);
            $this->edit_modal = true;
        } else if ($action_type == 'view') {
            $this->emit('request_management_view_mount', $data['parent_reference_no']);
            $this->view_modal = true;
        } else if ($action_type == 'confirmation_delete') {
            $this->confirmation_message = "Are you sure to delete this request?";
            $this->confirmation_modal = true;
        } else if ($action_type == 'confirmation_restore') {
            $this->confirmation_message = "Are you sure to restore this request?";
            $this->confirmation_modal = true;
        }

        $this->parent_reference_no = $data['parent_reference_no'] ?? null;
        $this->request_id = $data['id'] ?? null;
    }

    public function remove(RequestManagementInterface $request_management_interface)
    {
        $request_for_payment = RequestForPayment::withTrashed()->find($this->request_id);
        if ($this->action_type == 'confirmation_delete') {
            $response = $request_management_interface->destroy($request_for_payment);
        } elseif ($this->action_type == 'confirmation_restore') {
            $response = $request_management_interface->restore($request_for_payment);
        }

        if ($response['code'] == 200) {
            $this->emit('request_management_index');
            $this->sweetAlert('success', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } else if ($action_type == 'view') {
            $this->view_modal = false;
        }
    }

    public function import()
    {
        try {
            $validated = $this->validate([
                'import' => 'mimes:xlsx,csv'
            ]);

            Excel::import(new ReqeustForPaymentImport, $this->import);
            $this->reset('import');
            $this->sweetAlert('success', 'Request For Payment Successfully Imported!');
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            foreach ($failures as $failure) {
                $this->sweetAlertError('error', 'Something Went Wrong', "Row: " . $failure->row() .
                    "\n Column: " . $failure->attribute() .
                    "\n Message: " . $failure->errors()[0] .
                    "\n Value: " . $failure->values()[$failure->attribute()]);
            }
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.request-management.index', [
            'request_for_payments' => RequestForPayment::with('type', 'payee', 'user', 'opex', 'status', 'requestForPaymentDetails','checkVoucher')
                ->when($this->trashed == 'with_trashed', function ($query) {
                    $query->withTrashed();
                })
                ->when($this->trashed == 'only_trashed', function ($query) {
                    $query->onlyTrashed();
                })
                ->whereHas('budget', function ($query) {
                    $query->when($this->division, function ($query) {
                        $query->where('division_id', $this->division);
                    });
                })
                ->whereHas('user', function ($query) {
                    $query->when($this->requester, function ($query) {
                        $query->where('name', 'like', '%' . $this->requester . '%');
                    });
                })
                ->when($this->reference_no, function ($query) {
                    $query->where('reference_id', 'like', '%' . $this->reference_no . '%');
                })
                ->when($this->request_type, function ($query) {
                    $query->where('type_id', $this->request_type);
                })
                ->when($this->payee, function ($query) {
                    $query->whereHas('payee', function ($query) {
                        $query->where('company', 'like', '%' . $this->payee . '%');
                    });
                })
                ->when($this->opex_type, function ($query) {
                    $query->where('opex_type_id', 'like', '%' . $this->opex_type . '%');
                })
                ->when($this->status, function ($query) {
                    $query->where('status_id', $this->status);
                })
                ->when($this->date_created, function ($query) {
                    $query->whereDate('created_at', $this->date_created);
                })
                ->when($this->description, function ($query) {
                    $query->where('description', 'like', '%' . $this->description . '%');
                })
                ->when(Auth::user()->level_id < 5, function ($query) {
                    $query->where('user_id', Auth::user()->id);
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)

        ]);
    }
}
