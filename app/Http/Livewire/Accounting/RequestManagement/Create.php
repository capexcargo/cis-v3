<?php

namespace App\Http\Livewire\Accounting\RequestManagement;

use App\Interfaces\Accounting\RequestManagementInterface;
use App\Traits\Accounting\RequestManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use RequestManagementTrait, PopUpMessagesTrait, WithFileUploads;

    protected $listeners = ['generate_reference_number' => 'generateReferenceNumber', 'generate_parent_reference_number' => 'generateParentReferenceNumber'];

    public function mount()
    {
        $this->addAttachments();
    }

    public function submit(RequestManagementInterface $RequestManagementInterface)
    {

        // dd($this->attachments);
        if ($this->rfp_type == 2 && $this->accounting_account_type == 1 || $this->payment_type == 2) {
            $this->date_of_transaction_from = now();
            $this->date_of_transaction_to = now();
        }

        $approver_1_id = $this->approver_1_id;
        if ($this->rfp_type == 1) {
            $this->updatedRequestForPayments();
        } elseif ($this->rfp_type == 2) {
            $this->updatedFreights();
        } elseif ($this->rfp_type == 3) {
            $this->updatedPayables();
        } elseif ($this->rfp_type == 4) {
            $this->updatedCashAdvances();
            $this->generateCANumber();
        }
        $this->approver_1_id = $approver_1_id;

        $validated = $this->validate([
            'parent_reference_number' => 'required',
            'reference_number' => 'required',
            'rfp_type' => 'required',
            'is_payee_contact_person' => 'sometimes',
            'payee' => 'required',
            'subpayee' => 'sometimes',
            'is_subpayee' => 'sometimes',
            'description' => 'required',
            'remarks' => 'sometimes',
            'branch' => 'required',
            'coa_category' => 'required',
            'date_needed' => 'required',
            'date_of_transaction_from' => 'required',
            'date_of_transaction_to' => 'required',
            'is_set_approver_1' => 'sometimes',
            'approver_1_id' => 'required',
            'approver_2_id' => 'required',
            'approver_3_id' => 'sometimes',
            'opex_type' => 'required',

            'attachments' => 'required',
            'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),

            'priority' => 'required',
            'canvasser' => 'sometimes',

            'division' => 'required',
            'budget_source' => 'required',
            'chart_of_accounts_search' => 'required',
            'chart_of_accounts' => 'required',

            'canvassing_supplier' => 'required_if:rfp_type,1',
            'canvassing_supplier_id' => 'required_if:rfp_type,1',
            'po_reference_no' => 'required_if:rfp_type,1',

            'accounting_account_type' => 'required_if:rfp_type,2',
            'loader' => 'required_if:rfp_type,2',

            'ca_no' => 'sometimes',
            'ca_reference_type' => 'sometimes',

            'request_for_payments' => 'required_if:rfp_type,1',
            'freights' => 'required_if:rfp_type,2',
            'payables' => 'required_if:rfp_type,3',
            'cash_advances' => 'required_if:rfp_type,4',
        ], [
            'attachments.*.attachment.required' => 'The attachment field is required.',
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png,xlsx,doc,docx.',
        ]);

        if ($validated['rfp_type'] == 1) {
            $validated_details = $this->validate([
                'request_for_payments.*.particulars' => 'required',
                'request_for_payments.*.plate_no' => 'nullable',
                'request_for_payments.*.labor_cost' => 'nullable',
                'request_for_payments.*.unit_cost' => 'required',
                'request_for_payments.*.quantity' => 'required',
                'request_for_payments.*.amount' => 'required',
            ], [
                'request_for_payments.*.particulars.required' => 'The particulars field is required.',
                'request_for_payments.*.plate_no.nullable' => 'The plate number field is required.',
                'request_for_payments.*.labor_cost.nullable' => 'The labor cost field is required.',
                'request_for_payments.*.unit_cost.required' => 'The unit cost field is required.',
                'request_for_payments.*.quantity.required' => 'The quantity field is required.',
                'request_for_payments.*.amount.required' => 'The amount field is required.',
            ]);

            $response = $RequestManagementInterface->createRequestForPayments($validated, $validated_details, Auth::user()->id);

            if ($response['code'] == 200) {
                $this->resetForm();
                $this->emitTo('accounting.request-management.index', 'close_modal', 'create');
                $this->emitTo('accounting.request-management.index', 'index');
                $this->sweetAlert('success', $response['message']);
                return redirect()->route('accounting.request-management.print-request-for-payment', ['selected' => json_encode([$response['result']['reference_id']])]);
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($validated['rfp_type'] == 2) {
            $validated_details = $this->validate([
                'freights.*.freight_reference_no' => 'required',
                'freights.*.freight_reference_type' => 'required',
                'freights.*.soa_no' => 'required',
                'freights.*.trucking_type' => 'required',
                'freights.*.trucking_amount' => 'required',
                'freights.*.freight_amount' => 'required',
                'freights.*.freight_usage' => 'required',
                'freights.*.transaction_date' => 'required',
                'freights.*.allowance' => 'required',
            ], [
                'freights.*.freight_reference_no.required' => 'The freight reference no field is required.',
                'freights.*.freight_reference_type.required' => 'The freight reference type field is required.',
                'freights.*.soa_no.required' => 'The soa no field is required.',
                'freights.*.trucking_type.required' => 'The trucking category field is required.',
                'freights.*.trucking_amount.required' => 'The trucking amount field is required.',
                'freights.*.freight_amount.required' => 'The freight amount field is required.',
                'freights.*.freight_usage.required' => 'The freight usage field is required.',
                'freights.*.transaction_date.required' => 'The trucking date field is required.',
                'freights.*.allowance.required' => 'The allowance field is required.',
            ]);

            $response = $RequestManagementInterface->createFreights($validated, $validated_details, Auth::user()->id);

            if ($response['code'] == 200) {
                $this->resetForm();
                $this->emitTo('accounting.request-management.index', 'close_modal', 'create');
                $this->emitTo('accounting.request-management.index', 'index');
                $this->sweetAlert('success', $response['message']);
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } else if ($validated['rfp_type'] == 3) {
            $validated_details = $this->validate([
                'account_no' => 'required',
                'payment_type' => 'required',
                'terms' => 'required',
                'pdc_from' => 'required',
                'pdc_to' => 'required',
                'payables.*.invoice_no' => 'required',
                'payables.*.invoice_amount' => 'required',
                'payables.*.date_of_transaction' => 'required_if:payment_type,2',
            ], [
                'payables.*.invoice_no.required' => 'The invoice number field is required.',
                'payables.*.invoice_amount.required' => 'The invoice amount field is required.',
                'payables.*.date_of_transaction.required_if' => 'The date of transaction field is required.',
            ]);

            $response = $RequestManagementInterface->createPayables($validated, $validated_details, Auth::user()->id);

            if ($response['code'] == 200) {
                $this->resetForm();
                $this->emitTo('accounting.request-management.index', 'close_modal', 'create');
                $this->emitTo('accounting.request-management.index', 'index');
                $this->sweetAlert('success', $response['message']);
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } else if ($validated['rfp_type'] == 4) {
            $validated_details = $this->validate([
                'cash_advances.*.particulars' => 'required',
                'cash_advances.*.ca_reference_no' => 'sometimes',
                'cash_advances.*.unit_cost' => 'required',
                'cash_advances.*.quantity' => 'required',
                'cash_advances.*.amount' => 'required',
            ], [
                'cash_advances.*.particulars.required' => 'The particulars field is required.',
                'cash_advances.*.ca_reference_no.required' => 'The ' . ($this->ca_reference_type_display ?? 'CA Reference No') . ' field is required.',
                'cash_advances.*.unit_cost.required' => 'The unit cost field is required.',
                'cash_advances.*.quantity.required' => 'The quantity field is required.',
                'cash_advances.*.amount.required' => 'The amount field is required.',
            ]);

            $response = $RequestManagementInterface->createCashAdvances($validated, $validated_details, Auth::user()->id);

            if ($response['code'] == 200) {
                $this->resetForm();
                $this->emitTo('accounting.request-management.index', 'close_modal', 'create');
                $this->emitTo('accounting.request-management.index', 'index');
                $this->sweetAlert('success', $response['message']);
                return redirect()->route('accounting.request-management.print-request-for-payment', ['selected' => json_encode([$response['result']['reference_id']])]);
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function render()
    {
        return view('livewire.accounting.request-management.create');
    }
}
