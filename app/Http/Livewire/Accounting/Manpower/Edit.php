<?php

namespace App\Http\Livewire\Accounting\Manpower;

use App\Interfaces\Fims\DataManagement\ManpowerInterface;
use App\Models\FimsManPowerReference;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $confirmation_modal;
    
    public $manpowerId;

    public $name;
    public $email_address;
    public $mobile_number;
    public $tel_number;
    public $agency;
    public $tin;

    public function mount($id)
    {
        $this->manpowerId = $id;
        $subAccounts_name_lists = FimsManPowerReference::findOrFail($id);
        
        $this->name = $subAccounts_name_lists['name'];
        $this->email_address = $subAccounts_name_lists['email_address'];
        $this->mobile_number = $subAccounts_name_lists['mobile_number'];
        $this->tel_number = $subAccounts_name_lists['tel_number'];
        $this->agency = $subAccounts_name_lists['agency'];
        $this->tin = $subAccounts_name_lists['tin'];
    }

    public function load(){
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.manpower.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(ManpowerInterface $manpower_interface)
    {
        $response = $manpower_interface->updateValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(ManpowerInterface $manpower_interface)
    {
        $response = $manpower_interface->update2($this->getRequest(), $this->manpowerId);

        if ($response['code'] == 200) {
            $this->emitTo('accounting.manpower.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'name' => $this->name,
            'email_address' => $this->email_address,
            'mobile_number' => $this->mobile_number,
            'tel_number' => $this->tel_number,
            'agency' => $this->agency,
            'tin' => $this->tin,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'name',
            'email_address',
            'mobile_number',
            'tel_number',
            'agency',
            'tin',
        ]);
    }
    public function render()
    {
        return view('livewire.accounting.manpower.edit');
    }
}
