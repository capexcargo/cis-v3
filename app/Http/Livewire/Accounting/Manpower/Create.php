<?php

namespace App\Http\Livewire\Accounting\Manpower;

use App\Interfaces\Fims\DataManagement\ManpowerInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function mount()
    {
    }
    public $confirmation_modal;
    public $name;
    public $email_address;
    public $mobile_number;
    public $tel_number;
    public $agency;
    public $tin;

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.manpower.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(ManpowerInterface $manpower_interface)
    {
        $response = $manpower_interface->createValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function submit(ManpowerInterface $manpower_interface)
    {
        $response = $manpower_interface->create2($this->getRequest());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.manpower.index', 'close_modal', 'create');
            $this->emitTo('accounting.manpower.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'name' => $this->name,
            'email_address' => $this->email_address,
            'mobile_number' => $this->mobile_number,
            'tel_number' => $this->tel_number,
            'agency' => $this->agency,
            'tin' => $this->tin,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'name',
            'email_address',
            'mobile_number',
            'tel_number',
            'agency',
            'tin',
        ]);
    }
    public function render()
    {
        return view('livewire.accounting.manpower.create');
    }
}
