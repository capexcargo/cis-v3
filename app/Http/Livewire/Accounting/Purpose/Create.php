<?php

namespace App\Http\Livewire\Accounting\Purpose;

use App\Interfaces\Fims\DataManagement\PurposeInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount', 'submit' => 'submit', 'index' => 'render'];

    public function mount()
    {
    }
    public $confirmation_modal;
    public $purpose_name;

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.purpose.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(PurposeInterface $purpose_interface)
    {
        $response = $purpose_interface->createValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function submit(PurposeInterface $purpose_interface)
    {
        $response = $purpose_interface->create2($this->getRequest());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.purpose.index', 'close_modal', 'create');
            $this->emitTo('accounting.purpose.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'purpose_name' => $this->purpose_name,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'purpose_name',
        ]);
    }
    public function render()
    {
        return view('livewire.accounting.purpose.create');
    }
}
