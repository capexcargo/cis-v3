<?php

namespace App\Http\Livewire\Accounting\Purpose;

use App\Interfaces\Fims\DataManagement\PurposeInterface;
use App\Models\FimsPurpose;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $confirmation_modal;
    
    public $purposeId;

    public $purpose_name;

    public function mount($id)
    {
        $this->purposeId = $id;
        $purpose_name_lists = FimsPurpose::findOrFail($id);
        
        $this->purpose_name = $purpose_name_lists['name'];
    }

    public function load(){
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.purpose.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(PurposeInterface $purpose_interface)
    {
        $response = $purpose_interface->updateValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(PurposeInterface $purpose_interface)
    {
        $response = $purpose_interface->update2($this->getRequest(), $this->purposeId);

        if ($response['code'] == 200) {
            $this->emitTo('accounting.purpose.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'purpose_name' => $this->purpose_name,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'purpose_name',
        ]);
    }
    public function render()
    {
        return view('livewire.accounting.purpose.edit');
    }
}
