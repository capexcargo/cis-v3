<?php

namespace App\Http\Livewire\Accounting\ChartofAccounts;

use App\Interfaces\Fims\DataManagement\ChartofAccountsInterface;
use App\Models\FimsChartsOfAccounts;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $confirmation_modal;
    
    public $subAccountId;

    public $chartofAccounts_name;
    public $acct_code;

    public function mount($id)
    {
        $this->subAccountId = $id;
        $chartofAccounts_name_lists = FimsChartsOfAccounts::findOrFail($id);
        
        $this->chartofAccounts_name = $chartofAccounts_name_lists['name'];
        $this->acct_code = $chartofAccounts_name_lists['account_code'];
    }

    public function load(){
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.chartof-accounts.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function confirmationSubmit(ChartofAccountsInterface $chartofAccounts_interface)
    {
        $response = $chartofAccounts_interface->updateValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(ChartofAccountsInterface $chartofAccounts_interface)
    {
        $response = $chartofAccounts_interface->update2($this->getRequest(), $this->subAccountId);

        if ($response['code'] == 200) {
            $this->emitTo('accounting.chartof-accounts.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'chartofAccounts_name' => $this->chartofAccounts_name,
            'acct_code' => $this->acct_code,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'chartofAccounts_name',
            'acct_code',
        ]);
    }
    public function render()
    {
        return view('livewire.accounting.chartof-accounts.edit');
    }
}
