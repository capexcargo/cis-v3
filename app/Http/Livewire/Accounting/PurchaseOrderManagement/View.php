<?php

namespace App\Http\Livewire\Accounting\PurchaseOrderManagement;

use App\Interfaces\Accounting\CanvassingManagementInterface;
use App\Models\Accounting\CanvassingSupplier;
use App\Models\Accounting\CanvassingSupplierItem;
use App\Models\Accounting\WaybillSeriesPo;
use App\Models\BranchReference;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class View extends Component
{
    use PopUpMessagesTrait;

    public $canvassing_supplier;
    public $waybills = [];


    protected $listeners = ['purchar_order_management_view_mount' => 'mount'];

    public function mount($id)
    {
        $this->canvassing_supplier = CanvassingSupplier::with(['waybillseries','supplier' => function ($query) {
            $query->with('industry', 'type', 'branch');
        }, 'canvassingSupplierItem' => function ($query) {
            $query->with('supplierItem')->where('final_status', true);
        }])->findOrFail($id);

        // dd($this->canvassing_supplier);
    }

    public function addWaybillSeries()
    {
        $this->waybills[] = [
            'id' => null,
            'branch_id' => null,
            'waybill_from' => null,
            'waybill_to' => null,
            'is_deleted' => false,
        ];
    }
    public function removeWaybill($i)
    {
        if (count($this->waybills) > 1) {
            if ($this->waybills[$i]['id']) {
                $this->waybills[$i]['is_deleted'] = true;
            } else {
                unset($this->waybills[$i]);
            }
            array_values($this->waybills);
        }
    }
    public function updatedWaybills()
    {

        foreach ($this->waybills as $i => $waybill) {
            if (strlen($this->waybills[$i]['branch_id']) == 3) {
                $getbranchid = BranchReference::where('code', $this->waybills[$i]['branch_id'])->first();
                $getlastwaybill = WaybillSeriesPo::where('branch_id', $getbranchid->id)->orderBy('id', 'DESC')->get();
                if (count($getlastwaybill) > 1) {

                    $this->waybills[$i] = [
                        'id' => null,
                        'branch_id' => $this->waybills[$i]['branch_id'],
                        'waybill_from' => $this->waybills[$i]['branch_id'] . (substr($getlastwaybill[0]['waybill'], 3) + 1),
                        'waybill_to' => $this->waybills[$i]['waybill_to'],
                        'is_deleted' => false,
                    ];
                } else {

                    $this->waybills[$i] = [
                        'id' => null,
                        'branch_id' => $this->waybills[$i]['branch_id'],
                        'waybill_from' => $this->waybills[$i]['branch_id'] . "1",
                        'waybill_to' => $this->waybills[$i]['waybill_to'],
                        'is_deleted' => false,
                    ];
                }

                // dd($getlastwaybill[0]['waybill']);
            }
        }
    }
    public function resetForm()
    {
        $this->reset([
            'waybills'
        ]);
    }

    public function submitWaybillSeries(CanvassingManagementInterface $canvassing_supplier_item_interface)
    {
        // dd($this->waybills, $this->canvassing_supplier->canvassing_id);

        $response = $canvassing_supplier_item_interface->create_waybill($this->waybills, $this->canvassing_supplier->canvassing_id);

        if ($response['code'] == 200) {
            $this->resetForm();
            // $this->emitTo('accounting.canvassing-management.index', 'close_modal', 'create');
            $this->emitTo('accounting.purchase-order-management.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function download($id)
    {
        if ($this->canvassing_supplier) {
            $document = $this->canvassing_supplier->attachments()->find($id);
            if ($document) {
                if (Storage::disk('accounting_gcs')->exists($document->path . $document->name)) {
                    return Storage::disk('accounting_gcs')->download($document->path . $document->name, $document->id . '-' . $this->canvassing_supplier->po_reference_no . '.' . $document->extension);
                } else {
                    $this->sweetAlert('error', 'File doesn`t exist in cloud.');
                }
            }
        }
    }

    public function render()
    {
        return view('livewire.accounting.purchase-order-management.view');
    }
}
