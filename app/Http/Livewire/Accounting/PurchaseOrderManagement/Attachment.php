<?php

namespace App\Http\Livewire\Accounting\PurchaseOrderManagement;

use App\Interfaces\Accounting\CanvassingManagementInterface;
use App\Models\Accounting\CanvassingSupplier;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Attachment extends Component
{
    use WithFileUploads, PopUpMessagesTrait;

    public $canvassing_supplier;
    public $attachment;

    protected $listeners = ['purchar_order_management_attachment_mount' => 'mount'];

    public function mount($id)
    {
        $this->canvassing_supplier = CanvassingSupplier::find($id);

        if (!$this->canvassing_supplier) {
            abort(404);
        }
    }

    public function submit(CanvassingManagementInterface $canvassing_management_interface)
    {
        $validated = $this->validate([
            'attachment' => 'required|max:1024|'.config('filesystems.validation_all'),
        ]);

        $response = $canvassing_management_interface->attachment($this->canvassing_supplier, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('accounting.purchase-order-management.index', 'close_modal', 'attachment');
            $this->emitTo('accounting.purchase-order-management.index', 'index');
            $this->sweetAlert('success', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            'attachment'
        ]);
    }

    public function render()
    {
        return view('livewire.accounting.purchase-order-management.attachment');
    }
}
