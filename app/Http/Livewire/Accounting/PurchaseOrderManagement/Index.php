<?php

namespace App\Http\Livewire\Accounting\PurchaseOrderManagement;

use App\Models\Accounting\CanvassingSupplier;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $purchase_order_management_view_modal = false;
    public $attachment_modal = false;
    public $canvassing_supplier_id;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $canvas_reference_no;
    public $po_reference_no;
    public $company_name;
    public $item_name;
    public $created_from;
    public $created_to;

    public $selected = [];

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emit('purchar_order_management_view_mount', $data['id']);
            $this->purchase_order_management_view_modal = true;
        } else if ($action_type == 'attachment') {
            $this->emit('purchar_order_management_attachment_mount', $data['id']);
            $this->attachment_modal = true;
        }

        $this->canvassing_supplier_id = $data['id'];
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'edit') {
            $this->purchase_order_management_view_modal = false;
        } else if ($action_type == 'attachment') {
            $this->attachment_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.purchase-order-management.index', [
            'purchase_orders' => CanvassingSupplier::with(['canvassing', 'supplier', 'canvassingSupplierItem' => function ($query) {
                $query->with('supplierItem')->where('final_status', true);
            }])
                ->withSum(['canvassingSupplierItem' => function ($query) {
                    $query->where('final_status', true);
                }], 'total_amount')
                ->whereHas('canvassing', function ($query) {
                    $query->when($this->canvas_reference_no, function ($query) {
                        $query->where('reference_id', 'like', '%' . $this->canvas_reference_no . '%');
                    })
                        ->when(Auth::user()->level_id < 5, function ($query) {
                            $query->where('created_by', Auth::user()->id);
                        });
                })
                ->when($this->po_reference_no, function ($query) {
                    $query->where('po_reference_no', 'like', '%' . $this->po_reference_no . '%');
                })
                ->when(!$this->po_reference_no, function ($query) {
                    $query->where('po_reference_no', '!=', null);
                })
                ->whereHas('supplier', function ($query) {
                    $query->when($this->company_name, function ($query) {
                        $query->where('company', 'like', '%' . $this->company_name . '%');
                    });
                })
                ->whereHas('canvassingSupplierItem', function ($query) {
                    $query->whereHas('supplierItem', function ($query) {
                        $query->when($this->item_name, function ($query) {
                            $query->where('name', 'like', '%' . $this->item_name . '%');
                        });
                    });
                })
                ->when($this->created_from, function ($query) {
                    $query->whereDate('created_at', '>=', $this->created_from);
                })
                ->when($this->created_to, function ($query) {
                    $query->whereDate('created_at', '<=', $this->created_to);
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
