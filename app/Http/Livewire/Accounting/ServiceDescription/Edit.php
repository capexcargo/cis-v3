<?php

namespace App\Http\Livewire\Accounting\ServiceDescription;

use App\Interfaces\Fims\DataManagement\ServiceDescriptionInterface;
use App\Models\FimsServiceCategory;
use App\Models\FimsServiceDescription;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
        use WithPagination, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public $confirmation_modal;
    
    public $servDesId;

    public $service_category_id;
    public $description;
    public $price;

    public $service_category_id_references = [];

    public function mount($id)
    {
        $this->servDesId = $id;
        $service_category_id_lists = FimsServiceDescription::with('ServCat')->findOrFail($id);
        
        $this->service_category_id = $service_category_id_lists['ServCat']['id'];
        $this->description = $service_category_id_lists->description;
        $this->price = $service_category_id_lists->price;
    }

    public function load(){
        $this->ServiceCategoryIdReference();
    }


    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('accounting.service-description.index', 'close_modal', 'edit');
        $this->confirmation_modal = false;
    }

    public function ServiceCategoryIdReference()
    {
        $this->service_category_id_references = FimsServiceCategory::get();
    }

    public function confirmationSubmit(ServiceDescriptionInterface $servicedes_interface)
    {
        $response = $servicedes_interface->updateValidation2($this->getRequest());
        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function submit(ServiceDescriptionInterface $servicedes_interface)
    {
        $response = $servicedes_interface->update2($this->getRequest(), $this->servDesId);

        if ($response['code'] == 200) {
            $this->emitTo('accounting.service-description.index', 'close_modal', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'service_category_id' => $this->service_category_id,
            'description' => $this->description,
            'price' => $this->price,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'service_category_id',
            'description',
            'price',
        ]);
    }
    public function render()
    {
        return view('livewire.accounting.service-description.edit');
    }
}
