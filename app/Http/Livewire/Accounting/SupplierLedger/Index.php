<?php

namespace App\Http\Livewire\Accounting\SupplierLedger;

use App\Models\Accounting\Supplier;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $view_modal = false;
    public $supplier_id;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $company;
    public $reference_no;
    public $requested_from;
    public $requested_to;

    public $supplier_references = [];
    public $selected = [];

    protected $listeners = ['purchase_order_management_index' => 'render', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadSupplierReference();
    }

    public function loadSupplierReference()
    {
        $this->supplier_references = Supplier::where('company', 'like', '%' . $this->company . '%')->take(10)->get();
    }

    public function updatedCompany()
    {
        $this->loadSupplierReference();
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'view') {
            $this->emit('supplier_ledger_view_mount', $data['id']);
            $this->view_modal = true;
        }

        $this->supplier_id = $data['id'];
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'view') {
            $this->view_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.supplier-ledger.index', [
            'suppliers' => Supplier::withCount(['payeeRequestForPayments' => function ($query) {
                $query->whereHas('checkVoucher');
            }, 'canvassingSupplierRequestForPayments'])
                ->withSum(['payeeRequestForPayments'  => function ($query) {
                    $query->whereHas('checkVoucher');
                }], 'amount')
                ->withSum('canvassingSupplierRequestForPayments', 'amount')
                ->whereHas('payeeRequestForPayments', function ($query) {
                    $query->whereHas('checkVoucher')
                        ->when($this->reference_no, function ($query) {
                            $query->where('reference_id', 'like', '%' . $this->reference_no . '%');
                        })->when($this->requested_from && $this->requested_to, function ($query) {
                            $query->whereDate('created_at', '>=', $this->requested_from)
                                ->whereDate('created_at', '<=', $this->requested_to);
                        });
                })->when($this->company, function ($query) {
                    $query->where('company', 'like', '%' . $this->company . '%');
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)

        ]);
    }
}
