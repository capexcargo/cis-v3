<?php

namespace App\Http\Livewire\Accounting\SupplierLedger;

use App\Models\Accounting\Supplier;
use Livewire\Component;

class View extends Component
{
    public $view_modal = false;
    public $purchase_order_management_view_modal = false;
    public $parent_reference_no;
    public $canvassing_supplier_id;

    public $supplier;

    protected $listeners = ['supplier_ledger_view_mount' => 'mount', 'close_modal' => 'closeModal'];

    public function mount($id)
    {
        $this->supplier = Supplier::with(['industry', 'type', 'payeeRequestForPayments' => function ($query) {
            $query->with('user')->whereHas('checkVoucher');
        }, 'canvassingSupplierRequestForPayments' => function ($query) {
            $query->with('canvassingSupplier');
        }])
            ->withSum(['payeeRequestForPayments'  => function ($query) {
                $query->whereHas('checkVoucher');
            }], 'amount')
            ->withSum('canvassingSupplierRequestForPayments', 'amount')
            ->find($id);

        abort_if(!$this->supplier, 404);
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'view') {
            $this->parent_reference_no = $data['parent_reference_no'];
            $this->emit('request_management_view_mount', $data['parent_reference_no']);
            $this->view_modal = true;
        } elseif ($action_type == 'purchase_order_view') {
            $this->canvassing_supplier_id = $data['canvassing_supplier_id'];
            $this->emit('purchar_order_management_view_mount', $data['canvassing_supplier_id']);
            $this->purchase_order_management_view_modal = true;
        }
    }

    public function render()
    {
        return view('livewire.accounting.supplier-ledger.view');
    }
}
