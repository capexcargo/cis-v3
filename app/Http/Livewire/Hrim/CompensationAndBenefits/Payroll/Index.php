<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\Payroll;

use App\Exports\Hrim\Payroll\Payrollgenerate as Payrollgenerate;
use App\Interfaces\Hrim\CompensationAndBenefits\PayrollInterface;
use App\Models\Hrim\CutOffManagement;
use App\Models\MonthReference;
use App\Models\UserDetails;
use App\Traits\Hrim\CompensationAndBenefits\PayrollTrait;
use App\Traits\PopUpMessagesTrait;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Maatwebsite\Excel\Facades\Excel;

class Index extends Component
{
    use PayrollTrait;
    use PopUpMessagesTrait;
    use ResponseTrait;

    public $action_type;

    public $month_references = [];
    public $year;
    public $month;
    public $cut_off;

    public $payrollss = [];
    public $selected = [];
    public $selecteds = [];
    public $selectAll = false;
    public $payroll_interface;

    public $payrollforcheckbox = [];


    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function mount()
    {
        $cutoff = CutOffManagement::where('cutoff_date', date('Y-m-d'))->first();

        if ($cutoff->payout_cutoff == 1) {
            $cutoffs = '1';
        } else {
            $cutoffs = '2';
        }

        $this->month = $cutoff->payout_month;
        $this->cut_off = $cutoffs;
        $this->year = date('Y');
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'generate_payroll_modal') {
            $this->generate_payroll_modal = false;
            $this->selectAll = false;
        } elseif ($action_type == 'gen_payslip_initial') {
            $this->confirmation_modal_initial = false;
            $this->generate_payslip_modal = false;
            $this->selectAll = false;
        } elseif ($action_type == 'gen_payslip_final') {
            $this->confirmation_modal_final = false;
            $this->generate_payslip_modal = false;
            $this->selectAll = false;
        }
    }


    public function updatedSelectAll($value)
    {
        if ($value) {

            $request = [
                'year' => $this->year,
                'month' => $this->month,
                'cut_off' => $this->cut_off,
                'branch' => $this->branch,
                'employee_id' => $this->employee_id,
                'employee_name' => $this->employee_name,
                'employee_category' => $this->employee_category,
                'employee_code' => $this->employee_code,
                'division' => $this->division,
                'job_rank' => $this->job_rank,
            ];


            $this->payrollforcheckbox = UserDetails::whereNotNull('employment_category_id')
                ->when($request['branch'] ?? false, function ($query) use ($request) {
                    $query->where('branch', $request['branch']);
                })
                ->when($request['employee_id'] ?? false, function ($query) use ($request) {
                    $query->where('employee_number', $request['employee_id']);
                })
                ->when($request['employee_category'] ?? false, function ($query) use ($request) {
                    $query->where('employment_category_id', $request['employee_category']);
                })
                ->when($request['employee_name'], function ($query) use ($request) {
                    $query->whereRaw('CONCAT(`first_name`, " " , `middle_name`, " ", `last_name`) LIKE "%' . $request['employee_name'] . '%"');
                    $query->orWhereRaw('CONCAT(`first_name`, " " , `last_name`) LIKE "%' . $request['employee_name'] . '%"');
                    $query->orWhere('first_name', 'like', '"%' . $request['employee_name'] . '%"');
                    $query->orWhere('middle_name', 'like', '"%' . $request['employee_name'] . '%"');
                    $query->orWhere('last_name', 'like', '"%' . $request['employee_name'] . '%"');
                })
                ->when($request['employee_id'] ?? false, function ($query) use ($request) {
                    $query->where('employee_number', $request['employee_id']);
                })
                ->when($request['division'] ?? false, function ($query) use ($request) {
                    $query->where('division_id', $request['division']);
                })
                ->when($request['job_rank'] ?? false, function ($query) use ($request) {
                    $query->where('job_level_id', $request['job_rank']);
                })->pluck('employee_number');

            $this->selected = $this->payrollforcheckbox;
        } else {
            $this->selected = [];
        }
    }

    public function closePayroll(PayrollInterface $payroll_interface)
    {

        $response = $payroll_interface->createHistory($this->selected);
        if ($response['code'] == 200) {
            $this->selected = [];
            $this->emitTo('hrim.compensation-and-benefits.payroll.index', 'close_modal', 'generate_payroll_modal');
            $this->emitTo('hrim.compensation-and-benefits.payroll.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function generatePayslipInitial(PayrollInterface $payroll_interface)
    {
        $response = $payroll_interface->generatePayslipInitial($this->selected);
        if ($response['code'] == 200) {
            $this->selected = [];
            $this->emitTo('hrim.compensation-and-benefits.payroll.index', 'close_modal', 'gen_payslip_initial');
            $this->emitTo('hrim.compensation-and-benefits.payroll.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function generatePayslipFinal(PayrollInterface $payroll_interface)
    {
        $response = $payroll_interface->generatePayslipInitial($this->selected);
        if ($response['code'] == 200) {
            $this->selected = [];
            $this->emitTo('hrim.compensation-and-benefits.payroll.index', 'close_modal', 'gen_payslip_final');
            $this->emitTo('hrim.compensation-and-benefits.payroll.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'generate_payroll') {
            $this->generate_payroll_modal = true;
            // dd($this->selected);
        } elseif ($action_type == 'generate_bankfile') {
            // $this->confirmation_modal = true;
            // $this->confirmation_message = "";
        } elseif ($action_type == 'generate_payslip') {
            $this->generate_payslip_modal = true;
        } elseif ($action_type == 'generate_payslip_initial') {
            $this->confirmation_modal_initial = true;
            $this->confirmation_message_initial = 'Are you sure you want to send the INITIAL PAYSLIP?';
        } elseif ($action_type == 'generate_payslip_final') {
            $this->confirmation_modal_final = true;
            $this->confirmation_message_final = 'Are you sure you want to send the FINAL PAYSLIP?';
        }
    }

    public function load()
    {
        $this->loadMonthReference();
    }

    public function loadMonthReference()
    {
        $this->month_references = MonthReference::get();
    }

    public function export()
    {

        $this->generate_payroll_modal = false;

        if ($this->cut_off == 1) {
            $begin = date('Y-m-d', strtotime($this->year . '-' . ($this->month - 1) . '-26'));
            $end = date('Y-m-d', strtotime($this->year . '-' . $this->month . '-10'));
        } else {
            $begin = date('Y-m-d', strtotime($this->year . '-' . ($this->month) . '-11'));
            $end = date('Y-m-d', strtotime($this->year . '-' . $this->month . '-25'));
        }

        $where = '';
        if ($this->branch != '') {
            $where .= " AND branch_id = '" . $this->branch . "'";
        }
        if ($this->employee_id != '') {
            $where .= " AND employee_number = '" . $this->employee_id . "'";
        }
        if ($this->employee_category != '') {
            $where .= " AND employment_category_id = '" . $this->employee_category . "'";
        }
        if ($this->employee_name != '') {
            $where .= " AND (first_name like '%" . $this->employee_name . "%' OR middle_name like '%" . $this->employee_name . "%' OR last_name like '%" . $this->employee_name . "%') ";
        }
        if ($this->employee_id != '') {
            $where .= " AND employee_number = '" . $this->employee_id . "'";
        }
        if ($this->division != '') {
            $where .= " AND a.division_id = '" . $this->division . "'";
        }
        if ($this->job_rank != '') {
            $where .= " AND a.job_level_id = '" . $this->job_rank . "'";
        }

        if ($this->year == '' || $this->month == '' || $this->cut_off == '') {
            $payrolls = [];
        } else {



            $payrolls =   DB::select('

            SELECT ' . $this->cut_off . ' as cut_off,a.user_id, b.photo_path, b.photo_name,h.description as divname, a.employee_number, a.first_name, a.last_name, a.hired_date, c.display AS emp_status, d.display AS job_rank, e.display AS position, f.display AS branch,
            g.basic_pay, g.cola, (g.basic_pay + g.cola) AS gross,ROUND((g.basic_pay / 26) / 8,2) as phour,
            
            (SELECT sum(ot.overtime_approved * (ROUND((g.basic_pay / 26) / 8,2) * bb.rate_percentage)) FROM hrim_overtime ot 
            LEFT JOIN hrim_date_category_reference bb ON bb.id = ot.date_category_id
            WHERE ot.user_id = a.user_id 
            AND ot.date_time_from >= "' . $begin . '" AND ot.date_time_from <= "' . $end . '") AS ot_pay,
            
            (SELECT round(SUM( tl.late * (ROUND((g.basic_pay / 26) / 8,2))), 2) FROM hrim_time_log tl WHERE tl.user_id = a.user_id  
            AND tl.date >= "' . $begin . '" AND tl.date <= "' . $end . '") AS tardiness,
            
            (SELECT SUM( tl.undertime * (ROUND((g.basic_pay / 26) / 8,2))) FROM hrim_time_log tl WHERE tl.user_id = a.user_id  
            AND tl.date >= "' . $begin . '" AND tl.date <= "' . $end . '") AS undertime,
            
            (SELECT SUM(bb.amount) FROM hrim_loan lns
            LEFT JOIN hrim_loan_details bb ON bb.loan_id = lns.id 
            where lns.user_id = a.user_id AND bb.month = "' . $this->month . '" AND bb.cutoff = "' . $this->cut_off . '" AND bb.year = "' . $this->year . '" ) AS loans,

            (SELECT CASE WHEN bb.cutoff = 2 THEN SUM(bb.amount) ELSE 0 END
            FROM hrim_loan lns
            LEFT JOIN hrim_loan_details bb ON bb.loan_id = lns.id 
            where lns.user_id = a.user_id AND bb.month = "' . $this->month . '" AND bb.cutoff = "' . $this->cut_off . '" AND bb.year = "' . $this->year . '" AND lns.`type` IN (2,3,4) AND lns.final_status = 3) as loansgovt,

            (SELECT COUNT(abs.id)  FROM hrim_absent abs WHERE abs.user_id = a.user_id AND 
            abs.absent_date >= "' . $begin . '" AND abs.absent_date <= "' . $end . '") AS absentcount,

            (SELECT round(sum(leaves.apply_for), 2) FROM hrim_leave_details leaves WHERE leaves.user_id = a.user_id AND leaves.leave_type = 0 AND leaves.leave_date >= "' . $begin . '" AND leaves.leave_date <= "' . $end . '") AS leaves,

            (SELECT round(sum(leaves.apply_for), 2) FROM hrim_leave_details leaves WHERE leaves.user_id = a.user_id AND leaves.leave_type = 1 AND leaves.leave_date >= "' . $begin . '" AND leaves.leave_date <= "' . $end . '") AS leaveswtpay,
            (SELECT CASE WHEN g.basic_pay <= 10000 THEN 350
            ELSE g.basic_pay * a.premium_rate END AS asd
            FROM hrim_philhealth a WHERE a.year = 2022) AS philhealth,
            
            (SELECT a.share
            FROM hrim_pagibig a WHERE a.year = 2022) AS pagibig,
           
            (SELECT SUM(htl.rendered_time) * (ROUND((g.basic_pay / 26) / 8,2))
            FROM hrim_rest_day rd 
            LEFT JOIN hrim_time_log htl on htl.user_id = rd.user_id where rd.date >= "' . $begin . '" and rd.date <= "' . $end . '"
            AND rd.user_id = a.user_id AND (case when rd.date = htl.date then 1 END) IS NOT null) as restdaypays,

            (SELECT 

            round(SUM(
            CONCAT(
            HOUR(
            TIMEDIFF( 
            (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) <= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT(htls.time_out_date, " ", htls.time_out)) 
            when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT((CURDATE()), " 06:00:00")) END), (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) then (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) END))) 
            ,".",
            MINUTE(
            TIMEDIFF( 
            (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) <= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT(htls.time_out_date, " ", htls.time_out)) 
            when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT((CURDATE()), " 06:00:00")) END), (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) then (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) END)))))
            * (ROUND((g.basic_pay / 26) / 8,2)), 2)
            
            FROM hrim_time_log htls WHERE htls.user_id = a.user_id AND htls.date >= "' . $begin . '" AND htls.date <= "' . $end . '") as ndval
            ,

            (SELECT 

            round(sum((g.basic_pay / 26) * otr.rate_percentage), 2) 
                        FROM hrim_holiday hl 
                        LEFT JOIN hrim_time_log htl on htl.date = hl.date 
                        LEFT JOIN user_details ud ON ud.user_id = htl.user_id
                        LEFT JOIN hrim_date_category_reference otr ON otr.id = hl.type_id
                     where htl.date >= "' . $begin . '" and htl.date <= "' . $end . '"
                     AND (hl.branch_id = a.branch_id OR hl.branch_id IS NULL) and htl.user_id = a.user_id) as holidayp,

            (SELECT SUM(dl.amount)
                     FROM hrim_dlb dl WHERE 
                     dl.month = "' . $this->month . '" 
                     AND dl.payout_cutoff = "' . $this->cut_off . '" 
                     AND dl.year = "' . $this->year . '"
                     AND dl.user_id = a.user_id) AS dlbpay,

            (SELECT SUM(aad.amount)
                     FROM hrim_admin_adjustment aad WHERE 
                     aad.month = "' . $this->month . '" 
                     AND aad.payout_cutoff = "' . $this->cut_off . '" 
                     AND aad.year = "' . $this->year . '"
                     AND aad.user_id = a.user_id 
                     AND aad.first_status = 3) AS aadpay
            
            
             
            
            FROM user_details a
            LEFT JOIN users b ON b.id = a.user_id
            LEFT JOIN hrim_employment_status c ON c.id = a.employment_status_id
            LEFT JOIN hrim_job_levels d ON d.id = a.job_level_id
            LEFT JOIN hrim_positions e ON e.id = a.position_id
            LEFT JOIN branch_reference f ON f.id = a.branch_id
            LEFT JOIN hrim_earnings g ON g.user_id = a.user_id
            LEFT JOIN division h ON h.id = a.division_id

            WHERE a.user_id is not null ' . $where . '
            
            
            order by a.last_name ASC
            ');
        }


        //  dd(compact('payrolls'));

        return Excel::download(new Payrollgenerate(compact('payrolls')), 'payroll.xlsx');
    }


    public function render(PayrollInterface $payroll_interface)
    {
        $request = [
            'year' => $this->year,
            'month' => $this->month,
            'cut_off' => $this->cut_off,
            'branch' => $this->branch,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee_name,
            'employee_category' => $this->employee_category,
            'employee_code' => $this->employee_code,
            'division' => $this->division,
            'job_rank' => $this->job_rank,
        ];

        $response = $payroll_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'payrolls' => [],
                ];
        }

        return view('livewire.hrim.compensation-and-benefits.payroll.index', [
            'payrolls' => $response['result']['payrolls'],
        ]);
    }
}
