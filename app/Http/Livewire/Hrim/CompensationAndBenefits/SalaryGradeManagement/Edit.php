<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\SalaryGradeManagement;

use App\Interfaces\Hrim\CompensationAndBenefits\SalaryGradeManagementInterface;
use App\Models\Hrim\SalaryGradeManagement;
use App\Traits\Hrim\CompensationAndBenefits\SalaryGradeManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use SalaryGradeManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['salary_grade_management_mount' => 'mount'];


    public function mount($id)
    {
        $this->resetForm();
        $this->salary_grade_management = SalaryGradeManagement::findOrFail($id);

        $this->grade = $this->salary_grade_management->grade;
        $this->minimun_amount = $this->salary_grade_management->minimun_amount;
        $this->maximum_amount = $this->salary_grade_management->maximum_amount;
    }

    public function submit(SalaryGradeManagementInterface $salary_grade_management_interface)
    {
        $response = $salary_grade_management_interface->update($this->getRequest(), $this->salary_grade_management->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.compensation-and-benefits.salary-grade-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.compensation-and-benefits.salary-grade-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.hrim.compensation-and-benefits.salary-grade-management.edit');
    }
}
