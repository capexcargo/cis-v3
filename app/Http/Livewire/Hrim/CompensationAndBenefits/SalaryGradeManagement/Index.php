<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\SalaryGradeManagement;

use App\Interfaces\Hrim\CompensationAndBenefits\SalaryGradeManagementInterface;
use App\Traits\Hrim\CompensationAndBenefits\SalaryGradeManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use SalaryGradeManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create') {
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('salary_grade_management_mount', $data['id']);
            $this->edit_modal = true;
            $this->salary_grade_management_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }


    public function render(SalaryGradeManagementInterface $salary_grade_management_interface)
    {
        $request = [];

        $response = $salary_grade_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'salary_grade_managements' => [],
                ];
        }
        return view('livewire.hrim.compensation-and-benefits.salary-grade-management.index', [
            'salary_grade_managements' => $response['result']['salary_grade_managements']
        ]);
    }
}
