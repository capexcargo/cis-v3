<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\SalaryGradeManagement;

use App\Interfaces\Hrim\CompensationAndBenefits\SalaryGradeManagementInterface;
use App\Traits\Hrim\CompensationAndBenefits\SalaryGradeManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{

    use SalaryGradeManagementTrait, PopUpMessagesTrait;

    protected $rules = [
        'grade' => 'required',
        'minimun_amount' => 'required',
        'maximum_amount' => 'required',
    ];

    public function confirmationSubmit()
    {
        $this->validate();
        $this->confirmation_modal = true;
        // $this->emitTo('hrim.payroll-management.holiday-management.index', 'close_modal', 'create');
    }

    public function submit(SalaryGradeManagementInterface $salary_grade_management_interface)
    {

        $response = $salary_grade_management_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.compensation-and-benefits.salary-grade-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.compensation-and-benefits.salary-grade-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->confirmation_modal = false;
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.compensation-and-benefits.salary-grade-management.create');
    }
}
