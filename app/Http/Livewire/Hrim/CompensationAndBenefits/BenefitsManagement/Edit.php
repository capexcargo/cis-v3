<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\BenefitsManagement;

use App\Interfaces\Hrim\CompensationAndBenefits\BenefitsManagementInterface;
use App\Models\Hrim\BenefitsManagement;
use App\Traits\Hrim\CompensationAndBenefits\BenefitsManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use BenefitsManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['benefits_management_mount' => 'mount'];


    public function mount($id)
    {
        $this->resetForm();
        $this->benefits_management = BenefitsManagement::findOrFail($id);

        $this->benefit_type = $this->benefits_management->benefit_type;
        $this->description = $this->benefits_management->description;
    }

    public function submit(BenefitsManagementInterface $benefit_management_interface)
    {
        $response = $benefit_management_interface->update($this->getRequest(), $this->benefits_management->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.compensation-and-benefits.benefits-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.compensation-and-benefits.benefits-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.hrim.compensation-and-benefits.benefits-management.edit');
    }
}
