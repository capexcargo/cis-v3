<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\BenefitsManagement;

use App\Interfaces\Hrim\CompensationAndBenefits\BenefitsManagementInterface;
use App\Models\Hrim\BenefitsManagement;
use App\Traits\Hrim\CompensationAndBenefits\BenefitsManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use BenefitsManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create') {
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('benefits_management_mount', $data['id']);
            $this->edit_modal = true;
            $this->benefits_management_id = $data['id'];
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this benefit?";
            $this->delete_modal = true;
            $this->benefits_management_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }


    public function confirm(BenefitsManagementInterface $benefits_management_interface)
    {
        if ($this->action_type == "delete") {
            $response = $benefits_management_interface->destroy($this->benefits_management_id);
        }
        if ($response['code'] == 200) {
            $this->emitTo('hrim.compensation-and-benefits.benefits-management.index', 'close_modal', 'delete');
            $this->emitTo('hrim.compensation-and-benefits.benefits-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(BenefitsManagementInterface $benefits_management_interface)
    {
        $request = [];

        $response = $benefits_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'benefits_managements' => [],
                ];
        }
        return view('livewire.hrim.compensation-and-benefits.benefits-management.index', [
            'benefits_managements' => $response['result']['benefits_managements']
        ]);
    }
}
