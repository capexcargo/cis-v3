<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\BenefitsManagement;

use App\Interfaces\Hrim\CompensationAndBenefits\BenefitsManagementInterface;
use App\Traits\Hrim\CompensationAndBenefits\BenefitsManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use BenefitsManagementTrait, PopUpMessagesTrait;

    protected $rules = [
        'benefit_type' => 'required',
        'description' => 'required',
    ];

    public function confirmationSubmit()
    {
        $this->validate();
        $this->confirmation_modal = true;
        // $this->emitTo('hrim.payroll-management.holiday-management.index', 'close_modal', 'create');
    }

    public function submit(BenefitsManagementInterface $benefits_management_interface)
    {

        $response = $benefits_management_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.compensation-and-benefits.benefits-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.compensation-and-benefits.benefits-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->confirmation_modal = false;
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.compensation-and-benefits.benefits-management.create');
    }
}
