<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\Payslips;

use App\Interfaces\Hrim\CompensationAndBenefits\PayrollInterface;
use App\Traits\Hrim\CompensationAndBenefits\PayslipsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class View extends Component
{

    use PayslipsTrait, PopUpMessagesTrait;


    public $year;
    public $month;
    public $cut_off;
    public $id2;
    public $details = [];
    public $first_name;


    protected $listeners = ['view' => 'mount'];

    public function mount(PayrollInterface $payroll_interface, $id, array $request = [])
    {

        $response = $payroll_interface->index2($id, $request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }
        $this->details = $response['result']['payrolls'] ?? [];
        // $this->first_name = $this->details->first_name;
        // dd($this->details);
    }

    public function render()
    {
        return view('livewire.hrim.compensation-and-benefits.payslips.view');
    }
}
