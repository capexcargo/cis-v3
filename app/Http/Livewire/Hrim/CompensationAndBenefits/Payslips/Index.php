<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\Payslips;

use App\Interfaces\Hrim\CompensationAndBenefits\PayrollInterface;
use App\Models\MonthReference;
use App\Traits\Hrim\CompensationAndBenefits\PayslipsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use PayslipsTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'view_payslip') {
            $this->emitTo('hrim.compensation-and-benefits.payslips.view', 'view', $data['id'], $this->getRequest());
            $this->user_id = $data['id'];
            // dd($this->getRequest());
            $this->view_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'view') {
            $this->view_modal = false;
        }
    }

    public function render(PayrollInterface $payroll_interface)
    {
        $request = [
            'year' => $this->year,
            'month' => $this->month,
            'cut_off' => $this->cut_off,
            'branch' => $this->branch,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee_name,
            'employee_category' => $this->employee_category,
            'employee_code' => $this->employee_code,
            'division' => $this->division,
            'job_rank' => $this->job_rank,
            'paginate' => $this->paginate,
        ];

        $response = $payroll_interface->index($request);
        // dd($response);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'payrolls' => [],
                ];
        }
        return view('livewire.hrim.compensation-and-benefits.payslips.index', [
            'payrolls' => $response['result']['payrolls']
        ]);
    }
}
