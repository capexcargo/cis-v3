<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\Payslips;

use App\Interfaces\Hrim\CompensationAndBenefits\PayrollInterface;
use App\Traits\Hrim\CompensationAndBenefits\PayslipsTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class MyPayslip extends Component
{
    use PopUpMessagesTrait, PayslipsTrait;

    public function render(PayrollInterface $payroll_interface)
    {


        $response = $payroll_interface->index2(Auth::user()->id, $this->getRequest());
        // dd($response);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'payrolls' => [],
                ];
        }
        // dd($response);
        return view('livewire.hrim.compensation-and-benefits.payslips.my-payslip', [
            'payrolls' => $response['result']['payrolls']
        ]);
    }
}
