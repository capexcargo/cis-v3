<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\LoansAndLedgerMonitoring;

use App\Interfaces\Hrim\CompensationAndBenefits\LoansAndLedgerInterface;
use Livewire\Component;

class ViewApproval extends Component
{
    public $final_status;
    public $third_status;
    public $third_approver;

    protected $listeners = ['view_approval' => 'mount'];

    public function mount(LoansAndLedgerInterface $loans_and_ledger_interface, $id)
    {
        $response = $loans_and_ledger_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->loans_and_ledger = $response['result'];
        $this->final_status = $this->loans_and_ledger->final_status;
        $this->third_status = $this->loans_and_ledger->third_status;
        $this->third_approver = $this->loans_and_ledger->third_approver;
    }

    public function render()
    {
        return view('livewire.hrim.compensation-and-benefits.loans-and-ledger-monitoring.view-approval');
    }
}
