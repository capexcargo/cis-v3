<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\LoansAndLedgerMonitoring;

use App\Interfaces\Hrim\CompensationAndBenefits\LoansAndLedgerInterface;
use App\Traits\Hrim\CompensationAndBenefits\LoansAndLedgerTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Index extends Component
{
    use LoansAndLedgerTrait, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'view_loan') {
            $this->emitTo('hrim.compensation-and-benefits.loans-and-ledger-monitoring.view', 'view', $data['id']);
            $this->loans_and_ledger_id = $data['id'];
            $this->view_loan_modal = true;
        } else if ($action_type == 'view_approval') {
            $this->emitTo('hrim.compensation-and-benefits.loans-and-ledger-monitoring.view-approval', 'view_approval', $data['id']);
            $this->loans_and_ledger_id = $data['id'];
            $this->view_approval_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'view') {
            $this->view_loan_modal = false;
        } else if ($action_type == 'approve') {
            $this->confirmation_approval_modal = false;
            $this->view_loan_modal = false;
        } else if ($action_type == 'decline') {
            $this->confirmation_decline_modal = false;
            $this->view_loan_modal = false;
        }
    }

    public function render(LoansAndLedgerInterface $loans_and_ledger_interface)
    {
        $request = [
            'branch' => $this->branch,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee_name,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $loans_and_ledger_interface->index2($request);
        // dd($response);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.compensation-and-benefits.loans-and-ledger-monitoring.index', [
            'loans_and_ledger' => $response['result']
        ]);
    }
}
