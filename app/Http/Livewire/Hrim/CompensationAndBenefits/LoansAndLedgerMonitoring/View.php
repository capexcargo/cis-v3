<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\LoansAndLedgerMonitoring;

use App\Interfaces\Hrim\CompensationAndBenefits\LoansAndLedgerInterface;
use App\Models\Hrim\LoansAndLedger;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\Hrim\CompensationAndBenefits\LoansAndLedgerTrait;
use App\Traits\PopUpMessagesTrait;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class View extends Component
{
    use LoansAndLedgerTrait, PopUpMessagesTrait, ResponseTrait;

    protected $listeners = ['view' => 'mount'];

    public $decline_reason;
    public $approved_amount;

    public $approver;

    public $divsionhead;
    public $divsionhead_id;
    public $seg = [];
    public $seg_id = [];

    public $loans_and_ledgers = [];

    public function mount($id)
    {
        $loans = LoansAndLedger::get();
        foreach ($loans as $loan) {
            $this->loan_user_id = $loan->user_id;
        }
        $user = User::findOrFail($id);
        $this->employee_name = $user->name;
        $this->user_id = $user->id;

        $this->loans_and_ledgers = User::with('loans')->where('id', $id)->get();

        // dd($this->loans_and_ledgers);
        $this->divsionhead = UserDetails::whereHas('position', function ($query) {
            $query->whereIn('job_level_id', [1, 2])
                ->where('division_id', Auth::user()->division_id);
        })->select('user_id')->first();

        $this->divsionhead_id = $this->divsionhead["user_id"];

        $this->seg = UserDetails::whereHas('position', function ($query) {
            $query->whereIn('job_level_id', [1])
                ->where('division_id', 8);
        })->get();

        foreach ($this->seg as $val) {
            $this->seg_id[] = $val['user_id'];
        }
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'confirm_approve') {
            $this->confirmation_message = "Are you sure you want to approve this loan request?";
            $this->confirmation_approval_modal = true;
            $this->loans_and_ledger_id = $data['id'];
            $this->approver = $data['approver'];

            // dd($data['approver']);

        } else if ($action_type == 'confirm_decline') {
            $this->confirmation_message = "Are you sure you want to decline this loan request?";
            $this->confirmation_decline_modal = true;
            $this->loans_and_ledger_id = $data['id'];
            $this->approver = $data['approver'];
        }
    }

    public function confirm(LoansAndLedgerInterface $loans_and_ledger_interface, array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'approve_loan') {
            $response = $loans_and_ledger_interface->approve($this->loans_and_ledger_id, $this->approved_amount, $this->approver);
            // dd($this->loans_and_ledger_id);
            if ($response['code'] == 200) {
                $this->emitTo('hrim.compensation-and-benefits.loans-and-ledger-monitoring.index', 'close_modal', 'approve');
                $this->emitTo('hrim.compensation-and-benefits.loans-and-ledger-monitoring.index', 'approve');
                $this->sweetAlert('', $response['message']);
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } else if ($action_type == 'decline_loan') {

            $response = $loans_and_ledger_interface->decline($this->loans_and_ledger_id, $this->getConfirmationRequest(), $this->approver);

            if ($response['code'] == 200) {
                $this->emitTo('hrim.compensation-and-benefits.loans-and-ledger-monitoring.index', 'close_modal', 'decline');
                $this->emitTo('hrim.compensation-and-benefits.loans-and-ledger-monitoring.index', 'decline');
                $this->sweetAlert('', $response['message']);
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function render()
    {
        return view('livewire.hrim.compensation-and-benefits.loans-and-ledger-monitoring.view');
    }
}
