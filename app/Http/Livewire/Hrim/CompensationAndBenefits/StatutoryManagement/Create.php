<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\StatutoryManagement;

use App\Interfaces\Hrim\CompensationAndBenefits\StatutoryManagementInterface;
use App\Traits\Hrim\CompensationAndBenefits\StatutoryManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use StatutoryManagementTrait, PopUpMessagesTrait;

    protected $rules = [
        'statutor_benefit_type' => 'required',
        'description' => 'required',
    ];

    public function confirmationSubmit()
    {
        $this->validate();
        $this->confirmation_modal = true;
        // $this->emitTo('hrim.payroll-management.holiday-management.index', 'close_modal', 'create');
    }

    public function submit(StatutoryManagementInterface $statutory_management_interface)
    {

        $response = $statutory_management_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.compensation-and-benefits.statutory-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.compensation-and-benefits.statutory-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->confirmation_modal = false;
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.compensation-and-benefits.statutory-management.create');
    }
}
