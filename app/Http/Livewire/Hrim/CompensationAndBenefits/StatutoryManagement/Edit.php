<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\StatutoryManagement;

use App\Interfaces\Hrim\CompensationAndBenefits\StatutoryManagementInterface;
use App\Models\Hrim\StatutoryManagement;
use App\Traits\Hrim\CompensationAndBenefits\StatutoryManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use StatutoryManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['statutory_management_mount' => 'mount'];


    public function mount($id)
    {
        $this->resetForm();
        $this->statutory_management = StatutoryManagement::findOrFail($id);

        $this->statutor_benefit_type = $this->statutory_management->statutor_benefit_type;
        $this->description = $this->statutory_management->description;
    }

    public function submit(StatutoryManagementInterface $statutory_management_interface)
    {
        $response = $statutory_management_interface->update($this->getRequest(), $this->statutory_management->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.compensation-and-benefits.statutory-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.compensation-and-benefits.statutory-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.hrim.compensation-and-benefits.statutory-management.edit');
    }
}
