<?php

namespace App\Http\Livewire\Hrim\CompensationAndBenefits\StatutoryManagement;

use App\Interfaces\Hrim\CompensationAndBenefits\StatutoryManagementInterface;
use App\Traits\Hrim\CompensationAndBenefits\StatutoryManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use StatutoryManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create') {
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('statutory_management_mount', $data['id']);
            $this->edit_modal = true;
            $this->statutory_management_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }


    public function render(StatutoryManagementInterface $statutory_management_interface)
    {
        $request = [];

        $response = $statutory_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'statutory_managements' => [],
                ];
        }
        return view('livewire.hrim.compensation-and-benefits.statutory-management.index', [
            'statutory_managements' => $response['result']['statutory_managements']
        ]);
    }
}
