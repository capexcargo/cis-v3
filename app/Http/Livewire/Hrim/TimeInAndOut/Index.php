<?php

namespace App\Http\Livewire\Hrim\TimeInAndOut;

use App\Interfaces\Hrim\TimeInAndOutInterface;
use App\Models\Hrim\TimeLog;
use App\Repositories\Hrim\TimeInAndOutRepository;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Index extends Component
{
    use PopUpMessagesTrait;

    public $image;

    public $time_log;
    public $confirmation_modal = false;

    protected $listeners = ['time_in' => 'timeIn', 'time_out' => 'timeOut'];

    public function mount(TimeInAndOutInterface $time_in_and_out_interface)
    {
        // $this->time_log = TimeLog::where('user_id', Auth::user()->id)
        //     ->whereDate('date', now())
        //     ->first();
        // dd('shit');
        $response = $time_in_and_out_interface->currentTimelog();
        // dd($response);
        abort_if($response['code'] != 200, $response['code'], $response['message']);
        $this->time_log = $response['result'];
    }

    public function timeIn($image)
    {
        $this->image = $image;

        $request = [
            'image' => $this->image
        ];

        $time_in_and_out_repository = new TimeInAndOutRepository();
        $response = $time_in_and_out_repository->timeIn($request);

        if ($response['code'] == 200) {
            $this->time_log = $response['result'];
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 409) {
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function confirm()
    {
        $time_in_and_out_repository = new TimeInAndOutRepository();
        $response = $time_in_and_out_repository->CheckIfAlreadyTimeOut();

        // dd($response);
        if ($response['result']) {
            $this->sweetAlert('', 'Duplicate Time Out');
        } else {
            $this->confirmation_modal = true;
        }
    }

    public function timeOut($image)
    {
        $this->confirmation_modal = false;

        $this->image = $image;

        $request = [
            'image' => $this->image
        ];

        $time_in_and_out_repository = new TimeInAndOutRepository();
        $response = $time_in_and_out_repository->timeOut($request);

        if ($response['code'] == 200) {
            $this->time_log = $response['result'];
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 409) {
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.time-in-and-out.index');
    }
}
