<?php

namespace App\Http\Livewire\Hrim\Csp\CspManagement;

use App\Interfaces\Hrim\Csp\CspManagementInterface;
use App\Traits\Hrim\Csp\CspManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use CspManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create') {
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('csp_management_mount', $data['id']);
            $this->edit_modal = true;
            $this->csp_management_id = $data['id'];
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this CSP?";
            $this->delete_modal = true;
            $this->csp_management_id = $data['id'];
        } else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->activate_modal = true;
                $this->confirmation_message = "Are you sure you want to activate this CSP?";
                $this->csp_management_id = $data['id'];
                $this->is_deactivated = false;
            } else {
                $this->deactivate_modal = true;
                $this->confirmation_message = "Are you sure you want to deactivate this CSP?";
                $this->csp_management_id = $data['id'];
                $this->is_deactivated = true;
            }
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        } elseif ($action_type == 'update_status') {
            $this->activate_modal = false;
            $this->deactivate_modal = false;
        }
    }


    public function updateStatus($id, $status, CspManagementInterface $csp_management_interface)
    {
        // dd($id, $status);
        $response = $csp_management_interface->updateStatus($id, $status);

        if ($response['code'] == 200) {
            $this->emitTo('hrim.csp.csp-management.index', 'close_modal', 'update_status');
            $this->emitTo('hrim.csp.csp-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function confirm(CspManagementInterface $csp_management_interface)
    {
        if ($this->action_type == "delete") {
            $response = $csp_management_interface->destroy($this->csp_management_id);
        }
        if ($response['code'] == 200) {
            $this->emitTo('hrim.csp.csp-management.index', 'close_modal', 'delete');
            $this->emitTo('hrim.csp.csp-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(CspManagementInterface $csp_management_interface)
    {

        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $csp_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'csp_managements' => [],
                ];
        }
        return view('livewire.hrim.csp.csp-management.index', [
            'csp_managements' => $response['result']['csp_managements']
        ]);
    }
}
