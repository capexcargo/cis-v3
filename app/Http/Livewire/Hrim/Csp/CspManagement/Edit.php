<?php

namespace App\Http\Livewire\Hrim\Csp\CspManagement;

use App\Interfaces\Hrim\Csp\CspManagementInterface;
use App\Models\Hrim\Agencies;
use App\Traits\Hrim\Csp\CspManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use CspManagementTrait, PopUpMessagesTrait;


    protected $listeners = ['csp_management_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->csp_management = Agencies::findOrFail($id);

        $this->name = $this->csp_management->name;
        $this->branch_id = $this->csp_management->branch_id;
    }


    public function submit(CspManagementInterface $csp_management_interface)
    {
        $response = $csp_management_interface->update($this->getRequest(), $this->csp_management->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.csp.csp-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.csp.csp-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.csp.csp-management.edit');
    }
}
