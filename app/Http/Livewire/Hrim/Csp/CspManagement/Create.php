<?php

namespace App\Http\Livewire\Hrim\Csp\CspManagement;

use App\Interfaces\Hrim\Csp\CspManagementInterface;
use App\Traits\Hrim\Csp\CspManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use CspManagementTrait, WithFileUploads, PopUpMessagesTrait;

    protected $rules = [
        'name' => 'required',
        'branch_id' => 'required',
    ];


    public function confirmationSubmit()
    {
        $this->validate();
        $this->confirmation_modal = true;
        // $this->emitTo('hrim.payroll-management.holiday-management.index', 'close_modal', 'create');
    }

    public function submit(CspManagementInterface $hcsp_management_interface)
    {

        $response = $hcsp_management_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.csp.csp-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.csp.csp-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render()
    {
        return view('livewire.hrim.csp.csp-management.create');
    }
}
