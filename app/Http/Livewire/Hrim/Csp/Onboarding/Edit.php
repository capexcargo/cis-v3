<?php

namespace App\Http\Livewire\Hrim\Csp\Onboarding;

use App\Interfaces\Hrim\EmployeeManagement\EmployeeInformationInterface;
use App\Traits\Hrim\EmployeeManagement\EmployeeInformantionTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use EmployeeInformantionTrait, PopUpMessagesTrait, WithFileUploads;

    protected $listeners = ['mount' => 'mount'];

    public function mount(EmployeeInformationInterface $employee_information_interface, $id)
    {
        $this->resetForm();
        $response = $employee_information_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->user = $response['result'];
        $this->employee_id = $this->user->userDetails->employee_number;
        $this->branch = $this->user->userDetails->branch_id;
        $this->first_name = $this->user->userDetails->first_name;
        $this->middle_name = $this->user->userDetails->middle_name;
        $this->last_name = $this->user->userDetails->last_name;
        $this->position = $this->user->userDetails->position_id;
        $this->job_level = $this->user->userDetails->job_level_id;
        $this->department = $this->user->userDetails->department_id;
        $this->division = $this->user->userDetails->division_id;
        $this->employement_category = $this->user->userDetails->employment_category_id;
        $this->employement_status = $this->user->userDetails->employment_status_id;
        $this->gender = $this->user->userDetails->gender_id;
        $this->agency = $this->user->userDetails->agency_id;
        $this->date_of_birth = $this->user->userDetails->birth_date;
        $this->date_hired = $this->user->userDetails->hired_date;
        $this->current_address = $this->user->userDetails->current_address;
        $this->permanent_address = $this->user->userDetails->permanent_address;
        $this->personal_mobile_number = $this->user->userDetails->personal_mobile_number;
        $this->personal_telephone_number = $this->user->userDetails->personal_telephone_number;
        $this->personal_email_address = $this->user->userDetails->personal_email;

        $this->company_mobile_number = $this->user->userDetails->company_mobile_number;
        $this->company_telephone_number = $this->user->userDetails->company_telephone_number;
        $this->company_email_address = $this->user->userDetails->company_email;

        $this->work_schedule = $this->user->userDetails->schedule_id;

        if ($this->user->governmentInformation) {
            $this->sss = $this->user->governmentInformation->sss_no;
            $this->pagibig_number = $this->user->governmentInformation->pagibig_no;
            $this->philhealth_number = $this->user->governmentInformation->philhealth_no;
            $this->tin_number = $this->user->governmentInformation->tin_no;
        }

        if ($this->user->edutcationalAttainment) {
            $this->educational_level = $this->user->edutcationalAttainment->ea_level_id;
            $this->educational_institution = $this->user->edutcationalAttainment->educational_institution;
            $this->educational_inclusive_year_from = $this->user->edutcationalAttainment->inclusive_year_from;
            $this->educational_inclusive_year_to = $this->user->edutcationalAttainment->inclusive_year_to;
        }

        if ($this->user->employmentRecord) {
            $this->employment_employer = $this->user->employmentRecord->employer;
            $this->employment_position = $this->user->employmentRecord->position;
            $this->employment_inclusive_year_from = $this->user->employmentRecord->inclusive_year_from;
            $this->employment_inclusive_year_to = $this->user->employmentRecord->inclusive_year_to;
        }

        if ($this->user->characterReference) {
            $this->character_reference_name = $this->user->characterReference->name;
            $this->character_reference_position = $this->user->characterReference->position;
            $this->character_reference_company = $this->user->characterReference->company;
            $this->character_reference_mobile_number = $this->user->characterReference->mobile_number;
            $this->character_reference_telephone_number = $this->user->characterReference->telephone_number;
        }

        if ($this->user->emergencyContactDetails) {
            $this->emergency_contact_name = $this->user->emergencyContactDetails->name;
            $this->emergency_contact_relationship = $this->user->emergencyContactDetails->ecd_relationship_id;
            $this->emergency_contact_address = $this->user->emergencyContactDetails->address;
            $this->emergency_contact_mobile_number = $this->user->emergencyContactDetails->mobile_number;
            $this->emergency_contact_telephone_number = $this->user->emergencyContactDetails->telephone_number;
        }

        if ($this->user->familyInformation) {
            $this->family_mother_maiden_name = $this->user->familyInformation->mothers_maiden_name;
            $this->family_father_name = $this->user->familyInformation->fathers_name;
            foreach ($this->user->familyInformation->siblings as $sibling) {
                $this->family_siblings[] = [
                    'id' => $sibling->id,
                    'name' => $sibling->name,
                    'is_deleted' => false,
                ];
            }
            $this->family_spouse = $this->user->familyInformation->spouse;
            foreach ($this->user->familyInformation->childrens as $children) {
                $this->family_childrens[] = [
                    'id' => $children->id,
                    'name' => $children->name,
                    'is_deleted' => false,
                ];
            }
        }


        if ($this->user->earnings) {
            $this->earnings_basic_pay = $this->user->earnings->basic_pay;
            $this->earnings_cola = $this->user->earnings->cola;
            $this->earnings_gross_pay = $this->user->earnings->gross_pay;
        }
    }

    public function submit(EmployeeInformationInterface $employee_information_interface)
    {
        $response = $employee_information_interface->update($this->user->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.csp.onboarding.index', 'close_modal', 'edit');
            $this->emitTo('hrim.csp.onboarding.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.csp.onboarding.edit');
    }
}
