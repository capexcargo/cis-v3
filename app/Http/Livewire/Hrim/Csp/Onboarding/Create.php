<?php

namespace App\Http\Livewire\Hrim\Csp\Onboarding;

use App\Interfaces\Hrim\EmployeeManagement\EmployeeInformationInterface;
use App\Traits\Hrim\EmployeeManagement\EmployeeInformantionTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use EmployeeInformantionTrait, PopUpMessagesTrait, WithFileUploads;

    public function mount()
    {
        $this->employement_category = 3;
    }

    public function submit(EmployeeInformationInterface $employee_information_interface)
    {
        $response = $employee_information_interface->create($this->getRequest());
        // dd($response);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.csp.onboarding.index', 'close_modal', 'create');
            $this->emitTo('hrim.csp.onboarding.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.csp.onboarding.create');
    }
}
