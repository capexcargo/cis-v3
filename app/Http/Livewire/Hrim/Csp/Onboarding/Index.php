<?php

namespace App\Http\Livewire\Hrim\Csp\Onboarding;

use App\Interfaces\Hrim\EmployeeManagement\EmployeeInformationInterface;
use App\Models\AccountStatusReference;
use App\Models\Hrim\Position;
use App\Models\User;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $confirmation_modal = false;
    public $confirmation_message;
    public $action_type;

    public $create_modal = false;
    public $edit_modal = false;
    public $user_id;

    public $employee_name;
    public $position;
    public $status;
    public $employment_categories = [3];
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $position_references = [];
    public $status_references = [];

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadPositionReference();
        $this->loadStatusReference();
    }

    public function loadPositionReference()
    {
        $this->position_references = Position::get();
    }

    public function loadStatusReference()
    {
        $this->status_references = AccountStatusReference::get();
    }

    public function action(array $data, $action_type)
    {
        $this->user_id = $data['id'];
        $this->action_type = $action_type;
        if ($action_type == 'edit') {
            $this->emitTo('hrim.csp.onboarding.edit', 'mount', $data['id']);
            $this->edit_modal = true;
        } elseif ($action_type == 'confirmation_deactivate') {
            $user = User::find($data['id']);
            $this->confirmation_message = "Are you sure you want to deactivate " . $user->name . "?";
            $this->confirmation_modal = true;
        } elseif ($action_type == 'confirmation_reactivate') {
            $user = User::find($data['id']);
            $this->confirmation_message = "Are you sure you want to reactivate " . $user->name . "?";
            $this->confirmation_modal = true;
        }
    }

    public function confirm(EmployeeInformationInterface $employee_information_interface)
    {
        if ($this->action_type == 'confirmation_deactivate') {
            $response = $employee_information_interface->updateStatus($this->user_id, 3);
        } elseif ($this->action_type == 'confirmation_reactivate') {
            $response = $employee_information_interface->updateStatus($this->user_id, 1);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.csp.onboarding.index', 'close_modal', 'confirmation');
            $this->emitTo('hrim.csp.onboarding.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'confirmation') {
            $this->confirmation_modal = false;
        }
    }

    public function render(EmployeeInformationInterface $employee_information_interface)
    {
        $request = [
            'employee_name' => $this->employee_name,
            'position' => $this->position,
            'status' => $this->status,
            'employment_categories' => $this->employment_categories,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $employee_information_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [
                'employee_informations' => []
            ];
        }

        return view('livewire.hrim.csp.onboarding.index', [
            'employee_informations' => $response['result']['employee_informations']
        ]);
    }
}
