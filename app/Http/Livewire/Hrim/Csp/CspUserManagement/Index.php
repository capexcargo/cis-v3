<?php

namespace App\Http\Livewire\Hrim\Csp\CspUserManagement;

use App\Interfaces\Hrim\Csp\CspUserManagementInterface;
use App\Models\Hrim\CspUserManagement;
use App\Traits\Hrim\Csp\CspUserManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use CspUserManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_csp_user_management') {
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('csp_user_management_mount', $data['id']);
            $this->edit_modal = true;
            $this->csp_user_management_id = $data['id'];
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this CSP User Admin?";
            $this->delete_modal = true;
            $this->csp_user_management_id = $data['id'];
        } else if ($action_type == 'update_status') {
            if ($data['status'] == 1) {
                $this->activate_modal = true;
                $this->confirmation_message = "Are you sure you want to activate this CSP User Admin?";
                $this->csp_user_management_id = $data['id'];
                $this->is_deactivated = false;
            } else {
                $this->deactivate_modal = true;
                $this->confirmation_message = "Are you sure you want to deactivate this CSP User Admin?";
                $this->csp_user_management_id = $data['id'];
                $this->is_deactivated = true;
            }
        }
    }

    public function delete(CspUserManagementInterface $CspUserManagementInterface)
    {

        if ($this->action_type == 'delete') {
            $response = $CspUserManagementInterface->destroy($this->csp_user_management_id);
        }

        if ($response['code'] == 200) {
            $this->delete_modal = false;
            $this->emitTo('hrim.csp.csp-user-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'update_status') {
            $this->activate_modal = false;
            $this->deactivate_modal = false;
        }
    }

    public function updateStatus($id, $status, CspUserManagementInterface $csp_user_management_interface)
    {
        // dd($id, $status);
        $response = $csp_user_management_interface->updateStatus($id, $status);

        if ($response['code'] == 200) {
            $this->emitTo('hrim.csp.csp-user-management.index', 'close_modal', 'update_status');
            $this->emitTo('hrim.csp.csp-user-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(CspUserManagementInterface $csp_user_management_interface)
    {
        $request = [
            'agency_id' => $this->agency_id,
            'branch_id' => $this->branch_id,
            'agency_contact_person' => $this->agency_contact_person,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $csp_user_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'csp_user_managements' => [],
                ];
        }
        return view('livewire.hrim.csp.csp-user-management.index', [
            'csp_user_managements' => $response['result']['csp_user_managements']
        ]);
    }
}
