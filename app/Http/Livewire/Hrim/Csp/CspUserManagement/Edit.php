<?php

namespace App\Http\Livewire\Hrim\Csp\CspUserManagement;

use App\Interfaces\Hrim\Csp\CspUserManagementInterface;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\Hrim\Csp\CspUserManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{

    use CspUserManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['csp_user_management_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->csp_user_management = UserDetails::findOrFail($id);
        $this->csp_user_management_users = User::findOrFail($this->csp_user_management->user_id);

        $this->agency_id = $this->csp_user_management->agency_id;
        $this->branch_id = $this->csp_user_management->branch_id;
        $this->agency_contact_person = $this->csp_user_management->agency_contact_person;
        $this->agency_contact_no = $this->csp_user_management->agency_contact_no;
        $this->agency_position = $this->csp_user_management->agency_position;
        $this->personal_email = $this->csp_user_management->personal_email;
    }

    public function submit(CspUserManagementInterface $csp_user_management_interface)
    {

        if ($this->password) {
            $validaterequest = [
                'agency_id' => 'required',
                'branch_id' => 'required',
                'agency_contact_person' => 'required',
                'agency_contact_no' => 'required|numeric|digits:11',
                'agency_position' => 'required',
                'personal_email' => 'required|email:rfc,dns|unique:users,email,' . $this->csp_user_management_users->id . ',id',

                'password' => 'required|confirmed|string|min:8',
                'password_confirmation' => 'required',
            ];
        } else {
            $validaterequest = [
                'agency_id' => 'required',
                'branch_id' => 'required',
                'agency_contact_person' => 'required',
                'agency_contact_no' => 'required|numeric|digits:11',
                'agency_position' => 'required',
                'personal_email' => 'required|email:rfc,dns|unique:users,email,' . $this->csp_user_management_users->id . ',id',
            ];
        }



        $validated = $this->validate($validaterequest);

        $response = $csp_user_management_interface->update($this->csp_user_management->id, $validated, $this->password);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.csp.csp-user-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.csp.csp-user-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.csp.csp-user-management.edit');
    }
}
