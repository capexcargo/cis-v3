<?php

namespace App\Http\Livewire\Hrim\Csp\CspUserManagement;

use App\Interfaces\Hrim\Csp\CspUserManagementInterface;
use App\Traits\Hrim\Csp\CspUserManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{

    use CspUserManagementTrait;
    use WithFileUploads;
    use PopUpMessagesTrait;

    protected $rules = [
        'agency_id' => 'required',
        'branch_id' => 'required',
        'agency_contact_person' => 'required',
        'agency_contact_no' => 'required|numeric|digits:11',
        'agency_position' => 'required',
        'personal_email' => 'required|email:rfc,dns',
        'password' => 'required|confirmed|string|min:8',
        'password_confirmation' => 'required',
    ];

    public function confirmationSubmit()
    {
        $this->validate();
        $this->confirmation_modal = true;
        // $this->emitTo('hrim.payroll-management.holiday-management.index', 'close_modal', 'create');
    }


    public function submit(CspUserManagementInterface $csp_user_management_interface)
    {

      
        $response = $csp_user_management_interface->create($this->getRequest());
        // dd($response);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.csp.csp-user-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.csp.csp-user-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.csp.csp-user-management.create');
    }
}
