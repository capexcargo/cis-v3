<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\DailyTimeRecords;

use App\Interfaces\Hrim\EmployeeAttendance\DailyTimeRecordsInterface;
use App\Models\Hrim\CutOffManagement;
use App\Models\MonthReference;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    use PopUpMessagesTrait;

    public $create_modal = false;
    public $view_modal = false;
    public $time_log_id;
    public $tar_date;

    public $month;
    public $cut_off;
    public $year;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;
    public $payout_months;
    public $month_references = [];

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function mount()
    {
        $cutoff = CutOffManagement::where('cutoff_date', date('Y-m-d'))->first();

        if ($cutoff->payout_cutoff == 1) {
            $cutoffs = 'first';
        } else {
            $cutoffs = 'second';
        }

        $this->month = $cutoff->payout_month;
        $this->cut_off = $cutoffs;
        $this->year = date('Y');
    }

    public function load()
    {
        $this->loadMonthReference();
    }

    public function loadMonthReference()
    {
        $this->month_references = MonthReference::get();
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'create') {
            $this->emitTo('hrim.employee-attendance.tar.update-or-create', 'mount', $data['time_log_id'], $data['date']);
            $this->time_log_id = $data['time_log_id'];
            $this->tar_date = $data['date'];
            $this->create_modal = true;
        } elseif ($action_type == 'view') {
            $this->emitTo('hrim.employee-attendance.tar.view', 'mount', $data['date']);
            $this->tar_date = $data['date'];
            $this->view_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'view') {
            $this->view_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render(DailyTimeRecordsInterface $daily_time_records)
    {
        $request = [
            'month' => $this->month,
            'cut_off' => $this->cut_off,
            'year' => $this->year,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc ? 'asc' : 'desc'),
        ];

        $response = $daily_time_records->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.employee-attendance.daily-time-records.index', [
            'time_logs' => $response['result'],
        ]);
    }
}
