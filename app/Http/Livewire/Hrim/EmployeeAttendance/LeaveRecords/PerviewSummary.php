<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\LeaveRecords;

use App\Interfaces\Hrim\EmployeeAttendance\LeaveRecordsInterface;
use App\Models\Hrim\LeaveDayTypeReference;
use App\Models\Hrim\LeaveTypeReference;
use App\Models\User;
use App\Traits\Hrim\Attendance\LeaveRecordsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class PerviewSummary extends Component
{
    use LeaveRecordsTrait;
    use WithFileUploads;
    use PopUpMessagesTrait;

    public $action_type;
    public $data;

    public $user_leaves;

    public $leave_type;
    public $apply_for;
    public $reliever;

    public function mount(LeaveRecordsInterface $leave_records_interface, $data, $action_type)
    {
        $this->data = $data;
        $this->action_type = $action_type;

        $response = $leave_records_interface->getUserAvailableLeaves();
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }
        $this->user_leaves = $response['result']['user_leaves'] ?? [];

        $leave_type = LeaveTypeReference::find($data['type_of_leave']);
        $this->leave_type = $leave_type->display;

        $leave_day_type = LeaveDayTypeReference::find($data['apply_for']);
        $this->apply_for = $leave_day_type->display;

        $user = User::find($data['reliever']);
        if ($user) {
            $this->reliever = $user->name;
        }
    }

    public function submit()
    {
        if ($this->action_type == 'create') {
            $this->emitTo('hrim.employee-attendance.leave-records.create', 'submit');
        } elseif ($this->action_type == 'edit') {
            $this->emitTo('hrim.employee-attendance.leave-records.edit', 'submit');
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-attendance.leave-records.perview-summary');
    }
}
