<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\LeaveRecords;

use App\Interfaces\Hrim\EmployeeAttendance\LeaveRecordsInterface;
use App\Interfaces\Hrim\EmployeeAttendance\TarInterface;
use Livewire\Component;

class View extends Component
{
    public $leave;
    
    protected $listeners = ['mount' => 'mount'];

    public function mount(LeaveRecordsInterface $leave_records_interface, $id)
    {
        $response = $leave_records_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->leave = $response['result'];
    }

    public function render()
    {
        return view('livewire.hrim.employee-attendance.leave-records.view');
    }
}
