<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\LeaveRecords;

use App\Interfaces\Hrim\EmployeeAttendance\LeaveRecordsInterface;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class Attachments extends Component
{
    use PopUpMessagesTrait;

    public $view_modal = false;
    public $active = 0;
    public $total_attachments = 0;
    public $leave;

    protected $listeners = ['mount' => 'mount'];

    public function mount(LeaveRecordsInterface $leave_records_interface, $id)
    {
        $response = $leave_records_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);
        $this->leave = $response['result'];
        $this->total_attachments = ($this->leave->attachments_count - 1);
    }

    public function previous()
    {
        if ($this->active == 0) {
            return;
        }
        $this->active--;
    }

    public function next()
    {
        if ($this->active < $this->total_attachments) {
            $this->active++;
        }
    }

    public function download()
    {
        if (Storage::disk('hrim_gcs')->exists($this->leave->attachments[$this->active]->path . $this->leave->attachments[$this->active]->name)) {
            $downloaded_name = $this->leave->attachments[$this->active]->id . '-' . $this->leave->attachments[$this->active]->name . '.' . $this->leave->attachments[$this->active]->extension;
            if ($this->leave->attachments[$this->active]->original_name) {
                $downloaded_name = $this->leave->attachments[$this->active]->original_name . '.' . $this->leave->attachments[$this->active]->extension;
            }

            return Storage::disk('hrim_gcs')->download($this->leave->attachments[$this->active]->path . $this->leave->attachments[$this->active]->name, $downloaded_name);
        } else {
            $this->sweetAlert('error', 'File doesn`t exist in cloud.');
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-attendance.leave-records.attachments');
    }
}
