<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\LeaveRecords;

use App\Interfaces\Hrim\EmployeeAttendance\LeaveRecordsInterface;
use App\Traits\Hrim\EmployeeAttendance\LeaveRecordsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use LeaveRecordsTrait, WithFileUploads, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount', 'close_modal' => 'closeModal', 'submit' => 'submit'];

    public function mount(LeaveRecordsInterface $leave_records_interface, $id)
    {
        $this->resetForm();
        $response = $leave_records_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->leave = $response['result'];
        $this->inclusive_date_from = $this->leave->inclusive_date_from;
        $this->inclusive_date_to = $this->leave->inclusive_date_to;
        $this->resume_of_work = $this->leave->resume_date;
        $this->type_of_leave = $this->leave->leave_type_id;
        $this->other = $this->leave->other;
        $this->is_with_medical_certificate = $this->leave->is_with_medical_certificate;
        $this->apply_for = $this->leave->leave_day_type_id;
        $this->is_with_pay = $this->leave->is_with_pay;
        $this->reason = $this->leave->reason;
        $this->reliever = $this->leave->reliever;
    }

    public function submit(LeaveRecordsInterface $leave_records_interface)
    {
        $response = $leave_records_interface->update($this->leave->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-attendance.leave-records.index', 'close_modal', 'edit');
            $this->emitTo('hrim.employee-attendance.leave-records.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-attendance.leave-records.edit');
    }
}
