<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\LeaveRecords;

use App\Interfaces\Hrim\EmployeeAttendance\LeaveRecordsInterface;
use App\Models\Hrim\LeaveDayTypeReference;
use App\Traits\Hrim\EmployeeAttendance\LeaveRecordsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use LeaveRecordsTrait, WithFileUploads, PopUpMessagesTrait;

    public $nextDate;

    protected $listeners = ['close_modal' => 'closeModal', 'submit'];

    public function submit(LeaveRecordsInterface $leave_records_interface)
    {
        $response = $leave_records_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-attendance.leave-records.index', 'close_modal', 'create');
            $this->emitTo('hrim.employee-attendance.leave-records.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getApplyFor()
    {
        if ($this->inclusive_date_to) {
            if ($this->apply_for == 2) {
                $this->nextDate = date('Y-m-d');
            } else {
                $this->nextDate = date('Y-m-d', strtotime($this->inclusive_date_to . ' +1 day'));
            }

            $this->resume_of_work = $this->nextDate;
        }
    }

    public function getNextDateExcludingWeekdays()
    {
        if ($this->inclusive_date_from != $this->inclusive_date_to) {
            $this->leave_day_type_references = LeaveDayTypeReference::where('id', 1)->get();

            $this->apply_for = 1;
        } else {
            $this->loadLeaveDayTypeReference();
        }

        $this->nextDate = date('Y-m-d', strtotime($this->inclusive_date_to . ' +1 day'));

        while (date('N', strtotime($this->nextDate)) == 7) {
            $this->nextDate = date('Y-m-d', strtotime($this->nextDate . ' +1 day'));
        }

        $this->resume_of_work = $this->nextDate;
    }

    public function render()
    {
        return view('livewire.hrim.employee-attendance.leave-records.create');
    }
}
