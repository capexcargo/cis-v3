<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\OvertimeRecords;

use App\Interfaces\Hrim\EmployeeAttendance\OvertimeRecordsInterface;
use App\Interfaces\Hrim\EmployeeAttendance\TarInterface;
use Livewire\Component;

class View extends Component
{
    public $overtime_record;
    
    protected $listeners = ['mount' => 'mount'];

    public function mount(OvertimeRecordsInterface $overtime_records_interface, $id)
    {
        $response = $overtime_records_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);
        $this->overtime_record = $response['result'];
    }

    public function render()
    {
        return view('livewire.hrim.employee-attendance.overtime-records.view');
    }
}
