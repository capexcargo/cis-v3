<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\OvertimeRecords;

use App\Interfaces\Hrim\EmployeeAttendance\OvertimeRecordsInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $create_modal = false;
    public $edit_modal = false;
    public $view_modal = false;
    public $overtime_id;

    public $date;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emitTo('hrim.employee-attendance.overtime-records.edit', 'mount', $data['id']);
            $this->overtime_id = $data['id'];
            $this->edit_modal = true;
        } elseif ($action_type == 'view') {
            $this->emitTo('hrim.employee-attendance.overtime-records.view', 'mount', $data['id']);
            $this->overtime_id = $data['id'];
            $this->view_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'view') {
            $this->view_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render(OvertimeRecordsInterface $overtime_records)
    {
        $request = [
            'date' => $this->date,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $overtime_records->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.employee-attendance.overtime-records.index', [
            'overtime_records' => $response['result']
        ]);
    }
}
