<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\OvertimeRecords;

use App\Interfaces\Hrim\EmployeeAttendance\OvertimeRecordsInterface;
use App\Traits\Hrim\EmployeeAttendance\OvertimeRecordsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use OvertimeRecordsTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(OvertimeRecordsInterface $overtime_records_interface, $id)
    {
        $this->resetForm();
        $response = $overtime_records_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->overtime_record = $response['result'];
        $this->month  = (int) date('m', strtotime($this->overtime_record->date_time_from));
        $this->cut_off  = 'first';

        $day = (int) date('d', strtotime($this->overtime_record->date_time_from));
        if ($day >= 11 && $day <= 26) {
            $this->cut_off  = 'second';
        }
        $this->year  = date('Y', strtotime($this->overtime_record->date_time_from));
        $this->getOvertimed();

        $this->date_time_in = $this->overtime_record->date_time_from;
        $this->updatedDateTimeIn();

        $this->type_of_ot = $this->overtime_record->date_category_id;
        $this->actual_date_from = $this->overtime_record->date_time_from;
        $this->actual_date_to = $this->overtime_record->date_time_to;
        $this->from = date('H:i', strtotime($this->overtime_record->time_from));
        $this->to = date('H:i', strtotime($this->overtime_record->time_to));
        $this->overtime_request = $this->overtime_record->overtime_request;
        $this->reason = $this->overtime_record->reason;
    }

    public function submit(OvertimeRecordsInterface $overtime_records_interface)
    {
        $response = $overtime_records_interface->update($this->overtime_record->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-attendance.overtime-records.index', 'close_modal', 'edit');
            $this->emitTo('hrim.employee-attendance.overtime-records.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-attendance.overtime-records.edit');
    }
}
