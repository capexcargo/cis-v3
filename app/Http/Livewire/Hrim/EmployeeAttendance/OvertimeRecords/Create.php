<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\OvertimeRecords;

use App\Interfaces\Hrim\EmployeeAttendance\OvertimeRecordsInterface;
use App\Models\Hrim\CutOffManagement;
use App\Traits\Hrim\EmployeeAttendance\OvertimeRecordsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use OvertimeRecordsTrait, PopUpMessagesTrait;

    public function mount()
    {
        $cutoff = CutOffManagement::where('cutoff_date', date('Y-m-d'))->first();

        if ($cutoff->payout_cutoff == 1) {
            $cutoffs = 'first';
        } else {
            $cutoffs = 'second';
        }

        $this->month = $cutoff->payout_month;
        $this->cut_off = $cutoffs;
        $this->year = date('Y');

        $this->getOvertimed();
    }

    public function submit(OvertimeRecordsInterface $overtime_records_interface)
    {
        $response = $overtime_records_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-attendance.overtime-records.index', 'close_modal', 'create');
            $this->emitTo('hrim.employee-attendance.overtime-records.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-attendance.overtime-records.create');
    }
}
