<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\ScheduleAdjustment;

use App\Interfaces\Hrim\EmployeeAttendance\ScheduleAdjustmentInterface;
use App\Models\User;
use App\Traits\Hrim\EmployeeAttendance\ScheduleAdjustmentTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Create extends Component
{
    use ScheduleAdjustmentTrait, PopUpMessagesTrait;

    public function mount()
    {
        $this->employee_name = Auth::user()->name;

        $employee_information = User::with(['userDetails' => function ($query) {
            $query->with('workSchedule');
        }])
            ->find(Auth::user()->id);

        if ($employee_information) {
            $this->employee_name = $employee_information->name;
            $this->employee_id = $employee_information->id;
            $this->current_schedule = $employee_information->userDetails->schedule_id;

            $days = '';
            if ($employee_information->userDetails->workSchedule) {
                if ($employee_information->userDetails->workSchedule->monday) {
                    $days .= 'Monday ';
                }
                if ($employee_information->userDetails->workSchedule->tuesday) {
                    $days .= 'Tuesday ';
                }
                if ($employee_information->userDetails->workSchedule->wednesday) {
                    $days .= 'Wednesday ';
                }
                if ($employee_information->userDetails->workSchedule->thursday) {
                    $days .= 'Thurday ';
                }
                if ($employee_information->userDetails->workSchedule->friday) {
                    $days .= 'Friday ';
                }
                if ($employee_information->userDetails->workSchedule->saturday) {
                    $days .= 'Saturday ';
                }
                if ($employee_information->userDetails->workSchedule->sunday) {
                    $days .= 'Sunday ';
                }

                $this->current_days = $days;
                $this->current_time_from = $employee_information->userDetails->workSchedule->time_from;
                $this->current_time_to = $employee_information->userDetails->workSchedule->time_to;
            }
        }
    }
    public function submit(ScheduleAdjustmentInterface $schedule_adjustment_interface)
    {
        $response = $schedule_adjustment_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-attendance.schedule-adjustment.index', 'close_modal', 'create');
            $this->emitTo('hrim.employee-attendance.schedule-adjustment.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-attendance.schedule-adjustment.create');
    }
}
