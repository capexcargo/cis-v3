<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\ScheduleAdjustment;

use App\Interfaces\Hrim\EmployeeAttendance\ScheduleAdjustmentInterface;
use Livewire\Component;

class View extends Component
{
    public $schedule_adjustment;
    
    protected $listeners = ['mount' => 'mount'];

    public function mount(ScheduleAdjustmentInterface $schedule_adjustment_interface, $id)
    {
        $response = $schedule_adjustment_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);
        $this->schedule_adjustment = $response['result'];
    }

    public function render()
    {
        return view('livewire.hrim.employee-attendance.schedule-adjustment.view');
    }
}
