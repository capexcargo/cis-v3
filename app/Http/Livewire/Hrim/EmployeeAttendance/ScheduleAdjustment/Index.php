<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\ScheduleAdjustment;

use App\Interfaces\Hrim\EmployeeAttendance\OvertimeRecordsInterface;
use App\Interfaces\Hrim\EmployeeAttendance\ScheduleAdjustmentInterface;
use App\Models\Hrim\StatusReference;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $action_type;
    public $confirmation_modal = false;
    public $confirmation_message;

    public $create_modal = false;
    public $edit_modal = false;
    public $view_modal = false;
    public $schedule_adjustment_id;

    public $employee_name;
    public $date_filed;
    public $status;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $status_references = [];

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadStatusReferences();
    }

    public function loadStatusReferences()
    {
        $this->status_references = StatusReference::get();
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'edit') {
            $this->emitTo('hrim.employee-attendance.schedule-adjustment.edit', 'mount', $data['id']);
            $this->schedule_adjustment_id = $data['id'];
            $this->edit_modal = true;
        } elseif ($action_type == 'view') {
            $this->emitTo('hrim.employee-attendance.schedule-adjustment.view', 'mount', $data['id']);
            $this->schedule_adjustment_id = $data['id'];
            $this->view_modal = true;
        } elseif ($action_type == 'delete') {
            $this->schedule_adjustment_id = $data['id'];

            $this->confirmation_message = "Are you sure you want to delete this Schedule Adjustment Request?";
            $this->confirmation_modal = true;
        }
    }

    public function confirm(ScheduleAdjustmentInterface $schedule_adjustment_interface)
    {
        if ($this->action_type == 'delete') {
            $response = $schedule_adjustment_interface->destroy($this->schedule_adjustment_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.employee-attendance.schedule-adjustment.index', 'close_modal', 'confirmation');
            $this->emitTo('hrim.employee-attendance.schedule-adjustment.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            if (is_array($response['result'])) {
                foreach ($response['result'] as $a => $messages) {
                    $this->addError($a, $messages);
                }
            } else {
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'view') {
            $this->view_modal = false;
        } elseif ($action_type == 'confirmation') {
            $this->confirmation_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render(ScheduleAdjustmentInterface $schedule_adjustment_interface)
    {
        $request = [
            'employee_name' => $this->employee_name,
            'date_filed' => $this->date_filed,
            'status' => $this->status,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $schedule_adjustment_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }
        return view('livewire.hrim.employee-attendance.schedule-adjustment.index', [
            'schedule_adjustments' => $response['result']
        ]);
    }
}
