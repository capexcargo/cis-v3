<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\ScheduleAdjustment;

use App\Interfaces\Hrim\EmployeeAttendance\ScheduleAdjustmentInterface;
use App\Traits\Hrim\EmployeeAttendance\ScheduleAdjustmentTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use ScheduleAdjustmentTrait, PopUpMessagesTrait;

    public function mount(ScheduleAdjustmentInterface $schedule_adjustment_interface, $id)
    {
        $this->resetForm();
        $response = $schedule_adjustment_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->schedule_adjustment = $response['result'];
        $this->getEmployeeInformation($this->schedule_adjustment->user_id);
        $this->new_schedule = $this->schedule_adjustment->new_work_schedule_id;
        $this->updatedNewSchedule();
        $this->new_inclusive_date_start_date = $this->schedule_adjustment->inclusive_date_from;
        $this->new_inclusive_date_end_date = $this->schedule_adjustment->inclusive_date_to;
        $this->set_an_end_date = ($this->schedule_adjustment->inclusive_date_to ? true : false);
        $this->reason = $this->schedule_adjustment->reason;
    }

    public function submit(ScheduleAdjustmentInterface $schedule_adjustment_interface)
    {
        $response = $schedule_adjustment_interface->update($this->schedule_adjustment->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-attendance.schedule-adjustment.index', 'close_modal', 'edit');
            $this->emitTo('hrim.employee-attendance.schedule-adjustment.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-attendance.schedule-adjustment.edit');
    }
}
