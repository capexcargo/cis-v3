<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\Tar;

use App\Interfaces\Hrim\EmployeeAttendance\TarInterface;
use Livewire\Component;

class View extends Component
{
    public $tar;
    
    protected $listeners = ['mount' => 'mount'];

    public function mount(TarInterface $tar_interface, $date)
    {
        $this->date = $date;
        $request = [
            'date' => $date
        ];
        $response = $tar_interface->show($request);
        abort_if($response['code'] != 200, $response['code'], $response['message']);
        $this->tar = $response['result'];
    }

    public function render()
    {
        return view('livewire.hrim.employee-attendance.tar.view');
    }
}
