<?php

namespace App\Http\Livewire\Hrim\EmployeeAttendance\Tar;

use App\Interfaces\Hrim\EmployeeAttendance\TarInterface;
use App\Traits\Hrim\EmployeeAttendance\TarTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class UpdateOrCreate extends Component
{
    use TarTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(TarInterface $tar_interface, $time_log_id = null, $date)
    {
        $this->time_log_id = $time_log_id;
        $this->date_from = $date;
        $this->date_to = $date;
        $request = [
            'date' => $date
        ];
        $response = $tar_interface->show($request);
        if ($response['code'] == 200) {
            $this->tar = $response['result'];
            $this->date_from = $this->tar->date_from;
            $this->date_to = $this->tar->date_to;
            $this->actual_time_in = $this->tar->actual_time_in;
            $this->actual_time_out = $this->tar->actual_time_out;
            $this->reason_of_adjustment = $this->tar->tar_reason_id;
        }
    }

    public function submit(TarInterface $tar_interface)
    {
        $response = $tar_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-attendance.daily-time-records.index', 'close_modal', 'create');
            $this->emitTo('hrim.employee-attendance.daily-time-records.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-attendance.tar.update-or-create');
    }
}
