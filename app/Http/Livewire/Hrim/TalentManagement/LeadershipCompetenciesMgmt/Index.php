<?php

namespace App\Http\Livewire\Hrim\TalentManagement\LeadershipCompetenciesMgmt;

use App\Interfaces\Hrim\TalentManagement\LeadershipCompetenciesMgmtInterface;
use App\Traits\Hrim\TalentManagement\LeadershipCompetenciesMgmtTrait;
use App\Traits\Hrim\TalentManagement\PerformanceHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use LeadershipCompetenciesMgmtTrait, PerformanceHeaderTrait, WithPagination, PopUpMessagesTrait;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_leadership_competencies') {
            $this->create_leadership_competencies_modal = true;
        } else if ($action_type == 'edit_leadership_competencies') {
            $this->emitTo('hrim.talent-management.leadership-competencies-mgmt.edit', 'mount', $data['id']);
            $this->leadership_competencies_id = $data['id'];
            $this->edit_leadership_competencies_modal = true;
        } else if ($action_type == 'delete_leadership_competencies') {
            $this->confirmation_message = "Are you sure you want to delete this Leadership Competency item?";
            $this->confirmation_modal = true;
            $this->leadership_competencies_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_leadership_competencies_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_leadership_competencies_modal = false;
        } else if ($action_type == 'delete') {
            $this->confirmation_modal = false;
        }
    }
    
    public function deleteCoreValue(LeadershipCompetenciesMgmtInterface $leadership_competencies_mgmt_interface){
        if ($this->action_type == 'delete_leadership_competencies') {
            $response = $leadership_competencies_mgmt_interface->destroy($this->leadership_competencies_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.talent-management.leadership-competencies-mgmt.index', 'close_modal', 'delete');
            $this->emitTo('hrim.talent-management.leadership-competencies-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(LeadershipCompetenciesMgmtInterface $leadership_competencies_mgmt_interface)
    {
        $request = [
            'leadership_competencies' => $this->leadership_competencies,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $leadership_competencies_mgmt_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.talent-management.leadership-competencies-mgmt.index', [
            'leadership_competencies_management' => $response['result']
        ]);
    }
}
