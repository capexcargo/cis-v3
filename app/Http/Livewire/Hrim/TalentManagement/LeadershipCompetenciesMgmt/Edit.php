<?php

namespace App\Http\Livewire\Hrim\TalentManagement\LeadershipCompetenciesMgmt;

use App\Interfaces\Hrim\TalentManagement\LeadershipCompetenciesMgmtInterface;
use App\Traits\Hrim\TalentManagement\LeadershipCompetenciesMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use LeadershipCompetenciesMgmtTrait, WithPagination, PopUpMessagesTrait;
    protected $listeners = ['mount' => 'mount'];

    public function mount(LeadershipCompetenciesMgmtInterface $leadership_competencies_mgmt_interface, $id)
    {
        $this->resetForm();
        $response = $leadership_competencies_mgmt_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->leadershipCompetencies = $response['result'];
        $this->leadership_competencies = $this->leadershipCompetencies->description;
        
    }

    public function submit(LeadershipCompetenciesMgmtInterface $leadership_competencies_mgmt_interface)
    {
        $response = $leadership_competencies_mgmt_interface->update($this->leadershipCompetencies->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.talent-management.leadership-competencies-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('hrim.talent-management.leadership-competencies-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.leadership-competencies-mgmt.edit');
    }
}
