<?php

namespace App\Http\Livewire\Hrim\TalentManagement\LeadershipCompetenciesMgmt;

use App\Interfaces\Hrim\TalentManagement\LeadershipCompetenciesMgmtInterface;
use App\Traits\Hrim\TalentManagement\LeadershipCompetenciesMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use LeadershipCompetenciesMgmtTrait, PopUpMessagesTrait;

    public function submit(LeadershipCompetenciesMgmtInterface $leadership_competencies_mgmt_interface)
    {
        $response = $leadership_competencies_mgmt_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.talent-management.leadership-competencies-mgmt.index', 'close_modal', 'create');
            $this->emitTo('hrim.talent-management.leadership-competencies-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.leadership-competencies-mgmt.create');
    }
}
