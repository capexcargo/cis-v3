<?php

namespace App\Http\Livewire\Hrim\TalentManagement\KpiTagging;

use App\Models\Hrim\PerformanceEvaluation;
use App\Models\Hrim\Position;
use Livewire\Component;

class View extends Component
{
    protected $listeners = ['mount' => 'mount'];

    public $position;
    public $kpi_taggings = [];

    public function mount($id, $quarter, $year)
    {
        $position_reference = Position::findOrFail($id);
        $this->position = $position_reference->display;
        $this->job_description = $position_reference->job_overview;

        $this->kpi_tagged_references = PerformanceEvaluation::with('kra', 'kpi')
            ->where('position_id', $id)
            ->where('kra_id', '!=', null)
            ->where('quarter', '=', $quarter)
            ->where('year', '=', $year)
            ->orderBy('kra_id', 'asc')
            ->select('kra_id', 'kpi_id')->distinct()
            ->get();

        foreach ($this->kpi_tagged_references as $j => $kpi_tagged) {
            $this->kpi_taggings[] = [
                'id' => $kpi_tagged->id,
                'kra' => $kpi_tagged->kra->name,
                'kpi' => $kpi_tagged->kpi->name,
                'target' => $kpi_tagged->kpi->target,
                'points' => $kpi_tagged->kpi->points,
                'remarks' => $kpi_tagged->kpi->remarks,
            ];
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.kpi-tagging.view');
    }
}
