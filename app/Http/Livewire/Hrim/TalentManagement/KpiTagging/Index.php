<?php

namespace App\Http\Livewire\Hrim\TalentManagement\KpiTagging;

use App\Interfaces\Hrim\TalentManagement\KpiTaggingInterface;
use App\Traits\Hrim\TalentManagement\KpiTaggingTrait;
use App\Traits\Hrim\TalentManagement\PerformanceHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use KpiTaggingTrait, PerformanceHeaderTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_kpi_tagging_modal = false;
        } else if ($action_type == 'view') {
            $this->edit_kpi_tagging_modal = false;
        }
    }
        
    public function render(KpiTaggingInterface $kra_tagging_interface)
    {
        $request = [
            'position' => $this->position,
            'quarter' => $this->quarter,
            'year' => $this->year,
            // 'sort_field' => $this->sortField,
            // 'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];
        // dd($request);
        $response = $kra_tagging_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.talent-management.kpi-tagging.index', [
            'kpi_tags' => $response['result']
        ]);

    }
}
