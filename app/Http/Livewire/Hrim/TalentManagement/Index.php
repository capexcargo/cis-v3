<?php

namespace App\Http\Livewire\Hrim\TalentManagement;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.hrim.talent-management.index');
    }
}
