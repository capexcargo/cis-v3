<?php

namespace App\Http\Livewire\Hrim\TalentManagement\KraPointsManagement;

use App\Interfaces\Hrim\TalentManagement\KraPointsManagementInterface;
use App\Traits\Hrim\TalentManagement\KraPointsManagementTrait;
use App\Traits\Hrim\TalentManagement\PerformanceHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use KraPointsManagementTrait, PerformanceHeaderTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_kra_points') {
            $this->create_kra_points_modal = true;
        } else if ($action_type == 'edit_kra_points') {
            $this->emitTo('hrim.talent-management.kra-points-management.edit', 'mount', $data['id']);
            $this->kra_points_id = $data['id'];
            $this->edit_kra_points_modal = true;
        } else if ($action_type == 'delete_kra_points') {
            $this->confirmation_message = "Are you sure you want to delete this KRA Points?";
            $this->confirmation_modal = true;
            $this->kra_points_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_kra_points_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_kra_points_modal = false;
        }
    }

    public function deleteKRA(KraPointsManagementInterface $kra_management_interface){
        if ($this->action_type == 'delete_kra_points') {
            $response = $kra_management_interface->destroy($this->kra_points_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.talent-management.kra-points-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    
    public function render(KraPointsManagementInterface $kra_management_interface)
    {
        $request = [
            'points' => $this->points,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $kra_management_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.talent-management.kra-points-management.index', [
            'kra_points_managements' => $response['result']
        ]);
    }
}
