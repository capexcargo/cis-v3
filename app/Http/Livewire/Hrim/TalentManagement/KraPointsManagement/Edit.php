<?php

namespace App\Http\Livewire\Hrim\TalentManagement\KraPointsManagement;

use App\Interfaces\Hrim\TalentManagement\KraPointsManagementInterface;
use App\Traits\Hrim\TalentManagement\KraPointsManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use KraPointsManagementTrait, WithPagination, PopUpMessagesTrait;
    
    protected $listeners = ['mount' => 'mount'];

    public function mount(KraPointsManagementInterface $kra_points_management_interface, $id)
    {
        $this->resetForm();
        $response = $kra_points_management_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->kra_points = $response['result'];
        $this->points = $this->kra_points->points;
        $this->description = $this->kra_points->description;
        $this->quarter = $this->kra_points->quarter;
        $this->year = $this->kra_points->year;
    }

    public function submit(KraPointsManagementInterface $kra_points_management_interface)
    {
        $response = $kra_points_management_interface->update($this->kra_points->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.talent-management.kra-points-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.kra-points-management.edit');
    }
}
