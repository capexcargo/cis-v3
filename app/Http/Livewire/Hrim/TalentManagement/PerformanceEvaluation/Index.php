<?php

namespace App\Http\Livewire\Hrim\TalentManagement\PerformanceEvaluation;

use App\Interfaces\Hrim\TalentManagement\PerformanceEvaluationInterface;
use App\Models\Hrim\PerformanceEvaluation;
use App\Models\User;
use App\Traits\Hrim\TalentManagement\PerformanceEvaluationTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use PerformanceEvaluationTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render'];

    public function action($data, $action_type)
    {
        $this->action_type = $action_type;

        // dd($data);
        if ($action_type == 'view_performance') {
            $this->performance_id = $data['id'];
            return redirect()->route('hrim.talent-management.performance-evaluation.view', ['id' => $this->performance_id, 'quarter' => $data['quarter'], 'years' => $data['years']]);
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.performance-evaluation.index', [
            'performance_evaluations' => PerformanceEvaluation::with('userDetails', 'user')
                ->whereHas('userDetails', function ($query) {
                    $query
                        ->when($this->position, function ($query) {
                            $query->where('position_id', $this->position)
                                ->where('position_id', '!=', '');
                        })
                        ->when($this->division, function ($query) {
                            $query->where('division_id', $this->division)
                                ->where('division_id', '!=', '');
                        })
                        ->when($this->branch, function ($query) {
                            $query->where('branch_id', $this->branch);
                        });
                    // });
                })
                ->whereHas('user', function ($query) {
                    $query->when($this->employee_name, function ($query) {
                        $query->where('name', 'like', '%' . $this->employee_name . '%');
                    })
                        ->orWhere('level_id', 5)
                        ->where('division_id',  Auth::user()->division_id)
                        ->orderBy('division_id', 'ASC');
                })
                ->groupBy('quarter', 'year', 'user_id')
                ->paginate($this->paginate)
        ]);
    }
}
