<?php

namespace App\Http\Livewire\Hrim\TalentManagement\PerformanceEvaluation;

use App\Interfaces\Hrim\TalentManagement\PerformanceEvaluationInterface;
use App\Models\Hrim\PerformanceEvaluation;
use App\Models\Hrim\Position;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\Hrim\TalentManagement\PerformanceEvaluationTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class View extends Component
{
    use PerformanceEvaluationTrait, PopUpMessagesTrait;

    public $breakdown_pct_goals;
    public $breakdown_pct_core_values;
    public $breakdown_pct_leadership_competencies;

    public $view_jd_modal = false;
    public $view_201_files_modal = false;
    public $view_justification_modal = false;
    public $position_id;
    public $emp_id;
    public $eval_id;

    public $photo_name;
    public $photo_path;


    protected $listeners = ['view' => 'render', 'mount' => 'mount'];

    public function mount($id, $quarter, $years)
    {
        $this->performance_evaluation_references = UserDetails::with('performanceEvals', 'breakdownPct', 'position', 'division', 'jobLevel')->where('id', $id)
            ->get();
            
        foreach ($this->performance_evaluation_references as $photo) {
            $this->photo_path = $photo->user->photo_path;
            $this->photo_name = $photo->user->photo_name;
        }

        foreach ($this->performance_evaluation_references as $perform_breakdown_percentage) {
            $this->breakdown_pct_goals = $perform_breakdown_percentage->breakdownPct->goals;
            $this->breakdown_pct_core_values = $perform_breakdown_percentage->breakdownPct->core_values;
            $this->breakdown_pct_leadership_competencies = $perform_breakdown_percentage->breakdownPct->leadership_competencies;
        }
        $this->performance_evaluation_evals = PerformanceEvaluation::where('user_id', $id)
            ->select('first_evaluator', 'second_evaluator', 'third_evaluator', 'first_status', 'second_status', 'third_status', 'first_remarks', 'second_remarks', 'third_remarks', 'year', 'quarter')
            ->where('quarter', $quarter)
            ->where('year', $years)
            ->distinct()
            ->get();

        $this->kpi_tagged_references = PerformanceEvaluation::with('kra', 'kpi')->where('quarter', $quarter)->where('year', $years)->where('user_id', $id)->where('kra_id', '!=', null)->get();
        $this->kpi_tagged_core_values = PerformanceEvaluation::with('core_values')->where('quarter', $quarter)->where('year', $years)->where('user_id', $id)->where('core_values_id', '!=', null)->get();
        $this->kpi_tagged_leadership_comps = PerformanceEvaluation::with('leadership_comp')->where('quarter', $quarter)->where('year', $years)->where('user_id', $id)->where('leadership_comp_id', '!=', null)->get();

        foreach ($this->kpi_tagged_references as $j => $kpi_tagged) {
            $this->performance_scorings[] = [
                'id' => $kpi_tagged->id,
                'kra' => $kpi_tagged->kra->name,
                'kpi' => $kpi_tagged->kpi->name,
                'target' => $kpi_tagged->kpi->target,
                'points' => $kpi_tagged->kpi->points,
                'first_evaluator' => $kpi_tagged->first_evaluator,
                'second_evaluator' => $kpi_tagged->second_evaluator,
                'third_evaluator' => $kpi_tagged->third_evaluator,
                'first_evaluator_points' => $kpi_tagged->first_points == '' ? null : $kpi_tagged->first_points,
                'second_evaluator_points' => $kpi_tagged->second_points == '' ? null : $kpi_tagged->second_points,
                'third_evaluator_points' => $kpi_tagged->third_points == '' ? null : $kpi_tagged->third_points,
                'first_status' => $kpi_tagged->first_status,
                'second_status' => $kpi_tagged->second_status,
                'third_status' => $kpi_tagged->third_status,
                'first_points' => $kpi_tagged->first_points,
                'second_points' => $kpi_tagged->second_points,
                'third_points' => $kpi_tagged->third_points,
            ];
        }

        foreach ($this->kpi_tagged_core_values as $j => $core_val) {
            $this->core_value_assessments[] = [
                'id' => $core_val->id,
                'description' => $core_val->core_values->description,
                'first_evaluator' => $core_val->first_evaluator,
                'second_evaluator' => $core_val->second_evaluator,
                'third_evaluator' => $core_val->third_evaluator,
                'first_evaluator_points' => $core_val->first_points == '' ? null : $core_val->first_points,
                'second_evaluator_points' => $core_val->second_points == '' ? null : $core_val->second_points,
                'third_evaluator_points' => $core_val->third_points == '' ? null : $core_val->third_points,
                'first_status' => $core_val->first_status,
                'second_status' => $core_val->second_status,
                'third_status' => $core_val->third_status,
                'first_points' => $core_val->first_points,
                'second_points' => $core_val->second_points,
                'third_points' => $core_val->third_points,
                'employee_self_points' => $core_val->employee_self_points,
                'emp_critical_remarks' => $core_val->critical_remarks,
                'evaluator_1st_critical_remarks' => $core_val->core_val_1st_critical_remarks == '' ? null : $core_val->core_val_1st_critical_remarks,
                'evaluator_2nd_critical_remarks' => $core_val->core_val_2nd_critical_remarks == '' ? null : $core_val->core_val_2nd_critical_remarks,
                'evaluator_3rd_critical_remarks' => $core_val->core_val_3rd_critical_remarks == '' ? null : $core_val->core_val_3rd_critical_remarks,
            ];
        }

        if (count($this->kpi_tagged_leadership_comps) > 0) {
            foreach ($this->kpi_tagged_leadership_comps as $j => $lead_comp) {
                $this->leadership_competencies[] = [
                    'id' => $lead_comp->id,
                    'description' => $lead_comp->leadership_comp->description,
                    'first_evaluator' => $lead_comp->first_evaluator,
                    'second_evaluator' => $lead_comp->second_evaluator,
                    'third_evaluator' => $lead_comp->third_evaluator,
                    'first_evaluator_points' => $lead_comp->first_points == '' ? null : $lead_comp->first_points,
                    'second_evaluator_points' => $lead_comp->second_points == '' ? null : $lead_comp->second_points,
                    'third_evaluator_points' => $lead_comp->third_points == '' ? null : $lead_comp->third_points,
                    'first_status' => $lead_comp->first_status,
                    'second_status' => $lead_comp->second_status,
                    'third_status' => $lead_comp->third_status,
                    'first_points' => $lead_comp->first_points,
                    'second_points' => $lead_comp->second_points,
                    'third_points' => $lead_comp->third_points,
                    'employee_self_points' => $lead_comp->employee_self_points,
                    'emp_critical_remarks' => $lead_comp->critical_remarks,
                    'evaluator_1st_critical_remarks' => $lead_comp->lead_com_1st_critical_remarks == '' ? null : $lead_comp->lead_com_1st_critical_remarks,
                    'evaluator_2nd_critical_remarks' => $lead_comp->lead_com_2nd_critical_remarks == '' ? null : $lead_comp->lead_com_2nd_critical_remarks,
                    'evaluator_3rd_critical_remarks' => $lead_comp->lead_com_3rd_critical_remarks == '' ? null : $lead_comp->lead_com_3rd_critical_remarks,
                ];
            }
        }

        foreach ($this->performance_evaluation_evals as $j => $evaluator) {
            $this->performance_evaluators[] = [
                'id' => $evaluator->id,
                'first_evaluator' => $evaluator->first_evaluator,
                'second_evaluator' => $evaluator->second_evaluator,
                'third_evaluator' => $evaluator->third_evaluator,
                'first_status' => $evaluator->first_status,
                'second_status' => $evaluator->second_status,
                'third_status' => $evaluator->third_status,
                'first_remarks' => $evaluator->first_remarks == '' ? null : $evaluator->first_remarks,
                'second_remarks' => $evaluator->second_remarks == '' ? null : $evaluator->second_remarks,
                'third_remarks' => $evaluator->third_remarks == '' ? null : $evaluator->third_remarks,
                'year' => $evaluator->year,
                'quarter' => $evaluator->quarter,
            ];
        }
        // dd($this->performance_evaluators);
        // dd($this->performance_scorings);
        // dd($this->core_value_assessments);
        // dd($this->leadership_competencies);
    }

    public function action(PerformanceEvaluationInterface $performance_evaluation_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'back') {
            return redirect()->to(route('hrim.talent-management.performance-evaluation.index'));
        } else if ($action_type == 'save') {
            // dd($this->getRequest());
            $response = $performance_evaluation_interface->save($this->getRequest());

            if ($response['code'] == 200) {
                $this->emitTo('hrim.talent-management.performance-evaluation.view', 'view');
                $this->sweetAlert('', $response['message']);
                return redirect()->to(route('hrim.talent-management.performance-evaluation.index'));
            }
        } else if ($action_type == 'close') {
            // dd($this->getRequest());

            $response = $performance_evaluation_interface->close($this->getRequest());

            if ($response['code'] == 200) {
                $this->emitTo('hrim.talent-management.performance-evaluation.view', 'view');
                $this->sweetAlert('', $response['message']);
                return redirect()->to(route('hrim.talent-management.performance-evaluation.index'));
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } else if ($action_type == 'view_jd') {
            $this->position_id = $data['id'];
            $this->view_jd_modal = true;
            $this->emitTo('hrim.workforce.position-management.view', 'view');
        } else if ($action_type == 'view_201_files') {
            $this->emp_id = $data['id'];
            $this->view_201_files_modal = true;
            // $this->emitTo('hrim.employee-management.employee-information.files.view', 'view', $data['id'], $this->id);
            $this->emitTo('hrim.employee-management.employee-information.files.view', 'mount', $data['id']);
        } else if ($action_type == 'view_justification') {
            $this->eval_id = $data['id'];
            $this->view_justification_modal = true;
        }
    }
    public function render()
    {
        return view('livewire.hrim.talent-management.performance-evaluation.view');
    }
}
