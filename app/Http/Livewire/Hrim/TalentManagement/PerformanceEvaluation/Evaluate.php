<?php

namespace App\Http\Livewire\Hrim\TalentManagement\PerformanceEvaluation;

use Livewire\Component;

class Evaluate extends Component
{
    public function render()
    {
        return view('livewire.hrim.talent-management.performance-evaluation.evaluate');
    }
}
