<?php

namespace App\Http\Livewire\Hrim\TalentManagement\PerformanceEvaluation;

use App\Interfaces\Hrim\TalentManagement\PerformanceEvaluationInterface;
use App\Models\Hrim\PerformanceEvaluation;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class ViewJustification extends Component
{
    use PopUpMessagesTrait;

    protected $listeners = ['view_justification' => 'render', 'mount' => 'mount'];

    public $first_justification;
    public $second_justification;
    public $third_justification;

    public $first_status;
    public $second_status;
    public $third_status;

    public $first_evaluator;
    public $second_evaluator;
    public $third_evaluator;

    public $eval_id;

    public function getJustificationRequest()
    {
        return [
            'first_justification' => $this->first_justification,
            'second_justification' => $this->second_justification,
            'third_justification' => $this->third_justification,
            'id' => $this->eval_id,
        ];
    }

    public function mount($id)
    {
        $first = PerformanceEvaluation::select('first_status', 'first_evaluator', 'first_justification')->where('id', $id)->first();
        $second = PerformanceEvaluation::select('second_status', 'second_evaluator', 'second_justification')->where('id', $id)->first();
        $third = PerformanceEvaluation::select('third_status', 'third_evaluator', 'third_justification')->where('id', $id)->first();

        $this->first_justification = $first->first_justification;
        $this->second_justification = $second->second_justification;
        $this->third_justification = $third->third_justification;

        $this->first_status = $first->first_status;
        $this->second_status = $second->second_status;
        $this->third_status = $third->third_status;

        $this->first_evaluator = $first->first_evaluator;
        $this->second_evaluator = $second->second_evaluator;
        $this->third_evaluator = $third->third_evaluator;

        // dd($this->first_evaluator);
        $this->eval_id = $id;
    }

    public function action(PerformanceEvaluationInterface $performance_evaluation_interface, $data, $action_type)
    {
        if ($action_type == 'save_1st_justification') {

            $response = $performance_evaluation_interface->save1stJustification($this->getJustificationRequest());

            if ($response['code'] == 200) {
                $this->emitTo('hrim.talent-management.performance-evaluation.view-justification', 'view_justification');
                // $this->sweetAlert('', $response['message']);
            }
        } elseif ($action_type == 'save_2nd_justification') {

            $response = $performance_evaluation_interface->save2ndJustification($this->getJustificationRequest());

            if ($response['code'] == 200) {
                $this->emitTo('hrim.talent-management.performance-evaluation.view-justification', 'view_justification');
                // $this->sweetAlert('', $response['message']);
            }
        } elseif ($action_type == 'save_3rd_justification') {

            $response = $performance_evaluation_interface->save3rdJustification($this->getJustificationRequest());

            if ($response['code'] == 200) {
                $this->emitTo('hrim.talent-management.performance-evaluation.view-justification', 'view_justification');
                // $this->sweetAlert('', $response['message']);
            }
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.performance-evaluation.view-justification');
    }
}
