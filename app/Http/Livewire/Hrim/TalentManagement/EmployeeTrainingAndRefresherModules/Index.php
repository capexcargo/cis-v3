<?php

namespace App\Http\Livewire\Hrim\TalentManagement\EmployeeTrainingAndRefresherModules;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.hrim.talent-management.employee-training-and-refresher-modules.index');
    }
}
