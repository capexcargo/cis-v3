<?php

namespace App\Http\Livewire\Hrim\TalentManagement\KraManagement;

use App\Interfaces\Hrim\TalentManagement\KraManagementInterface;
use App\Traits\Hrim\TalentManagement\KraManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use KraManagementTrait, WithPagination, PopUpMessagesTrait;
    protected $listeners = ['mount' => 'mount'];

    public function mount(KraManagementInterface $kra_management_interface, $id)
    {
        $this->resetForm();
        $response = $kra_management_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->kra = $response['result'];
        $this->kra_name = $this->kra->name;
        $this->type = $this->kra->type;
        $this->quarter = $this->kra->quarter;
        $this->year = $this->kra->year;
    }

    public function submit(KraManagementInterface $kra_management_interface)
    {
        $response = $kra_management_interface->update($this->kra->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.talent-management.kra-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.talent-management.kra-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.kra-management.edit');
    }
}
