<?php

namespace App\Http\Livewire\Hrim\TalentManagement\KraManagement;

use App\Interfaces\Hrim\TalentManagement\KraManagementInterface;
use App\Traits\Hrim\TalentManagement\KraManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use KraManagementTrait, PopUpMessagesTrait;

    public function submit(KraManagementInterface $kra_management_interface)
    {
        $response = $kra_management_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.talent-management.kra-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.talent-management.kra-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.kra-management.create');
    }
}
