<?php

namespace App\Http\Livewire\Hrim\TalentManagement\KraManagement;

use App\Interfaces\Hrim\TalentManagement\KraManagementInterface;
use App\Traits\Hrim\TalentManagement\KraManagementTrait;
use App\Traits\Hrim\TalentManagement\PerformanceHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use KraManagementTrait, PerformanceHeaderTrait, WithPagination, PopUpMessagesTrait;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_kra') {
            $this->create_kra_modal = true;
        } else if ($action_type == 'edit_kra') {
            $this->emitTo('hrim.talent-management.kra-management.edit', 'mount', $data['id']);
            $this->kra_id = $data['id'];
            $this->edit_kra_modal = true;
        } else if ($action_type == 'delete_kra') {
            $this->confirmation_message = "Are you sure you want to delete this KRA?";
            $this->confirmation_modal = true;
            $this->kra_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_kra_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_kra_modal = false;
        } else if ($action_type == 'delete') {
            $this->confirmation_modal = false;
        }
    }

    public function deleteKRA(KraManagementInterface $kra_management_interface)
    {
        if ($this->action_type == 'delete_kra') {
            $response = $kra_management_interface->destroy($this->kra_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.talent-management.kra-management.index', 'close_modal', 'delete');
            $this->emitTo('hrim.talent-management.kra-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(KraManagementInterface $kra_management_interface)
    {
        $request = [
            'kra_name' => $this->kra_name,
            'type' => $this->type,
            'quarter' => $this->quarter,
            'year' => $this->year,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $kra_management_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.talent-management.kra-management.index', [
            'kra_managements' => $response['result']
        ]);
    }
}
