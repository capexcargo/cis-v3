<?php

namespace App\Http\Livewire\Hrim\TalentManagement\EvaluatorManagement;

use App\Interfaces\Hrim\TalentManagement\EvaluatorManagementInterface;
use App\Models\Division;
use App\Models\Hrim\EvaluatorManagement;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\Hrim\TalentManagement\EvaluatorManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use EvaluatorManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(EvaluatorManagementInterface $evaluator_management_interface, $id)
    {
        $this->resetForm();
        $response = $evaluator_management_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->employee = $response['result'];
        $this->evaluators = EvaluatorManagement::where('user_id', $id)->get();

        foreach ($this->evaluators as $evaluator){
            $this->first_evaluator = $evaluator->first_approver;
            $this->second_evaluator = $evaluator->second_approver;
            $this->third_evaluator = $evaluator->third_approver;
        }

        $this->employee_name = $this->employee->name;
        $this->division = $this->employee->division->name;
        if ($this->employee->userDetails->branch_id != '') {
            $this->branch = $this->employee->userDetails->branch->display;
        }

        $this->division_id = $this->employee->division_id;
    }

    public function submit(EvaluatorManagementInterface $evaluator_management_interface)
    {
        $response = $evaluator_management_interface->update($this->employee->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.talent-management.evaluator-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.talent-management.evaluator-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.evaluator-management.edit');
    }
}
