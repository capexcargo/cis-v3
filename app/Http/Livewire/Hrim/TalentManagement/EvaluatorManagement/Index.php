<?php

namespace App\Http\Livewire\Hrim\TalentManagement\EvaluatorManagement;

use App\Models\BranchReference;
use App\Models\Hrim\EvaluatorManagement;
use App\Models\User;
use App\Models\UserDetails;
use App\Traits\Hrim\TalentManagement\EvaluatorManagementTrait;
use App\Traits\Hrim\TalentManagement\PerformanceHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use EvaluatorManagementTrait, PerformanceHeaderTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'edit_evaluator') {
            $this->emitTo('hrim.talent-management.evaluator-management.edit', 'mount', $data['id']);
            $this->evaluator_id = $data['id'];
            $this->edit_evaluator_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'edit') {
            $this->edit_evaluator_modal = false;
        }
    }

    public function render()
    {
        // dd(UserDetails::with('user', 'branch','division', 'kpiTagged'));
        return view('livewire.hrim.talent-management.evaluator-management.index', [
            'evaluators' => EvaluatorManagement::get(),
            'employees' => UserDetails::with('user', 'branch', 'division', 'kpiTagged')
                ->whereHas('user', function ($query) {
                    $query->when($this->employee_name, function ($query) {
                        $query->where('name', 'like', '%' . $this->employee_name . '%');
                    });
                })
                ->when($this->division, function ($query) {
                    $query->where('division_id', $this->division);
                })
                ->when($this->branch, function ($query) {
                    $query->where('branch_id', $this->branch);
                })
                ->whereHas('kpiTagged', function ($query) {
                    $query->select('position_id')->distinct();
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
