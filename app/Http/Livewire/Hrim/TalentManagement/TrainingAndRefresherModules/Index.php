<?php

namespace App\Http\Livewire\Hrim\TalentManagement\TrainingAndRefresherModules;

use App\Interfaces\Hrim\TalentManagement\TrainingAndRefresherModulesInterface;
use App\Traits\Hrim\TalentManagement\TrainingAndRefresherTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use TrainingAndRefresherTrait, PopUpMessagesTrait, WithPagination;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create') {
            $this->create_modal = true;
        } elseif ($action_type == 'play') {
            $this->play_modal = true;
            $this->train_and_ref_id = $data['id'];
        } elseif ($action_type == 'edit') {
            $this->edit_modal = true;
            $this->train_and_ref_id = $data['id'];
        } elseif ($action_type == 'view') {
            $this->view_modal = true;
            $this->train_and_ref_id = $data['id'];
        } elseif ($action_type == 'delete') {
            $this->confirmation_modal = true;
            $this->train_and_ref_id = $data['id'];
            $this->confirmation_message = "Are you sure you want to delete this video?";
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function confirm(TrainingAndRefresherModulesInterface $training_and_refreshers_interface)
    {
        if ($this->action_type == "delete") {
            $response = $training_and_refreshers_interface->destroy($this->train_and_ref_id);
        }

        if ($response['code'] == 200) {
            $this->emit('index');
            $this->confirmation_modal = false;
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(TrainingAndRefresherModulesInterface $training_and_refreshers_interface)
    {
        $request = [
            'employee_name_search' => $this->employee_name_search,
            'position' => $this->position,
            'division' => $this->division,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $training_and_refreshers_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'training_and_refreshers' => [],
                    'get_tagged_position' => [],
                ];
        }

        return view('livewire.hrim.talent-management.training-and-refresher-modules.index', [
            'training_and_refreshers' => $response['result']['training_and_refreshers'],
            'get_tagged_position' => $response['result']['get_tagged_position']
        ]);
    }
}
