<?php

namespace App\Http\Livewire\Hrim\TalentManagement\TrainingAndRefresherModules;

use App\Interfaces\Hrim\TalentManagement\TrainingAndRefresherModulesInterface;
use App\Models\Hrim\TrainingAndRefreshers;
use App\Traits\Hrim\TalentManagement\TrainingAndRefresherTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class View extends Component
{
    use TrainingAndRefresherTrait, PopUpMessagesTrait;
    public $get_tagged_position = [];
    
    protected $listeners = ['view' => 'mount'];

    public function mount(TrainingAndRefresherModulesInterface $training_and_refresher_interface, $id)
    {
        $this->resetForm();
        $response = $training_and_refresher_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->train_and_ref = $response['result'];

        $this->title = $this->train_and_ref->trainingRef->title;
        $this->description = $this->train_and_ref->trainingRef->description;
        $this->is_all_tagged = $this->train_and_ref->trainingRef->is_tag_all_position;
        $this->video_ref = $this->train_and_ref->trainingRef->video_ref;

        $this->get_tagged_position = TrainingAndRefreshers::get();

        $this->attachments[] = [
            'id' => $this->train_and_ref->id,
            'attachment' => '',
            'path' => $this->train_and_ref->path,
            'name' => $this->train_and_ref->name,
            'extension' => $this->train_and_ref->extension,
            'original_filename' => $this->train_and_ref->original_filename,
            'is_deleted' => false,
        ];
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.training-and-refresher-modules.view');
    }
}
