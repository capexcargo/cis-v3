<?php

namespace App\Http\Livewire\Hrim\TalentManagement\TrainingAndRefresherModules;

use App\Interfaces\Hrim\TalentManagement\TrainingAndRefresherModulesInterface;
use App\Models\Hrim\Position;
use App\Models\Hrim\TrainingAndRefreshers;
use App\Traits\Hrim\TalentManagement\TrainingAndRefresherTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use TrainingAndRefresherTrait, PopUpMessagesTrait, WithFileUploads;
    public $get_tagged_position = [];

    protected $listeners = ['edit' => 'mount'];

    public function mount(TrainingAndRefresherModulesInterface $training_and_refresher_interface, $id)
    {
        $this->resetForm();
        $response = $training_and_refresher_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->train_and_ref = $response['result'];

        $this->title = $this->train_and_ref->trainingRef->title;
        $this->description = $this->train_and_ref->trainingRef->description;
        $this->is_all_tagged = $this->train_and_ref->trainingRef->is_tag_all_position;
        $this->video_ref = $this->train_and_ref->trainingRef->video_ref;
        $this->division = $this->train_and_ref->trainingRef->first()->tagged_division;

        $this->get_tagged_position = TrainingAndRefreshers::where('video_ref', $this->video_ref)->get();

        $get_positions = Position::where('division_id', $this->division)->orderBy('id', 'desc')->get();

        foreach ($get_positions as $i => $get) {
            $found = false;

            foreach ($this->get_tagged_position as $tagged_position) {
                if ($tagged_position->position == $get->id) {
                    $this->positions[$i] = [
                        'position' => $get->id
                    ];
                    $found = true;
                    break;
                } else {
                    $this->positions[$i] = [
                        'position' => null
                    ];
                }
            }

            if (!$found) {
                $this->positions[$i] = [
                    'position' => null
                ];
            }
        }

        $this->attachments[] = [
            'id' => $this->train_and_ref->id,
            'attachment' => '',
            'path' => $this->train_and_ref->path,
            'name' => $this->train_and_ref->name,
            'extension' => $this->train_and_ref->extension,
            'original_filename' => $this->train_and_ref->original_filename,
            'is_deleted' => false,
        ];
    }

    public function isChecked($id, $index)
    {
        $this->positions[$index]['position'] = $id;
    }

    public function confirmationSubmit()
    {
        if (!($this->is_all_tagged || $this->division)) {
            $this->validate(
                [
                    'title' => 'required',
                    'description' => 'required',
                    'tag_atleast_one' => 'required',
                    'attachments' => 'required',
                    'attachments.*.attachment' => 'sometimes|mimes:mp4,mov',
                ],
                [
                    'tag_atleast_one.required' => 'Tag to All Positions or Division.',
                    'attachments.*.attachment.mimes' => 'Video file must be mp4 or mov.',
                ]
            );
        } else {
            if ($this->division) {
                if (count($this->positions) > 0) {
                    $this->validate(
                        [
                            'title' => 'required',
                            'description' => 'required',
                            'is_all_tagged' => 'nullable',
                            'division' => 'nullable',
                            'positions' => 'required',
                            'positions.*.position' => 'required',
                            'attachments' => 'required',
                            'attachments.*.attachment' => 'sometimes|mimes:mp4,mov',
                        ],
                        [
                            'attachments.*.attachment.mimes' => 'Video file must be mp4 or mov.',
                        ]
                    );
                } else {
                    $this->validate(
                        [
                            'title' => 'required',
                            'description' => 'required',
                            'is_all_tagged' => 'nullable',
                            'division' => 'nullable',
                            'positions' => 'required',
                            'positions.*.position' => 'required',
                            'attachments' => 'required',
                            'attachments.*.attachment' => 'sometimes|mimes:mp4,mov',
                        ],
                        [
                            'positions.required' => 'Select at least one position',
                            'attachments.*.attachment.mimes' => 'Video file must be mp4 or mov.',
                        ]
                    );
                }
            } else {
                $this->validate(
                    [
                        'title' => 'required',
                        'description' => 'required',
                        'is_all_tagged' => 'nullable',
                        'division' => 'nullable',
                        'positions' => 'sometimes',
                        'positions.*.position' => 'sometimes',
                        'attachments' => 'required',
                        'attachments.*.attachment' => 'sometimes|mimes:mp4,mov',
                    ],
                    [
                        'attachments.*.attachment.mimes' => 'Video file must be mp4 or mov.',
                    ]
                );
            }
        }

        $this->confirmation_message = "Are you sure you want to update this module?";
        $this->confirmation_modal = true;
    }

    public function submit(TrainingAndRefresherModulesInterface $training_and_refresher_modules_interface)
    {
        $response = $training_and_refresher_modules_interface->update($this->train_and_ref->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = false;
            $this->resetForm();
            $this->load();
            $this->resetErrorBag();
            $this->emitTo('hrim.talent-management.training-and-refresher-modules.index', 'close_modal', 'edit');
            $this->emitTo('hrim.talent-management.training-and-refresher-modules.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.training-and-refresher-modules.edit');
    }
}
