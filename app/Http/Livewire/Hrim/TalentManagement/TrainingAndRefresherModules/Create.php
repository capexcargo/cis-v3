<?php

namespace App\Http\Livewire\Hrim\TalentManagement\TrainingAndRefresherModules;

use App\Interfaces\Hrim\TalentManagement\TrainingAndRefresherModulesInterface;
use App\Traits\Hrim\TalentManagement\TrainingAndRefresherTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use TrainingAndRefresherTrait, PopUpMessagesTrait, WithFileUploads;

    public function confirmationSubmit()
    {
        if (!($this->is_all_tagged || $this->division)) {
            $this->validate(
                [
                    'title' => 'required',
                    'description' => 'required',
                    'tag_atleast_one' => 'required',
                    'attachments' => 'required',
                    'attachments.*.attachment' => 'required|mimes:mp4,mov',
                ],
                [
                    'tag_atleast_one.required' => 'Tag to All Positions or Division.',
                    'attachments.*.attachment.required' => 'Video file is required.',
                ]
            );
        } else {
            if ($this->division) {
                if (count($this->positions) > 0) {
                    $this->validate(
                        [
                            'title' => 'required',
                            'description' => 'required',
                            'is_all_tagged' => 'nullable',
                            'division' => 'nullable',
                            'positions' => 'required',
                            'positions.*.position' => 'required',
                            'attachments' => 'required',
                            'attachments.*.attachment' => 'required|mimes:mp4,mov',
                        ],
                        [
                            'attachments.*.attachment.required' => 'Video file is required.',
                        ]
                    );
                } else {
                    $this->validate(
                        [
                            'title' => 'required',
                            'description' => 'required',
                            'is_all_tagged' => 'nullable',
                            'division' => 'nullable',
                            'positions' => 'required',
                            'positions.*.position' => 'required',
                            'attachments' => 'required',
                            'attachments.*.attachment' => 'required|mimes:mp4,mov',
                        ],
                        [
                            'positions.required' => 'Select at least one position',
                            'attachments.*.attachment.required' => 'Video file is required.',
                        ]
                    );
                }
            } else {
                $this->validate(
                    [
                        'title' => 'required',
                        'description' => 'required',
                        'is_all_tagged' => 'nullable',
                        'division' => 'nullable',
                        'positions' => 'sometimes',
                        'positions.*.position' => 'sometimes',
                        'attachments' => 'required',
                        'attachments.*.attachment' => 'required|mimes:mp4,mov',
                    ],
                    [
                        'attachments.*.attachment.required' => 'Video file is required.',
                    ]
                );
            }
        }

        $this->confirmation_message = "Are you sure you want to create this module?";
        $this->confirmation_modal = true;
    }

    public function submit(TrainingAndRefresherModulesInterface $training_and_refresher_modules_interface)
    {
        $response = $training_and_refresher_modules_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = false;
            $this->resetForm();
            $this->load();
            $this->resetErrorBag();
            $this->emitTo('hrim.talent-management.training-and-refresher-modules.index', 'close_modal', 'create');
            $this->emitTo('hrim.talent-management.training-and-refresher-modules.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.training-and-refresher-modules.create');
    }
}
