<?php

namespace App\Http\Livewire\Hrim\TalentManagement\PercentageManagement;

use App\Interfaces\Hrim\TalentManagement\PercentageManagementInterface;
use App\Traits\Hrim\TalentManagement\PercentageManagementTrait;
use App\Traits\Hrim\TalentManagement\PerformanceHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use PerformanceHeaderTrait, PercentageManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_percentage') {
            $this->create_modal = true;
        } else if ($action_type == 'edit_percentage') {
            $this->emitTo('hrim.talent-management.percentage-management.edit', 'mount', $data['id']);
            $this->percentage_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'delete_percentage') {
            $this->confirmation_message = "Are you sure you want to delete this percentage?";
            $this->delete_modal = true;
            $this->percentage_id = $data['id'];
        }
    }
    
    public function delete(PercentageManagementInterface $percentage_management_interface)
    {
        if ($this->action_type == 'delete_percentage') {
            $response = $percentage_management_interface->destroy($this->percentage_id);
        }

        if ($response['code'] == 200) {
            $this->delete_modal = false;
            $this->emitTo('hrim.talent-management.percentage-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function render(PercentageManagementInterface $percentage_management_interface)
    {
        $request = [
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $percentage_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'percentage_managements' => [],
                ];
        }
        return view('livewire.hrim.talent-management.percentage-management.index', [
            'percentage_managements' => $response['result']['percentage_managements']
        ]);
    }
}
