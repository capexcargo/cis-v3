<?php

namespace App\Http\Livewire\Hrim\TalentManagement\PercentageManagement;

use App\Interfaces\Hrim\TalentManagement\PercentageManagementInterface;
use App\Traits\Hrim\TalentManagement\PercentageManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use PercentageManagementTrait, PopUpMessagesTrait;

    protected $rules = [
        'job_level' => 'required|unique:hrim_performance_breakdown_pct,job_level_id',
        'goal_percentage' => 'required',
        'core_value_percentage' => 'required',
        'lead_com_percentage' => 'required',
    ];

    public function confirmationSubmit()
    {
        $this->validate();
        $this->confirmation_modal = true;
    }

    public function submit(PercentageManagementInterface $percentage_management_interface)
    {
        $response = $percentage_management_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.talent-management.percentage-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.talent-management.percentage-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.percentage-management.create');
    }
}
