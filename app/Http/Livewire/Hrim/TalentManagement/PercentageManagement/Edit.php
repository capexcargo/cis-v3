<?php

namespace App\Http\Livewire\Hrim\TalentManagement\PercentageManagement;

use App\Interfaces\Hrim\PayrollManagement\AdminAdjustmentInterface;
use App\Interfaces\Hrim\TalentManagement\PercentageManagementInterface;
use App\Traits\Hrim\TalentManagement\PercentageManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use PercentageManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(PercentageManagementInterface $percentage_management_interface, $id)
    {
        $this->resetForm();
        $response = $percentage_management_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->percentage = $response['result'];

        $this->job_level = $this->percentage->job_level_id;
        $this->goal_percentage = $this->percentage->goals;
        $this->core_value_percentage = $this->percentage->core_values;
        $this->lead_com_percentage = $this->percentage->leadership_competencies;
    }

    public function submit(PercentageManagementInterface $percentage_management_interface)
    {
        $response = $percentage_management_interface->update($this->getRequest(), $this->percentage->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.talent-management.percentage-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.talent-management.percentage-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.percentage-management.edit');
    }
}
