<?php

namespace App\Http\Livewire\Hrim\TalentManagement\Performance;

use App\Traits\Hrim\TalentManagement\PerformanceHeaderTrait;
use Livewire\Component;

class Index extends Component
{
    use PerformanceHeaderTrait;

    public function render()
    {
        return view('livewire.hrim.talent-management.performance.index');
    }
}
