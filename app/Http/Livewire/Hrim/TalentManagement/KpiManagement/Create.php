<?php

namespace App\Http\Livewire\Hrim\TalentManagement\KpiManagement;

use App\Interfaces\Hrim\TalentManagement\KpiManagementInterface;
use App\Traits\Hrim\TalentManagement\KpiManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use KpiManagementTrait, PopUpMessagesTrait;

    public function submit(KpiManagementInterface $kpi_management_interface)
    {
        $response = $kpi_management_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.talent-management.kpi-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.talent-management.kpi-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.kpi-management.create');
    }
}
