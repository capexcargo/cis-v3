<?php

namespace App\Http\Livewire\Hrim\TalentManagement\KpiManagement;

use App\Interfaces\Hrim\TalentManagement\KpiManagementInterface;
use App\Traits\Hrim\TalentManagement\KpiManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use KpiManagementTrait, WithPagination, PopUpMessagesTrait;
    protected $listeners = ['mount' => 'mount'];

    public function mount(KpiManagementInterface $kpi_management_interface, $id)
    {
        $this->resetForm();
        $response = $kpi_management_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->kpi = $response['result'];
        $this->kpi_name = $this->kpi->name;
        $this->quarter = $this->kpi->quarter;
        $this->year = $this->kpi->year;
        $this->target = $this->kpi->target;
        $this->points = $this->kpi->points;
        $this->remarks = $this->kpi->remarks;
    }

    public function submit(KpiManagementInterface $kpi_management_interface)
    {
        $response = $kpi_management_interface->update($this->kpi->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.talent-management.kpi-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.talent-management.kpi-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.kpi-management.edit');
    }
}
