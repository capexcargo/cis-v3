<?php

namespace App\Http\Livewire\Hrim\TalentManagement\KpiManagement;

use App\Interfaces\Hrim\TalentManagement\KpiManagementInterface;
use App\Traits\Hrim\TalentManagement\KpiManagementTrait;
use App\Traits\Hrim\TalentManagement\PerformanceHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use KpiManagementTrait, PerformanceHeaderTrait, WithPagination, PopUpMessagesTrait;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_kpi') {
            $this->create_kpi_modal = true;
        } else if ($action_type == 'edit_kpi') {
            $this->emitTo('hrim.talent-management.kpi-management.edit', 'mount', $data['id']);
            $this->kpi_id = $data['id'];
            $this->edit_kpi_modal = true;
        } else if ($action_type == 'delete_kpi') {
            $this->confirmation_message = "Are you sure you want to delete this KPI?";
            $this->confirmation_modal = true;
            $this->kpi_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_kpi_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_kpi_modal = false;
        } else if ($action_type == 'delete') {
            $this->confirmation_modal = false;
        }
    }

    public function deleteKPI(KpiManagementInterface $kpi_management_interface)
    {
        if ($this->action_type == 'delete_kpi') {
            $response = $kpi_management_interface->destroy($this->kpi_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.talent-management.kpi-management.index', 'close_modal', 'delete');
            $this->emitTo('hrim.talent-management.kpi-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(KpiManagementInterface $kpi_management_interface)
    {
        $request = [
            'kpi_name' => $this->kpi_name,
            'quarter' => $this->quarter,
            'year' => $this->year,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];
        $response = $kpi_management_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.talent-management.kpi-management.index', [
            'kpi_managements' => $response['result']
        ]);
    }
}
