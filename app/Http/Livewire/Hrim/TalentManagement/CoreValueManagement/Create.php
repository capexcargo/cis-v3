<?php

namespace App\Http\Livewire\Hrim\TalentManagement\CoreValueManagement;

use App\Interfaces\Hrim\TalentManagement\CoreValueManagementInterface;
use App\Traits\Hrim\TalentManagement\CoreValueManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use CoreValueManagementTrait, PopUpMessagesTrait;

    public function submit(CoreValueManagementInterface $core_value_management_interface)
    {
        $response = $core_value_management_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.talent-management.core-value-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.talent-management.core-value-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.talent-management.core-value-management.create');
    }
}
