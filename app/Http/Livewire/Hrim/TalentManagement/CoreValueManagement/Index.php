<?php

namespace App\Http\Livewire\Hrim\TalentManagement\CoreValueManagement;

use App\Interfaces\Hrim\TalentManagement\CoreValueManagementInterface;
use App\Traits\Hrim\TalentManagement\CoreValueManagementTrait;
use App\Traits\Hrim\TalentManagement\PerformanceHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use CoreValueManagementTrait, PerformanceHeaderTrait, WithPagination, PopUpMessagesTrait;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_core_value') {
            $this->create_core_value_modal = true;
        } else if ($action_type == 'edit_core_value') {
            $this->emitTo('hrim.talent-management.core-value-management.edit', 'mount', $data['id']);
            $this->core_value_id = $data['id'];
            $this->edit_core_value_modal = true;
        } else if ($action_type == 'delete_core_value') {
            $this->confirmation_message = "Are you sure you want to delete this Core Value?";
            $this->confirmation_modal = true;
            $this->core_value_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_core_value_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_core_value_modal = false;
        } else if ($action_type == 'delete') {
            $this->confirmation_modal = false;
        }
    }
    
    public function deleteCoreValue(CoreValueManagementInterface $core_value_management_interface){
        if ($this->action_type == 'delete_core_value') {
            $response = $core_value_management_interface->destroy($this->core_value_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.talent-management.core-value-management.index', 'close_modal', 'delete');
            $this->emitTo('hrim.talent-management.core-value-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(CoreValueManagementInterface $core_value_management_interface)
    {
        $request = [
            'core_value' => $this->core_value,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $core_value_management_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.talent-management.core-value-management.index', [
            'core_values_management' => $response['result']
        ]);
    }
}
