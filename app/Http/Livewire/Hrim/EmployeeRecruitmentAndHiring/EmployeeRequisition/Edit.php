<?php

namespace App\Http\Livewire\Hrim\EmployeeRecruitmentAndHiring\EmployeeRequisition;

use App\Interfaces\Hrim\RecruitmentAndHiring\EmployeeRequisitionInterface;
use App\Models\Hrim\EmployeeRequisition;
use App\Traits\Hrim\RecruitmentAndHiring\EmloyeeRequisitionTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use EmloyeeRequisitionTrait, WithFileUploads, PopUpMessagesTrait;

    protected $listeners = ['employee_requisition_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->employee_requisition = EmployeeRequisition::findOrFail($id);

        $this->erf_reference = $this->employee_requisition->erf_reference_no;
        $this->branch = $this->employee_requisition->branch_id;
        $this->division = $this->employee_requisition->division_id;
        $this->position = $this->employee_requisition->position_id;
        $this->job_level = $this->employee_requisition->job_level_id;
        $this->employment_cat = $this->employee_requisition->employment_category_id;
        $this->emp_cat_type = $this->employee_requisition->employment_type_id;
        $this->project_name = $this->employee_requisition->project_name;
        $this->duration_from = $this->employee_requisition->duration_from;
        $this->duration_to = $this->employee_requisition->duration_to;
        $this->request_reason = $this->employee_requisition->requisition_reason_id;
        $this->target_hire_date = $this->employee_requisition->target_hire;
        $this->approvers();
        foreach ($this->employee_requisition->attachments as $attachment) {
            $this->attachments[] = [
                'id' => $attachment->id,
                'attachment' => '',
                'path' => $attachment->path,
                'name' => $attachment->name,
                'extension' => $attachment->extension,
                'is_deleted' => false,
            ];
        }
    }

    public function submit(EmployeeRequisitionInterface $employee_requisition_interface)
    {
        // dd($employee_requisition_interface);
        
        if ($this->employment_cat == 2) {
            $validated = $this->validate([
                'erf_reference' => 'required',
                'branch' => 'required',
                'division' => 'required',
                'position' => 'required',
                'job_level' => 'required',
                'employment_cat' => 'required',
                'emp_cat_type' => 'required',
                'project_name' => 'required',
                'duration_from' => 'required',
                'duration_to' => 'required',
                'request_reason' => 'required',
                'target_hire_date' => 'required',

                'attachments' => 'required',
                // 'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),
            ],[
                'attachments.*.attachment.required' => 'This attachment field is required.',
                'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png,xlsx,doc,docx.',
            ]);
        } else {
            $validated = $this->validate([
                'erf_reference' => 'required',
                'branch' => 'required',
                'division' => 'required',
                'position' => 'required',
                'job_level' => 'required',
                'employment_cat' => 'required',
                'request_reason' => 'required',
                'target_hire_date' => 'required',
                
                'attachments' => 'required',
                // 'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),
            ],[
                'attachments.*.attachment.required' => 'This attachment field is required.',
                'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png,xlsx,doc,docx.',
            ]);
        }

        $response = $employee_requisition_interface->update($this->employee_requisition, $validated);
        
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-recruitment-and-hiring.employee-requisition.index', 'close_modal', 'edit');
            $this->emitTo('hrim.employee-recruitment-and-hiring.employee-requisition.index', 'load_header_cards');
            $this->emitTo('hrim.employee-recruitment-and-hiring.employee-requisition.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-recruitment-and-hiring.employee-requisition.edit');
    }
}
