<?php

namespace App\Http\Livewire\Hrim\EmployeeRecruitmentAndHiring\EmployeeRequisition;

use App\Interfaces\Hrim\CompanyManagement\EmployerComplianceInterface;
use App\Interfaces\Hrim\RecruitmentAndHiring\EmployeeRequisitionInterface;
use App\Models\Hrim\EmployeeRequisition;
use App\Models\Hrim\StatusReference;
use App\Traits\Hrim\RecruitmentAndHiring\EmloyeeRequisitionTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use EmloyeeRequisitionTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];
    public $position_search;

    public function load()
    {
        $this->loadStatusHeaderCards();
    }

    public function loadStatusHeaderCards()
    {
        $status = StatusReference::withCount('employee_requisitions')->whereHas('employee_requisitions', function ($query) {
            $query->where('created_by', Auth::user()->id);
        })
            ->get();


        $this->status_header_cards = [
            [
                'title' => 'All',
                'value' => $status->sum('employee_requisitions_count'),
                'class' => 'bg-gray-100 text-gray-500 border border-gray-600 shadow-sm rounded-l-md',
                'color' => 'text-blue',
                'action' => 'final_status',
                'id' => false
            ],
            [
                'title' => 'For Approval',
                'value' => $status[0]->employee_requisitions_count ?? 0 + $status[1]->employee_requisitions_count ?? 0,
                'class' => 'bg-gray-100 border border-gray-600 shadow-sm text-gray-500 ',
                'color' => 'text-blue',
                'action' => 'final_status',
                'id' => 2
            ],
            [
                'title' => 'Approved',
                'value' => $status[2]->employee_requisitions_count ?? 0,
                'class' => 'bg-gray-100 border border-gray-600 shadow-sm text-gray-500',
                'color' => 'text-blue',
                'action' => 'final_status',
                'id' => 3
            ],
            [
                'title' => 'Declined',
                'value' => $status[3]->employee_requisitions_count ?? 0,
                'class' => 'bg-gray-100 border border-gray-600 shadow-sm text-gray-500 rounded-r-md',
                'color' => 'text-blue',
                'action' => 'final_status',
                'id' => 4
            ],

        ];
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_employee_requisition') {
            $this->emit('generate_erf_reference');
            $this->create_employee_requisition_modal = true;
        } else if ($action_type == 'edit_employee_requisition') {
            $this->emit('employee_requisition_edit_mount', $data['id']);
            $this->edit_employee_requisition_modal = true;
            $this->employee_requisition_id = $data['id'];
        } else if ($action_type == 'cancel_employee_requisition') {
            $this->confirmation_message = "Are you sure you want to cancel this request?";
            $this->confirmation_modal = true;
            $this->employee_requisition_id = $data['id'];
        } else if ($action_type == 'view_employee_requisition') {
            $this->emit('employee_requisition_view_mount', $data['id']);
            $this->view_employee_requisition_modal = true;
            $this->employee_requisition_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_employee_requisition_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_employee_requisition_modal = false;
        } else if ($action_type == 'view') {
            $this->view_employee_requisition_modal = false;
        } else if ($action_type == 'cancel') {
            $this->confirmation_modal = false;
        }
    }

    public function cancelRequest(EmployeeRequisitionInterface $employee_requisition_interface)
    {
        if ($this->action_type == 'cancel_employee_requisition') {
            $response = $employee_requisition_interface->cancel($this->employee_requisition_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.employee-recruitment-and-hiring.employee-requisition.index', 'close_modal', 'cancel');
            $this->emitTo('hrim.employee-recruitment-and-hiring.employee-requisition.index', 'load_header_cards');
            $this->emitTo('hrim.employee-recruitment-and-hiring.employee-requisition.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-recruitment-and-hiring.employee-requisition.index', [
            'employee_requisitions' => EmployeeRequisition::with('branch', 'division', 'position', 'job_level', 'employment_category', 'employment_category_type', 'requisition_reason', 'user', 'status')
                ->whereHas('position', function ($query) {
                    $query->when($this->position_search, function ($query) {
                        $query->where('display', 'like', '%' . $this->position_search . '%');
                    });
                })
                ->when($this->final_status, function ($query) {
                    $query->where('final_status', $this->final_status);
                })
                ->when($this->date_created, function ($query) {
                    $query->whereDate('created_at', $this->date_created);
                })
                ->when($this->position, function ($query) {
                    $query->where('position_id', $this->position);
                })
                ->where('created_by', Auth::user()->id)
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
