<?php

namespace App\Http\Livewire\Hrim\EmployeeRecruitmentAndHiring\EmployeeRequisition;

use App\Repositories\Hrim\RecruitmentAndHiring\EmployeeRequisitionRepository;
use App\Traits\Hrim\RecruitmentAndHiring\EmloyeeRequisitionTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use EmloyeeRequisitionTrait, WithFileUploads, PopUpMessagesTrait;

    protected $listeners = ['generate_erf_reference' => 'generateErfReference', 'mount' => 'mount'];

    public function submit()
    {
        if ($this->employment_cat == 2) {
            $validated = $this->validate([
                'erf_reference' => 'required',
                'branch' => 'required',
                'division' => 'required',
                'position' => 'required',
                'job_level' => 'required',
                'employment_cat' => 'required',
                'emp_cat_type' => 'required',
                'project_name' => 'required',
                'duration_from' => 'required',
                'duration_to' => 'required',
                'request_reason' => 'required',
                'target_hire_date' => 'required',

                'attachments' => 'required',
                'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),
            ], [
                'attachments.*.attachment.required' => 'This attachment field is required.',
                'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png,xlsx,doc,docx.',
            ]);
        } else {
            $validated = $this->validate([
                'erf_reference' => 'required',
                'branch' => 'required',
                'division' => 'required',
                'position' => 'required',
                'job_level' => 'required',
                'employment_cat' => 'required',
                'request_reason' => 'required',
                'target_hire_date' => 'required',

                'attachments' => 'required',
                'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),
            ], [
                'attachments.*.attachment.required' => 'This attachment field is required.',
                'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png,xlsx,doc,docx.',
            ]);
        }

        $employee_requisition_repository = new EmployeeRequisitionRepository();
        $response = $employee_requisition_repository->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-recruitment-and-hiring.employee-requisition.index', 'close_modal', 'create');
            $this->emitTo('hrim.employee-recruitment-and-hiring.employee-requisition.index', 'load_header_cards');
            $this->emitTo('hrim.employee-recruitment-and-hiring.employee-requisition.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-recruitment-and-hiring.employee-requisition.create');
    }
}
