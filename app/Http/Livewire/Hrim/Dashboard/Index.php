<?php

namespace App\Http\Livewire\Hrim\Dashboard;

use App\Interfaces\Hrim\Dashboard\DashboardInterface;
use App\Models\Hrim\Leave;
use App\Models\Hrim\Tar;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Index extends Component
{

    use PopUpMessagesTrait;

    public $action_type;

    public $employees_on_leave = [];
    public $tar_report = [];

    public $get_status = [];

    public $status_probationary;
    public $status_contractual;
    public $status_regular;
    public $status_regulars;

    public $get_status_by_branchs = [];
    public $get_status_by_branchs_res = [];
    public $get_status_by_branchs_res_footer_reg = [];
    public $get_status_by_branchs_res_footer_contractual = [];
    public $get_status_by_branchs_res_footer_probi = [];

    public $regular;

    public $get_emp_status = [];
    public $direct_emp;
    public $csp_emp;

    public $get_emp_gender = [];
    public $male_emp;
    public $female_emp;

    public $get_emp_age = [];
    public $age1824 = [];
    public $age2534 = [];
    public $age3544 = [];
    public $age4554 = [];
    public $age5564 = [];
    public $age64up = [];

    public $get_salary_grade = [];
    public $salary_grade_a1;
    public $salary_grade_a2;
    public $salary_grade_a3;
    public $salary_grade_b1;
    public $salary_grade_b2;
    public $salary_grade_b3;
    public $salary_grade_c1;
    public $salary_grade_c2;
    public $salary_grade_c3;
    public $salary_grade_d1;
    public $salary_grade_d2;

    public $get_emp_requisition = [];

    public $get_emp_late = [];
    public $get_emp_on_leave = [];
    public $get_emp_today_absent = [];

    public $listeners = ['index' => 'mount'];

    public function mount(DashboardInterface $dashboard_interface)
    {
        $this->employees_on_leave = Leave::with('user', 'leaveType')->get();

        // HEADER CARD - ACTIVE EMPLOYEES

        $response = $dashboard_interface->countEmployeeByStatus();
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }
        $this->get_status = $response['result']['get_status'] ?? [];

        $this->status_regular = $this->get_status[0]['regular'];
        $this->status_probationary = $this->get_status[2]['probi'];
        $this->status_contractual = $this->get_status[3]['contractual'];

        // GET ACTIVE EMPLOYEES BRANCH BY STATUS - LIST

        $response2 = $dashboard_interface->countEmployeeStatusByBranch();
        if ($response2['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response2['message'], $response2['result']);
            $response2['result'] = [];
        }

        $this->get_status_by_branchs = $response2['result']['get_branch_status'];
        $this->tar_report = Tar::with('user', 'finalStatus')->get();

        foreach ($this->get_status_by_branchs as $val) {
            $this->get_status_by_branchs_res[] = $val;
            $this->get_status_by_branchs_res_footer_reg[] = $val['regular'];
            $this->get_status_by_branchs_res_footer_contractual[] = $val['contractual'];
            $this->get_status_by_branchs_res_footer_probi[] = $val['probi'];
        }

        // GET EMPLOYEE STATUS

        $res = $dashboard_interface->graphs();
        if ($res['code'] != 200) {
            $this->sweetAlertDefaultError('error', $res['message'], $res['result']);
            $res['result'] = [];
        }

        // GET EMPLOYEE CATEGORY
        $this->get_emp_status = $res['result']['get_emp_status'] ?? [];
        $this->direct_emp = $this->get_emp_status[0]['direct'] ?? 0;
        $this->csp_emp = $this->get_emp_status[1]['csp'] ?? 0;

        // GET EMPLOYEE GENDER
        $this->get_emp_gender = $res['result']['get_emp_gender'] ?? [];
        $this->male_emp = $this->get_emp_gender[0]['male'] ?? 0;
        $this->female_emp = $this->get_emp_gender[1]['female'] ?? 0;

        // GET EMPLOYEE AGE
        $this->get_emp_age = $res['result']['get_emp_age'] ?? [];
        foreach ($this->get_emp_age as $i => $emp_age) {
            if ($emp_age['age'] >= 18 && $emp_age['age'] <= 24) {
                $this->age1824[] = $emp_age['age'];
            }
            if ($emp_age['age'] >= 25 && $emp_age['age'] <= 34) {
                $this->age2534[] = $emp_age['age'];
            }
            if ($emp_age['age'] >= 35 && $emp_age['age'] <= 44) {
                $this->age3544[] = $emp_age['age'];
            }
            if ($emp_age['age'] >= 45 && $emp_age['age'] <= 54) {
                $this->age4554[] = $emp_age['age'];
            }
            if ($emp_age['age'] >= 55 && $emp_age['age'] <= 64) {
                $this->age5564[] = $emp_age['age'];
            }
            if ($emp_age['age'] >= 64) {
                $this->age64up[] = $emp_age['age'];
            }
        }

        // GET EMPLOYEE SALARY GRADE
        $this->get_salary_grade = $res['result']['get_salary_grade'] ?? [];
        $this->salary_grade_a1 = $this->get_salary_grade[0]['a1'] ?? 0;
        $this->salary_grade_a2 = $this->get_salary_grade[1]['a2'] ?? 0;
        $this->salary_grade_a3 = $this->get_salary_grade[2]['a3'] ?? 0;
        $this->salary_grade_b1 = $this->get_salary_grade[3]['b1'] ?? 0;
        $this->salary_grade_b2 = $this->get_salary_grade[4]['b2'] ?? 0;
        $this->salary_grade_b3 = $this->get_salary_grade[5]['b3'] ?? 0;
        $this->salary_grade_c1 = $this->get_salary_grade[6]['c1'] ?? 0;
        $this->salary_grade_c2 = $this->get_salary_grade[7]['c2'] ?? 0;
        $this->salary_grade_c3 = $this->get_salary_grade[8]['c3'] ?? 0;
        $this->salary_grade_d1 = $this->get_salary_grade[9]['d1'] ?? 0;
        $this->salary_grade_d2 = $this->get_salary_grade[10]['d2'] ?? 0;

        // GET EMPLOYEE REQUISITION
        $this->get_emp_requisition = $res['result']['get_emp_requisition'] ?? [];

        //GET EMPLOYEE ON LEAVE TODAY
        $response3 = $dashboard_interface->tableLists();
        if ($res['code'] != 200) {
            $this->sweetAlertDefaultError('error', $res['message'], $res['result']);
            $res['result'] = [];
        }

        $this->get_emp_late = $response3['result']['get_emp_late'] ?? [];

        $this->get_emp_on_leave = $response3['result']['get_emp_on_leave'] ?? [];

        $this->get_emp_today_absent = $response3['result']['get_emp_today_absent'] ?? [];

        // dd($this->get_emp_late);
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'see_details_emp_info') {
            return redirect()->route('hrim.employee-management.employee-information.index');
        } elseif ($action_type == 'see_details_employee_req') {
            return redirect()->route('hrim.recruitment-and-hiring.employee-requisition.index');
        } elseif ($action_type == 'see_details_total_late') {
            return redirect()->route('hrim.attendance.daily-time-records.index');
        } elseif ($action_type == 'see_details_leave_rec') {
            return redirect()->route('hrim.attendance.leave-records.index');
        } elseif ($action_type == 'see_details_tar') {
            return redirect()->route('hrim.employee-attendance.schedule-adjustment.index');
        }
    }

    public function render()
    {
        return view('livewire.hrim.dashboard.index');
    }
}
