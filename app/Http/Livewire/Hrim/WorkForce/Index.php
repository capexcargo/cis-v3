<?php

namespace App\Http\Livewire\Hrim\Workforce;

use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait, WithFileUploads;


    public $action_type;

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToPosition') {
            return redirect()->to(route('hrim.workforce.position.index'));
        } else if ($action_type == 'redirectToJobLevel') {
            return redirect()->to(route('hrim.workforce.job-level.index'));
        } else if ($action_type == 'redirectToDepartment') {
            return redirect()->to(route('hrim.workforce.department.index'));
        } else if ($action_type == 'redirectToDivision') {
            return redirect()->to(route('accounting.division-management.index'));
        }
    }

    public function render()
    {
        return view('livewire.hrim.workforce.index');
    }
}
