<?php

namespace App\Http\Livewire\Hrim\Workforce\DepartmentManagement;

use App\Interfaces\Hrim\Workforce\DepartmentInterface;
use App\Models\Hrim\Department;
use App\Traits\Hrim\Workforce\DepartmentTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{

    use DepartmentTrait, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->department_dets = Department::findOrFail($id);

        $this->code = $this->department_dets->code;
        $this->department = $this->department_dets->display;
        $this->division = $this->department_dets->division_id;
    }

    public function submit(DepartmentInterface $departmentInterface)
    {
        $validated = $this->validate([
            // 'code' => 'required',
            'department' => 'required',
            'division' => 'required'
        ]);

        $response = $departmentInterface->updateDepartment($this->department_dets, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.workforce.department-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.workforce.department-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            // "code",
            "department",
            "division",
        ]);
    }

    public function render()
    {
        return view('livewire.hrim.workforce.department-management.edit');
    }
}
