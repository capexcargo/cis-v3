<?php

namespace App\Http\Livewire\Hrim\Workforce\DepartmentManagement;

use App\Interfaces\Hrim\Workforce\DepartmentInterface;
use App\Models\Hrim\Department;
use App\Traits\Hrim\Workforce\DepartmentTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use DepartmentTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToPosition') {
            return redirect()->to(route('hrim.workforce.position.index'));
        } else if ($action_type == 'redirectToJobLevel') {
            return redirect()->to(route('hrim.workforce.job-level.index'));
        } else if ($action_type == 'redirectToDepartment') {
            return redirect()->to(route('hrim.workforce.department.index'));
        } else if ($action_type == 'redirectToDivision') {
            return redirect()->to(route('accounting.division-management.index'));
        } else if ($action_type == 'create_department') {
            $this->create_department_modal = true;
        } else if ($action_type == 'edit_department') {
            $this->emit('edit', $data['id']);
            $this->edit_department_modal = true;
            $this->department_id = $data['id'];
        } else if ($action_type == 'delete_department') {
            $this->confirmation_message = "Are you sure you want to delete this department?";
            $this->delete_department_modal = true;
            $this->department_id = $data['id'];
        }
    }


    public function deleteDepartment(DepartmentInterface $departmentInterface)
    {
        if ($this->action_type == 'delete_department') {
            $response = $departmentInterface->destroy($this->department_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.workforce.department-management.index', 'close_modal', 'delete');
            $this->emitTo('hrim.workforce.department-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_department_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_department_modal = false;
        } else if ($action_type == 'delete') {
            $this->delete_department_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {

        return view('livewire.hrim.workforce.department-management.index', [
            'departments' => Department::where('display', 'like', '%' . $this->department . '%')
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'desc' : 'asc');
                })->paginate($this->paginate)

        ]);
    }
}
