<?php

namespace App\Http\Livewire\Hrim\Workforce\DepartmentManagement;

use App\Interfaces\Hrim\Workforce\DepartmentInterface;
use App\Traits\Hrim\Workforce\DepartmentTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use DepartmentTrait, PopUpMessagesTrait;

    public function submit(DepartmentInterface $departmentInterface)
    {
        $validated = $this->validate([
            'department' => 'required',
            'division' => 'required',
        ]);

        $response = $departmentInterface->createDepartment($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.workforce.department-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.workforce.department-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            // "code",
            "department",
            "division",
        ]);
    }

    public function render()
    {
        return view('livewire.hrim.workforce.department-management.create');
    }
}
