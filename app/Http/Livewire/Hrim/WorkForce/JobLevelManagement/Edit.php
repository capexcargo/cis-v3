<?php

namespace App\Http\Livewire\Hrim\Workforce\JobLevelManagement;

use App\Interfaces\Hrim\Workforce\JobLevelInterface;
use App\Models\Hrim\JobLevel;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{

    use PopUpMessagesTrait;

    public $code;
    public $display;

    public $joblevel;

    protected $listeners = ['job_level_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->joblevel = JobLevel::findOrFail($id);

        // $this->code = $this->joblevel->code;
        $this->display = $this->joblevel->display;
    }
    
    public function submit(JobLevelInterface $jobLevelInterface)
    {
        $validated = $this->validate([
            // 'code' => 'required',
            'display' => 'required'
        ]);

        $response = $jobLevelInterface->updateJobLevel($this->joblevel, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.workforce.job-level-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.workforce.job-level-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            // "code",
            "display",
        ]);
    }
    
    public function render()
    {
        return view('livewire.hrim.workforce.job-level-management.edit');
    }
}
