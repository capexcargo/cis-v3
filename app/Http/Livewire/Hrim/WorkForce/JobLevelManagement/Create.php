<?php

namespace App\Http\Livewire\Hrim\Workforce\JobLevelManagement;

use App\Interfaces\Hrim\Workforce\JobLevelInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{

    use PopUpMessagesTrait;

    public $code;
    public $display;

    public function submit(JobLevelInterface $joblevelInterface)
    {
        $validated = $this->validate([
            // 'code' => 'required',
            'display' => 'required',
        ]);

        $response = $joblevelInterface->createJobLevel($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.workforce.job-level-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.workforce.job-level-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            // "code",
            "display"
        ]);
    }
    
    public function render()
    {
        return view('livewire.hrim.workforce.job-level-management.create');
    }
}
