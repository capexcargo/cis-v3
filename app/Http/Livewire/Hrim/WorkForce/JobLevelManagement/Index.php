<?php

namespace App\Http\Livewire\Hrim\Workforce\JobLevelManagement;

use App\Interfaces\Hrim\Workforce\JobLevelInterface;
use App\Models\Hrim\JobLevel;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;


    public $action_type;
    public $confirmation_message;

    public $create_job_level_modal = false;
    public $edit_job_level_modal = false;
    public $delete_job_level_modal = false;
    public $view_job_level_modal = false;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;

    public $job_level;
    public $job_level_id;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToPosition') {
            return redirect()->to(route('hrim.workforce.position.index'));
        } else if ($action_type == 'redirectToJobLevel') {
            return redirect()->to(route('hrim.workforce.job-level.index'));
        } else if ($action_type == 'redirectToDepartment') {
            return redirect()->to(route('hrim.workforce.department.index'));
        } else if ($action_type == 'redirectToDivision') {
            return redirect()->to(route('accounting.division-management.index'));
        } else if ($action_type == 'create_job_level') {
            $this->create_job_level_modal = true;
        } else if ($action_type == 'edit_job_level') {
            $this->emit('job_level_edit_mount', $data['id']);
            $this->edit_job_level_modal = true;
            $this->job_level_id = $data['id'];
        } else if ($action_type == 'delete_job_level') {
            $this->confirmation_message = "Are you sure you want to delete this job level?";
            $this->delete_job_level_modal = true;
            $this->job_level_id = $data['id'];
        }
    }

    public function deleteJobLevel(JobLevelInterface $jobLevelInterface)
    {

        if ($this->action_type == 'delete_job_level') {
            $response = $jobLevelInterface->destroy($this->job_level_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.workforce.job-level-management.index', 'close_modal', 'delete');
            $this->emitTo('hrim.workforce.job-level-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_job_level_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_job_level_modal = false;
        } else if ($action_type == 'delete') {
            $this->delete_job_level_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.hrim.workforce.job-level-management.index', [
            'joblevels' => JobLevel::where('display', 'like', '%' . $this->job_level . '%')
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'desc' : 'asc');
                })->paginate($this->paginate)

        ]);
    }
}
