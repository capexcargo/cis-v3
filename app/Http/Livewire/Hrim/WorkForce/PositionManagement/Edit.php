<?php

namespace App\Http\Livewire\Hrim\Workforce\PositionManagement;

use App\Interfaces\Hrim\Workforce\PositionInterface;
use App\Models\Hrim\JobLevel;
use App\Models\Hrim\Position;
use App\Traits\Hrim\Workforce\PositionTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use PositionTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(PositionInterface $position_interface, $id)
    {

        $this->resetForm();

        $response = $position_interface->show($id);

        if ($response['code'] == 200) {
            $this->position = $response['result'];
        } else {
            return $this->sweetAlesrtError('error', $response['message'], $response['result']);
        }

        $this->position = Position::findOrFail($id);


        $this->display = $this->position->display;
        $this->job_level = $this->position->job_level_id;
        $this->department = $this->position->department_level_id;
        $this->division = $this->position->division_id;
        $this->job_overview = $this->position->job_overview;

        foreach ($this->position->responsibilities as $responsibility) {
            $this->responsibilities[] = [
                'id' => $responsibility->id,
                'duties' => $responsibility->duties,
                'position_id' => $responsibility->position_id,
                'is_deleted' => false,
            ];
        }

        foreach ($this->position->requirements as $requirement) {
            $this->requirements[] = [
                'id' => $requirement->id,
                'code' => $requirement->code,
                'position_id' => $requirement->position_id,
                'is_deleted' => false,
            ];
        }
    }

    public function submit(PositionInterface $position_interface)
    {
        $validated = $this->validate(
            [
                'display' => 'required',
                'job_level' => 'required',
                'department' => 'required',
                'division' => 'required',
                'job_overview' => 'required',
                
                'responsibilities.*.id' => 'sometimes',
                'responsibilities.*.duties' => 'required',
                'responsibilities.*.position_id' => 'sometimes',
                'responsibilities.*.is_deleted' => 'sometimes',

                'requirements.*.id' => 'sometimes',
                'requirements.*.code' => 'required',
                'requirements.*.position_id' => 'sometimes',
                'requirements.*.is_deleted' => 'sometimes',
            ],
            [
                'display.required' => 'The position field is required.',
                'responsibilities.*.duties.required' => 'The responsibility and duty field is required.',
                'requirements.*.code.required' => 'The job requirement field is required.',
            ]
        );

        $response = $position_interface->update($this->position, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.workforce.position-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.workforce.position-management.index', 'index');

            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.workforce.position-management.edit');
    }
}
