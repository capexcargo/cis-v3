<?php

namespace App\Http\Livewire\Hrim\Workforce\PositionManagement;

use App\Interfaces\Hrim\Workforce\PositionInterface;
use App\Traits\Hrim\Workforce\PositionTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use PositionTrait, PopUpMessagesTrait;

    public $code;
    public $display;
    public $position;
    public $job_level;
    public $department;
    public $division;
    public $job_overview;

    public function mount()
    {
        $this->addResponsibilities();
        $this->addRequirements();
    }

    public function submit(PositionInterface $position_interface)
    {
        $validated = $this->validate(
            [
                'position' => 'required',
                'job_level' => 'required',
                'department' => 'required',
                'division' => 'required',
                'job_overview' => 'required',

                'responsibilities.*.duties' => 'required',
                'requirements.*.code' => 'required',
            ],
            [
                'responsibilities.*.duties.required' => 'The responsibility and duty field is required.',
                'requirements.*.code.required' => 'The job requirement field is required.',
            ]
        );

        $response = $position_interface->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->addResponsibilities();
            $this->addRequirements();

            $this->emitTo('hrim.workforce.position-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.workforce.position-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.workforce.position-management.create');
    }
}
