<?php

namespace App\Http\Livewire\Hrim\Workforce\PositionManagement;

use App\Interfaces\Hrim\Workforce\PositionInterface;
use App\Models\Hrim\JobLevel;
use App\Models\Hrim\Position;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $confirmation_modal = false;
    public $action_type;
    public $confirmation_message;

    public $create_position_modal = false;
    public $view_position_modal = false;
    public $edit_position_modal = false;
    public $delete_position_modal = false;

    public $position_id;
    public $job_level;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;

    public $position;
    public $division;
    public $department;
    public $jobLevel;

    public $paginateAnnouncement = 5;

    public $job_levels = [];
    public $departments = [];
    public $divisions = [];
    public $positions_count = [];
    public $header_cards = [];

    public $count;

    protected $listeners = ['index' => 'render', 'load_head_cards' => 'loadHeadCards', 'close_modal' => 'closeModal'];

    public function mount()
    {
        $this->loadHeadCards();
    }

    public function loadHeadCards()
    {
        $jobLevels = JobLevel::withCount('positions')->get();

        $this->header_cards = [
            [
                'title' => 'All',
                'value' => $jobLevels->sum('positions_count'),
                'color' => 'text-blue',
                'action' => 'jobLevel',
                'id' => null
            ],

        ];

        foreach ($jobLevels as $count) {
            $this->header_cards[] = [
                'title' => $count->display,
                'value' => $count->positions_count,
                'color' => 'text-blue',
                'action' => 'jobLevel',
                'id' => $count->id
            ];
        }
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToPosition') {
            return redirect()->to(route('hrim.workforce.position.index'));
        } else if ($action_type == 'redirectToJobLevel') {
            return redirect()->to(route('hrim.workforce.job-level.index'));
        } else if ($action_type == 'redirectToDepartment') {
            return redirect()->to(route('hrim.workforce.department.index'));
        } else if ($action_type == 'redirectToDivision') {
            return redirect()->to(route('hrim.workforce.division-management.index'));
        } else if ($action_type == 'create_position') {
            $this->create_position_modal = true;
        } else if ($action_type == 'view_position') {
            $this->emit('position_view_mount', $data['id']);
            $this->view_position_modal = true;
            $this->position_id = $data['id'];
        } else if ($action_type == 'edit_position') {
            $this->emitTo('hrim.workforce.position-manangement.edit', 'mount', $data['id']);
            $this->edit_position_modal = true;
            $this->position_id = $data['id'];
        } else if ($action_type == 'delete_position') {
            $this->confirmation_message = "Are you sure you want to delete this position?";
            $this->delete_position_modal = true;
            $this->position_id = $data['id'];
        }
    }

    public function deletePosition(PositionInterface $positionInterface)
    {

        if ($this->action_type == 'delete_position') {
            $response = $positionInterface->destroy($this->position_id);
            $this->emitTo('hrim.workforce.position-management.index', 'close_modal', 'delete');
            $this->emitTo('hrim.workforce.position-management.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.workforce.position-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_position_modal = false;
        } else if ($action_type == 'view') {
            $this->view_position_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_position_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.hrim.workforce.position-management.index', [
            'positions' => Position::with('division', 'department', 'jobLevel')
                // , 'requirements', 'responsibilities')
                ->where('display', 'like', '%' . $this->position . '%')
                ->when($this->jobLevel, function ($query) {
                    $query->where('job_level_id', $this->jobLevel);
                })
                ->when($this->division, function ($query) {
                    $query->where('division_id', $this->division);
                })
                ->when($this->department, function ($query) {
                    $query->where('department_level_id', $this->department);
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)

        ]);
    }
}
