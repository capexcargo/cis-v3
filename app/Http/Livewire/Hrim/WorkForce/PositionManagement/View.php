<?php

namespace App\Http\Livewire\Hrim\Workforce\PositionManagement;

use App\Models\Hrim\Position;
use App\Models\Hrim\PositionJobRequirements;
use App\Models\Hrim\PositionResponsibilities;
use Livewire\Component;

class View extends Component
{
    public $requests;
    public $requires;
    public $responsi;
    public $requirements;
    public $responsibilities;

    protected $listeners = ['position_view_mount' => 'mount'];

    public function mount($id)
    {
        $this->reset([
            'requests',
            'requires',
            'responsi',
        ]);

        $this->requests = Position::with('division', 'department', 'jobLevel')
        ->where('id', $id)
        ->get();
        
        $this->requires = PositionJobRequirements::with('position')->where('position_id', $id)->get();

        $this->responsi = PositionResponsibilities::with('position')->where('position_id', $id)->get();
    } 

    public function render()
    {
        return view('livewire.hrim.workforce.position-management.view');
    }
}
