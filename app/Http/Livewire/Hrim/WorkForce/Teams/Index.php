<?php

namespace App\Http\Livewire\Hrim\Workforce\Teams;

use App\Interfaces\Hrim\Workforce\TeamsInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait, WithFileUploads;

    public $confirmation_modal = false;
    public $confirmation_message;
    public $action_type;

    public $create_modal = false;
    public $edit_modal = false;
    public $upload_modal = false;
    public $division_id;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'edit') {
            $this->emitTo('hrim.workforce.teams.edit', 'mount', $data['id']);
            $this->edit_modal = true;
            $this->division_id = $data['id'];
        } elseif ($action_type == 'deactivate') {
            $this->division_id = $data['id'];
            $this->confirmation_message = "Are you sure you want to Deactivate this Team?";
            $this->confirmation_modal = true;
        } elseif ($action_type == 'reactivate') {
            $this->division_id = $data['id'];
            $this->confirmation_message = "Are you sure you want to Reactivate this Team?";
            $this->confirmation_modal = true;
        } elseif ($action_type == 'upload') {
            $this->emitTo('hrim.workforce.teams.uploadfile', 'mount', $data['id']);
            $this->upload_modal = true;
            $this->division_id = $data['id'];
        }
    }

    public function confirm(TeamsInterface $teams_interface)
    {
        if ($this->action_type == 'deactivate' || $this->action_type == 'reactivate') {
            $response = $teams_interface->updateStatus($this->division_id, ($this->action_type == 'deactivate' ? 0 : 1));
            if ($response['code'] == 200) {
                $this->sweetAlert('', $response['message']);
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }

            $this->emitTo('hrim.workforce.teams.index', 'close_modal', 'confirmation');
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'confirmation') {
            $this->confirmation_modal = false;
        } elseif ($action_type == 'upload') {
            $this->upload_modal = false;
        }
    }

    public function render(TeamsInterface $teams_interface)
    {
        $request = [];

        $response = $teams_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }
        // dd($response);
        return view('livewire.hrim.workforce.teams.index', [
            'divisions' => $response['result']
        ]);
    }
}
