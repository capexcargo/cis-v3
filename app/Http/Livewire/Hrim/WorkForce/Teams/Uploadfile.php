<?php

namespace App\Http\Livewire\Hrim\Workforce\Teams;

use App\Interfaces\Hrim\Workforce\TeamsInterface;
use App\Traits\Hrim\Workforce\TeamsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Uploadfile extends Component
{
    use TeamsTrait, PopUpMessagesTrait, WithFileUploads;
    public $attachment = '';

    protected $listeners = ['mount' => 'mount'];

    public $teams_model;
    public $division;
    public $attachment_id;
    public $attachment_path;
    public $attachment_name;
    public $attachment_extension;

    public function getRequestfile()
    {
        return [
            'attachment' => $this->attachment,
        ];
    }

    public function mount(TeamsInterface $teams_interface, $id)
    {
        $this->resetForm();
        $response = $teams_interface->showTeams($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->teams_model = $response['result'];
        $this->division = $this->teams_model->name;
        $this->attachment_name = $this->teams_model->attachments;

        foreach ($this->teams_model->attachments as $attachment) {
            $this->attachment_id = $attachment->id;
            $this->attachment_path = $attachment->path;
            $this->attachment_name = $attachment->name;
            $this->attachment_extension = $attachment->extension;
        }
    }

    public function submit(TeamsInterface $teams_interface)
    {
        $response = $teams_interface->addFile($this->getRequestfile(), $this->teams_model->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.workforce.teams.index', 'close_modal', 'upload');
            $this->emitTo('hrim.workforce.teams.index', 'indexs');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.workforce.teams.uploadfile');
    }
}
