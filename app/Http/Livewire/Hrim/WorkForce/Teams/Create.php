<?php

namespace App\Http\Livewire\Hrim\Workforce\Teams;

use App\Interfaces\Hrim\Workforce\TeamsInterface;
use App\Traits\Hrim\Workforce\TeamsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use TeamsTrait, PopUpMessagesTrait, WithFileUploads;

    public function submit(TeamsInterface $teams_interface)
    {
        $response = $teams_interface->updateOrCreate($this->division, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.workforce.teams.index', 'close_modal', 'create');
            $this->emitTo('hrim.workforce.teams.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.workforce.teams.create');
    }
}
