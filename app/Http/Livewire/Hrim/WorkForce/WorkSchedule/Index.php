<?php

namespace App\Http\Livewire\Hrim\Workforce\WorkSchedule;

use App\Interfaces\Hrim\Workforce\WorkScheduleInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $create_modal = false;
    public $edit_modal = false;
    public $work_schedule_id;

    public $work_schedule;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emitTo('hrim.workforce.work-schedule.edit', 'mount', $data['id']);
            $this->edit_modal = true;
        }

        $this->work_schedule_id = $data['id'] ?? null;
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function render(WorkScheduleInterface $work_schedule_interface)
    {
        $request = [
            'work_schedule' => $this->work_schedule,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $work_schedule_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.workforce.work-schedule.index', [
            'work_schedules' => $response['result']
        ]);
    }
}
