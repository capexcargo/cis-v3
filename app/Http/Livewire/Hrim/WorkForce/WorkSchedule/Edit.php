<?php

namespace App\Http\Livewire\Hrim\Workforce\WorkSchedule;

use App\Interfaces\Hrim\Workforce\WorkScheduleInterface;
use App\Traits\Hrim\Workforce\WorkScheduleTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use WorkScheduleTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(WorkScheduleInterface $work_schedule_interface, $id)
    {
        $this->resetForm();
        $response = $work_schedule_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);
        
        $this->work_schedule_model = $response['result'];
        $this->work_schedule = $this->work_schedule_model->name;
        $this->work_mode = $this->work_schedule_model->work_mode_id;
        $this->shift = $this->work_schedule_model->shift;
        $this->inclusive_days[] = $this->work_schedule_model->monday ? 'monday' : '';
        $this->inclusive_days[] = $this->work_schedule_model->tuesday ? 'tuesday' : '';
        $this->inclusive_days[] = $this->work_schedule_model->wednesday ? 'wednesday' : '';
        $this->inclusive_days[] = $this->work_schedule_model->thursday ? 'thursday' : '';
        $this->inclusive_days[] = $this->work_schedule_model->friday ? 'friday' : '';
        $this->inclusive_days[] = $this->work_schedule_model->saturday ? 'saturday' : '';
        $this->inclusive_days[] = $this->work_schedule_model->sunday ? 'sunday' : '';
        $this->time_from = date('H:i', strtotime($this->work_schedule_model->time_from));
        $this->time_to = date('H:i', strtotime($this->work_schedule_model->time_to));
        $this->break_from = date('H:i', strtotime($this->work_schedule_model->break_from));
        $this->break_to = date('H:i', strtotime($this->work_schedule_model->break_to));
        $this->ot_break_from = date('H:i', strtotime($this->work_schedule_model->ot_break_from));
        $this->ot_break_to = date('H:i', strtotime($this->work_schedule_model->ot_break_to));
    }

    public function submit(WorkScheduleInterface $work_schedule_interface)
    {
        $response = $work_schedule_interface->update($this->work_schedule_model->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
           $this->emitTo('hrim.workforce.work-schedule.index', 'close_modal', 'edit');
           $this->emitTo('hrim.workforce.work-schedule.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.workforce.work-schedule.edit');
    }
}
