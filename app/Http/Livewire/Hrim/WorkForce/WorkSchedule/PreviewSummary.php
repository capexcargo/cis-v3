<?php

namespace App\Http\Livewire\Hrim\Workforce\WorkSchedule;

use App\Traits\Hrim\Workforce\WorkScheduleTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class PreviewSummary extends Component
{
    use WorkScheduleTrait, PopUpMessagesTrait;

    public $action_type;
    public $data;

    public function mount(array $data, $action_type)
    {
        $this->data = $data;
        $this->action_type = $action_type;
    }

    public function closePreview()
    {
        $this->perview_summary_modal = true;
    }

    public function submit()
    {
        if ($this->action_type == 'create') {

            $this->emitTo('hrim.workforce.work-schedule.create', 'submit');
        }
        //  elseif ($this->action_type == 'edit') {
        //     $this->emitTo('hrim.workforce.work-schedule.edit', 'submit');
        // }
    }

    public function render()
    {
        return view('livewire.hrim.workforce.work-schedule.preview-summary');
    }
}
