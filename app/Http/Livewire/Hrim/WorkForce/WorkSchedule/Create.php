<?php

namespace App\Http\Livewire\Hrim\Workforce\WorkSchedule;

use App\Interfaces\Hrim\Workforce\WorkScheduleInterface;
use App\Traits\Hrim\Workforce\WorkScheduleTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use WorkScheduleTrait, PopUpMessagesTrait;

    protected $listeners = ['submit'];

    public function preview(WorkScheduleInterface $work_schedule_interface)
    {
        $rules = [
            'work_schedule' => 'required',
            'work_mode' => 'required',
            'shift' => 'required',
            'inclusive_days' => 'required',
            'time_from' => 'required',
            'time_to' => 'required',
            'break_from' => 'required',
            'break_to' => 'required',
            'ot_break_from' => 'required',
            'ot_break_to' => 'required',
        ];

        $validated = $this->validate(
            $rules,
            [
                'sr_description.required' => 'The Service request description field is required.',
                'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
            ]
        );

        $this->perview_summary_modal = true;
    }

    public function submit(WorkScheduleInterface $work_schedule_interface)
    {
        $response = $work_schedule_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->perview_summary_modal = false;
            $this->resetForm();
            $this->emitTo('hrim.workforce.work-schedule.index', 'close_modal', 'create');
            $this->emitTo('hrim.workforce.work-schedule.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->perview_summary_modal = false;
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.workforce.work-schedule.create');
    }
}
