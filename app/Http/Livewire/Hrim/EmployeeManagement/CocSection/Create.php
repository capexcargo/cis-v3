<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\CocSection;

use App\Interfaces\Hrim\EmployeeManagement\CocSectionInterface;
use App\Repositories\Hrim\EmployeeManagement\CocSectionRepository;
use App\Traits\Hrim\EmployeeManagement\CocSectionTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use CocSectionTrait, PopUpMessagesTrait;

    public function submit(CocSectionInterface $coc_section_interface)
    {
        $response = $coc_section_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
            $this->confirmation_message = "Are you sure you want to submit this new COC Section?";
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.coc-section.create');
    }
}
