<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\CocSection;

use App\Interfaces\Hrim\EmployeeManagement\CocSectionInterface;
use App\Models\Hrim\CocSection;
use App\Traits\Hrim\EmployeeManagement\CocSectionTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use CocSectionTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(CocSectionInterface $coc_section_interface, $id)
    {
        $this->resetForm();
        $response = $coc_section_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->coc_section = $response['result'];
        $this->display = $this->coc_section->display;
    }

    public function submit(CocSectionInterface $coc_section_interface)
    {
        $response = $coc_section_interface->update($this->coc_section->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-management.coc-section.index', 'close_modal', 'edit');
            $this->emitTo('hrim.employee-management.coc-section.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            "display",
        ]);
    }
    
    public function render()
    {
        return view('livewire.hrim.employee-management.coc-section.edit');
    }
}
