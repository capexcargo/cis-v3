<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\ResignationReasonReferenceManagement;

use App\Interfaces\Hrim\EmployeeManagement\ResignationReasonReferenceInterface;
use App\Models\Hrim\ResignationReasonReference;
use App\Traits\Hrim\EmployeeManagement\ResignationReasonReferenceTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use ResignationReasonReferenceTrait, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->reason_reference = ResignationReasonReference::findOrFail($id);

        $this->reason = $this->reason_reference->display;
        // $this->category = $this->employment_category_type->employment_category_id;
    }

    public function submit(ResignationReasonReferenceInterface $reason_reference)
    {
        $response = $reason_reference->update($this->getRequest(), $this->reason_reference->id);
        
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-management.resignation-reason-reference-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.employee-management.resignation-reason-reference-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render()
    {
        return view('livewire.hrim.employee-management.resignation-reason-reference-management.edit');
    }
}
