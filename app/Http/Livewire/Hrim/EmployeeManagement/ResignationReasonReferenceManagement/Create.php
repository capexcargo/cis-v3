<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\ResignationReasonReferenceManagement;

use App\Interfaces\Hrim\EmployeeManagement\ResignationReasonReferenceInterface;
use App\Traits\Hrim\EmployeeManagement\ResignationReasonReferenceTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use ResignationReasonReferenceTrait, PopUpMessagesTrait;

    protected $rules = [
        'reason' => 'required',
    ];

    public function confirmationSubmit()
    {
        $this->validate();

        $this->confirmation_modal = true;
    }

    
    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('hrim.employee-management.resignation-reason-reference-management.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    public function submit(ResignationReasonReferenceInterface $resignation_reason_reference_interface)
    {
        // dd($this->getRequest());
        
        $response = $resignation_reason_reference_interface->create($this->getRequest());
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-management.resignation-reason-reference-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.employee-management.resignation-reason-reference-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.resignation-reason-reference-management.create');
    }
}
