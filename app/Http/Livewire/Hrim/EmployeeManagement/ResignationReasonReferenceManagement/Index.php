<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\ResignationReasonReferenceManagement;

use App\Interfaces\Hrim\EmployeeManagement\ResignationReasonReferenceInterface;
use App\Traits\PopUpMessagesTrait;
use App\Traits\Hrim\EmployeeManagement\ResignationReasonReferenceTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use ResignationReasonReferenceTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_res_res') {
            $this->create_modal = true;
        }elseif ($action_type == 'edit') {
            $this->emitTo('hrim.employee-management.resignation-reason-reference-management.edit', 'edit', $data['id']);
            $this->res_rr_id = $data['id'];
            $this->edit_modal = true;
            
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this Resignation Reason Reference?";
            $this->delete_modal = true;
            $this->res_rr_id = $data['id'];
        }
    }
        
    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }

    public function confirm(ResignationReasonReferenceInterface $resignation_reason_interface)
    {
        if ($this->action_type == "delete") {
            $response = $resignation_reason_interface->destroy($this->res_rr_id);
            $this->emitTo('hrim.employee-management.resignation-reason-reference-management.index', 'close_modal', 'delete');
            $this->emitTo('hrim.employee-management.resignation-reason-reference-management.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.employee-management.resignation-reason-reference-management.index', 'close_modal', 'delete');
            $this->emitTo('hrim.employee-management.resignation-reason-reference-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render(ResignationReasonReferenceInterface $resignation_reason_reference_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $resignation_reason_reference_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'reason_reference1' => [],
                ];
        }
    
        return view('livewire.hrim.employee-management.resignation-reason-reference-management.index',[
            'reason_reference1' => $response['result']['reason_reference1']
        ]);
    }
}
