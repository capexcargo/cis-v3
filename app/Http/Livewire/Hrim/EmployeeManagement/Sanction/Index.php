<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\Sanction;

use App\Interfaces\Hrim\EmployeeManagement\SanctionInterface;
use App\Models\Hrim\Sanction;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $confirmation_modal = false;
    public $confirmation_message;

    public $action_type;
    public $create_sanction_modal = false;
    public $edit_sanction_modal = false;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;

    public $sanction_id;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToCocSection') {
            return redirect()->to(route('hrim.employee-management.coc-section.index'));
        } else if ($action_type == 'redirectToViolation') {
            return redirect()->to(route('hrim.employee-management.violation.index'));
        } else if ($action_type == 'redirectToDiscRecord') {
            return redirect()->to(route('hrim.employee-management.disciplinary-record.index'));
        } else if ($action_type == 'redirectToSanction') {
            return redirect()->to(route('hrim.employee-management.sanction.index'));
        } else if ($action_type == 'redirectToSanctionStat') {
            return redirect()->to(route('hrim.employee-management.sanction-status.index'));
        } else if ($action_type == 'create_sanction') {
            $this->create_sanction_modal = true;
        } else if ($action_type == 'edit_sanction') {
            $this->emitTo('hrim.employee-management.sanction.edit', $data['id']);
            $this->edit_sanction_modal = true;
            $this->sanction_id = $data['id'];
        } else if ($action_type == 'delete_sanction') {
            $this->confirmation_message = "Are you sure you want to delete this sanction?";
            $this->confirmation_modal = true;
            $this->sanction_id = $data['id'];
        }
    }

    public function confirm(SanctionInterface $sanction_interface)
    {
        if ($this->action_type == "delete_sanction") {
            $response = $sanction_interface->destroy($this->sanction_id);
            $this->emitTo('hrim.employee-management.sanction.index', 'close_modal', 'delete');
            $this->emitTo('hrim.employee-management.sanction.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emit('index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_sanction_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_sanction_modal = false;
        } else if ($action_type == 'delete') {
            $this->confirmation_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.sanction.index', [
            'sanctions' => Sanction::where('display', 'like', '%' . $this->display . '%')
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'desc' : 'asc');
                })->paginate($this->paginate)

        ]);
    }
}
