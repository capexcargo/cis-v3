<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\Sanction;

use App\Interfaces\Hrim\EmployeeManagement\SanctionInterface;
use App\Models\Hrim\Sanction;
use App\Traits\Hrim\EmployeeManagement\SanctionTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use SanctionTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(SanctionInterface $sanction_interface, $id)
    {
        $this->resetForm();
        $response = $sanction_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->sanction = $response['result'];
        $this->display = $this->sanction->display;
    }

    public function submit(SanctionInterface $sanction_interface)
    {
        $response = $sanction_interface->update($this->sanction->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-management.sanction.index', 'close_modal', 'edit');
            $this->emitTo('hrim.employee-management.sanction.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function resetForm()
    {
        $this->reset([
            "display",
        ]);
    }
    
    public function render()
    {
        return view('livewire.hrim.employee-management.sanction.edit');
    }
}
