<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\EmployeeInformation;

use App\Interfaces\Hrim\EmployeeManagement\EmployeeInformationInterface;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $create_modal = false;
    public $view_modal = false;
    public $resignation_modal = false;
    public $user_id = 1;

    public $status = false;
    public $employee_name;
    public $employment_categories = [1, 2];
    public $sortField = 'last_name';
    public $sortAsc = true;
    public $paginate = 10;
    public $sort_by_status = false;
    public $sort_clicked = false;
    public $sort_clicked_alt = 'asc';

    public $header_cards = [];
    public $requests_if_clicked = [];
    public $requests = [];

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        if ($action_type == 'view') {
            $this->user_id = $data['id'];
            $this->emitTo('hrim.employee-management.employee-information.view', 'mount', $data['id']);
            $this->view_modal = true;
        } elseif ($action_type == 'resignation') {
            $this->user_id = $data['id'];
            $this->emitTo('hrim.employee-management.employee-information.resignation-reason', 'mount', $data['id']);
            $this->resignation_modal = true;
            //
        } elseif ($action_type == 'sortByName') {
            $this->sort_clicked = true;
            $this->sort_clicked_alt = $this->sort_clicked_alt === 'asc' ? 'desc' : 'asc';

            $this->requests_if_clicked = [
                'status' => $this->status,
                'employee_name' => $this->employee_name,
                'employment_categories' => $this->employment_categories,
                'sort_field' => $this->sortField,
                'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
                'paginate' => $this->paginate,
                'sort_by_name' => $this->sort_clicked_alt,
            ];
        } elseif ($action_type == 'sortByPosition') {
            $this->sort_clicked = true;
            $this->sort_clicked_alt = $this->sort_clicked_alt === 'desc' ? 'asc' : 'desc';

            $this->requests_if_clicked = [
                'status' => $this->status,
                'employee_name' => $this->employee_name,
                'employment_categories' => $this->employment_categories,
                'sort_field' => $this->sortField,
                'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
                'paginate' => $this->paginate,
                'sort_by_position' => $this->sort_clicked_alt,
            ];
        } elseif ($action_type == 'sortByStatus') {
            $this->sort_clicked = true;
            $this->sort_clicked_alt = $this->sort_clicked_alt === 'desc' ? 'asc' : 'desc';

            $this->requests_if_clicked = [
                'status' => $this->status,
                'employee_name' => $this->employee_name,
                'employment_categories' => $this->employment_categories,
                'sort_field' => $this->sortField,
                'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
                'paginate' => $this->paginate,
                'sort_by_status' => $this->sort_clicked_alt,
            ];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'view') {
            $this->view_modal = false;
        } elseif ($action_type == 'resignation') {
            $this->resignation_modal = false;
        }
    }

    protected function updatedStatus()
    {
        $this->sort_clicked = false;
    }

    public function render(EmployeeInformationInterface $employee_information_interface)
    {
        $this->requests = [
            'status' => $this->status,
            'employee_name' => $this->employee_name,
            'employment_categories' => $this->employment_categories,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
            'sort_by_status' => null,
        ];

        $response = $employee_information_interface->index($this->sort_clicked ? $this->requests_if_clicked : $this->requests);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        $employee_information_statuses = $response['result']['employee_information_statuses'];

        $this->header_cards = [];

        $this->header_cards[] = [
            'title' => 'Total Employees',
            'value' => $employee_information_statuses->sum('users_count'),
            'action' => 'status',
            'id' => false
        ];

        $this->header_cards[] = [
            'title' => 'Active Employees',
            'value' => $employee_information_statuses[0]->users_count,
            'action' => 'status',
            'id' => $employee_information_statuses[0]->id
        ];

        $this->header_cards[] = [
            'title' => 'Resigned Employees',
            'value' => $employee_information_statuses[1]->users_count,
            'action' => 'status',
            'id' => $employee_information_statuses[1]->id
        ];

        return view('livewire.hrim.employee-management.employee-information.index', [
            'employee_informations' => $response['result']['employee_informations']
        ]);
    }
}
