<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\EmployeeInformation;

use App\Repositories\Hrim\TimeInAndOutRepository;
use DateTime;
use Livewire\Component;

class Schedule extends Component
{
    public $month;
    public $year;
    public $date;

    public $user_id;
    public $events = [];


    protected $listeners = ['get_events' => 'getEvents'];

    public function mount($user_id)
    {
        $this->month = now()->month;
        $this->year = now()->year;
        $this->date = now();

        $this->user_id = $user_id;
    }

    public function getEvents($date)
    {
        $this->date = $date;
        $request = [
            'user_id' => $this->user_id,
            'date' => $this->date,
            'sort_field' => 'date',
            'sort_type' => 'asc'
        ];
        $time_in_and_out_repository = new TimeInAndOutRepository();
        // $response = $time_in_and_out_repository->getUserTimeLogs($request);
        $response = $time_in_and_out_repository->getWorkSchedule($request);
        $response2 = $time_in_and_out_repository->getWorkSchedule2($request);




        if (count($response['result']) > 0) {
            // dd(count($response2['result']));
            // foreach ($response['result'] as $time_log) {
            // $this->events[] = [
            //     'work_schedule' => $time_log->workSchedule->name,
            //     'work_schedule_time_log' => date('h:i A', strtotime($time_log->workSchedule->time_from)) . '- ' . date('h:i A', strtotime($time_log->workSchedule->time_to)),
            //     'time_log' => date('h:i A', strtotime($time_log->time_in)) . '- ' . date('h:i A', strtotime($time_log->time_out)),
            //     'event_date_time' => $time_log->date,
            // ];

            $this->events = [];
            foreach ($response['result'] as $time_log) {
                if (date('m', strtotime($this->date)) == date('m', strtotime($time_log->inclusive_date_from))) {
                    $begin = new DateTime(date('Y-m-d', strtotime($time_log->inclusive_date_from)));
                } else {
                    $begin = new DateTime(date('Y', strtotime($this->date)) . "-" . date('m', strtotime($this->date)) . "-01");
                }
                if (date('m', strtotime($this->date)) == date('m', strtotime($time_log->inclusive_date_to))) {
                    $end = new DateTime(date('Y-m-d', strtotime($time_log->inclusive_date_to)));
                } else {
                    $end = new DateTime(date('Y', strtotime($this->date)) . "-" . date('m', strtotime($this->date)) . "-31");
                }

                for ($i = $begin; $i <= $end; $i->modify('+1 day')) {


                    $daysched = "";
                    $dayschedtime = "";
                    if ($time_log->currentWorkSchedule->monday == 1 && $i->format("l") == "Monday") {
                        $daysched = $time_log->currentWorkSchedule->name;
                        $dayschedtime = date('h:i A', strtotime($time_log->inclusive_date_from)) . '- ' . date('h:i A', strtotime($time_log->inclusive_date_to));
                    }
                    if ($time_log->currentWorkSchedule->tuesday == 1 && $i->format("l") == "Tuesday") {
                        $daysched = $time_log->currentWorkSchedule->name;
                        $dayschedtime = date('h:i A', strtotime($time_log->inclusive_date_from)) . '- ' . date('h:i A', strtotime($time_log->inclusive_date_to));
                    }
                    if ($time_log->currentWorkSchedule->wednesday == 1 && $i->format("l") == "Wednesday") {
                        $daysched = $time_log->currentWorkSchedule->name;
                        $dayschedtime = date('h:i A', strtotime($time_log->inclusive_date_from)) . '- ' . date('h:i A', strtotime($time_log->inclusive_date_to));
                    }
                    if ($time_log->currentWorkSchedule->thursday == 1 && $i->format("l") == "Thursday") {
                        $daysched = $time_log->currentWorkSchedule->name;
                        $dayschedtime = date('h:i A', strtotime($time_log->inclusive_date_from)) . '- ' . date('h:i A', strtotime($time_log->inclusive_date_to));
                    }
                    if ($time_log->currentWorkSchedule->friday == 1 && $i->format("l") == "Friday") {
                        $daysched = $time_log->currentWorkSchedule->name;
                        $dayschedtime = date('h:i A', strtotime($time_log->inclusive_date_from)) . '- ' . date('h:i A', strtotime($time_log->inclusive_date_to));
                    }
                    if ($time_log->currentWorkSchedule->saturday == 1 && $i->format("l") == "Saturday") {
                        $daysched = $time_log->currentWorkSchedule->name;
                        $dayschedtime = date('h:i A', strtotime($time_log->inclusive_date_from)) . '- ' . date('h:i A', strtotime($time_log->inclusive_date_to));
                    }
                    if ($time_log->currentWorkSchedule->sunday == 1 && $i->format("l") == "Sunday") {
                        $daysched = $time_log->currentWorkSchedule->name;
                        $dayschedtime = date('h:i A', strtotime($time_log->inclusive_date_from)) . '- ' . date('h:i A', strtotime($time_log->inclusive_date_to));
                    }

                    $this->events[] = [
                        'work_schedule' => $daysched,
                        'work_schedule_time_log' => $dayschedtime,
                        'time_log' => '',
                        'event_date_time' => $i->format("Y-m-d"),
                    ];
                }
            }
        } else if (count($response2['result']) > 0) {



            $this->events = [];
            $begin = new DateTime(date('Y', strtotime($this->date)) . "-" . date('m', strtotime($this->date)) . "-01");
            $end = new DateTime(date('Y', strtotime($this->date)) . "-" . date('m', strtotime($this->date)) . "-31");


            for ($i = $begin; $i <= $end; $i->modify('+1 day')) {


                foreach ($response2['result'] as $time_log) {

                    // dd($time_log->workSchedule->monday);
                    $daysched = "";
                    $dayschedtime = "";
                    if ($time_log->workSchedule->monday == 1 && $i->format("l") == "Monday") {
                        $daysched = $time_log->workSchedule->name;
                        $dayschedtime = date('h:i A', strtotime($time_log->workSchedule->time_from)) . '- ' . date('h:i A', strtotime($time_log->workSchedule->time_to));
                    }
                    if ($time_log->workSchedule->tuesday == 1 && $i->format("l") == "Tuesday") {
                        $daysched = $time_log->workSchedule->name;
                        $dayschedtime = date('h:i A', strtotime($time_log->workSchedule->time_from)) . '- ' . date('h:i A', strtotime($time_log->workSchedule->time_to));
                    }
                    if ($time_log->workSchedule->wednesday == 1 && $i->format("l") == "Wednesday") {
                        $daysched = $time_log->workSchedule->name;
                        $dayschedtime = date('h:i A', strtotime($time_log->workSchedule->time_from)) . '- ' . date('h:i A', strtotime($time_log->workSchedule->time_to));
                    }
                    if ($time_log->workSchedule->thursday == 1 && $i->format("l") == "Thursday") {
                        $daysched = $time_log->workSchedule->name;
                        $dayschedtime = date('h:i A', strtotime($time_log->workSchedule->time_from)) . '- ' . date('h:i A', strtotime($time_log->workSchedule->time_to));
                    }
                    if ($time_log->workSchedule->friday == 1 && $i->format("l") == "Friday") {
                        $daysched = $time_log->workSchedule->name;
                        $dayschedtime = date('h:i A', strtotime($time_log->workSchedule->time_from)) . '- ' . date('h:i A', strtotime($time_log->workSchedule->time_to));
                    }
                    if ($time_log->workSchedule->saturday == 1 && $i->format("l") == "Saturday") {
                        $daysched = $time_log->workSchedule->name;
                        $dayschedtime = date('h:i A', strtotime($time_log->workSchedule->time_from)) . '- ' . date('h:i A', strtotime($time_log->workSchedule->time_to));
                    }
                    if ($time_log->workSchedule->sunday == 1 && $i->format("l") == "Sunday") {
                        $daysched = $time_log->workSchedule->name;
                        $dayschedtime = date('h:i A', strtotime($time_log->workSchedule->time_from)) . '- ' . date('h:i A', strtotime($time_log->workSchedule->time_to));
                    }

                    $this->events[] = [
                        'work_schedule' => $daysched,
                        'work_schedule_time_log' => $dayschedtime,
                        'time_log' => '',
                        'event_date_time' => $i->format("Y-m-d"),
                    ];
                }
            }
        } else {
            $this->events = [];
        }
        $this->emit('updateEvents', $this->events);
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.employee-information.schedule');
    }
}
