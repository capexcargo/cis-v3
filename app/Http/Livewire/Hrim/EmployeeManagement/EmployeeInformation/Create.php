<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\EmployeeInformation;

use App\Interfaces\Hrim\EmployeeManagement\EmployeeInformationInterface;
use App\Models\UserDetails;
use App\Traits\Hrim\EmployeeManagement\EmployeeInformantionTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use EmployeeInformantionTrait;
    use PopUpMessagesTrait;
    use WithFileUploads;

    protected $listeners = ['submit','generate_announcement_no' => 'generateAnnouncementNo'];

    public function mount()
    {
        $this->generateDefaultEmail();

        $highest_employee_no = DB::table('user_details')
        ->select(DB::raw('MAX(CAST(employee_number AS UNSIGNED)) AS max_employee_number'))
        ->first()
        ->max_employee_number;
        
        $this->employee_id = (int)$highest_employee_no + 1;
    }

    public function submit(EmployeeInformationInterface $employee_information_interface)
    {
        $response = $employee_information_interface->create($this->getRequest());
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-management.employee-information.index', 'close_modal', 'create');
            $this->emitTo('hrim.employee-management.employee-information.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) {
                        $this->addError($a, $message);
                    }
                } else {
                    $this->addError($a, $messages);
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.employee-information.create');
    }
}
