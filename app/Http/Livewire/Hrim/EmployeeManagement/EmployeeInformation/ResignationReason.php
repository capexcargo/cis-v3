<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\EmployeeInformation;

use App\Interfaces\Hrim\EmployeeManagement\ResignationReasonInterface;
use App\Traits\PopUpMessagesTrait;
use App\Models\Hrim\ResignationReasonReference;
use Livewire\Component;
use Livewire\WithFileUploads;

class ResignationReason extends Component
{
    use PopUpMessagesTrait, WithFileUploads, PopUpMessagesTrait;

    public $date_resigned;
    public $reason_for_severance;
    public $reason_for_resignation;

    public $user;

    public $resignation_reason_references = [];
    public $attachments = [];

    protected $listeners = ['mount' => 'mount'];

    public function mount(ResignationReasonInterface $resignation_reason_interface, $id)
    {
        $this->resetForm();
        $response = $resignation_reason_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->user = $response['result'];
        if ($this->user->resignationReason) {
            $this->date_resigned = $this->user->resignationReason->date_resignation;
            $this->reason_for_severance = $this->user->resignationReason->reason_description;
            $this->reason_for_resignation = $this->user->resignationReason->reason_reference_id;
        }

        foreach ($this->user->attachments as $attachment) {
            $this->attachments[] = [
                'id' => $attachment->id,
                'attachment' => '',
                'path' => $attachment->path,
                'name' => $attachment->name,
                'extension' => $attachment->extension,
                'is_deleted' => false,
            ];
        }
    }

    public function load()
    {
        $this->loadResignationReasonReference();
        $this->addAttachments();
    }

    public function loadResignationReasonReference()
    {
        $this->resignation_reason_references = ResignationReasonReference::get();
    }

    public function addAttachments()
    {
        $this->attachments[] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }

    public function removeAttachments($i)
    {
        if (count(collect($this->attachments)->where('is_deleted', false)) > 1) {
            if ($this->attachments[$i]['id']) {
                $this->attachments[$i]['is_deleted'] = true;
            } else {
                unset($this->attachments[$i]);
            }

            array_values($this->attachments);
        }
    }

    public function submit(ResignationReasonInterface $resignation_reason_interface)
    {
        $response = $resignation_reason_interface->updateOrCreate($this->user->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-management.employee-information.index', 'close_modal', 'resignation');
            $this->emitTo('hrim.employee-management.employee-information.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function getRequest()
    {
        return [
            'date_resigned' => $this->date_resigned,
            'reason_for_severance' => $this->reason_for_severance,
            'reason_for_resignation' => $this->reason_for_resignation,
            'attachments' => $this->attachments,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'date_resigned',
            'reason_for_severance',
            'reason_for_resignation',
            'attachments',
        ]);
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.employee-information.resignation-reason');
    }
}
