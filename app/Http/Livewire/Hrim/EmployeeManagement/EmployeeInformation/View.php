<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\EmployeeInformation;

use App\Interfaces\Hrim\EmployeeManagement\EmployeeInformationInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class View extends Component
{
    use PopUpMessagesTrait;

    public $hrim_201_files_modal = false;
    public $edit_modal = false;
    public $schedule_modal = false;
    
    public $user_id;
    public $user;

    protected $listeners = ['mount' => 'mount', 'close_modal' => 'closeModal'];

    public function mount(EmployeeInformationInterface $employee_information_interface, $id)
    {
        $this->reset([
            'user_id',
            'user'
        ]);

        $this->user_id = $id;
        $response = $employee_information_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->user = $response['result'];
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == '201_files') {
            $this->user_id = $data['id'];
            $this->emitTo('hrim.employee-management.employee-information.files.index', 'mount', $data['id']);
            $this->hrim_201_files_modal = true;
        } elseif ($action_type == 'edit') {
            $this->user_id = $data['id'];
            $this->emitTo('hrim.employee-management.employee-information.edit', 'mount', $data['id']);
            $this->edit_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == '201_files') {
            $this->edit_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.employee-information.view');
    }
}
