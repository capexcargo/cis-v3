<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\EmployeeInformation;

use App\Models\BranchReference;
use App\Models\Division;
use App\Models\Hrim\Agencies;
use App\Models\Hrim\Department;
use App\Models\Hrim\EALevelReference;
use App\Models\Hrim\ECDRelationshipReference;
use App\Models\Hrim\EducationalAttainment;
use App\Models\Hrim\EmploymentCategory;
use App\Models\Hrim\EmploymentStatus;
use App\Models\Hrim\GenderReference;
use App\Models\Hrim\JobLevel;
use App\Models\Hrim\Position;
use App\Models\Hrim\WorkSchedule;
use Livewire\Component;
use Livewire\WithFileUploads;

class PerviewSummary extends Component
{
    use WithFileUploads;

    public $action_type;
    public $data;

    public $branch;
    public $position;
    public $job_level;
    public $department;
    public $division;
    public $employment_category;
    public $employment_status;
    public $gender;
    public $agency;
    public $work_schedule;
    public $ecd_relationship;
    public $ed_attainment;

    public function mount($data, $action_type)
    {
        $this->data = $data;
        $this->action_type = $action_type;

        $this->branch = BranchReference::find($data['branch']);
        $this->position = Position::find($data['position']);
        $this->job_level = JobLevel::find($data['job_level']);
        $this->department = Department::find($data['department']);
        $this->division = Division::find($data['division']);
        $this->employment_category = EmploymentCategory::find($data['employement_category']);
        $this->employment_status = EmploymentStatus::find($data['employement_status']);
        $this->gender = GenderReference::find($data['gender']);
        $this->agency = Agencies::find($data['agency']);
        $this->work_schedule = WorkSchedule::find($data['work_schedule']);
        // $this->ecd_relationship = ECDRelationshipReference::find($data['emergency_contact_relationship']);
        $this->ed_attainment = EALevelReference::find($data['educational_level']);
    }

    public function submit()
    {
        if ($this->action_type == 'create') {
            $this->emitTo('hrim.employee-management.employee-information.create', 'submit');
        } elseif ($this->action_type == 'edit') {
            $this->emitTo('hrim.employee-management.employee-information.edit', 'submit');
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.employee-information.perview-summary');
    }
}
