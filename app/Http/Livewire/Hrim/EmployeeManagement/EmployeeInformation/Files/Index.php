<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\EmployeeInformation\Files;

use App\Interfaces\Hrim\EmployeeManagement\Hrim201FilesInterface;
use App\Repositories\Hrim\EmployeeManagement\Hrim201FilesRepository;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Index extends Component
{
    use WithFileUploads, PopUpMessagesTrait;

    public $action_type;
    public $confirmation_modal = false;
    public $confirmation_message;

    public $view_modal = false;

    public $user_id;
    public $hrim_201_files_reference_id;
    public $hrim_201_files_references = [];
    public $disciplinaries = [];

    protected $listeners = ['mount' => 'mount'];

    public function mount(Hrim201FilesInterface $hrim_201_files_interface, $id)
    {
        $this->dispatchBrowserEvent('DOMContentLoaded');
        $this->user_id = $id;
        $request = [
            'user_id' => $id,
        ];
        $response = $hrim_201_files_interface->index($request);
        abort_if($response['code'] != 200, $response['code'], $response['message']);
        $this->hrim_201_files_references = $response['result'];
        $response = $hrim_201_files_interface->disciplinaryIndex($request);
        $this->disciplinaries = $response['result'];
    }

    protected function updatedHrim201FilesReferences()
    {
        $hrim_201_files_repository = new Hrim201FilesRepository();
        $response = $hrim_201_files_repository->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetErrorBag();
            $this->emitTo('hrim.employee-management.employee-information.files.index', 'mount', $this->user_id);
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'view') {
            $this->hrim_201_files_reference_id = $data['id'];
            $this->emitTo('hrim.employee-management.employee-information.files.view', 'mount', $data['id'], $this->user_id);
            $this->view_modal = true;
        } else if ($action_type == 'confirmation_delete') {
            $this->hrim_201_files_reference_id = $data['id'];
            $this->confirmation_message = "Are you sure to delete all this attachment?";
            $this->confirmation_modal = true;
        } else if ($action_type == 'confirmation_restore') {
            $this->hrim_201_files_reference_id = $data['id'];
            $this->confirmation_message = "Are you sure to restore all this attachment?";
            $this->confirmation_modal = true;
        }
    }

    public function confirm(Hrim201FilesInterface $hrim_201_files_interface)
    {
        if ($this->action_type == 'confirmation_delete') {
            $response = $hrim_201_files_interface->destroy($this->hrim_201_files_reference_id, $this->user_id);
        } elseif ($this->action_type == 'confirmation_restore') {
            $response = $hrim_201_files_interface->restore($this->hrim_201_files_reference_id, $this->user_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.employee-management.employee-information.files.index', 'mount', $this->user_id);
            // $this->sweetAlert('', $response['message']);
            $this->confirmation_modal = false;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'view') {
            $this->view_modal = false;
        }
    }

    public function getRequest()
    {
        return [
            'user_id' => $this->user_id,
            'hrim_201_files_references' => $this->hrim_201_files_references,
        ];
    }


    public function render()
    {
        return view('livewire.hrim.employee-management.employee-information.files.index');
    }
}
