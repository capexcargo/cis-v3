<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\EmployeeInformation\Files;

use App\Interfaces\Hrim\EmployeeManagement\Hrim201FilesInterface;
use Livewire\Component;

class View extends Component
{
    public $confirmation_modal = false;
    public $confirmation_message;

    public $view_modal = false;
    public $hrim_201_files_attachment_id;

    public $hrim_201_files;
    public $hrim_201_reference_id;
    public $user_id;

    protected $listeners = ['mount' => 'mount', 'close_modal' => 'closeModal'];

    public function mount(Hrim201FilesInterface $hrim_201_files_interface, $id, $user_id)
    {
        $this->dispatchBrowserEvent('DOMContentLoaded');
        $this->hrim_201_reference_id = $id;
        $this->user_id = $user_id;
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'view') {
            $this->emitTo('hrim.employee-management.employee-information.files.view-image', 'mount', $data['id']);
            $this->view_modal = true;
        } else if ($action_type == 'confirmation_delete') {
            $this->confirmation_message = "Are you sure to delete this attachment?";
            $this->confirmation_modal = true;
        } else if ($action_type == 'confirmation_restore') {
            $this->confirmation_message = "Are you sure to restore this attachment?";
            $this->confirmation_modal = true;
        }

        $this->hrim_201_files_attachment_id = $data['id'] ?? null;
    }

    public function confirm(Hrim201FilesInterface $hrim_201_files_interface)
    {
        if ($this->action_type == 'confirmation_delete') {
            $response = $hrim_201_files_interface->destroyAttachment($this->hrim_201_files_attachment_id);
            $this->view_modal = false;
        } elseif ($this->action_type == 'confirmation_restore') {
            $response = $hrim_201_files_interface->restoreAttachment($this->hrim_201_files_attachment_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.employee-management.employee-information.files.view', 'mount', $this->hrim_201_files->hrim_201_reference_id, $this->hrim_201_files->user_id);
            $this->emitTo('hrim.employee-management.employee-information.files.index', 'mount', $this->hrim_201_files->user_id);
            // $this->sweetAlert('', $response['message']);
            $this->confirmation_modal = false;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(Hrim201FilesInterface $hrim_201_files_interface)
    {
        $response = $hrim_201_files_interface->show($this->hrim_201_reference_id, $this->user_id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);
        $this->hrim_201_files = $response['result'];
        return view('livewire.hrim.employee-management.employee-information.files.view');
    }
}
