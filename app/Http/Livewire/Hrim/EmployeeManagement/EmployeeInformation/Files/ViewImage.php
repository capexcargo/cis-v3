<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\EmployeeInformation\Files;

use App\Interfaces\Hrim\EmployeeManagement\Hrim201FilesInterface;
use Livewire\Component;

class ViewImage extends Component
{
    public $hrim_201_files_attachment;

    protected $listeners = ['mount' => 'mount'];

    public function mount(Hrim201FilesInterface $hrim_201_files_interface, $id)
    {
        $this->dispatchBrowserEvent('DOMContentLoaded');
        $response = $hrim_201_files_interface->showAttachment($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);
        $this->hrim_201_files_attachment = $response['result'];
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.employee-information.files.view-image');
    }
}
