<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\DisciplinaryRecord;

use App\Interfaces\Hrim\EmployeeManagement\DisciplinaryRecordInterface;
use App\Models\Hrim\DisciplinaryRecord;
use App\Traits\Hrim\EmployeeManagement\DisciplinaryRecordTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use DisciplinaryRecordTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(DisciplinaryRecordInterface $disciplinary_record_interface, $id)
    {
        $this->resetForm();
        $response = $disciplinary_record_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->disciplinary_record = $response['result'];
        $this->display = $this->disciplinary_record->display;
    }

    public function submit(DisciplinaryRecordInterface $disciplinary_record_interface)
    {
        $response = $disciplinary_record_interface->update($this->disciplinary_record->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-management.disciplinary-record.index', 'close_modal', 'edit');
            $this->emitTo('hrim.employee-management.disciplinary-record.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            "display",
        ]);
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.disciplinary-record.edit');
    }
}
