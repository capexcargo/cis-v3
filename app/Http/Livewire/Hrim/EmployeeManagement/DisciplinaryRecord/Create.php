<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\DisciplinaryRecord;

use App\Interfaces\Hrim\EmployeeManagement\DisciplinaryRecordInterface;
use App\Repositories\Hrim\EmployeeManagement\DisciplinaryRecordRepository;
use App\Traits\Hrim\EmployeeManagement\DisciplinaryRecordTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use DisciplinaryRecordTrait, PopUpMessagesTrait;

    public function submit(DisciplinaryRecordInterface $disciplinary_record_interface)
    {
        $response = $disciplinary_record_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
            $this->confirmation_message = "Are you sure you want to submit this new Disciplinary Record?";
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.disciplinary-record.create');
    }
}
