<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\Violation;

use App\Interfaces\Hrim\EmployeeManagement\ViolationInterface;
use App\Repositories\Hrim\EmployeeManagement\ViolationRepository;
use App\Traits\Hrim\EmployeeManagement\ViolationTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{

    use ViolationTrait, PopUpMessagesTrait;

    public function submit(ViolationInterface $violation_interface)
    {
        $response = $violation_interface->create($this->getRequest());

        if ($response['code'] == 200) {

            $this->confirmation_modal = true;
            $this->confirmation_message = "Are you sure you want to submit this new violation?";
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.violation.create');
    }
}
