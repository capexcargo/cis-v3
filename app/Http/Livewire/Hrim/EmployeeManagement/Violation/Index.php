<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\Violation;

use App\Interfaces\Hrim\EmployeeManagement\ViolationInterface;
use App\Models\Hrim\Violation;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $confirmation_modal = false;
    public $confirmation_message;

    public $action_type;
    public $create_violation_modal = false;
    public $edit_violation_modal = false;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;

    public $violation_id;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToCocSection') {
            return redirect()->to(route('hrim.employee-management.coc-section.index'));
        } else if ($action_type == 'redirectToViolation') {
            return redirect()->to(route('hrim.employee-management.violation.index'));
        } else if ($action_type == 'redirectToDiscRecord') {
            return redirect()->to(route('hrim.employee-management.disciplinary-record.index'));
        } else if ($action_type == 'redirectToSanction') {
            return redirect()->to(route('hrim.employee-management.sanction.index'));
        } else if ($action_type == 'redirectToSanctionStat') {
            return redirect()->to(route('hrim.employee-management.sanction-status.index'));
        } else if ($action_type == 'create_violation') {
            $this->create_violation_modal = true;
        } else if ($action_type == 'edit_violation') {
            $this->emitTo('hrim.employee-management.violation.edit', $data['id']);
            $this->edit_violation_modal = true;
            $this->violation_id = $data['id'];
        } else if ($action_type == 'delete_violation') {
            $this->confirmation_message = "Are you sure you want to delete this violation?";
            $this->confirmation_modal = true;
            $this->violation_id = $data['id'];
        }
    }

    public function confirm(ViolationInterface $violation_interface)
    {
        if ($this->action_type == "delete_violation") {
            $response = $violation_interface->destroy($this->violation_id);
            $this->emitTo('hrim.employee-management.violation.index', 'close_modal', 'delete');
            $this->emitTo('hrim.employee-management.violation.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emit('index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_violation_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_violation_modal = false;
        } else if ($action_type == 'delete') {
            $this->confirmation_modal = false;
        }
    }


    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.violation.index', [
            'violations' => Violation::where('display', 'like', '%' . $this->display . '%')
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'desc' : 'asc');
                })->paginate($this->paginate)

        ]);
    }
}
