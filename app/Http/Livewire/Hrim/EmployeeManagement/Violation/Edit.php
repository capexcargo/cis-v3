<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\Violation;

use App\Interfaces\Hrim\EmployeeManagement\ViolationInterface;
use App\Models\Hrim\Violation;
use App\Traits\Hrim\EmployeeManagement\ViolationTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use ViolationTrait, PopUpMessagesTrait;

    // public $code;
    public $display;

    public $violation;

    protected $listeners = ['mount' => 'mount'];

    public function mount(ViolationInterface $violation_interface, $id)
    {
        $this->resetForm();
        $response = $violation_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->violation = $response['result'];
        $this->display = $this->violation->display;
    }

    public function submit(ViolationInterface $violation_interface)
    {
        $response = $violation_interface->update($this->violation->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-management.violation.index', 'close_modal', 'edit');
            $this->emitTo('hrim.employee-management.violation.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            "display",
        ]);
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.violation.edit');
    }
}
