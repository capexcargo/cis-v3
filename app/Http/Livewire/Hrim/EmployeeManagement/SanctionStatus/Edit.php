<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\SanctionStatus;

use App\Interfaces\Hrim\EmployeeManagement\SanctionStatusInterface;
use App\Traits\Hrim\EmployeeManagement\SanctionStatusTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use SanctionStatusTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(SanctionStatusInterface $sanction_status_interface, $id)
    {
        $this->resetForm();
        $response = $sanction_status_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->sanction_status = $response['result'];
        $this->display = $this->sanction_status->display;
    }

    public function submit(SanctionStatusInterface $sanction_status_interface)
    {
        $response = $sanction_status_interface->update($this->sanction_status->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-management.sanction-status.index', 'close_modal', 'edit');
            $this->emitTo('hrim.employee-management.sanction-status.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            "display",
        ]);
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.sanction-status.edit');
    }
}
