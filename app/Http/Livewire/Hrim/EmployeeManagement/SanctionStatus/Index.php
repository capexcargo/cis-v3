<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\SanctionStatus;

use App\Interfaces\Hrim\EmployeeManagement\SanctionStatusInterface;
use App\Models\Hrim\SanctionStatus;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $confirmation_modal = false;
    public $confirmation_message;

    public $action_type;
    public $create_sanction_status_modal = false;
    public $edit_sanction_status_modal = false;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;

    public $sanction_status_id;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToCocSection') {
            return redirect()->to(route('hrim.employee-management.coc-section.index'));
        } else if ($action_type == 'redirectToViolation') {
            return redirect()->to(route('hrim.employee-management.violation.index'));
        } else if ($action_type == 'redirectToDiscRecord') {
            return redirect()->to(route('hrim.employee-management.disciplinary-record.index'));
        } else if ($action_type == 'redirectToSanction') {
            return redirect()->to(route('hrim.employee-management.sanction.index'));
        } else if ($action_type == 'redirectToSanctionStat') {
            return redirect()->to(route('hrim.employee-management.sanction-status.index'));
        } else if ($action_type == 'create_sanction_status') {
            $this->create_sanction_status_modal = true;
        } else if ($action_type == 'edit_sanction_status') {
            $this->emitTo('hrim.employee-management.sanction-status.edit', $data['id']);
            $this->edit_sanction_status_modal = true;
            $this->sanction_status_id = $data['id'];
        } else if ($action_type == 'delete_sanction_status') {
            $this->confirmation_message = "Are you sure you want to delete this sanction status?";
            $this->confirmation_modal = true;
            $this->sanction_status_id = $data['id'];
        }
    }

    public function confirm(SanctionStatusInterface $sanction_status_interface)
    {
        if ($this->action_type == "delete_sanction_status") {
            $response = $sanction_status_interface->destroy($this->sanction_status_id);
            $this->emitTo('hrim.employee-management.sanction-status.index', 'close_modal', 'delete');
            $this->emitTo('hrim.employee-management.sanction-status.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emit('index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_sanction_status_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_sanction_status_modal = false;
        } else if ($action_type == 'delete') {
            $this->confirmation_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.sanction-status.index', [
            'sanction_statuses' => SanctionStatus::where('display', 'like', '%' . $this->display . '%')
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'desc' : 'asc');
                })->paginate($this->paginate)

        ]);
    }
}
