<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\SanctionStatus;

use App\Interfaces\Hrim\EmployeeManagement\SanctionStatusInterface;
use App\Repositories\Hrim\EmployeeManagement\SanctionStatusRepository;
use App\Traits\Hrim\EmployeeManagement\SanctionStatusTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{

    use SanctionStatusTrait, PopUpMessagesTrait;

    public function submit(SanctionStatusInterface $sanction_status_interface)
    {
        $response = $sanction_status_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->confirmation_modal = true;
            $this->confirmation_message = "Are you sure you want to submit this new Sanction Status?";
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.sanction-status.create');
    }
}
