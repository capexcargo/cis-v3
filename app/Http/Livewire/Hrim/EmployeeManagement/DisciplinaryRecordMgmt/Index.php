<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\DisciplinaryRecordMgmt;

use Livewire\Component;

class Index extends Component
{
    
    public $action_type;

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToCocSection') {
            return redirect()->to(route('hrim.employee-management.coc-section.index'));
        } else if ($action_type == 'redirectToViolation') {
            return redirect()->to(route('hrim.employee-management.violation.index'));
        } else if ($action_type == 'redirectToDiscRecord') {
            return redirect()->to(route('hrim.employee-management.disciplinary-record.index'));
        } else if ($action_type == 'redirectToSanction') {
            return redirect()->to(route('hrim.employee-management.sanction.index'));
        } else if ($action_type == 'redirectToSanctionStat') {
            return redirect()->to(route('hrim.employee-management.sanction-status.index'));
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.disciplinary-record-mgmt.index');
    }
}
