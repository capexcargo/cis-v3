<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\DisciplinaryHistory;

use App\Repositories\Hrim\EmployeeManagement\DisciplinaryHistoryRepository;
use App\Traits\Hrim\EmployeeManagement\DisciplinaryHistoryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use DisciplinaryHistoryTrait, PopUpMessagesTrait, WithFileUploads;

    public function mount()
    {
        $this->addAttachments();
    }

    public function submit()
    {
        $validated = $this->validate([
            'employee_name' => 'required',
            'incident_date' => 'required',
            'incident_time' => 'required',
            'incident_location' => 'required',
            'description' => 'required',
            'coc_section' => 'required',
            'violation' => 'required',
            'disciplinary_record' => 'required',
            'sanction' => 'required',
            'sanction_status' => 'required',

            'attachments' => 'required',
            'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),
        ], [
            'attachments.*.attachment.required' => 'This attachment field is required.',
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
        ]);

        $disciplinary_history_repository = new DisciplinaryHistoryRepository();
        $response = $disciplinary_history_repository->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-management.disciplinary-history.index', 'close_modal', 'create');
            $this->emitTo('hrim.employee-management.disciplinary-history.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.disciplinary-history.create');
    }
}
