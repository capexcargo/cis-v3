<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\DisciplinaryHistory;

use App\Interfaces\Hrim\EmployeeManagement\DisciplinaryHistoryInterface;
use App\Models\Hrim\DisciplinaryHistory;
use App\Models\Hrim\Position;
use App\Models\User;
use App\Traits\Hrim\EmployeeManagement\DisciplinaryHistoryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use DisciplinaryHistoryTrait, PopUpMessagesTrait, WithFileUploads;

    protected $listeners = ['edit' => 'mount'];

    public function mount(DisciplinaryHistoryInterface $disciplinary_interface, $id)
    {
        $this->resetForm();

        // $this->disciplinary_history = DisciplinaryHistory::with('userDetails')->findOrFail($id);

        $response = $disciplinary_interface->show($id);

        if ($response['code'] == 200) {
            $this->disciplinary_history = $response['result'];
        } else {
            return $this->sweetAlertError('error', $response['message'], $response['result']);
        }

        $this->user = User::with('userDetails')->find($this->disciplinary_history->user_id);
        $this->emp_position = Position::find($this->user->userDetails->position_id);

        $this->employee_name_search = $this->user->name;
        $this->employee_name = $this->user->id;
        $this->position = $this->emp_position->display;
        $this->incident_date = $this->disciplinary_history->incident_date;
        $this->incident_time = $this->disciplinary_history->incident_time;
        $this->incident_location = $this->disciplinary_history->incident_branch;
        $this->description = $this->disciplinary_history->description;
        $this->coc_section = $this->disciplinary_history->coc_section_id;
        $this->violation = $this->disciplinary_history->violation_id;
        $this->disciplinary_record = $this->disciplinary_history->disciplinary_record_id;
        $this->sanction = $this->disciplinary_history->sanction_id;
        $this->sanction_status = $this->disciplinary_history->sanction_status_id;

        foreach ($this->disciplinary_history->attachments as $attachment) {
            $this->attachments[] = [
                'id' => $attachment->id,
                'attachment' => '',
                'path' => $attachment->path,
                'name' => $attachment->name,
                'extension' => $attachment->extension,
                'is_deleted' => false,
            ];
        }
    }

    public function submit(DisciplinaryHistoryInterface $disciplinary_history_interface)
    {
        $validated = $this->validate([
            'employee_name' => 'required',
            'incident_date' => 'required',
            'incident_time' => 'required',
            'incident_location' => 'required',
            'description' => 'required',
            'coc_section' => 'required',
            'violation' => 'required',
            'disciplinary_record' => 'required',
            'sanction' => 'required',
            'sanction_status' => 'required',

            'attachments' => 'required',
            // 'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),
        ], [
            'attachments.*.attachment.required' => 'This attachment field is required.',
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
        ]);

        $response = $disciplinary_history_interface->update($this->disciplinary_history, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-management.disciplinary-history.index', 'close_modal', 'edit');
            $this->emitTo('hrim.employee-management.disciplinary-history.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.disciplinary-history.edit');
    }
}
