<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\DisciplinaryHistory;

use App\Models\Hrim\DisciplinaryHistory;
use Livewire\Component;

class View extends Component
{
    public $requests;

    protected $listeners = ['view' => 'mount'];

    public function mount($id)
    {
        $this->reset([
            'requests',
        ]);

        $this->requests = DisciplinaryHistory::with('attachments')->where('id', $id)->get();
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.disciplinary-history.view');
    }
}
