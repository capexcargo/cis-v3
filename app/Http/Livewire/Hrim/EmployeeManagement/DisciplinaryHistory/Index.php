<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\DisciplinaryHistory;

use App\Interfaces\Hrim\EmployeeManagement\DisciplinaryHistoryInterface;
use App\Models\Hrim\DisciplinaryHistory;
use App\Models\Hrim\Position;
use App\Traits\Hrim\EmployeeManagement\DisciplinaryHistoryTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use DisciplinaryHistoryTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_disciplinary_history') {
            $this->create_disciplinary_history_modal = true;
        } else if ($action_type == 'edit_disciplinary_history') {
            $this->emit('edit', $data['id']);
            $this->edit_disciplinary_history_modal = true;
            $this->disciplinary_history_id = $data['id'];
        } else if ($action_type == 'view_disciplinary_history') {
            $this->emit('view', $data['id']);
            $this->view_disciplinary_history_modal = true;
            $this->disciplinary_history_id = $data['id'];
        }
    }

    public function confirm(DisciplinaryHistoryInterface $disciplinary_history_interface)
    {
        if ($this->action_type == "delete_disciplinary_history") {
            $response = $disciplinary_history_interface->destroy($this->disciplinary_history_id);
        }

        if ($response['code'] == 200) {
            $this->emit('index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_disciplinary_history_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_disciplinary_history_modal = false;
        } else if ($action_type == 'view') {
            $this->view_disciplinary_history_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        
        return view('livewire.hrim.employee-management.disciplinary-history.index',
            [
                'disciplinary_histories' => DisciplinaryHistory::with('user', 'userDetails', 'branch', 'cocSection', 'violation', 'disciplinaryRecord', 'sanction', 'sanctionStatus', 'createdBy')
                    ->whereHas('user', function ($query) {
                        $query->when($this->employee_name_search, function ($query) {
                            $query->where('name', 'like', '%' . $this->employee_name_search . '%');
                        });
                    })
                    ->when($this->date_created, function ($query) {
                        $query->whereDate('created_at', $this->date_created);
                    })
                    ->when($this->incident_date, function ($query) {
                        $query->whereDate('incident_date', $this->incident_date);
                    })
                    ->when($this->sortField, function ($query) {
                        $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                    })->paginate($this->paginate)
            ]
        );
    }
}
