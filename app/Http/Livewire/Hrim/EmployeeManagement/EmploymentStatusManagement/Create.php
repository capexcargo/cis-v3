<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\EmploymentStatusManagement;

use App\Interfaces\Hrim\EmployeeManagement\EmploymentStatusManagementInterface;
use App\Traits\Hrim\EmployeeManagement\EmploymentStatusTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use EmploymentStatusTrait, PopUpMessagesTrait;

    protected $rules = [
        'status' => 'required',
    ];

    public function confirmationSubmit()
    {
        $this->validate();

        $this->confirmation_modal = true;
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('hrim.employee-management.employment-status-management.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }

    
    public function submit(EmploymentStatusManagementInterface $employment_status_management_interface)
    {
        // dd($this->getRequest());
        
        $response = $employment_status_management_interface->create($this->getRequest());
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-management.employment-status-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.employee-management.employment-status-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.employment-status-management.create');
    }
}


