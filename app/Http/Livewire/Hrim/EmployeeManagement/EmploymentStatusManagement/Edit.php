<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\EmploymentStatusManagement;

use App\Interfaces\Hrim\EmployeeManagement\EmploymentStatusManagementInterface;
use App\Models\Hrim\EmploymentStatus;
use App\Traits\Hrim\EmployeeManagement\EmploymentStatusTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use EmploymentStatusTrait, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->employment_status = EmploymentStatus::findOrFail($id);

        $this->status = $this->employment_status->display;
        // $this->category = $this->employment_category_type->employment_category_id;
    }

    public function submit(EmploymentStatusManagementInterface $employment_status)
    {
        $response = $employment_status->update($this->getRequest(), $this->employment_status->id);
        
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-management.employment-status-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.employee-management.employment-status-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-management.employment-status-management.edit');
    }
}
