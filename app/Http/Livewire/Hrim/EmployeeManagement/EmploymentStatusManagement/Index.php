<?php

namespace App\Http\Livewire\Hrim\EmployeeManagement\EmploymentStatusManagement;

use App\Interfaces\Hrim\EmployeeManagement\EmploymentStatusManagementInterface;
use App\Traits\Hrim\EmployeeManagement\EmploymentStatusTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use EmploymentStatusTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_emp_stat') {
            $this->create_modal = true;
        }elseif ($action_type == 'edit') {
            $this->emitTo('hrim.employee-management.employment-status-management.edit', 'edit', $data['id']);
            $this->emp_status_id = $data['id'];
            $this->edit_modal = true;
            
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this Employment Status?";
            $this->delete_modal = true;
            $this->emp_status_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }

    public function confirm(EmploymentStatusManagementInterface $employment_status_management_interface)
    {
        if ($this->action_type == "delete") {
            $response = $employment_status_management_interface->destroy($this->emp_status_id);
            $this->emitTo('hrim.employee-management.employment-status-management.index', 'close_modal', 'delete');
            $this->emitTo('hrim.employee-management.employment-status-management.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.employee-management.employment-status-management.index', 'close_modal', 'delete');
            $this->emitTo('hrim.employee-management.employment-status-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }







    public function render(EmploymentStatusManagementInterface $employment_status_management_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        $response = $employment_status_management_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'employment_status1' => [],
                ];
        }
    
        return view('livewire.hrim.employee-management.employment-status-management.index',[
            'employment_status1' => $response['result']['employment_status1']
        ]);
    }
}
