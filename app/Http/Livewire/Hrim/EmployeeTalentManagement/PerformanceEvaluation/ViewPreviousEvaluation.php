<?php

namespace App\Http\Livewire\Hrim\EmployeeTalentManagement\PerformanceEvaluation;

use App\Models\Hrim\PerformanceEvaluation;
use Livewire\Component;


class ViewPreviousEvaluation extends Component
{
    protected $listeners = ['mount' => 'mount'];

    protected $previous_evaluations = [];
    
    public function mount($id)
    {
        $this->previous_evaluations = PerformanceEvaluation::where('user_id', $id)
            ->select('user_id', 'year', 'quarter', 'first_status', 'second_status', 'third_status', 'first_evaluator', 'second_evaluator', 'third_evaluator')
            ->distinct()
            ->get();
    }

    public function action($data, $action_type)
    {
        if ($action_type == 'view') {
            return redirect()->route('hrim.employee-talent-management.performance-evaluation.view-evaluation', ['id' => $data['id'], 'quarter' => $data['quarter'], 'year' => $data['year']]);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-talent-management.performance-evaluation.view-previous-evaluation', ['previous_evaluations' => $this->previous_evaluations]);
    }
}
