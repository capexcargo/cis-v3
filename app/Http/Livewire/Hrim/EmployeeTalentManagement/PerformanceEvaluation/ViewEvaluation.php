<?php

namespace App\Http\Livewire\Hrim\EmployeeTalentManagement\PerformanceEvaluation;

use App\Models\Hrim\PerformanceEvaluation;
use App\Models\UserDetails;
use Livewire\Component;

class ViewEvaluation extends Component
{
    public $view_jd_modal = false;
    public $view_justification_modal = false;

    public $performance_evaluation_references = [];
    public $position_id;
    public $eval_id;

    public $breakdown_pct_goals;
    public $breakdown_pct_core_values;
    public $breakdown_pct_leadership_competencies;

    public $performance_scorings = [];
    public $core_value_assessments = [];
    public $leadership_competencies = [];
    public $performance_evaluators = [];

    public $total_first_points_eval_scoring;
    public $total_second_points_eval_scoring;
    public $total_third_points_eval_scoring;

    public $total_first_points_core_value;
    public $total_second_points_core_value;
    public $total_third_points_core_value;

    public $total_first_points_leader_comp;
    public $total_second_points_leader_comp;
    public $total_third_points_leader_comp;

    public $total_employee_points_core_value;
    public $total_employee_points_leader_comp;
    
    protected $listeners = ['view' => 'render', 'mount' => 'mount'];

    public function mount($id, $quarter, $year)
    {
        $this->performance_evaluation_references = UserDetails::with('performanceEvals', 'breakdownPct', 'position', 'division', 'jobLevel')->where('id', $id)
            ->get();

        foreach ($this->performance_evaluation_references as $perform_breakdown_percentage) {
            $this->breakdown_pct_goals = $perform_breakdown_percentage->breakdownPct->goals;
            $this->breakdown_pct_core_values = $perform_breakdown_percentage->breakdownPct->core_values;
            $this->breakdown_pct_leadership_competencies = $perform_breakdown_percentage->breakdownPct->leadership_competencies;
        }

        $kpi_tagged_references = PerformanceEvaluation::with('kra', 'kpi')->where('quarter', $quarter)->where('year', $year)->where('user_id', $id)->where('kra_id', '!=', null)->get();
        $kpi_tagged_core_values = PerformanceEvaluation::with('core_values')->where('quarter', $quarter)->where('year', $year)->where('user_id', $id)->where('core_values_id', '!=', null)->get();
        $kpi_tagged_leadership_comps = PerformanceEvaluation::with('leadership_comp')->where('quarter', $quarter)->where('year', $year)->where('user_id', $id)->where('leadership_comp_id', '!=', null)->get();
        $performance_evaluation_evals = PerformanceEvaluation::where('user_id', $id)
            ->select('first_evaluator', 'second_evaluator', 'third_evaluator', 'first_status', 'second_status', 'third_status', 'first_remarks', 'second_remarks', 'third_remarks', 'year', 'quarter')
            ->where('quarter', $quarter)
            ->where('year', $year)
            ->distinct()
            ->get();

        foreach ($kpi_tagged_references as $j => $kpi_tagged) {
            $this->performance_scorings[] = [
                'id' => $kpi_tagged->id,
                'kra' => $kpi_tagged->kra->name,
                'kpi' => $kpi_tagged->kpi->name,
                'target' => $kpi_tagged->kpi->target,
                'points' => $kpi_tagged->kpi->points,
                'first_evaluator' => $kpi_tagged->first_evaluator,
                'second_evaluator' => $kpi_tagged->second_evaluator,
                'third_evaluator' => $kpi_tagged->third_evaluator,
                'first_evaluator_points' => $kpi_tagged->first_points == '' ? null : $kpi_tagged->first_points,
                'second_evaluator_points' => $kpi_tagged->second_points == '' ? null : $kpi_tagged->second_points,
                'third_evaluator_points' => $kpi_tagged->third_points == '' ? null : $kpi_tagged->third_points,
                'first_status' => $kpi_tagged->first_status,
                'second_status' => $kpi_tagged->second_status,
                'third_status' => $kpi_tagged->third_status,
                'first_points' => $kpi_tagged->first_points,
                'second_points' => $kpi_tagged->second_points,
                'third_points' => $kpi_tagged->third_points,
            ];
        }

        foreach ($kpi_tagged_core_values as $j => $core_val) {
            $this->core_value_assessments[] = [
                'id' => $core_val->id,
                'description' => $core_val->core_values->description,
                'first_evaluator' => $core_val->first_evaluator,
                'second_evaluator' => $core_val->second_evaluator,
                'third_evaluator' => $core_val->third_evaluator,
                'first_evaluator_points' => $core_val->first_points == '' ? null : $core_val->first_points,
                'second_evaluator_points' => $core_val->second_points == '' ? null : $core_val->second_points,
                'third_evaluator_points' => $core_val->third_points == '' ? null : $core_val->third_points,
                'first_status' => $core_val->first_status,
                'second_status' => $core_val->second_status,
                'third_status' => $core_val->third_status,
                'first_points' => $core_val->first_points,
                'second_points' => $core_val->second_points,
                'third_points' => $core_val->third_points,
                'employee_self_points' => $core_val->employee_self_points,
                'emp_critical_remarks' => $core_val->critical_remarks,
                'evaluator_1st_critical_remarks' => $core_val->core_val_1st_critical_remarks == '' ? null : $core_val->core_val_1st_critical_remarks,
                'evaluator_2nd_critical_remarks' => $core_val->core_val_2nd_critical_remarks == '' ? null : $core_val->core_val_2nd_critical_remarks,
                'evaluator_3rd_critical_remarks' => $core_val->core_val_3rd_critical_remarks == '' ? null : $core_val->core_val_3rd_critical_remarks,
            ];
        }

        if (count($kpi_tagged_leadership_comps) > 0) {
            foreach ($kpi_tagged_leadership_comps as $j => $lead_comp) {
                $this->leadership_competencies[] = [
                    'id' => $lead_comp->id,
                    'description' => $lead_comp->leadership_comp->description,
                    'first_evaluator' => $lead_comp->first_evaluator,
                    'second_evaluator' => $lead_comp->second_evaluator,
                    'third_evaluator' => $lead_comp->third_evaluator,
                    'first_evaluator_points' => $lead_comp->first_points == '' ? null : $lead_comp->first_points,
                    'second_evaluator_points' => $lead_comp->second_points == '' ? null : $lead_comp->second_points,
                    'third_evaluator_points' => $lead_comp->third_points == '' ? null : $lead_comp->third_points,
                    'first_status' => $lead_comp->first_status,
                    'second_status' => $lead_comp->second_status,
                    'third_status' => $lead_comp->third_status,
                    'first_points' => $lead_comp->first_points,
                    'second_points' => $lead_comp->second_points,
                    'third_points' => $lead_comp->third_points,
                    'employee_self_points' => $lead_comp->employee_self_points,
                    'emp_critical_remarks' => $lead_comp->critical_remarks,
                    'evaluator_1st_critical_remarks' => $lead_comp->lead_com_1st_critical_remarks == '' ? null : $lead_comp->lead_com_1st_critical_remarks,
                    'evaluator_2nd_critical_remarks' => $lead_comp->lead_com_2nd_critical_remarks == '' ? null : $lead_comp->lead_com_2nd_critical_remarks,
                    'evaluator_3rd_critical_remarks' => $lead_comp->lead_com_3rd_critical_remarks == '' ? null : $lead_comp->lead_com_3rd_critical_remarks,
                ];
            }
        }

        foreach ($performance_evaluation_evals as $j => $evaluator) {
            $this->performance_evaluators[] = [
                'id' => $evaluator->id,
                'first_evaluator' => $evaluator->first_evaluator,
                'second_evaluator' => $evaluator->second_evaluator,
                'third_evaluator' => $evaluator->third_evaluator,
                'first_status' => $evaluator->first_status,
                'second_status' => $evaluator->second_status,
                'third_status' => $evaluator->third_status,
                'first_remarks' => $evaluator->first_remarks == '' ? null : $evaluator->first_remarks,
                'second_remarks' => $evaluator->second_remarks == '' ? null : $evaluator->second_remarks,
                'third_remarks' => $evaluator->third_remarks == '' ? null : $evaluator->third_remarks,
                'year' => $evaluator->year,
                'quarter' => $evaluator->quarter,
            ];
        }
    }

    public function action($data, $action_type)
    {
        if ($action_type == 'back') {
            return redirect(route('hrim.employee-talent-management.performance-evaluation.index'));
        } else if ($action_type == 'view_jd') {
            $this->position_id = $data['id'];
            $this->view_jd_modal = true;
            $this->emitTo('hrim.workforce.position-management.view', 'view');
        } else if ($action_type == 'view_justification') {
            $this->eval_id = $data['id'];
            $this->view_justification_modal = true;
        }
    }

    public function load()
    {
        $this->compute_eval_scoring();
        $this->compute_core_value();
        $this->compute_leader_comp();
    }

    public function compute_eval_scoring()
    {
        foreach ($this->performance_scorings as $i => $scoring) {
            $scoring_first_points[] =  $scoring['first_evaluator_points'];
            $total = array_sum($scoring_first_points) / count($scoring_first_points);
            $this->total_first_points_eval_scoring = number_format($total, 1, '.', '');
        
            $scoring_second_points[] =  $scoring['second_evaluator_points'];
            $total = array_sum($scoring_second_points) / count($scoring_second_points);
            $this->total_second_points_eval_scoring = number_format($total, 1, '.', '');
        
            $scoring_third_points[] =  $scoring['third_evaluator_points'];
            $total = array_sum($scoring_third_points) / count($scoring_third_points);
            $this->total_third_points_eval_scoring = number_format($total, 1, '.', '');
        }
    }

    public function compute_core_value()
    {
        foreach ($this->core_value_assessments as $i => $scoring) {
            $core_value_first_points[] =  $scoring['first_evaluator_points'];
            $core_value_employee_points[] =  $scoring['employee_self_points'];
            $total = array_sum($core_value_first_points) / count($core_value_first_points);
            $this->total_first_points_core_value = number_format($total, 1, '.', '');
            $total_employee_points = array_sum($core_value_employee_points) / count($core_value_employee_points);
            $this->total_employee_points_core_value = number_format($total_employee_points, 1, '.', '');

            $core_value_second_points[] =  $scoring['second_evaluator_points'];
            $total = array_sum($core_value_second_points) / count($core_value_second_points);
            $this->total_second_points_core_value = number_format($total, 1, '.', '');

            $core_value_third_points[] =  $scoring['third_evaluator_points'];
            $total = array_sum($core_value_third_points) / count($core_value_third_points);
            $this->total_third_points_core_value = number_format($total, 1, '.', '');
        }
    }
    
    public function compute_leader_comp()
    {
        foreach ($this->leadership_competencies as $i => $scoring) {
            $lead_comp_first_points[] =  $scoring['first_evaluator_points'];
            $lead_comp_employee_points[] =  $scoring['employee_self_points'];
            $total = array_sum($lead_comp_first_points) / count($lead_comp_first_points);
            $this->total_first_points_leader_comp = number_format($total, 1, '.', '');
            $total_employee_points = array_sum($lead_comp_employee_points) / count($lead_comp_employee_points);
            $this->total_employee_points_leader_comp = number_format($total_employee_points, 1, '.', '');

            $lead_comp_second_points[] =  $scoring['second_evaluator_points'];
            $total = array_sum($lead_comp_second_points) / count($lead_comp_second_points);
            $this->total_second_points_leader_comp = number_format($total, 1, '.', '');

            $lead_comp_third_points[] =  $scoring['third_evaluator_points'];
            $total = array_sum($lead_comp_third_points) / count($lead_comp_third_points);
            $this->total_third_points_leader_comp = number_format($total, 1, '.', '');
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-talent-management.performance-evaluation.view-evaluation');
    }
}
