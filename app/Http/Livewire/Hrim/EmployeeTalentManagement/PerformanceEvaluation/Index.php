<?php

namespace App\Http\Livewire\Hrim\EmployeeTalentManagement\PerformanceEvaluation;

use App\Interfaces\Hrim\TalentManagement\PerformanceEvaluationInterface;
use App\Models\Hrim\PerformanceEvaluation;
use App\Traits\Hrim\TalentManagement\PerformanceEvaluationTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use PerformanceEvaluationTrait, WithPagination, PopUpMessagesTrait;

    public $view_previous_modal = false;
    public $employee_id;

    protected $listeners = ['index' => 'render', 'mount' => 'mount'];

    public function mount(PerformanceEvaluationInterface $performance_evaluation_interface)
    {
        $response = $performance_evaluation_interface->eIndex($this->filterRequest());

        $this->core_values = $response['result']['core_values'];
        $this->leadership_comps = $response['result']['leadership_comps'];
        $this->employee_evals = $response['result']['employee_evals'];

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [
                'core_values' => [],
                'leadership_comps' => [],
                'employee_evals' => [],
            ];
        }
        // dd($response['result']);
    }

    protected function updatedQuarter()
    {
        $this->emit('mount');
        $this->total_employee_points_core_value = 0;
        $this->total_employee_points_leader_comp = 0;
    }

    protected function updatedYear()
    {
        $this->emit('mount');
        $this->total_employee_points_core_value = 0;
        $this->total_employee_points_leader_comp = 0;
    }

    public function action(PerformanceEvaluationInterface $performance_evaluation_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'previous_evaluation') {
        } else if ($action_type == 'save') {

            $response = $performance_evaluation_interface->eSave($this->eGetRequest());

            if ($response['code'] == 200) {
                $this->emitTo('livewire.hrim.employee-talent-management.performance-evaluation.index', 'index');
                $this->sweetAlert('', $response['message']);
                return redirect()->to(route('hrim.employee-talent-management.performance-evaluation.index'));
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } else if ($action_type == 'view_previou_eval') {
            $this->view_previous_modal = true;
            $this->employee_id = $data['id'];
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-talent-management.performance-evaluation.index');
    }
}
