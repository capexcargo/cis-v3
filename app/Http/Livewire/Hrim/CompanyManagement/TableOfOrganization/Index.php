<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\TableOfOrganization;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.hrim.company-management.table-of-organization.index');
    }
}
