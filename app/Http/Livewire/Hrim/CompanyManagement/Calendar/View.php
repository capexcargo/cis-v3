<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\Calendar;

use App\Interfaces\Hrim\CompanyManagement\CalendarInterface;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class View extends Component
{
    use PopUpMessagesTrait;

    public $action_type;
    public $confirmation_modal = false;
    public $confirmation_message;

    public $edit_modal = false;
    public $calendar_id;

    public $calendar_event_references = [];
    public $birth_month_references = [];

    protected $listeners = ['mount' => 'mount', 'close_modal' => 'closeModal'];

    public function mount(CalendarInterface $calendar_interface, $date)
    {
        $this->dispatchBrowserEvent('DOMContentLoaded');
        $request = [
            'with_trashed' => (Auth::user()->accesses()->contains('hrim_calendar_delete') ? true : false),
            'type' => 'date',
            'date' => $date,
            'sort_field' => 'event_date_time',
            'sort_type' => 'asc',
        ];

        $response = $calendar_interface->getCalendarEvents($request);
        abort_if($response['code'] != 200, $response['code'], $response['message']);
        $this->calendar_event_references = $response['result']['calendars'];
        $this->birth_month_references = $response['result']['birth_months'];
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'edit') {
            $this->calendar_id = $data['id'];
            $this->edit_modal = true;
        } elseif ($action_type == 'delete') {
            $this->calendar_id = $data['id'];
            $this->confirmation_message = 'Are you sure you want to delete this event?';
            $this->confirmation_modal = true;
        } elseif ($action_type == 'restore') {
            $this->calendar_id = $data['id'];
            $this->confirmation_message = 'Are you sure you want to restore this event?';
            $this->confirmation_modal = true;
        }
    }

    public function confirm(CalendarInterface $calendar_interface)
    {
        if ($this->action_type == 'delete') {
            $response = $calendar_interface->destroy($this->calendar_id);
        } elseif ($this->action_type == 'restore') {
            $response = $calendar_interface->restore($this->calendar_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.company-management.calendar.view', 'close_modal', 'confirmation');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'edit') {
            $this->edit_modal = false;
        } else if ($action_type == 'confirmation') {
            $this->confirmation_modal = false;
        }
    }

    public function render()
    {
        return view('livewire.hrim.company-management.calendar.view');
    }
}
