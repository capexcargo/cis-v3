<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\Calendar;

use App\Interfaces\Hrim\CompanyManagement\CalendarInterface;
use App\Traits\Hrim\CompanyManagement\CalendarTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Edit extends Component
{
    use CalendarTrait, PopUpMessagesTrait;

    public function mount(CalendarInterface $calendar_interface, $id)
    {
        $request = [
            'with_trashed' => (Auth::user()->accesses()->contains('hrim_calendar_delete') ? true : false),
        ];
        $response = $calendar_interface->show($id, $request);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->calendar = $response['result'];
        $this->branch = $this->calendar->branch_id;
        $this->event_type = $this->calendar->event_type_id;
        $this->date_and_time = $this->calendar->event_date_time;
        $this->event_title = $this->calendar->title;
        $this->description = $this->calendar->description;
    }

    public function submit(CalendarInterface $calendar_interface)
    {
        $response = $calendar_interface->update($this->calendar->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.company-management.calendar.index', 'close_modal', 'edit');
            $this->emitTo('hrim.company-management.calendar.view', 'close_modal', 'edit');
            $this->emitTo('hrim.company-management.calendar.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.company-management.calendar.edit');
    }
}
