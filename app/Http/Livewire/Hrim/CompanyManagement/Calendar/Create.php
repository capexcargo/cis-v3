<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\Calendar;

use App\Interfaces\Hrim\CompanyManagement\CalendarInterface;
use App\Traits\Hrim\CompanyManagement\CalendarTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use CalendarTrait, PopUpMessagesTrait;

    public $message;
    public $message_modal = false;
    public function submit(CalendarInterface $calendar_interface)
    {
        $response = $calendar_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            // $this->emitTo('hrim.company-management.calendar.index', 'close_modal', 'create');
            // $this->emitTo('hrim.company-management.calendar.index', 'index');
            // $this->sweetAlert('', $response['message']);
            $this->message_modal = true;
            $this->message = $response['message'];
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function okAction()
    {
        $this->message_modal = false;
        $this->emitTo('hrim.company-management.calendar.index', 'close_modal', 'create');
        // $this->emitTo('hrim.company-management.calendar.index', 'index', now());

        return redirect()->route('hrim.company-management.calendar.index');
    }


    public function render()
    {
        return view('livewire.hrim.company-management.calendar.create');
    }
}
