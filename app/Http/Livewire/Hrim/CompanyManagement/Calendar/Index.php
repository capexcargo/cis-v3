<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\Calendar;

use App\Interfaces\Hrim\CompanyManagement\CalendarInterface;
use App\Repositories\Hrim\CompanyManagement\CalendarRepository;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Index extends Component
{
    use PopUpMessagesTrait;

    public $action_type;
    public $confirmation_modal = false;
    public $confirmation_message;

    public $create_modal = false;
    public $view_modal = false;
    public $edit_modal = false;
    public $calendar_id;

    public $month;
    public $year;
    public $date;

    public $calendar_event_references = [];
    public $birth_month_references = [];
    public $holiday_references = [];

    protected $listeners = ['index' => 'render', 'month_calendar_event' => 'monthCalendarEvent', 'close_modal' => 'closeModal'];

    public function mount()
    {
        $this->month = now()->month;
        $this->year = now()->year;
        $this->date = now();
    }

    public function monthCalendarEvent($date)
    {
        $this->date = $date;
        $request = [
            'with_trashed' => (Auth::user()->accesses()->contains('hrim_calendar_delete') ? true : false),
            'type' => 'month',
            'date' => $date,
            'sort_field' => 'event_date_time',
            'sort_type' => 'desc',
        ];

        $calendar_repository = new CalendarRepository();
        $response = $calendar_repository->getCalendarEvents($request);

        if ($response['code'] != 200) $this->sweetAlertError('error', $response['message'], $response['result']);
        $this->calendar_event_references = $response['result']['calendars']->toArray();
        $this->birth_month_references = $response['result']['birth_months'];

        $this->holiday_references = $response['result']['holiday_in_month'];
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'view') {
            $this->month = date('m', strtotime($data['date']));
            $this->year = date('Y', strtotime($data['date']));
            $this->date = $data['date'];
            $this->emitTo('hrim.company-management.calendar.view', 'mount', $data['date']);
            $this->view_modal = true;
        } elseif ($action_type == 'edit') {
            $this->calendar_id = $data['id'];
            $this->edit_modal = true;
        } elseif ($action_type == 'delete') {
            $this->calendar_id = $data['id'];
            $this->confirmation_message = 'Are you sure you want to delete this event?';
            $this->confirmation_modal = true;
        } elseif ($action_type == 'restore') {
            $this->calendar_id = $data['id'];
            $this->confirmation_message = 'Are you sure you want to restore this event?';
            $this->confirmation_modal = true;
        }
    }

    public function confirm(CalendarInterface $calendar_interface)
    {
        if ($this->action_type == 'delete') {
            $response = $calendar_interface->destroy($this->calendar_id);
        } elseif ($this->action_type == 'restore') {
            $response = $calendar_interface->restore($this->calendar_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.company-management.calendar.index', 'close_modal', 'confirmation');
            $this->emitTo('hrim.company-management.calendar.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } else if ($action_type == 'view') {
            $this->view_modal = false;
        } else if ($action_type == 'confirmation') {
            $this->confirmation_modal = false;
        }
    }

    public function render()
    {
        $this->monthCalendarEvent($this->date);
        return view('livewire.hrim.company-management.calendar.index');
    }
}
