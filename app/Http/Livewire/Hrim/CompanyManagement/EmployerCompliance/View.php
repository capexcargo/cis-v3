<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\EmployerCompliance;

use App\Interfaces\Hrim\CompanyManagement\EmployerComplianceInterface;
use App\Traits\Hrim\CompanyManagement\EmployerComplianceTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class View extends Component
{
    use EmployerComplianceTrait, PopUpMessagesTrait;

    protected $listeners = ['employer_compliance_view_mount' => 'mount'];

    public function mount(EmployerComplianceInterface $employer_compliance_interface, $id)
    {
        $this->resetForm();
        $request = [
            'with_trashed' => (Auth::user()->level_id == 5 ? true : false),
        ];

        $response = $employer_compliance_interface->show($id, $request);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->employer_compliance = $response['result'];
    }

    public function render()
    {
        return view('livewire.hrim.company-management.employer-compliance.view');
    }
}
