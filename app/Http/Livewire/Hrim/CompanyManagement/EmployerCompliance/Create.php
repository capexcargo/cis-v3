<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\EmployerCompliance;

use App\Interfaces\Hrim\CompanyManagement\EmployerComplianceInterface;
use App\Traits\Hrim\CompanyManagement\EmployerComplianceTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use EmployerComplianceTrait, WithFileUploads, PopUpMessagesTrait;

    public function submit(EmployerComplianceInterface $employer_compliance_interface)
    {
        $response = $employer_compliance_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.company-management.employer-compliance.index', 'close_modal', 'create');
            $this->emitTo('hrim.company-management.employer-compliance.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 409) {
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.company-management.employer-compliance.create');
    }
}
