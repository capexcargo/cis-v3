<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\EmployerCompliance;

use App\Interfaces\Hrim\CompanyManagement\EmployerComplianceInterface;
use App\Traits\Hrim\CompanyManagement\EmployerComplianceTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use EmployerComplianceTrait, WithFileUploads, PopUpMessagesTrait;

    protected $listeners = ['employer_compliance_edit_mount' => 'mount'];

    public function mount(EmployerComplianceInterface $employer_compliance_interface, $id)
    {
        $this->resetForm();
        $request = [
            'with_trashed' => (Auth::user()->level_id == 5 ? true : false),
        ];

        $response = $employer_compliance_interface->show($id, $request);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->employer_compliance = $response['result'];
        $this->title = $this->employer_compliance->title;
        $this->description = $this->employer_compliance->description;
    }

    public function submit(EmployerComplianceInterface $employer_compliance_interface)
    {
        $response = $employer_compliance_interface->update($this->employer_compliance->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.company-management.employer-compliance.index', 'close_modal', 'edit');
            $this->emitTo('hrim.company-management.employer-compliance.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 409) {
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.company-management.employer-compliance.edit');
    }
}
