<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\EmployerCompliance;

use App\Interfaces\Hrim\CompanyManagement\EmployerComplianceInterface;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Index extends Component
{
    use PopUpMessagesTrait;

    public $confirmation_modal = false;
    public $confirmation_message;
    public $view_modal = false;
    public $create_modal = false;
    public $edit_modal = false;
    public $action_type;
    public $employer_compliance_id;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'view') {
            $this->emit('employer_compliance_view_mount', $data['id']);
            $this->view_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emit('employer_compliance_edit_mount', $data['id']);
            $this->edit_modal = true;
        } else if ($action_type == 'confirmation_delete') {
            $this->confirmation_message = "Are you sure you want to delete this employer compliance?";
            $this->confirmation_modal = true;
        } else if ($action_type == 'confirmation_restore') {
            $this->confirmation_message = "Are you sure you want to restore this employer compliance?";
            $this->confirmation_modal = true;
        }

        $this->employer_compliance_id = $data['id'] ?? null;
    }

    public function confirm(EmployerComplianceInterface $employer_compliance_interface)
    {
        if ($this->action_type == 'confirmation_delete') {
            $response = $employer_compliance_interface->destroy($this->employer_compliance_id);
        } elseif ($this->action_type == 'confirmation_restore') {
            $response = $employer_compliance_interface->restore($this->employer_compliance_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.company-management.employer-compliance.index', 'close_modal', 'confirmation');
            $this->emitTo('hrim.company-management.employer-compliance.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'view') {
            $this->view_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } else if ($action_type == 'confirmation') {
            $this->confirmation_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render(EmployerComplianceInterface $employer_compliance_interface)
    {
        $request = [
            'with_trashed' => (Auth::user()->level_id == 5 ? true : false),
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $employer_compliance_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.company-management.employer-compliance.index', [
            'employer_compliances' => $response['result']
        ]);
    }
}
