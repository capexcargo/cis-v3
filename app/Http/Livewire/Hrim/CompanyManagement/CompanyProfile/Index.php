<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\CompanyProfile;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.hrim.company-management.company-profile.index');
    }
}
