<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\Bulletin\Memo;

use App\Interfaces\Hrim\CompanyManagement\Bulletin\MemoInterface;
use App\Models\Hrim\Memos;
use App\Traits\Hrim\CompanyManagement\Bulletin\MemoTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use MemoTrait, PopUpMessagesTrait, WithFileUploads;

    public $memo_no;
    public $title;
    public $details;

    protected $listeners = ['memo_edit_mount' => 'mount'];

    public function mount(MemoInterface $memo_interface, $id)
    {
        $this->resetForm();

        $response = $memo_interface->show($id);

        if ($response['code'] == 200) {
            $this->memo = $response['result'];
        } else {
            return $this->sweetAlertError('error', $response['message'], $response['result']);
        }

        $this->memo = Memos::find($id);

        $this->memo_no = $this->memo->memorandum_no;
        $this->title = $this->memo->title;
        $this->details = $this->memo->details;

        foreach ($this->memo->attachments as $attachment) {
            $this->attachments[] = [
                'id' => $attachment->id,
                'attachment' => '',
                'path' => $attachment->path,
                'name' => $attachment->name,
                'extension' => $attachment->extension,
                'is_deleted' => false,
            ];
        }
    }

    public function submit(MemoInterface $MemoInterface)
    {
        $validated = $this->validate([
            'title' => 'required',
            'details' => 'required',

            'attachments' => 'sometimes',
            // 'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),
        ], [
            // 'attachments.*.attachment.required' => 'The attachment field is required.',
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png,xlsx,doc,docx.',
        ]);

        $response = $MemoInterface->update($this->memo, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.company-management.bulletin.index', 'close_modal', 'edit');
            $this->emitTo('hrim.company-management.bulletin.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.company-management.bulletin.memo.edit');
    }
}
