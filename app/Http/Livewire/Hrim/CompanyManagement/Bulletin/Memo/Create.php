<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\Bulletin\Memo;

use App\Models\User;
use App\Repositories\Hrim\CompanyManagement\Bulletin\MemoRepository;
use App\Traits\Hrim\CompanyManagement\Bulletin\MemoTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use MemoTrait, PopUpMessagesTrait, WithFileUploads;

    protected $listeners = ['generate_memo_no' => 'generateMemoNo'];

    public function mount()
    {
        $this->addAttachments();
    }

    public function submit()
    {
        $validated = $this->validate([
            'memorandum_no' => 'required',
            // 'attention_to' => 'required',
            'title' => 'required',
            'details' => 'required',

            'attachments' => 'required',
            'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),

        ],[
            'attachments.*.attachment.required' => 'The attachment field is required.',
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
        ]);
        $memo_repository = new MemoRepository();
        $response = $memo_repository->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            // $this->addAttachments();
            $this->emitTo('hrim.company-management.bulletin.index', 'close_modal', 'create');
            $this->emitTo('hrim.company-management.bulletin.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.company-management.bulletin.memo.create', [
            'employees' => User::get()
        ]);
    }
}
