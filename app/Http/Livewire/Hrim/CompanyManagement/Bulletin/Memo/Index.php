<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\Bulletin\Memo;

use App\Models\Hrim\Memos;
use Livewire\Component;

class Index extends Component
{
    public $confirmation_modal = false;
    public $action_type;
    public $confirmation_message;
    
    public $create_memo_modal = false;
    public $edit_memo_modal = false;
    public $delete_memo_modal = false;
    public $view_memo_modal = false;
    
    public $memo_id;
    
    protected $listeners = ['memo_index' => 'render', 'close_modal' => 'closeModal'];
    
    public function action(array $data, $action_type)
    {
        if ($action_type == 'create') {
            $this->emit('generate_memo_no');
            $this->create_annountment_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('memo_edit_mount', $data['id']);
            $this->edit_memo_modal = true;
        } else if ($action_type == 'delete') {
            $this->emit('memo_delete_mount', $data['id']);
            $this->delete_memo_modal = true;
        }

        $this->memo_id = $data['id'] ?? null;
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_annountment_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_memo_modal = false;
        }
    }
    
    // public function render()
    // {
    //     return view('livewire.hrim.company-management.bulletin.memo.index', [
    //         'memos' => Memos::with('attachments')->paginate($this->paginate)
    //     ]);
    // }
}
