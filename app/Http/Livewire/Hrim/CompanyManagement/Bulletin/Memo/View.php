<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\Bulletin\Memo;

use App\Models\Hrim\Memos;
use Livewire\Component;

class View extends Component
{
    public $requests;

    protected $listeners = ['memo_view_mount' => 'mount'];

    public function mount($memorandum_no)
    {
        $this->reset([
            'requests',
        ]);

        $this->requests = Memos::with('attachments')->where('memorandum_no', $memorandum_no)->get();
    }

    public function render()
    {
        return view('livewire.hrim.company-management.bulletin.memo.view');
    }
}
