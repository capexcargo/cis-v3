<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\Bulletin\Announcement;

use App\Repositories\Hrim\CompanyManagement\Bulletin\AnnouncementRepository;
use App\Traits\Hrim\CompanyManagement\Bulletin\AnnouncementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use AnnouncementTrait, PopUpMessagesTrait, WithFileUploads;

    protected $listeners = ['generate_announcement_no' => 'generateAnnouncementNo'];

    public function mount()
    {
        $this->addAttachments();
    }

    public function submit()
    {

        $validated = $this->validate([
            'announcement_no' => 'required',
            'date_posted' => 'required',
            'title' => 'required',
            'details' => 'required',

            'attachments' => 'nullable',
            'attachments.*.attachment' => 'nullable|' . config('filesystems.validation_all'),
        ], [
            // 'attachments.*.attachment.required' => 'This attachment field is required.',
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
        ]);

        $announcement_repository = new AnnouncementRepository();
        $response = $announcement_repository->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            // $this->addAttachments();
            $this->emitTo('hrim.company-management.bulletin.index', 'close_modal', 'create');
            $this->emitTo('hrim.company-management.bulletin.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.company-management.bulletin.announcement.create');
    }
}
