<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\Bulletin\Announcement;

use App\Models\Hrim\Announcements;
use Livewire\Component;

class Index extends Component
{
    public $action_type;

    public $create_announcement_modal = false;
    public $edit_announcement_modal = false;
    public $delete_announcement_modal = false;
    public $view_announcement_modal = false;

    public $announcement_id;

    protected $listeners = ['announcement_index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        if ($action_type == 'create') {
            $this->emit('generate_announcement_no');
            $this->create_announcement_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emit('announcement_edit_mount', $data['id']);
            $this->edit_announcement_modal = true;
        } elseif ($action_type == 'delete') {
            $this->emit('announcement_delete_mount', $data['id']);
            $this->delete_announcement_modal = true;
        }

        $this->announcement_id = $data['id'] ?? null;
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_announcement_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_announcement_modal = false;
        }
    }

    // public function render()
    // {
    //     return view('livewire.hrim.company-management.bulletin.announcement.index', [
    //         'announcements' => Announcements::with('attachments')->paginate($this->paginate)
    //     ]);
    // }
}
