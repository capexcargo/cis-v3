<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\Bulletin\Announcement;

use App\Models\Hrim\Announcements;
use Livewire\Component;

class View extends Component
{
    public $requests;

    protected $listeners = ['announcement_view_mount' => 'mount'];

    public function mount($announcement_no)
    {
        $this->reset([
            'requests',
        ]);

        $this->requests = Announcements::with('attachments')->where('announcement_no', $announcement_no)->get();
    }

    public function render()
    {
        return view('livewire.hrim.company-management.bulletin.announcement.view');
    }
}
