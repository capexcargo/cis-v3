<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\Bulletin\Announcement;

use App\Interfaces\Hrim\CompanyManagement\Bulletin\AnnouncementInterface;
use App\Models\Hrim\Announcements;
use App\Traits\Hrim\CompanyManagement\Bulletin\AnnouncementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use AnnouncementTrait, PopUpMessagesTrait, WithFileUploads;

    public $announcement_no;
    public $title;
    public $details;

    protected $listeners = ['announcement_edit_mount' => 'mount'];

    public function mount(AnnouncementInterface $announcement_interface, $id)
    {
        $this->resetForm();

        $response = $announcement_interface->show($id);
      
        if ($response['code'] == 200) {
            $this->announcement = $response['result'];
        } else {
           return $this->sweetAlertError('error', $response['message'], $response['result']);
        }

        $this->announcement = Announcements::find($id);

        $this->announcement_no = $this->announcement->announcement_no;
        $this->title = $this->announcement->title;
        $this->details = $this->announcement->details;

        foreach ($this->announcement->attachments as $attachment) {
            $this->attachments[] = [
                'id' => $attachment->id,
                'attachment' => '',
                'path' => $attachment->path,
                'name' => $attachment->name,
                'extension' => $attachment->extension,
                'is_deleted' => false,
            ];
        }
  
    }

    public function submit(AnnouncementInterface $AnnouncementInterface)
    {
        $validated = $this->validate([
            'title' => 'required',
            'details' => 'required',
            
            'attachments' => 'nullable',
            'attachments.*.attachment' => 'nullable|' . config('filesystems.validation_all'),
        ], [
            // 'attachments.*.attachment.required' => 'This attachment field is required.',
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
        ]);


        $response = $AnnouncementInterface->update($this->announcement, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.company-management.bulletin.index', 'close_modal', 'edit');
            $this->emitTo('hrim.company-management.bulletin.index', 'index');            
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.company-management.bulletin.announcement.edit');
    }
}
