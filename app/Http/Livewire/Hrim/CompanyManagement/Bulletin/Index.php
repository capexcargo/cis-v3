<?php

namespace App\Http\Livewire\Hrim\CompanyManagement\Bulletin;

use App\Interfaces\Hrim\CompanyManagement\Bulletin\AnnouncementInterface;
use App\Interfaces\Hrim\CompanyManagement\Bulletin\MemoInterface;
use App\Models\Hrim\Announcements;
use App\Models\Hrim\Memos;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait, WithFileUploads;

    public $confirmation_modal = false;
    public $confirmation_message;

    public $action_type;
    public $confirmation_message_ann;
    public $confirmation_message_memo;
    public $confirmation_message_publish;

    public $create_announcement_modal = false;
    public $edit_announcement_modal = false;
    public $view_announcement_modal = false;
    public $publish_announcement_modal = false;
    public $delete_announcement_modal = false;

    public $create_memo_modal = false;
    public $edit_memo_modal = false;
    public $view_memo_modal = false;
    public $delete_memo_modal = false;

    public $announcement_id;
    public $memo_id;
    public $announcement_no;
    public $memorandum_no;

    public $sortField = 'created_at';
    public $sortAsc = false;

    public $paginateAnnouncement = 5;
    public $paginateMemo = 5;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'action_announcement' => 'action'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_announcement') {
            $this->emit('generate_announcement_no');
            $this->create_announcement_modal = true;
        } else if ($action_type == 'edit_announcement') {
            $this->emit('announcement_edit_mount', $data['id']);
            $this->edit_announcement_modal = true;
            $this->announcement_id = $data['id'];
        } else if ($action_type == 'view_announcement') {
            $this->emit('announcement_view_mount', $data['announcement_no']);
            $this->view_announcement_modal = true;
            $this->announcement_no = $data['announcement_no'];
        } else if ($action_type == 'delete_announcement') {
            $this->confirmation_message = "Are you sure you want to delete this announcement?";
            $this->confirmation_modal = true;
            $this->announcement_id = $data['id'];
        } else if ($action_type == 'create_memo') {
            $this->emit('generate_memo_no');
            $this->create_memo_modal = true;
        } else if ($action_type == 'edit_memo') {
            $this->emit('memo_edit_mount', $data['id']);
            $this->edit_memo_modal = true;
            $this->memo_id = $data['id'];
        } else if ($action_type == 'view_memo') {
            $this->emit('memo_view_mount', $data['memorandum_no']);
            $this->view_memo_modal = true;
            $this->memorandum_no = $data['memorandum_no'];
        } else if ($action_type == 'delete_memo') {
            $this->confirmation_message = "Are you sure you want to delete this memo?";
            $this->confirmation_modal = true;
            $this->memo_id = $data['id'];
        }
    }


    public function confirm(AnnouncementInterface $announcementInterface, MemoInterface $memoInterface)
    {
        if ($this->action_type == "delete_announcement") {
            $response = $announcementInterface->destroy($this->announcement_id);
            $this->emitTo('hrim.company-management.bulletin.index', 'close_modal', 'delete');
            $this->emitTo('hrim.company-management.bulletin.index', 'index');
        } else if ($this->action_type == "delete_memo") {
            $response = $memoInterface->destroy($this->memo_id);
            $this->emitTo('hrim.company-management.bulletin.index', 'close_modal', 'delete');
            $this->emitTo('hrim.company-management.bulletin.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.company-management.bulletin.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_announcement_modal = false;
            $this->create_memo_modal = false;
            $this->confirmation_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_announcement_modal = false;
            $this->edit_memo_modal = false;
        } else if ($action_type == 'view') {
            $this->view_announcement_modal = false;
            $this->view_memo_modal = false;
        } else if ($action_type == 'delete') {
            $this->confirmation_modal = false;
        }
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.hrim.company-management.bulletin.index', [
            'announcements' => Announcements::with('attachments')->when($this->sortField, function ($query) {
                $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
            })->paginate($this->paginateAnnouncement, ['*'], 'announcementPage'),

            'memos' => Memos::with('attachments')->when($this->sortField, function ($query) {
                $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
            })->paginate($this->paginateMemo, ['*'], 'memoPage')
        ]);
    }
}
