<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\Onboarding;

use App\Models\Hrim\Onboarding;
use App\Models\Hrim\OnboardingStatusMgmt;
use App\Traits\Hrim\RecruitmentAndHiring\OnboardingTrait;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use OnboardingTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public function load()
    {
        $this->loadHeadCards();
        $this->loadStatusHeaderCards();
    }

    public function loadHeadCards()
    {
        $onboarding = OnboardingStatusMgmt::withCount('onboardings')->get();

        $this->header_cards = [
            [
                'title' => 'ONBOARDING',
                'value' => $onboarding[0]->id ?? 0 ?? 0 <= 4 || $onboarding[1]->id ?? 0 <= 4 || $onboarding[2]->id ?? 0 <= 4 ? ($onboarding[0]->onboardings_count ?? 0 + $onboarding[1]->onboardings_count ?? 0  + $onboarding[2]->onboardings_count ?? 0) : 0,
                'color' => 'text-blue',
                'action' => 'onboarding_status',
                'id' => 1,
            ],
            [
                'title' => 'ONBOARDED',
                'value' => $onboarding[3]->id ?? 0 ?? 0 == 4 ? $onboarding[3]->onboardings_count ?? 0 : 0,
                'color' => 'text-blue',
                'action' => 'onboarding_status',
                'id' => 4,
            ]
        ];
    }

    public function loadStatusHeaderCards()
    {
        $status = OnboardingStatusMgmt::withCount('onboardings')->get();
        // dd($status);
        $this->status_header_cards = [
            [
                'title' => 'All',
                'value' => $status->sum('onboardings_count'),
                'class' => 'bg-blue text-white border border-gray-600 shadow-sm rounded-l-md',
                'color' => 'text-blue',
                'action' => 'onboarding_status_headers',
                'id' => false
            ],
            [
                'title' => 'Preboarding',
                'value' => $status[0]->onboardings_count ?? 0,
                'class' => 'bg-gray-100 border border-gray-600 shadow-sm text-gray-500 ',
                'color' => 'text-blue',
                'action' => 'onboarding_status_headers',
                'id' => 1
            ],
            [
                'title' => 'Admin & Provisioning',
                'value' => $status[1]->onboardings_count ?? 0,
                'class' => 'bg-gray-100 border border-gray-600 shadow-sm text-gray-500',
                'color' => 'text-blue',
                'action' => 'onboarding_status_headers',
                'id' => 2
            ],
            [
                'title' => 'Orientation & Training',
                'value' => $status[2]->onboardings_count ?? 0,
                'class' => 'bg-gray-100 border border-gray-600 shadow-sm text-gray-500',
                'color' => 'text-blue',
                'action' => 'onboarding_status_headers',
                'id' => 3
            ],
            [
                'title' => 'Onboarding Evaluation',
                'value' => $status[3]->onboardings_count ?? 0,
                'class' => 'bg-gray-100 border border-gray-600 shadow-sm text-gray-500 rounded-r-md',
                'color' => 'text-blue',
                'action' => 'onboarding_status_headers',
                'id' => 4
            ],

        ];
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'edit_onboarding') {
            $this->emit('onboarding_edit_mount', $data['id']);
            $this->edit_onboarding_modal = true;
            $this->onboarding_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'edit') {
            $this->edit_onboarding_modal = false;
        }
    }

    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.onboarding.index', [
            'onboardings' => Onboarding::with('erfReference', 'onboardingStatus')
                ->when($this->fullname, function ($query) {
                    $query->orWhereRaw("CONCAT(`firstname`, ' ', `middlename`, ' ', `lastname`) LIKE ?", ['%' . $this->fullname . '%' . '%']);
                    $query->orWhere('firstname', 'like', '%' . $this->fullname . '%');
                    $query->orWhere('middlename', 'like', '%' . $this->fullname . '%');
                    $query->orWhere('lastname', 'like', '%' . $this->fullname . '%');
                })
                ->whereHas('onboardingStatus', function ($query) {
                    $query->when($this->onboarding_status, function ($query) {
                        $query->where('status_id', $this->onboarding_status);
                    });
                })
                ->whereHas('onboardingStatus', function ($query) {
                    $query->when($this->onboarding_status_headers, function ($query) {
                        $query->where('status_id', $this->onboarding_status_headers);
                    });
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'desc' : 'asc');
                })->paginate($this->paginate)

        ]);
    }
}
