<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\Onboarding;

use App\Interfaces\Hrim\RecruitmentAndHiring\OnboardingInterface;
use App\Models\Hrim\ApplicantTracking;
use App\Models\Hrim\EmployeeRequisition;
use App\Models\Hrim\Onboarding;
use App\Traits\Hrim\RecruitmentAndHiring\OnboardingTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use OnboardingTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['onboarding_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->onboarding = Onboarding::findOrFail($id);
        $this->emp_requisition = EmployeeRequisition::where('id', '=', $this->onboarding->erf_reference_no_id)->get();

        foreach ($this->emp_requisition as $requisition) {
            $this->erf_reference_no_id = $requisition->id;

            $this->app_tracks = ApplicantTracking::where('erf_reference_no_id', '=', $requisition->id)->get();

            foreach ($this->app_tracks as $app_track) {
                $this->final_remarks = $app_track->remarks;
            }
        }


        $this->onboarding_status = $this->onboarding->status_id;
    }

    public function submit(OnboardingInterface $onboarding_interface)
    {
        $validated = $this->validate([
            'onboarding_status' => 'required',
        ]);

        // dd($this->final_remarks);

        $response = $onboarding_interface->update($this->onboarding, $this->erf_reference_no_id, $this->final_remarks, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.recruitment-and-hiring.onboarding.index', 'close_modal', 'edit');
            $this->emitTo('hrim.recruitment-and-hiring.onboarding.index', 'load_header_cards');
            $this->emitTo('hrim.recruitment-and-hiring.onboarding.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            "onboarding_status",
        ]);
    }

    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.onboarding.edit');
    }
}
