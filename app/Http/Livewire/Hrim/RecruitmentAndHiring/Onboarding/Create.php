<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\Onboarding;

use Livewire\Component;

class Create extends Component
{
    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.onboarding.create');
    }
}
