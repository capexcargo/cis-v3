<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\EmploymentCategoryType;

use App\Interfaces\Hrim\RecruitmentAndHiring\EmploymentCategoryTypeInterface;
use App\Traits\Hrim\RecruitmentAndHiring\EmploymentCategoryTypeTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use EmploymentCategoryTypeTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];


    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_emp_cat_type') {
            $this->create_modal = true;
        }elseif ($action_type == 'edit') {
            $this->emitTo('hrim.recruitment-and-hiring.employment-category-type.edit', 'edit', $data['id']);
            $this->emp_cat_type_id = $data['id'];
            $this->edit_modal = true;
            
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this Employment Category Type?";
            $this->delete_modal = true;
            $this->emp_cat_type_id = $data['id'];
        }
    }


    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        }else if ($action_type == 'edit') {
            $this->edit_modal = false;
        } elseif ($action_type == 'delete') {
            $this->delete_modal = false;
        }
    }

    public function confirm(EmploymentCategoryTypeInterface $employment_category_type_interface)
    {
        if ($this->action_type == "delete") {
            $response = $employment_category_type_interface->destroy($this->emp_cat_type_id);
            $this->emitTo('hrim.recruitment-and-hiring.employment-category-type.index', 'close_modal', 'delete');
            $this->emitTo('hrim.recruitment-and-hiring.employment-category-type.index', 'index');
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.recruitment-and-hiring.employment-category-type.index', 'close_modal', 'delete');
            $this->emitTo('hrim.recruitment-and-hiring.employment-category-type.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(EmploymentCategoryTypeInterface $employment_category_type_interface)
    {
        $request = [
            'paginate' => $this->paginate,
        ];

        // dd($request);
        $response = $employment_category_type_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'employment_category_types' => [],
                ];
        }
        // dd($response['result']['employment_category_types']);

        return view('livewire.hrim.recruitment-and-hiring.employment-category-type.index', [
            'employment_category_types' => $response['result']['employment_category_types']
        ]);
    }
}
