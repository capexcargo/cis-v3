<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\EmploymentCategoryType;

use App\Interfaces\Hrim\RecruitmentAndHiring\EmploymentCategoryTypeInterface;
use App\Models\Hrim\EmploymentCategoryType;
use App\Traits\Hrim\RecruitmentAndHiring\EmploymentCategoryTypeTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use EmploymentCategoryTypeTrait, PopUpMessagesTrait;

    protected $listeners = ['edit' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $this->resetForm();
        $this->employment_category_type = EmploymentCategoryType::findOrFail($id);

        $this->category_type = $this->employment_category_type->display;
        $this->category = $this->employment_category_type->employment_category_id;
    }

    public function submit(EmploymentCategoryTypeInterface $employment_category_type_interface)
    {
        $response = $employment_category_type_interface->update($this->getRequest(), $this->employment_category_type->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.recruitment-and-hiring.employment-category-type.index', 'close_modal', 'edit');
            $this->emitTo('hrim.recruitment-and-hiring.employment-category-type.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.employment-category-type.edit');
    }
}
