<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\EmploymentCategoryType;

use App\Interfaces\Hrim\RecruitmentAndHiring\EmploymentCategoryTypeInterface;
use App\Traits\Hrim\RecruitmentAndHiring\EmploymentCategoryTypeTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use EmploymentCategoryTypeTrait, PopUpMessagesTrait;

    protected $rules = [
        'category' => 'required',
        'category_type' => 'required',
    ];

    public function confirmationSubmit()
    {
        $this->validate();

        $this->confirmation_modal = true;
    }

    public function closecreatemodal()
    {
        $this->resetForm();
        $this->resetErrorBag();
        $this->emitTo('hrim.recruitment-and-hiring.employment-category-type.index', 'close_modal', 'create');
        $this->confirmation_modal = false;
    }


    
    public function submit(EmploymentCategoryTypeInterface $employment_category_type_interface)
    {
        // dd($this->getRequest());
        
        $response = $employment_category_type_interface->create($this->getRequest());
        // dd($response['code']);
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.recruitment-and-hiring.employment-category-type.index', 'close_modal', 'create');
            $this->emitTo('hrim.recruitment-and-hiring.employment-category-type.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.employment-category-type.create');
    }
}
