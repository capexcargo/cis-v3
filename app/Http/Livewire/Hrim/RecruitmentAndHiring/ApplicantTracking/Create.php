<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\ApplicantTracking;

use App\Repositories\Hrim\RecruitmentAndHiring\ApplicantTrackingRepository;
use App\Traits\Hrim\RecruitmentAndHiring\ApplicantTrackingTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Create extends Component
{
    use ApplicantTrackingTrait, WithPagination, PopUpMessagesTrait, WithFileUploads;


    protected $listeners = ['generate_erf_reference' => 'generateErfReference'];

    public function mount()
    {
        $this->addAttachments();
    }

    public function submit()
    {
        $validated = $this->validate([
            'erf_reference' => 'required',
            'first_name' => 'required',
            'middle_name' => 'sometimes',
            'last_name' => 'required',
            'position' => 'required',
            'branch' => 'required',
            'date_applied' => 'required',
            'requesting_manager' => 'required',
            'requesting_division' => 'required',
            'hr_notes' => 'sometimes',
            'hiring_manager_notes' => 'sometimes',
            'gm_segs_notes' => 'sometimes',
            'status_of_application' => 'required',
            'final_remarks' => 'sometimes',

            'attachments' => 'required',
            'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),
        ], [
            'attachments.*.attachment.required' => 'This attachment field is required.',
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
        ]);
        
        $applicant_tracking = new ApplicantTrackingRepository();
        $response = $applicant_tracking->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.recruitment-and-hiring.applicant-tracking.index', 'close_modal', 'create');
            $this->emitTo('hrim.recruitment-and-hiring.applicant-tracking.index', 'load_header_cards');
            $this->emitTo('hrim.recruitment-and-hiring.applicant-tracking.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.applicant-tracking.create');
    }
}
