<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\ApplicantTracking;

use App\Interfaces\Hrim\RecruitmentAndHiring\ApplicantTrackingInterface;
use App\Models\Hrim\ApplicantTracking;
use App\Models\Hrim\EmployeeRequisition;
use App\Traits\Hrim\RecruitmentAndHiring\ApplicantTrackingTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Edit extends Component
{
    use ApplicantTrackingTrait, PopUpMessagesTrait, WithFileUploads;

    protected $listeners = ['applicant_tracking_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->applicant_tracking = ApplicantTracking::findOrFail($id);
        $this->emp_requisition = EmployeeRequisition::where('id', '=', $this->applicant_tracking->erf_reference_no_id)->get();

        foreach ($this->emp_requisition as $requisition) {
            $this->erf_reference = $requisition->erf_reference_no;
            $this->erf_reference_no_id = $requisition->id;
            $this->position = $requisition->position_id;
            $this->branch = $requisition->branch_id;
            $this->requesting_manager = $requisition->created_by;
            $this->requesting_division = $requisition->division_id;
        }

        $this->first_name = $this->applicant_tracking->firstname;
        $this->middle_name = $this->applicant_tracking->middlename;
        $this->last_name = $this->applicant_tracking->lastname;
        $this->date_applied = $this->applicant_tracking->date_applied;
        $this->hr_notes = $this->applicant_tracking->hr_notes;
        $this->hiring_manager_notes = $this->applicant_tracking->hiring_manager_notes;
        $this->gm_segs_notes = $this->applicant_tracking->gm_notes;
        $this->status_of_application = $this->applicant_tracking->status_id;
        $this->final_remarks = $this->applicant_tracking->remarks;

        foreach ($this->applicant_tracking->attachments as $attachment) {
            $this->attachments[] = [
                'id' => $attachment->id,
                'attachment' => '',
                'path' => $attachment->path,
                'name' => $attachment->name,
                'extension' => $attachment->extension,
                'is_deleted' => false,
            ];
        }
    }

    public function submit(ApplicantTrackingInterface $applicant_tracking_interface)
    {
        $validated = $this->validate([
            'erf_reference' => 'required',
            'erf_reference_no_id' => 'required',
            'first_name' => 'required',
            'middle_name' => 'sometimes',
            'last_name' => 'required',
            'position' => 'required',
            'branch' => 'required',
            'date_applied' => 'required',
            'requesting_manager' => 'required',
            'requesting_division' => 'required',
            'hr_notes' => 'sometimes',
            'hiring_manager_notes' => 'sometimes',
            'gm_segs_notes' => 'sometimes',
            'status_of_application' => 'required',
            'final_remarks' => 'sometimes',

            'attachments' => 'required',
            // 'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),
        ], [
            'attachments.*.attachment.required' => 'This attachment field is required.',
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
        ]);


        $response = $applicant_tracking_interface->update($this->applicant_tracking, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.recruitment-and-hiring.applicant-tracking.index', 'close_modal', 'edit');
            $this->emitTo('hrim.recruitment-and-hiring.applicant-tracking.index', 'load_header_cards');
            $this->emitTo('hrim.recruitment-and-hiring.applicant-tracking.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.applicant-tracking.edit');
    }
}
