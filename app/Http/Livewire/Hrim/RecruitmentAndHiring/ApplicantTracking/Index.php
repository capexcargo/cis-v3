<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\ApplicantTracking;

use App\Models\Division;
use App\Models\Hrim\ApplicantTracking;
use App\Models\Hrim\EmployeeRequisition;
use App\Traits\Hrim\RecruitmentAndHiring\ApplicantTrackingTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use ApplicantTrackingTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public function load()
    {
        $this->loadHeadCards();
    }

    public function loadHeadCards()
    {

        $applicant_tracks = Division::withCount('applicant_trackings')->get();

        $this->header_cards = [
            [
                'title' => 'All',
                'value' => $applicant_tracks->sum('applicant_trackings_count'),
                'color' => 'text-blue',
                'action' => 'division',
                'id' => null
            ],

        ];

        foreach ($applicant_tracks as $count) {
            $this->header_cards[] = [
                'title' => $count->name,
                'value' => $count->applicant_trackings_count,
                'color' => 'text-blue',
                'action' => 'division',
                'id' => $count->id
            ];
        }
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_applicant_tracking') {
            $this->emit('generate_erf_reference');
            $this->create_applicant_tracking_modal = true;
        } else if ($action_type == 'edit_applicant_tracking') {
            $this->emit('applicant_tracking_edit_mount', $data['id']);
            $this->edit_applicant_tracking_modal = true;
            $this->applicant_tracking_id = $data['id'];
        } else if ($action_type == 'view_applicant_tracking') {
            $this->view_applicant_tracking_modal = true;
            $this->applicant_tracking_id = $data['id'];
        } else if ($action_type == 'view_applicant_tracking_notes') {
            $this->view_applicant_tracking_notes_modal = true;
            $this->applicant_tracking_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_applicant_tracking_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_applicant_tracking_modal = false;
        } else if ($action_type == 'view') {
            $this->view_applicant_tracking_modal = false;
        } else if ($action_type == 'view_notes') {
            $this->view_applicant_tracking_notes_modal = false;
        }
    }

    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.applicant-tracking.index', [
            'applicant_trackings' => ApplicantTracking::with('erfReference', 'applicantStatus')
                ->when($this->fullname, function ($query) {
                    $query->whereRaw("CONCAT(`firstname`, ' ', `middlename`, ' ', `lastname`) LIKE ?", ['%' . $this->fullname . '%' . '%']);
                    $query->orWhereRaw("CONCAT(`firstname`, ' ', `lastname`) LIKE ?", ['%' . $this->fullname . '%' . '%']);
                    $query->orWhere('firstname', 'like', '%' . $this->fullname . '%');
                    $query->orWhere('middlename', 'like', '%' . $this->fullname . '%');
                    $query->orWhere('lastname', 'like', '%' . $this->fullname . '%');
                })
                ->whereHas('erfReference', function ($query) {
                    $query->when($this->position, function ($query) {
                        $query->where('position_id', $this->position);
                    })->when($this->branch, function ($query) {
                        $query->where('branch_id', $this->branch);
                    })->when($this->status_of_application, function ($query) {
                        $query->where('status_id', $this->status_of_application);
                    })
                        ->when($this->division, function ($query) {
                            $query->where('division_id', $this->division);
                        });
                })
                ->when($this->date_applied, function ($query) {
                    $query->whereDate('date_applied', $this->date_applied);
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
