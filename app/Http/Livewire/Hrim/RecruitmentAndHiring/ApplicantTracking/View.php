<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\ApplicantTracking;

use App\Models\Hrim\ApplicantTracking;
use Livewire\Component;

class View extends Component
{
    public $requests;

    protected $listeners = ['view' => 'mount'];

    public function mount($id)
    {
        $this->reset([
            'requests',
        ]);

        $this->requests = ApplicantTracking::with('attachments')->where('id', $id)->get();
    }

    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.applicant-tracking.view');
    }
}
