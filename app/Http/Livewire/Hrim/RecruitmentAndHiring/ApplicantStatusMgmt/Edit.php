<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\ApplicantStatusMgmt;

use App\Interfaces\Hrim\RecruitmentAndHiring\ApplicantStatusMgmtInterface;
use App\Models\Hrim\ApplicantStatusMgmt;
use App\Traits\Hrim\RecruitmentAndHiring\ApplicantStatusMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use ApplicantStatusMgmtTrait, PopUpMessagesTrait;

    public $display;

    public $applicant_status_mgmt;

    protected $listeners = ['applicant_status_mgmt_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->applicant_status_mgmt = ApplicantStatusMgmt::findOrFail($id);

        // $this->code = $this->applicant_status->code;
        $this->applicant_status = $this->applicant_status_mgmt->display;
    }

    public function submit(ApplicantStatusMgmtInterface $applicantStatusMgmtInterface)
    {
        $validated = $this->validate([
            // 'code' => 'required',
            'applicant_status' => 'required'
        ]);

        $response = $applicantStatusMgmtInterface->update($this->applicant_status_mgmt, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.recruitment-and-hiring.applicant-status-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('hrim.recruitment-and-hiring.applicant-status-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function resetForm()
    {
        $this->reset([
            // "code",
            "display",
        ]);
    }
    
    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.applicant-status-mgmt.edit');
    }
}
