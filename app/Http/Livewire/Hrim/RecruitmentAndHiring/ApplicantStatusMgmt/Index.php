<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\ApplicantStatusMgmt;

use App\Interfaces\Hrim\RecruitmentAndHiring\ApplicantStatusMgmtInterface;
use App\Models\Hrim\ApplicantStatusMgmt;
use App\Traits\Hrim\RecruitmentAndHiring\ApplicantStatusMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use ApplicantStatusMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_applicant_status_mgmt') {
            $this->create_applicant_status_mgmt_modal = true;
        } else if ($action_type == 'edit_applicant_status_mgmt') {
            $this->emit('applicant_status_mgmt_edit_mount', $data['id']);
            $this->edit_applicant_status_mgmt_modal = true;
            $this->applicant_status_mgmt_id = $data['id'];
        } else if ($action_type == 'delete_applicant_status_mgmt') {
            $this->confirmation_message = "Are you sure you want to delete this Applicant Status?";
            $this->delete_applicant_status_mgmt_modal = true;
            $this->department_id = $data['id'];
        }
    }

    
    public function deleteApplicantStatus(ApplicantStatusMgmtInterface $applicantStatusMgmtInterface)
    {
        if ($this->action_type == 'delete_applicant_status_mgmt') {
            $response = $applicantStatusMgmtInterface->destroy($this->department_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.recruitment-and-hiring.applicant-status-mgmt.index', 'close_modal', 'delete');
            $this->emitTo('hrim.recruitment-and-hiring.applicant-status-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_applicant_status_mgmt_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_applicant_status_mgmt_modal = false;
        } else if ($action_type == 'delete') {
            $this->delete_applicant_status_mgmt_modal = false;
        }
    }

    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.applicant-status-mgmt.index', [
            'applicant_status_mgmts' => ApplicantStatusMgmt::when($this->sortField, function ($query) {
                $query->orderBy($this->sortField, $this->sortAsc ? 'desc' : 'asc');
            })->paginate($this->paginate)

        ]);
    }
}
