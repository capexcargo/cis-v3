<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\ApplicantStatusMgmt;

use App\Repositories\Hrim\RecruitmentAndHiring\ApplicantStatusMgmtRepository;
use App\Traits\Hrim\RecruitmentAndHiring\ApplicantStatusMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use ApplicantStatusMgmtTrait, PopUpMessagesTrait;

    public function submit()
    {
        $validated = $this->validate([
            'applicant_status' => 'required',
        ]);

        $applicant_status_repository = new ApplicantStatusMgmtRepository();
        $response = $applicant_status_repository->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.recruitment-and-hiring.applicant-status-mgmt.index', 'close_modal', 'create');
            $this->emitTo('hrim.recruitment-and-hiring.applicant-status-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.applicant-status-mgmt.create');
    }
}
