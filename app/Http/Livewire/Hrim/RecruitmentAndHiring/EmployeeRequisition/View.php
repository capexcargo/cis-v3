<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\EmployeeRequisition;

use App\Models\Hrim\EmployeeRequisition;
use Livewire\Component;

class View extends Component
{
    public $requests;

    protected $listeners = ['employee_requisition_view_mount' => 'mount'];

    public function mount($id)
    {
        $this->reset([
            'requests',
        ]);

        $this->requests = EmployeeRequisition::with('attachments')->where('id', $id)->get();
    }
    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.employee-requisition.view');
    }
}
