<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\EmployeeRequisition;

use App\Interfaces\Hrim\RecruitmentAndHiring\EmployeeRequisitionInterface;
use App\Models\Division;
use App\Models\Hrim\EmployeeRequisition;
use App\Models\Hrim\StatusReference;
use App\Traits\Hrim\RecruitmentAndHiring\EmloyeeRequisitionTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use EmloyeeRequisitionTrait, WithPagination, PopUpMessagesTrait;

    public $position_search;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'load_header_cards' => 'load'];

    public function load()
    {
        $this->loadHeadCards();
        $this->loadStatusHeaderCards();
    }

    public function loadHeadCards()
    {
        $divisions = Division::withCount('employee_requisitions')->get();

        $this->header_cards = [
            [
                'title' => 'All',
                'value' => $divisions->sum('employee_requisitions_count'),
                'color' => 'text-blue',
                'action' => 'division',
                'id' => null
            ],

        ];

        foreach ($divisions as $count) {
            $this->header_cards[] = [
                'title' => $count->name,
                'value' => $count->employee_requisitions_count,
                'color' => 'text-blue',
                'action' => 'division',
                'id' => $count->id
            ];
        }
    }

    public function loadStatusHeaderCards()
    {
        $status = StatusReference::withCount('employee_requisitions')->get();

        $this->status_header_cards = [
            [
                'title' => 'All',
                'value' => $status->sum('employee_requisitions_count'),
                'class' => 'bg-gray-100 text-gray-500 border border-gray-600 shadow-sm rounded-l-md',
                'color' => 'text-blue',
                'action' => 'final_status',
                'id' => false
            ],
            [
                'title' => 'For Approval',
                'value' => $status[0]->employee_requisitions_count + $status[1]->employee_requisitions_count,
                'class' => 'bg-gray-100 border border-gray-600 shadow-sm text-gray-500 ',
                'color' => 'text-blue',
                'action' => 'final_status',
                'id' => 2
            ],
            [
                'title' => 'Approved',
                'value' => $status[2]->employee_requisitions_count,
                'class' => 'bg-gray-100 border border-gray-600 shadow-sm text-gray-500',
                'color' => 'text-blue',
                'action' => 'final_status',
                'id' => 3
            ],
            [
                'title' => 'Declined',
                'value' => $status[3]->employee_requisitions_count,
                'class' => 'bg-gray-100 border border-gray-600 shadow-sm text-gray-500 rounded-r-md',
                'color' => 'text-blue',
                'action' => 'final_status',
                'id' => 4
            ],

        ];
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'approve_employee_requisition') {
            $this->confirmation_message = "Are you sure you want to approve this request?";
            $this->confirmation_modal = true;
            $this->employee_requisition_id = $data['id'];
        } else if ($action_type == 'decline_employee_requisition') {
            $this->confirmation_message = "Are you sure you want to decline this request?";
            $this->confirmation_modal = true;
            $this->employee_requisition_id = $data['id'];
        } else if ($action_type == 'view_employee_requisition') {
            $this->emit('employee_requisition_view_mount', $data['id']);
            $this->view_employee_requisition_modal = true;
            $this->employee_requisition_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'approval') {
            $this->confirmation_modal = false;
        }
    }

    public function confirm(EmployeeRequisitionInterface $employee_requisition_interface)
    {
        if ($this->action_type == 'approve_employee_requisition') {
            $response = $employee_requisition_interface->approve($this->employee_requisition_id);
        } else if ($this->action_type == 'decline_employee_requisition') {
            $response = $employee_requisition_interface->decline($this->employee_requisition_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.recruitment-and-hiring.employee-requisition.index', 'close_modal', 'approval');
            $this->emitTo('hrim.recruitment-and-hiring.employee-requisition.index', 'load_header_cards');
            $this->emitTo('hrim.recruitment-and-hiring.employee-requisition.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(EmployeeRequisitionInterface $employee_requisition_interface)
    {
        $request = [
            'position_search' => $this->position_search,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
            'final_status' => $this->final_status,
            'branch' => $this->branch,
            'division' => $this->division,
            'request_reason' => $this->request_reason,
        ];

        $response = $employee_requisition_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'employee_requisitions' => [],
                ];
        }
        return view(
            'livewire.hrim.recruitment-and-hiring.employee-requisition.index',
            [
                'employee_requisitions' => $response['result']['employee_requisitions']
            ]
        );
    }
}
