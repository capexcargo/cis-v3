<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\OnboardingStatusMgmt;

use App\Interfaces\Hrim\RecruitmentAndHiring\OnboardingStatusMgmtInterface;
use App\Models\Hrim\OnboardingStatusMgmt;
use App\Traits\Hrim\RecruitmentAndHiring\OnboardingStatusMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use OnboardingStatusMgmtTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_onboarding_status_mgmt') {
            $this->create_onboarding_status_mgmt_modal = true;
        } else if ($action_type == 'edit_onboarding_status_mgmt') {
            $this->emit('onboarding_status_mgmt_edit_mount', $data['id']);
            $this->edit_onboarding_status_mgmt_modal = true;
            $this->onboarding_status_mgmt_id = $data['id'];
        } else if ($action_type == 'delete_onboarding_status_mgmt') {
            $this->confirmation_message = "Are you sure you want to delete this Onboarding Status?";
            $this->delete_onboarding_status_mgmt_modal = true;
            $this->department_id = $data['id'];
        }
    }


    public function deleteOnboardingStatus(OnboardingStatusMgmtInterface $onboardingStatusMgmtInterface)
    {
        if ($this->action_type == 'delete_onboarding_status_mgmt') {
            $response = $onboardingStatusMgmtInterface->destroy($this->department_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.recruitment-and-hiring.onboarding-status-mgmt.index', 'close_modal', 'delete');
            $this->emitTo('hrim.recruitment-and-hiring.onboarding-status-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_onboarding_status_mgmt_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_onboarding_status_mgmt_modal = false;
        } else if ($action_type == 'delete') {
            $this->delete_onboarding_status_mgmt_modal = false;
        }
    }

    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.onboarding-status-mgmt.index', [
            'onboarding_status_mgmts' => OnboardingStatusMgmt::when($this->sortField, function ($query) {
                $query->orderBy($this->sortField, $this->sortAsc ? 'desc' : 'asc');
            })->paginate($this->paginate)

        ]);
    }
}
