<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\OnboardingStatusMgmt;

use App\Interfaces\Hrim\RecruitmentAndHiring\OnboardingStatusMgmtInterface;
use App\Models\Hrim\OnboardingStatusMgmt;
use App\Traits\Hrim\RecruitmentAndHiring\OnboardingStatusMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use OnboardingStatusMgmtTrait, PopUpMessagesTrait;

    public $display;

    public $onboarding_status_mgmt;

    protected $listeners = ['onboarding_status_mgmt_edit_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->onboarding_status_mgmt = OnboardingStatusMgmt::findOrFail($id);

        $this->onboarding_status = $this->onboarding_status_mgmt->display;
        $this->description = $this->onboarding_status_mgmt->description;
    }

    public function submit(OnboardingStatusMgmtInterface $onboardingStatusMgmtInterface)
    {
        $validated = $this->validate([
            // 'code' => 'required',
            'onboarding_status' => 'required',
            'description' => 'required'
        ]);

        $response = $onboardingStatusMgmtInterface->update($this->onboarding_status_mgmt, $validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.recruitment-and-hiring.onboarding-status-mgmt.index', 'close_modal', 'edit');
            $this->emitTo('hrim.recruitment-and-hiring.onboarding-status-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.onboarding-status-mgmt.edit');
    }
}
