<?php

namespace App\Http\Livewire\Hrim\RecruitmentAndHiring\OnboardingStatusMgmt;

use App\Repositories\Hrim\RecruitmentAndHiring\OnboardingStatusMgmtRepository;
use App\Traits\Hrim\RecruitmentAndHiring\OnboardingStatusMgmtTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use OnboardingStatusMgmtTrait, PopUpMessagesTrait;

    public function submit()
    {
        $validated = $this->validate([
            'onboarding_status' => 'required',
            'description' => 'required',
        ]);

        $onboarding_status_repository = new OnboardingStatusMgmtRepository();
        $response = $onboarding_status_repository->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.recruitment-and-hiring.onboarding-status-mgmt.index', 'close_modal', 'create');
            $this->emitTo('hrim.recruitment-and-hiring.onboarding-status-mgmt.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.hrim.recruitment-and-hiring.onboarding-status-mgmt.create');
    }
}
