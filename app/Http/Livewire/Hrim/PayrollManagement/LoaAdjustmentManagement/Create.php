<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\LoaAdjustmentManagement;

use App\Interfaces\Hrim\PayrollManagement\LoaAdjustmentInterface;
use App\Traits\Hrim\PayrollManagement\LoaAdjustmentTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use LoaAdjustmentTrait, PopUpMessagesTrait;

    protected $rules = [
        'min_amount' => 'required',
        'max_amount' => 'required',
        'category' => 'required',
        'approver' => 'required',
    ];

    public function confirmationSubmit()
    {
        $this->validate();
        $this->confirmation_modal = true;
    }

    public function submit(LoaAdjustmentInterface $loa_adjustment_interface)
    {
        $response = $loa_adjustment_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.payroll-management.loa-adjustment-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.payroll-management.loa-adjustment-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.payroll-management.loa-adjustment-management.create');
    }
}
