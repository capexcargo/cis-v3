<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\LoaAdjustmentManagement;

use App\Interfaces\Hrim\PayrollManagement\LoaAdjustmentInterface;
use App\Traits\Hrim\PayrollManagement\LoaAdjustmentTrait;
use App\Traits\Hrim\PayrollManagement\PayrollManagementHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use PayrollManagementHeaderTrait, LoaAdjustmentTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_loa_adjustment') {
            $this->create_modal = true;
        } else if ($action_type == 'edit_loa_adjustment') {
            $this->emitTo('hrim.payroll-management.loa-adjustment-management.edit', 'mount', $data['id']);
            $this->adjustment_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'delete_loa_adjustment') {
            $this->confirmation_message = "Are you sure you want to delete this LOA Adjustment?";
            $this->delete_modal = true;
            $this->adjustment_id = $data['id'];
        }
    }
    

    public function delete(LoaAdjustmentInterface $loa_adjustment_interface)
    {
        if ($this->action_type == 'delete_loa_adjustment') {
            $response = $loa_adjustment_interface->destroy($this->adjustment_id);
        }

        if ($response['code'] == 200) {
            $this->delete_modal = false;
            $this->emitTo('hrim.payroll-management.loa-adjustment-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function render(LoaAdjustmentInterface $loa_adjustment_interface)
    {
        $request = [
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $loa_adjustment_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'loa_adjustments' => [],
                ];
        }

        return view('livewire.hrim.payroll-management.loa-adjustment-management.index', [
            'loa_adjustments' => $response['result']['loa_adjustments']
        ]);
    }
}
