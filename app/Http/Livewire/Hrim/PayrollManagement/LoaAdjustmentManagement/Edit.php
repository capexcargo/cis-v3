<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\LoaAdjustmentManagement;

use App\Interfaces\Hrim\PayrollManagement\LoaAdjustmentInterface;
use App\Models\Hrim\AdminAdjustment;
use App\Traits\Hrim\PayrollManagement\LoaAdjustmentTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use LoaAdjustmentTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(LoaAdjustmentInterface $loa_adjustment_interface, $id)
    {
        $this->resetForm();
        $response = $loa_adjustment_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        // dd($response['result']);
        $this->admin_adjustment = $response['result'];

        $this->min_amount = $this->admin_adjustment->min_amount;
        $this->max_amount = $this->admin_adjustment->max_amount;
        $this->category = $this->admin_adjustment->category_id;
        $this->approver = $this->admin_adjustment->approver_id;
    }

    public function submit(LoaAdjustmentInterface $loa_adjustment_interface)
    {
        $response = $loa_adjustment_interface->update($this->getRequest(), $this->admin_adjustment->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.payroll-management.loa-adjustment-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.payroll-management.loa-adjustment-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.payroll-management.loa-adjustment-management.edit');
    }
}
