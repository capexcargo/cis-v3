<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\DlbManagement;

use Livewire\Component;

class View extends Component
{
    public function render()
    {
        return view('livewire.hrim.payroll-management.dlb-management.view');
    }
}
