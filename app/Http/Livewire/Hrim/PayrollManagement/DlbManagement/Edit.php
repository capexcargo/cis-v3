<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\DlbManagement;

use App\Interfaces\Hrim\PayrollManagement\DlbManagementInterface;
use App\Traits\Hrim\PayrollManagement\DlbManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use DlbManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['edit_mount' => 'mount'];

    public function mount(DlbManagementInterface $dlb_management_interface, $id)
    {
        $this->resetForm();

        $response = $dlb_management_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $dlb = $response['result'];
        $this->user_id = $dlb->id;

        foreach ($dlb->dlbUsers as $dlb) {
            $this->dlbs[] = [
                'id' => $dlb->id,
                'year' => $dlb->year,
                'month' => $dlb->month,
                'payout_cutoff' => $dlb->payout_cutoff,
                'amount' => $dlb->amount,
                'is_deleted' => false,
            ];
        }
    }

    public function submit(DlbManagementInterface $dlb_management_interface)
    {
        $rules = [
            'user_id' => 'required',
        ];

        foreach ($this->dlbs as $i => $dlb) {
            $rules['dlbs.' . $i . '.id'] = 'sometimes';
            $rules['dlbs.' . $i . '.year'] = 'required';
            $rules['dlbs.' . $i . '.month'] = 'required';
            $rules['dlbs.' . $i . '.payout_cutoff'] = 'required';
            $rules['dlbs.' . $i . '.amount'] = 'required';
        }

        $validated = $this->validate(
            $rules,
            [
                'dlbs.*.year.required' => 'The Year field is required.',
                'dlbs.*.month.required' => 'The Month field is required.',
                'dlbs.*.payou.requiredt_cutoff' => 'The Cutoff field is required.',
                'dlbs.*.amount.required' => 'The Amount field is required.',
            ]
        );

        $response = $dlb_management_interface->update($validated, $this->user_id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.payroll-management.dlb-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.payroll-management.dlb-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.payroll-management.dlb-management.edit');
    }
}
