<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\DlbManagement;

use App\Interfaces\Hrim\PayrollManagement\DlbManagementInterface;
use App\Models\Hrim\CutOffManagement;
use App\Models\MonthReference;
use App\Traits\Hrim\PayrollManagement\DlbManagementTrait;
use App\Traits\Hrim\PayrollManagement\PayrollManagementHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use PayrollManagementHeaderTrait;
    use DlbManagementTrait;
    use WithPagination;
    use PopUpMessagesTrait;
    public $month_references = [];
    public $year;
    public $month;
    public $cut_off;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal', 'index_mount' => 'mount'];

    public function mount()
    {
        $cutoff = CutOffManagement::where('cutoff_date', date('Y-m-d'))->first();

        if ($cutoff->payout_cutoff == 1) {
            $cutoffs = '1';
        } else {
            $cutoffs = '2';
        }

        $this->month = $cutoff->payout_month;
        $this->cut_off = $cutoffs;
        $this->year = date('Y');
    }

    public function load()
    {
        $this->loadMonthReference();
    }

    public function loadMonthReference()
    {
        $this->month_references = MonthReference::get();
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create') {
            $this->create_modal = true;
        } elseif ($action_type == 'edit') {
            $this->emitTo('hrim.payroll-management.dlb-management.edit', 'edit', $data['id']);
            $this->user_id = $data['id'];
            $this->edit_modal = true;
            // dd($data['id']);
        }
        if ($action_type == 'view_dlb') {
            $this->view_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function render(DlbManagementInterface $dlb_management_interface)
    {
        $request = [
            'paginate' => $this->paginate,
            'year' => $this->year,
            'month' => $this->month,
            'cut_off' => $this->cut_off,
        ];

        $response = $dlb_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'dlb_managements' => [],
                ];
        }

        return view('livewire.hrim.payroll-management.dlb-management.index', [
            'dlb_managements' => $response['result']['dlb_managements'],
        ]);

        // dd($response['result']['dlb_managements']);
    }
}
