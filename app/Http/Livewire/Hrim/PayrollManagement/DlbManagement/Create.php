<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\DlbManagement;

use App\Interfaces\Hrim\PayrollManagement\DlbManagementInterface;
use App\Traits\Hrim\PayrollManagement\DlbManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
  
    use DlbManagementTrait, PopUpMessagesTrait;

    public $validated;

    public function confirmationSubmit()
    {

        $rules = [
            'user_id' => 'required',
        ];

        foreach ($this->dlbs as $i => $dlb) {
            $rules['dlbs.' . $i . '.id'] = 'sometimes';
            $rules['dlbs.' . $i . '.year'] = 'required';
            $rules['dlbs.' . $i . '.month'] = 'required';
            $rules['dlbs.' . $i . '.payout_cutoff'] = 'required';
            $rules['dlbs.' . $i . '.amount'] = 'required';
        }

        $validated = $this->validate(
            $rules,
            [
                'dlbs.*.year.required' => 'The Year field is required.',
                'dlbs.*.month.required' => 'The Month field is required.',
                'dlbs.*.payou.requiredt_cutoff' => 'The Cutoff field is required.',
                'dlbs.*.amount.required' => 'The Amount field is required.',
            ]
        );

        // dd($validated);

        // $this->validate();
        $this->confirmation_modal = true;
        // $this->emitTo('hrim.payroll-management.holiday-management.index', 'close_modal', 'create');
    }


    public function submit(DlbManagementInterface $dlb_management_interface)
    {

        $rules = [
            'user_id' => 'required',
        ];

        foreach ($this->dlbs as $i => $dlb) {
            $rules['dlbs.' . $i . '.id'] = 'sometimes';
            $rules['dlbs.' . $i . '.year'] = 'required';
            $rules['dlbs.' . $i . '.month'] = 'required';
            $rules['dlbs.' . $i . '.payout_cutoff'] = 'required';
            $rules['dlbs.' . $i . '.amount'] = 'required';
        }

        $validated = $this->validate(
            $rules,
            [
                'dlbs.*.year.required' => 'The Year field is required.',
                'dlbs.*.month.required' => 'The Month field is required.',
                'dlbs.*.payou.requiredt_cutoff' => 'The Cutoff field is required.',
                'dlbs.*.amount.required' => 'The Amount field is required.',
            ]
        );

        $response = $dlb_management_interface->create($validated);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.payroll-management.dlb-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.payroll-management.dlb-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.payroll-management.dlb-management.create');
    }
}
