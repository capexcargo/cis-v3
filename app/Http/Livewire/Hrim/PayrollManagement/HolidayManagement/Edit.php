<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\HolidayManagement;

use App\Interfaces\Hrim\PayrollManagement\HolidayManagementInterface;
use App\Models\Hrim\HolidayManagement;
use App\Traits\Hrim\PayrollManagement\HolidayManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{

    use HolidayManagementTrait, PopUpMessagesTrait;


    protected $listeners = ['holiday_management_mount' => 'mount'];

    public function mount($id)
    {
        $this->resetForm();
        $this->holiday_management = HolidayManagement::findOrFail($id);

        $this->name = $this->holiday_management->name;
        $this->date = $this->holiday_management->date;
        $this->branch_id = $this->holiday_management->branch_id;
        $this->type_id = $this->holiday_management->type_id;
    }


    public function submit(HolidayManagementInterface $holiday_management_interface)
    {
        $response = $holiday_management_interface->update($this->getRequest(), $this->holiday_management->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.payroll-management.holiday-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.payroll-management.holiday-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render()
    {
        return view('livewire.hrim.payroll-management.holiday-management.edit');
    }
}
