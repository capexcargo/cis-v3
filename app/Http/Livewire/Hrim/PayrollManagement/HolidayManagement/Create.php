<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\HolidayManagement;

use App\Interfaces\Hrim\PayrollManagement\HolidayManagementInterface;
use App\Traits\Hrim\PayrollManagement\HolidayManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use HolidayManagementTrait;
    use WithFileUploads;
    use PopUpMessagesTrait;

    protected $rules = [
        'name' => 'required',
        'date' => 'required|unique:hrim_holiday,date',
        'branch_id' => 'sometimes',
        'type_id' => 'required',
    ];

    public function confirmationSubmit()
    {
        $this->validate();
        $this->confirmation_modal = true;
        // $this->emitTo('hrim.payroll-management.holiday-management.index', 'close_modal', 'create');
    }

    public function submit(HolidayManagementInterface $holiday_management_interface)
    {
        $response = $holiday_management_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.payroll-management.holiday-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.payroll-management.holiday-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } elseif ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.payroll-management.holiday-management.create');
    }
}
