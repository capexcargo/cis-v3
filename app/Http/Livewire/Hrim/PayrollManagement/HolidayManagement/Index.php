<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\HolidayManagement;

use App\Interfaces\Hrim\PayrollManagement\HolidayManagementInterface;
use App\Models\Hrim\HolidayTypeReference;
use App\Traits\Hrim\Csp\CspManagementTrait;
use App\Traits\Hrim\PayrollManagement\HolidayManagementTrait;
use App\Traits\Hrim\PayrollManagement\PayrollManagementHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use PayrollManagementHeaderTrait, HolidayManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_holiday_management') {
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('holiday_management_mount', $data['id']);
            $this->edit_modal = true;
            $this->holiday_management_id = $data['id'];
        } else if ($action_type == 'delete') {
            $this->confirmation_message = "Are you sure you want to delete this Holiday?";
            $this->delete_modal = true;
            $this->holiday_management_id = $data['id'];
        }
    }

    public function delete(HolidayManagementInterface $HolidayManagementInterface)
    {

        if ($this->action_type == 'delete') {
            $response = $HolidayManagementInterface->destroy($this->holiday_management_id);
        }

        if ($response['code'] == 200) {
            $this->delete_modal = false;
            $this->emitTo('hrim.payroll-management.holiday-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }


    public function render(HolidayManagementInterface $holiday_management_interface)
    {

        $request = [
            'name' => $this->name,
            'date' => $this->date,
            'branch_id' => $this->branch_id,
            'type_id' => $this->type_id,
            'paginate' => $this->paginate,

        ];

        $response = $holiday_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'holiday_managements' => [],
                ];
        }
        return view('livewire.hrim.payroll-management.holiday-management.index', [
            'holiday_managements' => $response['result']['holiday_managements']
        ]);
    }
}
