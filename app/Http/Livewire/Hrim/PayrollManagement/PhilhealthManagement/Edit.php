<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\PhilhealthManagement;

use App\Interfaces\Hrim\PayrollManagement\PhilhealthManagementInterface;
use App\Models\Hrim\PhilhealthManagement;
use App\Traits\Hrim\PayrollManagement\PhilhealthManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{

    use PhilhealthManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['philhealth_management_mount' => 'mount'];


    public function mount($id)
    {
        $this->resetForm();
        $this->philhealth_management = PhilhealthManagement::findOrFail($id);

        $this->year = $this->philhealth_management->year;
        $this->premium_rate = $this->philhealth_management->premium_rate;
    }

    public function submit(PhilhealthManagementInterface $philhealth_management_interface)
    {
        $response = $philhealth_management_interface->update($this->getRequest(), $this->philhealth_management->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.payroll-management.philhealth-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.payroll-management.philhealth-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.payroll-management.philhealth-management.edit');
    }
}
