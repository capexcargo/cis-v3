<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\PhilhealthManagement;

use App\Interfaces\Hrim\PayrollManagement\PhilhealthManagementInterface;
use App\Traits\Hrim\PayrollManagement\PhilhealthManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use PhilhealthManagementTrait, PopUpMessagesTrait;

    protected $rules = [
        'year' => 'required',
        'premium_rate' => 'required',
    ];

    public function confirmationSubmit()
    {
        $this->validate();
        $this->confirmation_modal = true;
        // $this->emitTo('hrim.payroll-management.holiday-management.index', 'close_modal', 'create');
    }

    public function submit(PhilhealthManagementInterface $philhealth_management_interface)
    {

        $response = $philhealth_management_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.payroll-management.philhealth-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.payroll-management.philhealth-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->confirmation_modal = false;
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.payroll-management.philhealth-management.create');
    }
}
