<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\PhilhealthManagement;

use App\Interfaces\Hrim\PayrollManagement\PhilhealthManagementInterface;
use App\Traits\Hrim\PayrollManagement\PayrollManagementHeaderTrait;
use App\Traits\Hrim\PayrollManagement\PhilhealthManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use PayrollManagementHeaderTrait, PhilhealthManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create') {
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('philhealth_management_mount', $data['id']);
            $this->edit_modal = true;
            $this->philhealth_management_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }


    public function render(PhilhealthManagementInterface $philhealth_management_interface)
    {
        $request = [];

        $response = $philhealth_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'philhealth_managements' => [],
                ];
        }
        return view('livewire.hrim.payroll-management.philhealth-management.index', [
            'philhealth_managements' => $response['result']['philhealth_managements']
        ]);
    }
}
