<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\Month13th;

use Livewire\Component;

class View extends Component
{
    public function render()
    {
        return view('livewire.hrim.payroll-management.month13th.view');
    }
}
