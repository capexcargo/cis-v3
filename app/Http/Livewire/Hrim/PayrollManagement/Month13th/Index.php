<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\Month13th;

use App\Exports\Hrim\Month13thPay\Download13thMonth;
use App\Exports\Hrim\Month13thPay\Generate13thMonthBankFile;
use App\Interfaces\Hrim\PayrollManagement\Month13thPayInterface;
use App\Models\Hrim\CutOffManagement;
use App\Models\MonthReference;
use App\Models\UserDetails;
use App\Traits\Hrim\CompensationAndBenefits\PayrollTrait;
use App\Traits\Hrim\PayrollManagement\PayrollManagementHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use App\Traits\ResponseTrait;
use Livewire\Component;
use Maatwebsite\Excel\Facades\Excel;

class Index extends Component
{
    use PayrollTrait;
    use PayrollManagementHeaderTrait;
    use PopUpMessagesTrait;
    use ResponseTrait;

    public $generate_13th_month_modal = false;
    public $generate_bankfile_modal = false;

    public $month_references = [];
    public $year;
    public $month;
    public $cut_off;

    public $payrollss = [];
    public $selected = [];
    public $selecteds = [];
    public $selectAll = false;
    public $payroll_interface;

    public $payrollforcheckbox = [];

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadMonthReference();
        $this->branchReferences();
        $this->employeeCategoryReferences();
        $this->divisionReferences();
        $this->jobRankReferences();
    }

    public function mount()
    {
        $cutoff = CutOffManagement::where('cutoff_date', date('Y-m-d'))->first();

        if ($cutoff->payout_cutoff == 1) {
            $cutoffs = '1';
        } else {
            $cutoffs = '2';
        }

        $this->month = $cutoff->payout_month;
        $this->cut_off = $cutoffs;
        $this->year = date('Y');
    }

    public function loadMonthReference()
    {
        $this->month_references = MonthReference::get();
    }

    public function action($action_type)
    {
        if ($action_type == 'generate_13th_month') {
            $this->generate_13th_month_modal = true;
        } else if ($action_type == 'generate_bank_file') {
            $this->generate_bankfile_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'generate_13th_month') {
            $this->generate_13th_month_modal = false;
        } elseif ($action_type == 'generate_bank_file') {
            $this->generate_bankfile_modal = false;
        }
    }

    public function updatedSelectAll($value)
    {
        if ($value) {

            $request = [
                'year' => $this->year,
                'month' => $this->month,
                'cut_off' => $this->cut_off,
                'branch' => $this->branch,
                'employee_id' => $this->employee_id,
                'employee_name' => $this->employee_name,
                'employee_category' => $this->employee_category,
                'employee_code' => $this->employee_code,
                'division' => $this->division,
                'job_rank' => $this->job_rank,
            ];


            $this->payrollforcheckbox = UserDetails::whereNotNull('employment_category_id')
                ->when($request['branch'] ?? false, function ($query) use ($request) {
                    $query->where('branch', $request['branch']);
                })
                ->when($request['employee_id'] ?? false, function ($query) use ($request) {
                    $query->where('employee_number', $request['employee_id']);
                })
                ->when($request['employee_category'] ?? false, function ($query) use ($request) {
                    $query->where('employment_category_id', $request['employee_category']);
                })
                ->when($request['employee_name'], function ($query) use ($request) {
                    $query->whereRaw('CONCAT(`first_name`, " " , `middle_name`, " ", `last_name`) LIKE "%' . $request['employee_name'] . '%"');
                    $query->orWhereRaw('CONCAT(`first_name`, " " , `last_name`) LIKE "%' . $request['employee_name'] . '%"');
                    $query->orWhere('first_name', 'like', '"%' . $request['employee_name'] . '%"');
                    $query->orWhere('middle_name', 'like', '"%' . $request['employee_name'] . '%"');
                    $query->orWhere('last_name', 'like', '"%' . $request['employee_name'] . '%"');
                })
                ->when($request['employee_id'] ?? false, function ($query) use ($request) {
                    $query->where('employee_number', $request['employee_id']);
                })
                ->when($request['division'] ?? false, function ($query) use ($request) {
                    $query->where('division_id', $request['division']);
                })
                ->when($request['job_rank'] ?? false, function ($query) use ($request) {
                    $query->where('job_level_id', $request['job_rank']);
                })->pluck('employee_number');

            $this->selected = $this->payrollforcheckbox;
        } else {
            $this->selected = [];
        }
    }

    public function email_13th_month(Month13thPayInterface $month_13th_pay_interface)
    {
        $request = [
            'year' => $this->year,
            'month' => $this->month,
            'cut_off' => $this->cut_off,
            'branch' => $this->branch,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee_name,
            'employee_category' => $this->employee_category,
            'employee_code' => $this->employee_code,
            'division' => $this->division,
            'job_rank' => $this->job_rank,
        ];

        if ($this->selectAll || count($this->selected) > 0) {
            $response = $month_13th_pay_interface->email13thMonth($this->selected, $request);
            if ($response['code'] == 200) {
                $this->sweetAlert('', $response['message']);
                $this->emitTo('hrim.payroll-management.month13th.index', 'close_modal', 'generate_13th_month');
                $this->emitTo('hrim.payroll-management.month13th.index', 'index');
            }
        }
    }

    public function download(Month13thPayInterface $month_13th_pay_interface)
    {
        $request = [
            'year' => $this->year,
            'month' => $this->month,
            'cut_off' => $this->cut_off,
            'branch' => $this->branch,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee_name,
            'employee_category' => $this->employee_category,
            'employee_code' => $this->employee_code,
            'division' => $this->division,
            'job_rank' => $this->job_rank,
        ];

        if ($this->selectAll || count($this->selected) > 0) {
            $response = $month_13th_pay_interface->export13thMonth($this->selected, $request);

            $month_13th_details = $response['result']['month_13th_pays'];

            // dd($month_13th_details);
            $this->emitTo('hrim.payroll-management.month13th.index', 'close_modal', 'generate_13th_month');
            $this->emitTo('hrim.payroll-management.month13th.index', 'index');

            if ($response['code'] == 200) {
                return Excel::download(new Download13thMonth(compact('month_13th_details')), '13th-month-pay.xlsx');
            }
        }
    }

    public function generate(Month13thPayInterface $month_13th_pay_interface)
    {
        $request = [
            'year' => $this->year,
            'month' => $this->month,
            'cut_off' => $this->cut_off,
            'branch' => $this->branch,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee_name,
            'employee_category' => $this->employee_category,
            'employee_code' => $this->employee_code,
            'division' => $this->division,
            'job_rank' => $this->job_rank,
        ];

        if ($this->selectAll || count($this->selected) > 0) {
            $response = $month_13th_pay_interface->export13thMonth($this->selected, $request);

            $month_13th_details = $response['result']['month_13th_pays'];

            // dd($month_13th_details);
            $this->emitTo('hrim.payroll-management.month13th.index', 'close_modal', 'generate_bank_file');
            $this->emitTo('hrim.payroll-management.month13th.index', 'index');

            if ($response['code'] == 200) {
                return Excel::download(new Generate13thMonthBankFile(compact('month_13th_details')), 'bank-file-13th-month-pay.xlsx');
            }
        }
    }

    public function render(Month13thPayInterface $month_13th_pay_interface)
    {
        $request = [
            'year' => $this->year,
            'month' => $this->month,
            'cut_off' => $this->cut_off,
            'branch' => $this->branch,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee_name,
            'employee_category' => $this->employee_category,
            'employee_code' => $this->employee_code,
            'division' => $this->division,
            'job_rank' => $this->job_rank,
        ];

        $response = $month_13th_pay_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'month_13th_pays' => [],
                ];
        }

        return view('livewire.hrim.payroll-management.month13th.index', [
            'month_13th_pays' => $response['result']['month_13th_pays'],
        ]);
    }
}
