<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\CutOffManagement;

use App\Interfaces\Hrim\PayrollManagement\CutOffManagementInterface;
use App\Traits\Hrim\PayrollManagement\CutOffManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use CutOffManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(CutOffManagementInterface $cutoff_management_interface, $id)
    {
        // dd($id);
    }
    
    public function render()
    {
        return view('livewire.hrim.payroll-management.cut-off-management.edit');
    }
}
