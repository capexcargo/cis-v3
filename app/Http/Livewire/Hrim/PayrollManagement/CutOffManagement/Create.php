<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\CutOffManagement;

use App\Interfaces\Hrim\PayrollManagement\CutOffManagementInterface;
use App\Traits\Hrim\PayrollManagement\CutOffManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use CutOffManagementTrait, PopUpMessagesTrait;

    protected $rules = [
        'category' => 'required',
        'cutoff_date_from' => 'required',
        'cutoff_date_to' => 'required',
        'payout_date' => 'required',
    ];

    public function confirmationSubmit()
    {
        $this->validate();
        $this->confirmation_modal = true;
    }

    
    public function submit(CutOffManagementInterface $cutoff_management_interface)
    {
        // $response = $admin_adjustment_interface->create($this->getRequest());

        // if ($response['code'] == 200) {
        //     $this->resetForm();
        //     $this->emitTo('hrim.payroll-management.admin-adjustment.index', 'close_modal', 'create');
        //     $this->emitTo('hrim.payroll-management.admin-adjustment.index', 'index');
        //     $this->sweetAlert('', $response['message']);
        // } else if ($response['code'] == 400) {
        //     foreach ($response['result'] as $a => $result) {
        //         $this->addError($a, $result);
        //     }
        //     return;
        // } else {
        //     $this->sweetAlertError('error', $response['message'], $response['result']);
        // }
    }

    public function render()
    {
        return view('livewire.hrim.payroll-management.cut-off-management.create');
    }
}
