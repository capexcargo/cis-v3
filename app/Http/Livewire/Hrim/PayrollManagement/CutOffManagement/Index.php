<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\CutOffManagement;

use App\Interfaces\Hrim\PayrollManagement\CutOffManagementInterface;
use App\Traits\Hrim\PayrollManagement\CutOffManagementTrait;
use App\Traits\Hrim\PayrollManagement\PayrollManagementHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use PayrollManagementHeaderTrait, CutOffManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_cutoff') {
            $this->create_modal = true;
        } else if ($action_type == 'edit_cutoff') {
            $this->emitTo('hrim.payroll-management.cut-off-management.edit', 'mount', $data['id']);
            $this->cutoff_id = $data['id'];
            $this->edit_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function render(CutOffManagementInterface $cutoff_management_interface)
    {
        $request = [
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $cutoff_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'cutoff_managements' => [],
                ];
        }
        
        return view('livewire.hrim.payroll-management.cut-off-management.index', [
            'cutoff_managements' => $response['result']['cutoff_managements']
        ]);
    }
}
