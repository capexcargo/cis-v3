<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\GovernmentContribution;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.hrim.payroll-management.government-contribution.index');
    }
}
