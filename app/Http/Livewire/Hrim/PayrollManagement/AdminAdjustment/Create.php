<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\AdminAdjustment;

use App\Interfaces\Hrim\PayrollManagement\AdminAdjustmentInterface;
use App\Traits\Hrim\PayrollManagement\AdminAdjustmentTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use AdminAdjustmentTrait, PopUpMessagesTrait;

    protected $rules = [
        'employee' => 'required',
        'category' => 'required',
        'description' => 'sometimes',
        'amount' => 'required',
        'year' => 'required',
        'month' => 'required',
        'cutoff' => 'required',
    ];

    public function confirmationSubmit()
    {
        $this->validate();
        $this->confirmation_modal = true;
    }

    public function submit(AdminAdjustmentInterface $admin_adjustment_interface)
    {
        $response = $admin_adjustment_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.payroll-management.admin-adjustment.index', 'close_modal', 'create');
            $this->emitTo('hrim.payroll-management.admin-adjustment.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render()
    {
        return view('livewire.hrim.payroll-management.admin-adjustment.create');
    }
}
