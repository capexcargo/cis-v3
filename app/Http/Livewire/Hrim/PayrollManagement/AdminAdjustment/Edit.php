<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\AdminAdjustment;

use App\Interfaces\Hrim\PayrollManagement\AdminAdjustmentInterface;
use App\Traits\Hrim\PayrollManagement\AdminAdjustmentTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use AdminAdjustmentTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(AdminAdjustmentInterface $admin_adjustment_interface, $id)
    {
        $this->resetForm();
        $response = $admin_adjustment_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->admin_adjustment = $response['result'];

        $this->employee = $this->admin_adjustment->user->name;
        $this->category = $this->admin_adjustment->category;
        $this->description = $this->admin_adjustment->description;
        $this->amount = $this->admin_adjustment->amount;
        $this->year = $this->admin_adjustment->year;
        $this->month = $this->admin_adjustment->month;
        $this->cutoff = $this->admin_adjustment->payout_cutoff;
    }

    public function submit(AdminAdjustmentInterface $admin_adjustment_interface)
    {
        $response = $admin_adjustment_interface->update($this->getRequest(), $this->admin_adjustment->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.payroll-management.admin-adjustment.index', 'close_modal', 'edit');
            $this->emitTo('hrim.payroll-management.admin-adjustment.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.payroll-management.admin-adjustment.edit');
    }
}
