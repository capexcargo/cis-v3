<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\AdminAdjustment;

use App\Interfaces\Hrim\PayrollManagement\AdminAdjustmentInterface;
use App\Traits\Hrim\PayrollManagement\AdminAdjustmentTrait;
use App\Traits\Hrim\PayrollManagement\PayrollManagementHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use PayrollManagementHeaderTrait, AdminAdjustmentTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_adjustment') {
            $this->create_modal = true;
        } else if ($action_type == 'edit_adjustment') {
            $this->emitTo('hrim.payroll-management.admin-adjustment.edit', 'mount', $data['id']);
            $this->adjustment_id = $data['id'];
            $this->edit_modal = true;
        } else if ($action_type == 'delete_adjustment') {
            $this->confirmation_message = "Are you sure you want to delete this Adjustment?";
            $this->delete_modal = true;
            $this->adjustment_id = $data['id'];
        } else if ($action_type == 'approve_adjustment') {
            $this->confirmation_message = "Are you sure you want to approve this Admin Adjustment?";
            $this->approve_modal = true;
            $this->adjustment_id = $data['id'];
        }
    }

    public function approve(AdminAdjustmentInterface $admin_adjustment_interface)
    {
        if ($this->action_type == 'approve_adjustment') {
            $response = $admin_adjustment_interface->approve($this->adjustment_id);
        }
        if ($response['code'] == 200) {
            $this->approve_modal = false;
            $this->emitTo('hrim.payroll-management.admin-adjustment.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function delete(AdminAdjustmentInterface $admin_adjustment_interface)
    {
        if ($this->action_type == 'delete_adjustment') {
            $response = $admin_adjustment_interface->destroy($this->adjustment_id);
        }

        if ($response['code'] == 200) {
            $this->delete_modal = false;
            $this->emitTo('hrim.payroll-management.admin-adjustment.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }


    public function render(AdminAdjustmentInterface $admin_adjustment_interface)
    {
        $request = [
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $admin_adjustment_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'admin_adjustments' => [],
                ];
        }

        return view('livewire.hrim.payroll-management.admin-adjustment.index', [
            'admin_adjustments' => $response['result']['admin_adjustments']
        ]);
    }
}
