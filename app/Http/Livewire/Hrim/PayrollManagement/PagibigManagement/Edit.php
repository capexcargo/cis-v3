<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\PagibigManagement;

use App\Interfaces\Hrim\PayrollManagement\PagibigManagementInterface;
use App\Models\Hrim\PagibigManagement;
use App\Traits\Hrim\PayrollManagement\PagibigManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{

    use PagibigManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['pagibig_management_mount' => 'mount'];


    public function mount($id)
    {
        $this->resetForm();
        $this->pagibig_management = PagibigManagement::findOrFail($id);

        $this->year = $this->pagibig_management->year;
        $this->share = $this->pagibig_management->share;
    }

    public function submit(PagibigManagementInterface $pagibig_management_interface)
    {
        $response = $pagibig_management_interface->update($this->getRequest(), $this->pagibig_management->id);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.payroll-management.pagibig-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.payroll-management.pagibig-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    public function render()
    {
        return view('livewire.hrim.payroll-management.pagibig-management.edit');
    }
}
