<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\PagibigManagement;

use App\Interfaces\Hrim\PayrollManagement\PagibigManagementInterface;
use App\Traits\Hrim\PayrollManagement\PagibigManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{

    use PagibigManagementTrait, PopUpMessagesTrait;

    protected $rules = [
        'year' => 'required|numeric',
        'share' => 'required|numeric',
    ];

    public function confirmationSubmit()
    {
        $this->validate();
        $this->confirmation_modal = true;
        // $this->emitTo('hrim.payroll-management.holiday-management.index', 'close_modal', 'create');
    }

    public function submit(PagibigManagementInterface $pagibig_management_interface)
    {

        $response = $pagibig_management_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.payroll-management.pagibig-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.payroll-management.pagibig-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            $this->confirmation_modal = false;
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render()
    {
        return view('livewire.hrim.payroll-management.pagibig-management.create');
    }
}
