<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\PagibigManagement;

use App\Interfaces\Hrim\PayrollManagement\PagibigManagementInterface;
use App\Traits\Hrim\PayrollManagement\PagibigManagementTrait;
use App\Traits\Hrim\PayrollManagement\PayrollManagementHeaderTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use PayrollManagementHeaderTrait, PagibigManagementTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create') {
            $this->create_modal = true;
        } else if ($action_type == 'edit') {
            $this->emit('pagibig_management_mount', $data['id']);
            $this->edit_modal = true;
            $this->pagibig_management_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }


    public function render(PagibigManagementInterface $pagibig_management_interface)
    {
        $request = [];

        $response = $pagibig_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'pagibig_management' => [],
                ];
        }
        return view('livewire.hrim.payroll-management.pagibig-management.index', [
            'pagibig_managements' => $response['result']['pagibig_management']
        ]);
    }
}
