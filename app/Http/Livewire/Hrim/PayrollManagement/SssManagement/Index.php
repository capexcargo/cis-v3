<?php

namespace App\Http\Livewire\Hrim\PayrollManagement\SssManagement;

use App\Imports\SssManagement;
use App\Interfaces\Hrim\PayrollManagement\SssManagementInterface;
use App\Traits\Hrim\PayrollManagement\PayrollManagementHeaderTrait;
use App\Traits\Hrim\PayrollManagement\SssManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Maatwebsite\Excel\Facades\Excel;


class Index extends Component
{
    use PayrollManagementHeaderTrait, SssManagementTrait, WithPagination, PopUpMessagesTrait, WithFileUploads;

    public function import()
    {
        try {
            $validated = $this->validate([
                'import' => 'mimes:xlsx,csv'
            ]);

            Excel::import(new SssManagement, $this->import);
            $this->resetForm();
            $this->sweetAlert('', 'New SSS Contribution Imported!');
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            foreach ($failures as $failure) {
                $this->sweetAlertError('error', 'Something Went Wrong', "Row: " . $failure->row() .
                    "\n Column: " . $failure->attribute() .
                    "\n Message: " . $failure->errors()[0] .
                    "\n Value: " . $failure->values()[$failure->attribute()]);
            }
        }
    }
    public function render(SssManagementInterface $sss_management_interface)
    {
        $request = [
            'year' => $this->year,
            'paginate' => $this->paginate,
        ];


        $response = $sss_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] =
                [
                    'sss_management' => [],
                ];
        }
        return view('livewire.hrim.payroll-management.sss-management.index', [
            'sss_managements' => $response['result']['sss_management']
        ]);
    }
}

