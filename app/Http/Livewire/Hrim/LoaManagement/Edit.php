<?php

namespace App\Http\Livewire\Hrim\LoaManagement;

use App\Interfaces\Hrim\LoaManagementInterface;
use App\Traits\Hrim\LoaManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Edit extends Component
{
    use LoaManagementTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(LoaManagementInterface $loa_management_interface, $id)
    {
        $this->resetForm();
        $response = $loa_management_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->loa_management = $response['result'];
        $this->division = $this->loa_management->division_id;
        $this->department_id = $this->loa_management->department_id;
        $this->branch = $this->loa_management->branch_id;
        $this->type = $this->loa_management->loa_type_id;
        $this->first_approver = $this->loa_management->first_approver;
        $this->second_approver = $this->loa_management->second_approver;
        $this->third_approver = $this->loa_management->third_approver;

        $this->loadFirstApproverReference();
        $this->loadSecodeApproverReference();
        $this->loadThirdApproverReference();
    }

    public function submit(LoaManagementInterface $loa_management_interface)
    {
        $response = $loa_management_interface->updateOrCreate($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.loa-management.index', 'close_modal', 'edit');
            $this->emitTo('hrim.loa-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.loa-management.edit');
    }
}
