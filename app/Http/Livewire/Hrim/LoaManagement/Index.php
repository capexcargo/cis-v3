<?php

namespace App\Http\Livewire\Hrim\LoaManagement;

use App\Interfaces\Hrim\LoaManagementInterface;
use App\Models\BranchReference;
use App\Models\Division;
use App\Models\Hrim\Department;
use App\Models\Hrim\LoaTypeReference;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $create_modal = false;
    public $edit_modal = false;
    public $loa_management_id;

    public $division;
    public $department_id;
    public $branch;
    public $type;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $division_references = [];
    public $department_references = [];
    public $branch_references = [];
    public $type_references = [];

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadDivisionReference();
        $this->loadDepartmentReference();
        $this->loadBranchReference();
        $this->loadTypeReference();
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function loadDepartmentReference()
    {
        $this->department_references = Department::where('division_id', $this->division)->get();
    }

    public function loadBranchReference()
    {
        $this->branch_references = BranchReference::get();
    }

    public function loadTypeReference()
    {
        $this->type_references = LoaTypeReference::get();
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emitTo('hrim.loa-management.edit', 'mount', $data['id']);
            $this->loa_management_id = $data['id'];
            $this->edit_modal = true;
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } elseif ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function render(LoaManagementInterface $loa_management_interface)
    {
        $request = [
            'division' => $this->division,
            'department_id' => $this->department_id,
            'branch' => $this->branch,
            'type' => $this->type,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];
        
        $response = $loa_management_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.loa-management.index', [
            'loa_managements' => $response['result']
        ]);
    }
}
