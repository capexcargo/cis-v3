<?php

namespace App\Http\Livewire\Hrim\LoaManagement;

use App\Interfaces\Hrim\LoaManagementInterface;
use App\Traits\Hrim\LoaManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use LoaManagementTrait, PopUpMessagesTrait;

    public function submit(LoaManagementInterface $loa_management_interface)
    {
        $response = $loa_management_interface->updateOrCreate($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.loa-management.index', 'close_modal', 'create');
            $this->emitTo('hrim.loa-management.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.loa-management.create');
    }
}
