<?php

namespace App\Http\Livewire\Hrim\EmployeeCompensationAndBenefits\LoansAndLedgerMonitoring;

use App\Interfaces\Hrim\CompensationAndBenefits\LoansAndLedgerInterface;
use App\Models\Hrim\LoanDetails;
use App\Models\Hrim\LoansAndLedger;
use App\Traits\Hrim\CompensationAndBenefits\LoansAndLedgerTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class ViewLoanPaySchedule extends Component
{
    use LoansAndLedgerTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['view_payment' => 'mount'];

    public function mount($id)
    {
        // dd($id);
        $loans = LoansAndLedger::findOrFail($id);
        $this->terms = $loans->terms;

        $this->loan_payments_references = LoanDetails::where('loan_id', $id)->get();
        // dd($this->loan_payments_references);
        $this->remaining = LoanDetails::where('loan_id', $id)->where('status', 1)->sum('status');
    }

    public function render()
    {
        return view('livewire.hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.view-loan-pay-schedule');
    }
}
