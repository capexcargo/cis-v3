<?php

namespace App\Http\Livewire\Hrim\EmployeeCompensationAndBenefits\LoansAndLedgerMonitoring;

use App\Interfaces\Hrim\CompensationAndBenefits\LoansAndLedgerInterface;
use App\Traits\Hrim\CompensationAndBenefits\LoansAndLedgerTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use LoansAndLedgerTrait, PopUpMessagesTrait;

    protected $listeners = ['generate_reference_no' => 'generateRefernenceNo'];

    public function submit(LoansAndLedgerInterface $loans_and_ledger_interface)
    {
        $response = $loans_and_ledger_interface->create($this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.index', 'close_modal', 'create');
            $this->emitTo('hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.create');
    }
}
