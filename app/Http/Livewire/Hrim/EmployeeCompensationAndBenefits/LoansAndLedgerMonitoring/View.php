<?php

namespace App\Http\Livewire\Hrim\EmployeeCompensationAndBenefits\LoansAndLedgerMonitoring;

use App\Traits\Hrim\CompensationAndBenefits\LoansAndLedgerTrait;
use Livewire\Component;

class View extends Component
{
    use LoansAndLedgerTrait; 

    public function render()
    {
        return view('livewire.hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.view');
    }
}
