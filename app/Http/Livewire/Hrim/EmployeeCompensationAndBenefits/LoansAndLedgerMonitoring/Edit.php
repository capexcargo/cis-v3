<?php

namespace App\Http\Livewire\Hrim\EmployeeCompensationAndBenefits\LoansAndLedgerMonitoring;

use App\Interfaces\Hrim\CompensationAndBenefits\LoansAndLedgerInterface;
use App\Models\Hrim\LoanType;
use App\Traits\Hrim\CompensationAndBenefits\LoansAndLedgerTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use LoansAndLedgerTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(LoansAndLedgerInterface $loans_and_ledger_interface, $id)
    {
        $this->resetForm();
        $response = $loans_and_ledger_interface->show($id);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->loans_and_ledger = $response['result'];
        $this->reference_number = $this->loans_and_ledger->reference_no;
        $this->type = $this->loans_and_ledger->type;
        $this->principal_amount = $this->loans_and_ledger->principal_amount;
        $this->terms = $this->loans_and_ledger->terms;
        $this->term_type = $this->loans_and_ledger->term_type;
        $this->payment_start_date = $this->loans_and_ledger->start_date;
        $this->year = substr($this->loans_and_ledger->start_date, 0, -6);
        $this->month_day = substr($this->loans_and_ledger->start_date, 5);
        $this->payment_end_date = $this->loans_and_ledger->end_date;
        $this->reference_number = $this->loans_and_ledger->reference_no;
        $this->purpose = $this->loans_and_ledger->purpose;

        $this->checkLoanTypeCat = LoanType::where('id', $this->type)->first();
        $this->loan_type_category = $this->checkLoanTypeCat->loan_type_category ?? null;
    }

    public function submit(LoansAndLedgerInterface $loans_and_ledger_interface)
    {
        $response = $loans_and_ledger_interface->update($this->loans_and_ledger->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.index', 'close_modal', 'edit');
            $this->emitTo('hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.index', 'edit');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            foreach ($response['result']->getMessages() as $a => $messages) {
                if (is_array($messages)) {
                    foreach ($messages as $message) :
                        $this->addError($a, $message);
                    endforeach;
                } else {
                    $this->addError($a, $messages);
                }
            }
            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.edit');
    }
}
