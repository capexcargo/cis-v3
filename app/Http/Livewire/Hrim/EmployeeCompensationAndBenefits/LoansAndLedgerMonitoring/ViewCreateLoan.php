<?php

namespace App\Http\Livewire\Hrim\EmployeeCompensationAndBenefits\LoansAndLedgerMonitoring;

use Livewire\Component;

class ViewCreateLoan extends Component
{
    public function render()
    {
        return view('livewire.hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.view-create-loan');
    }
}
