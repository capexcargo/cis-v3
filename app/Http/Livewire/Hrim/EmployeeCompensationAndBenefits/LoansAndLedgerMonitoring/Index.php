<?php

namespace App\Http\Livewire\Hrim\EmployeeCompensationAndBenefits\LoansAndLedgerMonitoring;

use App\Interfaces\Hrim\CompensationAndBenefits\LoansAndLedgerInterface;
use App\Models\Hrim\CutOffManagement;
use App\Traits\Hrim\CompensationAndBenefits\LoansAndLedgerTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use LoansAndLedgerTrait, WithPagination, PopUpMessagesTrait;

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public $payout_date_start = [];
    public $payout_date_end = [];
    public function mount(LoansAndLedgerInterface $loans_and_ledger_interface)
    {
        $request = [
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];
        
        $response = $loans_and_ledger_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        foreach ($response['result'] as $i => $loans) {
            $cutoff_mgmt_start[$i] = CutOffManagement::where('cutoff_date', $loans->start_date)->first();
            $cutoff_mgmt_start[$i]->payout_cutoff == 1 ? $payout_day = 15 : $payout_day = 30;

            $this->payout_date_start[] = [
                "start_id" => $loans->id,
                "start_date" => $cutoff_mgmt_start[$i]->payout_year . '-' . $cutoff_mgmt_start[$i]->payout_month . '-' . $payout_day,
            ];

            $cutoff_mgmt_end[$i] = CutOffManagement::where('cutoff_date', $loans->end_date)->first();
            $cutoff_mgmt_end[$i]->payout_cutoff == 1 ? $payout_day = 15 : $payout_day = 30;

            $this->payout_date_end[] = [
                "end_id" => $loans->id,
                "end_date" => $cutoff_mgmt_end[$i]->payout_year . '-' . $cutoff_mgmt_end[$i]->payout_month . '-' . $payout_day,
            ];
        }
    }
    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'apply_loan') {
            $this->emit('generate_reference_no');
            $this->create_loan_modal = true;
        } else if ($action_type == 'edit_loan') {
            $this->emitTo('hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.edit', 'mount', $data['id']);
            $this->loans_and_ledger_id = $data['id'];
            $this->edit_loan_modal = true;
        } else if ($action_type == 'view_approval') {
            $this->emitTo('hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.view-approval', 'view_approval', $data['id']);
            $this->view_approval_modal = true;
            $this->loans_and_ledger_id = $data['id'];
        } else if ($action_type == 'view_payment_schedule') {
            $this->emitTo('hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.view-loan-pay-schedule', 'view_payment', $data['id']);
            $this->view_payment_modal = true;
            $this->loans_and_ledger_id = $data['id'];
        } else if ($action_type == 'delete_loan') {
            $this->confirmation_message = "Are you sure you want to cancel this loan request?";
            $this->confirmation_modal = true;
            $this->loans_and_ledger_id = $data['id'];
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_loan_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_loan_modal = false;
        } else if ($action_type == 'delete') {
            $this->confirmation_modal = false;
        }
    }

    public function cancelLoanRequest(LoansAndLedgerInterface $loans_and_ledger_interface)
    {
        if ($this->action_type == 'delete_loan') {
            $response = $loans_and_ledger_interface->destroy($this->loans_and_ledger_id);
        }

        if ($response['code'] == 200) {
            $this->emitTo('hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.index', 'close_modal', 'delete');
            $this->emitTo('hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.index', 'index');
            $this->sweetAlert('', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render(LoansAndLedgerInterface $loans_and_ledger_interface)
    {
        $request = [
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $loans_and_ledger_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        // foreach ($response['result'] as $i => $loans) {
        //     $cutoff_mgmt_start[$i] = CutOffManagement::where('cutoff_date', $loans->start_date)->first();
        //     $cutoff_mgmt_start[$i]->payout_cutoff == 1 ? $payout_day = 15 : $payout_day = 30;

        //     $this->payout_date_start[] = [
        //         "start_id" => $loans->id,
        //         "start_date" => $cutoff_mgmt_start[$i]->payout_year . '-' . $cutoff_mgmt_start[$i]->payout_month . '-' . $payout_day,
        //     ];

        //     $cutoff_mgmt_end[$i] = CutOffManagement::where('cutoff_date', $loans->end_date)->first();
        //     $cutoff_mgmt_end[$i]->payout_cutoff == 1 ? $payout_day = 15 : $payout_day = 30;

        //     $this->payout_date_end[] = [
        //         "end_id" => $loans->id,
        //         "end_date" => $cutoff_mgmt_end[$i]->payout_year . '-' . $cutoff_mgmt_end[$i]->payout_month . '-' . $payout_day,
        //     ];
        // }

        // dd($this->payout_date_start);

        return view('livewire.hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.index', [
            'loans_and_ledger' => $response['result'],
            'payout_date_start' => $this->payout_date_start,
            'payout_date_end' => $this->payout_date_end,
        ]);
    }
}
