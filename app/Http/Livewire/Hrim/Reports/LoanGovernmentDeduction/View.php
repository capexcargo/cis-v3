<?php

namespace App\Http\Livewire\Hrim\Reports\LoanGovernmentDeduction;

use App\Interfaces\Hrim\Reports\LoansGovtDeductionInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class View extends Component
{
    use WithPagination, PopUpMessagesTrait;
    
    public $payment_start_date;
    public $payment_end_date;
    public $loan_reference;
    public $loan_deduction_particular;
    public $loan_type;
    public $amount;
    public $status;

    protected $listeners = ['view' => 'mount'];

    public function mount(LoansGovtDeductionInterface $loans_govt_deduction_interface, $id)
    {
        $response = $loans_govt_deduction_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->loans_govt_deductions = $response['result'];

        // dd($this->loans_govt_deductions);
    }

    public function render()
    {
        return view('livewire.hrim.reports.loan-government-deduction.view');
    }
}
