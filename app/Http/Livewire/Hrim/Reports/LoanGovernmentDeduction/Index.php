<?php

namespace App\Http\Livewire\Hrim\Reports\LoanGovernmentDeduction;

use App\Interfaces\Hrim\Reports\LoansGovtDeductionInterface;
use App\Traits\Hrim\Reports\LoansGovtDeductionTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use LoansGovtDeductionTrait, WithPagination, PopUpMessagesTrait;

    public $balance = [];
    public $all;
    public $paid;

    protected $listeners = ['index' => 'render'];

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'view_loan_status') {
            $this->view_modal = true;
            $this->loan_govt_id = $data['id'];
            $this->emitTo('hrim.reports.loan-government-deduction.view', 'view', $data['id']);
        }
    }

    public function render(LoansGovtDeductionInterface $loans_govt_deduction_interface)
    {
        $request = [
            'employee_name' => $this->employee_name,
            'employee_id' => $this->employee_id,
            'division' => $this->division,
            'department' => $this->department,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $loans_govt_deduction_interface->index($request);

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $res['result'] = [];
        }

        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [
                'loans_govt_deductions' => [],
            ];
        }

        return view('livewire.hrim.reports.loan-government-deduction.index', [
            'loans_govt_deductions' => $response['result']['loans_govt_deductions'],
        ]);
    }
}
