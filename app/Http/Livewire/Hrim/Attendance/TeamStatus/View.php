<?php

namespace App\Http\Livewire\Hrim\Attendance\TeamStatus;

use App\Interfaces\Hrim\Attendance\TeamStatusInterface;
use App\Models\Division;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class View extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $division_id;
    public $divisionnames;
    public $divisionname;

    public $employee_id;
    public $employee_name;

    public $user_id;
    public $view_modal = false;

    protected $listeners = ['index' => 'render'];


    public function mount($id)
    {
        $this->division_id = $id;

        $divisions = Division::find($id);
        $this->divisionname = $divisions->description;
        // dd($id);
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'view') {
            $this->user_id = $data['id'];
            $this->emitTo('hrim.employee-management.employee-information.view', 'mount', $data['id']);
            $this->view_modal = true;
        }
    }

    public function render(TeamStatusInterface $team_status_interface)
    {
        $request = [
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee_name,
        ];

        $response = $team_status_interface->showTeam($this->division_id, $request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [
                'team_records' => [],
            ];
        }

        return view('livewire.hrim.attendance.team-status.view', [
            'team_records' => $response['result']
        ]);
    }
}
