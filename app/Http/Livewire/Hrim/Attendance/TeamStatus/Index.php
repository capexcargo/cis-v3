<?php

namespace App\Http\Livewire\Hrim\Attendance\TeamStatus;

use App\Interfaces\Hrim\Attendance\TeamStatusInterface;
use Livewire\Component;

class Index extends Component
{
    public function render(TeamStatusInterface $team_status_interface)
    {
        $request = [];

        $response = $team_status_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }
        return view('livewire.hrim.attendance.team-status.index', [
            'divisions' => $response['result']
        ]);
    }
}
