<?php

namespace App\Http\Livewire\Hrim\Attendance\ScheduleAdjustment;

use App\Interfaces\Hrim\Attendance\LeaveRecordsInterface;
use App\Interfaces\Hrim\Attendance\ScheduleAdjustmentInterface;
use App\Models\UserDetails;
use App\Repositories\Hrim\Attendance\ScheduleAdjustmentRepository;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class View extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $action_type;
    public $confirmation_modal = false;
    public $confirmation_message;
    public $approver;
    public $is_approved;
    public $remarks;

    public $approval_modal = false;
    public $view_approval_status_modal = false;
    public $schedule_adjustment_id;

    public $user_id;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $divsionhead;
    public $divsionhead_id;
    public $seg = [];
    public $seg_id = [];

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function mount($id)
    {
        $this->user_id = $id;

        $this->divsionhead = UserDetails::whereHas('position', function ($query) {
            $query->whereIn('job_level_id', [1, 2])
                ->where('division_id', Auth::user()->division_id);
        })->select('user_id')->first();
        $this->divsionhead_id = $this->divsionhead["user_id"];


        $this->seg = UserDetails::whereHas('position', function ($query) {
            $query->whereIn('job_level_id', [1])
                ->where('division_id', 8);
        })->get();

        foreach ($this->seg as $val) {
            $this->seg_id[] = $val['user_id'];
        }
    }

    public function action(array $data, $action_type)
    {
        $this->resetErrorBag();
        $this->action_type = $action_type;
        if ($action_type == 'approval') {
            $this->emitTo('hrim.attendance.schedule-adjustment.approval', 'mount', $data['id']);
            $this->schedule_adjustment_id = $data['id'];
            $this->approval_modal = true;
        } elseif ($action_type == 'view_approval_status') {
            $this->emitTo('hrim.attendance.tar.view-approval-status', 'mount', $data['id'], $this->user_id);
            $this->schedule_adjustment_id = $data['id'];
            $this->view_approval_status_modal = true;
        } elseif ($action_type == 'approve') {
            $this->schedule_adjustment_id = $data['id'];
            $this->approver = $data['approver'];
            $this->is_approved = 1;

            $this->confirmation_message = "Are you sure you want to approve this SAR?";
            $this->confirmation_modal = true;
        } elseif ($action_type == 'decline') {
            $this->schedule_adjustment_id = $data['id'];
            $this->approver = $data['approver'];
            $this->is_approved = 0;

            $schedule_adjustment_repository = new ScheduleAdjustmentRepository();
            $response = $schedule_adjustment_repository->show($this->schedule_adjustment_id, $this->getRequest());
            if ($response['code'] == 200) {
                $schedule_adjustment = $response['result'];
                if ($this->approver == 'first') {
                    $this->remarks = $schedule_adjustment->first_approver_remarks;
                } elseif ($this->approver == 'second') {
                    $this->remarks = $schedule_adjustment->second_approver_remarks;
                } elseif ($this->approver == 'third') {
                    $this->remarks = $schedule_adjustment->third_approver_remarks;
                }
            }

            $this->confirmation_message = "Are you sure you want to decline this TAR?";
            $this->confirmation_modal = true;
        }
    }

    public function confirm(ScheduleAdjustmentInterface $schedule_adjustment_interface)
    {
        $response = [];
        if ($this->action_type == 'approve') {
            $response = $schedule_adjustment_interface->approve($this->schedule_adjustment_id, $this->getRequest());
        } elseif ($this->action_type == 'decline') {
            $response = $schedule_adjustment_interface->approve($this->schedule_adjustment_id, $this->getRequest());
        }

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.attendance.schedule-adjustment.view', 'close_modal', 'confirmation');
            $this->emitTo('hrim.attendance.schedule-adjustment.view', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            if (is_array($response['result'])) {
                foreach ($response['result'] as $a => $messages) {
                    $this->addError($a, $messages);
                }
            } else {
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'approval') {
            $this->approval_modal = false;
        } elseif ($action_type == 'confirmation') {
            $this->confirmation_modal = false;
        }
    }

    public function getRequest()
    {
        return [
            'user_id' => $this->user_id,
            'approver' => $this->approver,
            'is_approved' => $this->is_approved,
            'remarks' => $this->remarks,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'approver',
            'is_approved',
            'remarks',
        ]);
    }

    public function render(ScheduleAdjustmentInterface $schedule_adjustment_interface)
    {
        $request = [
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $schedule_adjustment_interface->showUserScheduleAdjustment($this->user_id, $request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [
                'user' => null,
                'schedule_adjustments' => [],
            ];
        }

        return view('livewire.hrim.attendance.schedule-adjustment.view', [
            'user' => $response['result']['user'],
            'schedule_adjustments' => $response['result']['schedule_adjustments'],
        ]);
    }
}
