<?php

namespace App\Http\Livewire\Hrim\Attendance\ScheduleAdjustment;

use App\Interfaces\Hrim\Attendance\ScheduleAdjustmentInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $month;
    public $cut_off;
    public $year;

    public $sortField = 'created_at';
    public $sortAsc = true;
    public $paginate = 10;

    public $request = [];

    protected $listeners = ['mount' => 'mount'];

    public function mount($request = [])
    {
        if (!$request) {
            $this->month  = (int) date('m');
            $this->cut_off  = 'second';
            $this->year  = date('Y');

            return $this->request = [
                'month' => $this->month,
                'cut_off' => $this->cut_off,
                'year' => $this->year,
                'sort_field' => $this->sortField,
                'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
                'paginate' => $this->paginate,
            ];
        }

        $this->request = $request;
    }

    public function render(ScheduleAdjustmentInterface $schedule_adjustment_interface)
    {
        $response = $schedule_adjustment_interface->index($this->request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.attendance.schedule-adjustment.index', [
            'users' => $response['result']
        ]);
    }
}
