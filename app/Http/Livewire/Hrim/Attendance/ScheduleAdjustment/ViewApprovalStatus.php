<?php

namespace App\Http\Livewire\Hrim\Attendance\ScheduleAdjustment;

use App\Interfaces\Hrim\Attendance\ScheduleAdjustmentInterface;
use App\Interfaces\Hrim\Attendance\TarInterface;
use Livewire\Component;

class ViewApprovalStatus extends Component
{
    public $schedule_adjustment;

    protected $listeners = ['mount' => 'mount'];

    public function mount(ScheduleAdjustmentInterface $schedule_adjustment_interface, $id, $user_id)
    {
        $request = [
            'user_id' => $user_id
        ];
        $response = $schedule_adjustment_interface->show($id, $request);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->schedule_adjustment = $response['result'];
    }

    public function render()
    {
        return view('livewire.hrim.attendance.schedule-adjustment.view-approval-status');
    }
}
