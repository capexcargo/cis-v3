<?php

namespace App\Http\Livewire\Hrim\Attendance\ScheduleAdjustment;

use App\Interfaces\Hrim\Attendance\ScheduleAdjustmentInterface;
use App\Traits\Hrim\Attendance\ScheduleAdjustmentTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Approval extends Component
{
    use ScheduleAdjustmentTrait, PopUpMessagesTrait;

    public function mount(ScheduleAdjustmentInterface $schedule_adjustment_interface, $id, $user_id)
    {
        $this->resetForm();
        $request = [
            'user_id' => $user_id
        ];
        $response = $schedule_adjustment_interface->show($id, $request);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->schedule_adjustment = $response['result'];
        $this->getEmployeeInformation($this->schedule_adjustment->user_id);
        $this->new_schedule = $this->schedule_adjustment->new_work_schedule_id;
        $this->updatedNewSchedule();
        $this->new_inclusive_date_start_date = $this->schedule_adjustment->inclusive_date_from;
        $this->new_inclusive_date_end_date = $this->schedule_adjustment->inclusive_date_to;
        $this->set_an_end_date = ($this->schedule_adjustment->inclusive_date_to ? true : false);
        $this->reason = $this->schedule_adjustment->reason;

        $this->user_id = $user_id;
        $this->first_approver_id = $this->schedule_adjustment->first_approver;
        $this->second_approver_id = $this->schedule_adjustment->second_approver;
        $this->third_approver_id = $this->schedule_adjustment->third_approver;
        $this->is_first_approved = $this->schedule_adjustment->first_status;
        $this->is_second_approved = $this->schedule_adjustment->second_status;
        $this->is_third_approved = $this->schedule_adjustment->third_status;
        $this->first_approver_remarks = $this->schedule_adjustment->first_approver_remarks;
        $this->second_approver_remarks = $this->schedule_adjustment->second_approver_remarks;
        $this->third_approver_remarks = $this->schedule_adjustment->third_approver_remarks;
    }

    public function submit(ScheduleAdjustmentInterface $schedule_adjustment_interface)
    {
        $response = $schedule_adjustment_interface->approve($this->schedule_adjustment->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.attendance.schedule-adjustment.view', 'close_modal', 'approval');
            $this->emitTo('hrim.attendance.schedule-adjustment.view', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            if (is_array($response['result'])) {
                foreach ($response['result'] as $a => $messages) {
                    $this->addError($a, $messages);
                }
            } else {
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
            }
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }
    
    public function render()
    {
        return view('livewire.hrim.attendance.schedule-adjustment.approval');
    }
}
