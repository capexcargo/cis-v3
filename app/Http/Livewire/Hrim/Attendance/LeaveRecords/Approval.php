<?php

namespace App\Http\Livewire\Hrim\Attendance\LeaveRecords;

use App\Interfaces\Hrim\Attendance\LeaveRecordsInterface;
use App\Traits\Hrim\Attendance\LeaveRecordsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Approval extends Component
{
    use LeaveRecordsTrait, PopUpMessagesTrait;

    public function mount(LeaveRecordsInterface $leave_records_interface, $id, $user_id)
    {
        $this->resetForm();
        $request = [
            'user_id' => $user_id
        ];
        $response = $leave_records_interface->show($id, $request);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->leave = $response['result'];
        $this->inclusive_date_from = $this->leave->inclusive_date_from;
        $this->inclusive_date_to = $this->leave->inclusive_date_to;
        $this->resume_of_work = $this->leave->resume_date;
        $this->type_of_leave = $this->leave->leave_type_id;
        $this->is_with_medical_certificate = $this->leave->is_with_medical_certificate;
        $this->apply_for = $this->leave->leave_day_type_id;
        $this->is_with_pay = $this->leave->is_with_pay;
        $this->reason = $this->leave->reason;
        $this->reliever = $this->leave->reliever;

        $this->user_id = $user_id;
        $this->first_approver_id = $this->leave->first_approver;
        $this->second_approver_id = $this->leave->second_approver;
        $this->third_approver_id = $this->leave->third_approver;
        $this->is_first_approved = $this->leave->first_status;
        $this->is_second_approved = $this->leave->second_status;
        $this->is_third_approved = $this->leave->third_status;
        $this->first_approver_remarks = $this->leave->first_approver_remarks;
        $this->second_approver_remarks = $this->leave->second_approver_remarks;
        $this->third_approver_remarks = $this->leave->third_approver_remarks;
    }

    public function submit(LeaveRecordsInterface $leave_records_interface)
    {
        $response = $leave_records_interface->approve($this->leave->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.attendance.leave-records.view', 'close_modal', 'approval');
            $this->emitTo('hrim.attendance.leave-records.view', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            if (is_array($response['result'])) {
                foreach ($response['result'] as $a => $messages) {
                    $this->addError($a, $messages);
                }
            } else {
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.attendance.leave-records.approval');
    }
}
