<?php

namespace App\Http\Livewire\Hrim\Attendance\LeaveRecords;

use App\Interfaces\Hrim\Attendance\LeaveRecordsInterface;
use App\Models\UserDetails;
use App\Repositories\Hrim\Attendance\LeaveRecordsRepository;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class View extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $action_type;
    public $confirmation_modal = false;
    public $confirmation_message;
    public $approver;
    public $is_approved;
    public $remarks;

    public $attachment_modal = false;
    public $approval_modal = false;
    public $view_approval_status_modal = false;
    public $leave_id;

    public $user_id;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $divsionhead;
    public $divsionhead_id;
    public $seg = [];
    public $seg_id = [];

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function mount($id)
    {
        $this->user_id = $id;

        $this->divsionhead = UserDetails::whereHas('position', function ($query) {
            $query->whereIn('job_level_id', [1, 2])
                ->where('division_id', Auth::user()->division_id);
        })->select('user_id')->first();
        $this->divsionhead_id = $this->divsionhead["user_id"];


        $this->seg = UserDetails::whereHas('position', function ($query) {
            $query->whereIn('job_level_id', [1])
                ->where('division_id', 8);
        })->get();

        foreach ($this->seg as $val) {
            $this->seg_id[] = $val['user_id'];
        }
    }

    public function action(array $data, $action_type)
    {
        $this->resetErrorBag();
        $this->action_type = $action_type;
        if ($action_type == 'attachment') {
            $this->emitTo('hrim.attendance.leave-records.attachment', 'mount', $data['id']);
            $this->leave_id = $data['id'];
            $this->attachment_modal = true;
        } elseif ($action_type == 'approval') {
            $this->emitTo('hrim.attendance.leave-records.approval', 'mount', $data['id']);
            $this->leave_id = $data['id'];
            $this->approval_modal = true;
        } elseif ($action_type == 'view_approval_status') {
            $this->emitTo('hrim.attendance.leave-records.view-approval-status', 'mount', $data['id'], $this->user_id);
            $this->leave_id = $data['id'];
            $this->view_approval_status_modal = true;
        } elseif ($action_type == 'approve') {
            $this->leave_id = $data['id'];
            $this->approver = $data['approver'];
            $this->is_approved = 1;

            $this->confirmation_message = "Are you sure you want to approve this Leave?";
            $this->confirmation_modal = true;
        } elseif ($action_type == 'decline') {
            $this->leave_id = $data['id'];
            $this->approver = $data['approver'];
            $this->is_approved = 0;

            $leave_records_repository = new LeaveRecordsRepository();
            $response = $leave_records_repository->show($this->leave_id, $this->getRequest());
            if ($response['code'] == 200) {
                $leave_record = $response['result'];
                if ($this->approver == 'first') {
                    $this->remarks = $leave_record->first_approver_remarks;
                } elseif ($this->approver == 'second') {
                    $this->remarks = $leave_record->second_approver_remarks;
                } elseif ($this->approver == 'third') {
                    $this->remarks = $leave_record->third_approver_remarks;
                }
            }

            $this->confirmation_message = "Are you sure you want to decline this Leave?";
            $this->confirmation_modal = true;
        }
    }

    public function confirm(LeaveRecordsInterface $leave_records_interface)
    {
        $response = [];
        if ($this->action_type == 'approve') {
            $response = $leave_records_interface->approve($this->leave_id, $this->getRequest());
        } elseif ($this->action_type == 'decline') {
            $response = $leave_records_interface->approve($this->leave_id, $this->getRequest());
        }

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.attendance.leave-records.view', 'close_modal', 'confirmation');
            $this->emitTo('hrim.attendance.leave-records.view', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            if (is_array($response['result'])) {
                foreach ($response['result'] as $a => $messages) {
                    $this->addError($a, $messages);
                }
            } else {
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'approval') {
            $this->approval_modal = false;
        } elseif ($action_type == 'attachment') {
            $this->attachment_modal = false;
        } elseif ($action_type == 'confirmation') {
            $this->confirmation_modal = false;
        }
    }

    public function getRequest()
    {
        return [
            'user_id' => $this->user_id,
            'approver' => $this->approver,
            'is_approved' => $this->is_approved,
            'remarks' => $this->remarks,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'approver',
            'is_approved',
            'remarks',
        ]);
    }

    public function render(LeaveRecordsInterface $leave_records_interface)
    {
        $request = [
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $leave_records_interface->showUserLeaves($this->user_id, $request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [
                'user' => null,
                'leaves' => [],
            ];
        }

        return view('livewire.hrim.attendance.leave-records.view', [
            'user' => $response['result']['user'],
            'leaves' => $response['result']['leaves'],
        ]);
    }
}
