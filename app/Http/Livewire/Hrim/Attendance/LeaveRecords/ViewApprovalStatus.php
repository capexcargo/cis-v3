<?php

namespace App\Http\Livewire\Hrim\Attendance\LeaveRecords;

use App\Interfaces\Hrim\Attendance\LeaveRecordsInterface;
use Livewire\Component;

class ViewApprovalStatus extends Component
{
    public $leave;

    protected $listeners = ['mount' => 'mount'];

    public function mount(LeaveRecordsInterface $leave_records_interface, $id, $user_id)
    {
        $request = [
            'user_id' => $user_id
        ];
        $response = $leave_records_interface->show($id, $request);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->leave = $response['result'];
    }

    public function render()
    {
        return view('livewire.hrim.attendance.leave-records.view-approval-status');
    }
}
