<?php

namespace App\Http\Livewire\Hrim\Attendance\DailyTimeRecords;

use App\Interfaces\Hrim\Attendance\DailyTimeRecordsInterface;
use App\Models\BranchReference;
use App\Models\Hrim\CutOffManagement;
use App\Models\Hrim\EmploymentCategory;
use App\Models\MonthReference;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $date;
    public $branch;
    public $employment_category;
    public $employee_id;
    public $employee_name;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 20;

    public $branch_references = [];
    public $employment_category_references = [];
    public $month_references = [];

    public $month;
    public $cut_off;
    public $year;

    protected $listeners = ['index' => 'render'];

    public function mount()
    {
        $this->date = date('Y-m-d');

        $cutoff = CutOffManagement::where('cutoff_date', date('Y-m-d'))->first();

        if ($cutoff->payout_cutoff == 1) {
            $cutoffs = 'first';
        } else {
            $cutoffs = 'second';
        }

        $this->month = $cutoff->payout_month;
        $this->cut_off = $cutoffs;
        $this->year = date('Y');
    }
    
    public function load()
    {
        $this->loadBranchReference();
        $this->loadEmploymentCategoryReference();
        $this->loadMonthReference();
    }

    public function loadBranchReference()
    {
        $this->branch_references = BranchReference::get();
    }

    public function loadEmploymentCategoryReference()
    {
        $this->employment_category_references = EmploymentCategory::get();
    }

    public function loadMonthReference()
    {
        $this->month_references = MonthReference::get();
    }
    public function render(DailyTimeRecordsInterface $daily_time_records)
    {
        $request = [
            'month' => $this->month,
            'cut_off' => $this->cut_off,
            'year' => $this->year,
            'date' => $this->date,
            'branch' => $this->branch,
            'employment_category' => $this->employment_category,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee_name,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

   
        $response = $daily_time_records->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.attendance.daily-time-records.index', [
            'time_logs' => $response['result']
        ]);
    }
}
