<?php

namespace App\Http\Livewire\Hrim\Attendance\Schedules;

use App\Interfaces\Hrim\Attendance\SchedulesInterface;
use App\Models\BranchReference;
use App\Models\Hrim\CutOffManagement;
use App\Models\Hrim\EmploymentCategory;
use App\Models\MonthReference;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Index extends Component
{
    use PopUpMessagesTrait;

    public $status = 'tar_all';

    public $month;
    public $cut_off;
    public $year;

    public $branch;
    public $employment_category;
    public $employee_id;
    public $employee_name;
    public $sortField = 'created_at';
    public $sortAsc = true;
    public $paginate = 10;

    public $month_references = [];
    public $branch_references = [];
    public $employment_category_references = [];

    protected $listeners = ['index' => 'render'];

    public function mount()
    {
        $cutoff = CutOffManagement::where('cutoff_date', date('Y-m-d'))->first();

        if ($cutoff->payout_cutoff == 1) {
            $cutoffs = 'first';
        } else {
            $cutoffs = 'second';
        }

        $this->month = $cutoff->payout_month;
        $this->cut_off = $cutoffs;
        $this->year = date('Y');
    }

    public function load()
    {
        $this->loadMonthReference();
        $this->loadBranchReference();
        $this->loadEmploymentCategoryReference();
    }

    public function loadMonthReference()
    {
        $this->month_references = MonthReference::get();
    }

    public function loadBranchReference()
    {
        $this->branch_references = BranchReference::get();
    }

    public function loadEmploymentCategoryReference()
    {
        $this->employment_category_references = EmploymentCategory::get();
    }

    protected function updated()
    {
        if ($this->status == 'tar_all' || $this->status == 'tar_for_approval') {
            $this->emitTo('hrim.attendance.tar.index', 'mount', $this->getRequest());
        } elseif ($this->status == 'sar_all' || $this->status == 'sar_for_approval') {
            $this->emitTo('hrim.attendance.schedule-adjustment.index', 'mount', $this->getRequest());
        }
    }

    public function getRequest()
    {
        return [
            'status' => $this->status,
            'month' => $this->month,
            'cut_off' => $this->cut_off,
            'year' => $this->year,
            'branch' => $this->branch,
            'employment_category' => $this->employment_category,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee_name,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];
    }

    public function render(SchedulesInterface $schedules_interface)
    {
        $request = $this->getRequest();

        $response = $schedules_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.attendance.schedules.index', [
            'request' => $request,
            'result' => $response['result'],
        ]);
    }
}
