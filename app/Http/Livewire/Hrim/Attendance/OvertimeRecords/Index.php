<?php

namespace App\Http\Livewire\Hrim\Attendance\OvertimeRecords;

use App\Interfaces\Hrim\Attendance\OvertimeRecordsInterface;
use App\Models\BranchReference;
use App\Models\Hrim\CutOffManagement;
use App\Models\Hrim\EmploymentCategory;
use App\Models\Hrim\EmploymentCategoryType;
use App\Models\MonthReference;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $status = 'all';

    public $month;
    public $cut_off;
    public $year;

    public $branch;
    public $employment_category;
    public $employment_category_type;
    public $employee_id;
    public $employee_name;
    public $sortField = 'created_at';
    public $sortAsc = true;
    public $paginate = 10;

    public $month_references = [];
    public $branch_references = [];
    public $employment_category_references = [];
    public $employment_category_type_references = [];

    protected $listeners = ['index' => 'render'];

    public function mount()
    {
        $cutoff = CutOffManagement::where('cutoff_date', date('Y-m-d'))->first();

        if ($cutoff->payout_cutoff == 1) {
            $cutoffs = 'first';
        } else {
            $cutoffs = 'second';
        }

        $this->month = $cutoff->payout_month;
        $this->cut_off = $cutoffs;
        $this->year = date('Y');
    }

    public function load()
    {
        $this->loadMonthReference();
        $this->loadBranchReference();
        $this->loadEmploymentCategoryReference();
        $this->loadEmploymentCategoryTypeReference();
    }

    public function loadMonthReference()
    {
        $this->month_references = MonthReference::get();
    }

    public function loadBranchReference()
    {
        $this->branch_references = BranchReference::get();
    }

    public function loadEmploymentCategoryReference()
    {
        $this->employment_category_references = EmploymentCategory::get();
    }

    public function loadEmploymentCategoryTypeReference()
    {
        $this->employment_category_type_references = EmploymentCategoryType::get();
    }

    public function render(OvertimeRecordsInterface $overtime_records_interface)
    {
        $request = [
            'status' => $this->status,
            'month' => $this->month,
            'cut_off' => $this->cut_off,
            'year' => $this->year,
            'branch' => $this->branch,
            'employment_category' => $this->employment_category,
            'employment_category_type' => $this->employment_category_type,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee_name,
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $overtime_records_interface->index($request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [
                'users_overtime_records_all' => 0,
                'users_overtime_records_for_approval' => 0,
                'users' => [],
            ];
        }
        
        return view('livewire.hrim.attendance.overtime-records.index', [
            'users_overtime_records_all' => $response['result']['users_overtime_records_all'],
            'users_overtime_records_for_approval' => $response['result']['users_overtime_records_for_approval'],
            'users' => $response['result']['users']
        ]);
    }
}
