<?php

namespace App\Http\Livewire\Hrim\Attendance\OvertimeRecords;

use App\Interfaces\Hrim\Attendance\OvertimeRecordsInterface;
use App\Models\Hrim\Position;
use App\Models\UserDetails;
use App\Repositories\Hrim\Attendance\OvertimeRecordsRepository;
use App\Traits\PopUpMessagesTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class View extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $action_type;
    public $confirmation_modal = false;
    public $confirmation_message;
    public $approver;
    public $is_approved;
    public $remarks;

    public $name;
    public $date_filed;
    public $ot_date;
    public $day;
    public $actual_time_in;
    public $actual_time_out;
    public $hours_rendered;
    public $ot_request;
    public $reason;
    public $approved_ot;

    public $approval_modal = false;
    public $view_approval_status_modal = false;
    public $overtime_id;

    public $user_id;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $divsionhead;
    public $divsionhead_id;
    public $seg = [];
    public $seg_id = [];

    protected $listeners = ['index' => 'render', 'close_modal' => 'closeModal'];

    public function mount($id)
    {
        $this->user_id = $id;

        $this->divsionhead = UserDetails::whereHas('position', function ($query) {
            $query->whereIn('job_level_id', [1, 2])
                ->where('division_id', Auth::user()->division_id);
        })->select('user_id')->first();
        $this->divsionhead_id = $this->divsionhead["user_id"];


        $this->seg = UserDetails::whereHas('position', function ($query) {
            $query->whereIn('job_level_id', [1])
                ->where('division_id', 8);
        })->get();

        foreach ($this->seg as $val) {
            $this->seg_id[] = $val['user_id'];
        }
    }

    public function action(array $data, $action_type)
    {
        $this->resetErrorBag();
        $this->action_type = $action_type;
        if ($action_type == 'approval') {
            $this->emitTo('hrim.attendance.overtime-records.approval', 'mount', $data['id'], $this->user_id);
            $this->overtime_id = $data['id'];
            $this->approval_modal = true;
        } elseif ($action_type == 'view_approval_status') {
            $this->emitTo('hrim.attendance.overtime-records.view-approval-status', 'mount', $data['id'], $this->user_id);
            $this->overtime_id = $data['id'];
            $this->view_approval_status_modal = true;
        } elseif ($action_type == 'approve') {
            $this->overtime_id = $data['id'];
            $this->approver = $data['approver'];
            $this->is_approved = 1;

            $overtime_records_repository = new OvertimeRecordsRepository();
            $response = $overtime_records_repository->show($this->overtime_id, $this->getRequest());
            if ($response['code'] == 200) {
                $overtime_record = $response['result'];

                $this->name = $overtime_record->user->name;
                $this->date_filed = $overtime_record->created_at;
                $this->ot_date = $overtime_record->date_time_from;
                $this->day = $overtime_record->date_time_from;
                $this->actual_time_in = $overtime_record->time_from;
                $this->actual_time_out = $overtime_record->time_to;
                $this->hours_rendered = $overtime_record->overtime_rendered;
                $this->ot_request = $overtime_record->overtime_request;
                $this->reason = $overtime_record->reason;
                $this->approved_ot = $overtime_record->overtime_approved;

                if ($this->approver == 'first') {
                    $this->remarks = $overtime_record->first_approver_remarks;
                } elseif ($this->approver == 'second') {
                    $this->remarks = $overtime_record->second_approver_remarks;
                } elseif ($this->approver == 'third') {
                    $this->remarks = $overtime_record->third_approver_remarks;
                }
            }

            $this->confirmation_message = "Are you sure you want to approve this OT Request?";
            $this->confirmation_modal = true;
        } elseif ($action_type == 'decline') {
            $this->overtime_id = $data['id'];
            $this->approver = $data['approver'];
            $this->is_approved = 0;

            $overtime_records_repository = new OvertimeRecordsRepository();
            $response = $overtime_records_repository->show($this->overtime_id, $this->getRequest());
            if ($response['code'] == 200) {
                $overtime_record = $response['result'];

                $this->name = $overtime_record->user->name;
                $this->date_filed = date('F d, Y', strtotime($overtime_record->created_at));
                $this->ot_date = date('F d, Y', strtotime($overtime_record->date_time_from));
                $this->day = date('l ', strtotime($overtime_record->date_time_from));
                $this->actual_time_in = date('h:i A', strtotime($overtime_record->time_from));
                $this->actual_time_out = date('h:i A', strtotime($overtime_record->time_to));
                $this->hours_rendered = $overtime_record->overtime_rendered;
                $this->ot_request = $overtime_record->overtime_request;
                $this->reason = $overtime_record->reason;
                $this->approved_ot = $overtime_record->overtime_approved;

                if ($this->approver == 'first') {
                    $this->remarks = $overtime_record->first_approver_remarks;
                } elseif ($this->approver == 'second') {
                    $this->remarks = $overtime_record->second_approver_remarks;
                } elseif ($this->approver == 'third') {
                    $this->remarks = $overtime_record->third_approver_remarks;
                }
            }

            $this->confirmation_message = "Are you sure you want to decline this OT Request?";
            $this->confirmation_modal = true;
        }
    }

    public function confirm(OvertimeRecordsRepository $overtime_records)
    {
        $response = [];
        if ($this->action_type == 'approve') {
            $response = $overtime_records->approve($this->overtime_id, $this->getRequest());
        } elseif ($this->action_type == 'decline') {
            $response = $overtime_records->approve($this->overtime_id, $this->getRequest());
        }

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.attendance.overtime-records.view', 'close_modal', 'confirmation');
            $this->emitTo('hrim.attendance.overtime-records.view', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            if (is_array($response['result'])) {
                foreach ($response['result'] as $a => $messages) {
                    $this->addError($a, $messages);
                }
            } else {
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'approval') {
            $this->approval_modal = false;
        } elseif ($action_type == 'confirmation') {
            $this->confirmation_modal = false;
        }
    }

    public function getRequest()
    {
        return [
            'user_id' => $this->user_id,
            'approver' => $this->approver,
            'is_approved' => $this->is_approved,
            'remarks' => $this->remarks,

            'approved_ot' => $this->approved_ot,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'approver',
            'is_approved',
            'remarks',

            'approved_ot',
        ]);
    }

    public function render(OvertimeRecordsInterface $overtime_records_interface)
    {
        $request = [
            'sort_field' => $this->sortField,
            'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $this->paginate,
        ];

        $response = $overtime_records_interface->showUserOvertimeRecords($this->user_id, $request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [
                'user' => null,
                'overtime_records' => [],
            ];
        }

        return view('livewire.hrim.attendance.overtime-records.view', [
            'user' => $response['result']['user'],
            'overtime_records' => $response['result']['overtime_records'],
        ]);
    }
}
