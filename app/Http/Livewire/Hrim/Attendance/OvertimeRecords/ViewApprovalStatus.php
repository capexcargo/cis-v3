<?php

namespace App\Http\Livewire\Hrim\Attendance\OvertimeRecords;

use App\Interfaces\Hrim\Attendance\OvertimeRecordsInterface;
use Livewire\Component;

class ViewApprovalStatus extends Component
{
    public $overtime_record;

    protected $listeners = ['mount' => 'mount'];

    public function mount(OvertimeRecordsInterface $overtime_records_interface, $id, $user_id)
    {
        $request = [
            'user_id' => $user_id
        ];
        $response = $overtime_records_interface->show($id, $request);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->overtime_record = $response['result'];
    }

    public function render()
    {
        return view('livewire.hrim.attendance.overtime-records.view-approval-status');
    }
}
