<?php

namespace App\Http\Livewire\Hrim\Attendance\OvertimeRecords;

use App\Interfaces\Hrim\Attendance\OvertimeRecordsInterface;
use App\Traits\Hrim\Attendance\OvertimeRecordsTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Approval extends Component
{
    use OvertimeRecordsTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(OvertimeRecordsInterface $overtime_records_interface, $id, $user_id)
    {
        $this->resetForm();
        $request = [
            'user_id' => $user_id
        ];
        $response = $overtime_records_interface->show($id, $request);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        // dd($response);
        $this->overtime_record = $response['result'];
        $this->month  = (int) date('m', strtotime($this->overtime_record->date_time_from));
        $this->cut_off  = 'first';

        $day = (int) date('d', strtotime($this->overtime_record->date_time_from));
        if ($day >= 11 && $day <= 26) {
            $this->cut_off  = 'second';
        }
        $this->year  = date('Y', strtotime($this->overtime_record->date_time_from));
        $this->getOvertimed();

        $this->date_time_in = $this->overtime_record->date_time_from;
        $this->updatedDateTimeIn();

        $this->type_of_ot = $this->overtime_record->date_category_id;
        $this->actual_date_from = $this->overtime_record->date_time_from;
        $this->actual_date_to = $this->overtime_record->date_time_to;
        $this->from = date('H:i', strtotime($this->overtime_record->time_from));
        $this->to = date('H:i', strtotime($this->overtime_record->time_to));
        $this->overtime_request = $this->overtime_record->overtime_request;
        $this->reason = $this->overtime_record->reason;

        $this->user_id = $user_id;
        $this->approved_overtime_hours = $this->overtime_record->overtime_approved;
        $this->first_approver_id = $this->overtime_record->first_approver;
        $this->second_approver_id = $this->overtime_record->second_approver;
        $this->third_approver_id = $this->overtime_record->third_approver;
        $this->is_first_approved = $this->overtime_record->first_status;
        $this->is_second_approved = $this->overtime_record->second_status;
        $this->is_third_approved = $this->overtime_record->third_status;
        $this->first_approver_remarks = $this->overtime_record->first_approver_remarks;
        $this->second_approver_remarks = $this->overtime_record->second_approver_remarks;
        $this->third_approver_remarks = $this->overtime_record->third_approver_remarks;
    }

    public function submit(OvertimeRecordsInterface $overtime_records_interface)
    {
        $response = $overtime_records_interface->approve($this->overtime_record->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.attendance.overtime-records.view', 'close_modal', 'approval');
            $this->emitTo('hrim.attendance.overtime-records.view', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            if (is_array($response['result'])) {
                foreach ($response['result'] as $a => $messages) {
                    $this->addError($a, $messages);
                }
            } else {
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
            }
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }


    public function render()
    {
        return view('livewire.hrim.attendance.overtime-records.approval');
    }
}
