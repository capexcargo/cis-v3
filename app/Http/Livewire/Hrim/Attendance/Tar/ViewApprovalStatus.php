<?php

namespace App\Http\Livewire\Hrim\Attendance\Tar;

use App\Interfaces\Hrim\Attendance\TarInterface;
use Livewire\Component;

class ViewApprovalStatus extends Component
{
    public $tar;

    protected $listeners = ['mount' => 'mount'];

    public function mount(TarInterface $tar_interface, $id, $user_id)
    {
        $request = [
            'user_id' => $user_id
        ];
        $response = $tar_interface->show($id, $request);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->tar = $response['result'];
    }

    public function render()
    {
        return view('livewire.hrim.attendance.tar.view-approval-status');
    }
}
