<?php

namespace App\Http\Livewire\Hrim\Attendance\Tar;

use App\Interfaces\Hrim\Attendance\TarInterface;
use App\Traits\Hrim\Attendance\TarTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Approval extends Component
{
    use TarTrait, PopUpMessagesTrait;

    protected $listeners = ['mount' => 'mount'];

    public function mount(TarInterface $tar_interface, $id, $user_id)
    {
        $this->resetForm();
        $request = [
            'user_id' => $user_id
        ];
        $response = $tar_interface->show($id, $request);
        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->tar = $response['result'];
        $this->date = $this->tar->date;
        $this->request_time_in = $this->tar->actual_time_in;
        $this->request_time_out = $this->tar->actual_time_out;
        $this->reason_of_adjustment = $this->tar->tar_reason_id;

        $this->user_id = $user_id;
        $this->first_approver_id = $this->tar->first_approver;
        $this->second_approver_id = $this->tar->second_approver;
        $this->third_approver_id = $this->tar->third_approver;
        $this->is_first_approved = $this->tar->first_status;
        $this->is_second_approved = $this->tar->second_status;
        $this->is_third_approved = $this->tar->third_status;
        $this->first_approver_remarks = $this->tar->first_approver_remarks;
        $this->second_approver_remarks = $this->tar->second_approver_remarks;
        $this->third_approver_remarks = $this->tar->third_approver_remarks;
    }

    public function submit(TarInterface $tar_interface)
    {
        $response = $tar_interface->approve($this->tar->id, $this->getRequest());

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emitTo('hrim.attendance.tar.view', 'close_modal', 'approval');
            $this->emitTo('hrim.attendance.tar.view', 'index');
            $this->sweetAlert('', $response['message']);
        } else if ($response['code'] == 400) {
            $this->resetErrorBag();
            if (is_array($response['result'])) {
                foreach ($response['result'] as $a => $messages) {
                    $this->addError($a, $messages);
                }
            } else {
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
            }

            return;
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.hrim.attendance.tar.approval');
    }
}
