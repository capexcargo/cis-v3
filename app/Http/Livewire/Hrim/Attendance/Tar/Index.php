<?php

namespace App\Http\Livewire\Hrim\Attendance\Tar;

use App\Interfaces\Hrim\Attendance\TarInterface;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $month;
    public $cut_off;
    public $year;

    public $sortField = 'created_at';
    public $sortAsc = true;
    public $paginate = 10;

    public $request = [];

    protected $listeners = ['mount' => 'mount'];

    public function mount($request = [])
    {
        if (!$request) {
            $this->month  = (int) date('m');
            $this->cut_off  = 'second';
            $this->year  = date('Y');

            return $this->request = [
                'month' => $this->month,
                'cut_off' => $this->cut_off,
                'year' => $this->year,
                'sort_field' => $this->sortField,
                'sort_type' => ($this->sortAsc  ? 'asc' : 'desc'),
                'paginate' => $this->paginate,
            ];
        }

        $this->request = $request;
    }

    public function render(TarInterface $tar_interface)
    {
        $response = $tar_interface->index($this->request);
        if ($response['code'] != 200) {
            $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
            $response['result'] = [];
        }

        return view('livewire.hrim.attendance.tar.index', [
            'users' => $response['result']
        ]);
    }
}
