<?php

namespace App\Http\Livewire\Admin\AccountManagement;

use App\Models\AccountStatusReference;
use App\Models\Division;
use App\Models\User;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination, PopUpMessagesTrait;

    public $create_modal = false;
    public $edit_modal = false;
    public $reactivate_modal = false;
    public $deactivate_modal = false;
    public $user_id;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $division = false;
    public $name;
    public $status;

    public $division_references = [];
    public $account_status_references = [];
    public $header_cards = [];

    protected $listeners = ['account_management_index' => 'render', 'load_head_cards' => 'loadHeadCards', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadHeadCards();
        $this->loadDivisionReference();
        $this->loadAccountStatusReference();
    }

    public function loadHeadCards()
    {
        $division_users = Division::withCount('users')->get();

        $this->header_cards = [
            [
                'title' => 'All',
                'value' => $division_users->sum('users_count'),
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-blue',
                'action' => 'division',
                'id' => false
            ],
            [
                'title' => $division_users[0]->name,
                'value' => $division_users[0]->users_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-blue',
                'action' => 'division',
                'id' => 1
            ],
            [
                'title' => $division_users[1]->name,
                'value' => $division_users[1]->users_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-yellow',
                'action' => 'division',
                'id' => 2
            ],
            [
                'title' => $division_users[2]->name,
                'value' => $division_users[2]->users_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-yellow',
                'action' => 'division',
                'id' => 3
            ],
            [
                'title' => $division_users[3]->name,
                'value' => $division_users[3]->users_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-yellow',
                'action' => 'division',
                'id' => 4
            ],
            [
                'title' => $division_users[4]->name,
                'value' => $division_users[4]->users_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-yellow',
                'action' => 'division',
                'id' => 5
            ],
            [
                'title' => $division_users[5]->name,
                'value' => $division_users[5]->users_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-yellow',
                'action' => 'division',
                'id' => 6
            ],
            [
                'title' => $division_users[6]->name,
                'value' => $division_users[6]->users_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-yellow',
                'action' => 'division',
                'id' => 7
            ]
        ];
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function loadAccountStatusReference()
    {
        $this->account_status_references = AccountStatusReference::where('is_visible', 1)->get();
    }

    public function action(array $data, $action_type)
    {
        if ($action_type == 'edit') {
            $this->emit('account_management_edit_mount', $data['id']);
            $this->edit_modal = true;
        } else if ($action_type == 'update_status') {
            if ($data['status_id'] == 1) {
                $this->reactivate_modal = true;
                return;
            }

            $this->deactivate_modal = true;
        }

        $this->user_id = $data['id'];
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function updateStatus($id, $value)
    {
        $user = User::findOrFail($id);
        $user->update([
            'status_id' => $value
        ]);

        $this->emit('load_head_cards');

        if ($value == 1) {
            $this->sweetAlert('success', 'Account Successfully Reactivated!');
            return;
        }

        $this->sweetAlert('success', 'Account Successfully Deactivated!');
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.admin.account-management.index', [
            'users' => User::with('division', 'status', 'role', 'userDetails')
                ->where('name', 'like', '%' . $this->name . '%')
                ->when($this->division, function ($query) {
                    $query->where('division_id', $this->division);
                })
                ->when($this->status, function ($query) {
                    $query->where('status_id', $this->status);
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)

        ]);
    }
}
