<?php

namespace App\Http\Livewire\Admin\AccountManagement;

use App\Interfaces\Globals\AccountManagementInterface;
use App\Models\User;
use App\Traits\Admin\AccountManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class CisRegistration extends Component
{
    use AccountManagementTrait, PopUpMessagesTrait;

    protected $queryString = ['employee_number', 'first_name', 'middle_name', 'last_name', 'email', 'mobile_number'];

    public function mount()
    {
        // http://cis-v3.test/cis-registration?employee_number=917&first_name=Devin&middle_name=M&last_name=Mcmahon&email=zafikewob%40mailinator.com&mobile_number=09612155595
        // $this->division = 6;

        if (!$this->employee_number && !$this->first_name && !$this->last_name && !$this->email) {
            abort(404);
        }
    }

    // !”#$%&*+,-.:;<=>?@^_`~
    public function submit(AccountManagementInterface $account_management_interface)
    {
        $this->level = 1;
        $this->role = 3;

        $validated = $this->validate([
            'employee_number' => 'required|unique:user_details,employee_number',
            'first_name' => 'required',
            'middle_name' => 'sometimes',
            'last_name' => 'required',
            'mobile_number' => 'required|numeric|digits:11',
            'telephone_number' => 'nullable|numeric|digits_between:8, 10',
            'suffix' => 'sometimes',
            'division' => 'required',
            'level' => 'required',
            'role' => 'required',
            'email' => 'required|email:rfc,dns|unique:users,email',
            'password' => 'required|confirmed|string|min:8|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[!”#$%&*+,-.:;<=>?@^_`~]/',
            'password_confirmation' => 'required',
        ], [
            'password.regex' => 'Password must be contain at least one uppercase and lowercase letter, one number and one special character'
        ]);

        $response = $account_management_interface->create($validated, $validated['division']);
        
        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emit('account_management_index');
            $this->sweetAlert('success', $response['message']);
            return redirect()->to('/login');
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        $user = User::whereHas('userDetails', function ($query) {
            $query->where('employee_number', $this->employee_number);
        })->first();
        if ($user) {
            return view('livewire.all.login');
        } else {
            return view('livewire.admin.account-management.cis-registration');
        }
    }
}
