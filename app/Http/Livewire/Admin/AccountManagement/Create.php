<?php

namespace App\Http\Livewire\Admin\AccountManagement;

use App\Interfaces\Globals\AccountManagementInterface;
use App\Traits\Admin\AccountManagementTrait;
use App\Traits\PopUpMessagesTrait;
use Livewire\Component;

class Create extends Component
{
    use AccountManagementTrait, PopUpMessagesTrait;

    public function submit(AccountManagementInterface $AccountManagementInterface)
    {
        $validated = $this->validate([
            'employee_number' => 'required',
            'first_name' => 'required',
            'middle_name' => 'sometimes',
            'last_name' => 'required',
            'mobile_number' => 'required|numeric|digits:11',
            'telephone_number' => 'nullable|numeric|digits_between:8, 10',
            'suffix' => 'sometimes',
            'division' => 'required',
            'level' => 'required',
            'role' => 'required',
            'email' => 'required|email:rfc,dns|unique:users,email',
            'password' => 'required|confirmed|string|min:8|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/',
            'password_confirmation' => 'required',
        ], [
            'password.regex' => 'Password must be contain at least one uppercase and lowercase letter, one number and one special character'
        ]);

        $response = $AccountManagementInterface->create($validated, $validated['division']);

        if ($response['code'] == 200) {
            $this->resetForm();
            $this->emit('account_management_index');
            $this->sweetAlert('success', $response['message']);
        } else {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function render()
    {
        return view('livewire.admin.account-management.create');
    }
}
