<?php

namespace App\Http\Livewire\Admin\SecurityManagement\RolesManagement;

use App\Models\Division;
use App\Models\RolesParentAccessReference;
use App\Traits\Admin\SecurityManagement\RoleManagementTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Create extends Component
{
    use RoleManagementTrait, WithPagination;

    protected $listeners = ['load_parent_access' => 'render', 'close_modal' => 'closeModal'];

    public function submit()
    {
        $validated = $this->validate([
            'division' => 'required',
            'level' => 'required',
            'role_name' => 'required',
            'selected_accesses' => 'sometimes'
        ]);

        $division = Division::find($validated['division']);

        $role = $division->roles()->create([
            'level_id' => $validated['level'],
            'code' => strtolower(str_replace(" ", "_", $validated['role_name'])),
            'display' => $validated['role_name']
        ]);

        foreach ($validated['selected_accesses'] as $selected_access) {
            $role->accesses()->create([
                'access_id' => $selected_access
            ]);
        }

        $this->resetForm();
        $this->emit('roles_management_index');
        $this->emit('load_head_cards');
        $this->dispatchBrowserEvent('swal', [
            'icon' => 'success',
            'title' => 'Role Successfully Created!',
        ]);
    }

    public function render()
    {
        return view('livewire.admin.security-management.roles-management.create', [
            'parent_accesses' => RolesParentAccessReference::with(['accesses' => function ($query) {
                $query->where('is_visible', 1);
            }])->where([
                ['is_visible', 1],
                ['display', 'like', '%' . $this->search . '%']
            ])->orderBy('display')->paginate(10)
        ]);
    }
}
