<?php

namespace App\Http\Livewire\Admin\SecurityManagement\RolesManagement;

use App\Models\AccountTypeReference;
use App\Models\Division;
use App\Models\DivisionRolesReference;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $create_modal = false;
    public $edit_modal = false;

    public $role_id;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $division = false;
    public $role_name;
    public $status;

    public $division_references = [];
    public $header_cards = [];

    protected $listeners = ['roles_management_index' => 'render', 'load_head_cards' => 'loadHeadCards', 'close_modal' => 'closeModal'];

    public function load()
    {
        $this->loadHeadCards();
        $this->loadDivisionReference();
    }

    public function loadHeadCards()
    {
        $division_roles = Division::withCount('roles')->get();
        $this->header_cards = [
            [
                'title' => 'All',
                'value' => $division_roles->sum('roles_count'),
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-blue',
                'action' => 'division',
                'id' => false
            ],
            [
                'title' => $division_roles[0]->name,
                'value' => $division_roles[0]->roles_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-blue',
                'action' => 'division',
                'id' => 1
            ],
            [
                'title' => $division_roles[1]->name,
                'value' => $division_roles[1]->roles_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-yellow',
                'action' => 'division',
                'id' => 2
            ],
            [
                'title' => $division_roles[2]->name,
                'value' => $division_roles[2]->roles_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-yellow',
                'action' => 'division',
                'id' => 3
            ],
            [
                'title' => $division_roles[3]->name,
                'value' => $division_roles[3]->roles_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-yellow',
                'action' => 'division',
                'id' => 4
            ],
            [
                'title' => $division_roles[4]->name,
                'value' => $division_roles[4]->roles_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-yellow',
                'action' => 'division',
                'id' => 5
            ],
            [
                'title' => $division_roles[5]->name,
                'value' => $division_roles[5]->roles_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-yellow',
                'action' => 'division',
                'id' => 6
            ],
            [
                'title' => $division_roles[6]->name,
                'value' => $division_roles[6]->roles_count,
                'icon' => '<svg class="w-10 h-10" fill="currentColor" viewBox="0 0 20 20"><path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path></svg>',
                'color' => 'text-yellow',
                'action' => 'division',
                'id' => 7
            ]
        ];
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function action($id)
    {
        $this->emit('roles_management_edit_mount', $id);
        $this->emit('load_parent_access');
        $this->role_id = $id;
        $this->edit_modal = true;
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'create') {
            $this->create_modal = false;
        } else if ($action_type == 'edit') {
            $this->edit_modal = false;
        }
    }

    public function updateStatus($id, $value)
    {
        $role = DivisionRolesReference::findOrFail($id);
        $role->update([
            'is_visible' => $value
        ]);

        $this->emit('roles_management_index');
        $this->emit('load_head_cards');
        $this->dispatchBrowserEvent('swal', [
            'icon' => 'success',
            'title' => 'Successfully Updated Status.',
        ]);
    }

    public function sortBy($field)
    {
        $this->sortField = $field;
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.admin.security-management.roles-management.index', [
            'roles' => DivisionRolesReference::with('division', 'level')
                ->where([['display', 'like', '%' . $this->role_name . '%']])
                ->when($this->division, function ($query) {
                    $query->where('division_id', $this->division);
                })
                ->when($this->status, function ($query) {
                    $query->where('is_visible', $this->status);
                })
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
