<?php

namespace App\Http\Livewire\Admin\SecurityManagement\RolesManagement;

use App\Models\Division;
use App\Models\DivisionRolesReference;
use App\Models\RolesParentAccessReference;
use App\Traits\Admin\SecurityManagement\RoleManagementTrait;
use Livewire\Component;
use Livewire\WithPagination;

class Edit extends Component
{
    use RoleManagementTrait, WithPagination;

    protected $listeners = ['roles_management_edit_mount' => 'mount', 'load_parent_access' => 'render', 'close_modal' => 'closeModal'];

    public function mount($id)
    {
        $this->resetForm();
        $this->role = DivisionRolesReference::with('accesses')->findOrFail($id);
        $this->division = $this->role->division_id;
        $this->level = $this->role->level_id;
        $this->role_name = $this->role->display;
        $this->selected_accesses = array_map('strval', $this->role->accesses->pluck('access_id')->toArray());
    }

    public function submit()
    {
        $validated = $this->validate([
            'division' => 'required',
            'level' => 'required',
            'role_name' => 'required',
            'selected_accesses' => 'sometimes'
        ]);

        $division = Division::find($validated['division']);

        $role = $division->roles()->find($this->role->id);
        $role->update([
            'level_id' => $validated['level'],
            'code' => strtolower(str_replace(" ", "_", $validated['role_name'])),
            'display' => $validated['role_name']
        ]);

        foreach ($role->accesses as $access) {
            $access->delete();
        }
        foreach ($validated['selected_accesses'] as $selected_access) {
            $role->accesses()->create([
                'access_id' => $selected_access
            ]);
        }

        $this->resetForm();
        $this->emit('roles_management_index');
        $this->emit('load_head_cards');
        $this->dispatchBrowserEvent('swal', [
            'icon' => 'success',
            'title' => 'Role Successfully Updated!',
        ]);
    }

    public function render()
    {
        return view('livewire.admin.security-management.roles-management.edit', [
            'parent_accesses' => RolesParentAccessReference::with(['accesses' => function ($query) {
                $query->where('is_visible', 1);
            }])->where([
                ['is_visible', 1],
                ['display', 'like', '%' . $this->search . '%']
            ])->orderBy('display')->paginate(10)
        ]);
    }
}
