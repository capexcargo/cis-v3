<?php

namespace App\Http\Livewire\Admin\SecurityManagement\ChildAccessManagement;

use App\Models\Division;
use App\Models\RolesChildAccessReference;
use App\Models\RolesParentAccessReference;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $paginate = 5;
    public $sortField;
    public $sortAsc = true;

    public $function = 'create';
    public $division;
    public $parent_name;
    public $child_name;

    public $parent;
    public $child;

    public $division_references = [];
    public $parent_access_references = [];

    public function load()
    {
        $this->loadDivisionReferece();
        $this->loadParentAccessReference();
    }

    public function loadDivisionReferece()
    {
        $this->division_references = Division::get();
    }
    public function loadParentAccessReference()
    {
        $this->parent_access_references = RolesParentAccessReference::where('division_id', $this->division)->get();
    }

    public function updateStatus($id, $value)
    {
        $child = RolesChildAccessReference::find($id);
        $child->update(['is_visible' => $value]);
        $this->emit('load_parent_access');
        session()->flash('message', 'Successfully Updated The Status.');
    }

    public function action($parent_id, $child_id, $action)
    {
        if ($action == 'update') {
            $this->parent = RolesParentAccessReference::with('accesses')->findOrFail($parent_id);
            $this->child = $this->parent->accesses()->find($child_id);
            $this->function = $action;
            $this->parent_name = $this->parent->id;
            $this->child_name = $this->child->display;
            return;
        }

        $this->function = $action;
        $this->parent_name = '';
        $this->child_name = '';
    }

    public function actionDelete($parent_id, $child_id)
    {
        $parent = RolesParentAccessReference::with('accesses')->findOrFail($parent_id);
        $child = $parent->accesses()->find($child_id);
        $child->delete();
    }

    protected function updatedDivision()
    {
        $this->loadParentAccessReference();
        $this->reset(
            'parent_name'
        );
    }

    protected function updatedParentName()
    {
        $this->resetPage();
    }

    public function submitCreate()
    {
        $validated = $this->validate([
            'parent_name' => 'required',
            'child_name' => 'required'
        ]);

        $parent = RolesParentAccessReference::findOrFail($validated['parent_name']);
        $parent->accesses()->create([
            'code' => $parent->code . '_' . strtolower(str_replace(" ", "_", $validated['child_name'])),
            'display' => $validated['child_name']
        ]);

        $this->child_name = '';
        $this->emit('load_parent_access');
        session()->flash('message', 'Successfully Created.');
    }

    public function submitUpdate()
    {
        $validated = $this->validate([
            'parent_name' => 'required',
            'child_name' => 'required'
        ]);

        $this->child->update([
            'code' => $this->parent->code . '_' . strtolower(str_replace(" ", "_", $validated['child_name'])),
            'display' => $validated['child_name']
        ]);

        $this->function = 'create';
        $this->child_name = '';
        $this->emit('load_parent_access');
        session()->flash('message', 'Successfully Updated.');
    }

    public function sortBy($field)
    {
        $this->sortField = $field;

        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.admin.security-management.child-access-management.index', [
            'parent_accesses' => RolesParentAccessReference::with('accesses')->where('id', $this->parent_name)
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
