<?php

namespace App\Http\Livewire\Admin\SecurityManagement\ParentAccessManagement;

use App\Models\AccountTypeReference;
use App\Models\Division;
use App\Models\RolesParentAccessReference;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $paginate = 5;
    public $sortField = 'created_at';
    public $sortAsc = false;

    public $function = 'create';
    public $division;
    public $parent_name;
    public $parent_id;
    public $parent;

    public $division_references = [];

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function updatingParentName()
    {
        $this->resetPage();
    }

    protected function updatedDivision()
    {
        $this->resetPage();
    }

    public function updateStatus($id, $value)
    {
        $parent = RolesParentAccessReference::find($id);
        $parent->update(['is_visible' => $value]);
        $this->emit('load_parent_access');
        session()->flash('message', 'Successfully Updated The Status.');
    }

    public function action($id, $action)
    {
        if ($action == 'update') {
            $this->parent = RolesParentAccessReference::findOrFail($id);
            $this->function = $action;
            $this->parent_name = $this->parent->display;
            $this->parent_id = $id;
            return;
        }

        $this->function = $action;
        $this->parent_name = '';
        $this->parent_id = '';
    }


    public function submitCreate()
    {
        $validated = $this->validate([
            'division' => 'required',
            'parent_name' => 'required|unique:roles_parent_access_reference,display'
        ]);

        RolesParentAccessReference::create([
            'division_id' => $validated['division'],
            'code' => strtolower(str_replace(" ", "_", $validated['parent_name'])),
            'display' => $validated['parent_name']
        ]);

        $this->parent_name = '';
        $this->emit('load_parent_access');
        session()->flash('message', 'Successfully Created.');
    }

    public function submitUpdate()
    {
        $validated = $this->validate([
            'division' => 'required',
            'parent_name' => 'required|unique:roles_parent_access_reference,display,' . $this->parent_id . ',id'
        ]);

        $this->parent->update([
            'division_id' => $validated['division'],
            'code' => strtolower(str_replace(" ", "_", $validated['parent_name'])),
            'display' => $validated['parent_name']
        ]);

        foreach ($this->parent->accesses as $access) {
            $access->update([
                'code' => $this->parent->code . '_' . strtolower(str_replace(" ", "_", $access->display)),
            ]);
        }

        $this->function = 'create';
        $this->parent_name = '';
        $this->parent_id = '';
        $this->emit('load_parent_access');
        session()->flash('message', 'Successfully Updated.');
    }

    public function sortBy($field)
    {
        $this->sortField = $field;

        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
    }

    public function render()
    {
        return view('livewire.admin.security-management.parent-access-management.index', [
            'parent_accesses' => RolesParentAccessReference::with('division')->where([
                ['display', 'like', '%' . $this->parent_name . '%'],
                ['division_id', $this->division]
            ])
                ->when($this->sortField, function ($query) {
                    $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');
                })->paginate($this->paginate)
        ]);
    }
}
