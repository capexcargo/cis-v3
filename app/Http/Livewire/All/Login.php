<?php

namespace App\Http\Livewire\All;

use Livewire\Component;

class Login extends Component
{
    public function render()
    {
        return view('livewire.all.login');
    }
}
