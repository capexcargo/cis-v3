<?php

namespace App\Http\Livewire\All;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class Navbar extends Component
{

    public function action($id)
    {
        Session::put('menu', $id);
        if ($id == 1) {
            return redirect()->route('admin.account-management.index');
        } elseif ($id == 2) {
            return redirect()->route('accounting.canvassing-management.index');
        } elseif ($id == 5) {
            return redirect()->route('oims.reports.order-management-reports.index');
        } else if ($id == 6) {
            return redirect()->route('crm.reports.cdm-reports.index');
        } else if ($id == 7) {
            if (Auth::user()->accesses()->contains('hrim_dashboard_view')) {
                return redirect()->route('hrim.dashboard.index');
            } else {
                return redirect()->route('hrim.company-management.bulletin.index');
            }
        } else if ($id == 8) {
            return redirect()->route('ticketing.ticket-management.dashboard.index');
        }

        $this->emit('load_menu');
    }

    public function render()
    {
        return view('livewire.all.navbar');
    }
}
