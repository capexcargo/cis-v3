<?php

namespace App\Http\Livewire\All;

use Livewire\Component;

class ForgotPassword extends Component
{
    public function render()
    {
        return view('livewire.all.forgot-password');
    }
}
