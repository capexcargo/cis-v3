<?php

namespace App\Http\Livewire\All;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Notification extends Component
{
    public $show_notifications = false;
    public $notifications;
    public $type = 'all';
    public $take = 5;

    public function mount()
    {
        $this->notifications('all');
    }

    public function notifications($type)
    {
        $this->type = $type;
        if ($type == 'all') {
            $this->notifications = Auth::user()->notifications()->take($this->take)->get();
        } else if ($type == 'unread') {
            $this->notifications = Auth::user()->unreadNotifications()->take($this->take)->get();
        } else if ($type == 'read') {
            $this->notifications = Auth::user()->readNotifications()->take($this->take)->get();
        }
    }

    public function markAsRead($id, $read_at)
    {
        if ($read_at) {
            $notification = Auth::user()->readNotifications->find($id);
        } else {
            $notification = Auth::user()->unreadNotifications->find($id);
            $notification->markAsRead();
        }
        return redirect($notification->data['redirection']);
    }

    public function markAsReadAll()
    {
        Auth::user()->unreadNotifications->markAsRead();

        $this->notifications('unread');
    }

    public function loadMore()
    {
        $this->take += 5;
        $this->notifications($this->type);
    }

    public function render()
    {
        return view('livewire.all.notification');
    }
}
