<?php

namespace App\Http\Controllers\Fims\ServicePurchaseRequisition;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\ServicePurchaseRequisition\PurchaseReqInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class PurchaseReqController extends Controller
{
    use ResponseTrait;

    public $pr_interface;
    public function __construct(PurchaseReqInterface $pr_interface)
    {
        $this->pr_interface = $pr_interface;
    }
    public function createPR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );
        // dd($request_data);

        $response = $this->pr_interface->create($request_data);
        // dd($response);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function reqstatPR(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->pr_interface->reqstat($request_data, $id);
        // dd($response);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexPR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->pr_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getItemCategPR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->pr_interface->getItemCateg($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getItemDescPR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->pr_interface->getItemDesc($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getPurpPR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->pr_interface->getPurp($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getBenBranchPR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->pr_interface->getBenBranch($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getPrefSupPR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->pr_interface->getPrefSup($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
