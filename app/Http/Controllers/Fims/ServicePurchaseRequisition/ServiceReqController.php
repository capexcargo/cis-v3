<?php

namespace App\Http\Controllers\Fims\ServicePurchaseRequisition;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\ServicePurchaseRequisition\ServiceReqInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ServiceReqController extends Controller
{
    use ResponseTrait;

    public $sr_interface;
    public function __construct(ServiceReqInterface $sr_interface)
    {
        $this->sr_interface = $sr_interface;
    }
    public function createSR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );
        // dd($request_data);

        $response = $this->sr_interface->create($request_data);
        // dd($response);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }


    public function reqstatSR(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->sr_interface->reqstat($request_data, $id);
        // dd($response);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexSR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->sr_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getServiceCategSR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->sr_interface->getServiceCateg($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getServiceDescSR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->sr_interface->getServiceDesc($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getPrefWorkSR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->sr_interface->getPrefWork($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getPurpSR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->sr_interface->getPurp($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getLocSR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->sr_interface->getLoc($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
