<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\BankNameInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class BankNameController extends Controller
{
    use ResponseTrait;

    public $bank_interface;
    public function __construct(BankNameInterface $bank_interface)
    {
        $this->bank_interface = $bank_interface;
    }
    public function createBN(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->bank_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateBN(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->bank_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatusBN(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->bank_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexBN(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->bank_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
