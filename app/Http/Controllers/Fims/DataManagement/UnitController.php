<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\UnitInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    use ResponseTrait;

    public $unit_interface;
    public function __construct(UnitInterface $unit_interface)
    {
        $this->unit_interface = $unit_interface;
    }
    public function createU(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->unit_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateU(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->unit_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatusU(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->unit_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexU(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->unit_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
