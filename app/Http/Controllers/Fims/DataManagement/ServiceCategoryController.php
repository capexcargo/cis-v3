<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\ServiceCategoryInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ServiceCategoryController extends Controller
{
    use ResponseTrait;

    public $service_interface;
    public function __construct(ServiceCategoryInterface $service_interface)
    {
        $this->service_interface = $service_interface;
    }
    public function createSC(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->service_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateSC(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->service_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatusSC(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->service_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexSC(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->service_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getBudgetSC(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->service_interface->getBudget($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getOpexSC(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->service_interface->getOpex($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getChartsSC(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->service_interface->getCharts($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getSubAccSC(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->service_interface->getSubAcc($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
