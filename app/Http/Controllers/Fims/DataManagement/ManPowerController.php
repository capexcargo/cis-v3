<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\ManpowerInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ManPowerController extends Controller
{
    use ResponseTrait;

    public $man_interface;
    public function __construct(ManpowerInterface $man_interface)
    {
        $this->man_interface = $man_interface;
    }
    public function createMP(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->man_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateMP(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->man_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatusMP(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->man_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexMP(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->man_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
