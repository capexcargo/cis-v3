<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\IndustryInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class IndustryController extends Controller
{
    use ResponseTrait;

    public $ind_interface;
    public function __construct(IndustryInterface $ind_interface)
    {
        $this->ind_interface = $ind_interface;
    }
    public function createIN(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->ind_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateIN(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->ind_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatusIN(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->ind_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexIN(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->ind_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
