<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\SubAccountsInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class SubAccountsController extends Controller
{
    use ResponseTrait;

    public $sub_interface;
    public function __construct(SubAccountsInterface $sub_interface)
    {
        $this->sub_interface = $sub_interface;
    }
    public function createSA(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->sub_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateSA(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->sub_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatusSA(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->sub_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexSA(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->sub_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
