<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\ChartofAccountsInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ChartofAccountsController extends Controller
{
    use ResponseTrait;

    public $chart_interface;
    public function __construct(ChartofAccountsInterface $chart_interface)
    {
        $this->chart_interface = $chart_interface;
    }
    public function createCA(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->chart_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateCA(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->chart_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatusCA(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->chart_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexCA(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->chart_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
