<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\AcctngSupplierInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class AcctngSupplierController extends Controller
{
    use ResponseTrait;

    public $accsup_interface;
    public function __construct(AcctngSupplierInterface $accsup_interface)
    {
        $this->accsup_interface = $accsup_interface;
    }
    public function createAS(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->accsup_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateAS(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->accsup_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatusAS(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->accsup_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexAS(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->accsup_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getIndustryAS(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->accsup_interface->getIndustry($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getRegionAS(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->accsup_interface->getRegion($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getProvinceAS(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->accsup_interface->getProvince($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getMunicipalAS(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->accsup_interface->getMunicipal($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getBarangayAS(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->accsup_interface->getBarangay($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getBankAS(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->accsup_interface->getBank($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
