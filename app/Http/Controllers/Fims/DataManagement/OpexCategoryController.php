<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\OpexCategoryInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class OpexCategoryController extends Controller
{
    use ResponseTrait;

    public $opex_category_interface;
    public function __construct(OpexCategoryInterface $opex_category_interface)
    {
        $this->opex_category_interface = $opex_category_interface;
    }
    public function createOC(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->opex_category_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateOC(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->opex_category_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatusOC(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->opex_category_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexOC(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->opex_category_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
