<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\ItemDescriptionInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ItemDescriptionController extends Controller
{
    use ResponseTrait;

    public $itemdes_interface;
    public function __construct(ItemDescriptionInterface $itemdes_interface)
    {
        $this->itemdes_interface = $itemdes_interface;
    }
    public function createID(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->itemdes_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateID(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->itemdes_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatusID(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->itemdes_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexID(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->itemdes_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getItemCategID(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->itemdes_interface->getItemCateg($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
