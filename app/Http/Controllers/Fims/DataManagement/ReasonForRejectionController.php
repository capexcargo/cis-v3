<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\ReasonForRejectionInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ReasonForRejectionController extends Controller
{
    use ResponseTrait;

    public $reason_interface;
    public function __construct(ReasonForRejectionInterface $reason_interface)
    {
        $this->reason_interface = $reason_interface;
    }
    public function createRR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->reason_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateRR(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->reason_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatusRR(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->reason_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexRR(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->reason_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
