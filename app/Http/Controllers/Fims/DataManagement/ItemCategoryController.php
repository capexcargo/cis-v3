<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\ItemCategoryInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ItemCategoryController extends Controller
{
    use ResponseTrait;

    public $item_interface;
    public function __construct(ItemCategoryInterface $item_interface)
    {
        $this->item_interface = $item_interface;
    }
    public function createIC(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->item_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateIC(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->item_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatusIC(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->item_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexIC(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->item_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getBudgetIC(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->item_interface->getBudget($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getOpexIC(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->item_interface->getOpex($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getChartsIC(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->item_interface->getCharts($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getSubAccIC(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->item_interface->getSubAcc($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
