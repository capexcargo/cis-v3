<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\ServiceDescriptionInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ServiceDescriptionController extends Controller
{
    use ResponseTrait;

    public $servdes_interface;
    public function __construct(ServiceDescriptionInterface $servdes_interface)
    {
        $this->servdes_interface = $servdes_interface;
    }
    public function createSD(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->servdes_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateSD(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->servdes_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatusSD(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->servdes_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexSD(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->servdes_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getServiceCategSD(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->servdes_interface->getServiceCateg($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
