<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\PurposeInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class PurposeController extends Controller
{
    use ResponseTrait;

    public $purpose_interface;
    public function __construct(PurposeInterface $purpose_interface)
    {
        $this->purpose_interface = $purpose_interface;
    }
    public function createP(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->purpose_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateP(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->purpose_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatusP(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->purpose_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexP(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->purpose_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
