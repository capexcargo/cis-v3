<?php

namespace App\Http\Controllers\Fims\DataManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\DataManagement\BudgetSRCInterface;
use App\Interfaces\Fims\DataManagement\OpexCategoryInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class DataManagementController extends Controller
{
    use ResponseTrait;

    public $budget_src_interface;
    public function __construct(BudgetSRCInterface $budget_src_interface)
    {
        $this->budget_src_interface = $budget_src_interface;
    }


    public function createBSM(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->budget_src_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateBSM(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->budget_src_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function toggleStatus(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->budget_src_interface->toggleStatus($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function index(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->budget_src_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
