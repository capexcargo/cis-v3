<?php

namespace App\Http\Controllers\Fims\Accounting;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\Accounting\BudgetInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class BudgetMgmtController extends Controller
{
    use ResponseTrait;

    public $budget_interface;
    public function __construct(BudgetInterface $budget_interface)
    {
        $this->budget_interface = $budget_interface;
    }
    public function createBP(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->budget_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexBP(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->budget_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getCoaBP(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->budget_interface->getCoa($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
