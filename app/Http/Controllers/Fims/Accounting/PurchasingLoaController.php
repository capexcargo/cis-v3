<?php

namespace App\Http\Controllers\Fims\Accounting;

use App\Http\Controllers\Controller;
use App\Interfaces\Fims\Accounting\AcctngPurchasingLoaInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class PurchasingLoaController extends Controller
{
    use ResponseTrait;

    public $accpurchaseloa_interface;
    public function __construct(AcctngPurchasingLoaInterface $accpurchaseloa_interface)
    {
        $this->accpurchaseloa_interface = $accpurchaseloa_interface;
    }
    public function createPL(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->accpurchaseloa_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updatePL(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->accpurchaseloa_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function deletePL(Request $request, $id)
    {
        // $request_data = array_merge(
        //     $request->all(),
        // );

        $response = $this->accpurchaseloa_interface->destroy($id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function indexPL(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->accpurchaseloa_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
