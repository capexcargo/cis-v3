<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PurposeController extends Controller
{
    public function index()
    {
        return view('view/accounting/purpose/index');
    }
}
