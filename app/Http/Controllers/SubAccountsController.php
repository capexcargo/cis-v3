<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubAccountsController extends Controller
{
    public function index()
    {
        return view('view/accounting/sub-accounts/index');
    }
}
