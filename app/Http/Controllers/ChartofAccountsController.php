<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChartofAccountsController extends Controller
{
    public function index()
    {
        return view('view/accounting/chart-of-accounts/index');
    }
}
