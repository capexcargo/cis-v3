<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    use ResponseTrait;

    public function login(Request $request)
    {
        $login = $request->validate([
            'email' => 'required|string|exists:users,email',
            'password' => 'required|string',
        ]);

        if (!Auth::attempt($login)) {
            return $this->responseAPI(400, 'Login', ['message' => 'Invalid login credentials.']);
        }

        $user = User::with('division', 'status', 'role', 'userDetails')->find(Auth::user()->id);
        $token = $user->createToken('authToken');

        return $this->responseAPI(200, 'Login', ['user' => $user, 'token' => $token->plainTextToken]);
    }
}
