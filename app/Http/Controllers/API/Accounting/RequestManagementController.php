<?php

namespace App\Http\Controllers\API\Accounting;

use App\Http\Controllers\Controller;
use App\Interfaces\Accounting\RequestManagementInterface;
use App\Models\Accounting\BudgetPlan;
use App\Models\Accounting\RequestForPayment;
use App\Models\Accounting\RequestForPaymentTypeReference;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class RequestManagementController extends Controller
{
    use ResponseTrait;

    public $request_management_interface;

    public function __construct(RequestManagementInterface $request_management_interface)
    {
        $this->request_management_interface = $request_management_interface;
    }

    public function dashboard(Request $request)
    {
        $request_for_payment_types = RequestForPaymentTypeReference::withCount(['requestForPayments' => function ($query) use ($request) {
            $query->when(Auth::user()->level_id < 5, function ($query) use ($request) {
                $query->when($request->view_type == 'request_for_payment_approval', function ($query) {
                    $query->whereRaw("(approver_1_id = " . Auth::user()->id . " OR approver_2_id = " . Auth::user()->id . " OR approver_3_id = " . Auth::user()->id . ")");
                })
                    ->when($request->view_type == 'request_for_payment', function ($query) {
                        $query->where('user_id', Auth::user()->id);
                    });
            })
                ->where('budget_id', '!=', null);
        }])
            ->get();

        $datas = [
            [
                'id' => null,
                'display' => 'Total',
                'value' => $request_for_payment_types->sum('request_for_payments_count'),
            ]
        ];

        foreach ($request_for_payment_types as $request_for_payment_type) {
            $datas[] = [
                'id' => $request_for_payment_type->id,
                'display' => $request_for_payment_type->display,
                'value' => $request_for_payment_type->request_for_payments_count,
            ];
        }
        return $this->responseAPI(200, 'Request For Payment Dashboard', $datas);
    }

    public function index(Request $request)
    {
        $request_for_payments = RequestForPayment::with(['type', 'payee', 'budget', 'user', 'approver1', 'approver2', 'approver3', 'status', 'requestForPaymentDetails', 'checkVoucher' => function ($query) {
            $query->with('checkVoucherDetails');
        }])
            ->when($request->reference_no, function ($query) use ($request) {
                $query->where('reference_id', 'like', '%' . $request->reference_no . '%');
            })
            ->when($request->type, function ($query) use ($request) {
                $query->where('type_id', $request->type);
            })
            ->when(Auth::user()->level_id < 5, function ($query) use ($request) {
                $query->when($request->view_type == 'request_for_payment_approval', function ($query) {
                    $query->whereRaw("(approver_1_id = " . Auth::user()->id . " OR approver_2_id = " . Auth::user()->id . " OR approver_3_id = " . Auth::user()->id . ")");
                })
                    ->when($request->view_type == 'request_for_payment', function ($query) {
                        $query->where('user_id', Auth::user()->id);
                    });
            })
            ->where('budget_id', '!=', null)
            ->when($request->status, function ($query) use ($request) {
                $query->where('status_id', $request->status);
            })
            ->when($request->sort_field, function ($query) use ($request) {
                $query->orderBy($request->sort_field, $request->sort_type);
            })
            ->paginate($request->paginate);

        return $this->responseAPI(200, 'Request For Payment List', $request_for_payments);
    }

    public function show(Request $request, $id)
    {
        $request_for_payment = RequestForPayment::with(['type', 'payee', 'budget' => function ($query) {
            $query->with('division', 'source', 'chart');
        }, 'user', 'branch', 'priority', 'canvassingSupplier', 'opex', 'approver1', 'approver2', 'approver3', 'status', 'attachments', 'requestForPaymentDetails', 'checkVoucher' => function ($query) {
            $query->with('checkVoucherDetails');
        }])
            ->when(Auth::user()->level_id < 5, function ($query) use ($request) {
                $query->when($request->view_type == 'request_for_payment_approval', function ($query) {
                    $query->whereRaw("(approver_1_id = " . Auth::user()->id . " OR approver_2_id = " . Auth::user()->id . " OR approver_3_id = " . Auth::user()->id . ")");
                })
                    ->when($request->view_type == 'request_for_payment', function ($query) {
                        $query->where('user_id', Auth::user()->id);
                    });
            })
            ->find($id);

        if (!$request_for_payment) return $this->responseAPI(404, 'Request For Payment', 'Not Found!');

        $month = strtolower(date('F', strtotime(now())));

        if ($request_for_payment) {
            if ($request_for_payment->type_id == 2 && $request_for_payment->account_type_id == 1) {
                $month = strtolower(date('F', strtotime(now())));
            } else if ($request_for_payment->type_id == 3 && $request_for_payment->requestForPaymentDetails[0]->payment_type_id == 2) {
                $month = strtolower(date('F', strtotime(now())));
            } else {
                $month = strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from)));;
            }
        }

        $budget_plan = BudgetPlan::with(['chart' => function ($query) use ($request_for_payment) {
            $query->withSum(['transferBudgetFrom' => function ($query) use ($request_for_payment) {
                $query->where('month_from', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))));
            }], 'amount')
                ->withSum(['transferBudgetTo' => function ($query) use ($request_for_payment) {
                    $query->where('month_to', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))));
                }], 'amount')
                ->withSum(['availment' => function ($query) use ($request_for_payment){
                    $query->where('month', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))));
                }], 'amount')
                ->withSum('budgetPlan', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))));
        }, 'budgetLoa' => function ($query) {
            $query->with('firstApprover', 'secondApprover', 'thirdApprover');
        }, 'division'])
            ->withSum(['transferBudgetFrom' => function ($query) use ($month) {
                $query->where('month_from', $month);
            }], 'amount')
            ->withSum(['transferBudgetTo' => function ($query) use ($month) {
                $query->where('month_to', $month);
            }], 'amount')
            ->withSum(['availment' => function ($query) use ($month) {
                $query->where('month', $month);
            }], 'amount')
            ->findOrFail($request_for_payment->budget_id);

        if ($budget_plan) {
            $budget_plan_availed = $budget_plan->chart->availment_sum_amount;
            $budget_plan_total_amount = ($budget_plan->chart['budget_plan_sum_' . $month] + $budget_plan->chart->transfer_budget_to_sum_amount) - $budget_plan->chart->transfer_budget_from_sum_amount;
            $budget_availed_percent = round($budget_plan_availed && $budget_plan_total_amount ? ($budget_plan_availed / $budget_plan_total_amount) * 100 : 0, 2);
        }

        $budget =  [
            'chart_of_accounts_approved' => [
                'title' => strtoupper($month),
                'value' => $budget_plan_total_amount
            ],
            'chart_of_accounts_availed' => [
                'title' => strtoupper($month),
                'value' => $budget_plan_availed
            ]
        ];

        return $this->responseAPI(200, 'Request For Payment', compact('request_for_payment', 'budget'));
    }

    public function attachment(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'attachments' => 'required',
            'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),
        ], [
            'attachments.*.attachment.required' => "The attachment field is required."
        ]);

        if ($validator->fails()) return $this->responseAPI(400, 'Please Fill Required Field', $validator->errors());
        $validated = $validator->validated();
        $request_for_payment = RequestForPayment::whereIn('status_id', [1, 3])->find($id);
        if (!$request_for_payment) return $this->responseAPI(404, 'Request For Payment', 'Not Found!');

        $response = $this->request_management_interface->attachment($request_for_payment, $validated['attachments']);

        if ($response['code'] == 200) {
            return $this->responseAPI($response['code'], $response['message'], $response['result']);
        } else if ($response['code'] == 400) {
            $validator->after(function ($validator) use ($response) {
                foreach ($response['result'] as $a => $result) {
                    $validator->errors()->add($a, $result);
                }
            });
            if ($validator->fails()) return $this->responseAPI(400, 'Please Fill Required Field', $validator->errors());
        } else {
            return $this->responseAPI($response['code'], $response['message'], $response['result']);
        }
    }

    public function approved(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'is_approved_1' => 'sometimes',
            'is_approved_2' => 'sometimes',
            'is_approved_3' => 'sometimes',
            'approver_remarks_1' => 'sometimes',
            'approver_remarks_2' => 'sometimes',
            'approver_remarks_3' => 'sometimes',
        ]);

        $validated = $validator->validated();
        $request_for_payment = RequestForPayment::find($id);
        if (!$request_for_payment) return $this->responseAPI(404, 'Request For Payment', 'Not Found!');

        if ($request_for_payment->approver_1_id) {
            $validator->after(function ($validator) use ($request_for_payment, $validated) {
                if ($request_for_payment->approver_1_id == Auth::user()->id && !$validated['approver_remarks_1'] || Auth::user()->level_id == 5 && $request_for_payment->approver_1_id) {
                    if (!$validated['is_approved_1'] && !$validated['approver_remarks_1']) {
                        $validator->errors()->add(
                            'approver_remarks_1',
                            'The approver remarks 1 field is required.'
                        );
                    }
                    if ($validated['is_approved_2'] && !$validated['is_approved_1']) {
                        $validator->errors()->add(
                            'approver_2_id',
                            'The approver 2 must be disapproved.'
                        );
                    }
                }
            });
        }

        if ($request_for_payment->approver_2_id) {
            $validator->after(function ($validator) use ($request_for_payment, $validated) {
                if ($validated['is_approved_1'] || !$validated['is_approved_2']) {
                    if ($request_for_payment->approver_2_id == Auth::user()->id && !$validated['approver_remarks_2'] || Auth::user()->level_id == 5 && $request_for_payment->approver_2_id) {
                        if (!$validated['is_approved_2'] && !$validated['approver_remarks_2']) {
                            $validator->errors()->add(
                                'approver_remarks_2',
                                'The approver remarks 2 field is required.'
                            );
                        }
                    }

                    if ($validated['is_approved_3'] && !$validated['is_approved_2']) {
                        $validator->errors()->add(
                            'approver_3_id',
                            'The approver 3 must be disapproved.'
                        );
                    }
                } else {
                    $validator->errors()->add(
                        'approver_1_id',
                        'The approver 1 must be approved.'
                    );
                }
            });
        }

        if ($request_for_payment->approver_3_id) {
            $validator->after(function ($validator) use ($request_for_payment, $validated) {
                if ($validated['is_approved_2'] || !$validated['is_approved_3']) {
                    if ($request_for_payment->approver_3_id == Auth::user()->id && !$validated['approver_remarks_3'] || Auth::user()->level_id == 5 && $request_for_payment->approver_3_id) {
                        if (!$validated['is_approved_3'] && !$validated['approver_remarks_3']) {
                            $validator->errors()->add(
                                'approver_remarks_3',
                                'The approver remarks 3 field is required.'
                            );
                        }
                    }
                } else {
                    $validator->errors()->add(
                        'approver_2_id',
                        'The approver 2 must be approved.'
                    );
                }
            });
        }

        if ($validator->fails()) return $this->responseAPI(400, 'Please Fill Required Field', $validator->errors());

        $response = $this->request_management_interface->approved($request_for_payment, $validated, Auth::user()->id,  Auth::user()->level_id);

        if ($response['code'] == 200) {
            return $this->responseAPI($response['code'], $response['message'], $response['result']);
        } else if ($response['code'] == 400) {
            $validator->after(function ($validator) use ($response) {
                foreach ($response['result'] as $a => $result) {
                    $validator->errors()->add($a, $result);
                }
            });
            if ($validator->fails()) return $this->responseAPI(400, 'Please Fill Required Field', $validator->errors());
        } else {
            return $this->responseAPI($response['code'], $response['message'], $response['result']);
        }
    }
}
