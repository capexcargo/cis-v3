<?php

namespace App\Http\Controllers\Api\Accounting;

use App\Http\Controllers\Controller;
use App\Interfaces\Accounting\LoaderManagementInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class LoaderManagementController extends Controller
{
    use ResponseTrait;

    public $loader_management_interface;

    public function __construct(LoaderManagementInterface $loader_management_interface)
    {
        $this->loader_management_interface = $loader_management_interface;
    }

    public function create(Request $request)
    {
        $response = $this->loader_management_interface->create($request->all());
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function update($code, Request $request)
    {
        $response = $this->loader_management_interface->update2($code, $request->all());
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
