<?php

namespace App\Http\Controllers\API\Accounting;

use App\Http\Controllers\Controller;
use App\Interfaces\Accounting\CheckVoucherInterface;
use App\Models\Accounting\CheckVoucher;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CheckVoucherController extends Controller
{
    use ResponseTrait;

    public $check_voucher_interface;

    public function __construct(CheckVoucherInterface $check_voucher_interface)
    {
        $this->check_voucher_interface = $check_voucher_interface;
    }

    public function index(Request $request)
    {
        $response = $this->check_voucher_interface->index($request->all());
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function show($id)
    {
        $check_voucher = CheckVoucher::with(['requestForPayment' => function ($query) {
            $query->with('payee', 'opex');
        }, 'status', 'firstApprover', 'secondApprover', 'receivedBy', 'createdBy', 'checkVoucherDetails', 'attachments'])
            ->whereHas('status', function ($query) {
                $query->when(Auth::user()->level_id < 5, function ($query) {
                    $query->checkVoucher();
                });
            })
            ->find($id);
        if (!$check_voucher) return $this->responseAPI(404, 'Check Voucher', 'Not Found!');

        return $this->responseAPI(200, 'Check Voucher', $check_voucher);
    }



    public function approved(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'is_approved_1' => 'sometimes',
            'is_approved_2' => 'sometimes',
            'approver_remarks_1' => 'sometimes',
            'approver_remarks_2' => 'sometimes',
        ]);

        $validated = $validator->validated();
        $check_voucher = CheckVoucher::find($id);
        if (!$check_voucher) return $this->responseAPI(404, 'Check Voucher', 'Not Found!');

        if ($check_voucher->approver_1_id) {
            $validator->after(function ($validator) use ($check_voucher, $validated) {
                if ($check_voucher->approver_1_id == Auth::user()->id && !$validated['approver_remarks_1'] || Auth::user()->level_id == 5 && $check_voucher->approver_1_id) {
                    if (!$validated['is_approved_1'] && !$validated['approver_remarks_1']) {
                        $validator->errors()->add(
                            'approver_remarks_1',
                            'The approver remarks 1 field is required.'
                        );
                    }

                    if ($validated['is_approved_2'] && !$validated['is_approved_1']) {
                        $validator->errors()->add(
                            'approver_2_id',
                            'The approver 2 must be disapproved.'
                        );
                    }
                }
            });
        }

        if ($check_voucher->approver_2_id) {
            $validator->after(function ($validator) use ($check_voucher, $validated) {
                if ($validated['is_approved_1'] || !$validated['is_approved_2']) {
                    if ($check_voucher->approver_2_id == Auth::user()->id && !$validated['approver_remarks_2'] || Auth::user()->level_id == 5 && $check_voucher->approver_1_id) {
                        if (!$validated['is_approved_2'] && !$validated['approver_remarks_2']) {
                            $validator->errors()->add(
                                'approver_remarks_2',
                                'The approver remarks 2 field is required.'
                            );
                        }
                    }
                } else {
                    $validator->errors()->add(
                        'approver_1_id',
                        'The approver 1 must be approved.'
                    );
                }
            });
        }

        if ($validator->fails()) return $this->responseAPI(400, 'Please Fill Required Field', $validator->errors());

        $response = $this->check_voucher_interface->approved($check_voucher, $validated);

        if ($response['code'] == 200) {
            return $this->responseAPI($response['code'], $response['message'], $response['result']);
        } else if ($response['code'] == 400) {
            $validator->after(function ($validator) use ($response) {
                foreach ($response['result'] as $a => $result) {
                    $validator->errors()->add($a, $result);
                }
            });
            if ($validator->fails()) return $this->responseAPI(400, 'Please Fill Required Field', $validator->errors());
        } else {
            return $this->responseAPI($response['code'], $response['message'], $response['result']);
        }
    }

    public function confirmation(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'attachments' => 'required',
            'attachments.*.attachment' => 'sometimes|' . config('filesystems.validation_all'),
            'remarks' => 'sometimes'
        ]);

        if ($validator->fails()) return $this->responseAPI(400, 'Please Fill Required Field', $validator->errors());

        $validated = $validator->validated();
        $check_voucher = CheckVoucher::find($id);
        if (!$check_voucher) return $this->responseAPI(404, 'Check Voucher', 'Not Found!');

        $validated_final = array_merge($validated, [
            'released_date' => now(),
            'received_date' => now(),
            'received_by' => $check_voucher->requestForPayment->payee_id,
        ]);

        $response = $this->check_voucher_interface->confirmation($check_voucher, $validated_final);

        if ($response['code'] == 200) {
            return $this->responseAPI($response['code'], $response['message'], $response['result']);
        } else if ($response['code'] == 400) {
            $validator->after(function ($validator) use ($response) {
                foreach ($response['result'] as $a => $result) {
                    $validator->errors()->add($a, $result);
                }
            });
            if ($validator->fails()) return $this->responseAPI(400, 'Please Fill Required Field', $validator->errors());
        } else {
            return $this->responseAPI($response['code'], $response['message'], $response['result']);
        }
    }
}
