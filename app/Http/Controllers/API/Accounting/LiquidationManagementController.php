<?php

namespace App\Http\Controllers\API\Accounting;

use App\Http\Controllers\Controller;
use App\Interfaces\Accounting\LiquidationManagementInterface;
use Illuminate\Http\Request;
use App\Traits\ResponseTrait;

class LiquidationManagementController extends Controller
{
    use ResponseTrait;

    public $liquidation_management_interface;

    public function __construct(LiquidationManagementInterface $liquidation_management_interface)
    {
        $this->liquidation_management_interface = $liquidation_management_interface;
    }

    public function index(Request $request)
    {
        $response = $this->liquidation_management_interface->index($request->all());
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function loadRequestForPaymentReference(Request $request)
    {
        $request = [
            'request_for_payment_search' => $request->request_for_payment_search,
            'where_request_for_payments' => $request->where_request_for_payments,
            'request_for_payments' => json_decode($request->request_for_payments, true),
            'liquidation_details' => json_decode($request->liquidation_details, true),
            'liquidation_details_index' => $request->liquidation_details_index,
        ];

        $response = $this->liquidation_management_interface->loadRequestForPaymentReference($request);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function showRequestForPaymentReference(Request $request, $id)
    {
        $request = [
            'where_request_for_payments' => $request->where_request_for_payments,
            'request_for_payments' => json_decode($request->request_for_payments, true),
            'liquidation_details' => json_decode($request->liquidation_details, true),
            'liquidation_details_index' => $request->liquidation_details_index,
        ];

        $response = $this->liquidation_management_interface->showRequestForPaymentReference($id, $request);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function create(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
            ['liquidation_reference_no' => "LIQ-" . date("ymdhis") . "-" . rand(111, 999)],
        );

        $response = $this->liquidation_management_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function show($id)
    {
        $response = $this->liquidation_management_interface->show($id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function update(Request $request, $id)
    {
        $response = $this->liquidation_management_interface->update($id, $request->all());
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
