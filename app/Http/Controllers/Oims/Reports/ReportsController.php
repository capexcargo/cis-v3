<?php

namespace App\Http\Controllers\Oims\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function orderManagementReports()
    {
        return view('view/oims/reports/order-management-reports/index');
    }
}
