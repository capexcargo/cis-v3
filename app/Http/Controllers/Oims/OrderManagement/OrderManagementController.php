<?php

namespace App\Http\Controllers\Oims\OrderManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Oims\DispatchControlHub\DispatchControlHubInterface;
use App\Interfaces\Oims\EDtr\EDtrInterface;
use App\Interfaces\Oims\TeamRouteAssignment\TeamRouteAssignmentInterface;
use App\Interfaces\Oims\TransactionEntry\TransactionEntryInterface;
use App\Models\CrmBooking;
use App\Models\OimsEdtr;
use App\Models\OimsEdtrDetails;
use App\Models\OimsTeamRouteAssignment;
use App\Models\OimsVehicle;
use App\Models\RouteCategoryReference;
use App\Models\UserDetails;
use App\Traits\ResponseTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderManagementController extends Controller
{
    public function teamRouteAssignmentOM()
    {
        return view('view/oims/order-management/team-route-assignment/index');
    }

    // public function printTR(TeamRouteAssignmentInterface $team_interface)
    // {


    //     $request = [
    //         'paginate' => $this->paginate,
    //         'stats' => $this->stats,
    //         'getuserbranch' => $this->getuserbranch,

    //     ];

    //     $response = $team_interface->print($request);
    //     if ($response['code'] != 200) {
    //         $this->sweetAlertDefaultError('error', $response['message'], $response['result']);
    //         $response['result'] =
    //             [
    //                 'team_routes' => [],
    //             ];
    //     }
    //     return view('view/oims/order-management/team-route-assignment/print', [
    //         'team_routes' => $response['result']['team_routes']
    //     ]);
    // }
    public $current;
    public $bid;
    public $totalTRK;
    public $totalTRKActive;
    public $totalTRKInactive;
    public $totalMT;
    public $getTotalMTActive;
    public $totalMTActive;
    public $totalMTInactive;
    public $totalFTE;
    public $totalFTEActive;
    public $totalFTEActiveD;
    public $totalFTEActiveC1;
    public $totalFTEActiveC2;
    public $totalFTEInactive;
    public $getRoutes = [];
    public $getCurrent = [];
    public $plate_id;
    public $did;
    public $cid1;
    public $cid2;
    public $rid;
    public $vcdd;
    public $getuserbranch;
    public $transactionentry;
    public $team_routes = [];

    public function printTR(EDtrInterface $edtr_interface, $id)
    {
        $getub = UserDetails::where('user_id', $id)->first();
        $this->getuserbranch = $getub->branch_id;

        $this->getCurrent = OimsTeamRouteAssignment::with('teamDetails')->where('branch_id', $this->getuserbranch)->whereDate('dispatch_date', Carbon::today())->first();
        if ($this->getCurrent != null) {
            $this->current = $this->getCurrent->tra_no;
            $this->bid = $this->getCurrent->branch_id;
            $this->vcdd = $this->getCurrent->dispatch_date;

            foreach ($this->getCurrent->teamDetails as $c => $getCurrents) {
                $this->plate_id[$c] = $getCurrents->plate_no_id;
                $this->did[$c] = $getCurrents->driver_id;
                $this->cid1[$c] = $getCurrents->checker1_id;
                $this->cid2[$c] = $getCurrents->checker2_id;

                $this->rid[$c] = $getCurrents->route_category_id;
            }
        } else {
            $this->current = '-';
        }

        $getTotalTRK = OimsVehicle::with('teamDetails')->where('vehicle_type', 1)->get();
        $this->totalTRK = count($getTotalTRK);

        if ($this->getCurrent != null) {
            $getTotalTRKActive = OimsVehicle::where('branch_id', $this->bid)->where('vehicle_type', 1)->whereIn('id', $this->plate_id)->get();
            $this->totalTRKActive = count($getTotalTRKActive);
            $this->totalTRKInactive = $this->totalTRK - $this->totalTRKActive;
        } else {
            $this->totalTRKActive = 0;
            $this->totalTRKInactive = 0;
        }

        $getTotalMT = OimsVehicle::with('teamDetails')->where('vehicle_type', 2)->get();
        $this->totalMT = count($getTotalMT);
        if ($this->getCurrent != null) {
            $getTotalMTActive = OimsVehicle::where('branch_id', $this->bid)->where('vehicle_type', 2)->whereIn('id', $this->plate_id)->get();

            $this->totalMTActive = count($getTotalMTActive);
            $this->totalMTInactive = ($this->totalMT - $this->totalMTActive);
        } else {
            $this->totalMTActive = 0;
            $this->totalMTInactive = 0;
        }

        $getTotalFTE = UserDetails::whereIn('position_id', [25, 26])->get();
        $this->totalFTE = count($getTotalFTE);

        if ($this->getCurrent != null) {
            $getTotalFTEActiveD = UserDetails::where('branch_id', $this->bid)->whereIn('user_id', $this->did)->get();
            $getTotalFTEActiveC1 = UserDetails::where('branch_id', $this->bid)->whereIn('user_id', $this->cid1)->get();
            $getTotalFTEActiveC2 = UserDetails::where('branch_id', $this->bid)->whereIn('user_id', $this->cid2)->get();

            $this->totalFTEActiveD = count($getTotalFTEActiveD);
            $this->totalFTEActiveC1 = count($getTotalFTEActiveC1);
            $this->totalFTEActiveC2 = count($getTotalFTEActiveC2);

            $this->totalFTEActive = $this->totalFTEActiveD + $this->totalFTEActiveC1 + $this->totalFTEActiveC2;
            $this->totalFTEInactive = $this->totalFTE - $this->totalFTEActive;
        } else {
            $this->totalFTEActive = 0;
            $this->totalFTEInactive = 0;
        }

        $this->getRoutes = RouteCategoryReference::with(['teamDetails' => function ($query) {
            $query->withCount('teamPlateReference');
            $query->withCount('teamDriverReference');
            $query->withCount('teamChecker1Reference');
            $query->withCount('teamChecker2Reference');
            $query->with(['teamPlateReference' => function ($query2) {
                $query2->withCount('teamDetails');
            }]);
            $query->whereDate('dispatch_date', Carbon::today());
        }])->withCount(['teamDetails as truck' => function ($query) {
            $query->whereDate('dispatch_date', Carbon::today())
                ->where('vehicle_type', 1);
        }, 'teamDetails as motorcycle' => function ($query) {
            $query->whereDate('dispatch_date', Carbon::today())
                ->where('vehicle_type', 2);
        }])
            ->orderBy('name', 'ASC')->get();


        $this->team_routes = OimsVehicle::with(['teamDetails' => function ($query) {
            $query->whereDate('dispatch_date', Carbon::today());
            $query->with('teamIdReference', 'teamRouteReference', 'teamPlateReference', 'teamChecker2Reference');
            $query->with(['teamRouteAssignmentReference' => function ($queryd1) {
                $queryd1->groupBy('id')->get();
            }]);
            $query->with(['teamDriverReference' => function ($queryd1) {
                $queryd1->with(['userDetails' => function ($queryd2) {
                    $queryd2->with('timeLog');
                }]);
            }]);
            $query->with(['teamChecker1Reference' => function ($queryd1) {
                $queryd1->with(['userDetails' => function ($queryd2) {
                    $queryd2->with('timeLog');
                }]);
            }]);
            $query->with(['teamChecker2Reference' => function ($queryd1) {
                $queryd1->with(['userDetails' => function ($queryd2) {
                    $queryd2->with('timeLog');
                }]);
            }]);
        }])
            ->paginate(10);


        return view(
            'view/oims/order-management/team-route-assignment/print',
            [
                'team_routes' => $this->team_routes,
                'getCurrent' => $this->getCurrent,
                'current' => $this->current,
                'bid' => $this->bid,
                'plate_id' => $this->plate_id,
                'did' => $this->did,
                'cid1' => $this->cid1,
                'cid2' => $this->cid2,
                'rid' => $this->rid,
                'totalTRK' => $this->totalTRK,
                'totalTRKActive' => $this->totalTRKActive,
                'totalTRKInactive' => $this->totalTRKInactive,
                'totalMT' => $this->totalMT,
                'totalMTActive' => $this->totalMTActive,
                'totalMTInactive' => $this->totalMTInactive,
                'totalFTE' => $this->totalFTE,
                'totalFTEActiveD' => $this->totalFTEActiveD,
                'totalFTEActiveC1' => $this->totalFTEActiveC1,
                'totalFTEActiveC2' => $this->totalFTEActiveC2,
                'totalFTEActive' => $this->totalFTEActive,
                'totalFTEInactive' => $this->totalFTEInactive,
                'getRoutes' => $this->getRoutes,
            ]
        );
    }

    public function routeCategoryOM()
    {
        return view('view/oims/order-management/team-route-assignment/route-category/index');
    }

    public function areaOM()
    {
        return view('view/oims/order-management/team-route-assignment/area-mgmt/index');
    }

    public function teamOM()
    {
        return view('view/oims/order-management/team-route-assignment/team-mgmt/index');
    }

    public function quadrantOM()
    {
        return view('view/oims/order-management/team-route-assignment/quadrant-mgmt/index');
    }

    public function tEDropdowMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/index');
    }

    public function branchMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/branch-mgmt/index');
    }

    public function transportModeMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/transport-mgmt/index');
    }

    public function payModeMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/paymode-mgmt/index');
    }

    public function serviceMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/service-mgmt/index');
    }

    public function odaopaMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/odaopa-mgmt/index');
    }

    public function serviceAreaMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/service-area-mgmt/index');
    }

    public function serviceabilityMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/serviceability-mgmt/index');
    }

    public function zipcodeMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/zipcode-mgmt/index');
    }

    public function barangayMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/barangay-mgmt/index');
    }

    public function municipalityMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/municipality-mgmt/index');
    }

    public function provinceMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/province-mgmt/index');
    }

    public function dangerousMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/dangerous-mgmt/index');
    }

    public function packagingMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/packaging-mgmt/index');
    }

    public function reasonMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/reason-mgmt/index');
    }

    public function cargoMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/cargo-mgmt/index');
    }

    public function remarksMgmtOM()
    {
        return view('view/oims/order-management/t-e-dropdown-mgmt/remarks-mgmt/index');
    }

    public function waybillRegistryOM()
    {
        return view('view/oims/order-management/waybill-registry/index');
    }

    use ResponseTrait;

    public $dispatchch;
    public function __construct(DispatchControlHubInterface $dispatchch, TransactionEntryInterface $transactionentry)
    {
        $this->dispatchch = $dispatchch;
        $this->transactionentry = $transactionentry;
    }

    public function updateactualStartTravel(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->dispatchch->actualStartTravel($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updateactualEndTravel(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->dispatchch->actualEndTravel($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updatepickupConfirmedtime(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->dispatchch->pickupConfirmedtime($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updatepickupStartTime(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->dispatchch->pickupStartTime($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updatepickupEndTime(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->dispatchch->pickupEndTime($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updatedeliveryStartTime(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->dispatchch->deliveryStartTime($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updatedeliveryEndTime(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->dispatchch->deliveryEndTime($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function dispatchConsole()
    {
        return view('view/oims/order-management/dispatch-console/index');
    }

    public function transactionEntryOM()
    {
        return view('view/oims/order-management/transaction-entry/index',);
        // ['id' => $id]);
    }

    public function transactionEntryOMCreate($id)
    {
        return view(
            'view/oims/order-management/transaction-entry/create',
            ['id' => $id]
        );
    }



    public function transactionSumOM()
    {
        return view('view/oims/order-management/transaction-summary/index');
    }

    public function transactionEntryWS()
    {
        return view('view/oims/order-management/transaction-entry/waybill-summary');
    }

    public function printWS()
    // (PositionInterface $position_interface, $id)
    {
        // $response = $position_interface->show($id);

        // abort_if($response['code'] != 200, $response['code'], $response['message']);

        // $this->position = $response['result'];

        return view('view/oims/order-management/transaction-entry/print');
        // ['position' => $this->position]);
    }

    public function printSTCKR()
    // (PositionInterface $position_interface, $id)
    {
        // $response = $position_interface->show($id);

        // abort_if($response['code'] != 200, $response['code'], $response['message']);

        // $this->position = $response['result'];

        return view('view/oims/order-management/transaction-entry/printsticker');
        // ['position' => $this->position]);
    }

    public function eDtr()
    {
        return view('view/oims/order-management/e-dtr/index');
    }

    public $getEDtrs = [];
    public $Edtrid;
    public $rmrefno;
    public $rmcreated;
    public $rmdisp;
    public $rmbrnch;
    public $rmplate;
    public $rmteam;
    public $rmdrv;
    public $rmchk1;
    public $rmchk2;
    public $latestPickup;

    public function print(EDtrInterface $edtr_interface, $id)
    {
        // dd($id);
        $this->Edtrid = $id;

        $getEdtrDet = OimsEdtrDetails::where('edtr_id', $this->Edtrid)->latest('created_at')->first();

        $this->latestPickup = date('m/d/Y H:i:s', strtotime($getEdtrDet['created_at']));

        $this->getEDtrs = OimsEdtr::with('Traref', 'cbref', 'edtrBranch')->with(['Traref' => function ($query) {
            $query->with('teamRouteAssignmentReference', 'teamIdReference', 'teamPlateReference', 'teamDriverReference', 'teamChecker1Reference', 'teamChecker2Reference');
        }])
            ->with(['edtrDetails' => function ($query) {
                $query->with(['edtrbook' => function ($query) {
                    $query->with('BookShipper', 'FinalStatusReferenceBK', 'ActivityReferenceBK');
                }]);
            }])->where('id', $this->Edtrid)->get();
        // dd($this->getEDtrs);

        $this->rmrefno = $this->getEDtrs[0]['reference_no'];
        $this->rmcreated = date('m/d/Y', strtotime($this->getEDtrs[0]['dispatch_date']));
        $this->rmdisp = date('m/d/Y', strtotime($this->getEDtrs[0]['dispatch_date']));
        $this->rmbrnch = $this->getEDtrs[0]['edtrBranch']['name'];
        $this->rmplate = $this->getEDtrs[0]['Traref']['teamPlateReference']['plate_no'];

        $this->rmteam = $this->getEDtrs[0]['Traref']['teamIdReference']['name'];
        $this->rmdrv = $this->getEDtrs[0]['Traref']['teamDriverReference']['name'];
        $this->rmchk1 = $this->getEDtrs[0]['Traref']['teamChecker1Reference']['name'] ?? '-';
        $this->rmchk2 = $this->getEDtrs[0]['Traref']['teamChecker2Reference']['name'] ?? '-';

        // dd(
        //     $this->rmrefno,
        //     $this->rmcreated,
        //     $this->rmdisp,
        //     $this->rmbrnch,
        //     $this->rmplate,
        //     $this->rmteam,
        //     $this->rmdrv,
        //     $this->rmchk1,
        //     $this->rmchk2
        // );
        return view(
            'view/oims/order-management/e-dtr/print',
            [
                'getEDtrs' => $this->getEDtrs,
                'rmrefno' => $this->rmrefno,
                'rmcreated' => $this->rmcreated,
                'rmdisp' => $this->rmdisp,
                'rmbrnch' => $this->rmbrnch,
                'rmplate' => $this->rmplate,
                'rmteam' => $this->rmteam,
                'rmdrv' => $this->rmdrv,
                'rmchk1' => $this->rmchk1,
                'rmchk2' => $this->rmchk2,
                'latestPickup' => $this->latestPickup
            ]
        );
    }

    public function vUtil()
    {
        return view('view/oims/order-management/vehicle-utilization/index');
    }

    public function createTransactionEntry(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );
        // $det['details'] = json_decode($request['details']);
        // dd($request);
        $response = $this->transactionentry->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
