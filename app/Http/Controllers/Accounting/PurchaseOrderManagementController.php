<?php

namespace App\Http\Controllers\Accounting;

use App\Http\Controllers\Controller;
use App\Models\Accounting\CanvassingSupplier;
use Illuminate\Http\Request;

class PurchaseOrderManagementController extends Controller
{
    public function index()
    {
        return view('view/accounting/purchase-order-management/index');
    }

    public function printPurchaseOrder($selected = '[]')
    {
        $purchase_orders = CanvassingSupplier::with(['canvassing' => function($query){
            $query->with('createdBy');
        },'canvassingSupplierItem' => function ($query) {
            $query->with('supplierItem')->where('final_status', true);
        }])->whereIn('po_reference_no', json_decode($selected))->get();
        
        return view('view/accounting/purchase-order-management/print-purchase-order', compact('purchase_orders'));
    }
}
