<?php

namespace App\Http\Controllers\Accounting\SupplierManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index()
    {
        return view('view/accounting/supplier-management/supplier/index');
    }
}
