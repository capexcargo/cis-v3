<?php

namespace App\Http\Controllers\Accounting\RequestManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\Accounting\RequestForPayment;
use App\Traits\ResponseTrait;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use DB;

class AddAttachementController extends Controller
{
    use ResponseTrait;

    /**
     * Request title
     *
     * @var string
     */
    protected $title = 'Add Attachment to RFP';

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(AttachmentRequest $request, RequestForPayment $rfp){

        DB::beginTransaction();
        try {
            $month = strtolower(date('F', strtotime($rfp->created_at)));
            $year = date('Y', strtotime($rfp->created_at));

            foreach ($request->attachments as $attachment) {

                $file_path = strtolower(str_replace(" ", "_", $year . '/' . $month . '/attachments/request_for_payment/'));
                $file_name = date('mdYHis') . uniqid() . '.' . $attachment->extension();

                if (in_array($attachment->extension(), config('filesystems.image_type'))) {
                    $image_resize = Image::make($attachment->getRealPath())->resize(1024, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });

                    Storage::disk('accounting_gcs')->put($file_path . $file_name, $image_resize->stream()->__toString());
                } else if (in_array($attachment->extension(), config('filesystems.file_type'))) {
                    Storage::disk('accounting_gcs')->putFileAs($file_path, $attachment, $file_name);
                }

                $rfp->attachments()->create([
                    'path' => $file_path,
                    'name' => $file_name,
                    'extension' => $attachment->extension(),
                ]);
            }

            DB::commit();
            return $this->response(200, $this->title, []);
        } catch (\Exception $e) {
            dd($e);
            return $this->response(500, 'Something Went Wrong', $e->getMessage());
        }
    }
}

class AttachmentRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'attachments' => 'required',
            'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),
        ];
    }

}
