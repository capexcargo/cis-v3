<?php

namespace App\Http\Controllers\Accounting\BudgetManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BudgetPlanSummaryController extends Controller
{
    public function index()
    {
        return view('view/accounting/budget-management/budget-plan-summary/index');
    }
}
