<?php

namespace App\Http\Controllers\Accounting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OpexTypeManagementController extends Controller
{
    public function index()
    {
        return view('view/accounting/opex-type-management/index');
    }
}
