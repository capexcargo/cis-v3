<?php

namespace App\Http\Controllers\Accounting\ApprovalManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RequestForPaymentController extends Controller
{
    public function index()
    {
        return view('view/accounting/approval-management/request-for-payment/index');
    }
}
