<?php

namespace App\Http\Controllers\Accounting\ApprovalManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CheckVoucherApprovalController extends Controller
{
    public function index()
    {
        return view('view/accounting/approval-management/check-voucher/index');
    }
}
