<?php

namespace App\Http\Controllers\Accounting;

use App\Http\Controllers\Controller;
use App\Models\Accounting\RequestForPayment;

class RequestManagementController extends Controller
{
    public function index()
    {
        return view('view/accounting/request-management/index');
    }

    public function monitoring()
    {
        return view('view/accounting/request-management/monitoring');
    }

    public function printRequestForPayment($selected = '[]')
    {
        $request_for_payments = RequestForPayment::with(['payee', 'branch', 'priority', 'CAReferenceType', 'approver1Signature', 'approver2Signature', 'approver3Signature', 'budget' => function ($query) {
            $query->with('division', 'source', 'chart');
        }, 'requestForPaymentDetails' => function ($query) {
            $query->with('paymentType');
        }])->whereHas('budget')
            ->whereIn('reference_id', json_decode($selected))->get();

        // dd($request_for_payments);

        return view('view/accounting/request-management/print-request-for-payment', compact('request_for_payments'));
    }
}
