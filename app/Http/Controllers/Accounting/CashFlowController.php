<?php

namespace App\Http\Controllers\Accounting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CashFlowController extends Controller
{
    public function index()
    {
        return view('view/accounting/cash-flow/index');
    }
}
