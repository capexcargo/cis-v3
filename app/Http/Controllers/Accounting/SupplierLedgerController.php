<?php

namespace App\Http\Controllers\Accounting;

use App\Http\Controllers\Controller;
use App\Models\Accounting\Supplier;
use Illuminate\Http\Request;

class SupplierLedgerController extends Controller
{
    public function index()
    {
        return view('view/accounting/supplier-ledger/index');
    }

    public function printSupplierLedger($selected = '[]')
    {
        $suppliers = Supplier::with(['requestForPayments' => function ($query) {
            $query->whereHas('checkVoucher');
        }, 'industry', 'type'])
            ->withSum(['requestForPayments'  => function ($query) {
                $query->whereHas('checkVoucher');
            }], 'amount')
            ->whereHas('requestForPayments', function ($query) {
                $query->whereHas('checkVoucher');
            })
            ->whereIn('id', json_decode($selected))->get();
        return view('view/accounting/supplier-ledger/print-supplier-ledger', compact('suppliers'));
    }
}
