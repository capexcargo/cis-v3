<?php

namespace App\Http\Controllers\Accounting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CaReferenceManagementController extends Controller
{
    public function index()
    {
        return view('view/accounting/ca-reference-management/index');
    }
}
