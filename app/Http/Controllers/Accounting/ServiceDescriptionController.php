<?php

namespace App\Http\Controllers\Accounting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ServiceDescriptionController extends Controller
{
    public function index()
    {
        return view('view/accounting/service-description/index');
    }
}
