<?php

namespace App\Http\Controllers\Accounting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LiquidationManagementController extends Controller
{
    public function index()
    {
        return view('view/accounting/liquidation-management/index');
    }
}
