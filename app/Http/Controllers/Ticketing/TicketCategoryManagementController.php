<?php

namespace App\Http\Controllers\Ticketing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TicketCategoryManagementController extends Controller
{
    public function index()
    {
        return view('view/ticketing/category-management/index');
    }
}
