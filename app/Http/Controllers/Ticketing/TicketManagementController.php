<?php

namespace App\Http\Controllers\Ticketing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TicketManagementController extends Controller
{
    public function index()
    {
        return view('view/ticketing/ticket-management/index');
    }

    public function indexalltickets()
    {
        return view('view/ticketing/ticket-management/indexalltickets');
    }

    public function indexmytickets()
    {
        return view('view/ticketing/ticket-management/indexmytickets');
    }

    public function indexmytasks()
    {
        return view('view/ticketing/ticket-management/indexmytasks');
    }
}
