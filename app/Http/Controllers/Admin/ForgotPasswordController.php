<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Globals\ForgotPasswordInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ForgotPasswordController extends Controller
{
    use ResponseTrait;
    
    public $forgot_password_interface;

    public function __construct(ForgotPasswordInterface $forgot_password_interface)
    {
        $this->forgot_password_interface = $forgot_password_interface;
    }

    public function forgot(Request $request)
    {
        $request_data = [
            'email' => $request->emails,
            'password' => $request->passwords,
            'otp' => $request->otps,
            'id' => Auth()->user()->id
        ];

        // dd($request_data);
        $response = $this->forgot_password_interface->update($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
}
