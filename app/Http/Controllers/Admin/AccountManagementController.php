<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Globals\AccountManagementRepository;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountManagementController extends Controller
{
    use ResponseTrait;

    public function show(Request $request)
    {

        $account_repository = new AccountManagementRepository();
        $response = $account_repository->show($request->id);
        // if ($response['code'] != 200) $this->sweetAlertError('error', $response['message'], $response['result']);

        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function showCurrent()
    {

        $account_repository = new AccountManagementRepository();
        $response = $account_repository->show(Auth::user()->id);
        // if ($response['code'] != 200) $this->sweetAlertError('error', $response['message'], $response['result']);

        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function index()
    {
        return view('view/admin/account-management/index');
    }

    public function cisRegistration()
    {
        return view('view/admin/account-management/cis-registration');
    }
}
