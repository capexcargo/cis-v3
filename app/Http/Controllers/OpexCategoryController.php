<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OpexCategoryController extends Controller
{
    public function index()
    {
        return view('view/accounting/opex-category/index');
    }
}
