<?php

namespace App\Http\Controllers\Hrim\PayrollManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CutOffMgmtController extends Controller
{
    public function index()
    {
        return view('view/hrim/payroll-management/cut-off-management/index');
    }
}
