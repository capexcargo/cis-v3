<?php

namespace App\Http\Controllers\Hrim\PayrollManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Month13thController extends Controller
{
    public function index()
    {
        return view('view/hrim/payroll-management/month13th/index');
    }
}
