<?php

namespace App\Http\Controllers\Hrim\PayrollManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HolidayManagementController extends Controller
{
    public function index()
    {
        return view('view/hrim/payroll-management/holiday-management/index');
    }
}
