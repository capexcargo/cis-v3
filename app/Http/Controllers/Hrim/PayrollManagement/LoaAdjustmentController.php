<?php

namespace App\Http\Controllers\Hrim\PayrollManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoaAdjustmentController extends Controller
{
    public function index()
    {
        return view('view/hrim/payroll-management/loa-adjustment-management/index');
    }
}
