<?php

namespace App\Http\Controllers\Hrim;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('view/hrim/dashboard/index');
    }
}
