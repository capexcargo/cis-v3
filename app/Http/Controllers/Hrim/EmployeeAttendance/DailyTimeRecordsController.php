<?php

namespace App\Http\Controllers\Hrim\EmployeeAttendance;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrim\EmployeeAttendance\DailyTimeRecordsInterface;
use App\Interfaces\Hrim\EmployeeAttendance\TarInterface;
use App\Models\Hrim\TarReasonReference;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class DailyTimeRecordsController extends Controller
{
    use ResponseTrait;

    public $timeandout_interface;

    public function __construct(DailyTimeRecordsInterface $daily_time_records_interface, TarInterface $tar_interface)
    {
        $this->daily_time_records_interface = $daily_time_records_interface;
        $this->tar_interface = $tar_interface;
    }

    public function show(Request $request)
    {

        $request_data = [
            'month' => $request->month,
            'cut_off' => $request->cut_off,
            'year' => $request->year,
            'sort_field' => $request->sortField,
            'sort_type' => ($request->sortAsc  ? 'asc' : 'desc'),
        ];

        $response = $this->daily_time_records_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function create(Request $request)
    {
        $request_data = [
            'time_log_id' => $request->time_log_id,
            'date_from' => $request->date_from,
            'date_to' => $request->date_to,
            'actual_time_in' => $request->actual_time_in,
            'actual_time_out' => $request->actual_time_out,
            'reason_of_adjustment' => $request->reason_of_adjustment,
        ];

        $response = $this->tar_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function getTarReason()
    {
        $this->tar_reason_reference = TarReasonReference::get();
        return $this->tar_reason_reference;
    }


    public function index()
    {
        return view('view/hrim/employee-attendance/daily-time-records/index');
    }
}
