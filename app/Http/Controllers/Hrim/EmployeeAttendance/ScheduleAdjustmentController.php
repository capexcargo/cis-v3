<?php

namespace App\Http\Controllers\Hrim\EmployeeAttendance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ScheduleAdjustmentController extends Controller
{
    public function index()
    {
        return view('view/hrim/employee-attendance/schedule-adjustment/index');
    }
}
