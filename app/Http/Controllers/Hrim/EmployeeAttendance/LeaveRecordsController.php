<?php

namespace App\Http\Controllers\Hrim\EmployeeAttendance;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrim\EmployeeAttendance\LeaveRecordsInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class LeaveRecordsController extends Controller
{
    use ResponseTrait;

    public $timeandout_interface;

    public function __construct(LeaveRecordsInterface $leave_records_interface)
    {
        $this->leave_records_interface = $leave_records_interface;
    }

    public function create(Request $request)
    {
        $request_data = [
            'inclusive_date_from' => $request->inclusive_date_from,
            'inclusive_date_to' => $request->inclusive_date_to,
            'resume_of_work' => $request->resume_of_work,
            'type_of_leave' => $request->type_of_leave,
            'is_with_medical_certificate' => $request->is_with_medical_certificate,
            'apply_for' => $request->apply_for,
            'is_with_pay' => $request->is_with_pay,
            'reason' => $request->reason,
            'reliever' => $request->reliever,
            'attachment' => $request->attachment ? $request->attachment : "",
        ];

        $response = $this->leave_records_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function show(Request $request)
    {
        $request_data = [
            'sort_field' => $request->sortField,
            'sort_type' => ($request->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $request->paginate,
        ];

        $response = $this->leave_records_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function update($id, Request $request)
    {
        $request_data = [
            'inclusive_date_from' => $request->inclusive_date_from,
            'inclusive_date_to' => $request->inclusive_date_to,
            'resume_of_work' => $request->resume_of_work,
            'type_of_leave' => $request->type_of_leave,
            'is_with_medical_certificate' => $request->is_with_medical_certificate,
            'apply_for' => $request->apply_for,
            'is_with_pay' => $request->is_with_pay,
            'reason' => $request->reason,
            'reliever' => $request->reliever,
            'attachment' => $request->attachment ? $request->attachment : "",
        ];

        $response = $this->leave_records_interface->update($id, $request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function index()
    {
        return view('view/hrim/employee-attendance/leave-records/index');
    }
}
