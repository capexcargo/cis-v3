<?php

namespace App\Http\Controllers\Hrim\EmployeeAttendance;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrim\EmployeeAttendance\OvertimeRecordsInterface;
use App\Repositories\Hrim\EmployeeAttendance\OvertimeRecordsRepository;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class OvertimeRecordsController extends Controller
{

    use ResponseTrait;

    public $date_time_in;
    public $date_time_out;
    public $actual_date_from;
    public $actual_date_to;
    public $is_have_ot_break;
    public $from;
    public $to;
    public $rendered_ot_hours;
    public $overtime_request;
    public $type_of_ot;
    public $reason;

    public $timeandout_interface;

    public function __construct(OvertimeRecordsInterface $overtime_records_interface)
    {
        $this->overtime_records_interface = $overtime_records_interface;
    }


    public function get_compute(Request $request)
    {

       
        $overtime_records_repository = new OvertimeRecordsRepository();
        $requests = [
            'user_id' => $request->user_id,
            'date' => $request->date_time_in,
            'actual_date_to' => $request->actual_date_to,
            'to' => $request->to,
            'is_have_ot_break' => $request->is_have_ot_break,
        ];
       
        $responses = $overtime_records_repository->showDate($requests);
        if ($responses['code'] != 200) {
            $this->responseAPI($responses['code'], $responses['message'], $responses['result']);
        }

        $this->time_log = $responses['result'];
        $request_data = [
            'time_log' => $this->time_log,
            'actual_date_to' => $request->actual_date_to,
            'to' => $request->to,
            'is_have_ot_break' => $request->is_have_ot_break,
        ];

        $response = $this->overtime_records_interface->getComputedOvertime($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function get_date(Request $request)
    {
        $request_data = [
            'user_id' =>  $request->user_id,

            'month' => $request->month,
            'cut_off' => $request->cut_off,
            'year' => $request->year,

            'time_log' => $request->time_log,
            'actual_date_from' => $request->actual_date_from,
            'actual_date_to' => $request->actual_date_to,
            'from' => $request->from,
            'to' => $request->to,
            'type_of_ot' => $request->type_of_ot,
            'reason' => $request->reason,
            'time_in' => $request->time_in,
            'time_out' => $request->time_out,
            'work_schedule' => $request->work_schedule,
            'rendered_ot_hours' => $request->rendered_ot_hours,
            'is_have_ot_break' => $request->is_have_ot_break,
            'actual_date_from' => $request->actual_date_from,
            'actual_date_to' => $request->actual_date_to,
            'overtime_request' => $request->overtime_request,

        ];

        $response = $this->overtime_records_interface->getOvertimed($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function get_date_specific(Request $request)
    {
        $request_data = [
            'user_id' =>  $request->user_id,

            'date_ot' => $request->date_ot,


        ];

        $response = $this->overtime_records_interface->getOvertimedate($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }



    public function create(Request $request)
    {
        $overtime_records_repository = new OvertimeRecordsRepository();
        $requests = [
            'user_id' => $request->user_id,
            'date' => $request->date_time_in,
            'actual_date_to' => $request->actual_date_to,
            'to' => $request->to,
            'is_have_ot_break' => $request->is_have_ot_break,
        ];
        $responses = $overtime_records_repository->showDate($requests);
        if ($responses['code'] != 200) {
            $this->responseAPI($responses['code'], $responses['message'], $responses['result']);
        }

        $this->time_log = $responses['result'];
// dd($this);

        $request_datas = [
            'time_log' => $this->time_log,
            'date_time_in' => $request->date_time_in,
            'date_time_out' => $request->date_time_out,
            'actual_date_from' => $request->actual_date_from,
            'actual_date_to' => $request->actual_date_to,
            'from' => $request->from,
            'to' => $request->to,
            'time_in' => $request->time_in,
            'time_out' => $request->time_out,
            'work_schedule' => $request->work_schedule,
            'rendered_ot_hours' => $request->rendered_ot_hours,
            'is_have_ot_break' => $request->is_have_ot_break,
            'overtime_request' => $request->overtime_request,
            'type_of_ot' => $request->type_of_ot,
            'reason' => $request->reason,
        ];

        $response = $this->overtime_records_interface->create($request_datas);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function show(Request $request)
    {
        $request_data = [
            'sort_field' => $request->sortField,
            'sort_type' => ($request->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $request->paginate,
        ];

        $response = $this->overtime_records_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
    public function index()
    {
        return view('view/hrim/employee-attendance/overtime-records/index');
    }
}
