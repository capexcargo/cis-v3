<?php

namespace App\Http\Controllers\Hrim\RecruitmentAndHiring;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmploymentCategoryTypeController extends Controller
{
    public function index()
    {
        return view('view/hrim/recruitment-and-hiring/employment-category-type/index');
    }
}
