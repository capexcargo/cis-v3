<?php

namespace App\Http\Controllers\Hrim\RecruitmentAndHiring;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OnboardingController extends Controller
{
    public function index()
    {
        return view('view/hrim/recruitment-and-hiring/onboarding/index');
    }
}
