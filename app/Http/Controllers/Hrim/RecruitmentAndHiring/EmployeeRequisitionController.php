<?php

namespace App\Http\Controllers\Hrim\RecruitmentAndHiring;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmployeeRequisitionController extends Controller
{
    public function index()
    {
        return view('view/hrim/recruitment-and-hiring/employee-requisition/index');
    }
    
    public function empolyee_index()
    {
        return view('view/hrim/employee-recruitment-and-hiring/employee-requisition/index');
    }
}
