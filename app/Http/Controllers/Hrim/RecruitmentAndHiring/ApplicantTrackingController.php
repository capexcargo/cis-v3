<?php

namespace App\Http\Controllers\Hrim\RecruitmentAndHiring;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApplicantTrackingController extends Controller
{
    public function index()
    {
        return view('view/hrim/recruitment-and-hiring/applicant-tracking/index');
    }
}
