<?php

namespace App\Http\Controllers\Hrim\EmployeeRecruitmentAndHiring;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmployeeRequisitionController extends Controller
{
    public function index()
    {
        return view('view/hrim/employee-recruitment-and-hiring/employee-requisition/index');
    }
}
