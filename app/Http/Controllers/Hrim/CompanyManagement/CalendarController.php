<?php

namespace App\Http\Controllers\Hrim\CompanyManagement;

use App\Http\Controllers\Controller;
use App\Repositories\Hrim\CompanyManagement\CalendarRepository;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CalendarController extends Controller
{
    use ResponseTrait;

    public function show(Request $request)
    {
        $request = [
            'with_trashed' => (Auth::user()->accesses()->contains('hrim_calendar_delete') ? true : false),
            'type' => $request->type,
            'date' => $request->date,
            'sort_field' => 'event_date_time',
            'sort_type' => 'asc',
        ];

        $calendar_repository = new CalendarRepository();
        $response = $calendar_repository->getCalendarEvents($request);
        if ($response['code'] != 200) $this->sweetAlertError('error', $response['message'], $response['result']);
        
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    
    public function index()
    {
        return view('view/hrim/company-management/calendar/index');
    }
}
