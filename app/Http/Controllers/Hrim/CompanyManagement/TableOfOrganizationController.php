<?php

namespace App\Http\Controllers\Hrim\CompanyManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TableOfOrganizationController extends Controller
{
    public function index()
    {
        return view('view/hrim/company-management/table-of-organization/index');
    }
}
