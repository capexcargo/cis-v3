<?php

namespace App\Http\Controllers\Hrim\CompanyManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrim\CompanyManagement\Bulletin\AnnouncementInterface;
use App\Interfaces\Hrim\CompanyManagement\Bulletin\MemoInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class BulletinController extends Controller
{

    use ResponseTrait;

    public $announcement_interface;

    public function __construct(AnnouncementInterface $announcement_interface,MemoInterface $memo_interface)
    {
        $this->announcement_interface = $announcement_interface;
        $this->memo_interface = $memo_interface;
    }


    public function showAnnouncement(Request $request)
    {
        $request_data = [
            'sort_field' => $request->sortField,
            'sort_type' => ($request->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $request->paginate,
        ];

        $response = $this->announcement_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function showMemos(Request $request)
    {
        $request_data = [
            'sort_field' => $request->sortField,
            'sort_type' => ($request->sortAsc  ? 'asc' : 'desc'),
            'paginate' => $request->paginate,
        ];

        $response = $this->memo_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function index()
    {
        return view('view/hrim/company-management/bulletin/index');
    }
}

