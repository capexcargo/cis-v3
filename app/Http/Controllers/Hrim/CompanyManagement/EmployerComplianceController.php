<?php

namespace App\Http\Controllers\Hrim\CompanyManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmployerComplianceController extends Controller
{
    public function index()
    {
        return view('view/hrim/company-management/employer-compliance/index');
    }
}
