<?php

namespace App\Http\Controllers\Hrim\Workforce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WorkScheduleController extends Controller
{
    public function index()
    {
        return view('view/hrim/workforce/work-schedule/index');
    }
}
