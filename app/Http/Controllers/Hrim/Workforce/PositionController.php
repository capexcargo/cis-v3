<?php

namespace App\Http\Controllers\Hrim\Workforce;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrim\Workforce\PositionInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    use ResponseTrait;
    public $position_interface;

    public $position;
    public $requirements;
    public $responsibilities;

    public function __construct(PositionInterface $position_interface)
    {
        $this->position_interface = $position_interface;
    }

    public function index()
    {
        return view('view/hrim/workforce/position/index');
    }


    public function print(PositionInterface $position_interface, $id)
    {
        $response = $position_interface->show($id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->position = $response['result'];

        return view('view/hrim/workforce/position/print', ['position' => $this->position]);
    }
}
