<?php

namespace App\Http\Controllers\Hrim\Workforce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JobLevelController extends Controller
{
    public function index()
    {
        return view('view/hrim/workforce/job-level/index');
    }
}
