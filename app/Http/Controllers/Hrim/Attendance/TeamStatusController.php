<?php

namespace App\Http\Controllers\Hrim\Attendance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TeamStatusController extends Controller
{
    public function index()
    {
        return view('view/hrim/attendance/team-status/index');
    }

    public function view($id)
    {
        return view('view/hrim/attendance/team-status/view', ['id' => $id]);
    }
}
