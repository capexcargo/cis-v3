<?php

namespace App\Http\Controllers\Hrim\Attendance;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrim\Attendance\DailyTimeRecordsInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class DailyTimeRecordsController extends Controller
{

  

    public function index()
    {
        return view('view/hrim/attendance/daily-time-records/index');
    }
}
