<?php

namespace App\Http\Controllers\Hrim\Attendance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LeaveRecordsController extends Controller
{
    public function index()
    {
        return view('view/hrim/attendance/leave-records/index');
    }

    public function view($id)
    {
        return view('view/hrim/attendance/leave-records/view', ['id' => $id]);
    }
}
