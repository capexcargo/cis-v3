<?php

namespace App\Http\Controllers\Hrim\Attendance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OvertimeRecordsController extends Controller
{
    public function index()
    {
        return view('view/hrim/attendance/overtime-records/index');
    }

    public function view($id)
    {
        return view('view/hrim/attendance/overtime-records/view', ['id' => $id]);
    }
}
