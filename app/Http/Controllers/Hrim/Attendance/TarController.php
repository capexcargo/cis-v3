<?php

namespace App\Http\Controllers\Hrim\Attendance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TarController extends Controller
{
    public function view($id)
    {
        return view('view/hrim/attendance/tar/view', ['id' => $id]);
    }
}
