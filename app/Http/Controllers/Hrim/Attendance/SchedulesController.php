<?php

namespace App\Http\Controllers\Hrim\Attendance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SchedulesController extends Controller
{
    public function index()
    {
        return view('view/hrim/attendance/schedules/index');
    }
}
