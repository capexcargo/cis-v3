<?php

namespace App\Http\Controllers\Hrim\Attendance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ScheduleAdjustmentController extends Controller
{
    public function view($id)
    {
        return view('view/hrim/attendance/schedule-adjustment/view', ['id' => $id]);
    }
}
