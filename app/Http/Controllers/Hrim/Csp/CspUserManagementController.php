<?php

namespace App\Http\Controllers\Hrim\Csp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CspUserManagementController extends Controller
{
    public function index()
    {
        return view('view/hrim/csp/csp-user-management/index');
    }
}
