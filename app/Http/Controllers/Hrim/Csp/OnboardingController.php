<?php

namespace App\Http\Controllers\Hrim\Csp;

use App\Http\Controllers\Controller;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class OnboardingController extends Controller
{
    public function index()
    {
        return view('view/hrim/csp/onboarding/index');
    }
}
