<?php

namespace App\Http\Controllers\Hrim\EmployeeCompensationAndBenefits;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrim\CompensationAndBenefits\LoansAndLedgerInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoansAndLedgerMonitoringController extends Controller
{
    use ResponseTrait;

    public $loans_ledger_interface;
    public $loans_payment_sched = [];

    public function __construct(LoansAndLedgerInterface $loans_ledger_interface)
    {
        $this->loans_ledger_interface = $loans_ledger_interface;
    }

    public function showPaymentSchedule(Request $request)
    {

        $response = $this->loans_ledger_interface->show_schedule_payment($request->id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function show(Request $request)
    {
        $request_data = [
            'sort_field' => $request->sort_field,
            'sort_type' => $request->sort_type,
            'paginate' => $request->paginate,
        ];

        $response = $this->loans_ledger_interface->index($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function create(Request $request)
    {
        $request_data = [
            'reference_number' => $request->reference_number,
            'purpose' => $request->purpose,
            'type' => $request->type,
            'principal_amount' => $request->principal_amount,
            'terms' => $request->terms,
            'term_type' => $request->term_type,
            'payment_start_date' => $request->payment_start_date,
            'payment_end_date' => $request->payment_end_date,
            'first_status' => 1,
        ];

        $response = $this->loans_ledger_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
    public function index()
    {
        return view('view/hrim/employee-compensation-and-benefits/loans-and-ledger-monitoring/index');
    }

    public function atd(LoansAndLedgerInterface $loans_and_ledger_interface, $id)
    {
        $response = $loans_and_ledger_interface->showForPrint($id, Auth::user()->id);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->loans_and_ledger = $response['result'];

        $this->employee_name = $this->loans_and_ledger->user->name;
        $this->position = $this->loans_and_ledger->userDetails->position->display;
        $this->date_filed = $this->loans_and_ledger->created_at;
        $this->division = $this->loans_and_ledger->userDetails->division->name;
        $this->type_of_loan = $this->loans_and_ledger->loanType->name;
        $this->principal_amount = $this->loans_and_ledger->principal_amount;
        $this->reference_no = $this->loans_and_ledger->reference_no;
        $this->interest = $this->loans_and_ledger->interest;
        $this->term_type = $this->loans_and_ledger->term_type;
        $this->terms = $this->loans_and_ledger->terms;
        $this->reason = $this->loans_and_ledger->purpose;
        $this->payment_start_date = $this->loans_and_ledger->start_date;
        $this->payment_end_date = $this->loans_and_ledger->end_date;

        return view('view/hrim/employee-compensation-and-benefits/loans-and-ledger-monitoring/print-atd', [
            'employee_name' => $this->employee_name,
            'position' => $this->position,
            'date_filed' => $this->date_filed,
            'division' => $this->division,
            'type_of_loan' => $this->type_of_loan,
            'principal_amount' => $this->principal_amount,
            'reference_no' => $this->reference_no,
            'interest' => $this->interest,
            'term_type' => $this->term_type,
            'terms' => $this->terms,
            'reason' => $this->reason,
            'payment_start_date' => $this->payment_start_date,
            'payment_end_date' => $this->payment_end_date,
        ]);
    }
}
