<?php

namespace App\Http\Controllers\Hrim\TalentManagement;

use App\Http\Controllers\Controller;

class PerformanceEvaluationController extends Controller
{

    public function index()
    {
        return view('view/hrim/talent-management/performance-evaluation/index');
    }

    public function view($id, $quarter, $years)
    {
        return view('view/hrim/talent-management/performance-evaluation/view', compact('id', 'quarter', 'years'));
    }
}
