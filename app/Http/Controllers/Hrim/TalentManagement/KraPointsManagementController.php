<?php

namespace App\Http\Controllers\Hrim\TalentManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KraPointsManagementController extends Controller
{
    public function index()
    {
        return view('view/hrim/talent-management/kra-points-management/index');
    }
}
