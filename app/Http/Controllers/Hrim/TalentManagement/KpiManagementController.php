<?php

namespace App\Http\Controllers\Hrim\TalentManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KpiManagementController extends Controller
{
    public function index()
    {
        return view('view/hrim/talent-management/kpi-management/index');
    }
}
