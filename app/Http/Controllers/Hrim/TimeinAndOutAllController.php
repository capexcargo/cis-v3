<?php

namespace App\Http\Controllers\Hrim;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrim\TimeInAndOutInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;


class TimeinAndOutAllController extends Controller
{
    public function index()
    {
        return view('view/hrim/time-in-and-out/index-all');
    }
}
