<?php

namespace App\Http\Controllers\Hrim\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoanGovtDeductionController extends Controller
{
    public function index()
    {
        return view('view/hrim/reports/loan-government-deduction/index');
    }
}
