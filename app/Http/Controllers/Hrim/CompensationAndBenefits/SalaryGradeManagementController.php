<?php

namespace App\Http\Controllers\Hrim\CompensationAndBenefits;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SalaryGradeManagementController extends Controller
{
    public function index()
    {
        return view('view/hrim/compensation-and-benefits/salary-grade-management/index');
    }
}
