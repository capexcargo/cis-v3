<?php

namespace App\Http\Controllers\Hrim\CompensationAndBenefits;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrim\CompensationAndBenefits\PayrollInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PayslipsController extends Controller
{

    use ResponseTrait;

    public $loans_ledger_interface;

    public function __construct(PayrollInterface $payroll_interface)
    {
        $this->payroll_interface = $payroll_interface;
    }

    public function show(Request $request)
    {
        $request_data = [
            'year' => $request->year,
            'month' => $request->month,
            'cut_off' => $request->cut_off,
            'branch' => $request->branch,
            'employee_id' => $request->employee_id,
            'employee_name' => $request->employee_name,
            'employee_category' => $request->employee_category,
            'employee_code' => $request->employee_code,
            'division' => $request->division,
            'job_rank' => $request->job_rank,
        ];

        $response = $this->payroll_interface->index2($request->user_id, $request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }


    public function index()
    {
        return view('view/hrim/compensation-and-benefits/payslips/index');
    }

    public function my_payslip()
    {
        return view('view/hrim/compensation-and-benefits/payslips/my-payslip');
    }

    public function printMyPayslip($id, $cutoff, $month, $year)
    {

        if ($cutoff == 1) {
            $begin = date('Y-m-d', strtotime($year . '-' . ($month - 1) . '-26'));
            $end = date('Y-m-d', strtotime($year . '-' . $month . '-10'));
        } else {
            $begin = date('Y-m-d', strtotime($year . '-' . ($month) . '-11'));
            $end = date('Y-m-d', strtotime($year . '-' . $month . '-25'));
        }
        $monthday = date('F Y', strtotime($year . '-' . $month . '-25'));

        if ($year == '' || $month == '' || $cutoff == '') {
            $payrolls = [];
        } else {
            // dd($where);
            $payrolls = DB::select('

        SELECT a.user_id, b.photo_path, b.photo_name, a.employee_number, a.first_name, a.last_name, a.hired_date, c.display AS emp_status, d.display AS job_rank, e.display AS position, f.display AS branch,
        g.basic_pay, g.cola, (g.basic_pay + g.cola) AS gross,ROUND((g.basic_pay / 26) / 8,2) as phour,
        
        (SELECT sum(ot.overtime_approved * (ROUND((g.basic_pay / 26) / 8,2) * bb.rate_percentage)) FROM hrim_overtime ot 
        LEFT JOIN hrim_date_category_reference bb ON bb.id = ot.date_category_id
        WHERE ot.user_id = a.user_id 
        AND ot.date_time_from >= "' . $begin . '" AND ot.date_time_from <= "' . $end . '") AS ot_pay,

        (SELECT sum(ot.overtime_approved * (ROUND((g.basic_pay / 26) / 8,2) * bb.rate_percentage)) FROM hrim_overtime ot 
        LEFT JOIN hrim_date_category_reference bb ON bb.id = ot.date_category_id
        WHERE ot.user_id = a.user_id 
        AND ot.date_time_from >= "' . $begin . '" AND ot.date_time_from <= "' . $end . '" and ot.date_category_id = 1) AS ot_reg_pay,

        (SELECT sum(ot.overtime_approved * (ROUND((g.basic_pay / 26) / 8,2) * bb.rate_percentage)) FROM hrim_overtime ot 
        LEFT JOIN hrim_date_category_reference bb ON bb.id = ot.date_category_id
        WHERE ot.user_id = a.user_id 
        AND ot.date_time_from >= "' . $begin . '" AND ot.date_time_from <= "' . $end . '" and ot.date_category_id = 2) AS ot_holiday_pay,

        (SELECT sum(ot.overtime_approved * (ROUND((g.basic_pay / 26) / 8,2) * bb.rate_percentage)) FROM hrim_overtime ot 
        LEFT JOIN hrim_date_category_reference bb ON bb.id = ot.date_category_id
        WHERE ot.user_id = a.user_id 
        AND ot.date_time_from >= "' . $begin . '" AND ot.date_time_from <= "' . $end . '" and ot.date_category_id = 3) AS ot_special_pay,

        (SELECT sum(ot.overtime_approved * (ROUND((g.basic_pay / 26) / 8,2) * bb.rate_percentage)) FROM hrim_overtime ot 
        LEFT JOIN hrim_date_category_reference bb ON bb.id = ot.date_category_id
        WHERE ot.user_id = a.user_id 
        AND ot.date_time_from >= "' . $begin . '" AND ot.date_time_from <= "' . $end . '" and ot.date_category_id = 4) AS ot_restday_pay,
        
        (SELECT round(SUM( tl.late * (ROUND((g.basic_pay / 26) / 8,2))), 2) FROM hrim_time_log tl WHERE tl.user_id = a.user_id  
        AND tl.date >= "' . $begin . '" AND tl.date <= "' . $end . '") AS tardiness,
        
        (SELECT SUM( tl.undertime * (ROUND((g.basic_pay / 26) / 8,2))) FROM hrim_time_log tl WHERE tl.user_id = a.user_id  
        AND tl.date >= "' . $begin . '" AND tl.date <= "' . $end . '") AS undertime,
        
        (SELECT SUM(bb.amount) FROM hrim_loan lns
        LEFT JOIN hrim_loan_details bb ON bb.loan_id = lns.id 
        where lns.user_id = a.user_id AND bb.month = "' . $month . '" AND bb.cutoff = "' . $cutoff . '" AND bb.year = "' . $year . '"  AND lns.`type` IN (1,5,6,7,8,9)) AS loans,

        (SELECT CASE WHEN bb.cutoff = 2 THEN SUM(bb.amount) ELSE 0 END
        FROM hrim_loan lns
        LEFT JOIN hrim_loan_details bb ON bb.loan_id = lns.id 
        where lns.user_id = a.user_id AND bb.month = "' . $month . '" AND bb.cutoff = "' . $cutoff . '" AND bb.year = "' . $year . '" AND lns.`type` IN (2,3,4) AND lns.final_status = 3) as loansgovt,

        (SELECT CASE WHEN bb.cutoff = 2 THEN SUM(bb.amount) ELSE 0 END
        FROM hrim_loan lns
        LEFT JOIN hrim_loan_details bb ON bb.loan_id = lns.id 
        where lns.user_id = a.user_id AND bb.month = "' . $month . '" AND bb.cutoff = "' . $cutoff . '" AND bb.year = "' . $year . '" AND lns.`type` IN (2) AND lns.final_status = 3) as loansgovtsss,

        (SELECT CASE WHEN bb.cutoff = 2 THEN SUM(bb.amount) ELSE 0 END
        FROM hrim_loan lns
        LEFT JOIN hrim_loan_details bb ON bb.loan_id = lns.id 
        where lns.user_id = a.user_id AND bb.month = "' . $month . '" AND bb.cutoff = "' . $cutoff . '" AND bb.year = "' . $year . '" AND lns.`type` IN (3) AND lns.final_status = 3) as loansgovtpagibig,

        (SELECT CASE WHEN bb.cutoff = 2 THEN SUM(bb.amount) ELSE 0 END
        FROM hrim_loan lns
        LEFT JOIN hrim_loan_details bb ON bb.loan_id = lns.id 
        where lns.user_id = a.user_id AND bb.month = "' . $month . '" AND bb.cutoff = "' . $cutoff . '" AND bb.year = "' . $year . '" AND lns.`type` IN (4) AND lns.final_status = 3) as loansgovtphilhealth,

        (SELECT COUNT(abs.id)  FROM hrim_absent abs WHERE abs.user_id = a.user_id AND 
        abs.absent_date >= "' . $begin . '" AND abs.absent_date <= "' . $end . '") AS absentcount,

        (SELECT round(sum(leaves.apply_for), 2) FROM hrim_leave_details leaves WHERE leaves.user_id = a.user_id AND leaves.leave_type = 0 AND leaves.leave_date >= "' . $begin . '" AND leaves.leave_date <= "' . $end . '") AS leaves,

        (SELECT round(sum(leaves.apply_for), 2) FROM hrim_leave_details leaves WHERE leaves.user_id = a.user_id AND leaves.leave_type = 1 AND leaves.leave_date >= "' . $begin . '" AND leaves.leave_date <= "' . $end . '") AS leaveswtpay,

        (SELECT CASE WHEN g.basic_pay <= 10000 THEN 350
        ELSE g.basic_pay * a.premium_rate END AS asd
        FROM hrim_philhealth a WHERE a.year = 2022) AS philhealth,
        
        (SELECT a.share
        FROM hrim_pagibig a WHERE a.year = 2022) AS pagibig,
       
        (SELECT SUM(htl.rendered_time) * (ROUND((g.basic_pay / 26) / 8,2))
        FROM hrim_rest_day rd 
        LEFT JOIN hrim_time_log htl on htl.user_id = rd.user_id where rd.date >= "' . $begin . '" and rd.date <= "' . $end . '"
        AND rd.user_id = a.user_id AND (case when rd.date = htl.date then 1 END) IS NOT null) as restdaypays,

        (SELECT 

        round(SUM(
        CONCAT(
        HOUR(
        TIMEDIFF( 
        (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) <= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT(htls.time_out_date, " ", htls.time_out)) 
        when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT((CURDATE()), " 06:00:00")) END), (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) then (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) END))) 
        ,".",
        MINUTE(
        TIMEDIFF( 
        (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) <= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT(htls.time_out_date, " ", htls.time_out)) 
        when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE()), " 06:00:00")) then (CONCAT((CURDATE()), " 06:00:00")) END), (case when (CONCAT(htls.time_out_date, " ", htls.time_out)) >= (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) then (CONCAT((CURDATE() - INTERVAL 1 DAY), " 22:00:00")) END)))))
        * (ROUND((g.basic_pay / 26) / 8,2)), 2)
        
        FROM hrim_time_log htls WHERE htls.user_id = a.user_id AND htls.date >= "' . $begin . '" AND htls.date <= "' . $end . '") as ndval
        ,

        (SELECT 

        round(sum((g.basic_pay / 26) * otr.rate_percentage), 2) 
                    FROM hrim_holiday hl 
                    LEFT JOIN hrim_time_log htl on htl.date = hl.date 
                    LEFT JOIN user_details ud ON ud.user_id = htl.user_id
                    LEFT JOIN hrim_date_category_reference otr ON otr.id = hl.type_id
                 where htl.date >= "' . $begin . '" and htl.date <= "' . $end . '"
                 AND (hl.branch_id = a.branch_id OR hl.branch_id IS NULL) and htl.user_id = a.user_id ) as holidayp,

                 (SELECT 

                 round(sum((g.basic_pay / 26) * otr.rate_percentage), 2) 
                             FROM hrim_holiday hls 
                             LEFT JOIN hrim_time_log htl on htl.date = hls.date 
                             LEFT JOIN user_details ud ON ud.user_id = htl.user_id
                             LEFT JOIN hrim_date_category_reference otr ON otr.id = hls.type_id
                          where htl.date >= "' . $begin . '" and htl.date <= "' . $end . '"
                          AND (hls.branch_id = a.branch_id OR hls.branch_id IS NULL) and htl.user_id = a.user_id and hls.type_id = 1) as holidaypregular,

                 (SELECT 
                 round(sum((g.basic_pay / 26) * otr.rate_percentage), 2) 
                 FROM hrim_holiday hlss 
                 LEFT JOIN hrim_time_log htl on htl.date = hlss.date 
                 LEFT JOIN user_details ud ON ud.user_id = htl.user_id
                 LEFT JOIN hrim_date_category_reference otr ON otr.id = hlss.type_id
              where htl.date >= "' . $begin . '" and htl.date <= "' . $end . '"
              AND (hlss.branch_id = a.branch_id OR hlss.branch_id IS NULL) and htl.user_id = a.user_id and hlss.type_id = 2) as holidaypspecial,

        (SELECT SUM(dl.amount)
                 FROM hrim_dlb dl WHERE 
                 dl.month = "' . $month . '" 
                 AND dl.payout_cutoff = "' . $cutoff . '" 
                 AND dl.year = "' . $year . '"
                 AND dl.user_id = a.user_id) AS dlbpay,

        (SELECT SUM(aad.amount)
                 FROM hrim_admin_adjustment aad WHERE 
                 aad.month = "' . $month . '" 
                 AND aad.payout_cutoff = "' . $cutoff . '" 
                 AND aad.year = "' . $year . '"
                 AND aad.user_id = a.user_id 
                 AND aad.first_status = 3) AS aadpay,

                 ' . $cutoff . ' as cutoffidentify,


                 (SELECT MIN(a.cutoff_date) FROM hrim_cutoff_management a 
                 WHERE a.payout_year = "' . $year . '" 
                 AND a.payout_month = "' . $month . '" 
                 AND a.payout_cutoff = "' . $cutoff . '" AND a.category_id = 3)
                 AS paydatefrom,

                 (SELECT MAX(a.cutoff_date) FROM hrim_cutoff_management a 
                 WHERE a.payout_year = "' . $year . '" 
                 AND a.payout_month = "' . $month . '" 
                 AND a.payout_cutoff = "' . $cutoff . '" AND a.category_id = 3)
                 AS paydateto,

                 h.description as division,
                 "' . ($monthday) . '" as payoutdatemonth,
                 ' . $cutoff . ' as period,
                 ' . $month . ' as reqmonth,
                 ' . $year . ' as reqyear,
                 f.display as branchname,
                 a.employee_code as employee_code,

                 (SELECT COUNT(twd.date)
                    FROM hrim_time_log twd WHERE twd.user_id = a.user_id AND 
                    twd.date >= "' . $begin . '" AND twd.date <= "' . $end . '") AS workingdays
        
        FROM user_details a
        LEFT JOIN users b ON b.id = a.user_id
        LEFT JOIN hrim_employment_status c ON c.id = a.employment_status_id
        LEFT JOIN hrim_job_levels d ON d.id = a.job_level_id
        LEFT JOIN hrim_positions e ON e.id = a.position_id
        LEFT JOIN branch_reference f ON f.id = a.branch_id
        LEFT JOIN hrim_earnings g ON g.user_id = a.user_id
        LEFT JOIN division h ON h.id = a.division_id

        WHERE a.user_id = "' . $id . '" 
        and a.employment_category_id is not null
        order by a.last_name ASC
        
        ');


        $loans = DB::select('
        SELECT c.name AS deductionname, b.amount as loandeducamount FROM hrim_loan a 
        LEFT JOIN hrim_loan_details b ON b.loan_id = a.id
        LEFT JOIN hrim_loan_type c ON c.id = a.`type`
        WHERE b.year = ' . $year . ' 
        AND b.month = ' . $month . ' 
        AND b.cutoff = ' . $cutoff . '
        AND a.user_id = "' . $id . '" 
        AND b.`status` = 0 AND a.final_status = 3
        AND a.`type` IN (1,5,6,7,8,9)
        ');
        }


        return view('view/hrim/compensation-and-benefits/payslips/print-my-payslip', compact('payrolls','loans'));
    }
}
