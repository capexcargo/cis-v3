<?php

namespace App\Http\Controllers\Hrim\CompensationAndBenefits;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PayrollController extends Controller
{
    public function index()
    {
        return view('view/hrim/compensation-and-benefits/payroll/index');
    }
}
