<?php

namespace App\Http\Controllers\Hrim\EmployeeManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DisciplinaryHistoryController extends Controller
{
    public function index()
    {
        return view('view/hrim/employee-management/disciplinary-history/index');
    }
}
