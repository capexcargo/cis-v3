<?php

namespace App\Http\Controllers\Hrim;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrim\TimeInAndOutInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class TimeInAndOutController extends Controller
{
    use ResponseTrait;

    public $timeandout_interface;

    public function __construct(TimeInAndOutInterface $timeandout_interface)
    {
        $this->timeandout_interface = $timeandout_interface;
    }

    public function timeIn(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->timeandout_interface->timeIn($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function timeOut(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->timeandout_interface->timeOut($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
    public function index()
    {
        return view('view/hrim/time-in-and-out/index');
    }
}
