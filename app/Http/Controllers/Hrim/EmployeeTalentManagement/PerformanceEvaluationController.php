<?php

namespace App\Http\Controllers\Hrim\EmployeeTalentManagement;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrim\TalentManagement\PerformanceEvaluationInterface;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class PerformanceEvaluationController extends Controller
{
    use ResponseTrait;
    public $performance_evaluation_interface;

    public function __construct(PerformanceEvaluationInterface $performance_evaluation_interface)
    {
        $this->performance_evaluation_interface = $performance_evaluation_interface;
    }

    public function show(Request $request)
    {
        $request_data = [
            'year' => $request->year,
            'quarter' => $request->quarter,
        ];

        $response = $this->performance_evaluation_interface->eIndex($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function create(Request $request)
    {
        $request_data = [
            // 'core_value_assessments' => $request->core_values,
            // 'leadership_competencies' => $request->leadership_comps,
            // 'employee_evaluations' => $request->employee_evals,
            'core_values' => $request->core_values,
            'leadership_comps' => $request->leadership_comps,
            'employee_evals' => $request->employee_evals,
            'employee_remarks' => $request->employee_remarks,
        ];

        $response = $this->performance_evaluation_interface->eSave($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
    public function index()
    {
        return view('view/hrim/employee-talent-management/performance-evaluation/index');
    }

    public function view($id, $quarter, $year)
    {
        return view('view/hrim/employee-talent-management/performance-evaluation/view', compact('id', 'quarter', 'year'));
    }
}
