<?php

namespace App\Http\Controllers\Crm\ServiceRequest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SrSubCategoryController extends Controller
{
    public function index()
    {
        return view('view/crm/service-request/sr-related-mgmt/sr-sub-category/index');
    }
}
