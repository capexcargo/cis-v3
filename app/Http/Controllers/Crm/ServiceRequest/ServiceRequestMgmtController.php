<?php

namespace App\Http\Controllers\Crm\ServiceRequest;

use App\Http\Controllers\Controller;

class ServiceRequestMgmtController extends Controller
{
    public function index()
    {
        return view('view/crm/service-request/service-request-mgmt/index');
    }

    public function edit($id)
    {
        return view('view/crm/service-request/service-request-mgmt/edit', compact('id'));
    }
}
