<?php

namespace App\Http\Controllers\Crm\ServiceRequest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SrRelatedMgmtController extends Controller
{
    public function srtype()
    {
        return view('view/crm/service-request/sr-related-mgmt/index');
    }

    public function srsubcat()
    {
        return view('view/crm/service-request/sr-related-mgmt/sr-sub-category/index');
    }

    public function srres()
    {
        return view('view/crm/service-request/sr-related-mgmt/response/index');
    }

    public function srreso()
    {
        return view('view/crm/service-request/sr-related-mgmt/resolution/index');
    }

    public function urge()
    {
        return view('view/crm/service-request/sr-related-mgmt/urgency/index');
    }

    public function hier()
    {
        return view('view/crm/service-request/sr-related-mgmt/hierarchy/index');
    }

    public function chan()
    {
        return view('view/crm/service-request/sr-related-mgmt/channel/index');
    }

    public function serv()
    {
        return view('view/crm/service-request/sr-related-mgmt/service/index');
    }



    //// KNOWLEDGE ////

    public function know()
    {
        return view('view/crm/service-request/knowledge/index');
    }

    public function sale()
    {
        return view('view/crm/service-request/knowledge/sales-coach/index');
    }

}
