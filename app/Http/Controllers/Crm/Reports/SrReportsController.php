<?php

namespace App\Http\Controllers\Crm\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SrReportsController extends Controller
{
    public function index()
    {
        return view('view/crm/reports/sr-reports/index');
    }
}
