<?php

namespace App\Http\Controllers\Crm\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SalesModuleReportsController extends Controller
{
    public function index()
    {
        return view('view/crm/reports/sales-module-reports/index');
    }
}
