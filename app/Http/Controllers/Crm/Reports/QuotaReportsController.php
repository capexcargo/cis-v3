<?php

namespace App\Http\Controllers\Crm\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QuotaReportsController extends Controller
{
    public function index()
    {
        return view('view/crm/reports/quota-reports/index');
    }
}
