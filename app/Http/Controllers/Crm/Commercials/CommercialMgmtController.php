<?php

namespace App\Http\Controllers\Crm\Commercials;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommercialMgmtController extends Controller
{
    public function AirFreight()
    {
        return view('view/crm/commercials/commercial-mgmt/index');
    }

    public function Pouch()
    {
        return view('view/crm/commercials/commercial-mgmt/pouch/index');
    }

    public function Loa()
    {
        return view('view/crm/commercials/commercial-mgmt/loa-mgmt/index');
    }

    public function Box()
    {
        return view('view/crm/commercials/commercial-mgmt/box/index');
    }

    public function Ancillary()
    {
        return view('view/crm/commercials/ancillary/index');
    }

    public function AncillaryDisplay()
    {
        return view('view/crm/commercials/ancillary-display/index');
    }

    public function Crating()
    {
        return view('view/crm/commercials/commercial-mgmt/crating/index');
    }

    public function Warehousing()
    {
        return view('view/crm/commercials/commercial-mgmt/warehousing/index');
    }

    public function LandFreight()
    {
        return view('view/crm/commercials/commercial-mgmt/land-freight/index');
    }
    public function SeaFreight()
    {
        return view('view/crm/commercials/commercial-mgmt/sea-freight/index');
    }
    public function AirFreightPremium()
    {
        return view('view/crm/commercials/commercial-mgmt/air-freight-premium/index');
    }
}
