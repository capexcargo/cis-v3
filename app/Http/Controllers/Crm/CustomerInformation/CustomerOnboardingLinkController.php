<?php

namespace App\Http\Controllers\Crm\CustomerInformation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Livewire\Livewire;

class CustomerOnboardingLinkController extends Controller
{
    public function create()
    {
        $pageTitle = null;

        Livewire::listen('pageTitleSet', function ($title) use (&$pageTitle) {
            $pageTitle = $title;
        });

        return view('view/crm/customer-information/customer-onboarding-link/create', compact('pageTitle'));
    }
}
