<?php

namespace App\Http\Controllers\Crm\CustomerInformation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomerDataMgmtController extends Controller
{
    public function index()
    {
        return view('view/crm/customer-information/customer-data-mgmt/index');
    }

    public function search_result($id)
    {
        return view('view/crm/customer-information/customer-data-mgmt/search-result', compact('id'));
    }
}
