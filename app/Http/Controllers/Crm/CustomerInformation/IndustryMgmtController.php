<?php

namespace App\Http\Controllers\Crm\CustomerInformation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndustryMgmtController extends Controller
{
    public function index()
    {
        return view('view/crm/customer-information/industry-mgmt/index');
    }
}
