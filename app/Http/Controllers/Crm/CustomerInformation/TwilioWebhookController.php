<?php

namespace App\Http\Controllers\Crm\CustomerInformation;

use App\Http\Controllers\Controller;
use App\Interfaces\Crm\CustomerInformation\CustomerOnboardingInterface;
use App\Models\Crm\CustomerInformation\CrmCusInformationInitiatedCall;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Twilio\TwiML\VoiceResponse;

class TwilioWebhookController extends Controller
{
    use ResponseTrait;
    public $customer_onboarding_interface;

    public function __construct(CustomerOnboardingInterface $customer_onboarding_interface)
    {
        $this->customer_onboarding_interface = $customer_onboarding_interface;
    }

    public function handle(Request $request)
    {
        try {

            $request_data = [
                'callSid' => $request->input('CallSid'),
                'callStatus' => $request->input('CallStatus'),
            ];

            $response = $this->customer_onboarding_interface->updateCallStatus($request_data);
            return $this->responseAPI($response['code'], $response['message'], $response['result']);

            // return response('Webhook received and processed successfully', 200);
        } catch (\Exception $e) {
            return $this->responseAPI(500, 'Error processing webhook: ', $e->getMessage());
        }
    }

    // public function handlereq(Request $request)
    // {
    //     try {

    //         $request_data = [
    //             'number' => "639190760715"
    //         ];

    //         $response = $this->customer_onboarding_interface->missedcall($request_data);
    //         return $this->responseAPI($response['code'], $response['message'], $response['result']);

    //         // return response('Webhook received and processed successfully', 200);
    //     } catch (\Exception $e) {
    //         return $this->responseAPI(500, 'Error processing webhook: ', $e->getMessage());
    //     }
    // }

    // public function handlereqnotif(Request $request)
    // {
    //     try {

    //         $request_data = [
    //             'event' => $request->input(),
    //         ];

    //         $response = $this->customer_onboarding_interface->missedcallnotif($request_data);
    //         // dd($response);

    //         return $this->responseAPI($response['code'], $response['message'], $response['result']);

    //         // return response('Webhook received and processed successfully', 200);
    //     } catch (\Exception $e) {
    //         return $this->responseAPI(500, 'Error processing webhook: ', $e->getMessage());
    //     }
    // }

    

}
