<?php

namespace App\Http\Controllers\Crm\Sales;

use App\Http\Controllers\Controller;
use App\Interfaces\Crm\Sales\QuotationInterface;
use App\Models\Crm\CrmCustomerInformation;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class QuotationController extends Controller
{
    use ResponseTrait;

    public $quotation_interface;

    public $quotation;
    public $reference_no;

    public $date;
    public $name;
    public $position;
    public $address;
    public $mobile_number;

    public $shipment_type;
    public $origin;
    public $origin_code;
    public $exact_pickup_address;
    public $destination;
    public $destination_code;
    public $exact_dropoff_address;
    public $description;
    public $declared_value;
    public $transport_mode;
    public $service_mode;
    public $quantity;
    public $weight;
    public $dimension;
    public $paymode;


    public $weight_charge;
    public $awb_fee;
    public $valuation;
    public $cod_charge;
    public $insurance;
    public $handling_fee;

    public $evat_chkbox;
    public $evat;

    public $other_fees;
    public $opa_fee;
    public $oda_fee;
    public $equipment_rental;
    public $crating_fee;
    public $lashing_fee;
    public $manpower_fee;
    public $dangerous_goods_fee;
    public $trucking_fee;
    public $perishable_fee;
    public $packaging_fee;
    public $discount_amount;
    public $discount_percentage;
    public $subtotal;
    public $grand_total;

    public function index()
    {
        return view('view/crm/sales/quotation/index');
    }

    public function showQuotation($id, $from_sr)
    {
        return view('view/crm/sales/quotation/index-quotation', compact('id', 'from_sr'));
    }

    public function printGeneratedQuotation(QuotationInterface $quotation_interface, $id, $from_sr)
    {
        $response = $quotation_interface->show($id, $from_sr);

        abort_if($response['code'] != 200, $response['code'], $response['message']);

        $this->quotation = $response['result'];

        $this->reference_no = $this->quotation->reference_no;

        // if ($from_sr == "sr-create-quotation") {
        //     $this->date = date('M d, Y');
        // } else {
        $this->date = date('M d, Y', strtotime($this->quotation->created_at));
        $this->declared_value = number_format($this->quotation->shipmentDetails->declared_value, 2) ?? null;


        $customer_data = CrmCustomerInformation::with('contactPersons', 'mobileNumbers', 'emails', 'addresses')->find($this->quotation->account_id);

        if ($customer_data) {

            // $this->company_business_name = $customer_data->account_type ?? null == 2 ? $customer_data->company_name : '';
            foreach ($customer_data->contactPersons as $i => $contact_person) {
                if ($contact_person->is_primary == 1) {
                    $this->name = $contact_person->first_name . " " . $contact_person->middle_name . " " . $contact_person->last_name;
                    $this->position = $contact_person->position;
                }
            }

            foreach ($customer_data->mobileNumbers as $i => $mobile_number) {
                if ($mobile_number->is_primary == 1) {
                    $this->mobile_number = $mobile_number->mobile;
                }
            }

            foreach ($customer_data->addresses as $i => $address) {
                if ($address->is_primary == 1) {
                    $address_line_1 = $address->address_line_1;
                    $address_line_2 = $address->address_line_2;

                    $postal = $address->postal_id;
                    $state_province = '';
                    $city_municipal = '';
                    $barangay = '';

                    $this->address =  $address_line_1 ??  $address_line_2 . " " . $barangay . ", " . $city_municipal . ", " . $state_province . " " . $postal;
                }
            }
        }

        $this->shipment_type = $this->quotation->shipmentType->name ?? null;
        $this->origin = $this->quotation->shipmentDetails->origin->display ?? null;
        $this->origin_code = $this->quotation->shipmentDetails->origin->code ?? null;
        $this->exact_pickup_address = $this->quotation->shipmentDetails->origin_address ?? null;
        $this->destination = $this->quotation->shipmentDetails->destination->display ?? null;
        $this->destination_code = $this->quotation->shipmentDetails->destination->code ?? null;
        $this->exact_dropoff_address = $this->quotation->shipmentDetails->destination_address ?? null;
        $this->description = null;
        $this->transport_mode = $this->quotation->shipmentDetails->transportMode->name ?? null;
        $this->service_mode = $this->quotation->shipmentDetails->serviceMode->name ?? null;
        $this->paymode = $this->quotation->shipmentDetails->payMode->name ?? null;
        $this->quantity = $this->quotation->shipmentDimensions['quantity'] . " " . ($this->quotation->shipmentDimensions['type_of_packaging_id'] == 1 ? "Box" : ($this->quotation->shipmentDimensions['type_of_packaging_id'] == 2 ? "Plastic" : "Crate"));
        $this->weight = $this->quotation->shipmentDimensions['weight'];
        $this->dimension = $this->quotation->shipmentDimensions['length'] . "x" . $this->quotation->shipmentDimensions['width'] . "x" . $this->quotation->shipmentDimensions['height'];

        // Breakdown of charges
        $this->weight_charge = number_format($this->quotation->shipmentCharges['weight_charge'], 2);
        $this->awb_fee = number_format($this->quotation->shipmentCharges['awb_fee'], 2);
        $this->valuation = number_format($this->quotation->shipmentCharges['valuation'], 2);
        $this->cod_charge = number_format($this->quotation->shipmentCharges['cod_charge'], 2);
        $this->insurance = number_format($this->quotation->shipmentCharges['insurance'], 2);
        $this->handling_fee = number_format($this->quotation->shipmentCharges['handling_fee'], 2);

        $this->evat_chkbox = $this->quotation->shipmentCharges['is_evat'];
        $this->evat =  number_format($this->quotation->shipmentCharges['evat'], 2);

        $this->other_fees = $this->quotation->shipmentCharges['is_other_fee'];
        if ($this->other_fees == 1) {
            $this->opa_fee = number_format($this->quotation->shipmentCharges['opa_fee'], 2);
            $this->oda_fee = number_format($this->quotation->shipmentCharges['oda_fee'], 2);
            $this->equipment_rental = number_format($this->quotation->shipmentCharges['equipment_rental'], 2);
            $this->crating_fee = number_format($this->quotation->shipmentCharges['crating_fee'], 2);
            $this->lashing_fee = number_format($this->quotation->shipmentCharges['lashing'], 2);
            $this->manpower_fee = number_format($this->quotation->shipmentCharges['manpower'], 2);
            $this->dangerous_goods_fee = number_format($this->quotation->shipmentCharges['dangerous_goods_fee'], 2);
            $this->trucking_fee = number_format($this->quotation->shipmentCharges['trucking'], 2);
            $this->perishable_fee = number_format($this->quotation->shipmentCharges['perishable_fee'], 2);
            $this->packaging_fee = number_format($this->quotation->shipmentCharges['packaging_fee'], 2);
        }

        $this->discount_amount =  number_format($this->quotation->shipmentCharges['discount_amount'], 2);
        $this->discount_percentage =  number_format($this->quotation->shipmentCharges['discount_percentage'], 2);
        $this->subtotal =  number_format($this->quotation->shipmentCharges['subtotal'], 2);
        $this->grand_total = "P" . number_format($this->quotation->shipmentCharges['grandtotal'], 2);
        // }

        return view('view/crm/sales/quotation/print-generated-quotation', [
            'reference_no' => $this->reference_no,
            'date' => $this->date,
            'shipment_type' => $this->shipment_type,
            'name' => $this->name,
            'position' => $this->position,
            'mobile_number' => $this->mobile_number,
            'address' => $this->address,

            'origin' => $this->origin,
            'origin_code' => $this->origin_code,
            'exact_pickup_address' => $this->exact_pickup_address,
            'destination' => $this->destination,
            'destination_code' => $this->destination_code,
            'exact_dropoff_address' => $this->exact_dropoff_address,
            'description' => $this->description,
            'declared_value' => $this->declared_value,
            'transport_mode' => $this->transport_mode,
            'service_mode' => $this->service_mode,
            'paymode' => $this->paymode,
            'quantity' => $this->quantity,
            'weight' => $this->weight,
            'dimension' => $this->dimension,

            'weight_charge' => $this->weight_charge,
            'awb_fee' => $this->awb_fee,
            'valuation' => $this->valuation,
            'cod_charge' => $this->cod_charge,
            'insurance' => $this->insurance,
            'handling_fee' => $this->handling_fee,
            'evat' => $this->evat,
            'opa_fee' => $this->opa_fee,
            'oda_fee' => $this->oda_fee,
            'equipment_rental' => $this->equipment_rental,
            'crating_fee' => $this->crating_fee,
            'lashing_fee' => $this->lashing_fee,
            'manpower_fee' => $this->manpower_fee,
            'dangerous_goods_fee' => $this->dangerous_goods_fee,
            'trucking_fee' => $this->trucking_fee,
            'perishable_fee' => $this->perishable_fee,
            'packaging_fee' => $this->packaging_fee,

            'discount_amount' => $this->discount_amount,
            'discount_percentage' => $this->discount_percentage,
            'subtotal' => $this->subtotal,
            'grand_total' => $this->grand_total,
        ]);
    }
}
