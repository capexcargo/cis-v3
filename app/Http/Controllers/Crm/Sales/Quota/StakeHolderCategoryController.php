<?php


namespace App\Http\Controllers\Crm\Sales\Quota;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StakeholderCategoryController extends Controller
{
    public function index()
    {
        return view('view/crm/sales/quota/stakeholder-category/index');
    }
}
