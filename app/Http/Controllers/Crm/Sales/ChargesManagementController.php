<?php

namespace App\Http\Controllers\Crm\Sales;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ChargesManagementController extends Controller
{
    public function index()
    {
        return view('view/crm/sales/charges-management/index');
    }
}
