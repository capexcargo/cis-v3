<?php

namespace App\Http\Controllers\Crm\Sales;

use App\Http\Controllers\Controller;
use App\Interfaces\Crm\Sales\BookingManagement\BookingMgmt\BookingMgmtInterface;
use App\Models\Crm\CrmCustomerInformation;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    

    public function Leads()
    {
        return view('view/crm/sales/leads/index');
    }

    public function EditLeads($id)
    {
        return view('view/crm/sales/leads/edit', compact('id'));
    }

    public function Qualification()
    {
        return view('view/crm/sales/leads/qualification-mgmt/index');
    }

    public function Opportunities()
    {
        return view('view/crm/sales/opportunities/index');
    }

    public function EditOpportunities($id)
    {
        return view('view/crm/sales/opportunities/edit', compact('id'));
    }


    public function SalesStage()
    {
        return view('view/crm/sales/opportunities/sales-stage/index');
    }

    public function OppStatus()
    {
        return view('view/crm/sales/opportunities/opportunity-status/index');
    }

    public function BookingDropList()
    {
        return view('view/crm/sales/booking-dropdown-list/index');
    }

    public function BookingTimeSlot()
    {
        return view('view/crm/sales/booking-dropdown-list/timeslot/index');
    }

    public function BookingMgmt()
    {
        return view('view/crm/sales/booking-mgmt/index');
    }
    public function SalesCampaign()
    {
        return view('view/crm/sales/sales-campaign/index');
    }
    public function SalesCampaignSummary()
    {
        return view('view/crm/sales/sales-campaign/summary/index');
    }
    public function AudienceSegmentaion()
    {
        return view('view/crm/sales/audience-segmentation/index');
    }
    public function BookingHistory()
    {
        return view('view/crm/sales/booking-mgmt/booking-history/index2');
    }

    use ResponseTrait;

    public $booking_mgmt_interface;
    public function __construct(BookingMgmtInterface $booking_mgmt_interface)
    {
        $this->booking_mgmt_interface = $booking_mgmt_interface;
    }
    public function createbooking(Request $request)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->booking_mgmt_interface->create($request_data);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function showbooking($id)
    {
        $response = $this->booking_mgmt_interface->show($id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updatebooking(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->booking_mgmt_interface->update($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }
    

    public function ShipperCreateAddress(Request $request, $customer_no_2)
    {
        $customerinfos = CrmCustomerInformation::with('mobileNumbers')->where('account_no', $customer_no_2)->first();
        $numbers = 0;
        // dd($customerinfos->mobileNumbers);

        if ($customerinfos) {
            foreach ($customerinfos->mobileNumbers as $i => $mobileNumber) {
                if ($mobileNumber->is_primary == 1) {
                    $numbers = $mobileNumber->mobile;
                }
            }
        }

        $response['result'] =
                [
                    'numbers' => $numbers,
                    'account_no' => $customerinfos->account_no,
                    'fullname' => $customerinfos->fullname,
                    'company_name' => $customerinfos->company_name,
                ];
        // $this->mobile_nos = $this->numbers;
        // // dd($this->mobile_nos);
        // $this->custno = $customerinfos->account_no;
        // $this->custshpr = $customerinfos->fullname;
        // $this->custcpny = $customerinfos->company_name;

        // $request_data = array_merge(
        //     $request->all(),
        // );

        // $response = $this->booking_mgmt_interface->create($request_data);
        return $this->responseAPI(200, 'ok', $response['result']);
    }

    public function updatebconf(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->booking_mgmt_interface->bookForConfirm($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updatebconfirmed(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->booking_mgmt_interface->bookConfirmed($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updatebong(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->booking_mgmt_interface->bookOngoing($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

    public function updatebcomp(Request $request, $id)
    {
        $request_data = array_merge(
            $request->all(),
        );

        $response = $this->booking_mgmt_interface->bookCompleted($request_data, $id);
        return $this->responseAPI($response['code'], $response['message'], $response['result']);
    }

}
