<?php

namespace App\Http\Controllers\Crm\Activities;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ActivitiesController extends Controller
{
    public function Activities()
    {
        return view('view/crm/activities/index');
    }

    public function schedameeting()
    {
        return view('view/crm/activities/schedule-meeting/index');
    }

}
