<?php

namespace App\Http\Controllers\Crm\Contracts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContractsController extends Controller
{
    // public function Contracts()
    // {
    //     return view('view/crm/contracts/index');
    // }

    public function ReqMgmt()
    {
        return view('view/crm/contracts/requirements-mgmt/index');
    }

    public function AccApp()
    {
        return view('view/crm/contracts/accounts-application/index');
    }
}
