<?php

namespace App\Mail\CustomerInformation;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\View;

class HTMLmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $mailData;

    public function __construct($mailData)
    {
        $this->mailData = $mailData;
    }

    public function build()
    {
        $br = '<br><br><br>';
        $emailView = 'emails.email-onboarding-customer';
        $welcome_name = $this->mailData['welcome_name'];

        $bodyContent = View::make($emailView, compact('br', 'welcome_name'))->render();

        
        return $this->subject($this->mailData['subject'])
            ->view('emails.email-onboarding-customer')
            ->with([
                'bodyContent' => $bodyContent,
                'welcome_name' => $welcome_name,
                'br' => $br,
            ])
            ->from($this->mailData['from'], 'Cargo Padala Express'); // Must change the "Cargo Padala Express" as name of email sender
    }
}
