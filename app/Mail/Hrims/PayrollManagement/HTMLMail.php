<?php

namespace App\Mail\Hrims\PayrollManagement;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use PDF;

class HTMLMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $mailData;

    public function __construct($mailData)
    {
        $this->mailData = $mailData;
    }

    public function build()
    {
        $br = '<br><br><br>';
        $emailView = 'emails.email-13th-month-pay';
        $emailViewPDF = 'emails.email-13th-month-pay-pdf';
        $month_13th = $this->mailData['month_13th_pays'];
        $year = $this->mailData['year'];

        foreach ($month_13th as $pay) {
            $filename = $pay->first_name . "-" .  $pay->last_name . "-13th-month-payslip.pdf";
            $body_header = $pay->first_name . " " .  $pay->last_name . "'s 13th Month Payslip";
        }

        // Render the email content as HTML
        $bodyContent = view($emailView, compact('br', 'month_13th', 'year', 'body_header'))->render();

        // Create a PDF from the HTML content
        $pdf = PDF::loadView($emailViewPDF, compact('br', 'month_13th', 'year'));


        return $this->subject('CaPEx - 13th Month Pay')
            ->view('emails.email-13th-month-pay')
            ->with([
                'bodyContent' => $bodyContent,
                'body_header' => $body_header,
                'month_13th' => $month_13th,
                'year' => $year,
                'br' => $br,
            ])
            ->attachData($pdf->output(), $filename, [
                'mime' => 'application/pdf',
            ]);
    }
}
