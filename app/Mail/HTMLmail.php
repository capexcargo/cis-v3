<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class HTMLmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $mailData;

    public function __construct($mailData)
    {
        $this->mailData = $mailData;
    }

    public function build()
    {
        $br = '<br><br><br>';
        $emailView = 'emails.email-signature';
        $body = $this->mailData['body'];

        $bodyContent = View::make($emailView, compact('br', 'body'))->render();


        return $this->subject($this->mailData['subject'])
            ->view('emails.email-signature')
            ->with([
                'bodyContent' => $bodyContent,
                'body' => $body,
                'br' => $br,
            ])
            ->from($this->mailData['from'], Auth::user()->name);
    }
}
