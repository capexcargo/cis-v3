<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;


trait InitializeFirestoreTrait {

    /**
     * @param Request $request
     * @return $this|false|string
     */
    private function initializeFirestore()
    {
        $factory = (new Factory)->withServiceAccount(base_path(env('FIREBASE_CREDENTIALS')));
        $firestore = $factory->createFirestore();
        return $database = $firestore->database();
    }

}