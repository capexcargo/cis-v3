<?php

namespace App\Traits\Hrim\EmployeeAttendance;

use App\Models\Hrim\DateCategoryReference;
use App\Models\Hrim\HolidayManagement;
use App\Models\MonthReference;
use App\Repositories\Hrim\EmployeeAttendance\OvertimeRecordsRepository;
use Illuminate\Support\Facades\Auth;

trait OvertimeRecordsTrait
{
    public $month;
    public $cut_off;
    public $year;

    public $time_log;
    public $date_time_in;
    public $date_time_out;
    public $time_in;
    public $time_out;
    public $work_schedule;
    public $rendered_ot_hours;

    public $type_of_ot;
    public $is_have_ot_break = true;
    public $actual_date_from;
    public $actual_date_to;
    public $from;
    public $to;
    public $overtime_request;
    public $reason;

    public $month_references = [];
    public $overtimed_date_references = [];
    public $date_category_references = [];

    public $overtime_record;

    public function load()
    {
        $this->loadMonthReference();
        $this->loadOvertimeTypeReference();
        $this->getOvertimed();
    }

    public function loadMonthReference()
    {
        $this->month_references = MonthReference::get();
    }

    public function loadOvertimeTypeReference()
    {
        $this->date_category_references = DateCategoryReference::get();
    }

    protected function updatedMonth()
    {
        $this->getOvertimed();
    }

    protected function updatedCutOff()
    {
        $this->getOvertimed();
    }

    protected function updatedYear()
    {
        $this->getOvertimed();
    }

    protected function updatedDateTimeIn()
    {
        $overtime_records_repository = new OvertimeRecordsRepository();
        $request = [
            'user_id' => Auth::user()->id,
            'date' => $this->date_time_in,
        ];
        $response = $overtime_records_repository->showDate($request);
        if ($response['code'] != 200) {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }

        $this->time_log = $response['result'];
        $this->date_time_out = $this->time_log->time_out_date;
        $this->time_in = date('H:i', strtotime($this->time_log->time_in));
        $this->time_out = date('H:i', strtotime($this->time_log->time_out));
        $this->work_schedule = $this->time_log->workSchedule->name;
        $this->rendered_ot_hours = $this->time_log->computed_ot;

        if (!$this->overtime_record) {
            $holiday = HolidayManagement::firstWhere('date', $this->time_log->date);

            $this->type_of_ot = 1;

            if ($holiday) {
                $this->type_of_ot = $holiday->date_category_id;
            }

            $this->actual_date_from = $this->time_log->date;
            $this->actual_date_to = $this->time_log->time_out_date;
            $this->from = date('H:i', strtotime($this->time_log->workSchedule->time_to));
            $this->to = date('H:i', strtotime($this->time_log->time_out));
            $this->overtime_request = $this->time_log->computed_ot;
        }
    }

    protected function updatedActualDateTo()
    {
        $this->getComputedOvertime();
    }

    protected function updatedTo()
    {
        $this->getComputedOvertime();
    }

    public function getComputedOvertime()
    {
        $overtime_records_repository = new OvertimeRecordsRepository();
        $response = $overtime_records_repository->getComputedOvertime($this->getRequest());
        if ($response['code'] != 200) {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }

        $this->overtime_request = $response['result'];
    }

    public function getOvertimed()
    {
        $overtime_records_repository = new OvertimeRecordsRepository();
        $response = $overtime_records_repository->getOvertimed($this->getRequest());
        if ($response['code'] != 200) {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }
// dd()
        $this->overtimed_date_references = $response['result'];
        $this->resetForm();
    }

    public function getRequest()
    {
        return [
            'user_id' => Auth::user()->id,

            'month' => $this->month,
            'cut_off' => $this->cut_off,
            'year' => $this->year,

            'time_log' => $this->time_log,
            'date_time_in' => $this->date_time_in,
            'date_time_out' => $this->date_time_out,
            'time_in' => $this->time_in,
            'time_out' => $this->time_out,
            'work_schedule' => $this->work_schedule,
            'rendered_ot_hours' => $this->rendered_ot_hours,

            'type_of_ot' => $this->type_of_ot,
            'is_have_ot_break' => $this->is_have_ot_break,
            'actual_date_from' => $this->actual_date_from,
            'actual_date_to' => $this->actual_date_to,
            'from' => $this->from,
            'to' => $this->to,
            'overtime_request' => $this->overtime_request,
            'reason' => $this->reason,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'time_log',
            'date_time_in',
            'date_time_out',
            'time_in',
            'time_out',
            'work_schedule',
            'rendered_ot_hours',

            'type_of_ot',
            'is_have_ot_break',
            'actual_date_from',
            'actual_date_to',
            'from',
            'to',
            'overtime_request',
            'reason',
        ]);
    }
}
