<?php

namespace App\Traits\Hrim\EmployeeAttendance;

use App\Interfaces\Hrim\EmployeeAttendance\LeaveRecordsInterface;
use App\Models\Hrim\LeaveDayTypeReference;
use App\Models\Hrim\LeaveTypeReference;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait LeaveRecordsTrait
{
    public $perview_summary_modal = false;

    public $type_of_leave_message;

    public $inclusive_date_from;
    public $inclusive_date_to;
    public $resume_of_work;
    public $type_of_leave;
    public $other;
    public $is_with_medical_certificate;
    public $apply_for;
    public $is_with_pay;
    public $reason;
    public $reliever;
    public $attachment = '';

    public $leave_type_references = [];
    public $leave_day_type_references = [];
    public $reliever_references = [];

    public $leave;
    
    public $today;

    public function load()
    {
        $this->loadLeaveTypeReference();
        $this->loadLeaveDayTypeReference();
        $this->loadRelieverReference();
        $this->today = date('Y-m-d');
    }

    public function loadLeaveTypeReference()
    {
        $this->leave_type_references = LeaveTypeReference::get();
    }

    public function loadLeaveDayTypeReference()
    {
        $this->leave_day_type_references = LeaveDayTypeReference::get();
    }

    public function loadRelieverReference()
    {
        $this->reliever_references = User::where('division_id', Auth::user()->division_id)->get();
    }

    protected function updatedTypeOfLeave()
    {
        $this->reset('type_of_leave_message');

        if ($this->type_of_leave != 1) {
            $this->reset('is_with_medical_certificate');
        }
        if ($this->type_of_leave == 3) {
            $this->type_of_leave_message = "Automatic deduct to VL credit if with pay";
        }
    }

    public function action(LeaveRecordsInterface $leave_records_interface, array $data, $action_type)
    {
        if ($action_type == 'perview') {
            $response = $leave_records_interface->validation($this->getRequest());
            if ($response['code'] == 200) {
                $this->resetErrorBag();
                $this->perview_summary_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'perview') {
            $this->perview_summary_modal = false;
        }
    }

    public function getRequest()
    {
        return [
            'inclusive_date_from' => $this->inclusive_date_from,
            'inclusive_date_to' => $this->inclusive_date_to,
            'resume_of_work' => $this->resume_of_work,
            'type_of_leave' => $this->type_of_leave,
            'other' => $this->other,
            'is_with_medical_certificate' => $this->is_with_medical_certificate,
            'apply_for' => $this->apply_for,
            'is_with_pay' => $this->is_with_pay,
            'reason' => $this->reason,
            'reliever' => $this->reliever,
            'attachment' => $this->attachment,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'inclusive_date_from',
            'inclusive_date_to',
            'resume_of_work',
            'type_of_leave',
            'other',
            'is_with_medical_certificate',
            'apply_for',
            'is_with_pay',
            'reason',
            'reliever',
            'attachment'
        ]);
    }
}
