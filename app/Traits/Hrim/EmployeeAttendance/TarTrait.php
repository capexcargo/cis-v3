<?php

namespace App\Traits\Hrim\EmployeeAttendance;

use App\Models\Hrim\TarReasonReference;

trait TarTrait
{
    public $time_log_id;
    public $date_from;
    public $date_to;
    public $actual_time_in;
    public $actual_time_out;
    public $reason_of_adjustment;

    public $tar_reason_references = [];

    public $tar;

    public function load()
    {
        $this->loadTarReasionReference();
    }

    public function loadTarReasionReference()
    {
        $this->tar_reason_references = TarReasonReference::get();
    }

    public function getRequest()
    {
        return [
            'time_log_id' => $this->time_log_id,
            'date_from' => $this->date_from,
            'date_to' => $this->date_to,
            'actual_time_in' => $this->actual_time_in,
            'actual_time_out' => $this->actual_time_out,
            'reason_of_adjustment' => $this->reason_of_adjustment,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'time_log_id',
            'date_from',
            'date_to',
            'actual_time_in',
            'actual_time_out',
            'reason_of_adjustment',
        ]);
    }
}
