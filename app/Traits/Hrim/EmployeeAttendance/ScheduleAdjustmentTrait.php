<?php

namespace App\Traits\Hrim\EmployeeAttendance;

use App\Models\Hrim\WorkSchedule;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait ScheduleAdjustmentTrait
{
    public $employee_name;
    public $employee_id;

    public $current_schedule;
    public $current_days;
    public $current_time_from;
    public $current_time_to;

    public $new_schedule;
    public $new_days;
    public $new_time_from;
    public $new_time_to;
    public $new_work_mode;
    public $new_inclusive_date_start_date;
    public $new_inclusive_date_end_date;
    public $set_an_end_date = false;
    public $reason;

    public $employee_references = [];
    public $work_schedule_references = [];

    public $schedule_adjustment;

    public function load()
    {
        $this->loadEmployeeReference();
        $this->loadWorkScheduleReference();
    }

    public function loadEmployeeReference()
    {
        $this->employee_references = User::where([
            ['division_id', Auth::user()->division_id],
            ['name', 'like', '%' . $this->employee_name . '%'],
        ])
            ->orderBy('name', 'asc')
            ->take(10)
            ->get();
    }

    public function loadWorkScheduleReference()
    {
        $this->work_schedule_references = WorkSchedule::get();
    }

    protected function updatedEmployeeName()
    {
        $this->loadEmployeeReference();
    }

    protected function updatedNewSchedule()
    {
        $work_schedule = WorkSchedule::find($this->new_schedule);
        if ($work_schedule) {
            $days = '';
            if ($work_schedule->monday) {
                $days .= 'Monday ';
            }
            if ($work_schedule->tuesday) {
                $days .= 'Tuesday ';
            }
            if ($work_schedule->wednesday) {
                $days .= 'Wednesday ';
            }
            if ($work_schedule->thursday) {
                $days .= 'Thurday ';
            }
            if ($work_schedule->friday) {
                $days .= 'Friday ';
            }
            if ($work_schedule->saturday) {
                $days .= 'Saturday ';
            }
            if ($work_schedule->sunday) {
                $days .= 'Sunday ';
            }

            $this->new_days = $days;
            $this->new_time_from = $work_schedule->time_from;
            $this->new_time_to = $work_schedule->time_to;
            $this->new_work_mode = $work_schedule->workMode->display;
        }
    }

    public function getEmployeeInformation($id)
    {
        $employee_information = User::with(['userDetails' => function ($query) {
            $query->with('workSchedule');
        }])
            ->find($id);

        if ($employee_information) {
            $this->employee_name = $employee_information->name;
            $this->employee_id = $employee_information->id;
            $this->current_schedule = $employee_information->userDetails->schedule_id;

            $days = '';
            if ($employee_information->userDetails->workSchedule->monday) {
                $days .= 'Monday ';
            }
            if ($employee_information->userDetails->workSchedule->tuesday) {
                $days .= 'Tuesday ';
            }
            if ($employee_information->userDetails->workSchedule->wednesday) {
                $days .= 'Wednesday ';
            }
            if ($employee_information->userDetails->workSchedule->thursday) {
                $days .= 'Thurday ';
            }
            if ($employee_information->userDetails->workSchedule->friday) {
                $days .= 'Friday ';
            }
            if ($employee_information->userDetails->workSchedule->saturday) {
                $days .= 'Saturday ';
            }
            if ($employee_information->userDetails->workSchedule->sunday) {
                $days .= 'Sunday ';
            }

            $this->current_days = $days;
            $this->current_time_from = $employee_information->userDetails->workSchedule->time_from;
            $this->current_time_to = $employee_information->userDetails->workSchedule->time_to;
        }
    }

    public function getRequest()
    {
        return [
            'employee_name' => $this->employee_name,
            'employee_id' => $this->employee_id,

            'current_schedule' => $this->current_schedule,

            'new_schedule' => $this->new_schedule,
            'new_inclusive_date_start_date' => $this->new_inclusive_date_start_date,
            'new_inclusive_date_end_date' => $this->new_inclusive_date_end_date,
            'set_an_end_date' => $this->set_an_end_date,
            'reason' => $this->reason,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'employee_name',
            'employee_id',

            'current_schedule',
            'current_days',
            'current_time_from',
            'current_time_to',

            'new_schedule',
            'new_days',
            'new_time_from',
            'new_time_to',
            'new_work_mode',
            'new_inclusive_date_start_date',
            'new_inclusive_date_end_date',
            'set_an_end_date',
            'reason',
        ]);
    }
}
