<?php

namespace App\Traits\Hrim\EmployeeManagement;

use App\Interfaces\Hrim\EmployeeManagement\ViolationInterface;

trait ViolationTrait
{
    public $confirmation_modal = false;
    public $confirmation_message;
    public $action_type;

    public $display;

    public function action(ViolationInterface $violation_interface, $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'submit') {
            $response = $violation_interface->createValidation($this->getRequest());
            if ($response['code'] == 200) {
                $this->emitTo('hrim.employee-management.violation.index', 'close_modal', 'create');
                $this->emitTo('hrim.employee-management.violation.index', 'index');
                $this->sweetAlert('', $response['message']);

                $this->resetForm();
                $this->confirmation_modal = false;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function getRequest()
    {
        return [
            'display' => $this->display,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'display',
        ]);
    }
}
