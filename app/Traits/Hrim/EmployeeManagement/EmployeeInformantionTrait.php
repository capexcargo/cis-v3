<?php

namespace App\Traits\Hrim\EmployeeManagement;

use App\Interfaces\Hrim\EmployeeManagement\EmployeeInformationInterface;
use App\Models\BranchReference;
use App\Models\Division;
use App\Models\Hrim\Agencies;
use App\Models\Hrim\ApplicantTracking;
use App\Models\Hrim\Department;
use App\Models\Hrim\EALevelReference;
use App\Models\Hrim\ECDRelationshipReference;
use App\Models\Hrim\EmployeeRequisition;
use App\Models\Hrim\EmploymentCategory;
use App\Models\Hrim\EmploymentStatus;
use App\Models\Hrim\GenderReference;
use App\Models\Hrim\JobLevel;
use App\Models\Hrim\Position;
use App\Models\Hrim\WorkSchedule;
use App\Models\UserDetails;

trait EmployeeInformantionTrait
{
    public $action_type;
    public $index;

    public $confirmation_message;
    public $confirmation_modal = false;
    public $perview_summary_modal = false;

    public $current_tab = 1;
    public $photo = "";
    public $erf_reference;
    public $employee_id;
    public $branch;
    public $first_name;
    public $middle_name;
    public $last_name;
    public $position;
    public $job_level;
    public $department;
    public $division;
    public $employement_category;
    public $employement_status;
    public $gender;
    public $agency;
    public $date_of_birth;
    public $date_hired;
    public $current_address;
    public $permanent_address;
    public $personal_mobile_number;
    public $personal_telephone_number;
    public $personal_email_address;

    public $company_mobile_number;
    public $company_telephone_number;

    public $company_email_address;
    public $password;
    public $password_confirmation;

    public $work_schedule;
    public $inclusive_days;
    public $time_schedule;

    public $sss;
    public $pagibig_number;
    public $philhealth_number;
    public $tin_number;

    public $educational_level;
    public $educational_institution;
    public $educational_inclusive_year_from;
    public $educational_inclusive_year_to;

    public $employment_employer;
    public $employment_position;
    public $employment_inclusive_year_from;
    public $employment_inclusive_year_to;

    public $character_reference_name;
    public $character_reference_position;
    public $character_reference_company;
    public $character_reference_mobile_number;
    public $character_reference_telephone_number;

    public $emergency_contact_name;
    public $emergency_contact_relationship;
    public $emergency_contact_address;
    public $emergency_contact_mobile_number;
    public $emergency_contact_telephone_number;

    public $family_mother_maiden_name;
    public $family_father_name;
    public $family_siblings = [];
    public $family_spouse;
    public $family_childrens = [];

    public $earnings_basic_pay;
    public $earnings_cola;
    public $earnings_gross_pay;

    public $branch_references = [];
    public $position_references = [];
    public $job_level_references = [];
    public $department_references = [];
    public $division_references = [];
    public $employment_category_references = [];
    public $employment_status_references = [];
    public $agency_references = [];
    public $gender_references = [];
    public $work_schedule_references = [];
    public $educational_level_references = [];
    public $ecd_relationship_references = [];

    public $user;

    public $erf_reference_search;
    public $erf_references = [];
    public $erf_reference_no;


    public function generateEmplyeeIDNo()
    {
        // $employee_no = UserDetails::select('employee_number')->latest()->first();
        // $highest_employee_no = UserDetails::max('employee_number');

        // $this->employee_id = $highest_employee_no + 1;
    }

    public function load()
    {
        $this->add('family_sibling');
        $this->add('family_children');
        $this->loadBranchReferences();
        $this->loadPositionReferences();
        $this->loadJobLevelReferences();
        $this->loadDepartmentReferences();
        $this->loadDivisionReferences();
        $this->loadEmploymentCategoryReferences();
        $this->loadEmploymentStatusReferences();
        $this->loadGenderReferences();
        $this->loadAgencyReferences();
        $this->loadWorkScheduleReferences();
        $this->loadEducationalLevelReferences();
        $this->loadECDRelationshipReference();
        $this->loadErfReferences();
    }

    public function generateDefaultEmail()
    {
        $this->company_email_address = 'default_email' . rand(100000, 999999) . '@capex.ph';
    }

    public function loadBranchReferences()
    {
        $this->branch_references = BranchReference::get();
    }

    public function loadPositionReferences()
    {
        $this->position_references = Position::where('division_id', $this->division)->where('department_level_id', $this->department)->get();
    }

    public function loadJobLevelReferences()
    {
        $this->job_level_references = JobLevel::get();
    }

    public function loadDepartmentReferences()
    {
        $this->department_references = Department::where('division_id', $this->division)->get();
    }

    public function loadDivisionReferences()
    {
        $this->division_references = Division::get();
    }

    protected function updatedDivision()
    {
        $this->loadDepartmentReferences();
        $this->loadPositionReferences();
    }

    protected function updatedDepartment()
    {
        $this->loadPositionReferences();
    }

    public function loadEmploymentCategoryReferences()
    {
        $this->employment_category_references = EmploymentCategory::get();
    }

    public function loadEmploymentStatusReferences()
    {
        $this->employment_status_references = EmploymentStatus::get();
    }

    public function loadGenderReferences()
    {
        $this->gender_references = GenderReference::get();
    }

    public function loadAgencyReferences()
    {
        $this->agency_references = Agencies::get();
    }

    public function loadWorkScheduleReferences()
    {
        $this->work_schedule_references = WorkSchedule::get();
    }

    public function loadEducationalLevelReferences()
    {
        $this->educational_level_references = EALevelReference::get();
    }

    public function loadECDRelationshipReference()
    {
        $this->ecd_relationship_references = ECDRelationshipReference::get();
    }

    protected function updatedWorkSchedule()
    {
        $work_schedule = WorkSchedule::find($this->work_schedule);

        if ($work_schedule) {
            if ($work_schedule->monday) {
                $this->inclusive_days .= "Monday";
            }
            if ($work_schedule->tuesday) {
                $this->inclusive_days .= ", Tuesday";
            }
            if ($work_schedule->wednesday) {
                $this->inclusive_days .= ", Wednesday";
            }
            if ($work_schedule->thursday) {
                $this->inclusive_days .= ", Thursday";
            }
            if ($work_schedule->friday) {
                $this->inclusive_days .= ", Friday";
            }
            if ($work_schedule->saturday) {
                $this->inclusive_days .= ", Saturday";
            }
            if ($work_schedule->sunday) {
                $this->inclusive_days .= ", Sunday";
            }

            $this->time_schedule = date('h:i A', strtotime($work_schedule->time_from)) . ' - ' . date('h:i A', strtotime($work_schedule->time_to));
        }
    }

    protected function updatedEarningsBasicPay()
    {
        $this->computeGrossPay();
    }

    protected function updatedEarningsCola()
    {
        $this->computeGrossPay();
    }

    protected function computeGrossPay()
    {
        // dd($this->earnings_basic_pay + $this->earnings_cola);
        $this->earnings_gross_pay = (($this->earnings_basic_pay == "" ? 0 : $this->earnings_basic_pay) + ($this->earnings_cola == "" ? 0 : $this->earnings_cola));
    }

    public function action(EmployeeInformationInterface $employee_information_interface, $data, $action_type)
    {
        $this->action_type = $action_type;
        if ($action_type == 'create_next') {
            $response = $employee_information_interface->createValidationTab1($this->getRequest());
            if ($response['code'] == 200) {
                $this->current_tab = 2;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($action_type == 'create_next_perview') {
            $response = $employee_information_interface->createValidationTab2($this->getRequest());
            if ($response['code'] == 200) {
                $this->perview_summary_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($action_type == 'edit_next') {
            $response = $employee_information_interface->updateValidationTab1($this->user, $this->getRequest());
            if ($response['code'] == 200) {
                $this->current_tab = 2;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($action_type == 'edit_next_perview') {
            $response = $employee_information_interface->updateValidationTab2($this->user, $this->getRequest());
            if ($response['code'] == 200) {
                $this->perview_summary_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } elseif ($action_type == 'remove_family_sibling') {
            $this->index = $data['index'];
            $this->confirmation_message = "Are you sure to remove this sibling.";
            $this->confirmation_modal = true;
        } elseif ($action_type == 'remove_family_children') {
            $this->index = $data['index'];
            $this->confirmation_message = "Are you sure to remove this children.";
            $this->confirmation_modal = true;
        }
    }

    public function add($action_type)
    {
        if ($action_type == 'family_sibling') {
            $this->family_siblings[] = [
                'id' => null,
                'name' => null,
                'is_deleted' => false
            ];
        } elseif ($action_type == 'family_children') {
            $this->family_childrens[] = [
                'id' => null,
                'name' => null,
                'is_deleted' => false
            ];
        }
    }

    public function remove()
    {
        if ($this->action_type == 'remove_family_sibling' && count(collect($this->family_siblings)->where('is_deleted', false)) > 1) {
            if ($this->family_siblings[$this->index]['id']) {
                $this->family_siblings[$this->index]['is_deleted'] = true;
            } else {
                unset($this->family_siblings[$this->index]);
            }
            array_values($this->family_siblings);
        } elseif ($this->action_type == 'remove_family_children' && count(collect($this->family_childrens)->where('is_deleted', false)) > 1) {
            if ($this->family_childrens[$this->index]['id']) {
                $this->family_childrens[$this->index]['is_deleted'] = true;
            } else {
                unset($this->family_childrens[$this->index]);
            }
            array_values($this->family_childrens);
        }

        $this->confirmation_modal = false;
    }

    public function loadErfReferences()
    {
        $this->erf_references = EmployeeRequisition::where([
            ['erf_reference_no', 'like', '%' . $this->erf_reference_search . '%']
        ])
            ->where('first_status', '=', 3)
            ->orderBy('erf_reference_no', 'asc')
            ->take(10)
            ->get();
    }


    protected function updatedErfReferenceSearch()
    {
        $this->loadErfReferences();
        if (!$this->erf_reference_search) {
            $this->reset([
                'erf_reference',
                'position',
                'branch',
                'first_name',
                'middle_name',
                'last_name',
                'division',
            ]);
        }

        $this->loadDepartmentReferences();
        $this->loadPositionReferences();
    }

    public function getErfDetails($id)
    {
        $applicant_tracking = EmployeeRequisition::with('erfReference')->find($id);
        // dd($applicant_tracking);
        if ($applicant_tracking) {
            $this->erf_reference_search = $applicant_tracking->erf_reference_no;
            $this->erf_reference = $applicant_tracking->id;
            $this->position = $applicant_tracking->position_id;
            $this->branch = $applicant_tracking->branch_id;
            $this->division = $applicant_tracking->division_id;
            $this->first_name = $applicant_tracking->erfReference->firstname;
            $this->middle_name = $applicant_tracking->erfReference->middlename;
            $this->last_name = $applicant_tracking->erfReference->lastname;
        }

        $this->loadDepartmentReferences();
        $this->loadPositionReferences();
    }

    public function getRequest()
    {
        return [
            'user' => $this->user,
            'erf_reference' => $this->erf_reference,

            'photo' => $this->photo,
            'employee_id' => $this->employee_id,
            'branch' => $this->branch,
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'last_name' => $this->last_name,
            'position' => $this->position,
            'job_level' => $this->job_level,
            'department' => $this->department,
            'division' => $this->division,
            'employement_category' => $this->employement_category,
            'employement_status' => $this->employement_status,
            'gender' => $this->gender,
            'agency' => $this->agency,
            'date_of_birth' => $this->date_of_birth,
            'date_hired' => $this->date_hired,
            'current_address' => $this->current_address,
            'permanent_address' => $this->permanent_address,
            // 'personal_mobile_number' => $this->personal_mobile_number,
            'personal_mobile_number' => preg_replace("/[^0-9]/", '', $this->personal_mobile_number),
            'personal_telephone_number' => $this->personal_telephone_number,
            'personal_email_address' => $this->personal_email_address,

            // 'company_mobile_number' => $this->company_mobile_number,
            'company_mobile_number' => preg_replace("/[^0-9]/", '', $this->company_mobile_number),
            'company_telephone_number' => $this->company_telephone_number,

            'company_email_address' => $this->company_email_address,
            'password' => $this->password,
            'password_confirmation' => $this->password_confirmation,

            'work_schedule' => $this->work_schedule,
            'inclusive_days' => $this->inclusive_days,
            'time_schedule' => $this->time_schedule,

            'sss' => $this->sss,
            'pagibig_number' => $this->pagibig_number,
            'philhealth_number' => $this->philhealth_number,
            'tin_number' => $this->tin_number,

            'educational_level' => $this->educational_level,
            'educational_institution' => $this->educational_institution,
            'educational_inclusive_year_from' => $this->educational_inclusive_year_from,
            'educational_inclusive_year_to' => $this->educational_inclusive_year_to,

            'employment_employer' => $this->employment_employer,
            'employment_position' => $this->employment_position,
            'employment_inclusive_year_from' => $this->employment_inclusive_year_from,
            'employment_inclusive_year_to' => $this->employment_inclusive_year_to,

            'character_reference_name' => $this->character_reference_name,
            'character_reference_position' => $this->character_reference_position,
            'character_reference_company' => $this->character_reference_company,
            // 'character_reference_mobile_number' => $this->character_reference_mobile_number,
            'character_reference_mobile_number' => preg_replace("/[^0-9]/", '', $this->character_reference_mobile_number),

            'character_reference_telephone_number' => $this->character_reference_telephone_number,

            'emergency_contact_name' => $this->emergency_contact_name,
            'emergency_contact_relationship' => $this->emergency_contact_relationship,
            'emergency_contact_address' => $this->emergency_contact_address,
            // 'emergency_contact_mobile_number' => $this->emergency_contact_mobile_number,
            'emergency_contact_mobile_number' => preg_replace("/[^0-9]/", '', $this->emergency_contact_mobile_number),

            'emergency_contact_telephone_number' => $this->emergency_contact_telephone_number,

            'family_mother_maiden_name' => $this->family_mother_maiden_name,
            'family_father_name' => $this->family_father_name,
            'family_siblings' => $this->family_siblings,
            'family_spouse' => $this->family_spouse,
            'family_childrens' => $this->family_childrens,

            'earnings_basic_pay' => $this->earnings_basic_pay,
            'earnings_cola' => $this->earnings_cola,
            'earnings_gross_pay' => $this->earnings_gross_pay,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'photo',
            'employee_id',
            'branch',
            'first_name',
            'middle_name',
            'last_name',
            'position',
            'job_level',
            'department',
            'division',
            'employement_category',
            'employement_status',
            'gender',
            'agency',
            'date_of_birth',
            'date_hired',
            'current_address',
            'permanent_address',
            'personal_mobile_number',
            'personal_telephone_number',
            'personal_email_address',

            'company_mobile_number',
            'company_telephone_number',

            'company_email_address',

            'work_schedule',
            'inclusive_days',
            'time_schedule',

            'sss',
            'pagibig_number',
            'philhealth_number',
            'tin_number',

            'educational_level',
            'educational_institution',
            'educational_inclusive_year_from',
            'educational_inclusive_year_to',

            'employment_employer',
            'employment_position',
            'employment_inclusive_year_from',
            'employment_inclusive_year_to',

            'character_reference_name',
            'character_reference_position',
            'character_reference_company',
            'character_reference_mobile_number',
            'character_reference_telephone_number',

            'emergency_contact_name',
            'emergency_contact_relationship',
            'emergency_contact_address',
            'emergency_contact_mobile_number',
            'emergency_contact_telephone_number',

            'family_mother_maiden_name',
            'family_father_name',
            'family_siblings',
            'family_spouse',
            'family_childrens',

            'earnings_basic_pay',
            'earnings_cola',
            'earnings_gross_pay',
        ]);
    }
}
