<?php

namespace App\Traits\Hrim\EmployeeManagement;

use App\Models\Hrim\EmploymentStatus;

trait EmploymentStatusTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $view_modal = false;
    public $delete_modal = false;

    public $confirmation_message;
    public $confirmation_modal = false;
    
    public $status;

    // public $category_references = [];

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;

    public $emp_status_id;

    public function resetForm()
    {
        $this->reset([
            'status',
        ]);        
        $this->confirmation_modal = false;

    }

    // public function categoryReferences()
    // {
    //     $this->category_references = EmploymentStatus::get();
    // }

    public function getRequest()
    {
        return [
            'status' =>$this->status,
        ];
        
    }
    
}

