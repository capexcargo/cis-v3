<?php

namespace App\Traits\Hrim\EmployeeManagement;


trait ResignationReasonReferenceTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $view_modal = false;
    public $delete_modal = false;

    public $confirmation_message;
    public $confirmation_modal = false;
    
    public $reason;

    // public $category_references = [];

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;

    public $res_rr_id;

    public function resetForm()
    {
        $this->reset([
            'reason',
        ]);        
        $this->confirmation_modal = false;

    }

    // public function categoryReferences()
    // {
    //     $this->category_references = EmploymentStatus::get();
    // }

    public function getRequest()
    {
        return [
            'reason' =>$this->reason,
        ];
        
    }
    
}

