<?php

namespace App\Traits\Hrim\EmployeeManagement;

use App\Models\BranchReference;
use App\Models\Hrim\CocSection;
use App\Models\Hrim\DisciplinaryRecord;
use App\Models\Hrim\Position;
use App\Models\Hrim\Sanction;
use App\Models\Hrim\SanctionStatus;
use App\Models\Hrim\Violation;
use App\Models\User;
use App\Models\UserDetails;

trait DisciplinaryHistoryTrait
{
    public $confirmation_modal = false;
    public $confirmation_message;
    public $action_type;

    public $create_disciplinary_history_modal = false;
    public $edit_disciplinary_history_modal = false;
    public $view_disciplinary_history_modal = false;
    
    public $employee_name;
    public $employee_name_search;
    public $position;

    public $incident_date;
    public $incident_time;
    public $incident_location;
    public $description;
    public $coc_section;
    public $violation;
    public $disciplinary_record;
    public $sanction;
    public $sanction_status;
    public $date_created;
    public $attachments = [];

    public $employee_name_reference = [];

    public $incident_location_reference = [];
    public $coc_section_reference = [];
    public $violation_reference = [];
    public $disciplinary_record_reference = [];
    public $sanction_reference = [];
    public $sanction_status_reference = [];


    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;

    public $disciplinary_history_id;

    public function load()
    {
        $this->loadFullnameReferences();
        // $this->addAttachments();
    }

    public function addAttachments()
    {
        $this->attachments[] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }

    public function removeAttachments($i)
    {
        if (count(collect($this->attachments)->where('is_deleted', false)) > 1) {
            if ($this->attachments[$i]['id']) {
                $this->attachments[$i]['is_deleted'] = true;
            } else {
                unset($this->attachments[$i]);
            }

            array_values($this->attachments);
        }
    }

    public function loadIncidentLocation()
    {
        $this->incident_location_reference = BranchReference::get();
    }

    public function loadCocSection()
    {
        $this->coc_section_reference = CocSection::get();
    }

    public function loadViolation()
    {
        $this->violation_reference = Violation::get();
    }

    public function loadDisciplinaryRecord()
    {
        $this->disciplinary_record_reference = DisciplinaryRecord::get();
    }

    public function loadSanction()
    {
        $this->sanction_reference = Sanction::get();
    }

    public function loadSanctionStatus()
    {
        $this->sanction_status_reference = SanctionStatus::get();
    }


    public function loadFullnameReferences()
    {
        $this->employee_name_reference = User::where([
            ['name', 'like', '%' . $this->employee_name_search . '%']
        ])
            ->orderBy('name', 'asc')
            ->take(10)
            ->get();
    }

    protected function updatedEmployeeNameSearch()
    {
        $this->loadFullnameReferences();
    }


    public function getEmployeeName($id)
    {
        $user = User::with('userDetails')->find($id);
        $emp_position = Position::find($user->userDetails->position_id);

        if ($user) {
            $this->employee_name_search = $user->name;
            $this->employee_name = $user->id;
            if (!empty($emp_position)) {
                $this->position = $emp_position->display;
            } else {
                $this->position = "No Position Assigned";
            }
        }
    }

    public function action(array $data, $action_type)
    {
        $validated = $this->validate([
            'employee_name' => 'required',
            'incident_date' => 'required',
            'incident_time' => 'required',
            'incident_location' => 'required',
            'description' => 'required',
            'coc_section' => 'required',
            'violation' => 'required',
            'disciplinary_record' => 'required',
            'sanction' => 'required',
            'sanction_status' => 'required',
            
            'attachments' => 'required',
            'attachments.*.attachment' => 'required|mimes:jpg,jpeg,png' . config('filesystems.validation_all'),
        ], [
            'attachments.*.attachment.required' => 'This attachment field is required.',
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
        ]);

        $this->action_type = $action_type;

        if ($validated && $this->action_type == "submit") {
            $this->confirmation_modal = true;
        }
    }

    public function confirm()
    {
        if ($this->action_type == "submit") {
            $this->confirmation_modal = false;
            $this->submit();
        }
    }

    public function resetForm()
    {
        $this->reset([
            'employee_name_search',
            'position',
            'incident_time',
            'incident_location',
            'description',
            'coc_section',
            'violation',
            'disciplinary_record',
            'sanction',
            'sanction_status',
            'attachments',
        ]);
    }
}
