<?php

namespace App\Traits\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\KraPointsManagementInterface;
use App\Models\Hrim\JobLevel;

trait KraPointsManagementTrait
{

    public $confirmation_modal = false;
    public $confirmation_message;
    public $create_kra_points_modal = false;
    public $edit_kra_points_modal = false;
    public $action_type;

    public $points;
    public $description;
    public $quarter;
    public $year;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public function loadYear()
    {
        $this->year = date('Y');
    }

    public function action(KraPointsManagementInterface $kra_points_management_interface, $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_kra_points') {
            $response = $kra_points_management_interface->createValidation($this->getRequest());
            if ($response['code'] == 200) {
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function getRequest()
    {
        return [
            'points' => $this->points,
            'description' => $this->description,
            'quarter' => $this->quarter,
            'year' => $this->year,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'points',
            'description',
            'quarter',
            'year',
        ]);
    }
}
