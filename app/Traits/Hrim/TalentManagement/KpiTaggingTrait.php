<?php

namespace App\Traits\Hrim\TalentManagement;

use App\Models\Hrim\KpiManagement;
use App\Models\Hrim\KraManagement;
use App\Models\Hrim\Position;

trait KpiTaggingTrait
{
    public $confirmation_modal = false;
    public $confirmation_message;
    public $action_type;

    public $create_kpi_tagging_modal = false;
    public $view_kpi_tagging_modal = false;

    public $position;
    public $quarter;
    public $year;
    public $kpis = [];
    public $kras = [];
    public $no = 1;

    public $position_reference = [];
    public $kpi_reference = [];
    public $kra_reference = [];


    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $years;

    public $kpi_tagging_id;

    public $kpi_tag = [];

    public function load()
    {
        $this->positionReference();
        $this->loadYear();
        $this->kpiReference();
        $this->kraReference();
    }

    public function loadYear()
    {
        $this->year = date('Y');
    }

    public function action($data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_kpi_tagging') {
            $this->create_kpi_tagging_modal = true;
        } else if ($action_type == 'view_kpi_tagging') {
            $this->view_kpi_tagging_modal = true;
            $this->kpi_tagging_id = $data['id'];
            $this->kpi_quarter = $data['quarter'];
            $this->kpi_year = $data['year'];

            $this->emitTo('hrim.talent-management.kpi-tagging.view', 'mount', $data['id'], $data['quarter'], $data['year']);
        }
    }

    public function positionReference()
    {
        $this->position_reference = Position::get();
    }

    public function kpiReference()
    {
        $this->kpi_reference = KpiManagement::get();
    }

    public function kraReference()
    {
        $this->kra_reference = KraManagement::get();
    }

    public function resetForm()
    {
        $this->reset([
            'position',
            'quarter',
            'kpis',
            'kras',
        ]);
    }

    public function getRequest()
    {
        return [
            'position' => $this->position,
            'quarter' => $this->quarter,
            'year' => $this->year,
            'kpis' => $this->kpis,
            'kras' => $this->kras,
        ];
    }
}
