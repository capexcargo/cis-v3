<?php

namespace App\Traits\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\CoreValueManagementInterface;
use App\Models\BranchReference;
use App\Models\Division;
use App\Models\User;

trait EvaluatorManagementTrait
{
    public $confirmation_modal = false;
    public $confirmation_message;
    public $edit_evaluator_modal = false;
    public $action_type;

    public $employee_name;
    public $division;
    public $branch;
    public $evaluator_id;
    public $division_id;

    public $division_reference = [];
    public $branch_reference = [];

    public $first_evaluator;
    public $second_evaluator;
    public $third_evaluator;
    public $first_evaluator_reference = [];
    public $second_evaluator_reference = [];
    public $third_evaluator_reference = [];

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;

    public function divisionReference()
    {
        $this->division_reference = Division::get();
    }

    public function branchReference()
    {
        $this->branch_reference = BranchReference::get();
    }

    public function firstEvaluatorReference()
    {
        $this->first_evaluator_reference = User::where('level_id', '>=', 2)->get();
    }

    public function secondEvaluatorReference()
    {
        $this->second_evaluator_reference = User::where('level_id', '>=', 2)->get();
    }

    public function thirdEvaluatorReference()
    {
        $this->third_evaluator_reference = User::where('level_id', '>=', 2)->get();
    }

    public function getRequest()
    {
        return [
            'first_evaluator' => $this->first_evaluator,
            'second_evaluator' => $this->second_evaluator,
            'third_evaluator' => $this->third_evaluator,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'first_evaluator',
            'second_evaluator',
            'third_evaluator',
        ]);
    }
}
