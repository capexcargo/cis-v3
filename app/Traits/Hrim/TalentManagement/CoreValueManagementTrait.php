<?php

namespace App\Traits\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\CoreValueManagementInterface;

trait CoreValueManagementTrait
{

    public $confirmation_modal = false;
    public $confirmation_message;
    public $create_core_value_modal = false;
    public $edit_core_value_modal = false;
    public $action_type;

    public $core_value;

    public function loadYear()
    {
        $this->year = date('Y');
    }
    
    public function action(CoreValueManagementInterface $core_value_management_interface, $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_core_value') {
            $response = $core_value_management_interface->createValidation($this->getRequest());
            if ($response['code'] == 200) {
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function getRequest()
    {
        return [
            'core_value' => $this->core_value,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'core_value',
        ]);
    }
}
