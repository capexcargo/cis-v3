<?php

namespace App\Traits\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\KpiManagementInterface;

trait KpiManagementTrait
{

    public $confirmation_modal = false;
    public $confirmation_message;
    public $create_kpi_modal = false;
    public $edit_kpi_modal = false;
    public $action_type;

    public $kpi_name;
    public $quarter;
    public $year;
    public $target;
    public $points;
    public $remarks;

    public function loadYear()
    {
        $this->year = date('Y');
    }
    
    public function action(KpiManagementInterface $kpi_management_interface, $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_kpi') {
            $response = $kpi_management_interface->createValidation($this->getRequest());
            if ($response['code'] == 200) {
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function getRequest()
    {
        return [
            'kpi_name' => $this->kpi_name,
            'quarter' => $this->quarter,
            'year' => $this->year,
            'target' => $this->target,
            'points' => $this->points,
            'remarks' => $this->remarks,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'kpi_name',
            'quarter',
            'target',
            'points',
            'remarks',
        ]);
    }
}
