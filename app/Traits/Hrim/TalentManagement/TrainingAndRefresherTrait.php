<?php

namespace App\Traits\Hrim\TalentManagement;

use App\Models\Division;
use App\Models\Hrim\Position;
use App\Models\User;

trait TrainingAndRefresherTrait
{
    public $action_type;

    public $employee_references = [];
    public $employee_name_search;
    public $employee_name;

    public $position;

    public $tag_atleast_one;
    public $title;
    public $description;
    public $is_all_tagged;
    public $division;
    public $positions = [];
    public $attachments = [];

    public $create_modal = false;
    public $play_modal = false;
    public $edit_modal = false;
    public $view_modal = false;
    public $confirmation_modal = false;
    public $confirmation_message;

    public $position_references = [];
    public $division_references = [];

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $train_and_ref_id;
    public $train_and_ref;
    public $video_ref;

    public function load()
    {
        $this->loadEmpNameReferences();
        $this->loadDivisions();
        $this->loadPositions();

        $this->attachments[] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }

    public function loadDivisions()
    {
        $this->division_references = Division::get();
        $this->position_references = Position::where('division_id', $this->division)->orderBy('id', 'desc')->get();
    }

    public function loadPositions()
    {
        if ($this->is_all_tagged != null) {
            $this->division = '';
            $this->resetCheckbox();
            $this->position_references = [];
        }
    }

    protected function updatedDivision()
    {
        $this->division_references = Division::get();
        $this->position_references = Position::where('division_id', $this->division)->orderBy('id', 'desc')->get();
        $this->is_all_tagged = '';
        $this->resetCheckbox();
    }

    public function loadEmpNameReferences()
    {
        $this->employee_references = User::where([
            ['name', 'like', '%' . $this->employee_name_search . '%']
        ])
            ->orderBy('name', 'asc')
            ->take(10)
            ->get();
    }


    public function getEmployeeDetails($id)
    {
        $employee_data = User::find($id);

        if ($employee_data) {
            $this->employee_name_search = $employee_data->name;
            $this->employee_name = $employee_data->id;
            $this->position = $employee_data->userDetails->position_id;
            $this->division = $employee_data->userDetails->divisions_id;
        }
    }

    protected function updatedEmployeeNameSearch()
    {
        $this->loadEmpNameReferences();
    }

    protected function updatedIsAllTagged()
    {
        $this->resetCheckbox();
        $this->division = '';
        $this->position_references = [];
    }

    protected function updatedPositions()
    {
        foreach ($this->positions as $i => $val) {
            if (!$val['position']) {
                unset($this->positions[$i]);
            }
        }
    }

    public function getRequest()
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'is_all_tagged' => $this->is_all_tagged,
            'division' => $this->division,
            'positions' => $this->positions,
            'attachments' => $this->attachments,
        ];
    }

    public function resetCheckbox()
    {
        $this->reset([
            'positions'
        ]);
    }

    public function resetForm()
    {
        $this->reset([
            'title',
            'description',
            'is_all_tagged',
            'division',
            'positions',
            'attachments',
        ]);
    }
}
