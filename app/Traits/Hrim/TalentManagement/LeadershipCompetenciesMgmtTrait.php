<?php

namespace App\Traits\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\LeadershipCompetenciesMgmtInterface;

trait LeadershipCompetenciesMgmtTrait
{

    public $confirmation_modal = false;
    public $confirmation_message;
    public $create_leadership_competencies_modal = false;
    public $edit_leadership_competencies_modal = false;
    public $action_type;

    public $leadership_competencies;

    public function loadYear()
    {
        $this->year = date('Y');
    }
    
    public function action(LeadershipCompetenciesMgmtInterface $leadership_competencies_mgmt_interface, $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_leadership_competencies') {
            $response = $leadership_competencies_mgmt_interface->createValidation($this->getRequest());
            if ($response['code'] == 200) {
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function getRequest()
    {
        return [
            'leadership_competencies' => $this->leadership_competencies,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'leadership_competencies',
        ]);
    }
}
