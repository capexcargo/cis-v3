<?php

namespace App\Traits\Hrim\TalentManagement;

use App\Models\BranchReference;
use App\Models\Division;
use App\Models\Hrim\CoreValueManagement;
use App\Models\Hrim\JobLevel;
use App\Models\Hrim\KraPointsManagement;
use App\Models\Hrim\LeadershipCompetenciesMgmt;
use App\Models\Hrim\PerformanceEvaluation;
use App\Models\Hrim\Position;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig\Sum;

trait PerformanceEvaluationTrait
{
    public $action_type;

    public $evaluation_date;
    public $date_evaluated_from;
    public $date_evaluated_to;
    public $quarter;
    public $year;
    public $employee_name;
    public $position;
    public $division;
    public $branch;
    public $evaluator;
    public $performance_evaluation;
    public $employee_remarks;

    public $performance_evaluation_references = [];

    public $position_references = [];
    public $division_references = [];
    public $job_level_references = [];
    public $branch_references = [];
    public $evaluator_references = [];

    public $performance_scorings = [];
    public $core_value_assessments = [];
    public $leadership_competencies = [];
    public $performance_evaluators = [];

    public $core_values = [];
    public $leadership_comps = [];
    public $employee_evals = [];

    public $performance_evaluation_evals = [];
    public $kpi_tagged_references = [];
    public $kra_references = [];
    public $kpi_references = [];
    public $evaluation_references = [];
    public $evaluation_points_references = [];

    public $performance_id;

    public $total = 0;


    public $total_first_points_eval_scoring;
    public $total_second_points_eval_scoring;
    public $total_third_points_eval_scoring;

    public $total_first_points_core_value;
    public $total_second_points_core_value;
    public $total_third_points_core_value;

    public $total_first_points_leader_comp;
    public $total_second_points_leader_comp;
    public $total_third_points_leader_comp;

    public $total_employee_points_core_value;
    public $total_employee_points_leader_comp;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;

    public $key = 1;

    public $scoring_first_points = [];
    public $scoring_second_points = [];
    public $scoring_third_points = [];

    public $core_value_first_points = [];
    public $core_value_second_points = [];
    public $core_value_third_points = [];

    public $lead_comp_first_points = [];
    public $lead_comp_second_points = [];
    public $lead_comp_third_points = [];

    public $core_value_employee_points = [];
    public $lead_comp_employee_points = [];

    public $scoring_first_weight;
    public $scoring_second_weight;
    public $scoring_third_weight;
    public $self_eval_status;

    public $breakdown_pct_goals;
    public $breakdown_pct_core_values;
    public $breakdown_pct_leadership_competencies;


    public function load()
    {
        $this->loadPositions();
        $this->loadDivisions();
        $this->loadJobLevels();
        $this->loadBranches();
        $this->loadEvaluators();

        $this->loadEvaluationPoints();

        $this->first_compute_eval_scoring();
        $this->second_compute_eval_scoring();
        $this->third_compute_eval_scoring();

        $this->first_compute_core_value();
        $this->second_compute_core_value();
        $this->third_compute_core_value();

        $this->first_compute_leader_comp();
        $this->second_compute_leader_comp();
        $this->third_compute_leader_comp();
    }

    public function loadEmployee()
    {
        $this->employee_self_eval_core_value();
        $this->employee_self_eval_leader_comp();
        $this->loadEvaluationPoints();
    }

    public function first_compute_eval_scoring()
    {
        foreach ($this->getRequest()['performance_scorings'] as $i => $scoring) {
            $scoring_first_points[] =  $scoring['first_evaluator_points'];

            // $weight = ($scoring['points'] / 100);
            // $weighted_score =  $weight * ($scoring['first_evaluator_points'] / $weight);
            // $total_score[] = $weighted_score / count($scoring_first_points);

            // $scoring_first_weight[] = array_sum($total_score) / count($scoring_first_points);
            $total = array_sum($scoring_first_points) / count($scoring_first_points);
            $this->total_first_points_eval_scoring = number_format($total, 1, '.', '');
        }
    }

    public function second_compute_eval_scoring()
    {
        foreach ($this->getRequest()['performance_scorings'] as $i => $scoring) {
            $scoring_second_points[] =  $scoring['second_evaluator_points'];

            // $weight = ($scoring['points'] / 100);
            // $weighted_score =  $weight * ($scoring['second_evaluator_points'] / $weight);
            // $total_score[] = $weighted_score / count($scoring_second_points);

            // $scoring_second_weight[] = array_sum($total_score) / count($scoring_second_points);
            $total = array_sum($scoring_second_points) / count($scoring_second_points);
            $this->total_second_points_eval_scoring = number_format($total, 1, '.', '');
        }
    }

    public function third_compute_eval_scoring()
    {
        foreach ($this->getRequest()['performance_scorings'] as $i => $scoring) {
            $scoring_third_points[] =  $scoring['third_evaluator_points'];

            // $weight = ($scoring['points'] / 100);
            // $weighted_score =  $weight * ($scoring['third_evaluator_points'] / $weight);
            // $total_score[] = $weighted_score / count($scoring_third_points);

            // $scoring_third_weight[] = array_sum($total_score) / count($scoring_third_points);
            $total = array_sum($scoring_third_points) / count($scoring_third_points);
            $this->total_third_points_eval_scoring = number_format($total, 1, '.', '');
        }
    }


    public function first_compute_core_value()
    {
        foreach ($this->getRequest()['core_value_assessments'] as $i => $scoring) {
            $core_value_first_points[] =  $scoring['first_evaluator_points'];
            $core_value_employee_points[] =  $scoring['employee_self_points'];

            $total = array_sum($core_value_first_points) / count($core_value_first_points);
            $this->total_first_points_core_value = number_format($total, 1, '.', '');

            $total_employee_points = array_sum($core_value_employee_points) / count($core_value_employee_points);
            $this->total_employee_points_core_value = number_format($total_employee_points, 1, '.', '');
        }
    }

    public function second_compute_core_value()
    {
        foreach ($this->getRequest()['core_value_assessments'] as $i => $scoring) {
            $core_value_second_points[] =  $scoring['second_evaluator_points'];
            $total = array_sum($core_value_second_points) / count($core_value_second_points);
            $this->total_second_points_core_value = number_format($total, 1, '.', '');
        }
    }

    public function third_compute_core_value()
    {
        foreach ($this->getRequest()['core_value_assessments'] as $i => $scoring) {
            $core_value_third_points[] =  $scoring['third_evaluator_points'];
            $total = array_sum($core_value_third_points) / count($core_value_third_points);
            $this->total_third_points_core_value = number_format($total, 1, '.', '');
        }
    }

    public function first_compute_leader_comp()
    {
        foreach ($this->getRequest()['leadership_competencies'] as $i => $scoring) {
            $lead_comp_first_points[] =  $scoring['first_evaluator_points'];
            $lead_comp_employee_points[] =  $scoring['employee_self_points'];

            $total = array_sum($lead_comp_first_points) / count($lead_comp_first_points);
            $this->total_first_points_leader_comp = number_format($total, 1, '.', '');

            $total_employee_points = array_sum($lead_comp_employee_points) / count($lead_comp_employee_points);
            $this->total_employee_points_leader_comp = number_format($total_employee_points, 1, '.', '');
        }
    }

    public function second_compute_leader_comp()
    {
        foreach ($this->getRequest()['leadership_competencies'] as $i => $scoring) {
            $lead_comp_second_points[] =  $scoring['second_evaluator_points'];

            $total = array_sum($lead_comp_second_points) / count($lead_comp_second_points);
            $this->total_second_points_leader_comp = number_format($total, 1, '.', '');
        }
    }

    public function third_compute_leader_comp()
    {
        foreach ($this->getRequest()['leadership_competencies'] as $i => $scoring) {
            $lead_comp_third_points[] =  $scoring['third_evaluator_points'];

            $total = array_sum($lead_comp_third_points) / count($lead_comp_third_points);
            $this->total_third_points_leader_comp = number_format($total, 1, '.', '');
        }
    }


    public function employee_self_eval_core_value()
    {
        foreach ($this->eGetRequest()['core_values'] as $i => $scoring) {
            $core_value_employee_points[] =  $scoring['employee_self_eval_points'];

            $total_employee_points = array_sum($core_value_employee_points) / count($core_value_employee_points);
            $this->total_employee_points_core_value = number_format($total_employee_points, 1, '.', '');
        }
    }

    public function employee_self_eval_leader_comp()
    {
        foreach ($this->eGetRequest()['leadership_comps'] as $i => $scoring) {
            $lead_comp_employee_points[] =  $scoring['employee_self_eval_points'];

            $total_employee_points = array_sum($lead_comp_employee_points) / count($lead_comp_employee_points);
            $this->total_employee_points_leader_comp = number_format($total_employee_points, 1, '.', '');
        }
    }

    public function loadPositions()
    {
        $this->position_references = Position::get();
    }

    public function loadDivisions()
    {
        $this->division_references = Division::get();
    }

    public function loadBranches()
    {
        $this->branch_references = BranchReference::get();
    }

    public function loadJobLevels()
    {
        $this->job_level_references = JobLevel::get();
    }

    public function loadEvaluators()
    {
        $this->evaluator_references = User::get();
    }

    public function loadEvaluationPoints()
    {
        $this->evaluation_points_references = KraPointsManagement::get();

        $this->employee_self_eval_core_value();
        $this->employee_self_eval_leader_comp();
    }

    public function getRequest()
    {
        return [
            'performance_scorings' => $this->performance_scorings,
            'core_value_assessments' => $this->core_value_assessments,
            'leadership_competencies' => $this->leadership_competencies,
            'performance_evaluators' => $this->performance_evaluators,
            'breakdown_pct_goals' => $this->breakdown_pct_goals,
            'breakdown_pct_core_values' => $this->breakdown_pct_core_values,
            'breakdown_pct_leadership_competencies' => $this->breakdown_pct_leadership_competencies,
            'first_scoring_total' => $this->total_first_points_eval_scoring * ($this->breakdown_pct_goals / 100),
            'second_scoring_total' => $this->total_second_points_eval_scoring * ($this->breakdown_pct_goals / 100),
            'third_scoring_total' => $this->total_third_points_eval_scoring * ($this->breakdown_pct_goals / 100),
            'first_core_values_total' => $this->total_first_points_core_value * ($this->breakdown_pct_core_values / 100),
            'second_core_values_total' => $this->total_second_points_core_value * ($this->breakdown_pct_core_values / 100),
            'third_core_values_total' => $this->total_third_points_core_value * ($this->breakdown_pct_core_values / 100),
            'first_lead_com_total' => $this->total_first_points_leader_comp * ($this->breakdown_pct_leadership_competencies / 100),
            'second_lead_com_total' => $this->total_second_points_leader_comp * ($this->breakdown_pct_leadership_competencies / 100),
            'third_lead_com_total' => $this->total_third_points_leader_comp * ($this->breakdown_pct_leadership_competencies / 100),
        ];
    }

    public function eGetRequest()
    {
        return [
            'core_values' => $this->core_values,
            'leadership_comps' => $this->leadership_comps,
            'employee_evals' => $this->employee_evals,
            'employee_remarks' => $this->employee_remarks,
        ];
    }

    public function filterRequest(){
        return [
            'quarter' => $this->quarter,
            'year' => $this->year,
        ];
    }
}
