<?php

namespace App\Traits\Hrim\TalentManagement;

use App\Models\Hrim\JobLevel;

trait PercentageManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $approve_modal = false;
    public $confirmation_modal = false;
    public $confirmation_message;

    public $job_level;
    public $goal_percentage;
    public $core_value_percentage;
    public $lead_com_percentage;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $job_level_references = [];

    public $percentage_id;

    public function loadJobLevels()
    {
        $this->job_level_references = JobLevel::get();
    }


    public function resetForm()
    {
        $this->reset([
            'job_level',
            'goal_percentage',
            'core_value_percentage',
            'lead_com_percentage',
        ]);

        $this->confirmation_modal = false;
    }

    public function getRequest()
    {
        return [
            'job_level' => $this->job_level,
            'goal_percentage' => $this->goal_percentage,
            'core_value_percentage' => $this->core_value_percentage,
            'lead_com_percentage' => $this->lead_com_percentage,
        ];
    }
}