<?php

namespace App\Traits\Hrim\TalentManagement;

use App\Interfaces\Hrim\TalentManagement\KraManagementInterface;
use App\Models\Hrim\JobLevel;

trait KraManagementTrait
{

    public $confirmation_modal = false;
    public $confirmation_message;
    public $create_kra_modal = false;
    public $edit_kra_modal = false;
    public $action_type;

    public $kra_name;
    public $type;
    public $quarter;
    public $year;

    public $job_level_references = [];

    public function loadYear()
    {
        $this->year = date('Y');
    }

    public function loadJobLevels()
    {
        $this->job_level_references = JobLevel::get();
    }

    public function action(KraManagementInterface $kra_management_interface, $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'create_kra') {
            $response = $kra_management_interface->createValidation($this->getRequest());
            if ($response['code'] == 200) {
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }
    }

    public function getRequest()
    {
        return [
            'kra_name' => $this->kra_name,
            'type' => $this->type,
            'quarter' => $this->quarter,
            'year' => $this->year,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'kra_name',
            'type',
            'quarter',
        ]);
    }
}
