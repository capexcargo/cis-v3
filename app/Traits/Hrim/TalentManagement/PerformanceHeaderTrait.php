<?php

namespace App\Traits\Hrim\TalentManagement;

trait PerformanceHeaderTrait
{

    public $confirmation_modal = false;
    public $confirmation_message;
    public $action_type;

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToKraMgmt') {
            return redirect()->to(route('hrim.talent-management.kra-management.index'));
        } else if ($action_type == 'redirectToKraPointsMgmt') {
            return redirect()->to(route('hrim.talent-management.kra-points-management.index'));
        } else if ($action_type == 'redirectToKpiMgmt') {
            return redirect()->to(route('hrim.talent-management.kpi-management.index'));
        } else if ($action_type == 'redirectToKpiTagging') {
            return redirect()->to(route('hrim.talent-management.kpi-tagging.index'));
        } else if ($action_type == 'redirectToEvaluatorMgmt') {
            return redirect()->to(route('hrim.talent-management.evaluator-management.index'));
        } else if ($action_type == 'redirectToCoreValueMgmt') {
            return redirect()->to(route('hrim.talent-management.core-value-management.index'));
        } else if ($action_type == 'redirectToLeadershipCompetenciesMgmt') {
            return redirect()->to(route('hrim.talent-management.leadership-competencies-mgmt.index'));
        } else if ($action_type == 'redirectToAdminpercentage') {
            return redirect()->to(route('hrim.talent-management.percentage-management.index'));
        }
    }
}
