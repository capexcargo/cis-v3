<?php

namespace App\Traits\Hrim\PayrollManagement;

use App\Models\Hrim\AdjustmentCategoryReference;
use App\Models\MonthReference;
use App\Models\User;

trait CutOffManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $confirmation_modal = false;
    public $confirmation_message;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $category;
    public $cutoff_date_from;
    public $cutoff_date_to;
    public $payout_date;

    public $cutoff_id;


    public function getPayoutDate()
    {
        $this->payout_date = date("Y-m-d", strtotime("+5days", strtotime($this->cutoff_date_to)));
    }
}
