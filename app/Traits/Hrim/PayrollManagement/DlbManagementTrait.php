<?php

namespace App\Traits\Hrim\PayrollManagement;

use App\Models\MonthReference;
use App\Models\User;

trait DlbManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $view_modal = false;
    public $delete_modal = false;

    // public $philhealth_management_id;
    public $confirmation_message;
    public $confirmation_modal = false;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $employees = [];
    public $months = [];

    public $dlbs = [];
    public $user_id;
    public $year;
    public $month;
    public $payout_cutoff;
    public $amount;
    public $dlb_id;


    public function load()
    {

        $this->loadEmployees();
        $this->loadMonthReferences();
    }

    public function loadEmployees()
    {
        $this->employees = User::with(['userDetails'])->get();
    }

    public function loadMonthReferences()
    {
        $this->months = MonthReference::get();
    }


    public function addDlb()
    {
        $this->dlbs[] = [
            'id' => null,
            'year' => null,
            'month' => null,
            'payout_cutoff' => null,
            'amount' => null,
            'dlb' => null,
            'is_deleted' => false,
        ];
    }

    public function removeDlb(array $data)
    {
        if (count(collect($this->dlbs)->where('is_deleted', false)) > 1) {
            if ($this->dlbs[$data["a"]]['id']) {
                $this->dlbs[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->dlbs[$data["a"]]);
            }

            array_values($this->dlbs);
        }
    }


    public function resetForm()
    {
        $this->reset([
            'user_id',
            'year',
            'month',
            'payout_cutoff',
            'amount',
        ]);

        $this->confirmation_modal = false;
    }

    public function getRequest()
    {
        return [
            'dlbs' => $this->dlbs,
        ];
    }
}
