<?php

namespace App\Traits\Hrim\PayrollManagement;

trait PhilhealthManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $philhealth_management_id;
    public $confirmation_message;
    public $confirmation_modal = false;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $year;
    public $premium_rate;


    public function resetForm()
    {
        $this->reset([
            'year',
            'premium_rate',
        ]);

        $this->confirmation_modal = false;
    }

    public function getRequest()
    {
        return [
            'year' => $this->year,
            'premium_rate' => $this->premium_rate,
        ];
    }
}
