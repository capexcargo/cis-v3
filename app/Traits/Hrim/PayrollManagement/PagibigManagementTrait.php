<?php

namespace App\Traits\Hrim\PayrollManagement;

trait PagibigManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $pagibig_management_id;
    public $confirmation_message;
    public $confirmation_modal = false;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $year;
    public $share;


    public function resetForm()
    {
        $this->reset([
            'year',
            'share',
        ]);

        $this->confirmation_modal = false;
    }

    public function getRequest()
    {
        return [
            'year' => $this->year,
            'share' => $this->share,
        ];
    }
}
