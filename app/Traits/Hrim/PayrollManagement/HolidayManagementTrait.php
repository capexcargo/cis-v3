<?php

namespace App\Traits\Hrim\PayrollManagement;

use App\Models\BranchReference;
use App\Models\Hrim\HolidayTypeReference;

trait HolidayManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $holiday_management_id;
    public $confirmation_message;
    public $confirmation_modal = false;
    public $branch_reference = [];
    public $holiday_type_references = [];

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $name;
    public $date;
    public $branch_id;
    public $type_id;

    public function resetForm()
    {
        $this->reset([
            'name',
            'date',
            'branch_id',
            'type_id',
        ]);

        $this->confirmation_modal = false;

    }

    public function branchReference()
    {
        $this->branch_reference = BranchReference::get();
    }

    public function holidayReferenceType()
    {
        $this->holiday_type_references = HolidayTypeReference::get();
    }

    public function getRequest()
    {
        return [
            'name' => $this->name,
            'date' => $this->date,
            'branch_id' => $this->branch_id,
            'type_id' => $this->type_id,

        ];
    }
}
