<?php

namespace App\Traits\Hrim\PayrollManagement;

trait PayrollManagementHeaderTrait
{
    public $action_type;

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToHolidayMgmt') {
            return redirect()->to(route('hrim.payroll-management.holiday-management.index'));
        } else if ($action_type == 'redirectToSssMgmt') {
            return redirect()->to(route('hrim.payroll-management.sss-management.index'));
        } else if ($action_type == 'redirectToPhilhealthMgmt') {
            return redirect()->to(route('hrim.payroll-management.philhealth-management.index'));
        } else if ($action_type == 'redirectToPagibigMgmt') {
            return redirect()->to(route('hrim.payroll-management.pagibig-management.index'));
        
        } else if ($action_type == 'redirectTo13thMonth') {
            return redirect()->to(route('hrim.payroll-management.13th-month-pay.index'));
        } else if ($action_type == 'redirectToDLB') {
            return redirect()->to(route('hrim.payroll-management.dlb-management.index'));
        } else if ($action_type == 'redirectToPayrollCutOff') {
            return redirect()->to(route('hrim.payroll-management.cut-off-management.index'));
        } else if ($action_type == 'redirectToAdminAdjustment') {
            return redirect()->to(route('hrim.payroll-management.admin-adjustment.index'));
        } else if ($action_type == 'redirectToLoaAdjustmentMgmt') {
            return redirect()->to(route('hrim.payroll-management.loa-adjustment-management.index'));
        }
    }
}
