<?php

namespace App\Traits\Hrim\PayrollManagement;

use App\Models\Hrim\AdjustmentCategoryReference;
use App\Models\MonthReference;
use App\Models\User;

trait AdminAdjustmentTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $approve_modal = false;
    public $confirmation_modal = false;
    public $confirmation_message;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;


    public $employee;
    public $employee_references = [];
    public $employee_search;

    public $category;
    public $description;
    public $amount;
    public $year;
    public $month;
    public $cutoff;

    public $category_references = [];
    public $months = [];

    public $date;
    public $branch_id;
    public $type_id;

    public $adjustment_id;

    public function load()
    {
        $this->categories();
        $this->months();
        $this->loadEmployeeReferences();
    }

    public function loadEmployeeReferences()
    {
        $this->employee_references = User::where([
            ['name', 'like', '%' . $this->employee_search . '%']
        ])
            ->orderBy('name', 'asc')
            ->take(10)
            ->get();

    }

    protected function updatedEmployeeSearch()
    {
        $this->loadEmployeeReferences();
    }
    
    public function getEmployeeDetails($id)
    {
        $employees = User::find($id);

        if ($employees) {
            $this->employee_search = $employees->name;
            $this->employee = $employees->id;
        }
    }

    public function categories()
    {
        $this->category_references = AdjustmentCategoryReference::get();
    }

    public function months()
    {
        $this->months = MonthReference::get();
    }

    public function resetForm()
    {
        $this->reset([
            'employee',
            'employee_search',
            'category',
            'description',
            'amount',
            'year',
            'month',
            'cutoff',
        ]);

        $this->confirmation_modal = false;
    }

    public function getRequest()
    {
        return [
            'employee' => $this->employee,
            'category' => $this->category,
            'description' => $this->description,
            'amount' => $this->amount,
            'year' => $this->year,
            'month' => $this->month,
            'cutoff' => $this->cutoff,
        ];
    }
}
