<?php

namespace App\Traits\Hrim\PayrollManagement;

use App\Models\Hrim\AdjustmentCategoryReference;
use App\Models\MonthReference;
use App\Models\User;

trait LoaAdjustmentTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $confirmation_modal = false;
    public $confirmation_message;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $min_amount;
    public $max_amount;
    public $category;
    public $approver;
    
    public $adjustment_id;

    public $category_references = [];
    public $approver_references = [];

    public function load()
    {
        $this->categories();
        $this->loadApprover();
    }

    public function categories()
    {
        $this->category_references = AdjustmentCategoryReference::get();
    }

    public function loadApprover()
    {
        $this->approver_references = User::where('level_id', '>', 2)->get();
    }

    public function resetForm()
    {
        $this->reset([
            'min_amount',
            'max_amount',
            'category',
            'approver',
        ]);

        $this->confirmation_modal = false;
    }

    public function getRequest()
    {
        return [
            'min_amount' => $this->min_amount,
            'max_amount' => $this->max_amount,
            'category' => $this->category,
            'approver' => $this->approver,
        ];
    }
}
