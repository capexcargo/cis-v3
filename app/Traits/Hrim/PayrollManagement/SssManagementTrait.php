<?php

namespace App\Traits\Hrim\PayrollManagement;

trait SssManagementTrait
{
    public $import_modal = false;
    public $import;
    public $year;
    public $paginate = 10;
    public $status;

    public function resetForm()
    {
        $this->reset([
            'import',
        ]);

        $this->import_modal = false;
    }
}
