<?php

namespace App\Traits\Hrim\Reports;

use App\Models\Division;
use App\Models\Hrim\Department;

trait LoansGovtDeductionTrait
{
    public $view_modal = false;

    public $employee_name;
    public $employee_id;
    public $division;
    public $department;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $loan_govt_id;

    public $division_references = [];
    public $department_references = [];

    public function load()
    {
        $this->loadDivisionReference();
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    protected function updatedDivision()
    {
        $this->department_references = Department::where('division_id', $this->division)->get();
    }
}
