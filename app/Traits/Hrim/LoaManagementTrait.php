<?php

namespace App\Traits\Hrim;

use App\Models\BranchReference;
use App\Models\Division;
use App\Models\Hrim\Department;
use App\Models\Hrim\LoaManagement;
use App\Models\Hrim\LoaTypeReference;
use App\Models\User;
use App\Repositories\Hrim\LoaManagementRepository;

trait LoaManagementTrait
{
    public $division;
    public $department_id;
    public $branch;
    public $type;
    public $first_approver;
    public $second_approver;
    public $third_approver;

    public $division_references = [];
    public $department_references = [];
    public $branch_references = [];
    public $type_references = [];
    public $first_approver_references = [];
    public $second_approver_references = [];
    public $third_approver_references = [];

    public $loa_management;

    public function load()
    {
        $this->loadDivisionReference();
        $this->loadBranchReference();
        $this->loadTypeReference();
        $this->loadDepartmentReference();
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function loadDepartmentReference()
    {
        $this->department_references = Department::where('division_id', $this->division)->get();
    }

    public function loadBranchReference()
    {
        $this->branch_references = BranchReference::get();
    }

    public function loadTypeReference()
    {
        $this->type_references = LoaTypeReference::get();
    }

    public function loadFirstApproverReference()
    {
        $this->first_approver_references = User::whereHas('userDetails', function ($query) {
            $query->where('branch_id', $this->branch)
                ->Where('department_id', $this->department_id)
                ->orWhere('level_id', 4);
        })
            ->where([
                ['division_id', $this->division],
                ['level_id', '>=', 1]
            ])
            ->get();
            // dd($this->first_approver_references);
    }

    public function loadSecodeApproverReference()
    {
        $this->second_approver_references = User::whereHas('userDetails', function ($query) {
            $query->where('branch_id', $this->branch)
                ->where('department_id', $this->department_id)
                ->orWhere('level_id', 4);
        })
            ->where([
                ['division_id', $this->division],
                ['level_id', '>=', 3]
            ])
            ->get();
    }

    public function loadThirdApproverReference()
    {
        $this->third_approver_references = User::whereHas('userDetails', function ($query) {
            $query->where('branch_id', $this->branch)
                ->where('department_id', $this->department_id)
                ->orWhere('level_id', 4);
        })
            ->where([
                ['division_id', $this->division],
                ['level_id', '>=', 4]
            ])
            ->get();
    }

    public function loadApprovers()
    {
        $this->reset([
            'first_approver',
            'second_approver',
            'third_approver'
        ]);

        $this->loadFirstApproverReference();
        $this->loadSecodeApproverReference();
        $this->loadThirdApproverReference();

        $loa_management_repository = new LoaManagementRepository();
        $response = $loa_management_repository->checkLoaManagement($this->getRequest());
        if ($response['code'] == 200) {
            $this->first_approver = $response['result']->first_approver;
            $this->second_approver = $response['result']->second_approver;
            $this->third_approver = $response['result']->third_approver;
        }
    }

    protected function updatedDivision()
    {
        $this->loadApprovers();
        $this->loadDepartmentReference();
    }

    protected function updatedDepartmentId()
    {
        $this->loadApprovers();
        $this->loadDivisionReference();
    }

    protected function updatedBranch()
    {
        $this->loadApprovers();
    }

    protected function updatedType()
    {
        $this->loadApprovers();
    }

    public function getRequest()
    {
        return [
            'division' => $this->division,
            'department_id' => $this->department_id,
            'branch' => $this->branch,
            'type' => $this->type,
            'first_approver' => $this->first_approver,
            'second_approver' => $this->second_approver,
            'third_approver' => $this->third_approver,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'division',
            'department_id',
            'branch',
            'type',
            'first_approver',
            'second_approver',
            'third_approver',
        ]);
    }
}
