<?php

namespace App\Traits\Hrim\CompanyManagement\Bulletin;

use App\Models\Hrim\Announcements;

trait AnnouncementTrait
{

    public $confirmation_modal = false;
    public $confirmation_message;
    public $action_type;

    public $announcement_no;
    public $publish_message;
    public $date_posted;
    public $title;
    public $details;
    public $posted_by;
    public $attachments = [];

    public function load()
    {
        $this->addAttachments();
    }

    public function generateAnnouncementNo()
    {
        // $this->resetForm();
        $ann_no = Announcements::select('id')->latest()->first();

        if ($ann_no) {
            $last_inserted_id = $ann_no->id + 1;
        } else {
            $last_inserted_id = 1;
        }

        $this->announcement_no = "ANCMT-" . str_pad(($last_inserted_id), 5, "0", STR_PAD_LEFT);

        // $this->announcement_no = "ANCMT-" . date("ymdhis") . "-" . $last_inserted_id;
    }

    public function addAttachments()
    {
        $this->attachments[] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }

    public function removeAttachments($i)
    {
        if (count(collect($this->attachments)->where('is_deleted', false)) > 1) {
            if ($this->attachments[$i]['id']) {
                $this->attachments[$i]['is_deleted'] = true;
            } else {
                unset($this->attachments[$i]);
            }

            array_values($this->attachments);
        }
    }

    public function action(array $data, $action_type)
    {
        $validated = $this->validate([
            'announcement_no' => 'required',
            'date_posted' => 'required',
            'title' => 'required',
            'details' => 'required',

            'attachments' => 'nullable',
            'attachments.*.attachment' => 'nullable|' . config('filesystems.validation_all'),
        ], [
            // 'attachments.*.attachment.required' => 'This attachment field is required.',
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
        ]);


        $this->action_type = $action_type;

        if ($action_type == "submit") {
            $this->confirmation_modal = true;
            $this->confirmation_message = "Are you sure you want to publish this announcement?";
        } else if ($action_type == 'cancel_announcement') {
            $this->confirmation_modal = true;
            $this->confirmation_message = "Are you sure you want to close this form?";
        }
    }

    public function confirm()
    {
        if ($this->action_type == "submit") {
            $this->submit();
            $this->confirmation_modal = false;
        }
    }

    public function resetForm()
    {
        $this->reset([
            'announcement_no',
            'date_posted',
            'title',
            'details',
            'attachments',
        ]);
    }
}
