<?php

namespace App\Traits\Hrim\CompanyManagement\Bulletin;

trait MemoTrait
{
    public $confirmation_modal = false;
    public $confirmation_message;
    public $action_type;

    public $memorandum_no;
    public $attention_to;
    public $date_posted;
    public $title;
    public $details;
    public $posted_by;

    public $attachments = [];
    
    public function load(){
        $this->addAttachments();
    }

    public function generateMemoNo()
    {
        $this->memorandum_no;
        // $this->memorandum_no = "IOM-CAS" . date("ymdhis") . "-" . rand(111, 999);
    }

    public function addAttachments()
    {
        $this->attachments[] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }
    
    public function removeAttachments($i)
    {
        if (count(collect($this->attachments)->where('is_deleted', false)) > 1) {
            if ($this->attachments[$i]['id']) {
                $this->attachments[$i]['is_deleted'] = true;
            } else {
                unset($this->attachments[$i]);
            }

            array_values($this->attachments);
        }
    }

    public function action(array $data, $action_type)
    {
        $validated = $this->validate([
            'memorandum_no' => 'required',
            'title' => 'required',
            'details' => 'required',

            'attachments' => 'required',
            'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),

        ],[
            'attachments.*.attachment.required' => 'The attachment field is required.',
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
        ]);

        $this->action_type = $action_type;

        if ($action_type == "submit") {
            $this->confirmation_modal = true;
            $this->confirmation_message = "Are you sure you want to publish this memo?";
        }
    }

    public function confirm()
    {
        if ($this->action_type == "submit") {
            $this->submit();
            $this->confirmation_modal = false;
        }
    }

    public function resetForm()
    {
        $this->reset([
            'memorandum_no',
            // 'date_posted',
            // 'attention_to',
            'title',
            'details',
            'attachments',
        ]);
    }
}
