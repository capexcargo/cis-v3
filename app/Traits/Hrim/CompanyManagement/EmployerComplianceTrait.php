<?php

namespace App\Traits\Hrim\CompanyManagement;

trait EmployerComplianceTrait
{
    public $image = "";
    public $title;
    public $description;

    public $employer_compliance;

    protected function updatedImage()
    {
        if ($this->image != "") {
            $this->resetErrorBag();
        }
    }

    public function getRequest()
    {
        return [
            'image' => $this->image,
            'title' => $this->title,
            'description' => $this->description,
        ];
    }

    public function resetForm()
    {
        $this->reset(
            'image',
            'title',
            'description',
        );
    }
}
