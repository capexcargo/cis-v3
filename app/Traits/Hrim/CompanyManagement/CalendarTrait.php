<?php

namespace App\Traits\Hrim\CompanyManagement;

use App\Interfaces\Hrim\CompanyManagement\CalendarInterface;
use App\Models\BranchReference;
use App\Models\Hrim\EventTypeReference;
use App\Repositories\Hrim\CompanyManagement\CalendarRepository;

trait CalendarTrait
{
    public $event_type = 1;
    public $event_title;
    public $date_and_time;
    public $branch;
    public $description;

    public $branch_references = [];
    public $event_type_references = [];

    public $calendar;

    public function load()
    {
        $this->loadBranchReferences();
        $this->loadEventTypeReferences();
    }

    public function loadBranchReferences()
    {
        $this->branch_references = BranchReference::
        // whereHas('users')->
        get();
    }

    public function loadEventTypeReferences()
    {
        $this->event_type_references = EventTypeReference::get();
    }

    // protected function updated($propertyName)
    // {
    //     $calendar_repository = new CalendarRepository();
    //     $response = $calendar_repository->validation([ $propertyName => $propertyName]);
    //     dd($response);
    //     $this->validateOnly($propertyName, [
    //         'event_title' => 'required',
    //         'date_and_time' => 'required',
    //         'branch' => 'required'
    //     ]);
    // }

    // protected function updatedEventTitle()
    // {
    //     $this->validate([
    //         'event_title' => 'required'
    //     ]);
    // }

    // protected function updatedDateAndTime()
    // {
    //     $this->validate([
    //         'date_and_time' => 'required'
    //     ]);
    // }

    // protected function updatedBranch()
    // {
    //     $this->validate([
    //         'branch' => 'required'
    //     ]);
    // }

    // protected function updatedDescription()
    // {
    //     $this->validate([
    //         'description' => 'required'
    //     ]);
    // }

    public function getRequest()
    {
        return [
            'event_type' => $this->event_type,
            'event_title' => $this->event_title,
            'date_and_time' => $this->date_and_time,
            'branch' => $this->branch,
            'description' => $this->description,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();

        $this->reset(
            'event_title',
            'date_and_time',
            'branch',
            'description'
        );
    }
}
