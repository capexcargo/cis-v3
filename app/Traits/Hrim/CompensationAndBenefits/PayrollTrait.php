<?php

namespace App\Traits\Hrim\CompensationAndBenefits;

use App\Models\BranchReference;
use App\Models\Division;
use App\Models\Hrim\EmploymentCategory;
use App\Models\Hrim\JobLevel;

trait PayrollTrait
{
    public $view_modal  = false;
    public $generate_payroll_modal = false;
    public $confirmation_modal = false;
    public $confirmation_message;

    public $generate_payslip_modal = false;
    public $confirmation_modal_initial = false;
    public $confirmation_message_initial;
    public $confirmation_modal_final = false;
    public $confirmation_message_final;

    public $payout_year;
    public $payout_date;
    public $branch;
    public $employee_id;
    public $employee_name;
    public $employee_category;
    public $employee_code;
    public $division;
    public $job_rank;

    public $branch_references = [];
    public $employee_category_references = [];
    public $division_references = [];
    public $job_rank_references = [];

    public function branchReferences()
    {
        $this->branch_references = BranchReference::get();
    }
    public function employeeCategoryReferences()
    {
        $this->employee_category_references = EmploymentCategory::get();
    }
    public function divisionReferences()
    {
        $this->division_references = Division::get();
    }
    public function jobRankReferences()
    {
        $this->job_rank_references = JobLevel::get();
    }
}
