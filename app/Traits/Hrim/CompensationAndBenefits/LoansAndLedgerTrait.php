<?php

namespace App\Traits\Hrim\CompensationAndBenefits;

use App\Interfaces\Hrim\CompensationAndBenefits\LoansAndLedgerInterface;
use App\Models\BranchReference;
use App\Models\Hrim\LoanDetails;
use App\Models\Hrim\LoansAndLedger;
use App\Models\Hrim\LoanType;
use Illuminate\Support\Facades\Auth;

// use App\Interfaces\Hrim\TalentManagement\CoreValueManagementInterface;

trait LoansAndLedgerTrait
{
    public $confirmation_message;
    public $confirmation_modal = false;
    public $confirmation_approval_modal = false;
    public $confirmation_decline_modal = false;
    public $create_loan_modal = false;
    public $edit_loan_modal = false;
    public $view_loan_modal = false;
    public $view_payment_modal = false;
    public $create_view_payment_modal = false;
    public $view_approval_modal = false;
    public $action_type;

    public $payout_year;
    public $payout_date;
    public $branch;
    public $employee_id;
    public $employee_name;

    public $reference_number;
    public $type;
    public $loan_type_category;
    public $principal_amount;
    public $total_interest;
    public $terms;
    public $term_type;
    public $year;
    public $month_day;
    public $payment_start_date;
    public $payment_end_date;
    public $purpose;
    public $total_installment_amount;
    public $remaining;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $loans_and_ledger_id;
    public $user_id;
    public $loans;
    public $loan_user_id;

    public $branch_references = [];
    public $loan_type_references = [];
    public $loans_and_ledger_references = [];
    public $loan_payments_references = [];
    public $loans_and_ledger_status = [];

    public $month_day_references = [];

    public function generateRefernenceNo()
    {
        $this->reference_number = "L-" . date("ymd") . "-" . rand(11111, 99999);
    }

    public function load()
    {
        $this->loanTypeReferences();
        $this->monthDayreferences();
    }

    public function branchReferences()
    {
        $this->branch_references = BranchReference::get();
    }

    public function monthDayreferences()
    {
        $this->month_day_references = [
            ['day' => 15, 'value' => '01-15', 'name' => 'January 15'],
            ['day' => 30, 'value' => '01-30', 'name' => 'January 30'],
            ['day' => 15, 'value' => '02-15', 'name' => 'February 15'],
            ['day' => 30, 'value' => '02-28', 'name' => 'February 28'],
            ['day' => 15, 'value' => '03-15', 'name' => 'March 15'],
            ['day' => 30, 'value' => '03-30', 'name' => 'March 30'],
            ['day' => 15, 'value' => '04-15', 'name' => 'April 15'],
            ['day' => 30, 'value' => '04-30', 'name' => 'April 30'],
            ['day' => 15, 'value' => '05-15', 'name' => 'May 15'],
            ['day' => 30, 'value' => '05-30', 'name' => 'May 30'],
            ['day' => 15, 'value' => '06-15', 'name' => 'June 15'],
            ['day' => 30, 'value' => '06-30', 'name' => 'June 30'],
            ['day' => 15, 'value' => '11-15', 'name' => 'July 15'],
            ['day' => 30, 'value' => '07-30', 'name' => 'July 30'],
            ['day' => 15, 'value' => '08-15', 'name' => 'August 15'],
            ['day' => 30, 'value' => '08-30', 'name' => 'August 30'],
            ['day' => 15, 'value' => '09-15', 'name' => 'September 15'],
            ['day' => 30, 'value' => '09-30', 'name' => 'September 30'],
            ['day' => 15, 'value' => '10-15', 'name' => 'October 15'],
            ['day' => 30, 'value' => '10-30', 'name' => 'October 30'],
            ['day' => 15, 'value' => '11-15', 'name' => 'November 15'],
            ['day' => 30, 'value' => '11-30', 'name' => 'November 30'],
            ['day' => 15, 'value' => '12-15', 'name' => 'December 15'],
            ['day' => 30, 'value' => '12-30', 'name' => 'December 30'],
        ];

        // $this->month_day_references = ($month_day_refs, function($v, $k) {
        //     return $k == 'day' && $v == 30;
        //   }, ARRAY_FILTER_USE_KEY);
    }

    public function loanTypeReferences()
    {
        $this->loan_type_references = LoanType::get();
    }

    protected function updatedType()
    {
        $this->checkLoanTypeCat = LoanType::where('id', $this->type)->first();

        $this->loan_type_category = $this->checkLoanTypeCat->loan_type_category ?? null;
    }

    public function loansAndLedgerReferences()
    {
        $this->loans_and_ledger_references = LoansAndLedger::get();
    }

    public function loansAndLedgerStatus()
    {
        $this->loans_and_ledger_status = LoansAndLedger::where('user_id', Auth::user()->id)->get();
    }

    protected function updatedTerms()
    {
        $this->payment_start_date = $this->year . '-' . $this->month_day;

        if ($this->term_type != '' && $this->terms != '' && $this->year != '' && $this->month_day != '') {
            if ($this->term_type == 1) {
                $days = ($this->terms * 15) - 15;
                $add_date = strtotime("+" . $days . "days", strtotime($this->payment_start_date));
            } else {
                $month = $this->terms - 1;
                $add_date = strtotime("+" . $month . "month", strtotime($this->payment_start_date));
            }
            $this->payment_end_date = date("Y-m-d", $add_date);
        }
    }

    protected function updatedTermType()
    {
        $this->payment_start_date = $this->year . '-' . $this->month_day;

        if ($this->term_type != '' && $this->terms != '' && $this->year != '' && $this->month_day != '') {
            if ($this->term_type == 1) {
                $days = ($this->terms * 15) - 15;
                $add_date = strtotime("+" . $days . "days", strtotime($this->payment_start_date));
            } else {
                $month = $this->terms - 1;
                $add_date = strtotime("+" . $month . "month", strtotime($this->payment_start_date));
            }
            $this->payment_end_date = date("Y-m-d", $add_date);
        }
    }

    protected function updatedMonthDay()
    {
        $this->payment_start_date = $this->year . '-' . $this->month_day;

        if ($this->term_type != '' && $this->terms != '' && $this->year != '' && $this->month_day != '') {
            if ($this->term_type == 1) {
                $days = ($this->terms * 15) - 15;
                $add_date = strtotime("+" . $days . "days", strtotime($this->payment_start_date));
            } else {
                $month = $this->terms - 1;
                $add_date = strtotime("+" . $month . "month", strtotime($this->payment_start_date));
            }
            $this->payment_end_date = date("Y-m-d", $add_date);
        }
    }

    // protected function updatedPaymentStartDate()
    // {
    //     if ($this->term_type != '' && $this->terms != '' && $this->payment_start_date != '') {
    //         if ($this->term_type == 1) {
    //             $days = ($this->terms * 15) - 15;
    //             $add_date = strtotime("+" . $days . "days", strtotime($this->payment_start_date));
    //         } else {
    //             $month = $this->terms - 1;
    //             $add_date = strtotime("+" . $month . "month", strtotime($this->payment_start_date));
    //         }
    //         $this->payment_end_date = date("Y-m-d", $add_date);
    //     }
    // }

    public function action(LoansAndLedgerInterface $loans_and_ledger_interface, $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'confirm_submit') {
            $response = $loans_and_ledger_interface->createValidation($this->getRequest());
            if ($response['code'] == 200) {
                $this->confirmation_message = "Are you sure you want to proceed with this request?";
                $this->confirmation_modal = true;
            } else if ($response['code'] == 400) {
                $this->resetErrorBag();
                foreach ($response['result']->getMessages() as $a => $messages) {
                    if (is_array($messages)) {
                        foreach ($messages as $message) :
                            $this->addError($a, $message);
                        endforeach;
                    } else {
                        $this->addError($a, $messages);
                    }
                }
                return;
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        } else if ($action_type == 'create_view_payment_schedule') {
            $this->create_view_payment_modal = true;
        }
    }

    public function getRequest()
    {
        return [
            'reference_number' => $this->reference_number,
            'type' => $this->type,
            'principal_amount' => $this->principal_amount,
            'total_interest' => $this->total_interest,
            'terms' => $this->terms,
            'term_type' => $this->term_type,
            'payment_start_date' => $this->payment_start_date,
            'payment_end_date' => $this->payment_end_date,
            'purpose' => $this->purpose,
            'total_installment_amount' => $this->total_installment_amount,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'reference_number',
            'type',
            'principal_amount',
            'total_interest',
            'terms',
            'term_type',
            'payment_start_date',
            'payment_end_date',
            'purpose',
            'total_installment_amount',
        ]);
    }

    public function getConfirmationRequest()
    {
        return [
            'decline_reason' => $this->decline_reason,
        ];
    }
}
