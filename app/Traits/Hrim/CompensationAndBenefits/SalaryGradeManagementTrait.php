<?php

namespace App\Traits\Hrim\CompensationAndBenefits;

// use App\Interfaces\Hrim\TalentManagement\CoreValueManagementInterface;

trait SalaryGradeManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $salary_grade_management_id;
    public $confirmation_message;
    public $confirmation_modal = false;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $grade;
    public $minimun_amount;
    public $maximum_amount;

    public function resetForm()
    {
        $this->reset([
            'grade',
            'minimun_amount',
            'maximum_amount',
        ]);

        $this->confirmation_modal = false;
    }

    public function getRequest()
    {
        return [
            'grade' => $this->grade,
            'minimun_amount' => $this->minimun_amount,
            'maximum_amount' => $this->maximum_amount,
        ];
    }
}
