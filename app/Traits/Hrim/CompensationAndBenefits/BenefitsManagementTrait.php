<?php

namespace App\Traits\Hrim\CompensationAndBenefits;

// use App\Interfaces\Hrim\TalentManagement\CoreValueManagementInterface;

trait BenefitsManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $benefits_management_id;
    public $confirmation_message;
    public $confirmation_modal = false;

    public $action_type;
    
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $benefit_type;
    public $description;
    public $created_by;

    public function resetForm()
    {
        $this->reset([
            'benefit_type',
            'description',
            'created_by',
        ]);

        $this->confirmation_modal = false;
    }

    public function getRequest()
    {
        return [
            'benefit_type' => $this->benefit_type,
            'description' => $this->description,
            'created_by' => $this->created_by,
        ];
    }
}
