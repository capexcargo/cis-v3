<?php

namespace App\Traits\Hrim\CompensationAndBenefits;

use App\Models\BranchReference;
use App\Models\Division;
use App\Models\Hrim\EmploymentCategory;
use App\Models\Hrim\JobLevel;
use App\Models\MonthReference;

trait PayslipsTrait
{
    public $view_modal = false;

    public $payout_year;
    public $payout_date;
    public $branch;
    public $employee_id;
    public $employee_name;
    public $employee_category;
    public $employee_code;
    public $division;
    public $job_rank;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;
    
    public $branch_references = [];
    public $employee_category_references = [];
    public $division_references = [];
    public $job_rank_references = [];
    public $month_references = [];

    public $year;
    public $month;
    public $cut_off;

    public function load()
    {
        $this->loadMonthReference();
        $this->employeeCategoryReferences();
    }

    public function loadMonthReference()
    {
        $this->month_references = MonthReference::get();
    }
    public function branchReferences()
    {
        $this->branch_references = BranchReference::get();
    }
    public function employeeCategoryReferences()
    {
        $this->employee_category_references = EmploymentCategory::get();
    }
    public function divisionReferences()
    {
        $this->division_references = Division::get();
    }
    public function jobRankReferences()
    {
        $this->job_rank_references = JobLevel::get();
    }

    public function getRequest()
    {
        return [
            'year' => $this->year,
            'month' => $this->month,
            'cut_off' => $this->cut_off,
            'branch' => $this->branch,
            'employee_id' => $this->employee_id,
            'employee_name' => $this->employee_name,
            'employee_category' => $this->employee_category,
            'employee_code' => $this->employee_code,
            'division' => $this->division,
            'job_rank' => $this->job_rank,
        ];
    }
}
