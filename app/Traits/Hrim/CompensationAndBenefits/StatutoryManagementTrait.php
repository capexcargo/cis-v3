<?php

namespace App\Traits\Hrim\CompensationAndBenefits;

// use App\Interfaces\Hrim\TalentManagement\CoreValueManagementInterface;

trait StatutoryManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $benefits_management_id;
    public $confirmation_message;
    public $confirmation_modal = false;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $statutor_benefit_type;
    public $description;
    public $created_by;

    public function resetForm()
    {
        $this->reset([
            'statutor_benefit_type',
            'description',
            'created_by',
        ]);

        $this->confirmation_modal = false;
    }

    public function getRequest()
    {
        return [
            'statutor_benefit_type' => $this->statutor_benefit_type,
            'description' => $this->description,
            'created_by' => $this->created_by,
        ];
    }
}
