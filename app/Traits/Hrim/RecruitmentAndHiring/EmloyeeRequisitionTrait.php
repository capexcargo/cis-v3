<?php

namespace App\Traits\Hrim\RecruitmentAndHiring;

use App\Models\BranchReference;
use App\Models\Division;
use App\Models\Hrim\EmploymentCategoryType;
use App\Models\Hrim\EmploymentStatus;
use App\Models\Hrim\JobLevel;
use App\Models\Hrim\LoaManagement;
use App\Models\Hrim\Position;
use App\Models\Hrim\RequestReason;
use App\Models\User;

trait EmloyeeRequisitionTrait
{
    public $confirmation_modal = false;
    public $confirmation_message;
    public $action_type;

    public $create_employee_requisition_modal = false;
    public $edit_employee_requisition_modal = false;
    public $view_employee_requisition_modal = false;

    public $erf_reference;
    public $branch;
    public $division;
    public $position;
    public $job_level;
    public $employment_cat;
    public $emp_cat_type;
    public $project_name;
    public $duration_from;
    public $duration_to;
    public $request_reason;
    public $target_hire_date;
    public $first_status;
    public $final_status;

    public $attachment;

    public $branch_reference = [];
    public $division_reference = [];
    public $position_reference = [];
    public $job_level_reference = [];
    public $employment_category_reference = [];
    public $employment_category_type_reference = [];
    public $request_reason_reference = [];

    public $approved_by_reference = [];
    public $approvedBy = [];

    public $approver = [];
    public $approvedBy1;
    public $approvedBy2;
    public $approvedBy3;
    // public $approved_by;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;

    public $employee_requisition;
    public $approvers;
    public $approver1;
    public $approver2;

    public $employee_reqs = [];
    public $employee_requisitions_count = [];
    public $header_cards = [];
    public $status_header_cards = [];

    public $employee_requisition_id;
    public $status;

    public $attachments = [];

    public function load()
    {
        $this->addAttachments();
        $this->branchReference();
        $this->divisionReference();
        $this->positionReference();
        $this->jobLevelReference();
        $this->employmentCategoryReference();
        $this->employmentCategoryTypeReference();
        $this->requestReasonReference();
    }

    public function addAttachments()
    {
        $this->attachments[] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }

    public function removeAttachments($i)
    {
        if (count(collect($this->attachments)->where('is_deleted', false)) > 1) {
            if ($this->attachments[$i]['id']) {
                $this->attachments[$i]['is_deleted'] = true;
            } else {
                unset($this->attachments[$i]);
            }

            array_values($this->attachments);
        }
    }

    public function generateErfReference()
    {
        $this->erf_reference = 'ERF-' . rand(111111, 99999);
    }

    public function branchReference()
    {
        $this->branch_reference = BranchReference::get();
    }

    public function divisionReference()
    {
        $this->division_reference = Division::get();
    }

    public function positionReference()
    {
        $this->position_reference = Position::where('division_id', $this->division)->orderBy('id', 'desc')->get();
    }

    public function jobLevelReference()
    {
        $this->job_level_reference = JobLevel::get();
    }

    public function employmentCategoryReference()
    {
        $this->employment_category_reference = EmploymentStatus::get();
        $this->employmentCategoryTypeReference();
    }

    public function employmentCategoryTypeReference()
    {
        $this->employment_category_type_reference = EmploymentCategoryType::get();
    }

    public function requestReasonReference()
    {
        $this->request_reason_reference = RequestReason::get();
    }

    public function approvedBy()
    {
        $this->approvedBy = LoaManagement::get();

        $this->approved_by_reference = User::get();
    }

    public function approvers()
    {
        $this->approver = LoaManagement::where('division_id', $this->division)->where('loa_type_id', 5)->first();
    }

    protected function updatedDivision()
    {
        $this->approvers();
        $this->positionReference();
    }

    public function action(array $data, $action_type)
    {
        if ($this->employment_cat == 2) {
            $validated = $this->validate([
                'erf_reference' => 'required',
                'branch' => 'required',
                'division' => 'required',
                'position' => 'required',
                'job_level' => 'required',
                'employment_cat' => 'required',
                'emp_cat_type' => 'required',
                'project_name' => 'required',
                'duration_from' => 'required',
                'duration_to' => 'required',
                'request_reason' => 'required',
                'target_hire_date' => 'required',

                'attachments' => 'required',
                'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),
            ], [
                'attachments.*.attachment.required' => 'This attachment field is required.',
                'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png,xlsx,doc,docx.',
            ]);
        } else {
            $validated = $this->validate([
                'erf_reference' => 'required',
                'branch' => 'required',
                'division' => 'required',
                'position' => 'required',
                'job_level' => 'required',
                'employment_cat' => 'required',
                'request_reason' => 'required',
                'target_hire_date' => 'required',

                'attachments' => 'required',
                'attachments.*.attachment' => 'required|' . config('filesystems.validation_all'),
            ], [
                'attachments.*.attachment.required' => 'This attachment field is required.',
                'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png,xlsx,doc,docx.',
            ]);
        }

        $this->action_type = $action_type;

        if ($validated && $this->action_type == 'submit') {
            $this->confirmation_modal = true;
        }
    }

    public function confirm()
    {
        if ($this->action_type == 'submit') {
            $this->confirmation_modal = false;
            $this->submit();
        }
    }

    public function resetForm()
    {
        $this->reset([
            'erf_reference',
            'branch',
            'division',
            'position',
            'job_level',
            'employment_cat',
            'emp_cat_type',
            'project_name',
            'duration_from',
            'duration_to',
            'request_reason',
            'target_hire_date',
            'attachments',
        ]);
    }
}
