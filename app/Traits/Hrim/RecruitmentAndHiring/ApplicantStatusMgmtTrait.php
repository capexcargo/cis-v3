<?php

namespace App\Traits\Hrim\RecruitmentAndHiring;

trait ApplicantStatusMgmtTrait
{
    public $confirmation_modal = false;
    public $confirmation_message;
    public $action_type;

    public $create_applicant_status_mgmt_modal = false;
    public $edit_applicant_status_mgmt_modal = false;
    public $delete_applicant_status_mgmt_modal;
    
    public $applicant_status;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;

    public $applicant_status_mgmt_id;

    public function resetForm()
    {
        $this->reset([
            'applicant_status',
        ]);
    }
}
