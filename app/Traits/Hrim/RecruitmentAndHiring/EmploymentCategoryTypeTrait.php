<?php

namespace App\Traits\Hrim\RecruitmentAndHiring;

use App\Models\Hrim\EmploymentCategory;

trait EmploymentCategoryTypeTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $view_modal = false;
    public $delete_modal = false;

    public $confirmation_message;
    public $confirmation_modal = false;
    
    public $category;
    public $category_type;

    public $category_references = [];

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;

    public $emp_cat_type_id;

    public function resetForm()
    {
        $this->reset([
            'category',
            'category_type',
        ]);
        $this->confirmation_modal = false;
    }

    public function categoryReferences()
    
    {
        $this->category_references = EmploymentCategory::get();
    }

    public function getRequest()
    {
        return [
            'category' =>$this->category,
            'category_type' =>$this->category_type,
        ];
    }
    
}

