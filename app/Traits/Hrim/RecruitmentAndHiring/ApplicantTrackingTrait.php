<?php

namespace App\Traits\Hrim\RecruitmentAndHiring;

use App\Models\BranchReference;
use App\Models\Division;
use App\Models\Hrim\ApplicantStatusMgmt;
use App\Models\Hrim\ApplicantTracking;
use App\Models\Hrim\EmployeeRequisition;
use App\Models\Hrim\Position;
use App\Models\User;
use PhpParser\Node\Expr\AssignOp\Concat;

trait ApplicantTrackingTrait
{
    public $confirmation_modal = false;
    public $confirmation_message;
    public $action_type;

    public $create_applicant_tracking_modal = false;
    public $edit_applicant_tracking_modal = false;
    public $view_applicant_tracking_modal = false;
    public $view_applicant_tracking_notes_modal = false;

    public $employee_reqs = [];
    public $employee_requisitions_count = [];
    public $header_cards = [];

    public $erf_reference;
    public $erf_reference_no_id;
    public $erf_references = [];
    public $erf_reference_search;

    public $first_name;
    public $middle_name;
    public $last_name;
    public $fullname;

    public $position;
    public $branch;
    public $date_applied;
    public $requesting_manager;
    public $requesting_division;
    public $hr_notes;
    public $hiring_manager_notes;
    public $gm_segs_notes;
    public $status_of_application;
    public $final_remarks;
    public $attachments = [];

    public $division;
    public $request_reason;

    public $applicant_details = [];
    public $position_reference = [];
    public $branch_reference = [];
    public $requesting_manager_reference = [];
    public $division_reference = [];
    public $applicant_status_reference = [];

    public $applicant_tracking;


    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;

    public $applicant_tracking_id;

    public function load()
    {
        $this->loadErfReferences();
        // $this->addAttachments();
    }

    public function addAttachments()
    {
        $this->attachments[] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }

    public function removeAttachments($i)
    {
        if (count(collect($this->attachments)->where('is_deleted', false)) > 1) {
            if ($this->attachments[$i]['id']) {
                $this->attachments[$i]['is_deleted'] = true;
            } else {
                unset($this->attachments[$i]);
            }

            array_values($this->attachments);
        }
    }

    public function generateErfReference()
    {
        $this->erf_reference_search = "ERF-" . rand(111111, 99999);
        $this->resetForm();
    }

    public function loadApplicantDetails()
    {
        $this->applicant_details = ApplicantTracking::get();
    }


    public function loadErfReferences()
    {
        $this->erf_references = EmployeeRequisition::where([
            ['erf_reference_no', 'like', '%' . $this->erf_reference_search . '%']
        ])
            ->where('first_status', '=', 3)
            ->orderBy('erf_reference_no', 'asc')
            ->take(10)
            ->get();
    }

    protected function updatedErfReferenceSearch()
    {
        $this->loadErfReferences();
    }

    public function getErfDetails($id)
    {
        $applicant_tracking = EmployeeRequisition::with('erfReference')->find($id);

        if ($applicant_tracking) {
            $this->erf_reference_search = $applicant_tracking->erf_reference_no;
            $this->erf_reference = $applicant_tracking->id;
            $this->position = $applicant_tracking->position_id;
            $this->branch = $applicant_tracking->branch_id;
            $this->requesting_manager = $applicant_tracking->created_by;
            $this->requesting_division = $applicant_tracking->division_id;
        }
    }

    public function positionReference()
    {
        $this->position_reference = Position::get();
    }

    public function branchReference()
    {
        $this->branch_reference = BranchReference::get();
    }

    public function requestingManagerReference()
    {
        $this->requesting_manager_reference = User::where('level_id', '>', 2)->get();
    }

    public function divisionReference()
    {
        $this->division_reference = Division::get();
    }


    public function applicantStatusReference()
    {
        $this->applicant_status_reference = ApplicantStatusMgmt::get();
    }

    public function action(array $data, $action_type)
    {
        $validated = $this->validate([
            'erf_reference' => 'required',
            'first_name' => 'required',
            'middle_name' => 'sometimes',
            'last_name' => 'required',
            'position' => 'required',
            'branch' => 'required',
            'date_applied' => 'required',
            'requesting_manager' => 'required',
            'requesting_division' => 'required',
            'hr_notes' => 'sometimes',
            'hiring_manager_notes' => 'sometimes',
            'gm_segs_notes' => 'sometimes',
            'status_of_application' => 'required',
            'final_remarks' => 'sometimes',
            
            'attachments' => 'required',
            'attachments.*.attachment' => 'required|mimes:jpg,jpeg,png' . config('filesystems.validation_all'),
        ], [
            'attachments.*.attachment.required' => 'This attachment field is required.',
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png',
        ]);

        $this->action_type = $action_type;

        if ($validated && $this->action_type == "submit") {
            $this->confirmation_modal = true;
        }
    }

    public function confirm()
    {
        if ($this->action_type == "submit") {
            $this->confirmation_modal = false;
            $this->submit();
        }
    }

    public function resetForm()
    {
        $this->reset([
            'erf_reference',
            'first_name',
            'middle_name',
            'last_name',
            'position',
            'branch',
            'date_applied',
            'requesting_manager',
            'requesting_division',
            'hr_notes',
            'hiring_manager_notes',
            'gm_segs_notes',
            'status_of_application',
            'final_remarks',
            'attachments',
        ]);
    }
}
