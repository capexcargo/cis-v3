<?php

namespace App\Traits\Hrim\RecruitmentAndHiring;

trait OnboardingStatusMgmtTrait
{
    public $confirmation_modal = false;
    public $confirmation_message;
    public $action_type;

    public $create_onboarding_status_mgmt_modal = false;
    public $edit_onboarding_status_mgmt_modal = false;
    public $delete_onboarding_status_mgmt_modal;
    
    public $onboarding_status;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $description;
    public $date_created;

    public $onboarding_status_mgmt_id;

    public function resetForm()
    {
        $this->reset([
            'onboarding_status',
            'description',
        ]);
    }
}
