<?php

namespace App\Traits\Hrim\RecruitmentAndHiring;

use App\Models\Hrim\OnboardingStatusMgmt;

trait OnboardingTrait
{
    public $onboarding;
    public $edit_onboarding_modal = false;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;

    public $fullname;
    
    public $erf_reference_no_id;
    public $onboarding_status;
    public $onboarding_status_headers;
    public $final_remarks;

    public $onboarding_status_reference = [];
    public $header_cards = [];
    public $status_header_cards = [];

    public $onboarding_id;

    public function onboardingStatusReference()
    {
        $this->onboarding_status_reference = OnboardingStatusMgmt::get();
    }

    public function resetForm()
    {
        $this->reset([
            'onboarding_status',
        ]);
    }
}
