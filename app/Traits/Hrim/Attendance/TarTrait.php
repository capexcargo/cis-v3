<?php

namespace App\Traits\Hrim\Attendance;

use App\Models\Hrim\TarReasonReference;

trait TarTrait
{
    public $date;
    public $request_time_in;
    public $request_time_out;
    public $reason_of_adjustment;

    public $user_id;
    public $first_approver_id;
    public $second_approver_id;
    public $third_approver_id;
    public $is_first_approved;
    public $is_second_approved;
    public $is_third_approved;
    public $first_approver_remarks;
    public $second_approver_remarks;
    public $third_approver_remarks;

    public $tar_reason_references = [];

    public $tar;

    public function load()
    {
        $this->loadTarReasionReference();
    }

    public function loadTarReasionReference()
    {
        $this->tar_reason_references = TarReasonReference::get();
    }

    public function getRequest()
    {
        return [
            'user_id' => $this->user_id,
            'first_approver_id' => $this->first_approver_id,
            'second_approver_id' => $this->second_approver_id,
            'third_approver_id' => $this->third_approver_id,
            'is_first_approved' => $this->is_first_approved,
            'is_second_approved' => $this->is_second_approved,
            'is_third_approved' => $this->is_third_approved,
            'first_approver_remarks' => $this->first_approver_remarks,
            'second_approver_remarks' => $this->second_approver_remarks,
            'third_approver_remarks' => $this->third_approver_remarks,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'date',
            'request_time_in',
            'request_time_out',
            'reason_of_adjustment',

            'first_approver_id',
            'second_approver_id',
            'third_approver_id',
            'is_first_approved',
            'is_second_approved',
            'is_third_approved',
            'first_approver_remarks',
            'second_approver_remarks',
            'third_approver_remarks',
        ]);
    }
}
