<?php

namespace App\Traits\Hrim\Attendance;

use App\Models\Hrim\LeaveDayTypeReference;
use App\Models\Hrim\LeaveTypeReference;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait LeaveRecordsTrait
{
    public $inclusive_date_from;
    public $inclusive_date_to;
    public $resume_of_work;
    public $type_of_leave;
    public $is_with_medical_certificate;
    public $apply_for;
    public $is_with_pay;
    public $reason;
    public $reliever;
    public $attachment = '';

    public $user_id;
    public $first_approver_id;
    public $second_approver_id;
    public $third_approver_id;
    public $is_first_approved;
    public $is_second_approved;
    public $is_third_approved;
    public $first_approver_remarks;
    public $second_approver_remarks;
    public $third_approver_remarks;

    public $leave_type_references = [];
    public $leave_day_type_references = [];
    public $reliever_references = [];

    public $leave;

    public function load()
    {
        $this->loadLeaveTypeReference();
        $this->loadLeaveDayTypeReference();
        $this->loadRelieverReference();
    }

    public function loadLeaveTypeReference()
    {
        $this->leave_type_references = LeaveTypeReference::get();
    }

    public function loadLeaveDayTypeReference()
    {
        $this->leave_day_type_references = LeaveDayTypeReference::get();
    }

    public function loadRelieverReference()
    {
        $this->reliever_references = User::where('division_id', Auth::user()->division_id)->get();
    }

    protected function updatedTypeOfLeave()
    {
        if ($this->type_of_leave != 1) {
            $this->reset('is_with_medical_certificate');
        }
    }

    public function getRequest()
    {
        return [
            'user_id' => $this->user_id,
            'first_approver_id' => $this->first_approver_id,
            'second_approver_id' => $this->second_approver_id,
            'third_approver_id' => $this->third_approver_id,
            'is_first_approved' => $this->is_first_approved,
            'is_second_approved' => $this->is_second_approved,
            'is_third_approved' => $this->is_third_approved,
            'first_approver_remarks' => $this->first_approver_remarks,
            'second_approver_remarks' => $this->second_approver_remarks,
            'third_approver_remarks' => $this->third_approver_remarks,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'first_approver_id',
            'second_approver_id',
            'third_approver_id',
            'is_first_approved',
            'is_second_approved',
            'is_third_approved',
            'first_approver_remarks',
            'second_approver_remarks',
            'third_approver_remarks',
        ]);
    }
}
