<?php

namespace App\Traits\Hrim\Attendance;

use App\Models\Hrim\DateCategoryReference;
use App\Models\MonthReference;
use App\Repositories\Hrim\EmployeeAttendance\OvertimeRecordsRepository;

trait OvertimeRecordsTrait
{
    public $month;
    public $cut_off;
    public $year;

    public $time_log;
    public $date_time_in;
    public $date_time_out;
    public $time_in;
    public $time_out;
    public $work_schedule;
    public $rendered_ot_hours;

    public $type_of_ot;
    public $is_have_ot_break = true;
    public $actual_date_from;
    public $actual_date_to;
    public $from;
    public $to;
    public $overtime_request;
    public $reason;

    public $user_id;
    public $approved_overtime_hours;
    public $first_approver_id;
    public $second_approver_id;
    public $third_approver_id;
    public $is_first_approved;
    public $is_second_approved;
    public $is_third_approved;
    public $first_approver_remarks;
    public $second_approver_remarks;
    public $third_approver_remarks;

    public $month_references = [];
    public $overtimed_date_references = [];
    public $date_category_references = [];

    public $overtime_record;

    public function load()
    {
        $this->loadMonthReference();
        $this->loadDateCategoryReference();
    }

    public function loadMonthReference()
    {
        $this->month_references = MonthReference::get();
    }

    public function loadDateCategoryReference()
    {
        $this->date_category_references = DateCategoryReference::get();
    }

    protected function updatedMonth()
    {
        $this->getOvertimed();
    }

    protected function updatedCutOff()
    {
        $this->getOvertimed();
    }

    protected function updatedYear()
    {
        $this->getOvertimed();
    }

    protected function updatedDateTimeIn()
    {
        $overtime_records_repository = new OvertimeRecordsRepository();
        $request = [
            'user_id' => $this->user_id,
            'date' => $this->date_time_in,
        ];
        $response = $overtime_records_repository->showDate($request);
        if ($response['code'] != 200) {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }

        $this->time_log = $response['result'];
        $this->date_time_out = $this->time_log->time_out_date;
        $this->time_in = date('H:i', strtotime($this->time_log->time_in));
        $this->time_out = date('H:i', strtotime($this->time_log->time_out));
        $this->work_schedule = $this->time_log->workSchedule->name;
        $this->rendered_ot_hours = $this->time_log->computed_ot;

        if (!$this->overtime_record) {
            $this->type_of_ot = 4;
            $this->actual_date_from = $this->time_log->date;
            $this->actual_date_to = $this->time_log->time_out_date;
            $this->from = date('H:i', strtotime($this->time_log->workSchedule->time_to));
            $this->to = date('H:i', strtotime($this->time_log->time_out));
            $this->overtime_request = $this->time_log->computed_ot;
        }
    }

    protected function updatedActualDateTo()
    {
        $this->getComputedOvertime();
    }

    protected function updatedTo()
    {
        $this->getComputedOvertime();
    }

    public function getComputedOvertime()
    {
        $overtime_records_repository = new OvertimeRecordsRepository();
        $response = $overtime_records_repository->getComputedOvertime($this->getRequest());
        if ($response['code'] != 200) {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }

        $this->overtime_request = $response['result'];
    }

    public function getOvertimed()
    {
        $overtime_records_repository = new OvertimeRecordsRepository();
        $response = $overtime_records_repository->getOvertimed($this->getRequest());
        if ($response['code'] != 200) {
            $this->sweetAlertError('error', $response['message'], $response['result']);
        }

        $this->overtimed_date_references = $response['result'];
        $this->resetForm();
    }

    public function getRequest()
    {
        return [
            'month' => $this->month,
            'cut_off' => $this->cut_off,
            'year' => $this->year,

            'time_log' => $this->time_log,
            'date_time_in' => $this->date_time_in,
            'date_time_out' => $this->date_time_out,
            'time_in' => $this->time_in,
            'time_out' => $this->time_out,
            'work_schedule' => $this->work_schedule,
            'rendered_ot_hours' => $this->rendered_ot_hours,

            'type_of_ot' => $this->type_of_ot,
            'is_have_ot_break' => $this->is_have_ot_break,
            'actual_date_from' => $this->actual_date_from,
            'actual_date_to' => $this->actual_date_to,
            'from' => $this->from,
            'to' => $this->to,
            'overtime_request' => $this->overtime_request,
            'reason' => $this->reason,
            
            'user_id' => $this->user_id,
            'approved_overtime_hours' => $this->approved_overtime_hours,
            'first_approver_id' => $this->first_approver_id,
            'second_approver_id' => $this->second_approver_id,
            'third_approver_id' => $this->third_approver_id,
            'is_first_approved' => $this->is_first_approved,
            'is_second_approved' => $this->is_second_approved,
            'is_third_approved' => $this->is_third_approved,
            'first_approver_remarks' => $this->first_approver_remarks,
            'second_approver_remarks' => $this->second_approver_remarks,
            'third_approver_remarks' => $this->third_approver_remarks,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'approved_overtime_hours',
            'first_approver_id',
            'second_approver_id',
            'third_approver_id',
            'is_first_approved',
            'is_second_approved',
            'is_third_approved',
            'first_approver_remarks',
            'second_approver_remarks',
            'third_approver_remarks',
        ]);
    }
}
