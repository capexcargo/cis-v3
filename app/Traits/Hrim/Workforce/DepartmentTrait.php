<?php

namespace App\Traits\Hrim\Workforce;

use App\Models\Division;

trait DepartmentTrait
{
    public $action_type;
    public $confirmation_message;

    public $create_department_modal = false;
    public $edit_department_modal = false;
    public $delete_department_modal = false;
    public $view_department_modal = false;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;

    public $code;
    public $department;
    public $division;

    public $division_references = [];

    public $department_id;

    public function divisionReferences()
    {
        $this->division_references = Division::get();
    }

    public function resetForm()
    {
        $this->reset([
            // 'code',
            'display',
            'department',
        ]);
    }
}
