<?php

namespace App\Traits\Hrim\Workforce;

use App\Interfaces\Hrim\Workforce\TeamsInterface;
use App\Models\Division;
use App\Models\Hrim\Department;
use App\Models\Hrim\Position;
use App\Models\Hrim\SectionReference;
use App\Repositories\Hrim\Workforce\TeamsRepository;

trait TeamsTrait
{
    public $confirmation_modal = false;
    public $confirmation_message;
    public $index;

    public $division;
    public $teams = [];

    public $division_references = [];
    public $position_references = [];
    public $department_references = [];
    public $section_references = [];

    public $division_model;

    public function load()
    {
        $this->add();
        $this->loadDivisionReferences();
        $this->loadDepartmentReferences();
        $this->loadSectionReferences();
    }

    public function loadDivisionReferences()
    {
        $this->division_references = Division::get();
    }

    public function loadPositionReferences()
    {
        $this->position_references = Position::where('division_id', $this->division)->get();
    }

    public function loadDepartmentReferences()
    {
        $this->department_references = Department::get();
    }

    public function loadSectionReferences()
    {
        $this->section_references = SectionReference::get();
    }

    public function updatedDivision()
    {
        $this->loadPositionReferences();
        $teams_repository = new TeamsRepository();
        $response = $teams_repository->show($this->division);
        if ($response['code'] == 200) {
            $division = $response['result'];
            $this->reset('teams');
            foreach ($division->teams as $team) {
                $this->teams[] = [
                    'id' => $team->id,
                    'role' => $team->position_id,
                    'reports_to' => $team->reports_to,
                    'department' => $team->department_id,
                    'section' => $team->section_id,
                    'is_deleted' => false,
                ];
            }
        } else {
            $this->add();
        }
    }

    public function action($data, $action_type)
    {
        if ($action_type == 'remove') {
            $this->index = $data['index'];
            $this->confirmation_message = "Are you sure you want to remove this row.";
            $this->confirmation_modal = true;
        }
    }

    public function add()
    {
        $this->teams[] = [
            'id' => null,
            'role' => null,
            'reports_to' => null,
            'department' => null,
            'section' => null,
            'is_deleted' => false,
        ];
    }

    public function confirm()
    {
        if (count(collect($this->teams)->where('is_deleted', false)) > 1) {
            if ($this->teams[$this->index]['id']) {
                $this->teams[$this->index]['is_deleted'] = true;
            } else {
                unset($this->teams[$this->index]);
            }
            array_values($this->teams);
        }

        $this->confirmation_modal = false;
    }

    public function getRequest()
    {
        return [
            'division' => $this->division,
            'teams' => $this->teams,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'division',
            'teams'
        ]);
    }
}
