<?php

namespace App\Traits\Hrim\Workforce;

trait JobLevelTrait
{

    // public $code;
    public $display;
    // public $is_visible;
    // public $posted_by;


    public function resetForm()
    {
        $this->reset([
            // 'code',
            'display',
            // 'is_visible',
            // 'posted_by',
        ]);
    }
}
