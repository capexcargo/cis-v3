<?php

namespace App\Traits\Hrim\Workforce;

use App\Models\Hrim\JobLevel;
use App\Models\Hrim\Department;
use App\Models\Division;

trait PositionTrait
{
    public $position;
    public $display;
    public $job_level;
    public $department;
    public $division;
    public $job_overview;

    public $job_levels = [];
    public $departments = [];
    public $divisions = [];

    public $responsibilities = [];
    public $requirements = [];

    public function addResponsibilities()
    {
        $this->responsibilities[] = [
            'id' => null,
            'duties' => null,
            'position_id' => null,
            'created_by' => null,
            'is_deleted' => false,
        ];
        // dd('addaad000');
    }

    public function removeResponsibilities($i)
    {
        if (count(collect($this->responsibilities)->where('is_deleted', false)) > 1) {
            if ($this->responsibilities[$i]['id']) {
                $this->responsibilities[$i]['is_deleted'] = true;
            } else {
                unset($this->responsibilities[$i]);
            }

            array_values($this->responsibilities);
        }
    }


    public function addRequirements()
    {
        $this->requirements[] = [
            'id' => null,
            'code' => null,
            'position_id' => null,
            'created_by' => null,
            'is_deleted' => false,
        ];
        // dd('addaad000');
    }

    public function removeRequirements($i)
    {
        if (count(collect($this->requirements)->where('is_deleted', false)) > 1) {
            if ($this->requirements[$i]['id']) {
                $this->requirements[$i]['is_deleted'] = true;
            } else {
                unset($this->requirements[$i]);
            }

            array_values($this->requirements);
        }
    }

    public function loadJobLevel()
    {
        $this->job_levels = JobLevel::get();
    }

    public function loadDepartment()
    {
        $this->departments = Department::get();
    }

    public function loadDivision()
    {
        $this->divisions = Division::get();
    }


    public function resetForm()
    {
        $this->reset([
            "position",
            "division",
            "department",
            "job_level",
            "job_overview",
            "responsibilities",
            "requirements",
        ]);
    }
}
