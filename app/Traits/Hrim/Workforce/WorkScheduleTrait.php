<?php

namespace App\Traits\Hrim\Workforce;

use App\Models\Hrim\WorkModeReference;

trait WorkScheduleTrait
{
    public $perview_summary_modal = false;

    public $work_schedule;
    public $work_mode;
    public $shift;
    public $inclusive_days = [];
    public $time_from;
    public $time_to;
    public $break_from;
    public $break_to;
    public $ot_break_from;
    public $ot_break_to;

    public $work_mode_references = [];
    public $work_schedule_model;

    public function load()
    {
        $this->loadWorkModeReference();
    }

    public function loadWorkModeReference()
    {
        $this->work_mode_references = WorkModeReference::get();
    }

    public function getRequest()
    {
        return [
            'work_schedule' => $this->work_schedule,
            'work_mode' => $this->work_mode,
            'shift' => $this->shift,
            'inclusive_days' => $this->inclusive_days,
            'time_from' => $this->time_from,
            'time_to' => $this->time_to,
            'break_from' => $this->break_from,
            'break_to' => $this->break_to,
            'ot_break_from' => $this->ot_break_from,
            'ot_break_to' => $this->ot_break_to,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'work_schedule',
            'work_mode',
            'shift',
            'inclusive_days',
            'time_from',
            'time_to',
            'break_from',
            'break_to',
            'ot_break_from',
            'ot_break_to',
        ]);
    }
}
