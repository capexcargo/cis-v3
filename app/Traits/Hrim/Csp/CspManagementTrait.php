<?php

namespace App\Traits\Hrim\Csp;

use App\Models\BranchReference;

trait CspManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $activate_modal = false;
    public $deactivate_modal = false;

    public $confirmation_modal = false;
    public $confirmation_message;
    public $branch_reference = [];
    public $csp_management_id;

    public $action_type;

    public $is_deactivated = false;
    
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $name;
    public $branch_id;

    public function resetForm()
    {
        $this->reset([
            'name',
            'branch_id',
        ]);

        $this->confirmation_modal = false;
    }
    public function branchReference()
    {
        $this->branch_reference = BranchReference::get();
    }
    public function getRequest()
    {
        return [
            'name' => $this->name,
            'branch_id' => $this->branch_id,
        ];
    }
}
