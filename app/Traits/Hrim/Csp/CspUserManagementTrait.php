<?php

namespace App\Traits\Hrim\Csp;

use App\Models\BranchReference;
use App\Models\Hrim\Agencies;

trait CspUserManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $activate_modal = false;
    public $deactivate_modal = false;

    public $confirmation_message;
    public $confirmation_modal = false;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;
    public $csp_user_management_id;
    
    public $action_type;
    public $is_deactivated = false;

    public $branch_reference = [];
    public $agency_reference = [];

    public $agency_id;
    public $branch_id;
    public $agency_contact_person;
    public $agency_contact_no;
    public $agency_position;
    public $personal_email;
    public $password;
    public $password_confirmation;

    public function agencyReference()
    {
        $this->agency_reference = Agencies::get();
    }

    public function branchReference()
    {
        $this->branch_reference = BranchReference::with('areaId')->get();
        // $this->branch_reference = BranchReference::with('areaId')->whereNotNull('area_id')->get();
    }

    public function resetForm()
    {
        $this->reset([

            'agency_id',
            'branch_id',
            'agency_contact_person',
            'agency_contact_no',
            'agency_position',
            'personal_email',
            'password',
            'password_confirmation',
        ]);

        $this->confirmation_modal = false;
    }

    public function getRequest()
    {
        return [
            'agency_id' => $this->agency_id,
            'branch_id' => $this->branch_id,
            'agency_contact_person' => $this->agency_contact_person,
            'agency_contact_no' => $this->agency_contact_no,
            'agency_position' => $this->agency_position,
            'personal_email' => $this->personal_email,
            'password' => $this->password,
            'password_confirmation' => $this->password_confirmation,
        ];
    }
}
