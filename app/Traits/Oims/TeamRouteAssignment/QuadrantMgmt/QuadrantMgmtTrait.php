<?php

namespace App\Traits\Oims\TeamRouteAssignment\QuadrantMgmt;

use App\Models\Crm\BarangayReference;
use App\Models\OimsAreaReference;
use App\Models\OimsZipcodeReference;

trait QuadrantMgmtTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $date_created;
    public $action_type;

    public $create_modal = false;
    public $edit_modal = false;
    public $confirmation_modal;
    public $reactivate_modal = false;
    public $deactivate_modal = false;

    public $name;
    public $area;
    public $quadrant_id;
    public $search_request;

    public $areaReferences = [];

    public $areatags = [];


    public function load()
    {
        $this->areaReference();
    }

    public function areaReference()
    {
        $this->areaReferences = OimsAreaReference::get();
    }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToTeamRoute') {
            return redirect()->to(route('oims.order-management.team-route-assignment.index'));
        }
    }

    public function getRequest()
    {
        return [
            'name' => $this->name,
            'areatags' => $this->areatags,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'name',
            'areatags',
        ]);

        $this->confirmation_modal = false;
    }
}
