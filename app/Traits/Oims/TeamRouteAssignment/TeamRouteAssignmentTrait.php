<?php

namespace App\Traits\Oims\TeamRouteAssignment;

use App\Models\OimsBranchReference;
use App\Models\OimsTeamReference;
use App\Models\OimsVehicle;
use App\Models\RouteCategoryReference;
use App\Models\User;
use App\Models\UserDetails;

trait TeamRouteAssignmentTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $create_modal = false;
    public $print_modal = false;
    public $share_modal = false;
    public $edit_modal = false;
    public $reactivate_modal = false;
    public $deactivate_modal = false;
    public $confirmation_modal;
    public $display;
    public $date_created;
    public $action_type;
    public $search_request;

    public $date_created_search;

    public $severity_references = [];
    public $channel_references = [];

    public $datefrom;
    public $dateto;

    public $tra;
    public $branch;
    public $dis_date;

    public $route;

    public $team_id;
    public $teams = [];

    public $teamBranchReferences = [];
    public $teamReferences = [];
    public $plateReferences = [];
    public $routeReferences = [];
    public $driverReferences = [];
    public $checkerReferences = [];


    public function load()
    {
        $this->teamBranchReference();
        $this->teamReference();
        $this->plateReference();
        $this->routeReference();
        $this->driverReference();
        $this->checkerReference();
    }


    // function updatedTeams()
    // {
    //     // dd($this->teams);
    //     $asdasd = [];
    //     $asdasds = [];
    //     $aaas = [];
    //     $s = 0;
    //     foreach ($this->teams as $i => $team) {
    //         $this->teams[$i]['team_name'] = $this->teams[$i]['team_name'];



    //         $asdasd[] = $team['team_name'];

    //         $teamReferencess = OimsTeamReference::where('status', 1)->where('id', $this->teams[$i]['team_name'])->first();

    //         if (isset($this->teams[$i]['team_name'])) {
    //             $this->teams[$i]['team_name_display'] = $teamReferencess->id;
    //             $this->teams[$i]['team_name_displaysss'] = false;
    //         }

    //         foreach ($asdasd as $a => $aaa) {
    //             if ($this->teams[$i]['team_name'] != $asdasd[$a]) {
    //                 $aaas[] = $asdasd[$a];
    //             }
    //         }
    //     }

    //     $this->teamReferences = OimsTeamReference::where('status', 1)->whereNotIn('id', $asdasd)->get();
    //     // dd($aaas,$this->teams);
    //     if (isset($this->teams)) {
    //         $this->teamReference();
    //     }

    //     // dd($this->teams);
    // }
    public function teamBranchReference()
    {
        $this->teamBranchReferences = OimsBranchReference::get();
    }

    public function teamReference()
    {


        // foreach ($this->teams as $i => $team) {
        //     $asdasd[] = $team['team_name'];
        //     // $this->teams[$i]['team_name'] = $team['team_name'];\\

        //     $aaas = [];
        //     foreach ($asdasd as $a => $aaa) {
        //         if ($asdasd[$a] != null) {
        //             $aaas[] = $asdasd[$a];
        //             // $aaas[] = $asdasd[$a];
        //         }
        //     }
        //     // $this->teamReferences = OimsTeamReference::where('status', 1)->whereNotIn('id', $aaas)->get();

        // }
        // $this->teamReferences = OimsTeamReference::where('status', 1)->whereNotIn('id', $asdasd)->get();

        // if (isset($this->teams)) {
            $this->teamReferences = OimsTeamReference::where('status', 1)->get();
        // }


        // dd($this->teams,$aaas);

    }


    public function plateReference()
    {
        // foreach ($this->teams as $a => $team) {
        // dd($this->teams[$a]['plate']);
        // $this->plate_ids[$a] = $this->teams[$a]['plate'];
        // // dd(count($this->teams));
        // if (!isset($this->plate_ids)) {
        $this->plateReferences = OimsVehicle::get();
        // return;
        //     } else {
        //         $this->plateReferences = OimsVehicle::whereNotIn('id', $this->plate_ids)->get();
        //     }
        // }


        // foreach ($this->teams as $a => $team) {
        //     // dd($this->teams[$a]['plate']);
        //     $this->plate_ids[$a] = $this->teams[$a]['plate'];
        // }
        // // dd($this->plate_ids);
        // // dd(count($this->plate_ids));
        // if (count($this->plate_ids) == 0) {
        //     $this->plateReferences = OimsVehicle::whereNotIn('id', $this->plate_ids)->get();

        // dd($this->plateReferences);
        //     // dd('asdasd');
        //     $this->plateReferences = OimsVehicle::get();
    }

    public function routeReference()
    {
        $this->routeReferences = RouteCategoryReference::get();
    }

    public function driverReference()
    {
        $this->driverReferences = UserDetails::where('division_id', 7)->get();
    }

    public function checkerReference()
    {
        $this->checkerReferences = UserDetails::where('division_id', 7)->get();
    }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToTeam') {
            return redirect()->to(route('oims.order-management.team-route-assignment.team-mgmt.index'));
        } else if ($action_type == 'redirectToQuadrant') {
            return redirect()->to(route('oims.order-management.team-route-assignment.quadrant-mgmt.index'));
        } else if ($action_type == 'redirectToArea') {
            return redirect()->to(route('oims.order-management.team-route-assignment.area-mgmt.index'));
        } else if ($action_type == 'redirectToRoute') {
            return redirect()->to(route('oims.order-management.team-route-assignment.route-category.index'));
        }
    }

    public function getRequest()
    {
        return [
            'tra' => $this->tra,
            'branch' => $this->branch,
            'dis_date' => $this->dis_date,
            'teams' => $this->teams,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'tra',
            'branch',
            'dis_date',
            'teams',
        ]);

        $this->confirmation_modal = false;
    }
}
