<?php

namespace App\Traits\Oims\TeamRouteAssignment\TeamMgmt;

use App\Models\Crm\BarangayReference;
use App\Models\Crm\BranchReferencesQuadrant;
use App\Models\OimsAreaReference;
use App\Models\OimsZipcodeReference;

trait TeamMgmtTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $date_created;
    public $action_type;

    public $create_modal = false;
    public $edit_modal = false;
    public $confirmation_modal;
    public $reactivate_modal = false;
    public $deactivate_modal = false;

    public $name;
    public $area;
    public $quad;
    public $team_id;
    public $search_request;

    public $quadrantReferences = [];

    
    public $getAreas = [];



    public function load()
    {
        $this->quadrantReference();
    }

    public function quadrantReference()
    {
        $this->quadrantReferences = BranchReferencesQuadrant::get();
    }

    public function updatedQuad(){

        $this->getAreas = OimsAreaReference::where('quadrant_id', $this->quad)->get();
        // dd($this->getAreas);
        // dd($this->quad);
        // $this->area = $getAreas->name
        // if(){

        // }
    }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToTeamRoute') {
            return redirect()->to(route('oims.order-management.team-route-assignment.index'));
        }
    }

    public function getRequest()
    {
        return [
            'name' => $this->name,
            'quad' => $this->quad,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'name',
            'quad',
        ]);

        $this->confirmation_modal = false;
    }
}
