<?php

namespace App\Traits\Oims\TeamRouteAssignment\RouteCategoryMgmt;

trait RouteCategoryMgmtTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $date_created;
    public $action_type;

    public $create_modal = false;
    public $edit_modal = false;
    public $confirmation_modal;
    public $reactivate_modal = false;
    public $deactivate_modal = false;

    public $name;
    public $rcm_id;

    public function getRequest()
    {
        return [
            'name' => $this->name,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'name',
        ]);

        $this->confirmation_modal = false;
    }
    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToTeamRoute') {
            return redirect()->to(route('oims.order-management.team-route-assignment.index'));
        } 
    //     else if ($action_type == 'redirectTobookhis') {
    //         return redirect()->to(route('crm.sales.booking-mgmt.booking-history.index2'));
    //     }
    }
}
