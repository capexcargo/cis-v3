<?php

namespace App\Traits\Oims\OrderManagement\DispatchConsole;

trait DispatchConsoleTrait
{
    public $actStatusHead;
    public $actstat;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $create_modal = false;
    public $dispatch_modal = false;
    public $confirmation_modal = false;
    public $confirmsub_modal = false;
    public $workinst_modal = false;
    public $no_workinst_modal = false;
    public $bookworkins = [];
    public $work_ins;
    public $work_ins_id;

    public $teamname;
    public $routec;
    public $plate;
    public $driver;
    public $checker1;
    public $asdasdasd = [];
    public $checker2;
    
    public $date_created;
    public $action_type;
    public $selCount;
    public $bookingCount;

    public $status_header_cards = [];

    public $search_request = [];
    public $stats;

    public $getbranches = [];
    public $isShow = 2;

    public $branch_id;
    public $brnch;
    public $tn;
    public $rdis;
    public $sel;

    public $selectAll = false;
    public $selecteds = [];
    public $lists = [];

    public $booking_reference_no;
    public $shipper_id;
    public $booking_branch_id;
    public $bid;

    public $customer_no;
    public $IDXpickupwalk;
    public $IDXsinglemulti;

    public $IDXbookingrefno;
    public $IDXbookingstat;
    public $IDXfullname;
    public $IDXfname;

    public $IDXcompany;
    public $IDXmobile_nos;
    public $IDXemail_address;
    public $IDXaddress;

    public $IDXcustomertype;
    public $IDXacttype;

    public $IDXdecval;
    public $IDXtransportmode;
    public $IDXservmode;
    public $IDXdescgoods;
    public $IDXmodeofp;
    public $IDXref;
    public $IDXwcount;
    public $IDXworkins;
    public $IDXintrem;

    public $IDXcbId;

    public $IDXtimeslot;

    public $vconsignee = [];
    public $IDXconsid = [];
    public $bookref_modal = false;

    public $bstatrefno;
    public $bstatstat;
    public $bstatcreated;
    public $bsl_modal = false;

    public $VBCD = [];
    public $bookcons = [];
    public $consigneedet_modal = false;




    // public function action(array $data, $action_type)
    // {
    //     if ($action_type == "activities") {
    //         $this->isShow = 1;
    //     }
    //     if ($action_type == "teams") {
    //         $this->isShow = 2;
    //     }
      

    // }


    public function getRequest()
    {
        return [
            'rdis' => $this->rdis,
            'selecteds' => $this->selecteds,
            'branch_id' => $this->branch_id,
            'actStatusHead' => $this->actStatusHead,
           
        ];
    }
    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'rdis',
            'branch_id',
            'selecteds',
            'actStatusHead',
        ]);

        // $this->confirmation_modal = false;
    }

    // public function redirectTo(array $data, $action_type)
    // {
    //     $this->action_type = $action_type;

    //     if ($action_type == 'redirectToActivities') {
    //         return redirect()->to(route('crm.activities.activities.index'));
    //     } 
    // //     else if ($action_type == 'redirectTobookhis') {
    // //         return redirect()->to(route('crm.sales.booking-mgmt.booking-history.index2'));
    // //     }
    // }
}
