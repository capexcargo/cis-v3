<?php

namespace App\Traits\Oims\OrderManagement\EDtr;

use App\Models\OimsBranchReference;
use App\Models\OimsTeamReference;
use App\Models\OimsTeamRouteAssignmentDetails;
use App\Models\OimsVehicle;
use App\Models\User;
use App\Models\UserDetails;

trait EDtrTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $create_modal = false;
    public $edtrr_modal = false;
    public $pua_modal = false;
    public $da_modal = false;
    public $ac_modal = false;
    public $pv_modal = false;
    public $date_created; 
    public $action_type;

    public $search_request;

    public $rmrefno;
    public $rmcreated;
    public $rmdisp;
    public $rmbrnch;
    public $rmplate;
    public $rmteam;
    public $rmdrv;
    public $rmchk1;
    public $rmchk2;
    public $latestPickup;


    // public $edtr_lists = [];

    public $isShow = 2;


    public function action(array $data, $action_type)
    {
        // dd($data, $action_type);

        if ($action_type == "activities") {
            $this->isShow = 1;
        }
        if ($action_type == "teams") {
            $this->isShow = 2;
        }
        // if ($action_type == 'view') {
        //     $this->emit('request_management_view_mount', $data['parent_reference_no']);
        //     $this->view_modal = true;
        // }
    }

    public $branchReferences = [];
    public $teamReferences = [];
    public $driverReferences = [];
    public $checkerReferences = [];
    public $plateReferences = [];

    public function branchReference()
    {
        $this->branchReferences = OimsBranchReference::get();
    }

    public function teamReference()
    {
        $this->teamReferences = OimsTeamReference::get();
    }

    public function driverReference()
    {
        $this->driverReferences = UserDetails::where('position_id', 25)->get();
    }

    public function checkerReference()
    {
        $this->checkerReferences = UserDetails::where('position_id', 26)->get();
    }

    public function plateReference()
    {
        $this->plateReferences = OimsVehicle::get();
    }


    // public function getRequest()
    // {
    //     return [
    //         'name' => $this->name,
    //     ];
    // }

    // public function resetForm()
    // {
    //     $this->resetErrorBag();
    //     $this->reset([
    //         'name',
    //     ]);

    //     $this->confirmation_modal = false;
    // }
    // public function redirectTo(array $data, $action_type)
    // {
    //     $this->action_type = $action_type;

    //     if ($action_type == 'redirectToActivities') {
    //         return redirect()->to(route('crm.activities.activities.index'));
    //     } 
    // //     else if ($action_type == 'redirectTobookhis') {
    // //         return redirect()->to(route('crm.sales.booking-mgmt.booking-history.index2'));
    // //     }
    // }
}
