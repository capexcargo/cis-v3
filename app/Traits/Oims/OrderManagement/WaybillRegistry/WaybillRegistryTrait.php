<?php

namespace App\Traits\Oims\OrderManagement\WaybillRegistry;

use App\Models\Crm\CrmCustomerInformation;
use App\Models\OimsAgent;
use App\Models\OimsBranchReference;
use App\Models\OimsWaybillRegistry;
use App\Models\OimsWaybillRegistryDetails;
use App\Models\User;
use App\Models\UserDetails;

trait WaybillRegistryTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $date_created;
    public $action_type;

    public $waybills;
    public $waybilldetails = [];
    
    public $create_modal = false;
    public $edit_modal = false;
    public $issue_modal = false;
    public $view_modal = false;
    public $confirmation_modal;
    public $confirmsubissue_modal = false;
    public $confirmsubedit_modal = false;
    public $confirmsubcreate_modal = false;
    public $reactivate_modal = false;
    public $deactivate_modal = false;

    public $from;
    public $to;
    public $name_1;
    public $name_2;
    public $branch;
    public $waybill_id;
    public $issue_id;
    public $view_id;
    public $search_request;


    public $awbs = [];
    public $subawbs = [];

    public $counter;
    public $is_deleted;
    public $countarr;
    public $min;
    public $nearest;

    public $branchReferences = [];
    public $empReferences = [];
    public $custReferences = [];
    public $agentReferences = [];
    public $omReferences = [];
    public $flsReferences = [];

    public $getStatus1;



    public function load()
    {
        $this->branchReference();
        $this->empReference();
        $this->custReference();
        $this->agentReference();
        $this->omReference();
        $this->flsReference();
        // $this->updatedName_1();
    }

    public function branchReference()
    {
        $this->branchReferences = OimsBranchReference::get();
    }

    public function empReference()
    {
        $this->empReferences = UserDetails::where('division_id', 7)->get();
    }

    public function custReference()
    {
        $this->custReferences = CrmCustomerInformation::where('account_type', 2)->get();
    }

    public function agentReference()
    {
        $this->agentReferences = OimsAgent::get();
    }

    public function omReference()
    {
        $this->omReferences = UserDetails::where('division_id', 7)->get();
    }

    public function flsReference()
    {
        $this->flsReferences = UserDetails::where('division_id', 6)->get();
    }
    
    public function getRequest()
    {
        return [
            'awbs' => $this->awbs,
            'subawbs' => $this->subawbs,
           
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'awbs',
            'subawbs',
        ]);

        $this->confirmation_modal = false;
    }

    public function getRequestEdit()
    {
        return [
            'from' => $this->from,
            'to' => $this->to,
            'name_1' => $this->name_1,
            'name_2' => $this->name_2,
            'branch' => $this->branch,
        ];
    }

    public function resetFormEdit()
    {
        $this->resetErrorBag();
        $this->reset([
            'from',
            'to',
            'name_1',
            'name_2',
            'branch',
        ]);

        $this->confirmation_modal = false;
    }
}
