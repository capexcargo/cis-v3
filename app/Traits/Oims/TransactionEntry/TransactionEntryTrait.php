<?php

namespace App\Traits\Oims\TransactionEntry;

use App\Models\BranchReference;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmMovementSfTypeReferences;
use App\Models\CrmBookingType;
use App\Models\CrmCommodityType;
use App\Models\CrmCrateType;
use App\Models\CrmModeOfPaymentReference;
use App\Models\CrmRateAirFreight;
use App\Models\CrmRateSeaFreight;
use App\Models\CrmServiceMode;
use App\Models\CrmTransportMode;
use App\Models\OimsBranchReference;
use App\Models\OimsTeamReference;
use App\Models\OimsVehicle;
use App\Models\RouteCategoryReference;
use App\Models\User;
use App\Models\UserDetails;

trait TransactionEntryTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $create_modal = false;
    public $scan_modal = false;
    public $view_remarks_modal = false;
    public $add_remarks_modal = false;
    public $override_history_modal = false;
    public $commodity_applicable_rate_modal = false;
    public $print_sticker_modal = false;
    public $print_waybill_modal = false;
    public $shipper_contact_person_modal = false;
    public $consignee_mobile_number_modal = false;
    public $service_mode_modal = false;
    public $waybill_summary_modal = false;
    public $waybill_summary;
    public $print_modal = false;
    public $share_modal = false;
    public $edit_modal = false;
    public $reactivate_modal = false;
    public $deactivate_modal = false;
    public $select_cons_modal = false;
    public $confirmation_modal;
    public $display;
    public $date_created;
    public $action_type;
    public $search_request;

    public $service;
    public $waybill_no;

    public $trans_type;
    public $book_ref_no;
    public $book_type;
    public $origin;
    public $trans_port;
    public $branch;
    public $trans_date;

    public $transport_mode;
    public $destination;

    public $origins;
    

    // public $air_cargo;
    // public $air_pouch;
    // public $air_box;
    public $air_cargo = false;
    public $air_cargos = [];
    public $air_pouch = false;
    public $air_pouches = [];
    public $air_box = false;
    public $air_boxes = [];

    public $movement_sf_type;
    public $sea_cargo = false;
    public $sea_cargos_is_lcl = [];
    public $sea_cargos_is_fcl = [];
    public $sea_cargos_is_rcl = [];
    public $sea_box = false;
    public $sea_boxes = [];

    public $land_packaging_type = 1;
    public $land = false;
    public $lands_shippers_box = [];
    public $lands_pouches = [];
    public $lands_boxes = [];
    public $lands_ftl = [];

    // public $sea_cargo;
    // public $sea_box;
    // public $movement_sf_type;

    // public $land_packaging_type;
    // public $land;
    public $booking_type;

    public $weight_charge;
    public $awb_fee;
    public $valuation;
    public $cod_charge;
    public $insurance;
    public $handling_fee;
    public $doc_fee;

    public $other_fees = false;
    public $opa_fee;
    public $oda_fee;
    public $crating_fee;
    public $equipment_rental;
    public $lashing_fee;
    public $manpower_fee;
    public $dangerous_goods_fee;
    public $trucking_fee;
    public $perishable_fee;
    public $packaging_fee;

    public $othercharges = [];

    public $subtotal;
    public $rate_discount;
    public $promo_code;
    public $promo_discount;
    public $promo_discount_amount;
    public $evat = true;
    public $grand_total;


    public $scust_no;
    public $scname;
    public $scont_person;
    public $smobile;
    public $sadrs;

    public $ccust_no;
    public $ccname;
    public $ccont_person;
    public $cmobile;
    public $cadrs;

    public $des_goods;
    public $types_goods;
    public $paymode;
    public $charge_to;
    public $com_type;
    public $dec_val;
    public $serv_mode;
    public $com_app_rate;

    public $acc_own;
    public $prep_by;
    public $cargo_stat;
    public $coll_stat;
    public $qc_by;
    public $over_by;
    public $qc_date_time;
    public $date_time_overdn;

    public $origin_references = [];
    public $destination_references = [];
    public $transhipment_references = [];

    public $commodity_rate_airs = [];
    public $commodity_rate_seas = [];

    public $commodity_type_references = [];






    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToWaybillSummary') {
            // return redirect()->to(route('oims.order-management.team-route-assignment.team-mgmt.index'));
            return redirect('oims/order-management/transaction-entry.waybill-summary');
        } elseif ($action_type == 'redirectToTransactionEntry') {
            // return redirect()->to(route('oims.order-management.team-route- assignment.team-mgmt.index'));
            return redirect('oims/order-management/transaction-entry');
        }
    }

    // public $booktyperefs = [];

    public $paymoderefs = [];
    public $servmoderefs = [];
    public $chargetorefs = [];

    public $crating_type_references = [];
    public $transport_mode_references = [];
    public $movement_sf_type_references = [];
    public $booking_type_references = [];

    public function load()
    {
        $this->bookingTypeReferences();
        $this->paymoderef();
        $this->servmoderef();
        $this->chargetoref();
        $this->OriginReference();
        $this->DestinationReference();
        $this->TranshipmentReference();
        $this->CommodityRateReference();
        $this->CommodityTypeReference();
    }

    public function CommodityRateReference()
    {
        $this->commodity_rate_airs = CrmRateAirFreight::get();
        $this->commodity_rate_seas = CrmRateSeaFreight::get();
    }

    public function CommodityTypeReference()
    {
        $this->commodity_type_references = CrmCommodityType::get();
    }

    // public function booktyperef()
    // {
    //     $this->booktyperefs = CrmBookingType::get();
    // }
    public function bookingTypeReferences()
    {
        $this->booking_type = 1;
        $this->booking_type_references = CrmBookingType::get();
    }

    public function paymoderef()
    {
        $this->paymoderefs = CrmModeOfPaymentReference::get();
    }

    public function servmoderef()
    {
        $this->servmoderefs = CrmServiceMode::get();
    }

    public function chargetoref()
    {
        $this->chargetorefs = CrmCustomerInformation::get();
    }

    public function transportModeReferences()
    {
        $this->transport_mode_references = CrmTransportMode::get();
    }

    public function cratingtypeReferences()
    {
        $this->crating_type_references = CrmCrateType::get();
    }

    public function movementSfTypeReferences()
    {
        $this->movement_sf_type = 1;
        $this->movement_sf_type_references = CrmMovementSfTypeReferences::get();
    }



    public function getRequestSelected()
    {
        return [
            'cons' => $this->cons,
        ];
    }




    public function getRequest()
    {
        return [

            'ConsID' => $this->ConsID,
            'service' => $this->service,
            'waybill_no' => $this->waybill_no,

            'trans_type' => $this->trans_type,
            'book_ref_no' => $this->book_ref_no,
            'book_type' => $this->book_type,
            'branch' => $this->branch,
            'origin' => $this->origin,
            'trans_port' => $this->trans_port,
            'trans_date' => $this->trans_date,
            'transport_mode' => $this->transport_mode,
            'destination' => $this->destination,

            'scust_no' => $this->scust_no,
            'scname' => $this->scname,
            'scont_person' => $this->scont_person,
            'smobile' => $this->smobile,
            'sadrs' => $this->sadrs,

            'ccust_no' => $this->ccust_no,
            'ccname' => $this->ccname,
            'ccont_person' => $this->ccont_person,
            'cmobile' => $this->cmobile,
            'cadrs' => $this->cadrs,

            'des_goods' => $this->des_goods,
            'com_type' => $this->com_type,
            'dec_val' => $this->dec_val,
            'types_goods' => $this->types_goods,
            'paymode' => $this->paymode,
            'serv_mode' => $this->serv_mode,
            'charge_to' => $this->charge_to,
            'com_app_rate' => $this->com_app_rate,

            'air_cargo' => $this->air_cargo,
            'air_cargos' => $this->air_cargos,

            'air_pouch' => $this->air_pouch,
            'air_pouches' => $this->air_pouches,

            'air_box' => $this->air_box,
            'air_boxes' => $this->air_boxes,

            //Transport mode = 2
            'sea_cargo' => $this->sea_cargo,
            'sea_cargos_is_lcl' => $this->sea_cargos_is_lcl,
            'sea_cargos_is_fcl' => $this->sea_cargos_is_fcl,
            'sea_cargos_is_rcl' => $this->sea_cargos_is_rcl,

            'sea_box' => $this->sea_box,
            'sea_boxes' => $this->sea_boxes,

            'land' => $this->land,
            'lands_shippers_box' => $this->lands_shippers_box,
            'lands_pouches' => $this->lands_pouches,
            'lands_boxes' => $this->lands_boxes,
            'lands_ftl' => $this->lands_ftl,

            'weight_charge' => $this->weight_charge,
            'awb_fee' => $this->awb_fee,
            'valuation' => $this->valuation,
            'cod_charge' => $this->cod_charge,
            'insurance' => $this->insurance,
            'handling_fee' => $this->handling_fee,
            'doc_fee' => $this->doc_fee,

            'opa_fee' => $this->opa_fee,
            'oda_fee' => $this->oda_fee,
            'crating_fee' => $this->crating_fee,
            'equipment_rental' => $this->equipment_rental,
            'lashing_fee' => $this->lashing_fee,
            'manpower_fee' => $this->manpower_fee,
            'dangerous_goods_fee' => $this->dangerous_goods_fee,
            'trucking_fee' => $this->trucking_fee,
            'perishable_fee' => $this->perishable_fee,
            'packaging_fee' => $this->packaging_fee,

            'subtotal' => $this->subtotal,
            'rate_discount' => $this->rate_discount,
            'promo_code' => $this->promo_code,
            'promo_discount' => $this->promo_discount,
            'promo_discount_amount' => $this->promo_discount_amount,

            'acc_own' => $this->acc_own,
            'prep_by' => $this->prep_by,
            'cargo_stat' => $this->cargo_stat,
            'coll_stat' => $this->coll_stat,
            'qc_by' => $this->qc_by,
            'over_by' => $this->over_by,
            'qc_date_time' => $this->qc_date_time,
            'date_time_overdn' => $this->date_time_overdn,
        ];
    }



    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'service',
            'waybill_no',

            'trans_type',
            'book_ref_no',
            'book_type',
            'origin',
            'trans_port',
            'branch',
            'trans_date',
            'transport_mode',
            'destination',

            'scust_no',
            'scname',
            'scont_person',
            'smobile',
            'sadrs',

            'ccust_no',
            'ccname',
            'ccont_person',
            'cmobile',
            'cadrs',

            'des_goods',
            'com_type',
            'dec_val',
            'types_goods',
            'paymode',
            'serv_mode',
            'charge_to',
            'com_app_rate',

            'air_cargo',
            'air_cargos',
            'air_pouch',
            'air_pouches',
            'air_boxes',

            'sea_cargo',
            'sea_cargos_is_lcl',
            'sea_cargos_is_fcl',
            'sea_cargos_is_rcl',

            'sea_box',
            'sea_boxes',

            'land',
            'lands_shippers_box',
            'lands_pouches',
            'lands_boxes',
            'lands_ftl',

            'weight_charge',
            'awb_fee',
            'valuation',
            'cod_charge',
            'insurance',
            'handling_fee',
            'doc_fee',

            'opa_fee',
            'oda_fee',
            'crating_fee',
            'equipment_rental',
            'lashing_fee',
            'manpower_fee',
            'dangerous_goods_fee',
            'trucking_fee',
            'perishable_fee',
            'packaging_fee',

            'subtotal',
            'rate_discount',
            'promo_code',
            'promo_discount',
            'promo_discount_amount',

            'acc_own',
            'prep_by',
            'cargo_stat',
            'coll_stat',
            'qc_by',
            'over_by',
            'qc_date_time',
            'date_time_overdn',
        ]);

        $this->confirmation_modal = false;
    }
    public function OriginReference()
    {
        $this->origin_references = BranchReference::get();
    }

    public function DestinationReference()
    {
        $this->destination_references = BranchReference::get();
    }

    public function TranshipmentReference()
    {
        $this->transhipment_references = BranchReference::get();
    }
}
