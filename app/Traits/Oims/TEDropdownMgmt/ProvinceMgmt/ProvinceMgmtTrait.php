<?php

namespace App\Traits\Oims\TEDropdownMgmt\ProvinceMgmt;

use App\Models\Crm\BarangayReference;
use App\Models\Crm\CityReference;
use App\Models\IslandGroup;
use App\Models\OimsZipcodeReference;
use App\Models\Region;

trait ProvinceMgmtTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $date_created;
    public $action_type;

    public $create_modal = false;
    public $edit_modal = false;
    public $confirmation_modal;
    public $reactivate_modal = false;
    public $deactivate_modal = false;

    public $name;
    public $mun;
    public $brgy;
    public $reg;
    public $isl;
    public $province_id;
    public $search_request;

    public $munReferences = [];
    public $regReferences = [];
    public $islReferences = [];

    public $provs = [];


    public function load()
    {
        $this->muniReference();
        $this->regiReference();
        $this->isleReference();
    }

    public function muniReference()
    {
        $this->munReferences = CityReference::get();
    }

    public function regiReference()
    {
        $this->regReferences = Region::get();
    }

    public function isleReference()
    {
        $this->islReferences = IslandGroup::get();
    }

    public function updatedReg()
    {
        $gregisl = Region::with('IslandGroupReference')->where('id', $this->reg)->first();

        if ($this->reg != '') {
            $this->isl = $gregisl->IslandGroupReference->id;
        }else{
            $this->isl = '';
        }
    }

    // public function GetIsl()
    // {
    //     foreach ($this->provs as $a => $prov) {
    //         if ($this->provs[$a]['reg'] != null) {
    //             $gregisl = Region::with('IslandGroupReference')->where('id', $this->provs[$a]['reg'])->first();
    //             if ($this->provs[$a]['is_deleted'] == false) {
    //                 $this->provs[$a] = [
    //                     'id' => $this->provs[$a]['id'],
    //                     'reg' => $this->provs[$a]['reg'],
    //                     'isl' => $gregisl->IslandGroupReference->name,
    //                     'is_deleted' => false,
    //                 ];
    //             }
    //         } else {
    //             $this->provs[$a] = [
    //                 'id' => null,
    //                 'reg' => $this->provs[$a]['reg'],
    //                 'isl' => '',
    //                 'is_deleted' => false,
    //             ];
    //         }
    //     }
    // }

    // protected function updatedBrgyzips()
    // {
    //     $this->GetIsl();
    //     $this->resetErrorBag();
    // }


    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToServicarea') {
            return redirect()->to(route('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index'));
        }
    }

    public function getRequest()
    {
        return [
            'name' => $this->name,
            'reg' => $this->reg,
            'isl' => $this->isl,
            'provs' => $this->provs,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'name',
            'reg',
            'isl',
            'provs',
        ]);

        $this->confirmation_modal = false;
    }
}
