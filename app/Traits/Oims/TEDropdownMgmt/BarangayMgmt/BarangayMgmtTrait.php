<?php

namespace App\Traits\Oims\TEDropdownMgmt\BarangayMgmt;

use App\Models\OimsZipcodeReference;

trait BarangayMgmtTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $date_created;
    public $action_type;

    public $create_modal = false;
    public $edit_modal = false;
    public $confirmation_modal;
    public $reactivate_modal = false;
    public $deactivate_modal = false;

    public $name;
    public $zip;
    public $barangay_id;
    public $search_request;

    public $zipReferences = [];

    public function load()
    {
        $this->zipReference();
    }

    public function zipReference()
    {
        $this->zipReferences = OimsZipcodeReference::get();
    }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToServicarea') {
            return redirect()->to(route('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index'));
        } 
    }

    public function getRequest()
    {
        return [
            'name' => $this->name,
            'zip' => $this->zip,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'name',
            'zip',
        ]);

        $this->confirmation_modal = false;
    }
}
