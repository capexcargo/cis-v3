<?php

namespace App\Traits\Oims\TEDropdownMgmt\MunicipalityMgmt;

use App\Models\Crm\BarangayReference;
use App\Models\OimsZipcodeReference;

trait MunicipalityMgmtTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $date_created;
    public $action_type;

    public $create_modal = false;
    public $edit_modal = false;
    public $confirmation_modal;
    public $reactivate_modal = false;
    public $deactivate_modal = false;

    public $name;
    public $brgy;
    public $zip;
    public $municipality_id;
    public $search_request;

    public $barReferences = [];
    public $zipReferences = [];

    public $brgyzips = [];


    public function load()
    {
        $this->brgyReference();
        $this->zipReference();
    }

    public function zipReference()
    {
        $this->zipReferences = OimsZipcodeReference::get();
    }

    public function brgyReference()
    {
        $this->barReferences = BarangayReference::where('status', 1)->get();
        // dd($this->barReferences);
    }

    public function GetZip()
    {
        foreach ($this->brgyzips as $a => $brgyzip) {
            if ($this->brgyzips[$a]['brgy'] != null) {
                $gbrgyzip = BarangayReference::with('ZipcodeReference')->where('id', $this->brgyzips[$a]['brgy'])->first();
                if ($this->brgyzips[$a]['is_deleted'] == false) {
                    $this->brgyzips[$a] = [
                        'id' => $this->brgyzips[$a]['id'],
                        'brgy' => $this->brgyzips[$a]['brgy'],
                        'zip' => $gbrgyzip->ZipcodeReference->name,
                        'is_deleted' => false,
                    ];
                }
            } else {
                $this->brgyzips[$a] = [
                    'id' => null,
                    'brgy' => $this->brgyzips[$a]['brgy'],
                    'zip' => '',
                    'is_deleted' => false,
                ];
            }
        }
    }

    protected function updatedBrgyzips()
    {
        $this->GetZip();
        $this->resetErrorBag();
    }


    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToServicarea') {
            return redirect()->to(route('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index'));
        } 
    }

    public function getRequest()
    {
        return [
            'name' => $this->name,
            'brgyzips' => $this->brgyzips,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'name',
            'brgyzips',
        ]);

        $this->confirmation_modal = false;
    }
}
