<?php

namespace App\Traits\Oims\TEDropdownMgmt\ServiceAreaMgmt;

use App\Models\Crm\BarangayReference;
use App\Models\Crm\CityReference;
use App\Models\Crm\StateReference;
use App\Models\IslandGroup;
use App\Models\OimsBranchReference;
use App\Models\OimsServiceabilityReference;
use App\Models\OimsTransportReference;
use App\Models\Region;

trait ServiceAreaMgmtTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $date_created;
    public $action_type;

    public $create_modal = false;
    public $edit_modal = false;
    public $confirmation_modal;
    public $reactivate_modal = false;
    public $deactivate_modal = false;

    public $isl;
    public $reg;
    public $prov;
    public $mun;
    public $bar;
    public $zip;
    public $ptc;
    public $serv;// public $odaopa;

    public $service_id;
    public $transaction_entry = 7;

    public $island_references = [];
    public $region_references = [];
    public $province_references = [];
    public $municipal_references = [];
    public $barangay_references = [];
    public $service_references = [];

    public $ports = [];


    public function island()
    {
        $this->island_references = IslandGroup::get();
    }

    protected function updatedIsl()
    {
        $this->resetErrorBag('isl');

        if ($this->isl == "") {
            $this->reg = "";
            $this->prov = "";
            $this->mun = "";
            $this->bar = "";
            $this->zip = "";
        } else {
            $this->region();
            $this->reg = "";
            $this->prov = "";
            $this->mun = "";
            $this->bar = "";
            $this->zip = "";
        }
    }

    public function region()
    {
        $this->region_references =  Region::where('island_group_id', $this->isl)->orderBy('name', 'asc')->get();
    }

    protected function updatedReg()
    {
        $this->resetErrorBag('reg');

        if ($this->prov = "") {
            $this->mun = "";
            $this->bar = "";
            $this->zip = "";
        } else {
            $this->province();
            $this->mun = "";
            $this->bar = "";
            $this->zip = "";
        }
    }

    public function province()
    {
        if (isset($this->reg)) {
            $this->province_references =  StateReference::where('region_id', $this->reg)->orderBy('name', 'asc')->get();
        }
    }

    protected function updatedProv()
    {
        $this->resetErrorBag('prov');

        if ($this->mun = "") {
            $this->bar = "";
            $this->zip = "";
        } else {
            $this->municipal();
            $this->bar = "";
            $this->zip = "";
        }
    }

    public function municipal()
    {
        $this->municipal_references =  CityReference::where('state_id', $this->prov)->get();
    }

    protected function updatedMun()
    {
        $this->resetErrorBag('mun');

        if ($this->bar = "") {
            $this->zip = "";
        } else {
            $this->barangay();
            $this->zip = "";
        }
    }

    public function barangay()
    {
        $this->barangay_references =  BarangayReference::where('city_id', $this->mun)->get();
    }

    protected function updatedBar()
    {
        $this->resetErrorBag('bar');

        $barzip = BarangayReference::with('ZipcodeReference')->where('id', $this->bar)->first();

        if ($this->bar != '') {
            $this->zip = $barzip->ZipcodeReference->name;
        } else {
            $this->zip = "";
        }
    }

    protected function updatedPtc()
    {
        $this->loadPorts();
        $this->resetErrorBag('ptc');
    }

    public function loadPorts()
    {
        $this->ports = OimsBranchReference::where([
            ['name', 'like', '%' . $this->ptc . '%']
        ])
            ->orderBy('name', 'asc')
            ->take(10)
            ->get();
    }

    public function getPortDetails($id)
    {
        $port_data = OimsBranchReference::with('OBRTelephoneHasMany', 'OBRContactHasMany')->find($id);
        $ports = OimsBranchReference::with('OBRTelephoneHasMany', 'OBRContactHasMany')->where([
            ['id', $id]
        ])
            ->orderBy('name', 'asc')
            ->take(10)
            ->first();

        if ($port_data) {
            $this->ptc = $port_data->name;
        }
    }

    public function serviceability()
    {
        $this->service_references = OimsServiceabilityReference::get();
    }

    protected function updatedServ()
    {
        $this->resetErrorBag('serv');
        // $this->getDistance();
    }

    // public $new_longitude;
    // public $new_latitude;
    // public $old_longitude;
    // public $old_latitude;

    // protected function updatedServ()
    // {
    //     $this->resetErrorBag('serv');
    // }

    // public function getDistance($unit = 'kilometers')
    // {
    //     $new_longitude = 14.5229563;
    //     $new_latitude = 120.9996478;

    //     // $user=User::find(1);
    //     // $old_longitude=$user->longitude
    //     // $old_latitude=$user->latitude

    //     $old_longitude = 14.5065483;
    //     $old_latitude = 121.0261283;
    //     // $dLat = deg2rad($new_latitude - $old_latitude);
    //     // $dLon = deg2rad($new_longitude - $old_longitude);
    //     // $radius = 6372.797;
    //     // $a = sin($dLat / 2) * sin($dLat / 2) +
    //     //     cos(deg2rad($old_latitude)) * cos(deg2rad($new_latitude)) *
    //     //     sin($dLon / 2) * sin($dLon / 2);
    //     // $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
    //     // $d = $radius * $c; // Distance in km

    //     // $this->odaopa = round($d).' '.'Km';


    //     // function getDistanceBetweenPointsNew($new_latitude, $new_longitude, $old_latitude, $old_longitude, $unit = 'miles') {
    //         $theta = $new_longitude - $old_longitude; 
    //         $distance = (sin(deg2rad($new_latitude)) * sin(deg2rad($old_latitude))) + (cos(deg2rad($new_latitude)) * cos(deg2rad($old_latitude)) * cos(deg2rad($theta))); 
    //         $distance = acos($distance); 
    //         $distance = rad2deg($distance); 
    //         $distance = $distance * 60 * 1.1515; 
    //         switch($unit) { 
    //           case 'miles': 
    //             break; 
    //           case 'kilometers' : 
    //             $distance = $distance * 1.609344; 
    //         } 
    //         $this->odaopa = (round($distance,2)); 
    //     //   }
    // }



    public function updatedTransactionEntry()
    {
        if ($this->transaction_entry == 1) {
            return redirect('oims/order-management/t-e-dropdown-mgmt');
        } else if ($this->transaction_entry == 2) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.branch-mgmt');
        } else if ($this->transaction_entry == 3) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.transport-mgmt');
        } else if ($this->transaction_entry == 4) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.paymode-mgmt');
        } else if ($this->transaction_entry == 5) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.service-mgmt');
        } else if ($this->transaction_entry == 6) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.odaopa-mgmt');
        } else if ($this->transaction_entry == 7) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.service-area-mgmt');
        } else if ($this->transaction_entry == 8) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.dangerous-mgmt');
        } else if ($this->transaction_entry == 9) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.crating-mgmt');
        } else if ($this->transaction_entry == 10) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.commodity-mgmt');
        } else if ($this->transaction_entry == 11) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.packaging-mgmt');
        } else if ($this->transaction_entry == 12) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.reason-mgmt');
        } else if ($this->transaction_entry == 13) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.putaway-mgmt');
        } else if ($this->transaction_entry == 14) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.remarks-mgmt');
        } else if ($this->transaction_entry == 15) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.cargo-mgmt');
        }
    }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToServicability') {
            return redirect()->to(route('oims.order-management.t-e-dropdown-mgmt.serviceability-mgmt.index'));
        } else if ($action_type == 'redirectToZipcode') {
            return redirect()->to(route('oims.order-management.t-e-dropdown-mgmt.zipcode-mgmt.index'));
        } else if ($action_type == 'redirectToBarangay') {
            return redirect()->to(route('oims.order-management.t-e-dropdown-mgmt.barangay-mgmt.index'));
        } else if ($action_type == 'redirectToCity') {
            return redirect()->to(route('oims.order-management.t-e-dropdown-mgmt.municipality-mgmt.index'));
        } else if ($action_type == 'redirectToProvince') {
            return redirect()->to(route('oims.order-management.t-e-dropdown-mgmt.province-mgmt.index'));
        }
    }



    public function getRequest()
    {
        return [
            'isl' => $this->isl,
            'reg' => $this->reg,
            'prov' => $this->prov,
            'mun' => $this->mun,
            'bar' => $this->bar,
            'zip' => $this->zip,
            'ptc' => $this->ptc,
            'serv' => $this->serv,
            // 'odaopa' => $this->odaopa,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'isl',
            'reg',
            'prov',
            'mun',
            'bar',
            'zip',
            'ptc',
            'serv',
            // 'odaopa',
        ]);

        $this->confirmation_modal = false;
    }
}
