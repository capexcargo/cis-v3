<?php

namespace App\Traits\Oims\TEDropdownMgmt\BranchMgmt;

use App\Models\OriginDestinationPortReference;
use App\Models\Region;
use Database\Seeders\Oims\OrderManagement\TEDropdownMgmt\OimsRegionSeeder;

trait BranchMgmtTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $date_created;
    public $action_type;

    public $create_modal = false;
    public $edit_modal = false;
    public $view_modal = false;
    public $confirmation_modal;
    public $reactivate_modal = false;
    public $deactivate_modal = false;

    public $r_name;
    public $b_name;
    public $adrs;
    public $orig;
    public $lat;
    public $long;
    public $branch_id;
    public $transaction_entry = 2;

    public $origReferences = [];

    
    public function origReference()
    {
        $this->origReferences = OriginDestinationPortReference::get();
    }

    public function getAddressName($name)
    {
        $this->adrs = $name;
    }

    public function getLat($lat)
    {
        $this->lat = $lat;
    }

    public function getLong($long)
    {
        $this->long = $long;
    }

    public function updatedTransactionEntry()
    {
        if ($this->transaction_entry == 1) {
            return redirect('oims/order-management/t-e-dropdown-mgmt');
        } else if ($this->transaction_entry == 2) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.branch-mgmt');
        } else if ($this->transaction_entry == 3) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.transport-mgmt');
        } else if ($this->transaction_entry == 4) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.paymode-mgmt');
        } else if ($this->transaction_entry == 5) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.service-mgmt');
        } else if ($this->transaction_entry == 6) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.odaopa-mgmt');
        } else if ($this->transaction_entry == 7) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.service-area-mgmt');
        } else if ($this->transaction_entry == 8) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.dangerous-mgmt');
        } else if ($this->transaction_entry == 9) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.crating-mgmt');
        } else if ($this->transaction_entry == 10) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.commodity-mgmt');
        } else if ($this->transaction_entry == 11) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.packaging-mgmt');
        } else if ($this->transaction_entry == 12) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.reason-mgmt');
        } else if ($this->transaction_entry == 13) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.putaway-mgmt');
        } else if ($this->transaction_entry == 14) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.remarks-mgmt');
        } else if ($this->transaction_entry == 15) {
            return redirect('oims/order-management/t-e-dropdown-mgmt.cargo-mgmt');
        }
    }

    public $oBRReferences = [];

    public function load()
    {
        $this->oimsRegionReference();
    }

    public function oimsRegionReference()
    {
        $this->oBRReferences = Region::get();
    }


    public function getRequest()
    {
        return [
            'r_name' => $this->r_name,
            'b_name' => $this->b_name,
            'adrs' => $this->adrs,
            'orig' => $this->orig,
            'tellnos' => $this->tellnos,
            'contacts' => $this->contacts,
            'lat' => $this->lat,
            'long' => $this->long,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'r_name',
            'b_name',
            'adrs',
            'orig',
            'tellnos',
            'contacts',
            'lat',
            'long',
        ]);

        $this->confirmation_modal = false;
    }
}
