<?php

namespace App\Traits\Oims\TEDropdownMgmt\ServiceabilityMgmt;

trait ServiceabilityMgmtTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $date_created;
    public $action_type;

    public $create_modal = false;
    public $edit_modal = false;
    public $confirmation_modal;
    public $reactivate_modal = false;
    public $deactivate_modal = false;

    public $name;
    public $color;
    public $serviceability_id;
    public $search_request;

    // public function updatedTransactionEntry()
    // {
    //     if ($this->transaction_entry == 1) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt');
    //     } else if ($this->transaction_entry == 2) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt.branch-mgmt');
    //     } else if ($this->transaction_entry == 3) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt.transport-mgmt');
    //     } else if ($this->transaction_entry == 4) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt.paymode-mgmt');
    //     } else if ($this->transaction_entry == 5) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt.service-mgmt');
    //     } else if ($this->transaction_entry == 6) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt.odaopa-mgmt');
    //     } else if ($this->transaction_entry == 7) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt.service-area-mgmt');
    //     } else if ($this->transaction_entry == 8) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt.dangerous-mgmt');
    //     } else if ($this->transaction_entry == 9) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt.crating-mgmt');
    //     } else if ($this->transaction_entry == 10) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt.commodity-mgmt');
    //     } else if ($this->transaction_entry == 11) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt.packaging-mgmt');
    //     } else if ($this->transaction_entry == 12) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt.reason-mgmt');
    //     } else if ($this->transaction_entry == 13) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt.putaway-mgmt');
    //     } else if ($this->transaction_entry == 14) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt.remarks-mgmt');
    //     } else if ($this->transaction_entry == 15) {
    //         return redirect('oims/order-management/t-e-dropdown-mgmt.cargo-mgmt');
    //     }
    // }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToServicarea') {
            return redirect()->to(route('oims.order-management.t-e-dropdown-mgmt.service-area-mgmt.index'));
        } 
    }

    public function getRequest()
    {
        return [
            'name' => $this->name,
            'color' => $this->color,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'name',
            'color',
        ]);

        $this->confirmation_modal = false;
    }
}
