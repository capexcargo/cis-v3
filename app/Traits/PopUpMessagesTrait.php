<?php

namespace App\Traits;

use Illuminate\Support\Facades\Session;

trait PopUpMessagesTrait
{
    public function sweetAlert($icon = 'success', $title = '', $text = '')
    {
        $this->dispatchBrowserEvent('swal', [
            'icon' => $icon,
            'title' => $title,
            'text' => $text,
        ]);
    }

    public function sweetAlertError($icon = 'error', $title = '', $text = '')
    {
        $this->dispatchBrowserEvent('swal', [
            'icon' => $icon,
            'title' => $title,
            'text' => $text,
        ]);
    }

    public function sweetAlertDefault($icon = 'success', $title = '', $text = '')
    {
        Session::put('swal_default', [
            'icon' => $icon,
            'title' => $title,
            'text' => $text,
        ]);
    }

    public function sweetAlertDefaultError($icon = 'error', $title = '', $text = '')
    {
        Session::put('swal_default', [
            'icon' => $icon,
            'title' => $title,
            'text' => $text,
        ]);
    }


    // public function sometingWentWrong($e)
    // {
    //    return $this->dispatchBrowserEvent('swal', [
    //         'icon' => 'error',
    //         'title' => 'Something Went Wrong',
    //         'text' => $e->getMessage(),
    //     ]);
    // }
}
