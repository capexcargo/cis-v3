<?php

namespace App\Traits\Ticketing;

use App\Models\Division;
use App\Models\Hrim\Department;
use App\Models\MonthReference;
use App\Models\Ticketing\TicketCategoryManagement;
use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Support\Facades\Auth;

trait TicketingSubcategoryManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $view_modal = false;
    public $delete_modal = false;

    public $confirmation_message;
    public $confirmation_modal = false;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $category_references = [];
    public $division_references = [];
    public $department_references = [];
    public $task_holder_references = [];

    public $category;
    public $subcategory;
    public $sla;
    public $severity;
    public $division;
    public $department;

    public $subcategory_id;

    public $taskholders = [];

    public function categoryReferences()
    {
        if (Auth::user()->level_id != 5) {
            $this->category_references = TicketCategoryManagement::where('division_id', Auth::user()->division_id)->get();
        } else {
            $this->category_references = TicketCategoryManagement::get();
        }
    }

    public function divisionReferences()
    {
        if (Auth::user()->level_id != 5) {
            $this->division_references = Division::where('id', Auth::user()->division_id)->get();
        } else {
            $this->division_references = Division::get();
        }
    }

    public function departmentReferences()
    {
        $this->department_references = Department::where('division_id', $this->division)->get();
    }

    public function taskHolderReferences()
    {
        $this->task_holder_references = UserDetails::where('division_id', $this->division)->get();
    }

    protected function updatedDivision()
    {
        $this->departmentReferences();
        $this->taskHolderReferences();
    }

    protected function updatedDepartment()
    {
        $this->updatedDivision();
    }

    public function addTaskholders()
    {
        $this->taskholders[] = [
            'id' => null,
            'task_holder' => null,
            'is_deleted' => false,
        ];
    }

    public function removeTaskholders($i)
    {
        if (count(collect($this->taskholders)->where('is_deleted', false)) > 1) {
            if ($this->taskholders[$i]['id']) {
                $this->taskholders[$i]['is_deleted'] = true;
            } else {
                unset($this->taskholders[$i]);
            }

            array_values($this->taskholders);
        }
    }

    public function getRequest()
    {
        return [
            'category' => $this->category,
            'subcategory' => $this->subcategory,
            'sla' => $this->sla,
            'severity' => $this->severity,
            'category' => $this->category,
            'division' => $this->division,
            'department' => $this->department,
            'taskholders' => $this->taskholders,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'category',
            'subcategory',
            'sla',
            'severity',
            'category',
            'division',
            'department',
            'taskholders',
        ]);

        $this->confirmation_modal = false;
    }
}
