<?php

namespace App\Traits\Ticketing;

use App\Models\Division;
use App\Models\Ticketing\TaskHolder;
use App\Models\Ticketing\TicketCategoryManagement;
use App\Models\Ticketing\TicketSubcategoryMgmt;

trait TicketManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $view_modal = false;
    public $delete_modal = false;

    public $action_type;
    
    public $confirmation_message;
    public $confirmation_modal = false;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $header_cards = [];

    public $ticket_ref_no;
    public $division;
    public $subject;
    public $category;
    public $subcategory;
    public $task_holder;
    public $message;
    public $concern_reference;
    public $concern_ref_url;
    public $actual_start_date;
    public $target_start_date;
    public $target_end_date;
    public $remarks;
    public $division_id;
    public $closer;

    public $date_closed;
    public $date_created;
    public $ticket_status;

    public $attachments = [];

    public $division_references = [];
    public $category_references = [];
    public $subcategory_references = [];
    public $taskholder_references = [];

    public $categories_references = [];

    public $ticket_id;

    // <-- Dashboard Variables -->
    public $total_tickets;
    public $open_tickets;
    public $closed_tickets;

    public $ticket_categories = [];
    public $help_desk;
    public $tech_supp;

    public $weekly_tickets;
    public $day1_tickets;
    public $day2_tickets;
    public $day3_tickets;
    public $day4_tickets;
    public $day5_tickets;
    public $day6_tickets;
    public $day7_tickets;

    public $weekly_closed1_tickets;
    public $weekly_closed2_tickets;
    public $weekly_closed3_tickets;
    public $weekly_closed4_tickets;
    public $weekly_closed5_tickets;
    public $weekly_closed6_tickets;
    public $weekly_closed7_tickets;

    public $weekly_total1_tickets;
    public $weekly_total2_tickets;
    public $weekly_total3_tickets;
    public $weekly_total4_tickets;
    public $weekly_total5_tickets;
    public $weekly_total6_tickets;
    public $weekly_total7_tickets;

    public $overdue_tickets;
    public $count_overdue = [];

    public $within_sla = [];
    public $beyond_sla = [];

    // <-- Dashboard Variables -->

    public function loadReferences()
    {
        $this->divisionReferences();
        $this->categoryReferences();
        $this->subcategoryReferences();
        $this->addAttachments();
    }

    public function generateTicketReference()
    {
        $this->ticket_ref_no = 'CPX-T' . rand(11111, 99999);
    }

    public function addAttachments()
    {
        $this->attachments[] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }

    public function removeAttachments($i)
    {
        if (count(collect($this->attachments)->where('is_deleted', false)) > 1) {
            if ($this->attachments[$i]['id']) {
                $this->attachments[$i]['is_deleted'] = true;
            } else {
                unset($this->attachments[$i]);
            }

            array_values($this->attachments);
        }
    }

    public function redirectTo(array $data, $action_type)
    {
        if ($action_type == 'redirectToDashboard') {
            return redirect(route('ticketing.ticket-management.dashboard.index'));
        } else if ($action_type == 'redirectToAllTickets') {
            return redirect(route('ticketing.ticket-management.all-tickets.indexalltickets'));
        } else if ($action_type == 'redirectToMyTickets') {
            return redirect(route('ticketing.ticket-management.my-tickets.indexmytickets'));
        } else if ($action_type == 'redirectToMyTasks') {
            return redirect(route('ticketing.ticket-management.my-tasks.indexmytasks'));
        }
    }

    public function divisionReferences()
    {
        // if (Auth::user()->level_id != 5) {
        //     $this->division_references = Division::where('id', Auth::user()->division_id)->get();
        // } else {
        $this->division_references = Division::get();
        // }
    }

    protected function updatedDivision()
    {
        $this->categoryReferences();
    }

    public function categoryReferences()
    {
        $this->category_references = TicketCategoryManagement::where('division_id', $this->division)->get();
    }

    protected function updatedCategory()
    {
        $this->subcategoryReferences();
    }

    public function subcategoryReferences()
    {
        $this->subcategory_references = TicketSubcategoryMgmt::where('category_id', $this->category)->get();
    }

    public function categories()
    {
        $this->categories_references = TicketCategoryManagement::get();
    }

    public function task_holders()
    {
        $this->taskholder_references = TaskHolder::groupBy('user_id')->get();
    }

    public function getRequest()
    {
        return [
            'ticket_ref_no' => $this->ticket_ref_no,
            'division' => $this->division,
            'subject' => $this->subject,
            'category' => $this->category,
            'subcategory' => $this->subcategory,
            'message' => $this->message,
            'concern_reference' => $this->concern_reference,
            'concern_ref_url' => $this->concern_ref_url,
            'attachments' => $this->attachments,
        ];
    }

    public function getTaskholder()
    {
        return [
            'task_holder' => $this->task_holder,
            'target_start_date' => $this->target_start_date,
            'target_end_date' => $this->target_end_date,
        ];
    }

    public function getActualDates()
    {
        return [
            'actual_start_date' => $this->actual_start_date,
            'remarks' => $this->remarks,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'ticket_ref_no',
            'division',
            'subject',
            'category',
            'subcategory',
            'message',
            'concern_reference',
            'concern_ref_url',
            'attachments',
        ]);

        $this->confirmation_modal = false;
    }
}
