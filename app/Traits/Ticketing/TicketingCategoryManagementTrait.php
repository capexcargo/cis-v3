<?php

namespace App\Traits\Ticketing;

use App\Models\Division;
use App\Models\MonthReference;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait TicketingCategoryManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $view_modal = false;
    public $delete_modal = false;

    public $confirmation_message;
    public $confirmation_modal = false;

    public $sortField = 'created_at';
    public $sortAsc = false;
    public $paginate = 10;

    public $division_references = [];

    public $name;
    public $division_id;

    public $category_id;
    public $divisionid;
    public $levelid;

    public function resetForm()
    {
        $this->reset([
            'name',
            'division_id'
        ]);

        $this->confirmation_modal = false;
    }

    public function divisionReferences()
    {

        if(Auth::user()->level_id != 5){
            $this->division_references = Division::where('id', Auth::user()->division_id)->get();
        }else{
            $this->division_references = Division::get();
        }
        

    }

    public function getRequest()
    {
        return [
            'name' => $this->name,
            'division_id' => $this->division_id,
        ];
    }
}
