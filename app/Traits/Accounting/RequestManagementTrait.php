<?php

namespace App\Traits\Accounting;

use App\Models\Accounting\AccountTypeReference;
use App\Models\Accounting\BudgetChart;
use App\Models\Accounting\BudgetPlan;
use App\Models\Accounting\BudgetSource;
use App\Models\Accounting\CanvassingSupplier;
use App\Models\Accounting\CAReferenceTypes;
use App\Models\Accounting\FreightReferenceType;
use App\Models\Accounting\FreightUsageReference;
use App\Models\Accounting\Loaders;
use App\Models\Accounting\OpexType;
use App\Models\Accounting\PaymentTypeReference;
use App\Models\Accounting\PriorityReference;
use App\Models\Accounting\RequestForPayment;
use App\Models\Accounting\Supplier;
use App\Models\Accounting\TermsReference;
use App\Models\Accounting\TruckingTypeReference;
use App\Models\BranchReference;
use App\Models\Division;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

trait RequestManagementTrait
{
    public $parent_reference_number;
    public $reference_number;
    public $rfp_type;

    public $is_payee_contact_person;
    public $payee;
    public $remarks;
    public $description;
    public $amount;
    public $multiple_budget;
    public $branch;
    public $coa_category;
    public $date_needed;
    public $priority;
    public $date_of_transaction_from;
    public $date_of_transaction_to;
    public $canvasser;
    public $is_set_approver_1 = false;
    public $approver_1_id;
    public $approver_2_id;
    public $approver_3_id;
    public $is_approved_1;
    public $is_approved_2;
    public $is_approved_3;
    public $approver_1_name;
    public $approver_2_name;
    public $approver_3_name;
    public $approve_1;
    public $approve_2;
    public $approve_3;
    public $approver_remarks_1;
    public $approver_remarks_2;
    public $approver_remarks_3;
    public $opex_type;
    public $attachments = [];

    public $division;
    public $budget_source;
    public $chart_of_accounts_search;
    public $chart_of_accounts;
    public $budget_plan;
    public $month;

    public $canvassing_supplier;
    public $canvassing_supplier_id;
    public $po_reference_no;

    public $accounting_account_type;
    public $loader;
    public $loader_search;

    public $ca_no;
    public $ca_reference_type;
    public $ca_reference_type_display;

    public $account_no;
    public $payment_type;
    public $terms;
    public $pdc_from;
    public $pdc_to;
    public $pdc_date;

    public $request_for_payments = [];
    public $freights = [];
    public $payables = [];
    public $cash_advances = [];

    public $priority_references = [];
    public $supplier_search;
    public $supplier_references = [];
    public $branch_references = [];
    public $payment_type_references = [];
    public $terms_references = [];
    public $accounting_account_type_references = [];
    public $loaders_references = [];
    public $freight_reference_type_references = [];
    public $freight_usage_references = [];
    public $trucking_type_references = [];
    public $approvers = [];
    public $opex_type_references = [];
    public $canvassing_supplier_references = [];
    public $ca_reference_types_references = [];

    public $budget_source_references = [];
    public $budget_chart_references = [];
    public $division_references = [];
    public $budget_plan_references = [];

    public $total_amount = 0;

    public $request;

    public $is_subpayee;
    public $subpayee;

    public $yearcreated;
    public $yearcreateds;
    public $created_at;


    public function generateParentReferenceNumber()
    {
        $this->parent_reference_number = "PRNT-" . date("ymdhis") . "-" . rand(111, 999);
    }

    public function generateReferenceNumber()
    {
        $this->reference_number = "RFP-" . date("ymdhis") . "-" . rand(111, 999);
    }

    public function generateCANumber()
    {
        if ($this->budget_plan) {
            $total_ca = RequestForPayment::whereHas('budget', function ($query) {
                $query->where('division_id', $this->division);
            })->where('type_id', 4)->count();

            $this->ca_no = $this->budget_plan->division->name . "-" . date("ymd") . "-" . ($total_ca + 1);
        }
    }

    public function load()
    {
        $this->loadDivisionReference();
        $this->loadPriorityReference();
        $this->loadSupplierReference();
        $this->loadBranchReference();
        $this->loadOpexReference();
        $this->loadCanvassingSupplierReference();
        $this->loadPaymentTypeReference();
        $this->loadTermsReference();
        $this->loadCAReferenceTypes();
    }

    public function loadApprovers()
    {
        $this->approvers = User::where('division_id', $this->division)
            ->whereBetween('level_id', [2, 4])
            ->get();
    }

    public function loadPriorityReference()
    {
        $this->priority_references = PriorityReference::active()->get();
    }

    public function loadSupplierReference()
    {
        $this->supplier_references = Supplier::when($this->is_payee_contact_person, function ($query) {
            $query->where('first_name', 'like', '%' . $this->supplier_search . '%');
        })
            ->when(!$this->is_payee_contact_person, function ($query) {
                $query->where('company', 'like', '%' . $this->supplier_search . '%');
            })
            ->orderBy(($this->is_payee_contact_person ? 'first_name' : 'company'), 'asc')
            ->take(10)
            ->get();
    }

    public function loadBranchReference()
    {
        $this->branch_references = BranchReference::get();
    }

    public function loadPaymentTypeReference()
    {
        $this->payment_type_references = PaymentTypeReference::active()->get();
    }

    public function loadTermsReference()
    {
        $this->terms_references = TermsReference::active()->get();
    }

    public function loadAccountingAccountTypeReference()
    {
        $this->accounting_account_type_references = AccountTypeReference::active()->get();
    }
    public function loadLoadersReference()
    {
        $this->loaders_references = Loaders::active()->where([
            ['name', 'like', '%' . $this->loader_search . '%']
        ])->orderBy('name', 'asc')->take(10)->get();
    }

    public function loadFreightReferenceTypeReference()
    {
        $this->freight_reference_type_references = FreightReferenceType::active()->get();
    }

    public function loadFreightUsageReference()
    {
        $this->freight_usage_references = FreightUsageReference::active()->get();
    }

    public function loadTruckingTypeReference()
    {
        $this->trucking_type_references = TruckingTypeReference::active()->get();
    }

    public function loadOpexReference()
    {
        $this->opex_type_references = OpexType::get();
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function loadBudgetSourceReference()
    {
        // if($this->division == 7 ){
        //     $this->budget_source_references = BudgetSource::where('division_id', $this->division)->where('id', 29)->get();
        // }else{
        $this->budget_source_references = BudgetSource::where('division_id', $this->division)->get();
        // }

    }

    public function loadBudgetChartReference()
    {
        $this->budget_chart_references = BudgetChart::where([
            ['division_id', $this->division],
            ['budget_source_id', $this->budget_source],
            ['name', 'like', '%' . trim($this->chart_of_accounts_search) . '%']
        ])->orderBy('name', 'asc')->get();

        // dd($this->budget_chart_references);
    }

    public function loadBudgetPlanReference()
    {

        // $this->yearcreateds = BudgetChart::where('id', $this->chart_of_accounts)->select('created_at')->get();
        // $this->yearcreated = $this->yearcreateds["created_at"];
        // if (date("Y", strtotime($this->yearcreated) == date("Y"))){
        //     $datesnow = date("Y");
        // }else{
        //     $datesnow = 2022;
        // }

        // dd($this->division,$this->budget_source,$this->chart_of_accounts);

        $this->budget_plan_references = BudgetPlan::where([
            ['division_id', $this->division],
            ['budget_source_id', $this->budget_source],
            ['budget_chart_id', $this->chart_of_accounts],
            ['total_amount', '>', 0],
            // ['year', $datesnow]
        ])->get();

        // dd($this->budget_plan_references, $this->chart_of_accounts);

    }

    public function loadCanvassingSupplierReference()
    {
        $this->canvassing_supplier_references = CanvassingSupplier::with(['supplier', 'canvassingSupplierItem' => function ($query) {
            $query->with('supplierItem')->where('final_status', true);
        }])
            ->where('po_reference_no', '!=', null)
            // ->when(Auth::user()->level_id < 3, function ($query) {
            //     $query->whereHas('canvassing', function ($query) {
            //         $query->where('created_by', Auth::user()->id);
            //     });
            // })
            ->whereDoesntHave('requestForPayment')
            ->where('po_reference_no', 'like', '%' . $this->po_reference_no . '%')->orderBy('created_at', 'desc')->take(10)->get();
    }

    public function loadCAReferenceTypes()
    {
        $this->ca_reference_types_references = CAReferenceTypes::get();
    }

    // protected function updatedPayee()
    // {
    //     if ($this->payee) {
    //         $supplier = Supplier::with('branch')->find($this->payee);
    //         if ($supplier->branch) {
    //             $this->branch = $supplier->branch->id;
    //         }
    //     }
    // }

    protected function updatedDivision()
    {
        $this->loadApprovers();
        $this->loadBudgetPlanReference();
        $this->loadBudgetSourceReference();
        $this->budget_chart_references = [];
        $this->budget_plan_references = [];
        $this->reset([
            'budget_source',
            'chart_of_accounts_search',
            'chart_of_accounts',
            'coa_category',

            'approver_1_id',
            'approver_2_id',
            'approver_3_id',
            'approver_1_name',
            'approver_2_name',
            'approver_3_name',
        ]);
    }

    protected function updatedBudgetSource()
    {
        $this->loadBudgetPlanReference();
        $this->loadBudgetChartReference();
        $this->budget_plan_references = [];
        $this->reset([
            'chart_of_accounts_search',
            'chart_of_accounts',
            'coa_category',

            'approver_1_id',
            'approver_2_id',
            'approver_3_id',
            'approver_1_name',
            'approver_2_name',
            'approver_3_name',
        ]);
    }

    protected function updatedChartOfAccountsSearch()
    {
        $budget_chart = BudgetChart::where([
            ['division_id', $this->division],
            ['budget_source_id', $this->budget_source],
            ['name', trim($this->chart_of_accounts_search)]
        ])->first();




        if ($budget_chart) {
            $this->chart_of_accounts = $budget_chart->id;
            // dd($budget_chart->id);
        } else {
            $this->reset('chart_of_accounts');
        }

        $this->loadBudgetPlanReference();
        $this->loadBudgetChartReference();
        $this->reset([
            'coa_category',

            'approver_1_id',
            'approver_2_id',
            'approver_3_id',
            'approver_1_name',
            'approver_2_name',
            'approver_3_name',
        ]);
    }

    public function getBudgetPlanBalanceFreight($month, $transaction_date)
    {
        return BudgetChart::withSum(['transferBudgetFrom' => function ($query) use ($transaction_date) {
            $query->where('month_from', strtolower(date('F', strtotime($transaction_date))))
                ->where('year', date('Y', strtotime($transaction_date)));
        }], 'amount')
            ->withSum(['transferBudgetTo' => function ($query) use ($transaction_date) {
                $query->where('month_to', strtolower(date('F', strtotime($transaction_date))))
                    ->where('year', date('Y', strtotime($transaction_date)));
            }], 'amount')
            ->withSum(['availment' => function ($query) use ($transaction_date) {
                $query->where('month', strtolower(date('F', strtotime($transaction_date))))
                    ->where('year', date('Y', strtotime($transaction_date)));
            }], 'amount')
            ->withSum(['budgetPlan' => function ($query) use ($transaction_date) {
                $query->where('year', date('Y', strtotime($transaction_date)));
            }], strtolower(date('F', strtotime($transaction_date))))
            // ->withSum('budgetPlan', strtolower(date('F', strtotime($request_for_payment->date_of_transaction_from))))
            ->find($this->chart_of_accounts);

      
        // dd($total_amount, $budget_chart, $budget_chart['budget_plan_sum_' . strtolower(date('F', strtotime($transaction_date)))], $budget_chart->transfer_budget_to_sum_amount);
    }

    public function getBudgetPlanBalance($month, $transaction_date)
    {

        return BudgetPlan::with(['chart' => function ($query) use ($month, $transaction_date) {
            $query->withSum(['transferBudgetFrom' => function ($query) use ($month, $transaction_date) {
                $query->where('month_from', $month)->where('year', date('Y', strtotime($transaction_date)))
                    ->where('year', date('Y', strtotime($transaction_date)));
            }], 'amount')
                ->withSum(['transferBudgetTo' => function ($query) use ($month, $transaction_date) {
                    $query->where('month_to', $month)->where('year', date('Y', strtotime($transaction_date)))
                        ->where('year', date('Y', strtotime($transaction_date)));
                }], 'amount')
                ->withSum(['availment' => function ($query) use ($month, $transaction_date) {
                    $query->where('month', $month)->where('year', date('Y', strtotime($transaction_date)));
                }], 'amount')
                ->withSum(['budgetPlan' => function ($query) use ($month, $transaction_date) {
                    $query->where('year', date('Y', strtotime($transaction_date)));
                }], strtolower(date('F', strtotime($transaction_date))));
            // ->withSum('budgetPlan', strtolower(date('F', strtotime($transaction_date))));
        }, 'budgetLoa' => function ($query) {
            $query->with('firstApprover', 'secondApprover', 'thirdApprover');
        }, 'division'])
            ->withSum(['transferBudgetFrom' => function ($query) use ($month, $transaction_date) {
                $query->where('month_from', $month)->where('year', date('Y', strtotime($transaction_date)));
            }], 'amount')
            ->withSum(['transferBudgetTo' => function ($query) use ($month, $transaction_date) {
                $query->where('month_to', $month)->where('year', date('Y', strtotime($transaction_date)));
            }], 'amount')
            ->withSum(['availment' => function ($query) use ($month, $transaction_date) {
                $query->where('month', $month)->where('year', date('Y', strtotime($transaction_date)));
            }], 'amount')
            ->findOrFail($this->coa_category);
    }

    protected function updatedCoaCategory()
    {
        if ($this->request) {
            if ($this->request->type_id == 2 && $this->request->account_type_id == 1) {
                $this->month = strtolower(date('F', strtotime(now())));
            } else if ($this->request->type_id == 3 && $this->request->requestForPaymentDetails[0]->payment_type_id == 2) {
                $this->month = strtolower(date('F', strtotime(now())));
            } else {
                $this->month = strtolower(date('F', strtotime($this->request->date_of_transaction_from)));;
            }
        }

        $this->budget_plan = $this->getBudgetPlanBalance($this->month, $this->date_of_transaction_from);

        $this->rfp_type = $this->budget_plan->rfp_type_id;
        $this->opex_type = $this->budget_plan->opex_type_id;
        if ($this->rfp_type == 2) {
            $this->loadAccountingAccountTypeReference();
            $this->loadLoadersReference();
            $this->loadFreightReferenceTypeReference();
            $this->loadFreightUsageReference();
            $this->loadTruckingTypeReference();
        } else if ($this->rfp_type == 4) {
            $this->generateCANumber();
        }

        $this->getApprover();
    }

    protected function updatedIsPayeeContactPerson()
    {
        $this->loadSupplierReference();
    }

    protected function updatedSupplierSearch()
    {
        $this->reset('payee');
        $this->loadSupplierReference();
    }

    protected function updatedRequestForPayments()
    {
        $this->validate([
            'request_for_payments.*.labor_cost' => 'required|numeric',
            'request_for_payments.*.unit_cost' => 'required|numeric',
            'request_for_payments.*.quantity' => 'required|numeric',
        ], [
            'request_for_payments.*.labor_cost.required' => 'Must be numeric',
            'request_for_payments.*.unit_cost.required' => 'Must be numeric',
            'request_for_payments.*.quantity.required' => 'Must be numeric',
            'request_for_payments.*.labor_cost.numeric' => 'Must be numeric',
            'request_for_payments.*.unit_cost.numeric' => 'Must be numeric',
            'request_for_payments.*.quantity.numeric' => 'Must be numeric',
        ]);

        $request_for_payments_collections = collect($this->request_for_payments);
        foreach ($this->request_for_payments as $i => $request_for_payment) {
            $this->request_for_payments[$i]['amount'] = ($request_for_payment['unit_cost'] * $request_for_payment['quantity']) + ($request_for_payment['labor_cost'] ? $request_for_payment['labor_cost'] : 0);
        }
        $totals = $request_for_payments_collections->map(function ($item, $key) {
            return ['total' => ($item['unit_cost'] * $item['quantity']) + ($item['labor_cost'] ? $item['labor_cost'] : 0)];
        });
        $this->total_amount = $totals->sum('total');

        $this->getApprover();
    }

    protected function updatedFreights()
    {
        $this->validate([
            'freights.*.trucking_amount' => 'required|numeric',
            'freights.*.freight_amount' => 'required|numeric',
            'freights.*.allowance' => 'required|numeric',
        ], [
            'freights.*.trucking_amount.required' => 'Must be numeric',
            'freights.*.freight_amount.required' => 'Must be numeric',
            'freights.*.allowance.required' => 'Must be numeric',
            'freights.*.trucking_amount.numeric' => 'Must be numeric',
            'freights.*.freight_amount.numeric' => 'Must be numeric',
            'freights.*.allowance.numeric' => 'Must be numeric',
        ]);

        $freights_collections = collect($this->freights);
        foreach ($this->freights as $i => $freight) {
            $this->freights[$i]['amount'] = ($freight['trucking_amount'] + $freight['freight_amount'] + $freight['allowance']);
        }
        $this->total_amount = $freights_collections->sum('trucking_amount') +
            $freights_collections->sum('freight_amount') +
            $freights_collections->sum('allowance');
        $this->getApprover();
    }

    protected function updatedPayables()
    {
        $this->validate([
            'payables.*.invoice_amount' => 'required|numeric',
        ], [
            'payables.*.invoice_amount.required' => 'Must be numeric',
            'payables.*.invoice_amount.numeric' => 'Must be numeric',
        ]);

        $payables_collections = collect($this->payables);
        $this->total_amount = $payables_collections->sum('invoice_amount');
        $this->getApprover();
    }

    protected function updatedCashAdvances()
    {
        $this->validate([
            'cash_advances.*.unit_cost' => 'required|numeric',
            'cash_advances.*.quantity' => 'required|numeric',
        ], [
            'cash_advances.*.unit_cost.required' => 'Must be numeric',
            'cash_advances.*.quantity.required' => 'Must be numeric',
            'cash_advances.*.unit_cost.numeric' => 'Must be numeric',
            'cash_advances.*.quantity.numeric' => 'Must be numeric',
        ]);
        $cash_advances_collections = collect($this->cash_advances);
        foreach ($this->cash_advances as $i => $cash_advance) {
            $this->cash_advances[$i]['amount'] = ($cash_advance['unit_cost'] * $cash_advance['quantity']);
        }
        $totals = $cash_advances_collections->map(function ($item, $key) {
            return ['total' => ($item['unit_cost'] * $item['quantity'])];
        });
        $this->total_amount = $totals->sum('total');
        $this->getApprover();
    }

    public function getApprover()
    {
        if ($this->budget_plan && $this->budget_plan->budgetLoa) {
            $approvers = $this->budget_plan->budgetLoa()->where([
                ['limit_min_amount', '<=', $this->total_amount],
                ['limit_max_amount', '>=', $this->total_amount]
            ])->first();

            if ($this->is_set_approver_1) {
                if ($approvers) {
                    $this->approver_2_id = $approvers->second_approver_id;
                    $this->approver_3_id = $approvers->third_approver_id;
                    $this->approver_1_name = $approvers->firstApprover ? $approvers->firstApprover->name : '';
                    $this->approver_2_name = $approvers->secondApprover  ? $approvers->secondApprover->name : '';
                    $this->approver_3_name = $approvers->thirdApprover  ? $approvers->thirdApprover->name : '';
                } else {
                    $this->reset([
                        'approver_2_id',
                        'approver_3_id',
                        'approver_1_name',
                        'approver_2_name',
                        'approver_3_name',
                    ]);
                }
            } else if ($approvers) {
                $this->approver_1_id = $approvers->first_approver_id;
                $this->approver_2_id = $approvers->second_approver_id;
                $this->approver_3_id = $approvers->third_approver_id;
                $this->approver_1_name = $approvers->firstApprover ? $approvers->firstApprover->name : '';
                $this->approver_2_name = $approvers->secondApprover  ? $approvers->secondApprover->name : '';
                $this->approver_3_name = $approvers->thirdApprover  ? $approvers->thirdApprover->name : '';
            } else {
                $this->reset([
                    'approver_1_id',
                    'approver_2_id',
                    'approver_3_id',
                    'approver_1_name',
                    'approver_2_name',
                    'approver_3_name',
                ]);
            }
        }
    }

    protected function updatedPoReferenceNo()
    {
        $this->loadCanvassingSupplierReference();
        $this->canvassing_supplier = CanvassingSupplier::with(['canvassing', 'supplier', 'canvassingSupplierItem' => function ($query) {
            $query->with('supplierItem')->where('final_status', true);
        }])->when(Auth::user()->level_id < 3, function ($query) {
            $query->whereHas('canvassing', function ($query) {
                $query->where('created_by', Auth::user()->id);
            });
        })
            ->whereDoesntHave('requestForPayment')
            ->where('po_reference_no', $this->po_reference_no)->first();

        $this->request_for_payments = [];
        if ($this->canvassing_supplier) {
            $this->canvassing_supplier_id = $this->canvassing_supplier->id;
            $this->payee = $this->canvassing_supplier->supplier_id;
            $this->branch = $this->canvassing_supplier->supplier->branch->id;

            foreach ($this->canvassing_supplier->canvassingSupplierItem as $canvassing_supplier_item) {
                $this->request_for_payments[] = [
                    'id' => null,
                    'particulars' => $canvassing_supplier_item->supplierItem->name,
                    'plate_no' => null,
                    'labor_cost' => 0,
                    'unit_cost' => $canvassing_supplier_item->unit_cost,
                    'quantity' => $canvassing_supplier_item->quantity,
                    'amount' => $canvassing_supplier_item->total_amount,
                    'is_deleted' => false,
                ];
            }

            $this->updatedRequestForPayments();
        }
    }

    protected function updatedCaReferenceType()
    {
        $this->ca_reference_type_display = (CAReferenceTypes::find($this->ca_reference_type))->display;
    }

    protected function updatedLoaderSearch()
    {
        $loader = Loaders::where([
            ['name', $this->loader_search]
        ])->first();

        if ($loader) {
            $this->loader = $loader->id;
        } else {
            $this->reset('loader');
        }

        $this->loadLoadersReference();
    }

    protected function updatedAttachments()
    {
        $this->validate([
            'attachments' => 'nullable',
            'attachments.*.attachment' => 'nullable|' . config('filesystems.validation_all'),
        ], [
            'attachments.*.attachment.mimes' => 'The attachment must be one of this jpg,jpeg,png,xlsx,doc,docx,csv,pdf.',
        ]);
    }

    public function getSupplier($id)
    {
        $supplier = Supplier::find($id);
        if ($supplier) {
            $this->payee = $supplier->id;
            $this->supplier_search = $supplier->company;

            if ($supplier->branch) {
                $this->branch = $supplier->branch->id;
            }
        }
    }

    public function addAttachments()
    {
        $this->attachments[] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }

    public function addRequestForPayments()
    {
        $this->request_for_payments[] = [
            'id' => null,
            'particulars' => null,
            'plate_no' => null,
            'labor_cost' => 0,
            'unit_cost' => 0,
            'quantity' => 0,
            'amount' => 0,
            'is_deleted' => false,
        ];
    }

    public function addFreights()
    {
        $this->freights[] = [
            'id' => null,
            'freight_reference_no' => null,
            'freight_reference_type' => null,
            'soa_no' => null,
            'trucking_category' => null,
            'trucking_amount' => 0,
            'freight_amount' => 0,
            'freight_usage' => null,
            'transaction_date' => null,
            'allowance' => 0,
            'amount' => 0,
            'is_deleted' => false,
        ];
    }

    public function addPayables()
    {
        $this->payables[] = [
            'id' => null,
            'invoice_no' => null,
            'invoice_amount' => 0,
            'date_of_transaction' => null,
            'is_deleted' => false,
        ];
    }

    public function addCashAdvances()
    {
        $this->cash_advances[] = [
            'id' => null,
            'particulars' => null,
            'ca_reference_no' => null,
            'unit_cost' => 0,
            'quantity' => 0,
            'amount' => 0,
            'is_deleted' => false,
        ];
    }

    public function removeAttachments($i)
    {
        if (count(collect($this->attachments)->where('is_deleted', false)) > 1) {
            if ($this->attachments[$i]['id']) {
                $this->attachments[$i]['is_deleted'] = true;
            } else {
                unset($this->attachments[$i]);
            }

            array_values($this->attachments);
        }
    }

    public function removeRequestForPayments($i)
    {
        if (count($this->request_for_payments) > 1) {
            if ($this->request_for_payments[$i]['id']) {
                $this->request_for_payments[$i]['is_deleted'] = true;
            } else {
                unset($this->request_for_payments[$i]);
            }
            array_values($this->request_for_payments);
        }
    }

    public function removeFreights($i)
    {
        if (count($this->freights) > 1) {
            if ($this->freights[$i]['id']) {
                $this->freights[$i]['is_deleted'] = true;
            } else {
                unset($this->freights[$i]);
            }
            array_values($this->freights);
        }
    }

    public function removePayables($i)
    {
        if (count($this->payables) > 1) {
            if ($this->payables[$i]['id']) {
                $this->payables[$i]['is_deleted'] = true;
            } else {
                unset($this->payables[$i]);
            }
            array_values($this->payables);
        }
    }

    public function removeCashAdvances($i)
    {
        if (count($this->cash_advances) > 1) {
            if ($this->cash_advances[$i]['id']) {
                $this->cash_advances[$i]['is_deleted'] = true;
            } else {
                unset($this->cash_advances[$i]);
            }
            array_values($this->cash_advances);
        }
    }

    public function download($id)
    {
        if ($this->request) {
            $document = $this->request->attachments()->find($id);
            if ($document) {
                if (Storage::disk('accounting_gcs')->exists($document->path . $document->name)) {
                    return Storage::disk('accounting_gcs')->download($document->path . $document->name, $document->id . '-' . $this->request->reference_id . '.' . $document->extension);
                } else {
                    $this->sweetAlert('error', 'File doesn`t exist in cloud.');
                }
            }
        }
    }

    public function resetForm()
    {
        $this->reset([
            'reference_number',
            'rfp_type',

            'supplier_search',
            'payee',
            'remarks',
            'description',
            'amount',
            'multiple_budget',
            'branch',
            'coa_category',
            'date_needed',
            'priority',
            'date_of_transaction_from',
            'date_of_transaction_to',
            'canvasser',
            'accounting_account_type',
            'is_set_approver_1',
            'approver_1_id',
            'approver_2_id',
            'approver_3_id',
            'is_approved_1',
            'is_approved_2',
            'is_approved_3',
            'approver_remarks_1',
            'approver_remarks_2',
            'approver_remarks_3',
            'opex_type',
            'attachments',

            'account_no',
            'payment_type',
            'terms',
            'pdc_from',
            'pdc_to',
            'pdc_date',

            'request_for_payments',
            'freights',
            'payables',
            'cash_advances',

            'division',
            'budget_source',
            'chart_of_accounts',
            'budget_plan',

            'canvassing_supplier',
            'canvassing_supplier_id',
            'po_reference_no',

            'ca_no',
            'ca_reference_type',
            'ca_reference_type_display',

            'subpayee',
            'is_subpayee',
        ]);
    }
}
