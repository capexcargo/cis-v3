<?php

namespace App\Traits\Accounting;

use App\Models\Accounting\LiquidationStatusReference;
use App\Repositories\Accounting\LiquidationManagementRepository;
use Illuminate\Support\Facades\Storage;

trait LiquidationManagementTrait
{
    public $confirmation_modal = false;
    public $request_for_payment_modal = false;
    public $liquidation_details_attachments_modal = false;
    public $liquidation_details_request_for_payment_modal = false;
    public $confirmation_message;

    public $liquidation_reference_no;
    public $request_total_amount;
    public $liquidation_total_amount;
    public $status = 1;

    public $request_for_payment_search;
    public $request_for_payments = [];
    public $liquidation_details = [];

    public $request_for_payment_references = [];
    public $liquidation_status_references = [];

    public $a;
    public $b;
    public $action_type;
    public $where_request_for_payments = 'where_not_in';
    public $liquidation;

    public function generateLiquidationReferenceNumber()
    {
        $this->liquidation_reference_no = "LIQ-" . date("ymdhis") . "-" . rand(111, 999);
    }

    public function load()
    {
        $this->loadRequestForPaymentReference();
        $this->loadLiquidationStatusReference();
    }

    public function loadRequestForPaymentReference()
    {
        $request = [
            'request_for_payment_search' => $this->request_for_payment_search,
            'where_request_for_payments' => $this->where_request_for_payments,
            'request_for_payments' => $this->request_for_payments,
            'liquidation_details' => $this->liquidation_details,
            'liquidation_details_index' => $this->a,
        ];

        $liquidation_management_interface = new LiquidationManagementRepository();
        $response = $liquidation_management_interface->loadRequestForPaymentReference($request);
        if ($response['code'] == 200) {
            $this->request_total_amount = $response['result']['request_total_amount'];
            $this->request_for_payment_references = $response['result']['request_for_payments'];
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            return $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function loadLiquidationStatusReference()
    {
        $this->liquidation_status_references = LiquidationStatusReference::active()->get();
    }

    protected function updatedRequestForPaymentSearch()
    {
        $this->loadRequestForPaymentReference();
    }

    protected function updatedLiquidationDetails()
    {
        $this->validate([
            'liquidation_details.*.amount' => 'required|numeric',
            'liquidation_details.*.cash_on_hand' => 'required|numeric',
        ], [
            'liquidation_details.*.amount.required' => 'Must be numeric',
            'liquidation_details.*.cash_on_hand.required' => 'Must be numeric',
            'liquidation_details.*.amount.numeric' => 'Must be numeric',
            'liquidation_details.*.cash_on_hand.numeric' => 'Must be numeric',
        ]);

        foreach ($this->liquidation_details as $i => $liquidation_detail) {
            $this->liquidation_details[$i]['total_amount'] = ($liquidation_detail['amount'] + $liquidation_detail['cash_on_hand']);
        }

        $liquidation_details_collections = collect($this->liquidation_details)->where('is_deleted', false);
        $this->liquidation_total_amount = $liquidation_details_collections->sum('total_amount');
    }

    protected function updatedLiquidationDetailsAttachmentsModal()
    {
        if ($this->action_type == 'liquidation_details_attachments') {
            $this->liquidation_details_attachments_modal = true;
            if (!$this->confirmation_modal) {
                $this->reset('action_type');
            }
        }
    }

    protected function updatedLiquidationDetailsRequestForPaymentModal()
    {
        if ($this->where_request_for_payments == 'where_in') {
            $this->liquidation_details_request_for_payment_modal = true;
            if (!$this->request_for_payment_modal) {
                $this->reset('where_request_for_payments');
            }
        } elseif ($this->action_type == 'liquidation_details_request_for_payment') {
            $this->liquidation_details_request_for_payment_modal = true;
            if (!$this->confirmation_modal) {
                $this->reset('action_type');
            }
        }
    }

    public function showRequestForPaymentReference($where_request_for_payments)
    {
        $this->request_for_payment_modal = true;
        $this->where_request_for_payments = $where_request_for_payments;
        $this->loadRequestForPaymentReference();
    }

    public function action(array $data, $action_type)
    {
        $this->a = $data['a'];
        $this->action_type = $action_type;
        if ($action_type == 'request_for_payment') {
            $this->confirmation_message = "Are you sure to remove this request.";
            $this->confirmation_modal = true;
        } else if ($action_type == 'liquidation_details') {
            $this->confirmation_message = "Are you sure to remove this liquidation detail.";
            $this->confirmation_modal = true;
        } elseif ($action_type == 'liquidation_details_attachments_list') {
            $this->liquidation_details_attachments_modal = true;
        } else if ($action_type == 'liquidation_details_attachments') {
            $this->b = $data['b'];
            $this->confirmation_message = "Are you sure to remove this liquidation details attachment.";
            $this->confirmation_modal = true;
        } else if ($action_type == 'liquidation_details_request_for_payment_list') {
            $this->liquidation_details_request_for_payment_modal = true;
        } else if ($action_type == 'liquidation_details_request_for_payment') {
            $this->b = $data['b'];
            $this->confirmation_message = "Are you sure to remove this liquidation details request.";
            $this->confirmation_modal = true;
        }
    }

    public function addRequestForPayment($id)
    {
        $request = [
            'where_request_for_payments' => $this->where_request_for_payments,
            'request_for_payments' => $this->request_for_payments,
            'liquidation_details' => $this->liquidation_details,
            'liquidation_details_index' => $this->a,
        ];

        $liquidation_management_interface = new LiquidationManagementRepository();
        $response = $liquidation_management_interface->showRequestForPaymentReference($id, $request);

        if ($response['code'] == 200) {
            $this->request_total_amount = $response['result']['request_total_amount'];
            $request_for_payment = $response['result']['request_for_payment'];

            if ($this->where_request_for_payments == 'where_not_in') {
                $this->request_for_payments[] = [
                    'id' => null,
                    'request_for_payment_id' => $request_for_payment->id,
                    'reference_id' => $request_for_payment->reference_id,
                    'pay_to' => $request_for_payment->payee->company,
                    'opex_type' => $request_for_payment->opex->name,
                    'requested_by' => $request_for_payment->user->name,
                    'amount' => $request_for_payment->amount,
                    'is_deleted' => false,
                ];
            } elseif ($this->where_request_for_payments == 'where_in') {
                $this->liquidation_details[$this->a]['request_for_payments'][] = [
                    'id' => null,
                    'request_for_payment_id' => $request_for_payment->id,
                    'reference_id' => $request_for_payment->reference_id,
                    'pay_to' => $request_for_payment->payee->company,
                    'opex_type' => $request_for_payment->opex->name,
                    'requested_by' => $request_for_payment->user->name,
                    'amount' => $request_for_payment->amount,
                    'is_deleted' => false,
                ];
            }

            $this->loadRequestForPaymentReference();
        } else if ($response['code'] == 400) {
            foreach ($response['result'] as $a => $result) {
                $this->addError($a, $result);
            }
            return;
        } else {
            return $this->sweetAlertError('error', $response['message'], $response['result']);
        }
    }

    public function addLiquidationDetail()
    {
        $this->liquidation_details[] = [
            'id' => null,
            'or_no' => null,
            'transaction_date' => null,
            'amount' => 0,
            'cash_on_hand' => 0,
            'description' => null,
            'remarks' => null,
            'total_amount' => 0,
            'is_deleted' => false,

            'attachments' => [],
            'request_for_payments' => []
        ];
    }

    public function addLiquidationDetailAttachments()
    {
        $this->liquidation_details[$this->a]['attachments'][] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }

    public function remove()
    {
        if ($this->action_type == 'request_for_payment') {
            foreach ($this->liquidation_details as $a => $liquidation_detail) {
                $request_for_payments = collect($liquidation_detail['request_for_payments'])->where('request_for_payment_id', $this->request_for_payments[$this->a]['request_for_payment_id']);
                foreach ($request_for_payments as $b => $request_for_payment) {
                    if ($this->liquidation_details[$a]['request_for_payments'][$b]['id']) {
                        $this->liquidation_details[$a]['request_for_payments'][$b]['is_deleted'] = true;
                    } else {
                        unset($this->liquidation_details[$a]['request_for_payments'][$b]);
                    }
                }
            }

            array_values($this->liquidation_details);

            if (count(collect($this->request_for_payments)->where('is_deleted', false)) > 1) {
                if ($this->request_for_payments[$this->a]['id']) {
                    $this->request_for_payments[$this->a]['is_deleted'] = true;
                } else {
                    unset($this->request_for_payments[$this->a]);
                }

                array_values($this->request_for_payments);
                $this->loadRequestForPaymentReference();
            }
        } elseif ($this->action_type == 'liquidation_details') {
            if (count(collect($this->liquidation_details)->where('is_deleted', false)) > 1) {
                if ($this->liquidation_details[$this->a]['id']) {
                    $this->liquidation_details[$this->a]['is_deleted'] = true;
                } else {
                    unset($this->liquidation_details[$this->a]);
                }

                array_values($this->liquidation_details);
            }
        } elseif ($this->action_type == 'liquidation_details_attachments') {
            if (count(collect($this->liquidation_details[$this->a]['attachments'])->where('is_deleted', false)) > 1) {
                if ($this->liquidation_details[$this->a]['attachments'][$this->b]['id']) {
                    $this->liquidation_details[$this->a]['attachments'][$this->b]['is_deleted'] = true;
                } else {
                    unset($this->liquidation_details[$this->a]['attachments'][$this->b]);
                }

                array_values($this->liquidation_details);
            }
        } elseif ($this->action_type == 'liquidation_details_request_for_payment') {
            if (count(collect($this->liquidation_details[$this->a]['request_for_payments'])->where('is_deleted', false)) > 1) {
                if ($this->liquidation_details[$this->a]['request_for_payments'][$this->b]['id']) {
                    $this->liquidation_details[$this->a]['request_for_payments'][$this->b]['is_deleted'] = true;
                } else {
                    unset($this->liquidation_details[$this->a]['request_for_payments'][$this->b]);
                }

                array_values($this->liquidation_details);
            }
        }

        $this->updatedLiquidationDetails();
        $this->reset([
            'confirmation_message',
        ]);
        $this->confirmation_modal = false;
    }

    public function download($id)
    {
        if ($this->request_for_payment) {
            $document = $this->request_for_payment->liquidation->liquidationDetails->map->attachments->flatten()->where('id', $id)->first();
            if ($document) {
                if (Storage::disk('accounting_gcs')->exists($document->path . $document->name)) {
                    return Storage::disk('accounting_gcs')->download($document->path . $document->name, $document->id . '-' . $this->request_for_payment->liquidation->liquidation_reference_no . '.' . $document->extension);
                } else {
                    $this->sweetAlert('error', 'File doesn`t exist in cloud.');
                }
            }
        }
    }

    public function resetForm()
    {
        $this->reset([
            'liquidation_reference_no',
            'request_total_amount',
            'liquidation_total_amount',
            'status',

            'request_for_payments',
            'liquidation_details',
            'a',
            'b',
        ]);
        $this->resetValidation();
    }
}
