<?php

namespace App\Traits\Accounting\BudgetManagement;

use App\Models\Accounting\BudgetSource;
use App\Models\Division;

trait BudgetChartTrait
{
    public $division;
    public $source;
    public $name;

    public $division_references = [];
    public $budget_source_references = [];

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function loadBudgetSourceReference()
    {
        $this->budget_source_references = BudgetSource::where('division_id', $this->division)->get();
    }

    protected function updatedDivision()
    {
        $this->loadBudgetSourceReference();
    }

    public function resetForm()
    {
        $this->reset([
            'division',
            'source',
            'name'
        ]);
    }
}
