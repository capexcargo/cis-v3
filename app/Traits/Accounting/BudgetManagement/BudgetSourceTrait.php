<?php

namespace App\Traits\Accounting\BudgetManagement;

use App\Models\Division;

trait BudgetSourceTrait
{
    public $division;
    public $name;

    public $division_references = [];

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function resetForm()
    {
        $this->reset([
            'division',
            'name'
        ]);
    }

}
