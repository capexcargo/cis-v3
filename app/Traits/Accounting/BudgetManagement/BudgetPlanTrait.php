<?php

namespace App\Traits\Accounting\BudgetManagement;

use App\Models\Accounting\BudgetChart;
use App\Models\Accounting\BudgetSource;
use App\Models\Accounting\OpexType;
use App\Models\Accounting\RequestForPaymentTypeReference;
use App\Models\Division;

trait BudgetPlanTrait
{
    public $division;
    public $source;
    public $chart;
    public $item;
    public $opex_type;
    public $year;
    public $request_type;

    public $january = 0;
    public $february = 0;
    public $march = 0;
    public $april = 0;
    public $may = 0;
    public $june = 0;
    public $july = 0;
    public $august = 0;
    public $september = 0;
    public $october = 0;
    public $november = 0;
    public $december = 0;

    public $opex_type_references = [];
    public $division_references = [];
    public $source_references = [];
    public $chart_references = [];
    public $request_for_pament_type_references = [];

    public function load()
    {
        $this->loadOpexTypeReference();
        $this->loadDivisionReference();
        $this->loadRequestForPaymentTypeReference();
    }

    public function loadOpexTypeReference()
    {
        $this->opex_type_references = OpexType::get();
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }
    
    public function loadSourceReference()
    {
        $this->source_references = BudgetSource::where('division_id', $this->division)->get();
    }

    public function loadChartReference()
    {
        $this->chart_references = BudgetChart::where([
            ['division_id', $this->division],
            ['budget_source_id', $this->source]
        ])->get();
    }

    public function loadRequestForPaymentTypeReference()
    {
        $this->request_for_pament_type_references = RequestForPaymentTypeReference::active()->get();
    }

    protected function updatedDivision()
    {
        $this->loadSourceReference();
        $this->reset([
            'source',
            'chart',
        ]);
        $this->chart_references = [];
    }

    protected function updatedSource()
    {
        $this->reset([
            'chart',
        ]);
        $this->loadChartReference();
    }

    public function resetForm()
    {
        $this->reset([
            'division',
            'source',
            'chart',
            'item',
            'opex_type',
            'year',

            'january',
            'february',
            'march',
            'april',
            'may',
            'june',
            'july',
            'august',
            'september',
            'october',
            'november',
            'december',
        ]);
    }
}
