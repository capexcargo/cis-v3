<?php

namespace App\Traits\Accounting\BudgetManagement;

use App\Models\Accounting\BudgetChart;
use App\Models\Accounting\BudgetLoa;
use App\Models\Accounting\BudgetPlan;
use App\Models\Accounting\BudgetSource;
use App\Models\Division;
use App\Models\User;

trait BudgetLoaTrait
{
    public $division;
    public $budget_source;
    public $budget_chart;
    public $budget_plan;
    public $limit_min_amount;
    public $limit_max_amount;

    public $first_approver_search;
    public $first_approver;

    public $second_approver_search;
    public $second_approver;

    public $third_approver_search;
    public $third_approver;


    public $division_references = [];
    public $budget_source_references = [];
    public $budget_chart_references = [];
    public $budget_plan_references = [];
    public $first_approver_references = [];
    public $second_approver_references = [];
    public $third_approver_references = [];

    public function load()
    {
        $this->loadDivisionReference();
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function loadBudgetSourceReference()
    {
        $this->budget_source_references = BudgetSource::where('division_id', $this->division)->get();
    }

    public function loadBudgetChartReference()
    {
        $this->budget_chart_references = BudgetChart::where([
            ['division_id', $this->division],
            ['budget_source_id', $this->budget_source],
        ])->get();
    }

    public function loadBudgetPlanReference()
    {
        $this->budget_plan_references = BudgetPlan::withCount('budgetLoa')->where([
            ['division_id', $this->division],
            ['budget_source_id', $this->budget_source],
            ['budget_chart_id', $this->budget_chart],
            ['year', date("Y")]
        ])->get();
    }

    public function loadFirstApproverReferences()
    {
        $this->first_approver_references = User::where([
            ['division_id', $this->division],
            ['name', 'like', '%' . $this->first_approver_search . '%']
        ])
            ->whereBetween('level_id', [2, 4])
            ->orderBy('name', 'asc')
            ->take(10)
            ->get();
    }

    public function loadSecondApproverReferences()
    {
        $this->second_approver_references = User::where([
            ['name', 'like', '%' . $this->second_approver_search . '%']
        ])
            ->whereBetween('level_id', [2, 5])
            ->orderBy('name', 'asc')
            ->take(10)
            ->get();
    }

    public function loadThirdApproverReferences()
    {
        $this->third_approver_references = User::where([
            ['name', 'like', '%' . $this->third_approver_search . '%']
        ])
            ->whereBetween('level_id', [2, 5])
            ->orderBy('name', 'asc')
            ->take(10)
            ->get();
    }

    protected function updatedDivision()
    {
        $this->reset([
            'first_approver_references',
            'second_approver_references',
            'third_approver_references'
        ]);
        $this->loadFirstApproverReferences();
        $this->loadSecondApproverReferences();
        $this->loadThirdApproverReferences();

        // dd($this->first_approver_references);
        $this->loadBudgetPlanReference();
        $this->loadBudgetSourceReference();
        $this->budget_chart_references = [];
        $this->budget_plan_references = [];
        $this->reset([
            'budget_source',
            'budget_chart',
            'budget_plan',
        ]);
    }

    protected function updatedBudgetSource()
    {
        $this->loadBudgetPlanReference();
        $this->loadBudgetChartReference();
        $this->budget_plan_references = [];
        $this->reset([
            'budget_chart',
            'budget_plan',
        ]);
    }

    protected function updatedBudgetChart()
    {
        $this->loadBudgetPlanReference();
        $this->reset([
            'budget_plan',
        ]);
    }

    public function updatedBudgetPlan()
    {
        $budget_loa = BudgetLoa::where('budget_plan_id', $this->budget_plan)->orderBy('id', 'desc')->first();
        if ($budget_loa) {
            $this->limit_min_amount = $budget_loa->limit_max_amount + 0.01;
        } else {
            $this->limit_min_amount = 0;
        }
    }

    protected function updatedFirstApproverSearch()
    {
        $this->loadFirstApproverReferences();

        $this->reset([
            'first_approver',
        ]);
    }

    protected function updatedSecondApproverSearch()
    {
        $this->loadSecondApproverReferences();

        $this->reset([
            'second_approver',
        ]);
    }

    protected function updatedThirdApproverSearch()
    {
        $this->loadThirdApproverReferences();

        $this->reset([
            'third_approver',
        ]);
    }

    public function getApproverName($id, $type)
    {
        $user = User::find($id);

        if ($user) {
            if ($type == 1) {
                $this->first_approver_search = $user->name;
                $this->first_approver = $user->id;
            } else if ($type == 2) {
                $this->second_approver_search = $user->name;
                $this->second_approver = $user->id;
            } else if ($type == 3) {
                $this->third_approver_search = $user->name;
                $this->third_approver = $user->id;
            }
        }
    }

    public function resetForm()
    {
        $this->reset([
            'division',
            'budget_source',
            'budget_chart',
            'budget_plan',
            'limit_min_amount',
            'limit_max_amount',

            'first_approver_search',
            'first_approver',

            'second_approver_search',
            'second_approver',

            'third_approver_search',
            'third_approver',
        ]);
    }
}
