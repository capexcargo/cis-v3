<?php

namespace App\Traits\Accounting;

use App\Models\Accounting\CheckVoucher;
use App\Models\Accounting\Supplier;

trait CheckVoucherTrait
{
    public $reference_no;
    public $rfp_reference_no;
    public $cv_no;
    public $pay_to;
    public $total_amount;
    public $description;

    public $vouchers = [];
    public $total_voucher_amount = 0;
    public $a = 0;

    public $supplier_search;
    public $supplier_references = [];

    public $request_for_payment;

    public function load()
    {
        $this->loadSupplierReference();
    }

    public function loadSupplierReference()
    {
        $this->supplier_references = Supplier::where('company', 'like', '%' . $this->supplier_search . '%')->take(10)->get();
    }

    protected function updatedSupplierSearch()
    {
        $this->reset('pay_to');
        $this->loadSupplierReference();
    }

    protected function updatedVouchers()
    {

        $this->validate([
            'vouchers.*.amount' => 'required|numeric',
        ], [
            'vouchers.*.amount.required' => 'Must be numeric',
            'vouchers.*.amount.numeric' => 'Must be numeric',
        ]);

        $voucher_collections = collect($this->vouchers)->where('is_deleted', false);

        $this->total_voucher_amount = $voucher_collections->sum('amount');
        // dd($this->total_voucher_amount);
    }

    public function generateReferenceNumber()
    {
        $this->reference_no = "VCH-" . date("ymdhis") . "-" . rand(111, 999);
    }

    public function generateCVNumber()
    {
        $count_cv = CheckVoucher::count();
        $this->cv_no = "CVNO-" . date('Y') . "-" . ($count_cv + 1);
    }

    public function addVoucherItem()
    {
        $this->vouchers[] = [
            'id' => null,
            'voucher_no' => null,
            'description' => null,
            'date' => null,
            'amount' => 0,
            'is_deleted' => false,
        ];
    }

    public function remove(array $data)
    {
        if ($this->vouchers[$data["a"]]['id'] && $this->vouchers[$data["a"]]['id']) {
            $this->vouchers[$data["a"]]['is_deleted'] = true;
        } else {
            unset($this->vouchers[$data["a"]]);
        }

        array_values($this->vouchers);

        $this->updatedVouchers();
    }

    public function getSupplier($id)
    {
        $supplier = Supplier::find($id);
        if ($supplier) {
            $this->pay_to = $supplier->id;
            $this->supplier_search = $supplier->company;
        }
    }

    public function resetForm()
    {
        $this->reset([
            'reference_no',
            'rfp_reference_no',
            'cv_no',
            'pay_to',
            'total_amount',
            'description',

            'vouchers',
            'total_voucher_amount',
            'a',
        ]);
    }
}
