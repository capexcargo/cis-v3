<?php

namespace App\Traits\Accounting;

use App\Interfaces\Accounting\RequestManagementInterface;
use App\Models\Accounting\Supplier;
use App\Models\Accounting\SupplierIndustryReference;
use App\Models\Accounting\SupplierItem;
use App\Models\Accounting\SupplierTypeReference;
use App\Models\BranchReference;

trait CanvassingManagementTrait
{
    public $action_type;
    public $confirmation_modal = false;
    public $confirmation_message;

    public $reference_number;
    public $suppliers = [];
    public $a = 0;
    public $b;

    public $supplier_industry_references = [];
    public $supplier_type_references = [];
    public $branch_references = [];

    public $supplier_references = [];
    public $supplier_item_references = [];

    public function generateReferenceNumber()
    {
        $this->reference_number = "CVS-" . date("ymdhis") . "-" . rand(111, 999);
    }

    public function load()
    {
        $this->loadSupplierIndustryReference();
        $this->loadSupplierTypeReference();
        $this->loadBranchReference();
    }

    public function loadSupplierIndustryReference()
    {
        $this->supplier_industry_references = SupplierIndustryReference::get();
    }

    public function loadSupplierTypeReference()
    {
        $this->supplier_type_references = SupplierTypeReference::get();
    }

    public function loadBranchReference()
    {
        $this->branch_references = BranchReference::get();
    }

    public function loadSupplierReference()
    {
        $this->supplier_references = Supplier::where('company', 'like', '%' . (isset($this->a) ? $this->suppliers[$this->a]['company'] : '') . '%')
            ->orderBy('company', 'asc')
            ->take(10)
            ->get();
    }

    public function loadSupplierItemReference()
    {
        if (isset($this->a)) {
            $this->supplier_item_references = SupplierItem::where([
                ['supplier_id', $this->suppliers[$this->a]['supplier_id']],
                ['name', 'like', '%' . (isset($this->b) ? $this->suppliers[$this->a]['items'][$this->b]['name'] : null) . '%']
            ])
                ->orderBy('name', 'asc')
                ->take(10)
                ->get();
        }
    }

    protected function updatedSuppliers()
    {
        $supplier = Supplier::where('company', (isset($this->a) ? $this->suppliers[$this->a]['company'] : null))->first();
        if ($supplier) {
            if ($this->action_type == 'suggestions_supplier') {
                $this->suppliers[$this->a] = [
                    'id' => $this->suppliers[$this->a]['id'],
                    'supplier_id' => $supplier->id,
                    'company' => $supplier->company,
                    'industry' => $this->suppliers[$this->a]['industry'] ? $this->suppliers[$this->a]['industry'] : $supplier->industry_id,
                    'type' => $this->suppliers[$this->a]['type'] ? $this->suppliers[$this->a]['type'] : $supplier->type_id,
                    'branch' => $this->suppliers[$this->a]['branch'] ? $this->suppliers[$this->a]['branch'] : $supplier->branch_id,
                    'first_name' => $this->suppliers[$this->a]['first_name'] ? $this->suppliers[$this->a]['first_name'] : $supplier->first_name,
                    'middle_name' => $this->suppliers[$this->a]['middle_name'] ? $this->suppliers[$this->a]['middle_name'] : $supplier->middle_name,
                    'last_name' => $this->suppliers[$this->a]['last_name'] ? $this->suppliers[$this->a]['last_name'] : $supplier->last_name,
                    'email' => $this->suppliers[$this->a]['email'] ? $this->suppliers[$this->a]['email'] : $supplier->email,
                    'mobile_number' => $this->suppliers[$this->a]['mobile_number'] ? $this->suppliers[$this->a]['mobile_number'] : $supplier->mobile_number,
                    'telephone_number' => $this->suppliers[$this->a]['telephone_number'] ? $this->suppliers[$this->a]['telephone_number'] : $supplier->telephone_number,
                    'address' => $this->suppliers[$this->a]['address'] ? $this->suppliers[$this->a]['address'] : $supplier->address,
                    'terms' => $this->suppliers[$this->a]['terms'] ? $this->suppliers[$this->a]['terms'] : $supplier->terms,
                    'items' => $this->suppliers[$this->a]['items'],
                    'total_amount' => $this->suppliers[$this->a]['total_amount'],
                    'is_deleted' => $this->suppliers[$this->a]['is_deleted'],
                ];
            } elseif ($this->action_type == 'suggestions_supplier_item_name') {
                $supplier_item = $supplier->items()->where('name', $this->suppliers[$this->a]['items'][$this->b]['name'])->first();
                $this->suppliers[$this->a]['items'][$this->b]['unit_cost'] = $supplier_item ? $supplier_item->unit_cost : 0;
            }
        }

        $this->loadSupplierItemReference();
        $this->validate([
            'suppliers.*.items.*.unit_cost' => 'required|numeric',
            'suppliers.*.items.*.quantity' => 'required|numeric',
        ], [
            'suppliers.*.items.*.unit_cost.required' => 'Must be numeric',
            'suppliers.*.items.*.unit_cost.numeric' => 'Must be numeric',
            'suppliers.*.items.*.quantity.required' => 'Must be numeric',
            'suppliers.*.items.*.quantity.numeric' => 'Must be numeric',
        ]);

        foreach ($this->suppliers as $a => $supplier) {
            foreach ($supplier['items'] as $b => $item) {
                $this->suppliers[$a]['items'][$b]['total_amount'] = ($item['unit_cost'] * $item['quantity']);
            }
            $this->suppliers[$this->a]['total_amount'] = (collect($this->suppliers[$this->a]['items']))->sum('total_amount');
        }
    }

    public function action(array $data, $action_type)
    {
        $this->action_type = $action_type;
        $this->a = $data['a'];
        if ($action_type == 'suggestions_supplier') {
            $this->b = null;
            $this->loadSupplierReference();
        } elseif ($action_type == 'suggestions_supplier_item_name') {
            $this->b = $data['b'];
            $this->loadSupplierItemReference();
        } elseif ($action_type == 'suggestions_supplier_item_unit_cost') {
        } elseif ($action_type == 'remove_supplier') {
            $this->confirmation_modal = true;
            $this->confirmation_message = "Are you sure to remove this supplier.";
        } elseif ($action_type == 'remove_item') {
            $this->b = $data['b'];
            $this->confirmation_modal = true;
            $this->confirmation_message = "Are you sure to remove this item.";
        } elseif ($action_type == 'untagged_PO') {
            $this->confirmation_modal = true;
            $this->confirmation_message = "Are you sure to untag this P.O To  " . $this->canvassing->canvassingSupplier[$data['a']]->requestForPayment->reference_id . ".";
        }
    }

    public function addSupplier()
    {
        $this->suppliers[] = [
            'id' => null,
            'supplier_id' => null,
            'company' => null,
            'industry' => null,
            'type' => null,
            'branch' => null,
            'first_name' => null,
            'middle_name' => null,
            'last_name' => null,
            'email' => null,
            'mobile_number' => null,
            'telephone_number' => null,
            'address' => null,
            'terms' => null,
            'items' => [],
            'total_amount' => 0,
            'is_deleted' => false,
        ];
    }

    public function addSupplierItem($i)
    {
        $this->suppliers[$i]['items'][] = [
            'id' => null,
            'name' => null,
            'unit_cost' => 0,
            'quantity' => 0,
            'total_amount' => 0,
            'recommended_status' => 0,
            'final_status' => 0,
            'is_deleted' => false,
        ];
    }

    public function remove(RequestManagementInterface $request_management_interface)
    {
        if ($this->action_type == 'remove_supplier') {
            if ($this->suppliers[$this->a]['id']) {
                $this->suppliers[$this->a]['is_deleted'] = true;
            } else {
                unset($this->suppliers[$this->a]);
            }

            array_values($this->suppliers);
        } else if ($this->action_type == 'remove_item') {
            if ($this->suppliers[$this->a]['id'] && $this->suppliers[$this->a]['items'][$this->b]['id']) {
                $this->suppliers[$this->a]['items'][$this->b]['is_deleted'] = true;
            } else {
                unset($this->suppliers[$this->a]['items'][$this->b]);
            }

            array_values($this->suppliers[$this->a]['items']);
        } else if ($this->action_type == 'untagged_PO') {
            $response = $request_management_interface->untaggedPO($this->canvassing->canvassingSupplier[$this->a]->requestForPayment);

            if ($response['code'] == 200) {
                $this->resetForm();
                $this->emit('canvassing_management_index');
                $this->sweetAlert('', $response['message']);
            } else {
                $this->sweetAlertError('error', $response['message'], $response['result']);
            }
        }

        $this->reset([
            'a',
            'b',
            'confirmation_message',
        ]);
        $this->confirmation_modal = false;
    }

    public function resetForm()
    {
        $this->reset([
            'suppliers'
        ]);
    }
}
