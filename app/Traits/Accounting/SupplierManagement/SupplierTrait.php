<?php

namespace App\Traits\Accounting\SupplierManagement;

use App\Models\Accounting\SupplierIndustryReference;
use App\Models\Accounting\SupplierTypeReference;
use App\Models\BranchReference;

trait SupplierTrait
{
    public $company;
    public $industry;
    public $type;
    public $branch;
    public $first_name;
    public $middle_name;
    public $last_name;
    public $email;
    public $mobile_number;
    public $telephone_number;
    public $address;
    public $terms;

    public $supplier_industry_references = [];
    public $supplier_type_references = [];
    public $branch_references = [];

    public function loadSupplierIndustryReference()
    {
        $this->supplier_industry_references = SupplierIndustryReference::get();
    }

    public function loadSupplierTypeReference()
    {
        $this->supplier_type_references = SupplierTypeReference::get();
    }

    public function loadBranchReference()
    {
        $this->branch_references = BranchReference::get();
    }

    public function resetForm()
    {
        $this->reset([
            'company',
            'industry',
            'type',
            'branch',
            'first_name',
            'middle_name',
            'last_name',
            'email',
            'mobile_number',
            'telephone_number',
            'address',
            'terms',
        ]);
    }
}
