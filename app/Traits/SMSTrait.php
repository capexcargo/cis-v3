<?php

namespace App\Traits;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

trait SMSTrait
{
    public function globeSendSMS($message, $number)
    {
        $unique_id = Str::random(20);
        $response = Http::post('https://api.m360.com.ph/v3/api/globelabs/mt/PGdAwgS86r', [
            'outboundSMSMessageRequest' => [
                'clientCorrelator' => $unique_id,
                'senderAddress' => "CaPEx Cargo",
                'outboundSMSTextMessage' => [
                    'message' => $message
                ],
                // "address" => "09176233252"
                "address" => $number
            ]
        ]);

        if ($response->status() == 201) {
            // Session::put('swal_default', [
            //     'icon' => 'success',
            //     'title' => 'Successfully Send',
            //     'text' => $response->collect(),
            // ]);
        } else {
            Session::put('swal_default', [
                'icon' => 'error',
                'title' => 'SMS Something Went Wrong',
                'text' => $response->collect(),
            ]);
        }
    }
}
