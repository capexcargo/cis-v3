<?php

namespace App\Traits\Admin\SecurityManagement;

use App\Models\Division;
use App\Models\LevelReference;

trait RoleManagementTrait
{
    public $parent_access_modal = false;
    public $child_access_modal = false;

    public $division;
    public $level;
    public $role_name;
    public $selected_accesses = [];

    public $division_references = [];
    public $level_references = [];

    public $search;

    public $role;

    public function load()
    {
        $this->loadDivisionReference();
        $this->loadLevelsReference();
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function loadLevelsReference()
    {
        $this->level_references = LevelReference::get();
    }

    public function closeModal($action_type)
    {
        if ($action_type == 'parent') {
            $this->parent_access_modal = false;
        } else if ($action_type == 'child') {
            $this->child_access_modal = false;
        }
    }

    protected function updatedSearch()
    {
        $this->resetPage();
    }

    public function resetForm()
    {
        $this->reset([
            'division',
            'level',
            'role_name',
            'selected_accesses'
        ]);
    }
}
