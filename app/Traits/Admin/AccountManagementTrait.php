<?php

namespace App\Traits\Admin;

use App\Models\Division;
use App\Models\DivisionRolesReference;
use App\Models\LevelReference;
use App\Models\SuffixReference;

trait AccountManagementTrait
{
    public $employee_number;
    public $first_name;
    public $middle_name;
    public $last_name;
    public $mobile_number;
    public $telephone_number;
    public $suffix;

    public $division;
    public $level;
    public $role;

    public $email;
    public $password;
    public $password_confirmation;

    public $suffix_references = [];
    public $division_references = [];
    public $level_references = [];
    public $role_references = [];


    public function addAttachments()
    {
        $this->attachments[] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }
    
    public function loadSuffixReference()
    {
        $this->suffix_references = SuffixReference::where('is_visible', 1)->get();
    }

    public function loadDivisionReference()
    {
        $this->division_references = Division::get();
    }

    public function loadLevelReference()
    {
        $this->level_references = LevelReference::where('is_visible', 1)->get();
    }

    public function loadRoles()
    {
        $this->role_references = DivisionRolesReference::where([
            ['division_id', $this->division],
            ['level_id', $this->level],
            ['is_visible', 1]
        ])->get();
    }

    protected function updatedDivision()
    {
        $this->role = '';
        $this->loadRoles();
    }

    protected function updatedLevel()
    {
        $this->role = '';
        $this->loadRoles();
    }

    public function resetForm()
    {
        $this->reset([
            'employee_number',
            'first_name',
            'middle_name',
            'last_name',
            'mobile_number',
            'telephone_number',
            'suffix',
            'division',
            'level',
            'role',
            'email',
            'password',
            'password_confirmation',
        ]);
    }
}
