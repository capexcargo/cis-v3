<?php

namespace App\Traits\Crm\Commercials\LandFreight;

use App\Models\BranchReference;
use App\Models\Crm\CrmAncillaryDisplay;
use App\Models\CrmApproval;
use App\Models\CrmCommodityType;
use App\Models\CrmRateApplyFor;
use App\Models\CrmRateLandFreightDetails;
use App\Models\CrmRateLoa;
use App\Models\CrmServiceMode;
use App\Models\CrmTransportMode;

trait LandFreightTrait
{

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $create_modal = false;
    public $confirmation_modal  = false;
    public $confirmation_message;
    public $action_type;
    public $current_tab = 1;

    public $created_by;
    public $name;
    public $effectivity_date;
    public $description;
    public $apply_for_id;
    public $amount_per_cbm;

    public $rate;
    public $vice_versa_display;

    public $origin_id;
    public $destination_id;
    public $vice_versa;
    public $service_mode_id;
    public $amount_weight_1;
    public $amount_weight_2;
    public $amount_weight_3;
    public $amount_weight_4;

    public $aw1;
    public $aw2;
    public $aw3;
    public $aw4;

    public $commodity_references = [];
    public $apply_references = [];
    public $ancillary_references = [];
    public $service_references = [];
    public $approver1_references = [];
    public $approver2_references = [];
    public $origin_references = [];
    public $destination_references = [];
    public $status_references = [];

    public $rates = [];

    public $porttodoor;





    public function load()
    {
        $this->RateTransportReference();
        $this->CommodityTypeReference();
        $this->ApplyReference();
        $this->AncillaryReference();
        $this->ServiceModeReference();
        $this->app1Reference();
        $this->app2Reference();
        $this->statusReference();
    }


    protected function updatedRates()
    {
        foreach ($this->rates as $a => $rate) {
            $this->rates[$a]['amount_weight_3'] = $this->rates[$a]['amount_weight_2'];
        }
    }

    protected function updatedViceVersa()
    {
        if ($this->vice_versa == 1) {
            $this->vice_versa_display = 'VV';
        } else {
            $this->vice_versa_display = '';
        }
    }


    public function RateTransportReference()

    {
        $this->transport_references = CrmTransportMode::get();
    }

    public function CommodityTypeReference()

    {
        $this->commodity_references = CrmCommodityType::get();
    }

    public function ApplyReference()

    {
        $this->apply_references = CrmRateApplyFor::get();
    }

    public function AncillaryReference()
    {
        $this->ancillary_references = CrmAncillaryDisplay::get();
    }

    public function ServiceModeReference()
    {
        $this->service_references = CrmServiceMode::Where('type', 2)->get();
    }

    public function app1Reference()

    {
        $this->approver1_references = CrmRateLoa::Where('rate_category', 3)->select('approver1_id')->first();
    }

    public function app2Reference()

    {
        $this->approver2_references = CrmRateLoa::Where('rate_category', 3)->select('approver2_id')->first();
    }

    public function OriginReference()
    {
        $this->origin_references = BranchReference::get();
    }

    public function DestinationReference()
    {
        $this->destination_references = BranchReference::get();
    }

    public function statusReference()
    {
        $this->status_references = CrmApproval::get();
    }

    public function addRates()
    {
        $this->rates[] = [
            'id' => null,
            'origin_id' => null,
            'destination_id' => null,
            'amount_weight_1' => null,
            'amount_weight_2' => null,
            'amount_weight_3' => null,
            'amount_weight_4' => null,
            'is_deleted' => false,
        ];
    }

    public function removeRates(array $data)
    {
        if (count(collect($this->rates)->where('is_deleted', false)) > 1) {
            if ($this->rates[$data["a"]]['id']) {
                $this->rates[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->rates[$data["a"]]);
            }

            array_values($this->rates);
        }
    }

    public function getRequest()
    {
        return [
            'created_by' => $this->created_by,
            'name' => $this->name,
            'effectivity_date' => $this->effectivity_date,
            'description' => $this->description,
            'ancillary_charge_id' => $this->ancillary_charge_id,
            'transport_mode_id' => $this->transport_mode_id,
            'commodity_type_id' => $this->commodity_type_id,
            'apply_for_id' => $this->apply_for_id,
            'vice_versa' => $this->vice_versa,
            'rates' => $this->rates,
            'amount_weight_1' => $this->amount_weight_1,
            'amount_weight_2' => $this->amount_weight_2,
            'amount_weight_3' => $this->amount_weight_3,
            'amount_weight_4' => $this->amount_weight_4,
        ];
    }

    public function getRequestLand()
    {
        return [
            'created_by' => $this->created_by,
            'name' => $this->name,
            'effectivity_date' => $this->effectivity_date,
            'description' => $this->description,
            'ancillary_charge_id' => $this->ancillary_charge_id,
            'transport_mode_id' => $this->transport_mode_id,
            'commodity_type_id' => $this->commodity_type_id,
            'apply_for_id' => $this->apply_for_id,
            'vice_versa' => $this->vice_versa,
            'rates' => $this->rates,
            'amount_weight_1' => $this->amount_weight_1,
            'amount_weight_2' => $this->amount_weight_2,
            'amount_weight_3' => $this->amount_weight_3,
            'amount_weight_4' => $this->amount_weight_4,
            'aw1' => $this->aw1,
            'aw2' => $this->aw2,
            'aw3' => $this->aw3,
            'aw4' => $this->aw4,
            'service_mode_id' => $this->service_mode_id,
            'base_rate_id' => $this->base_rate_id,

        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'created_by',
            'name',
            'effectivity_date',
            'description',
            'ancillary_charge_id',
            'transport_mode_id',
            'commodity_type_id',
            'apply_for_id',
            'rates',
            'origin_id',
            'destination_id',
            'vice_versa',
            'amount_weight_1',
            'amount_weight_2',
            'amount_weight_3',
            'amount_weight_4',
            'base_rate_id',
            'service_mode_id',
        ]);

        $this->confirmation_modal = false;
    }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToAFMgmt') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.index'));
        } else if ($action_type == 'redirectToPouch') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.pouch.index'));
        } else if ($action_type == 'redirectToLoa') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.loa-mgmt.index'));
        } else if ($action_type == 'redirectToBox') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.box.index'));
        } else if ($action_type == 'redirectToCrating') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.crating.index'));
        } else if ($action_type == 'redirectToWarehousing') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.warehousing.index'));
        } else if ($action_type == 'redirectToLand') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.land-freight.index'));
        } else if ($action_type == 'redirectToSea') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.sea-freight.index'));
        } else if ($action_type == 'redirectToAirFreightPremium') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.air-freight-premium.index'));
        }
    }
}
