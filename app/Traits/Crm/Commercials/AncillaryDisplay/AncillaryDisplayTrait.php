<?php

namespace App\Traits\Crm\Commercials\AncillaryDisplay;

use App\Models\Crm\CrmAncillary;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait AncillaryDisplayTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $action_type;

    public $create_modal = false;
    public $confirmation_modal = false;

    public $ancillary_charges_displays = [];
    public $ancillarycharges = [];
    public $ancillarychargeitems = [];
    public $ancillary_charge_id;
    public $description;
    public $name;

    public $ancillarydisplay_mgmt_id;
    public $edit_modal = false;
    public $ancillary;

    public $ancillarydetails = [];
    public $min;
    public $max;
    public $countarr;

    public $deactivate_modal = false;
    public $reactivate_modal = false;

    public $view_modal = false;

    public function updatedName(){
        $this->resetErrorBag('name');
    }


    public function AncillaryCharge()
    {
        $this->ancillarychargeitems = CrmAncillary::where('status', 1)->get();
    }

    public function GetDescription()
    {
        foreach ($this->ancillarycharges as $a => $ancillarych) {
            if ($this->ancillarycharges[$a]['ancillary_charge_id'] != null) {
                $ancillaryitem = CrmAncillary::where('id', $this->ancillarycharges[$a]['ancillary_charge_id'])->first();
                if ($this->ancillarycharges[$a]['is_deleted'] == false) {
                    $this->ancillarycharges[$a] = [
                        'id' => $this->ancillarycharges[$a]['id'],
                        'ancillary_charge_id' => $this->ancillarycharges[$a]['ancillary_charge_id'],
                        'description' => $ancillaryitem->description,
                        'is_deleted' => false,
                    ];
                }
            } else {
                $this->ancillarycharges[$a] = [
                    'id' => null,
                    'ancillary_charge_id' => $this->ancillarycharges[$a]['ancillary_charge_id'],
                    'description' => '',
                    'is_deleted' => false,
                ];
            }
        }
    }

    protected function updatedAncillarycharges()
    {
        $this->GetDescription();
        $this->resetErrorBag();
    }

    public function addAncillaryDisplay()
    {
        $this->ancillarycharges[] = [
            'id' => null,
            'ancillary_charge_id' => null,
            'description' => null,
            'is_deleted' => false,
        ];

        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->ancillarycharges as $a => $ancillarycharge) {

            if ($this->ancillarycharges[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->ancillarycharges[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->max = max($getkeyarray);
        $this->countarr = $countarray;
    }

    public function removeAncillary(array $data)
    {
        if (count(collect($this->ancillarycharges)->where('is_deleted', false)) > 1) {
            if ($this->ancillarycharges[$data["a"]]['id']) {
                $this->ancillarycharges[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->ancillarycharges[$data["a"]]);
            }

            array_values($this->ancillarycharges);
        }

        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->ancillarycharges as $a => $ancillarycharge) {

            if ($this->ancillarycharges[$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->ancillarycharges[$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->min = min($getkeyarray);
        $this->max = max($getkeyarray);
        $this->countarr = $countarray;
    }


    public function getRequest()
    {
        return [
            'name' => $this->name,
            'ancillarycharges' => $this->ancillarycharges
        ];
    }


    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'name',
            'ancillarycharges',
        ]);

        $this->confirmation_modal = false;
    }
}
