<?php

namespace App\Traits\Crm\Commercials\SeaFreight;

use App\Models\BranchReference;
use App\Models\Crm\CrmAncillaryDisplay;
use App\Models\CrmApproval;
use App\Models\CrmCommodityType;
use App\Models\CrmRateApplyFor;
use App\Models\CrmRateLoa;
use App\Models\CrmRateSeaFreight;
use App\Models\CrmRateSeaFreightDetails;
use App\Models\CrmServiceMode;
use App\Models\CrmShipmentType;
use App\Models\CrmTransportMode;

trait SeaFreightTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $create_modal = false;
    public $confirmation_modal  = false;
    public $confirmation_message;
    public $action_type;
    public $current_tab = 1;

    public $created_by;
    public $name;
    public $effectivity_date;
    public $description;
    public $crating_type_id;
    public $apply_for_id;
    public $amount_per_cbm;

    public $rate;
    public $vice_versa_display;
    public $ancillary_charge_id;
    public $shipment_type_id;


    public $origin_id;
    public $destination_id;
    public $vice_versa;
    public $size;

    public $amount_weight_1;
    public $amount_weight_2;
    public $amount_weight_3;
    public $amount_weight_4;

    public $aw1;
    public $aw2;
    public $aw3;
    public $aw4;

    public $amount_servicemode_1;
    public $amount_servicemode_2;
    public $amount_servicemode_3;
    public $amount_servicemode_4;

    public $amount_servicemode_1_ptp_10;
    public $amount_servicemode_1_ptp_20;
    public $amount_servicemode_1_ptp_40;

    public $aw1ptp10;
    public $aw1ptp20;
    public $aw1ptp40;

    public $amount_servicemode_2_ptp_10;
    public $amount_servicemode_2_ptp_20;
    public $amount_servicemode_2_ptp_40;

    public $aw2ptp10;
    public $aw2ptp20;
    public $aw2ptp40;

    public $amount_servicemode_3_ptp_10;
    public $amount_servicemode_3_ptp_20;
    public $amount_servicemode_3_ptp_40;

    public $aw3ptp10;
    public $aw3ptp20;
    public $aw3ptp40;

    public $amount_servicemode_4_ptp_10;
    public $amount_servicemode_4_ptp_20;
    public $amount_servicemode_4_ptp_40;

    public $aw4ptp10;
    public $aw4ptp20;
    public $aw4ptp40;

    public $commodity_references = [];
    public $apply_references = [];
    public $ancillary_references = [];
    public $service_references = [];
    public $approver1_references = [];
    public $approver2_references = [];
    public $origin_references = [];
    public $destination_references = [];
    public $status_references = [];
    public $shipment_references = [];


    public $rates = [];


    public function load()
    {
        $this->ShipmentTypeReference();
        $this->RateTransportReference();
        $this->CommodityTypeReference();
        $this->ApplyReference();
        $this->AncillaryReference();
        $this->ServiceModeReference();
        $this->app1Reference();
        $this->app2Reference();
        $this->statusReference();
    }

    // protected function updatedAmountWeight()
    // {
    //     $this->porttodoor = CrmRateSeaFreightDetails::where('id', $this->amount_weight_2)->get('id')->first();
    //     $this->amount_weight_3 = $this->porttodoor->id;
    // }

    protected function updatedShipmentTypeId()
    {
        $this->shipmenttype = $this->shipment_type_id;
        $this->resetPageSeaFreight();
        // $this->shipment_type_id = $this->shipmenttype->name;
        // dd($this->shipmenttype);
    }

    protected function updatedBaseRateId()
    {
        $this->baserates = CrmRateSeaFreight::where('id', $this->base_rate_id)->get()->first();
        $this->shipment_type_id = $this->baserates->shipment_type_id;

        // dd($this->shipment_type_id);

    }

    

    protected function updatedViceVersa()
    {
        if ($this->vice_versa == 1) {
            $this->vice_versa_display = 'VV';
        } else {
            $this->vice_versa_display = '';
        }
    }


    public function ShipmentTypeReference()
    {
        $this->shipment_references = CrmShipmentType::get();
    }

    public function RateTransportReference()

    {
        $this->transport_references = CrmTransportMode::get();
    }

    public function CommodityTypeReference()

    {
        $this->commodity_references = CrmCommodityType::get();
    }

    public function ApplyReference()

    {
        $this->apply_references = CrmRateApplyFor::get();
    }

    public function AncillaryReference()
    {
        $this->ancillary_references = CrmAncillaryDisplay::get();
    }

    public function ServiceModeReference()
    {
        $this->service_references = CrmServiceMode::get();
    }

    public function app1Reference()

    {
        $this->approver1_references = CrmRateLoa::Where('rate_category', 1)->select('approver1_id')->first();
    }

    public function app2Reference()

    {
        $this->approver2_references = CrmRateLoa::Where('rate_category', 1)->select('approver2_id')->first();
    }

    public function OriginReference()
    {
        $this->origin_references = BranchReference::get();
    }

    public function DestinationReference()
    {
        $this->destination_references = BranchReference::get();
    }

    public function statusReference()
    {
        $this->status_references = CrmApproval::get();
    }

    public function addRates()
    {
        $this->rates[] = [
            'id' => null,
            'origin_id' => null,
            'destination_id' => null,
            'size' => null,
            'amount_weight_1' => null,
            'amount_weight_2' => null,
            'amount_weight_3' => null,
            'amount_weight_4' => null,
            'amount_servicemode_1' => null,
            'amount_servicemode_2' => null,
            'amount_servicemode_3' => null,
            'amount_servicemode_4' => null,
            'is_deleted' => false,
        ];
    }

    public function removeRates(array $data)
    {
        if (count(collect($this->rates)->where('is_deleted', false)) > 1) {
            if ($this->rates[$data["a"]]['id']) {
                $this->rates[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->rates[$data["a"]]);
            }

            array_values($this->rates);
        }
    }

    public function getRequest()
    {
        return [
            'created_by' => $this->created_by,
            'name' => $this->name,
            'effectivity_date' => $this->effectivity_date,
            'description' => $this->description,
            'ancillary_charge_id' => $this->ancillary_charge_id,
            'transport_mode_id' => $this->transport_mode_id,
            'commodity_type_id' => $this->commodity_type_id,
            'apply_for_id' => $this->apply_for_id,
            'vice_versa' => $this->vice_versa,
            'shipment_type_id' => $this->shipment_type_id,
            'rates' => $this->rates,
           
        ];
    }

    public function getRequestSea()
    {
        return [
            'created_by' => $this->created_by,
            'name' => $this->name,
            'effectivity_date' => $this->effectivity_date,
            'description' => $this->description,
            'ancillary_charge_id' => $this->ancillary_charge_id,
            'transport_mode_id' => $this->transport_mode_id,
            'commodity_type_id' => $this->commodity_type_id,
            'apply_for_id' => $this->apply_for_id,
            'vice_versa' => $this->vice_versa,
            'shipment_type_id' => $this->shipment_type_id,
            'rates' => $this->rates,

            'amount_servicemode_1_ptp_10' => $this->amount_servicemode_1_ptp_10,
            'amount_servicemode_1_ptp_20' => $this->amount_servicemode_1_ptp_20,
            'amount_servicemode_1_ptp_40' => $this->amount_servicemode_1_ptp_40,
            'aw1ptp10' => $this->aw1ptp10,
            'aw1ptp20' => $this->aw1ptp20,
            'aw1ptp40' => $this->aw1ptp40,

            'amount_servicemode_2_ptp_10' => $this->amount_servicemode_2_ptp_10,
            'amount_servicemode_2_ptp_20' => $this->amount_servicemode_2_ptp_20,
            'amount_servicemode_2_ptp_40' => $this->amount_servicemode_2_ptp_40,
            'aw2ptp10' => $this->aw2ptp10,
            'aw2ptp20' => $this->aw2ptp20,
            'aw2ptp40' => $this->aw2ptp40,

            'amount_servicemode_3_ptp_10' => $this->amount_servicemode_3_ptp_10,
            'amount_servicemode_3_ptp_20' => $this->amount_servicemode_3_ptp_20,
            'amount_servicemode_3_ptp_40' => $this->amount_servicemode_3_ptp_40,
            'aw3ptp10' => $this->aw3ptp10,
            'aw3ptp20' => $this->aw3ptp20,
            'aw3ptp40' => $this->aw3ptp40,

            'amount_servicemode_4_ptp_10' => $this->amount_servicemode_4_ptp_10,
            'amount_servicemode_4_ptp_20' => $this->amount_servicemode_4_ptp_20,
            'amount_servicemode_4_ptp_40' => $this->amount_servicemode_4_ptp_40,
            'aw4ptp10' => $this->aw4ptp10,
            'aw4ptp20' => $this->aw4ptp20,
            'aw4ptp40' => $this->aw4ptp40,

            'amount_weight_1' => $this->amount_weight_1,
            'amount_weight_2' => $this->amount_weight_2,
            'amount_weight_3' => $this->amount_weight_3,
            'amount_weight_4' => $this->amount_weight_4,

            'aw1' => $this->aw1,
            'aw2' => $this->aw2,
            'aw3' => $this->aw3,
            'aw4' => $this->aw4,

            'base_rate_id' => $this->base_rate_id,

        ];
    }

    public function resetPageSeaFreight()
    {
        $this->resetErrorBag();
        $this->reset([
                    'rates',
                    'vice_versa',
                    'vice_versa_display',
                    'ancillary_charge_id',
                ]);
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'created_by',
            'name',
            'effectivity_date',
            'description',
            'ancillary_charge_id',
            'transport_mode_id',
            'commodity_type_id',
            'apply_for_id',
            'shipment_type_id',
            'rates',
            'origin_id',
            'destination_id',
            'vice_versa',
            'amount_weight_1',
            'amount_weight_2',
            'amount_weight_3',
            'amount_weight_4',
            'base_rate_id',
            'service_mode_id',
        ]);

        $this->confirmation_modal = false;

    }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToAFMgmt') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.index'));
        } else if ($action_type == 'redirectToPouch') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.pouch.index'));
        } else if ($action_type == 'redirectToLoa') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.loa-mgmt.index'));
        } else if ($action_type == 'redirectToBox') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.box.index'));
        } else if ($action_type == 'redirectToCrating') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.crating.index'));
        } else if ($action_type == 'redirectToWarehousing') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.warehousing.index'));
        } else if ($action_type == 'redirectToLand') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.land-freight.index'));
        } else if ($action_type == 'redirectToSea') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.sea-freight.index'));
        } else if ($action_type == 'redirectToAirFreightPremium') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.air-freight-premium.index'));
        }
    }
}
