<?php

namespace App\Traits\Crm\Commercials\Warehousing;

use App\Models\Crm\CrmAncillaryDisplay;
use App\Models\CrmApproval;
use App\Models\CrmRateApplyFor;
use App\Models\CrmRateLoa;

trait WarehousingTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $action_type;

    public $current_tab = 1;
    public $index;

    public $rate;

    public $created_by;
    public $name;
    public $effectivity_date;
    public $description;
    public $ancillary_charge_id;
    public $apply_for_id;

    public $option;
    public $rate_per_unit;
   
    public $amount_short_term;
    public $amount_long_term;

    public $create_base_modal = false;

    public $ancillary_references = [];
    public $apply_references = [];
    public $approver1_references = [];
    public $approver2_references = [];
    public $status_references = [];


    public function load()
    {
        $this->AncillaryReference();
        $this->ApplyReference();
        $this->app1Reference();
        $this->app2Reference();
        $this->statusReference();
    }

    public function AncillaryReference()
    {
        $this->ancillary_references = CrmAncillaryDisplay::get();
    }

    public function ApplyReference()
    {
        $this->apply_references = CrmRateApplyFor::get();
    }


    public function app1Reference()
    {
        $this->approver1_references = CrmRateLoa::Where('rate_category', 8)->select('approver1_id')->first();
    }

    public function app2Reference()
    {
        $this->approver2_references = CrmRateLoa::Where('rate_category', 8)->select('approver2_id')->first();
    }

    public function statusReference()
    {
        $this->status_references = CrmApproval::get();
    }

    public function getRequest()
    {
        return [
            'created_by' => $this->created_by,
            'name' => $this->name,
            'effectivity_date' => $this->effectivity_date,
            'description' => $this->description,
            'ancillary_charge_id' => $this->ancillary_charge_id,
            'apply_for_id' => $this->apply_for_id,
            'options' => $this->options,
        ];
    }

    public function getRequestWarehousing()
    {
        return [
            'created_by' => $this->created_by,
            'name' => $this->name,
            'effectivity_date' => $this->effectivity_date,
            'description' => $this->description,
            'ancillary_charge_id' => $this->ancillary_charge_id,
            'apply_for_id' => $this->apply_for_id,
            'options' => $this->options,
            'optionl' => $this->optionl,
            'base_rate_id' => $this->base_rate_id,
            'short' => $this->short,
            'long' => $this->long,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'created_by',
            'name',
            'effectivity_date',
            'description',
            'ancillary_charge_id',
            'apply_for_id',
            'options',
            'optionl',
            'short',
            'long',
        ]);

        $this->confirmation_modal = false;

    }



    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToAFMgmt') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.index'));
        } else if ($action_type == 'redirectToPouch') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.pouch.index'));
        } else if ($action_type == 'redirectToLoa') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.loa-mgmt.index'));
        } else if ($action_type == 'redirectToBox') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.box.index'));
        } else if ($action_type == 'redirectToCrating') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.crating.index'));
        } else if ($action_type == 'redirectToWarehousing') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.warehousing.index'));
        } else if ($action_type == 'redirectToLand') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.land-freight.index'));
        } else if ($action_type == 'redirectToSea') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.sea-freight.index'));
        } else if ($action_type == 'redirectToAirFreightPremium') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.air-freight-premium.index'));
        }
    }
}
