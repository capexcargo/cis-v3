<?php

namespace App\Traits\Crm\Commercials\LoaMgmt;

use App\Models\CrmRateLoa;
use App\Models\User;

trait LoaMgmtTrait
{

    public $create_modal = false;
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $action_type;

    public $rate_category;
    public $approver1_id;
    public $approver2_id;


    public $first_approver_references = [];
    public $second_approver_references = [];


    public function load()
    {
    $this-> loadFirstApproverReference();
    $this-> loadSecodeApproverReference();
    }

    public function loadFirstApproverReference()
    
    {
        $this->first_approver_references = User::Where('level_id', 4)->get();
    }

    public function loadSecodeApproverReference()
    
    {
        $this->second_approver_references = User::Where('level_id', 4)->get();
    }
    
    // public function loadFirstApproverReference()
    // {
    //     $this->first_approver_references = User::whereHas('userDetails', function ($query) {
    //         $query->where('branch_id', $this->branch)
    //             ->Where('department_id', $this->department_id)
    //             ->orWhere('level_id', 4);
    //     })
    //         ->where([
    //             ['division_id', $this->division],
    //             ['level_id', '>=', 1]
    //         ])
    //         ->get();
    //         // dd($this->first_approver_references);
    // }

    // public function loadSecodeApproverReference()
    // {
    //     $this->second_approver_references = User::whereHas('userDetails', function ($query) {
    //         $query->where('branch_id', $this->branch)
    //             ->where('department_id', $this->department_id)
    //             ->orWhere('level_id', 4);
    //     })
    //         ->where([
    //             ['division_id', $this->division],
    //             ['level_id', '>=', 3]
    //         ])
    //         ->get();
    // }

    public function getRequest()
    {
        return [
            'rate_category' =>$this->rate_category,
            'approver1_id' =>$this->approver1_id,
            'approver2_id' =>$this->approver2_id,
        ];
        
    }

    
    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'rate_category',
            'approver1_id',
            'approver2_id',
        ]);        
        $this->confirmation_modal = false;

    }



    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;
        
        if ($action_type == 'redirectToAFMgmt') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.index'));
        } else if ($action_type == 'redirectToPouch') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.pouch.index'));
        } else if ($action_type == 'redirectToLoa') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.loa-mgmt.index'));
        } else if ($action_type == 'redirectToBox') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.box.index'));
        } else if ($action_type == 'redirectToCrating') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.crating.index'));
        } else if ($action_type == 'redirectToWarehousing') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.warehousing.index'));
        } else if ($action_type == 'redirectToLand') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.land-freight.index'));
        } else if ($action_type == 'redirectToSea') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.sea-freight.index'));
        } else if ($action_type == 'redirectToAirFreightPremium') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.air-freight-premium.index'));
        }
        
    }
    
}