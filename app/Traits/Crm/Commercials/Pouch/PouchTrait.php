<?php

namespace App\Traits\Crm\Commercials\Pouch;

use App\Interfaces\Crm\Commercials\Pouch\PouchInterface;
use App\Models\BranchReference;
use App\Models\CrmApproval;
use App\Models\CrmBookingType;
use App\Models\CrmPouchType;
use App\Models\CrmRateApplyFor;
use App\Models\CrmRateLoa;
use App\Models\CrmRatePouch;
use App\Models\CrmRateType;
use App\Models\CrmTransportMode;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait PouchTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $action_type;

    public $current_tab = 1;
    public $index;

    public $rate;
    public $vice_versa_display;

    public $created_by;
    public $name;
    public $effectivity_date;
    public $description;
    public $rate_type_id;
    public $transport_mode_id;
    public $booking_type_id;
    public $apply_for_id;
    public $pouch_type_id;

    public $origin_id;
    public $destination_id;
    public $vice_versa;
    public $amount_small;
    public $amount_medium;
    public $amount_large;

    public $create_base_modal = false;


    public $rate_type_references = [];
    public $transport_references = [];
    public $booking_references = [];
    public $apply_references = [];
    public $pouch_type_references = [];
    public $approver1_references = [];
    public $approver2_references = [];
    public $origin_references = [];
    public $destination_references = [];
    public $status_references = [];

    public $rates = [];


    public function load()
    {
        $this->RateTypeReference();
        $this->RateTransportReference();
        $this->BookingReference();
        $this->ApplyReference();
        $this->PouchTypeReference();
        $this->app1Reference();
        $this->app2Reference();
        $this->statusReference();
    }

    protected function updatedRateTypeId()
    {
        $this->transportmode = CrmTransportMode::where('id', $this->rate_type_id)->select('name')->first();
        $this->transport_mode_id = $this->transportmode->name;
    }

    protected function updatedViceVersa()
    {
        if ($this->vice_versa == 1) {
            $this->vice_versa_display = 'VV';
        } else {
            $this->vice_versa_display = '';
        }
    }

    public function RateTypeReference()

    {
        $this->rate_type_references = CrmRateType::get();
    }

    public function RateTransportReference()

    {
        $this->transport_references = CrmTransportMode::get();
    }

    public function BookingReference()

    {
        $this->booking_references = CrmBookingType::get();
    }

    public function ApplyReference()

    {
        $this->apply_references = CrmRateApplyFor::get();
    }

    public function PouchTypeReference()

    {
        $this->pouch_type_references = CrmPouchType::get();
    }

    public function app1Reference()

    {
        $this->approver1_references = CrmRateLoa::Where('rate_category', 5)->select('approver1_id')->first();
    }

    public function app2Reference()

    {
        $this->approver2_references = CrmRateLoa::Where('rate_category', 5)->select('approver2_id')->first();
    }

    public function OriginReference()
    {
        $this->origin_references = BranchReference::get();
    }

    public function DestinationReference()
    {
        $this->destination_references = BranchReference::get();
    }

    public function statusReference()
    {
        $this->status_references = CrmApproval::get();
    }
    

   

    public function addRates()
    {
        $this->rates[] = [
            'id' => null,
            'origin_id' => null,
            'destination_id' => null,
            'amount_small' => null,
            'amount_medium' => null,
            'amount_large' => null,
            'is_deleted' => false,
        ];
    }

    public function removeRates(array $data)
    {
        if (count(collect($this->rates)->where('is_deleted', false)) > 1) {
            if ($this->rates[$data["a"]]['id']) {
                $this->rates[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->rates[$data["a"]]);
            }

            array_values($this->rates);
        }
    }

    public function getRequest()
    {
        return [
            'created_by' => $this->created_by,
            'name' => $this->name,
            'effectivity_date' => $this->effectivity_date,
            'description' => $this->description,
            'rate_type_id' => $this->rate_type_id,
            'transport_mode_id' => $this->transport_mode_id,
            'booking_type_id' => $this->booking_type_id,
            'apply_for_id' => $this->apply_for_id,
            'pouch_type_id' => $this->pouch_type_id,
            'rates' => $this->rates,
            'origin_id' => $this->origin_id,
            'destination_id' => $this->destination_id,
            'vice_versa' => $this->vice_versa,
            'amount_small' => $this->amount_small,
            'amount_medium' => $this->amount_medium,
            'amount_large' => $this->amount_large,

        ];
    }

    public function getRequestPouch()
    {
        return [
            'created_by' => $this->created_by,
            'name' => $this->name,
            'effectivity_date' => $this->effectivity_date,
            'description' => $this->description,
            'rate_type_id' => $this->rate_type_id,
            'transport_mode_id' => $this->transport_mode_id,
            'booking_type_id' => $this->booking_type_id,
            'apply_for_id' => $this->apply_for_id,
            'pouch_type_id' => $this->pouch_type_id,
            'origin_id' => $this->origin_id,
            'destination_id' => $this->destination_id,
            'amount_small' => $this->amount_small,
            'amount_medium' => $this->amount_medium,
            'amount_large' => $this->amount_large,
            'small' => $this->small,
            'medium' => $this->medium,
            'large' => $this->large,
            'base_rate_id' => $this->base_rate_id,

        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'created_by',
            'name',
            'effectivity_date',
            'description',
            'rate_type_id',
            'transport_mode_id',
            'booking_type_id',
            'apply_for_id',
            'pouch_type_id',
            'rates',
            'origin_id',
            'destination_id',
            'vice_versa',
            'amount_small',
            'amount_medium',
            'amount_large',
        ]);

        $this->confirmation_modal = false;

    }







    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToAFMgmt') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.index'));
        } else if ($action_type == 'redirectToPouch') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.pouch.index'));
        } else if ($action_type == 'redirectToLoa') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.loa-mgmt.index'));
        } else if ($action_type == 'redirectToBox') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.box.index'));
        } else if ($action_type == 'redirectToCrating') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.crating.index'));
        } else if ($action_type == 'redirectToWarehousing') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.warehousing.index'));
        } else if ($action_type == 'redirectToLand') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.land-freight.index'));
        } else if ($action_type == 'redirectToSea') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.sea-freight.index'));
        } else if ($action_type == 'redirectToAirFreightPremium') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.air-freight-premium.index'));
        }
    }
}
