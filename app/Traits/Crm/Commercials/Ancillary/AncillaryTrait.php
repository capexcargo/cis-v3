<?php

namespace App\Traits\Crm\Commercials\Ancillary;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait AncillaryTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $action_type;

    public $create_modal = false;
    public $confirmation_modal = false;

    public $ancillarycharges = [];

    public $ancillary;
    public $ancillary_mgmt_id;
    public $edit_modal = false;

    public $name;
    public $charges_amount;
    public $charges_rate;
    public $description;

    public $deactivate_modal = false;
    public $reactivate_modal = false;




    public function addAncillary()
    {
        $this->ancillarycharges[] = [
            'id' => null,
            'name' => null,
            'charges_amount' => null,
            'charges_rate' => null,
            'description' => null,
            'is_deleted' => false,
        ];

        // dd($this->ancillarycharges);
    }

    public function removeAncillary(array $data)
    {
        if (count(collect($this->ancillarycharges)->where('is_deleted', false)) > 1) {
            if ($this->ancillarycharges[$data["a"]]['id']) {
                $this->ancillarycharges[$data["a"]]['is_deleted'] = true;
            } else {
                unset($this->ancillarycharges[$data["a"]]);
            }

            array_values($this->ancillarycharges);
        }
    }


    public function getRequest()
    {
        return [
            'ancillarycharges' => $this->ancillarycharges
        ];
    }

    public function getRequestUpdate()
    {
        return [
            'name' => $this->name,
            'charges_amount' => $this->charges_amount,
            'charges_rate' => $this->charges_rate,
            'description' => $this->description,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'ancillarycharges',
        ]);

        $this->confirmation_modal = false;
    }
}
