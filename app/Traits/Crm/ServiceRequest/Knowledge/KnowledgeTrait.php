<?php

namespace App\Traits\Crm\ServiceRequest\Knowledge;

use App\Models\CrmKnowledgeResponse;

trait KnowledgeTrait
{

    // public $category_references = [];

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $action_type;
    public $response_references = [];


    public function resetForm()
    {
        $this->reset([
            'faq_concern',
            'response_area',
            'positions',
        ]);

        $this->confirmation_modal = false;
    }


    public function respHasMany()
    {
        $this->response_references = CrmKnowledgeResponse::get();
    }


    public function getRequest()
    {
        return [
            'faq_concern' => $this->faq_concern,
            'positions' => $this->positions,
            'response_area' => $this->response_area,
        ];
    }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToKnwoledge') {
            return redirect()->to(route('crm.service-request.knowledge.index'));
        } else if ($action_type == 'redirectToSalesCoach') {
            return redirect()->to(route('crm.service-request.knowledge.sales-coach.index'));
        } 
    }
}
