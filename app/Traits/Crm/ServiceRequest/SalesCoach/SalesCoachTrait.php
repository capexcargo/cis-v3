<?php

namespace App\Traits\Crm\ServiceRequest\SalesCoach;

use App\Models\CrmSalesCoachProcess;
use App\Models\CrmSalesCoachSubprocess;
use App\Models\User;

trait SalesCoachTrait
{

    // public $category_references = [];

    public $paginate = 10;
    
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $action_type;
    public $index;


    public $pros = [];
    public $subpros = [];
    public $counter;
    public $is_deleted;
    public $countarr;
    public $min;
    public $nearest;




    public function addProcess($a)
    {
        $this->counter = $a;
        $this->pros[$a][] = [
            'id' => null,
            'title' => null,
            'description' => null,
            'process' => null,
            'created_by' => null,
            'is_deleted' => false,
        ];


        // $this->addSubProcess($a);
    }

    public function addProcessEdit($a, $b)
    {
        $this->counter = $a;
        $this->pros[$a][$b] = [
            'id' => null,
            'title' => null,
            'description' => null,
            'process' => null,
            'created_by' => null,
            'is_deleted' => false,
        ];

        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->pros as $a => $pro) {
            if ($this->pros[$a][$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->pros[$a][$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->countarr = $countarray;
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
       
    }
   

    public function addSubProcess($a)
    {

        // dd($a);
        $this->subpros[$a][] = [
            'id' => null,
            'process_content' => null,
            'is_deleted' => false,
        ];

        // dd($this->subpros);
    }

    public function removeProcessCreate(array $data)
    {
        if (count(collect($this->pros)->where('is_deleted', false)) > 1) {
            // dd($this->pros);
            if ($this->pros[$data['a']][0]['id']) {
                // dd($this->pros)

                $this->pros[$data['a']][0]['is_deleted'] = true;
                // dd($this->pros[$data['a']]);
                $b = 0;
                if (isset($this->subpros[$data['a']])) {
                    foreach ($this->subpros[$data['a']] as $c => $process) {
                        $this->subpros[$data['a']][$c]['is_deleted'] = true;
                        $b++;
                    }
                }


                // unset($this->pros[$data['a']]);
            } else {

                unset($this->pros[$data['a']]);
                unset($this->subpros[$data['a']]);
            }
            array_values($this->pros);
        }


        // dd($this->count_isdeleted);
    }

    public function removeProcessEdit(array $data)
    {
        if (count(collect($this->pros)->where('is_deleted', false)) > 1) {
            // dd($this->pros);
            if ($this->pros[$data['a']][$data['a']]['id']) {
                $this->pros[$data['a']][$data['a']]['is_deleted'] = true;

                $b = 0;
                if (isset($this->subpros[$data['a']])) {
                    foreach ($this->subpros[$data['a']] as $c => $process) {
                        $this->subpros[$data['a']][$c]['is_deleted'] = true;
                        $b++;
                    }
                }
            } else {

                unset($this->pros[$data['a']]);
                unset($this->subpros[$data['a']]);

            }
            array_values($this->pros);
        }

        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->pros as $a => $pro) {
            if ($this->pros[$a][$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->pros[$a][$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->countarr = $countarray;
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
        // dd($this->countarr);

    }

    public function removeSubProcess(array $data)
    {
        // if (count(collect($this->subpros[$data['a']])->where('is_deleted', false)) > 0) {

            if ($this->subpros[$data['a']][$data['b']]['id']) {

                $this->subpros[$data['a']][$data['b']]['is_deleted'] = true;
            } else {

                unset($this->subpros[$data['a']][$data['b']]);
            }
        // }

        // dd(array_values($this->subpros[$data['a']]));
    }

    public function resetForm()
    {
        $this->reset([
            'title',
            'description',
            'process',
            'pros',
            'subpros',
        ]);
        $this->confirmation_modal = false;
    }


    public function getRequest()
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'pros' => $this->pros,
            'subpros' => $this->subpros,
        ];
    }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToKnwoledge') {
            return redirect()->to(route('crm.service-request.knowledge.index'));
        } else if ($action_type == 'redirectToSalesCoach') {
            return redirect()->to(route('crm.service-request.knowledge.sales-coach.index'));
        }
    }
}
