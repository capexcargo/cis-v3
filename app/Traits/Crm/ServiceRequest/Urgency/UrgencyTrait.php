<?php

namespace App\Traits\Crm\ServiceRequest\Urgency;

use App\Models\MileResolution;
use App\Models\SrType;
use App\Repositories\Crm\ServiceRequest\Urgency\UrgencyRepository;

trait UrgencyTrait
{

    // public $category_references = [];

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $urgency_impacts = [];
    public $severity_references = [];
    public $high_references = [];
    public $medium_references = [];
    public $low_references = [];
    public $action_type;


    
    public function loadUrgency()
    {
        $urgency_repository = new UrgencyRepository();
        $response = $urgency_repository->show();

        foreach($response['result'] as $urgency)
        {
            $this->urgency_impacts[] = [
                'id' => $urgency->id,
                'severity' => $urgency->severity_id,
                'high_priority_level' => $urgency->high_priority_level,
                'medium_priority_level' => $urgency->medium_priority_level,
                'low_priority_level' => $urgency->low_priority_level,
                'is_deleted' => false,
            ];
        }
        
    }

    public function addUrgency()
    {
        $this->urgency_impacts[] = [
            'id' => null,
            'severity' => null,
            'high_priority_level' => null,
            'medium_priority_level' => null,
            'low_priority_level' => null,
            'is_deleted' => false,
        ];
    }

    public function removeUrgency($i)
    {
        if (count(collect($this->urgency_impacts)->where('is_deleted', false)) > 1) {
            if ($this->urgency_impacts[$i]['id']) {
                $this->urgency_impacts[$i]['is_deleted'] = true;
                
            } else {
                unset($this->urgency_impacts[$i]);
            }
            array_values($this->urgency_impacts);
        }
    }

    public function load()
    {
        // $this->loadUrgency();
        $this->severityReferences();
        $this->highReferences();
        $this->mediumReferences();
        $this->lowReferences();

    }

    public function severityReferences()
    
    {
        $this->severity_references = MileResolution::get();
    }

    public function highReferences()
    
    {
        $this->high_references = MileResolution::get();
    }

    public function mediumReferences()
    
    {
        $this->medium_references = MileResolution::get();
    }

    public function lowReferences()
    
    {
        $this->low_references = MileResolution::get();
    }

    // public function resetForm()
    // {
    //     $this->reset([
    //         'channel',
    //     ]);
    //     $this->confirmation_modal = false;
    // }

    // public function categoryReferences()

    // {
    //     $this->category_references = SrType::get();
    // }


    public function getRequest()
    {
        return [
            'urgency_impacts' => $this->urgency_impacts,
        ];
    }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToSRTypeMgmt') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.index'));
        } else if ($action_type == 'redirectTosubMgmt') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.sr-sub-category.index'));
        } else if ($action_type == 'redirectToResponse') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.response.index'));
        } else if ($action_type == 'redirectToResolution') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.resolution.index'));
        } else if ($action_type == 'redirectToUrgency') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.urgency.index'));
        } else if ($action_type == 'redirectToHierarchy') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.hierarchy.index'));
        } else if ($action_type == 'redirectToChannel') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.channel.index'));
        } else if ($action_type == 'redirectToService') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.service.index'));
        }
    }
}
