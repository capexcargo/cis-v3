<?php

namespace App\Traits\Crm\ServiceRequest\Service;

use App\Models\SrType;

trait ServiceTrait
{

    // public $category_references = [];

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $action_type;



    public function resetForm()
    {
        $this->reset([
            'service',
        ]);
        $this->confirmation_modal = false;
    }

    // public function categoryReferences()

    // {
    //     $this->category_references = SrType::get();
    // }


    public function getRequest()
    {
        return [
            'service' => $this->service,
        ];
    }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToSRTypeMgmt') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.index'));
        } else if ($action_type == 'redirectTosubMgmt') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.sr-sub-category.index'));
        } else if ($action_type == 'redirectToResponse') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.response.index'));
        } else if ($action_type == 'redirectToResolution') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.resolution.index'));
        } else if ($action_type == 'redirectToUrgency') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.urgency.index'));
        } else if ($action_type == 'redirectToHierarchy') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.hierarchy.index'));
        } else if ($action_type == 'redirectToChannel') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.channel.index'));
        } else if ($action_type == 'redirectToService') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.service.index'));
        }
    }
}
