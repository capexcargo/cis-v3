<?php

namespace App\Traits\Crm\ServiceRequest;

use App\Models\ChannelSrSource;
use App\Models\Crm\CrmAccountTypeReference;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\MileResolution;
use App\Models\ServiceRequirements;
use App\Models\SrType;
use App\Models\SrTypeSubcategory;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait ServiceRequestMgmtTrait
{
    public $confirmation_message;
    public $confirmation_modal = false;
    public $create_modal = false;
    public $view_modal = false;
    public $edit_modal = false;

    public $action_type;

    public $subject;

    public $fname;
    public $mname;
    public $lname;

    public $company_business_references = [];
    public $company_business_search;
    public $company_business_name;

    public $contact_person_references = [];
    public $contact_person_search;
    public $primary_contact_person;

    public $mobile_number;
    public $telephone_number;
    public $email_address;
    public $sr_type;
    public $subcategory;
    public $booking_ref_no;
    public $waybill_no;
    public $severity;
    public $service_requirement;
    public $assigned_to;
    public $channel_sr_source;
    public $status = 1;
    public $attachments = [];
    public $sr_description;


    public $account_type;
    public $account_type_cards = [];

    public $sr_type_references = [];
    public $severity_references = [];
    public $sub_category_references = [];
    public $service_requirement_references = [];
    public $assigned_to_references = [];
    public $channel_references = [];
    // public $life_stage_references= [];
    public $clicked_edit;

    public $sr_id;

    public $sr_status;

    public function load()
    {
        $this->loadAccountTypeCards();
        $this->loadContactPersonReferences();
        $this->loadBusinessNameReferences();

        $this->addAttachments();
        $this->srTypeReferences();
        $this->severityReferences();
        // $this->subCategoryReferences();
        $this->serviceRequirementReferences();
        $this->assignedToReferences();
        $this->channelReferences();
    }

    public function loadAccountTypeCards()
    {
        $type = CrmAccountTypeReference::get();

        $this->account_type_cards = [
            [
                'title' => 'Individual',
                'value' => $type[0]['id'],
                'class' => 'text-gray-500 border border-gray-600 shadow-sm rounded-l-md',
                'color' => 'text-blue',
                'action' => 'account_type',
                'id' => 1
            ],
            [
                'title' => 'Corporate',
                'value' => $type[1]['id'],
                'class' => 'border border-gray-600 shadow-sm text-gray-500 rounded-r-md',
                'color' => 'text-blue',
                'action' => 'account_type',
                'id' => 2
            ],
        ];
    }

    protected function updatedAccountType()
    {
        $this->company_business_search = null;
        $this->company_business_name = null;

        $this->contact_person_search = null;
        $this->primary_contact_person = null;
        $this->primary_contact_person = null;

        $this->mobile_number = null;
        $this->telephone_number = null;
        $this->email_address = null;
    }

    public function loadBusinessNameReferences()
    {
        $this->company_business_references = CrmCustomerInformation::where([
            ['fullname', 'like', '%' . $this->company_business_search . '%']
        ])
            ->where('account_type', 2)
            ->orderBy('fullname', 'asc')
            ->take(10)
            ->get();
    }

    protected function updatedCompanyBusinessSearch()
    {
        $this->loadBusinessNameReferences();
    }

    public function getCompanyBusinessDetails($id)
    {
        $customer_data = CrmCustomerInformation::with('contactPersons', 'mobileNumbers', 'emails')->find($id);

        if ($customer_data) {
            $this->company_business_search = $customer_data->fullname;
            $this->company_business_name = $customer_data->id;

            foreach ($customer_data->contactPersons as $i => $contact_person) {
                if ($contact_person->is_primary == 1) {
                    $this->contact_person_search = $contact_person->first_name . " " . $contact_person->middle_name . " " . $contact_person->last_name;
                }
            }

            foreach ($customer_data->mobileNumbers as $i => $mobile_number) {
                if ($mobile_number->is_primary == 1) {
                    $this->mobile_number = $mobile_number->mobile;
                }
            }

            foreach ($customer_data->telephoneNumbers as $i => $telephone_number) {
                if ($telephone_number->is_primary == 1) {
                    $this->telephone_number = $telephone_number->telephone;
                }
            }

            foreach ($customer_data->emails as $i => $email_address) {
                if ($email_address->is_primary == 1) {
                    $this->email_address = $email_address->email;
                }
            }
        }
    }

    public function loadContactPersonReferences()
    {
        $this->contact_person_references = CrmCustomerInformation::where([
            ['fullname', 'like', '%' . $this->contact_person_search . '%']
        ])
            ->where('account_type', 1)
            ->orderBy('fullname', 'asc')
            ->take(10)
            ->get();
    }

    protected function updatedContactPersonSearch()
    {
        $this->loadContactPersonReferences();
    }

    public function getContactDetails($id)
    {
        $customer_data = CrmCustomerInformation::with('mobileNumbers', 'emails')->find($id);

        // dd($customer_data);
        if ($customer_data) {
            $this->contact_person_search = $customer_data->fullname;
            $this->primary_contact_person = $customer_data->id;

            foreach ($customer_data->mobileNumbers as $i => $mobile_number) {
                if ($mobile_number->is_primary == 1) {
                    $this->mobile_number = $mobile_number->mobile;
                }
            }

            foreach ($customer_data->telephoneNumbers as $i => $telephone_number) {
                if ($telephone_number->is_primary == 1) {
                    $this->telephone_number = $telephone_number->telephone;
                }
            }

            foreach ($customer_data->emails as $i => $email_address) {
                if ($email_address->is_primary == 1) {
                    $this->email_address = $email_address->email;
                }
            }
        }
    }

    public function addAttachments()
    {
        $this->attachments[] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }

    public function removeAttachments($i)
    {
        if (count(collect($this->attachments)->where('is_deleted', false)) > 1) {
            if ($this->attachments[$i]['id']) {
                $this->attachments[$i]['is_deleted'] = true;
            } else {
                unset($this->attachments[$i]);
            }

            array_values($this->attachments);
        }
    }

    public function srTypeReferences()
    {
        $this->sr_type_references = SrType::get();
    }

    public function severityReferences()
    {
        $this->severity_references = MileResolution::get();
    }

    // public function subCategoryReferences()
    // {
    //     $this->sub_category_references = SrTypeSubcategory::get();
    // }

    protected function updatedSrType()
    {
        $this->sub_category_references = SrTypeSubcategory::where('sr_type_id', $this->sr_type)->get();

        $this->subcategory = null;
        $this->booking_ref_no = null;
        $this->waybill_no = null;

        if ($this->sr_type == 1) {
            $this->severity = 1;
        } else if ($this->sr_type == 4) {
            $this->severity = 3;
        } else {
            $this->severity = null;
        }
    }

    public function serviceRequirementReferences()
    {
        $this->service_requirement_references = ServiceRequirements::get();
    }

    public function assignedToReferences()
    {
        $this->assigned_to_references = User::where('division_id', 2)->orderBy('name', 'asc')->get();
    }

    public function channelReferences()
    {
        $this->channel_references = ChannelSrSource::get();
    }

    public function getRequest()
    {
        return [
            'account_type' => $this->account_type,
            'subject' => $this->subject,

            'company_business_name' => $this->company_business_name,
            'primary_contact_person' => $this->primary_contact_person,
            'first_name' => $this->fname,
            'middle_name' => $this->mname,
            'last_name' => $this->lname,
            'clicked_edit' => $this->clicked_edit,

            'mobile_number' => $this->mobile_number,
            'telephone_number' => $this->telephone_number,
            'email_address' => $this->email_address,
            'sr_type' => $this->sr_type,
            'subcategory' => $this->subcategory,
            'booking_ref_no' => $this->booking_ref_no,
            'waybill_no' => $this->waybill_no,
            'severity' => $this->severity,
            'service_requirement' => $this->service_requirement,
            'assigned_to' => $this->assigned_to,
            'channel_sr_source' => $this->channel_sr_source,
            'status' => $this->status,

            'attachments' => $this->attachments,

            'sr_description' => $this->sr_description,
        ];
    }


    public function resetForm()
    {
        $this->reset([
            'account_type',
            'subject',

            'company_business_name',
            'primary_contact_person',
            'contact_person_search',

            'mobile_number',
            'telephone_number',
            'email_address',
            'sr_type',
            'subcategory',
            'booking_ref_no',
            'waybill_no',
            'severity',
            'service_requirement',
            'assigned_to',
            'channel_sr_source',
            'status',

            'attachments',

            'sr_description',
        ]);
    }
}
