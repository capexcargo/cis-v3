<?php

namespace App\Traits\Crm\ServiceRequest;

use App\Models\ChannelSrSource;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmMovementSfTypeReferences;
use App\Models\ServiceRequirements;

trait LeadTrait
{
    public $confirmation_modal = false;

    public $service_request;
    public $sr_id;
    public $sr_no;
    public $account_id;
    public $shipment_type_references = [];
    public $service_requirement_references = [];
    public $channel_source_references = [];

    public $lead_name;
    public $shipment_type;
    public $service_requirement;

    public $contact_person;

    public $contact_mobile_no;
    public $contact_email_address;
    public $description;

    public $contact_owner;
    public $customer_type;
    public $channel_source;
    public $currency;

    public $address_type;

    public function load()
    {
        $this->shipmentTypeReferences();
        $this->serviceRequirementReferences();
        $this->channelSourceReferences();
    }

    public function shipmentTypeReferences()
    {
        $this->shipment_type_references = CrmMovementSfTypeReferences::get();
    }

    public function serviceRequirementReferences()
    {
        $this->service_requirement_references = ServiceRequirements::get();
    }

    public function channelSourceReferences()
    {
        $this->channel_source_references = ChannelSrSource::get();
    }

    public function getRequest()
    {
        return [
            'sr_id' => $this->sr_id,
            'sr_no' => $this->sr_no,
            'account_id' => $this->account_id,
            'lead_name' => $this->lead_name,
            'shipment_type' => $this->shipment_type,
            'service_requirement' => $this->service_requirement,
            'description' => $this->description,
            'contact_owner' => $this->assigned_to_id,
            'customer_type' => $this->customer_type_id,
            'channel_source' => $this->channel_source,
            'currency' => $this->currency,
        ];
    }
}
