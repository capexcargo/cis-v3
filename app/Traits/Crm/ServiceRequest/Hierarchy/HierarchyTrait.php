<?php

namespace App\Traits\Crm\ServiceRequest\Hierarchy;

use App\Models\CrmHeirarchyDetails;
use App\Models\SrType;

trait HierarchyTrait
{

    // public $category_references = [];

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $hie_references = [];
    public $action_type;



    public function resetForm()
    {
        $this->reset([
            'email',
            'tagged_position_id',
            'positions',
        ]);

        $this->confirmation_modal = false;
    }

    public function tagHasMany()

    {
        $this->hie_references = CrmHeirarchyDetails::get();
    }


    public function getRequest()
    {
        return [
            'email' => $this->email,
            'positions' => $this->positions,
            'tagged_position_id' => $this->tagged_position_id,
        ];
    }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToSRTypeMgmt') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.index'));
        } else if ($action_type == 'redirectTosubMgmt') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.sr-sub-category.index'));
        } else if ($action_type == 'redirectToResponse') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.response.index'));
        } else if ($action_type == 'redirectToResolution') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.resolution.index'));
        } else if ($action_type == 'redirectToUrgency') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.urgency.index'));
        } else if ($action_type == 'redirectToHierarchy') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.hierarchy.index'));
        } else if ($action_type == 'redirectToChannel') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.channel.index'));
        } else if ($action_type == 'redirectToService') {
            return redirect()->to(route('crm.service-request.sr-related-mgmt.service.index'));
        }
    }
}
