<?php

namespace App\Traits\Crm\Contracts\RequirementsMgmt;

trait RequirementsMgmtTrait
{

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $action_type;

    public $name;

    public function getRequest()
    {
        return [
            'name' =>$this->name,
        ];
    }
    
    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'name',
        ]);        
        $this->confirmation_modal = false;

    }
    
}