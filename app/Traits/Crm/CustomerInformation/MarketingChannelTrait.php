<?php

namespace App\Traits\Crm\CustomerInformation;


trait MarketingChannelTrait
{

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;


    public function resetForm()
    {
        $this->reset([
            'marketing',
        ]);        
        $this->confirmation_modal = false;

    }


    public function getRequest()
    {
        return [
            'marketing' =>$this->marketing,
        ];
        
    }
    
}

