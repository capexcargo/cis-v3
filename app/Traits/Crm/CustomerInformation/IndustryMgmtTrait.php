<?php

namespace App\Traits\Crm\CustomerInformation;


trait IndustryMgmtTrait
{

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;


    public function resetForm()
    {
        $this->reset([
            'industry',
        ]);        
        $this->confirmation_modal = false;

    }


    public function getRequest()
    {
        return [
            'industry' =>$this->industry,
        ];
        
    }
    
}

