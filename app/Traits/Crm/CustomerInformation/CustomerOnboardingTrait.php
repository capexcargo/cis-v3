<?php

namespace App\Traits\Crm\CustomerInformation;

use App\Models\Crm\BarangayReference;
use App\Models\Crm\CityReference;
use App\Models\Crm\CrmAccountTypeReference;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\StateReference;
use App\Models\CrmRateAirFreight;
use App\Models\CrmRateSeaFreight;
use App\Models\Industry;
use App\Models\MarketingChannel;
use App\Models\User;

trait CustomerOnboardingTrait
{
    public $confirmation_message;
    public $confirmation_modal = false;
    public $create_modal = false;
    public $view_modal = false;
    public $edit_modal = false;

    public $header_cards = [];
    public $status_header_cards = [];

    public $action_type;
    public $current_tab = 1;

    public $account_type_cards = [];

    public $name_references = [];
    public $name_search;
    public $full_comp_name;
    public $customer_id;

    public $email_search;
    public $mobile_number_search;
    public $tin_search;

    public $search_request = [];
    public $search_result = [];

    public $account_type;
    public $customer_number;

    public $company_name;
    public $company_industry;
    public $company_website_soc_med;
    public $rate_name;
    public $is_mother_account;
    public $associate_with;
    public $tin;
    public $company_anniversary;

    public $first_name;
    public $middle_name;
    public $last_name;

    public $emails = [];
    public $mobile_numbers = [];
    public $telephone_numbers = [];
    public $addresses = [];

    public $life_stage = "Lead";
    public $contact_owner;
    public $marketing_channel;
    public $notes;


    public $con_per_first_name;
    public $con_per_middle_name;
    public $con_per_last_name;
    public $contact_persons = [];
    public $con_per_emails = [];
    public $con_per_mobile_numbers = [];
    public $con_per_telephone_numbers = [];
    public $con_per_life_stage = "Lead";
    public $con_per_contact_owner;
    public $con_per_marketing_channel;
    public $con_per_notes;

    public $contact_owner_references = [];
    public $associate_with_references = [];
    public $industry_references = [];
    public $marketing_channel_references = [];

    public $state_province_references = [];

    public $rate_references = [];


    public function generateCustomerNo()
    {
        $customer_no = CrmCustomerInformation::select('id')->latest()->first();
        if ($customer_no) {
            $last_inserted_id = $customer_no->id + 1;
        } else {
            $last_inserted_id = 1;
        }


        $this->customer_number = "CN-" . str_pad(($last_inserted_id), 5, "0", STR_PAD_LEFT);
    }

    public function load()
    {
        $this->loadAccountTypeCards();
        $this->loadNameReferences();
        $this->contactOwnerReferences();
        $this->marketingChannelReferences();
        $this->associateWithReferences();
        $this->industryReferences();
        $this->provinceStateReferences();
        $this->loadRateReference();
        

        foreach ($this->emails as $i => $email) {
            if (count($this->emails) <= 1) {
                $this->emails[$i]['default'] = true;
            }
        }

        foreach ($this->mobile_numbers as $i => $mobile_number) {
            if (count($this->mobile_numbers) <= 1) {
                $this->mobile_numbers[$i]['default'] = true;
            }
        }

        foreach ($this->telephone_numbers as $i => $telephone_number) {
            if (count($this->telephone_numbers) <= 1) {
                $this->telephone_numbers[$i]['default'] = true;
            }
        }

        foreach ($this->addresses as $i => $address) {
            if (count($this->addresses) <= 1) {
                $this->addresses[$i]['default'] = true;
            }
        }

        foreach ($this->contact_persons as $i => $contact_person) {
            if (count($this->contact_persons) <= 1) {
                $this->contact_persons[$i]['default'] = true;
            }
        }

        foreach ($this->con_per_emails as $i => $email) {
            if (count($this->con_per_emails) <= 1) {
                $this->con_per_emails[$i]['default'] = true;
            }
        }

        foreach ($this->con_per_mobile_numbers as $i => $email) {
            if (count($this->con_per_mobile_numbers) <= 1) {
                $this->con_per_mobile_numbers[$i]['default'] = true;
            }
        }

        foreach ($this->con_per_telephone_numbers as $i => $email) {
            if (count($this->con_per_telephone_numbers) <= 1) {
                $this->con_per_telephone_numbers[$i]['default'] = true;
            }
        }
    }

    public function loadRateReference()
    {
        $a = CrmRateAirFreight::get();

        $b = CrmRateSeaFreight::get();

        $this->rate_references = $a->merge($b);

        // dd($rate_references);
    }
    public function loadAccountTypeCards()
    {
        $type = CrmAccountTypeReference::get();

        $this->account_type_cards = [
            [
                'title' => 'Individual',
                'class' => 'text-gray-500 shadow-sm rounded-l-md',
                'color' => 'text-blue',
                'action' => 'account_type',
                'id' => 1
            ],
            [
                'title' => 'Corporate',
                'class' => 'text-gray-500 shadow-sm rounded-r-md',
                'color' => 'text-blue',
                'action' => 'account_type',
                'id' => 2
            ],
        ];
    }

    public function loadNameReferences()
    {
        $this->name_references = CrmCustomerInformation::where([
            ['fullname', 'like', '%' . $this->name_search . '%']
        ])
            ->orderBy('fullname', 'asc')
            ->take(10)
            ->get();
    }

    public function associateWithReferences()
    {
        $this->associate_with_references = CrmCustomerInformation::get();
    }

    public function industryReferences()
    {
        $this->industry_references = Industry::get();
    }

    public function contactOwnerReferences()
    {
        $this->contact_owner_references = User::where('division_id', 2)->get();
        // ->orWhere('level_id', 5)->get();
    }

    public function marketingChannelReferences()
    {
        $this->marketing_channel_references = MarketingChannel::get();
    }

    public function provinceStateReferences()
    {
        $this->state_province_references = StateReference::get();
    }

    public function getAddressType($i)
    {
        if ($this->addresses[$i]['address_type'] == 2) {
            $this->state_province_references = [];
            $this->addresses[$i]['city_municipal_references'] = [];
            $this->addresses[$i]['postal_code'] = null;
        } else {
            $this->provinceStateReferences();
        }
    }

    public function  getStateProvinceID($i)
    {
        $this->addresses[$i]['city_municipal_references'] = [];
        $this->addresses[$i]['postal_code'] = null;
        $this->addresses[$i]['barangay_references'] = [];

        $this->addresses[$i]['city_municipal_references'] = CityReference::where('state_id', $this->addresses[$i]['state_province'])->get();
    }

    public function  getCityMunicipalID($i)
    {
        foreach ($this->addresses[$i]['city_municipal_references'] as $a => $city_municipal) {
            if ($city_municipal['id'] == $this->addresses[$i]['city_municipality']) {
                $this->addresses[$i]['postal_code'] = $city_municipal['city_postal'];
                $this->addresses[$i]['barangay'] = [];
            }
        }
        $this->addresses[$i]['barangay_references'] = BarangayReference::where('state_id', $this->addresses[$i]['state_province'])
            ->where('city_id', $this->addresses[$i]['city_municipality'])->get();
    }

    public function getCustomerDetails($id)
    {
        $customer_data = CrmCustomerInformation::find($id);

        if ($customer_data) {
            $this->name_search = $customer_data->fullname;
            $this->full_comp_name = $customer_data->id;
        }
    }

    protected function updatedNameSearch()
    {
        $this->loadNameReferences();
    }

    protected function updatedAccountType()
    {
        // $this->resetForm();

        if ($this->account_type == 2) {
            foreach ($this->addresses as $i => $address) {
                if (count($this->addresses) <= 1) {
                    $this->addresses[$i]['address_label'] = 2;
                }
            }
        }
    }

    protected function updatedIsMotherAccount()
    {
        if ($this->is_mother_account == 1) {
            $this->reset(['associate_with']);
        }
    }

    public function addEmails()
    {
        $this->emails[] = [
            'id' => null,
            'email' => null,
            'default' => false,
            'is_deleted' => false,
        ];
    }

    public function removeEmails($i)
    {
        if (count(collect($this->emails)->where('is_deleted', false)) > 1) {
            if ($this->emails[$i]['id']) {
                $this->emails[$i]['is_deleted'] = true;
            } else {
                unset($this->emails[$i]);
            }

            array_values($this->emails);
        }
    }

    public function checkEmailDefault($count)
    {
        foreach ($this->emails as $i => $email) {
            if ($i == $count) {
                $this->emails[$i]['default'] = true;
            }
            if ($i != $count) {
                $this->emails[$i]['default'] = false;
            }
        }
    }

    public function addMobileNumbers()
    {
        $this->mobile_numbers[] = [
            'id' => null,
            'mobile' => null,
            'default' => false,
            'is_deleted' => false,
            'status' => null,
        ];
    }

    public function removeMobileNumbers($i)
    {
        if (count(collect($this->mobile_numbers)->where('is_deleted', false)) > 1) {
            if ($this->mobile_numbers[$i]['id']) {
                $this->mobile_numbers[$i]['is_deleted'] = true;
            } else {
                unset($this->mobile_numbers[$i]);
            }

            array_values($this->mobile_numbers);
        }
    }

    public function checkMobileNumberDefault($count)
    {
        foreach ($this->mobile_numbers as $i => $mobile_number) {
            if ($i == $count) {
                $this->mobile_numbers[$i]['default'] = true;
            }
            if ($i != $count) {
                $this->mobile_numbers[$i]['default'] = false;
            }
        }
    }

    public function addTelephoneNumbers()
    {
        $this->telephone_numbers[] = [
            'id' => null,
            'telephone' => null,
            'default' => false,
            'is_deleted' => false,
        ];
    }

    public function removeTelephoneNumbers($i)
    {
        if (count(collect($this->telephone_numbers)->where('is_deleted', false)) > 1) {
            if ($this->telephone_numbers[$i]['id']) {
                $this->telephone_numbers[$i]['is_deleted'] = true;
            } else {
                unset($this->telephone_numbers[$i]);
            }
            array_values($this->telephone_numbers);
        }
    }

    public function checkTelephoneNumberDefault($count)
    {
        foreach ($this->telephone_numbers as $i => $telephone_number) {
            if ($i == $count) {
                $this->telephone_numbers[$i]['default'] = true;
            }
            if ($i != $count) {
                $this->telephone_numbers[$i]['default'] = false;
            }
        }
    }

    public function addAddresses()
    {
        $this->addresses[] = [
            'id' => null,
            'address_type' => 1,
            'country' => null,
            'address_line1' => null,
            'address_line2' => null,
            'state_province' => null,
            'city_municipality' => null,
            'barangay' => null,
            'postal_code' => null,
            'address_label' => null,
            'city_municipal_references' => [],
            'barangay_references' => [],
            'default' => false,
            'is_deleted' => false,
        ];
        $i = 0;

        // $this->getStateProvinceID($i);
        // $this->getCityMunicipalID($i);

        foreach ($this->addresses as $i => $address) {
            $this->addresses[$i]['address_label'] = 2;
        }
    }

    public function removeAddresses($i)
    {
        if (count(collect($this->addresses)->where('is_deleted', false)) > 1) {
            if ($this->addresses[$i]['id']) {
                $this->addresses[$i]['is_deleted'] = true;
            } else {
                unset($this->addresses[$i]);
            }

            array_values($this->addresses);
        }
    }

    public function checkAddressDefault($count)
    {
        foreach ($this->addresses as $i => $address) {
            if ($i == $count) {
                $this->addresses[$i]['default'] = true;
            }
            if ($i != $count) {
                $this->addresses[$i]['default'] = false;
            }
        }
    }

    public function addContactPersons()
    {
        $this->contact_persons[] = [
            'id' => null,
            'first_name' => null,
            'middle_name' => null,
            'last_name' => null,
            'position' => null,
            'emails' => [],
            'telephone_numbers' => [],
            'mobile_numbers' => [],
            'default' => false,
            'is_deleted' => false,
        ];

        $numItems = count($this->contact_persons);
        $i = 0;
        foreach ($this->contact_persons as $k => $con_per) {
            if (++$i === $numItems) {
                $this->addConPerEmails($k);
                $this->addConPerTelephoneNumbers($k);
                $this->addConPerMobileNumbers($k);
            }
        }
    }

    public function removeContactPersons($i)
    {
        if (count(collect($this->contact_persons)->where('is_deleted', false)) > 1) {
            if ($this->contact_persons[$i]['id']) {
                $this->contact_persons[$i]['is_deleted'] = true;
            } else {
                unset($this->contact_persons[$i]);
            }

            array_values($this->contact_persons);
        }
    }

    public function checkContactPersonDefault($count)
    {
        foreach ($this->contact_persons as $i => $contact_person) {
            if ($i == $count) {
                $this->contact_persons[$i]['default'] = true;
            }
            if ($i != $count) {
                $this->contact_persons[$i]['default'] = false;
            }
        }
    }

    public function addConPerEmails($i)
    {
        $this->contact_persons[$i]['emails'][] = [
            'id' => null,
            'email' => null,
            'default' => false,
            'is_deleted' => false,
        ];

        $this->contact_persons[$i]['emails'][0]['default'] = true;
    }

    public function removeConPerEmails($i, $x)
    {
        if (count(collect($this->contact_persons[$i]['emails'])->where('is_deleted', false)) > 1) {
            if ($this->contact_persons[$i]['emails'][$x]['id']) {
                $this->contact_persons[$i]['emails'][$x]['is_deleted'] = true;
            } else {
                unset($this->contact_persons[$i]['emails'][$x]);
            }
            $this->contact_persons[$i];
            array_values($this->contact_persons[$i]['emails']);
        }
    }

    public function checkConPerEmailDefault($count)
    {
        foreach ($this->contact_persons as $i => $contact_person) {
            foreach ($this->contact_persons[$i]['emails'] as $x => $email) {
                if ($x == $count) {
                    $this->contact_persons[$i]['emails'][$x]['default'] = true;
                }
                if ($x != $count) {
                    $this->contact_persons[$i]['emails'][$x]['default'] = false;
                }
            }
        }
    }

    public function addConPerTelephoneNumbers($i)
    {
        $this->contact_persons[$i]['telephone_numbers'][] = [
            'id' => null,
            'telephone' => null,
            'default' => false,
            'is_deleted' => false,
        ];
        $this->contact_persons[$i]['telephone_numbers'][0]['default'] = true;
    }

    public function removeConPerTelephoneNumbers($i, $x)
    {
        if (count(collect($this->contact_persons[$i]['telephone_numbers'])->where('is_deleted', false)) > 1) {
            if ($this->contact_persons[$i]['telephone_numbers'][$x]['id']) {
                $this->contact_persons[$i]['telephone_numbers'][$x]['is_deleted'] = true;
            } else {
                unset($this->contact_persons[$i]['telephone_numbers'][$x]);
            }
            $this->contact_persons[$i];
            array_values($this->contact_persons[$i]['telephone_numbers']);
        }
    }

    public function checkConPerTelephoneNumberDefault($count)
    {
        foreach ($this->contact_persons as $i => $contact_person) {
            foreach ($this->contact_persons[$i]['telephone_numbers'] as $x => $email) {
                if ($x == $count) {
                    $this->contact_persons[$i]['telephone_numbers'][$x]['default'] = true;
                }
                if ($x != $count) {
                    $this->contact_persons[$i]['telephone_numbers'][$x]['default'] = false;
                }
            }
        }
    }

    public function addConPerMobileNumbers($i)
    {
        $this->contact_persons[$i]['mobile_numbers'][] = [
            'id' => null,
            'mobile' => null,
            'default' => false,
            'is_deleted' => false,
            'status' => null,
        ];
        $this->contact_persons[$i]['mobile_numbers'][0]['default'] = true;
    }

    public function removeConPerMobileNumbers($i, $x)
    {
        if (count(collect($this->contact_persons[$i]['mobile_numbers'])->where('is_deleted', false)) > 1) {
            if ($this->contact_persons[$i]['mobile_numbers'][$x]['id']) {
                $this->contact_persons[$i]['mobile_numbers'][$x]['is_deleted'] = true;
            } else {
                unset($this->contact_persons[$i]['mobile_numbers'][$x]);
            }
            $this->contact_persons[$i];
            array_values($this->contact_persons[$i]['mobile_numbers']);
        }
    }

    public function checkConPerMobileNumberDefault($count)
    {
        foreach ($this->contact_persons as $i => $contact_person) {
            foreach ($this->contact_persons[$i]['mobile_numbers'] as $x => $email) {
                if ($x == $count) {
                    $this->contact_persons[$i]['mobile_numbers'][$x]['default'] = true;
                }
                if ($x != $count) {
                    $this->contact_persons[$i]['mobile_numbers'][$x]['default'] = false;
                }
            }
        }
    }

    public function getRequest()
    {
        return [
            'account_type' => $this->account_type ?? 1,
            'customer_number' => $this->customer_number,

            //Corporate
            'company_name' => $this->company_name,
            'company_industry' => $this->company_industry,
            'company_website_soc_med' => $this->company_website_soc_med,
            'rate_name' => $this->rate_name,
            'is_mother_account' => $this->is_mother_account,
            'associate_with' => $this->associate_with,
            'tin' => $this->tin,
            'company_anniversary' => $this->company_anniversary,

            //Individual
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'last_name' => $this->last_name,

            'emails' => $this->emails,
            'mobile_numbers' => $this->mobile_numbers,
            'telephone_numbers' => $this->telephone_numbers,
            'addresses' => $this->addresses,

            'life_stage' => $this->life_stage,
            'contact_owner' => $this->contact_owner,
            'marketing_channel' => $this->marketing_channel,
            'notes' => $this->notes,

            //Contact Person
            // 'con_per_first_name' => $this->con_per_first_name,
            // 'con_per_middle_name' => $this->con_per_middle_name,
            // 'con_per_last_name' => $this->con_per_last_name,
            'contact_persons' => $this->contact_persons,
            // 'con_per_emails' => $this->con_per_emails,
            // 'con_per_mobile_numbers' => $this->con_per_mobile_numbers,
            // 'con_per_telephone_numbers' => $this->con_per_telephone_numbers,
            'con_per_life_stage' => $this->con_per_life_stage,
            'con_per_contact_owner' => $this->con_per_contact_owner,
            'con_per_marketing_channel' => $this->con_per_marketing_channel,
            'con_per_notes' => $this->con_per_notes,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'customer_number',

            //Corporate
            'company_name',
            'company_industry',
            'company_website_soc_med',
            'rate_name',
            'is_mother_account',
            'associate_with',
            'tin',
            'company_anniversary',

            //Individual
            'first_name',
            'middle_name',
            'last_name',

            'emails',
            'mobile_numbers',
            'telephone_numbers',
            'addresses',

            'life_stage',
            'contact_owner',
            'marketing_channel',
            'notes',
        ]);

        $this->confirmation_modal = false;
    }
}
