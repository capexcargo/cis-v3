<?php

namespace App\Traits\Crm\Activities;

use App\Models\Crm\CrmCustomerInformation;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait LogMeetingTraits
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $date_created;
    public $action_type;

    public $create_modal_log_meeting = false;
    public $confirmation_modal_meeting = false;

    public $customer_nos = [];
    public $customer_id_meeting;
    public $attendees;
    public $outcome;
    public $datetime;
    public $time;
    public $notes;
    public $sr_no;

    public $tagsasdasd = [];

    protected function updatedCustomerIdMeeting()
    {
        $this->customer_nos = CrmCustomerInformation::where([
            ['fullname', 'like', '%' . $this->customer_id_meeting . '%']
        ])
            ->orderBy('fullname', 'asc')
            ->take(10)
            ->get();
    }

    public function getCustomerDetailsMeeting($id)
    {
        $name = CrmCustomerInformation::find($id);

        $this->customer_id_meeting = $name->fullname;
    }

    public function getRequestMeeting()
    {
        return [
            'customer_id_meeting' => $this->customer_id_meeting,
            'attendees' => $this->attendees,
            'outcome' => $this->outcome,
            'datetime' => $this->datetime,
            'time' => $this->time,
            'notes' => $this->notes,
            'sr_no' => $this->sr_no
        ];
    }

    public function resetFormMeeting()
    {
        $this->resetErrorBag();
        $this->reset([
            'customer_id_meeting',
            'attendees',
            'outcome',
            'datetime',
            'time',
            'notes',
            'sr_no',
        ]);

        $this->confirmation_modal = false;
    }
}
