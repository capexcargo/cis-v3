<?php

namespace App\Traits\Crm\Activities\ScheduleMeeting;

trait ScheduleMeetingTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $date_created;
    public $action_type;

    public $create_modal = false;
    public $edit_modal = false;
    public $confirmation_modal;
    public $reactivate_modal = false;
    public $deactivate_modal = false;

    public $name;
    public $oda_id;


    public function getRequest()
    {
        return [
            'name' => $this->name,
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'name',
        ]);

        $this->confirmation_modal = false;
    }
    
}
