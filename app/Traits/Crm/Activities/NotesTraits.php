<?php

namespace App\Traits\Crm\Activities;

use App\Models\Crm\CrmCustomerInformation;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait NotesTraits
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $date_created;
    public $action_type;

    public $create_modal_note = false;
    public $confirmation_modal_note = false;

    public $customer_nos = [];
    public $customer_id_notes;
    public $title_notes;
    public $notes;
    public $sr_no_notes;

    public function updatedTitleNotes(){
        $this->resetErrorBag('title_notes');
    }

    public function updatedNotes(){
        $this->resetErrorBag('notes');
    }

    public function updatedSrNoNotes(){
        $this->resetErrorBag('sr_no_notes');
    }

    protected function updatedCustomerIdNotes()
    {
        $this->customer_nos = CrmCustomerInformation::where([
            ['fullname', 'like', '%' . $this->customer_id_notes . '%']
        ])
            ->orderBy('fullname', 'asc')
            ->take(10)
            ->get();
    }

    public function getCustomerDetailsNotes($id)
    {
        $name = CrmCustomerInformation::find($id);

        $this->customer_id_notes = $name->fullname;

    }

    public function getRequestNotes()
    {
        return [
            'customer_id' => $this->customer_id_notes,
            'title' => $this->title_notes,
            'notes' => $this->notes,
            'sr_no' => $this->sr_no_notes
        ];
    }

    public function resetFormNotes()
    {
        $this->resetErrorBag();
        $this->reset([
            'customer_id_notes',
            'title_notes',
            'notes',
            'sr_no_notes',
        ]);

        $this->confirmation_modal_note = false;
    }
}
