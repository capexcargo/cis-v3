<?php

namespace App\Traits\Crm\Activities;

use App\Models\Crm\CrmCustomerInformation;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait TaskTraits
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $date_created;
    public $action_type;

    public $create_modal_task = false;
    public $confirmation_modal_task = false;

    public $customer_nos = [];
    public $customer_id;
    public $title;
    public $priority;
    public $due_date;
    public $task;
    public $sr_no;

    public function updatedTitle(){
        $this->resetErrorBag('title');
    }

    public function updatedDueDate(){
        $this->resetErrorBag('due_date');
    }

    public function updatedTask(){
        $this->resetErrorBag('task');
    }

    public function updatedSrNo(){
        $this->resetErrorBag('sr_no');
    }

    protected function updatedCustomerId()
    {
        $this->customer_nos = CrmCustomerInformation::where([
            ['fullname', 'like', '%' . $this->customer_id . '%']
        ])
            ->orderBy('fullname', 'asc')
            ->take(10)
            ->get();
    }

    public function getCustomerDetails($id)
    {
        $name = CrmCustomerInformation::find($id);

        $this->customer_id = $name->fullname;
    }

    public function getRequest()
    {
        return [
            'customer_id' => $this->customer_id,
            'title' => $this->title,
            'priority' => $this->priority,
            'due_date' => $this->due_date,
            'task' => $this->task,
            'sr_no' => $this->sr_no
        ];
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'customer_id',
            'title',
            'priority',
            'due_date',
            'task',
            'sr_no',
        ]);

        $this->confirmation_modal = false;
    }
}
