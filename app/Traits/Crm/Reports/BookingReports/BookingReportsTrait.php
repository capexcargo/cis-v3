<?php

namespace App\Traits\Crm\Reports\BookingReports;

use App\Models\ChannelSrSource;
use App\Models\MileResolution;

trait BookingReportsTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $action_type;
    

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToBooking') {
            return redirect()->to(route('crm.sales.booking-mgmt.booking-history.index2'));
        } 
    }
}
