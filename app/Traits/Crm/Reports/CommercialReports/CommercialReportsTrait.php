<?php

namespace App\Traits\Crm\Reports\CommercialReports;

use App\Models\ChannelSrSource;
use App\Models\MileResolution;

trait CommercialReportsTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToCommercial') {
            return redirect()->to(route('crm.commercials.commercial-mgmt.index'));
        } 
    }
}
