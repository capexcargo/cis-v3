<?php

namespace App\Traits\Crm\Reports\ContractsReports;


trait ContractsReportsTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;

    public $date_created_search;

    public $severity_references = [];
    public $channel_references = [];
  
    public function load()
    {
        // $this->severityReferences();
        // $this->channelReferences();
    }

    // public function severityReferences()
    // {
    //     $this->severity_references = MileResolution::get();
    // }

    // public function channelReferences()
    // {
    //     $this->channel_references = ChannelSrSource::get();
    // }

    

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToAccountsApplication') {
            return redirect()->to(route('crm.contracts.accounts-application.index'));
        } 
        // else if ($action_type == 'redirectTobookhis') {
        //     return redirect()->to(route('crm.sales.booking-mgmt.booking-history.index2'));
        // }
    }
}
