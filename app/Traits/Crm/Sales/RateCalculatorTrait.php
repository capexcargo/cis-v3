<?php

namespace App\Traits\Crm\Sales;

use App\Models\BranchReference;
use App\Models\Crm\CrmMovementSfTypeReferences;
use App\Models\CrmBookingType;
use App\Models\CrmCommodityType;
use App\Models\CrmCrateType;
use App\Models\CrmModeOfPaymentReference;
use App\Models\CrmRateAirFreight;
use App\Models\CrmRateSeaFreight;
use App\Models\CrmServiceMode;
use App\Models\CrmShipmentType;
use App\Models\CrmTransportMode;

trait RateCalculatorTrait
{
    public $transport_mode;
    public $origin;
    public $destination;
    public $transhipment;

    public $rate_calculator = [];

    public $declared_value;
    public $commodity_type;
    public $commodity_applicable_rate;
    public $paymode;
    public $service_mode;

    public $transport_mode_references = [];
    public $movement_sf_type_references = [];
    public $booking_type_references = [];
    public $origin_references = [];
    public $destination_references = [];
    public $transhipment_references = [];

    public $commodity_type_references = [];
    public $commodity_applicable_rate_references = [];
    public $paymode_references = [];
    public $service_mode_references = [];
    public $crating_type_references = [];

    public $air_cargo = false;
    public $air_cargos = [];
    public $air_pouch = false;
    public $air_pouches = [];
    public $air_box = false;
    public $air_boxes = [];

    public $movement_sf_type;
    public $sea_cargo = false;
    public $sea_cargos_is_lcl = [];
    public $sea_cargos_is_fcl = [];
    public $sea_cargos_is_rcl = [];
    public $sea_box = false;
    public $sea_boxes = [];

    public $land_packaging_type = 1;
    public $booking_type;
    public $land = false;
    public $lands_shippers_box = [];
    public $lands_pouches = [];
    public $lands_boxes = [];
    public $lands_ftl = [];

    public $for_crating = false;

    public $other_services_measurements = [];

    public $service_type_references = [];

    public $domestic_freight = false;
    public $iff = false;
    public $project_shipment = false;
    public $brokerage = false;
    public $ftl = false;

    // BREAKDOWN OF FREIGHT CHARGES
    public $weight_charge;
    public $awb_fee;
    public $doc_fee;
    public $cod_charge;
    public $valuation;
    public $insurance;
    public $handling_fee;
    public $subtotal;
    public $grand_total;

    public $other_fees = false;
    public $evat = true;
    public $opa_fee;
    public $oda_fee;
    public $equipment_rental;
    public $crating_fee;
    public $lashing_fee;
    public $manpower_fee;
    public $dangerous_goods_fee;
    public $trucking_fee;
    public $perishable_fee;
    public $packaging_fee;
    public $discount_amount;
    public $discount_percentage;

    //Other Services
    public $os_transport_mode_references = [];
    public $os_origin_references = [];
    public $os_destination_references = [];
    public $os_transhipment_references = [];
    public $os_commodity_type_references = [];
    public $os_commodity_applicable_rate_references = [];
    public $os_paymode_references = [];
    public $os_service_mode_references = [];
    public $os_crating_type_references = [];


    public $os_transport_mode;
    public $os_origin;
    public $os_destination;
    public $os_transhipment;
    public $os_declared_value;
    public $os_commodity_type;
    public $os_commodity_applicable_rate;
    public $os_paymode;
    public $os_service_mode;

    public $os_weight_charge;
    public $os_awb_fee;
    public $os_doc_fee;
    public $os_cod_charge;
    public $os_valuation;
    public $os_insurance;
    public $os_handling_fee;
    public $os_subtotal;

    public $os_other_fees = true;
    public $os_evat = true;

    public $os_grand_total;

    public $os_transhipment_type = 1;

    public function transportModeReferences()
    {
        $this->transport_mode_references = CrmTransportMode::get();
        $this->os_transport_mode_references = CrmTransportMode::get();
    }

    public function movementSfTypeReferences()
    {
        $this->movement_sf_type = 1;
        $this->movement_sf_type_references = CrmMovementSfTypeReferences::get();
    }

    public function bookingTypeReferences()
    {
        $this->booking_type = 1;
        $this->booking_type_references = CrmBookingType::get();
    }

    public function addAirCargos()
    {
        $this->air_cargos[] = [
            'id' => null,
            'qty' => null,
            'wt' => null,
            'dimension_l' => null,
            'dimension_w' => null,
            'dimension_h' => null,
            'unit_of_measurement' => null,
            'measurement_type' => null,
            'type_of_packaging' => null,
            'for_crating' => null,
            'crating_type' => null,
            'cwt' => null,
            'is_deleted' => false,
        ];
    }

    public function removeAirCargos($i)
    {
        if (count(collect($this->air_cargos)->where('is_deleted', false)) > 1) {
            if ($this->air_cargos[$i]['id']) {
                $this->air_cargos[$i]['is_deleted'] = true;
            } else {
                unset($this->air_cargos[$i]);
            }

            array_values($this->air_cargos);
        }
    }

    public function addAirPouches()
    {
        $this->air_pouches[] = [
            'id' => null,
            'qty' => null,
            'pouch_size' => null,
            'amount' => null,
            'is_deleted' => false,
        ];
    }

    public function removeAirPouches($i)
    {
        if (count(collect($this->air_pouches)->where('is_deleted', false)) > 1) {
            if ($this->air_pouches[$i]['id']) {
                $this->air_pouches[$i]['is_deleted'] = true;
            } else {
                unset($this->air_pouches[$i]);
            }

            array_values($this->air_pouches);
        }
    }

    public function addAirBoxes()
    {
        $this->air_boxes[] = [
            'id' => null,
            'qty' => null,
            'box_size' => null,
            'weight' => null,
            'amount' => null,
            'is_deleted' => false,
        ];
    }

    public function removeAirBoxes($i)
    {
        if (count(collect($this->air_boxes)->where('is_deleted', false)) > 1) {
            if ($this->air_boxes[$i]['id']) {
                $this->air_boxes[$i]['is_deleted'] = true;
            } else {
                unset($this->air_boxes[$i]);
            }

            array_values($this->air_boxes);
        }
    }

    public function addSeaCargosLcl()
    {
        $this->sea_cargos_is_lcl[] = [
            'id' => null,
            'qty' => null,
            'wt' => null,
            'dimension_l' => null,
            'dimension_w' => null,
            'dimension_h' => null,
            'unit_of_measurement' => null,
            'measurement_type' => null,
            'type_of_packaging' => null,
            'for_crating' => null,
            'crating_type' => null,
            'cbm' => null,
            'is_deleted' => false,
        ];
    }

    public function removeSeaCargos($i)
    {
        if (count(collect($this->sea_cargos_is_lcl)->where('is_deleted', false)) > 1) {
            if ($this->sea_cargos_is_lcl[$i]['id']) {
                $this->sea_cargos_is_lcl[$i]['is_deleted'] = true;
            } else {
                unset($this->sea_cargos_is_lcl[$i]);
            }

            array_values($this->sea_cargos_is_lcl);
        }
    }

    public function addSeaCargosFcl()
    {
        $this->sea_cargos_is_fcl[] = [
            'id' => null,
            'qty' => null,
            'container' => null,
            'is_deleted' => false,
        ];
    }

    public function removeSeaCargosFcl($i)
    {
        if (count(collect($this->sea_cargos_is_fcl)->where('is_deleted', false)) > 1) {
            if ($this->sea_cargos_is_fcl[$i]['id']) {
                $this->sea_cargos_is_fcl[$i]['is_deleted'] = true;
            } else {
                unset($this->sea_cargos_is_fcl[$i]);
            }

            array_values($this->sea_cargos_is_fcl);
        }
    }

    public function addSeaCargosRcl()
    {
        $this->sea_cargos_is_rcl[] = [
            'id' => null,
            'qty' => null,
            'wt' => null,
            'dimension_l' => null,
            'dimension_w' => null,
            'dimension_h' => null,
            'unit_of_measurement' => null,
            'measurement_type' => null,
            'amount' => null,
            'for_crating' => null,
            'crating_type' => null,
            'cbm' => null,
            'is_deleted' => false,
        ];
    }

    public function removeSeaCargosRcl($i)
    {
        if (count(collect($this->sea_cargos_is_rcl)->where('is_deleted', false)) > 1) {
            if ($this->sea_cargos_is_rcl[$i]['id']) {
                $this->sea_cargos_is_rcl[$i]['is_deleted'] = true;
            } else {
                unset($this->sea_cargos_is_rcl[$i]);
            }

            array_values($this->sea_cargos_is_rcl);
        }
    }

    public function addSeaBoxes()
    {
        $this->sea_boxes[] = [
            'id' => null,
            'qty' => null,
            'box_size' => null,
            'weight' => null,
            'amount' => null,
            'is_deleted' => false,
        ];
    }

    public function removeSeaBoxes($i)
    {
        if (count(collect($this->sea_boxes)->where('is_deleted', false)) > 1) {
            if ($this->sea_boxes[$i]['id']) {
                $this->sea_boxes[$i]['is_deleted'] = true;
            } else {
                unset($this->sea_boxes[$i]);
            }

            array_values($this->sea_boxes);
        }
    }

    public function addLandsShippersBox()
    {
        $this->lands_shippers_box[] = [
            'id' => null,
            'total_qty' => null,
            'total_weight' => null,
            'dimension_l' => null,
            'dimension_w' => null,
            'dimension_h' => null,
            'cbm' => null,
            'cwt' => null,
            'land_rate' => null,
            'rate_amount' => null,
            'is_deleted' => false,
        ];
    }

    public function removeLandsShippersBox($i)
    {
        if (count(collect($this->lands_shippers_box)->where('is_deleted', false)) > 1) {
            if ($this->lands_shippers_box[$i]['id']) {
                $this->lands_shippers_box[$i]['is_deleted'] = true;
            } else {
                unset($this->lands_shippers_box[$i]);
            }

            array_values($this->lands_shippers_box);
        }
    }

    public function addLandsPouches()
    {
        $this->lands_pouches[] = [
            'id' => null,
            'size' => null,
            'weight_per_pouch' => null,
            'cbm' => null,
            'total_amount' => null,
            'is_deleted' => false,
        ];
    }

    public function removeLandsPouches($i)
    {
        if (count(collect($this->lands_pouches)->where('is_deleted', false)) > 1) {
            if ($this->lands_pouches[$i]['id']) {
                $this->lands_pouches[$i]['is_deleted'] = true;
            } else {
                unset($this->lands_pouches[$i]);
            }

            array_values($this->lands_pouches);
        }
    }

    public function addLandsBoxes()
    {
        $this->lands_boxes[] = [
            'id' => null,
            'size' => null,
            'weight_per_box' => null,
            'cbm' => null,
            'total_amount' => null,
            'is_deleted' => false,
        ];
    }

    public function removeLandsBoxes($i)
    {
        if (count(collect($this->lands_boxes)->where('is_deleted', false)) > 1) {
            if ($this->lands_boxes[$i]['id']) {
                $this->lands_boxes[$i]['is_deleted'] = true;
            } else {
                unset($this->lands_boxes[$i]);
            }

            array_values($this->lands_boxes);
        }
    }

    public function addLandsFtl()
    {
        $this->lands_ftl[] = [
            'id' => null,
            'quantity' => null,
            'truck_type' => null,
            'unit_price' => null,
            'amount' => null,
            'is_deleted' => false,
        ];
    }

    public function removeLandsFtl($i)
    {
        if (count(collect($this->lands_ftl)->where('is_deleted', false)) > 1) {
            if ($this->lands_ftl[$i]['id']) {
                $this->lands_ftl[$i]['is_deleted'] = true;
            } else {
                unset($this->lands_ftl[$i]);
            }

            array_values($this->lands_ftl);
        }
    }

    public function isForCreating($id, $trans_mode, $is_true)
    {
        if ($trans_mode == 1) {
            if ($is_true == true) {
                foreach ($this->air_cargos as $i => $ac) {
                    if ($i == $id) {
                        $this->air_cargos[$i]['for_crating'] = true;
                    }
                }
            } else {
                $this->air_cargos[$id]['for_crating'] = false;
                $this->air_cargos[$id]['crating_type'] = null;
            }
        }

        if ($trans_mode == 2) {
            if ($is_true == true) {
                foreach ($this->sea_cargos_is_lcl as $i => $sc) {
                    if ($i == $id) {
                        $this->sea_cargos_is_lcl[$i]['for_crating'] = true;
                    }
                }
            } else {
                $this->sea_cargos_is_lcl[$id]['for_crating'] = false;
                $this->air_cargos[$id]['crating_type'] = null;
            }
        }

        if ($trans_mode == 3) {
            if ($is_true == true) {
                foreach ($this->sea_cargos_is_rcl as $i => $sc) {
                    if ($i == $id) {
                        $this->sea_cargos_is_rcl[$i]['for_crating'] = true;
                    }
                }
            } else {
                $this->sea_cargos_is_rcl[$id]['for_crating'] = false;
                $this->air_cargos[$id]['crating_type'] = null;
            }
        }


        $this->crating_type_references = CrmCrateType::get();
    }

    protected function updatedTransportMode()
    {
        $this->origin_references = BranchReference::get();
        $this->destination_references = BranchReference::get();
        $this->transhipment_references = CrmShipmentType::get();
        $this->commodity_type_references = CrmCommodityType::get();
        $this->paymode_references = CrmModeOfPaymentReference::get();

        if ($this->transport_mode == 1) {
            $this->commodity_applicable_rate_references = CrmRateAirFreight::where('transport_mode_id', $this->transport_mode)->get();
            $this->service_mode_references = CrmServiceMode::whereIn('id', [1, 2, 3, 4])->get();
        } elseif ($this->transport_mode == 2) {
            $this->updatedTranshipment();
            $this->service_mode_references = CrmServiceMode::whereIn('id', [5, 6, 7, 8])->get();
        } elseif ($this->transport_mode == 3) {
            $this->commodity_applicable_rate_references = [];
            $this->service_mode_references = CrmServiceMode::get();
        } else {
            $this->commodity_applicable_rate_references = [];
        }

        $this->resetForm();
        $this->resetCharges();
        $this->addAirCargos();
        $this->addAirPouches();
        $this->addAirBoxes();
        $this->resetErrorBag();

        $this->air_cargo = false;
        $this->air_pouch = false;
        $this->air_box = false;
    }

    protected function updatedTranshipment()
    {
        $this->commodity_applicable_rate_references = CrmRateSeaFreight::where('transport_mode_id', $this->transport_mode)->where('shipment_type_id', $this->transhipment)->get();
    }

    protected function updatedAirCargo()
    {
        if ($this->air_cargo == false) {
            $this->reset([
                'air_cargos'
            ]);
            $this->resetCharges();
            $this->addAirCargos();
        }
    }

    protected function updatedAirPouch()
    {
        if ($this->air_pouch == false) {
            $this->reset([
                'air_pouches'
            ]);
            $this->resetCharges();
            $this->addAirPouches();
        }
    }

    protected function updatedAirBox()
    {
        if ($this->air_box == false) {
            $this->reset([
                'air_boxes'
            ]);
            $this->resetCharges();
            $this->addAirBoxes();
        }
    }

    protected function updatedSeaCargo()
    {
        if ($this->sea_cargo == false) {
            $this->reset([
                'sea_cargos_is_lcl',
                'sea_cargos_is_fcl',
                'sea_cargos_is_rcl',
            ]);
            $this->movement_sf_type = 1;
            $this->addSeaCargosLcl();
        }
    }

    protected function updatedMovementSfType()
    {
        if ($this->movement_sf_type == 1) {
            $this->reset([
                'sea_cargos_is_fcl',
                'sea_cargos_is_rcl'
            ]);
            $this->addSeaCargosLcl();
        } elseif ($this->movement_sf_type == 2) {
            $this->reset([
                'sea_cargos_is_lcl',
                'sea_cargos_is_rcl'
            ]);
            $this->addSeaCargosFcl();
        } elseif ($this->movement_sf_type == 3) {
            $this->reset([
                'sea_cargos_is_lcl',
                'sea_cargos_is_fcl'
            ]);
            $this->addSeaCargosRcl();
        }
    }

    protected function updatedSeaBox()
    {
        if ($this->sea_box == false) {
            $this->reset([
                'sea_boxes'
            ]);
            $this->addSeaBoxes();
        }
    }

    protected function updatedLand()
    {
        if ($this->land == false) {
            $this->reset([
                'lands_shippers_box',
                'lands_pouches',
                'lands_boxes',
                'lands_ftl'
            ]);
            $this->land_packaging_type = 1;
            $this->addLandsShippersBox();
        } else {
            $this->reset([
                'lands_pouches',
                'lands_boxes',
                'lands_ftl'
            ]);
        }
    }

    protected function updatedLandPackagingType()
    {
        if ($this->land_packaging_type == 1) {
            $this->reset([
                'lands_pouches',
                'lands_boxes',
                'lands_ftl',
            ]);
            $this->addLandsShippersBox();
        } else if ($this->land_packaging_type == 2) {
            $this->addLandsPouches();

            $this->reset([
                'lands_shippers_box',
                'lands_boxes',
                'lands_ftl',
            ]);
            $this->addSeaCargosLcl();
        } else if ($this->land_packaging_type == 3) {
            $this->addLandsBoxes();

            $this->reset([
                'lands_shippers_box',
                'lands_pouches',
                'lands_ftl',
            ]);
            $this->addSeaCargosLcl();
        } else if ($this->land_packaging_type == 4) {
            $this->reset([
                'lands_shippers_box',
                'lands_pouches',
                'lands_boxes',
            ]);
            $this->addLandsFtl();
        }
    }

    public function getRequest()
    {
        return [
            'transport_mode' => $this->transport_mode,
            'origin' => $this->origin,
            'destination' => $this->destination,
            'declared_value' => $this->declared_value,
            'commodity_type' => $this->commodity_type,
            'commodity_applicable_rate' => $this->commodity_applicable_rate,
            'paymode' => $this->paymode,
            'service_mode' => $this->service_mode,
            'shipment_type' => $this->movement_sf_type,
            'evat' => $this->evat,

            //Transport mode = 1
            'air_cargo' => $this->air_cargo,
            'air_cargos' => $this->air_cargos,

            'air_pouch' => $this->air_pouch,
            'air_pouches' => $this->air_pouches,

            'air_box' => $this->air_box,
            'air_boxes' => $this->air_boxes,


            //Transport mode = 2
            'sea_cargo' => $this->sea_cargo,
            'sea_cargos_is_lcl' => $this->sea_cargos_is_lcl,
            'sea_cargos_is_fcl' => $this->sea_cargos_is_fcl,
            'sea_cargos_is_rcl' => $this->sea_cargos_is_rcl,

            'sea_box' => $this->sea_box,
            'sea_boxes' => $this->sea_boxes,

            //Transport mode = 3
            'land' => $this->land,
            'lands_shippers_box' => $this->lands_shippers_box,
            'lands_pouches' => $this->lands_pouches,
            'lands_boxes' => $this->lands_boxes,
            'lands_ftl' => $this->lands_ftl,
        ];
    }

    public function osGetRequest()
    {
        return [
            'os_transport_mode' => $this->os_transport_mode,
            'os_origin' => $this->os_origin,
            'os_destination' => $this->os_destination,
            'os_declared_value' => $this->os_declared_value,
            'os_commodity_type' => $this->os_commodity_type,
            'os_commodity_applicable_rate' => $this->os_commodity_applicable_rate,
            'os_paymode' => $this->os_paymode,
            'os_service_mode' => $this->os_service_mode,
            // 'os_shipment_type' => $this->os_shipment_type,
            'os_evat' => $this->os_evat,

            //Transport mode = 1
            'other_services_measurements' => $this->other_services_measurements,


            // //Transport mode = 2
            // 'sea_cargo' => $this->sea_cargo,
            // 'sea_cargos_is_lcl' => $this->sea_cargos_is_lcl,
            // 'sea_cargos_is_fcl' => $this->sea_cargos_is_fcl,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'origin',
            'destination',
            'declared_value',
            'commodity_type',
            'commodity_applicable_rate',
            'paymode',
            'transhipment',
            'service_mode',
            'air_cargo',
            'air_cargos',
            'air_pouch',
            'air_pouches',
            'air_boxes',
        ]);
    }

    public function resetCharges()
    {
        $this->reset([
            // breakdown fields
            'weight_charge',
            'awb_fee',
            'cod_charge',
            'valuation',
            'insurance',
            'handling_fee',
            'subtotal',
            'grand_total',
        ]);
    }

    // OTHER SERVICES    
    public function addOtherServicesMeasurement()
    {
        $this->other_services_measurements[] = [
            'id' => null,
            'qty' => null,
            'wt' => null,
            'dimension_l' => null,
            'dimension_w' => null,
            'dimension_h' => null,
            'unit_of_measurement' => null,
            'measurement_type' => null,
            'type_of_packaging' => null,
            'for_crating' => null,
            'crating_type' => null,
            'cbm' => null,
            'is_deleted' => false,
        ];
    }

    public function removeOtherServicesMeasurement($i)
    {
        if (count(collect($this->other_services_measurements)->where('is_deleted', false)) > 1) {
            if ($this->other_services_measurements[$i]['id']) {
                $this->other_services_measurements[$i]['is_deleted'] = true;
            } else {
                unset($this->other_services_measurements[$i]);
            }

            array_values($this->other_services_measurements);
        }
    }

    public function osIsForCreating($id, $trans_mode, $is_true)
    {
        if ($trans_mode == 1) {
            if ($is_true == true) {
                foreach ($this->other_services_measurements as $i => $osm) {
                    if ($i == $id) {
                        $this->other_services_measurements[$i]['for_crating'] = true;
                    }
                }
            } else {
                $this->other_services_measurements[$id]['for_crating'] = false;
            }
        }


        $this->os_crating_type_references = CrmCrateType::get();
    }

    protected function updatedOsTransportMode()
    {
        $this->os_origin_references = BranchReference::get();
        $this->os_destination_references = BranchReference::get();
        $this->os_transhipment_references = CrmShipmentType::get();
        $this->os_commodity_type_references = CrmCommodityType::get();
        $this->os_paymode_references = CrmModeOfPaymentReference::get();

        if ($this->os_transport_mode == 1) {
            $this->os_commodity_applicable_rate_references = CrmRateAirFreight::where('transport_mode_id', $this->os_transport_mode)->get();
            $this->os_service_mode_references = CrmServiceMode::whereIn('id', [1, 2, 3, 4])->get();
        } elseif ($this->os_transport_mode == 2) {
            $this->updatedTranshipment();
            $this->os_service_mode_references = CrmServiceMode::whereIn('id', [5, 6, 7, 8])->get();
        } elseif ($this->os_transport_mode == 3) {
            $this->os_commodity_applicable_rate_references = [];
            $this->os_service_mode_references = CrmServiceMode::get();
        } else {
            $this->os_commodity_applicable_rate_references = [];
        }
    }

    protected function updatedOsTranshipment()
    {
        $this->os_commodity_applicable_rate_references = CrmRateSeaFreight::where('transport_mode_id', $this->os_transport_mode)->where('shipment_type_id', $this->os_transhipment)->get();
    }
}
