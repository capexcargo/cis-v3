<?php

namespace App\Traits\Crm\Sales\SalesCampaign;

use App\Models\ChannelSrSource;
use App\Models\Crm\CityReference;
use App\Models\Crm\CrmAccountTypeReference;
use App\Models\Crm\CrmAudienceSegmentManagementCriteria;
use App\Models\Crm\CrmCustomerStatusReferences;
use App\Models\Crm\CrmLifeStage;
use App\Models\CrmAccountType2Reference;
use App\Models\MarketingChannel;

trait AudienceSegmentationTrait
{

    public $create_modal = false;
    public $edit_modal = false;
    // public $delete_modal = false;
    public $reactivate_modal = false;
    public $deactivate_modal = false;
    public $confirmation_modal = false;
    public $action_type;
    public $paginate = 10;
    public $name;
    public $filter_id;
    public $criteria_id;
    public $positions  = [];
    public $criteria_name;
    public $onboarded_date_from;
    public $onboarded_date_to;
    public $audience_segmentation_id;
    public $countarr;
    public $nearest;
    public $min;
    public $criteria;

    public $lifeStage_references = [];
    public $customerStatus_references = [];
    public $accountType_references = [];
    public $channelSr_references = [];
    public $marketingChannel_references = [];
    public $city_references = [];

    public function load(){
        $this->lifeStageReference();
        $this->customerStatusReference();
        $this->accountTypeReference();
        $this->channelSrReference();
        $this->marketingChannelReference();
        $this->cityReference();
    }

    public function lifeStageReference()
    {
        $this->lifeStage_references = CrmLifeStage::get();
    }

    public function customerStatusReference()
    {
        $this->customerStatus_references = CrmCustomerStatusReferences::get();
    }

    public function accountTypeReference()
    {
        $this->accountType_references = CrmAccountType2Reference::get();
    }

    public function channelSrReference()
    {
        $this->channelSr_references = ChannelSrSource::get();
    }

    public function marketingChannelReference()
    {
        $this->marketingChannel_references = MarketingChannel::get();
    }

    public function cityReference()
    {
        $this->city_references = CityReference::get();
    }

    public function getRequest()
    {
        return [
            'name' => $this->name,
            'filter_id' => $this->filter_id,
            'onboarded_date_from' => $this->onboarded_date_from,
            'onboarded_date_to' => $this->onboarded_date_to,
            'positions' => $this->positions,
        ];
    }
    public function resetForm()
    {
        $this->reset([
            'name',
            'filter_id',
            'onboarded_date_from',
            'onboarded_date_to',
            'positions',
        ]);

        $this->confirmation_modal = false;
    }
    public function addPosition()
    {
        $this->positions[] = [
            'id' => null,
            'criteria_id' => null,
            'is_deleted' => false,
        ];

    }


    public function removePosition(array $data)
    {
        if (count(collect($this->positions)->where('is_deleted', false)) > 1) {
            if ($this->positions[$data["a"]]['id']) {
                $this->positions[$data["a"]]['is_deleted'] = true;
                // unset($this->positions[$data["a"]]);
            } else {
                unset($this->positions[$data["a"]]);
            }

            array_values($this->positions);
        }
    }

   
    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

         if ($action_type == 'redirectToHome') {
            return redirect()->to(route('crm.sales.sales-campaign.index'));
        }
        // else if($action_type == 'redirectToAudienceSegmentation'){
        //     return redirect()->to(route('crm.sales.audience-segmentation.index'));
        // }
    }
}