<?php

namespace App\Traits\Crm\Sales\SalesCampaign;


trait SummaryTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $confirmation_modal = false;
    public $summary_id;
    public $paginate = 10;
    public $name;
    public $voucher_code;
    public $voucher_reference;
    public $discount_type;
    public $discount_amount;
    public $discount_percentage;
    public $start_datetime;
    public $end_datetime;
    public $maximum_usage;
    public $action_type;
    public $terms_conditon;
    public $status;
    public $summary_attachment;
    public $terms_modal = false;

    public $reactivate_modal = false;
    public $deactivate_modal = false;

    public $attachments = [];


    // public $confirmation_message;

    public function getRequest()
    {
        return [
            'name' => $this->name,
            'voucher_code' => $this->voucher_code,
            'discount_type' => $this->discount_type,
            'discount_amount' => $this->discount_amount,
            'discount_percentage' => $this->discount_percentage,
            'start_datetime' => $this->start_datetime,
            'end_datetime' => $this->end_datetime,
            'maximum_usage' => $this->maximum_usage,
            'terms_conditon' => $this->terms_conditon,
            'status' => $this->status,
            'attachments' => $this->attachments,
        ];
    }

    public function addAttachments()
    {
        $this->attachments[] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }


    public function removeAttachments($i)
    {
        if (count(collect($this->attachments)->where('is_deleted', false)) > 1) {
            if ($this->attachments[$i]['id']) {
                $this->attachments[$i]['is_deleted'] = true;
            } else {
                unset($this->attachments[$i]);
            }

            array_values($this->attachments);
        }
    }

    public function resetForm()
    {
        $this->reset([
            'name',
            'voucher_code',
            'discount_type',
            'discount_amount',
            'discount_percentage',
            'start_datetime',
            'end_datetime',
            'maximum_usage',
            'terms_conditon',
            'status',
            'attachments',
        ]);
        $this->confirmation_modal = false;
    }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToSummary') {
            return redirect()->to(route('crm.sales.sales-campaign.summary.index'));
        } else if ($action_type == 'redirectToHome') {
            return redirect()->to(route('crm.sales.sales-campaign.index'));
        }
        // else if($action_type == 'redirectToAudienceSegmentation'){
        //     return redirect()->to(route('crm.sales.audience-segmentation.index'));
        // }
    }
}
