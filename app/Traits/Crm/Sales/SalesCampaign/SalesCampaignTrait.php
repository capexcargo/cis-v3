<?php

namespace App\Traits\Crm\Sales\SalesCampaign;


trait SalesCampaignTrait{
    // public $create_modal = false;
    // public $edit_modal = false;
    // public $delete_modal = false;
    // public $type_id;
    public $paginate = 10;
    public $action_type;
    // public $confirmation_message;
    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;
        
        if ($action_type == 'redirectToHome') {
            return redirect()->to(route('crm.sales.sales-campaign.index'));
        } else if ($action_type == 'redirectToSummary') {
            return redirect()->to(route('crm.sales.sales-campaign.summary.index'));
        }
        
    }
}