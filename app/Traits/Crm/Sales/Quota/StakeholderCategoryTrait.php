<?php

namespace App\Traits\Crm\Sales\Quota;

use App\Models\BranchReference;
use App\Models\ChannelSrSource;
use App\Models\Crm\BranchReferencesQuadrant;
use App\Models\Crm\BranchReferencesWalkIn;
use App\Models\Crm\CrmQuotaAccountType;
use App\Models\Crm\StakeholderCategory;
use App\Models\MonthReference;
use App\Models\User;

trait StakeholderCategoryTrait
{
    public $confirmation_modal = false;

    public $stakeholder_category_name;
    public $search_management_data;

    public $stakeholder_mgmt_data_refs = [];
    public $selected_subs = [];
    public $selected = [];

    public $stakeholder;
    public $stakeholders = [];

    public $months_references = [];
    public $account_type_refs = [];
    public $branch_refs_nl_sl = [];
    public $branch_refs_quadrant = [];
    public $branch_refs_walk_in = [];
    public $branch_refs_provincial = [];
    public $online_channels_refs = [];

    public $account_type = [];
    public $nl_sl = [];
    public $quadrants = [];
    public $walk_ins = [];
    public $provincial_branches = [];
    public $online_channels = [];

    public $stakeholder_category_id;

    public function load()
    {
        $this->stakeholderMgmtDataRefs();
    }

    public function stakeholderMgmtDataRefs()
    {
        $this->months_references = MonthReference::get();

        $this->account_type_refs = CrmQuotaAccountType::
            // where('id', '<>', 4)->
            get();
        $this->branch_refs_nl_sl = BranchReference::where('area_id', 3)->get();
        $this->branch_refs_quadrant = BranchReferencesQuadrant::get();
        $this->branch_refs_walk_in = BranchReferencesWalkIn::get();
        $this->branch_refs_provincial = BranchReference::where('area_id', 2)->get();
        $this->online_channels_refs = ChannelSrSource::get();

        foreach ($this->account_type_refs as $account_type_ref) {
            $this->account_type[] =
                [
                    'id' => $account_type_ref->id,
                    'reference_id' => 1,
                    // 'reference_id' => $this->search_management_data ?? 1,
                    'name' => $account_type_ref->name,
                ];
        }

        foreach ($this->branch_refs_nl_sl as $refs_nl_sl) {
            $this->nl_sl[] =
                [
                    'id' => $refs_nl_sl->id,
                    'reference_id' => 2,
                    // 'reference_id' => $this->search_management_data ?? 2,
                    'name' => $refs_nl_sl->display,
                ];
        }

        foreach ($this->branch_refs_quadrant as $ref_quadrant) {
            $this->quadrants[] =
                [
                    'id' => $ref_quadrant->id,
                    'reference_id' => 3,
                    // 'reference_id' => $this->search_management_data ?? 3,
                    'name' => $ref_quadrant->name,
                ];
        }

        foreach ($this->branch_refs_walk_in as $ref_walk_in) {
            $this->walk_ins[] =
                [
                    'id' => $ref_walk_in->id,
                    'reference_id' => 4,
                    // 'reference_id' => $this->search_management_data ?? 4,
                    'name' => $ref_walk_in->name,
                ];
        }

        foreach ($this->branch_refs_provincial as $ref_provincial) {
            $this->provincial_branches[] =
                [
                    'id' => $ref_provincial->id,
                    'reference_id' => 5,
                    // 'reference_id' => $this->search_management_data ?? 5,
                    'name' => $ref_provincial->display,
                ];
        }

        foreach ($this->online_channels_refs as $oc) {
            $this->online_channels[] =
                [
                    'id' => $oc->id,
                    'reference_id' => 6,
                    // 'reference_id' => $this->search_management_data ?? 6,
                    'name' => $oc->name,
                ];
        }

        $this->stakeholder_mgmt_data_refs = array_merge($this->account_type, $this->nl_sl, $this->quadrants, $this->walk_ins, $this->provincial_branches, $this->online_channels);

        // dd($this->stakeholder_mgmt_data_refs);
    }

    protected function updatedStakeholder()
    {
        $this->stakeholders = StakeholderCategory::when($this->stakeholder == 1, function ($query) {
            $query->where('management_id', 1)
                ->whereHas('accountType', function ($query) {
                    $query->where('id', '<>', 4);
                })
                ->orderBy('id', 'desc');
        })
            ->when($this->stakeholder == 3, function ($query) {
                $query->where('management_id', 3)
                    ->orderBy('id', 'desc');
            })
            ->when($this->stakeholder == 5, function ($query) {
                $query->where('management_id', 5)
                    ->orderBy('id', 'desc');
            })
            ->get();


        foreach ($this->stakeholders as $i => $stakeholder) {
            $this->subcategories[$i] = [
                'stakeholder_category_name' => $stakeholder->stakeholder_category_name,
                'sh_subcategory' => $stakeholder->subcategory,
                'account_type' => $stakeholder->account_type,
                'distribution_pct' => null,
                'amount' => null,
            ];
        }
    }

    protected function updatedSearchManagementData()
    {
        if ($this->search_management_data == 1) {
            $filtered_search = array_merge($this->account_type);
        } elseif ($this->search_management_data == 2) {
            $filtered_search = array_merge($this->nl_sl);
        } elseif ($this->search_management_data == 3) {
            $filtered_search = array_merge($this->quadrants);
        } elseif ($this->search_management_data == 4) {
            $filtered_search = array_merge($this->walk_ins);
        } elseif ($this->search_management_data == 5) {
            $filtered_search = array_merge($this->provincial_branches);
        } elseif ($this->search_management_data == 6) {
            $filtered_search = array_merge($this->online_channels);
        } else {
            $filtered_search = array_merge($this->account_type, $this->nl_sl, $this->quadrants, $this->walk_ins, $this->provincial_branches, $this->online_channels);
        }
        $this->stakeholder_mgmt_data_refs = $filtered_search;

        $this->selected_subs = [];
        $this->selected = [];
    }

    public function getRequest()
    {
        return [
            'stakeholder_category_name' => $this->stakeholder_category_name,
            'search_management_data' => $this->search_management_data,
            'selected_subs' => $this->selected,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'stakeholder_category_name',
            'search_management_data',
            'selected_subs',
            'selected',
        ]);
        $this->confirmation_modal = false;
    }
}
