<?php

namespace App\Traits\Crm\Sales\Quota;

use App\Models\Crm\StakeholderCategory;
use App\Models\MonthReference;

trait TargetTrait
{
    public $months_references = [];
    public $month;
    public $year;
    public $stakeholder_category;
    public $subcategories = [];

    public $stakeholders = [];
    public $total_days_count;
    public $monthly_total_target_amount;

    public function loadReferences()
    {
        $this->monthReferences();
        $this->stakeholdersCorporateTargetReference();
    }

    public function monthReferences()
    {
        $this->months_references = MonthReference::get();
    }

    public function stakeholdersCorporateTargetReference()
    {
        $subcategories = [];

        $stakeholder_categories = StakeholderCategory::groupBy('stakeholder_category_name', 'management_id')
            ->get();

        $stakeholder_subcategories = StakeholderCategory::with('accountType')
            ->where('management_id', 1)
            ->whereHas('accountType', function ($query) {
                $query->where('id', '<>', 4);
            })
            ->orderBy('id', 'desc')
            ->get();

        foreach ($stakeholder_subcategories as $i => $sub_cat) {
            $subcategories[] = [
                'id' => $sub_cat->id,
                'subcategory_name' => $sub_cat->accountTypeBT->name,
                'monthly_tonnage' => null,
                'daily_tonnage' => null,
                'distribution_pct' => null,
                'monthly_cbm_sea' => null,
                'daily_cbm_sea' => null,
                'monthly_kg_air' => null,
                'daily_kg_air' => null,
                'monthly_cbm_land' => null,
                'daily_cbm_land' => null,
                'monthly_kg_int' => null,
                'daily_kg_int' => null,
            ];
        }

        foreach ($stakeholder_categories as $i => $stakeholder_cat) {
            if ($stakeholder_cat->management_id == 1) {
                $this->stakeholders[] = [
                    'no' => $i += 1,
                    'stakeholder_category_name' => $stakeholder_cat->stakeholder_category_name,
                    'sh_subcategory' => $stakeholder_cat->management_id == 1 ? $subcategories : [],
                    'account_type_id' => $stakeholder_cat->account_type,
                    'account_type' => $stakeholder_cat->accountTypeReference->name,
                ];
            } else {
                $this->stakeholders[] = [
                    'no' => $i += 1,
                    'stakeholder_category_name' => $stakeholder_cat->stakeholder_category_name,
                    'sh_subcategory' => [],
                    'account_type_id' => $stakeholder_cat->account_type,
                    'account_type' => $stakeholder_cat->accountTypeReference->name,
                    'monthly_tonnage' => null,
                    'daily_tonnage' => null,
                    'distribution_pct' => null,
                    'monthly_cbm_sea' => null,
                    'daily_cbm_sea' => null,
                    'monthly_kg_air' => null,
                    'daily_kg_air' => null,
                    'monthly_cbm_land' => null,
                    'daily_cbm_land' => null,
                    'monthly_kg_int' => null,
                    'daily_kg_int' => null,
                ];
            }
        }
    }

    public function computeDaily()
    {
        foreach ($this->stakeholders as $i => $subcategory) {
            if (count($subcategory['sh_subcategory']) > 0) {
                foreach ($subcategory['sh_subcategory'] as $x => $sh_subcat) {
                    $this->stakeholders[$i]['sh_subcategory'][$x]['daily_tonnage'] = $sh_subcat['monthly_tonnage'] > 0 ? number_format($sh_subcat['monthly_tonnage'] / 26, 2) : '';

                    $this->stakeholders[$i]['sh_subcategory'][$x]['daily_cbm_sea'] = $sh_subcat['monthly_cbm_sea'] > 0 ? number_format($sh_subcat['monthly_cbm_sea'] / 26, 2) : '';
                    $this->stakeholders[$i]['sh_subcategory'][$x]['daily_kg_air'] = $sh_subcat['monthly_kg_air'] > 0 ? number_format($sh_subcat['monthly_kg_air'] / 26, 2) : '';
                    $this->stakeholders[$i]['sh_subcategory'][$x]['daily_cbm_land'] = $sh_subcat['monthly_cbm_land'] > 0 ? number_format($sh_subcat['monthly_cbm_land'] / 26, 2) : '';
                    $this->stakeholders[$i]['sh_subcategory'][$x]['daily_kg_int'] = $sh_subcat['monthly_kg_int'] > 0 ? number_format($sh_subcat['monthly_kg_int'] / 26, 2) : '';
                }
            } else {
                $this->stakeholders[$i]['daily_tonnage'] = $subcategory['monthly_tonnage'] > 0 ? number_format($subcategory['monthly_tonnage'] / 26, 2) : '';

                $this->stakeholders[$i]['daily_cbm_sea'] = $subcategory['monthly_cbm_sea'] > 0 ? number_format($subcategory['monthly_cbm_sea'] / 26, 2) : '';
                $this->stakeholders[$i]['daily_kg_air'] = $subcategory['monthly_kg_air'] > 0 ? number_format($subcategory['monthly_kg_air'] / 26, 2) : '';
                $this->stakeholders[$i]['daily_cbm_land'] = $subcategory['monthly_cbm_land'] > 0 ? number_format($subcategory['monthly_cbm_land'] / 26, 2) : '';
                $this->stakeholders[$i]['daily_kg_int'] = $subcategory['monthly_kg_int'] > 0 ? number_format($subcategory['monthly_kg_int'] / 26, 2) : '';
            }
        }
    }

    public function getRequestStakeholder()
    {
        return [
            'month' => $this->month,
            'year' => $this->year,
            'stakeholder_category' => $this->stakeholder_category,
            'subcategories' => $this->subcategories,
        ];
    }

    public function getRequestCorporate()
    {
        return [
            'month' => $this->month,
            'year' => $this->year,
            'stakeholders' => $this->stakeholders,
            'total_days_count' => $this->total_days_count,
            'monthly_total_target_amount' => $this->monthly_total_target_amount,
        ];
    }
}
