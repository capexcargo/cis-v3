<?php

namespace App\Traits\Crm\Sales\Leads;

use App\Models\ChannelSrSource;
use App\Models\Crm\CrmLeadClassification;
use App\Models\Crm\CrmMarketingChannel;
use App\Models\Crm\CrmMovementSfTypeReferences;
use App\Models\Crm\CrmQualificationQuestionnaire;
use App\Models\ServiceRequirements;

trait LeadsTrait
{
    public $shipment_type_references = [];
    public $service_requirement_references = [];
    public $lead_classification_references = [];
    public $channel_source_references = [];
    public $currency_references = [];

    public $qualifications_questionnaires = [];

    public $lead;
    public $lead_id;
    public $lead_name;
    public $sr_number;
    public $account_id;
    public $industry_id;
    public $shipment_type;
    public $service_requirement;
    public $lead_status;
    public $qualification_score;
    public $lead_classification;
    public $deal_size;
    public $actual_amount_converted;

    public $contact_person;
    public $contact_mobile_no;
    public $contact_email_add;
    public $description;
    public $contact_owner;
    public $customer_type;
    public $channel_source;
    public $currency;
    public $attachments;

    public $address_type;
    public $address_line1;
    public $address_line2;
    public $state_province;
    public $city_municipality;
    public $barangay;
    public $postal_code;

    public function load()
    {
        $this->shipmentTypeReferences();
        $this->serviceRequirementReferences();
        $this->leadClassificationReferences();
        $this->channelSourceReferences();
        // $this->currencyReferences();
    }

    public function shipmentTypeReferences()
    {
        $this->shipment_type_references = CrmMovementSfTypeReferences::get();
    }

    public function serviceRequirementReferences()
    {
        $this->service_requirement_references = ServiceRequirements::get();
    }

    public function leadClassificationReferences()
    {
        $this->lead_classification_references = CrmLeadClassification::get();
    }

    public function channelSourceReferences()
    {
        $this->channel_source_references = ChannelSrSource::get();
    }

    // public function currencyReferences()
    // {
    //     $this->currency_references = CrmLeadClassification::get();
    // }

    public function qualificationsGetRequest()
    {
        return [
            'qualification_questionnaires' => $this->qualifications_questionnaires,
            'qualification_score' => $this->qualification_score,
        ];
    }

    public function convertGetRequest()
    {
        return [
            'sr_no' => $this->sr_number,
            'account_id' => $this->account_id,
            'opportunity_name' => $this->lead_name,
            'industry_id' => $this->industry_id,
            'deal_size' => $this->deal_size,
            'actual_amount' => $this->actual_amount_converted,
        ];
    }

    public function retirementGetRequest()
    {
        return [
            'retirement_reason' => $this->retirement_reason,
            'remarks' => $this->remarks,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'retirement_reason',
            'remarks',
        ]);
        $this->confirmation_modal = false;
    }
}
