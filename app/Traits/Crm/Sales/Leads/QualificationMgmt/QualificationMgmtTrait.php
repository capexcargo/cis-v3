<?php

namespace App\Traits\Crm\Sales\Leads\QualificationMgmt;

trait QualificationMgmtTrait
{

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $action_type;

    public $question;
    public $qualification_type;

    public function getRequest()
    {
        return [
            'question' =>$this->question,
            'qualification_type' =>$this->qualification_type,
        ];
    }
    
    public function resetForm()
    {
        $this->reset([
            'question',
            'qualification_type',
        ]);        
        $this->confirmation_modal = false;

    }
    
}