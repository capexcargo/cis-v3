<?php

namespace App\Traits\Crm\Sales\BookingDropdownList\ActivityType;

use App\Models\CrmActivityTypeDeliveryReference;
use App\Models\CrmActivityTypePickupReference;
use App\Models\User;

trait ActivityTypeTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $action_type;


    public $act_id;
    public $activity_type;
    public $pickup_execution_time_id;
    public $delivery_execution_time_id;


    public $pickup_references = [];
    public $delivery_references = [];


    public function load()
    {
        $this->PickupReference();
        $this->DeliveryReference();
    }

    public function PickupReference()

    {
        $this->pickup_references = CrmActivityTypePickupReference::get();
        // dd($this->pickup_references);
    }

    public function DeliveryReference()

    {
        $this->delivery_references = CrmActivityTypeDeliveryReference::get();
    }

    // public function loadFirstApproverReference()
    // {
    //     $this->first_approver_references = User::whereHas('userDetails', function ($query) {
    //         $query->where('branch_id', $this->branch)
    //             ->Where('department_id', $this->department_id)
    //             ->orWhere('level_id', 4);
    //     })
    //         ->where([
    //             ['division_id', $this->division],
    //             ['level_id', '>=', 1]
    //         ])
    //         ->get();
    //         // dd($this->first_approver_references);
    // }

    // public function loadSecodeApproverReference()
    // {
    //     $this->second_approver_references = User::whereHas('userDetails', function ($query) {
    //         $query->where('branch_id', $this->branch)
    //             ->where('department_id', $this->department_id)
    //             ->orWhere('level_id', 4);
    //     })
    //         ->where([
    //             ['division_id', $this->division],
    //             ['level_id', '>=', 3]
    //         ])
    //         ->get();
    // }

    public function getRequest()
    {
        return [
            'activity_type' => $this->activity_type,
            'pickup_execution_time_id' => $this->pickup_execution_time_id,
            'delivery_execution_time_id' => $this->delivery_execution_time_id,
        ];
    }


    public function resetForm()
    {
        $this->reset([
            'activity_type',
            'pickup_execution_time_id',
            'delivery_execution_time_id',
        ]);
        $this->confirmation_modal = false;
    }



    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToActType') {
            return redirect()->to(route('crm.sales.booking-dropdown-list.index'));
        } else if ($action_type == 'redirectToTimeslot') {
            return redirect()->to(route('crm.sales.booking-dropdown-list.timeslot.index'));
        }
    }
}
