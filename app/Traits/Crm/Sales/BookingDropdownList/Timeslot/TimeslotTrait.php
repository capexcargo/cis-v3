<?php

namespace App\Traits\Crm\Sales\BookingDropdownList\Timeslot;

use App\Models\User;

trait TimeslotTrait
{

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $action_type;

    public $name;

    public function getRequest()
    {
        return [
            'name' => $this->name,
            
        ];
    }


    public function resetForm()
    {
        $this->reset([
            'name',
          
        ]);
        $this->confirmation_modal = false;
    }



    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectToActType') {
            return redirect()->to(route('crm.sales.booking-dropdown-list.index'));
        } else if ($action_type == 'redirectToTimeslot') {
            return redirect()->to(route('crm.sales.booking-dropdown-list.timeslot.index'));
        }
    }
}
