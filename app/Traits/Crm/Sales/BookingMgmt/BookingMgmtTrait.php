<?php

namespace App\Traits\Crm\Sales\BookingMgmt;

use App\Models\BranchReference;
use App\Models\Crm\BarangayReference;
use App\Models\Crm\CityReference;
use App\Models\Crm\CrmCustomerInformation;
use App\Models\Crm\CrmMarketingChannel;
use App\Models\Crm\StateReference;
use App\Models\CrmActivityType;
use App\Models\CrmBookingFailedReasonReference;
use App\Models\CrmBookingStatusReference;
use App\Models\CrmBookingType;
use App\Models\CrmModeOfPaymentReference;
use App\Models\CrmRateLoa;
use App\Models\CrmServiceMode;
use App\Models\CrmTimeslot;
use App\Models\CrmTransportMode;
use App\Models\CrmVehicleTypeReference;
use App\Models\User;
use App\View\Components\Crm\Select;

trait BookingMgmtTrait
{
    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $create_modal = false;
    public $edit_modal = false;
    public $cargo_details_modal = false;
    public $change_address_modal = false;
    public $change_address_cons_modal = false;
    public $create_addrs_modal = false;
    public $create_addrs_cons_modal = false;

    public $confirmsub_modal = false;
    public $book_again_modal = false;

    public $confirmshipper_modal = false;
    public $viewExisting_modal = false;
    public $consigneedetails_modal = false;
    public $bookstatlog_modal = false;
    public $consigneedet_modal = false;
    public $bsl_modal = false;
    public $no_workinst_modal = false;
    public $work_ins_id;
    public $workinst_modal = false;
    public $vimg_modal = false;
    public $vpdf_modal = false;
    public $internalrem_modal = false;

    public $bookref_modal = false;
    public $cancel_modal = false;
    public $cancelconfirm_modal = false;
    public $reschedule_modal = false;
    public $rescheduleconfirm_modal = false;

    public $confirmation_modal  = false;
    public $confirmation_message;
    public $action_type;

    public $approve_cancel_modal = false;
    public $decline_cancel_modal = false;
    public $approve_resched_modal = false;
    public $decline_resched_modal = false;

    public $current_tab = 1;

    public $booking_type_1;
    public $vehicle_type_1;
    public $pickup_date_1;
    public $time_slot_1;
    public $consignee_count_1;
    public $walk_in_branch_1;
    public $activity_type_1;
    public $consignee_category_1;
    public $booking_category_1;
    // public $channel_1;
    public $time_1;
    public $time_from_1;
    public $time_to_1;

    public $account_type_2;
    public $customer_no_2;
    public $booking_reference_no_2;
    public $company_name_2;
    public $first_name_2;
    public $middle_name_2;
    public $last_name_2;
    public $mobile_number_2;
    public $email_address_2;
    public $address_2;

    public $account_type_3;
    public $customer_no_3;
    public $company_name_3;
    public $first_name_3;
    public $middle_name_3;
    public $last_name_3;
    public $mobile_number_3;
    public $email_address_3;
    public $address_3;
    public $quantity_3;
    public $weight_3;
    public $length_3;
    public $width_3;
    public $height_3;
    public $size_3;
    public $work_instruction_3;
    public $remarks_3;
    public $stype_3;

    public $cons = [];
    public $cardets = [];
    public $attachments = [];
    public $counter;

    public $booking_type_referencesbk = [];
    public $vehicle_type_referencesbk = [];
    public $timeslot_referencesbk = [];
    public $walk_referencesbk = [];
    public $activity_referencesbk = [];
    public $shipper_referencesbk = [];
    public $final_referencesbk = [];
    public $bookingbranch_referencesbk = [];
    public $marketingchannel_referencesbk = [];
    public $created_referencesbk = [];

    public $transport_referencescd = [];
    public $service_referencescd = [];
    public $modepayment_referencescd = [];
    public $modepayment_referencescdcorps = [];
    public $chargedto_references = [];

    public $customer_nos = [];
    public $company_names  = [];
    public $customer_noc = [];
    public $company_namescons = [];

    public $customer_add = [];

    public $detailscons;
    public $cardetsss = [];
    public $asd = [];

    public $shipperaddresses = [];
    public $shipperAddressList;
    public $consigneeaddresses = [];
    public $consigneeAddressList;
    public $idcons;

    public $customerinfos = [];
    public $numbers;
    public $custno;
    public $custshpr;
    public $custcpny;
    public $mobile_nos;
    public $custnocons;
    public $custshprcons;
    public $custcpnycons;
    public $mobile_nos_cons;
    public $add1;
    public $add2;
    public $addresslabel;

    public $statenames = [];
    public $state_name;
    public $citynames = [];
    public $city_name;
    public $barangaynames = [];
    public $barangay_name;

    public $booking_ref_no;

    public $decval_summary;
    public $transposrt_mode_3_summary;
    public $service_mode_3_summary;
    public $mode_of_payment_3_summary;
    public $description_goods_3_summary;

    public $pickupwalk;
    public $singlemulti;

    public $SDbookingrefno;
    public $SDbookingstat;
    public $SDfullname;
    public $SDcompany;

    public $SDmobile_nos;
    public $SDemail_address;
    public $SDcustomertype;
    public $SDacttype;
    public $SDfname;
    public $SDmname;
    public $SDlname;

    public $CDdecval;
    public $CDtransportmode;
    public $CDservmode;
    public $CDdescgoods;
    public $CDmodeofp;
    public $CDassignedt;
    public $CDwcount;
    public $CDfailedactreason;
    public $CDworkins;
    public $CDintrem;

    public $bstatrefno;
    public $bstatstat;
    public $bstatcreated;

    public $viewbookingdetails;
    public $viewbookingdetailsconsignee = [];

    public $viewbookingconsigneesdetails;
    public $viewbookingconsigneesdetail = [];
    public $viewbookingconsignees = [];

    public $consid = [];

    public $SDaddress;
    public $SDtimeslot;

    public $search_request;

    public $cancelreason_referenceLG = [];
    public $reschedreason_referenceLG = [];

    public $is_deleted;
    public $countarr;
    public $min;
    public $nearest;

    public $state;
    public $state_province_references = [];
    public $city;
    public $city_municipality_references = [];
    public $barangay;
    public $barangay_references = [];
    public $stateids = [];
    public $states;
    public $postal;

    public $state2;
    public $city2;
    public $barangay2;
    public $postal2;

    public $state3;
    public $city3;
    public $barangay3;
    public $postal3;

    public $state_3;
    public $city_3;
    public $barangay_3;
    public $postal_3;

    public $custid2;
    public $acctype2;





    // public $servmode_identify = [];


    public function load()
    {
        $this->current_tab = 1;
        $this->BookingTypeReferenceBK();
        $this->VehicleTypeReferenceBK();
        $this->TimeslotReferenceBK();
        $this->WalkinReferenceBK();
        $this->ActivityReferenceBK();
        $this->ShipperReferenceBK();
        $this->FinalStatusReferenceBK();
        $this->BookingBranchReferenceBK();
        $this->MarketingChannelReferenceBK();
        $this->CreatedByBK();

        $this->TransportReferenceCD();
        // $this->ServiceModeReferenceCD();
        $this->ModeOfPaymenReferenceCD();

        $this->CancelReasonReferenceLG();
        $this->ReschedReasonReferenceLG();

        $this->provinceState();
        $this->BookingChargeTo();
        // $this->updatedCity();
        // $this->loadShipperCityMunicipality();


        // $this->CityMunicipality();
    }

    public function BookingTypeReferenceBK()
    {
        $this->booking_type_referencesbk = CrmBookingType::get();
    }

    public function VehicleTypeReferenceBK()
    {
        $this->vehicle_type_referencesbk = CrmVehicleTypeReference::get();
    }

    public function TimeslotReferenceBK()
    {
        $this->timeslot_referencesbk = CrmTimeslot::orderBy('id', 'desc')->get();
    }

    public function WalkinReferenceBK()
    {
        $this->walk_referencesbk = BranchReference::get();
    }

    public function ActivityReferenceBK()
    {
        $this->activity_referencesbk = CrmActivityType::get();
        // dd($this->activity_referencesbk);
    }

    public function ShipperReferenceBK()
    {
        $this->shipper_referencesbk = CrmCustomerInformation::get();
    }

    public function FinalStatusReferenceBK()
    {
        $this->final_referencesbk = CrmBookingStatusReference::get();
    }

    public function BookingBranchReferenceBK()
    {
        $this->bookingbranch_referencesbk = BranchReference::get();
    }

    public function MarketingChannelReferenceBK()
    {
        $this->marketingchannel_referencesbk = CrmMarketingChannel::get();
    }

    public function CreatedByBK()
    {
        $this->created_referencesbk = User::get();
    }

    public function TransportReferenceCD()
    {
        $this->transport_referencescd = CrmTransportMode::get();
    }

    public function CancelReasonReferenceLG()
    {
        $this->cancelreason_referenceLG = CrmBookingFailedReasonReference::get();
    }

    public function ReschedReasonReferenceLG()
    {
        $this->reschedreason_referenceLG = CrmBookingFailedReasonReference::get();
    }

    public function listServiceMode($tm)
    {
        $this->cons[$tm][$tm]['servmode_identify'] = $this->cons[$tm][$tm]['transposrt_mode_3'];
    }

    public function ModeOfPaymenReferenceCD()
    {
        $this->modepayment_referencescd = CrmModeOfPaymentReference::WhereIn('id', [1, 2, 3])->get();
        $this->modepayment_referencescdcorps = CrmModeOfPaymentReference::WhereIn('id', [1, 2, 4, 5])->get();
    }

    public function BookingChargeTo()
    {
        $this->chargedto_references = CrmCustomerInformation::Where('account_type', 2)->get();
    }

    public function provinceState()
    {
        $this->state_province_references = StateReference::get();
    }

    protected function updatedState()
    {
        $this->loadShipperCityMunicipality();
        $this->loadShipperBarangay();

        if ($this->city != '') {
            $this->barangay_references = [];
            $this->postal = "";
        }
    }

    protected function updatedCity()
    {
        $this->loadShipperCityMunicipality();
        $this->loadShipperBarangay();
        $this->postals = CityReference::where('id', $this->city)->select('city_postal')->first();
        $this->postal = $this->postals->city_postal == "" ? 1 : $this->postals->city_postal;
    }

    public function loadShipperCityMunicipality()
    {
        if ($this->state != '') {
            $this->city_municipality_references = CityReference::where([
                ['state_id', $this->state]
            ])->orderBy('name', 'asc')->get();
        }
    }

    // protected function updatedBarangay()
    // {
    //     // $this->loadShipperBarangay();
    // }

    public function loadShipperBarangay()
    {

        $this->stateids = CityReference::where('id', $this->city)->get('state_id');

        foreach ($this->stateids as $a => $stateid) {
            $this->states = $this->stateids[$a]['state_id'];
        }
        // $this->states = $this->stateid[0]['state_id'];
        if ($this->state != '' && $this->city != '') {
            $this->barangay_references = BarangayReference::where([
                ['state_id', $this->states]
            ])->where([
                ['city_id', $this->city]
            ])->orderBy('name', 'asc')->get();
            // dd($this->city_municipality_references);
        }
    }

    // public function updatedDeclaredValue3($a){

    //     $this->counter = $a;
    //     $this->cons[$a][$a]['declared_value_3'] = number_format($this->cons[$a][$a]['declared_value_3'], 0, '', '');
    // }

    public function addConsignee($a)
    {
        $this->counter = $a;
        $this->cons[$a][] = [
            'id' => null,
            'account_type_3' => 1,
            'customer_no_3' => null,
            'company_name_3' => null,
            'first_name_3' => null,
            'middle_name_3' => null,
            'last_name_3' => null,
            'mobile_number_3' => null,
            'email_address_3' => null,
            'address_3' => null,
            'state_3' => null,
            'city_3' => null,
            'barangay_3' => null,
            'postal_3' => null,

            'stype_3' => null,
            'declared_value_3' => null,
            'description_goods_3' => null,
            'transposrt_mode_3' => null,
            'service_mode_3' => null,
            'mode_of_payment_3' => null,
            'servmode_identify' => false,
            'charge_to_3' => null,
            'charge_to_name_3' => null,
            'is_deleted' => false,
        ];

        // dd($this->cons);
        $this->cons[$a][$a]['account_type_3'] = 1;
        $this->cons[$a][$a]['customer_no_3'] = null;
        $this->cons[$a][$a]['company_name_3'] = null;
        $this->cons[$a][$a]['first_name_3'] = null;
        $this->cons[$a][$a]['middle_name_3'] = null;
        $this->cons[$a][$a]['last_name_3'] = null;
        $this->cons[$a][$a]['mobile_number_3'] = null;
        $this->cons[$a][$a]['email_address_3'] = null;
        $this->cons[$a][$a]['address_3'] = null;
        $this->cons[$a][$a]['state_3'] = null;
        $this->cons[$a][$a]['city_3'] = null;
        $this->cons[$a][$a]['barangay_3'] = null;
        $this->cons[$a][$a]['postal_3'] = null;

        $this->cons[$a][$a]['stype_3'] = null;
        $this->cons[$a][$a]['declared_value_3'] = null;
        $this->cons[$a][$a]['description_goods_3'] = null;
        $this->cons[$a][$a]['transposrt_mode_3'] = null;
        $this->cons[$a][$a]['service_mode_3'] = null;
        $this->cons[$a][$a]['mode_of_payment_3'] = null;
        $this->cons[$a][$a]['servmode_identify'] = null;
        $this->cons[$a][$a]['charge_to_3'] = null;
        $this->cons[$a][$a]['charge_to_name_3'] = null;


        $this->cardets[$a][] = [
            'id' => null,
            'quantity_3' => null,
            'weight_3' => null,
            'length_3' => null,
            'width_3' => null,
            'height_3' => null,
            'size_3' => null,
            'is_deleted' => false,
        ];
        // dd($this->cons);
        // $this->cardets[$a][$a]['quantity_3'] = null;
        // $this->cardets[$a][$a]['weight_3'] = null;
        // $this->cardets[$a][$a]['length_3'] = null;
        // $this->cardets[$a][$a]['width_3'] = null;
        // $this->cardets[$a][$a]['height_3'] = null;

    }

    public function addConsigneeEdit($a, $b)
    {
        $this->counter = $a;
        $this->cons[$a][$b] = [
            'id' => null,
            'account_type_3' => 1,
            'customer_no_3' => null,
            'company_name_3' => null,
            'first_name_3' => null,
            'middle_name_3' => null,
            'last_name_3' => null,
            'mobile_number_3' => null,
            'email_address_3' => null,
            'address_3' => null,
            'state_3' => null,
            'city_3' => null,
            'barangay_3' => null,
            'postal_3' => null,

            'stype_3' => null,
            'declared_value_3' => null,
            'description_goods_3' => null,
            'transposrt_mode_3' => null,
            'service_mode_3' => null,
            'mode_of_payment_3' => null,
            'servmode_identify' => false,
            'created_by' => null,
            'is_deleted' => false,
        ];

        $this->cons[$a][$a]['account_type_3'] = 1;
        $this->cons[$a][$a]['customer_no_3'] = null;
        $this->cons[$a][$a]['company_name_3'] = null;
        $this->cons[$a][$a]['first_name_3'] = null;
        $this->cons[$a][$a]['middle_name_3'] = null;
        $this->cons[$a][$a]['last_name_3'] = null;
        $this->cons[$a][$a]['mobile_number_3'] = null;
        $this->cons[$a][$a]['email_address_3'] = null;
        $this->cons[$a][$a]['address_3'] = null;

        $this->cons[$a][$a]['stype_3'] = null;
        $this->cons[$a][$a]['declared_value_3'] = null;
        $this->cons[$a][$a]['description_goods_3'] = null;
        $this->cons[$a][$a]['transposrt_mode_3'] = null;
        $this->cons[$a][$a]['service_mode_3'] = null;
        $this->cons[$a][$a]['mode_of_payment_3'] = null;
        $this->cons[$a][$a]['servmode_identify'] = null;

        $this->cardets[$a][] = [
            'id' => null,
            'quantity_3' => null,
            'weight_3' => null,
            'length_3' => null,
            'width_3' => null,
            'height_3' => null,
            'size_3' => null,
            'is_deleted' => false,
        ];

        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->cons as $a => $con) {
            if ($this->cons[$a][$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->cons[$a][$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->countarr = $countarray;
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);
    }

    public function removeConsigneeEdit(array $data)
    {
        if (count(collect($this->cons)->where('is_deleted', false)) > 1) {
            // dd($this->cons);
            if ($this->cons[$data['a']][$data['a']]['id']) {
                $this->cons[$data['a']][$data['a']]['is_deleted'] = true;
                $this->cons[$data['a']][$data['a']]['stype_3'] = '';

                $b = 0;
                if (isset($this->cardets[$data['a']])) {
                    foreach ($this->cardets[$data['a']] as $c => $process) {
                        $this->cardets[$data['a']][$c]['is_deleted'] = true;
                        $b++;
                    }
                }
            } else {
                unset($this->cons[$data['a']]);
                unset($this->cons[$data['a']][$data['a']]);
                unset($this->cardets[$data['a']]);

                // unset($this->pros[$data['a']]);
                // unset($this->subpros[$data['a']]);

            }
            array_values($this->cons);
        }


        $countarray = 0;
        $getkeyarray = [];
        foreach ($this->cons as $a => $con) {
            if ($this->cons[$a][$a]['is_deleted'] == false) {
                $getkeyarray[] = $a;
            }

            $countarray += ($this->cons[$a][$a]['is_deleted'] == false ? 1 : 0);
        }
        $this->countarr = $countarray;
        $this->min = min($getkeyarray);
        $this->nearest = max($getkeyarray);

        // dd($this->countarr);

    }

    public function removeConsigneeCreate(array $data)
    {
        if (count(collect($this->cons)->where('is_deleted', false)) > 1) {
            if ($this->cons[$data['a']][0]['id']) {
                $this->cons[$data['a']][0]['is_deleted'] = true;
                $this->cons[$data['a']][0]['stype_3'] = '';

                $b = 0;
                if (isset($this->cardets[$data['a']])) {
                    foreach ($this->cardets[$data['a']] as $c => $customer_no_3) {
                        $this->cardets[$data['a']][$c]['is_deleted'] = true;
                        $b++;
                    }
                }
            } else {
                unset($this->cons[$data['a']]);
                unset($this->cons[$data['a']][$data['a']]);
                unset($this->cardets[$data['a']]);
                // unset($this->cardets[$data['a']][$data['a']]);
            }
            array_values($this->cons);
        }
    }

    public function addConsigneeDetails($a)
    {
        $this->cardets[$a][] = [
            'id' => null,
            'quantity_3' => null,
            'weight_3' => null,
            'length_3' => null,
            'width_3' => null,
            'height_3' => null,
            'size_3' => null,
            'is_deleted' => false,
        ];
    }

    public function removeConsigneeDetails(array $data)
    {
        if ($this->cardets[$data['a']][$data['b']]['id']) {

            $this->cardets[$data['a']][$data['b']]['is_deleted'] = true;
        } else {
            unset($this->cardets[$data['a']][$data['b']]);
        }
    }

    protected function updatedCons()
    {
        $this->loadConsigneeCustomerNo();
        $this->loadConsigneeCompanyName();
        // dd($this->cons);
    }

    public $getexist = [];

    public function loadConsigneeCustomerNo()
    {
        foreach ($this->cons as $x => $con) {
            $this->customer_noc = CrmCustomerInformation::where([
                ['account_no', 'like', '%' . $this->cons[$x][$x]['customer_no_3'] . '%']
            ])
                ->orderBy('fullname', 'asc')
                ->take(10)
                ->get();
            // dd($this->customer_noc);
            // $getifexist = CrmCustomerInformation::where(
            //     'account_no', 'like', '%' . $this->cons[$x][$x]['customer_no_3'] . '%'
            // )->first();

            // if (!isset($getifexist->id)) {
            //     $this->cons[$x][$x]['first_name_3'] = "";
            //     $this->cons[$x][$x]['middle_name_3'] = "";
            //     $this->cons[$x][$x]['last_name_3'] = "";
            //     $this->cons[$x][$x]['company_name_3'] = "";
            //     $this->cons[$x][$x]['mobile_number_3'] = "";
            //     $this->cons[$x][$x]['email_address_3'] = "";
            //     $this->cons[$x][$x]['address_3'] = "";
            // }

            // $this->getexist = CrmCustomerInformation::where([['account_no', 'like', '%' . $this->cons[$x][$x]['customer_no_3'] . '%']])->first();

            // if($getexist == null){
            //     $this->identify == 0;
            // }else{
            //     $this->identify == 1;
            // }
            // dd($this->idenitfy);
            // dd($getexist);

        }
    }

    public $customer_data;

    public function getCustomerDetailsConsignee(array $data)
    {
        $this->customer_data = CrmCustomerInformation::with('mobileNumbers')->find($data['id']);
        $customer_noc = CrmCustomerInformation::with('mobileNumbers', 'emails')->with(['addresses' => function ($query) {
            $query->with('stateRef', 'cityRef', 'barangayRef');
        }])->where([
            ['id', $data['id']]
        ])
            ->orderBy('fullname', 'asc')
            ->take(10)
            ->first();

        if ($customer_noc) {
            foreach ($customer_noc->mobileNumbers as $i => $mobileNumber) {
                if ($mobileNumber->is_primary == 1) {
                    $this->number = $mobileNumber->mobile;
                }
            }
            $this->cons[$data['a']][$data['a']]['mobile_number_3'] = $this->number;

            foreach ($customer_noc->emails as $i => $email) {
                if ($email->is_primary == 1) {
                    $this->email = $email->email;
                }
            }
            $this->cons[$data['a']][$data['a']]['email_address_3'] = $this->email;

            foreach ($customer_noc->addresses as $i => $address) {
                if ($address->is_primary == 1) {
                    $this->address = $address->address_line_1 . ' ' . $address->address_line_2 . ' ' . $address->stateRef->name . ', ' . $address->cityRef->name . ', ' . $address->barangayRef->name;

                    $this->state3 = $address->state_id;
                    $this->city3 = $address->city_id;
                    $this->barangay3 = $address->barangay_id;
                    $this->postal3 = $address->postal_id;
                    // dd($this->state3);
                }
            }
            $this->cons[$data['a']][$data['a']]['address_3'] = $this->address;

            $this->cons[$data['a']][$data['a']]['state_3'] = $this->state3;
            $this->cons[$data['a']][$data['a']]['city_3'] = $this->city3;
            $this->cons[$data['a']][$data['a']]['barangay_3'] = $this->barangay3;
            $this->cons[$data['a']][$data['a']]['postal_3'] = $this->postal3;
            // dd($this->cons[$data['a']][$data['a']]['state_3'] = $this->state3);
        }

        if ($this->customer_data) {
            $this->cons[$data['a']][$data['a']]['customer_no_3'] = $this->customer_data->account_no;
            $this->cons[$data['a']][$data['a']]['first_name_3'] = $this->customer_data->first_name;
            $this->cons[$data['a']][$data['a']]['middle_name_3'] = $this->customer_data->middle_name;
            $this->cons[$data['a']][$data['a']]['last_name_3'] = $this->customer_data->last_name;
            $this->cons[$data['a']][$data['a']]['company_name_3'] = $this->customer_data->company_name;
        }
    }

    public function loadConsigneeCompanyName()
    {
        foreach ($this->cons as $x => $con) {
            $this->company_namescons = CrmCustomerInformation::where([
                ['company_name', 'like', '%' . $this->cons[$x][$x]['company_name_3'] . '%']
            ])
                ->orderBy('company_name', 'asc')
                ->take(10)
                ->get();
        }
    }

    public function getCustomerDetailsConsigneeCampName(array $data)
    {
        $this->customer_data = CrmCustomerInformation::with('mobileNumbers')->find($data['id']);
        $company_namescons = CrmCustomerInformation::with('mobileNumbers', 'emails')->with(['addresses' => function ($query) {
            $query->with('stateRef', 'cityRef', 'barangayRef');
        }])->where([
            ['id', $data['id']]
        ])
            ->orderBy('company_name', 'asc')
            ->take(10)
            ->first();

        if ($company_namescons) {
            foreach ($company_namescons->mobileNumbers as $i => $mobileNumber) {
                if ($mobileNumber->is_primary == 1) {
                    $this->number = $mobileNumber->mobile;
                }
            }
            $this->cons[$data['a']][$data['a']]['mobile_number_3'] = $this->number;

            foreach ($company_namescons->emails as $i => $email) {
                if ($email->is_primary == 1) {
                    $this->email = $email->email;
                }
            }
            $this->cons[$data['a']][$data['a']]['email_address_3'] = $this->email;

            foreach ($company_namescons->addresses as $i => $address) {
                if ($address->is_primary == 1) {
                    $this->address = $address->address_line_1 . ' ' . $address->address_line_2 . ' ' . $address->stateRef->name . ', ' . $address->cityRef->name . ', ' . $address->barangayRef->name;

                    $this->state3 = $address->state_id;
                    $this->city3 = $address->city_id;
                    $this->barangay3 = $address->barangay_id;
                    $this->postal3 = $address->postal_id;
                    // dd($this->state3);
                }
            }
            $this->cons[$data['a']][$data['a']]['address_3'] = $this->address;

            $this->cons[$data['a']][$data['a']]['state_3'] = $this->state3;
            $this->cons[$data['a']][$data['a']]['city_3'] = $this->city3;
            $this->cons[$data['a']][$data['a']]['barangay_3'] = $this->barangay3;
            $this->cons[$data['a']][$data['a']]['postal_3'] = $this->postal3;
            // dd($this->cons[$data['a']][$data['a']]['state_3'] = $this->state3);
        }

        if ($this->customer_data) {
            $this->cons[$data['a']][$data['a']]['customer_no_3'] = $this->customer_data->account_no;
            $this->cons[$data['a']][$data['a']]['first_name_3'] = $this->customer_data->first_name;
            $this->cons[$data['a']][$data['a']]['middle_name_3'] = $this->customer_data->middle_name;
            $this->cons[$data['a']][$data['a']]['last_name_3'] = $this->customer_data->last_name;
            $this->cons[$data['a']][$data['a']]['company_name_3'] = $this->customer_data->company_name;
        }
    }

    protected function updatedCustomerNo2()
    {
        $this->loadShipperCustomerNo();
    }

    public function loadShipperCustomerNo()
    {
        $this->customer_nos = CrmCustomerInformation::where([
            ['account_no', 'like', '%' . $this->customer_no_2 . '%']
        ])
            ->orderBy('fullname', 'asc')
            ->take(10)
            ->get();
    }

    public function getCustomerDetails($id)
    {
        $customer_data = CrmCustomerInformation::with('mobileNumbers')->find($id);
        $customer_nos = CrmCustomerInformation::with('mobileNumbers', 'emails')->with(['addresses' => function ($query) {
            $query->with('stateRef', 'cityRef', 'barangayRef');
        }])->where([
            ['id', $id]
        ])
            ->orderBy('fullname', 'asc')
            ->take(10)
            ->first();


        if ($customer_nos) {
            foreach ($customer_nos->mobileNumbers as $i => $mobileNumber) {
                if ($mobileNumber->is_primary == 1) {
                    $this->number = $mobileNumber->mobile;
                }
            }

            foreach ($customer_nos->emails as $i => $email) {
                if ($email->is_primary == 1) {
                    $this->email = $email->email;
                }
            }

            foreach ($customer_nos->addresses as $i => $address) {
                // dd($address);
                if ($address->is_primary == 1) {
                    $this->address = $address->address_line_1 . ' ' . $address->address_line_2 . ' ' . $address->stateRef->name . ', ' . $address->cityRef->name . ', ' . $address->barangayRef->name;
                    // $this->address = 'asdasdas';

                    $this->state2 = $address->state_id;
                    $this->city2 = $address->city_id;
                    $this->barangay2 = $address->barangay_id;
                    $this->postal2 = $address->postal_id;
                }
            }
        }

        $this->mobile_number_2 = $this->number;
        $this->email_address_2 = $this->email;
        $this->address_2 = $this->address;

        if ($customer_data) {
            $this->customer_no_2 = $customer_data->account_no;
            $this->first_name_2 = $customer_data->first_name;
            $this->middle_name_2 = $customer_data->middle_name;
            $this->last_name_2 = $customer_data->last_name;
            $this->company_name_2 = $customer_data->company_name;
        }
    }

    protected function updatedCompanyName2()
    {
        $this->loadShipperCompanyName();
    }

    public function loadShipperCompanyName()
    {
        $this->company_names = CrmCustomerInformation::where([
            ['company_name', 'like', '%' . $this->company_name_2 . '%']
        ])
            ->orderBy('company_name', 'asc')
            ->take(10)
            ->get();
    }

    public function getCustomerCompanyName($id)
    {
        $customer_data = CrmCustomerInformation::with('mobileNumbers')->find($id);
        $company_names = CrmCustomerInformation::with('mobileNumbers', 'emails')->with(['addresses' => function ($query) {
            $query->with('stateRef', 'cityRef', 'barangayRef');
        }])->where([
            ['id', $id]
        ])
            ->orderBy('company_name', 'asc')
            ->take(10)
            ->first();


        if ($company_names) {
            foreach ($company_names->mobileNumbers as $i => $mobileNumber) {
                if ($mobileNumber->is_primary == 1) {
                    $this->number = $mobileNumber->mobile;
                }
            }

            foreach ($company_names->emails as $i => $email) {
                if ($email->is_primary == 1) {
                    $this->email = $email->email;
                }
            }

            foreach ($company_names->addresses as $i => $address) {
                // dd($address);
                if ($address->is_primary == 1) {
                    $this->address = $address->address_line_1 . ' ' . $address->address_line_2 . ' ' . $address->stateRef->name . ', ' . $address->cityRef->name . ', ' . $address->barangayRef->name;
                    // $this->address = 'asdasdas';

                    $this->state2 = $address->state_id;
                    $this->city2 = $address->city_id;
                    $this->barangay2 = $address->barangay_id;
                    $this->postal2 = $address->postal_id;
                }
            }
        }

        $this->mobile_number_2 = $this->number;
        $this->email_address_2 = $this->email;
        $this->address_2 = $this->address;

        if ($customer_data) {
            $this->customer_no_2 = $customer_data->account_no;
            $this->first_name_2 = $customer_data->first_name;
            $this->middle_name_2 = $customer_data->middle_name;
            $this->last_name_2 = $customer_data->last_name;
            $this->company_name_2 = $customer_data->company_name;
        }
    }

    public function addAttachments()
    {
        $this->attachments[] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }


    public function removeAttachments($i)
    {
        if (count(collect($this->attachments)->where('is_deleted', false)) > 1) {
            if ($this->attachments[$i]['id']) {
                $this->attachments[$i]['is_deleted'] = true;
            } else {
                unset($this->attachments[$i]);
            }

            array_values($this->attachments);
        }
    }


    public function updatedTime1()
    {
        if ($this->time_1 == 1) {
            $this->time_from_1 = "13:00:00";
            $this->time_to_1 = "15:00:00";
        } else if ($this->time_1 == 2) {
            $this->time_from_1 = "15:00:00";
            $this->time_to_1 = "17:00:00";
        } else if ($this->time_1 == 3) {
            $this->time_from_1 = "17:00:00";
            $this->time_to_1 = "19:00:00";
        } else if ($this->time_1 == 4) {
            $this->time_from_1 = "19:00:00";
            $this->time_to_1 = "21:00:00";
        } else if ($this->time_1 == 5) {
            $this->time_from_1 = "21:00:00";
            $this->time_to_1 = "23:00:00";
        } else {
            $this->time_from_1 = "";
            $this->time_to_1 = "";
        }
    }


    public function getRequest()
    {
        return [
            'booking_type_1' => $this->booking_type_1,
            'vehicle_type_1' => $this->vehicle_type_1,
            'pickup_date_1' => $this->pickup_date_1,
            'time_slot_1' => $this->time_slot_1,
            'consignee_count_1' => $this->consignee_count_1,
            'walk_in_branch_1' => $this->walk_in_branch_1,
            'activity_type_1' => $this->activity_type_1,
            'consignee_category_1' => $this->consignee_category_1,
            'booking_category_1' => $this->booking_category_1,
            'time_1' => $this->time_1,
            'time_from_1' => $this->time_from_1,
            'time_to_1' => $this->time_to_1,
            'time_to_1' => $this->time_to_1,
            'ismobile' => null,
            // 'channel_1' => $this->channel_1,

            'account_type_2' => $this->account_type_2,
            'booking_reference_no_2' => $this->booking_reference_no_2,
            'customer_no_2' => $this->customer_no_2,
            'company_name_2' => $this->company_name_2,
            'first_name_2' => $this->first_name_2,
            'middle_name_2' => $this->middle_name_2,
            'last_name_2' => $this->last_name_2,
            'mobile_number_2' => $this->mobile_number_2,
            'email_address_2' => $this->email_address_2,
            'address_2' => $this->address_2,
            'attachments' => $this->attachments,

            'work_instruction_3' => $this->work_instruction_3,
            'remarks_3' => $this->remarks_3,
            'cons' => $this->cons,
            'cardets' => $this->cardets,

            // 'addresslabel' => $this->addresslabel,
            // 'add1'=> $this->add1,
            // 'add2'=> $this->add2,
            // 'state' => $this->state,
            // 'city' => $this->city,
            // 'barangay' => $this->barangay,
            // 'postal' => $this->postal,

            'state2' => $this->state2,
            'city2' => $this->city2,
            'barangay2' => $this->barangay2,
            'postal2' => $this->postal2,



        ];
    }

    public function getRequestcancel()
    {
        return [
            'cancel_booking_idx' => $this->cancel_booking_idx,
        ];
    }
    public function resetCancel()
    {
        $this->resetForm();
        $this->reset([
            'cancel_booking_idx',
        ]);
    }

    public function getRequestresched()
    {
        return [
            'reschedule_booking_idx' => $this->reschedule_booking_idx,
        ];
    }

    public function resetResched()
    {
        $this->resetForm();
        $this->reset([
            'reschedule_booking_idx',
        ]);
    }

    public function resetForm()
    {
        $this->resetErrorBag();
        $this->reset([
            'booking_type_1',
            'vehicle_type_1',
            'pickup_date_1',
            'time_slot_1',
            'consignee_count_1',
            'walk_in_branch_1',
            'activity_type_1',
            'consignee_category_1',
            'booking_category_1',
            // 'channel_1',
            'time_1',
            'time_from_1',
            'time_to_1',

            'account_type_2',
            'booking_reference_no_2',
            'customer_no_2',
            'company_name_2',
            'first_name_2',
            'middle_name_2',
            'last_name_2',
            'mobile_number_2',
            'email_address_2',
            'address_2',
            'attachments',

            'work_instruction_3',
            'remarks_3',
            'cons',
            'cardets',

            // 'addresslabel',
            // 'add1',
            // 'add2',
            // 'state',
            // 'city',
            // 'barangay',
            // 'postal',

            'state2',
            'city2',
            'barangay2',
            'postal2',
        ]);

        $this->confirmation_modal = false;
    }

    public function redirectTo(array $data, $action_type)
    {
        $this->action_type = $action_type;

        if ($action_type == 'redirectTobook') {
            return redirect()->to(route('crm.sales.booking-mgmt.index'));
        } else if ($action_type == 'redirectTobookhis') {
            return redirect()->to(route('crm.sales.booking-mgmt.booking-history.index2'));
        }
    }
}
