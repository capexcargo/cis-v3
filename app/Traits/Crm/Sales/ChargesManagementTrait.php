<?php

namespace App\Traits\Crm\Sales;


trait ChargesManagementTrait
{
    public $create_modal = false;
    public $edit_modal = false;
    public $delete_modal = false;
    public $update_status_modal = false;
    public $confirmation_modal = false;
    public $confirmation_message;

    public $status;
    public $charge_id;
    public $action_type;

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;

    public $charges_management;
    public $rate_calcu_charges;
    public $charges_category;

    public function getRequest()
    {
        return [
            'rate_calcu_charges' => $this->rate_calcu_charges,
            'charges_category' => $this->charges_category,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'rate_calcu_charges',
            'charges_category',
        ]);
        $this->confirmation_modal = false;
    }
}
