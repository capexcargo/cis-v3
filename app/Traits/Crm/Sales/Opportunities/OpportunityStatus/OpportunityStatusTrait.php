<?php

namespace App\Traits\Crm\Sales\Opportunities\OpportunityStatus;

trait OpportunityStatusTrait
{

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $action_type;

    public $name;

    public function getRequest()
    {
        return [
            'name' =>$this->name,
        ];
    }
    
    public function resetForm()
    {
        $this->reset([
            'name',
        ]);        
        $this->confirmation_modal = false;

    }
    
}