<?php

namespace App\Traits\Crm\Sales\Opportunities\SalesStage;

use App\Models\CrmOpportunityStatusMgmt;
use App\Models\CrmSalesStage;

trait SalesStageTrait
{

    public $paginate = 10;
    public $sortField = 'created_at';
    public $sortAsc = false;
    public $display;
    public $date_created;
    public $action_type;

    public $sales_stage;
    public $opportunity_status_id;


    public $sales_references = [];


    public function load()
    {
    $this-> OpportunityStatusReference();
    }

    public function OpportunityStatusReference()
    {
        $this->sales_references = CrmOpportunityStatusMgmt::get();
    }
    
    public function getRequest()
    {
        return [
            'sales_stage' =>$this->sales_stage,
            'opportunity_status_id' =>$this->opportunity_status_id,
        ];
        
    }

    
    public function resetForm()
    {
        $this->reset([
            'sales_stage',
            'opportunity_status_id',
        ]);        
        $this->confirmation_modal = false;

    }



}