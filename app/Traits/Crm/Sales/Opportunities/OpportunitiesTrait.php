<?php

namespace App\Traits\Crm\Sales\Opportunities;


trait OpportunitiesTrait
{
    public $company_industry_references = [];
    public $sales_stage_references = [];

    public $opportunity;
    public $opportunity_id;
    public $opportunity_name;
    public $sr_number;
    public $account_id;
    public $company_industry;
    public $sales_stage;
    public $opportunity_status;
    public $deal_size;
    public $actual_amount_converted;
    public $booking_reference;
    public $lead_conversion_date;
    public $close_date;
    public $attachments = [];

    public $contact_person;
    public $contact_mobile_no;
    public $contact_email_add;
    public $description;
    public $contact_owner;
    public $customer_type;

    public $view_modal = false;

    public function addAttachments()
    {
        $this->attachments[] = [
            'id' => null,
            'attachment' => null,
            'path' => null,
            'name' => null,
            'extension' => null,
            'is_deleted' => false,
        ];
    }


    public function removeAttachments($i)
    {
        if (count(collect($this->attachments)->where('is_deleted', false)) > 1) {
            if ($this->attachments[$i]['id']) {
                $this->attachments[$i]['is_deleted'] = true;
            } else {
                unset($this->attachments[$i]);
            }

            array_values($this->attachments);
        }
    }

    public function getRequest()
    {
        return [
            'opportunity_name' => $this->opportunity_name,
            // 'sr_number' => $this->sr_number,
            'company_industry' => $this->company_industry,
            'sales_stage' => $this->sales_stage,
            'deal_size' => $this->deal_size,
            'actual_amount' => $this->actual_amount_converted,
            'lead_conversion_date' => $this->lead_conversion_date,
            'close_date' => $this->close_date,

            'attachments' => $this->attachments,
        ];
    }

    public function resetForm()
    {
        $this->reset([
            'retirement_reason',
            'remarks',
        ]);
        $this->confirmation_modal = false;
    }
}
