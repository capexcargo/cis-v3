<?php

namespace App\Traits\Crm\Sales;

use App\Models\Crm\CrmQuotation;
use App\Models\CrmShipmentType;

trait QuotationTrait
{
    public $generate_quotation_modal = false;
    public $action_type;
    public $from_sr;

    public $date;
    public $quotation_ref_no;

    public $service_request;
    public $quotation;

    public $sr_id;
    public $shipment_type;
    public $sr_number;
    public $account_id;
    public $contact_owner;
    public $assigned_to_id;

    public $subject;

    public $fname;
    public $mname;
    public $lname;

    public $company_business_references = [];
    public $company_business_search;
    public $company_business_name;
    public $shipment_type_references = [];

    public $contact_person_references = [];
    public $contact_person_search;
    public $primary_contact_person;
    public $position;

    public $mobile_number;
    public $telephone_number;
    public $email_address;

    public $channel_sr_source;
    public $status = 1;
    public $sr_description;

    public $address_line_1;
    public $address_line_2;
    public $address_type;
    public $address_label;
    public $state_province;
    public $city_municipal;
    public $barangay;
    public $postal;

    public $evat_chkbox = true;

    public $exact_pickup_location;
    public $exact_dropoff_location;

    public $description;

    public function shipmentTypeReferences()
    {
        $this->shipment_type_references = CrmShipmentType::get();
    }

    public function generateQuotationRefNo()
    {
        $ref_no = CrmQuotation::select('id')->latest()->first();
        if ($ref_no) {
            $last_inserted_id = $ref_no->id + 1;
        } else {
            $last_inserted_id = 1;
        }

        $this->quotation_ref_no = "CPX-Q-" . str_pad(($last_inserted_id), 4, "0", STR_PAD_LEFT);
    }

    public function getQuotationRequests()
    {
        return [
            'date' => $this->date,
            'quotation_ref_no' => $this->quotation_ref_no,
            'shipment_type' => $this->shipment_type,
            'sr_number' => $this->sr_number,
            'subject' => $this->subject,
            'account_id' => $this->account_id,
            'contact_owner' => $this->contact_owner,
            'assigned_to_id' => $this->assigned_to_id,

            'transport_mode' => $this->transport_mode,
            'origin' => $this->origin,
            'destination' => $this->destination,
            'exact_pickup_location' => $this->exact_pickup_location,
            'exact_dropoff_location' => $this->exact_dropoff_location,
            'declared_value' => $this->declared_value,
            'commodity_type' => $this->commodity_type,
            'commodity_applicable_rate' => $this->commodity_applicable_rate,
            'paymode' => $this->paymode,
            'description' => $this->description,
            'service_mode' => $this->service_mode,

            //Transport mode = 1
            'air_cargo' => $this->air_cargo,
            'air_cargos' => $this->air_cargos,

            'air_pouch' => $this->air_pouch,
            'air_pouches' => $this->air_pouches,

            'air_box' => $this->air_box,
            'air_boxes' => $this->air_boxes,


            //Transport mode = 2
            'sea_cargo' => $this->sea_cargo,
            'sea_cargos_is_lcl' => $this->sea_cargos_is_lcl,
            'sea_cargos_is_fcl' => $this->sea_cargos_is_fcl,
            'sea_cargos_is_rcl' => $this->sea_cargos_is_rcl,

            'sea_box' => $this->sea_box,
            'sea_boxes' => $this->sea_boxes,

            //Transport mode = 3
            'land' => $this->land,
            'lands_shippers_box' => $this->lands_shippers_box,
            'lands_pouches' => $this->lands_pouches,
            'lands_boxes' => $this->lands_boxes,
            'lands_ftl' => $this->lands_ftl,


            // BREAKDOWN OF FREIGHT CHARGES
            'weight_charge' => $this->weight_charge,
            'awb_fee' => $this->awb_fee,
            'valuation' => $this->valuation,
            'cod_charge' => $this->cod_charge,
            'insurance' => $this->insurance,
            'handling_fee' => $this->handling_fee,
            
            'evat_chkbox' => $this->evat_chkbox,
            'evat' => $this->evat,
            
            'other_fees' => $this->other_fees,
            'opa_fee' => $this->opa_fee,
            'oda_fee' => $this->oda_fee,
            'equipment_rental' => $this->equipment_rental,
            'crating_fee' => $this->crating_fee,
            'lashing_fee' => $this->lashing_fee,
            'manpower_fee' => $this->manpower_fee,
            'dangerous_goods_fee' => $this->dangerous_goods_fee,
            'trucking_fee' => $this->trucking_fee,
            'perishable_fee' => $this->perishable_fee,
            'packaging_fee' => $this->packaging_fee,
            'discount_amount' => $this->discount_amount,
            'discount_percentage' => $this->discount_percentage,
            'subtotal' => $this->subtotal,
            'grand_total' => $this->grand_total,
        ];
    }
}
