<?php

namespace App\Interfaces\Globals;

interface AccountManagementInterface
{
    public function create($validated, $division);
    public function update($user, $validated, $validated_password, $division);
    public function show($id);
    
}