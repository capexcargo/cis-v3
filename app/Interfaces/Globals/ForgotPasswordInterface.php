<?php

namespace App\Interfaces\Globals;

interface ForgotPasswordInterface
{
    public function update($request);
    public function show();

}