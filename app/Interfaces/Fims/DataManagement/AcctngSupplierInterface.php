<?php
namespace App\Interfaces\Fims\DataManagement;

interface AcctngSupplierInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
    public function toggleStatus($request, $id);
    public function getIndustry($request);
    public function getRegion($request);
    public function getProvince($request);
    public function getMunicipal($request);
    public function getBarangay($request);
    public function getBank($request);

}