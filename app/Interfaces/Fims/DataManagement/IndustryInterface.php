<?php
namespace App\Interfaces\Fims\DataManagement;

interface IndustryInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
    public function toggleStatus($request, $id);
}