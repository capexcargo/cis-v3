<?php
namespace App\Interfaces\Fims\DataManagement;

interface ItemDescriptionInterface
{
    public function index($request);
    public function getItemCateg($request);
    public function create($request);
    public function update($request, $id);
    public function toggleStatus($request, $id);

    public function createValidation2($request);
    public function create2($request);
    public function updateValidation2($request);
    public function update2($request, $id);
    public function index2($request, $search_request);

}