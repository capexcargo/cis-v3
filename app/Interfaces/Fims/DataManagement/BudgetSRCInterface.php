<?php
namespace App\Interfaces\Fims\DataManagement;

interface BudgetSRCInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
    public function toggleStatus($request, $id);
}