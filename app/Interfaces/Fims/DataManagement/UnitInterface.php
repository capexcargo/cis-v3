<?php
namespace App\Interfaces\Fims\DataManagement;

interface UnitInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
    public function toggleStatus($request, $id);

    public function index2($request, $search_request);
    public function createValidation2($request);
    public function create2($request);
    public function updateValidation2($request);
    public function update2($request, $id);
}