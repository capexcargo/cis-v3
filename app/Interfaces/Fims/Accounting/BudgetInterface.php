<?php
namespace App\Interfaces\Fims\Accounting;

interface BudgetInterface
{
    public function index($request);
    public function create($request);
    public function getCoa($request);
    // public function update($request, $id);
    // public function destroy($id);
}