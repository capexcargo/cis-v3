<?php
namespace App\Interfaces\Fims\Accounting;

interface AcctngPurchasingLoaInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
    public function destroy($id);
}