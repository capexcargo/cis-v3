<?php
namespace App\Interfaces\Fims\ServicePurchaseRequisition;

interface PurchaseReqInterface
{
    
    // public function update($request, $id);
    public function create($request);
    public function reqstat($request, $id);
    public function index($request);
    public function getItemCateg($request);
    public function getItemDesc($request);
    public function getPurp($request);
    public function getBenBranch($request);
    public function getPrefSup($request);

    
    public function index2($request, $search_request);
    public function update2($request, $id);
    public function createValidation2($request);
    public function create2($request);
   
}