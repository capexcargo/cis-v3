<?php
namespace App\Interfaces\Fims\ServicePurchaseRequisition;

interface ServiceReqInterface
{
    public function index($request);
    public function index2($request);
    public function getServiceCateg($request);
    public function getServiceDesc($request);
    public function getPrefWork($request);
    public function getPurp($request);
    public function getLoc($request);
    public function create($request);
    public function createservice($request);
    public function update2($request, $id);
    
    // public function update($request, $id);
    public function reqstat($request, $id);
    public function approvestat($request, $id);
    
    // public function attachment(RequestForPayment $request_for_payment, $attachments);
    // public function destroy($id);
}