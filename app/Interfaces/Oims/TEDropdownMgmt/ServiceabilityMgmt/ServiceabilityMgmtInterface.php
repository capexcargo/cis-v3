<?php
namespace App\Interfaces\Oims\TEDropdownMgmt\ServiceabilityMgmt;

interface ServiceabilityMgmtInterface
{
    public function index($request, $search_request);
    public function createValidation($request);
    public function updateValidation($request, $id);
    public function create($request);
    public function update($request, $id);
}