<?php

namespace App\Interfaces\Oims\TransactionEntry;

interface TransactionEntryInterface
{
    // public function index($request, $search_request);
    // public function dispatch( $request);
    public function selectedValidation($request);
    public function createValidation($request);
    // public function updateValidation($request, $id);
    public function create($request);
    // public function update($request, $id);
}
