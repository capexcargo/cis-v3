<?php
namespace App\Interfaces\Oims\WaybillRegistry;

interface WaybillRegistryInterface
{
    public function index($request, $search_request);
    public function view($request);
    
    public function create($request);
    public function createValidation($request);
    public function issueToOMSkip();
    public function issueToCheckerSkip();

    public function update($request, $id);
    public function updateValidation($request, $id);
    public function issueToOMSkipEdit();
    public function issueToCheckerSkipEdit();

    public function issue($request, $id);
    public function issueValidation($request, $id);
    public function issueToOMSkipIssue();
    public function issueToCheckerSkipIssue();
}