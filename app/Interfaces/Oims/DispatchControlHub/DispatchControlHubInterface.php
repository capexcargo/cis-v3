<?php

namespace App\Interfaces\Oims\DispatchControlHub;

interface DispatchControlHubInterface
{
    public function index($request, $actStatusHead);
    public function dispatch($request);
    public function createValidation($request);
    // public function updateValidation($request, $id);
    public function create($request);
    public function actualStartTravel($request, $id);
    public function actualEndTravel($request, $id);
    public function pickupConfirmedtime($request, $id);
    public function pickupStartTime($request, $id);
    public function pickupEndTime($request, $id);
    public function deliveryStartTime($request, $id);
    public function deliveryEndTime($request, $id);
    // public function update($request, $id);
}
