<?php
namespace App\Interfaces\Oims\TeamMgmt;

interface TeamMgmtInterface
{
    public function index($request);
    public function createValidation($request);
    public function updateValidation($request, $id);
    public function create($request);
    public function update($request, $id);
}