<?php
namespace App\Interfaces\Oims\TeamRouteAssignment;

interface TeamRouteAssignmentInterface
{
    public function index($request, $search_request);
    public function print($request, $search_request);
    public function createValidation($request);
    public function updateValidation($request, $id);
    public function show($id);
    public function create($request);
    public function update($request, $id);
}