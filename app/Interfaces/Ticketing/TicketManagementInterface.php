<?php

namespace App\Interfaces\Ticketing;

interface TicketManagementInterface
{
    public function show($id);
    public function index($request);
    public function index_dashboard($request);
    public function index_my_tickets($request);
    public function index_my_tasks($request);
    public function create($request, $division);
    public function update($request, $id);
    public function updateActualDate($request, $id);
    public function close($id, $closer);
}
