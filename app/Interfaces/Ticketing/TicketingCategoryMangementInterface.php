<?php

namespace App\Interfaces\Ticketing;

interface TicketingCategoryMangementInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
}
