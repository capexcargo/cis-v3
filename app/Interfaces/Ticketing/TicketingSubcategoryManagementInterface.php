<?php

namespace App\Interfaces\Ticketing;

interface TicketingSubcategoryManagementInterface
{
    public function show($id);
    public function index($request);
    public function create($request);
    public function update($request, $id);
}
