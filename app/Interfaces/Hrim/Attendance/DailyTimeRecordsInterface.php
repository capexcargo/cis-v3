<?php

namespace App\Interfaces\Hrim\Attendance;

interface DailyTimeRecordsInterface
{
    public function index($request);
}
