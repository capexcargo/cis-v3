<?php

namespace App\Interfaces\Hrim\Attendance;

interface TeamStatusInterface
{
    public function index($request);
    public function showTeam($id, $request);
}
