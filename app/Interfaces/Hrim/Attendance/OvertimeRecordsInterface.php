<?php

namespace App\Interfaces\Hrim\Attendance;

interface OvertimeRecordsInterface
{
    public function index($request);
    public function showUserOvertimeRecords($id, $request = []);
    public function show($id, $request = []);
    public function approve($id, $request);
}
