<?php

namespace App\Interfaces\Hrim\Attendance;

interface SchedulesInterface
{
    public function index($request);
}
