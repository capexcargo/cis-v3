<?php

namespace App\Interfaces\Hrim\Attendance;

interface TarInterface
{
    public function index($request);
    public function showUserTar($id, $request = []);
    public function show($id, $request = []);
    public function approve($id, $request);
}
