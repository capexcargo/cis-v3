<?php

namespace App\Interfaces\Hrim\Attendance;

interface LeaveRecordsInterface
{
    public function index($request);
    public function showUserLeaves($id, $request = []);
    public function show($id, $request = []);
    public function approve($id, $request);
}
