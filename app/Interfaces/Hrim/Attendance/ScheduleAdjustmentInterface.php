<?php

namespace App\Interfaces\Hrim\Attendance;

interface ScheduleAdjustmentInterface
{
    public function index($request);
    public function showUserScheduleAdjustment($id, $request = []);
    public function show($id, $request = []);
    public function approve($id, $request);
}
