<?php

namespace App\Interfaces\Hrim\RecruitmentAndHiring;

use App\Models\Hrim\EmployeeRequisition;

interface EmployeeRequisitionInterface
{
    public function index($requested);
    public function create($validated);
    public function update(EmployeeRequisition $employeeRequisition, $validated);
    public function cancel(EmployeeRequisition $employeeRequisition);
    public function approve(EmployeeRequisition $employeeRequisition);
    public function decline(EmployeeRequisition $employeeRequisition);
}
