<?php

namespace App\Interfaces\Hrim\RecruitmentAndHiring;

use App\Models\Hrim\EmployeeRequisition;

interface EmploymentCategoryTypeInterface
{
    public function index($request);
    public function create($request);
    public function destroy($id);
    public function update($request, $id);

}