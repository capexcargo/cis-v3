<?php

namespace App\Interfaces\Hrim\RecruitmentAndHiring;

use App\Models\Hrim\ApplicantStatusMgmt;

interface ApplicantStatusMgmtInterface
{
    public function create($validated);
    public function update(ApplicantStatusMgmt $applicantStatusMgmt, $validated);

    public function destroy(ApplicantStatusMgmt $applicantStatusMgmt);
}
