<?php

namespace App\Interfaces\Hrim\RecruitmentAndHiring;

use App\Models\Hrim\Onboarding;

interface OnboardingInterface
{
    public function update(Onboarding $onboarding, $erf_reference_no_id, $final_remarks, $validated);
}
