<?php

namespace App\Interfaces\Hrim\RecruitmentAndHiring;

use App\Models\Hrim\ApplicantTracking;

interface ApplicantTrackingInterface
{
    public function create($validated);
    public function update(ApplicantTracking $applicantTracking, $validated);
}
