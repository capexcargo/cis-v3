<?php

namespace App\Interfaces\Hrim\RecruitmentAndHiring;

use App\Models\Hrim\OnboardingStatusMgmt;

interface OnboardingStatusMgmtInterface
{
    public function create($validated);
    public function update(OnboardingStatusMgmt $applicantStatusMgmt, $validated);

    public function destroy(OnboardingStatusMgmt $applicantStatusMgmt);
}
