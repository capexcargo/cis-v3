<?php

namespace App\Interfaces\Hrim\CompanyManagement;

interface EmployerComplianceInterface
{
    public function index($request);
    public function create($request);
    public function show($id, $request = []);
    public function update($id, $request);
    public function destroy($id);
    public function restore($id);
}
