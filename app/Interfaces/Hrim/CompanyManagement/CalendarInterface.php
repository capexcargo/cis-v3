<?php

namespace App\Interfaces\Hrim\CompanyManagement;

interface CalendarInterface
{
    public function validation($request);
    public function create($request);
    public function show($id, $request = []);
    public function update($id, $request);
    public function destroy($id);
    public function restore($id);
    public function getCalendarEvents($request);
}