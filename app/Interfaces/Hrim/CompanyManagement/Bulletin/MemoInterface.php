<?php

namespace App\Interfaces\Hrim\CompanyManagement\Bulletin;

use App\Models\Hrim\Memos;

interface MemoInterface
{
    public function create($validated);
    public function update(Memos $memo, $validated);
    public function show($id);
    public function index($request);


    public function destroy(Memos $memo);

}
