<?php

namespace App\Interfaces\Hrim\CompanyManagement\Bulletin;

use App\Models\Hrim\Announcements;

interface AnnouncementInterface
{
    public function create($validated);
    public function update(Announcements $announcement, $validated);
    public function show($id);
    public function index($request);


    public function destroy(Announcements $announcement);


}
