<?php

namespace App\Interfaces\Hrim\Workforce;

interface TeamsInterface
{
    public function index($request);
    public function show($id);
    public function validation($request);
    public function updateOrCreate($id, $request);
    public function updateStatus($id, $status);
    public function showTeams($id);
    public function validateAttachment($request);
    public function addFile($request, $id);
}
