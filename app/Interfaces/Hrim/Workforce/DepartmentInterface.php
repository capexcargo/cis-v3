<?php

namespace App\Interfaces\Hrim\Workforce;

use App\Models\Hrim\Department;

interface DepartmentInterface
{
    public function createDepartment($validated);
    public function updateDepartment(Department $department, $validated);
    public function show($id);

    public function destroy(Department $department);
}


?>