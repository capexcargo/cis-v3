<?php

namespace App\Interfaces\Hrim\Workforce;

use App\Models\Hrim\Position;

interface PositionInterface
{
    public function create($validated);
    public function update(Position $position, $validated);
    public function show($id);

    public function destroy(Position $position);
}


?>