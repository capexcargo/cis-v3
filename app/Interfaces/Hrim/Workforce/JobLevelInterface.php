<?php

namespace App\Interfaces\Hrim\Workforce;

use App\Models\Hrim\JobLevel;

interface JobLevelInterface
{
    public function createJobLevel($validated);
    public function updateJobLevel(JobLevel $joblevel, $validated);
    public function show($id);


    public function destroy(JobLevel $joblevel);
}


?>