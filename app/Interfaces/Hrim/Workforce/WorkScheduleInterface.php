<?php

namespace App\Interfaces\Hrim\Workforce;

interface WorkScheduleInterface
{
    public function index($request);
    public function validation($request);
    public function create($request);
    public function show($id);
    public function update($id, $request);
}
