<?php

namespace App\Interfaces\Hrim\Csp;

interface CspUserManagementInterface
{
    public function index($request);
    public function create($request);
    public function update($id, $validated, $pass);
    public function updateStatus($id, $status);

    public function destroy($id);
}
