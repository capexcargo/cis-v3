<?php

namespace App\Interfaces\Hrim\Csp;

use App\Models\Hrim\Agencies;

interface CspManagementInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
    public function updateStatus($id, $status);

    public function destroy(Agencies $agencies);
}
