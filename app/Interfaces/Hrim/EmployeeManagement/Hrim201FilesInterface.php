<?php

namespace App\Interfaces\Hrim\EmployeeManagement;

interface Hrim201FilesInterface
{
    public function index($request);
    public function disciplinaryIndex($request);
    public function createValidation($request);
    public function create($request);
    public function show($id, $user_id, $request = []);
    public function showAttachment($id, $request = []);
    public function destroy($id, $user_id);
    public function restore($id, $user_id);
    public function destroyAttachment($id);
    public function restoreAttachment($id);
}
