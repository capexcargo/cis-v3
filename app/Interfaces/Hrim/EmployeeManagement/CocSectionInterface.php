<?php

namespace App\Interfaces\Hrim\EmployeeManagement;

use App\Models\Hrim\CocSection;

interface CocSectionInterface
{
    public function createValidation($request);
    public function create($request);
    public function show($id);
    public function updateValidation($request);
    public function update($i, $request);

    public function destroy(CocSection $coc_section);
}
