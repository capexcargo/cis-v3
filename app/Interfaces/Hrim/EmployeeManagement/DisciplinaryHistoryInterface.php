<?php

namespace App\Interfaces\Hrim\EmployeeManagement;

use App\Models\Hrim\DisciplinaryHistory;

interface DisciplinaryHistoryInterface
{
    public function create($validated);
    public function update(DisciplinaryHistory $disciplinaryHistory, $validated);
    public function show($id);

    public function destroy(DisciplinaryHistory $disciplinaryHistory);
}
