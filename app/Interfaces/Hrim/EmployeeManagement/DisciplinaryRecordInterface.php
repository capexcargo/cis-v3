<?php

namespace App\Interfaces\Hrim\EmployeeManagement;

use App\Models\Hrim\DisciplinaryRecord;

interface DisciplinaryRecordInterface
{
    public function createValidation($request);
    public function create($request);
    public function show($id);
    public function updateValidation($request);
    public function update($i, $request);

    public function destroy(DisciplinaryRecord $disciplinary_record);
}
