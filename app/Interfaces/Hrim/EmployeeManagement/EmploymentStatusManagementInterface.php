<?php

namespace App\Interfaces\Hrim\EmployeeManagement;

use App\Models\Hrim\EmployeeRequisition;

interface EmploymentStatusManagementInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
    public function destroy($id);

}