<?php

namespace App\Interfaces\Hrim\EmployeeManagement;

use App\Models\Hrim\SanctionStatus;

interface SanctionStatusInterface
{
    public function createValidation($request);
    public function create($request);
    public function show($id);
    public function updateValidation($request);
    public function update($i, $request);

    public function destroy(SanctionStatus $sanction_status);
}
