<?php

namespace App\Interfaces\Hrim\EmployeeManagement;

interface EmployeeInformationInterface
{
    public function index($request);
    public function createValidationTab1($request);
    public function createValidationTab2($request);
    public function create($request);
    public function show($id);
    public function updateValidationTab1($user, $request);
    public function updateValidationTab2($user, $request);
    public function update($id, $request);
    public function updateStatus($id, $status_id);
}
