<?php

namespace App\Interfaces\Hrim\EmployeeManagement;

use App\Models\Hrim\Sanction;

interface SanctionInterface
{
    public function createValidation($request);
    public function create($request);
    public function show($id);
    public function updateValidation($request);
    public function update($i, $request);

    public function destroy(Sanction $sanction);


}
