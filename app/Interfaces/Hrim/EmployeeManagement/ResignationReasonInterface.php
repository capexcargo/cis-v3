<?php

namespace App\Interfaces\Hrim\EmployeeManagement;

interface ResignationReasonInterface
{
    public function show($id);
    public function updateOrCreate($id, $request);
}
