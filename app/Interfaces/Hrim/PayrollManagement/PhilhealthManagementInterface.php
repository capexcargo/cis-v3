<?php

namespace App\Interfaces\Hrim\PayrollManagement;

interface PhilhealthManagementInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
}
