<?php

namespace App\Interfaces\Hrim\PayrollManagement;

interface CutOffManagementInterface
{
    public function index($request);
    public function create($request);
    public function show($id);
    public function update($request, $id);
}
