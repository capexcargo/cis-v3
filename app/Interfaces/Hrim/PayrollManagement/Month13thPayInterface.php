<?php

namespace App\Interfaces\Hrim\PayrollManagement;

interface Month13thPayInterface
{
    public function index($request);
    public function email13thMonth($selected, $request);
    public function export13thMonth($selected, $request);
}
