<?php

namespace App\Interfaces\Hrim\PayrollManagement;

interface DlbManagementInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
    public function show($id);
}
