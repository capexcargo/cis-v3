<?php

namespace App\Interfaces\Hrim\PayrollManagement;

interface HolidayManagementInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
    public function destroy($id);
}
