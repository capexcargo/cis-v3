<?php

namespace App\Interfaces\Hrim\PayrollManagement;

interface AdminAdjustmentInterface
{
    public function show($id);
    public function index($request);
    public function create($request);
    public function update($request, $id);
    public function approve($id);
    public function destroy($id);
}
