<?php

namespace App\Interfaces\Hrim;

interface TimeInAndOutInterface
{
    public function getUserTimeLogs($request);
    public function currentTimelog();
    public function timeIn($request);
    public function timeOut($request);
    public function timeInAll($request);
    public function timeOutAll($request);
    public function getWorkSchedule($request);
    public function getWorkSchedule2($request);
    
}
