<?php

namespace App\Interfaces\Hrim\Dashboard;

interface DashboardInterface
{
    public function countEmployeeByStatus();
    public function countEmployeeStatusByBranch();
    public function graphs();
    public function tableLists();
}
