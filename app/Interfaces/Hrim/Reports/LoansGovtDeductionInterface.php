<?php

namespace App\Interfaces\Hrim\Reports;

interface LoansGovtDeductionInterface
{
    public function index($requested);
    public function show($id);
}
