<?php

namespace App\Interfaces\Hrim\EmployeeAttendance;

interface TarInterface
{
    public function index($request);
    public function create($request);
    public function show($request);
}
