<?php

namespace App\Interfaces\Hrim\EmployeeAttendance;

interface DailyTimeRecordsInterface
{
    public function index($request);
}
