<?php

namespace App\Interfaces\Hrim\EmployeeAttendance;

interface OvertimeRecordsInterface
{
    public function index($request);
    public function validation($request);
    public function create($request);
    public function show($id, $request = []);
    public function showDate($date);
    public function update($id, $request);
    public function getOvertimed($request);
    public function getComputedOvertime($request);
}
