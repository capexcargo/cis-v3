<?php

namespace App\Interfaces\Hrim\EmployeeAttendance;

interface LeaveRecordsInterface
{
    public function index($request);
    public function validation($request);
    public function create($request);
    public function show($id, $request = []);
    public function update($id, $request);
    public function destroy($id);
    public function restore($id);
    public function getUserAvailableLeaves();
}
