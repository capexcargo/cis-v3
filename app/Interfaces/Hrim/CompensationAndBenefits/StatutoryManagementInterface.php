<?php

namespace App\Interfaces\Hrim\CompensationAndBenefits;

interface StatutoryManagementInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
}
