<?php

namespace App\Interfaces\Hrim\CompensationAndBenefits;

interface BenefitsManagementInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);

    public function destroy($id);
}
