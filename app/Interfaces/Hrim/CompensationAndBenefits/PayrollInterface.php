<?php

namespace App\Interfaces\Hrim\CompensationAndBenefits;

interface PayrollInterface
{
    public function index($request);
    public function index2($id, $request);
    public function show($request);
    public function createHistory($selected);
    public function generatePayslipInitial($selected);
    public function generatePayslipFinal($selected);
}
