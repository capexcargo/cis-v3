<?php

namespace App\Interfaces\Hrim\CompensationAndBenefits;

use App\Models\Hrim\LoansAndLedger;

interface LoansAndLedgerInterface
{
    public function index($request);
    public function index2($request);
    public function createValidation($request);
    public function create($request);
    public function show($id);
    public function updateValidation($request);
    public function update($id, $request);

    public function approve(LoansAndLedger $loans_and_ledger, $request, $approver);

    public function reasonValidation($request);
    public function decline(LoansAndLedger $loans_and_ledger, $request, $approver);

    public function destroy(LoansAndLedger $loans_and_ledger);
    public function show_schedule_payment($id);

    public function showForPrint($id, $emp_id);
}
