<?php

namespace App\Interfaces\Hrim\CompensationAndBenefits;

interface SalaryGradeManagementInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
}
