<?php

namespace App\Interfaces\Hrim;

interface LoaManagementInterface
{
    public function index($request);
    public function updateOrCreate($request);
    public function show($id, $request = []);
    public function checkLoaManagement($request);
}
