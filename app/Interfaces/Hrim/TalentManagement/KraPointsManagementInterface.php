<?php

namespace App\Interfaces\Hrim\TalentManagement;

use App\Models\Hrim\KraPointsManagement;

interface KraPointsManagementInterface
{
    public function index($request);
    public function createValidation($request);
    public function create($request);
    public function show($id);
    public function updateValidation($request);
    public function update($id, $request);

    public function destroy(KraPointsManagement $kra_points_management);

}