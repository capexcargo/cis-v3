<?php

namespace App\Interfaces\Hrim\TalentManagement;

use App\Models\Hrim\CoreValueManagement;

interface CoreValueManagementInterface
{
    public function index($request);
    public function createValidation($request);
    public function create($request);
    public function show($id);
    public function updateValidation($request);
    public function update($id, $request);

    public function destroy(CoreValueManagement $core_value_management);

}