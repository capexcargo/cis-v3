<?php

namespace App\Interfaces\Hrim\TalentManagement;

use App\Models\Hrim\CoreValueManagement;
use App\Models\Hrim\EvaluatorManagement;

interface EvaluatorManagementInterface
{
    public function show($id);
    public function updateValidation($request);
    public function update($id, $request);
}
