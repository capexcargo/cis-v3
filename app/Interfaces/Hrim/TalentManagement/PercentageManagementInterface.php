<?php

namespace App\Interfaces\Hrim\TalentManagement;


interface PercentageManagementInterface
{
    public function show($id);
    public function index($request);
    public function create($request);
    public function update($request, $id);
    public function destroy($id);
}
