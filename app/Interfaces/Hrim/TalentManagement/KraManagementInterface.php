<?php

namespace App\Interfaces\Hrim\TalentManagement;

use App\Models\Hrim\KraManagement;

interface KraManagementInterface
{
    public function index($request);
    public function createValidation($request);
    public function create($request);
    public function show($id);
    public function updateValidation($request);
    public function update($id, $request);

    public function destroy(KraManagement $kra_management);

}