<?php

namespace App\Interfaces\Hrim\TalentManagement;

use App\Models\Hrim\KpiManagement;

interface PerformanceEvaluationInterface
{
    public function save1stJustification($request);
    public function save2ndJustification($request);
    public function save3rdJustification($request);
    public function closeValidation($request);
    public function save($request);
    public function close($request);

    public function eIndex($request);
    public function eSaveValidation($request);
    public function eSave($request);

    public function eIndexMobile($request);
}
