<?php

namespace App\Interfaces\Hrim\TalentManagement;

use App\Models\Hrim\KpiManagement;

interface TrainingAndRefresherModulesInterface
{
    public function index($request);
    public function create($request);
    public function show($id);
    public function update($id, $request);
    public function destroy($id);
}
