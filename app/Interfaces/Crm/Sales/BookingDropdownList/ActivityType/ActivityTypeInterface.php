<?php

namespace App\Interfaces\Crm\Sales\BookingDropdownList\ActivityType;


interface ActivityTypeInterface
{
    public function index($request);
    public function createValidation($request);
    public function updateValidation($request);
    public function create($request);
    public function update($request, $id);
    // public function destroy($id);
}