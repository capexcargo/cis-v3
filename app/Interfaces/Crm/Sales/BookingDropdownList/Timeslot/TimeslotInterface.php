<?php

namespace App\Interfaces\Crm\Sales\BookingDropdownList\Timeslot;


interface TimeslotInterface
{
    public function index($request);
    public function create($request);
    public function createValidation($request);
    public function updateValidation($request);
    public function update($request, $id);
    public function destroy($id);
}