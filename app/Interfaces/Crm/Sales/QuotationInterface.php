<?php

namespace App\Interfaces\Crm\Sales;


interface QuotationInterface
{
    public function index($request);
    public function show($id, $from_sr);
    public function saveSrCreateQuotation($request);
    public function saveEditQuotation($request, $from_sr, $id);
    // public function showForPrint($id);
}
