<?php

namespace App\Interfaces\Crm\Sales;


interface RateCalculatorInterface
{
    public function compute($request);
    public function osCompute($request);
}