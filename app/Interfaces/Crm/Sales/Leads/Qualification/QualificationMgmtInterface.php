<?php

namespace App\Interfaces\Crm\Sales\Leads\Qualification;


interface QualificationMgmtInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
    public function destroy($id);
}