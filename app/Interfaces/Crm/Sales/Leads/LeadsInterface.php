<?php

namespace App\Interfaces\Crm\Sales\Leads;


interface LeadsInterface
{
    public function index($request);
    public function saveIfNotExisting();
    public function show($id);
    public function updateOrCreate($request);
    public function convert($request, $id);
    public function retire($request, $id);
}
