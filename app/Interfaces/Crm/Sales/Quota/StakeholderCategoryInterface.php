<?php

namespace App\Interfaces\Crm\Sales\Quota;


interface StakeholderCategoryInterface
{
    public function index($request);
    public function create($request);
    public function show($id);
    public function update($request, $id);
    public function report($request);
}
