<?php

namespace App\Interfaces\Crm\Sales\Quota;


interface TargetInterface
{
    public function index($request);
    public function updateOrCreate($type, $request);
    public function show($id);
}
