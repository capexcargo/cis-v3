<?php

namespace App\Interfaces\Crm\Sales\SalesCampaign;

interface SummaryInterface

{
    public function index($request);
    public function create($request);
    public function show($id);
    public function update($request, $id);
    public function delete($id);
}