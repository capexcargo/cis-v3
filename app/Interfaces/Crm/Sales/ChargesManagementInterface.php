<?php

namespace App\Interfaces\Crm\Sales;


interface ChargesManagementInterface
{
    public function index($request);
    public function create($request);
    public function createValidation($request);
    public function updateValidation($request, $id);
    public function show($id);
    public function update($request, $id);
    public function update_status($id, $status);
}
