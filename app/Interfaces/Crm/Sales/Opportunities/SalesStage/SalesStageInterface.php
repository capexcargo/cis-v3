<?php

namespace App\Interfaces\Crm\Sales\Opportunities\SalesStage;


interface SalesStageInterface
{
    public function index($request);
    public function createValidation($request);
    public function updateValidation($request);
    public function create($request);
    public function update($request, $id);
    // public function destroy($id);
}