<?php

namespace App\Interfaces\Crm\Sales\Opportunities;


interface OpportunitiesInterface
{
    public function index($request);
    public function show($request);
    public function update($request, $id);
    // public function destroy($id);
}