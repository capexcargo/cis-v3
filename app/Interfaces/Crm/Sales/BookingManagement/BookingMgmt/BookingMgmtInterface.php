<?php

namespace App\Interfaces\Crm\Sales\BookingManagement\BookingMgmt;
interface BookingMgmtInterface
{
    public function index($request, $search_request);
    public function index2($request, $search_request);
    public function createValidationTab1($request);
    public function createValidationTab2($request);
    public function createValidationTab3($request);
    public function createValidationTab4($request);
    public function create($request);
    public function cancelationValidation($request);
    public function bookcancel($request, $id);
    public function rescheduleValidation($request);
    public function bookresched($request, $id);
    public function editValidationTab1($request);
    public function editValidationTab2($request);
    public function editValidationTab3($request);
    public function editValidationTab4($request);
    public function update($request, $id);
    public function show($id);
    public function bookForConfirm($request, $id);
    public function bookConfirmed($request, $id);
    public function bookOngoing($request, $id);
    public function bookCompleted($request, $id);

    // public function updateValidationTab1($request);
    // public function updateValidationTab2($request);

}