<?php

namespace App\Interfaces\Crm\Contracts\AccountsApplication;


interface AccountsApplicationInterface
{
    public function index($request, $search_request);
    public function show($id);
    // public function create($request);
    // public function update($request, $id);
    public function destroy($id);
}