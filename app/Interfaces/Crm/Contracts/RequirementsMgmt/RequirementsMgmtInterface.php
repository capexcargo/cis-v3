<?php

namespace App\Interfaces\Crm\Contracts\RequirementsMgmt;


interface RequirementsMgmtInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
    // public function destroy($id);
}