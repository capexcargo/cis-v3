<?php

namespace App\Interfaces\Crm\CustomerInformation;


interface CustomerOnboardingInterface
{
    public function index($request);
    public function search($request);
    public function verifyNumber($request);
    public function updateCallStatus($request);
    public function retrieveCallStatus($request);
    
    public function create($request, $is_customer);
    public function update($request, $id);
    public function destroy($id);
    public function show($id);
    public function updateStatus($id, $status);
    // public function missedcall($request);
    // public function missedcallnotif($request);
}
