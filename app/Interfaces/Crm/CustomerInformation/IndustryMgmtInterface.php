<?php

namespace App\Interfaces\Crm\CustomerInformation;


interface IndustryMgmtInterface
{
    public function index($request);
    public function create($request);
    public function update($request, $id);
    public function updateValidation($request, $id);
    public function Validation($request);
    public function destroy($id);
}
