<?php

namespace App\Interfaces\Crm\CustomerInformation;


interface MarketingChannelInterface
{
    public function index($request);
    public function create($request);
    public function createValidation($request);
    public function updateValidation($request, $id);
    public function update($request, $id);
    public function destroy($id);

}