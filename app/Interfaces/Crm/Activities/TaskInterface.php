<?php

namespace App\Interfaces\Crm\Activities;


interface TaskInterface
{
    public function createValidation($request);
    public function create($request);
}
