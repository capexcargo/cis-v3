<?php

namespace App\Interfaces\Crm\Activities;


interface NotesInterface
{
    public function createValidation($request);
    public function create($request);
}
