<?php

namespace App\Interfaces\Crm\Activities;


interface LogMeetingInterface
{
    public function createValidation($request);
    public function create($request);
}
