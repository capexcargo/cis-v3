<?php

namespace App\Interfaces\Crm\ServiceRequest;


interface ServiceRequestMgmtInterface
{
    public function index($request);

    public function send($request);

    public function retrieveEmail();
    public function retrieveReplyEmail($sr_no);
    public function sendReply($sr_no, $body, $message_id);

    public function create($request);
    public function update($request, $id);
    public function updateSrDescription($request, $id);
    public function destroy($id);
    public function show($id);

    public function createLead($request);

    public function updateSrStatus($id);
}
