<?php

namespace App\Interfaces\Crm\ServiceRequest\Hierarchy;


interface HierarchyInterface
{
    public function index($request);
    // public function getposition($request);
    public function create($request);
    public function update($validated, $id);
    public function show($id);
    // public function destroy($id);

}