<?php

namespace App\Interfaces\Crm\ServiceRequest\SRType;


interface SRTypeInterface
{
    public function index($request);
    public function create($request);
    public function validation($request);
    public function validationUpdate($request, $id);
    public function update($request, $id);
    public function destroy($id);
}
