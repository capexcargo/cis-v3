<?php

namespace App\Interfaces\Crm\ServiceRequest\SalesCoach;

use App\Models\CrmSalesCoach;

// use App\Models\Hrim\Position;

interface SalesCoachInterface
{
    public function index($request);
    public function create($request);
    public function update($validated, $id);
    // public function show($id);

    // public function destroy(Position $position);
}
