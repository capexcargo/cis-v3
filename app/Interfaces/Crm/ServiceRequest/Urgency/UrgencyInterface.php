<?php

namespace App\Interfaces\Crm\ServiceRequest\Urgency;


interface UrgencyInterface
{
    public function index($request);
    public function create($request);
    public function show();
    // public function update($request, $id);
    // public function destroy($id);

}