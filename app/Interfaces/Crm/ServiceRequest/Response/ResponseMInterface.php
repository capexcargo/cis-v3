<?php

namespace App\Interfaces\Crm\ServiceRequest\Response;


interface ResponseMInterface
{
    public function index($request);
    public function create($request);
    public function validation($request);
    public function validationUpdate($request);
    public function update($request, $id);
    public function destroy($id);

}