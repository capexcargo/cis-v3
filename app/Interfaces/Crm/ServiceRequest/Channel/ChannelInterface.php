<?php

namespace App\Interfaces\Crm\ServiceRequest\Channel;


interface ChannelInterface
{
    public function index($request);
    public function create($request);
    public function validation($request);
    public function validationUpdate($request);
    public function update($request, $id);
    public function destroy($id);

}