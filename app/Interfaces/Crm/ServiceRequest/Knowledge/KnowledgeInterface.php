<?php

namespace App\Interfaces\Crm\ServiceRequest\Knowledge;


interface KnowledgeInterface
{
    public function index($request);
    public function getposition($request);
    public function create($request);
    public function update($validated, $id);
    public function show($id);
    // public function updatestatus($request, $id);

}