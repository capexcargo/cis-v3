<?php

namespace App\Interfaces\Crm\Reports;


interface SalesModuleReportsInterface
{
    public function index($request);
}
