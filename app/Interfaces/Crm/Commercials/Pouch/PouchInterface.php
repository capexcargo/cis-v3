<?php

namespace App\Interfaces\Crm\Commercials\Pouch;


interface PouchInterface
{
    public function index($request, $search_request);
    // public function search($request);
    public function createValidationTab1($request);
    public function createValidationTab2($request);
    public function createValidationTab1of1($request);
    public function createValidationTab2of2($request);
    public function createPouch($request);
    public function create($request);
    public function update($request, $id);
    public function updateValidationTab1($request);
    public function updateValidationTab2($request);
    // public function destroy($id);

}