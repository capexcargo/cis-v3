<?php

namespace App\Interfaces\Crm\Commercials\Ancillary;


interface AncillaryMgmtInterface
{
    public function index($request);
    public function createValidation($request);
    public function updateValidation($request, $id);
    public function create($request);
    public function update($request, $id);
}
