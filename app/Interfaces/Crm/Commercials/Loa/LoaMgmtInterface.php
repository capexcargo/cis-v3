<?php

namespace App\Interfaces\Crm\Commercials\Loa;


interface LoaMgmtInterface
{
    public function index($request, $search_request);
    public function create($request);
    public function createValidation($request);
    public function updateValidation($request);
    public function update($request, $id);
    // public function destroy($id);

}