<?php

namespace App\Interfaces\Accounting\Notifications;

use App\Models\Accounting\CheckVoucher;

interface CheckVoucherNotificationInterface
{
    public function create(CheckVoucher $check_voucher);
    public function finalApproved(CheckVoucher $check_voucher);
}