<?php

namespace App\Interfaces\Accounting\Notifications;

use App\Models\Accounting\RequestForPayment;

interface RequestForPaymentNotificationInterface
{
    public function create(RequestForPayment $request_for_payment);
    public function update(RequestForPayment $request_for_payment);
    public function approved(RequestForPayment $request_for_payment);
    public function finalApproved(RequestForPayment $request_for_payment);
}