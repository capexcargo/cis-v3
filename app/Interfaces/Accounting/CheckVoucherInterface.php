<?php

namespace App\Interfaces\Accounting;

use App\Models\Accounting\CashFlowDetails;
use App\Models\Accounting\CheckVoucher;
use App\Models\Accounting\RequestForPayment;

interface CheckVoucherInterface
{
    public function index($request);
    public function createOrUpdate(RequestForPayment $request_for_payment, $validated);
    public function approved(CheckVoucher $check_voucher, $validated);
    public function confirmation(CheckVoucher $check_voucher, $validated);

    public function updateStatusCashFlow(CashFlowDetails $cash_flow_details, $status_id);
    public function createOrUpdateCashFlowSnapshot($validated);
}
