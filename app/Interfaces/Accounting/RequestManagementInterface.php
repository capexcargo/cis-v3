<?php

namespace App\Interfaces\Accounting;

use App\Models\Accounting\RequestForPayment;

interface RequestManagementInterface
{
    public function createRequestForPayments($validated, $validated_details, $user_id);
    public function createFreights($validated, $validated_details, $user_id);
    public function createPayables($validated, $validated_details, $user_id);
    public function createCashAdvances($validated, $validated_details, $user_id);

    public function updateRequestForPayments(RequestForPayment $request_for_payment, $validated, $validated_details, $user_id);
    public function updateFreights(RequestForPayment $request_for_payment, $validated, $validated_details, $user_id);
    public function updatePayables(RequestForPayment $request_for_payment, $validated, $validated_details, $user_id);
    public function updateCashAdvances(RequestForPayment $request_for_payment, $validated, $validated_details, $user_id);

    public function attachment(RequestForPayment $request_for_payment, $attachments);
    public function approved(RequestForPayment $request_for_payment, $validated, $user_id, $level);
    public function untaggedPO(RequestForPayment $request_for_payment);
    public function destroy(RequestForPayment $request_for_payment);
    public function restore(RequestForPayment $request_for_payment);

}