<?php

namespace App\Interfaces\Accounting\SupplierManagement;

interface SupplierInterface
{
    public function create($validated);
    public function update($supplier, $validated);
    public function createItem($supplier, $validated);
    public function updateItem($supplier, $supplier_item, $validated);
    public function createIndustry($validated);
    public function updateIndustry($industry, $validated);
}