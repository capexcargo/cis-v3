<?php

namespace App\Interfaces\Accounting\BudgetManagement;

interface BudgetSourceInterface
{
    public function create($validated);
    public function update($budget_source, $validated);

    public function index2($request, $search_request);
    public function createValidation2($request);
    public function create2($request);
    public function updateValidation2($request);
    public function update2($request, $id);
}