<?php

namespace App\Interfaces\Accounting\BudgetManagement;

interface BudgetPlanInterface
{
    public function create($validated);
    public function update($budget_plan, $validated);
    public function transferBudget($budget_plan_from, $budget_plan_to, $validated, $validated_amount);
}
