<?php

namespace App\Interfaces\Accounting\BudgetManagement;

interface BudgetChartInterface
{
    public function create($validated);
    public function update($budget_chart, $validated);
}