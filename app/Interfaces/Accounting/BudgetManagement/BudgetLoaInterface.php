<?php

namespace App\Interfaces\Accounting\BudgetManagement;

interface BudgetLoaInterface
{
    public function create($validated);
    public function update($budget_loa, $validated);
}