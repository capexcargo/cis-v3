<?php

namespace App\Interfaces\Accounting;

interface LiquidationManagementInterface
{
    public function index($request);
    public function show($id);
    public function loadRequestForPaymentReference($request);
    public function showRequestForPaymentReference($id, $request);
    public function create($validated);
    public function update($id, $validated);
    public function updateStatus($id, $status_id);
}