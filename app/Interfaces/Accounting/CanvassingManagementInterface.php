<?php

namespace App\Interfaces\Accounting;

interface CanvassingManagementInterface
{
    public function create($validated, $user_id);
    public function update($canvassing, $validated);
    public function deleteSupplier($canvassing_supplier_id);
    public function deleteSupplierItem($canvassing_supplier_id, $canvassing_supplier_item_id);
    public function attachment($canvassing_supplier, $validated);
    public function create_waybill($waybill, $canvas_id);
}
