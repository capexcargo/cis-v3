<?php

namespace App\Interfaces\Accounting;

interface OpexTypeManagementInterface
{
    public function create($validated);
    public function update($opex_type, $validated);
}