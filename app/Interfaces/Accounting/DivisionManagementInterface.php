<?php

namespace App\Interfaces\Accounting;

interface DivisionManagementInterface
{
    public function create($validated);
    public function update($division, $validated);
}