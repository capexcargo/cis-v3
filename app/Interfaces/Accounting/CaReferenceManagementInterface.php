<?php

namespace App\Interfaces\Accounting;

interface CaReferenceManagementInterface
{
    public function create($validated);
    public function update($ca_reference, $validated);
}