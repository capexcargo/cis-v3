<?php

namespace App\Interfaces\Accounting;

interface LoaderManagementInterface
{
    public function create($request);
    public function update($id, $request);
    public function update2($code, $request);
    
}