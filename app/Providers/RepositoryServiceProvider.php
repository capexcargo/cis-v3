<?php

namespace App\Providers;

use App\Interfaces\Accounting\LoaderManagementInterface;
use App\Interfaces\Accounting\CaReferenceManagementInterface;
use App\Interfaces\Accounting\CheckVoucherInterface;
use App\Interfaces\Accounting\BudgetManagement\BudgetChartInterface;
use App\Interfaces\Accounting\BudgetManagement\BudgetLoaInterface;
use App\Interfaces\Accounting\BudgetManagement\BudgetPlanInterface;
use App\Interfaces\Accounting\BudgetManagement\BudgetSourceInterface;
use App\Interfaces\Accounting\CanvassingManagementInterface;
use App\Interfaces\Accounting\DivisionManagementInterface;
use App\Interfaces\Accounting\LiquidationManagementInterface;
use App\Interfaces\Accounting\Notifications\CheckVoucherNotificationInterface;
use App\Interfaces\Accounting\Notifications\RequestForPaymentNotificationInterface;
use App\Interfaces\Accounting\OpexTypeManagementInterface;
use App\Interfaces\Accounting\RequestManagementInterface;
use App\Interfaces\Accounting\SupplierManagement\SupplierInterface;
use App\Interfaces\Crm\Activities\LogMeetingInterface;
use App\Interfaces\Crm\Activities\NotesInterface;
use App\Interfaces\Crm\Activities\TaskInterface;
use App\Interfaces\Crm\Commercials\AirFreight\AirFreightInterface;
use App\Interfaces\Crm\Commercials\AirFreightPremium\AirFreightPremiumInterface;
use App\Interfaces\Crm\Commercials\Ancillary\AncillaryMgmtInterface;
use App\Interfaces\Crm\Commercials\Box\BoxInterface;
use App\Interfaces\Crm\Commercials\AncillaryDisplay\AncillaryDisplayMgmtInterface;
use App\Interfaces\Crm\Commercials\Crating\CratingInterface;
use App\Interfaces\Crm\Commercials\LandFreight\LandFreightInterface;
use App\Interfaces\Crm\Commercials\Loa\LoaMgmtInterface;
use App\Interfaces\Crm\Commercials\Pouch\PouchInterface;
use App\Interfaces\Crm\Commercials\SeaFreight\SeaFreightInterface;
use App\Interfaces\Crm\Commercials\Warehousing\WarehousingInterface;
use App\Interfaces\Crm\Contracts\AccountsApplication\AccountsApplicationInterface;
use App\Interfaces\Crm\Contracts\RequirementsMgmt\RequirementsMgmtInterface;
use App\Interfaces\Crm\CustomerInformation\CustomerOnboardingInterface;
use App\Interfaces\Crm\CustomerInformation\IndustryMgmtInterface;
use App\Interfaces\Crm\CustomerInformation\MarketingChannelInterface;
use App\Interfaces\Crm\Reports\CdmReportsInterface;
use App\Interfaces\Crm\Reports\SalesModuleReportsInterface;
use App\Interfaces\Crm\Reports\SRReports\SRReportsInterface;
use App\Interfaces\Crm\Sales\BookingDropdownList\ActivityType\ActivityTypeInterface;
use App\Interfaces\Crm\Sales\BookingDropdownList\Timeslot\TimeslotInterface;
use App\Interfaces\Crm\Sales\BookingManagement\BookingMgmt\BookingMgmtInterface;
use App\Interfaces\Crm\Sales\ChargesManagementInterface;
use App\Interfaces\Crm\Sales\Leads\LeadsInterface;
use App\Interfaces\Crm\Sales\Leads\Qualification\QualificationMgmtInterface;
use App\Interfaces\Crm\Sales\Opportunities\OpportunitiesInterface;
use App\Interfaces\Crm\Sales\Opportunities\OpportunityStatus\OpportunityStatusInterface;
use App\Interfaces\Crm\Sales\Opportunities\SalesStage\SalesStageInterface;
use App\Interfaces\Crm\Sales\Quota\StakeholderCategoryInterface;
use App\Interfaces\Crm\Sales\Quota\TargetInterface;
use App\Interfaces\Crm\Sales\SalesCampaign\SummaryInterface;
use App\Interfaces\Crm\Sales\QuotationInterface;
use App\Interfaces\Crm\Sales\RateCalculatorInterface;
use App\Interfaces\Crm\Sales\SalesCampaign\AudienceSegmentationInterface;
use App\Interfaces\Crm\ServiceRequest\Channel\ChannelInterface;
use App\Interfaces\Crm\ServiceRequest\Hierarchy\HierarchyInterface;
use App\Interfaces\Crm\ServiceRequest\Knowledge\KnowledgeInterface;
use App\Interfaces\Crm\ServiceRequest\Resolution\ResolutionInterface;
use App\Interfaces\Crm\ServiceRequest\Response\ResponseMInterface;
use App\Interfaces\Crm\ServiceRequest\SalesCoach\SalesCoachInterface;
use App\Interfaces\Crm\ServiceRequest\Service\ServiceInterface;
use App\Interfaces\Crm\ServiceRequest\ServiceRequestMgmtInterface;
use App\Interfaces\Crm\ServiceRequest\SRSub\SrSubCategoryInterface;
use App\Interfaces\Crm\ServiceRequest\SRType\SRTypeInterface;
use App\Interfaces\Crm\ServiceRequest\Urgency\UrgencyInterface;
use App\Interfaces\Fims\Accounting\AcctngPurchasingLoaInterface;
use App\Interfaces\Fims\Accounting\BudgetInterface;
use App\Interfaces\Fims\DataManagement\AcctngSupplierInterface;
use App\Interfaces\Fims\DataManagement\BankNameInterface;
use App\Interfaces\Fims\DataManagement\BudgetSRCInterface;
use App\Interfaces\Fims\DataManagement\ChartofAccountsInterface;
use App\Interfaces\Fims\DataManagement\IndustryInterface;
use App\Interfaces\Fims\DataManagement\ItemCategoryInterface;
use App\Interfaces\Fims\DataManagement\ItemDescriptionInterface;
use App\Interfaces\Fims\DataManagement\ManpowerInterface;
use App\Interfaces\Fims\DataManagement\OpexCategoryInterface;
use App\Interfaces\Fims\DataManagement\PurposeInterface;
use App\Interfaces\Fims\DataManagement\ReasonForRejectionInterface;
use App\Interfaces\Fims\DataManagement\ServiceCategoryInterface;
use App\Interfaces\Fims\DataManagement\ServiceDescriptionInterface;
use App\Interfaces\Fims\DataManagement\SubAccountsInterface;
use App\Interfaces\Fims\DataManagement\UnitInterface;
use App\Interfaces\Fims\ServicePurchaseRequisition\PurchaseReqInterface;
use App\Interfaces\Fims\ServicePurchaseRequisition\ServiceReqInterface;
use App\Interfaces\Globals\AccountManagementInterface;
use App\Interfaces\Globals\ForgotPasswordInterface;
use App\Interfaces\Hrim\Attendance\DailyTimeRecordsInterface;
use App\Interfaces\Hrim\Attendance\LeaveRecordsInterface;
use App\Interfaces\Hrim\Attendance\OvertimeRecordsInterface;
use App\Interfaces\Hrim\Attendance\ScheduleAdjustmentInterface;
use App\Interfaces\Hrim\Attendance\SchedulesInterface;
use App\Interfaces\Hrim\Attendance\TarInterface;
use App\Interfaces\Hrim\Attendance\TeamStatusInterface;
use App\Interfaces\Hrim\CompanyManagement\Bulletin\AnnouncementInterface;
use App\Interfaces\Hrim\CompanyManagement\Bulletin\MemoInterface;
use App\Interfaces\Hrim\CompanyManagement\CalendarInterface;
use App\Interfaces\Hrim\CompanyManagement\EmployerComplianceInterface;
use App\Interfaces\Hrim\CompensationAndBenefits\LoansAndLedgerInterface;
use App\Interfaces\Hrim\CompensationAndBenefits\BenefitsManagementInterface;
use App\Interfaces\Hrim\CompensationAndBenefits\PayrollInterface;
use App\Interfaces\Hrim\CompensationAndBenefits\SalaryGradeManagementInterface;
use App\Interfaces\Hrim\CompensationAndBenefits\StatutoryManagementInterface;
use App\Interfaces\Hrim\Csp\CspManagementInterface;
use App\Interfaces\Hrim\Csp\CspUserManagementInterface;
use App\Interfaces\Hrim\Dashboard\DashboardInterface;
use App\Interfaces\Hrim\EmployeeAttendance\DailyTimeRecordsInterface as EmployeeDailyTimeRecordsInterface;
use App\Interfaces\Hrim\EmployeeAttendance\LeaveRecordsInterface as EmployeeLeaveRecordsInterface;
use App\Interfaces\Hrim\EmployeeAttendance\OvertimeRecordsInterface as EmployeeOvertimeRecordsInterface;
use App\Interfaces\Hrim\EmployeeAttendance\ScheduleAdjustmentInterface as EmployeeScheduleAdjustmentInterface;
use App\Interfaces\Hrim\EmployeeAttendance\TarInterface as EmployeeTarInterface;
use App\Interfaces\Hrim\EmployeeManagement\CocSectionInterface;
use App\Interfaces\Hrim\EmployeeManagement\DisciplinaryHistoryInterface;
use App\Interfaces\Hrim\EmployeeManagement\DisciplinaryRecordInterface;
use App\Interfaces\Hrim\EmployeeManagement\EmployeeInformationInterface;
use App\Interfaces\Hrim\EmployeeManagement\EmploymentStatusManagementInterface;
use App\Interfaces\Hrim\EmployeeManagement\Hrim201FilesInterface;
use App\Interfaces\Hrim\EmployeeManagement\ResignationReasonInterface;
use App\Interfaces\Hrim\EmployeeManagement\ResignationReasonReferenceInterface;
use App\Interfaces\Hrim\EmployeeManagement\SanctionInterface;
use App\Interfaces\Hrim\EmployeeManagement\SanctionStatusInterface;
use App\Interfaces\Hrim\EmployeeManagement\ViolationInterface;
use App\Interfaces\Hrim\RecruitmentAndHiring\EmployeeRequisitionInterface;
use App\Interfaces\Hrim\LoaManagementInterface;
use App\Interfaces\Hrim\PayrollManagement\AdminAdjustmentInterface;
use App\Interfaces\Hrim\PayrollManagement\CutOffManagementInterface;
use App\Interfaces\Hrim\PayrollManagement\DlbManagementInterface;
use App\Interfaces\Hrim\PayrollManagement\HolidayManagementInterface;
use App\Interfaces\Hrim\PayrollManagement\LoaAdjustmentInterface;
use App\Interfaces\Hrim\PayrollManagement\Month13thPayInterface;
use App\Interfaces\Hrim\PayrollManagement\PagibigManagementInterface;
use App\Interfaces\Hrim\PayrollManagement\PhilhealthManagementInterface;
use App\Interfaces\Hrim\PayrollManagement\SssManagementInterface;
use App\Interfaces\Hrim\RecruitmentAndHiring\ApplicantStatusMgmtInterface;
use App\Interfaces\Hrim\RecruitmentAndHiring\ApplicantTrackingInterface;
use App\Interfaces\Hrim\RecruitmentAndHiring\EmploymentCategoryTypeInterface;
use App\Interfaces\Hrim\RecruitmentAndHiring\OnboardingInterface;
use App\Interfaces\Hrim\RecruitmentAndHiring\OnboardingStatusMgmtInterface;
use App\Interfaces\Hrim\Reports\LoansGovtDeductionInterface;
use App\Interfaces\Hrim\TalentManagement\CoreValueManagementInterface;
use App\Interfaces\Hrim\TalentManagement\EvaluatorManagementInterface;
use App\Interfaces\Hrim\TalentManagement\KpiManagementInterface;
use App\Interfaces\Hrim\TalentManagement\KpiTaggingInterface;
use App\Interfaces\Hrim\TalentManagement\KraManagementInterface;
use App\Interfaces\Hrim\TalentManagement\KraPointsManagementInterface;
use App\Interfaces\Hrim\TalentManagement\LeadershipCompetenciesMgmtInterface;
use App\Interfaces\Hrim\TalentManagement\PercentageManagementInterface;
use App\Interfaces\Hrim\TalentManagement\PerformanceEvaluationInterface;
use App\Interfaces\Hrim\TalentManagement\TrainingAndRefresherModulesInterface;
use App\Interfaces\Hrim\TimeInAndOutInterface;
use App\Interfaces\Hrim\Workforce\DepartmentInterface;
use App\Interfaces\Hrim\Workforce\JobLevelInterface;
use App\Interfaces\Hrim\Workforce\PositionInterface;
use App\Interfaces\Hrim\Workforce\TeamsInterface;
use App\Interfaces\Hrim\Workforce\WorkScheduleInterface;
use App\Interfaces\Oims\AreaMgmt\AreaMgmtInterface;
use App\Interfaces\Oims\DispatchControlHub\DispatchControlHubInterface;
use App\Interfaces\Oims\EDtr\EDtrInterface;
use App\Interfaces\Oims\QuadrantMgmt\QuadrantMgmtInterface;
use App\Interfaces\Oims\RouteCategory\RouteCategoryInterface;
use App\Interfaces\Oims\TeamMgmt\TeamMgmtInterface;
use App\Interfaces\Oims\TeamRouteAssignment\TeamRouteAssignmentInterface;
use App\Interfaces\Oims\TEDropdownMgmt\BarangayMgmt\BarangayMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\BranchMgmt\BranchMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\CargoMgmt\CargoMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\DangerousMgmt\DangerousMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\MunicipalityMgmt\MunicipalityMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\OdaopaMgmt\OdaopaMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\PackagingMgmt\PackagingMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\PaymodeMgmt\PaymodeMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\ProvinceMgmt\ProvinceMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\ReasonMgmt\ReasonMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\RemarksMgmt\RemarksMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\ServiceabilityMgmt\ServiceabilityMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\ServiceAreaMgmt\ServiceAreaMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\ServiceMgmt\ServiceMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\TEDropdownMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\TransportMgmt\TransportMgmtInterface;
use App\Interfaces\Oims\TEDropdownMgmt\ZipcodeMgmt\ZipcodeMgmtInterface;
use App\Interfaces\Oims\TransactionEntry\TransactionEntryInterface;
use App\Interfaces\Oims\WaybillRegistry\WaybillRegistryInterface;
use App\Interfaces\TemplateInterface;
use App\Interfaces\Ticketing\TicketingCategoryMangementInterface;
use App\Interfaces\Ticketing\TicketingSubcategoryManagementInterface;
use App\Interfaces\Ticketing\TicketManagementInterface;
use App\Repositories\Accounting\LoaderManagementRepository;
use App\Repositories\Accounting\CaReferenceManagementRepository;
use App\Repositories\Accounting\CheckVoucherRepository;
use App\Repositories\Accounting\BudgetManagement\BudgetChartRepository;
use App\Repositories\Accounting\BudgetManagement\BudgetLoaRepository;
use App\Repositories\Accounting\BudgetManagement\BudgetPlanRepository;
use App\Repositories\Accounting\BudgetManagement\BudgetSourceRepository;
use App\Repositories\Accounting\CanvassingManagementRepository;
use App\Repositories\Accounting\DivisionManagementRepository;
use App\Repositories\Accounting\LiquidationManagementRepository;
use App\Repositories\Accounting\Notifications\CheckVoucherNotificationRepository;
use App\Repositories\Accounting\Notifications\RequestForPaymentNotificationRepository;
use App\Repositories\Accounting\OpexTypeManagementRepository;
use App\Repositories\Accounting\RequestManagementRepository;
use App\Repositories\Accounting\SupplierManagement\SupplierRepository;
use App\Repositories\Crm\Activities\LogMeetingRepository;
use App\Repositories\Crm\Activities\NotesRepository;
use App\Repositories\Crm\Activities\TaskRepository;
use App\Repositories\Crm\Commercials\AirFreight\AirFreightRepository;
use App\Repositories\Crm\Commercials\AirFreightPremium\AirFreightPremiumRepository;
use App\Repositories\Crm\Commercials\Loa\LoaMgmtRepository;
use App\Repositories\Crm\Commercials\Ancillary\AncillaryMgmtRepository;
use App\Repositories\Crm\Commercials\Box\BoxRepository;
use App\Repositories\Crm\Commercials\AncillaryDisplay\AncillaryDisplayMgmtRepository;
use App\Repositories\Crm\Commercials\Crating\CratingRepository;
use App\Repositories\Crm\Commercials\LandFreight\LandFreightRepository;
use App\Repositories\Crm\Commercials\Pouch\PouchRepository;
use App\Repositories\Crm\Commercials\SeaFreight\SeaFreightRepository;
use App\Repositories\Crm\Commercials\Warehousing\WarehousingRepository;
use App\Repositories\Crm\Contracts\AccountsApplication\AccountsApplicationRepository;
use App\Repositories\Crm\Contracts\RequirementsMgmt\RequirementsMgmtRepository;
use App\Repositories\Crm\CustomerInformation\CustomerOnboardingRepository;
use App\Repositories\Crm\CustomerInformation\IndustryMgmtRepository;
use App\Repositories\Crm\CustomerInformation\MarketingChannelRepository;
use App\Repositories\Crm\Reports\CdmReportsRepository;
use App\Repositories\Crm\Reports\SalesModuleReportsRepository;
use App\Repositories\Crm\Reports\SRReports\SRReportsRepository;
use App\Repositories\Crm\Sales\BookingDropdownList\ActivityType\ActivityTypeRepository;
use App\Repositories\Crm\Sales\BookingDropdownList\Timeslot\TimeslotRepository;
use App\Repositories\Crm\Sales\BookingManagement\BookingMgmt\BookingMgmtRepository;
use App\Repositories\Crm\Sales\ChargesManagementRepository;
use App\Repositories\Crm\Sales\Leads\LeadsRepository;
use App\Repositories\Crm\Sales\Leads\Qualification\QualificationMgmtRepository;
use App\Repositories\Crm\Sales\Opportunities\OpportunitiesRepository;
use App\Repositories\Crm\Sales\Opportunities\OpportunityStatus\OpportunityStatusRepository;
use App\Repositories\Crm\Sales\Opportunities\SalesStage\SalesStageRepository;
use App\Repositories\Crm\Sales\Quota\StakeholderCategoryRepository;
use App\Repositories\Crm\Sales\Quota\TargetRepository;
use App\Repositories\Crm\Sales\SalesCampaign\SummaryRepository;
use App\Repositories\Crm\Sales\QuotationRepository;
use App\Repositories\Crm\Sales\RateCalculatorRepository;
use App\Repositories\Crm\Sales\SalesCampaign\AudienceSegmentationRepository;
use App\Repositories\Crm\ServiceRequest\Channel\ChannelRepository;
use App\Repositories\Crm\ServiceRequest\Hierarchy\HierarchyRepository;
use App\Repositories\Crm\ServiceRequest\Knowledge\KnowledgeRepository;
use App\Repositories\Crm\ServiceRequest\Resolution\ResolutionRepository;
use App\Repositories\Crm\ServiceRequest\Response\ResponseMRepository;
use App\Repositories\Crm\ServiceRequest\SalesCoach\SalesCoachRepository;
use App\Repositories\Crm\ServiceRequest\Service\ServiceRepository;
use App\Repositories\Crm\ServiceRequest\ServiceRequestMgmtRepository;
use App\Repositories\Crm\ServiceRequest\SrSub\SrSubRepository;
use App\Repositories\Crm\ServiceRequest\SRType\SRTypeRepository;
use App\Repositories\Crm\ServiceRequest\Urgency\UrgencyRepository;
use App\Repositories\Fims\Accounting\AcctngPurchasingLoaRepository;
use App\Repositories\Fims\Accounting\BudgetRepository;
use App\Repositories\Fims\DataManagement\AcctngSupplierRepository;
use App\Repositories\Fims\DataManagement\BankNameRepository;
use App\Repositories\Fims\DataManagement\BudgetSRCRepository;
use App\Repositories\Fims\DataManagement\ChartofAccountsRepository;
use App\Repositories\Fims\DataManagement\IndustryRepository;
use App\Repositories\Fims\DataManagement\ItemCategoryRepository;
use App\Repositories\Fims\DataManagement\ItemDescriptionRepository;
use App\Repositories\Fims\DataManagement\ManpowerRepository;
use App\Repositories\Fims\DataManagement\OpexCategoryRepository;
use App\Repositories\Fims\DataManagement\PurposeRepository;
use App\Repositories\Fims\DataManagement\ReasonForRejectionRepository;
use App\Repositories\Fims\DataManagement\ServiceCategoryRepository;
use App\Repositories\Fims\DataManagement\ServiceDescriptionRepository;
use App\Repositories\Fims\DataManagement\SubAccountsRepository;
use App\Repositories\Fims\DataManagement\UnitRepository;
use App\Repositories\Fims\ServicePurchaseRequisition\PurchaseReqRepository;
use App\Repositories\Fims\ServicePurchaseRequisition\ServiceReqRepository;
use App\Repositories\Hrim\Attendance\DailyTimeRecordsRepository;
use App\Repositories\Globals\AccountManagementRepository;
use App\Repositories\Globals\ForgotPasswordRepository;
use App\Repositories\Hrim\Attendance\LeaveRecordsRepository;
use App\Repositories\Hrim\Attendance\OvertimeRecordsRepository;
use App\Repositories\Hrim\Attendance\ScheduleAdjustmentRepository;
use App\Repositories\Hrim\Attendance\SchedulesRepository;
use App\Repositories\Hrim\Attendance\TarRepository;
use App\Repositories\Hrim\Attendance\TeamStatusRepository;
use App\Repositories\Hrim\CompanyManagement\Bulletin\AnnouncementRepository;
use App\Repositories\Hrim\CompanyManagement\Bulletin\MemoRepository;
use App\Repositories\Hrim\CompanyManagement\CalendarRepository;
use App\Repositories\Hrim\CompanyManagement\EmployerComplianceRepository;
use App\Repositories\Hrim\CompensationAndBenefits\LoansAndLedgerRepository;
use App\Repositories\Hrim\CompensationAndBenefits\BenefitsManagementRepository;
use App\Repositories\Hrim\CompensationAndBenefits\PayrollRepository;
use App\Repositories\Hrim\CompensationAndBenefits\SalaryGradeManagementRepository;
use App\Repositories\Hrim\CompensationAndBenefits\StatutoryManagementRepository;
use App\Repositories\Hrim\EmployeeAttendance\DailyTimeRecordsRepository as EmployeeDailyTimeRecordsRepository;
use App\Repositories\Hrim\EmployeeAttendance\LeaveRecordsRepository as EmployeeLeaveRecordsRepository;
use App\Repositories\Hrim\EmployeeAttendance\OvertimeRecordsRepository as EmployeeOvertimeRecordsRepository;
use App\Repositories\Hrim\EmployeeAttendance\ScheduleAdjustmentRepository as EmployeeScheduleAdjustmentRepository;
use App\Repositories\Hrim\EmployeeAttendance\TarRepository as EmployeeTarRepository;
use App\Repositories\Hrim\EmployeeManagement\CocSectionRepository;
use App\Repositories\Hrim\EmployeeManagement\DisciplinaryHistoryRepository;
use App\Repositories\Hrim\EmployeeManagement\DisciplinaryRecordRepository;
use App\Repositories\Hrim\EmployeeManagement\EmployeeInformationRepository;
use App\Repositories\Hrim\EmployeeManagement\Hrim201FilesRepository;
use App\Repositories\Hrim\EmployeeManagement\ResignationReasonRepository;
use App\Repositories\Hrim\EmployeeManagement\SanctionRepository;
use App\Repositories\Hrim\EmployeeManagement\SanctionStatusRepository;
use App\Repositories\Hrim\EmployeeManagement\ViolationRepository;
use App\Repositories\Hrim\RecruitmentAndHiring\EmployeeRequisitionRepository;
use App\Repositories\Hrim\LoaManagementRepository;
use App\Repositories\Hrim\Csp\CspManagementRepository;
use App\Repositories\Hrim\Csp\CspUserManagementRepository;
use App\Repositories\Hrim\Dashboard\DashboardRepository;
use App\Repositories\Hrim\EmployeeManagement\EmploymentStatusManagementRepository;
use App\Repositories\Hrim\EmployeeManagement\ResignationReasonReferenceRepository;
use App\Repositories\Hrim\PayrollManagement\AdminAdjustmentRepository;
use App\Repositories\Hrim\PayrollManagement\CutOffMgmtReposirtory;
use App\Repositories\Hrim\PayrollManagement\DlbManagementRepository;
use App\Repositories\Hrim\PayrollManagement\HolidayManagementRepository;
use App\Repositories\Hrim\PayrollManagement\LoaAdjustmentRepository;
use App\Repositories\Hrim\PayrollManagement\Month13thPayRepository;
use App\Repositories\Hrim\PayrollManagement\PagibigManagementRepository;
use App\Repositories\Hrim\PayrollManagement\PhilhealthManagementRepository;
use App\Repositories\Hrim\PayrollManagement\SssManagementRepository;
use App\Repositories\Hrim\RecruitmentAndHiring\ApplicantStatusMgmtRepository;
use App\Repositories\Hrim\RecruitmentAndHiring\ApplicantTrackingRepository;
use App\Repositories\Hrim\RecruitmentAndHiring\EmploymentCategoryTypeRepository;
use App\Repositories\Hrim\RecruitmentAndHiring\OnboardingRepository;
use App\Repositories\Hrim\RecruitmentAndHiring\OnboardingStatusMgmtRepository;
use App\Repositories\Hrim\Reports\LoansGovtDeductionRepository;
use App\Repositories\Hrim\TalentManagement\CoreValueManagementRepository;
use App\Repositories\Hrim\TalentManagement\EvaluatorManagementRepository;
use App\Repositories\Hrim\TalentManagement\KpiManagementRepository;
use App\Repositories\Hrim\TalentManagement\KpiTaggingRepository;
use App\Repositories\Hrim\TalentManagement\KraManagementRepository;
use App\Repositories\Hrim\TalentManagement\KraPointsManagementRepository;
use App\Repositories\Hrim\TalentManagement\LeadershipCompetenciesMgmtRepository;
use App\Repositories\Hrim\TalentManagement\PercentageManagementRepository;
use App\Repositories\Hrim\TalentManagement\PerformanceEvaluationRepository;
use App\Repositories\Hrim\TalentManagement\TrainingAndRefresherModulesRepository;
use App\Repositories\Hrim\TimeInAndOutRepository;
use App\Repositories\Hrim\Workforce\DepartmentRepository;
use App\Repositories\Hrim\Workforce\JobLevelRepository;
use App\Repositories\Hrim\Workforce\PositionRepository;
use App\Repositories\Hrim\Workforce\TeamsRepository;
use App\Repositories\Hrim\Workforce\WorkScheduleRepository;
use App\Repositories\Oims\AreaMgmt\AreaMgmtRepository;
use App\Repositories\Oims\DispatchControlHub\DispatchControlHubRepository;
use App\Repositories\Oims\EDtr\EDtrRepository;
use App\Repositories\Oims\QuadrantMgmt\QuadrantMgmtRepository;
use App\Repositories\Oims\RouteCategory\RouteCategoryRepository;
use App\Repositories\Oims\TeamMgmt\TeamMgmtRepository;
use App\Repositories\Oims\TeamRouteAssignment\TeamRouteAssignmentRepository;
use App\Repositories\Oims\TEDropdownMgmt\BarangayMgmt\BarangayMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\BranchMgmt\BranchMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\CargoMgmt\CargoMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\DangerousMgmt\DangerousMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\MunicipalityMgmt\MunicipalityMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\OdaopaMgmt\OdaopaMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\PackagingMgmt\PackagingMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\PaymodeMgmt\PaymodeMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\ProvinceMgmt\ProvinceMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\ReasonMgmt\ReasonMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\RemarksMgmt\RemarksMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\ServiceabilityMgmt\ServiceabilityMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\ServiceAreaMgmt\ServiceAreaMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\ServiceMgmt\ServiceMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\TEDropdownMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\TransportMgmt\TransportMgmtRepository;
use App\Repositories\Oims\TEDropdownMgmt\ZipcodeMgmt\ZipcodeMgmtRepository;
use App\Repositories\Oims\TransactionEntry\TransactionEntryRepository;
use App\Repositories\Oims\WaybillRegistry\WaybillRegistryRepository;
use App\Repositories\TemplateRepository;
use App\Repositories\Ticketing\TicketingCategoryManagementRepository;
use App\Repositories\Ticketing\TicketingSubcategoryManagementRepository;
use App\Repositories\Ticketing\TicketManagementRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        /*
        |--------------------------------------------------------------------------
        | Global
        |--------------------------------------------------------------------------
        */
        $this->app->bind(
            TemplateInterface::class,
            TemplateRepository::class
        );

        $this->app->bind(
            AccountManagementInterface::class,
            AccountManagementRepository::class
        );

        /*
        |--------------------------------------------------------------------------
        | Notifications
        |--------------------------------------------------------------------------
        */
        $this->app->bind(
            RequestForPaymentNotificationInterface::class,
            RequestForPaymentNotificationRepository::class
        );

        $this->app->bind(
            CheckVoucherNotificationInterface::class,
            CheckVoucherNotificationRepository::class
        );

        /*
        |--------------------------------------------------------------------------
        | Accounting
        |--------------------------------------------------------------------------
        */
        $this->app->bind(
            RequestManagementInterface::class,
            RequestManagementRepository::class
        );

        $this->app->bind(
            CanvassingManagementInterface::class,
            CanvassingManagementRepository::class
        );

        // Accounting Budget Management
        $this->app->bind(
            BudgetSourceInterface::class,
            BudgetSourceRepository::class
        );

        $this->app->bind(
            BudgetChartInterface::class,
            BudgetChartRepository::class
        );

        $this->app->bind(
            BudgetPlanInterface::class,
            BudgetPlanRepository::class
        );

        $this->app->bind(
            BudgetLoaInterface::class,
            BudgetLoaRepository::class
        );

        // Accounting Supplier Management
        $this->app->bind(
            SupplierInterface::class,
            SupplierRepository::class
        );

        // Accounting Opex Type Management
        $this->app->bind(
            OpexTypeManagementInterface::class,
            OpexTypeManagementRepository::class
        );

        // Accounting Division Management
        $this->app->bind(
            DivisionManagementInterface::class,
            DivisionManagementRepository::class
        );

        // Check Voucher Management
        $this->app->bind(
            CheckVoucherInterface::class,
            CheckVoucherRepository::class
        );

        // Liquidation Management
        $this->app->bind(
            LiquidationManagementInterface::class,
            LiquidationManagementRepository::class
        );

        // CA Reference Management
        $this->app->bind(
            CaReferenceManagementInterface::class,
            CaReferenceManagementRepository::class
        );

        // Loader Management
        $this->app->bind(
            LoaderManagementInterface::class,
            LoaderManagementRepository::class
        );

        /*
        |--------------------------------------------------------------------------
        | Hrim
        |--------------------------------------------------------------------------
        */

        // Time In / Out
        $this->app->bind(
            TimeInAndOutInterface::class,
            TimeInAndOutRepository::class
        );

        // Company Management
        $this->app->bind(
            EmployerComplianceInterface::class,
            EmployerComplianceRepository::class
        );

        $this->app->bind(
            CalendarInterface::class,
            CalendarRepository::class
        );

        $this->app->bind(
            AnnouncementInterface::class,
            AnnouncementRepository::class
        );

        $this->app->bind(
            MemoInterface::class,
            MemoRepository::class
        );

        // Employee Management
        $this->app->bind(
            EmployeeInformationInterface::class,
            EmployeeInformationRepository::class
        );

        $this->app->bind(
            ResignationReasonInterface::class,
            ResignationReasonRepository::class
        );

        $this->app->bind(
            Hrim201FilesInterface::class,
            Hrim201FilesRepository::class
        );
        $this->app->bind(
            EmploymentStatusManagementInterface::class,
            EmploymentStatusManagementRepository::class
        );

        $this->app->bind(
            ResignationReasonReferenceInterface::class,
            ResignationReasonReferenceRepository::class
        );

        // Workforce
        $this->app->bind(
            WorkScheduleInterface::class,
            WorkScheduleRepository::class
        );

        $this->app->bind(
            TeamsInterface::class,
            TeamsRepository::class
        );

        $this->app->bind(
            DepartmentInterface::class,
            DepartmentRepository::class
        );

        $this->app->bind(
            JobLevelInterface::class,
            JobLevelRepository::class
        );

        $this->app->bind(
            PositionInterface::class,
            PositionRepository::class
        );

        //Attendance
        $this->app->bind(
            DailyTimeRecordsInterface::class,
            DailyTimeRecordsRepository::class
        );

        $this->app->bind(
            LeaveRecordsInterface::class,
            LeaveRecordsRepository::class
        );

        $this->app->bind(
            OvertimeRecordsInterface::class,
            OvertimeRecordsRepository::class
        );

        $this->app->bind(
            SchedulesInterface::class,
            SchedulesRepository::class
        );

        $this->app->bind(
            TarInterface::class,
            TarRepository::class
        );

        $this->app->bind(
            ScheduleAdjustmentInterface::class,
            ScheduleAdjustmentRepository::class
        );

        //Employee Attendance
        $this->app->bind(
            EmployeeDailyTimeRecordsInterface::class,
            EmployeeDailyTimeRecordsRepository::class
        );

        $this->app->bind(
            EmployeeTarInterface::class,
            EmployeeTarRepository::class
        );

        $this->app->bind(
            EmployeeLeaveRecordsInterface::class,
            EmployeeLeaveRecordsRepository::class
        );

        $this->app->bind(
            EmployeeOvertimeRecordsInterface::class,
            EmployeeOvertimeRecordsRepository::class
        );

        $this->app->bind(
            EmployeeScheduleAdjustmentInterface::class,
            EmployeeScheduleAdjustmentRepository::class
        );

        // Loa Management
        $this->app->bind(
            LoaManagementInterface::class,
            LoaManagementRepository::class
        );

        // Disciplinary Record
        $this->app->bind(
            CocSectionInterface::class,
            CocSectionRepository::class
        );

        $this->app->bind(
            ViolationInterface::class,
            ViolationRepository::class
        );

        $this->app->bind(
            DisciplinaryRecordInterface::class,
            DisciplinaryRecordRepository::class
        );

        $this->app->bind(
            SanctionInterface::class,
            SanctionRepository::class
        );

        $this->app->bind(
            SanctionStatusInterface::class,
            SanctionStatusRepository::class
        );

        // Disciplinary History

        $this->app->bind(
            DisciplinaryHistoryInterface::class,
            DisciplinaryHistoryRepository::class
        );


        //Recruitment and Hiring
        //Employee Requisition

        $this->app->bind(
            EmployeeRequisitionInterface::class,
            EmployeeRequisitionRepository::class
        );

        // Applicant Status Management
        $this->app->bind(
            ApplicantStatusMgmtInterface::class,
            ApplicantStatusMgmtRepository::class
        );

        // Applicant Tracking
        $this->app->bind(
            ApplicantTrackingInterface::class,
            ApplicantTrackingRepository::class
        );

        // Onboarding
        $this->app->bind(
            OnboardingInterface::class,
            OnboardingRepository::class
        );

        // Onboarding Status Management
        $this->app->bind(
            OnboardingStatusMgmtInterface::class,
            OnboardingStatusMgmtRepository::class
        );


        // Employment Category Type Management
        $this->app->bind(
            EmploymentCategoryTypeInterface::class,
            EmploymentCategoryTypeRepository::class
        );

        // Talent Management
        // Kra Management
        $this->app->bind(
            KraManagementInterface::class,
            KraManagementRepository::class
        );

        // Kra Points Management
        $this->app->bind(
            KraPointsManagementInterface::class,
            KraPointsManagementRepository::class
        );

        // Kpi Management
        $this->app->bind(
            KpiManagementInterface::class,
            KpiManagementRepository::class
        );

        // Kpi Tagging
        $this->app->bind(
            KpiTaggingInterface::class,
            KpiTaggingRepository::class
        );

        // Core Value Management
        $this->app->bind(
            CoreValueManagementInterface::class,
            CoreValueManagementRepository::class
        );

        // Evaluator Management
        $this->app->bind(
            EvaluatorManagementInterface::class,
            EvaluatorManagementRepository::class
        );

        // Leadership Competencies Management
        $this->app->bind(
            LeadershipCompetenciesMgmtInterface::class,
            LeadershipCompetenciesMgmtRepository::class
        );

        // Talent Management
        $this->app->bind(
            PerformanceEvaluationInterface::class,
            PerformanceEvaluationRepository::class
        );

        $this->app->bind(
            TrainingAndRefresherModulesInterface::class,
            TrainingAndRefresherModulesRepository::class
        );

        // Leadership Competencies Management
        $this->app->bind(
            PercentageManagementInterface::class,
            PercentageManagementRepository::class
        );

        // Holiday Management
        $this->app->bind(
            HolidayManagementInterface::class,
            HolidayManagementRepository::class
        );
        // SSS Management
        $this->app->bind(
            SssManagementInterface::class,
            SssManagementRepository::class
        );
        // Philhealth Management
        $this->app->bind(
            PhilhealthManagementInterface::class,
            PhilhealthManagementRepository::class
        );
        // Pagibig Management
        $this->app->bind(
            PagibigManagementInterface::class,
            PagibigManagementRepository::class
        );

        // Dlb Management
        $this->app->bind(
            DlbManagementInterface::class,
            DlbManagementRepository::class
        );

        // Cut-off Management
        $this->app->bind(
            CutOffManagementInterface::class,
            CutOffMgmtReposirtory::class
        );

        // 13th Month Pay
        $this->app->bind(
            Month13thPayInterface::class,
            Month13thPayRepository::class
        );

        // Admin Adjustment
        $this->app->bind(
            AdminAdjustmentInterface::class,
            AdminAdjustmentRepository::class
        );

        // Loa Adjustment Management
        $this->app->bind(
            LoaAdjustmentInterface::class,
            LoaAdjustmentRepository::class
        );

        // Loans And Ledger
        $this->app->bind(
            LoansAndLedgerInterface::class,
            LoansAndLedgerRepository::class
        );

        // Salary Grade Management
        $this->app->bind(
            SalaryGradeManagementInterface::class,
            SalaryGradeManagementRepository::class
        );

        // Benefits Management
        $this->app->bind(
            BenefitsManagementInterface::class,
            BenefitsManagementRepository::class
        );

        // Statutory Management
        $this->app->bind(
            StatutoryManagementInterface::class,
            StatutoryManagementRepository::class
        );

        // CSP Management
        $this->app->bind(
            CspManagementInterface::class,
            CspManagementRepository::class
        );

        // Reports
        $this->app->bind(
            LoansGovtDeductionInterface::class,
            LoansGovtDeductionRepository::class
        );

        // Payroll
        $this->app->bind(
            PayrollInterface::class,
            PayrollRepository::class
        );

        // Dashboard
        $this->app->bind(
            DashboardInterface::class,
            DashboardRepository::class
        );

        // CSP User Management
        $this->app->bind(
            CspUserManagementInterface::class,
            CspUserManagementRepository::class
        );

        // Team Status
        $this->app->bind(
            TeamStatusInterface::class,
            TeamStatusRepository::class
        );


        // Ticket Category Management
        $this->app->bind(
            TicketingCategoryMangementInterface::class,
            TicketingCategoryManagementRepository::class
        );

        // Ticket Subcategory Management
        $this->app->bind(
            TicketingSubcategoryManagementInterface::class,
            TicketingSubcategoryManagementRepository::class
        );


        // Ticket Management
        $this->app->bind(
            TicketManagementInterface::class,
            TicketManagementRepository::class
        );

        // Forgot Password
        $this->app->bind(
            ForgotPasswordInterface::class,
            ForgotPasswordRepository::class
        );

        /*
        |--------------------------------------------------------------------------
        | Crm
        |--------------------------------------------------------------------------
        */

        // Marketing Channel

        $this->app->bind(
            MarketingChannelInterface::class,
            MarketingChannelRepository::class
        );

        // Industry Mgmt

        $this->app->bind(
            IndustryMgmtInterface::class,
            IndustryMgmtRepository::class
        );

        // Sales Campaign

        $this->app->bind(
            SummaryInterface::class,
            SummaryRepository::class
        );

        //Audience Segmentation Management

        $this->app->bind(
            AudienceSegmentationInterface::class,
            AudienceSegmentationRepository::class
        );

        /*
        |--------------------------------------------------------------------------
        | Service Request
        |--------------------------------------------------------------------------
        */

        // SR TYPE

        $this->app->bind(
            SRTypeInterface::class,
            SRTypeRepository::class
        );

        //  SR SUB

        $this->app->bind(
            SrSubCategoryInterface::class,
            SrSubRepository::class
        );

        //  MILESTONE RESPONSE

        $this->app->bind(
            ResponseMInterface::class,
            ResponseMRepository::class
        );

        //  MILESTONE RESOLUTION

        $this->app->bind(
            ResolutionInterface::class,
            ResolutionRepository::class
        );

        //  URGENCY AND IMPACT

        $this->app->bind(
            UrgencyInterface::class,
            UrgencyRepository::class
        );

        //  HIERARCHY

        $this->app->bind(
            HierarchyInterface::class,
            HierarchyRepository::class
        );

        //  CHANNEL SR SOURCE

        $this->app->bind(
            ChannelInterface::class,
            ChannelRepository::class
        );

        //  SERVICE REQUEST

        $this->app->bind(
            ServiceInterface::class,
            ServiceRepository::class
        );

        //  KNOWLEDGE

        $this->app->bind(
            KnowledgeInterface::class,
            KnowledgeRepository::class
        );

        //  SALES COACH

        $this->app->bind(
            SalesCoachInterface::class,
            SalesCoachRepository::class
        );

        //  CUSTOMER INFORMATION

        $this->app->bind(
            CustomerOnboardingInterface::class,
            CustomerOnboardingRepository::class
        );


        //  SERVICE REQUEST MANAGEMENT

        $this->app->bind(
            ServiceRequestMgmtInterface::class,
            ServiceRequestMgmtRepository::class
        );

        //  SALES - RATE CALCULATOR

        $this->app->bind(
            RateCalculatorInterface::class,
            RateCalculatorRepository::class
        );
        
        //  SALES - RATE CALCULATOR - CHARGES MANAGEMENT

        $this->app->bind(
            ChargesManagementInterface::class,
            ChargesManagementRepository::class
        );

        //  SALES - LEADS

        $this->app->bind(
            LeadsInterface::class,
            LeadsRepository::class
        );

        //  SALES - OPPORTUNITIES

        $this->app->bind(
            OpportunitiesInterface::class,
            OpportunitiesRepository::class
        );

        //  SALES - QUOTA - STAKEHOLDER CATEGORY
        $this->app->bind(
            StakeholderCategoryInterface::class,
            StakeholderCategoryRepository::class
        );

        //  SALES - QUOTA - TARGET
        $this->app->bind(
            TargetInterface::class,
            TargetRepository::class
        );

        //  SALES - QUOTATION
        $this->app->bind(
            QuotationInterface::class,
            QuotationRepository::class
        );

        //  CRM LOA MANAGEMENT

        $this->app->bind(
            LoaMgmtInterface::class,
            LoaMgmtRepository::class
        );

        //  Pouch MANAGEMENT

        $this->app->bind(
            PouchInterface::class,
            PouchRepository::class
        );

        //  Ancillary Display MANAGEMENT

        $this->app->bind(
            AncillaryDisplayMgmtInterface::class,
            AncillaryDisplayMgmtRepository::class
        );

        //  Ancillary MANAGEMENT

        $this->app->bind(
            AncillaryMgmtInterface::class,
            AncillaryMgmtRepository::class
        );

        //  BOX MANAGEMENT

        $this->app->bind(
            BoxInterface::class,
            BoxRepository::class
        );

        //  CRATING MANAGEMENT

        $this->app->bind(
            CratingInterface::class,
            CratingRepository::class
        );

        //  WAREHOUSING MANAGEMENT

        $this->app->bind(
            WarehousingInterface::class,
            WarehousingRepository::class
        );

        //  LAND FREIGHT

        $this->app->bind(
            LandFreightInterface::class,
            LandFreightRepository::class
        );

        //  AIR FREIGHT

        $this->app->bind(
            AirFreightInterface::class,
            AirFreightRepository::class
        );

        //  AIR FREIGHT PREMIUM

        $this->app->bind(
            AirFreightPremiumInterface::class,
            AirFreightPremiumRepository::class
        );

        //  SEA FREIGHT

        $this->app->bind(
            SeaFreightInterface::class,
            SeaFreightRepository::class
        );

        //  QUALIFICATION MGMT

        $this->app->bind(
            QualificationMgmtInterface::class,
            QualificationMgmtRepository::class
        );

        //  OPPORTUNITY STATUS

        $this->app->bind(
            OpportunityStatusInterface::class,
            OpportunityStatusRepository::class
        );

        //  SALES STAGE

        $this->app->bind(
            SalesStageInterface::class,
            SalesStageRepository::class
        );

        //  ACTIVITY TYPE

        $this->app->bind(
            ActivityTypeInterface::class,
            ActivityTypeRepository::class
        );

        //  TIME SLOT

        $this->app->bind(
            TimeslotInterface::class,
            TimeslotRepository::class
        );

        //  BOOKING MANAGEMENT

        $this->app->bind(
            BookingMgmtInterface::class,
            BookingMgmtRepository::class
        );

        //  TASK

        $this->app->bind(
            TaskInterface::class,
            TaskRepository::class
        );

        //  NOTES

        $this->app->bind(
            NotesInterface::class,
            NotesRepository::class
        );

        //  LOG MEETING

        $this->app->bind(
            LogMeetingInterface::class,
            LogMeetingRepository::class
        );

        //  APPLICATION REQUIREMENTS MANAGEMENT

        $this->app->bind(
            RequirementsMgmtInterface::class,
            RequirementsMgmtRepository::class
        );

        //  ACCOUNTS APPLICATION

        $this->app->bind(
            AccountsApplicationInterface::class,
            AccountsApplicationRepository::class
        );

        //  REPORTS
        $this->app->bind(
            CdmReportsInterface::class,
            CdmReportsRepository::class
        );

        $this->app->bind(
            SalesModuleReportsInterface::class,
            SalesModuleReportsRepository::class
        );

        //  SR REPORTS

        //  $this->app->bind(
        //     SRReportsInterface::class,
        //     SRReportsRepository::class
        // );

         /*
        |--------------------------------------------------------------------------
        | Oims
        |--------------------------------------------------------------------------
        */

        $this->app->bind(
            RouteCategoryInterface::class,
            RouteCategoryRepository::class
        );

        $this->app->bind(
            AreaMgmtInterface::class,
            AreaMgmtRepository::class
        );

        $this->app->bind(
            QuadrantMgmtInterface::class,
            QuadrantMgmtRepository::class
        );

        $this->app->bind(
            TeamMgmtInterface::class,
            TeamMgmtRepository::class
        );

        $this->app->bind(
            TEDropdownMgmtInterface::class,
            TEDropdownMgmtRepository::class
        );

        $this->app->bind(
            BranchMgmtInterface::class,
            BranchMgmtRepository::class
        );

        $this->app->bind(
            TransportMgmtInterface::class,
            TransportMgmtRepository::class
        );

        $this->app->bind(
            PaymodeMgmtInterface::class,
            PaymodeMgmtRepository::class
        );

        $this->app->bind(
            ServiceMgmtInterface::class,
            ServiceMgmtRepository::class
        );

        $this->app->bind(
            OdaopaMgmtInterface::class,
            OdaopaMgmtRepository::class
        );

        $this->app->bind(
            ServiceAreaMgmtInterface::class,
            ServiceAreaMgmtRepository::class
        );

        $this->app->bind(
            ServiceabilityMgmtInterface::class,
            ServiceabilityMgmtRepository::class
        );

        $this->app->bind(
            ZipcodeMgmtInterface::class,
            ZipcodeMgmtRepository::class
        );

        $this->app->bind(
            BarangayMgmtInterface::class,
            BarangayMgmtRepository::class
        );

        $this->app->bind(
            MunicipalityMgmtInterface::class,
            MunicipalityMgmtRepository::class
        );

        $this->app->bind(
            ProvinceMgmtInterface::class,
            ProvinceMgmtRepository::class
        );

        $this->app->bind(
            DangerousMgmtInterface::class,
            DangerousMgmtRepository::class
        );

        $this->app->bind(
            PackagingMgmtInterface::class,
            PackagingMgmtRepository::class
        );

        $this->app->bind(
            ReasonMgmtInterface::class,
            ReasonMgmtRepository::class
        );

        $this->app->bind(
            CargoMgmtInterface::class,
            CargoMgmtRepository::class
        );

        $this->app->bind(
            RemarksMgmtInterface::class,
            RemarksMgmtRepository::class
        );

        $this->app->bind(
            TeamRouteAssignmentInterface::class,
            TeamRouteAssignmentRepository::class
        );

        $this->app->bind(
            WaybillRegistryInterface::class,
            WaybillRegistryRepository::class
        );

        $this->app->bind(
            DispatchControlHubInterface::class,
            DispatchControlHubRepository::class
        );

        $this->app->bind(
            EDtrInterface::class,
            EDtrRepository::class
        );

         /*
        |--------------------------------------------------------------------------
        | Fims
        |--------------------------------------------------------------------------
        */

        $this->app->bind(
            BudgetSRCInterface::class,
            BudgetSRCRepository::class
        );

        $this->app->bind(
            OpexCategoryInterface::class,
            OpexCategoryRepository::class
        );

        $this->app->bind(
            ChartofAccountsInterface::class,
            ChartofAccountsRepository::class
        );

        $this->app->bind(
            SubAccountsInterface::class,
            SubAccountsRepository::class
        );

        $this->app->bind(
            ReasonForRejectionInterface::class,
            ReasonForRejectionRepository::class
        );

        $this->app->bind(
            PurposeInterface::class,
            PurposeRepository::class
        );

        $this->app->bind(
            UnitInterface::class,
            UnitRepository::class
        );

        $this->app->bind(
            ItemCategoryInterface::class,
            ItemCategoryRepository::class
        );

        $this->app->bind(
            ServiceCategoryInterface::class,
            ServiceCategoryRepository::class
        );

        $this->app->bind(
            ItemDescriptionInterface::class,
            ItemDescriptionRepository::class
        );

        $this->app->bind(
            ServiceDescriptionInterface::class,
            ServiceDescriptionRepository::class
        );

        $this->app->bind(
            IndustryInterface::class,
            IndustryRepository::class
        );

        $this->app->bind(
            BankNameInterface::class,
            BankNameRepository::class
        );

        $this->app->bind(
            ManpowerInterface::class,
            ManpowerRepository::class
        );

        $this->app->bind(
            AcctngSupplierInterface::class,
            AcctngSupplierRepository::class
        );

        $this->app->bind(
            AcctngPurchasingLoaInterface::class,
            AcctngPurchasingLoaRepository::class
        );

        $this->app->bind(
            TransactionEntryInterface::class,
            TransactionEntryRepository::class
        );

        $this->app->bind(
            BudgetInterface::class,
            BudgetRepository::class
        );

        $this->app->bind(
            ServiceReqInterface::class,
            ServiceReqRepository::class
        );

        $this->app->bind(
            PurchaseReqInterface::class,
            PurchaseReqRepository::class
        );
    }

}
