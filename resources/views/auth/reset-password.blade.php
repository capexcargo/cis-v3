{{-- <x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $request->route('token') }}">

            <div class="block">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email', $request->email)" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            <div class="flex items-center justify-end mt-4">
                <x-jet-button>
                    {{ __('Reset Password') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout> --}}

@extends('layouts/guest')
@section('content')
    <div class="flex items-center h-screen">
        <div class="w-1/4 ml-24 space-y-6">
            <div class="flex flex-col text-4xl font-semibold text-center text-gray-800">
                <span>CaPEx</span>
                <span>Integrated System</span>
            </div>
            <div class="flex flex-col text-xl font-medium text-left text-gray-800">
                <span>Reset Password</span>
            </div>
            <div>
                <x-jet-validation-errors class="mb-4" />
                <form method="POST" action="{{ route('password.update') }}">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="space-y-2">
                        <label for="email" class="text-base font-medium text-gray-700">Email</label>
                        <input type="email" id="email" name="email"
                            class="block w-full px-3 py-5 bg-gray-200 border rounded-md" placeholder="Email"
                            value="{{ $email }}" required autofocus>
                    </div>
                    <div class="mt-4 space-y-2">
                        <label for="password" class="text-base font-medium text-gray-700">Password</label>
                        <input id="password" class="block w-full px-3 py-5  border rounded-md" type="password"
                            name="password" required autocomplete="new-password">
                    </div>

                    <div class="mt-4 space-y-2">
                        <label for="password_confirmation" class="text-base font-medium text-gray-700">Confirm
                            Password</label>
                        <input id="password_confirmation" class="block w-full px-3 py-5  border rounded-md" type="password"
                            name="password_confirmation" required autocomplete="new-password">
                    </div>

                    <div class="flex items-center justify-end mt-8">
                        <button type="submit" class="w-full bg-[#003399] rounded-md py-4 text-white text-lg">Reset
                            Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
