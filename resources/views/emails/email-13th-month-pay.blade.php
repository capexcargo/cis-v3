{{-- <div
    style="padding-left: 1rem;padding-right: 1rem; border: 1px solid #ccc; border-color: #ccc; border-radius: 0.375rem;box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);">
    @foreach ($month_13th as $payroll)
        <div style="display: flex;justify-content: space-between;gap: 0.75rem;margin-top: 1rem;display: flex;">
            <div>
                <div style="font-size: 24px; font-weight: 600;">
                    {{ $payroll->first_name . ' ' . $payroll->last_name . "'s " }}13th Month Payslip</div>
            </div>
            <div style="display: inline-block;">
                <table style="min-width: 100%;font-weight: 500;border-top-width: 2px;border-color: #e2e8f0;">
                    <tr>
                        <td style="margin-right: 0.5rem; font-size: 0.75rem; color: #718096;">Cutoff Period:</td>
                        <td style="white-space: nowrap;font-weight: 600;white-space: nowrap;">
                            {{ $payroll->cut_off == 1 ? 'January - June' : 'July - December' }},
                            {{ $year }}
                        </td>
                    </tr>
                </table>
            </div>
            <div style="display: flex;justify-content: flex-end; gap: 1rem; margin-top: 1.5rem;"></div>
        </div>
        <div style="flex justify-end gap-4 mt-6 md:flex"></div>
        <div
            style="margin-bottom: 1rem;border-radius: 1rem;box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);position: relative;">
            <div
                style="border-left: 0; border-right: 0;display: flex;justify-content: space-between;gap: 1rem;margin-top: 1.5rem;border-width: 2px; border-style: solid;border-bottom-width: 0;border-color: rgba(156, 163, 175, var(--tw-border-opacity));">
            </div>
        </div>
    @endforeach
</div> --}}
{{ $body_header }}
