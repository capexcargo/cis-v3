<!DOCTYPE html>
<html>

<head>
    <style type="text/css">
        body {
            font-family: 'Poppins', sans-serif;
            margin-top: -20px;
            color: #3A3A3A;
        }
    </style>
</head>

<body>
    <div style="border-bottom: 1.3px solid #3A3A3A; display: flex; justify-content: space-between; gap: 0.75rem;">
        <div style="display: grid; grid-template-columns: 1fr 1fr; gap: 1rem;">
            <div>
                <table style="width: 100%;">
                    <tr style="border-bottom: 1.3px solid #3A3A3A">
                        <td style="font-weight: semi-bold;">
                            <div style="padding:1rem;margin-top:1rem">
                                <img src="https://capex.com.ph/app/uploads/2020/07/CaPEx-Logo.png" height="65"
                                    width="130">
                            </div>
                        </td>
                        <td style="font-weight: semi-bold;">
                            <div style="text-align: center; margin-left:-25%">
                                <div style="font-size:20px;">13th Month Payslip</div>
                                <div>
                                    Cargo Padala Express Forwarding Services Corp.
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div style="padding-left: 1rem;padding-right: 1rem;">
        @foreach ($month_13th as $payroll)
            <div style="display: flex;justify-content: space-between;gap: 0.75rem;margin-top: 1rem;display: flex;">
                <div>
                    <div style="font-weight: semi-bold;">
                        {{ $payroll->first_name . ' ' . $payroll->last_name }}</div>
                </div>
                <div>
                    <div style="white-space: nowrap; font-size: 14px;">
                        Cutoff Period : <span> {{ $payroll->cut_off == 1 ? 'January - June' : 'July - December' }},
                            {{ $year }}</span>
                    </div>
                </div>
                <div style="display: flex;justify-content: flex-end; gap: 1rem; margin-top: 1.5rem;"></div>
            </div>
            <div>
                <div
                    style="padding-left: 1rem; padding-right: 1rem; border: 1.3px solid #ccc; border-color: #ccc; border-radius: 0.375rem; box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);">
                    <div style="margin-top: .8rem; display: grid; grid-template-columns: 1fr 1fr; gap: 1rem;">
                        <div>
                            <table style="width: 100%;">
                                <tr>
                                    <td>
                                        <div style="color: #003399; font-size: 14px; font-weight: semi-bold;">Earnings
                                        </div>
                                    </td>
                                    <td style="border-left:1.8px dashed #acacac; padding-left:1rem">
                                        <div style="color: #003399; font-size: 14px; font-weight: semi-bold;">Deductions
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: semi-bold;">
                                        <div style="font-size: 14px;">
                                            <table style="width: 100%;">
                                                <tr style="border-bottom: 1.3px solid #3A3A3A;">
                                                    <td style="font-size:12px">Basic Pay :
                                                    </td>
                                                    <td style="font-weight: semi-bold;">
                                                        {{ 'P' . number_format($payroll->basic_pay) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span></span></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td><span></span></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td><span></span></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td
                                        style="font-weight: semi-bold;border-left:1.8px dashed #acacac; padding-left:1rem">
                                        <div style="font-size: 14px;">
                                            <table style="width: 100%;">
                                                <tr style="border-bottom: 1.3px solid #3A3A3A;">
                                                    <td style="font-size:12px">Leave W/O Pay :
                                                    </td>
                                                    <td style="font-weight: semi-bold;">
                                                        {{ 'P' . number_format(round($payroll->leaveswtpay * ($payroll->basic_pay / 26))) }}
                                                    </td>
                                                </tr>
                                                <tr style="border-bottom: 1.3px solid #3A3A3A;">
                                                    <td style="font-size:12px">Tardiness :
                                                    </td>
                                                    <td style="font-weight: semi-bold;">
                                                        {{ 'P' . number_format($payroll->tardiness) }}
                                                    </td>
                                                </tr>
                                                <tr style="border-bottom: 1.3px solid #3A3A3A;">
                                                    <td style="font-size:12px">Undertime :
                                                    </td>
                                                    <td style="font-weight: semi-bold;">
                                                        {{ 'P' . number_format($payroll->undertime) }}
                                                    </td>
                                                </tr>
                                                <tr style="border-bottom: 1.3px solid #3A3A3A;">
                                                    <td style="font-size:12px">Absences :
                                                    </td>
                                                    <td style="font-weight: semi-bold;">
                                                        {{ 'P' . number_format($payroll->absentcount * ($payroll->basic_pay / 26)) }}
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div style="border-bottom: 1.8px solid #acacac;"></div>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="font-weight: semi-bold;">
                                        <div style="font-size: 14px;">
                                            <table style="width: 55%;">
                                                <tr style="border-bottom: 1.3px solid #3A3A3A;">
                                                    <td style="font-size:12px">
                                                        Total Basic Pay
                                                    </td>
                                                    <td style="font-weight: semi-bold; color: #003399;">
                                                        <span style="position:absolute; left:26%">
                                                            {{ 'P' . number_format($payroll->basic_pay * 6) }}
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td style="font-weight: semi-bold;">
                                        <div style="font-size: 14px;">
                                            <table style="width: 100%;">
                                                <tr style="border-bottom: 1.3px solid #3A3A3A;">
                                                    <td style="font-size:12px">
                                                        <span style="position: absolute; left: 47%; top:29.7%;">Total
                                                            Deductions</span>
                                                    </td>
                                                    <td style="font-weight: semi-bold; color: #003399;">
                                                        <span style="position:absolute; right:15%; top:29.7%;">
                                                            {{ 'P' . number_format($payroll->tardiness + $payroll->undertime + $payroll->absentcount * ($payroll->basic_pay / 26)) }}
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div
                        style="border-top: 1.8px solid #acacac; display: flex; justify-content: space-between; gap: 0.75rem;">
                        <div style="margin-top: 1.5rem; display: grid; grid-template-columns: 1fr 1fr; gap: 1rem;">
                            <div>
                                <table style="width: 100%;">
                                    <tr style="border-bottom: 1.3px solid #3A3A3A">
                                        <td style="font-size:14px;">Total Basic Pay Earned For
                                            The
                                            Month
                                            Of {{ $payroll->cut_off == 1 ? 'January - June' : 'July - December' }},
                                            {{ $year }} : <span
                                                style="font-weight: semi-bold; color: #003399; font-size: 16px;">{{ 'P' . number_format($payroll->basic_pay * 6 - ($payroll->tardiness + $payroll->undertime + $payroll->absentcount * ($payroll->basic_pay / 26))) }}</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div style="margin-top: 1.5rem; display: flex; gap: 1rem;">
                        <table style="width: 100%;">
                            <tr style="border-bottom: 1.3px solid #3A3A3A">
                                <td>
                                    <div style="text-align:center;">
                                        <div style="font-weight: semi-bold; font-size: 16px;">
                                            {{ 'P' . number_format($payroll->basic_pay * 6 - ($payroll->tardiness + $payroll->undertime + round($payroll->absentcount * ($payroll->basic_pay / 26)))) }}
                                        </div>
                                        <div style="border-top:1.3px solid #3A3A3A;"></div>
                                        <div style="font-weight: semi-bold;font-size: 14px;">12 Months
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div style="margin-left:20px; font-weight: semi-bold;">
                                        =</div>
                                </td>
                                <td>
                                    <div style="width:50%;text-align:center">
                                        <div style="font-weight: semi-bold; font-size: 18px">
                                            {{ 'P' . number_format((($payroll->basic_pay / 2) * 6 - ($payroll->tardiness + $payroll->undertime + round($payroll->absentcount * ($payroll->basic_pay / 26), 2))) / 6, 2) }}
                                        </div>
                                        <div style="font-size:14px;">Is the
                                            Proportionate
                                            13th
                                            Month Pay For The Month Of
                                            {{ $payroll->cut_off == 1 ? 'January - June' : 'July - December' }},
                                            {{ $year }} </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</body>

</html>
