<div dir="ltr">
    Hello, <span style="font-weight: 600">{!! $welcome_name !!}</span>
    <br><br>
    Welcome to <b>CaPEx<b>! We are thrilled to have you on board. At <b>CaPEx<b>, we take pride in our commitment to delivering top-notch logistics services, and we are excited to embark on this journey with you.
    <br><br>
    Thank you for choosing us as your logistics partner. As you settle in, we encourage you to explore our range of services tailored to meet your unique needs. Browse through our website <b>(www.capex.com.ph)</b> or visit our Facebook <b>(www.facebook.com/CaPExCargoPadalaExpress)</b> and TikTok <b>(www.tiktok.com/@cargopadalaexpress)</b> pages.
    Whether you're looking for efficient transportation or secure warehousing solutions, we've got you covered.
    <br><br>
    If you have any questions or need assistance, please don't hesitate to email <b>support@capex.com.ph</b> or call us at <b>(02) 8396 8888</b>. Our team is here to ensure a seamless experience for you.
    <br><br>
    Here's to a successful and collaborative journey with <b>CaPEx<b>!
    <br><br>
    Best regards,
    <br>
    <div class="mt-6">
        <table width="100%">
            <tbody>
                <tr>
                    <td width="200"><a href="https://capex.com.ph"> <img
                                src="https://capex.com.ph/app/uploads/2020/07/CaPEx-Logo.png" width="180" /> </a></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div style="border-top: 1px solid #E5E7EB">
                            <p style="font-family: Poppins; font-size: 9px; padding: 6px 0 6px 0;">Disclaimer: "The
                                information in this
                                electronic message, including any file(s) transmitted with it, is legally privileged and
                                confidential, intended only for the sole use of the individual or entity named as
                                addressee and recipient hereof. If you are not the addressee indicated in this message
                                (or responsible for the delivery of the message to such person) or you have received
                                this message by mistake; you are prohibited to copy, use, disseminate or deliver this
                                message. In such case, you should immediately delete this e-mail from your system and
                                notify the sender by e-mail reply."
                            </p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    {{-- @livewire('crm.service-request.service-request-mgmt.email-signature') --}}
</div>
