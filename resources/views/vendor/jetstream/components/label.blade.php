@props(['value', 'required' => false])

<label {{ $attributes->merge(['class' => 'block font-bold text-xs text-[#003399]']) }}>
    {{ $value ?? $slot }}
    @if ($required)
        <span class="text-red">*</span>
    @endif
</label>
