<div x-show="create_modal" x-cloak @keydown.escape.window="create_modal = false"
    class="fixed inset-0 z-40 w-full bg-gray-900 bg-opacity-60">
    <div class="flex items-center justify-center h-screen py-10 overflow-auto">
        <div class="w-11/12 p-4 m-auto bg-white rounded-md shadow-md xs:w-11/12 sm:w-11/12 md:w-2/4"
            @click.away="create_modal = false">
            <livewire:admin.account-management.create />
        </div>
    </div>
</div>
