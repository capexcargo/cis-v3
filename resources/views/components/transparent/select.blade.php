<div class="relative z-0 w-full mb-5">
    <select {{ $attributes }}
        class="block w-full px-0 pt-2 pb-1 pl-2 mt-0 bg-transparent border-0 border-b-2 border-gray-600 focus:border-[#003399] appearance-none z-1 focus:outline-none focus:ring-0 ">
        {{ $slot }}
    </select>
    <label class="absolute text-gray-500 duration-300 top-3 -z-1 origin-0 whitespace-nowrap">
        <span>{{ $label }}</span>
        @if ($required)
            <span class="text-red">*</span>
        @endif
    </label>
</div>
