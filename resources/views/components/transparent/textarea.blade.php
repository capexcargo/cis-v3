<div class="relative z-0 w-full mb-5">
    <textarea {{ $attributes }} placeholder=""
        class="pt-2 pl-2 pb-1 block w-full mt-0 bg-transparent border-0 border-b-2 appearance-none focus:outline-none focus:ring-0 border-gray-600 focus:border-[#003399]" ></textarea>
    <label for="email" class="absolute text-gray-500 duration-300 top-3 -z-1 origin-0">
        <span>{{ $label }}</span>
        @if ($required)
            <span class="text-red">*</span>
        @endif
    </label>
</div>