<th {{ $attributes->merge(['class' => 'p-3 tracking-wider whitespace-nowrap']) }} scope="col">
    <span class="flex justify-between font-medium">
        {{ $name }}
        {{ $sort ?? '' }}
    </span>
</th>
