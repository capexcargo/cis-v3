<div class="mb-2 overflow-auto rounded-lg">
    <div class="inline-block min-w-full align-middle">
        <table {{ $attributes->merge(['class' => 'min-w-full font-medium divide-y-2']) }}>
            <thead class="text-xs text-gray-700 capitalize bg-white border-0 cursor-pointer">
                <tr>
                    {{ $thead ?? '' }}
                </tr>
            </thead>
            <thead class="text-xs text-gray-700 capitalize bg-white border-0 cursor-pointer">
                <tr>
                    {{ $thead1 ?? '' }}
                </tr>
            </thead>
            <tbody class="text-sm bg-gray-100 border-0 divide-y-4 divide-white">
                {{ $tbody ?? '' }}
            </tbody>

            <tbody class="text-xs text-gray-700 capitalize bg-white border-0 cursor-pointer">
                {{ $tbody1 ?? '' }}
            </tbody>
        </table>
    </div>
</div>
