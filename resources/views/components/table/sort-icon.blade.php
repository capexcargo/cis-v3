<span>
    <span class="flex">
        <svg class="{{ $field == $sortField && $sortAsc ? 'text-gray-900' : '' }} w-3 h-5 -mr-2"
            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7l4-4m0 0l4 4m-4-4v18" />
        </svg>
        <svg class="{{ $field == $sortField && !$sortAsc ? 'text-gray-900' : '' }} w-3 h-5"
            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 17l-4 4m0 0l-4-4m4 4V3" />
        </svg>
    </span>
</span>
