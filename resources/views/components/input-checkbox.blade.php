@props(['label', 'name'])
<div>
    <label class="inline-flex items-center space-x-3">
        <input {{ $attributes }} type="checkbox" class="p-1 border border-[#003399] rounded text-blue focus:ring-0"
            name="{{ $name }}" id="{{ $name }}">
        <label class="text-sm gray-300">{{ $label }}</label>
    </label>
</div>
