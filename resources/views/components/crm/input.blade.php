<input {{ $attributes }} name="{{ $name }}" id="{{ $name }}"
    class="text-gray-600 block w-full border border-gray-500 py-[2px] px-2 rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50 @error($name) border-red-600 @enderror">
