@props(['name'])
<div class="relative mt-1 rounded-md shadow-sm">
    @if ($icon_left ?? false)
        <div class="absolute inset-y-0 left-0 flex items-center pl-2 pointer-events-none">
            {{ $icon_left }}
        </div>
    @endif
    <input {{ $attributes }}
        class="@if ($icon_left ?? false) pl-8 @endif @if ($icon_right ?? false) pr-8 @endif block w-full p-[5px] shadow-sm border-gray-500 rounded-md focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50  @error($name) border-red-600 @enderror">
    @if ($icon_right ?? false)
        <div class="absolute inset-y-0 right-0 flex items-center pr-2">
            {{ $icon_right }}
        </div>
    @endif
</div>
