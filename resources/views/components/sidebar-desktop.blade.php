<aside class="z-20 flex-shrink-0 hidden overflow-y-auto bg-white w-72 md:block" x-show="isSideMenuOpenDesktop"
    x-transition:enter="transition-all transform ease-in-out duration-500" x-transition:enter-start="opacity-0 -ml-64"
    x-transition:enter-end="opacity-100 ml-0" x-transition:leave="transition-all transform ease-in-out duration-500"
    x-transition:leave-start="opacity-100 ml-0" x-transition:leave-end="opacity-0 -ml-64">
    <div class="py-2 text-gray-500 ">
        <div class="py-2 bg-white">
            <a href="/dashboard" class="">
                <img class="mx-auto h-14 w-100" src="
                /images/logo/capex-logo.png" alt="VeMoBro_logo">
            </a>
        </div>

        @livewire('all.sidebar')
    </div>
</aside>
