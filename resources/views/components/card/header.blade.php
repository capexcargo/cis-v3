<div {{ $attributes }}
    class="flex flex-col items-center justify-center px-3 py-5 text-gray-700 bg-white  border-blue-800 border-solid rounded-md "
    :class="$wire.{{ $card['action'] }} == '{{$card['id']}}' ? 'border-[3px]' : 'border'">
    <div class="flex items-center justify-between w-full">
        <span class="flex-1 text-base font-medium uppercase">{{ $card['title'] }}</span>
        <span class="flex-1 text-2xl font-semibold text-right">{{ $card['value'] }}</span>
    </div>
</div>
