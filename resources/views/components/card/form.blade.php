<div {{ $attributes }} class="p-6 mb-8 bg-white shadow-md rounded-lg">
    {{ $slot }}
</div>
