@for ($i = 1; $i <= $count; $i++)
    <div class="w-full transition-all duration-500 ease-out delay-75 transform animate-pulse">
        <div
            class="flex flex-row justify-center p-3 space-x-3 duration-500 transform bg-white shadow-md cursor-pointer rounded-3xl hover:scale-105">
            <div class="flex flex-col items-center space-y-1">
                <span class="w-5 h-10 bg-gray-500 "></span>
                <span class="w-12 h-4 bg-gray-300"></span>
            </div>
            <div class="flex items-center justify-center">
                @if ($icon)
                    <span class="w-10 h-10 bg-gray-300"></span>
                @endif
            </div>
        </div>
    </div>
@endfor
