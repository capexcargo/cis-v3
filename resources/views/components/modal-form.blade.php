<div {{ $attributes }} class="px-6 pb-6 text-gray-800">
    {{ $loading ?? '' }}
    {{ $modals ?? '' }}
    <div class="mt-6 space-y-6">
        @if ($header_card ?? false)
            <div class="grid grid-cols-2 gap-6 md:grid-cols-5 lg:grid-cols-5">
                {{ $header_card ?? '' }}
            </div>
        @endif
        @if ($search_form ?? false)
            <div x-cloak x-show="search_form">
                <div class="grid grid-cols-2 gap-3 sm:grid-cols-2 md:grid-cols-5 lg:grid-cols-5 xl:grid-cols-5">
                    {{ $search_form ?? '' }}
                </div>
            </div>
        @endif
        {{ $body ?? '' }}
    </div>
</div>
