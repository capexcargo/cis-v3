@props(['value', 'required' => false])

<label {{ $attributes->merge(['class' => 'block font-medium text-sm text-[#003399] mb-1']) }}>
    {{ $value ?? $slot }}
    @if ($required)
        <span class="text-red">*</span>
        @else
        <i class="text-gray-400 text-1xs">
            {{-- (Optional) --}}
        </i>
    @endif
</label>
