@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.csp.csp-user-management.index')
        </div>
    </div>
@endsection
