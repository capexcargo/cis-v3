@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.csp.onboarding.index')
        </div>
    </div>
@endsection
