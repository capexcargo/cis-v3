@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.talent-management.performance-evaluation.view', ['id' => $id,'quarter' => $quarter,'years' => $years])
        </div>
    </div>
@endsection
