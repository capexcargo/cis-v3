@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.talent-management.training-and-refresher-modules.index')
        </div>
    </div>
@endsection