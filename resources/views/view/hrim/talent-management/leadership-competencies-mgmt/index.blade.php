@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.talent-management.leadership-competencies-mgmt.index')
        </div>
    </div>
@endsection