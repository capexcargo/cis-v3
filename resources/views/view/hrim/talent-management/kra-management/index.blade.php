@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.talent-management.kra-management.index')
        </div>
    </div>
@endsection