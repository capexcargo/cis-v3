@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.employee-talent-management.performance-evaluation.view-evaluation', ['id' => $id,'quarter' => $quarter,'year' => $year])
        </div>
    </div>
@endsection
