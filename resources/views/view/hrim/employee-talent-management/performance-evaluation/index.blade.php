@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.employee-talent-management.performance-evaluation.index')
        </div>
    </div>
@endsection
