@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.payroll-management.admin-adjustment.index')
        </div>
    </div>
@endsection
