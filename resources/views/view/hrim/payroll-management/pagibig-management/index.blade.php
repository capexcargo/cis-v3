@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.payroll-management.pagibig-management.index')
        </div>
    </div>
@endsection
