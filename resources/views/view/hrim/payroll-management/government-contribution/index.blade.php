@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.payroll-management.government-contribution.index')
        </div>
    </div>
@endsection
