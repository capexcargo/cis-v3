@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.payroll-management.month13th.index')
        </div>
    </div>
@endsection
