@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.reports.loan-government-deduction.index')
        </div>
    </div>
@endsection
