@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.attendance.schedule-adjustment.view', ['id' => $id])
        </div>
    </div>
@endsection
