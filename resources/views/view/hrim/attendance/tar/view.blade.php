@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.attendance.tar.view', ['id' => $id])
        </div>
    </div>
@endsection
