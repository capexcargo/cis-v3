@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.attendance.daily-time-records.index')
        </div>
    </div>
@endsection
