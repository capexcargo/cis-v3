@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.attendance.team-status.index')
        </div>
    </div>
@endsection
