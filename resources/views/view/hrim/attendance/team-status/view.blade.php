@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.attendance.team-status.view', ['id' => $id])
        </div>
    </div>
@endsection
