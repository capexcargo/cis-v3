@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.attendance.overtime-records.index')
        </div>
    </div>
@endsection
