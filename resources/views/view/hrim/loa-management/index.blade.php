@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.loa-management.index')
        </div>
    </div>
@endsection
