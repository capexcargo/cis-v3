@extends('layouts/app')
@section('content')
    <div class="container grid mx-auto">
        <div class="overflow-hidden">
            @livewire('hrim.company-management.employer-compliance.index')
        </div>
    </div>
@endsection
