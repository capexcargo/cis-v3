@extends('layouts/app')
@section('content')
    <div class="container grid mx-auto">
        <div class="overflow-hidden">
            @livewire('hrim.company-management.calendar.index')
        </div>
    </div>
@endsection
