@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.workforce.work-schedule.index')
        </div>
    </div>
@endsection
