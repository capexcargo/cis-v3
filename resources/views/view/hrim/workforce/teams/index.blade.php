@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.workforce.teams.index')
        </div>
    </div>
@endsection
