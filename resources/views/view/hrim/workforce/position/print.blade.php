<!DOCTYPE html>
<html :class="{ 'theme-dark': dark }" x-data="data()" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Position Job Description</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    {{-- <script src="https://cdn.tailwindcss.com"></script> --}}
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
        body {
            font-family: 'Poppins';
        }
    </style>
</head>

<body onload="window.print()">
    <div>
        <div class="px-6 text-gray-800">
            <div class="flex justify-between">
                <div>
                    <p class="mb-4 text-3xl font-bold">{{ $position->display }}</p>
                </div>
                <div>
                </div>
            </div>
            <p class="text-xl font-bold">Job Overview</p>
            <p class="text-sm font-semibold">{{ $position->job_overview }}</p>

            <p class="mt-6 text-xl font-bold">Responsibilities and Duties</p>
            <ul class="px-6 mb-1 text-lg list-disc">
                @foreach ($position->responsibilities as $key => $resp)
                    <li class="px-2 mb-2 text-sm font-semibold">{{ $resp->duties }}</li>
                @endforeach
            </ul>

            <p class="mt-6 mb-1 text-xl font-bold">Job Requirements</p>
            <ul class="px-6 text-lg list-disc">
                @foreach ($position->requirements as $key => $require)
                    <li class="px-2 mb-2 text-sm font-semibold">{{ $require->code }}</li>
                @endforeach
            </ul>
        </div>
    </div>
</body>

<script type="text/javascript">
    window.onload = function() {
        window.print();

        setTimeout(function() {
            window.close();
        }, 3000);
    };
</script>
