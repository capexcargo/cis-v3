@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.workforce.department-management.index')
        </div>
    </div>
@endsection
