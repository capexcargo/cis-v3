@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.recruitment-and-hiring.employment-category-type.index')
        </div>
    </div>
@endsection
