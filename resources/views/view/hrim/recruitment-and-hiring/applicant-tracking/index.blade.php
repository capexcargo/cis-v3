@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.recruitment-and-hiring.applicant-tracking.index')
        </div>
    </div>
@endsection
