@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.recruitment-and-hiring.employee-requisition.index')
        </div>
    </div>
@endsection
