@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.recruitment-and-hiring.applicant-status-mgmt.index')
        </div>
    </div>
@endsection
