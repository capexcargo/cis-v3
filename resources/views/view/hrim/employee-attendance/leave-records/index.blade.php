@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.employee-attendance.leave-records.index')
        </div>
    </div>
@endsection
