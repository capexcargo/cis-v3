@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.employee-attendance.schedule-adjustment.index')
        </div>
    </div>
@endsection
