@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.dashboard.index')
        </div>
    </div>
@endsection
