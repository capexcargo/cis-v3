@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.compensation-and-benefits.salary-grade-management.index')
        </div>
    </div>
@endsection
