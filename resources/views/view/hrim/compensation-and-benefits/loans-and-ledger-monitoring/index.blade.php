@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.compensation-and-benefits.loans-and-ledger-monitoring.index')
        </div>
    </div>
@endsection
