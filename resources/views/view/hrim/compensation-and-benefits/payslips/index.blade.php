@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.compensation-and-benefits.payslips.index')
        </div>
    </div>
@endsection
