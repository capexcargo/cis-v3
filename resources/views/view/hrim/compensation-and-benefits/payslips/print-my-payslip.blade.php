<!DOCTYPE html>
<html :class="{ 'theme-dark': dark }" x-data="data()" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>CaPEx Loans ATD</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    {{-- <script src="https://cdn.tailwindcss.com"></script> --}}
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
        body {
            font-family: 'Poppins';
        }
    </style>
</head>

<body>
    {{-- <body> --}}
    <div class="p-2 text-xs">
        <div class="text-lg text-center border-2 border-gray-400 rounded-t-lg">
            <div class="grid grid-cols-4 gap-2">
                <div>
                    <div class="fixed px-4 py-[2px] mt-2">
                        <img class="h-16" src="/images/logo/capex-logo.png" alt="CaPEx_logo">
                    </div>
                </div>
                <div class="p-6 text-lg font-semibold whitespace-nowrap">
                    <span class="text-3xl text-center" style="margin-left: 110px">PAYSLIP</span>
                    <div class="text-sm font-bold text-gray-600">
                        Cargo Padala Express Forwarding Services Corp.
                    </div>
                </div>
                <div></div>
                <div></div>
            </div>
        </div>
        @foreach ($payrolls as $payroll)
            <div class="border-2 border-t-0 border-gray-400 text-md">
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px] pt-2">
                    <div class="grid grid-cols-2">
                        <div class="font-semibold text-gray-600">Employee's Name :</div>
                        <div class="ml-6 font-semibold">{{ $payroll->first_name . ' ' . $payroll->last_name }}</div>
                    </div>
                    <div class="grid grid-cols-2 ">
                        <div class="font-semibold text-gray-600">Branch :</div>
                        <div class="ml-6 font-semibold">{{ $payroll->branchname }}</div>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-2">
                        <div class="font-semibold text-gray-600">Employee Code :</div>
                        <div class="w-full ml-6 font-semibold break-words">{{ $payroll->employee_code }}</div>
                    </div>
                    <div class="grid grid-cols-2 ">
                        <div class="font-semibold text-gray-600">Total Worked Hours :</div>
                        <div class="ml-6 font-semibold">{{ $payroll->workingdays }} Day/s</div>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-2">
                        <div class="font-semibold text-gray-600">Division :</div>
                        <div class="w-full ml-6 font-semibold break-words">{{ $payroll->division }}</div>
                    </div>
                    <div class="grid grid-cols-2 ">
                        <div class="font-semibold text-gray-600">Pay Period :</div>
                        <div class="ml-6 font-semibold">{{ $payroll->period == 1 ? 'First Cutoff' : 'Second Cutoff' }}
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]  mb-2">
                    <div class="grid grid-cols-2">
                        <div class="font-semibold text-gray-600">Position :</div>
                        <div class="w-full ml-6 font-semibold break-words">{{ $payroll->position }}</div>
                    </div>
                    <div class="grid grid-cols-2 ">
                        <div class="font-semibold text-gray-600">Payout Date :</div>
                        <div class="ml-6 font-semibold">
                            {{ ($payroll->period == 1 ? 'First Cutoff' : 'Second Cutoff') . ' of ' . $payroll->payoutdatemonth }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-md border-2 border-t-0 border-gray-400 py-[2px]">
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-1">
                        <div class="text-lg text-[#003399] font-bold py-[3px]"
                            style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                            Earnings</div>
                    </div>
                    <div class="grid grid-cols-1 ">
                        <div class="text-lg text-[#003399] font-bold py-[3px]">Deductions</div>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-3"
                        style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Basic Pay :</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 font-semibold">{{ number_format($payroll->basic_pay / 2, 2) }}</div>
                    </div>
                    <div class="grid grid-cols-3 ">
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Leave :</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Without Pay :</div>
                        <div class="ml-6 font-semibold">
                            {{ number_format(round($payroll->leaves * ($payroll->basic_pay / 26), 2), 2) }}</div>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-3"
                        style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                        <div class="font-semibold text-gray-600 whitespace-nowrap">COLA :</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 font-semibold">{{ number_format($payroll->cola / 2, 2) }}</div>
                    </div>
                    <div class="grid grid-cols-3 ">
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Tardiness</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 font-semibold">{{ number_format($payroll->tardiness, 2) }}</div>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-3"
                        style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Gross Pay :</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 font-semibold">{{ number_format($payroll->gross / 2, 2) }}</div>
                    </div>
                    <div class="grid grid-cols-3">
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Undertime :</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 font-semibold">{{ number_format($payroll->undertime, 2) }}</div>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-3"
                        style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Holiday Pay :</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 font-semibold">
                            {{ number_format($payroll->holidaypregular + $payroll->holidaypspecial, 2) }}</div>
                    </div>
                    <div class="grid grid-cols-3 ">
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Absences :</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 font-semibold">
                            {{ number_format(round($payroll->absentcount * ($payroll->basic_pay / 26), 2), 2) }}</div>
                    </div>
                </div>

                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-3"
                        style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                        <div class="font-semibold text-gray-600 whitespace-nowrap">ND Pay :</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 font-semibold">{{ number_format($payroll->ndval / 2, 2) }}</div>
                    </div>
                    <div class="grid grid-cols-3">
                        <div class="w-12 font-semibold text-gray-600">Gov't Contributions</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap">SSS :</div>
                        <div class="ml-6 font-semibold"></div>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-3" style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                        <div class="font-semibold text-gray-600 whitespace-nowrap">OT Pay :</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Regular OT Pay :</div>
                        <div class="ml-6 font-semibold">{{ number_format($payroll->ot_reg_pay, 2) }}</div>
                    </div>
                    <div class="grid grid-cols-3 ">
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Philhealth :</div>
                        <div class="ml-6 font-semibold">
                            {{ number_format($payroll->cutoffidentify == 1 ? $payroll->philhealth : 0, 2) }}</div>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-3" style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Regular Holiday OT :</div>
                        <div class="ml-6 font-semibold">{{ number_format($payroll->ot_holiday_pay, 2) }}</div>
                    </div>
                    <div class="grid grid-cols-3 ">
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Pag-IBIG :</div>
                        <div class="ml-6 font-semibold">
                            {{ number_format($payroll->cutoffidentify == 1 ? $payroll->pagibig : 0, 2) }}</div>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-3" style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="w-12 font-semibold text-gray-600">Special Non-working Holiday OT :
                        </div>
                        <div class="ml-6 font-semibold">{{ number_format($payroll->ot_special_pay, 2) }}</div>
                    </div>
                    <div class="grid grid-cols-3 ">
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Gov't Loans</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap">SSS :</div>
                        <div class="ml-6 font-semibold">{{ number_format($payroll->loansgovtsss, 2) }}</div>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-3" style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Rest Day OT :</div>
                        <div class="ml-6 font-semibold">{{ number_format($payroll->ot_restday_pay, 2) }}</div>
                    </div>
                    <div class="grid grid-cols-3 ">
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Philhealth:</div>
                        <div class="ml-6 font-semibold">{{ number_format($payroll->loansgovtphilhealth, 2) }}</div>

                    </div>
                </div>
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-3" style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Rest Day Pay :</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 font-semibold">{{ number_format(round($payroll->restdaypays, 2), 2) }}</div>
                    </div>
                    <div class="grid grid-cols-3 ">
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Pag-IBIG:</div>
                        <div class="ml-6 font-semibold">{{ number_format($payroll->loansgovtpagibig, 2) }}</div>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-3" style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 font-semibold"></div>
                    </div>
                    <div class="grid grid-cols-3 ">
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Withholding Tax</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 font-semibold">{{ number_format(0, 2) }}</div>
                    </div>
                </div>
                @if ($loans)
                    @foreach ($loans as $loan)
                        <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                            <div class="grid grid-cols-3" style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                                <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                                <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                                <div class="ml-6 font-semibold"></div>
                            </div>
                            <div class="grid grid-cols-3 ">
                                <div class="w-6 font-semibold text-gray-600">Other Loans and Deductions</div>
                                <div class="font-semibold text-gray-600 whitespace-nowrap">{{ $loan->deductionname }}
                                </div>
                                <div class="ml-6 font-semibold">{{ $loan->loandeducamount }}</div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                        <div class="grid grid-cols-3" style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                            <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                            <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                            <div class="ml-6 font-semibold"></div>
                        </div>
                        <div class="grid grid-cols-3 ">
                            <div class="w-6 font-semibold text-gray-600">Other Loans and Deductions</div>
                            <div class="font-semibold text-gray-600 whitespace-nowrap">
                            </div>
                            <div class="ml-6 font-semibold">0.00</div>
                        </div>
                    </div>
                @endif
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px] ">
                    <div class="grid grid-cols-3 pb-6" style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 font-semibold"></div>
                    </div>
                    <div class="grid grid-cols-3 pb-6">
                        <div class="font-semibold text-gray-600 whitespace-nowrap">Admin Adjustments</div>
                        <div class="font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 font-semibold">{{ number_format($payroll->aadpay, 2) ?? '0.00' }}</div>
                    </div>
                </div>
            
                <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                    <div class="grid grid-cols-3" style="border-right:dashed; margin-right:-40px; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
                        <div class="text-base font-semibold text-gray-600 whitespace-nowrap">Total Earnings :</div>
                        <div class="text-base font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 text-base font-semibold">
                            {{ number_format(
                                $payroll->gross / 2 +
                                    $payroll->holidayp / 2 +
                                    $payroll->ot_pay +
                                    $payroll->ndval +
                                    round($payroll->restdaypays, 2) +
                                    $payroll->dlbpay +
                                    $payroll->aadpay +
                                    round($payroll->leaveswtpay * ($payroll->basic_pay / 26), 2),
                                2,
                            ) }}
                        </div>
                    </div>
                    <div class="grid grid-cols-3 ">
                        <div class="text-base font-semibold text-gray-600 whitespace-nowrap">Total Deductions :
                        </div>
                        <div class="text-base font-semibold text-gray-600 whitespace-nowrap"></div>
                        <div class="ml-6 text-base font-semibold">
                            {{ number_format(
                                round($payroll->leaves * ($payroll->basic_pay / 26), 2) +
                                    $payroll->tardiness +
                                    $payroll->undertime +
                                    (round($payroll->absentcount * ($payroll->basic_pay / 26), 2) +
                                        ($payroll->cutoffidentify == 1 ? $payroll->philhealth + $payroll->pagibig : $payroll->loansgovt)) +
                                    $payroll->loans,
                                2,
                            ) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-md border-2 border-t-0 rounded-b-lg border-gray-400 py-[2px]">
                <div class="grid justify-between grid-cols-5 px-8 py-4">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div class="text-lg text-[#003399] font-bold">NET SALARY</div>
                    <div class="text-lg font-bold text-right">
                        {{ number_format(
                            $payroll->gross / 2 +
                                $payroll->ot_pay +
                                $payroll->holidayp / 2 +
                                $payroll->ndval +
                                round($payroll->restdaypays, 2) +
                                $payroll->dlbpay +
                                $payroll->aadpay +
                                round($payroll->leaveswtpay * ($payroll->basic_pay / 26), 2) -
                                (round($payroll->leaves * ($payroll->basic_pay / 26), 2) +
                                    $payroll->tardiness +
                                    $payroll->undertime +
                                    (round($payroll->absentcount * ($payroll->basic_pay / 26), 2) +
                                        ($payroll->cutoffidentify == 1 ? $payroll->philhealth + $payroll->pagibig : $payroll->loansgovt)) +
                                    $payroll->loans),
                            2,
                        ) }}
                    </div>
                    <div></div>
                </div>

            </div>
    </div>
    @endforeach
</body>
