@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.employee-management.sanction-status.index')
        </div>
    </div>
@endsection
