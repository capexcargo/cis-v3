@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.employee-management.resignation-reason-reference-management.index')
        </div>
    </div>
@endsection
