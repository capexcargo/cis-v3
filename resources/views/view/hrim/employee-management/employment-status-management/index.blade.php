@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.employee-management.employment-status-management.index')
        </div>
    </div>
@endsection
