@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.employee-management.disciplinary-history.index')
        </div>
    </div>
@endsection
