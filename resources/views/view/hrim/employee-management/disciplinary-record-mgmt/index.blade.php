@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('hrim.employee-management.disciplinary-record-mgmt.index')
        </div>
    </div>
@endsection
