<!DOCTYPE html>
<html :class="{ 'theme-dark': dark }" x-data="data()" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>CaPEx Loans ATD</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    {{-- <script src="https://cdn.tailwindcss.com"></script> --}}
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
        body {
            font-family: 'Poppins';
        }
    </style>
</head>

<body onload="window.print()">
    {{-- <body> --}}
    <div class="p-2 text-xs">
        <div class="text-lg rounded-t-lg text-center border-2 border-gray-400">
            <div class="grid grid-cols-4 gap-2">
                <div>
                    <div class="fixed px-4 py-[2px] mt-2">
                        <img class="h-16" src="/images/logo/capex-logo.png" alt="CaPEx_logo">
                    </div>
                </div>
                <div class="whitespace-nowrap text-lg font-semibold p-6">
                    <span class="text-center" style="margin-left: 72px">AUTHORITY TO DEDUCT</span>
                    <div class="text-sm font-bold text-gray-600">
                        Cargo Padala Express Forwarding Services Corp.
                    </div>
                </div>
                <div></div>
                <div></div>
            </div>
        </div>
        <div class="text-md border-2 border-t-0 border-gray-400"
            style="border-bottom:dashed; border-color:rgb(156 163 175 / var(--tw-border-opacity));">
            <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                <div class="grid grid-cols-2">
                    <div class="font-semibold text-gray-600">Employee's Name :</div>
                    <div class="font-semibold ml-6">{{ $employee_name }}</div>
                </div>
                <div class="grid grid-cols-2 ">
                    <div class="font-semibold text-gray-600">Date Filed :</div>
                    <div class="font-semibold ml-6">{{ date('M d, Y', strtotime($date_filed)) }}</div>
                </div>
            </div>
            <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                <div class="grid grid-cols-2">
                    <div class="font-semibold text-gray-600">Position :</div>
                    <div class="font-semibold ml-6 break-words w-full">{{ $position }}</div>
                </div>
                <div class="grid grid-cols-2 ">
                    <div class="font-semibold text-gray-600">Division :</div>
                    <div class="font-semibold ml-6">{{ $division }}</div>
                </div>
            </div>
        </div>
        <div class="text-md border-2 border-t-0 border-gray-400 py-[2px]">
            <div class="px-6 text-lg text-[#003399] font-bold py-[3px]">ATD Details</div>
            <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                <div class="grid grid-cols-2">
                    <div class="font-semibold text-gray-600 whitespace-nowrap">Type of Loan :</div>
                    <div class="font-semibold ml-6">{{ $type_of_loan }}</div>
                </div>
                <div class="grid grid-cols-2 ">
                    <div class="font-semibold text-gray-600 whitespace-nowrap">Reference Number :</div>
                    <div class="font-semibold ml-6">{{ $reference_no }}</div>
                </div>
            </div>
            <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                <div class="grid grid-cols-2">
                    <div class="font-semibold text-gray-600 whitespace-nowrap">Principal Amount :</div>
                    <div class="font-semibold ml-6">{{ number_format($principal_amount, 2) }}</div>
                </div>
                <div class="grid grid-cols-2 ">
                    <div class="font-semibold text-gray-600 whitespace-nowrap">Terms :</div>
                    <div class="font-semibold ml-6">{{ $terms }}</div>
                </div>
            </div>
            <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                <div class="grid grid-cols-2">
                    <div class="font-semibold text-gray-600 whitespace-nowrap">Total Interest :</div>
                    <div class="font-semibold ml-6">{{ number_format($interest, 2) }}</div>
                </div>
                <div class="grid grid-cols-2 ">
                    <div class="font-semibold text-gray-600 whitespace-nowrap">Payment Terms :</div>
                    <div class="font-semibold ml-6">{{ $term_type == 1 ? 'Bi-Monthly' : 'Monthly' }}</div>
                </div>
            </div>
            <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                <div class="grid grid-cols-2">
                    <div class="font-semibold text-gray-600 whitespace-nowrap">Total Installment Amount :</div>
                    <div class="font-semibold ml-6">{{ number_format($principal_amount + $interest, 2) }}</div>
                </div>
                <div class="grid grid-cols-2 ">
                    <div class="font-semibold text-gray-600 whitespace-nowrap">Reason :</div>
                    <div class="font-semibold ml-6">{{ $reason }}</div>
                </div>
            </div>
            <div class="grid grid-cols-2 gap-16 px-6 py-[2px]">
                <div class="grid grid-cols-2">
                    <div class="font-semibold text-gray-600 whitespace-nowrap">Payment Start Date :</div>
                    <div class="font-semibold ml-6">{{ date('M d, Y', strtotime($payment_start_date)) }}</div>
                </div>
                <div class="grid grid-cols-2 ">
                    <div class="font-semibold text-gray-600 whitespace-nowrap">Payment End Date :</div>
                    <div class="font-semibold ml-6">{{ date('M d, Y', strtotime($payment_end_date)) }}</div>
                </div>
            </div>
        </div>
        <div class="text-md border-2 border-t-0 rounded-b-lg border-gray-400 py-[2px]">
            <div class="px-6 text-lg text-[#003399] font-bold py-[3px]">Approval</div>
            <div class="grid grid-cols-4 justify-between px-6 mt-4 py-3">
                <div class="text-center">
                    <div class="border-b-2 border-gray-800"></div>
                    Division Head
                </div>
                <div></div>
                <div></div>
                <div class="text-center">
                    <div class="border-b-2 border-gray-800"></div>
                    Hr Department
                </div>
            </div>
            <div class="grid grid-cols-4 justify-between px-6 mt-4 py-3">
                <div class="text-center">
                    <div class="border-b-2 border-gray-800"></div>
                    Employee's Signature
                </div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
</body>
