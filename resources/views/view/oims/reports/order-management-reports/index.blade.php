@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('oims.reports.order-management-reports.index')
        </div>
    </div>
@endsection
