@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('oims.order-management.t-e-dropdown-mgmt.municipality-mgmt.index')
        </div>
    </div>
@endsection
