@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('oims.order-management.e-dtr.index')
        </div>
    </div>
@endsection
