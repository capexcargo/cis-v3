<!DOCTYPE html>
<html :class="{ 'theme-dark': dark }" x-data="data()" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>e-DTR</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    {{-- <script src="https://cdn.tailwindcss.com"></script> --}}
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
        body {
            font-family: 'Poppins';
        }
    </style>
</head>

<body class="">
    {{-- onload="window.print()" 
    class=""> --}}

    {{-- @dd($getEDtrs); --}}
    <div class="flex flex-col min-h-screen p-2">
        
        <div class="flex justify-between col-span-12">
            <div class="">
                <span class=""><img class="h-24" src="/images/logo/capex-logo.png" alt="VeMoBro_logo"></span>
            </div>
            <div class="">
                <table class="text-right">
                    <tr>
                        <td class="text-lg font-semibold">
                            Electronic
                        </td>
                    </tr>
                    <tr>
                        <td class="text-3xl font-semibold">
                            Daily Trip Record
                        </td>
                    </tr>
                    <tr>
                        <td class="flex ">
                            <span class="text-xs text-gray-400 ">e-DTR Reference Number :</span>
                            <span class="text-sm text-blue-800 capitalize">{{ $rmrefno }}</span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div
            class="flex flex-col justify-between flex-1 col-span-12 mt-4 overflow-auto border border-black rounded-md ">
            <div class="">
                <div class="flex p-2 px-3">
                    <table class="w-1/2 border-t border-b border-l border-black">
                        <tr class="border-b border-black">
                            <td class="flex p-1 px-3 ">
                                <span class="text-[10px] text-gray-500" style="min-width: 130px;">Date Created
                                    :</span>
                                <span class="text-xs font-semibold text-right">{{ $rmcreated }}</span>
                            </td>
                        </tr>
                        <tr class="border-b border-black">
                            <td class="flex p-1 px-3 ">
                                <span class="text-[10px] text-gray-500 " style="min-width: 130px;">Date of
                                    Dispatch :</span>
                                <span class="text-xs font-semibold text-right">{{ $rmdisp }}</span>
                            </td>
                        </tr>
                        <tr class="border-b border-black">
                            <td class="flex p-1 px-3 ">
                                <span class="text-[10px] text-gray-500 " style="min-width: 130px;">Branch
                                    :</span>
                                <span class="text-xs font-semibold text-right">{{ $rmbrnch }}</span>
                            </td>
                        </tr>
                        <tr class="border-b border-black">
                            <td class="flex p-1 px-3 ">
                                <span class="text-[10px] text-gray-500 " style="min-width: 130px;">Plate
                                    Number :</span>
                                <span class="text-xs font-semibold text-right">{{ $rmplate }}</span>
                            </td>
                        </tr>
                    </table>

                    <table class="w-1/2 border border-black">
                        <tr class="border-b border-black">
                            <td class="flex p-1 px-3 ">
                                <span class="text-[10px] text-gray-500" style="min-width: 100px;">Team
                                    :</span>
                                <span class="text-xs font-semibold text-right">{{ $rmteam }}</span>
                            </td>
                        </tr>
                        <tr class="border-b border-black">
                            <td class="flex p-1 px-3 ">
                                <span class="text-[10px] text-gray-500 " style="min-width: 100px;">Driver
                                    :</span>
                                <span class="text-xs font-semibold text-right">{{ $rmdrv }}</span>
                            </td>
                        </tr>
                        <tr class="border-b border-black">
                            <td class="flex p-1 px-3 ">
                                <span class="text-[10px] text-gray-500 " style="min-width: 100px;">Checker 1
                                    :</span>
                                <span class="text-xs font-semibold text-right">{{ $rmchk1 }}</span>
                            </td>
                        </tr>
                        <tr class="border-b border-black">
                            <td class="flex p-1 px-3 ">
                                <span class="text-[10px] text-gray-500 " style="min-width: 100px;">Checker 2
                                    :</span>
                                <span class="text-xs font-semibold text-right">{{ $rmchk2 }}</span>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class=" px-4 text-sm py-1 font-normal mt-2 text-white bg-[#003399] rounded-tr-xl"
                    style="border-top-right-radius: 40px; width: 200px">
                    Delivery Activities
                </div>

                <div class="">
                    <table class="min-w-full border-t border-b border-black ">
                        <thead>
                            <tr class="overflow-auto border-b border-gray-400">
                                <th class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                    No.</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                    Waybill Number</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                    Origin</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                    Destination</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                    Shipper</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                    Consignee</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                    Transaction Date</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                    Transport Mode</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                    Qty</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                    Paymode</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                    Amount for Collection</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                    Amount Collected</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-left text-black ">
                                    Remarks</th>
                            </tr>
                        </thead>
                        <tbody class="divide-y-4 divide-white">
                            <tr class="overflow-auto">
                                <td class="px-1.5 py-2 text-[10px] font-medium whitespace-nowrap border-r border-gray-400"
                                    style="background-color: #F7F7F7;">1.
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-left whitespace-nowrap border-r border-gray-400"
                                    style="background-color: #F7F7F7;">CEB1234567
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-center whitespace-nowrap border-r border-gray-400"
                                    style="background-color: #F7F7F7;">CEB
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-center whitespace-nowrap border-r border-gray-400"
                                    style="background-color: #F7F7F7;">MNL
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-left border-r border-gray-400" style="background-color: #F7F7F7;">Ace Carlo Cunanan
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-left whitespace-nowrap border-r border-gray-400"
                                    style="background-color: #F7F7F7;">UBIC
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-center whitespace-nowrap border-r border-gray-400"
                                    style="background-color: #F7F7F7;">12/20/2022
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-center whitespace-nowrap border-r border-gray-400"
                                    style="background-color: #F7F7F7;">AIR
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-center whitespace-nowrap border-r border-gray-400"
                                    style="background-color: #F7F7F7;">5
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-center whitespace-nowrap border-r border-gray-400"
                                    style="background-color: #F7F7F7;">COD
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-center whitespace-nowrap border-r border-gray-400"
                                    style="background-color: #F7F7F7;">500.00
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-center whitespace-nowrap border-r border-gray-400"
                                    style="background-color: #F7F7F7;">500.00
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-left whitespace-nowrap"
                                    style="background-color: #F7F7F7;">asdasdas
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <span class="flex justify-end p-2 text-[10px] font-medium text-red">- no other delivery
                        activity follows as of 07/21/2022 13:32 -</span>
                </div>

                <div class=" px-4 text-sm py-1 font-normal  text-white bg-[#003399] rounded-tr-xl"
                    style="border-top-right-radius: 40px; width: 200px">
                    Pick Up Activities
                </div>

                <div class="">
                    <table class="min-w-full border-t border-b border-black ">
                        <thead>
                            <tr class="border-b border-gray-400">
                                <th class="px-1.5 py-2 text-[10px] font-normal text-black  border-r border-gray-400">
                                    No.</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                    Booking Reference No.</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                    Shipper</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                    Pick Up Address</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                    Pick Up Date</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                    Activity Type</th>
                                <th class="px-1.5 py-2 text-[10px] font-normal text-left text-black ">
                                    Remarks</th>
                            </tr>
                        </thead>
                        <tbody class="divide-y-4 divide-white">
                            @foreach ($getEDtrs[0]['edtrDetails'] as $r => $getEDtr)
                            <tr>
                                <td class="px-1.5 py-2 text-[10px] font-medium whitespace-nowrap border-r border-gray-400"
                                    style="background-color: #F7F7F7;">{{ $r + 1 }}.
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-left whitespace-nowrap border-r border-gray-400"
                                    style="background-color: #F7F7F7;">{{$getEDtr['edtrbook']['booking_reference_no']}}
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-left border-r border-gray-400"
                                    style="background-color: #F7F7F7;">{{$getEDtr['edtrbook']['BookShipper'][0]['name']}}
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-left border-r border-gray-400"
                                    style="background-color: #F7F7F7;">{{$getEDtr['edtrbook']['BookShipper'][0]['address']}}
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-left whitespace-nowrap border-r border-gray-400"
                                    style="background-color: #F7F7F7;">{{date('m/d/Y', strtotime($getEDtr['edtrbook']['pickup_date']))}}
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium border-r border-gray-400" style="background-color: #F7F7F7; ">{{$getEDtr['edtrbook']['ActivityReferenceBK']['activity_type']}}
                                </td>
                                <td class="px-1.5 py-2 text-[10px] font-medium text-left"
                                    style="background-color: #F7F7F7;">{{$getEDtr['edtrbook']['remarks']}}
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    <span class="flex justify-end p-2 text-[10px] font-medium text-red">- no other pick up
                        activity follows as of {{$latestPickup}}</span>
                </div>
            </div>



            <div class="min-w-full col-span-12 pb-2 border-t border-black ">
                <div class="text-[9px] text-gray-500 px-6 py-2">Acknowledged and Received by :</div>

                <div class="flex justify-center mt-6 space-x-10">
                    <span class="text-[10px] font-semibold text-center text-black" style="width: 28%">{{ $rmchk1 }}</span>
                    <span class="text-[10px] font-semibold text-center text-black" style="width: 28%">{{ $rmchk2 }}</span>
                    <span class="text-[10px] font-semibold text-center text-black" style="width: 28%">{{ $rmdrv }}</span>
                </div>
                <div class="flex justify-center space-x-10">
                    <hr class="text-black border border-gray-400" style="width: 28%">
                    <hr class="text-black border border-gray-400" style="width: 28%">
                    <hr class="text-black border border-gray-400" style="width: 28%">
                </div>
                <div class="flex justify-center space-x-10">
                    <span class="text-[9px] text-center text-gray-500" style="width: 28%">Printed Name and
                        Signature</span>
                    <span class="text-[9px] text-center text-gray-500" style="width: 28%">Printed Name and
                        Signature</span>
                    <span class="text-[9px] text-center text-gray-500" style="width: 28%">Printed Name and
                        Signature</span>
                </div>
                <div class="flex justify-center space-x-10">
                    <span class="text-[10px] font-semibold text-center text-black" style="width: 28%">Checker
                        1</span>
                    <span class="text-[10px] font-semibold text-center text-black" style="width: 28%">Checker
                        2</span>
                    <span class="text-[10px] font-semibold text-center text-black" style="width: 28%">Driver</span>
                </div>



                <div class="flex items-end justify-center w-full mt-10 space-x-10">
                    <div class="flex justify-center mt-6">
                        <span class="w-full text-center text-[10px] font-semibold text-black"></span>
                        <span class="text-center text-[10px] font-semibold text-black"></span>
                    </div>
                    <div class="flex flex-col justify-center" style="width: 28%">
                        <div class="mb-8 text-[9px] text-gray-500">Dispatched by :</div>
                        <hr class="text-black border border-gray-400" />
                        <span class="text-center text-[9px] text-gray-500">Printed Name and Signature</span>
                        <span class="text-center text-[10px] font-semibold text-black">Order Controller</span>
                    </div>
                    <div class="flex flex-col justify-center" style="width: 28%">
                        <hr class="text-black border border-gray-400" />
                        <span class="text-center text-[9px] text-gray-500">Printed Name and Signature</span>
                        <span class="text-center text-[10px] font-semibold text-black">Guard on Duty</span>
                    </div>
                </div>


            </div>

        </div>

    </div>
</body>

<script type="text/javascript">
    window.onload = function() {
        window.print();

        setTimeout(function() {
            window.close();
        }, 3000);
    };
</script>
