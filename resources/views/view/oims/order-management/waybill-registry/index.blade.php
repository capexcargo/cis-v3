@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('oims.order-management.waybill-registry.index')
        </div>
    </div>
@endsection
