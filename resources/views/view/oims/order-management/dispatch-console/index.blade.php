@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('oims.order-management.dispatch-console.index')
        </div>
    </div>
@endsection
