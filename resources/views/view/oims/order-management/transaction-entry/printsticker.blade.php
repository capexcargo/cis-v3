<!DOCTYPE html>
<html :class="{ 'theme-dark': dark }" x-data="data()" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>e-DTR</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    {{-- <script src="https://cdn.tailwindcss.com"></script> --}}
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
        body {
            font-family: 'Poppins';
        }
    </style>
</head>

{{-- <body onload="window.print()" class=""> --}}

<body>

    <div class="min-h-screen p-2">
        <div class="flex justify-between ">
            <div>
                <span class=""><img class="w-48 pl-4 h-22" src="
            /images/logo/capex-logo.png"
                        alt="VeMoBro_logo"></span>
                <p class="mt-12 ml-6 text-3xl font-medium text-blue-800">
                    1/2
                </p>
            </div>
            <div class="">
                <span class=""><img class="w-40 h-34" src="
            /images/logo/qr_code_PNG3.png"
                        alt="VeMoBro_logo"></span>
                <p class="ml-6 -mt-4 text-lg font-medium">
                    MNL0001837
                </p>

            </div>
        </div>

        <div class="p-2 pr-4 mt-2 border-2 border-blue-800 rounded-lg">
            <h1 class="text-2xl font-semibold text-blue-800">Shipper Details</h1>
            <div class="flex">
                <table class="w-1/2 mt-2 text-sm text-black ">
                    <tr>
                        <td class="flex font-medium whitespace-nowrap">
                            <div>
                                Name :
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="flex font-medium whitespace-nowrap">
                            <div>
                                Address :
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="flex font-medium whitespace-nowrap">
                            <div>
                                Contact Person :
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="flex font-medium whitespace-nowrap">
                            <div>
                                Contact Number :
                            </div>
                        </td>
                    </tr>

                </table>
                <table class="w-1/2 mt-2 text-sm text-black ">
                    <tr>
                        <td class="flex font-medium ">
                            <div>
                                Glenly Mallari
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="flex font-medium ">
                            <div>
                                10th St. Brgy. 183 Villamor, Pasay City, Metro Manila
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="flex font-medium ">
                            <div>
                                Glenly Mallari
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="flex font-medium ">
                            <div>
                                0926 291 8371
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

        </div>

        <div class="p-2 pr-4 mt-4 border-2 border-blue-800 rounded-lg">
            <h1 class="text-2xl font-semibold text-blue-800">Consignee Details</h1>
            <div class="flex">
                <table class="w-1/2 mt-2 text-sm text-black ">
                    <tr>
                        <td class="flex font-medium whitespace-nowrap">
                            <div>
                                Name :
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="flex font-medium whitespace-nowrap">
                            <div>
                                Address :
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="flex font-medium whitespace-nowrap">
                            <div>
                                Contact Person :
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="flex font-medium whitespace-nowrap">
                            <div>
                                Contact Number :
                            </div>
                        </td>
                    </tr>

                </table>
                <table class="w-1/2 mt-2 text-sm text-black ">
                    <tr>
                        <td class="flex font-medium ">
                            <div>
                                Hills Inc.
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="flex font-medium ">
                            <div>
                                Sample Street, Barangay Cebu City
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="flex font-medium ">
                            <div>
                                Paris Becker
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="flex font-medium ">
                            <div>
                                0927 262 5142
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </div>


</body>
<script type="text/javascript">
    window.onload = function() {
        window.print();

        setTimeout(function() {
            window.close();
        }, 3000);
    };
</script>
