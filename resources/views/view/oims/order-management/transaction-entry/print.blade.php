<!DOCTYPE html>
<html :class="{ 'theme-dark': dark }" x-data="data()" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Waybill Summary</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    {{-- <script src="https://cdn.tailwindcss.com"></script> --}}
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
        body {
            font-family: 'Poppins';
        }
    </style>
</head>

{{-- <body onload="print()" class=""> --}}

<body class="">
    <div class="flex flex-col justify-between min-h-screen">

        <div class="grid grid-cols-12 bg-white rounded-lg shadow-md ">

            <div class="col-span-12 p-2 px-6 border-b-2 border-blue-800 rounded-tl-lg rounded-tr-lg bg-blue ">
                <span class="w-full text-lg font-medium text-white rounded-tr-lg bg-blue" style="">
                    Waybill Summary
                </span>
            </div>
            <div class="col-span-3 p-2">
                <span class=""><img class="w-40 h-16 pl-4"
                        src="
                            /images/logo/capex-logo.png" alt="VeMoBro_logo"></span>

                <div class="p-4 pt-2">
                    <table class="w-full mt-2 text-sm text-gray-500">
                        <tr>
                            <td class="w-[10%] whitespace-nowrap font-medium text-gray-400">
                                <div>
                                    Transaction Date :
                                </div>
                            </td>
                            <td class="font-semibold whitespace-nowrap" style="padding-left:4%; ">
                                <div>
                                    July 25,2022
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-sm text-gray-500">
                        <tr>
                            <td class="w-[10%] whitespace-nowrap font-medium text-gray-400">
                                <div>
                                    Origin :
                                </div>
                            </td>
                            <td class="font-semibold" style="padding-left:4%; ">
                                <div>
                                    Manila(MNL)
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="flex items-end col-span-3 p-2">
                <div class="p-4 pt-2 ">
                    <table class="w-full mt-2 text-sm text-gray-500">
                        <tr>
                            <td class="w-[10%] whitespace-nowrap font-medium text-gray-400">
                                <div>
                                    Transhipment :
                                </div>
                            </td>
                            <td class="font-semibold" style="padding-left:4%; ">
                                <div>
                                    -
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="flex items-end col-span-3 p-2">
                <div class="p-4 pt-2">
                    <table class="w-full mt-2 text-sm text-gray-500">
                        <tr>
                            <td class="w-[10%] whitespace-nowrap font-medium text-gray-400">
                                <div>
                                    Destination :
                                </div>
                            </td>
                            <td class="font-semibold" style="padding-left:4%; ">
                                <div>
                                    Cebu(CEB)
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="flex justify-end col-span-3 ">
                <div class="pt-2 pr-4">
                    <span class=""><img class="w-35 h-30"
                            src="
                            /images/logo/qr_code_PNG3.png" alt="VeMoBro_logo"></span>
                    <p class="ml-1 -mt-2 text-lg font-medium">
                        MNL0001837
                    </p>
                </div>
            </div>

            <div class="col-span-12">
                <hr class="text-black border border-gray-300">
            </div>

            <div class=col-span-8 ">
                    <div class="grid grid-cols-8">

                        <div class="col-span-4 p-6 border-r-2 border-gray-300 border-dashed">
                            <h1 class="text-lg font-bold text-blue-800">Shipper Details</h1>
                            <div class="flex">
                                <table class="w-1/2 mt-2 text-xs text-gray-500 ">
                                    <tr>
                                        <td class="flex font-medium whitespace-nowrap">
                                            <div>
                                                Company Name/Name :
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="flex font-medium whitespace-nowrap">
                                            <div>
                                                Contact Person :
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="flex font-medium whitespace-nowrap">
                                            <div>
                                                Contact Number :
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="flex font-medium whitespace-nowrap">
                                            <div>
                                                Address :
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-1/2 mt-2 text-xs text-black ">
                                    <tr>
                                        <td class="flex font-medium ">
                                            <div>
                                                Glenly Mallari
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="flex font-medium ">
                                            <div>
                                                Glenly Mallari
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="flex font-medium ">
                                            <div>
                                                0926 291 8371
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="flex font-medium ">
                                            <div>
                                                10th St. Brgy. 183 Villamor, Pasay City, Metro Manila
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>

                        <div class="col-span-4 p-6 ">
                            <h1 class="text-lg font-bold text-blue-800">Consignee Details</h1>
                            <div class="flex">
                                <table class="w-1/2 mt-2 text-xs text-gray-500 ">
                                    <tr>
                                        <td class="font-medium whitespace-nowrap">
                                            <div>
                                                Company Name/Name :
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-medium whitespace-nowrap">
                                            <div>
                                                Contact Person :
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-medium whitespace-nowrap">
                                            <div>
                                                Contact Number :
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-medium whitespace-nowrap">
                                            <div>
                                                Address :
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-1/2 mt-2 text-xs text-black">
                                    <tr>
                                        <td class="font-medium ">
                                            <div>
                                                Hills Inc.
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-medium ">
                                            <div>
                                                Paris Becker
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-medium ">
                                            <div>
                                                0927 262 5142
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-medium ">
                                            <div>
                                                Sample Street, Barangay Cebu City
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>

                        <div class="col-span-8">
                            <hr class="text-black border border-gray-300">
                        </div>

                        <div class="col-span-8 px-4 py-2">
                            <h1 class="pl-2 mt-4 text-base font-semibold text-black">Cargo Details :</h1>
                            <div class="mt-1 border-2 border-gray-300 rounded-lg ">
                                <x-table.table class="overflow-hidden" style="width: 100%">
                                    <x-slot name="thead" class="">
                                        <tr>
                                            <th scope="col"
                                                class="px-2 text-[10px] py-1 font-normal text-left border-r-2 border-gray-300 whitespace-nowrap">
                                                Transport Mode
                                            </th>
                                            <th scope="col"
                                                class="px-2 py-1 text-[10px] font-normal text-left border-r-2 border-gray-300">
                                                Description of Goods
                                            </th>
                                            <th scope="col"
                                                class="px-2 py-1 text-[10px] font-normal text-left border-r-2 border-gray-300 whitespace-nowrap">
                                                Commodity Type
                                            </th>
                                            <th scope="col"
                                                class="px-2 py-1 text-[10px] font-normal text-left border-r-2 border-gray-300 whitespace-nowrap">
                                                Type of Goods
                                            </th>
                                            <th scope="col"
                                                class="px-2 py-1 text-[10px] font-normal text-left border-r-2 border-gray-300">
                                                Commodity Applicable Rate
                                            </th>
                                            <th scope="col"
                                                class="px-2 py-1 text-[10px] font-normal text-left border-r-2 border-gray-300 whitespace-nowrap">
                                                Declared Value
                                            </th>
                                            <th scope="col"
                                                class="px-2 py-1 text-[10px] font-normal text-left border-r-2 border-gray-300">
                                                Paymode
                                            </th>
                                            <th scope="col" class="px-2 py-1 text-[10px] font-normal text-left">
                                                Service mode
                                            </th>
                                        </tr>
                                    </x-slot>
                                    <x-slot name="tbody">
                                        <tr class="text-xs text-center border-gray-300">
                                            <td class="px-2 py-1 font-normal border-r-2 border-gray-300 whitespace-nowrap">
                                                AIR
                                            </td>
                                            <td class="px-2 py-1 font-normal text-left border-r-2 border-gray-300">
                                                Construction Materials
                                            </td>
                                            <td class="px-2 py-1 font-normal border-r-2 border-gray-300 whitespace-nowrap">
                                                General Cargo
                                            </td>
                                            <td class="px-2 py-1 font-normal border-r-2 border-gray-300 whitespace-nowrap">
                                                Fragile
                                            </td>
                                            <td class="px-2 py-1 font-normal border-r-2 border-gray-300 whitespace-nowrap">
                                                Bida RG
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap">
                                                4,500.00
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap">
                                                PP
                                            </td>
                                            <td class="px-2 py-1 whitespace-nowrap">
                                                Airport to Door
                                            </td>
                                        </tr>

                                    </x-slot>
                                </x-table.table>
                            </div>
                        </div>

                        <div class="col-span-8 px-4 py-2 ">
                            <h1 class="pl-2 mt-4 text-base font-semibold text-black">Item Details :</h1>
                            <div class="mt-1 border-2 border-gray-300 rounded-lg ">
                                <table class="overflow-hidden " style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th scope="col"
                                                class="px-2 text-[10px] py-2  font-normal text-center border-r-2 border-gray-300 whitespace-nowrap">
                                                Qty
                                            </th>
                                            <th scope="col"
                                                class="px-2 py-2 text-[10px] font-normal text-center border-r-2 border-gray-300">
                                                Weight
                                            </th>
                                            <th scope="col"
                                                class="px-2 py-2 text-[10px] font-normal text-center border-r-2 border-gray-300 whitespace-nowrap">
                                                Dimension
                                            </th>
                                            <th scope="col"
                                                class="px-2 py-2 text-[10px] font-normal text-center border-r-2 border-gray-300 ">
                                                Unit of Measurement
                                            </th>
                                            <th scope="col"
                                                class="px-2 py-2 text-[10px] font-normal text-center border-r-2 border-gray-300">
                                                Measurement Type
                                            </th>
                                            <th scope="col"
                                                class="px-2 py-2 text-[10px] font-normal text-center border-r-2 border-gray-300 whitespace-nowrap">
                                                Type of Packaging
                                            </th>
                                            <th scope="col"
                                                class="px-2 py-2 text-[10px] font-normal text-center border-r-2 border-gray-300">
                                                For Crating
                                            </th>
                                            <th scope="col"
                                                class="px-2 py-2 text-[10px] font-normal text-center border-r-2 border-gray-300">
                                                Crating Status
                                            </th>
                                            <th
                                                class="px-2 py-2 text-[10px] font-normal text-center border-r-2 border-gray-300">
                                                Crating Type
                                            </th>
                                            <th scope="col" class="px-2 py-2 text-[10px] font-normal text-center">
                                                CWT
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="divide-y-4 divide-white ">
                                        <tr class="text-xs text-center border-gray-300 ">
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7; ">
                                                1
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                35
                                            </td>
                                            <td class="px-2 py-1 text-left border-r-2 border-gray-300" style="background-color: #F7F7F7;">
                                                25x25x25
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                in
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Per Piece
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crate
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Yes
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crated
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                SCRT
                                            </td>
                                            <td class="px-2 py-1 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                0.009
                                            </td>
                                        </tr>
                                        {{-- ///////////////////////////////////////////////////////////////////=== DELETE THIS WHEN ADDING FOREACH ===///////////////////////////////////////////////////////////////// --}}
                                        <tr class="text-xs text-center border-gray-300 ">
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7; ">
                                                1
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                35
                                            </td>
                                            <td class="px-2 py-1 text-left border-r-2 border-gray-300" style="background-color: #F7F7F7;">
                                                25x25x25
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                in
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Per Piece
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crate
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Yes
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crated
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                SCRT
                                            </td>
                                            <td class="px-2 py-1 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                0.009
                                            </td>
                                        </tr>
                                        <tr class="text-xs text-center border-gray-300 ">
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7; ">
                                                1
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                35
                                            </td>
                                            <td class="px-2 py-1 text-left border-r-2 border-gray-300" style="background-color: #F7F7F7;">
                                                25x25x25
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                in
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Per Piece
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crate
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Yes
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crated
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                SCRT
                                            </td>
                                            <td class="px-2 py-1 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                0.009
                                            </td>
                                        </tr>
                                        <tr class="text-xs text-center border-gray-300 ">
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7; ">
                                                1
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                35
                                            </td>
                                            <td class="px-2 py-1 text-left border-r-2 border-gray-300" style="background-color: #F7F7F7;">
                                                25x25x25
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                in
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Per Piece
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crate
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Yes
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crated
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                SCRT
                                            </td>
                                            <td class="px-2 py-1 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                0.009
                                            </td>
                                        </tr>
                                        <tr class="text-xs text-center border-gray-300 ">
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7; ">
                                                1
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                35
                                            </td>
                                            <td class="px-2 py-1 text-left border-r-2 border-gray-300" style="background-color: #F7F7F7;">
                                                25x25x25
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                in
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Per Piece
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crate
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Yes
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crated
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                SCRT
                                            </td>
                                            <td class="px-2 py-1 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                0.009
                                            </td>
                                        </tr>
                                        <tr class="text-xs text-center border-gray-300 ">
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7; ">
                                                1
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                35
                                            </td>
                                            <td class="px-2 py-1 text-left border-r-2 border-gray-300" style="background-color: #F7F7F7;">
                                                25x25x25
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                in
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Per Piece
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crate
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Yes
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crated
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                SCRT
                                            </td>
                                            <td class="px-2 py-1 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                0.009
                                            </td>
                                        </tr>
                                        <tr class="text-xs text-center border-gray-300 ">
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7; ">
                                                1
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                35
                                            </td>
                                            <td class="px-2 py-1 text-left border-r-2 border-gray-300" style="background-color: #F7F7F7;">
                                                25x25x25
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                in
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Per Piece
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crate
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Yes
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crated
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                SCRT
                                            </td>
                                            <td class="px-2 py-1 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                0.009
                                            </td>
                                        </tr>
                                        <tr class="text-xs text-center border-gray-300 ">
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7; ">
                                                1
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                35
                                            </td>
                                            <td class="px-2 py-1 text-left border-r-2 border-gray-300" style="background-color: #F7F7F7;">
                                                25x25x25
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                in
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Per Piece
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crate
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Yes
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crated
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                SCRT
                                            </td>
                                            <td class="px-2 py-1 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                0.009
                                            </td>
                                        </tr>
                                        <tr class="text-xs text-center border-gray-300 ">
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7; ">
                                                1
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                35
                                            </td>
                                            <td class="px-2 py-1 text-left border-r-2 border-gray-300" style="background-color: #F7F7F7;">
                                                25x25x25
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                in
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Per Piece
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crate
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Yes
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                Crated
                                            </td>
                                            <td class="px-2 py-1 border-r-2 border-gray-300 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                SCRT
                                            </td>
                                            <td class="px-2 py-1 whitespace-nowrap" style="background-color: #F7F7F7;">
                                                0.009
                                            </td>
                                        </tr>
                                        


                                        {{-- ///////////////////////////////////////////////////////////////////=== DELETE THIS WHEN ADDING FOREACH ===///////////////////////////////////////////////////////////////// --}}

                                    </tbody>
                                </table>
                                <div
                                    class="col-span-12 p-2 px-4 bg-white border-2 border-blue-800 rounded-bl-lg rounded-br-lg text-blue">
                                    <div class="flex justify-center space-x-6">
                                        <span class="pt-0.5 font-semibold text-black">TOTAL:</span>
                                        <span class="pt-1 text-sm font-medium text-black">Quantity: <span
                                                class="text-blue-800">5</span></span>
                                        <span class="pt-1 text-sm font-medium text-black">Weight: <span
                                                class="text-blue-800">158</span></span>
                                        <span class="pt-1 text-sm font-medium text-black">Volume: <span
                                                class="text-blue-800">0.045</span></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-span-4">
                    <div class="grid grid-cols-4">
                        <div class="col-span-4 border shadow-md">
                            <div class="col-span-12 px-4 bg-gray-200 border-b-2 border-gray-300 ">
                                <span class="w-full text-base font-medium text-gray-500 bg-gray-200" style="">
                                    BREAKDOWN OF FREIGHT CHARGES
                                </span>
                            </div>
                            <div class="p-4 pt-0">
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Weight Charge :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left: ">
                                            <div>
                                                1,510.95
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                AWB Fee :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                100.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Valuation :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                10.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                COD Charges :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Insurance :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                245.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Handling Fee :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                5.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Doc Fee :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                OPA Fee :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                ODA Fee :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Crating Fee :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Equipment Rental :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Lashing :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400 whitespace-nowrap">
                                            <div>
                                                Manpower/Special Handling :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400 whitespace-nowrap">
                                            <div>
                                                Dangerous Goods Fee :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Trucking : :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Perishable Fee :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Packaging Fee :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Other Charges :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Subtotal :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                2,240.95
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Promo Code :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                -
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Discount Percentage :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0%
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full mt-1 text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                Discount Amount :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                0.00
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="w-full text-xs text-gray-500">
                                    <tr>
                                        <td class="w-[40%] font-medium text-gray-400">
                                            <div>
                                                EVAT :
                                            </div>
                                        </td>
                                        <td class="font-semibold text-right text-black" style="padding-left:">
                                            <div>
                                                268.91
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div
                                class="col-span-12 p-2 px-4 bg-white border-2 border-blue-800 rounded-bl-lg rounded-br-lg text-blue">
                                <div class="flex justify-between">
                                    <span class="font-semibold text-black">Grand Total:</span>
                                    <span class="font-medium">2509.86</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        
            <div class="grid grid-cols-12 bg-white rounded-lg shadow-md ">
                <div class="col-span-12 mt-10">
                    <div class="flex justify-center space-x-10">
                        <span class="text-xs font-semibold text-center text-black" style="width: 20%">Mon
                            Manangan</span>
                        <span class="text-xs font-semibold text-center text-black" style="width: 20%"></span>
                        <span class="text-xs font-semibold text-center text-black" style="width: 20%"></span>
                        <span class="text-xs font-semibold text-center text-black" style="width: 20%"></span>
                    </div>
                    <div class="flex justify-center space-x-10">
                        <hr class="text-black border border-gray-300" style="width: 20%">
                        <hr class="text-black border border-gray-300" style="width: 20%">
                        <hr class="text-black border border-gray-300" style="width: 20%">
                        <hr class="text-black border border-gray-300" style="width: 20%">
                    </div>
                    <div class="flex justify-center space-x-10">
                        <span class="text-[10px] text-center text-black" style="width: 20%">Shipper's Name &
                            Signature</span>
                        <span class="text-[10px] text-center text-black" style="width: 20%">Picked-up By</span>
                        <span class="text-[10px] text-center text-black" style="width: 20%">Delivered By</span>
                        <span class="text-[10px] text-center text-black" style="width: 20%">Received in Good
                            Quantity/Condition By</span>
                    </div>
                </div>

                <div class="col-span-12 mt-8">
                    <hr class="text-black border border-gray-300">
                </div>

                <div class="col-span-12 px-4 py-2 mt-2">
                    <div class="flex text-xs">
                        <span><b>IMPORTANT NOTICE :</b> This serves as a system generated supplemental computation sheet
                            of CaPEx House Airwaybill/Waybill indicated herein. This, however,
                            does not supersede or disregard any agreement duty signed by the Shipper and Cargo Padala
                            Express Forwarding Services Corporation
                            (CaPEx) checker/representative.</span>
                    </div>
                </div>
            </div>
            

        </div>

</body>

<script type="text/javascript">
    window.onload = function() {
        window.print();

        setTimeout(function() {
            window.close();
        }, 3000);
    };
</script>
