@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('oims.order-management.transaction-entry.create', ['id' => $id])
        </div>
    </div>
@endsection
