@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('oims.order-management.team-route-assignment.team-mgmt.index')
        </div>
    </div>
@endsection
