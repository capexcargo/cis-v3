<!DOCTYPE html>
<html :class="{ 'theme-dark': dark }" x-data="data()" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Team Route Assignment</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    {{-- <script src="https://cdn.tailwindcss.com"></script> --}}
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style type="text/css">
        body {
            font-family: 'Poppins';
        }
    </style>
</head>

{{-- <body onload="print()" class=""> --}}

<body class="">
    <div id="" class="border-2 border-black ">
        {{-- @dd($getuserbranch) --}}

        <div class="grid grid-cols-12 pt-6">
            <div class="col-span-4">
                <span class=""><img class="pl-6 h-14" src="
       /images/logo/capex-logo.png"
                        alt="VeMoBro_logo"></span>
            </div>
            <div class="col-span-8 mt-4">
                <span class="text-2xl font-bold text-center text-blue">Team Route
                    Assignment</span>
            </div>
        </div>
        <div class="grid grid-cols-12 pl-6 mt-6 text-sm">
            <div class="col-span-3">
                <table class="">
                    <tr>
                        <td class="font-semibold">Date :</td>
                        <td class="font-semibold">{{ date('F-d-Y') }}</td>
                    </tr>
                </table>
            </div>
            <div class="col-span-3">
                <table class="">
                    <tr>
                        <td class="font-semibold">Day :</td>
                        <td class="font-semibold uppercase">{{ date('l') }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="grid grid-cols-12">
            <div class="col-span-12 p-6 pt-1">
                <div class="grid grid-cols-12 gap-x-2">
                    <div class="col-span-2 p-3 bg-white border rounded-t-md">
                        <div class="grid justify-center">
                            <h1 class="text-xl font-semibold text-center text-blue">
                                {{ $totalTRK }}
                            </h1>
                            <h1 class="text-xs font-semibold text-center">TRUCKS</h1>
                        </div>
                    </div>

                    <div class="col-span-2 p-3 ml-2 bg-white border rounded-t-md">
                        <div class="grid justify-center">
                            <h1 class="text-xl font-semibold text-center text-blue">
                                {{ $totalMT }}
                            </h1>
                            <h1 class="text-xs font-semibold text-center">MOTORCYCLES</h1>
                        </div>
                    </div>

                    <div class="col-span-2 p-3 ml-2 bg-white border rounded-t-md">
                        <div class="grid justify-center">
                            <h1 class="text-xl font-semibold text-center text-blue">
                                {{ $totalFTE }}
                            </h1>
                            <h1 class="text-xs font-semibold text-center">FTE</h1>
                        </div>
                    </div>

                    <div class="col-span-2 p-3">
                        <div class="grid justify-center">
                        </div>
                    </div>
                    <div class="col-span-2 p-3">
                        <div class="grid justify-center">
                        </div>
                    </div>
                    <div class="col-span-2 p-3">
                        <div class="grid justify-center">
                        </div>
                    </div>

                    <div class="col-span-2 bg-white rounded-b-md">
                        <div class="grid grid-cols-2">
                            <div class="grid justify-center px-6 border-2">
                                <h1 class="text-lg font-semibold text-center" style="color: #18EB00;">
                                    {{ $totalTRKActive }}
                                </h1>
                                <h1 class="text-xs font-normal text-center">Active</h1>
                            </div>
                            <div class="grid justify-center px-4 border-2">
                                <h1 class="text-lg font-semibold text-center " style="color: #FF0000">
                                    {{ $totalTRKInactive }}
                                </h1>
                                <h1 class="text-xs font-normal text-center">Inactive</h1>
                            </div>
                        </div>
                    </div>

                    <div class="col-span-2 ml-2 bg-white rounded-b-md">
                        <div class="grid grid-cols-2">
                            <div class="grid justify-center px-6 border-2">
                                <h1 class="text-lg font-semibold text-center" style="color: #18EB00;">
                                    {{ $totalMTActive }}
                                </h1>
                                <h1 class="text-xs font-normal text-center">Active</h1>
                            </div>
                            <div class="grid justify-center px-4 border-2">
                                <h1 class="text-lg font-semibold text-center" style="color: #FF0000">
                                    {{ $totalMTInactive }}
                                </h1>
                                <h1 class="text-xs font-normal text-center">Inactive</h1>
                            </div>
                        </div>
                    </div>

                    <div class="col-span-2 ml-2 bg-white rounded-b-md">
                        <div class="grid grid-cols-2">
                            <div class="grid justify-center px-6 border-2">
                                <h1 class="text-lg font-semibold text-center " style="color: #18EB00;">
                                    {{ $totalFTEActive }}
                                </h1>
                                <h1 class="text-xs font-normal text-center">Active</h1>
                            </div>
                            <div class="grid justify-center px-4 border-2">
                                <h1 class="text-lg font-semibold text-center" style="color: #FF0000">
                                    {{ $totalFTEInactive }}
                                </h1>
                                <h1 class="text-xs font-normal text-center">Inactive</h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="grid grid-cols-12 gap-4 mt-2">

                    <div class="col-span-8 text-xs bg-white border-2 rounded-md">
                        <x-table.table class="overflow-hidden text-xs text-center">
                            <x-slot name="thead" style="text-align: center;" class="text-xs">
                                <tr class="">
                                    <x-table.th name="" style="padding-left:;" />
                                    <x-table.th colspan="3" name="No. of Vehicles" class="border-2"
                                        style="padding-left:20%;height:; background-color: #F2F2F2; color: #8F8F8F;" />
                                    <x-table.th name="" style="padding-left:;" />
                                </tr>
                                <x-table.th name="ROUTE CATEGORY" style="padding-left:; " />
                                <x-table.th name="MOTORCYCLE"
                                    style="padding-left:4%; border-left:1px solid #8F8F8F; " />
                                <x-table.th name="TRUCKS"
                                    style="padding-left:3%; border-left:1px solid #8F8F8F; border-right:1px solid #8F8F8F; " />
                                <x-table.th name="TOTAL" style="padding-left:3%;border-right:1px solid #8F8F8F; " />
                                <x-table.th name="FTE" style="padding-left:3%; " />
                            </x-slot>
                            <x-slot name="tbody" class="text-xs">

                                @foreach ($getRoutes as $v => $getRoute)
                                    <tr
                                        class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-normal">
                                        <td class="w-auto p-1 text-xs text-left whitespace-nowrap"
                                            style="padding-left:;">
                                            {{ $getRoute->name ?? '-' }}
                                        </td>
                                        <td class="w-auto p-1 text-xs whitespace-nowrap "
                                            style="padding-left:; border-left:1px solid #8F8F8F;">
                                            @if ($getCurrent != null)
                                                {{ $getRoute->motorcycle ?? '0' }}
                                            @endif
                                        </td>
                                        <td class="w-auto p-1 text-xs whitespace-nowrap"
                                            style="padding-left:; border-left:1px solid #8F8F8F; border-right:1px solid #8F8F8F;">
                                            @if ($getCurrent != null)
                                                {{ $getRoute->truck ?? '0' }}
                                            @endif
                                        </td>
                                        <td class="w-auto p-1 text-xs whitespace-nowrap"
                                            style="padding-left:;border-right:1px solid #8F8F8F;">
                                            @if ($getCurrent != null)
                                                {{ $getRoute->motorcycle + $getRoute->truck ?? '0' }}
                                            @endif
                                        </td>
                                        <td class="w-auto p-1 text-xs whitespace-nowrap" style="padding-left:;">
                                            @if ($getCurrent != null)
                                                @if (isset(
                                                        $getRoute->teamDetails[0]['team_plate_reference_count'],
                                                        $getRoute->teamDetails[0]['team_checker1_reference_count'],
                                                        $getRoute->teamDetails[0]['team_checker2_reference_count']))
                                                    {{ $getRoute->teamDetails[0]['team_driver_reference_count'] + $getRoute->teamDetails[0]['team_checker1_reference_count'] + $getRoute->teamDetails[0]['team_checker2_reference_count'] ?? '0' }}
                                                @else
                                                    0
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </x-slot>
                        </x-table.table>
                    </div>
                </div>

                <div class="mt-2 bg-white rounded-lg shadow-md">
                    <x-table.table class="overflow-hidden">
                        <x-slot name="thead" class="text-xs">
                            <x-table.th name="No." style="padding-left:;" />
                            <x-table.th name="Team Name" style="padding-left:%;" />
                            <x-table.th name="Route Category" style="padding-left:%;" />
                            <x-table.th name="Plate Number" style="padding-left:%;" />
                            <x-table.th name="Driver" style="padding-left:%;" />
                            <x-table.th name="Checker 1" style="padding-left:%;" />
                            <x-table.th name="Checker 2" style="padding-left:%;" />
                            <x-table.th name="Remarks" style="padding-left:%;" />
                        </x-slot>
                        <x-slot name="tbody">
                            {{-- @dd($team_routes) --}}
                            @foreach ($team_routes as $i => $team_route)
                                @if (isset($team_routes[$i]->teamDetails->teamIdReference->name))
                                    <tr
                                        class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-normal">
                                        <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:2%;">
                                            <p class="text-xs">
                                                {{ ($team_routes->currentPage() - 1) * $team_routes->links()->paginator->perPage() + $loop->iteration }}
                                            </p>
                                        </td>
                                        <td class="w-1/5 p-1 whitespace-nowrap" style="padding-left:1.5%;">
                                            <p class="text-xs">
                                                {{ $team_routes[$i]->teamDetails->teamIdReference->name ?? '-' }}
                                            </p>
                                        </td>
                                        <td class="w-1/5 p-1 whitespace-nowrap" style="padding-left:1.5%;">
                                            <p class="text-xs">
                                                {{ $team_routes[$i]->teamDetails->teamRouteReference->name ?? '-' }}
                                            </p>
                                        </td>
                                        <td class="w-1/5 p-1 whitespace-nowrap" style="padding-left:1.5%;">
                                            <p class="text-xs">{{ $team_routes[$i]->plate_no }}</p>
                                        </td>


                                        <td class="w-1/5 p-1 whitespace-nowrap " style="padding-left:1.5%;">
                                            @if (isset($team_routes[$i]->teamDetails))
                                                @if (isset($team_routes[$i]->teamDetails->teamDriverReference->userDetails->timeLog[0]['date']) &&
                                                        $team_routes[$i]->teamDetails->teamDriverReference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                                    <p class="text-xs">
                                                        {{ $team_routes[$i]->teamDetails->teamDriverReference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamDriverReference->userDetails->last_name ?? '-' }}
                                                    </p>
                                                @else
                                                    <p class=" text-red" style="">
                                                    <p class="text-xs text-red">
                                                        {{ $team_routes[$i]->teamDetails->teamDriverReference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamDriverReference->userDetails->last_name ?? '-' }}
                                                    </p>
                                                    </p>
                                                @endif
                                            @else
                                                -
                                            @endif
                                        </td>

                                        <td class="w-1/5 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            @if (isset($team_routes[$i]->teamDetails))
                                                @if (isset($team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->timeLog[0]['date']) &&
                                                        $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                                    <p class="text-xs">
                                                        {{ $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->last_name ?? '-' }}
                                                    </p>
                                                @else
                                                    <p class="text-red">
                                                    <p class="text-xs text-red">
                                                        {{ $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->last_name ?? '-' }}
                                                    </p>
                                                    </p>
                                                @endif
                                            @else
                                                <p class="text-center">-</p>
                                            @endif
                                        </td>

                                        <td class="w-1/5 p-1 whitespace-nowrap" style="padding-left:1.5%;">
                                            @if (isset($team_routes[$i]->teamDetails))
                                                @if (isset($team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->timeLog[0]['date']) &&
                                                        $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                                    @if ($team_routes[$i]->teamDetails->checker2_id != null)
                                                        <p class="text-xs">
                                                            {{ $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->last_name ?? '-' }}
                                                        </p>
                                                    @else
                                                        <p class="text-center">-</p>
                                                    @endif
                                                @else
                                                    <p class="text-red">
                                                        @if ($team_routes[$i]->teamDetails->checker2_id != null)
                                                            <p class="text-xs text-red">
                                                                {{ $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->last_name ?? '-' }}
                                                            </p>
                                                        @else
                                                            <p class="text-center">-</p>
                                                        @endif
                                                    </p>
                                                @endif
                                            @else
                                                <p class="text-center">-</p>
                                            @endif
                                        </td>

                                        <td class="w-1/5 p-1 whitespace-nowrap" style="padding-left:1.5%;">
                                            @if (isset($team_routes[$i]->teamDetails))
                                                @if (isset($team_routes[$i]->teamDetails))
                                                    @if ($team_routes[$i]->teamDetails->remarks != null)
                                                        <p class="text-xs">
                                                            {{ $team_routes[$i]->teamDetails->remarks ?? '-' }}
                                                        </p>
                                                    @else
                                                        <p class="text-center">-</p>
                                                    @endif
                                                @endif
                                            @else
                                                <p class="text-center">-</p>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </x-slot>
                    </x-table.table>

                </div>
                <span class="flex justify-center py-2">
                    <hr class="w-full text-black border-2 border-blue">
                </span>
                <div class="flex justify-between text-xs text-blue">
                    <table class="">
                        <tr>
                            <td class="font-semibold text-justify" style="margin-top: -4%">Main
                                Office :</td>
                            <td class="">
                                Building 9A, Salem International
                            </td>
                        </tr>
                        <tr>
                            <td class="font-semibold whitespace-nowrap"> </td>
                            <td class="">Commercial Complex, Domestic
                                Road, Pasay City</td>
                        </tr>
                        <tr>
                            <td class="font-semibold whitespace-nowrap"> </td>
                            <td class="">1301, Philippines
                        </tr>
                    </table>
                    <table class="">
                        <tr>
                            <td class="font-semibold">Telephone :</td>
                            <td>(+632)8396-8888</td>
                        </tr>
                        <br>
                        <tr>
                            <td class="font-semibold">Website :</td>
                            <td>www.capex.com.ph</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>


</body>

<script type="text/javascript">
    window.onload = function() {
        window.print();

        setTimeout(function() {
            window.close();
        }, 3000);
    };
</script>
