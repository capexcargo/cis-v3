@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.reports.quota-reports.index')
        </div>
    </div>
@endsection
