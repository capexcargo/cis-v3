@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.reports.booking-reports.index')
        </div>
    </div>
@endsection
