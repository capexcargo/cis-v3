@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.sales.rate-calculator.index')
        </div>
    </div>
@endsection
