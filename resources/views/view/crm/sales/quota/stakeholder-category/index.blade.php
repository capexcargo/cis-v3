@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.sales.quota.stakeholder-category.index')
        </div>
    </div>
@endsection
