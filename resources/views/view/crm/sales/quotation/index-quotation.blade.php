@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.sales.quotation.index-quotation', ['id' => $id, 'from_sr' => $from_sr])
        </div>
    </div>
@endsection
