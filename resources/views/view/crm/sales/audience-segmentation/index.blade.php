@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.sales.audience-segmentation.index')
        </div>
    </div>
@endsection