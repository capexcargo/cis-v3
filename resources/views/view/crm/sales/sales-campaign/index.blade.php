@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.sales.sales-campaign.index')
        </div>
    </div>
@endsection
