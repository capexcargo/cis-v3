@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.sales.rate-calculator.charges-management.index')
        </div>
    </div>
@endsection
