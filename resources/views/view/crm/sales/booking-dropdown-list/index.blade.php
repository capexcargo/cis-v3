@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.sales.booking-dropdown-list.index')
        </div>
    </div>
@endsection
