@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.sales.leads.edit', ['id' => $id])
        </div>
    </div>
@endsection
