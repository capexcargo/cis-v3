@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.sales.leads.qualification-mgmt.index')
        </div>
    </div>
@endsection
