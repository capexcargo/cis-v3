@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.sales.booking-mgmt.index')
        </div>
    </div>
@endsection
