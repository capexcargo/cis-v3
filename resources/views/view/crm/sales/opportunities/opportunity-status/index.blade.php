@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.sales.opportunities.opportunity-status.index')
        </div>
    </div>
@endsection
