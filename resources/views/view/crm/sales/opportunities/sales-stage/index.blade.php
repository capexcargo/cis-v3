@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.sales.opportunities.sales-stage.index')
        </div>
    </div>
@endsection
