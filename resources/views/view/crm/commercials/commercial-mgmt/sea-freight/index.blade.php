@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.commercials.commercial-mgmt.sea-freight.index')
        </div>
    </div>
@endsection
