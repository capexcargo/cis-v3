@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.commercials.commercial-mgmt.air-freight-premium.index')
        </div>
    </div>
@endsection
