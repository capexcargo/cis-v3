@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.commercials.ancillary.index')
        </div>
    </div>
@endsection
