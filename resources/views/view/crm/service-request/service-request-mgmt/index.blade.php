@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.service-request.service-request-mgmt.index')
        </div>
    </div>
@endsection
