@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.service-request.knowledge.sales-coach.index')
        </div>
    </div>
@endsection
