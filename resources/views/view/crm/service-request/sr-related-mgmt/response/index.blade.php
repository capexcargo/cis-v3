@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.service-request.sr-related-mgmt.response.index')
        </div>
    </div>
@endsection
