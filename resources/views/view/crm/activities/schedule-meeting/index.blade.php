@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.activities.schedule-meeting.index')
        </div>
    </div>
@endsection
