@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.contracts.accounts-application.index')
        </div>
    </div>
@endsection
