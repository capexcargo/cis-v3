@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.contracts.requirements-mgmt.index')
        </div>
    </div>
@endsection
