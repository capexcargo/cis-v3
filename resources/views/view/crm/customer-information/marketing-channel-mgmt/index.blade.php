@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.customer-information.marketing-channel-mgmt.index')
        </div>
    </div>
@endsection
