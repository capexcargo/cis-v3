@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.customer-information.customer-data-mgmt.search-result', ['id' => $id])
        </div>
    </div>
@endsection
