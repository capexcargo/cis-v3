@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.customer-information.customer-onboarding.index')
        </div>
    </div>
@endsection