@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('crm.customer-information.industry-mgmt.index')
        </div>
    </div>
@endsection