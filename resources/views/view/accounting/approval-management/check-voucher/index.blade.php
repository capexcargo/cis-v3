@extends('layouts/app')
@section('content')
    <div class="container grid mx-auto">
        <div class="overflow-hidden">
            @livewire('accounting.approval-management.check-voucher.index')
        </div>
    </div>
@endsection
