@extends('layouts/app')
@section('content')
    <div class="container grid mx-auto">
        <div class="overflow-hidden">
            @livewire('accounting.liquidation-management.index')
        </div>
    </div>
@endsection
