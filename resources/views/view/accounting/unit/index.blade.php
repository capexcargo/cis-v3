@extends('layouts/app')
@section('content')
    <div class="container grid mx-auto">
        <div class="overflow-hidden">
            @livewire('accounting.unit.index')
        </div>
    </div>
@endsection