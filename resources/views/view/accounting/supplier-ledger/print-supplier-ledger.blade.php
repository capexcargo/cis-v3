<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Purchase Order</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <style>
        footer {
            page-break-after: always;
        }

    </style>
</head>

<body>
    <div class="p-1">
        @foreach ($suppliers as $supplier)
            <table style="width:100%">
                <tr>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <td>
                        <img class="h-20 w-100" src="{{ url('/images/logo/capex-logo.png') }}" />
                    </td>
                    <td class="text-right">
                        <div class="text-2xl font-semibold">
                            Supplier Ledger
                        </div>
                    </td>
                </tr>
            </table>
            <div class="grid grid-cols-2 border-2 border-black divide-x divide-black">
                <div class="divide-y divide-black ">
                    <div class="pl-1 pr-1">Name:
                        {{ $supplier->first_name . ' ' . $supplier->last_name }}</div>
                    <div class="pl-1 pr-1">Company Name: {{ $supplier->company }}</div>
                    <div class="pl-1 pr-1">Address: {{ $supplier->address }}</div>

                </div>
                <div class="divide-y divide-black">
                    <div class="pl-1 pr-1">Contact No:
                        {{ $supplier->mobile_number . ' / ' . $supplier->telephone_number }}
                    </div>
                    <div class="pl-1 pr-1">Business Type: {{ $supplier->type->display }}</div>
                    <div class="pl-1 pr-1">Industy: {{ $supplier->industry->display }}</div>
                </div>
            </div>
            <div class="grid grid-cols-4 mt-3 border-2 border-black divide-x divide-y divide-black ">
                <div class="pl-1 pr-1">Reference No</div>
                <div class="pl-1 pr-1">Requester</div>
                <div class="pl-1 pr-1">Total Amount</div>
                <div class="pl-1 pr-1">Created At</div>

                @foreach ($supplier->requestForPayments as $request_for_payment)
                    <div class="pl-1 pr-1">{{ $request_for_payment->reference_id }}</div>
                    <div class="pl-1 pr-1">{{ $request_for_payment->user->name }}</div>
                    <div class="pl-1 pr-1">{{ number_format($request_for_payment->amount, 2) }}</div>
                    <div class="pl-1 pr-1"> {{ date('M. d,  Y', strtotime($request_for_payment->created_at)) }}
                    </div>
                @endforeach
                <div class="col-span-2"></div>
                <div class="col-span-2">
                    <p class="pl-1 pr-1 text-right">Total: Php
                        {{ number_format($supplier->request_for_payments_sum_amount, 2) }}</p>
                </div>
            </div>
            <footer></footer>
        @endforeach
    </div>
    <script>
        window.print();
    </script>
</body>

</html>
