@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('accounting.account-management.index')
        </div>
    </div>
@endsection
