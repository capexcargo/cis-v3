@extends('layouts/app')
@section('content')
    <div class="container grid mx-auto">
        <div class="overflow-hidden">
            @livewire('accounting.budget-management.budget-loa.index')
        </div>
    </div>
@endsection
