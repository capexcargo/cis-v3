@extends('layouts/app')
@section('content')
    <div class="container grid mx-auto">
        <div class="overflow-hidden">
            @livewire('accounting.supplier-management.supplier.index')
        </div>
    </div>
@endsection
