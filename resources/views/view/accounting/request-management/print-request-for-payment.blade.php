<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Untitled Document</title>
    <style type="text/css">
        .style3 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 10px;
            padding: 0 0 0 10px
        }

        .style5 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            font-weight: bold;
            padding: 5px;
        }

        .style6 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 9px;
        }

        .style8 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            font-weight: bold;
            padding: 0 0 0 10px
        }

        .style9 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: bold;
            padding: 10 10 10 10px;
            margin-top: 10px;
            margin-bottom: 10px
        }

        .style10 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            padding: 0 0 0 5px;
            text-align: left;
        }

        body {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }

        .dim_txt_d {

            border: none;
            cursor: default;
            color: 333;
            background-color: #FFFFFF;

        }

        @media print {
            footer {
                page-break-after: always;
            }
        }

        table {
            border-collapse: collapse !important;
            padding: 5px;

        }
    </style>
</head>

<body>

    @foreach ($request_for_payments as $request_for_payment)
        <table i width="100%" cellpadding="0" cellspacing="3px">
            <tr>
                <td width="100%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr style="border-radius: 25px; padding: 20px;">
                            <td width="33%"></td>
                            <td width="33%">
                                <div align="center"><span class="style5">CARGO PADALA EXPRESS</span><br />
                                    <span class="style6">FORWARDING SERVICES CORPORATION</span>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>

                <td width="100%" border="1">
                    <table i width="100%" border="1" cellpadding="0" cellspacing="3px">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="15px" cellpadding="0">
                                    <tr>
                                        <td width="33%"></td>
                                        <td width="33%">
                                            <div align="center"><span class="style5">REQUEST FOR PAYMENT
                                                    (RFP)</span>
                                            </div>
                                        </td>
                                        <td width="33%"
                                            style="font-family: Arial, Helvetica, sans-serif; font-size: 18px; padding:0 0 0 10px ">
                                            <div align="right">Request Ref. No. :
                                                <strong> &nbsp;
                                                    {{ $request_for_payment->reference_id }}
                                                </strong>&nbsp;&nbsp;
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%" border="1">
                    <table i width="100%" border="1" cellpadding="0" cellspacing="3px">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="15px" cellpadding="0">
                                    <tr>
                                        <td width="20%" class="style5" style="font-size:12px">PAYEE</td>
                                        <td width="20%"
                                            style=" text-decoration: underline;  display: inline-block;
                                            text-decoration: none;
                                            border-bottom: 1px solid black;
                                            margin-bottom: -1px;
                                            width: 200px;">
                                            <div align="left" style="font-size:12px" class="style5">
                                                {{ $request_for_payment->payee->company }}
                                            </div>
                                        </td>
                                        <td width="20%">
                                            <div align="right" class="style5" style="font-size:12px">REQ. DATE
                                            </div>
                                        </td>
                                        <td width="30%"
                                            style=" text-decoration: underline;  display: inline-block;
                                            text-decoration: none;
                                            border-bottom: 1px solid black;
                                            margin-bottom: -1px;
                                            width: 100px;">
                                            <div align="left">
                                                <span class="style5"
                                                    style="font-size:12px">{{ date('M. d, Y', strtotime($request_for_payment->created_at)) }}
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" class="style5" style="font-size:12px">SUB-PAYEE</td>
                                        <td width="20%"
                                            style=" text-decoration: underline;  display: inline-block;
                                            text-decoration: none;
                                            border-bottom: 1px solid black;
                                            margin-bottom: -1px;
                                            width: 200px;">
                                            <div align="left" style="font-size:12px" class="style5">
                                                {{ $request_for_payment->subpayee }}
                                            </div>
                                        </td>
                                        <td width="30%">
                                            <div align="right" class="style5" style="font-size:12px">REQ. BRANCH
                                            </div>
                                        </td>
                                        <td width="20%"
                                            style=" text-decoration: underline;  display: inline-block;
                                            text-decoration: none;
                                            border-bottom: 1px solid black;
                                            margin-bottom: -1px;
                                            width: 80px;">
                                            <div align="left">
                                                <span class="style5"
                                                    style="font-size:12px">{{ $request_for_payment->branch->display }}
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" class="style5" style="font-size:12px">PRIORITY</td>
                                        <td width="50%"
                                            style=" text-decoration: underline;  display: inline-block;
                                            text-decoration: none;
                                            border-bottom: 1px solid black;
                                            margin-bottom: -1px;
                                            width: 220px;">
                                            <div align="left" style="font-size:12px" class="style5">
                                                {{ $request_for_payment->priority->display }}</div>
                                        </td>
                                        <td width="10%">
                                            <div align="right" class="style5" style="font-size:12px">TOTAL AMOUNT (P)
                                            </div>
                                        </td>
                                        <td width="20px"
                                            style=" text-decoration: underline;  display: inline-block;
                                            text-decoration: none;
                                            border-bottom: 1px solid black;
                                            margin-bottom: -1px;
                                            width: 120px;">
                                            <div align="left">
                                                <span class="style5" style="font-size:16px">
                                                    {{ number_format($request_for_payment->amount, 2) }}</span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" class="style5" style="font-size:12px">DATE NEEDED
                                        </td>
                                        <td width="60%"
                                            style=" text-decoration: underline;  display: inline-block;
                                            text-decoration: none;
                                            border-bottom: 1px solid black;
                                            margin-bottom: -1px;
                                            width: 220px;">
                                            <div align="left" style="font-size:12px" class="style5">
                                                {{ date('M. d, Y', strtotime($request_for_payment->date_needed)) }}
                                            </div>
                                        </td>
                                        {{-- <td width="20px">
                                            <div align="right" class="style5" style="font-size:12px">DATE NEEDED
                                            </div>
                                        </td>
                                        <td width="20px"
                                            style=" text-decoration: underline;  display: inline-block;
                                            text-decoration: none;
                                            border-bottom: 1px solid black;
                                            margin-bottom: -1px;
                                            width: 80px;">
                                            <div align="left"><span class="style5"
                                                    style="font-size:12px">{{ date('M. d, Y', strtotime($request_for_payment->date_needed)) }}</span>
                                            </div>
                                        </td> --}}
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <table i width="100%" style="border: 1px solid black">
                                                <tr>
                                                    <thead>
                                                        <tr cellspacing="15px">
                                                            <td style="border: 1px solid black" cellspacing="15px"
                                                                class="style8">PARTICULARS / DETAILS</td>
                                                        </tr>
                                                        @if ($request_for_payment->type_id == 1)
                                                            <tr>
                                                                <td width="70%" style="border: 1px solid black">
                                                                    <table
                                                                        style=" font-size: 12px;margin-top: 20px;margin-bottom: 20px;margin-left: 20px;margin-right: 20px"
                                                                        align="center" width="100%">
                                                                        <tr>
                                                                            <td class="style9"><strong>Request
                                                                                    Description </strong></td>
                                                                            <td class="style9"><strong>Plate
                                                                                    No.</strong></td>
                                                                            <td class="style9"><strong>Labor
                                                                                    Cost
                                                                                </strong></td>
                                                                            <td class="style9"><strong>Unit Cost
                                                                                </strong></td>
                                                                            <td class="style9"><strong>Quantity
                                                                                </strong></td>
                                                                            <td class="style9"><strong>Amount
                                                                                </strong>
                                                                            </td>
                                                                        </tr>
                                                                        @foreach ($request_for_payment->requestForPaymentDetails as $request_for_payment_detail)
                                                                            <tr>
                                                                                <td class="style10">
                                                                                    {{ $request_for_payment_detail->particulars }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ $request_for_payment_detail->plate_no }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ number_format($request_for_payment_detail->labor_cost, 2) }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ number_format($request_for_payment_detail->unit_cost, 2) }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ $request_for_payment_detail->quantity }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ number_format($request_for_payment_detail->amount, 2) }}
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        @elseif($request_for_payment->type_id == 2)
                                                            <tr>
                                                                <td width="70%" style="border: 1px solid black">
                                                                    <table
                                                                        style=" font-size: 12px;margin-top: 20px;margin-bottom: 20px;margin-left: 20px;margin-right: 20px"
                                                                        align="center" width="100%">
                                                                        <tr>
                                                                            <td class="style9"><strong>Reference
                                                                                    No.</strong></td>
                                                                            <td class="style9"><strong>SOA
                                                                                    No.</strong></td>
                                                                            <td class="style9"><strong>Trucking
                                                                                    Amount</strong></td>
                                                                            <td class="style9"><strong>Freight
                                                                                    Amount</strong></td>
                                                                            <td class="style9">
                                                                                <strong>Allowance</strong>
                                                                            </td>
                                                                        </tr>
                                                                        @foreach ($request_for_payment->requestForPaymentDetails as $request_for_payment_detail)
                                                                            <tr>
                                                                                <td class="style10">
                                                                                    {{ $request_for_payment_detail->freight_reference_no }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ $request_for_payment_detail->soa_no }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ number_format($request_for_payment_detail->trucking_amount, 2) }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ number_format($request_for_payment_detail->freight_amount, 2) }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ number_format($request_for_payment_detail->allowance, 2) }}
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        @elseif($request_for_payment->type_id == 3)
                                                            <tr>
                                                                <td width="70%" style="border: 1px solid black">
                                                                    <table
                                                                        style=" font-size: 12px;margin-top: 20px;margin-bottom: 20px;margin-left: 20px;margin-right: 20px"
                                                                        align="center" width="100%">
                                                                        <tr>
                                                                            <td class="style9"><strong>Account
                                                                                    No.</strong></td>
                                                                            <td class="style9"><strong>Payment
                                                                                    Type</strong></td>
                                                                            <td class="style9"><strong>Invoice
                                                                                    Number</strong></td>
                                                                            <td class="style9"><strong>Invoice
                                                                                    Amount</strong></td>
                                                                            @if ($request_for_payment->requestForPaymentDetails[0]->payment_type_id == 2)
                                                                                <td class="style9"><strong>Date
                                                                                        Of
                                                                                        Transaction</strong></td>
                                                                            @endif
                                                                        </tr>
                                                                        @foreach ($request_for_payment->requestForPaymentDetails as $request_for_payment_detail)
                                                                            <tr>
                                                                                <td class="style10">
                                                                                    {{ $request_for_payment_detail->account_no }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ $request_for_payment_detail->paymentType->display }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ $request_for_payment_detail->invoice }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ number_format($request_for_payment_detail->invoice_amount, 2) }}
                                                                                </td>
                                                                                @if ($request_for_payment->requestForPaymentDetails[0]->payment_type_id == 2)
                                                                                    <td class="style10">
                                                                                        {{ date('M. d, Y', strtotime($request_for_payment_detail->transaction_date)) }}
                                                                                    </td>
                                                                                @endif
                                                                            </tr>
                                                                        @endforeach
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        @elseif($request_for_payment->type_id == 4)
                                                            <tr>
                                                                <td width="70%" style="border: 1px solid black">
                                                                    <table
                                                                        style=" font-size: 12px;margin-top: 20px;margin-bottom: 20px;margin-left: 20px;margin-right: 20px"
                                                                        align="center" width="100%">
                                                                        <tr>
                                                                            <td class="style9">
                                                                                <strong>Description</strong>
                                                                            </td>
                                                                            <td class="style9">
                                                                                <strong>{{ $request_for_payment->CAReferenceType->display ?? '' }}</strong>
                                                                            </td>
                                                                            <td class="style9"><strong>Unit
                                                                                    Cost</strong></td>
                                                                            <td class="style9">
                                                                                <strong>Quantity</strong>
                                                                            </td>
                                                                            <td class="style9">
                                                                                <strong>Amount</strong>
                                                                            </td>
                                                                        </tr>
                                                                        @foreach ($request_for_payment->requestForPaymentDetails as $request_for_payment_detail)
                                                                            <tr>
                                                                                <td class="style10">
                                                                                    {{ $request_for_payment_detail->particulars }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ $request_for_payment_detail->ca_reference_no }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ number_format($request_for_payment_detail->unit_cost, 2) }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ number_format($request_for_payment_detail->quantity, 2) }}
                                                                                </td>
                                                                                <td class="style10">
                                                                                    {{ number_format($request_for_payment_detail->amount, 2) }}
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        <tr>

                                                            <td colspan="2" style="border: 1px solid black"><strong
                                                                    class="style8">REMARKS</strong></td>
                                                        </tr>
                                                        <tr>

                                                            <td colspan="2"
                                                                style="border: 1px solid black; padding:20px"><strong
                                                                    class="style8">{{ $request_for_payment->remarks }}</strong>
                                                            </td>
                                                        </tr>
                                                        <tr>

                                                            <td colspan="2" style="border: 1px solid black"><strong
                                                                    class="style8">OTHER
                                                                    DETAILS</strong></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2" style="border: 1px solid black">
                                                                <table
                                                                    style=" font-size: 12px;margin-top: 20px;margin-bottom: 20px;margin-left: 20px;margin-right: 20px"
                                                                    align="center" width="100%">
                                                                    <tr>
                                                                        <td class="style9">
                                                                            Requesting Division </br>
                                                                            <strong>{{ $request_for_payment->budget->division->name }}</strong>
                                                                        </td>
                                                                        <td class="style9">
                                                                            Budget Source </br>
                                                                            <strong>{{ $request_for_payment->budget->source->name }}</strong>
                                                                        </td>
                                                                        <td class="style9">
                                                                            Chart Of Accounts </br>
                                                                            <strong>{{ $request_for_payment->budget->chart->name }}</strong>
                                                                        </td>
                                                                        <td class="style9">
                                                                            Coa Category </br>
                                                                            <strong>{{ $request_for_payment->budget->item }}</strong>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                    </thead>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td width="100%" border="1">
                    <table i width="100%" border="1" cellpadding="0" cellspacing="3px">
                        <tr>
                            <td class="style8"><strong> REQUESTED BY:</strong></td>
                            <td class="style8"><strong> 1ST APPROVER:</strong></td>
                            <td class="style8"><strong> 2ND APPROVER:</strong></td>
                            <td class="style8"><strong> FINAL APPROVER:</strong></td>
                        </tr>
                        <tr>
                            <td class="style8" align="left" style="padding: 10px">
                                &nbsp;<br>&nbsp;<br>{{ $request_for_payment->user->userDetails->last_name . ', ' . $request_for_payment->user->userDetails->first_name }}
                            </td>
                            <td class="style8" align="left" style="padding: 10px">
                                @if ($request_for_payment->is_approved_1 == 1)
                                    <a href="{{ Storage::disk('accounting_gcs')->url(($request_for_payment->approver1Signature->path ?? '') . ($request_for_payment->approver1Signature->name ?? '')) }}"
                                        target="_blank"><img height=50 width=120 style="width: 120px; height: 50px;"
                                            src="{{ Storage::disk('accounting_gcs')->url(($request_for_payment->approver1Signature->path ?? '') . ($request_for_payment->approver1Signature->name ?? '')) }}"></a>
                                    &nbsp;<br>{{ $request_for_payment->approver1 ? $request_for_payment->approver1->userDetails->last_name . ', ' . $request_for_payment->approver1->userDetails->first_name : 'N/A' }}
                                @else
                                    &nbsp;<br>
                                    &nbsp;<br>{{ $request_for_payment->approver1 ? $request_for_payment->approver1->userDetails->last_name . ', ' . $request_for_payment->approver1->userDetails->first_name : 'N/A' }}
                                @endif

                            </td>
                            <td class="style8" align="left" style="padding: 10px">
                                @if ($request_for_payment->is_approved_2 == 1)
                                    <a href="{{ Storage::disk('accounting_gcs')->url(($request_for_payment->approver2Signature->path ?? '') . ($request_for_payment->approver2Signature->name ?? '')) }}"
                                        target="_blank"><img height=50 width=120 style="width: 120px; height: 50px;"
                                            src="{{ Storage::disk('accounting_gcs')->url(($request_for_payment->approver2Signature->path ?? '') . ($request_for_payment->approver2Signature->name ?? '')) }}"></a>
                                    &nbsp;<br>{{ $request_for_payment->approver2 ? $request_for_payment->approver2->userDetails->last_name . ', ' . $request_for_payment->approver2->userDetails->first_name : 'N/A' }}
                                @else
                                    &nbsp;<br> &nbsp;<br>
                                    &nbsp;<br>{{ $request_for_payment->approver2 ? $request_for_payment->approver2->userDetails->last_name . ', ' . $request_for_payment->approver2->userDetails->first_name : 'N/A' }}
                                @endif


                            </td>
                            <td class="style8" align="left" style="padding: 10px">
                                @if ($request_for_payment->is_approved_3 == 1)
                                    <a href="{{ Storage::disk('accounting_gcs')->url(($request_for_payment->approver3Signature->path ?? '') . ($request_for_payment->approver3Signature->name ?? '')) }}"
                                        target="_blank"><img height=50 width=120 style="width: 120px; height: 50px;"
                                            src="{{ Storage::disk('accounting_gcs')->url(($request_for_payment->approver3Signature->path ?? '') . ($request_for_payment->approver3Signature->name ?? '')) }}"></a>
                                    &nbsp;<br>{{ $request_for_payment->approver3 ? $request_for_payment->approver3->userDetails->last_name . ', ' . $request_for_payment->approver3->userDetails->first_name : 'N/A' }}
                                @else
                                    &nbsp;<br> &nbsp;<br>
                                    &nbsp;<br>{{ $request_for_payment->approver3 ? $request_for_payment->approver3->userDetails->last_name . ', ' . $request_for_payment->approver3->userDetails->first_name : 'N/A' }}
                                @endif

                            </td>
                        </tr>
                        <tr>
                            <td class="style8" align="left" style="margin-left: 15px">Name and Signature</td>
                            <td class="style8" align="left" style="margin-left: 15px">Name and Signature</td>
                            <td class="style8" align="left" style="margin-left: 15px">Name and Signature</td>
                            <td class="style8" align="left" style="margin-left: 15px">Name and Signature</td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>
        <br>
        <table i width="100%" cellpadding="0" cellspacing="3px">
            <tr>
                <td width="100%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr style="border-radius: 25px; padding: 20px;">

                            <td width="100%" align="center"><img style="float: center;"
                                    src="{{ url('/images/logo/capex-logo.png') }}" width="100" height="45" />
                            </td>

                        </tr>
                        <tr>
                            <td width="100%" align="center">
                                <span class="style3">
                                    <br>
                                    Copyright © 2017 by CaPEx Cargo Padala Express.
                                    <br>
                                    All Rights Reserved.
                                </span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <footer></footer>
    @endforeach
    <script>
        window.print();
    </script>
</body>

</html>
