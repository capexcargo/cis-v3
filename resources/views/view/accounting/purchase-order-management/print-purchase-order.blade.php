<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Purchase Order</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <style>
        footer {
            page-break-after: always;
        }

    </style>
</head>

<body>
    <div class="p-1">
        @foreach ($purchase_orders as $purchase_order)
            <table style="width:100%">
                <tr>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <td>
                        <img class="h-20 w-100" src="{{ url('/images/logo/capex-logo.png') }}" />
                    </td>
                    <td class="text-right">
                        <div class="text-2xl font-semibold">
                            PURCHASE ORDER
                        </div>
                        <table>
                            <tr>
                                <th></th>
                            </tr>
                            <tr>
                                P.O. #: {{ $purchase_order->po_reference_no }}
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <div class="grid grid-cols-2 ">
                <div>Supplier</div>
                <div>Delivered To</div>
            </div>
            <div class="grid grid-cols-2 border-2 border-black divide-x divide-black">
                <div class="divide-y divide-black ">
                    <div class="pl-1 pr-1">Name:
                        {{ $purchase_order->supplier->first_name . ' ' . $purchase_order->supplier->last_name }}</div>
                    <div class="pl-1 pr-1">Company Name: {{ $purchase_order->supplier->company }}</div>
                    <div class="pl-1 pr-1">Street Address: {{ $purchase_order->supplier->address }}</div>
                    <div class="pl-1 pr-1">Contact No:
                        {{ $purchase_order->supplier->mobile_number . ' / ' . $purchase_order->supplier->telephone_number }}
                    </div>
                </div>
                <div class="divide-y divide-black">
                    <div class="pl-1 pr-1">Name: N/A</div>
                    <div class="pl-1 pr-1">Company Name: N/A</div>
                    <div class="pl-1 pr-1">Street Address: N/A</div>
                    <div class="pl-1 pr-1">Contact No: N/A</div>
                </div>
            </div>


            <div class="grid grid-cols-5 mt-3 border-2 border-black divide-x divide-y divide-black ">
                <div class="pl-1 pr-1">Item #</div>
                <div class="pl-1 pr-1">Description</div>
                <div class="pl-1 pr-1">Quantity</div>
                <div class="pl-1 pr-1">Unit Price</div>
                <div class="pl-1 pr-1">Total</div>
                @php
                    $total = 0;
                @endphp
                @foreach ($purchase_order->canvassingSupplierItem as $i => $canvassing_supplier_item)
                    @php
                        $total += $canvassing_supplier_item->unit_cost * $canvassing_supplier_item->quantity;
                    @endphp
                    <div class="pl-1 pr-1">{{ ($i + 1)  }}</div>
                    <div class="pl-1 pr-1">{{ $canvassing_supplier_item->supplierItem->name }}</div>
                    <div class="pl-1 pr-1">{{ $canvassing_supplier_item->quantity }}</div>
                    <div class="pl-1 pr-1">{{ $canvassing_supplier_item->unit_cost }}</div>
                    <div class="pl-1 pr-1">
                        {{ $canvassing_supplier_item->unit_cost * $canvassing_supplier_item->quantity }}</div>
                @endforeach
                <div class="col-span-3"></div>
                <div class="col-span-2">
                    <p class="pl-1 pr-1 text-right">Total: Php {{ number_format($total, 2) }}</p>
                </div>
            </div>

            <div class="grid grid-cols-2 mt-3 border-2 border-black divide-x divide-y divide-black ">
                <div>
                    <div class="pl-1 pr-1">Other Commnets or Special Instructions</div>
                </div>
                <div>
                    <div class="border-b-2 border-black ">
                        <div class="p-5">

                        </div>
                        <p class="text-center ">
                            {{ $purchase_order->canvassing->createdBy->name }}
                        </p>
                    </div>
                    <div class="flex justify-between">
                        <div class="pl-1 pr-1">Requested By</div>
                        <div class="pl-1 pr-1">Date</div>
                    </div>

                    <div class="p-5 border-b-2 border-black">

                    </div>
                    <div class="flex justify-between">
                        <div class="pl-1 pr-1">Authorized By</div>
                        <div class="pl-1 pr-1">Date</div>
                    </div>
                </div>
            </div>

            <footer></footer>
        @endforeach
    </div>
    <script>
        window.print();
    </script>
</body>

</html>
