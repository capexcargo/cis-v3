<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Voucher Print</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style type="text/css">
        .font1 {
            font-size: 14px;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
        }
        .font2 {
            font-size: 18px;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
        }
        .font3 {
            font-size: 18px;
            /* font-weight: bold; */
            font-family: Arial, Helvetica, sans-serif;

        }

        @media print {
            footer {
                page-break-after: always;
            }
        }

    </style>
</head>

<body>
    @foreach ($request_for_payments as $request_for_payment)
            {{-- <div style="display: flex;flex-direction: column;justify-content: space-between;height: 100vh;"> --}}
            <div style="display: flex;flex-direction: column;justify-content: flex-start;height: 100vh;">
                <div>
                    <div class="row">
                        <div class="col-9">
                            <span class="fw-bold font1">CARGO PADALA EXPRESS</span><br />
                            <span class="fw-bold font1">FORWARDING SERVICES CORPORATION</span>
                            <br />
                            <span class="fw-bold font1">Main Office: Building 9A, Salem International
                                Complex,<br>
                                Domestic Road, Pasay City 1301, Philippines</span>
                            <br />
                            <span class="fw-bold font1">VAT Reg. TIN: 008-022-724-000</span>
                        </div>
                        <div class="col-2">
                            <img src="{{ url('/images/logo/capex-logo.png') }}" width="150" height="70" />
                        </div>
                    </div>
                    <br>
                    <div class="mt-12 row justify-content-md-center">
                        <div class="col">
                            <span class="mt-6 h5 d-flex justify-content-center">CHECK VOUCHER</span>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col font1">RFP REFERENCE NO :</div>
                                <div class="text-right col font2 fw-bold">{{ $request_for_payment->reference_id }}</div>
                            </div>
                            <div class="row">
                                <div class="col-4 font1">DATE : </div>
                                <div class="col-8 font2 fw-bold">
                                    {{ date('M. d, Y', strtotime($request_for_payment->checkVoucher->created_at)) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4 font1">CV SERIES NO:</div>
                                <div class="col-8 font2 fw-bold">{{ $request_for_payment->checkVoucher->cv_no }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4 font1">PAY TO :</div>
                                <div class="col-8 font1 fw-bold"> {{ $request_for_payment->payee->company }}</div>
                            </div>
                            <div class="row">
                                <div class="col-4 font1">AMOUNT IN WORDS : </div>
                                <div class="col-8 font1 fw-bold">
                                    {{ $request_for_payment->checkVoucher['amount_word'] }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4 font1">Php:</div>
                                <div class="col-8 font3 fw-bold">
                                    {{ number_format($request_for_payment->checkVoucher->total_amount, 2) }}</div>
                            </div>
                        </div>
                  

                    </div>
                    <br>
                    <div class="mt-12 row justify-content-md-center">
                        <div class="col">
                            <span class="mt-6 h6 d-flex justify-content-center">RECEIVED BY:
                                _____________________________________</span>
                        </div>
                    </div>
                    <div class="mt-12 row justify-content-md-center">
                        <div class="col">
                            <span class="mt-6 h6 font1 d-flex justify-content-center">Printed Name and
                                Signature</span>
                        </div>
                    </div>
                    <br>

                </div>
                <div style="height:100%">
                    <table class="table table-bordered" style="height: 99%">
                        <tr>
                            <td class="font1 fw-bold">PARTICULARS / DETAILS</td>
                            <td colspan="2" class="font1 fw-bold">TOTAL AMOUNT</td>
                        </tr>
                        <tr style="height: 100%">
                            <td style="height: 100%">
                                <div class="mt-12 row justify-content-md-center">
                                    <div class="row">
                                        <div class="col-3 fw-bold font1">
                                            Check No.
                                        </div>
                                        <div class="col-3 fw-bold font1">
                                            Check Date
                                        </div>
                                        <div class="col-3 fw-bold font1">
                                            Check Amount
                                        </div>
                                    </div>
                                    @foreach ($request_for_payment->checkVoucher->checkVoucherDetails as $check_voucher_details)
                                        <div class="row">
                                            <div class="col-3 font3">
                                                {{ $check_voucher_details->voucher_no }}
                                            </div>
                                            <div class="col-3 font3">
                                                {{ date('M. d, Y', strtotime($check_voucher_details->date)) }}
                                            </div>
                                            <div class="col-3 font3">
                                                {{ number_format($check_voucher_details->amount, 2) }}
                                            </div>
                                        </div>
                                    @endforeach
                                  
                                </div>
                            </td>
                            <td colspan="2" valign="middle">
                                <div class="row">
                                    <div class="col d-flex justify-content-center">
                                        <span
                                            class="fw-bold d-flex justify-content-center">{{ number_format($request_for_payment->checkVoucher->total_amount, 2) }}</span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="font1 fw-bold">ACCOUNT DESCRIPTION</td>
                            <td class="font1 fw-bold">DEBIT</td>
                            <td class="font1 fw-bold">CREDIT</td>
                        </tr>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col fw-bold">
                                        OPEX TYPE : <span
                                            class="fw-bold">{{ $request_for_payment->opex->name }}</span>
                                    </div>
                                </div>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="font1 fw-bold">DESCRIPTION</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="row">
                                    <div class="col fw-bold">
                                        {{ $request_for_payment->checkVoucher->description }}
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="row">
                                    <div class="col-4" class="font1">
                                        Prepared By:<br>
                                        <span class="fw-bold font1">{{ auth()->user()->name }}</span>
                                    </div>
                                    <div class="col-4" class="font1">
                                        Checked By:<br>
                                        <span class="fw-bold font1">Evangeline Manliguez</span>

                                    </div>
                                    <div class="col-4" class="font1">
                                        Approved By:
                                        <br>
                                        <span class="fw-bold">_______________________</span>
                                    </div>
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        {{-- <footer></footer> --}}
        {{-- </div> --}}
    @endforeach

    <script>
        window.print();
    </script>
</body>
