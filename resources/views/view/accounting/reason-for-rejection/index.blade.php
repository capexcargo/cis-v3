@extends('layouts/app')
@section('content')
    <div class="container grid mx-auto">
        <div class="overflow-hidden">
            @livewire('accounting.reason-for-rejection.index')
        </div>
    </div>
@endsection