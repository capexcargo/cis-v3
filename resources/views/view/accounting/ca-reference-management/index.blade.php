@extends('layouts/app')
@section('content')
    <div class="container grid mx-auto">
        <div class="overflow-hidden">
            @livewire('accounting.ca-reference-management.index')
        </div>
    </div>
@endsection
