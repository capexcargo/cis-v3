@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('accounting.division-management.index')
        </div>
    </div>
@endsection
