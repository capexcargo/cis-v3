@extends('layouts/app')
@section('content')
    <div class="container grid mx-auto">
        <div class="overflow-hidden">
            @livewire('admin.security-management.roles-management.index')
        </div>
    </div>
@endsection
