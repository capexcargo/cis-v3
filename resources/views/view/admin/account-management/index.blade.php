@extends('layouts/app')
@section('content')
    <div class="container grid mx-auto">
        <div class="overflow-hidden">
            @livewire('admin.account-management.index')
        </div>
    </div>
@endsection
