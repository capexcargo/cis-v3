@extends('layouts/guest')
@section('content')
    <div class="container grid px-6 mx-auto">
        <div class="overflow-hidden">
            @livewire('admin.account-management.cis-registration')
        </div>
    </div>
@endsection
