@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('ticketing.ticket-management.my-tasks.index')
        </div>
    </div>
@endsection
