@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('ticketing.ticket-management.my-tickets.index')
        </div>
    </div>
@endsection
