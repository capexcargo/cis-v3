@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('ticketing.subcategory-management.index')
        </div>
    </div>
@endsection
