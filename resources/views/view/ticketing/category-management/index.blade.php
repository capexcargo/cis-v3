@extends('layouts/app')
@section('content')
    <div class="grid">
        <div class="overflow-hidden">
            @livewire('ticketing.category-management.index')
        </div>
    </div>
@endsection
