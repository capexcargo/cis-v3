 <x-form wire:init="load" x-data="{
     create_modal: '{{ $create_modal }}',
     edit_modal: '{{ $edit_modal }}',
     print_modal: '{{ $print_modal }}',
     share_modal: '{{ $share_modal }}',
 }">

     <x-slot name="loading">
         <x-loading />
     </x-slot>

     <x-slot name="modals">
        @if($create_modal)
         @can('oims_order_management_team_route_assignment_add')
             <x-modal id="create_modal" size="w-11/12" hasClose="1">
                 <x-slot name="body">
                     @livewire('oims.order-management.team-route-assignment.create')
                 </x-slot>
             </x-modal>
         @endcan
        @endif

         @if($edit_modal)
         @can('oims_order_management_team_route_assignment_edit')
             @if ($team_id && $edit_modal)
                 <x-modal id="edit_modal" size="w-11/12" hasClose="1">
                     <x-slot name="body">
                         @livewire('oims.order-management.team-route-assignment.edit', ['id' => $team_id])
                     </x-slot>
                 </x-modal>
             @endif
         @endcan
         @endif

         @if($share_modal)
         @can('oims_order_management_team_route_assignment_share')
             <x-modal id="share_modal" size="w-3/5" >
                 <x-slot name="body">
                     <div class="p-8 ">
                         <div class="border-2 border-black ">
                             <div class="grid grid-cols-12 pt-1">
                                 <div class="col-span-4">
                                     <span class=""><img class="pl-6 h-14"
                                             src="
                            /images/logo/capex-logo.png"
                                             alt="VeMoBro_logo"></span>
                                 </div>
                                 <div class="col-span-8 mt-4">
                                     <span class="text-2xl font-bold text-center text-blue">Team Route
                                         Assignment</span>
                                 </div>
                             </div>
                             <div class="grid grid-cols-12 pl-6 mt-6 text-sm">
                                 <div class="col-span-3">
                                     <table class="">
                                         <tr>
                                             <td class="font-semibold">Date :</td>
                                             <td class="font-semibold">{{ date('F d,Y') }}</td>
                                         </tr>
                                     </table>
                                 </div>
                                 <div class="col-span-3">
                                     <table class="">
                                         <tr>
                                             <td class="font-semibold">Day :</td>
                                             <td class="font-semibold uppercase">{{ date('l') }}</td>
                                         </tr>
                                     </table>
                                 </div>
                             </div>
                             <div class="grid grid-cols-12">
                                 <div class="col-span-12 p-6 pt-1">
                                     <div class="grid grid-cols-12 gap-x-2">
                                         <div class="col-span-2 p-3 bg-white border rounded-t-md">
                                             <div class="grid justify-center">
                                                 <h1 class="text-xl font-semibold text-center text-blue">
                                                     {{ $this->totalTRK }}
                                                 </h1>
                                                 <h1 class="text-xs font-semibold text-center">TRUCKS</h1>
                                             </div>
                                         </div>

                                         <div class="col-span-2 p-3 ml-2 bg-white border rounded-t-md">
                                             <div class="grid justify-center">
                                                 <h1 class="text-xl font-semibold text-center text-blue">
                                                     {{ $this->totalMT }}
                                                 </h1>
                                                 <h1 class="text-xs font-semibold text-center">MOTORCYCLES</h1>
                                             </div>
                                         </div>

                                         <div class="col-span-2 p-3 ml-2 bg-white border rounded-t-md">
                                             <div class="grid justify-center">
                                                 <h1 class="text-xl font-semibold text-center text-blue">
                                                     {{ $this->totalFTE }}
                                                 </h1>
                                                 <h1 class="text-xs font-semibold text-center">FTE</h1>
                                             </div>
                                         </div>

                                         <div class="col-span-2 p-3">
                                             <div class="grid justify-center">
                                             </div>
                                         </div>
                                         <div class="col-span-2 p-3">
                                             <div class="grid justify-center">
                                             </div>
                                         </div>
                                         <div class="col-span-2 p-3">
                                             <div class="grid justify-center">
                                             </div>
                                         </div>

                                         <div class="col-span-2 bg-white rounded-b-md">
                                             <div class="grid grid-cols-2">
                                                 <div class="grid justify-center px-6 border-2">
                                                     <h1 class="text-lg font-semibold text-center" style="color: #18EB00;">
                                                         {{ $this->totalTRKActive }}
                                                     </h1>
                                                     <h1 class="text-xs font-normal text-center">Active</h1>
                                                 </div>
                                                 <div class="grid justify-center px-4 border-2">
                                                     <h1 class="text-lg font-semibold text-center " style="color: #FF0000">
                                                         {{ $this->totalTRKInactive }}
                                                     </h1>
                                                     <h1 class="text-xs font-normal text-center">Inactive</h1>
                                                 </div>
                                             </div>
                                         </div>

                                         <div class="col-span-2 ml-2 bg-white rounded-b-md">
                                             <div class="grid grid-cols-2">
                                                 <div class="grid justify-center px-6 border-2">
                                                     <h1 class="text-lg font-semibold text-center" style="color: #18EB00;">
                                                         {{ $this->totalMTActive }}
                                                     </h1>
                                                     <h1 class="text-xs font-normal text-center">Active</h1>
                                                 </div>
                                                 <div class="grid justify-center px-4 border-2">
                                                     <h1 class="text-lg font-semibold text-center" style="color: #FF0000">
                                                         {{ $this->totalMTInactive }}
                                                     </h1>
                                                     <h1 class="text-xs font-normal text-center">Inactive</h1>
                                                 </div>
                                             </div>
                                         </div>

                                         <div class="col-span-2 ml-2 bg-white rounded-b-md">
                                             <div class="grid grid-cols-2">
                                                 <div class="grid justify-center px-6 border-2">
                                                     <h1 class="text-lg font-semibold text-center " style="color: #18EB00;">
                                                         {{ $this->totalFTEActive }}
                                                     </h1>
                                                     <h1 class="text-xs font-normal text-center">Active</h1>
                                                 </div>
                                                 <div class="grid justify-center px-4 border-2">
                                                     <h1 class="text-lg font-semibold text-center" style="color: #FF0000">
                                                         {{ $this->totalFTEInactive }}
                                                     </h1>
                                                     <h1 class="text-xs font-normal text-center">Inactive</h1>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>

                                     <div class="grid grid-cols-12 gap-4 mt-2">

                                         <div class="col-span-8 text-xs bg-white border-2 rounded-md">
                                             <x-table.table class="overflow-hidden text-xs text-center">
                                                 <x-slot name="thead" style="text-align: center; font-size: xx-small">
                                                     <tr class="">
                                                         <x-table.th name="" style="padding-left:;" />
                                                         <x-table.th colspan="3" name="No. of Vehicles" class="border-2"
                                                             style="padding-left:20%;height:; background-color: #F2F2F2; color: #8F8F8F;" />
                                                         <x-table.th name="" style="padding-left:;" />
                                                     </tr>
                                                     <x-table.th name="ROUTE CATEGORY" style="padding-left:; " />
                                                     <x-table.th name="MOTORCYCLE"
                                                         style="padding-left:4%; border-left:1px solid #8F8F8F; " />
                                                     <x-table.th name="TRUCKS"
                                                         style="padding-left:3%; border-left:1px solid #8F8F8F; border-right:1px solid #8F8F8F; " />
                                                     <x-table.th name="TOTAL"
                                                         style="padding-left:3%;border-right:1px solid #8F8F8F; " />
                                                     <x-table.th name="FTE" style="padding-left:3%; " />
                                                 </x-slot>
                                                 <x-slot name="tbody" class="text-xs">

                                                     @foreach ($getRoutes as $v => $getRoute)
                                                         <tr
                                                             class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-normal">
                                                             <td class="w-auto p-1 text-xs text-left whitespace-nowrap"
                                                                 style="padding-left:;">
                                                                 {{ $getRoute->name ?? '-' }}
                                                             </td>
                                                             <td class="w-auto p-1 text-xs whitespace-nowrap "
                                                                 style="padding-left:; border-left:1px solid #8F8F8F;">
                                                                 @if ($getCurrent != null)
                                                                     {{ $getRoute->motorcycle ?? '0' }}
                                                                 @endif
                                                             </td>
                                                             <td class="w-auto p-1 text-xs whitespace-nowrap"
                                                                 style="padding-left:; border-left:1px solid #8F8F8F; border-right:1px solid #8F8F8F;">
                                                                 @if ($getCurrent != null)
                                                                     {{ $getRoute->truck ?? '0' }}
                                                                 @endif
                                                             </td>
                                                             <td class="w-auto p-1 text-xs whitespace-nowrap"
                                                                 style="padding-left:;border-right:1px solid #8F8F8F;">
                                                                 @if ($getCurrent != null)
                                                                     {{ $getRoute->motorcycle + $getRoute->truck ?? '0' }}
                                                                 @endif
                                                             </td>
                                                             <td class="w-auto p-1 text-xs whitespace-nowrap"
                                                                 style="padding-left:;">
                                                                 @if ($getCurrent != null)
                                                                     @if (isset(
                                                                             $getRoute->teamDetails[0]['team_plate_reference_count'],
                                                                             $getRoute->teamDetails[0]['team_checker1_reference_count'],
                                                                             $getRoute->teamDetails[0]['team_checker2_reference_count']))
                                                                         {{ $getRoute->teamDetails[0]['team_driver_reference_count'] + $getRoute->teamDetails[0]['team_checker1_reference_count'] + $getRoute->teamDetails[0]['team_checker2_reference_count'] ?? '0' }}
                                                                     @else
                                                                         0
                                                                     @endif
                                                                 @endif
                                                             </td>
                                                         </tr>
                                                     @endforeach
                                                 </x-slot>
                                             </x-table.table>
                                         </div>
                                     </div>

                                     <div class="mt-2 bg-white rounded-lg shadow-md">
                                         <x-table.table class="overflow-hidden">
                                             <x-slot name="thead">
                                                 {{-- <x-table.th name="No." style="padding-left:;" /> --}}
                                                 <x-table.th name="Team Name" style="padding-left:%;" />
                                                 <x-table.th name="Route Category" style="padding-left:%;" />
                                                 <x-table.th name="Plate Number" style="padding-left:%;" />
                                                 <x-table.th name="Driver" style="padding-left:%;" />
                                                 <x-table.th name="Checker 1" style="padding-left:%;" />
                                                 <x-table.th name="Checker 2" style="padding-left:%;" />
                                                 <x-table.th name="Remarks" style="padding-left:%;" />
                                             </x-slot>
                                             <x-slot name="tbody">
                                                 @foreach ($team_routes as $i => $team_route)
                                                     <tr
                                                         class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-normal">
                                                         <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:2%;">
                                                             <p class="text-xs">
                                                                 {{ ($team_routes->currentPage() - 1) * $team_routes->links()->paginator->perPage() + $loop->iteration }}
                                                             </p>
                                                         </td>
                                                         <td class="w-1/5 p-1 whitespace-nowrap"
                                                             style="padding-left:1.5%;">
                                                             <p class="text-xs">
                                                                 {{ $team_routes[$i]->teamDetails->teamIdReference->name ?? '-' }}
                                                             </p>
                                                         </td>
                                                         <td class="w-1/5 p-1 whitespace-nowrap"
                                                             style="padding-left:1.5%;">
                                                             <p class="text-xs">
                                                                 {{ $team_routes[$i]->teamDetails->teamRouteReference->name ?? '-' }}
                                                             </p>
                                                         </td>
                                                         <td class="w-1/5 p-1 whitespace-nowrap"
                                                             style="padding-left:1.5%;">
                                                             <p class="text-xs">{{ $team_routes[$i]->plate_no }}</p>
                                                         </td>


                                                         <td class="w-1/5 p-1 whitespace-nowrap "
                                                             style="padding-left:1.5%;">
                                                             @if (isset($team_routes[$i]->teamDetails))
                                                                 @if (isset($team_routes[$i]->teamDetails->teamDriverReference->userDetails->timeLog[0]['date']) &&
                                                                         $team_routes[$i]->teamDetails->teamDriverReference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                                                     <p class="text-xs">
                                                                         {{ $team_routes[$i]->teamDetails->teamDriverReference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamDriverReference->userDetails->last_name ?? '-' }}
                                                                     </p>
                                                                 @else
                                                                     <p class=" text-red" style="">
                                                                     <p class="text-xs text-red">
                                                                         {{ $team_routes[$i]->teamDetails->teamDriverReference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamDriverReference->userDetails->last_name ?? '-' }}
                                                                     </p>
                                                                     </p>
                                                                 @endif
                                                             @else
                                                                 <p class="text-center">-</p>
                                                             @endif
                                                         </td>

                                                         <td class="w-1/5 p-1 whitespace-nowrap" style="padding-left:2%;">
                                                             @if (isset($team_routes[$i]->teamDetails))
                                                                 @if (isset($team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->timeLog[0]['date']) &&
                                                                         $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                                                     <p class="text-xs">
                                                                         {{ $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->last_name ?? '-' }}
                                                                     </p>
                                                                 @else
                                                                     <p class="text-red">
                                                                     <p class="text-xs text-red">
                                                                         {{ $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->last_name ?? '-' }}
                                                                     </p>
                                                                     </p>
                                                                 @endif
                                                             @else
                                                                 <p class="text-center">-</p>
                                                             @endif
                                                         </td>

                                                         <td class="w-1/5 p-1 whitespace-nowrap"
                                                             style="padding-left:1.5%;">
                                                             @if (isset($team_routes[$i]->teamDetails))
                                                                 @if (isset($team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->timeLog[0]['date']) &&
                                                                         $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                                                     @if ($team_routes[$i]->teamDetails->checker2_id != null)
                                                                         <p class="text-xs">
                                                                             {{ $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->last_name ?? '-' }}
                                                                         </p>
                                                                     @else
                                                                         <p class="text-center">-</p>
                                                                     @endif
                                                                 @else
                                                                     <p class="text-red">
                                                                         @if ($team_routes[$i]->teamDetails->checker2_id != null)
                                                                             <p class="text-xs text-red">
                                                                                 {{ $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->last_name ?? '-' }}
                                                                             </p>
                                                                         @else
                                                                             <p class="text-center">-</p>
                                                                         @endif
                                                                     </p>
                                                                 @endif
                                                             @else
                                                                 <p class="text-center">-</p>
                                                             @endif
                                                         </td>

                                                         <td class="w-1/5 p-1 whitespace-nowrap"
                                                             style="padding-left:1.5%;">
                                                             @if (isset($team_routes[$i]->teamDetails))
                                                                 @if (isset($team_routes[$i]->teamDetails))
                                                                     @if ($team_routes[$i]->teamDetails->remarks != null)
                                                                         <p class="text-xs">
                                                                             {{ $team_routes[$i]->teamDetails->remarks ?? '-' }}
                                                                         </p>
                                                                     @else
                                                                         <p class="text-center">-</p>
                                                                     @endif
                                                                 @endif
                                                             @else
                                                                 <p class="text-center">-</p>
                                                             @endif
                                                         </td>
                                                     </tr>
                                                 @endforeach
                                             </x-slot>
                                         </x-table.table>

                                     </div>
                                     <span class="flex justify-center py-2">
                                         <hr class="w-full text-black border-2 border-blue">
                                     </span>
                                     {{-- <div class="flex justify-between text-blue">
                                         <table class="">
                                             <tr>
                                                 <td class="font-semibold">Main Office :</td>
                                                 <td>asdasd</td>
                                             </tr>
                                         </table>
                                         
                                         <table class="">
                                             <tr>
                                                 <td class="font-semibold">Telephone :</td>
                                                 <td>asdasd</td>
                                             </tr>
                                             <br>
                                             <tr>
                                                 <td class="font-semibold">Website :</td>
                                                 <td>asdasd</td>
                                             </tr>
                                         </table>
                                     </div> --}}
                                 </div>
                             </div>
                         </div>
                     </div>
                 </x-slot>
             </x-modal>
         @endcan
         @endif
         @if($print_modal)
         @can('oims_order_management_team_route_assignment_print')
             {{-- <x-modal id="print_modal" size="w-3/5" hasClose="1"> --}}
             <x-modal id="print_modal" size="w-3/5">
                 <x-slot name="body">
                     {{-- <a target="_blank"
                     href="{{ route('oims.order-management.transaction-entry.printsticker') }}" 
                         class="absolute bg-white cursor-pointer top-2 right-2">
                         <svg width="20" height="20" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                             <path fill="#ff0000"
                                 d="M16 2C8.2 2 2 8.2 2 16s6.2 14 14 14s14-6.2 14-14S23.8 2 16 2zm0 26C9.4 28 4 22.6 4 16S9.4 4 16 4s12 5.4 12 12s-5.4 12-12 12z" />
                             <path fill="#ff0000"
                                 d="M21.4 23L16 17.6L10.6 23L9 21.4l5.4-5.4L9 10.6L10.6 9l5.4 5.4L21.4 9l1.6 1.6l-5.4 5.4l5.4 5.4z" />
                         </svg>
                        </a> --}}
                     <div class="p-8 ">
                         <div class="flex justify-end"> 
                             <a
                              target="_blank"
                                 href="{{ route('oims.order-management.team-route-assignment.print', ['id' => $userid]) }}" 
                                 
                                 type="button" value="click"
                                 class="p-1 px-4 mb-3 text-sm bg-white border-2 rounded-full text-blue border-blue hover:bg-blue-900 hover:text-white">
                                 <div class="flex items-start justify-between">
                                     <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false"
                                         data-prefix="far" data-icon="print-alt" role="img"
                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                         <path fill="currentColor"
                                             d="M128 0C92.7 0 64 28.7 64 64v96h64V64H354.7L384 93.3V160h64V93.3c0-17-6.7-33.3-18.7-45.3L400 18.7C388 6.7 371.7 0 354.7 0H128zM384 352v32 64H128V384 368 352H384zm64 32h32c17.7 0 32-14.3 32-32V256c0-35.3-28.7-64-64-64H64c-35.3 0-64 28.7-64 64v96c0 17.7 14.3 32 32 32H64v64c0 35.3 28.7 64 64 64H384c35.3 0 64-28.7 64-64V384zM432 248a24 24 0 1 1 0 48 24 24 0 1 1 0-48z" />
                                     </svg>
                                     Print
                                 </div>
                             </a>
                         </div>
                         <div id="" class="border-2 border-black ">

                             <div class="grid grid-cols-12 pt-6">
                                 <div class="col-span-4">
                                     <span class=""><img class="pl-6 h-14"
                                             src="
                            /images/logo/capex-logo.png"
                                             alt="VeMoBro_logo"></span>
                                 </div>
                                 <div class="col-span-8 mt-4">
                                     <span class="text-2xl font-bold text-center text-blue">Team Route
                                         Assignment</span>
                                 </div>
                             </div>
                             <div class="grid grid-cols-12 pl-6 mt-6 text-sm">
                                 <div class="col-span-3">
                                     <table class="">
                                         <tr>
                                             <td class="font-semibold">Date :</td>
                                             <td class="font-semibold">{{ date('F-d-Y') }}</td>
                                         </tr>
                                     </table>
                                 </div>
                                 <div class="col-span-3">
                                     <table class="">
                                         <tr>
                                             <td class="font-semibold">Day :</td>
                                             <td class="font-semibold uppercase">{{ date('l') }}</td>
                                         </tr>
                                     </table>
                                 </div>
                             </div>
                             <div class="grid grid-cols-12">
                                 <div class="col-span-12 p-6 pt-1">
                                     <div class="grid grid-cols-12 gap-x-2">
                                         <div class="col-span-2 p-3 bg-white border rounded-t-md">
                                             <div class="grid justify-center">
                                                 <h1 class="text-xl font-semibold text-center text-blue">
                                                     {{ $this->totalTRK }}
                                                 </h1>
                                                 <h1 class="text-xs font-semibold text-center">TRUCKS</h1>
                                             </div>
                                         </div>

                                         <div class="col-span-2 p-3 ml-2 bg-white border rounded-t-md">
                                             <div class="grid justify-center">
                                                 <h1 class="text-xl font-semibold text-center text-blue">
                                                     {{ $this->totalMT }}
                                                 </h1>
                                                 <h1 class="text-xs font-semibold text-center">MOTORCYCLES</h1>
                                             </div>
                                         </div>

                                         <div class="col-span-2 p-3 ml-2 bg-white border rounded-t-md">
                                             <div class="grid justify-center">
                                                 <h1 class="text-xl font-semibold text-center text-blue">
                                                     {{ $this->totalFTE }}
                                                 </h1>
                                                 <h1 class="text-xs font-semibold text-center">FTE</h1>
                                             </div>
                                         </div>

                                         <div class="col-span-2 p-3">
                                             <div class="grid justify-center">
                                             </div>
                                         </div>
                                         <div class="col-span-2 p-3">
                                             <div class="grid justify-center">
                                             </div>
                                         </div>
                                         <div class="col-span-2 p-3">
                                             <div class="grid justify-center">
                                             </div>
                                         </div>

                                         <div class="col-span-2 bg-white rounded-b-md">
                                             <div class="grid grid-cols-2">
                                                 <div class="grid justify-center px-6 border-2">
                                                     <h1 class="text-lg font-semibold text-center"
                                                         style="color: #18EB00;">
                                                         {{ $this->totalTRKActive }}
                                                     </h1>
                                                     <h1 class="text-xs font-normal text-center">Active</h1>
                                                 </div>
                                                 <div class="grid justify-center px-4 border-2">
                                                     <h1 class="text-lg font-semibold text-center "
                                                         style="color: #FF0000">
                                                         {{ $this->totalTRKInactive }}
                                                     </h1>
                                                     <h1 class="text-xs font-normal text-center">Inactive</h1>
                                                 </div>
                                             </div>
                                         </div>

                                         <div class="col-span-2 ml-2 bg-white rounded-b-md">
                                             <div class="grid grid-cols-2">
                                                 <div class="grid justify-center px-6 border-2">
                                                     <h1 class="text-lg font-semibold text-center"
                                                         style="color: #18EB00;">
                                                         {{ $this->totalMTActive }}
                                                     </h1>
                                                     <h1 class="text-xs font-normal text-center">Active</h1>
                                                 </div>
                                                 <div class="grid justify-center px-4 border-2">
                                                     <h1 class="text-lg font-semibold text-center" style="color: #FF0000">
                                                         {{ $this->totalMTInactive }}
                                                     </h1>
                                                     <h1 class="text-xs font-normal text-center">Inactive</h1>
                                                 </div>
                                             </div>
                                         </div>

                                         <div class="col-span-2 ml-2 bg-white rounded-b-md">
                                             <div class="grid grid-cols-2">
                                                 <div class="grid justify-center px-6 border-2">
                                                     <h1 class="text-lg font-semibold text-center "
                                                         style="color: #18EB00;">
                                                         {{ $this->totalFTEActive }}
                                                     </h1>
                                                     <h1 class="text-xs font-normal text-center">Active</h1>
                                                 </div>
                                                 <div class="grid justify-center px-4 border-2">
                                                     <h1 class="text-lg font-semibold text-center" style="color: #FF0000">
                                                         {{ $this->totalFTEInactive }}
                                                     </h1>
                                                     <h1 class="text-xs font-normal text-center">Inactive</h1>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>

                                     <div class="grid grid-cols-12 gap-4 mt-2">

                                         <div class="col-span-8 text-xs bg-white border-2 rounded-md">
                                             <x-table.table class="overflow-hidden text-xs text-center">
                                                 <x-slot name="thead" style="text-align: center;" class="text-xs">
                                                     <tr class="">
                                                         <x-table.th name="" style="padding-left:;" />
                                                         <x-table.th colspan="3" name="No. of Vehicles"
                                                             class="border-2"
                                                             style="padding-left:20%;height:; background-color: #F2F2F2; color: #8F8F8F;" />
                                                         <x-table.th name="" style="padding-left:;" />
                                                     </tr>
                                                     <x-table.th name="ROUTE CATEGORY" style="padding-left:; " />
                                                     <x-table.th name="MOTORCYCLE"
                                                         style="padding-left:4%; border-left:1px solid #8F8F8F; " />
                                                     <x-table.th name="TRUCKS"
                                                         style="padding-left:3%; border-left:1px solid #8F8F8F; border-right:1px solid #8F8F8F; " />
                                                     <x-table.th name="TOTAL"
                                                         style="padding-left:3%;border-right:1px solid #8F8F8F; " />
                                                     <x-table.th name="FTE" style="padding-left:3%; " />
                                                 </x-slot>
                                                 <x-slot name="tbody" class="text-xs">

                                                     @foreach ($getRoutes as $v => $getRoute)
                                                         <tr
                                                             class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-normal">
                                                             <td class="w-auto p-1 text-xs text-left whitespace-nowrap"
                                                                 style="padding-left:;">
                                                                 {{ $getRoute->name ?? '-' }}
                                                             </td>
                                                             <td class="w-auto p-1 text-xs whitespace-nowrap "
                                                                 style="padding-left:; border-left:1px solid #8F8F8F;">
                                                                 @if ($getCurrent != null)
                                                                     {{ $getRoute->motorcycle ?? '0' }}
                                                                 @endif
                                                             </td>
                                                             <td class="w-auto p-1 text-xs whitespace-nowrap"
                                                                 style="padding-left:; border-left:1px solid #8F8F8F; border-right:1px solid #8F8F8F;">
                                                                 @if ($getCurrent != null)
                                                                     {{ $getRoute->truck ?? '0' }}
                                                                 @endif
                                                             </td>
                                                             <td class="w-auto p-1 text-xs whitespace-nowrap"
                                                                 style="padding-left:;border-right:1px solid #8F8F8F;">
                                                                 @if ($getCurrent != null)
                                                                     {{ $getRoute->motorcycle + $getRoute->truck ?? '0' }}
                                                                 @endif
                                                             </td>
                                                             <td class="w-auto p-1 text-xs whitespace-nowrap"
                                                                 style="padding-left:;">
                                                                 @if ($getCurrent != null)
                                                                     @if (isset(
                                                                             $getRoute->teamDetails[0]['team_plate_reference_count'],
                                                                             $getRoute->teamDetails[0]['team_checker1_reference_count'],
                                                                             $getRoute->teamDetails[0]['team_checker2_reference_count']))
                                                                         {{ $getRoute->teamDetails[0]['team_driver_reference_count'] + $getRoute->teamDetails[0]['team_checker1_reference_count'] + $getRoute->teamDetails[0]['team_checker2_reference_count'] ?? '0' }}
                                                                     @else
                                                                         0
                                                                     @endif
                                                                 @endif
                                                             </td>
                                                         </tr>
                                                     @endforeach
                                                 </x-slot>
                                             </x-table.table>
                                         </div>
                                     </div>

                                     <div class="mt-2 bg-white rounded-lg shadow-md">
                                         <x-table.table class="overflow-hidden">
                                             <x-slot name="thead" class="text-xs">
                                                 <x-table.th name="No." style="padding-left:;" />
                                                 <x-table.th name="Team Name" style="padding-left:%;" />
                                                 <x-table.th name="Route Category" style="padding-left:%;" />
                                                 <x-table.th name="Plate Number" style="padding-left:%;" />
                                                 <x-table.th name="Driver" style="padding-left:%;" />
                                                 <x-table.th name="Checker 1" style="padding-left:%;" />
                                                 <x-table.th name="Checker 2" style="padding-left:%;" />
                                                 <x-table.th name="Remarks" style="padding-left:%;" />
                                             </x-slot>
                                             <x-slot name="tbody">
                                                 @foreach ($team_routes as $i => $team_route)
                                                     <tr
                                                         class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-normal">
                                                         <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:2%;">
                                                             <p class="text-xs">
                                                                 {{ ($team_routes->currentPage() - 1) * $team_routes->links()->paginator->perPage() + $loop->iteration }}
                                                             </p>
                                                         </td>
                                                         <td class="w-1/5 p-1 whitespace-nowrap"
                                                             style="padding-left:1.5%;">
                                                             <p class="text-xs">
                                                                 {{ $team_routes[$i]->teamDetails->teamIdReference->name ?? '-' }}
                                                             </p>
                                                         </td>
                                                         <td class="w-1/5 p-1 whitespace-nowrap"
                                                             style="padding-left:1.5%;">
                                                             <p class="text-xs">
                                                                 {{ $team_routes[$i]->teamDetails->teamRouteReference->name ?? '-' }}
                                                             </p>
                                                         </td>
                                                         <td class="w-1/5 p-1 whitespace-nowrap"
                                                             style="padding-left:1.5%;">
                                                             <p class="text-xs">{{ $team_routes[$i]->plate_no }}</p>
                                                         </td>


                                                         <td class="w-1/5 p-1 whitespace-nowrap "
                                                             style="padding-left:1.5%;">
                                                             @if (isset($team_routes[$i]->teamDetails))
                                                                 @if (isset($team_routes[$i]->teamDetails->teamDriverReference->userDetails->timeLog[0]['date']) &&
                                                                         $team_routes[$i]->teamDetails->teamDriverReference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                                                     <p class="text-xs">
                                                                         {{ $team_routes[$i]->teamDetails->teamDriverReference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamDriverReference->userDetails->last_name ?? '-' }}
                                                                     </p>
                                                                 @else
                                                                     <p class=" text-red" style="">
                                                                     <p class="text-xs text-red">
                                                                         {{ $team_routes[$i]->teamDetails->teamDriverReference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamDriverReference->userDetails->last_name ?? '-' }}
                                                                     </p>
                                                                     </p>
                                                                 @endif
                                                             @else
                                                                 -
                                                             @endif
                                                         </td>

                                                         <td class="w-1/5 p-1 whitespace-nowrap" style="padding-left:2%;">
                                                             @if (isset($team_routes[$i]->teamDetails))
                                                                 @if (isset($team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->timeLog[0]['date']) &&
                                                                         $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                                                     <p class="text-xs">
                                                                         {{ $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->last_name ?? '-' }}
                                                                     </p>
                                                                 @else
                                                                     <p class="text-red">
                                                                     <p class="text-xs text-red">
                                                                         {{ $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->last_name ?? '-' }}
                                                                     </p>
                                                                     </p>
                                                                 @endif
                                                             @else
                                                                 <p class="text-center">-</p>
                                                             @endif
                                                         </td>

                                                         <td class="w-1/5 p-1 whitespace-nowrap"
                                                             style="padding-left:1.5%;">
                                                             @if (isset($team_routes[$i]->teamDetails))
                                                                 @if (isset($team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->timeLog[0]['date']) &&
                                                                         $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                                                     @if ($team_routes[$i]->teamDetails->checker2_id != null)
                                                                         <p class="text-xs">
                                                                             {{ $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->last_name ?? '-' }}
                                                                         </p>
                                                                     @else
                                                                         <p class="text-center">-</p>
                                                                     @endif
                                                                 @else
                                                                     <p class="text-red">
                                                                         @if ($team_routes[$i]->teamDetails->checker2_id != null)
                                                                             <p class="text-xs text-red">
                                                                                 {{ $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->last_name ?? '-' }}
                                                                             </p>
                                                                         @else
                                                                             <p class="text-center">-</p>
                                                                         @endif
                                                                     </p>
                                                                 @endif
                                                             @else
                                                                 <p class="text-center">-</p>
                                                             @endif
                                                         </td>

                                                         <td class="w-1/5 p-1 whitespace-nowrap"
                                                             style="padding-left:1.5%;">
                                                             @if (isset($team_routes[$i]->teamDetails))
                                                                 @if (isset($team_routes[$i]->teamDetails))
                                                                     @if ($team_routes[$i]->teamDetails->remarks != null)
                                                                         <p class="text-xs">
                                                                             {{ $team_routes[$i]->teamDetails->remarks ?? '-' }}
                                                                         </p>
                                                                     @else
                                                                         <p class="text-center">-</p>
                                                                     @endif
                                                                 @endif
                                                             @else
                                                                 <p class="text-center">-</p>
                                                             @endif
                                                         </td>
                                                     </tr>
                                                 @endforeach
                                             </x-slot>
                                         </x-table.table>

                                     </div>
                                     <span class="flex justify-center py-2">
                                         <hr class="w-full text-black border-2 border-blue">
                                     </span>
                                     <div class="flex justify-between text-xs text-blue">
                                         <table class="">
                                             <tr>
                                                 <td class="font-semibold text-justify" style="margin-top: -4%">Main
                                                     Office :</td>
                                                 <td class="">
                                                     Building 9A, Salem International
                                                 </td>
                                             </tr>
                                             <tr>
                                                 <td class="font-semibold whitespace-nowrap"> </td>
                                                 <td class="">Commercial Complex, Domestic
                                                     Road, Pasay City</td>
                                             </tr>
                                             <tr>
                                                 <td class="font-semibold whitespace-nowrap"> </td>
                                                 <td class="">1301, Philippines
                                             </tr>
                                         </table>
                                         <table class="">
                                             <tr>
                                                 <td class="font-semibold">Telephone :</td>
                                                 <td>(+632)8396-8888</td>
                                             </tr>
                                             <br>
                                             <tr>
                                                 <td class="font-semibold">Website :</td>
                                                 <td>www.capex.com.ph</td>
                                             </tr>
                                         </table>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </x-slot>
             </x-modal>
         @endcan
         @endif

     </x-slot>

     <x-slot name="header_title" style="font-family-poppins: Poppins;">Team Route Assignment</x-slot>
     <x-slot name="header_button">
         @can('oims_order_management_team_route_assignment_add')
             {{-- @dd(Auth::user_details()) --}}
             @if ($getuserbranch == $bid || date(now()) == $vcdd)
                 <button disabled wire:click="" class="p-2 px-3 mr-3 text-sm text-white bg-gray-400 rounded-md">
                     <div class="flex items-start justify-between">
                         <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                             data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                             viewBox="0 0 448 512">
                             <path fill="currentColor"
                                 d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                         </svg>
                         Create Team
                     </div>
                 </button>
             @else
                 <button wire:click="action({}, 'add')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                     <div class="flex items-start justify-between">
                         <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                             data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                             viewBox="0 0 448 512">
                             <path fill="currentColor"
                                 d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                         </svg>
                         Create Team
                     </div>
                 </button>
             @endif
         @endcan

         <button wire:click="action({}, 'create')"
             class="p-2 px-3 mr-3 text-sm bg-white border-2 rounded-md text-blue border-blue hover:bg-blue-900 hover:text-white">
             <div class="flex items-start justify-between">
                 Advance TRA
             </div>
         </button>
         <div class="text-blue whitespace-nowrap" x-data="{ open: false }">
             <button @click="open = !open"
                 class="p-2 px-3 mr-3 text-sm bg-white border-2 border-blue-900 rounded-md text-blue hover:bg-blue-900 hover:text-white ">
                 <div class="flex items-start justify-between">
                     Management
                     <svg class="w-3 h-3 mt-1 ml-2" aria-hidden="true" focusable="false" data-prefix="far"
                         data-icon="print-alt" role="img" x mlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 448 512">
                         <path fill="currentColor"
                             d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                     </svg>
                 </div>
             </button>
             <div class="absolute z-10 mt-2 border border-gray-500 rounded-md" style="margin-left: -7.9rem">
                 <ul class="text-gray-600 bg-white rounded shadow" x-show="open" @click.away="open = false">
                     <li class="px-3 py-1 cursor-pointer" x-data="{ open: false }"
                         wire:click="redirectTo({}, 'redirectToTeam')">Team Management</a>
                     </li>
                     <li class="px-3 py-1 cursor-pointer" x-data="{ open: false }"
                         wire:click="redirectTo({}, 'redirectToQuadrant')">Quadrant Management</a>
                     </li>
                     <li class="px-3 py-1 cursor-pointer" x-data="{ open: false }"
                         wire:click="redirectTo({}, 'redirectToArea')">Area Management</a>
                     </li>
                     <li class="px-3 py-1 cursor-pointer" x-data="{ open: false }"
                         wire:click="redirectTo({}, 'redirectToRoute')">Route Category Management</a>
                     </li>
                 </ul>
             </div>
         </div>

     </x-slot>

     <x-slot name="body">

         <div class="grid w-11/12 grid-cols-7 gap-3 text-sm " style="margin-top: 2%;">
             <div class="w-11/12">
                 <x-transparent.input style="font-size: 12px" type="text" onfocus="(this.type='date')"
                     onblur="(this.type='text')" label="" placeholder="Dispatch Date" name="dispatch_date"
                     wire:model.defer="dispatch_date" />
             </div>

             <div class="w-11/12" wire:init="routeReference">
                 <x-transparent.select value="{{ $route }}" label="Route Category" name="route_category_id"
                     wire:model.defer="route_category_id">
                     <option value="route_category_id"></option>
                     @foreach ($routeReferences as $routeReference)
                         <option value="{{ $routeReference->id }}">
                             {{ $routeReference->name ?? '-' }}
                         </option>
                     @endforeach
                 </x-transparent.select>
             </div>

             <div class="w-11/12" wire:init="plateReference">
                 <x-transparent.select value="" label="Plate#" name="plate_no_id"
                     wire:model.defer="plate_no_id">
                     <option value=""></option>
                     @foreach ($plateReferences as $plateReference)
                         <option value="{{ $plateReference->id }}">
                             {{ $plateReference->plate_no }}
                         </option>
                     @endforeach
                 </x-transparent.select>
             </div>

             <div class="w-11/12" wire:init="teamBranchReference">
                 <x-transparent.select value="" label="Area" name="branch_id" wire:model.defer="branch_id">
                     <option value=""></option>
                     @foreach ($teamBranchReferences as $teamBranchReference)
                         <option value="{{ $teamBranchReference->id }}">
                             {{ $teamBranchReference->name }}
                         </option>
                     @endforeach
                 </x-transparent.select>
             </div>

             <div class="w-11/12" wire:init="driverReference">
                 <x-transparent.select value="" label="Driver" name="driver_id" wire:model.defer="driver_id">
                     <option value=""></option>
                     @foreach ($driverReferences as $driverReference)
                         <option value="{{ $driverReference->user_id }}">
                             {{ $driverReference->first_name . ' ' . $driverReference->last_name }}
                         </option>
                     @endforeach
                 </x-transparent.select>
             </div>

             <div class="w-11/12" wire:init="checkerReference">
                 <x-transparent.select value="" label="Checker" name="checker1_id"
                     wire:model.defer="checker1_id">
                     <option value=""></option>
                     @foreach ($checkerReferences as $checkerReference)
                         <option value="{{ $checkerReference->user_id }}">
                             {{ $checkerReference->first_name . ' ' . $checkerReference->last_name }}
                         </option>
                     @endforeach
                 </x-transparent.select>
             </div>

             <div class="w-11/12">
                 <x-button type="button" wire:click="search" title="Search"
                     class="px-3 py-1.5 text-white bg-blue hover:bg-blue-800" />
             </div>


         </div>

         <div class="grid grid-cols-12 gap-x-4">
             <div class="col-span-2 p-3 bg-white rounded-t-md">
                 <div class="grid justify-center">
                     <h1 class="text-3xl font-semibold text-center text-blue">{{ $this->totalTRK }}</h1>
                     <h1 class="text-base font-semibold text-center">TRUCKS</h1>
                 </div>
             </div>

             <div class="col-span-2 p-3 bg-white rounded-t-md">
                 <div class="grid justify-center">
                     <h1 class="text-3xl font-semibold text-center text-blue">{{ $this->totalMT }}</h1>
                     <h1 class="text-base font-semibold text-center">MOTORCYCLES</h1>
                 </div>
             </div>

             <div class="col-span-2 p-3 bg-white rounded-t-md">
                 <div class="grid justify-center">
                     <h1 class="text-3xl font-semibold text-center text-blue">{{ $this->totalFTE }}</h1>
                     <h1 class="text-base font-semibold text-center">FTE</h1>
                 </div>
             </div>

             <div class="col-span-2 p-3">
                 <div class="grid justify-center">
                 </div>
             </div>
             <div class="col-span-2 p-3">
                 <div class="grid justify-center">
                 </div>
             </div>
             <div class="col-span-2 p-3">
                 <div class="grid justify-center">
                 </div>
             </div>

             <div class="col-span-2 bg-white rounded-b-md">
                 <div class="grid grid-cols-2">
                     <div class="grid justify-center px-6 border-2">
                         <h1 class="text-2xl font-semibold text-center" style="color: #18EB00;">
                             {{ $this->totalTRKActive }}
                         </h1>
                         <h1 class="text-sm font-normal text-center">Active</h1>
                     </div>
                     <div class="grid justify-center px-4 border-2">
                         <h1 class="text-2xl font-semibold text-center " style="color: #FF0000">
                             {{ $this->totalTRKInactive }}
                         </h1>
                         <h1 class="text-sm font-normal text-center">Inactive</h1>
                     </div>
                 </div>
             </div>

             <div class="col-span-2 bg-white rounded-b-md">
                 <div class="grid grid-cols-2">
                     <div class="grid justify-center px-6 border-2">
                         <h1 class="text-2xl font-semibold text-center" style="color: #18EB00;">
                             {{ $this->totalMTActive }}
                         </h1>
                         <h1 class="text-sm font-normal text-center">Active</h1>
                     </div>
                     <div class="grid justify-center px-4 border-2">
                         <h1 class="text-2xl font-semibold text-center" style="color: #FF0000">
                             {{ $this->totalMTInactive }}
                         </h1>
                         <h1 class="text-sm font-normal text-center">Inactive</h1>
                     </div>
                 </div>
             </div>

             <div class="col-span-2 bg-white rounded-b-md">
                 <div class="grid grid-cols-2">
                     <div class="grid justify-center px-6 border-2">
                         <h1 class="text-2xl font-semibold text-center " style="color: #18EB00;">
                             {{ $this->totalFTEActive }}
                         </h1>
                         <h1 class="text-sm font-normal text-center">Active</h1>
                     </div>
                     <div class="grid justify-center px-4 border-2">
                         <h1 class="text-2xl font-semibold text-center" style="color: #FF0000">
                             {{ $this->totalFTEInactive }}
                         </h1>
                         <h1 class="text-sm font-normal text-center">Inactive</h1>
                     </div>
                 </div>
             </div>


         </div>

         <div class="grid grid-cols-12 gap-4">

             <div class="col-span-6 bg-white border-2 rounded-md">
                 <x-table.table class="overflow-hidden text-center">
                     <x-slot name="thead" style="text-align: center;">
                         <tr class="">
                             <x-table.th name="" style="padding-left:;" />
                             <x-table.th colspan="3" name="No. of Vehicles" class="border-2"
                                 style="padding-left:20%;height:1px; background-color: #F2F2F2; color: #8F8F8F;" />
                             <x-table.th name="" style="padding-left:;" />
                         </tr>
                         <x-table.th name="ROUTE CATEGORY" style="padding-left:; " />
                         <x-table.th name="MOTORCYCLE" style="padding-left:4%; border-left:1px solid #8F8F8F; " />
                         <x-table.th name="TRUCKS"
                             style="padding-left:3%; border-left:1px solid #8F8F8F; border-right:1px solid #8F8F8F; " />
                         <x-table.th name="TOTAL" style="padding-left:3%;border-right:1px solid #8F8F8F; " />
                         <x-table.th name="FTE" style="padding-left:3%; " />
                     </x-slot>
                     <x-slot name="tbody">
                         @foreach ($getRoutes as $v => $getRoute)
                             <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-normal">
                                 <td class="w-auto p-3 text-left whitespace-nowrap" style="padding-left:;">
                                     {{ $getRoute->name ?? '-' }}
                                 </td>
                                 <td class="w-auto p-3 whitespace-nowrap "
                                     style="padding-left:; border-left:1px solid #8F8F8F;">
                                     @if ($getCurrent != null)
                                         {{ $getRoute->motorcycle ?? '0' }}
                                     @else
                                         0
                                     @endif
                                 </td>
                                 <td class="w-auto p-3 whitespace-nowrap"
                                     style="padding-left:; border-left:1px solid #8F8F8F; border-right:1px solid #8F8F8F;">
                                     @if ($getCurrent != null)
                                         {{ $getRoute->truck ?? '0' }}
                                     @else
                                         0
                                     @endif
                                 </td>
                                 <td class="w-auto p-3 whitespace-nowrap"
                                     style="padding-left:;border-right:1px solid #8F8F8F;">
                                     @if ($getCurrent != null)
                                         {{ $getRoute->motorcycle + $getRoute->truck ?? '0' }}
                                     @else
                                         0
                                     @endif

                                 </td>
                                 <td class="w-auto p-3 whitespace-nowrap" style="padding-left:;">
                                     @if ($getCurrent != null)
                                         @if (isset(
                                                 $getRoute->teamDetails[0]['team_plate_reference_count'],
                                                 $getRoute->teamDetails[0]['team_checker1_reference_count'],
                                                 $getRoute->teamDetails[0]['team_checker2_reference_count']))
                                             {{ $getRoute->teamDetails[0]['team_driver_reference_count'] + $getRoute->teamDetails[0]['team_checker1_reference_count'] + $getRoute->teamDetails[0]['team_checker2_reference_count'] ?? '0' }}
                                         @else
                                             0
                                         @endif
                                     @else
                                         0
                                     @endif
                                 </td>
                             </tr>
                         @endforeach
                     </x-slot>
                 </x-table.table>
             </div>
         </div>

         <div class="grid grid-cols-12">
             <div class="col-span-4 p-3 bg-white rounded-md">
                 <table class="text-sm text-gray-500">
                     <tr>
                         <td class="">
                             <div>
                                 Current Selection :
                             </div>
                         </td>
                         <td>
                             <div class="ml-6 font-semibold text-blue">
                                 {{ $current }}
                             </div>
                         </td>
                     </tr>
                 </table>
             </div>
         </div>

         <div class="">
             <div class="flex justify-between">
                 <div class="flex items-center justify-start">
                     <ul class="flex mt-6">
                         @foreach ($status_header_cards as $i => $status_card)
                             <li
                                 class="px-3 py-1 text-xs {{ $status_header_cards[$i]['class'] }} {{ $stats == $status_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                                 <button
                                     wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                     {{ $status_header_cards[$i]['title'] }}
                                     <span class="ml-6 text-sm">{{ $status_header_cards[$i]['value'] }}</span>
                                 </button>
                             </li>
                         @endforeach
                     </ul>
                 </div>
                 <div class="flex items-center justify-end">
                     <ul class="flex pb-2 mt-6">
                         @can('oims_order_management_team_route_assignment_edit')
                             {{-- @dd($team_routes); --}}
                             {{-- @dd($team_routes[0]->teamDetails->teamRouteAssignmentReference->id); --}}
                             {{-- @dd($team_route) --}}


                             {{-- @foreach ($team_routes as $i => $team_route) --}}
                             {{-- @dd($team_route->teamDetails) --}}
                             {{-- @if (isset($team_routes[$i]->teamDetails)) --}}
                             {{-- @dd($team_route->teamDetails->teamRouteAssignmentReference->id) --}}
                             {{-- {{ dd($team_route->teamDetails->teamRouteAssignmentReference->id) }} --}}


                             {{-- @if (isset($team_routes[$i]->teamDetails)) --}}
                             {{-- @dd($getCurrent) --}}
                             @if ($getCurrent != null)
                                 <button
                                     wire:click="action({'id': {{ $getCurrent->id }}}, 'edit')"
                                     {{-- wire:click="action({'id': {{ $team_routes[0]->teamDetails->teamRouteAssignmentReference->id }}}, 'edit')" --}}
                                     class="p-1 px-3 mr-3 text-sm text-black bg-gray-200 border-2 border-gray-300 rounded-md hover:bg-blue-900 hover:text-white">
                                     <div class="flex items-start justify-between">
                                         <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false"
                                             data-prefix="far" data-icon="print-alt" role="img"
                                             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                             <path fill="currentColor"
                                                 d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z" />
                                         </svg>
                                         Edit
                                     </div>
                                 </button>
                             @endif
                             {{-- @endif --}}
                             {{-- @endforeach --}}
                         @endcan

                         @can('oims_order_management_team_route_assignment_share')
                             <button wire:click="action({}, 'share')"
                                 class="p-1 px-3 mr-3 text-sm text-black bg-gray-200 border-2 border-gray-300 rounded-md hover:bg-blue-900 hover:text-white">
                                 <div class="flex items-start justify-between">
                                     <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false"
                                         data-prefix="far" data-icon="print-alt" role="img"
                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                         <path fill="currentColor"
                                             d="M352 224H305.5c-45 0-81.5 36.5-81.5 81.5c0 22.3 10.3 34.3 19.2 40.5c6.8 4.7 12.8 12 12.8 20.3c0 9.8-8 17.8-17.8 17.8h-2.5c-2.4 0-4.8-.4-7.1-1.4C210.8 374.8 128 333.4 128 240c0-79.5 64.5-144 144-144h80V34.7C352 15.5 367.5 0 386.7 0c8.6 0 16.8 3.2 23.2 8.9L548.1 133.3c7.6 6.8 11.9 16.5 11.9 26.7s-4.3 19.9-11.9 26.7l-139 125.1c-5.9 5.3-13.5 8.2-21.4 8.2H384c-17.7 0-32-14.3-32-32V224zM80 96c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16H400c8.8 0 16-7.2 16-16V384c0-17.7 14.3-32 32-32s32 14.3 32 32v48c0 44.2-35.8 80-80 80H80c-44.2 0-80-35.8-80-80V112C0 67.8 35.8 32 80 32h48c17.7 0 32 14.3 32 32s-14.3 32-32 32H80z" />
                                     </svg>
                                     Share
                                 </div>
                             </button>
                         @endcan

                         @can('oims_order_management_team_route_assignment_print')
                             <button wire:click="action({}, 'print')"
                             {{-- wire:click="actionp({'id':{{ $edtr_list->id }}}, 'print')" --}}
                                 class="p-1 px-3 mr-3 text-sm text-black bg-gray-200 border-2 border-gray-300 rounded-md hover:bg-blue-900 hover:text-white">
                                 <div class="flex items-start justify-between">
                                     <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false"
                                         data-prefix="far" data-icon="print-alt" role="img"
                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                         <path fill="currentColor"
                                             d="M128 0C92.7 0 64 28.7 64 64v96h64V64H354.7L384 93.3V160h64V93.3c0-17-6.7-33.3-18.7-45.3L400 18.7C388 6.7 371.7 0 354.7 0H128zM384 352v32 64H128V384 368 352H384zm64 32h32c17.7 0 32-14.3 32-32V256c0-35.3-28.7-64-64-64H64c-35.3 0-64 28.7-64 64v96c0 17.7 14.3 32 32 32H64v64c0 35.3 28.7 64 64 64H384c35.3 0 64-28.7 64-64V384zM432 248a24 24 0 1 1 0 48 24 24 0 1 1 0-48z" />
                                     </svg>
                                     Print
                                 </div>
                             </button>
                         @endcan
                     </ul>
                 </div>
             </div>
             <div class="bg-white rounded-lg shadow-md">
                 <x-table.table class="overflow-hidden">
                     <x-slot name="thead">
                         <x-table.th name="No." style="padding-left:2%;" />
                         <x-table.th name="Team Name" style="padding-left:%;" />
                         <x-table.th name="Route Category" style="padding-left:%;" />
                         <x-table.th name="Plate Number" style="padding-left:%;" />
                         <x-table.th name="Driver" style="padding-left:%;" />
                         <x-table.th name="Checker 1" style="padding-left:%;" />
                         <x-table.th name="Checker 2" style="padding-left:%;" />
                         <x-table.th name="Remarks" style="padding-left:%;" />
                     </x-slot>
                     <x-slot name="tbody">
                        {{-- @dd($team_routes) --}}
                         @foreach ($team_routes as $i => $team_route)

                             <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-normal">
                                 <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:2%;">
                                     <p class="w-24">
                                         {{ ($team_routes->currentPage() - 1) * $team_routes->links()->paginator->perPage() + $loop->iteration }}
                                     </p>
                                 </td>
                                 <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                     {{ $team_routes[$i]->teamDetails->teamIdReference->name ?? '-' }}
                                 </td>
                                 <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                     {{ $team_routes[$i]->teamDetails->teamRouteReference->name ?? '-' }}
                                 </td>
                                 <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                     {{ $team_routes[$i]->plate_no }}
                                 </td>

                                 <td class="w-1/3 p-3 whitespace-nowrap " style="padding-left:;">
                                     @if (isset($team_routes[$i]->teamDetails))
                                         @if (isset($team_routes[$i]->teamDetails->teamDriverReference->userDetails->timeLog[0]['date']) &&
                                                 $team_routes[$i]->teamDetails->teamDriverReference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                             {{ $team_routes[$i]->teamDetails->teamDriverReference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamDriverReference->userDetails->last_name ?? '-' }}
                                         @else
                                             <p class="text-red">
                                                 {{ $team_routes[$i]->teamDetails->teamDriverReference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamDriverReference->userDetails->last_name ?? '-' }}
                                             </p>
                                         @endif
                                     @else
                                         <p class="text-center">-</p>
                                     @endif
                                 </td>


                                 <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                     @if (isset($team_routes[$i]->teamDetails))
                                         @if (isset($team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->timeLog[0]['date']) &&
                                                 $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                             {{ $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->last_name ?? '-' }}
                                         @else
                                             <p class="text-red" style="padding-left:;">
                                                 {{ $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker1Reference->userDetails->last_name ?? '-' }}
                                             </p>
                                         @endif
                                     @else
                                         <p class="text-center">-</p>
                                     @endif
                                 </td>


                                 <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                     @if (isset($team_routes[$i]->teamDetails))
                                         @if (isset($team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->timeLog[0]['date']) &&
                                                 $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                             @if ($team_routes[$i]->teamDetails->checker2_id != null)
                                                 {{ $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->last_name ?? '-' }}
                                             @else
                                                 <p class="text-center">-</p>
                                             @endif
                                         @else
                                             <p class="text-red" style="padding-left:;">
                                                 @if ($team_routes[$i]->teamDetails->checker2_id != null)
                                                     {{ $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->first_name . ' ' . $team_routes[$i]->teamDetails->teamChecker2Reference->userDetails->last_name ?? '-' }}
                                                 @else
                                                     <p class="text-center">-</p>
                                                 @endif
                                             </p>
                                         @endif
                                     @else
                                         <p class="text-center">-</p>
                                     @endif
                                 </td>

                                 <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                     @if (isset($team_routes[$i]->teamDetails))
                                         @if (isset($team_routes[$i]->teamDetails))
                                             @if ($team_routes[$i]->teamDetails->remarks != null)
                                                 {{ $team_routes[$i]->teamDetails->remarks ?? '-' }}
                                             @else
                                                 <p class="text-center">-</p>
                                             @endif
                                         @endif
                                     @else
                                         <p class="text-center">-</p>
                                     @endif
                                 </td>
                             </tr>
                         @endforeach
                     </x-slot>
                 </x-table.table>
                 <div class="px-1 pb-2">
                     {{ $team_routes->links() }}
                 </div>
             </div>
         </div>


     </x-slot>

 </x-form>

 <style type="text/css">
     /* body {
         font-family: 'Poppins';
     }

     @media print {
         body {
             display:none;

         }
         #tratoprint{
            display:block;
         }

     } */
 </style>




