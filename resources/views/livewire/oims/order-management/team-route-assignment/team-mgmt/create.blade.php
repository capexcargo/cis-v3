<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>
@if($confirmation_modal)
    <x-modal id="confirmation_modal" size="w-auto" hasClose="1">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to submit this new Team?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
@endif


    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="mt-5 space-y-3">
            <div class="grid grid-cols-12 gap-6">
                <div class="col-span-12">
                    <x-label for="name" value="Team Name" :required="true" />
                    <x-input type="text" name="name" wire:model.defer='name'></x-input>
                    <x-input-error for="name" />
                </div>

                <div class="col-span-12">

                    <x-label for="quad" value="Quadrant Name" :required="true" />
                    <div wire:init="quadrantReference" class="flex items-center">
                        <x-select class="w-full h-10 rounded-md" style="cursor: pointer;" name="quad"
                            wire:model='quad'>
                            <option value="">Select</option>
                            @foreach ($quadrantReferences as $quadrantReference)
                                <option value="{{ $quadrantReference->id }}">
                                    {{ $quadrantReference->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="quad" />
                </div>

                <div class="col-span-12">
                    <x-label for="name" value="Area" />
                    @if ($quad == null)
                        <x-input type="text" disabled placeholder="" name=""></x-input>
                    @endif

                    @foreach ($getAreas as $i => $getArea)
                        {{-- @dd($getAreas['name']); --}}
                        @if ($i != 0)
                            <x-label style="color: white;" for="" value="'" />
                        @endif
                        @if($quad != '')
                        <x-input type="text" disabled placeholder="{{ $getArea->name }}"
                            name="{{ $getArea->name }}"></x-input>
                        @endif
                    @endforeach
                </div>
            </div>

        </div>
        <div class="flex justify-end gap-3 mt-6 space-x-3">
            <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                class="px-12 bg-white text-blue hover:bg-gray-100" />
            <x-button type="submit" title="Submit" class="px-12 bg-blue text-white hover:bg-[#002161]" />
        </div>
    </form>
</div>
