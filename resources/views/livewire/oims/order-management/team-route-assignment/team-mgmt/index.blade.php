<x-form wire:init="load" x-data="{
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    reactivate_modal: '{{ $reactivate_modal }}',
    deactivate_modal: '{{ $deactivate_modal }}',
}">

    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @if($create_modal)
        @can('oims_order_management_team_route_assignment_team_mgmt_add')
            <x-modal id="create_modal" size="w-1/4" hasClose="1">
                <x-slot name="title">Tag Team</x-slot>
                <x-slot name="body">
                    @livewire('oims.order-management.team-route-assignment.team-mgmt.create')
                </x-slot>
            </x-modal>
        @endcan
        @endif
        @if($edit_modal)
        @can('oims_order_management_team_route_assignment_team_mgmt_edit')
            @if ($team_id && $edit_modal)
                <x-modal id="edit_modal" size="w-1/4" hasClose="1">
                    <x-slot name="title">Edit Team</x-slot>
                    <x-slot name="body">
                        @livewire('oims.order-management.team-route-assignment.team-mgmt.edit', ['id' => $team_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @endif
        @can('oims_order_management_team_route_assignment_team_mgmt_deactivate')
        @if($reactivate_modal)
            <x-modal id="reactivate_modal" size="w-auto" hasClose="1">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            Are you sure you want to reactivate this Team?
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('reactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">No</button>
                            <button wire:click="updateStatus('{{ $team_id }}', 1)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">Reactivate</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endif
            @if($deactivate_modal)
            <x-modal id="deactivate_modal" size="w-auto" hasClose="1">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            Are you sure you want to deactivate this Team?
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('deactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">No</button>
                            <button wire:click="updateStatus('{{ $team_id }}', 2)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-red">Deactivate</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endif

        @endcan
    </x-slot>

    <x-slot name="header_title">
        <div class="flex space-x-2">
            <svg wire:click="redirectTo({}, 'redirectToTeamRoute')" class="w-10 h-10 font-bold cursor-pointer text-blue"
                aria-hidden="true" focusable="false" data-prefix="far" data-icon="edit" role="img"
                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                <path fill="currentColor"
                    d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z">
                </path>
            </svg>
            <div>
                Team Management
            </div>
        </div>
    </x-slot>
    <x-slot name="header_button">
        @can('oims_order_management_team_route_assignment_team_mgmt_add')
            <button wire:click="action({}, 'add')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Tag Team
                </div>
            </button>
        @endcan

    </x-slot>


    <x-slot name="body">
        <div class="py-6">
            <div class="flex items-center justify-start">
                <ul class="flex mt-8">
                    @foreach ($status_header_cards as $i => $status_card)
                        <li
                            class="px-3 py-1 text-xs {{ $status_header_cards[$i]['class'] }} {{ $stats == $status_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                            <button
                                wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                {{ $status_header_cards[$i]['title'] }}
                                <span class="ml-6 text-sm">{{ $status_header_cards[$i]['value'] }}</span>
                            </button>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="No." style="padding-left:2%;" />
                        <x-table.th name="Quadrant" style="padding-left:%;" />
                        <x-table.th name="Area" style="padding-left:%;" />
                        <x-table.th name="Team Name" style="padding-left:%;" />
                        <x-table.th name="Action" style="padding-left:;" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($team_lists as $i => $team_list)
                            <tr class="font-normal border-0 cursor-pointer">
                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:2%;">
                                    <p class="w-24">
                                        {{ ($team_lists->currentPage() - 1) * $team_lists->links()->paginator->perPage() + $loop->iteration }}
                                    </p>
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ strtoupper($team_list->quadrantReference->name) }}
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    <table class="">
                                        @foreach ($team_list->quadrantReference->AreaReference as $c => $ar)
                                            <tr>
                                                <td>
                                                    {{ $team_list->quadrantReference->AreaReference[$c]['name'] }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>
                                <td class="w-3/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    <table class="">
                                        @foreach ($team_list->quadrantReference->TeamReference as $x => $ars)
                                            <tr>
                                                <td>
                                                    @if ($stats == 1)
                                                        @if ($team_list->quadrantReference->TeamReference[$x]['status'] == 1)
                                                            {{ strtoupper($team_list->quadrantReference->TeamReference[$x]['name']) }}
                                                        @endif
                                                    @elseif($stats == 2)
                                                        @if ($team_list->quadrantReference->TeamReference[$x]['status'] == 2)
                                                            {{ strtoupper($team_list->quadrantReference->TeamReference[$x]['name']) }}
                                                        @endif
                                                    @else
                                                        {{ strtoupper($team_list->quadrantReference->TeamReference[$x]['name']) }}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>
                                <td class="p-3 whitespace-nowrap" style="padding-left:;">
                                    @foreach ($team_list->quadrantReference->TeamReference as $z => $ars)
                                        <div class="flex space-x-1">
                                            @can('oims_order_management_team_route_assignment_team_mgmt_edit')
                                                @if ($stats == false)
                                                    @if ($team_list->quadrantReference->TeamReference[$z]['status'] == 1)
                                                        <span title="Edit">
                                                            <svg wire:click="action({'id': {{ $team_list->quadrantReference->TeamReference[$z]['id'] }}}, 'edit')"
                                                                class="w-5 h-5 text-blue" aria-hidden="true"
                                                                focusable="false" data-prefix="far" data-icon="edit"
                                                                role="img" xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 576 512">
                                                                <path fill="currentColor"
                                                                    d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                                                </path>
                                                            </svg>
                                                        </span>
                                                    @else
                                                        <svg class="w-5 h-5" style="color: #F3F4F6;" aria-hidden="true"
                                                            focusable="false" data-prefix="far" data-icon="edit"
                                                            role="img" xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 576 512">
                                                            <path fill="currentColor"
                                                                d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                                            </path>
                                                        </svg>
                                                    @endif
                                                @endif
                                            @endcan
                                            @can('oims_order_management_team_route_assignment_team_mgmt_deactivate')
                                                <span title="Deactivate">
                                                    @if ($stats != 2)
                                                        <svg x-cloak
                                                            x-show="'{{ $team_list->quadrantReference->TeamReference[$z]['status'] == 1 }}'"
                                                            wire:click="action({'id': {{ $team_list->quadrantReference->TeamReference[$z]['id'] }}, 'status': 2}, 'update_status')"
                                                            class="w-8 h-5 pl-2 text-blue" aria-hidden="true"
                                                            focusable="false" data-prefix="fas" data-icon="user-slash"
                                                            role="img" xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 640 512">
                                                            <path fill="currentColor"
                                                                d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                            </path>
                                                        </svg>
                                                    @endif
                                                </span>
                                                <span title="Reactivate" class="">
                                                    @if ($stats != 1)
                                                        <svg x-cloak
                                                            x-show="'{{ $team_list->quadrantReference->TeamReference[$z]['status'] == 2 }}'"
                                                            wire:click="action({'id': {{ $team_list->quadrantReference->TeamReference[$z]['id'] }}, 'status': 1}, 'update_status')"
                                                            class="w-8 h-5 pl-1 text-gray-400 rotate-180 "
                                                            aria-hidden=" true" focusable="false" data-prefix="fas"
                                                            data-icon="user" role="img"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                                            <path fill="currentColor"
                                                                d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zm192 96a96 96 0 1 1 0 192 96 96 0 1 1 0-192z">
                                                            </path>
                                                        </svg>
                                                    @endif
                                                </span>
                                            @endcan
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $team_lists->links() }}
                </div>
            </div>
    </x-slot>

</x-form>
