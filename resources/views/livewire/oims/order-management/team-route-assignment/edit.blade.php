<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>
    @if($confirmation_modal)
    <x-modal id="confirmation_modal" size="w-auto" hasClose="1">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to save this Team Route Assignment?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
    @endif

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="col-span-12">
            <h1 class="mt-1 text-xl font-semibold text-left text-blue ">Edit Team
            </h1>
        </div>
        <div class="grid grid-cols-12">
            <div class="col-span-4 py-4">
                <table class="text-sm text-gray-500">
                    <tr>
                        <td class="">
                            <div>
                                TRA No :
                            </div>
                        </td>
                        <td>
                            <div>
                                <x-input disabled class="w-full h-8 ml-4 text-sm text-gray-500" type="text"
                                    name="tra" wire:model.defer='tra'></x-input>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="pt-2">
                            <div>
                                Branch :
                            </div>
                        </td>
                        <td>
                            <div>
                                <x-input disabled class="w-full h-8 mt-2 ml-4 text-sm text-gray-500" type="text"
                                    name="branch" wire:model.defer='branch'></x-input>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-span-8 py-4">
                <table class="text-sm text-gray-500">
                    <tr>
                        <td class="pt-10">
                            <div>
                                Dispatch Date :
                            </div>
                        </td>
                        <td>
                            <div>
                                <x-input class="w-full h-8 mt-10 ml-4" type="date" name="dis_date"
                                    wire:model.defer='dis_date'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="dis_date" />
                            </div>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
        <div class="mt-5 space-y-3">
            <div class="grid grid-cols-12 gap-6">
                <div class="col-span-12 bg-white border border-gray-300 rounded-lg shadow-md">
                    <x-table.table class="overflow-hidden">
                        <x-slot name="thead">
                            <x-table.th name="Team Name" style="padding-left:;" />
                            <x-table.th name="Plate Number" style="padding-left:%;" />
                            <x-table.th name="Route Category" style="padding-left:%;" />
                            <x-table.th name="Driver" style="padding-left:%;" />
                            <x-table.th name="Checker 1" style="padding-left:%;" />
                            <x-table.th name="Checker 2" style="padding-left:%;" />
                            <x-table.th name="Remarks" style="padding-left:%;" />
                            <x-table.th name="Action" style="padding-left:4%;" />
                        </x-slot>
                        <x-slot name="tbody">
                            <?php $i = 0; ?>
                            @foreach ($teams as $a => $team)
                                @if (!$team['is_deleted'])
                                    <tr class="text-xs font-normal border-0 cursor-pointer ">
                                        <td class="relative w-1/12 p-2 pb-4 whitespace-nowrap" style="padding-left:;">
                                            <div wire:init="teamReference" class="flex">
                                                <x-select class="w-full h-10 rounded-md" style="cursor: pointer;"
                                                    name="teams.{{ $a }}.team_name"
                                                    wire:model.defer='teams.{{ $a }}.team_name'>
                                                    <option value="">Select</option>
                                                    @foreach ($teamReferences as $teamReference)
                                                        <option value="{{ $teamReference->id }}">
                                                            {{ $teamReference->name }}
                                                        </option>
                                                    @endforeach
                                                </x-select>
                                            </div>
                                            <x-input-error class="absolute text-xs"
                                                for="teams.{{ $a }}.team_name" />
                                        </td>
                                        <td class="w-1/12 p-2 pb-4 whitespace-nowrap" style="padding-left:;">
                                            <div wire:init="plateReference" class="flex">
                                                <x-select class="w-full h-10 rounded-md" style="cursor: pointer;"
                                                    name="teams.{{ $a }}.plate"
                                                    wire:model.defer='teams.{{ $a }}.plate'>
                                                    <option value="">Select</option>
                                                    @foreach ($plateReferences as $plateReference)
                                                        <option value="{{ $plateReference->id }}">
                                                            {{ $plateReference->plate_no }}
                                                        </option>
                                                    @endforeach
                                                </x-select>
                                            </div>
                                            <x-input-error class="absolute text-xs"
                                                for="teams.{{ $a }}.plate" />
                                        </td>
                                        <td class="w-1/12 p-2 pb-4 whitespace-nowrap" style="padding-left:;">
                                            <div wire:init="routeReference" class="flex">
                                                <x-select class="w-full h-10 rounded-md" style="cursor: pointer;"
                                                    name="teams.{{ $a }}.route"
                                                    wire:model.defer='teams.{{ $a }}.route'>
                                                    <option value="">Select</option>
                                                    @foreach ($routeReferences as $routeReference)
                                                        <option value="{{ $routeReference->id }}">
                                                            {{ $routeReference->name }}
                                                        </option>
                                                    @endforeach
                                                </x-select>
                                            </div>
                                            <x-input-error class="absolute text-xs"
                                                for="teams.{{ $a }}.route" />
                                        </td>
                                        <td class="w-1/12 p-2 pb-4 whitespace-nowrap" style="padding-left:;">
                                            <div wire:init="driverReference" class="flex">
                                                <x-select class="w-full h-10 rounded-md" style="cursor: pointer;"
                                                    name="teams.{{ $a }}.driver"
                                                    wire:model.defer='teams.{{ $a }}.driver'>
                                                    <option value="">Select</option>
                                                    @foreach ($driverReferences as $driverReference)
                                                        <option value="{{ $driverReference->user_id }}">
                                                            {{ $driverReference->first_name . ' ' . $driverReference->last_name }}
                                                        </option>
                                                    @endforeach
                                                </x-select>
                                            </div>
                                            <x-input-error class="absolute text-xs"
                                                for="teams.{{ $a }}.driver" />
                                        </td>
                                        <td class="w-1/12 p-2 pb-4 whitespace-nowrap" style="padding-left:;">
                                            <div wire:init="checkerReference" class="flex">
                                                <x-select class="w-full h-10 rounded-md" style="cursor: pointer;"
                                                    name="teams.{{ $a }}.check_1"
                                                    wire:model.defer='teams.{{ $a }}.check_1'>
                                                    <option value="">Select</option>
                                                    @foreach ($checkerReferences as $checkerReference)
                                                        <option value="{{ $checkerReference->user_id }}">
                                                            {{ $checkerReference->first_name . ' ' . $checkerReference->last_name }}
                                                        </option>
                                                    @endforeach
                                                </x-select>
                                            </div>
                                            <x-input-error class="absolute text-xs"
                                                for="teams.{{ $a }}.check_1" />
                                        </td>
                                        <td class="w-1/12 p-2 pb-4 whitespace-nowrap" style="padding-left:;">
                                            <div wire:init="checkerReference" class="flex">
                                                <x-select class="w-full h-10 rounded-md" style="cursor: pointer;"
                                                    name="teams.{{ $a }}.check_2"
                                                    wire:model.defer='teams.{{ $a }}.check_2'>
                                                    <option value="">Select</option>
                                                    @foreach ($checkerReferences as $checkerReference)
                                                        <option value="{{ $checkerReference->user_id }}">
                                                            {{ $checkerReference->first_name . ' ' . $checkerReference->last_name }}
                                                        </option>
                                                    @endforeach
                                                </x-select>
                                            </div>
                                            <x-input-error class="absolute text-xs"
                                                for="teams.{{ $a }}.check_2" />
                                        </td>
                                        <td class="w-1/12 p-2 pb-4 whitespace-nowrap" style="padding-right:;">
                                            <div class="flex">
                                                <x-input class="w-full h-10 rounded-md" type="text"
                                                    name="teams.{{ $a }}.remarks"
                                                    wire:model.defer='teams.{{ $a }}.remarks'></x-input>
                                            </div>
                                            <x-input-error class="absolute text-xs"
                                                for="teams.{{ $a }}.remarks" />
                                        </td>
                                        <td class="w-1/12 p-2 pb-4 text-center whitespace-nowrap"
                                            style="padding-left:2.5%;">
                                            <div class="flex space-x-4">
                                                @if ($nearest > $min)
                                                    <div class="">
                                                        <x-label for="process" value="" />
                                                        <svg wire:click="removeteam({'a': {{ $a }}})"
                                                            class="z-10 w-8 h-6 cursor-pointer text-red"
                                                            aria-hidden="true" focusable="false" data-prefix="fas"
                                                            data-icon="circle-minus" role="img"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                            <path fill="currentColor"
                                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z">
                                                            </path>
                                                        </svg>
                                                    </div>
                                                @endif

                                                {{-- @if (count($teams) == 1)
                                                    <div class="">
                                                        <x-label for="TellNo" value="" />
                                                        <svg wire:click="addteam({{ $a + 1 }})"
                                                            class="w-8 h-6 cursor-pointer text-blue"
                                                            aria-hidden="true" focusable="false" data-prefix="fas"
                                                            data-icon="trash-alt" role="img"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                            <path fill="currentColor"
                                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                            </path>
                                                        </svg>
                                                    </div>
                                                @endif --}}

                                                {{-- @if ($i != 0) --}}
                                                @if ($nearest == $a)
                                                    @if ($min == $a)
                                                        <div class="">
                                                            <x-label for="TellNo" value="" />
                                                            <svg wire:click="addteam()"
                                                                class="w-8 h-6 cursor-pointer text-blue"
                                                                aria-hidden="true" focusable="false"
                                                                data-prefix="fas" data-icon="trash-alt"
                                                                role="img" xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 448 512">
                                                                <path fill="currentColor"
                                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                                </path>
                                                            </svg>
                                                        </div>
                                                    @else
                                                        <div class="">
                                                            <x-label for="TellNo" value="" />
                                                            <svg wire:click="addteam()"
                                                                class="w-8 h-6 cursor-pointer text-blue"
                                                                aria-hidden="true" focusable="false"
                                                                data-prefix="fas" data-icon="trash-alt"
                                                                role="img" xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 448 512">
                                                                <path fill="currentColor"
                                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                                </path>
                                                            </svg>
                                                        </div>
                                                    @endif
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                <?php $i++; ?>
                            @endforeach
                        </x-slot>
                    </x-table.table>
                </div>
            </div>

        </div>
        <div class="flex justify-end gap-3 mt-6 space-x-3">
            <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                class="px-12 bg-white text-blue hover:bg-gray-100" />
            <x-button type="submit" title="Save" class="px-12 bg-blue text-white hover:bg-[#002161]" />
        </div>
    </form>
</div>
