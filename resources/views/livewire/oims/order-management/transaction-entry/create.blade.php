<div class="px-6 pb-6 text-gray-800" wire:init="load" x-data="{
    scan_modal: '{{ $scan_modal }}',
    view_remarks_modal: '{{ $view_remarks_modal }}',
    add_remarks_modal: '{{ $add_remarks_modal }}',
    override_history_modal: '{{ $override_history_modal }}',
    waybill_summary: '{{ $waybill_summary }}',
    select_cons_modal: '{{ $select_cons_modal }}',
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading />
    {{-- <x-form wire:init="" x-data="{
        scan_modal: '{{ $scan_modal }}',
    }"> --}}

    @if ($confirmation_modal)
        <x-modal id="confirmation_modal" size="w-auto" hasClose="1">
            <x-slot name="body">
                <span class="relative block">
                    <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                        wire:click="$set('confirmation_modal', false)">
                    </span>
                </span>
                <h2 class="text-xl text-center">
                    Are you sure you want to save changes?
                </h2>

                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                        No
                    </button>
                    <button type="button" wire:click="submit"
                        class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                        Yes
                    </button>
                </div>
            </x-slot>
        </x-modal>
    @endif

    @if ($scan_modal)
        @can('oims_order_management_transaction_entry_scan')
            <x-modal id="scan_modal" size="w-2/5">
                {{-- <x-slot name="title">Scan Waybill QR Code</x-slot> --}}
                <x-slot name="body">
                    @livewire('oims.order-management.transaction-entry.scan')
                </x-slot>
            </x-modal>
        @endcan
    @endif
    @if ($view_remarks_modal)
        @can('oims_order_management_transaction_entry_view_remarks')
            {{-- @if ($summary_id && $terms_modal) --}}
            <x-modal id="view_remarks_modal" size="w-2/3">
                {{-- <x-slot name="title">Terms and Conditions</x-slot> --}}
                <x-slot name="body">
                    @livewire('oims.order-management.transaction-entry.view-remarks')
                    {{-- @livewire('crm.sales.sales-campaign.summary.terms-view', ['id' => $summary_id]) --}}
                </x-slot>
            </x-modal>
            {{-- @endif --}}
        @endcan
    @endif
    @if ($add_remarks_modal)
        @can('oims_order_management_transaction_entry_add_remarks')
            {{-- @if ($summary_id && $terms_modal) --}}
            <x-modal id="add_remarks_modal" size="w-11/12">
                {{-- <x-slot name="title">Terms and Conditions</x-slot> --}}
                <x-slot name="body">
                    @livewire('oims.order-management.transaction-entry.add-remarks')
                    {{-- @livewire('crm.sales.sales-campaign.summary.terms-view', ['id' => $summary_id]) --}}
                </x-slot>
            </x-modal>
        @endcan
    @endif

    @if ($override_history_modal)
        @can('oims_order_management_transaction_entry_override_history')
            {{-- @if ($summary_id && $terms_modal) --}}
            <x-modal id="override_history_modal" size="w-3/5">
                {{-- <x-slot name="title">Terms and Conditions</x-slot> --}}
                <x-slot name="body">
                    @livewire('oims.order-management.transaction-entry.override-history')
                    {{-- @livewire('crm.sales.sales-campaign.summary.terms-view', ['id' => $summary_id]) --}}
                </x-slot>
            </x-modal>
            {{-- @endif --}}
        @endcan
    @endif

    @if ($select_cons_modal)
        <x-modal id="select_cons_modal" size="w-1/3">
            {{-- <x-slot name="title">Select Consignee</x-slot> --}}
            <x-slot name="body">
                <form wire:submit.prevent="confirmationSubmitSelected" autocomplete="off">
                    <div class="col-span-12">
                        <div class="ml-1 text-lg font-semibold capitalize">SELECT CONSIGNEE</div>
                    </div>
                    <hr class="col-span-12 mt-1 text-blue-800 border border-blue-800">
                    <div class="col-span-12 mt-2 bg-white rounded-lg shadow-md">
                        <table class="w-full">
                            <thead>
                                <tr class="">
                                    <th class="px-3 py-2 text-xs font-normal tracking-wider text-left text-black ">
                                    </th>
                                    <th class="px-3 py-2 text-xs font-normal tracking-wider text-left text-black ">
                                        Consignee
                                        Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($bookings['BookingConsigneeHasManyBK'] as $i => $booking)
                                    <tr class="font-normal divide-y-4 divide-white cursor-pointer">
                                        <td class="px-3 py-2 text-sm font-medium border-2 border-b border-white whitespace-nowrap"
                                            style="background-color: #F7F7F7;">
                                            <input type="radio" name="cons" wire:model='cons'
                                                value="{{ $booking['id'] }}" class="w-4 h-4 border border-blue">
                                        </td>

                                        <td class="px-3 py-2 text-sm font-medium whitespace-nowrap"
                                            style="background-color: #F7F7F7;">
                                            {{ $booking['name'] }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="flex justify-end gap-2 mt-6 space-x-2">
                        <x-button type="button" wire:click="closecreatemodal" title="Delete Consignee"
                            class="px-8 bg-white border-none text-red hover:bg-gray-100" />
                        <x-button type="submit" title="Next" class="px-8 bg-blue text-white hover:bg-[#002161]" />
                    </div>
                </form>
            </x-slot>
        </x-modal>
    @endif



    {{-- @can('oims_order_management_transaction_entry_waybill_summary')
        <x-modal id="waybill_summary_modal" size="w-10/12">
            <x-slot name="body">
                @livewire('oims.order-management.transaction-entry.waybill-summary')
            </x-slot>
        </x-modal>
    @endcan --}}
    <div class="pb-10 mt-6 space-y-6">

        <div class="grid grid-cols-12 bg-white rounded-lg shadow-md ">
            <div class="col-span-12 p-2 px-6 border-b-2 border-blue-800 rounded-tl-lg rounded-tr-lg bg-blue ">
                <span class="w-full text-base font-medium text-white rounded-tr-lg bg-blue" style="">
                    TRANSACTION DETAILS
                </span>
            </div>

            <div class="col-span-8 p-6">
                <table class="w-full text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Service :
                            </div>
                        </td>
                        <td class="">
                            <div wire:init="" class="text-base">
                                <x-select class="rounded-md h-9" style="cursor: pointer; width: 51.1%" name="service"
                                    wire:model.defer='service'>
                                    <option value="">Select</option>
                                    {{-- @foreach ($plateReferences as $plateReference)
                                        <option value="{{ $plateReference->id }}">
                                            {{ $plateReference->plate_no }}
                                        </option>
                                    @endforeach --}}
                                </x-select>
                            </div>
                        </td>
                    </tr>

                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Waybill Number :
                            </div>
                        </td>
                        <td class="">
                            <div>
                                <x-input class="text-sm rounded-md h-9" style="width: 112%" type="text"
                                    name="waybill_no" wire:model.defer='waybill_no'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="waybill_no" />
                            </div>
                        </td>
                        <td class="" style="padding-left: 6%">
                            <div class="flex space-x-2">
                                <div class="">
                                    <button wire:click=""
                                        class="px-6 py-2 text-sm text-white rounded-md bg-blue hover:bg-blue-800 ">
                                        <div class="flex items-start justify-between">
                                            <svg class="w-4 h-4 mr-2" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="print-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z" />
                                            </svg>
                                            Search
                                        </div>
                                    </button>
                                </div>
                                <div class="">
                                    @can('oims_order_management_transaction_entry_scan')
                                        <button wire:click="action({}, 'scan')"
                                            class="px-6 py-2 text-sm text-white rounded-md bg-blue hover:bg-blue-800 ">
                                            <div class="flex items-start justify-between">
                                                <svg class="w-4 h-4 mr-2" aria-hidden="true" focusable="false"
                                                    data-prefix="far" data-icon="print-alt" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                    <path fill="currentColor"
                                                        d="M0 80C0 53.5 21.5 32 48 32h96c26.5 0 48 21.5 48 48v96c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V80zM64 96v64h64V96H64zM0 336c0-26.5 21.5-48 48-48h96c26.5 0 48 21.5 48 48v96c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V336zm64 16v64h64V352H64zM304 32h96c26.5 0 48 21.5 48 48v96c0 26.5-21.5 48-48 48H304c-26.5 0-48-21.5-48-48V80c0-26.5 21.5-48 48-48zm80 64H320v64h64V96zM256 304c0-8.8 7.2-16 16-16h64c8.8 0 16 7.2 16 16s7.2 16 16 16h32c8.8 0 16-7.2 16-16s7.2-16 16-16s16 7.2 16 16v96c0 8.8-7.2 16-16 16H368c-8.8 0-16-7.2-16-16s-7.2-16-16-16s-16 7.2-16 16v64c0 8.8-7.2 16-16 16H272c-8.8 0-16-7.2-16-16V304zM368 480a16 16 0 1 1 0-32 16 16 0 1 1 0 32zm64 0a16 16 0 1 1 0-32 16 16 0 1 1 0 32z" />
                                                </svg>
                                                Scan
                                            </div>
                                        </button>
                                    @endcan
                                </div>

                            </div>
                        </td>
                    </tr>
                </table>

            </div>

            <div class="col-span-4 p-6">
                <div class="flex justify-end text-blue whitespace-nowrap" x-data="{ open: false }">
                    <button @click="open = !open"
                        class="px-4 py-2 text-sm text-white rounded-lg bg-blue hover:bg-blue-800">
                        <div class="flex items-center justify-between">
                            Action
                            <svg class="w-3 h-3 ml-2" aria-hidden="true" focusable="false" data-prefix="far"
                                data-icon="print-alt" role="img" x mlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 448 512">
                                <path fill="currentColor"
                                    d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                            </svg>
                        </div>
                    </button>
                    <div class="absolute z-10 mt-10 border border-gray-500 rounded-md" style="margin-left: -7.9rem">
                        <ul class="p-3 font-medium text-gray-600 bg-white rounded shadow" x-show="open"
                            @click.away="open = false">
                            <li class="px-3 py-1 cursor-pointer" x-data="{ open: false }"
                                wire:click="action({}, 'waybill_summary')">
                                {{-- wire:click="action({'id': {{ $transaction['id'] }}}, 'waybill_summary')" --}}
                                Print</a>
                            </li>
                            <hr class="text-black border border-gray-300">
                            <li class="px-3 py-1 cursor-pointer" x-data="{ open: false }"
                                wire:click="redirectTo({}, '')">
                                Quality Check</a>
                            </li>
                            <hr class="text-black border border-gray-300">
                            <li class="px-3 py-1 cursor-pointer" x-data="{ open: false }"
                                wire:click="redirectTo({}, '')">
                                Override</a>
                            </li>
                            <hr class="text-black border border-gray-300">
                            <li class="px-3 py-1 cursor-pointer" x-data="{ open: false }"
                                wire:click="redirectTo({}, '')">
                                Cancel</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-span-12">
                <hr class="text-black border border-gray-300">
            </div>

            <div class="col-span-6 p-6">
                <table class="w-full text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Transaction Type :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div wire:init="" class="text-base">
                                <x-select class="pl-2 rounded-md h-9" style="cursor: pointer; width: 80%"
                                    name="trans_type" wire:model.defer='trans_type'>
                                    <option value="">Select</option>
                                    <option value="1">Pick Up</option>
                                    <option value="2">Walk In</option>
                                </x-select>
                            </div>
                        </td>
                    </tr>

                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Booking Reference Number :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="book_ref_no" wire:model.defer='book_ref_no'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="book_ref_no" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Booking Type :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div wire:init="bookingTypeReferences" class="text-base">
                                <x-select class="pl-2 rounded-md h-9" style="cursor: pointer; width: 80%"
                                    name="book_type" wire:model.defer='book_type'>
                                    <option value="">Select</option>
                                    @foreach ($booking_type_references as $booking_type_reference)
                                        <option value="{{ $booking_type_reference->id }}">
                                            {{ $booking_type_reference->name }}
                                        </option>
                                    @endforeach
                                </x-select>
                            </div>
                        </td>
                    </tr>

                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Origin :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div class="text-base">
                                <x-select class="pl-2 rounded-md h-9" style="cursor: pointer; width: 80%"
                                    name="origin" wire:model.defer='origin'>
                                    <option value="">Select</option>
                                    @foreach ($origin_references as $origin_reference)
                                        <option value="{{ $origin_reference->id }}">
                                            {{ $origin_reference->display }}
                                        </option>
                                    @endforeach
                                </x-select>
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Transipment Port :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-select class="pl-2 rounded-md h-9" style="cursor: pointer; width: 80%"
                                    name="trans_port" wire:model.defer='trans_port'>
                                    <option value="">Select</option>
                                    @foreach ($transhipment_references as $transhipment_reference)
                                        <option value="{{ $transhipment_reference->id }}">
                                            {{ $transhipment_reference->display }}
                                        </option>
                                    @endforeach
                                </x-select>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-span-6 p-6">
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Branch :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="branch" wire:model.defer='branch'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="branch" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Transport Date :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="trans_date" wire:model.defer='trans_date'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="trans_date" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Transport Mode :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div wire:init="transportModeReferences" class="text-base">
                                <x-select class="pl-2 rounded-md h-9" style="cursor: pointer; width: 80%"
                                    name="transport_mode" wire:model='transport_mode'>
                                    <option value="">Select</option>
                                    @foreach ($transport_mode_references as $transport_mode_reference)
                                        <option value="{{ $transport_mode_reference->id }}">
                                            {{ $transport_mode_reference->name }}
                                        </option>
                                    @endforeach
                                </x-select>
                            </div>
                        </td>
                        {{-- @dd($transport_mode) --}}
                    </tr>

                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Destination :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-select class="pl-2 rounded-md h-9" style="cursor: pointer; width: 80%"
                                    name="destination" wire:model.defer='destination'>
                                    <option value="">Select</option>
                                    @foreach ($destination_references as $destination_reference)
                                        <option value="{{ $destination_reference->id }}">
                                            {{ $destination_reference->display }}
                                        </option>
                                    @endforeach
                                </x-select>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-span-12">
                <hr class="text-black border border-gray-300">
            </div>

            <div class="col-span-6 p-6">
                <h1 class="text-xl font-semibold text-blue">Shipper Details</h1>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium whitespace-nowrap">
                            <div>
                                Customer Number :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="scust_no" wire:model.defer='scust_no'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="scust_no" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Company Name/Name :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="scname" wire:model.defer='scname'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="scname" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Contact Person :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="scont_person" wire:model.defer='scont_person'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="scont_person" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Mobile Number :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="smobile" wire:model.defer='smobile'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="smobile" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Address :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="sadrs" wire:model.defer='sadrs'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="sadrs" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-span-6 p-6 border-l-2 border-gray-300 border-dashed">
                <h1 class="text-xl font-semibold text-blue">Consignee Details</h1>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium whitespace-nowrap">
                            <div>
                                Customer Number :
                            </div>
                        </td>
                        <td class="" style="padding-left: 8.5%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80.5%" type="text"
                                    name="ccust_no" wire:model.defer='ccust_no'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="ccust_no" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Company Name/Name :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="ccname" wire:model.defer='ccname'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="ccname" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Contact Person :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="ccont_person" wire:model.defer='ccont_person'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="ccont_person" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Mobile Number :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="cmobile" wire:model.defer='cmobile'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="cmobile" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Address :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="cadrs" wire:model.defer='cadrs'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="cadrs" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-span-12">
                <hr class="text-black border border-gray-300">
            </div>

            <div class="col-span-6 p-6">
                <h1 class="text-xl font-semibold text-blue">Cargo Details</h1>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Description of Goods :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="des_goods" wire:model.defer='des_goods'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="des_goods" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Type of Goods : <p class="italic font-normal text-gray-400">(Optional)</p>
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div wire:init="" class="text-base">
                                <x-select class="pl-2 rounded-md h-9" style="cursor: pointer; width: 80%"
                                    name="types_goods" wire:model.defer='types_goods'>
                                    <option value="">Select</option>
                                    {{-- @foreach ($plateReferences as $plateReference)
                                        <option value="{{ $plateReference->id }}">
                                            {{ $plateReference->plate_no }}
                                        </option>
                                    @endforeach --}}
                                </x-select>
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Paymode :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div wire:init="paymoderef" class="text-base">
                                <x-select class="pl-2 rounded-md h-9" style="cursor: pointer; width: 80%"
                                    name="paymode" wire:model.defer='paymode'>
                                    <option value="">Select</option>
                                    @foreach ($paymoderefs as $paymoderef)
                                        <option value="{{ $paymoderef->id }}">
                                            {{ $paymoderef->name }}
                                        </option>
                                    @endforeach
                                </x-select>

                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Charge To :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div wire:init="chargetoref" class="text-base">
                                @if ($consacctype == 2)
                                    <x-select class="pl-2 rounded-md h-9" style="cursor: pointer; width: 80%"
                                        name="charge_to" wire:model.defer='charge_to'>
                                        <option value="">Select</option>
                                        @foreach ($chargetorefs as $chargetoref)
                                            <option value="{{ $chargetoref->id }}">
                                                {{ $chargetoref->fullname }}
                                            </option>
                                        @endforeach
                                    </x-select>
                                @else
                                    <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                        name="charge_to" wire:model.defer='charge_to'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="charge_to" />
                                @endif
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-span-6 p-6">
                <h1 class="text-xl font-semibold text-blue"></h1>
                <table class="w-full mt-10 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Commodity Type :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div wire:init="" class="text-base">
                                <x-select class="pl-2 rounded-md h-9" style="cursor: pointer; width: 80%"
                                    name="com_type" wire:model.defer='com_type'>
                                    <option value="">Select</option>
                                    @foreach ($commodity_type_references as $commodity_type_reference)
                                        <option value="{{ $commodity_type_reference->id }}">
                                            {{ $commodity_type_reference->name }}
                                        </option>
                                    @endforeach
                                </x-select>
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Declared Value
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="dec_val" wire:model.defer='dec_val'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="dec_val" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Service Mode :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div wire:init="servmoderef" class="text-base">
                                <x-select class="pl-2 rounded-md h-9" style="cursor: pointer; width: 80%"
                                    name="serv_mode" wire:model.defer='serv_mode'>
                                    <option value="">Select</option>
                                    @foreach ($servmoderefs as $servmoderef)
                                        <option value="{{ $servmoderef->id }}">
                                            {{ $servmoderef->name }}
                                        </option>
                                    @endforeach
                                </x-select>
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Commodity Applicable Rate :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div wire:init="" class="text-base">
                                @if ($transport_mode == 1)
                                    <x-select class="pl-2 rounded-md h-9" style="cursor: pointer; width: 80%"
                                        name="com_app_rate" wire:model.defer='com_app_rate'>
                                        <option value="">Select</option>
                                        @foreach ($commodity_rate_airs as $commodity_rate_air)
                                            <option value="{{ $commodity_rate_air->id }}">
                                                {{ $commodity_rate_air->name }}
                                            </option>
                                        @endforeach
                                    </x-select>
                                @endif

                                @if ($transport_mode == 2)
                                    <x-select class="pl-2 rounded-md h-9" style="cursor: pointer; width: 80%"
                                        name="com_app_rate" wire:model.defer='com_app_rate'>
                                        <option value="">Select</option>
                                        @foreach ($commodity_rate_seas as $commodity_rate_sea)
                                            <option value="{{ $commodity_rate_sea->id }}">
                                                {{ $commodity_rate_sea->name }}
                                            </option>
                                        @endforeach
                                    </x-select>
                                @endif
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-span-12">
                <hr class="text-black border border-gray-300">
            </div>

            <div class="col-span-12 p-6">
                <h1 class="text-xl font-semibold text-blue">Item Details</h1>
            </div>

            <div class="col-span-8 p-6 pt-0">
                {{-- <div class="bg-white border rounded-lg shadow-lg ">
                    <x-table.table class="overflow-hidden text-center">
                        <x-slot name="thead">
                            <th name="" style="font-weight:500;">Qty</th>
                            <th name="" style="font-weight:500;">Weight</th>
                            <th name="" style="font-weight:500;">Dimensions</th>
                            <th name="" style="font-weight:500; white-space: normal;">Unit
                                of Measurement</th>
                            <th name="" style="font-weight:500;white-space: normal;">
                                Measurement Type</th>
                            <th name="" style="font-weight:500;white-space: normal;">Type
                                Of Packaging</th>
                            <th name="" style="font-weight:500;">For Crating</th>
                            <th name="" style="font-weight:500;white-space: normal;">
                                Crating Status</th>
                            <th name="" style="font-weight:500;white-space: normal;">
                                Crating Type</th>
                            <th name="" style="font-weight:500;">CWT</th>
                            <th name="" style="font-weight:500;">Action</th>
                        </x-slot>
                        <x-slot name="tbody">
                            <tr class="font-normal border-0 cursor-pointer">
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    1
                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    35
                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    25x25x25
                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    in
                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    Per Piece
                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    Crate
                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    <input type="radio" name=""
                                        class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                                        value="" wire:model="" >
                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    Crated
                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    SCRT
                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    0.009
                                </td>
                                <td class="p-3 whitespace-nowrap" style="padding-left:;">

                                </td>
                            </tr>
                        </x-slot>
                    </x-table.table>
                </div> --}}
                @if ($transport_mode == 1)
                    <div class="my-6">
                        <div>
                            <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                <div class="mt-[-8px]">
                                    <input type="checkbox"
                                        class="w-6 h-6 rounded-md  border-[#003399] border cursor-pointer"
                                        name="air_cargo" wire:model='air_cargo'
                                        @if ($air_cargo) checked @endif>
                                </div>
                                <div class="-mt-2 text-lg text-black">Air Cargo</div>
                            </div>
                            @if ($air_cargo)
                                <div class="flex px-2.5 mt-2">
                                    <div class="overflow-hidden border-l-4 border-[#003399]">
                                        <table class="text-md text-[#003399] ml-4">
                                            <thead>
                                                <tr class="text-left">
                                                    <th class="w-10 px-1">
                                                        <x-label value="Qty" class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="w-10 px-1">
                                                        <x-label value="Wt" class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="w-20 px-1">
                                                        <x-label value="Dimension (LxWxH)"
                                                            class="text-[10px] leading-tight " />
                                                    </th>
                                                    <th class="w-10 px-1">
                                                        <x-label value="Unit of Measurement"
                                                            class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="w-[20px] px-1">
                                                        <x-label value="Measurement Type"
                                                            class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="w-[20px] px-1">
                                                        <x-label value="Type of Packaging"
                                                            class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="w-10 px-1">
                                                        <x-label value="For Crating"
                                                            class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="w-10 px-1">
                                                        <x-label value="Crating Type"
                                                            class="text-[10px] leading-tight whitespace-nowrap" />
                                                    </th>
                                                    <th class="w-10 px-1">
                                                        <x-label value="CWT" class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="px-2"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 0; ?>
                                                @foreach ($air_cargos as $a => $aircargo)
                                                    <tr class="">
                                                        <td class="">
                                                            <div class="-mt-2">
                                                                <input type="text"
                                                                    class="w-6 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                    name="air_cargos.{{ $a }}.ac_qty"
                                                                    wire:model.defer='air_cargos.{{ $a }}.ac_qty'>
                                                            </div>
                                                        </td>
                                                        <td class="">
                                                            <div class="-mt-2">
                                                                <input type="text"
                                                                    class="w-6 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                    name="air_cargos.{{ $a }}.ac_wt"
                                                                    wire:model.defer='air_cargos.{{ $a }}.ac_wt'>
                                                            </div>
                                                        </td>
                                                        <td class="">
                                                            <div class="-mt-2">
                                                                <div class="flex gap-1">
                                                                    <input type="text"
                                                                        class="w-8 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                        name="air_cargos.{{ $a }}.ac_dl"
                                                                        wire:model.defer='air_cargos.{{ $a }}.ac_dl'>
                                                                    <input type="text"
                                                                        class="w-8 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                        name="air_cargos.{{ $a }}.ac_dw"
                                                                        wire:model.defer='air_cargos.{{ $a }}.ac_dw'>
                                                                    <input type="text"
                                                                        class="w-8 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                        name="air_cargos.{{ $a }}.ac_dh"
                                                                        wire:model.defer='air_cargos.{{ $a }}.ac_dh'>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="px-1">
                                                            <div class="w-full -mt-2 whitespace-normal">
                                                                <select
                                                                    class="h-6 p-1 pl-2 text-xs border rounded-sm cursor-pointer"
                                                                    name="air_cargos.{{ $a }}.ac_um"
                                                                    wire:model='air_cargos.{{ $a }}.ac_um'>
                                                                    <option value="">Select
                                                                    </option>
                                                                    <option value="1">mm</option>
                                                                    <option value="2">cm</option>
                                                                    <option value="3">inches
                                                                    </option>
                                                                    <option value="4">m</option>
                                                                    <option value="5">ft</option>
                                                                    <option value="6">yard
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td class="">
                                                            <div class="w-full -mt-2 whitespace-normal">
                                                                <select
                                                                    class="h-6 p-1 pl-2 text-xs border rounded-sm cursor-pointer"
                                                                    name="air_cargos.{{ $a }}.ac_umt"
                                                                    wire:model='air_cargos.{{ $a }}.ac_umt'>
                                                                    <option value="">Select
                                                                    </option>
                                                                    <option value="1">Lot</option>
                                                                    <option value="2">Per piece
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td class="px-1">
                                                            <div class="w-full -mt-2 whitespace-normal">
                                                                <select
                                                                    class="h-6 p-1 pl-2 text-xs border rounded-sm cursor-pointer"
                                                                    name="air_cargos.{{ $a }}.ac_tp"
                                                                    wire:model='air_cargos.{{ $a }}.ac_tp'>
                                                                    <option value="">Select
                                                                    </option>
                                                                    <option value="1">Box</option>
                                                                    <option value="2">Plastic
                                                                    </option>
                                                                    <option value="3">Crate
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </td>

                                                        <td class="pl-1">
                                                            <div class="-mt-2 whitespace-normal">
                                                                <div class="-mt-2">
                                                                    @if ($air_cargos[$a]['ac_fc'] == true)
                                                                        <svg wire:click="isForCreating({{ $a }}, 1, false)"
                                                                            class="w-10 h-10 py-1 rounded-full text-green"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="fas" data-icon="user-slash"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 680 510">
                                                                            <path fill="currentColor"
                                                                                d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                                            </path>
                                                                        </svg>
                                                                    @else
                                                                        <svg wire:click="isForCreating({{ $a }}, 1, true)"
                                                                            class="w-10 h-10 py-1 text-gray-300 rounded-full"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="fas" data-icon="user-slash"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 680 510">
                                                                            <path fill="currentColor"
                                                                                d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                                            </path>
                                                                        </svg>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="">
                                                            <div class="-mt-2" wire:init="cratingtypeReferences">
                                                                <select
                                                                    class="w-[28px] h-6 p-1 pl-2 text-xs border rounded-sm cursor-pointer"
                                                                    name="air_cargos.{{ $a }}.ac_ct"
                                                                    wire:model='air_cargos.{{ $a }}.ac_ct'
                                                                    @if ($air_cargos[$a]['ac_fc'] == false) disabled @endif>
                                                                    <option value="">Select
                                                                    </option>
                                                                    @foreach ($crating_type_references as $crating_type_ref)
                                                                        <option value="{{ $crating_type_ref->id }}">
                                                                            {{ $crating_type_ref->name }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </td>

                                                        <td class="pl-1">
                                                            <div class="-mt-2">
                                                                <input type="text"
                                                                    class="w-10 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                    name="air_cargos.{{ $a }}.ac_cwt"
                                                                    wire:model.defer='air_cargos.{{ $a }}.ac_cwt'
                                                                    disabled>
                                                            </div>
                                                        </td>
                                                        <td class="">
                                                            <div class="flex -mt-2">
                                                                @if (count($air_cargos) > 1)
                                                                    <button type="button" title="Remove"
                                                                        wire:click="removeair_cargos({'a': {{ $a }}})">
                                                                        <svg class="w-6 h-5 mt-1 mr-2 text-red"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="far" data-icon="print-alt"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 448 512">
                                                                            <path fill="currentColor"
                                                                                d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z" />
                                                                        </svg>
                                                                    </button>
                                                                @endif
                                                                @if (count($air_cargos) == 1)
                                                                    <button type="button" title="Adds"
                                                                        wire:click="addair_cargos({{ $a + 1 }})">
                                                                        <svg class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="far" data-icon="print-alt"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 448 512">
                                                                            <path fill="currentColor"
                                                                                d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                        </svg>
                                                                    </button>
                                                                @endif

                                                                @if ($i != 0)
                                                                    @if (count($air_cargos) == $i + 1)
                                                                        <button type="button" title="Add"
                                                                            wire:click="addair_cargos({{ $a + 1 }})">
                                                                            <svg class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div>
                            <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                <div class="mt-[-8px]">
                                    <input type="checkbox"
                                        class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                        name="air_pouch" wire:model='air_pouch'>
                                </div>
                                <div class="-mt-2 text-lg text-black">Air Pouch</div>
                            </div>
                            @if ($air_pouch)
                                <div class="flex px-2.5 mt-2">
                                    <div class="overflow-auto border-l-4 border-[#003399]">
                                        <table class="text-sm text-[#003399] ml-4">
                                            <thead>
                                                <tr class="text-left">
                                                    <th class="w-12 px-3">
                                                        <x-label value="Qty" class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="w-24 px-3">
                                                        <x-label value="Pouch Size"
                                                            class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="w-24 px-3">
                                                        <x-label value="Amount" class="text-[10px] leading-tight" />
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 0; ?>
                                                @foreach ($air_pouches as $a => $ap)
                                                    <tr class="mt-4">
                                                        <td class="px-2">
                                                            <div class="mb-2">
                                                                <input type="text"
                                                                    class="w-10 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                    name="air_pouches.{{ $a }}.ap_qty"
                                                                    wire:model.defer='air_pouches.{{ $a }}.ap_qty'>
                                                            </div>
                                                        </td>
                                                        <td class="px-2">
                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                <select
                                                                    class="w-24 h-6 p-0 px-2 text-xs border rounded-sm cursor-pointer"
                                                                    name="air_pouches.{{ $a }}.ap_ps"
                                                                    wire:model='air_pouches.{{ $a }}.ap_ps'
                                                                    {{-- wire:change="forAirPouchSize" --}}>
                                                                    <option value="">Select
                                                                    </option>
                                                                    <option value="1">Small
                                                                    </option>
                                                                    <option value="2">Medium
                                                                    </option>
                                                                    <option value="3">Large
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td class="px-2">
                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                <input type="text"
                                                                    class="w-24 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                    name="air_pouches.{{ $a }}.ap_amount"
                                                                    wire:model.defer='air_pouches.{{ $a }}.ap_amount'>
                                                            </div>
                                                        </td>
                                                        <td class="">
                                                            <div class="flex -mt-2">
                                                                @if (count($air_pouches) > 1)
                                                                    <button type="button" title="Remove">
                                                                        <svg wire:click="removeair_pouches({'a': {{ $a }}})"
                                                                            class="w-6 h-5 mt-1 mr-2 text-red"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="far" data-icon="print-alt"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 448 512">
                                                                            <path fill="currentColor"
                                                                                d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z" />
                                                                        </svg>
                                                                    </button>
                                                                @endif
                                                                @if (count($air_pouches) == 1)
                                                                    <button type="button" title="">
                                                                        <svg wire:click="addair_pouches({{ $a + 1 }})"
                                                                            class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="far" data-icon="print-alt"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 448 512">
                                                                            <path fill="currentColor"
                                                                                d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                        </svg>
                                                                    </button>
                                                                @endif
                                                                @if ($i != 0)
                                                                    @if (count($air_pouches) == $i + 1)
                                                                        <button type="button" title="">
                                                                            <svg wire:click="addair_pouches({{ $a + 1 }})"
                                                                                class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div>
                            <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                <div class="mt-[-8px]">
                                    <input type="checkbox"
                                        class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                        name="air_box" wire:model='air_box'>
                                </div>
                                <div class="-mt-2 text-lg text-black">Air Box</div>
                            </div>
                            @if ($air_box)
                                <div class="flex px-2.5 mt-2">
                                    <div class="overflow-auto border-l-4 border-[#003399]">
                                        <table class="text-sm text-[#003399] ml-4">
                                            <thead>
                                                <tr class="text-left">
                                                    <th class="w-12 px-3">
                                                        <x-label value="Qty" class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="w-24 px-3">
                                                        <x-label value="Box Size" class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="w-24 px-3">
                                                        <x-label value="Weight (KG)"
                                                            class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="w-24 px-3">
                                                        <x-label value="Amount" class="text-[10px] leading-tight" />
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 0; ?>
                                                @foreach ($air_boxes as $a => $ab)
                                                    <tr class="mt-4">
                                                        <td class="px-2">
                                                            <div class="mb-2">
                                                                <input type="text"
                                                                    class="w-10 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                    name="air_boxes.{{ $a }}.ab_qty"
                                                                    wire:model.defer='air_boxes.{{ $a }}.ab_qty'>
                                                            </div>
                                                        </td>
                                                        <td class="px-2">
                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                <select
                                                                    class="w-24 h-6 p-0 px-2 text-xs border rounded-sm cursor-pointer"
                                                                    name="air_boxes.{{ $a }}.ab_bs"
                                                                    wire:model='air_boxes.{{ $a }}.ab_bs'
                                                                    {{-- wire:change="forBoxWeight({{ $transport_mode }})" --}}>
                                                                    <option value="">Select
                                                                    </option>
                                                                    <option value="1">Small
                                                                    </option>
                                                                    <option value="2">Medium
                                                                    </option>
                                                                    <option value="2">Large
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td class="px-2">
                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                <input type="text"
                                                                    class="w-24 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                    name="air_boxes.{{ $a }}.ab_weight"
                                                                    wire:model.defer='air_boxes.{{ $a }}.ab_weight'
                                                                    {{-- wire:change="forBoxWeight({{ $transport_mode }})" --}}>
                                                            </div>
                                                        </td>
                                                        <td class="px-2">
                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                <input type="text"
                                                                    class="w-24 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                    name="air_boxes.{{ $a }}.ab_amount"
                                                                    wire:model.defer='air_boxes.{{ $a }}.ab_amount'
                                                                    {{-- disabled --}}>
                                                            </div>
                                                        </td>
                                                        <td class="">
                                                            <div class="flex -mt-2">
                                                                @if (count($air_boxes) > 1)
                                                                    <button type="button" title="Remove">
                                                                        <svg wire:click="removeair_boxes({'a': {{ $a }}})"
                                                                            class="w-6 h-5 mt-1 mr-2 text-red"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="far" data-icon="print-alt"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 448 512">
                                                                            <path fill="currentColor"
                                                                                d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z" />
                                                                        </svg>
                                                                    </button>
                                                                @endif
                                                                @if (count($air_boxes) == 1)
                                                                    <button type="button" title="">
                                                                        <svg wire:click="addair_boxes({{ $a + 1 }})"
                                                                            class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="far" data-icon="print-alt"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 448 512">
                                                                            <path fill="currentColor"
                                                                                d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                        </svg>
                                                                    </button>
                                                                @endif
                                                                @if ($i != 0)
                                                                    @if (count($air_boxes) == $i + 1)
                                                                        <button type="button" title="">
                                                                            <svg wire:click="addair_boxes({{ $a + 1 }})"
                                                                                class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif

                @if ($transport_mode == 2)
                    <div class="my-6">
                        <div>
                            <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                <div class="mt-[-8px]">
                                    <input type="checkbox"
                                        class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                        name="sea_cargo" wire:model='sea_cargo'
                                        @if ($sea_cargo) checked @endif>
                                </div>
                                <div class="-mt-2 text-lg">Sea Cargo</div>
                            </div>
                            @if ($sea_cargo)
                                <div class="flex pl-2.5 mt-2">
                                    <div class="overflow-hidden border-l-4 border-[#003399]">
                                        <div wire:init="movementSfTypeReferences"
                                            class="flex gap-6 mt-1 mb-4 ml-6 text-xs font-medium text-gray-500">
                                            @foreach ($movement_sf_type_references as $i => $sf_type)
                                                <div class="flex gap-1">
                                                    <input type="radio" class="border-[#003399] border"
                                                        name="movement_sf_type" wire:model='movement_sf_type'
                                                        value="{{ $movement_sf_type_references[$i]['id'] }}">
                                                    {{ $movement_sf_type_references[$i]['name'] }}
                                                </div>
                                            @endforeach
                                        </div>
                                        @if ($movement_sf_type == 1)
                                            <table class="text-md text-[#003399] ml-6">
                                                <thead>
                                                    <tr class="text-left">
                                                        <th class="w-10 px-1">
                                                            <x-label value="Qty"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-10 px-1">
                                                            <x-label value="Wt"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-[20px] px-1 whitespace-nowrap">
                                                            <x-label value="Dimension (LxWxH)"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-[20px] px-1">
                                                            <x-label value="Unit of Measurement"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-[20px] px-1">
                                                            <x-label value="Measurement Type"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-[20px] px-1">
                                                            <x-label value="Type of Packaging"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-10 px-1">
                                                            <x-label value="For Crating"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-10 px-1">
                                                            <x-label value="Crating Type"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-12 px-1">
                                                            <x-label value="CBM"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="px-1"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $a = 0; ?>
                                                    @foreach ($sea_cargos_is_lcl as $i => $sc)
                                                        <tr class="">
                                                            <td class="">
                                                                <div class="-mt-2">
                                                                    <input type="text"
                                                                        class="w-6 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                        name="sea_cargos_is_lcl.{{ $i }}.qty"
                                                                        wire:model.defer='sea_cargos_is_lcl.{{ $i }}.qty'
                                                                        {{-- wire:change="forCWT_CBM({{ $transport_mode }})" --}}>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="-mt-2">
                                                                    <input type="text"
                                                                        class="w-6 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                        name="sea_cargos_is_lcl.{{ $i }}.wt"
                                                                        wire:model.defer='sea_cargos_is_lcl.{{ $i }}.wt'
                                                                        {{-- wire:change="forCWT_CBM({{ $transport_mode }})" --}}>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="-mt-2">
                                                                    <div class="flex gap-1">
                                                                        <input type="text"
                                                                            class="w-8 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                            name="sea_cargos_is_lcl.{{ $i }}.dimension_l"
                                                                            wire:model.defer='sea_cargos_is_lcl.{{ $i }}.dimension_l'
                                                                            {{-- wire:change="forCWT_CBM({{ $transport_mode }})" --}}>
                                                                        <input type="text"
                                                                            class="w-8 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                            name="sea_cargos_is_lcl.{{ $i }}.dimension_w"
                                                                            wire:model.defer='sea_cargos_is_lcl.{{ $i }}.dimension_w'
                                                                            {{-- wire:change="forCWT_CBM({{ $transport_mode }})" --}}>
                                                                        <input type="text"
                                                                            class="w-8 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                            name="sea_cargos_is_lcl.{{ $i }}.dimension_h"
                                                                            wire:model.defer='sea_cargos_is_lcl.{{ $i }}.dimension_h'
                                                                            {{-- wire:change="forCWT_CBM({{ $transport_mode }})" --}}>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="w-24 -mt-2 whitespace-normal">
                                                                    <select
                                                                        class="w-24 h-6 p-0 px-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="sea_cargos_is_lcl.{{ $i }}.unit_of_measurement"
                                                                        wire:model='sea_cargos_is_lcl.{{ $i }}.unit_of_measurement'>
                                                                        <option value="">Select
                                                                        </option>
                                                                        <option value="1">mm
                                                                        </option>
                                                                        <option value="2">cm
                                                                        </option>
                                                                        <option value="3">inches
                                                                        </option>
                                                                        <option value="4">m
                                                                        </option>
                                                                        <option value="5">ft
                                                                        </option>
                                                                        <option value="6">yard
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="w-24 -mt-2 whitespace-normal">
                                                                    <select
                                                                        class="w-24 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                        name="sea_cargos_is_lcl.{{ $i }}.measurement_type"
                                                                        wire:model='sea_cargos_is_lcl.{{ $i }}.measurement_type'>
                                                                        <option value="">Select
                                                                        </option>
                                                                        <option value="1">Lot
                                                                        </option>
                                                                        <option value="2">Per
                                                                            piece
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="w-24 -mt-2 whitespace-normal">
                                                                    <select
                                                                        class="w-24 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                        name="sea_cargos_is_lcl.{{ $i }}.type_of_packaging"
                                                                        wire:model='sea_cargos_is_lcl.{{ $i }}.type_of_packaging'>
                                                                        <option value="">Select
                                                                        </option>
                                                                        <option value="1">Box
                                                                        </option>
                                                                        <option value="2">Plastic
                                                                        </option>
                                                                        <option value="3">Crate
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="-mt-2 whitespace-normal">
                                                                    <div class="pl-1 -mt-2">
                                                                        @if ($sea_cargos_is_lcl[$i]['for_crating'] == true)
                                                                            <svg wire:click="isForCreating({{ $i }}, 2, false)"
                                                                                class="py-1 rounded-full text-green"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="fas"
                                                                                data-icon="user-slash" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 720 510">
                                                                                <path fill="currentColor"
                                                                                    d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                                                </path>
                                                                            </svg>
                                                                        @else
                                                                            <svg wire:click="isForCreating({{ $i }}, 2, true)"
                                                                                class="py-1 text-gray-400 rounded-full"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="fas"
                                                                                data-icon="user-slash" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 720 510">
                                                                                <path fill="currentColor"
                                                                                    d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                                                </path>
                                                                            </svg>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="-mt-2">
                                                                    <select
                                                                        class="h-6 p-1 text-xs border rounded-sm cursor-pointer w-28"
                                                                        name="sea_cargos_is_lcl.{{ $i }}.crating_type"
                                                                        wire:model='sea_cargos_is_lcl.{{ $i }}.crating_type'
                                                                        @if ($sea_cargos_is_lcl[$i]['for_crating'] == false) disabled @endif>
                                                                        <option value="">Select
                                                                        </option>
                                                                        @foreach ($crating_type_references as $crating_type_ref)
                                                                            <option
                                                                                value="{{ $crating_type_ref->id }}">
                                                                                {{ $crating_type_ref->name }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="-mt-2">
                                                                    <input type="text"
                                                                        class="w-6 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                        name="sea_cargos_is_lcl.{{ $i }}.cbm"
                                                                        wire:model.defer='sea_cargos_is_lcl.{{ $i }}.cbm'
                                                                        {{-- disabled --}}>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="flex -mt-2">
                                                                    @if (count($sea_cargos_is_lcl) > 1)
                                                                        <button type="button" title="Remove">
                                                                            <svg wire:click="removeSeaCargos({'i': {{ $i }}})"
                                                                                class="w-6 h-5 mt-1 mr-2 text-red"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                    @if (count($sea_cargos_is_lcl) == 1)
                                                                        <button type="button" title="">
                                                                            <svg wire:click="addSeaCargosLcl({{ $i + 1 }})"
                                                                                class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                    @if ($a != 0)
                                                                        @if (count($sea_cargos_is_lcl) == $a + 1)
                                                                            <button type="button" title="">
                                                                                <svg wire:click="addSeaCargosLcl({{ $i + 1 }})"
                                                                                    class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                    aria-hidden="true"
                                                                                    focusable="false"
                                                                                    data-prefix="far"
                                                                                    data-icon="print-alt"
                                                                                    role="img"
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    viewBox="0 0 448 512">
                                                                                    <path fill="currentColor"
                                                                                        d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                                </svg>
                                                                            </button>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php $a++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @elseif($movement_sf_type == 2)
                                            <table class="text-md text-[#003399] ml-6 mb-4">
                                                <thead>
                                                    <tr class="text-left">
                                                        <th class="w-12 px-2">
                                                            <x-label value="Qty"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-12 px-2">
                                                            <x-label value="Container"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="px-2"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $a = 0; ?>
                                                    @foreach ($sea_cargos_is_fcl as $i => $sc_fcl)
                                                        <tr class="">
                                                            <td class="px-1">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-10 h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="sea_cargos_is_fcl.{{ $i }}.qty"
                                                                        wire:model.defer='sea_cargos_is_fcl.{{ $i }}.qty'>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="w-24 mb-2 whitespace-normal">
                                                                    <select
                                                                        class="w-24 h-6 p-0 px-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="sea_cargos_is_fcl.{{ $i }}.container"
                                                                        wire:model='sea_cargos_is_fcl.{{ $i }}.container'>
                                                                        <option value="">Select
                                                                        </option>
                                                                        <option value="10">10
                                                                        </option>
                                                                        <option value="20">20
                                                                        </option>
                                                                        <option value="40">40
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="flex -mt-2">
                                                                    @if (count($sea_cargos_is_fcl) > 1)
                                                                        <button type="button" title="Remove">
                                                                            <svg wire:click="removeSeaCargosFcl({'i': {{ $i }}})"
                                                                                class="w-6 h-5 mt-1 mr-2 text-red"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                    @if (count($sea_cargos_is_fcl) == 1)
                                                                        <button type="button" title="">
                                                                            <svg wire:click="addSeaCargosFcl({{ $i + 1 }})"
                                                                                class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                    @if ($a != 0)
                                                                        @if (count($sea_cargos_is_fcl) == $a + 1)
                                                                            <button type="button" title="">
                                                                                <svg wire:click="addSeaCargosFcl({{ $i + 1 }})"
                                                                                    class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                    aria-hidden="true"
                                                                                    focusable="false"
                                                                                    data-prefix="far"
                                                                                    data-icon="print-alt"
                                                                                    role="img"
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    viewBox="0 0 448 512">
                                                                                    <path fill="currentColor"
                                                                                        d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                                </svg>
                                                                            </button>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php $a++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @elseif ($movement_sf_type == 3)
                                            <table class="text-md text-[#003399] ml-6">
                                                <thead>
                                                    <tr class="text-left">
                                                        <th class="w-10 px-1">
                                                            <x-label value="Qty"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-10 px-1">
                                                            <x-label value="Wt"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-[20px] px-1 whitespace-nowrap">
                                                            <x-label value="Dimension (LxWxH)"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-[20px] px-1">
                                                            <x-label value="Unit of Measurement"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-[20px] px-1">
                                                            <x-label value="Measurement Type"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-[20px] px-1">
                                                            <x-label value="Amount"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-10 px-1">
                                                            <x-label value="For Crating"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-10 px-1">
                                                            <x-label value="Crating Type"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-10 px-1">
                                                            <x-label value="CBM"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="px-1"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $a = 0; ?>
                                                    @foreach ($sea_cargos_is_rcl as $i => $sc)
                                                        <tr class="">
                                                            <td class="">
                                                                <div class="-mt-2">
                                                                    <input type="text"
                                                                        class="w-6 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                        name="sea_cargos_is_rcl.{{ $i }}.qty"
                                                                        wire:model.defer='sea_cargos_is_rcl.{{ $i }}.qty'
                                                                        {{-- wire:change="forCWT_CBM({{ $transport_mode }})" --}}>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="-mt-2">
                                                                    <input type="text"
                                                                        class="w-6 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                        name="sea_cargos_is_rcl.{{ $i }}.wt"
                                                                        wire:model.defer='sea_cargos_is_rcl.{{ $i }}.wt'
                                                                        {{-- wire:change="forCWT_CBM({{ $transport_mode }})" --}}>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="-mt-2">
                                                                    <div class="flex gap-1">
                                                                        <input type="text"
                                                                            class="w-8 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                            name="sea_cargos_is_rcl.{{ $i }}.dimension_l"
                                                                            wire:model.defer='sea_cargos_is_rcl.{{ $i }}.dimension_l'
                                                                            {{-- wire:change="forCWT_CBM({{ $transport_mode }})" --}}>
                                                                        <input type="text"
                                                                            class="w-8 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                            name="sea_cargos_is_rcl.{{ $i }}.dimension_w"
                                                                            wire:model.defer='sea_cargos_is_rcl.{{ $i }}.dimension_w'
                                                                            {{-- wire:change="forCWT_CBM({{ $transport_mode }})" --}}>
                                                                        <input type="text"
                                                                            class="w-8 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                            name="sea_cargos_is_rcl.{{ $i }}.dimension_h"
                                                                            wire:model.defer='sea_cargos_is_rcl.{{ $i }}.dimension_h'
                                                                            {{-- wire:change="forCWT_CBM({{ $transport_mode }})" --}}>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="-mt-2 whitespace-normal">
                                                                    <select
                                                                        class="h-6 p-0 px-1 text-xs border rounded-sm cursor-pointer"
                                                                        name="sea_cargos_is_rcl.{{ $i }}.unit_of_measurement"
                                                                        wire:model='sea_cargos_is_rcl.{{ $i }}.unit_of_measurement'>
                                                                        <option value="">
                                                                            Select
                                                                        </option>
                                                                        <option value="1">mm
                                                                        </option>
                                                                        <option value="2">cm
                                                                        </option>
                                                                        <option value="3">
                                                                            inches
                                                                        </option>
                                                                        <option value="4">m
                                                                        </option>
                                                                        <option value="5">ft
                                                                        </option>
                                                                        <option value="6">yard
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="-mt-2 whitespace-normal ">
                                                                    <select
                                                                        class="h-6 p-0 px-2 text-xs border rounded-sm cursor-pointer "
                                                                        name="sea_cargos_is_rcl.{{ $i }}.measurement_type"
                                                                        wire:model='sea_cargos_is_rcl.{{ $i }}.measurement_type'>
                                                                        <option value="">
                                                                            Select
                                                                        </option>
                                                                        <option value="1">Lot
                                                                        </option>
                                                                        <option value="2">Per
                                                                            piece
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="-mt-2 whitespace-normal ">
                                                                    <input type="text"
                                                                        class="w-16 h-6 p-2 text-xs border rounded-sm cursor-pointer "
                                                                        name="sea_cargos_is_rcl.{{ $i }}.amount"
                                                                        wire:model.defer='sea_cargos_is_rcl.{{ $i }}.amount'
                                                                        {{-- wire:change="forCWT_CBM({{ $transport_mode }})" --}}>
                                                                </div>
                                                            </td>
                                                            <td class="pl-2">
                                                                <div class="-mt-2 whitespace-normal">
                                                                    <div class="-mt-2">
                                                                        @if ($sea_cargos_is_rcl[$i]['for_crating'] == true)
                                                                            <svg wire:click="isForCreating({{ $i }}, 2, false)"
                                                                                class="py-1 rounded-full text-green"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="fas"
                                                                                data-icon="user-slash"
                                                                                role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 720 510">
                                                                                <path fill="currentColor"
                                                                                    d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                                                </path>
                                                                            </svg>
                                                                        @else
                                                                            <svg wire:click="isForCreating({{ $i }}, 2, true)"
                                                                                class="py-1 text-gray-400 rounded-full"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="fas"
                                                                                data-icon="user-slash"
                                                                                role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 720 510">
                                                                                <path fill="currentColor"
                                                                                    d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                                                </path>
                                                                            </svg>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="-mt-2">
                                                                    <select
                                                                        class="h-6 p-1 text-xs border rounded-sm cursor-pointer w-28"
                                                                        name="sea_cargos_is_rcl.{{ $i }}.crating_type"
                                                                        wire:model='sea_cargos_is_rcl.{{ $i }}.crating_type'>
                                                                        @if ($sea_cargos_is_rcl[$i]['for_crating'] == false)
                                                                            disabled
                                                                        @endif>
                                                                        <option value="">
                                                                            Select
                                                                        </option>
                                                                        @foreach ($crating_type_references as $crating_type_ref)
                                                                            <option
                                                                                value="{{ $crating_type_ref->id }}">
                                                                                {{ $crating_type_ref->name }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="-mt-2">
                                                                    <input type="text"
                                                                        class="w-6 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                        name="sea_cargos_is_rcl.{{ $i }}.cbm"
                                                                        wire:model.defer='sea_cargos_is_rcl.{{ $i }}.cbm'
                                                                        disabled>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="flex -mt-2">
                                                                    @if (count($sea_cargos_is_rcl) > 1)
                                                                        <button type="button" title="Remove">
                                                                            <svg wire:click="removeSeaCargosRcl({'i': {{ $i }}})"
                                                                                class="w-6 h-5 mt-1 mr-2 text-red"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                    @if (count($sea_cargos_is_rcl) == 1)
                                                                        <button type="button" title="">
                                                                            <svg wire:click="addSeaCargosRcl({{ $i + 1 }})"
                                                                                class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                    @if ($a != 0)
                                                                        @if (count($sea_cargos_is_rcl) == $a + 1)
                                                                            <button type="button" title="">
                                                                                <svg wire:click="addSeaCargosRcl({{ $i + 1 }})"
                                                                                    class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                    aria-hidden="true"
                                                                                    focusable="false"
                                                                                    data-prefix="far"
                                                                                    data-icon="print-alt"
                                                                                    role="img"
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    viewBox="0 0 448 512">
                                                                                    <path fill="currentColor"
                                                                                        d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                                </svg>
                                                                            </button>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php $a++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div>
                            <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                <div class="mt-[-8px]">
                                    <input type="checkbox"
                                        class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                        name="sea_box" wire:model='sea_box'
                                        @if ($sea_box) checked @endif>
                                </div>
                                <div class="-mt-2 text-lg">Sea Box</div>
                            </div>
                            @if ($sea_box)
                                <div class="flex px-2.5 mt-2">
                                    <div class="overflow-auto border-l-4 border-[#003399]">
                                        <table class="text-md text-[#003399] ml-4">
                                            <thead>
                                                <tr class="text-left">
                                                    <th class="w-12 px-3">
                                                        <x-label value="Qty" class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="w-24 px-3">
                                                        <x-label value="Box Size"
                                                            class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="w-24 px-3">
                                                        <x-label value="Weight (KG)"
                                                            class="text-[10px] leading-tight" />
                                                    </th>
                                                    <th class="w-24 px-3">
                                                        <x-label value="Amount" class="text-[10px] leading-tight" />
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $a = 0; ?>
                                                @foreach ($sea_boxes as $i => $ab)
                                                    <tr class="mt-4">
                                                        <td class="px-2">
                                                            <div class="mb-2">
                                                                <input type="text"
                                                                    class="w-10 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                    name="sea_boxes.{{ $i }}.qty"
                                                                    wire:model.defer='sea_boxes.{{ $i }}.qty'>
                                                            </div>
                                                        </td>
                                                        <td class="px-2">
                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                <select
                                                                    class="w-24 h-6 p-0 px-2 text-xs border rounded-sm cursor-pointer"
                                                                    name="sea_boxes.{{ $i }}.box_size"
                                                                    wire:model='sea_boxes.{{ $i }}.box_size'>
                                                                    <option value="">Select
                                                                    </option>
                                                                    <option value="1">Small
                                                                    </option>
                                                                    <option value="2">Medium
                                                                    </option>
                                                                    <option value="2">Large
                                                                    </option>
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td class="px-2">
                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                <input type="text"
                                                                    class="w-24 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                    name="sea_boxes.{{ $i }}.weight"
                                                                    wire:model.defer='sea_boxes.{{ $i }}.weight'
                                                                    {{-- wire:change="forBoxWeight({{ $transport_mode }})" --}}>
                                                            </div>
                                                        </td>
                                                        <td class="px-2">
                                                            <div class="w-24 mb-2 whitespace-normal">
                                                                <input type="text"
                                                                    class="w-24 h-6 p-1 text-xs border rounded-sm cursor-pointer"
                                                                    name="sea_boxes.{{ $i }}.amount"
                                                                    wire:model.defer='sea_boxes.{{ $i }}.amount'
                                                                    disabled>
                                                            </div>
                                                        </td>
                                                        <td class="">
                                                            <div class="flex -mt-2">
                                                                @if (count($sea_boxes) > 1)
                                                                    <button type="button" title="Remove">
                                                                        <svg wire:click="removeSeaBoxes({'i': {{ $i }}})"
                                                                            class="w-6 h-5 mt-1 mr-2 text-red"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="far" data-icon="print-alt"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 448 512">
                                                                            <path fill="currentColor"
                                                                                d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z" />
                                                                        </svg>
                                                                    </button>
                                                                @endif
                                                                @if (count($sea_boxes) == 1)
                                                                    <button type="button" title="">
                                                                        <svg wire:click="addSeaBoxes({{ $i + 1 }})"
                                                                            class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="far" data-icon="print-alt"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 448 512">
                                                                            <path fill="currentColor"
                                                                                d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                        </svg>
                                                                    </button>
                                                                @endif
                                                                @if ($a != 0)
                                                                    @if (count($sea_boxes) == $a + 1)
                                                                        <button type="button" title="">
                                                                            <svg wire:click="addSeaBoxes({{ $i + 1 }})"
                                                                                class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $a++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif

                @if ($transport_mode == 3)
                    <div class="my-6">
                        <div>
                            <div class="flex gap-2 mt-4 font-medium text-gray-500">
                                <div class="mt-[-8px]">
                                    <input type="checkbox"
                                        class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                        name="land" wire:model='land'
                                        @if ($land) checked @endif>
                                </div>
                                <div class="-mt-2 text-lg">Land</div>
                            </div>
                            @if ($land)
                                <div class="flex px-2.5 mt-2">
                                    <div class="overflow-hidden border-l-4 border-[#003399]">
                                        <div>
                                            <div class="flex gap-6 mt-1 mb-4 ml-6 text-xs font-medium text-gray-500">
                                                <div class="flex gap-1">
                                                    <input type="radio" class="border-[#003399] border"
                                                        name="land_packaging_type" wire:model='land_packaging_type'
                                                        value="1">
                                                    <span class="">Shipper's Box</span>
                                                </div>
                                                <div class="flex gap-1">
                                                    <input type="radio" class="border-[#003399] border"
                                                        name="land_packaging_type" wire:model='land_packaging_type'
                                                        value="2">
                                                    <span class="">Land Pouch</span>
                                                </div>
                                                <div class="flex gap-1">
                                                    <input type="radio" class="border-[#003399] border"
                                                        name="land_packaging_type" wire:model='land_packaging_type'
                                                        value="3">
                                                    <span class="">Land Box</span>
                                                </div>
                                                <div class="flex gap-1">
                                                    <input type="radio" class="border-[#003399] border"
                                                        name="land_packaging_type" wire:model='land_packaging_type'
                                                        value="4">
                                                    <span class="">FTL</span>
                                                </div>
                                            </div>
                                            @if ($land_packaging_type == 2 || $land_packaging_type == 3)
                                                <div
                                                    class="flex gap-6 mt-1 mb-4 ml-6 text-xs font-medium text-gray-500">
                                                    @foreach ($booking_type_references as $i => $sf_type)
                                                        <div class="flex gap-1">
                                                            <input type="radio" class="border-[#003399] border"
                                                                name="booking_type" wire:model='booking_type'
                                                                value="{{ $booking_type_references[$i]['id'] }}">
                                                            <span class="uppercase">
                                                                {{ $booking_type_references[$i]['name'] }}
                                                            </span>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endif
                                        </div>
                                        @if ($land_packaging_type == 1)
                                            <table class="text-md text-[#003399] ml-4">
                                                <thead>
                                                    <tr class="text-left">
                                                        <th class="w-24 px-2 whitespace-nowrap">
                                                            <x-label value="Total Quantity"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-24 px-2">
                                                            <x-label value="Total"
                                                                class="text-[10px] leading-tight" />
                                                            <x-label value="Weight (KG)"
                                                                class="-mt-1 text-[10px] leading-tight whitespace-nowrap" />
                                                        </th>
                                                        <th class="w-40 px-1 whitespace-nowrap">
                                                            <x-label value="Dimension (LxWxH)"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-20 px-1">
                                                            <x-label value="CBM"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-20 px-1">
                                                            <x-label value="CWT"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-24 px-1">
                                                            <x-label value="Land Rate"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-24 px-1">
                                                            <x-label value="Rate Amount"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="px-1"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $a = 0; ?>
                                                    @foreach ($lands_shippers_box as $i => $ac)
                                                        <tr class="">
                                                            <td class="px-1">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-full h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_shippers_box.{{ $i }}.total_qty"
                                                                        wire:model.defer='lands_shippers_box.{{ $i }}.total_qty'>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-full h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_shippers_box.{{ $i }}.total_weight"
                                                                        wire:model.defer='lands_shippers_box.{{ $i }}.total_weight'>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="mb-2">
                                                                    <div class="flex gap-2">
                                                                        <input type="text"
                                                                            class="w-8 h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                            name="lands_shippers_box.{{ $i }}.dimension_l"
                                                                            wire:model.defer='lands_shippers_box.{{ $i }}.dimension_l'>
                                                                        <input type="text"
                                                                            class="w-8 h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                            name="lands_shippers_box.{{ $i }}.dimension_w"
                                                                            wire:model.defer='lands_shippers_box.{{ $i }}.dimension_w'>
                                                                        <input type="text"
                                                                            class="w-8 h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                            name="lands_shippers_box.{{ $i }}.dimension_h"
                                                                            wire:model.defer='lands_shippers_box.{{ $i }}.dimension_h'>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-12 h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_shippers_box.{{ $i }}.cbm"
                                                                        wire:model.defer='lands_shippers_box.{{ $i }}.cbm'
                                                                        disabled>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-12 h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_shippers_box.{{ $i }}.cwt"
                                                                        wire:model.defer='lands_shippers_box.{{ $i }}.cwt'
                                                                        disabled>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="w-24 mb-2 whitespace-normal">
                                                                    <select
                                                                        class="w-24 h-6 p-0 px-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_shippers_box.{{ $i }}.land_rate"
                                                                        wire:model='lands_shippers_box.{{ $i }}.land_rate'>
                                                                        <option value="">Select
                                                                        </option>
                                                                        <option value="1">Lot
                                                                        </option>
                                                                        <option value="2">Per
                                                                            piece
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-24 h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_shippers_box.{{ $i }}.rate_amount"
                                                                        wire:model.defer='lands_shippers_box.{{ $i }}.rate_amount'
                                                                        disabled>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="flex -mt-2">
                                                                    @if (count($lands_shippers_box) > 1)
                                                                        <button type="button" title="Remove">
                                                                            <svg wire:click="removeLandsShippersBox({'i': {{ $i }}})"
                                                                                class="w-6 h-5 mt-1 mr-2 text-red"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                    @if (count($lands_shippers_box) == 1)
                                                                        <button type="button" title="">
                                                                            <svg wire:click="addLandsShippersBox({{ $i + 1 }})"
                                                                                class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                    @if ($a != 0)
                                                                        @if (count($lands_shippers_box) == $a + 1)
                                                                            <button type="button" title="">
                                                                                <svg wire:click="addLandsShippersBox({{ $i + 1 }})"
                                                                                    class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                    aria-hidden="true"
                                                                                    focusable="false"
                                                                                    data-prefix="far"
                                                                                    data-icon="print-alt"
                                                                                    role="img"
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    viewBox="0 0 448 512">
                                                                                    <path fill="currentColor"
                                                                                        d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                                </svg>
                                                                            </button>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php $a++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @elseif($land_packaging_type == 2)
                                            <table class="text-md text-[#003399] ml-4">
                                                <thead>
                                                    <tr class="text-left">
                                                        <th class="w-24 px-1">
                                                            <x-label value="Size"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-24 px-2 whitespace-nowrap">
                                                            <x-label value="Weight Per"
                                                                class="text-[10px] leading-tight" />
                                                            <x-label value="Pouch (KG)"
                                                                class="-mt-1 text-[10px] leading-tight whitespace-nowrap" />
                                                        </th>
                                                        <th class="w-20 px-1">
                                                            <x-label value="CBM"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-24 px-1">
                                                            <x-label value="Total Amount"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="px-1"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $a = 0; ?>
                                                    @foreach ($lands_pouches as $i => $lc)
                                                        <tr class="">
                                                            <td class="px-1">
                                                                <div class="w-24 mb-2 whitespace-normal">
                                                                    <select
                                                                        class="w-24 h-6 p-0 px-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_pouches.{{ $i }}.size"
                                                                        wire:model='lands_pouches.{{ $i }}.size'>
                                                                        <option value="">Select
                                                                        </option>
                                                                        <option value="1">Small
                                                                        </option>
                                                                        <option value="2">Medium
                                                                        </option>
                                                                        <option value="3">Large
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-full h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_pouches.{{ $i }}.weight_per_pouch"
                                                                        wire:model.defer='lands_pouches.{{ $i }}.weight_per_pouch'>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-full h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_pouches.{{ $i }}.cbm"
                                                                        wire:model.defer='lands_pouches.{{ $i }}.cbm'
                                                                        disabled>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-24 h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_pouches.{{ $i }}.total_amount"
                                                                        wire:model.defer='lands_pouches.{{ $i }}.total_amount'
                                                                        disabled>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="flex -mt-2">
                                                                    @if (count($lands_pouches) > 1)
                                                                        <button type="button" title="Remove">
                                                                            <svg wire:click="removeLandsPouches({'i': {{ $i }}})"
                                                                                class="w-6 h-5 mt-1 mr-2 text-red"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                    @if (count($lands_pouches) == 1)
                                                                        <button type="button" title="">
                                                                            <svg wire:click="addLandsPouches({{ $i + 1 }})"
                                                                                class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                    @if ($a != 0)
                                                                        @if (count($lands_pouches) == $a + 1)
                                                                            <button type="button" title="">
                                                                                <svg wire:click="addLandsPouches({{ $i + 1 }})"
                                                                                    class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                    aria-hidden="true"
                                                                                    focusable="false"
                                                                                    data-prefix="far"
                                                                                    data-icon="print-alt"
                                                                                    role="img"
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    viewBox="0 0 448 512">
                                                                                    <path fill="currentColor"
                                                                                        d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                                </svg>
                                                                            </button>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php $a++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @elseif($land_packaging_type == 3)
                                            <table class="text-md text-[#003399] ml-4">
                                                <thead>
                                                    <tr class="text-left">
                                                        <th class="w-24 px-1">
                                                            <x-label value="Size"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-24 px-2 whitespace-nowrap">
                                                            <x-label value="Weight Per"
                                                                class="text-[10px] leading-tight" />
                                                            <x-label value="Box (KG)"
                                                                class="-mt-1 text-[10px] leading-tight whitespace-nowrap" />
                                                        </th>
                                                        <th class="w-20 px-1">
                                                            <x-label value="CBM"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-24 px-1">
                                                            <x-label value="Total Amount"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="px-1"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $a = 0; ?>
                                                    @foreach ($lands_boxes as $i => $lb)
                                                        <tr class="">
                                                            <td class="px-1">
                                                                <div class="w-24 mb-2 whitespace-normal">
                                                                    <select
                                                                        class="w-24 h-6 p-0 px-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_boxes.{{ $i }}.size"
                                                                        wire:model='lands_boxes.{{ $i }}.size'>
                                                                        <option value="">Select
                                                                        </option>
                                                                        <option value="1">Small
                                                                        </option>
                                                                        <option value="2">Medium
                                                                        </option>
                                                                        <option value="3">Large
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-full h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_boxes.{{ $i }}.weight_per_box"
                                                                        wire:model.defer='lands_boxes.{{ $i }}.weight_per_box'>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-full h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_boxes.{{ $i }}.cbm"
                                                                        wire:model.defer='lands_boxes.{{ $i }}.cbm'
                                                                        disabled>
                                                                </div>
                                                            </td>
                                                            <td class="px-1">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-24 h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_boxes.{{ $i }}.total_amount"
                                                                        wire:model.defer='lands_boxes.{{ $i }}.total_amount'
                                                                        disabled>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="flex -mt-2">
                                                                    @if (count($lands_boxes) > 1)
                                                                        <button type="button" title="Remove">
                                                                            <svg wire:click="removeLandsBoxes({'i': {{ $i }}})"
                                                                                class="w-6 h-5 mt-1 mr-2 text-red"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                    @if (count($lands_boxes) == 1)
                                                                        <button type="button" title="">
                                                                            <svg wire:click="addLandsBoxes({{ $i + 1 }})"
                                                                                class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                    @if ($a != 0)
                                                                        @if (count($lands_boxes) == $a + 1)
                                                                            <button type="button" title="">
                                                                                <svg wire:click="addLandsBoxes({{ $i + 1 }})"
                                                                                    class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                    aria-hidden="true"
                                                                                    focusable="false"
                                                                                    data-prefix="far"
                                                                                    data-icon="print-alt"
                                                                                    role="img"
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    viewBox="0 0 448 512">
                                                                                    <path fill="currentColor"
                                                                                        d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                                </svg>
                                                                            </button>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php $a++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @elseif($land_packaging_type == 4)
                                            <table class="text-md text-[#003399] ml-4">
                                                <thead>
                                                    <tr class="text-left">
                                                        <th class="w-20 px-2">
                                                            <x-label value="Quantity"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-48 px-2 whitespace-nowrap">
                                                            <x-label value="Track Type"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-24 px-2">
                                                            <x-label value="Unit Price"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="w-24 px-2">
                                                            <x-label value="Amount"
                                                                class="text-[10px] leading-tight" />
                                                        </th>
                                                        <th class="px-1"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $a = 0; ?>
                                                    @foreach ($lands_ftl as $i => $ftl)
                                                        <tr class="">
                                                            <td class="px-2">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-20 h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_ftl.{{ $i }}.quantity"
                                                                        wire:model.defer='lands_ftl.{{ $i }}.quantity'>
                                                                </div>
                                                            </td>
                                                            <td class="px-2">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-48 h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_ftl.{{ $i }}.truck_type"
                                                                        wire:model.defer='lands_ftl.{{ $i }}.truck_type'>
                                                                </div>
                                                            </td>
                                                            <td class="px-2">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-24 h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_ftl.{{ $i }}.unit_price"
                                                                        wire:model.defer='lands_ftl.{{ $i }}.unit_price'>
                                                                </div>
                                                            </td>
                                                            <td class="px-2">
                                                                <div class="mb-2">
                                                                    <input type="text"
                                                                        class="w-24 h-6 p-2 text-xs border rounded-sm cursor-pointer"
                                                                        name="lands_ftl.{{ $i }}.amount"
                                                                        wire:model.defer='lands_ftl.{{ $i }}.amount'
                                                                        disabled>
                                                                </div>
                                                            </td>
                                                            <td class="">
                                                                <div class="flex -mt-2">
                                                                    @if (count($lands_ftl) > 1)
                                                                        <button type="button" title="Remove">
                                                                            <svg wire:click="removeLandsFtl({'i': {{ $i }}})"
                                                                                class="w-6 h-5 mt-1 mr-2 text-red"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                    @if (count($lands_ftl) == 1)
                                                                        <button type="button" title="">
                                                                            <svg wire:click="addLandsFtl({{ $i + 1 }})"
                                                                                class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                aria-hidden="true" focusable="false"
                                                                                data-prefix="far"
                                                                                data-icon="print-alt" role="img"
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                viewBox="0 0 448 512">
                                                                                <path fill="currentColor"
                                                                                    d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                            </svg>
                                                                        </button>
                                                                    @endif
                                                                    @if ($a != 0)
                                                                        @if (count($lands_ftl) == $a + 1)
                                                                            <button type="button" title="">
                                                                                <svg wire:click="addLandsFtl({{ $i + 1 }})"
                                                                                    class="w-6 h-5 mt-1 mr-5 text-blue"
                                                                                    aria-hidden="true"
                                                                                    focusable="false"
                                                                                    data-prefix="far"
                                                                                    data-icon="print-alt"
                                                                                    role="img"
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    viewBox="0 0 448 512">
                                                                                    <path fill="currentColor"
                                                                                        d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                                                                </svg>
                                                                            </button>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php $a++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
            </div>

            <div class="col-span-4 mb-4 mr-4 border rounded-lg shadow-md">
                <div class="col-span-12 p-2 px-6 border-b-2 border-blue-800 rounded-tl-lg rounded-tr-lg bg-blue ">
                    <span class="w-full text-base font-medium text-white rounded-tr-lg bg-blue" style="">
                        BREAKDOWN OF FREIGHT CHARGES
                    </span>
                </div>
                <div class="p-4 pt-0">
                    <table class="w-full mt-2 text-xs text-gray-500">
                        <tr>
                            <td class="w-[40%] font-medium text-gray-400">
                                <div>
                                    Weight Charge :
                                </div>
                            </td>
                            <td class="" style="padding-left: 20%">
                                <div>
                                    <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                        type="text" name="weight_charge"
                                        wire:model.defer='weight_charge'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="weight_charge" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-xs text-gray-500">
                        <tr>
                            <td class="w-[40%] font-medium text-gray-400">
                                <div>
                                    AWB Fee :
                                </div>
                            </td>
                            <td class="" style="padding-left: 20%">
                                <div>
                                    <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                        type="text" name="awb_fee" wire:model.defer='awb_fee'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="awb_fee" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-xs text-gray-500">
                        <tr>
                            <td class="w-[40%] font-medium text-gray-400">
                                <div>
                                    Valuation :
                                </div>
                            </td>
                            <td class="" style="padding-left: 20%">
                                <div>
                                    <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                        type="text" name="valuation" wire:model.defer='valuation'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="valuation" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-xs text-gray-500">
                        <tr>
                            <td class="w-[40%] font-medium text-gray-400">
                                <div>
                                    COD Charges :
                                </div>
                            </td>
                            <td class="" style="padding-left: 20%">
                                <div>
                                    <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                        type="text" name="cod_charge" wire:model.defer='cod_charge'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="cod_charge" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-xs text-gray-500">
                        <tr>
                            <td class="w-[40%] font-medium text-gray-400">
                                <div>
                                    Insurance :
                                </div>
                            </td>
                            <td class="" style="padding-left: 20%">
                                <div>
                                    <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                        type="text" name="insurance" wire:model.defer='insurance'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="insurance" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-xs text-gray-500">
                        <tr>
                            <td class="w-[40%] font-medium text-gray-400">
                                <div>
                                    Handling Fee :
                                </div>
                            </td>
                            <td class="" style="padding-left: 20%">
                                <div>
                                    <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                        type="text" name="handling_fee"
                                        wire:model.defer='handling_fee'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="handling_fee" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-xs text-gray-500">
                        <tr>
                            <td class="w-[40%] font-medium text-gray-400">
                                <div>
                                    Doc Fee :
                                </div>
                            </td>
                            <td class="" style="padding-left: 20%">
                                <div>
                                    <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                        type="text" name="doc_fee" wire:model.defer='doc_fee'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="doc_fee" disabled />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-xs text-gray-500">
                        <tr>
                            <td class="w-[40%] font-medium text-gray-400">
                                <div>
                                    Other Fees : <input type="checkbox"
                                        class="w-6 h-6 rounded-md border-[#003399] border cursor-pointer"
                                        name="other_fees" wire:model='other_fees'
                                        @if ($other_fees) checked @endif>
                                </div>
                            </td>
                            <td class="" style="padding-left: 20%">
                                <div>
                                    {{-- <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%" type="text"
                                        name="waybill_no" wire:model.defer='waybill_no'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="waybill_no" /> --}}
                                </div>
                            </td>
                        </tr>
                    </table>
                    @if ($other_fees)
                        <table class="w-full mt-2 text-xs text-gray-500">
                            <tr>
                                <td class="w-[40%] font-medium text-gray-400">
                                    <div>
                                        OPA Fee :
                                    </div>
                                </td>
                                <td class="" style="padding-left: 20%">
                                    <div>
                                        <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                            type="text" name="oda_fee" wire:model.defer='oda_fee'></x-input>
                                        <x-input-error class="absolute ml-4 text-xs" for="oda_fee" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="w-full mt-2 text-xs text-gray-500">
                            <tr>
                                <td class="w-[40%] font-medium text-gray-400">
                                    <div>
                                        ODA Fee :
                                    </div>
                                </td>
                                <td class="" style="padding-left: 20%">
                                    <div>
                                        <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                            type="text" name="oda_fee" wire:model.defer='oda_fee'></x-input>
                                        <x-input-error class="absolute ml-4 text-xs" for="oda_fee" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="w-full mt-2 text-xs text-gray-500">
                            <tr>
                                <td class="w-[40%] font-medium text-gray-400">
                                    <div>
                                        Crating Fee :
                                    </div>
                                </td>
                                <td class="" style="padding-left: 20%">
                                    <div>
                                        <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                            type="text" name="crating_fee"
                                            wire:model.defer='crating_fee'></x-input>
                                        <x-input-error class="absolute ml-4 text-xs" for="crating_fee" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="w-full mt-2 text-xs text-gray-500">
                            <tr>
                                <td class="w-[40%] font-medium text-gray-400">
                                    <div>
                                        Equipment Rental :
                                    </div>
                                </td>
                                <td class="" style="padding-left: 20%">
                                    <div>
                                        <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                            type="text" name="equipment_rental"
                                            wire:model.defer='equipment_rental'></x-input>
                                        <x-input-error class="absolute ml-4 text-xs" for="equipment_rental" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="w-full mt-2 text-xs text-gray-500">
                            <tr>
                                <td class="w-[40%] font-medium text-gray-400">
                                    <div>
                                        Lashing :
                                    </div>
                                </td>
                                <td class="" style="padding-left: 20%">
                                    <div>
                                        <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                            type="text" name="lashing_fee"
                                            wire:model.defer='lashing_fee'></x-input>
                                        <x-input-error class="absolute ml-4 text-xs" for="lashing_fee" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="w-full mt-2 text-xs text-gray-500">
                            <tr>
                                <td class="w-[40%] font-medium text-gray-400">
                                    <div>
                                        Manpower/Special Handling :
                                    </div>
                                </td>
                                <td class="" style="padding-left: 20%">
                                    <div>
                                        <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                            type="text" name="manpower_fee"
                                            wire:model.defer='manpower_fee'></x-input>
                                        <x-input-error class="absolute ml-4 text-xs" for="manpower_fee" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="w-full mt-2 text-xs text-gray-500">
                            <tr>
                                <td class="w-[40%] font-medium text-gray-400">
                                    <div>
                                        Dangerous Goods Fee :
                                    </div>
                                </td>
                                <td class="" style="padding-left: 20%">
                                    <div>
                                        <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                            type="text" name="dangerous_goods_fee"
                                            wire:model.defer='dangerous_goods_fee'></x-input>
                                        <x-input-error class="absolute ml-4 text-xs" for="dangerous_goods_fee" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="w-full mt-2 text-xs text-gray-500">
                            <tr>
                                <td class="w-[40%] font-medium text-gray-400">
                                    <div>
                                        Trucking :
                                    </div>
                                </td>
                                <td class="" style="padding-left: 20%">
                                    <div>
                                        <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                            type="text" name="trucking_fee"
                                            wire:model.defer='trucking_fee'></x-input>
                                        <x-input-error class="absolute ml-4 text-xs" for="trucking_fee" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="w-full mt-2 text-xs text-gray-500">
                            <tr>
                                <td class="w-[40%] font-medium text-gray-400">
                                    <div>
                                        Perishable Fee :
                                    </div>
                                </td>
                                <td class="" style="padding-left: 20%">
                                    <div>
                                        <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                            type="text" name="perishable_fee"
                                            wire:model.defer='perishable_fee'></x-input>
                                        <x-input-error class="absolute ml-4 text-xs" for="perishable_fee" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="w-full mt-2 text-xs text-gray-500">
                            <tr>
                                <td class="w-[40%] font-medium text-gray-400">
                                    <div>
                                        Packaging Fee :
                                    </div>
                                </td>
                                <td class="" style="padding-left: 20%">
                                    <div>
                                        <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                            type="text" name="packaging_fee"
                                            wire:model.defer='packaging_fee'></x-input>
                                        <x-input-error class="absolute ml-4 text-xs" for="packaging_fee" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="w-full mt-2 text-xs text-gray-500">
                            <tr>
                                <td class="w-[40%] font-medium text-gray-400">
                                    <div>
                                        Other Charges :
                                    </div>
                                </td>
                                <td class="" style="padding-left: 20%">
                                    <div>
                                        <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                            type="text" name="" wire:model.defer=''></x-input>
                                        <x-input-error class="absolute ml-4 text-xs" for="" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    @endif
                    <table class="w-full mt-2 text-xs text-gray-500">
                        <tr>
                            <td class="w-[40%] font-medium text-gray-400">
                                <div>
                                    Subtotal :
                                </div>
                            </td>
                            <td class="" style="padding-left: 20%">
                                <div>
                                    <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                        type="text" name="subtotal" wire:model.defer='subtotal'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="subtotal" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-xs text-gray-500">
                        <tr>
                            <td class="w-[40%] font-medium text-gray-400">
                                <div>
                                    Rate Discount :
                                </div>
                            </td>
                            <td class="" style="padding-left: 20%">
                                <div>
                                    <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                        type="text" name="rate_discount"
                                        wire:model.defer='rate_discount'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="rate_discount" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-xs text-gray-500">
                        <tr>
                            <td class="w-[40%] font-medium text-gray-400">
                                <div>
                                    Promo/Promo Code :
                                </div>
                            </td>
                            <td class="" style="padding-left: 20%">
                                <div>
                                    <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                        type="text" name="promo_code" wire:model.defer='promo_code'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="promo_code" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-xs text-gray-500">
                        <tr>
                            <td class="w-[40%] font-medium text-gray-400">
                                <div>
                                    Promo Discount % :
                                </div>
                            </td>
                            <td class="" style="padding-left: 20%">
                                <div>
                                    <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                        type="text" name="promo_discount"
                                        wire:model.defer='promo_discount'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="promo_discount" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-xs text-gray-500">
                        <tr>
                            <td class="w-[40%] font-medium text-gray-400">
                                <div>
                                    Promo Discount Amount :
                                </div>
                            </td>
                            <td class="" style="padding-left: 20%">
                                <div>
                                    <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                        type="text" name="promo_discount_amount"
                                        wire:model.defer='promo_discount_amount'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="promo_discount_amount" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-xs text-gray-500">
                        <tr>
                            <td class="w-[40%] font-medium text-gray-400">
                                <div>
                                    EVAT : <input type="checkbox" class="form-checkbox" wire:model="evat">
                                </div>
                            </td>
                            <td class="" style="padding-left: 20%">
                                <div>
                                    <x-input class="w-2/3 text-sm rounded-md h-7" style=" width: 100%"
                                        type="text" name="" wire:model.defer=''></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-span-12 p-2 px-4 bg-white border-t-2 border-blue-800 text-blue">
                    <div class="flex justify-between">
                        <span class="font-medium">Grand Total:</span>
                        <span class="font-medium">2509.86</span>
                    </div>
                </div>
            </div>

            <div
                class="col-span-12 p-2 px-10 py-4 bg-white border border-blue-800 rounded-bl-lg rounded-br-lg text-blue">
                <div class="flex justify-start">
                    <span class="text-lg font-semibold">TOTAL :</span>
                    <span class="mt-1 ml-12 text-sm text-black">Quantity :</span>
                    <span class="ml-2 text-lg font-semibold">2</span>
                    <span class="mt-1 ml-12 text-sm text-black">Weight :</span>
                    <span class="ml-2 text-lg font-semibold">70</span>
                    <span class="mt-1 ml-12 text-sm text-black">Volume :</span>
                    <span class="ml-2 text-lg font-semibold">0.018</span>
                </div>
            </div>

        </div>

        <div class="grid grid-cols-12 bg-white rounded-lg shadow-md">
            <div class="col-span-6 p-6">
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Account Owner :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="acc_own" wire:model.defer='acc_own'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="acc_own" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Prepared By :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="prep_by" wire:model.defer='prep_by'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="prep_by" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-span-6 p-6 ">
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium whitespace-nowrap">
                            <div>
                                Cargo Status :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 102%" type="text"
                                    name="cargo_stat" wire:model.defer='cargo_stat'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="cargo_stat" />
                            </div>
                        </td>
                        <td class="" style="padding-left:3%;">
                            <div>
                                <p class="font-medium underline text-blue" {{-- wire:click="action({'id': {{ $customer->id }}}, 'view')"> --}} wire:click="">
                                    TRACK
                                </p>
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Collection Status :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="coll_stat" wire:model.defer='coll_stat'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="coll_stat" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Remarks :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div class="flex space-x-4">
                                @can('oims_order_management_transaction_entry_view_remarks')
                                    <p class="font-medium underline cursor-pointer text-blue"
                                        wire:click="action({}, 'view_remarks')">
                                        View Remarks
                                    </p>
                                @endcan
                                @can('oims_order_management_transaction_entry_add_remarks')
                                    <p class="font-medium underline cursor-pointer text-blue" {{-- wire:click="action({'id': {{ $customer->id }}}, 'view')"> --}}
                                        wire:click="action({}, 'add_remarks')">
                                        Add Remarks
                                    </p>
                                </div>
                            @endcan
                        </td>
                        {{-- <td class="" style="padding-left: -8%">
                            <div>
                                <p class="font-medium underline text-blue" wire:click="">
                                    Add Remarks
                                </p>
                            </div>
                        </td> --}}
                    </tr>
                </table>
            </div>
        </div>

        <div class="grid grid-cols-12 bg-white rounded-lg shadow-md">
            <div class="col-span-6 p-6">
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Quality Checked By :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="qc_by" wire:model.defer='qc_by'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="qc_by" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Overridden By :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="over_by" wire:model.defer='over_by'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="over_by" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium whitespace-nowrap">
                            <div>
                                @can('oims_order_management_transaction_entry_override_history')
                                    <p class="font-medium underline cursor-pointer text-blue" {{-- wire:click="action({'id': {{ $customer->id }}}, 'view')"> --}}
                                        wire:click="action({}, 'override_history')">
                                        View Override History
                                    </p>
                                @endcan
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-span-6 p-6 ">
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Quality Checking Date and Time :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="qc_date_time" wire:model.defer='qc_date_time'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="qc_date_time" />
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="w-full mt-2 text-sm text-gray-500">
                    <tr>
                        <td class="w-[25%] font-medium">
                            <div>
                                Date and Time Overridden :
                            </div>
                        </td>
                        <td class="" style="padding-left: 9%">
                            <div>
                                <x-input class="w-2/3 text-sm rounded-md h-9" style=" width: 80%" type="text"
                                    name="date_time_overdn" wire:model.defer='date_time_overdn'></x-input>
                                <x-input-error class="absolute ml-4 text-xs" for="date_time_overdn" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        {{-- @dd($id) --}}


        <div class="grid grid-cols-12 bg-white rounded-lg shadow-md">
            .
        </div>
        <div class="fixed bottom-0 left-0 grid w-full p-8 bg-white border-2 rounded-lg shadow-xl ">
            <div class="flex justify-end pr-8 space-x-8" style="">
                <button wire:click="actionCompute({}, 'compute')"
                    class="px-6 py-2 text-base font-medium text-white rounded-md whitespace-nowrap bg-blue hover:bg-blue-800 ">
                    Compute Transaction
                </button>
                {{-- </div>
            <div class=""> --}}
                {{-- <button wire:click="submit"
                    class="px-6 py-2 text-base font-medium text-white rounded-md whitespace-nowrap bg-blue hover:bg-blue-800 ">
                    Save Transaction
                </button> --}}
                <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
                    <x-button type="submit" title="Save Transaction"
                        class="px-6 py-2 text-base font-medium text-white rounded-md whitespace-nowrap bg-blue hover:bg-blue-800 " />
                </form>

            </div>
        </div>

    </div>



</div>
