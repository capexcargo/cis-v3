<div x-data="{
    add_remarks_modal: '{{ $add_remarks_modal }}',
    {{-- confirmation_modal: '{{ $confirmation_modal }}', --}}
}">
    <x-loading></x-loading>
    @if($add_remarks_modal)
    @can('oims_order_management_transaction_entry_add_remarks')
        {{-- @if ($summary_id && $terms_modal) --}}
        <x-modal id="add_remarks_modal" size="w-11/12">
            {{-- <x-slot name="title">Terms and Conditions</x-slot> --}}
            <x-slot name="body">
                @livewire('oims.order-management.transaction-entry.add-remarks')
                {{-- @livewire('crm.sales.sales-campaign.summary.terms-view', ['id' => $summary_id]) --}}
            </x-slot>
        </x-modal>
        {{-- @endif --}}
    @endcan
    @endif


    {{-- <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to submit this new Team?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal> --}}

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="mt-5 space-y-3">
            <div class="grid grid-cols-12 bg-white rounded-lg shadow-md ">
                <div class="col-span-12 p-2 px-6 border-b-2 border-blue-800 rounded-tl-lg rounded-tr-lg bg-blue ">
                    <span class="w-full text-base font-medium text-white rounded-tr-lg bg-blue" style="">
                        Remarks Overview
                    </span>
                </div>
                <div class="col-span-8 p-6">
                    <table class="w-full mt-2 text-sm text-gray-500">
                        <tr>
                            <td class="w-[25%] font-medium">
                                <div>
                                    <label for="name" class="text-sm font-normal text-gray-500">AWB
                                        Number</label><br>
                                    <x-input disabled class="w-2/3 h-8 text-sm rounded-md" style=" width: 80%"
                                        type="text" name="waybill_no" wire:model.defer='waybill_no'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="waybill_no" />
                                </div>
                            </td>
                            <td class="w-[25%] font-medium">
                                <div>
                                    <label for="name" class="text-sm font-normal text-gray-500">Transaction
                                        Date</label><br>
                                    <x-input disabled class="w-2/3 h-8 text-sm rounded-md" style=" width: 80%"
                                        type="text" name="waybill_no" wire:model.defer='waybill_no'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="waybill_no" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-sm text-gray-500">
                        <tr>
                            <td class="w-[25%] font-medium">
                                <div>
                                    <label for="name" class="text-sm font-normal text-gray-500">Transport
                                        Mode</label><br>
                                    <x-input disabled class="w-2/3 h-8 text-sm rounded-md" style=" width: 80%"
                                        type="text" name="waybill_no" wire:model.defer='waybill_no'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="waybill_no" />
                                </div>
                            </td>
                            <td class="w-[25%] font-medium">
                                <div>
                                    <label for="name" class="text-sm font-normal text-gray-500">Paymode</label><br>
                                    <x-input disabled class="w-2/3 h-8 text-sm rounded-md" style=" width: 80%"
                                        type="text" name="waybill_no" wire:model.defer='waybill_no'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="waybill_no" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="w-full mt-2 text-sm text-gray-500">
                        <tr>
                            <td class="w-[25%] font-medium">
                                <div>
                                    <label for="name" class="text-sm font-normal text-gray-500">Origin</label><br>
                                    <x-input disabled class="w-2/3 h-8 text-sm rounded-md" style=" width: 80%"
                                        type="text" name="waybill_no" wire:model.defer='waybill_no'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="waybill_no" />
                                </div>
                            </td>
                            <td class="w-[25%] font-medium">
                                <div>
                                    <label for="name"
                                        class="text-sm font-normal text-gray-500">Destination</label><br>
                                    <x-input disabled class="w-2/3 h-8 text-sm rounded-md" style=" width: 80%"
                                        type="text" name="waybill_no" wire:model.defer='waybill_no'></x-input>
                                    <x-input-error class="absolute ml-4 text-xs" for="waybill_no" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="col-span-12 p-2 bg-white border rounded-lg shadow-md">
                    <x-table.table class="overflow-hidden">
                        <x-slot name="thead">
                            <x-table.th name="Transaction Stage" style="padding-left:2%;" />
                            <x-table.th name="Date & Time" style="padding-left:%;" />
                            <x-table.th name="User" style="padding-left:%;" />
                            <x-table.th name="Internal Remarks" style="padding-left:%;" />
                            <x-table.th name="External Remarks" style="padding-left:%;" />
                            <x-table.th name="Action" style="padding-left:4%;" />
                        </x-slot>
                        <x-slot name="tbody">
                            {{-- @foreach ($route_categs as $i => $route_categ) --}}
                            <tr class="border cursor-pointer hover:text-white hover:bg-[#4068b8] font-normal">
                                <td class="w-1/6 p-3 border-r-2 whitespace-nowrap" style="padding-left:2%;">
                                    Pick-up
                                </td>
                                <td class="w-1/6 p-3 border-r-2 whitespace-nowrap" style="padding-left:;">
                                    11/16/2022 13:44:51
                                </td>
                                <td class="w-1/6 p-3 border-r-2 whitespace-nowrap" style="padding-left:;">
                                    Ace Cunanan
                                </td>
                                <td class="w-1/6 p-3 border-r-2 whitespace-nowrap" style="padding-left:;">
                                    For Crating
                                </td>
                                <td class="w-1/5 p-3 whitespace-pre-line border-r-2" style="padding-left:;">
                                    Contact person not available
                                </td>
                                <td class="p-3 whitespace-nowrap" style="padding-left:4%;">
                                    <div class="flex space-x-4">
                                        <span title="View">
                                            <svg wire:click="" class="w-5 h-5 text-blue" aria-hidden="true"
                                                focusable="false" data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M288 32c-80.8 0-145.5 36.8-192.6 80.6C48.6 156 17.3 208 2.5 243.7c-3.3 7.9-3.3 16.7 0 24.6C17.3 304 48.6 356 95.4 399.4C142.5 443.2 207.2 480 288 480s145.5-36.8 192.6-80.6c46.8-43.5 78.1-95.4 93-131.1c3.3-7.9 3.3-16.7 0-24.6c-14.9-35.7-46.2-87.7-93-131.1C433.5 68.8 368.8 32 288 32zM144 256a144 144 0 1 1 288 0 144 144 0 1 1 -288 0zm144-64c0 35.3-28.7 64-64 64c-7.1 0-13.9-1.2-20.3-3.3c-5.5-1.8-11.9 1.6-11.7 7.4c.3 6.9 1.3 13.8 3.2 20.7c13.7 51.2 66.4 81.6 117.6 67.9s81.6-66.4 67.9-117.6c-11.1-41.5-47.8-69.4-88.6-71.1c-5.8-.2-9.2 6.1-7.4 11.7c2.1 6.4 3.3 13.2 3.3 20.3z">
                                                </path>
                                            </svg>
                                        </span>

                                        <span title="View Image">
                                            <svg wire:click="" class="w-5 h-5 text-blue" aria-hidden="true"
                                                focusable="false" data-prefix="fas" data-icon="user-slash"
                                                role="img" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 640 512">
                                                <path fill="currentColor"
                                                    d="M64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V160H256c-17.7 0-32-14.3-32-32V0H64zM256 0V128H384L256 0zM64 256a32 32 0 1 1 64 0 32 32 0 1 1 -64 0zm152 32c5.3 0 10.2 2.6 13.2 6.9l88 128c3.4 4.9 3.7 11.3 1 16.5s-8.2 8.6-14.2 8.6H216 176 128 80c-5.8 0-11.1-3.1-13.9-8.1s-2.8-11.2 .2-16.1l48-80c2.9-4.8 8.1-7.8 13.7-7.8s10.8 2.9 13.7 7.8l12.8 21.4 48.3-70.2c3-4.3 7.9-6.9 13.2-6.9z">
                                                </path>
                                            </svg>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            {{-- @endforeach --}}
                        </x-slot>
                    </x-table.table>
                </div>
            </div>

        </div>

        <div class="flex justify-end mt-2">
            @can('oims_order_management_transaction_entry_add_remarks')
                <p class="font-medium underline text-blue" {{-- wire:click="action({'id': {{ $customer->id }}}, 'view')"> --}} wire:click="action({}, 'add_remarks')">
                    Add Remarks
                </p>
            @endcan
        </div>
        {{-- <div class="flex justify-end gap-3 mt-6 space-x-3">
            <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                class="px-12 bg-white text-blue hover:bg-gray-100" />
            <x-button type="submit" title="Submit" class="px-12 bg-blue text-white hover:bg-[#002161]" />
        </div> --}}
    </form>
</div>
