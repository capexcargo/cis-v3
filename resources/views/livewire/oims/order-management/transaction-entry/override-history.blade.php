<div x-data="{
    commodity_applicable_rate_modal: '{{ $commodity_applicable_rate_modal }}',
    service_mode_modal: '{{ $service_mode_modal }}',
    shipper_contact_person_modal: '{{ $shipper_contact_person_modal }}',
    consignee_mobile_number_modal: '{{ $consignee_mobile_number_modal }}',
}">
    <x-loading></x-loading>

    {{-- @can('oims_order_management_transaction_entry_add_remarks') --}}
    @if($commodity_applicable_rate_modal)
        <x-modal id="commodity_applicable_rate_modal" size="w-2/5">
            <x-slot name="body">
                {{-- @livewire('oims.order-management.transaction-entry.add-remarks') --}}
                <div class="grid grid-cols-12 bg-white rounded-lg ">
                    <div class="col-span-12 p-2">
                        <span class="w-full text-xl font-semibold text-black" style="">
                            Commodity Applicable Rate
                        </span>
                    </div>
    
                    <div class="col-span-12 p-2 bg-white border rounded-lg shadow-md">
                        <x-table.table class="overflow-hidden">
                            <x-slot name="thead">
                                <x-table.th name="Old" style="padding-left:2%;" />
                                <x-table.th name="New" style="padding-left:%; color:#1E40AF;" />
                            </x-slot>
                            <x-slot name="tbody">
                                {{-- @foreach ($route_categs as $i => $route_categ) --}}
                                <tr class="font-normal border cursor-pointer">
                                    <td class="w-1/6 p-3 font-medium border-r-2 border-gray-600 whitespace-nowrap" style="padding-left:2%;">
                                        Bongga RG
                                    </td>
                                    <td class="w-1/6 p-3 font-medium text-blue-800 whitespace-nowrap" style="padding-left:;">
                                        Bida RG
                                    </td>
                                </tr>
                                {{-- @endforeach --}}
                            </x-slot>
                        </x-table.table>
                    </div>
                </div>
            </x-slot>
        </x-modal>
    @endif

    {{-- @endcan --}}
    @if($service_mode_modal)
    <x-modal id="service_mode_modal" size="w-2/5">
        <x-slot name="body">
            {{-- @livewire('oims.order-management.transaction-entry.add-remarks') --}}
            <div class="grid grid-cols-12 bg-white rounded-lg ">
                <div class="col-span-12 p-2">
                    <span class="w-full text-xl font-semibold text-black" style="">
                        Service Mode
                    </span>
                </div>

                <div class="col-span-12 p-2 bg-white border rounded-lg shadow-md">
                    <x-table.table class="overflow-hidden">
                        <x-slot name="thead">
                            <x-table.th name="Old" style="padding-left:2%;" />
                            <x-table.th name="New" style="padding-left:%; color:#1E40AF;" />
                        </x-slot>
                        <x-slot name="tbody">
                            {{-- @foreach ($route_categs as $i => $route_categ) --}}
                            <tr class="font-normal border cursor-pointer">
                                <td class="w-1/6 p-3 font-medium border-r-2 border-gray-600 whitespace-nowrap" style="padding-left:2%;">
                                    Airport to Airport
                                </td>
                                <td class="w-1/6 p-3 font-medium text-blue-800 whitespace-nowrap" style="padding-left:;">
                                    Airport to Door
                                </td>
                            </tr>
                            {{-- @endforeach --}}
                        </x-slot>
                    </x-table.table>
                </div>
            </div>
        </x-slot>
    </x-modal>
    @endif

    @if($shipper_contact_person_modal)
    <x-modal id="shipper_contact_person_modal" size="w-2/5">
        <x-slot name="body">
            {{-- @livewire('oims.order-management.transaction-entry.add-remarks') --}}
            <div class="grid grid-cols-12 bg-white rounded-lg ">
                <div class="col-span-12 p-2">
                    <span class="w-full text-xl font-semibold text-black" style="">
                        Shipper Details-Contact Person
                    </span>
                </div>

                <div class="col-span-12 p-2 bg-white border rounded-lg shadow-md">
                    <x-table.table class="overflow-hidden">
                        <x-slot name="thead">
                            <x-table.th name="Old" style="padding-left:2%;" />
                            <x-table.th name="New" style="padding-left:%; color:#1E40AF;" />
                        </x-slot>
                        <x-slot name="tbody">
                            {{-- @foreach ($route_categs as $i => $route_categ) --}}
                            <tr class="font-normal border cursor-pointer">
                                <td class="w-1/6 p-3 font-medium border-r-2 border-gray-600 whitespace-nowrap" style="padding-left:2%;">
                                    Karl Magtanong
                                </td>
                                <td class="w-1/6 p-3 font-medium text-blue-800 whitespace-nowrap" style="padding-left:;">
                                    Glenly Mallari
                                </td>
                            </tr>
                            {{-- @endforeach --}}
                        </x-slot>
                    </x-table.table>
                </div>
            </div>
        </x-slot>
    </x-modal>
    @endif

    @if($consignee_mobile_number_modal)
    <x-modal id="consignee_mobile_number_modal" size="w-2/5">
        <x-slot name="body">
            {{-- @livewire('oims.order-management.transaction-entry.add-remarks') --}}
            <div class="grid grid-cols-12 bg-white rounded-lg ">
                <div class="col-span-12 p-2">
                    <span class="w-full text-xl font-semibold text-black" style="">
                        Consignee Details-Mobile Number
                    </span>
                </div>

                <div class="col-span-12 p-2 bg-white border rounded-lg shadow-md">
                    <x-table.table class="overflow-hidden">
                        <x-slot name="thead">
                            <x-table.th name="Old" style="padding-left:2%;" />
                            <x-table.th name="New" style="padding-left:%; color:#1E40AF;" />
                        </x-slot>
                        <x-slot name="tbody">
                            {{-- @foreach ($route_categs as $i => $route_categ) --}}
                            <tr class="font-normal border cursor-pointer">
                                <td class="w-1/6 p-3 font-medium border-r-2 border-gray-600 whitespace-nowrap" style="padding-left:2%;">
                                    0908 552 0019
                                </td>
                                <td class="w-1/6 p-3 font-medium text-blue-800 whitespace-nowrap" style="padding-left:;">
                                    0927 262 5142
                                </td>
                            </tr>
                            {{-- @endforeach --}}
                        </x-slot>
                    </x-table.table>
                </div>
            </div>
        </x-slot>
    </x-modal>
    @endif


    {{-- <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to submit this new Team?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal> --}}

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="mt-5 space-y-3">
            <div class="grid grid-cols-12 bg-white rounded-lg ">
                <div class="col-span-12 p-2 border-b-2 border-blue-800">
                    <span class="w-full text-xl font-semibold text-black" style="">
                        Override History
                    </span>
                </div>

                <div class="col-span-12 p-2 bg-white border rounded-lg shadow-md">
                    <x-table.table class="overflow-hidden">
                        <x-slot name="thead">
                            <x-table.th name="Date & Time" style="padding-left:2%;" />
                            <x-table.th name="Field/s" style="padding-left:%;" />
                            <x-table.th name="User" style="padding-left:%;" />
                            <x-table.th name="Approver" style="padding-left:%;" />
                        </x-slot>
                        <x-slot name="tbody">
                            {{-- @foreach ($route_categs as $i => $route_categ) --}}
                            <tr class="font-normal border cursor-pointer">
                                <td class="w-1/6 p-3 border-r-2 whitespace-nowrap" style="padding-left:2%;">
                                    7/21/2022 14:19:31
                                </td>
                                <td class="w-1/6 p-3 border-r-2 whitespace-nowrap" style="padding-left:;">
                                    <div>
                                        <p class="font-medium underline text-blue" {{-- wire:click="action({'id': {{ $customer->id }}}, 'view')"> --}}
                                            wire:click="action({}, 'commodity_applicable_rate')">
                                            Commodity Applicable Rate
                                        </p>
                                        <p class="font-medium underline text-blue" {{-- wire:click="action({'id': {{ $customer->id }}}, 'view')"> --}}
                                            wire:click="action({}, 'service_mode')">
                                            Service Mode
                                        </p>
                                        <p class="font-medium underline text-blue" {{-- wire:click="action({'id': {{ $customer->id }}}, 'view')"> --}}
                                            wire:click="action({}, 'service_mode')">
                                            Shipper - Contact Person
                                        </p>
                                        <p class="font-medium underline text-blue" {{-- wire:click="action({'id': {{ $customer->id }}}, 'view')"> --}}
                                            wire:click="action({}, 'service_mode')">
                                            Consignee - Mobile Number
                                        </p>
                                    </div>
                                </td>
                                <td class="w-1/6 p-3 border-r-2 whitespace-nowrap" style="padding-left:;">
                                    Ace Cunanan
                                </td>
                                <td class="w-1/6 p-3 border-r-2 whitespace-nowrap" style="padding-left:;">
                                    Gomez, Erika Mae
                                </td>
                            </tr>
                            {{-- @endforeach --}}
                        </x-slot>
                    </x-table.table>
                </div>
            </div>
        </div>

    </form>
</div>
