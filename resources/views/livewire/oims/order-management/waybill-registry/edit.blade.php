<div wire:init="load" x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
    confirmsubedit_modal: '{{ $confirmsubedit_modal }}',

}">
    <x-loading></x-loading>
    @if($confirmation_modal)
    <x-modal id="confirmation_modal" size="w-auto" hasClose="1">
        <x-slot name="body">
            <span class="relative block w-full">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to save edited information?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Save
                </button>
            </div>
        </x-slot>
    </x-modal>
    @endif
    @if($confirmsubedit_modal)
    <x-modal id="confirmsubedit_modal" size="w-auto" hasClose="1">
        <x-slot name="body">
            <form>
                <h2 class="text-xl text-center whitespace-nowrap"> Waybill series <span
                        class="text-blue-800">{{ $frm }}</span> has/have been updated!</h2>
                <div class="flex justify-center space-x-10">
                    <button wire:click="actionc({}, 'ba_close')"
                        class="px-8 py-1 mt-2 text-sm font-medium text-white bg-blue-800 border-2 rounded-md border-blue whitespace-nowrap hover:bg-blue-800">
                        OK
                    </button>
                </div>
            </form>
        </x-slot>
    </x-modal>
    @endif

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="grid grid-cols-12 gap-3">
            <div class="col-span-3 mt-3">
                <x-label for="from" value="From" :required="true" />

                <x-input class="block w-full rounded-md h-9" type="text" name="from" wire:model='from'></x-input>
                <x-input-error class="text-xs" for="from" />
            </div>
            <div class="col-span-3 mt-3">
                <x-label for="to" value="To" :required="true" />
                <x-input class="block w-full rounded-md h-9" type="text" name="to" wire:model='to'></x-input>
                <x-input-error class="text-xs" for="to" />
            </div>
            <div class="col-span-2 mt-3">
                <x-label for="" value="." style="color: white;" />

            </div>
            <div class="flex col-span-3 mt-3 ml-4">
            </div>
            <div class="col-span-12 bg-white border border-gray-400 rounded-md shadow-md">
                <table class="min-w-full p-2 overflow-hidden rounded-md" style="">
                    <thead>
                        <tr class="p-2 text-left">
                            <th colspan="2" class="px-3 py-2 text-xs font-normal border-r-2 border-gray-300 ">
                                Issued To
                                {{-- @dd($getStatus1) --}}
                            </th>
                            <th class="px-3 py-2 text-xs font-normal ">
                                Branch
                            </th>
                        </tr>
                    </thead>
                    <tbody class="divide-y-4 divide-white ">
                        <tr class="pb-2 text-left border-gray-300 ">
                            <td class="w-1/3 px-3 py-2 " style="background-color: #F7F7F7; ">
                                <select class="w-full rounded-md h-9" style="cursor: pointer;" name="name_1"
                                    wire:model='name_1'>
                                    <option value="">Select</option>
                                    {{-- @dd($name_2);
                                    @dd($getStatus1); --}}
                                    @if ($getStatus1->status != null)
                                        @if ($getStatus1->status == 4)
                                            <option value="1">Checker</option>
                                        @endif
                                        @if ($getStatus1->status == 3)
                                            <option value="1">Checker</option>
                                        @endif
                                        @if ($getStatus1->status == 2)
                                            <option value="4">OM</option>
                                        @endif
                                        @if ($getStatus1->status == 1)
                                            <option value="5">FLS</option>
                                            <option value="2">Customer</option>
                                            <option value="3">Agent</option>
                                        @endif
                                    @endif

                                    {{-- <option value="1">Checker</option>
                                    <option value="4">OM</option>

                                    <option value="5">FLS</option>

                                    <option value="2">Customer</option>
                                    <option value="3">Agent</option> --}}
                                </select>
                                <x-input-error class="text-xs" for="name_1" />
                            </td>
                            @if (isset($name_1))
                                @if ($name_1 == 1)
                                    <td class="w-1/3 px-3 py-2 border-r-2 border-gray-300 "
                                        style="background-color: #F7F7F7; " wire:init="empReference">
                                        <select class="w-full rounded-md h-9" style="cursor: pointer;" name="name_2"
                                            wire:model.defer='name_2'>
                                            <option value="">Select</option>
                                            @foreach ($empReferences as $empReference)
                                                <option value="{{ $empReference->id }}">
                                                    {{ $empReference->first_name }}
                                                    {{ $empReference->middle_name }}
                                                    {{ $empReference->last_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <x-input-error class="text-xs" for="name_2" />
                                    </td>
                                @elseif($name_1 == 2)
                                    <td class="w-1/3 px-3 py-2 border-r-2 border-gray-300 "
                                        style="background-color: #F7F7F7; " wire:init="custReference">
                                        <select class="w-full rounded-md h-9" style="cursor: pointer;" name="name_2"
                                            wire:model.defer='name_2'>
                                            <option value="">Select</option>
                                            @foreach ($custReferences as $custReference)
                                                <option value="{{ $custReference->id }}">
                                                    {{ $custReference->fullname }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <x-input-error class="text-xs" for="name_2" />
                                    </td>
                                @elseif($name_1 == 3)
                                    <td class="w-1/3 px-3 py-2 border-r-2 border-gray-300 "
                                        style="background-color: #F7F7F7; " wire:init="agentReference">
                                        <select class="w-full rounded-md h-9" style="cursor: pointer;" name="name_2"
                                            wire:model.defer='name_2'>
                                            <option value="">Select</option>
                                            @foreach ($agentReferences as $agentReference)
                                                <option value="{{ $agentReference->id }}">
                                                    {{ $agentReference->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <x-input-error class="text-xs" for="name_2" />
                                    </td>
                                @elseif($name_1 == 4)
                                    <td class="w-1/3 px-3 py-2 border-r-2 border-gray-300 "
                                        style="background-color: #F7F7F7; "wire:init="omReference">
                                        <select class="w-full rounded-md h-9" style="cursor: pointer;" name="name_2"
                                            wire:model.defer='name_2'>
                                            <option value="">Select</option>
                                            @foreach ($omReferences as $omReference)
                                                <option value="{{ $omReference->id }}">
                                                    {{ $omReference->first_name }}
                                                    {{ $omReference->middle_name }}
                                                    {{ $omReference->last_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <x-input-error class="text-xs" for="name_2" />
                                    </td>
                                @elseif($name_1 == 5)
                                    <td class="w-1/3 px-3 py-2 border-r-2 border-gray-300 "
                                        style="background-color: #F7F7F7; "wire:init="flsReference">
                                        <select class="w-full rounded-md h-9" style="cursor: pointer;" name="name_2"
                                            wire:model.defer='name_2'>
                                            <option value="">Select</option>
                                            @foreach ($flsReferences as $flsReference)
                                                <option value="{{ $flsReference->id }}">
                                                    {{ $flsReference->first_name }}
                                                    {{ $flsReference->middle_name }}
                                                    {{ $flsReference->last_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <x-input-error class="text-xs" for="name_2" />
                                    </td>
                                @endif
                            @else
                                <td class="w-1/3 px-3 py-2 border-r-2 border-gray-300 "
                                    style="background-color: #F7F7F7; " wire:init="">
                                    <select disabled class="w-full rounded-md h-9" style="cursor: pointer;"
                                        name="name_2" wire:model.defer='name_2'>
                                        <option value="">Select</option>
                                    </select>
                                    <x-input-error class="text-xs" for="name_2" />
                                </td>
                            @endif

                            <td class="w-1/3 px-3 py-2 " style="background-color: #F7F7F7; "
                                wire:init="branchReference">
                                <select class="w-full rounded-md h-9" style="cursor: pointer;" name="branch"
                                    wire:model.defer='branch'>
                                    <option value="">Select</option>
                                    @foreach ($branchReferences as $branchReference)
                                        <option value="{{ $branchReference->id }}">
                                            {{ $branchReference->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <x-input-error class="text-xs" for="branch" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="flex justify-end gap-3 mt-6 space-x-3">
            <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                class="px-12 bg-white text-blue hover:bg-gray-100" />
            <x-button type="submit" title="Save" class="px-12 bg-blue text-white hover:bg-[#002161]" />
        </div>
    </form>
</div>
