<div wire:init="load" x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
    confirmsubcreate_modal: '{{ $confirmsubcreate_modal }}',
}">
    <x-loading></x-loading>
    @if($confirmation_modal)
    <x-modal id="confirmation_modal" size="w-auto" hasClose="1">
        <x-slot name="body">
            <span class="relative block w-full">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                @if(isset($ws))
                Are you sure you want to register waybill series {{ $ws }}?
                @endif

            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Proceed
                </button>
            </div>
        </x-slot>
    </x-modal>
    @endif
    @if($confirmsubcreate_modal)
    <x-modal id="confirmsubcreate_modal" size="w-auto" hasClose="1">
        <x-slot name="body">
            <form>
                <h2 class="text-xl text-center whitespace-nowrap"> Waybill series <span
                        class="text-blue-800">{{ $ws }}</span> has/have been registered!</h2>

                <div class="flex justify-center space-x-10">
                    <button wire:click="actionc({}, 'ba_close')"
                        class="px-8 py-1 mt-2 text-sm font-medium text-white bg-blue-800 border-2 rounded-md border-blue whitespace-nowrap hover:bg-blue-800">
                        OK
                    </button>
                </div>
            </form>
        </x-slot>
    </x-modal>
    @endif

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="grid grid-cols-12 gap-3">
            <?php $i = 0; ?>
            @foreach ($awbs as $a => $awb)
                <div class="col-span-3 mt-3">
                    @if ($i == 0)
                        <x-label for="awbs.{{ $a }}.{{ $a }}.from" value="From"
                            :required="true" />
                    @else
                    @endif
                    <x-input class="block w-full rounded-md h-9" type="text"
                        name="awbs.{{ $a }}.{{ $a }}.from"
                        wire:model.defer='awbs.{{ $a }}.{{ $a }}.from'></x-input>
                    <x-input-error class="text-xs" for="awbs.{{ $a }}.{{ $a }}.from" />
                </div>
                <div class="col-span-3 mt-3">
                    @if ($i == 0)
                        <x-label for="awbs.{{ $a }}.{{ $a }}.to" value="To"
                            :required="true" />
                    @else
                    @endif
                    <x-input class="block w-full rounded-md h-9" type="text"
                        name="awbs.{{ $a }}.{{ $a }}.to"
                        wire:model.defer='awbs.{{ $a }}.{{ $a }}.to'></x-input>
                    <x-input-error class="text-xs" for="awbs.{{ $a }}.{{ $a }}.to" />
                </div>
                <div class="col-span-2 mt-3">
                    @if ($i == 0)
                        <x-label for="" value="." style="color: white;" />
                    @else
                    @endif
                    @if (isset($subawbs[$a]))
                        <x-button type="button" wire:click="removesubawb({'a': {{ $a }}})" title="Issue"
                            class="px-12 text-white bg-blue hover:bg-[#002161]" />
                    @else
                        <x-button type="button" wire:click="addsubawb({{ $a }})" title="Issue"
                            class="px-12 bg-white text-blue border-blue border hover:bg-[#002161] hover:text-white" />
                    @endif

                </div>
                <div class="flex col-span-3 mt-3 ml-4">
                    <div class="grid grid-cols-2">
                        @if (count($awbs) > 1)
                            @if ($i == 0)
                                <div class="col-span-1 mt-6"></div>
                            @else
                                <div class="col-span-1"></div>
                            @endif
                            <x-label for="process" value="" />
                            <svg wire:click="removeawb({'a': {{ $a }}})"
                                class="z-10 w-8 h-6 cursor-pointer text-[#FF0000]" aria-hidden="true" focusable="false"
                                data-prefix="fas" data-icon="circle-minus" role="img"
                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                <path fill="currentColor"
                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z">
                                </path>
                            </svg>
                        @endif

                        @if (count($awbs) == 1)
                            <div class="col-span-1 pr-4 mt-6">
                                <x-label for="TellNo" value="" />
                                <svg wire:click="addawb({{ $a + 1 }})"
                                    class="w-8 h-6 cursor-pointer text-[#003399]" aria-hidden="true" focusable="false"
                                    data-prefix="fas" data-icon="trash-alt" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                    </path>
                                </svg>
                            </div>
                        @endif

                        @if ($i != 0)
                            @if (count($awbs) == $i + 1)
                                <div class="col-span-1 ml-1">
                                    <svg wire:click="addawb({{ $a + 1 }})"
                                        class="w-8 h-6 cursor-pointer text-[#003399]" aria-hidden="true"
                                        focusable="false" data-prefix="fas" data-icon="trash-alt" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                        <path fill="currentColor"
                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                        </path>
                                    </svg>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>

                <?php $b = 0; ?>
                @if (isset($subawbs[$a]))
                    @foreach ($subawbs[$a] as $c => $subawb)
                        <div class="col-span-12 bg-white border border-gray-400 rounded-md shadow-md">
                            <table class="min-w-full p-2 overflow-hidden rounded-md" style="">
                                <thead>
                                    <tr class="p-2 text-left">
                                        <th colspan="2"
                                            class="px-3 py-2 text-xs font-normal border-r-2 border-gray-300 ">
                                            Issued To
                                        </th>
                                        <th class="px-3 py-2 text-xs font-normal ">
                                            Branch
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="divide-y-4 divide-white ">
                                    <tr class="pb-2 text-left border-gray-300 ">
                                        <td class="w-1/3 px-3 py-2 " style="background-color: #F7F7F7; ">
                                            <select class="w-full rounded-md h-9" style="cursor: pointer;"
                                                name="subawbs.{{ $a }}.{{ $c }}.name_1"
                                                wire:model='subawbs.{{ $a }}.{{ $c }}.name_1'>
                                                <option value="">Select</option>
                                                @if ($getStatus1->status != null)
                                                    @if ($getStatus1->status == 3)
                                                        <option value="1">Checker</option>
                                                    @endif
                                                    @if ($getStatus1->status == 2)
                                                        <option value="4">OM</option>
                                                    @endif
                                                    @if ($getStatus1->status == 1)
                                                        <option value="5">FLS</option>
                                                        <option value="2">Customer</option>
                                                        <option value="3">Agent</option>
                                                    @endif
                                                @endif
                                                
                                            </select>
                                            <x-input-error class="text-xs"
                                                for="subawbs.{{ $a }}.{{ $c }}.name_1" />
                                        </td>
                                        @if (isset($subawbs[$a][$c]['name_1']))
                                            @if ($subawbs[$a][$c]['name_1'] == 1)
                                                <td class="w-1/3 px-3 py-2 border-r-2 border-gray-300 "
                                                    style="background-color: #F7F7F7; " wire:init="empReference">
                                                    <select class="w-full rounded-md h-9" style="cursor: pointer;"
                                                        name="subawbs.{{ $a }}.{{ $c }}.name_2"
                                                        wire:model.defer='subawbs.{{ $a }}.{{ $c }}.name_2'>
                                                        <option value="">Select</option>
                                                        @foreach ($empReferences as $empReference)
                                                            <option value="{{ $empReference->id }}">
                                                                {{ $empReference->first_name }}
                                                                {{ $empReference->middle_name }}
                                                                {{ $empReference->last_name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    <x-input-error class="text-xs"
                                                        for="subawbs.{{ $a }}.{{ $c }}.name_2" />
                                                </td>
                                            @elseif($subawbs[$a][$c]['name_1'] == 2)
                                                <td class="w-1/3 px-3 py-2 border-r-2 border-gray-300 "
                                                    style="background-color: #F7F7F7; " wire:init="custReference">
                                                    <select class="w-full rounded-md h-9" style="cursor: pointer;"
                                                        name="subawbs.{{ $a }}.{{ $c }}.name_2"
                                                        wire:model.defer='subawbs.{{ $a }}.{{ $c }}.name_2'>
                                                        <option value="">Select</option>
                                                        @foreach ($custReferences as $custReference)
                                                            <option value="{{ $custReference->id }}">
                                                                {{ $custReference->fullname }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    <x-input-error class="text-xs"
                                                        for="subawbs.{{ $a }}.{{ $c }}.name_2" />
                                                </td>
                                            @elseif($subawbs[$a][$c]['name_1'] == 3)
                                                <td class="w-1/3 px-3 py-2 border-r-2 border-gray-300 "
                                                    style="background-color: #F7F7F7; " wire:init="agentReference">
                                                    <select class="w-full rounded-md h-9" style="cursor: pointer;"
                                                        name="subawbs.{{ $a }}.{{ $c }}.name_2"
                                                        wire:model.defer='subawbs.{{ $a }}.{{ $c }}.name_2'>
                                                        <option value="">Select</option>
                                                        @foreach ($agentReferences as $agentReference)
                                                            <option value="{{ $agentReference->id }}">
                                                                {{ $agentReference->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    <x-input-error class="text-xs"
                                                        for="subawbs.{{ $a }}.{{ $c }}.name_2" />
                                                </td>
                                            @elseif($subawbs[$a][$c]['name_1'] == 4)
                                                <td class="w-1/3 px-3 py-2 border-r-2 border-gray-300 "
                                                    style="background-color: #F7F7F7; "wire:init="omReference">
                                                    <select class="w-full rounded-md h-9" style="cursor: pointer;"
                                                        name="subawbs.{{ $a }}.{{ $c }}.name_2"
                                                        wire:model.defer='subawbs.{{ $a }}.{{ $c }}.name_2'>
                                                        <option value="">Select</option>
                                                        @foreach ($omReferences as $omReference)
                                                            <option value="{{ $omReference->id }}">
                                                                {{ $omReference->first_name }}
                                                                {{ $omReference->middle_name }}
                                                                {{ $omReference->last_name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    <x-input-error class="text-xs"
                                                        for="subawbs.{{ $a }}.{{ $c }}.name_2" />
                                                </td>
                                            @elseif($subawbs[$a][$c]['name_1'] == 5)
                                                <td class="w-1/3 px-3 py-2 border-r-2 border-gray-300 "
                                                    style="background-color: #F7F7F7; "wire:init="flsReference">
                                                    <select class="w-full rounded-md h-9" style="cursor: pointer;"
                                                        name="subawbs.{{ $a }}.{{ $c }}.name_2"
                                                        wire:model.defer='subawbs.{{ $a }}.{{ $c }}.name_2'>
                                                        <option value="">Select</option>
                                                        @foreach ($flsReferences as $flsReference)
                                                            <option value="{{ $flsReference->id }}">
                                                                {{ $flsReference->first_name }}
                                                                {{ $flsReference->middle_name }}
                                                                {{ $flsReference->last_name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    <x-input-error class="text-xs"
                                                        for="subawbs.{{ $a }}.{{ $c }}.name_2" />
                                                </td>
                                            @endif
                                        @else
                                            <td class="w-1/3 px-3 py-2 border-r-2 border-gray-300 "
                                                style="background-color: #F7F7F7; " wire:init="">
                                                <select disabled class="w-full rounded-md h-9"
                                                    style="cursor: pointer;"
                                                    name="subawbs.{{ $a }}.{{ $c }}.name_2"
                                                    wire:model.defer='subawbs.{{ $a }}.{{ $c }}.name_2'>
                                                    <option value="">Select</option>
                                                </select>
                                                <x-input-error class="text-xs"
                                                    for="subawbs.{{ $a }}.{{ $c }}.name_2" />
                                            </td>
                                        @endif

                                        <td class="w-1/3 px-3 py-2 " style="background-color: #F7F7F7; "
                                            wire:init="branchReference">
                                            <select class="w-full rounded-md h-9" style="cursor: pointer;"
                                                name="subawbs.{{ $a }}.{{ $c }}.branch"
                                                wire:model.defer='subawbs.{{ $a }}.{{ $c }}.branch'>
                                                <option value="">Select</option>
                                                @foreach ($branchReferences as $branchReference)
                                                    <option value="{{ $branchReference->id }}">
                                                        {{ $branchReference->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <x-input-error class="text-xs"
                                                for="subawbs.{{ $a }}.{{ $c }}.branch" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    @endforeach
                @endif
                <?php $b++; ?>


                <?php $i++; ?>
            @endforeach
        </div>

        <div class="flex justify-end gap-3 mt-6 space-x-3">
            <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                class="px-12 bg-white text-blue hover:bg-gray-100" />
            <x-button type="submit" title="Submit" class="px-12 bg-blue text-white hover:bg-[#002161]" />
        </div>
    </form>
</div>
