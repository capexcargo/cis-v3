<div x-data="{
    {{-- confirmation_modal: '{{ $confirmation_modal }}', --}}
}">
    <x-loading></x-loading>

    {{-- <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure yous want to save this new Area?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal> --}}

        <div class="grid grid-cols-12 ">
            <div class="flex col-span-12 space-x-2 font-medium">
                <span>
                    View
                </span>
                <span class="text-blue-800">
                    {{ $wstart }} -
                    {{ $wend }}
                </span>
            </div>
            <div class="flex col-span-12 mt-2 space-x-4 text-xs ">
                <span class="text-gray-500">
                    <table>
                        <tr>
                            <td>Issued to :</td>
                        </tr>
                        <tr>
                            <td>Branch</td>
                        </tr>
                        <tr>
                            <td>Issuance Date</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                        </tr>
                        <tr>
                            <td>Closed Date</td>
                        </tr>
                    </table>
                </span>
                <span class="font-semibold">
                    <table>
                        <tr>
                            <td>{{ $isto }}</td>
                        </tr>
                        <tr>
                            <td>{{ $branch }}</td>
                        </tr>
                        <tr>
                            <td>{{ date('m/d/Y', strtotime($isdate)) }}</td>
                        </tr>
                        <tr>
                            <td>
                                {{ $stat == 1 ? 'OPEN' : 'CLOSE' }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                @if (isset($waybill_list->issuance_date))
                                    {{ date('m/d/Y', strtotime($cldate)) }}
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                    </table>
                </span>
            </div>

            <div class="col-span-12 ">
                
                <div class="flex justify-between">
                    <ul class="flex justify-start mt-8" wire:init="loadviewheaders">
                        @foreach ($status_view_header_cards as $i => $status_card)
                            <li
                                class="px-2 py-1 text-xs  {{ $status_view_header_cards[$i]['class'] }} {{ $viewstats == $status_view_header_cards[$i]['id'] ? 'bg-blue text-white' : 'text-gray-400' }}">
                                <button
                                    wire:click="$set('{{ $status_view_header_cards[$i]['action'] }}', {{ $status_view_header_cards[$i]['id'] }})">
                                    {{ $status_view_header_cards[$i]['title'] }}
                                    <span class="ml-4 text-xs border-0 {{ $status_view_header_cards[$i]['class'] }} {{ $viewstats == $status_view_header_cards[$i]['id'] ? 'bg-blue text-white' : 'text-gray-400' }}">{{ $status_view_header_cards[$i]['value'] }}</span>
                                </button>
                            </li>
                        @endforeach
                    </ul>
                    <div class="relative flex ml-4 justify-items-end" style="margin-top:2.3%; margin-right:-1.5%;">
                        <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <svg class="w-3 h-3 -mt-2 text-gray-500 dark:text-gray-400" aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                            </svg>
                        </div>
                        <input type="search" id="default-search"
                            class="block w-11/12 h-3 p-3 pl-8 text-xs text-gray-900 border border-gray-800 rounded-sm focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder="Search Waybill No." wire:model="wb" name="wb">
                    </div>
                </div>
                    
                <div class="bg-white rounded-lg shadow-md ">
                    <table
                        class="min-w-full overflow-auto max-h-[100px] border divide-y divide-gray-200 border-gray-50">
                        <thead>
                            <tr>
                                <th class="px-4 py-2 text-[10px] font-medium tracking-wider text-center">
                                    No.</th>
                                <th class="px-4 py-2 text-[10px] font-medium tracking-wider text-center">
                                    Waybill No</th>
                                <th class="px-4 py-2 text-[10px] font-medium tracking-wider text-center">
                                    Transaction Date</th>
                                <th class="px-4 py-2 text-[10px] font-medium tracking-wider text-center">
                                    Paymode</th>
                                <th class="px-4 py-2 text-[10px] font-medium tracking-wider text-center">
                                    Cargo Status</th>
                                <th class="px-4 py-2 text-[10px] font-medium tracking-wider text-center">
                                    Collection Status</th>
                                <th class="px-4 py-2 text-[10px] font-medium tracking-wider text-center">
                                    Waybill Utilization</th>
                            </tr>
                        </thead>
                        <tbody class="overflow-auto bg-gray-100 divide-y-4 divide-white">
                            {{-- @dd($waybill_views) --}}
                            @foreach ($waybill_views as $w => $waybill_list)
                                <tr>
                                    <td class="px-4 py-2 text-xs font-medium text-center whitespace-nowrap">
                                        {{ $w + 1 }}.
                                    </td>
                                    <td class="px-4 py-2 text-xs font-medium text-center whitespace-nowrap"
                                        style="">
                                        <a class="text-blue-800 underline cursor-pointer">
                                            {{ $waybill_list->waybill }}
                                        </a>
                                    </td>
                                    <td class="px-4 py-2 text-xs font-medium text-center whitespace-nowrap"
                                        style="">
                                        08/01/2023
                                    </td>
                                    <td class="px-4 py-2 text-xs font-medium text-center whitespace-nowrap">
                                        COD
                                    </td>
                                    <td class="px-4 py-2 text-xs font-medium text-center whitespace-nowrap">
                                        Delivered
                                    </td>
                                    <td class="px-4 py-2 text-xs font-medium text-center whitespace-nowrap">
                                        Collected
                                    </td>
                                    <td class="px-4 py-2 text-xs font-medium text-center whitespace-nowrap">
                                        Used
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>


</div>
