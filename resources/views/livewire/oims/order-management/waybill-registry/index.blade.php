<x-form wire:init="load" x-data="{
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    issue_modal: '{{ $issue_modal }}',
    view_modal: '{{ $view_modal }}',
}">

    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @if($create_modal)
        @can('oims_order_management_waybill_registry_add')
            <x-modal id="create_modal" size="w-2/5" hasClose="1">
                <x-slot name="title">Register AWB</x-slot>
                <x-slot name="body">
                    @livewire('oims.order-management.waybill-registry.create')
                </x-slot>
            </x-modal>
        @endcan
        @endif

        @if ($waybill_id && $edit_modal)
        @can('oims_order_management_waybill_registry_edit')
                <x-modal id="edit_modal" size="w-2/5" hasClose="1">
                    <x-slot name="title">Edit Registry</x-slot>
                    <x-slot name="body">
                        @livewire('oims.order-management.waybill-registry.edit', ['id' => $waybill_id])
                    </x-slot>
                </x-modal>
        @endcan
        @endif

        @if ($issue_id && $issue_modal)
        @can('oims_order_management_waybill_registry_issue')
                <x-modal id="issue_modal" size="w-2/5" hasClose="1">
                    <x-slot name="title">Issue Waybill Series</x-slot>
                    <x-slot name="body">
                        @livewire('oims.order-management.waybill-registry.issue', ['id' => $issue_id])
                    </x-slot>
                </x-modal>
        @endcan
        @endif

        @if ($view_id && $view_modal)
        @can('oims_order_management_waybill_registry_view')
                <x-modal id="view_modal" size="w-2/3" hasClose="1">
                    {{-- <x-slot name="title">view Waybill Series</x-slot> --}}
                    <x-slot name="body">
                        @livewire('oims.order-management.waybill-registry.view', ['id' => $view_id])
                    </x-slot>
                </x-modal>
        @endcan
        @endif

    </x-slot>

    <x-slot name="header_title">
        <div class="flex space-x-2">
            <div>
                Waybill Registry
            </div>
        </div>
    </x-slot>
    <x-slot name="header_button">
        @can('oims_order_management_waybill_registry_add')
            <button wire:click="action({}, 'add')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    Register
                </div>
            </button>
        @endcan
    </x-slot>

    <x-slot name="body">
        <div class="flex gap-4">
            <button disabled wire:click="cardHeader(1)"
                class="flex items-center justify-between gap-12 px-4 py-4 bg-white rounded-lg ">
                <h5 class="text-sm font-semibold text-gray-700 uppercase dark:text-gray-400">Pads for<br>Issuance</h5>
                <h5 class="text-2xl font-semibold text-gray-700 dark:text-gray-400">{{ $pfiCount }}
                </h5>
            </button>
            <button disabled wire:click="cardHeader(2)"
                class="flex items-center justify-between gap-12 px-4 py-4 bg-white rounded-lg ">
                <h5 class="text-sm font-semibold text-gray-700 uppercase dark:text-gray-40">Issued Pads</h5>
                <h5 class="text-2xl font-semibold text-gray-700 dark:text-gray-400">{{ $ipCount }}
                </h5>
            </button>

            <button disabled wire:click="cardHeader(3)"
                class="flex items-center justify-between gap-10 px-4 py-4 bg-white rounded-lg ">
                <h5 class="text-sm font-semibold text-left text-gray-700 uppercase dark:text-gray-400">Used<br>Waybills
                </h5>
                <h5 class="text-2xl font-semibold text-gray-700 dark:text-gray-400">{{ $usedCount }}
                </h5>
            </button>
            <button disabled wire:click="cardHeader(3)"
                class="flex items-center justify-between gap-10 px-4 py-4 bg-white rounded-lg ">
                <h5 class="text-sm font-semibold text-left text-gray-700 uppercase dark:text-gray-400">
                    Unused<br>Waybills</h5>
                <h5 class="text-2xl font-semibold text-gray-700 dark:text-gray-400">{{ $unusedCount }}
                </h5>
            </button>
            <button disabled wire:click="cardHeader(3)"
                class="flex items-center justify-between gap-10 px-4 py-4 bg-white rounded-lg ">
                <h5 class="text-sm font-semibold text-left text-gray-700 uppercase dark:text-gray-400">
                    Cancelled<br>Waybills</h5>
                <h5 class="text-2xl font-semibold text-gray-700 dark:text-gray-400">{{ $cancelledCount }}
                </h5>
            </button>
        </div>

        <div class="grid grid-cols-7 text-sm" style="margin-top: 4%;">
            <div class="w-4/5">
                <div>
                    <x-label class="text-black" for="from" value="Series" />
                    <x-transparent.input type="text" label="From" name="from" wire:model.defer="from" />
                </div>
            </div>
            <div class="w-4/5">
                <div>
                    <x-label style="color: #F3F4F6;" value="'" />
                    <x-transparent.input type="text" label="To" name="to" wire:model.defer="to" />
                </div>
            </div>
            <div class="w-4/5">
                <x-label class="text-black" value="Issued To" />
                <x-transparent.select value="" label="" name="isto" wire:model="isto">
                    <option value="">Select</option>
                    <option value="1">Checker</option>
                    <option value="4">OM</option>
                    <option value="5">FLS</option>
                    <option value="2">Customer</option>
                    <option value="3">Agent</option>
                </x-transparent.select>
            </div>
            <div class="w-4/5">
                <x-label style="color: #F3F4F6;" value="'" />
                @if ($isto == 1)
                    <x-transparent.select value="" label="" name="istoname"
                        wire:model.defer="istoname" wire:init="empReference">
                        <option value="">Name</option>
                        @foreach ($empReferences as $empReference)
                            <option value="{{ $empReference->id }}">
                                {{ $empReference->first_name }}
                                {{ $empReference->middle_name }}
                                {{ $empReference->last_name }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                @elseif ($isto == 2)
                    <x-transparent.select value="" label="" name="istoname" wire:model.defer="istoname"
                        wire:init="custReference">
                        <option value="">Name</option>
                        @foreach ($custReferences as $custReference)
                            <option value="{{ $custReference->id }}">
                                {{ $custReference->fullname }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                @elseif ($isto == 3)
                    <x-transparent.select value="" label="" name="istoname" wire:model.defer="istoname"
                        wire:init="agentReference">
                        <option value="">Name</option>
                        @foreach ($agentReferences as $agentReference)
                            <option value="{{ $agentReference->id }}">
                                {{ $agentReference->name }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                @elseif ($isto == 4)
                    <x-transparent.select value="" label="" name="istoname" wire:model.defer="istoname"
                        wire:init="omReference">
                        <option value="">Name</option>
                        @foreach ($omReferences as $omReference)
                            <option value="{{ $omReference->id }}">
                                {{ $omReference->first_name }}
                                {{ $omReference->middle_name }}
                                {{ $omReference->last_name }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                @elseif ($isto == 5)
                    <x-transparent.select value="" label="" name="istoname" wire:model.defer="istoname"
                        wire:init="flsReference">
                        <option value="">Name</option>
                        @foreach ($flsReferences as $flsReference)
                            <option value="{{ $flsReference->id }}">
                                {{ $flsReference->first_name }}
                                {{ $flsReference->middle_name }}
                                {{ $flsReference->last_name }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                @else
                    <x-transparent.select value="" label="" name="istoname" wire:model.defer="istoname"
                        wire:init="">
                        <option value="">Name</option>
                    </x-transparent.select>

                @endif

            </div>
            <div class="w-4/5">
                <x-label style="color:black;" value="Issuance Date" />
                <x-transparent.input style="font-size: 14px;" type="text" onfocus="(this.type='date')"
                    onblur="(this.type='text')" label="" placeholder="MM-DD-YYYY" name="idate"
                    wire:model.defer="idate" />
            </div>
            <div class="w-4/5">
                <x-label style="color:black;" value="Status" />
                <x-transparent.select value="" label="" name="status" wire:model.defer="status">
                    <option value="">Select</option>
                    <option value="1">Open</option>
                    <option value="4">Close</option>
                </x-transparent.select>
            </div>
            <div class="">
                <x-label style="color: #F3F4F6;" value="'" />
                <x-button type="button" wire:click="search" title="Search"
                    class="px-2 py-1 mt-2 text-white bg-blue hover:bg-blue-800" />
            </div>
        </div>
        <div class="">
            <div class="flex items-center justify-start">
                <ul class="flex mt-8" wire:init="loadheaders">
                    @foreach ($status_header_cards as $i => $status_card)
                        <li
                            class="px-3 py-1 text-xs {{ $status_header_cards[$i]['class'] }} {{ $stats == $status_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                            <button
                                wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                {{ $status_header_cards[$i]['title'] }}
                                <span class="ml-6 text-sm">{{ $status_header_cards[$i]['value'] }}</span>
                            </button>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                        <tr>
                            <th
                                class="px-4 py-1 text-[10px] font-medium tracking-wider text-left text-gray-500 uppercase bg-gray-50 border-r-2 border-gray-300">
                            </th>
                            <th colspan="7"
                                class="px-4 py-1 text-[10px] font-medium tracking-wider text-center text-white uppercase"
                                style="background-color: #A1A1A1">
                                ISSUANCE DETAILS</th>
                            <th
                                class="px-4 py-1 text-[10px] font-medium tracking-wider text-left text-gray-500 uppercase bg-gray-50">
                            </th>
                        </tr>
                        <tr>
                            <th
                                class="px-4 py-1 text-[10px] font-medium tracking-wider text-center text-gray-500 uppercase bg-gray-50 border-r-2 border-gray-300">
                                AWB Series</th>
                            <th colspan="2"
                                class="px-4 py-1 text-[10px] font-medium tracking-wider text-center text-gray-500 uppercase bg-gray-50 border-r-2 border-gray-300">
                                Issued to</th>
                            <th
                                class="px-4 py-1 text-[10px] font-medium tracking-wider text-center text-gray-500 uppercase bg-gray-50">
                                Branch</th>
                            <th
                                class="px-4 py-1 text-[10px] font-medium tracking-wider text-center text-gray-500 uppercase bg-gray-50">
                                Issuance Date</th>
                            <th
                                class="px-4 py-1 text-[10px] font-medium tracking-wider text-center text-gray-500 uppercase bg-gray-50">
                                Status</th>
                            <th
                                class="px-4 py-1 text-[10px] font-medium tracking-wider text-center text-gray-500 uppercase bg-gray-50">
                                Closed Date</th>
                            <th
                                class="px-4 py-1 text-[10px] font-medium tracking-wider text-center text-gray-500 uppercase bg-gray-50 border-r-2 border-gray-300">
                                Waybill Utilization</th>
                            <th
                                class="px-4 py-1 text-[10px] font-medium tracking-wider text-center text-gray-500 uppercase bg-gray-50">
                                Action</th>
                        </tr>
                    </thead>
                    <tbody class="bg-gray-100 divide-y-4 divide-white">
                        @foreach ($waybill_lists as $w => $waybill_list)
                            <tr>
                                <td
                                    class="px-4 py-2 text-xs font-medium text-center border-r-2 border-gray-300 whitespace-nowrap">
                                    {{ $waybill_list->waybill_start }}<br>{{ $waybill_list->waybill_end }}</td>
                                <td class="px-4 py-2 text-xs font-medium text-center border-r-2 border-gray-300 whitespace-nowrap"
                                    style="min-width: 100px">
                                    {{ $waybill_list->issued_to == 1
                                        ? 'Checker'
                                        : ($waybill_list->issued_to == 2
                                            ? 'Customer'
                                            : ($waybill_list->issued_to == 3
                                                ? 'Agent'
                                                : ($waybill_list->issued_to == 4
                                                    ? 'OM'
                                                    : ($waybill_list->issued_to == 5
                                                        ? 'FLS'
                                                        : '-')))) }}
                                </td>
                                <td class="px-4 py-2 text-xs font-medium text-center border-r-2 border-gray-300 whitespace-nowrap"
                                    style="min-width: 100px">
                                    {{ $waybill_list->issued_to == 1
                                        ? $waybill_list->waybillEmpUser->first_name . ' ' . $waybill_list->waybillEmpUser->last_name
                                        : ($waybill_list->issued_to == 2
                                            ? $waybill_list->waybillCustInfo->fullname
                                            : ($waybill_list->issued_to == 3
                                                ? $waybill_list->waybillAgent->name
                                                : ($waybill_list->issued_to == 4
                                                    ? $waybill_list->waybillOmUser->first_name . ' ' . $waybill_list->waybillOmUser->last_name
                                                    : ($waybill_list->issued_to == 5
                                                        ? $waybill_list->waybillFlsUser->first_name . ' ' . $waybill_list->waybillFlsUser->last_name
                                                        : '-')))) }}
                                </td>
                                <td class="px-4 py-2 text-xs font-medium text-center whitespace-nowrap">
                                    {{ $waybill_list->waybillBranch->name ?? '-' }}
                                </td>
                                <td class="px-4 py-2 text-xs font-medium text-center whitespace-nowrap">
                                    @if (isset($waybill_list->issuance_date))
                                        {{ date('m/d/Y', strtotime($waybill_list->issuance_date)) }}
                                    @else
                                        -
                                    @endif

                                </td>
                                <td class="px-4 py-2 text-xs font-medium text-center whitespace-nowrap">
                                    <select class="w-11/12 h-8 text-xs text-left rounded-md" style="cursor: pointer;">
                                        @if ($waybill_list->status == 1)
                                            <option class="text-xs" value="1" selected>OPEN
                                            </option>
                                            <option class="text-xs" value="2">CLOSE
                                            </option>
                                        @elseif($waybill_list->status == 2)
                                            <option class="text-xs" value="1">OPEN
                                            </option>
                                            <option class="text-xs" value="2" selected>CLOSE
                                            </option>
                                        @endif
                                    </select>
                                </td>
                                <td class="px-4 py-2 text-xs font-medium text-center whitespace-nowrap">
                                    @if (isset($waybill_list->closed_date))
                                        {{ date('m/d/Y', strtotime($waybill_list->closed_date)) }}
                                    @else
                                        -
                                    @endif

                                </td>
                                <td
                                    class="px-4 py-2 text-xs font-medium border-r-2 border-gray-300 items-center-center whitespace-nowrap">
                                    <span class="flex justify-center space-x-6 text-left">
                                        <span class="">
                                            <p>Unused : </p>
                                            <p>Used : </p>
                                            <p>Cancelled : </p>
                                        </span>
                                        <span class="">
                                            <p class="text-blue-700 cursor-pointer">20</p>
                                            <p class="text-blue-700 cursor-pointer">0</p>
                                            <p class="text-blue-700 cursor-pointer">20</p>
                                        </span>
                                    </span>
                                </td>
                                <td class="relative p-3 px-4 py-2 text-xs font-medium text-center whitespace-nowrap"
                                    style=""x-data="{ open: false }">
                                    <svg @click="open = !open"
                                        class="w-5 h-5 ml-4 text-center text-gray-400 cursor-pointer hover:text-blue-800 "
                                        data-prefix="far" data-icon="approve" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 512">
                                        <path fill="currentColor"
                                            d="M64 360C94.93 360 120 385.1 120 416C120 446.9 94.93 472 64 472C33.07 472 8 446.9 8 416C8 385.1 33.07 360 64 360zM64 200C94.93 200 120 225.1 120 256C120 286.9 94.93 312 64 312C33.07 312 8 286.9 8 256C8 225.1 33.07 200 64 200zM64 152C33.07 152 8 126.9 8 96C8 65.07 33.07 40 64 40C94.93 40 120 65.07 120 96C120 126.9 94.93 152 64 152z">
                                        </path>
                                    </svg>
                                    <div class="absolute border border-gray-300 shadow-md cursor-pointer"
                                        style=" margin-left:-3rem; margin-top:-100%">
                                        <ul class="text-left text-gray-600 bg-white rounded shadow" x-show="open"
                                            @click.away="open = false">
                                            <li class="px-4 py-2 hover:bg-indigo-100 " x-data="{ open: false }">
                                                @can('oims_order_management_waybill_registry_view')
                                                    <a
                                                        wire:click="action({'id': {{ $waybill_list->id }}},'view') }}">View</a>
                                                @endcan
                                            </li>
                                            <li class="px-4 py-2 hover:bg-indigo-100 " x-data="{ open: false }">
                                                @can('oims_order_management_waybill_registry_edit')
                                                    <a
                                                        wire:click="action({'id': {{ $waybill_list->id }}},'edit') }}">Edit</a>
                                                @endcan
                                            </li>
                                            <li class="px-4 py-2 hover:bg-indigo-100 " x-data="{ open: false }">
                                                @can('oims_order_management_waybill_registry_issue')
                                                    <a
                                                        wire:click="action({'id': {{ $waybill_list->id }}},'issue') }}">Issue</a>
                                                @endcan
                                            </li>

                                        </ul>
                                    </div>
                                </td>
                                {{-- </td> --}}
                            </tr>
                        @endforeach

                    </tbody>
                </table>


                <div class="px-1 pb-2">
                    {{-- {{ $area_names->links() }} --}}
                </div>
            </div>
        </div>
    </x-slot>

</x-form>
