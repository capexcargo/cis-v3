<x-form wire:init="" x-data="{
    {{-- create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    reactivate_modal: '{{ $reactivate_modal }}',
    deactivate_modal: '{{ $deactivate_modal }}', --}}
    edtrr_modal: '{{ $edtrr_modal }}',
        pua_modal: '{{ $pua_modal }}',
        da_modal: '{{ $da_modal }}',
        ac_modal: '{{ $ac_modal }}',
        pv_modal: '{{ $pv_modal }}',
}">

    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @if ($edtrr_modal)
            <x-modal id="edtrr_modal" size="w-11/12">
                <x-slot name="body">
                    <div class="grid grid-cols-12 px-4">
                        <div class="flex col-span-6">
                            <div class="mt-1 text-base font-medium text-gray-500 ">e-DTR Reference Number :</div>
                            <div class="ml-1 text-lg font-semibold text-blue-800 capitalize">{{ $rmrefno }}</div>
                        </div>
                        <div class="flex justify-end col-span-6 pr-10">
                            <button wire:click="actionpv({'id':{{ $Edtrid }}}, 'pv')"
                                class="flex items-center justify-center px-6 py-2 text-sm text-[#8d8d8d] bg-gray-100 border border-gray-600 rounded hover:bg-blue-800 hover:text-white">

                                <svg class="mr-4" xmlns="http://www.w3.org/2000/svg" width="15.214" height="15.214"
                                    viewBox="0 0 15.214 15.214">
                                    <g id="Icon_feather-printer" data-name="Icon feather-printer"
                                        transform="translate(0.75 0.75)">
                                        <path id="Path_7" data-name="Path 7" d="M9,7.8V3h8.229V7.8"
                                            transform="translate(-6.257 -3)" fill="none" stroke="currentColor"
                                            stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                        <path id="Path_8" data-name="Path 8"
                                            d="M5.743,19.671H4.371A1.371,1.371,0,0,1,3,18.3V14.871A1.371,1.371,0,0,1,4.371,13.5H15.343a1.371,1.371,0,0,1,1.371,1.371V18.3a1.371,1.371,0,0,1-1.371,1.371H13.971"
                                            transform="translate(-3 -8.7)" fill="none" stroke="currentColor"
                                            stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                        <path id="Path_9" data-name="Path 9" d="M9,21h8.229v5.486H9Z"
                                            transform="translate(-6.257 -12.771)" fill="none" stroke="currentColor"
                                            stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    </g>
                                </svg>
                                Print
                            </button>
                        </div>

                        <div class="flex col-span-3 mt-2">
                            <div>
                                <table class="">
                                    <tr>
                                        <td class="text-xs text-gray-400">Date Created :</td>
                                        <td class="pl-4 text-sm font-medium">{{ $rmcreated }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-xs text-gray-400">Date of Dispatch :</td>
                                        <td class="pl-4 text-sm font-medium">{{ $rmdisp }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-xs text-gray-400">Branch :</td>
                                        <td class="pl-4 text-sm font-medium">{{ $rmbrnch }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-xs text-gray-400">Vehicle Plate Number :</td>
                                        <td class="pl-4 text-sm font-medium">{{ $rmplate }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="flex col-span-3 mt-2">
                            <div>
                                <table class="">
                                    <tr>
                                        <td class="text-xs text-gray-400">Team :</td>
                                        <td class="pl-4 text-sm font-medium">{{ $rmteam }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-xs text-gray-400">Driver :</td>
                                        <td class="pl-4 text-sm font-medium">{{ $rmdrv }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-xs text-gray-400">Checker 1 :</td>
                                        <td class="pl-4 text-sm font-medium">{{ $rmchk1 }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-xs text-gray-400">Checker 2 :</td>
                                        <td class="pl-4 text-sm font-medium">{{ $rmchk2 }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="col-span-12 px-6 text-base py-1 font-normal mt-8 text-white bg-[#003399] rounded-tl-lg rounded-tr-xl"
                            style="border-top-right-radius: 40px; width: 220px">
                            DELIVERY ACTIVITIES
                        </div>

                        <div class="col-span-12 bg-white border rounded-lg shadow-md dark">
                            <div class="w-full mx-auto">
                                <div class="w-full overflow-x-auto">
                                    <table class="w-full ">
                                        <thead>
                                            <tr>
                                                <th class="sticky left-0 py-1 text-xs font-medium tracking-wider text-left text-gray-500 bg-white border-r-2 border-gray-300 "
                                                    colspan="2">
                                                </th>
                                                <th class="px-3 py-1 text-xs font-medium tracking-wider text-left text-gray-500"
                                                    colspan="14">
                                                </th>
                                                <th class="px-3 py-1 text-xs font-medium tracking-wider text-center text-black border-r border-black"
                                                    style="background-color: #E0E0E0;" colspan="4">
                                                    Actual Truck Utilization</th>
                                                <th class="px-3 py-1 text-xs font-medium tracking-wider text-center"
                                                    colspan="6">
                                                </th>
                                            </tr>
                                            <tr>
                                                <th
                                                    class="sticky left-0 pl-6 py-2 text-[10px] font-normal tracking-wider text-left text-black  bg-white">
                                                    No.</th>
                                                <th class="sticky bg-white pr-4  py-2 text-left text-[10px] font-normal text-black  tracking-wider border-r-2 border-gray-300"
                                                    style=" left:100px;">
                                                    Waybill Number</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal whitespace-nowrap tracking-wider text-left text-black ">
                                                    Origin</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-left text-black ">
                                                    Destination</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-left text-black  ">
                                                    Shipper</th>
                                                <th
                                                    class="text-center px-3 py-2 text-[10px] font-normal tracking-wider text-black ">
                                                    Consignee</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Delivery Address</th>
                                                <th
                                                    class=" px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Transaction Date</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Delivery Status</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Actual Delivery Date</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Ageing</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Transport Mode</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center ">
                                                    Paymode</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center ">
                                                    Amount for Collection</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black">
                                                    Collection Status</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black border-r border-black ">
                                                    Amount Collected</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Total Qty</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Actual Weight (kgs)</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Actual Volume (tons)</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black border-r border-black">
                                                    CWT (for AF)</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Real-Time Scanning Performance</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] text-[#003399] font-normal tracking-wider text-center text-black ">
                                                    Estimated Execution Time</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] text-[#003399] font-normal tracking-wider text-center text-black ">
                                                    Actual Execution Time</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black">
                                                    Remarks</th>
                                            </tr>
                                        </thead>
                                        <tbody class="divide-y-4 divide-white ">

                                            {{-- @foreach ($getEDtrs as $i => $getEDtr)
                                            @foreach ($getEDtrs[$i]['edtrDetails'] as $o => $getdet)
                                            @if ($getEDtrs[$i]['edtrDetails'][$o]['edtr_item_type'] == 1)
                                                
                                            @endif
                                            @endforeach
                                        @endforeach --}}

                                        @foreach ($getEDtrs as $i => $getEDtr)
                                            @foreach ($getEDtrs[$i]['edtrDetails'] as $o => $getEDtrDet)
                                            @if ($getEDtrs[$i]['edtrDetails'][$o]['edtr_item_type'] == 2)
                                            {{-- @dd($getEDtrDet) --}}
                                            <tr>
                                                <td class="sticky left-0 py-2 pl-6 text-xs font-medium border-b border-gray-200 whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:100px;">
                                                    {{ $o + 1 }}
                                                </td>
                                                <td class="sticky py-2 pr-4 text-xs font-medium text-[#003399] underline border-b border-r-2 border-gray-300  whitespace-nowrap"
                                                    style=" left:100px; background-color:  #F7F7F7; min-width:120px;">
                                                    <span class="cursor-pointer" {{-- wire:click="actionedtrr({}, 'edtrr')" --}}>
                                                        {{$getEDtrDet['edtrtransaction']['waybill']}}
                                                    </span>
                                                </td>
                                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                    style="background-color: #F7F7F7;">{{$getEDtrDet['edtrtransaction']['OriginReference']['code']}}
                                                </td>
                                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                    style="background-color: #F7F7F7;">{{$getEDtrDet['edtrtransaction']['DestinationReference']['code']}}</td>
                                                <td class="px-3 py-2 text-xs font-medium"
                                                    style="background-color: #F7F7F7; min-width:100px;">
                                                    {{$getEDtrDet['edtrtransaction']['ShipperReference']['fullname']}}</td>
                                                <td class="px-3 py-2 text-xs font-medium whitespace-nowrap"
                                                    style="background-color: #F7F7F7;">{{$getEDtrDet['edtrtransaction']['ConsigneeReference']['fullname']}}</td>
                                                <td class="px-3 py-2 text-xs font-medium text-left"
                                                    style="background-color: #F7F7F7; min-width:180px;">{{$getEDtrDet['edtrtransaction']['consignee_address']}}</td>
                                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:120px;">
                                                    {{ date('m/d/Y', strtotime($getEDtrDet['edtrtransaction']['transaction_date'])) }}
                                                </td>
                                                <td class="px-3 py-2 text-xs font-medium text-center text-[#18EB00] whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:120px;">{{$getEDtrDet['edtrtransaction']['transaction_date'] != null ? 'Completed' : ''}}</td>
                                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:140px;">07/20/22</td>
                                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:100px;"> 
                                                    {{ date_diff(date_create($getEDtrDet['edtrtransaction']['transaction_date']), date_create(date('Y-m-d')))->format('%a Day/s') }}
                                                </td>
                                                <td class="px-3 py-2 text-xs font-medium text-center uppercase whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:120px;">{{$getEDtrDet['edtrtransaction']['TransportModeReference']['name']}}</td>
                                                <td class="px-3 py-2 text-xs font-medium text-center uppercase whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:100px;">{{$getEDtrDet['edtrtransaction']['CrmBookingReference']['BookingConsigneeHasManyBK'][0]['ModeOfPaymenReferenceCD']['name']}} </td>
                                                <td class="px-3 py-2 text-xs font-medium text-right whitespace-nowrap"
                                                    style="padding-right:20px; background-color: #F7F7F7; min-width:160px;">
                                                    500.00</td>
                                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:120px;">Collected</td>
                                                <td class="px-3 py-2 text-xs font-medium text-right border-r border-black whitespace-nowrap"
                                                    style="padding-right:20px; background-color: #F7F7F7; min-width:140px;">
                                                    400.00
                                                </td>

                                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:100px;">2</td>
                                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:120px;">45</td>
                                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:120px;">0.55</td>
                                                <td class="px-3 py-2 text-xs font-medium text-center border-r border-black whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:70px;">45</td>
                                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:140px;">100%</td>
                                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap text-[#003399]"
                                                    style="background-color: #F7F7F7; min-width:130px;">0:15:00</td>
                                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap text-[#003399]"
                                                    style="background-color: #F7F7F7; min-width:130px;">0:15:00</td>
                                                <td class="px-3 py-2 text-xs font-medium text-left"
                                                    style="background-color: #F7F7F7; min-width:135px;">{{$getEDtrDet['edtrtransaction']['CrmBookingReference']['remarks']}}</td>
                                            </tr>

                                            @endif
                                            @endforeach
                                        @endforeach
                                            
                                            <tr class="">
                                                <td class="sticky left-0 z-20 py-4 text-xs font-medium tracking-wider text-left text-gray-500 bg-white border-r-2 border-gray-300 "
                                                    colspan="2">
                                                    <div class="border-r border-black p-full "></div>

                                                </td>
                                                <td colspan="12"
                                                    class="text-xs font-medium border-t-2 border-b border-l border-black whitespace-nowrap"
                                                    style="background-color: ; min-height: full;">
                                                    <div class="border-t border-b border-l border-black"
                                                        style="background-color: ; min-height: 62px;">
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-right text-blue-900 border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        TOTAL:
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-right border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        1,800.00
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        66
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        580
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        10.10
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        300
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        100%
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        2:00:00
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        2:00:00
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-r border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="border-t border-b border-r border-black "
                                                        style="background-color: ; min-height: 62px;">
                                                    </div>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <div class="col-span-12 px-6 text-base py-1 font-normal mt-12 text-white bg-[#003399] rounded-tl-lg rounded-tr-xl"
                            style="border-top-right-radius: 40px; width: 220px">
                            PICK UP ACTIVITIES
                        </div>

                        <div class="col-span-12 bg-white border rounded-lg shadow-md dark">
                            <div class="w-full mx-auto">
                                <div class="w-full overflow-x-auto">
                                    <table class="w-full ">
                                        <thead>
                                            <tr>
                                                <th class="sticky left-0 py-1 text-xs font-medium tracking-wider text-left text-gray-500 bg-white border-r-2 border-gray-300 "
                                                    colspan="2">
                                                </th>
                                                <th class="px-3 py-1 text-xs font-medium tracking-wider text-left text-gray-500"
                                                    colspan="9">
                                                </th>
                                                <th class="px-3 py-1 text-xs font-medium tracking-wider text-center text-black border-r border-black"
                                                    style="background-color: #E0E0E0;" colspan="7">
                                                    Actual Truck Utilization</th>
                                                <th class="px-3 py-1 text-xs font-medium tracking-wider text-center"
                                                    colspan="4">
                                                </th>
                                            </tr>
                                            <tr>
                                                <th
                                                    class="sticky left-0 pl-6 py-2 text-[10px] font-normal tracking-wider text-left text-black  bg-white">
                                                    No.</th>
                                                <th class="sticky bg-white pr-4  py-2 text-left text-[10px] font-normal text-black  tracking-wider border-r-2 border-gray-300"
                                                    style=" left:100px;">
                                                    Booking Reference No.</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal whitespace-nowrap tracking-wider text-left text-black ">
                                                    Shipper</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-left text-black ">
                                                    Pick up Address</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-left text-black  ">
                                                    Pick up Date</th>
                                                <th
                                                    class="text-center px-3 py-2 text-[10px] font-normal tracking-wider text-black ">
                                                    Booking Status</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Actual Pick up Date</th>
                                                <th
                                                    class=" px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Ageing</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Amount for Collection</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Collection Status</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black border-r border-black">
                                                    Amount Collected</th>

                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Total No of WBs Created</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center ">
                                                    AF ACtual Weight (kgs)</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center ">
                                                    AF CWT (kgs)</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black">
                                                    SF Actual Weight (kgs)</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    SF Volume (CBM)</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Total Actual Wt (kgs)</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black border-r border-black">
                                                    Total Volume (tons)</th>

                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Real-Time Scanning Performance</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Estimated Execution Time</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                                    Actual Execution Time</th>
                                                <th
                                                    class="px-3 py-2 text-[10px] text-[#003399] font-normal tracking-wider text-center text-black ">
                                                    Remarks</th>
                                            </tr>
                                        </thead>
                                        <tbody class="divide-y-4 divide-white ">

                                            {{-- @foreach ($getEDtrs[0]['edtrDetails'] as $r => $getEDtr) --}}
                                            @foreach ($getEDtrs as $i => $getEDtr)
                                                @foreach ($getEDtrs[$i]['edtrDetails'] as $o => $getEDtrDet)
                                                    @if ($getEDtrs[$i]['edtrDetails'][$o]['edtr_item_type'] == 1)
                                                        <tr>
                                                            <td class="sticky left-0 py-2 pl-6 text-xs font-medium border-b border-gray-200 whitespace-nowrap"
                                                                style="background-color: #F7F7F7; min-width:100px;">
                                                                {{ $o + 1 }}
                                                            </td>
                                                            <td class="sticky py-2 pr-4 text-xs font-medium text-[#003399] underline border-b border-r-2 border-gray-300  whitespace-nowrap"
                                                                style=" left:100px; background-color:  #F7F7F7; min-width:120px;">
                                                                <span class="cursor-pointer" {{-- wire:click="actionedtrr({}, 'edtrr')" --}}>
                                                                    {{-- {{$getEDtr['edtrDetails'][$r]['edtrbook']['booking_reference_no']}} --}}
                                                                    {{ $getEDtrDet['edtrbook']['booking_reference_no'] }}
                                                                </span>
                                                            </td>
                                                            <td class="px-3 py-2 text-xs font-medium whitespace-nowrap"
                                                                style="background-color: #F7F7F7;">
                                                                {{ $getEDtrDet['edtrbook']['BookShipper'][0]['name'] }}
                                                            </td>
                                                            <td class="px-3 py-2 text-xs font-medium"
                                                                style="background-color: #F7F7F7;  min-width:180px;">
                                                                {{ $getEDtrDet['edtrbook']['BookShipper'][0]['address'] }}
                                                            </td>
                                                            <td class="px-3 py-2 text-xs font-medium"
                                                                style="background-color: #F7F7F7; min-width:120px;">
                                                                {{ date('m/d/Y', strtotime($getEDtrDet['edtrbook']['pickup_date'])) }}
                                                            </td>
                                                            <td class="px-3 py-2 text-xs font-medium whitespace-nowrap"
                                                                {{-- style="background-color: #F7F7F7; min-width:105px;"> --}}
                                                                style="background-color: #F7F7F7; min-width:105px; {{ $getEDtrDet['edtrbook']['final_status_id'] == 1
                                                                    ? 'color:#A775FF;'
                                                                    : ($getEDtrDet['edtrbook']['final_status_id'] == 2
                                                                        ? 'color:#A2C1FF;'
                                                                        : ($getEDtrDet['edtrbook']['final_status_id'] == 3
                                                                            ? 'color:#4D88FF;'
                                                                            : ($getEDtrDet['edtrbook']['final_status_id'] == 4
                                                                                ? 'color:#FF8800;'
                                                                                : ($getEDtrDet['edtrbook']['final_status_id'] == 5
                                                                                    ? 'color:#003399;'
                                                                                    : ($getEDtrDet['edtrbook']['final_status_id'] == 6
                                                                                        ? 'color:#FF7560;'
                                                                                        : ($getEDtrDet['edtrbook']['final_status_id'] == 7
                                                                                            ? 'color:#FF0000;'
                                                                                            : ($getEDtrDet['edtrbook']['final_status_id'] == 8
                                                                                                ? 'color:#32CD32;'
                                                                                                : ($getEDtrDet['edtrbook']['final_status_id'] == 9
                                                                                                    ? 'color:#FFD04F;'
                                                                                                    : ($getEDtrDet['edtrbook']['final_status_id'] == 10
                                                                                                        ? 'color:#FFD04F;'
                                                                                                        : ''))))))))) }} 
                                                            text-xs rounded-full p-1">
                                                                {{ $getEDtrDet['edtrbook']['FinalStatusReferenceBK']['name'] ?? '' }}
                                                                {{-- {{$getEDtrDet['edtrbook']['final_status_id']}} --}}
                                                            </td>
                                                            <td class="px-3 py-2 text-xs font-medium text-left"
                                                                style="background-color: #F7F7F7; min-width:130px;">
                                                                07/21/2022</td>
                                                            <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                                style="background-color: #F7F7F7; min-width:120px;">
                                                                {{ date_diff(date_create($getEDtrDet['edtrbook']['pickup_date']), date_create(date('Y-m-d')))->format('%a Day/s') }}
                                                            </td>
                                                            <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                                style="background-color: #F7F7F7; min-width:120px;">
                                                                0.00</td>
                                                            <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                                style="background-color: #F7F7F7; min-width:140px;">For
                                                                Collection</td>
                                                            <td class="px-3 py-2 text-xs font-medium text-center border-r border-black whitespace-nowrap"
                                                                style="background-color: #F7F7F7; min-width:120px;">
                                                                0.00</td>

                                                            <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                                style="background-color: #F7F7F7; min-width:100px;">0
                                                            </td>
                                                            <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                                style="background-color: #F7F7F7; min-width:100px;">0
                                                            </td>
                                                            <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                                style="padding-right:20px; background-color: #F7F7F7; min-width:80px;">
                                                                0</td>
                                                            <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                                style="background-color: #F7F7F7; min-width:100px;">0
                                                            </td>
                                                            <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                                style="padding-right:20px; background-color: #F7F7F7; min-width:100px;">
                                                                0
                                                            </td>
                                                            <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                                style="background-color: #F7F7F7; min-width:100px;">0
                                                            </td>
                                                            <td class="px-3 py-2 text-xs font-medium text-center border-r border-black whitespace-nowrap"
                                                                style="background-color: #F7F7F7; min-width:100px;">0
                                                            </td>

                                                            <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                                style="background-color: #F7F7F7; min-width:140px;">0
                                                            </td>
                                                            <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                                style="background-color: #F7F7F7; min-width:120px;">
                                                                0:00:00</td>
                                                            <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                                                style="background-color: #F7F7F7; min-width:140px;">
                                                                0:00:00%</td>
                                                            <td class="px-3 py-2 text-xs font-medium text-left"
                                                                style="background-color: #F7F7F7; min-width:135px;">
                                                                {{ $getEDtrDet['edtrbook']['remarks'] }}
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                            {{-- @endforeach --}}


                                            <tr class="">
                                                <td class="sticky left-0 z-20 py-4 text-xs font-medium tracking-wider text-left text-gray-500 bg-white border-r-2 border-gray-300 "
                                                    colspan="2">
                                                    <div class="border-r border-black p-full "></div>

                                                </td>
                                                <td colspan="5"
                                                    class="text-xs font-medium border-t-2 border-b border-l border-black whitespace-nowrap"
                                                    style="background-color: ; min-height: full;">
                                                    <div class="border-t border-b border-l border-black"
                                                        style="background-color: ; min-height: 62px;">
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-right text-blue-900 border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        TOTAL:
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-right border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        1,400.00
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">

                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        1,400.00
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        60
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        204
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        220
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        600
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        15.5
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        804
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        0.858
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        89.60%
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        1:45:00
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="px-3 py-4 border-t border-b border-black"
                                                        style="background-color: ; min-height: 60px;">
                                                        2:15:00
                                                    </div>
                                                </td>
                                                <td colspan=""
                                                    class="text-lg font-semibold text-center border-t border-b border-r border-black whitespace-nowrap"
                                                    style="background-color: ;">
                                                    <div class="border-t border-b border-r border-black "
                                                        style="background-color: ; min-height: 62px;">
                                                    </div>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endif

        @if ($pua_modal)
            <x-modal id="pua_modal" size="w-1/3" hasClose="1">
                <x-slot name="body">
                    <div class="grid grid-cols-12 px-4">

                        <div class="flex col-span-12 text-xl font-semibold">
                            PICK - UP ACTIVITY
                        </div>

                        <div class="flex col-span-12">
                            <div class="mt-1 text-sm text-gray-500 ">Booking Reference No. :</div>
                            <div class="ml-1 text-lg font-semibold text-blue-800 capitalize">WEB-07212022-1290</div>
                        </div>

                        <hr class="col-span-12 mt-1 text-blue-800 border border-blue-800">

                        <div class="col-span-6 mt-3">
                            <div class="relative" style="margin-top: ">
                                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                            stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                                    </svg>
                                </div>
                                <input type="search" id="default-search"
                                    class="block w-11/12 h-3 p-3 pl-10 text-xs text-gray-900 border border-gray-400 rounded-sm focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Search Waybill No." required>
                            </div>
                        </div>
                        <div class="col-span-12 mt-4 bg-white rounded-lg shadow-md">
                            <div class="w-full mx-auto">
                                <div class="w-full overflow-x-auto">
                                    <table class="w-full ">
                                        <thead>
                                            <tr>
                                                <th
                                                    class="px-6 py-2 text-xs font-normal tracking-wider text-left text-black whitespace-nowrap ">
                                                    No.</th>
                                                <th
                                                    class="px-3 py-2 text-xs font-normal tracking-wider text-left text-black ">
                                                    Waybill No.</th>
                                            </tr>
                                        </thead>
                                        <tbody class="divide-y-4 divide-white ">
                                            <tr>
                                                <td class="px-6 py-2 text-sm font-medium whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:10px;">
                                                    1.
                                                </td>
                                                <td class="px-3 py-2 text-sm font-medium text-[#003399] underline  whitespace-nowrap"
                                                    style="background-color: #F7F7F7; ">
                                                    <span class="cursor-pointer"
                                                        wire:click="actionpua({}, 'pua')">CEB4192738
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="px-6 py-2 text-sm font-medium whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:10px;">
                                                    2.
                                                </td>
                                                <td class="px-3 py-2 text-sm font-medium text-[#003399] underline  whitespace-nowrap"
                                                    style="background-color: #F7F7F7; ">
                                                    <span class="cursor-pointer" wire:click="actionpua({}, 'pua')">
                                                        CEB3817721
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endif

        @if ($da_modal)
            <x-modal id="da_modal" size="w-1/3" hasClose="1">
                <x-slot name="body">
                    <div class="grid grid-cols-12 px-4">

                        <div class="flex col-span-12 text-xl font-semibold">
                            DELIVERY ACTIVITY
                        </div>

                        <div class="flex col-span-12">
                            <div class="mt-1 text-sm text-gray-500 ">Booking Reference No. :</div>
                            <div class="ml-1 text-lg font-semibold text-blue-800 capitalize">WEB-07212022-1290</div>
                        </div>

                        <hr class="col-span-12 mt-1 text-blue-800 border border-blue-800">

                        <div class="col-span-6 mt-3">
                            <div class="relative" style="margin-top: ">
                                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                            stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                                    </svg>
                                </div>
                                <input type="search" id="default-search"
                                    class="block w-11/12 h-3 p-3 pl-10 text-xs text-gray-900 border border-gray-400 rounded-sm focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Search Waybill No." required>
                            </div>
                        </div>
                        <div class="col-span-12 mt-4 bg-white rounded-lg shadow-md">
                            <div class="w-full mx-auto">
                                <div class="w-full overflow-x-auto">
                                    <table class="w-full ">
                                        <thead>
                                            <tr>
                                                <th
                                                    class="px-6 py-2 text-xs font-normal tracking-wider text-left text-black whitespace-nowrap ">
                                                    No.</th>
                                                <th
                                                    class="px-3 py-2 text-xs font-normal tracking-wider text-left text-black ">
                                                    Waybill No.</th>
                                            </tr>
                                        </thead>
                                        <tbody class="divide-y-4 divide-white ">
                                            <tr>
                                                <td class="px-6 py-2 text-sm font-medium whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:10px;">
                                                    1.
                                                </td>
                                                <td class="px-3 py-2 text-sm font-medium text-[#003399] underline  whitespace-nowrap"
                                                    style="background-color: #F7F7F7; ">
                                                    <span class="cursor-pointer" wire:click="actionpua({}, 'pua')">
                                                        CEB4192738
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="px-6 py-2 text-sm font-medium whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:10px;">
                                                    2.
                                                </td>
                                                <td class="px-3 py-2 text-sm font-medium text-[#003399] underline  whitespace-nowrap"
                                                    style="background-color: #F7F7F7; ">
                                                    <span class="cursor-pointer" wire:click="actionpua({}, 'pua')">
                                                        CEB3817721
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endif

        @if ($ac_modal)
            <x-modal id="ac_modal" size="w-1/3" hasClose="1">
                <x-slot name="body">
                    <div class="grid grid-cols-12 px-4">

                        <div class="flex col-span-12 text-xl font-semibold">
                            Amount Collected
                        </div>

                        <div class="flex col-span-12">
                            <div class="mt-1 text-sm text-gray-500 ">e-DTR Reference No. :</div>
                            <div class="ml-1 text-lg font-semibold text-blue-800 capitalize">WEB-07212022-1290</div>
                        </div>

                        <hr class="col-span-12 mt-1 text-blue-800 border border-blue-800">

                        <div class="col-span-6 mt-3">
                            <div class="relative" style="margin-top: ">
                                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                            stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                                    </svg>
                                </div>
                                <input type="search" id="default-search"
                                    class="block w-11/12 h-3 p-3 pl-10 text-xs text-gray-900 border border-gray-400 rounded-sm focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Search Waybill No." required>
                            </div>
                        </div>
                        <div class="col-span-12 mt-4 bg-white rounded-lg shadow-md">
                            <div class="w-full mx-auto">
                                <div class="w-full overflow-x-auto">
                                    <table class="w-full ">
                                        <thead>
                                            <tr>
                                                <th
                                                    class="px-6 py-2 text-xs font-normal tracking-wider text-left text-black whitespace-nowrap ">
                                                    No.</th>
                                                <th
                                                    class="px-3 py-2 text-xs font-normal tracking-wider text-left text-black ">
                                                    Waybill No.</th>
                                            </tr>
                                        </thead>
                                        <tbody class="divide-y-4 divide-white ">
                                            <tr>
                                                <td class="px-6 py-2 text-sm font-medium whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:10px;">
                                                    1.
                                                </td>
                                                <td class="px-3 py-2 text-sm font-medium text-[#003399] underline  whitespace-nowrap"
                                                    style="background-color: #F7F7F7; ">
                                                    <span class="cursor-pointer" wire:click="actionpua({}, 'pua')">
                                                        CEB4192738
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="px-6 py-2 text-sm font-medium whitespace-nowrap"
                                                    style="background-color: #F7F7F7; min-width:10px;">
                                                    2.
                                                </td>
                                                <td class="px-3 py-2 text-sm font-medium text-[#003399] underline  whitespace-nowrap"
                                                    style="background-color: #F7F7F7; ">
                                                    <span class="cursor-pointer" wire:click="actionpua({}, 'pua')">
                                                        CEB3817721
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endif

        @if ($pv_modal)
            <x-modal id="pv_modal" size="w-3/5">
                <x-slot name="body">
                    <div class="grid col-span-12 p-10 border border-black">

                        <div class="col-span-6">
                            <span class=""><img class="h-24" src="/images/logo/capex-logo.png"
                                    alt="VeMoBro_logo"></span>
                        </div>
                        <div class="flex justify-end col-span-6">
                            <table class="text-right">
                                <tr>
                                    <td class="text-lg font-semibold">
                                        Electronic
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-3xl font-semibold">
                                        Daily Trip Record
                                    </td>
                                </tr>
                                <tr>
                                    <td class="flex ">
                                        <span class="text-xs text-gray-400 ">e-DTR Reference Number :</span>
                                        <span class="text-sm text-blue-800 capitalize">{{ $rmrefno }}</span>
                                    </td>
                                </tr>
                            </table>
                            <table>

                            </table>
                        </div>

                        <div class="col-span-12 mt-4 overflow-auto border border-black rounded-md">
                            <div class="flex p-2 px-3">
                                <table class="w-1/2 border-t border-b border-l border-black">
                                    <tr class="border-b border-black">
                                        <td class="flex p-2 px-3 ">
                                            <span class="text-[10px] text-gray-500" style="min-width: 130px;">Date
                                                Created
                                                :</span>
                                            <span class="text-xs font-semibold text-right">{{ $rmcreated }}</span>
                                        </td>
                                    </tr>
                                    <tr class="border-b border-black">
                                        <td class="flex p-2 px-3 ">
                                            <span class="text-[10px] text-gray-500 " style="min-width: 130px;">Date of
                                                Dispatch :</span>
                                            <span class="text-xs font-semibold text-right">{{ $rmdisp }}</span>
                                        </td>
                                    </tr>
                                    <tr class="border-b border-black">
                                        <td class="flex p-2 px-3 ">
                                            <span class="text-[10px] text-gray-500 " style="min-width: 130px;">Branch
                                                :</span>
                                            <span class="text-xs font-semibold text-right">{{ $rmbrnch }}</span>
                                        </td>
                                    </tr>
                                    <tr class="border-b border-black">
                                        <td class="flex p-2 px-3 ">
                                            <span class="text-[10px] text-gray-500 " style="min-width: 130px;">Plate
                                                Number :</span>
                                            <span class="text-xs font-semibold text-right">{{ $rmplate }}</span>
                                        </td>
                                    </tr>
                                </table>

                                <table class="w-1/2 border border-black">
                                    <tr class="border-b border-black">
                                        <td class="flex p-2 px-3 ">
                                            <span class="text-[10px] text-gray-500" style="min-width: 100px;">Team
                                                :</span>
                                            <span class="text-xs font-semibold text-right">{{ $rmteam }}</span>
                                        </td>
                                    </tr>
                                    <tr class="border-b border-black">
                                        <td class="flex p-2 px-3 ">
                                            <span class="text-[10px] text-gray-500 " style="min-width: 100px;">Driver
                                                :</span>
                                            <span class="text-xs font-semibold text-right">{{ $rmdrv }}</span>
                                        </td>
                                    </tr>
                                    <tr class="border-b border-black">
                                        <td class="flex p-2 px-3 ">
                                            <span class="text-[10px] text-gray-500 " style="min-width: 100px;">Checker
                                                1
                                                :</span>
                                            <span class="text-xs font-semibold text-right">{{ $rmchk1 }}</span>
                                        </td>
                                    </tr>
                                    <tr class="border-b border-black">
                                        <td class="flex p-2 px-3 ">
                                            <span class="text-[10px] text-gray-500 " style="min-width: 100px;">Checker
                                                2
                                                :</span>
                                            <span class="text-xs font-semibold text-right">{{ $rmchk2 }}</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class=" px-4 text-sm py-1 font-normal mt-2 text-white bg-[#003399] rounded-tr-xl"
                                style="border-top-right-radius: 40px; width: 200px">
                                Delivery Activities
                            </div>

                            <div class="">
                                <table class="min-w-full border-t border-b border-black ">
                                    <thead>
                                        <tr class="border-b border-gray-400">
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                                No.</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                                Waybill Number</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                                Origin</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                                Destination</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                                Shipper</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                                Consignee</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                                Transaction Date</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                                Transport Mode</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                                Qty</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                                Paymode</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                                Amount for Collection</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                                Amount Collected</th>
                                            <th class="px-1.5 py-2 text-[10px] font-normal text-black ">
                                                Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody class="divide-y-4 divide-white">
                            
                                        @foreach ($getEDtrs as $i => $getEDtr)
                                            @foreach ($getEDtrs[$i]['edtrDetails'] as $o => $getEDtrDet)
                                            @if ($getEDtrs[$i]['edtrDetails'][$o]['edtr_item_type'] == 2)
                                        <tr>
                                            <td class="px-1.5 py-2 text-xs font-medium whitespace-nowrap border-r border-gray-400"
                                                style="background-color: #F7F7F7;">{{ $o + 1 }}.
                                            </td>
                                            <td class="px-1.5 py-2 text-xs font-medium text-left whitespace-nowrap border-r border-gray-400"
                                                style="background-color: #F7F7F7;">{{$getEDtrDet['edtrtransaction']['waybill']}}
                                            </td>
                                            <td class="px-1.5 py-2 text-xs font-medium text-center whitespace-nowrap border-r border-gray-400"
                                                style="background-color: #F7F7F7;">{{$getEDtrDet['edtrtransaction']['OriginReference']['code']}}
                                            </td>
                                            <td class="px-1.5 py-2 text-xs font-medium text-center whitespace-nowrap border-r border-gray-400"
                                                style="background-color: #F7F7F7;">{{$getEDtrDet['edtrtransaction']['DestinationReference']['code']}}
                                            </td>
                                            <td class="px-1.5 py-2 text-xs font-medium border-r border-gray-400"
                                                style="background-color: #F7F7F7;">{{$getEDtrDet['edtrtransaction']['ShipperReference']['fullname']}}
                                            </td>
                                            <td class="px-1.5 py-2 text-xs font-medium text-center whitespace-nowrap border-r border-gray-400"
                                                style="background-color: #F7F7F7;">{{$getEDtrDet['edtrtransaction']['ConsigneeReference']['fullname']}}
                                            </td>
                                            <td class="px-1.5 py-2 text-xs font-medium whitespace-nowrap border-r border-gray-400"
                                                style="background-color: #F7F7F7;">{{ date('m/d/Y', strtotime($getEDtrDet['edtrtransaction']['transaction_date'])) }}
                                            </td>
                                            <td class="px-1.5 py-2 text-xs font-medium text-center whitespace-nowrap border-r border-gray-400"
                                                style="background-color: #F7F7F7;">{{$getEDtrDet['edtrtransaction']['TransportModeReference']['name']}}
                                            </td>
                                            <td class="px-1.5 py-2 text-xs font-medium text-center whitespace-nowrap border-r border-gray-400"
                                                style="background-color: #F7F7F7;">5
                                            </td>
                                            <td class="px-1.5 py-2 text-xs font-medium text-center whitespace-nowrap border-r border-gray-400"
                                                style="background-color: #F7F7F7;">{{$getEDtrDet['edtrtransaction']['CrmBookingReference']['BookingConsigneeHasManyBK'][0]['ModeOfPaymenReferenceCD']['name']}}
                                            </td>
                                            <td class="px-1.5 py-2 text-xs font-medium text-center whitespace-nowrap border-r border-gray-400"
                                                style="background-color: #F7F7F7;">500.00
                                            </td>
                                            <td class="px-1.5 py-2 text-xs font-medium text-center whitespace-nowrap border-r border-gray-400"
                                                style="background-color: #F7F7F7;">500.00
                                            </td>
                                            <td class="px-1.5 py-2 text-xs font-medium text-left whitespace-nowrap "
                                                style="background-color: #F7F7F7;">{{$getEDtrDet['edtrtransaction']['CrmBookingReference']['remarks']}}
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                                <span class="flex justify-end p-2 text-[10px] font-medium text-red">- no other delivery
                                    activity follows as of {{$latestDelivery}}
                                    </span>
                            </div>

                            <div class=" px-4 text-sm py-1 font-normal  text-white bg-[#003399] rounded-tr-xl"
                                style="border-top-right-radius: 40px; width: 200px">
                                Pickup Activities
                            </div>

                            <div class="">
                                <table class="min-w-full border-t border-b border-black">
                                    <thead>
                                        <tr class="border-b border-black">
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-black border-r border-gray-400">
                                                No.</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                                Booking Reference No.</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                                Shipper</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                                Pick Up Address</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                                Pick Up Date</th>
                                            <th
                                                class="px-1.5 py-2 text-[10px] font-normal text-left text-black border-r border-gray-400">
                                                Activity Type</th>
                                            <th class="px-1.5 py-2 text-[10px] font-normal text-left text-black ">
                                                Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody class="divide-y-4 divide-white">
                                         {{-- @foreach ($getEDtrs[0]['edtrDetails'] as $r => $getEDtr) --}}
                                         @foreach ($getEDtrs as $i => $getEDtr)
                                         @foreach ($getEDtrs[$i]['edtrDetails'] as $o => $getEDtrDet)
                                             @if ($getEDtrs[$i]['edtrDetails'][$o]['edtr_item_type'] == 1)
                                            <tr>
                                                <td class="px-1.5 py-2 text-xs font-medium whitespace-nowrap border-r border-gray-400"
                                                    style="background-color: #F7F7F7;">{{ $o + 1 }}.
                                                </td>
                                                <td class="px-1.5 py-2 text-xs font-medium text-left whitespace-nowrap border-r border-gray-400"
                                                    style="background-color: #F7F7F7;">
                                                    {{ $getEDtrDet['edtrbook']['booking_reference_no'] }}
                                                </td>
                                                <td class="px-1.5 py-2 text-xs font-medium text-left border-r border-gray-400"
                                                    style="background-color: #F7F7F7;">
                                                    {{ $getEDtrDet['edtrbook']['BookShipper'][0]['name'] }}
                                                </td>
                                                <td class="px-1.5 py-2 text-xs font-medium text-left border-r border-gray-400"
                                                    style="background-color: #F7F7F7;">
                                                    {{ $getEDtrDet['edtrbook']['BookShipper'][0]['address'] }}
                                                </td>
                                                <td class="px-1.5 py-2 text-xs font-medium text-left whitespace-nowrap border-r border-gray-400"
                                                    style="background-color: #F7F7F7;">
                                                    {{ date('m/d/Y', strtotime($getEDtrDet['edtrbook']['pickup_date'])) }}
                                                </td>
                                                <td class="px-1.5 py-2 text-xs font-medium border-r border-gray-400"
                                                    style="background-color: #F7F7F7;">
                                                    {{ $getEDtrDet['edtrbook']['ActivityReferenceBK']['activity_type'] }}
                                                </td>
                                                <td class="px-1.5 py-2 text-xs font-medium text-left "
                                                    style="background-color: #F7F7F7;">
                                                    {{ $getEDtrDet['edtrbook']['remarks'] }}
                                                </td>

                                            </tr>
                                            @endif
                                            @endforeach
                                        @endforeach
                                        {{-- @endforeach --}}
                                    </tbody>
                                </table>
                                <span class="flex justify-end p-2 text-[10px] font-medium text-red">- no other pick up
                                    activity follows as of {{ $latestPickup }}</span>
                            </div>

                            {{-- <div
                            class="max-w-screen-xl mx-auto mt-12 border-t border-black ">
                            <div class="text-[9px] text-gray-500 px-4 py-2">Acknowledged and Received by :</div>

                            <div class="flex items-center justify-center w-full p-8 space-x-2">
                                <hr class="w-full mt-2 text-black border border-black">
                                <hr class="w-full mt-2 text-black border border-black">
                                <hr class="w-full mt-2 text-black border border-black">
                            </div>
                        </div> --}}

                            <div class="col-span-12 pb-2 mt-10 border-t border-black">
                                <div class="text-[9px] text-gray-500 px-6 py-2">Acknowledged and Received by :</div>

                                <div class="flex justify-center mt-6 space-x-10">
                                    <span class="text-[10px] font-semibold text-center text-black"
                                        style="width: 28%">{{ $rmchk1 }}</span>
                                    <span class="text-[10px] font-semibold text-center text-black"
                                        style="width: 28%">{{ $rmchk2 }}</span>
                                    <span class="text-[10px] font-semibold text-center text-black"
                                        style="width: 28%">{{ $rmdrv }}</span>
                                </div>
                                <div class="flex justify-center space-x-10">
                                    <hr class="text-black border border-gray-400" style="width: 28%">
                                    <hr class="text-black border border-gray-400" style="width: 28%">
                                    <hr class="text-black border border-gray-400" style="width: 28%">
                                </div>
                                <div class="flex justify-center space-x-10">
                                    <span class="text-[9px] text-center text-gray-500" style="width: 28%">Printed Name
                                        and
                                        Signature</span>
                                    <span class="text-[9px] text-center text-gray-500" style="width: 28%">Printed Name
                                        and
                                        Signature</span>
                                    <span class="text-[9px] text-center text-gray-500" style="width: 28%">Printed Name
                                        and
                                        Signature</span>
                                </div>
                                <div class="flex justify-center space-x-10">
                                    <span class="text-[10px] font-semibold text-center text-black"
                                        style="width: 28%">Checker 1</span>
                                    <span class="text-[10px] font-semibold text-center text-black"
                                        style="width: 28%">Checker 2</span>
                                    <span class="text-[10px] font-semibold text-center text-black"
                                        style="width: 28%">Driver</span>
                                </div>



                                <div class="flex items-end justify-center w-full mt-10 space-x-10">
                                    <div class="flex justify-center mt-6">
                                        <span class="w-full text-center text-[10px] font-semibold text-black"></span>
                                        <span class="text-center text-[10px] font-semibold text-black"></span>
                                    </div>
                                    <div class="flex flex-col justify-center" style="width: 28%">
                                        <div class="mb-8 text-[9px] text-gray-500">Dispatched by :</div>
                                        <hr class="text-black border border-gray-400" />
                                        <span class="text-center text-[9px] text-gray-500">Printed Name and
                                            Signature</span>
                                        <span class="text-center text-[10px] font-semibold text-black">Order
                                            Controller</span>
                                    </div>
                                    <div class="flex flex-col justify-center" style="width: 28%">
                                        <hr class="text-black border border-gray-400" />
                                        <span class="text-center text-[9px] text-gray-500">Printed Name and
                                            Signature</span>
                                        <span class="text-center text-[10px] font-semibold text-black">Guard on
                                            Duty</span>
                                    </div>
                                </div>


                            </div>

                            {{-- <div class="col-span-12">
                            <div class="text-[9px] text-gray-500 px-6 py-2">Acknowledged and Received by :</div>
                            
                            <div class="flex justify-center mt-6 space-x-10">
                                <span class="text-[10px] font-semibold text-center text-black" style="width: 28%">Ceres, Diego</span>
                                <span class="text-[10px] font-semibold text-center text-black" style="width: 28%"></span>
                                <span class="text-[10px] font-semibold text-center text-black" style="width: 28%"></span>
                            </div>
                            <div class="flex justify-center space-x-10">
                                <hr class="text-black border border-gray-400" style="width: 28%">
                                <hr class="text-black border border-gray-400" style="width: 28%">
                                <hr class="text-black border border-gray-400" style="width: 28%">
                            </div>
                            <div class="flex justify-center space-x-10">
                                <span class="text-[9px] text-center text-gray-500" style="width: 28%">Printed Name and Signature</span>
                                <span class="text-[9px] text-center text-gray-500" style="width: 28%">Printed Name and Signature</span>
                                <span class="text-[9px] text-center text-gray-500" style="width: 28%">Printed Name and Signature</span>
                            </div>
                            <div class="flex justify-center space-x-10">
                                <span class="text-[10px] font-semibold text-center text-black" style="width: 28%">Checker 1</span>
                                <span class="text-[10px] font-semibold text-center text-black" style="width: 28%">Checker 2</span>
                                <span class="text-[10px] font-semibold text-center text-black" style="width: 28%">Driver</span>
                            </div>
                        </div> --}}

                        </div>

                    </div>

                    <div class="flex justify-end p-4 mt-2">
                        {{-- @dd($edtr) --}}

                        <a {{-- wire:click="actionpvs({'id':{{ $Edtrid }}}, 'pvs')" --}} target="_blank"
                            href="{{ route('oims.order-management.e-dtr.print', ['id' => $Edtrid]) }}"
                            class="flex items-center justify-center px-6 py-2 text-sm text-blue-800 border-2 border-blue-800 rounded-full hover:bg-blue-800 hover:text-white">

                            <svg class="mr-4" xmlns="http://www.w3.org/2000/svg" width="15.214" height="15.214"
                                viewBox="0 0 15.214 15.214">
                                <g id="Icon_feather-printer" data-name="Icon feather-printer"
                                    transform="translate(0.75 0.75)">
                                    <path id="Path_7" data-name="Path 7" d="M9,7.8V3h8.229V7.8"
                                        transform="translate(-6.257 -3)" fill="none" stroke="currentColor"
                                        stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Path_8" data-name="Path 8"
                                        d="M5.743,19.671H4.371A1.371,1.371,0,0,1,3,18.3V14.871A1.371,1.371,0,0,1,4.371,13.5H15.343a1.371,1.371,0,0,1,1.371,1.371V18.3a1.371,1.371,0,0,1-1.371,1.371H13.971"
                                        transform="translate(-3 -8.7)" fill="none" stroke="currentColor"
                                        stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                    <path id="Path_9" data-name="Path 9" d="M9,21h8.229v5.486H9Z"
                                        transform="translate(-6.257 -12.771)" fill="none" stroke="currentColor"
                                        stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" />
                                </g>
                            </svg>
                            Print e-DTR
                        </a>
                    </div>
                </x-slot>
            </x-modal>
        @endif


    </x-slot>

    <x-slot name="header_title">
        <div class="flex space-x-2">
            <div class="normal-case">
                e-DTR
            </div>
        </div>
    </x-slot>
    <x-slot name="header_button">
    </x-slot>

    <x-slot name="body">
        <div class="grid grid-cols-5 text-sm" style="margin-top: 4%;">
            <div class="w-3/4">
                <div>
                    <x-label style="color: #F3F4F6;" value="'" />
                    <x-transparent.input type="text" label="e-DTR Reference No" name="refno"
                        wire:model.defer="refno" />
                </div>
            </div>
            <div class="w-3/4">
                <div>
                    <x-label style="color: #F3F4F6;" value="'" />
                    <x-transparent.input type="text" label="Waybill No" name="waybill"
                        wire:model.defer="waybill" />
                </div>
            </div>
            <div class="w-3/4">
                <x-label style="color: #F3F4F6;" value="'" />
                <x-transparent.input style="font-size: 14px;" type="text" onfocus="(this.type='date')"
                    onblur="(this.type='text')" label="" placeholder="Dispatch Date" name="disdate"
                    wire:model.defer="disdate" />
            </div>

            <div class="w-3/4">
                <x-label style="color: #F3F4F6;" value="'" />
                <x-transparent.select value="" label="Branch" name="brnch" wire:init="branchReference"
                    wire:model.defer="brnch">
                    <option value=""></option>
                    @foreach ($branchReferences as $branchReference)
                        <option value="{{ $branchReference->id }}">
                            {{ $branchReference->name }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div>

            <div class="w-3/4">
            </div>

            <div class="w-3/4">
                <x-label style="color: #F3F4F6;" value="'" />
                <x-transparent.select value="" label="Team" name="team" wire:init="teamReference"
                    wire:model.defer="team">
                    <option value=""></option>
                    @foreach ($teamReferences as $teamReference)
                        <option value="{{ $teamReference->id }}">
                            {{ $teamReference->name }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div>

            <div class="w-3/4">
                <x-label style="color: #F3F4F6;" value="'" />
                <x-transparent.select value="" label="Driver" name="driver" wire:init="driverReference"
                    wire:model.defer="driver">
                    <option value=""></option>
                    @foreach ($driverReferences as $driverReference)
                        <option value="{{ $driverReference->user_id }}">
                            {{ $driverReference->first_name . ' ' . $driverReference->last_name }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div>

            <div class="w-3/4">
                <x-label style="color: #F3F4F6;" value="'" />
                <x-transparent.select value="" label="Checker" name="chk" wire:init="checkerReference"
                    wire:model.defer="chk">
                    <option value=""></option>
                    @foreach ($checkerReferences as $checkerReference)
                        <option value="{{ $checkerReference->user_id }}">
                            {{ $checkerReference->first_name . ' ' . $checkerReference->last_name }}
                            {{-- @dd($checkerReference) --}}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div>

            <div class="w-3/4">
                <x-label style="color: #F3F4F6;" value="'" />
                <x-transparent.select value="" label="Plate Number" name="plate" wire:init="plateReference"
                    wire:model.defer="plate">
                    <option value=""></option>
                    @foreach ($plateReferences as $plateReference)
                        <option value="{{ $plateReference->id }}">
                            {{ $plateReference->plate_no }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div>
            <div class="">
                <x-label style="color: #F3F4F6;" value="'" />
                <x-button type="button" wire:click="search" title="Search"
                    class="px-2 py-1 mt-3 font-normal text-white bg-blue hover:bg-[#003399] " />
            </div>
        </div>
        <div class="py-6">
            <div class="flex items-center justify-start">
                <ul class="flex mt-8">
                    {{-- @foreach ($status_header_cards as $i => $status_card)
                      
                        <li
                            class="px-3 py-1 text-xs {{ $status_header_cards[$i]['class'] }} {{ $stats == $status_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                            <button
                                wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                {{ $status_header_cards[$i]['title'] }}
                                <span class="ml-6 text-sm">{{ $status_header_cards[$i]['value'] }}</span>
                            </button>
                        </li>
                    @endforeach --}}
                </ul>
            </div>
            <div class="bg-white rounded-lg">
                <div class="w-full mx-auto">
                    <div class="w-full overflow-x-auto">
                        <table class="w-full ">
                            <thead>
                                <tr>
                                    <th class="sticky left-0 py-1 text-xs font-medium tracking-wider text-left text-gray-500 bg-white border-r-2 border-gray-300 "
                                        colspan="2">
                                    </th>
                                    <th class="px-3 py-1 text-xs font-medium tracking-wider text-left text-gray-500 border-r border-black"
                                        colspan="3">
                                    </th>
                                    <th class="px-3 py-1 text-xs font-medium tracking-wider text-center text-black border-r border-black"
                                        style="background-color: #E0E0E0;" colspan="10">
                                        Pick Up Activity</th>
                                    <th class="px-3 py-1 text-xs font-medium tracking-wider text-center text-black border-r border-black"
                                        style="background-color: #00000029;" colspan="9">
                                        Delivery Activity</th>

                                    <th class="px-3 py-1 text-xs font-medium tracking-wider text-center"
                                        colspan="9">
                                    </th>
                                </tr>
                                <tr>
                                    <th
                                        class="sticky left-0 pl-4 py-2 text-[10px] font-normal tracking-wider text-left text-black  bg-white">
                                        No.</th>
                                    <th class="sticky bg-white pr-4  py-2 text-left text-[10px] font-normal text-black  tracking-wider border-r-2 border-gray-300"
                                        style=" left:100px;">
                                        e-DTR Reference No</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal whitespace-nowrap tracking-wider text-left text-black ">
                                        Dispatch Date</th>
                                    <th class="px-3 py-2 text-[10px] font-normal tracking-wider text-left text-black ">
                                        Branch</th>

                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-left text-black  border-r border-black">
                                        Team</th>
                                    <th
                                        class="text-center px-3 py-2 text-[10px] font-normal tracking-wider text-black ">
                                        Total No. of Bookings</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        No. of Completed Bookings</th>
                                    <th
                                        class=" px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        Total No. of WBs Created</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        AF Actual Weight (kgs)</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        AF CWT (kgs)</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        SF Actual Weight (kgs)</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        SF Volume (CBM)</th>
                                    <th
                                        class="px-3 py-2 text-[10px] text-[#003399] font-normal tracking-wider text-center ">
                                        Total Actual Wt (kgs.)</th>
                                    <th
                                        class="px-3 py-2 text-[10px] text-[#003399] font-normal tracking-wider text-center ">
                                        Total Volume (tons)</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black  border-r border-black">
                                        % of Completion</th>

                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        No. of Waybills</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        Total No. of Completed Delivery</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        AF Actual Weight (kgs)</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        AF CWT (kgs)</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        SF Actual Weight (kgs)</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        SF Volume (CBM)</th>
                                    <th
                                        class="px-3 py-2 text-[10px] text-[#003399] font-normal tracking-wider text-center text-black ">
                                        Total Actual Wt (kgs.)</th>
                                    <th
                                        class="px-3 py-2 text-[10px] text-[#003399] font-normal tracking-wider text-center text-black ">
                                        Total Volume (tons)</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black border-r border-black">
                                        % of Completion</th>

                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        For Collection</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        Amount Collected</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        Amount for Remittance</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        Remittance</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        Pick-up Utilization %</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        Accumulated Utilization (Pick-up Delivery)</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        Real-Time Scanning Performance</th>
                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        e-DTR Status</th>

                                    <th
                                        class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody class="divide-y-4 divide-white ">
                                @foreach ($edtr_lists as $o => $edtr_list)
                                    <tr>
                                        <td class="sticky left-0 py-2 pl-4 text-xs font-medium border-b border-gray-200 whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">
                                            {{ ($edtr_lists->currentPage() - 1) * $edtr_lists->links()->paginator->perPage() + $loop->iteration }}.
                                        </td>
                                        <td class="sticky py-2 pr-4 text-xs font-medium text-[#003399] underline border-b border-r-2 border-gray-300  whitespace-nowrap"
                                            style=" left:100px; background-color: #F7F7F7;">
                                            <span class="cursor-pointer"
                                                wire:click="actionedtrr({'id':{{ $edtr_list->id }}},'edtrr') }}">
                                                {{ $edtr_list->reference_no }}
                                            </span>
                                        </td>
                                        <td class="px-3 py-2 text-xs font-medium whitespace-nowrap"
                                            style="background-color: #F7F7F7;">
                                            {{ date('m/d/Y', strtotime($edtr_list->dispatch_date)) }}
                                        </td>
                                        <td class="px-3 py-2 text-xs font-medium whitespace-nowrap"
                                            style="background-color: #F7F7F7;">{{ $edtr_list->edtrBranch['name'] }}
                                        </td>
                                        <td class="px-3 py-2 text-xs font-medium border-r border-black whitespace-nowrap"
                                            style="background-color: #F7F7F7;">
                                            {{ $edtr_list->Traref->teamIdReference['name'] }}
                                        </td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">
                                            {{ $edtr_list->edtr_details_count }}</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:120px;">
                                            <?php $countcompleted = []; ?>
                                            @foreach ($edtr_list->edtrDetails as $i => $edtrDetail)
                                                <?php $countcompleted[] += $edtrDetail['edtrcookstatus']; ?>
                                            @endforeach
                                            {{ array_sum($countcompleted) }}

                                        </td>
                                        <td class="px-3 py-2 text-xs font-medium text-center text-[#003399] underline  whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">
                                            <span class="cursor-pointer" wire:click="actionpua({}, 'pua')">
                                                58
                                            </span>
                                        </td>
                                        {{-- @dd(count($edtr_list->edtrDetails)) --}}
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">5</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:80px;">150</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">1,140</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">5</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center text-[#003399]  whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">1,290</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center text-[#003399]  whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">1.20</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center text-[#FF0000] border-r border-black  whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">90%</td>

                                        <td class="px-3 py-2 text-xs font-medium text-center text-[#003399] underline  whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;"><span
                                                class="cursor-pointer" wire:click="actionda({}, 'da')">
                                                4
                                            </span></td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:140px;">4</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">80</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:80px;">100</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">0</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">0</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap text-[#003399]"
                                            style="background-color: #F7F7F7; min-width:100px;">80</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap text-[#003399]"
                                            style="background-color: #F7F7F7; min-width:100px;">0.08</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center  whitespace-nowrap border-r border-black text-[#18EB00]"
                                            style="background-color: #F7F7F7; min-width:100px;">100%</td>

                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">15,600.00</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap text-[#003399] underline"
                                            style="background-color: #F7F7F7; min-width:100px;"><span
                                                class="cursor-pointer" wire:click="actionac({}, 'ac')">
                                                15,600.00
                                            </span></td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">9,000.00</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">15,600.00</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center text-[#FF0000] whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">64%</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center text-[#FF0000] whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:160px;">65%</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center text-[#FF0000] whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:160px;">90%</td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; min-width:100px;">
                                            <svg class="ml-6" xmlns="http://www.w3.org/2000/svg" width="16"
                                                height="16" viewBox="0 0 16 16">
                                                <circle id="Ellipse_43" data-name="Ellipse 43" cx="8"
                                                    cy="8" r="8" fill="#18eb00" />
                                            </svg>
                                        </td>
                                        <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                            style="background-color: #F7F7F7; color:#8d8d8d;">
                                            <div class="flex space-x-4">
                                                <span
                                                    wire:click="actionedtrr({'id':{{ $edtr_list->id }}},'edtrr') }}"
                                                    title="View">
                                                    <svg wire:click="" class="cursor-pointer hover:text-blue-800"
                                                        xmlns="http://www.w3.org/2000/svg" width="15.527"
                                                        height="10.352" viewBox="0 0 15.527 10.352">
                                                        <path id="Icon_awesome-eye" data-name="Icon awesome-eye"
                                                            d="M15.434,9.282A8.646,8.646,0,0,0,7.764,4.5,8.648,8.648,0,0,0,.094,9.282a.872.872,0,0,0,0,.787,8.646,8.646,0,0,0,7.67,4.782,8.648,8.648,0,0,0,7.67-4.782A.872.872,0,0,0,15.434,9.282Zm-7.67,4.275a3.882,3.882,0,1,1,3.882-3.882A3.882,3.882,0,0,1,7.764,13.558Zm0-6.47a2.569,2.569,0,0,0-.682.1,1.29,1.29,0,0,1-1.8,1.8A2.582,2.582,0,1,0,7.764,7.088Z"
                                                            transform="translate(0 -4.5)" fill="currentColor" />
                                                    </svg>
                                                </span>
                                                <span title="Print">
                                                    <svg wire:click="actionpv({'id':{{ $edtr_list->id }}}, 'pv')"
                                                        class="cursor-pointer hover:text-blue-800"
                                                        xmlns="http://www.w3.org/2000/svg" width="14.543"
                                                        height="13.424" viewBox="0 0 14.543 13.424">
                                                        <path id="Icon_ionic-md-print"
                                                            data-name="Icon ionic-md-print"
                                                            d="M15.679,7.856H5.614a2.245,2.245,0,0,0-2.239,2.237v4.848H6.168v2.983h8.957V14.941h2.793V10.093A2.245,2.245,0,0,0,15.679,7.856ZM14,16.806H7.29V12.331H14ZM15.125,4.5H6.168V7.3h8.957V4.5Z"
                                                            transform="translate(-3.375 -4.5)"
                                                            fill="currentColor" />
                                                    </svg>
                                                </span>

                                            </div>
                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>


            </div>
    </x-slot>

</x-form>
