<div wire:init="" x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
    confirmsub_modal: '{{ $confirmsub_modal }}',

}">
    <x-loading></x-loading>
    @if ($confirmation_modal)
        <x-modal id="confirmation_modal" size="w-auto" hasClose="1">
            <x-slot name="body">
                <span class="relative block w-full">
                    <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                        wire:click="$set('confirmation_modal', false)">
                    </span>
                </span>
                <h2 class="text-xl text-center">
                    Are you sure you want to select this team?
                </h2>

                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                        No
                    </button>
                    <button type="button" wire:click="submit"
                        class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                        Yes
                    </button>
                </div>
            </x-slot>
        </x-modal>
    @endif

    @if ($confirmsub_modal)
        <x-modal id="confirmsub_modal" size="auto" hasClose="1">
            <x-slot name="body">
                <form>
                    <div class="grid grid-cols-12 p-0">
                        <div class="col-span-12">
                            <span class="flex justify-center "><img class="w-16 h-16"
                                    src="/images/logo/viber_image_2023-09-15_14-06-35-034.png"
                                    alt="VeMoBro_logo"></span>
                            <div class="flex justify-center mt-4">
                                <h1 class="text-sm font-medium ">Team successfully Dispatched!</h1>
                            </div>
                            <div class="col-span-12 px-0 mx-0 mt-2 gap-x-0">
                                <hr class="text-black border border-gray-400 ">
                            </div>

                            <div class="flex gap-24">
                                <div class="flex col-span-8 mt-2 space-x-4 text-xs">
                                    <span class="font-semibold text-gray-500">
                                        <table>
                                            <tr>
                                                <td>Team Name :</td>
                                            </tr>
                                            <tr>
                                                <td>Route Category :</td>
                                            </tr>
                                            <tr>
                                                <td>Plate Number :</td>
                                            </tr>
                                        </table>
                                    </span>
                                    <span class="font-semibold">
                                        <table>
                                            <tr>
                                                <td>
                                                    {{ $teamname }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    {{ $routec }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    {{ $plate }}
                                                </td>
                                            </tr>
                                        </table>
                                    </span>
                                </div>

                                <div class="flex col-span-4 mt-2 space-x-4 text-xs ">
                                    <span class="font-semibold text-gray-500">
                                        <table>
                                            <tr>
                                                <td>Driver :</td>
                                            </tr>
                                            <tr>
                                                <td>Checker 1 :</td>
                                            </tr>
                                            <tr>
                                                <td>Checker 2 :</td>
                                            </tr>
                                        </table>
                                    </span>
                                    <span class="font-semibold">
                                        <table>
                                            <tr>
                                                <td>
                                                    {{ $driver }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    {{ $checker1 }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    {{ $checker2 ?? '-' }}
                                                </td>
                                            </tr>
                                        </table>
                                    </span>
                                </div>
                            </div>

                            <div class="mt-2 bg-white border border-gray-300 rounded-lg shadow-md">
                                <table
                                    class="min-w-full rounded-xl shadow-md overflow-auto max-h-[100px] border divide-y divide-gray-200 border-gray-50">
                                    <thead>
                                        <tr>
                                            <th class="px-3 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                No.</th>
                                            <th class="px-3 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                Booking Reference No</th>
                                            <th class="px-3 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                @if ($actstat == 1 || $actstat == null)
                                                    Shipper
                                                @elseif($actstat == 2)
                                                    Consignee
                                                @endif
                                            </th>
                                            <th class="px-3 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                Pick Up Address</th>
                                        </tr>
                                    </thead>
                                    <tbody class="overflow-auto bg-gray-100 divide-y-4 divide-white">
                                        <form>
                                            @foreach ($Getbookings as $i => $Getbooking)
                                                <tr>
                                                    <td
                                                        class="px-3 py-3 text-xs font-semibold text-center whitespace-nowrap">
                                                        {{ $i + 1 }}.
                                                    </td>
                                                    <td
                                                        class="px-3 py-3 text-xs font-semibold text-center whitespace-nowrap">
                                                        {{ $Getbooking['reference'] }}
                                                    </td>
                                                    <td class="px-3 py-3 text-xs font-semibold text-center whitespace-nowrap"
                                                        style="">
                                                        {{ $Getbooking['shippername'] }}
                                                    </td>
                                                    <td class="px-3 py-3 text-xs font-semibold text-center"
                                                        style="max-width: 250px;">
                                                        {{ $Getbooking['address'] }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </form>
                                    </tbody>
                                </table>
                            </div>

                            <div class="flex justify-center mt-4 space-x-10">
                                <div class="flex justify-start" style="">
                                    <button wire:click="actionc({}, 'ba_close')"
                                        class="px-6 py-1 text-xs font-normal text-white bg-blue-800 rounded-md whitespace-nowrap hover:bg-blue-900 "
                                        type="button">
                                        OK
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </x-slot>
        </x-modal>
    @endif


    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <hr class="text-black border-2 border-blue-800 ">
        <div class="relative mt-6 mb-6" style="margin-top:">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <svg class="w-3 h-3 text-gray-500 dark:text-gray-400" aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                </svg>
            </div>
            <input type="search" id="default-search"
                class="block w-1/6 h-3 p-3 pl-8 text-[10px] font-semibold text-gray-900 border border-gray-400 rounded-sm focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Search Team Name" wire:model="tn" name="tn">
        </div>

        <div class="bg-white rounded-lg shadow-md ">
            <table class="min-w-full overflow-auto max-h-[100px] border divide-y divide-gray-200 border-gray-50">
                <thead>
                    <tr>
                        <th class="px-4 py-2 text-[10px] font-semibold tracking-wider text-left ">

                        </th>
                        <th class="px-4 py-2 text-[10px] font-semibold tracking-wider text-left ">
                            Team Name</th>
                        <th class="px-4 py-2 text-[10px] font-semibold tracking-wider text-left ">
                            Route Category</th>
                        <th class="px-4 py-2 text-[10px] font-semibold tracking-wider text-left ">
                            Plate Number</th>
                        <th class="px-4 py-2 text-[10px] font-semibold tracking-wider text-left ">
                            Driver</th>
                        <th class="px-4 py-2 text-[10px] font-semibold tracking-wider text-left ">
                            Checker 1</th>
                        <th class="px-4 py-2 text-[10px] font-semibold tracking-wider text-left ">
                            Checker 2</th>
                    </tr>
                </thead>
                <tbody class="overflow-auto bg-gray-100 divide-y-4 divide-white">
                    @foreach ($team_lists as $i => $team_list)
                        <tr>
                            <td class="px-4 py-3 text-xs font-semibold whitespace-nowrap">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-blue-800 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 focus:outline-none"
                                    type="radio" name="rdis" wire:model.defer="rdis" value="{{ $team_list->id }}">
                            </td>
                            <td class="px-4 py-3 text-xs font-semibold whitespace-nowrap">
                                <p class="">
                                    {{ $team_list->teamIdReference['name'] }}
                                </p>
                            </td>
                            <td class="px-4 py-3 text-xs font-semibold whitespace-nowrap" style="">
                                {{ $team_list->teamRouteReference['name'] }}
                            </td>
                            <td class="px-4 py-3 text-xs font-semibold whitespace-nowrap" style="">
                                {{ $team_list->teamPlateReference['plate_no'] }}
                            </td>
                            <td class="px-4 py-3 text-xs font-semibold whitespace-nowrap">
                                @if (isset($team_list))
                                    @if (isset($team_list->teamDriverReference->userDetails->timeLog[0]['date']) &&
                                            $team_list->teamDriverReference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                        {{ $team_list->teamDriverReference->userDetails->first_name . ' ' . $team_list->teamDriverReference->userDetails->last_name ?? '-' }}
                                    @else
                                        <p class="text-red">
                                            {{ $team_list->teamDriverReference->userDetails->first_name . ' ' . $team_list->teamDriverReference->userDetails->last_name ?? '-' }}
                                        </p>
                                    @endif
                                @else
                                    <p class="text-center">-</p>
                                @endif
                            </td>
                            <td class="px-4 py-3 text-xs font-semibold whitespace-nowrap">
                                @if (isset($team_list))
                                    @if (isset($team_list->teamChecker1Reference->userDetails->timeLog[0]['date']) &&
                                            $team_list->teamChecker1Reference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                        {{ $team_list->teamChecker1Reference->userDetails->first_name . ' ' . $team_list->teamChecker1Reference->userDetails->last_name ?? '-' }}
                                    @else
                                        <p class="text-red" style="padding-left:;">
                                            {{ $team_list->teamChecker1Reference->userDetails->first_name . ' ' . $team_list->teamChecker1Reference->userDetails->last_name ?? '-' }}
                                        </p>
                                    @endif
                                @else
                                    <p class="text-center">-</p>
                                @endif
                            </td>
                            <td class="px-4 py-3 text-xs font-semibold whitespace-nowrap">
                                @if (isset($team_list))
                                    @if (isset($team_list->teamChecker2Reference->userDetails->timeLog[0]['date']) &&
                                            $team_list->teamChecker2Reference->userDetails->timeLog[0]['date'] == date('Y-m-d'))
                                        @if ($team_list->checker2_id != null)
                                            {{ $team_list->teamChecker2Reference->userDetails->first_name . ' ' . $team_list->teamChecker2Reference->userDetails->last_name ?? '-' }}
                                        @else
                                            <p class="text-center">-</p>
                                        @endif
                                    @else
                                        <p class="text-red" style="padding-left:;">
                                            @if ($team_list->checker2_id != null)
                                                {{ $team_list->teamChecker2Reference->userDetails->first_name . ' ' . $team_list->teamChecker2Reference->userDetails->last_name ?? '-' }}
                                            @else
                                                <p class="text-center">-</p>
                                            @endif
                                        </p>
                                    @endif
                                @else
                                    <p class="text-center">-</p>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="mt-2">
            <x-input-error for="rdis" />
        </div>
        <div class="flex justify-end gap-3 mt-6 space-x-3">
            <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                class="px-12 bg-white text-blue hover:bg-gray-100" />
            <x-button type="submit" title="select" class="px-12 bg-blue text-white hover:bg-[#002161]" />
        </div>
    </form>
</div>
