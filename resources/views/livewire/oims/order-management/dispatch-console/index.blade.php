<x-form wire:init='load' x-data="{
    dispatch_modal: '{{ $dispatch_modal }}',
    no_workinst_modal: '{{ $no_workinst_modal }}',
    workinst_modal: '{{ $workinst_modal }}',
    bookref_modal: '{{ $bookref_modal }}',
    consigneedet_modal: '{{ $consigneedet_modal }}',
    bsl_modal: '{{ $bsl_modal }}',
}">

    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @if (($bid && $sel && $dispatch_modal) || $actstat)
            <x-modal id="dispatch_modal" size="w-3/4" hasClose="1">
                <x-slot name="title">Select Team To Dispatch</x-slot>
                <x-slot name="body">
                    @livewire('oims.order-management.dispatch-console.dispatch', ['selecteds' => $sel, 'branch_id' => $bid, 'actStatusHead' => $actstat])
                </x-slot>
            </x-modal>
        @endif
        @if ($no_workinst_modal)
            <x-modal id="no_workinst_modal" size="w-1/4">
                <x-slot name="body">
                    <div class="grid grid-cols-12 px-4">
                        <div class="col-span-12">
                            <h1 class="text-xl font-bold text-left">Work Instruction
                            </h1>
                        </div>
                        <div class="col-span-12 mt-4 text-center">
                            <span class="text-lg font-normal">
                                No Work Instruction.
                            </span>
                        </div>
                    </div>
                    {{-- <div class="grid grid-cols-12">
                    <div class="col-span-12 mt-4">
                        <div class="flex justify-center">
                            <button 
                            @dd($work_ins_id)
                            wire:click="actionviewedit({'id': {{ $work_ins_id }}},'viewedit') }}"
                                class="flex justify-center w-full py-2 text-sm font-light text-white bg-blue-800 border-2 rounded-md whitespace-nowrap hover:bg-blue-900">
                                Update Booking
                            </button>
                        </div>
                    </div>
                </div> --}}
                </x-slot>
            </x-modal>
        @endif

        @if ($workinst_modal)
            <x-modal id="workinst_modal" size="w-1/2">
                <x-slot name="body">
                    <div class="grid grid-cols-12 px-6">
                        <div class="col-span-12">
                            <h1 class="text-xl font-bold text-left">Work Instruction
                            </h1>
                        </div>
                        @if (isset($work_ins) ?? '')
                            <div class="col-span-12 mt-2">
                                <span class="text-lg font-normal leading-none">
                                    {{ $work_ins }}
                                </span>
                            </div>
                        @endif

                    </div>

                    <div class="grid grid-cols-12 mt-2">
                        @if (isset($bookworkins->BookingAttachmentHasManyBK[0]))

                            <div class="flex justify-center col-span-12 mt-4 space-x-2 bg-gray-100 border-2 border-gray-400"
                                style="margin-left: -16px; margin-right: -16px;">
                                @foreach ($bookworkins->BookingAttachmentHasManyBK as $i => $book)
                                    @if (in_array($book['extension'], config('filesystems.image_type')))
                                        <div class="w-20 h-24 cursor-pointer" {{-- @dd($book->id) --}}
                                            {{-- href="{{ Storage::disk('crm_gcs')->url($book['path'] . $book['name']) }}"target="_blank" --}}>
                                            {{-- {{ $book->id }} --}}
                                            <img class="flex w-20 h-24 border border-gray-400 rounded-md cursor-pointer"
                                                wire:click="actionvimg({'id': {{ $book->id }}},'vimg') }}"
                                                src="{{ $book->extension != 'txt' ? Storage::disk('crm_gcs')->url($book->path . $book->name) : 'https://static.wikia.nocookie.net/logopedia/images/c/c4/Notepad_Vista_10.png' }}"
                                                alt="">
                                        </div>
                                    @elseif(in_array($book['extension'], config('filesystems.file_type')))
                                        <div class="w-20 h-24 bg-white rounded-md cursor-pointer"
                                            wire:click="actionvpdf({'id': {{ $book->id }}},'vpdf') }}">
                                            <span
                                                class="flex items-center justify-center w-20 h-24 border border-gray-400 rounded-md">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20"
                                                    viewBox="0 0 14.427 19.236">
                                                    <path id="Icon_awesome-file-pdf" data-name="Icon awesome-file-pdf"
                                                        d="M6.834,9.622A3.625,3.625,0,0,1,6.759,7.86C7.074,7.86,7.044,9.246,6.834,9.622ZM6.77,11.395A17.336,17.336,0,0,1,5.7,13.751a13.834,13.834,0,0,1,2.363-.823A4.867,4.867,0,0,1,6.77,11.395ZM3.235,16.084c0,.03.5-.2,1.311-1.51A5.193,5.193,0,0,0,3.235,16.084ZM9.317,6.011h5.109V18.334a.9.9,0,0,1-.9.9H.9a.9.9,0,0,1-.9-.9V.9A.9.9,0,0,1,.9,0H8.416V5.109A.9.9,0,0,0,9.317,6.011Zm-.3,6.454a3.771,3.771,0,0,1-1.6-2.021,5.4,5.4,0,0,0,.233-2.412.941.941,0,0,0-1.8-.255,6.247,6.247,0,0,0,.3,2.893,35.279,35.279,0,0,1-1.533,3.223s0,0-.008,0c-1.018.522-2.765,1.672-2.048,2.555a1.167,1.167,0,0,0,.808.376c.672,0,1.341-.676,2.3-2.322a21.415,21.415,0,0,1,2.968-.872,5.694,5.694,0,0,0,2.4.733.973.973,0,0,0,.74-1.631c-.522-.511-2.04-.364-2.765-.271Zm5.147-8.521L10.482.263A.9.9,0,0,0,9.843,0H9.618V4.809h4.809V4.58A.9.9,0,0,0,14.164,3.945ZM11.38,13.536c.154-.1-.094-.447-1.608-.338C11.166,13.792,11.38,13.536,11.38,13.536Z"
                                                        fill="#8d8d8d" />
                                                </svg>
                                            </span>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif
                        {{-- <div class="col-span-12 mt-4">
                        <div class="flex justify-center">
                            <button wire:click="actionviewedit({'id': {{ $work_ins_id }}},'viewedit') }}"
                                class="flex justify-center w-1/2 py-2 text-sm font-light text-white bg-blue-800 border-2 rounded-md whitespace-nowrap hover:bg-blue-900">
                                Update Booking
                            </button>
                        </div>
                    </div> --}}
                    </div>
                </x-slot>
            </x-modal>
        @endif

        @if ($bookref_modal)
            <x-modal id="bookref_modal" size="w-3/4">
                <x-slot name="body">
                    <form>
                        <div class="grid grid-cols-12 gap-2 px-6 gap-x-4 ">
                            <div class="col-span-6">
                                @if ($IDXpickupwalk == 1)
                                    <div class="flex items-center justify-start">
                                        <div wire:click=""
                                            class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                            <span>PICK UP</span>
                                        </div>
                                        <div wire:click=""
                                            class="flex justify-between px-6 py-2 text-sm text-left bg-white">
                                            <span class="">WALK IN</span>
                                        </div>
                                    </div>
                                @else
                                    <div class="flex items-center justify-start">
                                        <div wire:click="" class="flex justify-between px-6 py-2 text-sm text-left">
                                            <span>PICK UP</span>
                                        </div>
                                        <div wire:click=""
                                            class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                            <span class="">WALK IN</span>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            {{-- {{dd($IDXcbId);}} --}}
                            <div class="col-span-6">
                                <x-button type="button" wire:click="actionBSLogs({'id':{{ $IDXcbId }}},'')"
                                    {{-- {'id':{{ $vconsignee[0][$i]['id'] }}},'c_det') } --}} title="Booking Status Logs"
                                    class=" w-22 h-10 bg-blue text-white hover:bg-[#002161]" style="float: right;" />
                            </div>
                            <div class="w-11/12 col-span-6">
                                <table class="overflow-hidden">
                                    {{-- <x-slot name="tbody"> --}}
                                    <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                                        <h1 class="text-xl font-semibold text-left text-blue">Shipper Details
                                        </h1>
                                    </div>

                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Booking Reference No. :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXbookingrefno }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Booking Status :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXbookingstat }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Shipper :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXfullname }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Company :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black">
                                            {{ $IDXcompany ?? '-' }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Contact Person :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXfname }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Contact Number :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXmobile_nos }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Email Address :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXemail_address }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Customer Type :

                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXcustomertype }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Activity Type :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXacttype }}
                                        </td>
                                    </tr>

                                    {{-- </x-slot> --}}
                                </table>
                            </div>
                            <div class="w-11/12 col-span-6 ml-12">
                                <table class="overflow-hidden">
                                    {{-- <x-slot name="tbody" class=""> --}}
                                    <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                                        <h1 class="text-xl font-semibold text-left text-blue">Pick Up Details
                                        </h1>
                                    </div>

                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Complete Address :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black">
                                            {{ $IDXaddress }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Landmark :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            -
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Timeslot :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            {{ $IDXtimeslot }}
                                        </td>
                                    </tr>
                                    {{-- </x-slot> --}}
                                </table>
                            </div>
                        </div>
                        {{-- <div class="grid grid-cols-12 gap-0 px-0 m-0 border-b-2 border-gray-400">
                    </div> --}}
                        <div class="col-span-12 px-0 mx-0 mt-2 gap-x-0">
                            <hr class="text-black border border-gray-400 ">
                        </div>
                        <div class="grid grid-cols-12 gap-2 px-6 mt-4 gap-x-4">
                            <div class="col-span-6">
                                @if ($IDXsinglemulti == 2)
                                    <div class="flex items-center justify-start">
                                        <div wire:click=""
                                            class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                            <span>SINGLE</span>
                                        </div>
                                        <div wire:click="" class="flex justify-between px-6 py-2 text-sm text-left">
                                            <span class="">MULTIPLE</span>
                                        </div>
                                    </div>
                                @else
                                    <div class="flex items-center justify-start">
                                        <div wire:click="" class="flex justify-between px-6 py-2 text-sm text-left">
                                            <span>SINGLE</span>
                                        </div>
                                        <div wire:click=""
                                            class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                            <span class="">MULTIPLE</span>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-span-6">
                            </div>
                            <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                                <h1 class="text-xl font-semibold text-left text-blue">Consignee Details
                                </h1>
                            </div>
                            <div class="col-span-12">
                                <div class="overflow-auto bg-white rounded-lg shadow-md">
                                    <x-table.table class="overflow-hidden">
                                        <x-slot name="thead">
                                            <x-table.th name="No." style="padding-left:;" />
                                            <x-table.th name="Consignee Name" style="padding-left:%;" />
                                            <x-table.th name="Company" style="padding-left;" />
                                            <x-table.th name="Contact Person" style="padding-left:;" />
                                            <x-table.th name="Contact Number" style="padding-left:;" />
                                            <x-table.th name="Email Address" style="padding-left:;" />
                                            <x-table.th name="Customer Type" style="padding-left:;" />
                                            <x-table.th name="Drop Off Location" style="padding-left:;" />
                                        </x-slot>
                                        <x-slot name="tbody" class="text-xs">
                                            @if (isset($vconsignee[0]))
                                                @foreach ($vconsignee[0] as $i => $viewcons)
                                                    <tr
                                                        class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-semibold">
                                                        <td class="p-3 whitespace-nowrap" style="padding-left:;">
                                                            {{ $i + 1 }}.
                                                        </td>
                                                        <td class="w-1/5 p-3 whitespace-nowrap"
                                                            style="padding-left:%;">
                                                            <span
                                                                wire:click="actionvcdet({'id':{{ $vconsignee[0][$i]['id'] }}},'c_det') }}"
                                                                class="underline cursor-pointer text-blue">{{ $vconsignee[0][$i]['name'] }}</span>
                                                        </td>
                                                        <td class="w-1/5 p-3 whitespace-nowrap"
                                                            style="padding-left:;">
                                                            {{ $vconsignee[0][$i]['company_name'] ?? '-' }}
                                                        </td>
                                                        <td class="w-1/5 p-3 whitespace-nowrap"
                                                            style="padding-left:;">
                                                            {{ $vconsignee[0][$i]['name'] }}
                                                        </td>
                                                        <td class="w-1/5 p-3 whitespace-nowrap"
                                                            style="padding-left:;">
                                                            {{ $vconsignee[0][$i]['mobile_number'] }}
                                                        </td>
                                                        <td class="w-1/5 p-3 whitespace-nowrap"
                                                            style="padding-left:;">
                                                            {{ $vconsignee[0][$i]['email_address'] }}
                                                        </td>
                                                        <td class="w-1/5 p-3 whitespace-nowrap"
                                                            style="padding-left:;">
                                                            {{ $vconsignee[0][$i]['account_type_id'] == 1 ? 'Individual' : 'Corporate' }}
                                                        </td>
                                                        <td class="w-1/5 p-3 whitespace-nowrap"
                                                            style="padding-left:;">
                                                            {{ $vconsignee[0][$i]['address'] }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </x-slot>
                                    </x-table.table>
                                </div>
                            </div>
                        </div>
                    </form>
                </x-slot>
            </x-modal>
        @endif

        @if ($consigneedet_modal)
            <x-modal id="consigneedet_modal" size="w-2/5">
                <x-slot name="body">
                    <form>
                        <div class="col-span-12 p-6 text-left">
                            <div class="">
                                <table class="overflow-hidden">
                                    {{-- <x-slot name="tbody"> --}}
                                    <div class="col-span-12 mb-4 border-0 border-b-2 border-blue">
                                        <h1 class="mb-1 text-xl font-semibold text-left text-blue">Cargo Details
                                        </h1>
                                    </div>

                                    <tr class="font-normal bg-white border-none">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Quantity :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            @php $qtycnt = 0; @endphp
                                            @foreach ($VBCD as $v => $vcd)
                                                @php $qtycnt += $VBCD[$v]['quantity']; @endphp
                                            @endforeach
                                            <table>
                                                <tr>
                                                    <td>
                                                        {{ $qtycnt }}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-none">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Weight (Kg) :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            @php $qtycnt2 = 0; @endphp
                                            @foreach ($VBCD as $v => $vcd)
                                                @php $qtycnt2 += $VBCD[$v]['weight']; @endphp
                                            @endforeach
                                            <table>
                                                <tr>
                                                    <td>
                                                        {{ $qtycnt2 }}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white ">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Dimensions (LxWxH in cm) :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            @foreach ($VBCD as $v => $vcd)
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{ $VBCD[$v]['length'] }} X {{ $VBCD[$v]['width'] }} X
                                                            {{ $VBCD[$v]['height'] }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Declared Value :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXdecval }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Transport Mode :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXtransportmode }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Service Mode :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXservmode }}
                                        </td>

                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Description of Goods :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXdescgoods }}
                                        </td>
                                    </tr>
                                    <tr class="text-sm font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Mode of Payment :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXmodeofp }}
                                        </td>
                                    </tr>
                                    <tr class="text-sm font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Assigned Team :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            -
                                        </td>
                                    </tr>
                                    <tr class="text-sm font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Waybill Count :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXwcount }}
                                        </td>
                                    </tr>
                                    <tr class="text-sm font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Failed Activity Reason :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            -
                                        </td>
                                    </tr>
                                    {{-- </x-slot> --}}
                                </table>
                            </div>
                        </div>
                    </form>
                </x-slot>
            </x-modal>
        @endif

        @if ($bsl_modal)
            <x-modal id="bsl_modal" size="w-1/3">
                <x-slot name="body">
                    <div class="grid grid-cols-12 p-4">
                        <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                            <h1 class="text-lg font-semibold text-left text-blue">Booking Status Logs
                            </h1>
                        </div>
                        <div class="col-span-12 mb-4">
                            <table class="overflow-hidden">
                                {{-- <x-slot name="tbody"> --}}
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Booking Reference No. : <span
                                            class="text-lg font-semibold text-blue">{{ $bstatrefno }}</span>
                                    </td>
                                </tr>
                                {{-- </x-slot> --}}
                            </table>
                        </div>
                        <div class="col-span-12">
                            <div class="overflow-auto bg-white border rounded-lg shadow-md">
                                <x-table.table class="overflow-hidden">
                                    <x-slot name="thead">
                                        <x-table.th name="Booking Status" style="padding-left:2%;" />
                                        <x-table.th name="Date and Time" style="padding-left:%;" />

                                    </x-slot>
                                    <x-slot name="tbody">
                                        <tr
                                            class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-medium">
                                            <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:2%;">
                                                {{ $bstatstat }}
                                            </td>
                                            <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                                {{-- {{ $viewbookingdetailsconsignee[0][$i]['account_type_id'] == 1 ? 'Individual' : 'Corporate' }} --}}
                                                {{ date('m/d/Y h:i A', strtotime($bstatcreated)) }}
                                            </td>
                                        </tr>
                                    </x-slot>
                                </x-table.table>
                            </div>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endif
    </x-slot>

    <x-slot name="header_title">Dispatch Console</x-slot>
    <x-slot name="header_button">
        <button x-data="{ isShow: {{ $isShow }} }" x-show="isShow == 1" wire:click="action({},'dispatch')"
            class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue hover:bg-blue-900">
            <div class="flex items-start justify-between">
                <svg class="mr-2 " style="margin-top: 0.8px" xmlns="http://www.w3.org/2000/svg" width="16"
                    height="16" viewBox="0 0 16 16">
                    <path id="Icon_awesome-pen" data-name="Icon awesome-pen"
                        d="M8.087,2.594l3.561,3.561L3.915,13.888l-3.175.35A.668.668,0,0,1,0,13.5l.353-3.177,7.73-7.73Zm5.764-.53L12.179.392a1.336,1.336,0,0,0-1.889,0L8.717,1.965l3.561,3.561,1.573-1.573a1.336,1.336,0,0,0,0-1.889Z"
                        transform="translate(0.502 0.611)" fill="none" stroke="currentColor" stroke-width="1" />
                </svg>
                <span>
                    Dispatch
                </span>
            </div>
        </button>
        <button x-data="{ isShow: {{ $isShow }} }" x-show="isShow == 1" wire:click=""
            class="p-2 px-3 mr-3 text-sm bg-white border-2 rounded-md text-blue border-blue hover:bg-blue-900 hover:text-white">
            <div class="flex items-start justify-between">
                <svg class="mr-2 " style="margin-top: 0.8px" xmlns="http://www.w3.org/2000/svg" width="16"
                    height="16" viewBox="0 0 16 16">
                    <path id="Icon_ionic-md-checkmark-circle-outline"
                        data-name="Icon ionic-md-checkmark-circle-outline"
                        d="M8.052,9.8l-1.11,1.11,3.567,3.567,7.927-7.927-1.11-1.11-6.817,6.777ZM17.643,11.3a6.314,6.314,0,1,1-4.6-6.1L14.274,3.97A7.373,7.373,0,0,0,11.3,3.375,7.927,7.927,0,1,0,19.229,11.3Z"
                        transform="translate(-3.375 -3.375)" fill="currentColor" />
                </svg>
                Auto Dispatch
            </div>
        </button>

        <div x-data="{ isShow: {{ $isShow }} }" x-show="isShow == 2" class="flex">
            <button wire:click="action({}, 'create')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    Advance Dispatch Plan
                </div>
            </button>
            <div class="text-blue whitespace-nowrap" x-data="{ open: false }">
                <button @click="open = !open"
                    class="p-2 px-3 mr-3 text-sm bg-white border-2 border-blue-900 rounded-md text-blue hover:bg-blue-900 hover:text-white ">
                    <div class="flex items-start justify-between">
                        Legend
                        <svg class="w-3 h-3 mt-1 ml-2" aria-hidden="true" focusable="false" data-prefix="far"
                            data-icon="print-alt" role="img" x mlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 448 512">
                            <path fill="currentColor"
                                d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                        </svg>
                    </div>
                </button>
                <div name="box" class="absolute mt-2 p-2 gap-x-2 rounded-md grid grid-cols-2 border text-black text-left font-medium bg-[#FFFFFF] border-gray-500"
                x-show="open" @click.away="open = false" style="margin-left: -20rem; z-index: 999;">
                    <div class="col-span-1">
                        <div class="flex py-1">
                            <span>
                                <svg class="w-2 h-2 mt-2 ml-2" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="print-alt" role="img" x
                                    mlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="#B2CCFF" d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512z" />
                                </svg>
                            </span>
                            <span class="pl-2">
                                Confirmed for Pick up
                            </span>
                        </div>
                        <div class="flex py-1">
                            <span>
                                <svg class="w-2 h-2 mt-2 ml-2" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="print-alt" role="img" x
                                    mlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="#4784FF" d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512z" />
                                </svg>
                            </span>
                            <span class="pl-2 text-left">
                                Started Pick up
                            </span>
                        </div>
                        <div class="flex py-1">
                            <span>
                                <svg class="w-2 h-2 mt-2 ml-2" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="print-alt" role="img" x
                                    mlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="#548ACE" d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512z" />
                                </svg>
                            </span>
                            <span class="pl-2 text-left">
                                Completed Pick up
                            </span>
                        </div>
                        <div class="flex py-1">
                            <span>
                                <svg class="w-2 h-2 mt-2 ml-2" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="print-alt" role="img" x
                                    mlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="#FFA7A7" d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512z" />
                                </svg>
                            </span>
                            <span class="pl-2 text-left">
                                Rescheduled
                            </span>
                        </div>
                        <div class="flex py-1">
                            <span>
                                <svg class="w-2 h-2 mt-2 ml-2" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="print-alt" role="img" x
                                    mlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="#F05959" d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512z" />
                                </svg>
                            </span>
                            <span class="pl-2 text-left">
                                Cancelled
                            </span>
                        </div>
                        <div class="flex py-1">
                            <span>
                                <svg class="w-2 h-2 mt-2 ml-2" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="print-alt" role="img" x
                                    mlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="#E9E9E9" d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512z" />
                                </svg>
                            </span>
                            <span class="pl-2 text-left">
                                Undelivered
                            </span>
                        </div>
                    </div>

                    <div class="col-span-1">
                        <div class="flex py-1">
                            <span>
                                <svg class="w-2 h-2 mt-2 ml-2" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="print-alt" role="img" x
                                    mlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="#C1FFB9" d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512z" />
                                </svg>
                            </span>
                            <span class="pl-2">
                                Pending Delivery
                            </span>
                        </div>
                        <div class="flex py-1">
                            <span>
                                <svg class="w-2 h-2 mt-2 ml-2" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="print-alt" role="img" x
                                    mlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="#55DE45" d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512z" />
                                </svg>
                            </span>
                            <span class="pl-2 text-left">
                                Started Delivery
                            </span>
                        </div>
                        <div class="flex py-1">
                            <span>
                                <svg class="w-2 h-2 mt-2 ml-2" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="print-alt" role="img" x
                                    mlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="#6BAF92" d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512z" />
                                </svg>
                            </span>
                            <span class="pl-2 text-left">
                                Completed Delivery
                            </span>
                        </div>
                        <div class="flex py-1">
                            <span>
                                <svg class="w-2 h-2 mt-2 ml-2" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="print-alt" role="img" x
                                    mlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="#DCFEFF" d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512z" />
                                </svg>
                            </span>
                            <span class="pl-2 text-left">
                                Travel Time
                            </span>
                        </div>
                        <div class="flex py-1">
                            <span>
                                <svg class="w-2 h-2 mt-2 ml-2" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="print-alt" role="img" x
                                    mlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="#A775FF" d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512z" />
                                </svg>
                            </span>
                            <span class="pl-2 text-left">
                                Break
                            </span>
                        </div>
                        <div class="flex py-1">
                            <span>
                                <svg class="w-2 h-2 mt-2 ml-2" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="print-alt" role="img" x
                                    mlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="#F05959"
                                        d="M432 256c0 17.7-14.3 32-32 32L48 288c-17.7 0-32-14.3-32-32s14.3-32 32-32l352 0c17.7 0 32 14.3 32 32z" />
                                </svg>
                            </span>
                            <span class="pl-2 text-left">
                                Idle
                            </span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </x-slot>
    <x-slot name="body">

        <div class="flex justify-between">
            <div class="flex pl-2 space-x-6">
                <div>
                    <div class="flex justify-between p-2 bg-white border border-black rounded-md shadow-md">
                        <div>
                            {{ date('F d, Y l') }}
                        </div>

                        <div class="pt-1 pl-4 ">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14.565" height="15.962"
                                viewBox="0 0 14.565 15.962">
                                <g id="Icon_feather-calendar" data-name="Icon feather-calendar"
                                    transform="translate(-4 -2.5)">
                                    <path id="Path_64" data-name="Path 64"
                                        d="M5.9,6h9.773a1.4,1.4,0,0,1,1.4,1.4v9.773a1.4,1.4,0,0,1-1.4,1.4H5.9a1.4,1.4,0,0,1-1.4-1.4V7.4A1.4,1.4,0,0,1,5.9,6Z"
                                        transform="translate(0 -1.604)" fill="none" stroke="#5d5d5d"
                                        stroke-linecap="round" stroke-linejoin="round" stroke-width="1" />
                                    <path id="Path_65" data-name="Path 65" d="M24,3V5.792"
                                        transform="translate(-10.425 0)" fill="none" stroke="#5d5d5d"
                                        stroke-linecap="round" stroke-linejoin="round" stroke-width="1" />
                                    <path id="Path_66" data-name="Path 66" d="M12,3V5.792"
                                        transform="translate(-4.01 0)" fill="none" stroke="#5d5d5d"
                                        stroke-linecap="round" stroke-linejoin="round" stroke-width="1" />
                                    <path id="Path_67" data-name="Path 67" d="M4.5,15H17.065"
                                        transform="translate(0 -6.415)" fill="none" stroke="#5d5d5d"
                                        stroke-linecap="round" stroke-linejoin="round" stroke-width="1" />
                                </g>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="flex gap-4" x-data="{ isShow: {{ $isShow }} }" x-show="isShow == 1">
                    @if ($actStatusHead == 1 || $actStatusHead == null)
                        <button wire:click="action({'id': {{ $branch_id }}}, 'pa')"
                            class="flex items-center justify-between gap-12 px-4 py-3 text-blue-800 bg-white border-2 border-blue-800 rounded-lg">
                            <h5 class="text-sm font-semibold ">Pick up Activity</h5>
                            <h5 class="text-2xl font-semibold ">{{ $bookingCount }}
                            </h5>
                        </button>
                        <button wire:click="action({'id': {{ $branch_id }}}, 'da')"
                            class="flex items-center justify-between gap-12 px-4 py-3 text-gray-800 bg-white border-2 border-gray-400 rounded-lg hover:border-blue-800 hover:border-2 hover:text-blue-800">
                            <h5 class="text-sm font-semibold ">Delivery Activity</h5>
                            <h5 class="text-2xl font-semibold ">{{ $transactionCount }}
                            </h5>
                        </button>
                    @elseif($actStatusHead == 2)
                        <button wire:click="action({'id': {{ $branch_id }}}, 'pa')"
                            class="flex items-center justify-between gap-12 px-4 py-3 text-gray-800 bg-white border-2 border-gray-400 rounded-lg hover:border-blue-800 hover:border-2 hover:text-blue-800">
                            <h5 class="text-sm font-semibold ">Pick up Activity</h5>
                            <h5 class="text-2xl font-semibold ">{{ $bookingCount }}
                            </h5>
                        </button>
                        <button wire:click="action({'id': {{ $branch_id }}}, 'da')"
                            class="flex items-center justify-between gap-12 px-4 py-3 text-blue-800 bg-white border-2 border-blue-800 rounded-lg">
                            <h5 class="text-sm font-semibold ">Delivery Activity</h5>
                            <h5 class="text-2xl font-semibold ">{{ $transactionCount }}
                            </h5>
                        </button>
                    @endif

                </div>
            </div>


            <div>
                <div class="inline-flex mr-4 rounded-md shadow-sm" role="group">
                    <button type="button"
                        class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-black bg-white border border-black shadow-md rounded-l-md hover:text-white hover:bg-blue-800">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                            viewBox="0 0 15.455 15.455">
                            <g id="Icon_feather-clock" data-name="Icon feather-clock"
                                transform="translate(-2.5 -2.5)">
                                <path id="Path_79" data-name="Path 79"
                                    d="M17.455,10.228A7.228,7.228,0,1,1,10.228,3,7.228,7.228,0,0,1,17.455,10.228Z"
                                    fill="none" stroke="currentColor" stroke-linecap="round"
                                    stroke-linejoin="round" stroke-width="1" />
                                <path id="Path_80" data-name="Path 80" d="M18,9v4.337l2.891,1.446"
                                    transform="translate(-7.772 -3.109)" fill="none" stroke="currentColor"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="1" />
                            </g>
                        </svg>
                    </button>
                    <button type="button"
                        class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-black bg-white border border-black shadow-md hover:text-white hover:bg-blue-800">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                            viewBox="0 0 15.984 12.654">
                            <g id="Icon_ionic-ios-list" data-name="Icon ionic-ios-list"
                                transform="translate(-4.5 -7.313)">
                                <path id="Path_70" data-name="Path 70"
                                    d="M6.831,17.2a1.165,1.165,0,1,1-1.165-1.165A1.165,1.165,0,0,1,6.831,17.2Z"
                                    transform="translate(0 -3.557)" fill="currentColor" />
                                <path id="Path_71" data-name="Path 71"
                                    d="M6.831,8.478A1.165,1.165,0,1,1,5.665,7.313,1.165,1.165,0,0,1,6.831,8.478Z"
                                    fill="currentColor" />
                                <path id="Path_72" data-name="Path 72"
                                    d="M6.831,25.915A1.165,1.165,0,1,1,5.665,24.75a1.165,1.165,0,0,1,1.165,1.165Z"
                                    transform="translate(0 -7.115)" fill="currentColor" />
                                <path id="Path_73" data-name="Path 73"
                                    d="M23.016,16.875H13.006a.666.666,0,1,0,0,1.332H23.016a.666.666,0,0,0,0-1.332Z"
                                    transform="translate(-3.199 -3.902)" fill="currentColor" />
                                <path id="Path_74" data-name="Path 74"
                                    d="M23.016,25.594H13.006a.666.666,0,1,0,0,1.332H23.016a.666.666,0,0,0,0-1.332Z"
                                    transform="translate(-3.199 -7.459)" fill="currentColor" />
                                <path id="Path_75" data-name="Path 75"
                                    d="M13.006,9.488H23.016a.666.666,0,0,0,0-1.332H13.006a.666.666,0,1,0,0,1.332Z"
                                    transform="translate(-3.199 -0.344)" fill="currentColor" />
                            </g>
                        </svg>
                    </button>
                    <button type="button"
                        class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-black bg-white border border-black shadow-md rounded-r-md hover:text-white hover:bg-blue-800">
                        <svg class="hover:text-white" xmlns="http://www.w3.org/2000/svg" width="20"
                            height="20" viewBox="0 0 14.92 13.654">
                            <g id="Icon_feather-map" data-name="Icon feather-map" transform="translate(-1 -2.5)">
                                <path id="Path_76" data-name="Path 76"
                                    d="M1.5,5.531V15.654l4.429-2.531,5.062,2.531,4.429-2.531V3L10.991,5.531,5.929,3Z"
                                    transform="translate(0 0)" fill="none" stroke="currentColor"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="1" />
                                <path id="Path_77" data-name="Path 77" d="M12,3V13.123"
                                    transform="translate(-6.071 0)" fill="none" stroke="currentColor"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="1" />
                                <path id="Path_78" data-name="Path 78" d="M24,9V19.123"
                                    transform="translate(-13.009 -3.469)" fill="none" stroke="currentColor"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="1" />
                            </g>
                        </svg>
                    </button>
                </div>
                <button type="button"
                    class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-black bg-white border border-black rounded-md shadow-md hover:text-white hover:bg-blue-800">
                    <svg fill="currentColor" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                        viewBox="0 0 13.299 13.299">
                        <path id="Icon_material-call-split" data-name="Icon material-call-split"
                            d="M14.312,6l1.9,1.9L13.821,10.3,15,11.477,17.4,9.084l1.9,1.9V6ZM10.987,6H6v4.987l1.9-1.9,3.915,3.907V19.3h1.662v-6.99L9.084,7.9Z"
                            transform="translate(-6 -6)" fill="currentColor" />
                    </svg>
                </button>
            </div>
        </div>
        <div class="flex space-x-4 ">
            <div class="flex-none gap-5 w-72">
                <div class="bg-white rounded-lg shadow-lg">
                    <div class="col-span-12 mb-6 ml-2 mr-2 ">
                        <br>&nbsp;
                        <div class="mx-auto">
                            <div class="relative mb-6" style="margin-top: -12%">
                                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                            stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                                    </svg>
                                </div>
                                <input type="search" id="default-search"
                                    class="block w-full h-4 p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-full focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Name or ID" wire:model="brnch" name="brnch" required>
                            </div>
                            @foreach ($getbranches as $a => $getbranch)
                                <ul class="divide-y-2 divide-transparent " x-data="{ selected: null }">
                                    <li class="relative">
                                        <button type="button" x-cloak x-show="selected != '0'"
                                            class="w-full px-8 py-1 text-left bg-white border rounded-md shadow-md hover:bg-blue-900"
                                            @click="selected !== 0 ? selected = 0 : selected = null"
                                            wire:click.defer="action({'id': {{ $getbranch->id }}}, 'showBranchteam')">
                                            <div
                                                class="flex items-center justify-between text-base font-normal text-left text-gray-600 uppercase hover:text-white">
                                                {{ $getbranch->name }}

                                                @if ($actStatusHead == 1 || $actStatusHead == null)
                                                    ({{ $getbranch->booking_has_many_count }})
                                                @elseif ($actStatusHead == 2)
                                                    ({{ $getbranch->transaction_has_many_count }})
                                                @endif
                                                <span style="margin-right:-10%;">
                                                    <svg x-cloak x-show="selected != '0'" class="w-5 h-4"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                                        <path fill="currentColor"
                                                            d="M137.4 406.6l-128-127.1C3.125 272.4 0 264.2 0 255.1s3.125-16.38 9.375-22.63l128-127.1c9.156-9.156 22.91-11.9 34.88-6.943S192 115.1 192 128v255.1c0 12.94-7.781 24.62-19.75 29.58S146.5 415.8 137.4 406.6z" />
                                                    </svg>
                                                    <svg x-cloak x-show="selected == '0'" class="w-5 h-4"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                                        <path fill="currentColor"
                                                            d="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z" />
                                                    </svg>
                                                </span>
                                            </div>
                                        </button>
                                        <button type="button" x-cloak x-show="selected == '0'"
                                            class="w-full px-8 py-1 text-left bg-blue-900 border rounded-md shadow-md hover:bg-gray-200"
                                            @click="selected !== 0 ? selected = 0 : selected = null">
                                            <div
                                                class="flex items-center justify-between text-base font-normal text-left text-white hover:text-gray-600">
                                                {{ $getbranch->name }}
                                                @if ($actStatusHead == 1 || $actStatusHead == null)
                                                    ({{ $getbranch->booking_has_many_count }})
                                                @elseif ($actStatusHead == 2)
                                                    ({{ $getbranch->transaction_has_many_count }})
                                                @endif
                                                <span style="margin-right:-10%;">
                                                    <svg x-cloak x-show="selected != '0'" class="w-5 h-4"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                                        <path fill="currentColor"
                                                            d="M137.4 406.6l-128-127.1C3.125 272.4 0 264.2 0 255.1s3.125-16.38 9.375-22.63l128-127.1c9.156-9.156 22.91-11.9 34.88-6.943S192 115.1 192 128v255.1c0 12.94-7.781 24.62-19.75 29.58S146.5 415.8 137.4 406.6z" />
                                                    </svg>
                                                    <svg x-cloak x-show="selected == '0'" class="w-5 h-4"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                                        <path fill="currentColor"
                                                            d="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z" />
                                                    </svg>
                                                </span>
                                            </div>
                                        </button>

                                        <div class="static mt-2 ml-4 overflow-hidden transition-all duration-700 max-h-0"
                                            style="" x-ref="container0"
                                            x-bind:style="selected == 0 ? 'max-height: ' + $refs
                                                .container0
                                                .scrollHeight + 'px' : ''">

                                            <div x-data="{ isShow: {{ $isShow }} }">

                                                <span x-show="isShow == 1">
                                                    <h4 wire:click="action({'id': {{ $getbranch->id }}, 'isshow': '1'}, 'activities')"
                                                        style="cursor:pointer"
                                                        class="flex items-start justify-start text-base font-medium text-left text-blue-800">

                                                        <svg class="" xmlns="http://www.w3.org/2000/svg"
                                                            width="17" height="17"
                                                            viewBox="0 0 16.551 15.046">
                                                            <path id="Icon_feather-activity"
                                                                data-name="Icon feather-activity"
                                                                d="M18.051,11.273h-3.01l-2.258,6.773L8.268,4.5,6.01,11.273H3"
                                                                transform="translate(-2.25 -3.75)" fill="none"
                                                                stroke="currentColor" stroke-linecap="round"
                                                                stroke-linejoin="round" stroke-width="1.5" />
                                                        </svg>

                                                        ACTIVITIES
                                                    </h4>
                                                    <h4 wire:click="action({'id': {{ $getbranch->id }}, 'isshow': '2'}, 'teams')"
                                                        style="cursor:pointer"
                                                        class="flex items-center justify-start text-base font-medium text-left text-gray-400 ">

                                                        <svg class="" xmlns="http://www.w3.org/2000/svg"
                                                            width="17" height="17"
                                                            viewBox="0 0 17.609 12.326">
                                                            <path id="Icon_awesome-users"
                                                                data-name="Icon awesome-users"
                                                                d="M2.641,7.533A1.761,1.761,0,1,0,.88,5.772,1.762,1.762,0,0,0,2.641,7.533Zm12.326,0a1.761,1.761,0,1,0-1.761-1.761A1.762,1.762,0,0,0,14.967,7.533Zm.88.88H14.087a1.756,1.756,0,0,0-1.241.512,4.024,4.024,0,0,1,2.066,3.01h1.816a.879.879,0,0,0,.88-.88v-.88A1.762,1.762,0,0,0,15.848,8.413Zm-7.043,0A3.081,3.081,0,1,0,5.723,5.331,3.08,3.08,0,0,0,8.8,8.413Zm2.113.88h-.228a4.254,4.254,0,0,1-3.769,0H6.691a3.17,3.17,0,0,0-3.17,3.17v.792a1.321,1.321,0,0,0,1.321,1.321h7.924a1.321,1.321,0,0,0,1.321-1.321v-.792A3.17,3.17,0,0,0,10.917,9.293ZM4.763,8.925a1.756,1.756,0,0,0-1.241-.512H1.761A1.762,1.762,0,0,0,0,10.174v.88a.879.879,0,0,0,.88.88H2.694A4.035,4.035,0,0,1,4.763,8.925Z"
                                                                transform="translate(0 -2.25)" fill="currentColor" />
                                                        </svg>

                                                        TEAMS
                                                    </h4>
                                                </span>
                                                <span x-show="isShow == 2">
                                                    <h4 wire:click="action({'id': {{ $getbranch->id }}, 'isshow': '1'}, 'activities')"
                                                        style="cursor:pointer"
                                                        class="flex items-center justify-start text-base font-medium text-left text-gray-400 ">
                                                        <svg class="mr-2" xmlns="http://www.w3.org/2000/svg"
                                                            width="17" height="17"
                                                            viewBox="0 0 16.551 15.046">
                                                            <path id="Icon_feather-activity"
                                                                data-name="Icon feather-activity"
                                                                d="M18.051,11.273h-3.01l-2.258,6.773L8.268,4.5,6.01,11.273H3"
                                                                transform="translate(-2.25 -3.75)" fill="none"
                                                                stroke="currentColor" stroke-linecap="round"
                                                                stroke-linejoin="round" stroke-width="1.5" />
                                                        </svg>
                                                        ACTIVITIES
                                                    </h4>
                                                    <h4 wire:click="action({'id': {{ $getbranch->id }}, 'isshow': '2'}, 'teams')"
                                                        style="cursor:pointer"
                                                        class="flex items-center justify-start text-base font-medium text-left text-blue-800">
                                                        <svg class="mr-2" xmlns="http://www.w3.org/2000/svg"
                                                            width="17" height="17"
                                                            viewBox="0 0 17.609 12.326">
                                                            <path id="Icon_awesome-users"
                                                                data-name="Icon awesome-users"
                                                                d="M2.641,7.533A1.761,1.761,0,1,0,.88,5.772,1.762,1.762,0,0,0,2.641,7.533Zm12.326,0a1.761,1.761,0,1,0-1.761-1.761A1.762,1.762,0,0,0,14.967,7.533Zm.88.88H14.087a1.756,1.756,0,0,0-1.241.512,4.024,4.024,0,0,1,2.066,3.01h1.816a.879.879,0,0,0,.88-.88v-.88A1.762,1.762,0,0,0,15.848,8.413Zm-7.043,0A3.081,3.081,0,1,0,5.723,5.331,3.08,3.08,0,0,0,8.8,8.413Zm2.113.88h-.228a4.254,4.254,0,0,1-3.769,0H6.691a3.17,3.17,0,0,0-3.17,3.17v.792a1.321,1.321,0,0,0,1.321,1.321h7.924a1.321,1.321,0,0,0,1.321-1.321v-.792A3.17,3.17,0,0,0,10.917,9.293ZM4.763,8.925a1.756,1.756,0,0,0-1.241-.512H1.761A1.762,1.762,0,0,0,0,10.174v.88a.879.879,0,0,0,.88.88H2.694A4.035,4.035,0,0,1,4.763,8.925Z"
                                                                transform="translate(0 -2.25)" fill="currentColor" />
                                                        </svg>
                                                        TEAMS
                                                    </h4>


                                                    <table class="ml-8 text-sm font-small">
                                                        {{-- @dd($getbranch) --}}

                                                        {{-- @if (isset($getbranches[$a]['TeamRouteAssignRef'])) --}}
                                                        @if (isset($getbranch['TeamRouteAssignRef']))
                                                            {{-- @dd($getbranches[$a]['TeamRouteAssignRef']['teamDetails']) --}}
                                                            {{-- @foreach ($getbranches[$a]['TeamRouteAssignRef']['teamDetails'] as $s => $getSweepersDetail) --}}
                                                            @foreach ($getbranch['TeamRouteAssignRef']['teamDetails'] as $s => $getSweepersDetail)
                                                                <tr>
                                                                    <td>
                                                                        <div class="flex items-center justify-between"
                                                                            x-data="{ open: false }">
                                                                            <span
                                                                                class="static cursor-pointer hover:text-blue-700"
                                                                                onmouseover="showSweeper('tooltip{{ $s }}')"
                                                                                onmouseout="hideSweeper('tooltip{{ $s }}')">
                                                                                <div class="absolute z-20 hidden w-auto p-1 py-1 text-black border border-gray-400 rounded-md bg-gray-50"
                                                                                    style="margin-top: -20%; margin-left: 35%; z-index:"
                                                                                    id="tooltip{{ $s }}">
                                                                                    <span class=" whitespace-nowrap">
                                                                                        <div class="flex">
                                                                                            <span
                                                                                                class="text-xs text-gray-500">Driver
                                                                                                :</span>
                                                                                            <span
                                                                                                class="ml-1 text-sm font-medium">
                                                                                                {{ $getSweepersDetail->teamDriverReference->name }}
                                                                                            </span>
                                                                                        </div>
                                                                                        <div class="flex mt-1">
                                                                                            <span
                                                                                                class="text-xs text-gray-500">Checker
                                                                                                :</span>
                                                                                            <span
                                                                                                class="ml-1 text-sm font-medium">
                                                                                                {{ $getSweepersDetail->teamChecker1Reference->name }}
                                                                                            </span>
                                                                                        </div>
                                                                                    </span>

                                                                                </div>
                                                                                <span
                                                                                    wire:click="action({'id': {{ $getSweepersDetail->id }}}, 'sweeper')">
                                                                                    {{ $getSweepersDetail->teamIdReference->name }}
                                                                                </span>
                                                                            </span>

                                                                            <span class=""
                                                                                style="margin-right: -55%">
                                                                                <svg class="cursor-pointer text-[#CACACA] hover:text-blue-700"
                                                                                    style=""
                                                                                    @click="open = !open"
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    width="17" height="17"
                                                                                    viewBox="0 0 576 512">
                                                                                    <path id="Icon_awesome-users"
                                                                                        data-name="Icon awesome-users"
                                                                                        d="M288 32c-80.8 0-145.5 36.8-192.6 80.6C48.6 156 17.3 208 2.5 243.7c-3.3 7.9-3.3 16.7 0 24.6C17.3 304 48.6 356 95.4 399.4C142.5 443.2 207.2 480 288 480s145.5-36.8 192.6-80.6c46.8-43.5 78.1-95.4 93-131.1c3.3-7.9 3.3-16.7 0-24.6c-14.9-35.7-46.2-87.7-93-131.1C433.5 68.8 368.8 32 288 32zM144 256a144 144 0 1 1 288 0 144 144 0 1 1 -288 0zm144-64c0 35.3-28.7 64-64 64c-7.1 0-13.9-1.2-20.3-3.3c-5.5-1.8-11.9 1.6-11.7 7.4c.3 6.9 1.3 13.8 3.2 20.7c13.7 51.2 66.4 81.6 117.6 67.9s81.6-66.4 67.9-117.6c-11.1-41.5-47.8-69.4-88.6-71.1c-5.8-.2-9.2 6.1-7.4 11.7c2.1 6.4 3.3 13.2 3.3 20.3z"
                                                                                        transform="translate(0 -2.25)"
                                                                                        fill="currentColor" />
                                                                                </svg>
                                                                                <div x-show="open" @click.away="open"
                                                                                    class="absolute z-20 col-span-4 p-2 pr-4 ml-8 bg-white border border-gray-400 rounded-sm "
                                                                                    style="margin-top:-10%; width: 115%">

                                                                                    <div
                                                                                        class="col-span-4 text-base font-medium">
                                                                                        {{ $getSweepersDetail->teamIdReference->name }}
                                                                                    </div>

                                                                                    <div class="flex col-span-4 mt-2">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Plate
                                                                                            Number :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            {{ $getSweepersDetail->teamPlateReference->plate_no }}
                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Vehicle
                                                                                            Type :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            Mitsubishi-dummy</span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Vehicle
                                                                                            Size :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            8W-dummy</span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Driver
                                                                                            :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            {{ $getSweepersDetail->teamDriverReference->name }}
                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Checker
                                                                                            1 :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            {{ $getSweepersDetail->teamChecker1Reference->name }}
                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Checker
                                                                                            2 :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            {{ $getSweepersDetail->teamChecker2Reference->name ?? null }}
                                                                                        </span>
                                                                                    </div>

                                                                                    <hr
                                                                                        class="mt-2 text-gray-300 border border-gray-300">


                                                                                    <div class="flex col-span-4 mt-2">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Pickup
                                                                                            :</span>
                                                                                        <span
                                                                                            class="w-full h-2.5 mt-1 ml-6 text-sm font-medium bg-gray-100 rounded-lg whitespace-nowrap">
                                                                                            <div class="h-2.5 rounded-lg"
                                                                                                style="width:50%;background: #548ACE 100%">

                                                                                            </div>
                                                                                        </span>
                                                                                        <span
                                                                                            class="ml-2 text-sm font-medium whitespace-nowrap">4/8
                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Delivery
                                                                                            :</span>
                                                                                        <span
                                                                                            class="w-full h-2.5 mt-1 ml-4 text-sm font-medium bg-gray-100 rounded-lg whitespace-nowrap">
                                                                                            <div class="h-2.5 rounded-lg"
                                                                                                style="width:40%;background: #6BAF92 100%">

                                                                                            </div>
                                                                                        </span>
                                                                                        <span
                                                                                            class="ml-2 text-sm font-medium whitespace-nowrap">3/8
                                                                                        </span>
                                                                                    </div>

                                                                                    <hr
                                                                                        class="mt-2 text-gray-300 border border-gray-300">

                                                                                    <div class="flex col-span-4 mt-2">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 ">Vehicle
                                                                                            Utilization
                                                                                            :</span>
                                                                                        <span
                                                                                            class="w-full h-2.5 mt-2 ml-2 text-sm font-medium bg-gray-100 rounded-lg whitespace-nowrap">
                                                                                            <div class="h-2.5 rounded-lg"
                                                                                                style="width:89%;background: linear-gradient(to right, #ff872c 0%, #FFDA00 50%, #90ee91 100%)">

                                                                                            </div>
                                                                                        </span>
                                                                                        <span
                                                                                            class="ml-2 text-sm font-medium whitespace-nowrap">
                                                                                            89%
                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Weight
                                                                                            Capacity
                                                                                            :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            13,350 / 15,00 Kg.
                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Volume
                                                                                            Capacity
                                                                                            :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            30 / 50 cbm
                                                                                        </span>
                                                                                    </div>

                                                                                    <hr
                                                                                        class="mt-2 text-gray-300 border border-gray-300">

                                                                                    <div class="flex col-span-4 mt-2">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 ">Activation
                                                                                            Time
                                                                                            :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            7:01 AM
                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Deactivation
                                                                                            Time
                                                                                            :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            6:30 PM
                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <button type="button"
                                                                                            style="background-color: #E9E9E9;"
                                                                                            class="p-0.5 px-2 text-xs font-medium border border-gray-400 rounded-sm shadow-md">See
                                                                                            e-DTR</button>
                                                                                    </div>

                                                                                </div>
                                                                            </span>

                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        {{-- <tr>
                                                            <td class="">
                                                                <div class="flex items-center justify-between ">
                                                                    <span class="cursor-pointer hover:text-blue-700">
                                                                        SWEEPER 2 (2 / 5)
                                                                    </span>
                                                                    <span class="cursor-pointer hover:text-blue-700"
                                                                        style="margin-right: -55%">
                                                                        <svg class="" style=""
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            width="17" height="17"
                                                                            viewBox="0 0 576 512">
                                                                            <path id="Icon_awesome-users"
                                                                                data-name="Icon awesome-users"
                                                                                d="M288 32c-80.8 0-145.5 36.8-192.6 80.6C48.6 156 17.3 208 2.5 243.7c-3.3 7.9-3.3 16.7 0 24.6C17.3 304 48.6 356 95.4 399.4C142.5 443.2 207.2 480 288 480s145.5-36.8 192.6-80.6c46.8-43.5 78.1-95.4 93-131.1c3.3-7.9 3.3-16.7 0-24.6c-14.9-35.7-46.2-87.7-93-131.1C433.5 68.8 368.8 32 288 32zM144 256a144 144 0 1 1 288 0 144 144 0 1 1 -288 0zm144-64c0 35.3-28.7 64-64 64c-7.1 0-13.9-1.2-20.3-3.3c-5.5-1.8-11.9 1.6-11.7 7.4c.3 6.9 1.3 13.8 3.2 20.7c13.7 51.2 66.4 81.6 117.6 67.9s81.6-66.4 67.9-117.6c-11.1-41.5-47.8-69.4-88.6-71.1c-5.8-.2-9.2 6.1-7.4 11.7c2.1 6.4 3.3 13.2 3.3 20.3z"
                                                                                transform="translate(0 -2.25)"
                                                                                fill="#CACACA" />
                                                                        </svg>
                                                                    </span>


                                                                </div>

                                                            </td>

                                                        </tr> --}}
                                                    </table>
                                                </span>
                                                <span x-show="isShow == 3">
                                                    <h4 wire:click="action({'id': {{ $getbranch->id }}, 'isshow': '1'}, 'activities')"
                                                        style="cursor:pointer"
                                                        class="flex items-center justify-start text-base font-medium text-left text-gray-400 ">
                                                        <svg class="mr-2" xmlns="http://www.w3.org/2000/svg"
                                                            width="17" height="17"
                                                            viewBox="0 0 16.551 15.046">
                                                            <path id="Icon_feather-activity"
                                                                data-name="Icon feather-activity"
                                                                d="M18.051,11.273h-3.01l-2.258,6.773L8.268,4.5,6.01,11.273H3"
                                                                transform="translate(-2.25 -3.75)" fill="none"
                                                                stroke="currentColor" stroke-linecap="round"
                                                                stroke-linejoin="round" stroke-width="1.5" />
                                                        </svg>
                                                        ACTIVITIES
                                                    </h4>
                                                    <h4 wire:click="action({'id': {{ $getbranch->id }}, 'isshow': '2'}, 'teams')"
                                                        style="cursor:pointer"
                                                        class="flex items-center justify-start text-base font-medium text-left text-blue-800">
                                                        <svg class="mr-2" xmlns="http://www.w3.org/2000/svg"
                                                            width="17" height="17"
                                                            viewBox="0 0 17.609 12.326">
                                                            <path id="Icon_awesome-users"
                                                                data-name="Icon awesome-users"
                                                                d="M2.641,7.533A1.761,1.761,0,1,0,.88,5.772,1.762,1.762,0,0,0,2.641,7.533Zm12.326,0a1.761,1.761,0,1,0-1.761-1.761A1.762,1.762,0,0,0,14.967,7.533Zm.88.88H14.087a1.756,1.756,0,0,0-1.241.512,4.024,4.024,0,0,1,2.066,3.01h1.816a.879.879,0,0,0,.88-.88v-.88A1.762,1.762,0,0,0,15.848,8.413Zm-7.043,0A3.081,3.081,0,1,0,5.723,5.331,3.08,3.08,0,0,0,8.8,8.413Zm2.113.88h-.228a4.254,4.254,0,0,1-3.769,0H6.691a3.17,3.17,0,0,0-3.17,3.17v.792a1.321,1.321,0,0,0,1.321,1.321h7.924a1.321,1.321,0,0,0,1.321-1.321v-.792A3.17,3.17,0,0,0,10.917,9.293ZM4.763,8.925a1.756,1.756,0,0,0-1.241-.512H1.761A1.762,1.762,0,0,0,0,10.174v.88a.879.879,0,0,0,.88.88H2.694A4.035,4.035,0,0,1,4.763,8.925Z"
                                                                transform="translate(0 -2.25)" fill="currentColor" />
                                                        </svg>
                                                        TEAMS
                                                    </h4>


                                                    <table class="ml-8 text-sm font-small">
                                                        @if (isset($getbranches[$a]['TeamRouteAssignRef']))
                                                            @foreach ($getbranches[$a]['TeamRouteAssignRef']['teamDetails'] as $g => $getSweepersDetail)
                                                                <tr>
                                                                    <td>
                                                                        <div class="flex items-center justify-between"
                                                                            x-data="{ open: false }">
                                                                            <span
                                                                                class="static cursor-pointer hover:text-blue-700"
                                                                                onmouseover="showSweeper('tooltip1{{ $g }}')"
                                                                                onmouseout="hideSweeper('tooltip1{{ $g }}')">
                                                                                <div class="absolute z-20 hidden w-auto p-1 py-1 text-black border border-gray-400 rounded-md bg-gray-50"
                                                                                    style="margin-top: -20%; margin-left: 35%; z-index:"
                                                                                    id="tooltip1{{ $g }}">
                                                                                    <span class=" whitespace-nowrap">
                                                                                        <div class="flex">
                                                                                            <span
                                                                                                class="text-xs text-gray-500">Driver
                                                                                                :</span>
                                                                                            <span
                                                                                                class="ml-1 text-sm font-medium">
                                                                                                {{ $getSweepersDetail->teamDriverReference->name }}

                                                                                            </span>
                                                                                        </div>
                                                                                        <div class="flex mt-1">
                                                                                            <span
                                                                                                class="text-xs text-gray-500">Checker
                                                                                                :</span>
                                                                                            <span
                                                                                                class="ml-1 text-sm font-medium">
                                                                                                {{ $getSweepersDetail->teamChecker1Reference->name }}

                                                                                            </span>
                                                                                        </div>
                                                                                    </span>

                                                                                </div>
                                                                                <span
                                                                                    wire:click="action({'id': {{ $getSweepersDetail->id }}}, 'sweeper')">

                                                                                    {{ $getSweepersDetail->teamIdReference->name }}

                                                                                </span>
                                                                            </span>

                                                                            <span class=""
                                                                                style="margin-right: -55%">
                                                                                <svg class="cursor-pointer text-[#CACACA] hover:text-blue-700"
                                                                                    style=""
                                                                                    @click="open = !open"
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    width="17" height="17"
                                                                                    viewBox="0 0 576 512">
                                                                                    <path id="Icon_awesome-users"
                                                                                        data-name="Icon awesome-users"
                                                                                        d="M288 32c-80.8 0-145.5 36.8-192.6 80.6C48.6 156 17.3 208 2.5 243.7c-3.3 7.9-3.3 16.7 0 24.6C17.3 304 48.6 356 95.4 399.4C142.5 443.2 207.2 480 288 480s145.5-36.8 192.6-80.6c46.8-43.5 78.1-95.4 93-131.1c3.3-7.9 3.3-16.7 0-24.6c-14.9-35.7-46.2-87.7-93-131.1C433.5 68.8 368.8 32 288 32zM144 256a144 144 0 1 1 288 0 144 144 0 1 1 -288 0zm144-64c0 35.3-28.7 64-64 64c-7.1 0-13.9-1.2-20.3-3.3c-5.5-1.8-11.9 1.6-11.7 7.4c.3 6.9 1.3 13.8 3.2 20.7c13.7 51.2 66.4 81.6 117.6 67.9s81.6-66.4 67.9-117.6c-11.1-41.5-47.8-69.4-88.6-71.1c-5.8-.2-9.2 6.1-7.4 11.7c2.1 6.4 3.3 13.2 3.3 20.3z"
                                                                                        transform="translate(0 -2.25)"
                                                                                        fill="currentColor" />
                                                                                </svg>
                                                                                <div x-show="open" @click.away="open"
                                                                                    class="absolute z-20 col-span-4 p-2 pr-4 ml-8 bg-white border border-gray-400 rounded-sm "
                                                                                    style="margin-top:-10%; width: 115%">

                                                                                    <div
                                                                                        class="col-span-4 text-base font-medium">
                                                                                        {{ $getSweepersDetail->teamIdReference->name }}

                                                                                    </div>

                                                                                    <div class="flex col-span-4 mt-2">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Plate
                                                                                            Number :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            {{ $getSweepersDetail->teamPlateReference->plate_no }}

                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Vehicle
                                                                                            Type :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            Mitsubishi-dummy</span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Vehicle
                                                                                            Size :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            8W-dummy</span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Driver
                                                                                            :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            {{ $getSweepersDetail->teamDriverReference->name }}

                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Checker
                                                                                            1 :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            {{ $getSweepersDetail->teamChecker1Reference->name }}

                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Checker
                                                                                            2 :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            {{ $getSweepersDetail->teamChecker2Reference->name ?? null }}

                                                                                        </span>
                                                                                    </div>

                                                                                    <hr
                                                                                        class="mt-2 text-gray-300 border border-gray-300">


                                                                                    <div class="flex col-span-4 mt-2">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Pickup
                                                                                            :</span>
                                                                                        <span
                                                                                            class="w-full h-2.5 mt-1 ml-6 text-sm font-medium bg-gray-100 rounded-lg whitespace-nowrap">
                                                                                            <div class="h-2.5 rounded-lg"
                                                                                                style="width:50%;background: #548ACE 100%">

                                                                                            </div>
                                                                                        </span>
                                                                                        <span
                                                                                            class="ml-2 text-sm font-medium whitespace-nowrap">4/8
                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Delivery
                                                                                            :</span>
                                                                                        <span
                                                                                            class="w-full h-2.5 mt-1 ml-4 text-sm font-medium bg-gray-100 rounded-lg whitespace-nowrap">
                                                                                            <div class="h-2.5 rounded-lg"
                                                                                                style="width:40%;background: #6BAF92 100%">

                                                                                            </div>
                                                                                        </span>
                                                                                        <span
                                                                                            class="ml-2 text-sm font-medium whitespace-nowrap">3/8
                                                                                        </span>
                                                                                    </div>

                                                                                    <hr
                                                                                        class="mt-2 text-gray-300 border border-gray-300">

                                                                                    <div class="flex col-span-4 mt-2">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 ">Vehicle
                                                                                            Utilization
                                                                                            :</span>
                                                                                        <span
                                                                                            class="w-full h-2.5 mt-2 ml-2 text-sm font-medium bg-gray-100 rounded-lg whitespace-nowrap">
                                                                                            <div class="h-2.5 rounded-lg"
                                                                                                style="width:89%;background: linear-gradient(to right, #ff872c 0%, #FFDA00 50%, #90ee91 100%)">

                                                                                            </div>
                                                                                        </span>
                                                                                        <span
                                                                                            class="ml-2 text-sm font-medium whitespace-nowrap">
                                                                                            89%
                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Weight
                                                                                            Capacity
                                                                                            :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            13,350 / 15,00 Kg.
                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Volume
                                                                                            Capacity
                                                                                            :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            30 / 50 cbm
                                                                                        </span>
                                                                                    </div>

                                                                                    <hr
                                                                                        class="mt-2 text-gray-300 border border-gray-300">

                                                                                    <div class="flex col-span-4 mt-2">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 ">Activation
                                                                                            Time
                                                                                            :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            7:01 AM
                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <span
                                                                                            class="text-xs text-gray-500 whitespace-nowrap">Deactivation
                                                                                            Time
                                                                                            :</span>
                                                                                        <span
                                                                                            class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                            6:30 PM
                                                                                        </span>
                                                                                    </div>

                                                                                    <div class="flex col-span-4">
                                                                                        <button type="button"
                                                                                            style="background-color: #E9E9E9;"
                                                                                            class="p-0.5 px-2 text-xs font-medium border border-gray-400 rounded-sm shadow-md">See
                                                                                            e-DTR</button>
                                                                                    </div>

                                                                                </div>
                                                                            </span>

                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </table>
                                                </span>
                                            </div>
                                    </li>
                                </ul>
                            @endforeach


                        </div>
                    </div>
                </div>

            </div>
            <div class="w-full">
                <div x-data="{ isShow: {{ $isShow }} }">
                    <span x-show="isShow == 1">
                        @if ($actStatusHead == 1 || $actStatusHead == null)

                            <div class="flex justify-between">
                                <div class="flex items-center justify-start">
                                    <ul class="flex">
                                        @foreach ($status_header_cards as $i => $status_card)
                                            <li
                                                class="px-4 py-1 text-xs {{ $status_header_cards[$i]['class'] }} {{ $stats == $status_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                                                <button
                                                    wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                                    {{ $status_header_cards[$i]['title'] }}
                                                    {{ $status_header_cards[$i]['value'] }}
                                                </button>
                                            </li>
                                        @endforeach
                                    </ul>

                                </div>
                                <div class="flex items-end justify-end pr-1 text-sm whitespace-nowrap">
                                    @if ($selCount != 0)
                                        Selected : <p class="pl-1 text-blue-800">{{ $selCount }}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="bg-white rounded-lg shadow-md ">
                                <table
                                    class="min-w-full overflow-auto max-h-[100px] border divide-y divide-gray-200 border-gray-50">
                                    <thead>
                                        <tr>
                                            <th class="px-4 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                <input class="cursor-pointer" type="checkbox" class="form-checkbox"
                                                    wire:model="selectAll">
                                            </th>

                                            <th class="px-4 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                No.</th>
                                            <th class="px-4 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                Booking Reference No</th>
                                            <th class="px-4 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                Shipper</th>
                                            <th class="px-4 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                Pick Up Address</th>
                                            <th class="px-4 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                Time Slot</th>
                                            <th class="px-4 py-2 text-[10px] font-semibold tracking-wider text-center"
                                                style="max-width: 90px">
                                                Work Instructions</th>
                                        </tr>
                                    </thead>
                                    <tbody class="overflow-auto bg-gray-100 divide-y-4 divide-white">
                                        <form>
                                            @foreach ($pickup_lists as $i => $pickup_list)
                                                <tr>
                                                    <td
                                                        class="px-4 py-3 text-xs font-semibold text-center whitespace-nowrap">
                                                        <input wire:model="selecteds" type="checkbox"
                                                            class="cursor-pointer" name={{ $pickup_list->id }}
                                                            value={{ $pickup_list->id }}>
                                                    </td>
                                                    <td
                                                        class="px-4 py-3 text-xs font-semibold text-center whitespace-nowrap">
                                                        <p class="">
                                                            {{ ($pickup_lists->currentPage() - 1) * $pickup_lists->links()->paginator->perPage() + $loop->iteration }}.
                                                        </p>
                                                    </td>
                                                    <td class="px-4 py-3 text-xs font-semibold text-center whitespace-nowrap"
                                                        style="">
                                                        <a class="text-blue-800 underline cursor-pointer">
                                                            <span
                                                                wire:click="actionbookingref({'id':{{ $pickup_list->id }}},'bref') }}"
                                                                class="underline cursor-pointer text-blue">{{ $pickup_list->booking_reference_no }}
                                                            </span>
                                                        </a>
                                                    </td>
                                                    <td class="px-4 py-3 text-xs font-semibold text-center whitespace-nowrap"
                                                        style="">
                                                        {{ $pickup_list->ShipperReferenceBK->fullname ?? null }}

                                                    </td>
                                                    <td
                                                        class="px-4 py-3 text-xs font-semibold text-center whitespace-nowrap">
                                                        {{ $pickup_list->BookingBranchReferenceBK->display }}
                                                    </td>
                                                    <td
                                                        class="px-4 py-3 text-xs font-semibold text-center whitespace-nowrap">
                                                        {{ $pickup_list->time_slot_to ?? '-' }}
                                                    </td>
                                                    <td
                                                        class="flex justify-center px-4 py-3 text-xs font-semibold text-center">
                                                        @if ($pickup_list->work_instruction != null)
                                                            <span title="View" class="cursor-pointer">
                                                                <svg wire:click="actionviewwork({'id':{{ $pickup_list->id }}},'workins')"
                                                                    xmlns="http://www.w3.org/2000/svg" width="12.75"
                                                                    height="17" viewBox="0 0 12.75 17">
                                                                    <path id="Icon_awesome-file-alt"
                                                                        data-name="Icon awesome-file-alt"
                                                                        d="M7.438,4.516V0H.8A.8.8,0,0,0,0,.8V16.2a.8.8,0,0,0,.8.8H11.953a.8.8,0,0,0,.8-.8V5.313H8.234A.8.8,0,0,1,7.438,4.516Zm2.125,7.836a.4.4,0,0,1-.4.4H3.586a.4.4,0,0,1-.4-.4v-.266a.4.4,0,0,1,.4-.4H9.164a.4.4,0,0,1,.4.4Zm0-2.125a.4.4,0,0,1-.4.4H3.586a.4.4,0,0,1-.4-.4V9.961a.4.4,0,0,1,.4-.4H9.164a.4.4,0,0,1,.4.4Zm0-2.391V8.1a.4.4,0,0,1-.4.4H3.586a.4.4,0,0,1-.4-.4V7.836a.4.4,0,0,1,.4-.4H9.164A.4.4,0,0,1,9.563,7.836ZM12.75,4.047v.2H8.5V0h.2a.8.8,0,0,1,.564.232l3.251,3.254A.794.794,0,0,1,12.75,4.047Z"
                                                                        fill="#039" />
                                                                </svg>

                                                            </span>
                                                        @else
                                                            <span title="View" class="cursor-pointer">
                                                                <svg wire:click="actionviewwork({'id':{{ $pickup_list->id }}},'no_workins')"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="12.75" height="17"
                                                                    viewBox="0 0 12.75 17">
                                                                    <path id="Icon_awesome-file-alt"
                                                                        data-name="Icon awesome-file-alt"
                                                                        d="M7.438,4.516V0H.8A.8.8,0,0,0,0,.8V16.2a.8.8,0,0,0,.8.8H11.953a.8.8,0,0,0,.8-.8V5.313H8.234A.8.8,0,0,1,7.438,4.516Zm2.125,7.836a.4.4,0,0,1-.4.4H3.586a.4.4,0,0,1-.4-.4v-.266a.4.4,0,0,1,.4-.4H9.164a.4.4,0,0,1,.4.4Zm0-2.125a.4.4,0,0,1-.4.4H3.586a.4.4,0,0,1-.4-.4V9.961a.4.4,0,0,1,.4-.4H9.164a.4.4,0,0,1,.4.4Zm0-2.391V8.1a.4.4,0,0,1-.4.4H3.586a.4.4,0,0,1-.4-.4V7.836a.4.4,0,0,1,.4-.4H9.164A.4.4,0,0,1,9.563,7.836ZM12.75,4.047v.2H8.5V0h.2a.8.8,0,0,1,.564.232l3.251,3.254A.794.794,0,0,1,12.75,4.047Z"
                                                                        fill="#039" />
                                                                </svg>
                                                            </span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </form>
                                    </tbody>
                                </table>
                                <div class="px-1 pb-2">
                                    {{ $pickup_lists->links() }}
                                </div>
                            </div>
                        @elseif ($actStatusHead == 2)
                            <div class="flex justify-between">
                                <div class="flex items-center justify-start">
                                    <ul class="flex">
                                        @foreach ($status_header_cards as $i => $status_card)
                                            <li
                                                class="px-4 py-1 text-xs {{ $status_header_cards[$i]['class'] }} {{ $stats == $status_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                                                <button
                                                    wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                                    {{ $status_header_cards[$i]['title'] }}
                                                    {{ $status_header_cards[$i]['value'] }}
                                                </button>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="flex items-end justify-end pr-1 text-sm whitespace-nowrap">
                                    @if ($selCount != 0)
                                        Selected : <p class="pl-2 text-blue-800">{{ $selCount }}</p>
                                    @endif
                                </div>

                            </div>
                            <div class="bg-white rounded-lg shadow-md ">
                                <table
                                    class="min-w-full overflow-auto max-h-[100px] border divide-y divide-gray-200 border-gray-50">
                                    <thead>
                                        <tr>
                                            <th
                                                class="px-3 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                <input class="cursor-pointer" type="checkbox"
                                                    class="form-checkbox" wire:model="selectAll">
                                            </th>

                                            <th
                                                class="px-3 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                No.</th>
                                            <th
                                                class="px-3 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                Waybill No</th>
                                            <th
                                                class="px-3 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                Shipper</th>
                                            <th
                                                class="px-3 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                Consignee</th>
                                            <th
                                                class="px-3 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                Delivery Address</th>
                                            <th
                                                class="px-3 py-2 text-[10px] font-semibold tracking-wider text-center">
                                                Time Slot</th>
                                            <th class="px-3 py-2 text-[10px] font-semibold tracking-wider text-center"
                                                style="max-width: 90px">
                                                Work Instructions</th>
                                        </tr>
                                    </thead>
                                    <tbody class="overflow-auto bg-gray-100 divide-y-4 divide-white">
                                        <form>
                                            @foreach ($pickup_lists as $i => $pickup_list)
                                                <tr>
                                                    <td
                                                        class="px-3 py-3 text-xs font-semibold text-center whitespace-nowrap">
                                                        <input wire:model="selecteds" type="checkbox"
                                                            class="cursor-pointer" name={{ $pickup_list->id }}
                                                            value={{ $pickup_list->id }}>
                                                    </td>
                                                    <td
                                                        class="px-3 py-3 text-xs font-semibold text-center whitespace-nowrap">
                                                        <p class="">
                                                            {{ ($pickup_lists->currentPage() - 1) * $pickup_lists->links()->paginator->perPage() + $loop->iteration }}.
                                                        </p>
                                                    </td>
                                                    <td class="px-3 py-3 text-xs font-semibold text-center whitespace-nowrap"
                                                        style="">
                                                        <a class="text-blue-800 underline cursor-pointer">
                                                            <span {{-- wire:click="actionbookingref({'id':{{ $pickup_list->id }}},'bref') }}" --}}
                                                                class="underline cursor-pointer text-blue">{{ $pickup_list->waybill }}
                                                            </span>
                                                        </a>
                                                    </td>
                                                    <td class="px-3 py-3 text-xs font-semibold text-center whitespace-nowrap"
                                                        style="">
                                                        {{ $pickup_list->ShipperReference->fullname }}
                                                    </td>
                                                    <td
                                                        class="px-3 py-3 text-xs font-semibold text-center whitespace-nowrap">
                                                        {{ $pickup_list->ConsigneeReference->fullname }}
                                                    </td>
                                                    <td
                                                        class="px-3 py-3 text-xs font-semibold text-center whitespace-nowrap">
                                                        {{ $pickup_list->DestinationReference->display }}
                                                    </td>
                                                    <td
                                                        class="px-3 py-3 text-xs font-semibold text-center whitespace-nowrap">
                                                        {{ $pickup_list->time_slot_to ?? '-' }}
                                                    </td>
                                                    <td
                                                        class="flex justify-center px-3 py-3 text-xs font-semibold text-center">
                                                        {{-- @dd($pickup_list['CrmBookingReference']['id']) --}}
                                                        @if ($pickup_list['CrmBookingReference']['work_instruction'] != null)
                                                            <span title="View" class="cursor-pointer">
                                                                <svg wire:click="actionviewwork({'id':{{ $pickup_list['CrmBookingReference']['id'] }}},'workins')"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="12.75" height="17"
                                                                    viewBox="0 0 12.75 17">
                                                                    <path id="Icon_awesome-file-alt"
                                                                        data-name="Icon awesome-file-alt"
                                                                        d="M7.438,4.516V0H.8A.8.8,0,0,0,0,.8V16.2a.8.8,0,0,0,.8.8H11.953a.8.8,0,0,0,.8-.8V5.313H8.234A.8.8,0,0,1,7.438,4.516Zm2.125,7.836a.4.4,0,0,1-.4.4H3.586a.4.4,0,0,1-.4-.4v-.266a.4.4,0,0,1,.4-.4H9.164a.4.4,0,0,1,.4.4Zm0-2.125a.4.4,0,0,1-.4.4H3.586a.4.4,0,0,1-.4-.4V9.961a.4.4,0,0,1,.4-.4H9.164a.4.4,0,0,1,.4.4Zm0-2.391V8.1a.4.4,0,0,1-.4.4H3.586a.4.4,0,0,1-.4-.4V7.836a.4.4,0,0,1,.4-.4H9.164A.4.4,0,0,1,9.563,7.836ZM12.75,4.047v.2H8.5V0h.2a.8.8,0,0,1,.564.232l3.251,3.254A.794.794,0,0,1,12.75,4.047Z"
                                                                        fill="#039" />
                                                                </svg>

                                                            </span>
                                                        @else
                                                            <span title="View" class="cursor-pointer">
                                                                <svg wire:click="actionviewwork({'id':{{ $pickup_list['CrmBookingReference']['id'] }}},'no_workins')"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="12.75" height="17"
                                                                    viewBox="0 0 12.75 17">
                                                                    <path id="Icon_awesome-file-alt"
                                                                        data-name="Icon awesome-file-alt"
                                                                        d="M7.438,4.516V0H.8A.8.8,0,0,0,0,.8V16.2a.8.8,0,0,0,.8.8H11.953a.8.8,0,0,0,.8-.8V5.313H8.234A.8.8,0,0,1,7.438,4.516Zm2.125,7.836a.4.4,0,0,1-.4.4H3.586a.4.4,0,0,1-.4-.4v-.266a.4.4,0,0,1,.4-.4H9.164a.4.4,0,0,1,.4.4Zm0-2.125a.4.4,0,0,1-.4.4H3.586a.4.4,0,0,1-.4-.4V9.961a.4.4,0,0,1,.4-.4H9.164a.4.4,0,0,1,.4.4Zm0-2.391V8.1a.4.4,0,0,1-.4.4H3.586a.4.4,0,0,1-.4-.4V7.836a.4.4,0,0,1,.4-.4H9.164A.4.4,0,0,1,9.563,7.836ZM12.75,4.047v.2H8.5V0h.2a.8.8,0,0,1,.564.232l3.251,3.254A.794.794,0,0,1,12.75,4.047Z"
                                                                        fill="#039" />
                                                                </svg>
                                                            </span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </form>
                                    </tbody>
                                </table>
                                <div class="px-1 pb-2">
                                    {{ $pickup_lists->links() }}
                                </div>
                            </div>
                        @endif

                    </span>
                    <span x-show="isShow == 2">
                        <div class="relative">
                            {{-- dynamic moving time--}}
                        @php
                            $start_time = strtotime('7:00 AM');
                            $current_time = time();
                            $end_time = strtotime('7:00 PM');
                            $total_span = ($end_time - $start_time) / 60;
                            $time_passed = ($current_time - $start_time) / 60;
                            $time_percentage = ($time_passed / $total_span) * 100;
                            $time_percentage_in_table = round(($time_percentage / 100), 2) * 80;
                            $total_percentage1 = round(($time_percentage_in_table + 19.5), 2);
                        @endphp
                            {{-- dynamic time javascript --}}
                        <div class="absolute text-red -translate-y-20" id="timestamp" style="position: absolute; left: {{ $total_percentage1 }}%; top: -20px; transform: translateX(-1.5rem)">|</div>

                            <div class="bg-white rounded-lg shadow-md">
                                <x-table.table class="table-fixed" width=100%>
                                    <x-slot name="thead" class="text-center">
                                        <x-table.th name="TEAMS" class="self-center text-center border border-slate-300" width=20% />
                                        @for ($hour = 7; $hour <= 18; $hour++)
                                            @php
                                                $formattedHour = $hour <= 12 ? $hour . ' AM' : $hour - 12 . ' PM';
                                            @endphp
                                            <x-table.th name="{{ $formattedHour }}" class="text-center border border-slate-300" width=6.67% />
                                        @endfor
                                    </x-slot>
                                    <x-slot name="tbody">

                                        {{-- @dd($teamidentifier) --}}
                                        {{-- @if (isset($getSweepersDetails)) --}}
                                        @if ($teamidentifier == 1)
                                            @foreach ($getSweepersDetails as $z => $getSweepersDetail)
                                                {{-- @dd({{if($getSweepersDetail['e_d_t_r_reference_count'] > 0)}}) --}}
                                                <tr class="bg-white border-none">
                                                    <td class="p-3 text-center border border-slate-300 whitespace-nowrap" width=6.67%>
                                                        {{ $getSweepersDetail->teamIdReference['name'] }}
                                                        {{-- @if ($getSweepersDetail['e_d_t_r_reference_count'] > 0)
                                                            {{ $getSweepersDetail['e_d_t_r_reference_count'] }}
                                                        @endif --}}
                                                    </td>
                                                    <td class="pb-2 border border-slate-300 whitespace-nowrap" width=6.67%>
                                                        @php
                                                            $start_time = strtotime('7:00 AM');
                                                            $current_time = time();
                                                            $end_time = strtotime('7:00 PM');
                                                            $total_span = ($end_time - $start_time) / 60;
                                                            $time_passed = ($current_time - $start_time) / 60;
                                                            $time_percentage = ($time_passed / $total_span) * 100;
                                                            $time_percentage_in_table = round(($time_percentage / 100), 2) * 80;
                                                            $total_percentage = round(($time_percentage_in_table + 19.5), 2);
                                                        @endphp
                                                    {{-- @dd($total_percentage) --}}
                                                    {{-- red line --}}
                                                    <div id="redLine" class="absolute top-0 bottom-0" style="left: {{ $total_percentage }}%; background-color: red; width: 2px;"></div>
                                                    

                                                        @if ($getSweepersDetail['e_d_t_r_reference_count'] > 0)
                                                            @foreach ($getSweepersDetail['EDTRReference'] as $o => $details)
                                                                @if ($getSweepersDetail['EDTRReference'][$o]['edtrDetails'][0]['edtr_item_type'] == 1)
                                                                {{-- booking travel time calculation --}}
                                                                    @php
                                                                        $actualStartTravelTimeString = $details->edtrDetails[0]['actual_start_travel_time'];
                                                                        $actualEndTravelTimeString = $details->edtrDetails[0]['actual_end_travel_time'];
                                                                    
                                                                        $actualStartTravelTime = new DateTime($actualStartTravelTimeString);
                                                                        $actualEndTravelTime = new DateTime($actualEndTravelTimeString);
                                                                    
                                                                        $travelTimeDifferenceInMinutes = $actualEndTravelTime->diff($actualStartTravelTime)->format('%i');
                                                                        $totalTravelDuration = 60; // minutes
                                                                    
                                                                        $travelTimeWidthPercentage = ($travelTimeDifferenceInMinutes / $totalTravelDuration) * 100;
                                                                    @endphp
                                                                    {{-- @dd($width_percentage) --}}
                                                                    <button style="width: {{$travelTimeWidthPercentage}}%;background-color:aqua"
                                                                        class="py-0.5 font-bold text-white static cursor-pointer"
                                                                        onmouseover="showTraveltime('hover{{ $o }}')"
                                                                        onmouseout="hideTraveltime('hover{{ $o }}')">
                                                                        {{-- travel time --}}
                                                                        <div class="absolute hidden z-20 col-span-4 mt-2 p-2.5 ml-8 text-black bg-white border border-gray-400 rounded-sm "
                                                                            id="hover{{ $o }}">
                                                                            <div
                                                                                class="col-span-4 text-base font-medium text-left">
                                                                                Travel Time
                                                                            </div>

                                                                            <div id='showTraveltime'
                                                                                class="flex col-span-4 mt-2">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Estimated
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    {{-- {{ $details->edtrDetails[0]['estimated_time_travel'] }} --}}
                                                                                    32mins.</span>
                                                                            </div>

                                                                            <div class="flex col-span-2 space-x-10">

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">Start
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                        1 AM</span>
                                                                                </div>

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">End
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                        1 AM</span>
                                                                                </div>

                                                                            </div>

                                                                            <hr
                                                                                class="mt-2 text-gray-300 border border-gray-300">

                                                                            <div class="flex col-span-4 mt-2">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Actual
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    45mins.</span>
                                                                            </div>

                                                                            <div class="flex col-span-2 space-x-10">

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">Start
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                        {{ date('H:i:s a', strtotime($details->edtrDetails[0]['actual_start_travel_time'] ))}}
                                                                                        </span>
                                                                                </div>

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">End
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                        {{ date('H:i:s a', strtotime($details->edtrDetails[0]['actual_end_travel_time'] ))}}

                                                                                        </span>
                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                    </button>
                                                                    {{-- booking idle interval calculation --}}
                                                                    @php
                                                                        $StartIdleTimeString = $details->edtrDetails[0]['actual_end_travel_time'];
                                                                        $EndIdleTimeString = $details->edtrDetails[0]['pickup_start_time'];
                                                                    
                                                                        $StartIdleTime = new DateTime($StartIdleTimeString);
                                                                        $EndIdleTime = new DateTime($EndIdleTimeString);
                                                                    
                                                                        $idleTimeDifferenceInMinutes = $EndIdleTime->diff($StartIdleTime)->format('%i');
                                                                        $totalIdleDuration = 60; // minutes
                                                                    
                                                                        $idleTimeWidthPercentage = ($idleTimeDifferenceInMinutes / $totalIdleDuration) * 100;
                                                                    @endphp
                                                                    {{-- @dd($idleTimeWidthPercentage) --}}
                                                                    @if ($idleTimeWidthPercentage > 0)
                                                                    <button style="width: {{ $idleTimeWidthPercentage }}%;margin-left:-3px"
                                                                        class="font-bold text-white bg-red-500 border border-red-700 rounded hover:bg-red-700">
                                                                    </button>
                                                                    @endif
                                                                    {{-- booking interval calculation --}}
                                                                    @php
                                                                        $currentDateTime = new DateTime();
                                                                        $StartPickupTimeString = $details->edtrDetails[0]['pickup_start_time'];
                                                                        $EndPickupTimeString = $details->edtrDetails[0]['pickup_end_time'];
                                                                    
                                                                        $StartPickupTime = new DateTime($StartPickupTimeString);
                                                                        $EndPickupTime = new DateTime($EndPickupTimeString);
                                                                    
                                                                        $pickupTimeDifferenceInMinutes = $EndPickupTime->diff($StartPickupTime)->format('%i');
                                                                        $totalPickupDuration = 60; // minute
                                                                    
                                                                        $pickupTimeWidthPercentage = ($pickupTimeDifferenceInMinutes / $totalPickupDuration) * 100;

                                                                        if($currentDateTime > $StartPickupTime){
                                                                            $backgroundColor = '#548ACE';
                                                                        }elseif (($currentDateTime > $StartPickupTime) && ($currentDateTime < $EndPickupTime)) {
                                                                            $backgroundColor = '#4784FF';
                                                                        }else {
                                                                            $backgroundColor = '#B2CCFF';
                                                                        }

                                                                    @endphp
                                                                    {{-- @dd($pickupTimeWidthPercentage) --}}
                                                                    @if ($pickupTimeWidthPercentage > 0)
                                                                    <button style="width: {{ $pickupTimeWidthPercentage }}%;margin-left:-3px; background-color:{{ $backgroundColor }}"
                                                                        class="py-2.5 font-bold text-white bg-blue-500 border border-blue-700 rounded hover:bg-blue-700"
                                                                        onmouseover="showBRN('hovers{{ $o }}')"
                                                                        onmouseout="hideBRN('hovers{{ $o }}')">
                                                                        {{-- starter pickup (booking reference) --}}
                                                                        <div class="absolute z-20 hidden col-span-4 mt-2.5 text-black bg-white border border-gray-400 rounded-sm max-w-lg"
                                                                            id="hovers{{ $o }}"
                                                                            style="">
                                                                            <div class="col-span-4 mt-1.5">
                                                                                <span class="block w-auto p-2 text-xs font-normal text-white bg-blue-800">Booking Reference Number: {{ $details->edtrDetails[0]['edtrbook']['booking_reference_no'] }}</span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-4">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 ">Shipper
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium ">
                                                                                    {{ $details->edtrDetails[0]['edtrbook']['BookingShipper']['name'] }}</span>
                                                                            </div>

                                                                            {{-- address --}}
                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span class="text-xs font-normal text-gray-500">Pick up Address:</span>
                                                                                <span class="ml-1 text-sm font-medium flex w-full whitespace-pre-wrap text-left">{{ $details->edtrDetails[0]['edtrbook']['BookingShipper']['address'] }}</span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 ">Time
                                                                                    Slot
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium ">

                                                                                    {{-- {{$details->edtrDetails[0]['edtrbook']}} --}}1
                                                                                    PM</span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 ">Start
                                                                                    Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium ">
                                                                                    {{ date('H:i:s a', strtotime($details->edtrDetails[0]['pickup_start_time'] ))}}

                                                                                    </span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 ">End
                                                                                    Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium ">
                                                                                    {{ date('H:i:s a', strtotime($details->edtrDetails[0]['pickup_end_time'] ))}}

                                                                                    </span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 ">Estimated
                                                                                    Completion Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium ">
                                                                                    15 mins.</span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 ">Actual
                                                                                    Completion Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium ">
                                                                                    16 mins.</span>
                                                                            </div>
                                                                        </div>
                                                                    </button>
                                                                    @endif
                                                                {{-- for booking to waybill travel idle interval calculation --}}
                                                                    @php
                                                                    $StartIdleTimeString1 = $details->edtrDetails[0]['pickup_end_time'];
                                                                    @endphp
                                                                @endif
                                                                @if ($getSweepersDetail['EDTRReference'][$o]['edtrDetails'][0]['edtr_item_type'] == 2)
                                                                    {{-- booking to waybill travel idle interval calculation --}}
                                                                    @php
                                                                    $EndIdleTimeString1 = $details->edtrDetails[0]['actual_start_travel_time'];
                                                                
                                                                    $StartIdleTime1 = new DateTime($StartIdleTimeString1);
                                                                    $EndIdleTime1 = new DateTime($EndIdleTimeString1);
                                                                
                                                                    $idleTimeDifferenceInMinutes1 = $EndIdleTime1->diff($StartIdleTime1)->format('%i');
                                                                    $totalIdleDuration1 = 60; // minutes
                                                                
                                                                    $idleTimeWidthPercentage1 = ($idleTimeDifferenceInMinutes1 / $totalIdleDuration1) * 100;
                                                                @endphp
                                                                {{-- @dd($idleTimeWidthPercentage1) --}}
                                                                @if ($idleTimeWidthPercentage1 > 0)
                                                                <button style="width: {{ $idleTimeWidthPercentage1 }}%;margin-left:-3px"
                                                                    class="font-bold text-white bg-red-500 border border-red-700 rounded hover:bg-red-700">
                                                                </button>
                                                                @endif
                                                                    {{-- waybill travel time calculation --}}
                                                                    @php
                                                                        $waybillActualStartTravelTimeString = $details->edtrDetails[0]['actual_start_travel_time'];
                                                                        $waybillActualEndTravelTimeString = $details->edtrDetails[0]['actual_end_travel_time'];
                                                                    
                                                                        $waybillActualStartTravelTime = new DateTime($waybillActualStartTravelTimeString);
                                                                        $waybillActualEndTravelTime = new DateTime($waybillActualEndTravelTimeString);
                                                                    
                                                                        $waybillTravelTimeDifferenceInMinutes = $waybillActualEndTravelTime->diff($waybillActualStartTravelTime)->format('%i');
                                                                        $totalWaybillTravelTimeDuration = 60; // minutes
                                                                    
                                                                        $waybillTravelTimeWidthPercentage = ($waybillTravelTimeDifferenceInMinutes / $totalWaybillTravelTimeDuration) * 100;
                                                                    @endphp                                                                
                                                                    <button style="width: {{ $waybillTravelTimeWidthPercentage }}%;background-color:aqua"
                                                                        class="py-0.5 font-bold text-white static cursor-pointer"
                                                                        onmouseover="showTraveltime('hover{{ $o }}')"
                                                                        onmouseout="hideTraveltime('hover{{ $o }}')">
                                                                        {{-- @dd($o ) --}}
                                                                        {{-- travel time --}}
                                                                        <div class="absolute hidden z-20 col-span-4 mt-2 p-2.5 ml-8 text-black bg-white border border-gray-400 rounded-sm "
                                                                            id="hover{{ $o }}">
                                                                            <div
                                                                                class="col-span-4 text-base font-medium text-left">
                                                                                Travel Time
                                                                            </div>

                                                                            <div id='showTraveltime'
                                                                                class="flex col-span-4 mt-2">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Estimated
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    {{-- {{ $details->edtrDetails[0]['estimated_time_travel'] }} --}}
                                                                                    32mins.</span>
                                                                            </div>

                                                                            <div class="flex col-span-2 space-x-10">

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">Start
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                        1 AM</span>
                                                                                </div>

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">End
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                        1 AM</span>
                                                                                </div>

                                                                            </div>

                                                                            <hr
                                                                                class="mt-2 text-gray-300 border border-gray-300">

                                                                            <div class="flex col-span-4 mt-2">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Actual
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    45mins.</span>
                                                                            </div>

                                                                            <div class="flex col-span-2 space-x-10">

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">Start
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    {{ date('H:i:s a', strtotime($details->edtrDetails[0]['actual_start_travel_time'] ))}}

                                                                                        </span>
                                                                                </div>

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">End
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    {{ date('H:i:s a', strtotime($details->edtrDetails[0]['actual_end_travel_time'] ))}}

                                                                                        </span>
                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                    </button>

                                                                        {{-- waybill idle interval calculation --}}
                                                                        @php
                                                                        $waybillStartIdleTimeString = $details->edtrDetails[0]['actual_end_travel_time'];
                                                                        $waybillEndIdleTimeString = $details->edtrDetails[0]['delivery_start_time'];
                                                                    
                                                                        $waybillStartIdleTime = new DateTime($waybillStartIdleTimeString);
                                                                        $waybillEndIdleTime = new DateTime($waybillEndIdleTimeString);
                                                                    
                                                                        $waybillIdleTimeDifferenceInMinutes = $waybillEndIdleTime->diff($waybillStartIdleTime)->format('%i');
                                                                        $waybillTotalIdleDuration = 60; // minutes
                                                                    
                                                                        $waybillIdleTimeWidthPercentage = ($waybillIdleTimeDifferenceInMinutes / $waybillTotalIdleDuration) * 100;
                                                                    @endphp
                                                                    {{-- @dd($waybillIdleTimeWidthPercentage) --}}
                                                                @if ($waybillIdleTimeWidthPercentage > 0)
                                                                    <button style="width: {{ $waybillIdleTimeWidthPercentage }}%;margin-left:-3px"
                                                                        class="font-bold text-white bg-red-500 border border-red-700 rounded hover:bg-red-700">
                                                                    </button>
                                                                    @endif
                                                                        {{-- waybill interval calculation --}}
                                                                    @php
                                                                        $currentDateTime = new DateTime();
                                                                        $StartDeliveryTimeString = $details->edtrDetails[0]['delivery_start_time'];
                                                                        $EndDeliveryTimeString = $details->edtrDetails[0]['delivery_end_time'];
                                                                    
                                                                        $StartDeliveryTime = new DateTime($StartDeliveryTimeString);
                                                                        $EndDeliveryTime = new DateTime($EndDeliveryTimeString);
                                                                    
                                                                        $deliveryTimeDifferenceInMinutes = $EndDeliveryTime->diff($StartDeliveryTime)->format('%i');
                                                                        $totalDeliveryDuration = 60; // minutes
                                                                    
                                                                        $deliveryTimeWidthPercentage = ($deliveryTimeDifferenceInMinutes / $totalDeliveryDuration) * 100;

                                                                        
                                                                        if($currentDateTime > $StartDeliveryTime){
                                                                            $backgroundColor = '#6BAF92';
                                                                        }elseif (($currentDateTime > $StartDeliveryTime) && ($currentDateTime < $EndDeliveryTime)) {
                                                                            $backgroundColor = '#55DE45';
                                                                        }else {
                                                                            $backgroundColor = '#C1FFB9';
                                                                        }

                                                                    @endphp
                                                                    {{-- @dd($deliveryTimeWidthPercentage) --}}
                                                                    <button
                                                                        style="width: {{ $deliveryTimeWidthPercentage }}%;margin-left:-3px;background-color:{{ $backgroundColor }}"
                                                                        class="py-2.5 font-bold text-white static rounded "
                                                                        onmouseover="showWN('show3{{$o}}')" onmouseout="hideWN('show3{{$o}}')">

                                                                        {{-- completed delivery (waybill) --}}
                                                                        <div class="absolute z-20 hidden col-span-4 mt-2.5 text-black bg-white border border-gray-400 rounded-sm max-w-lg"
                                                                            id="show3{{$o}}" style="">
                                                                            <div class="col-span-4 mt-1.5">
                                                                                <span class="block w-auto p-2 text-xs font-normal text-white bg-blue-800">Waybill Number: {{$details->edtrDetails[0]['edtrtransaction']['waybill']}}</span>
                                                                            </div>
                                                                            <div class="flex col-span-4 px-2 mt-4">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Shipper
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    {{$details->edtrDetails[0]['edtrtransaction']['ShipperReference']['fullname']}}</span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">

                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Consignee
                                                                                    :</span>
                                                                                <span class="ml-1 text-sm font-medium">
                                                                                    {{$details->edtrDetails[0]['edtrtransaction']['ConsigneeReference']['fullname']}}</span>
                                                                            </div>
                                                                            
                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span class="text-xs font-normal text-gray-500">Delivery Address :</span>
                                                                                <span class="ml-1 text-sm font-medium flex w-full whitespace-pre-wrap text-left"> {{$details->edtrDetails[0]['edtrtransaction']['consignee_address']}}</span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Instruction
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                </span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                {{-- @dd($details->edtrDetails[0]) --}}

                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Start
                                                                                    Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    {{ date('H:i:s a', strtotime($details->edtrDetails[0]['delivery_start_time'] ))}}
                                                                                    </span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">End
                                                                                    Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    {{ date('H:i:s a', strtotime($details->edtrDetails[0]['delivery_end_time'] ))}}
                                                                                </span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Estimated
                                                                                    Completion Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    15 mins.</span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Actual
                                                                                    Completion Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    16 mins.</span>
                                                                            </div>
                                                                        </div>
                                                                    </button>
                                                                    <button style="width: 40%;margin-left:-3px;background-color:aqua"
                                                                        class="py-0.5 font-bold text-white ">
                                                                    </button>
                                                                    <button style="width: 35%;margin-left:-3px"
                                                                        class="py-2.5 font-bold text-white bg-blue-500 border border-blue-700 rounded hover:bg-blue-700">
                                                                    </button>
                                                                    <button style="width: 40%;margin-left:-3px;background-color:aqua"
                                                                        class="py-0.5 font-bold text-white ">
                                                                    </button>
                                                                    <button style="width: 20%;margin-left:-3px"
                                                                        class="py-2.5 font-bold text-white bg-gray-400 border border-gray-400 rounded hover:bg-gray-740000">
                                                                    </button>
                                                                    <button style="width: 30%;margin-left:-3px;background-color:aqua"
                                                                        class="py-0.5 font-bold text-white ">
                                                                    </button>
                                                                    <button style="width: 50%;margin-left:-3px"
                                                                        class="py-2.5 font-bold text-white bg-blue-500 border border-blue-700 rounded hover:bg-blue-700">
                                                                    </button>
                                                                    <button style="width: 30%;margin-left:-3px"
                                                                        class="font-bold text-white bg-red-500 border border-red-700 rounded hover:bg-red-700">
                                                                    </button>
                                                                    <button style="width: 60%;margin-left:-3px;background-color:aqua"
                                                                        class="py-0.5 font-bold text-white ">
                                                                    </button>
                                                                    @php
                                                                        $actualStartBreakTimeString = $details['actual_start_break'];
                                                                        $actualEndBreakTimeString = $details['actual_end_break'];
                                                                        $actualStartBreakTime = new DateTime($actualStartBreakTimeString);
                                                                        $actualEndBreakTime = new DateTime($actualEndBreakTimeString);
                                                                    
                                                                        $breakTimeDifferenceInMinutes = $actualEndBreakTime->diff($actualStartBreakTime)->format('%i');
                                                                        $totalBreakDuration = 60; // minutes
                                                                    
                                                                        $breakTimeWidthPercentage = ($breakTimeDifferenceInMinutes / $totalBreakDuration) * 100;
                                                                    @endphp
                                                                    <button
                                                                        style="width: {{ $breakTimeWidthPercentage }}%;margin-left:-3px;margin-top:8px;background-color:#c084fc;vertical-align:middle"
                                                                        class="inline-flex items-center h-4 py-2.5 font-bold text-white rounded">
                                                                        <div style="display:block;margin-left: 40%">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="15"
                                                                                height="15" viewBox="0 0 17.833 17.055">
                                                                                <g id="Icon_feather-coffee"
                                                                                    data-name="Icon feather-coffee"
                                                                                    transform="translate(0.75 0.75)">
                                                                                    <path id="Path_81" data-name="Path 81"
                                                                                        d="M27,12h.778a3.111,3.111,0,0,1,0,6.222H27"
                                                                                        transform="translate(-14.556 -6.556)"
                                                                                        fill="none" stroke="currentColor"
                                                                                        stroke-linecap="round"
                                                                                        stroke-linejoin="round"
                                                                                        stroke-width="1.5" />
                                                                                    <path id="Path_82" data-name="Path 82"
                                                                                        d="M3,12H15.444v7a3.111,3.111,0,0,1-3.111,3.111H6.111A3.111,3.111,0,0,1,3,19Z"
                                                                                        transform="translate(-3 -6.556)"
                                                                                        fill="none" stroke="currentColor"
                                                                                        stroke-linecap="round"
                                                                                        stroke-linejoin="round"
                                                                                        stroke-width="1.5" />
                                                                                    <path id="Path_83" data-name="Path 83"
                                                                                        d="M9,1.5V3.833"
                                                                                        transform="translate(-5.889 -1.5)"
                                                                                        fill="none" stroke="currentColor"
                                                                                        stroke-linecap="round"
                                                                                        stroke-linejoin="round"
                                                                                        stroke-width="1.5" />
                                                                                    <path id="Path_84" data-name="Path 84"
                                                                                        d="M15,1.5V3.833"
                                                                                        transform="translate(-8.778 -1.5)"
                                                                                        fill="none" stroke="currentColor"
                                                                                        stroke-linecap="round"
                                                                                        stroke-linejoin="round"
                                                                                        stroke-width="1.5" />
                                                                                    <path id="Path_85" data-name="Path 85"
                                                                                        d="M21,1.5V3.833"
                                                                                        transform="translate(-11.667 -1.5)"
                                                                                        fill="none" stroke="currentColor"
                                                                                        stroke-linecap="round"
                                                                                        stroke-linejoin="round"
                                                                                        stroke-width="1.5" />
                                                                                </g>
                                                                            </svg>
                                                                        </div>
                                                                    </button>
                                                                    <button style="width: 30%;margin-left:-3px;background-color:green"
                                                                        class="py-2.5 font-bold text-white  rounded ">
                                                                    </button>
                                                                    <button style="width: 60%;margin-left:-3px;background-color:aqua"
                                                                        class="py-0.5 font-bold text-white ">
                                                                    </button>
                                                        @endif
                                                        @endforeach
                                                    @endif
                                                    </td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </x-slot>
                                </x-table.table>
                            </div>
                        </div>
                    </span>
                    {{-- ================================================================ --}}
                    
                    <span x-show="isShow == 3">
                        <div class="relative">
                            {{-- dynamic moving time--}}
                        @php
                            $start_time = strtotime('7:00 AM');
                            $current_time = time();
                            $end_time = strtotime('7:00 PM');
                            $total_span = ($end_time - $start_time) / 60;
                            $time_passed = ($current_time - $start_time) / 60;
                            $time_percentage = ($time_passed / $total_span) * 100;
                            $time_percentage_in_table = round(($time_percentage / 100), 2) * 80;
                            $total_percentage1 = round(($time_percentage_in_table + 19.5), 2);
                        @endphp
                            {{-- dynamic time javascript --}}
                        <div class="absolute text-red -translate-y-20" id="timestamp1" style="position: absolute; left: {{ $total_percentage1 }}%; top: -20px; transform: translateX(-1.5rem)">|</div>

                            <div class="bg-white rounded-lg shadow-md">
                                <x-table.table class="table-fixed" width=100%>
                                    <x-slot name="thead" class="text-center">
                                        <x-table.th name="TEAMS" class="self-center text-center border border-slate-300" width=20% />
                                        @for ($hour = 7; $hour <= 18; $hour++)
                                            @php
                                                $formattedHour = $hour <= 12 ? $hour . ' AM' : $hour - 12 . ' PM';
                                            @endphp
                                            <x-table.th name="{{ $formattedHour }}" class="text-center border border-slate-300" width=6.67% />
                                        @endfor
                                    </x-slot>
                                    <x-slot name="tbody">
                                        @foreach ($getSweepersDetails as $z => $getSweepersDetail)
                                        @foreach ($getSweepersDetail['EDTRReference'] as $d => $details)
                                        {{-- @dd($details) --}}
                                                <tr class="bg-white border-none">
                                                    <td class="p-3 text-center border border-slate-300 whitespace-nowrap" width=6.67%>
                                                        {{ $sweeperName }}
                                                    </td>
                                                    <td class="pb-2 border border-slate-300 whitespace-nowrap" width=6.67%>
                                                        @php
                                                            $start_time = strtotime('7:00 AM');
                                                            $current_time = time();
                                                            $end_time = strtotime('7:00 PM');
                                                            $total_span = ($end_time - $start_time) / 60;
                                                            $time_passed = ($current_time - $start_time) / 60;
                                                            $time_percentage = ($time_passed / $total_span) * 100;
                                                            $time_percentage_in_table = round(($time_percentage / 100), 2) * 80;
                                                            $total_percentage = round(($time_percentage_in_table + 19.5), 2);
                                                        @endphp
                                                    <div id="redLine" class="absolute top-0 bottom-0" style="left: {{ $total_percentage }}%; background-color: red; width: 2px;"></div>
                                                                {{-- booking travel time calculation --}}
                                                                    @php
                                                                        $actualStartTravelTimeString = $start1;
                                                                        $actualEndTravelTimeString = $end1;
                                                                    
                                                                        $actualStartTravelTime = new DateTime($actualStartTravelTimeString);
                                                                        $actualEndTravelTime = new DateTime($actualEndTravelTimeString);
                                                                    
                                                                        $travelTimeDifferenceInMinutes = $actualEndTravelTime->diff($actualStartTravelTime)->format('%i');
                                                                        $totalTravelDuration = 60; // minutes
                                                                    
                                                                        $travelTimeWidthPercentage = ($travelTimeDifferenceInMinutes / $totalTravelDuration) * 100;
                                                                    @endphp

                                                                    <button style="width: {{$travelTimeWidthPercentage}}%;background-color:aqua"
                                                                        class="py-0.5 font-bold text-white static cursor-pointer"
                                                                        onmouseover="showTraveltime1('hoverA{{ $d }}')"
                                                                        onmouseout="hideTraveltime1('hoverA{{ $d }}')">
                                                                        {{-- travel time --}}
                                                                        <div class="absolute hidden z-20 col-span-4 mt-2 p-2.5 ml-8 text-black bg-white border border-gray-400 rounded-sm "
                                                                            id="hoverA{{ $d }}">
                                                                            <div
                                                                                class="col-span-4 text-base font-medium text-left">
                                                                                Travel Time
                                                                            </div>

                                                                            <div id='showTraveltime'
                                                                                class="flex col-span-4 mt-2">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Estimated
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    {{-- {{ $details->edtrDetails[0]['estimated_time_travel'] }} --}}
                                                                                    32mins.</span>
                                                                            </div>

                                                                            <div class="flex col-span-2 space-x-10">

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">Start
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                        1 AM</span>
                                                                                </div>

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">End
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                        1 AM</span>
                                                                                </div>

                                                                            </div>

                                                                            <hr
                                                                                class="mt-2 text-gray-300 border border-gray-300">

                                                                            <div class="flex col-span-4 mt-2">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Actual
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    45mins.</span>
                                                                            </div>

                                                                            <div class="flex col-span-2 space-x-10">

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">Start
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                        {{ date('H:i:s a', strtotime($start1))}}
                                                                                        </span>
                                                                                </div>

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">End
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                        {{ date('H:i:s a', strtotime($end1))}}

                                                                                        </span>
                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                    </button>
                                                                    {{-- booking idle interval calculation --}}
                                                                    @php
                                                                    $StartIdleTimeString = $end1;
                                                                    $EndIdleTimeString = $bookStart;
                                                                
                                                                    $StartIdleTime = new DateTime($StartIdleTimeString);
                                                                    $EndIdleTime = new DateTime($EndIdleTimeString);
                                                                
                                                                    $idleTimeDifferenceInMinutes = $EndIdleTime->diff($StartIdleTime)->format('%i');
                                                                    $totalIdleDuration = 60; // minutes
                                                                
                                                                    $idleTimeWidthPercentage = ($idleTimeDifferenceInMinutes / $totalIdleDuration) * 100;
                                                                @endphp
                                                                
                                                                {{-- Check if $idleTimeWidthPercentage is greater than 0 before rendering the button --}}
                                                                @if ($idleTimeWidthPercentage > 0)
                                                                    <button style="width: {{ $idleTimeWidthPercentage }}%; margin-left: -3px"
                                                                            class="font-bold text-white bg-red-500 border border-red-700 rounded hover:bg-red-700">
                                                                    </button>
                                                                @endif

                                                                    {{-- booking interval calculation --}}
                                                                    @php
                                                                        $currentDateTime = new DateTime();
                                                                        $StartPickupTimeString = $bookStart;
                                                                        $EndPickupTimeString = $bookEnd;
                                                                    
                                                                        $StartPickupTime = new DateTime($StartPickupTimeString);
                                                                        $EndPickupTime = new DateTime($EndPickupTimeString);
                                                                    
                                                                        $pickupTimeDifferenceInMinutes = $EndPickupTime->diff($StartPickupTime)->format('%i');
                                                                        $totalPickupDuration = 60; // minute
                                                                    
                                                                        $pickupTimeWidthPercentage = ($pickupTimeDifferenceInMinutes / $totalPickupDuration) * 100;

                                                                        if ($currentDateTime < $StartPickupTime) {
                                                                            $backgroundColor = '#B2CCFF';
                                                                        } elseif ($currentDateTime >= $StartPickupTime && $currentDateTime <= $EndPickupTime) {
                                                                            $backgroundColor = '#4784FF';
                                                                        } else {
                                                                            $backgroundColor = '#548ACE';
                                                                        }
                                                                    @endphp
                                                                    {{-- @dd($pickupTimeWidthPercentage) --}}
                                                                    @if ($pickupTimeWidthPercentage > 0)
                                                                    <button style="width: {{ $pickupTimeWidthPercentage }}%;margin-left:-3px; background-color:{{ $backgroundColor }}"
                                                                        class="py-2.5 font-bold text-white bg-blue-500 border border-blue-700 rounded hover:bg-blue-700"
                                                                        onmouseover="showBRN1('hoverB{{ $d ?? null }}')"
                                                                        onmouseout="hideBRN1('hoverB{{ $d  }}')">
                                                                        {{-- starter pickup (booking reference) --}}
                                                                        <div class="absolute z-20 hidden col-span-4 mt-2.5 text-black bg-white border border-gray-400 rounded-sm max-w-lg"
                                                                            id="hoverB{{ $d  }}"
                                                                            style="">
                                                                            <div class="col-span-4 mt-1.5">
                                                                                <span class="block w-auto p-2 text-xs font-normal text-white bg-blue-800">Booking Reference Number:{{ $bookRef}}</span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-4">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 ">Shipper
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium ">
                                                                                    {{ $shipperName }}</span>
                                                                            </div>

                                                                            {{-- address --}}
                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span class="text-xs font-normal text-gray-500">Pick up Address:</span>
                                                                                <span class="ml-1 text-sm font-medium flex w-full whitespace-pre-wrap text-left">{{ $pickAddress }}</span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 ">Time
                                                                                    Slot
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium ">

                                                                                    {{-- {{$details->edtrDetails[0]['edtrbook']}} --}}1
                                                                                    PM</span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 ">Start
                                                                                    Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium ">
                                                                                    {{ date('H:i:s a', strtotime($bookStart))}}

                                                                                    </span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 ">End
                                                                                    Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium ">
                                                                                    {{ date('H:i:s a', strtotime($bookEnd))}}

                                                                                    </span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 ">Estimated
                                                                                    Completion Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium ">
                                                                                    15 mins.</span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 ">Actual
                                                                                    Completion Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium ">
                                                                                    16 mins.</span>
                                                                            </div>
                                                                        </div>
                                                                    </button>
                                                                    @endif
                                                                {{-- for booking to waybill travel idle interval calculation --}}
                                                                    @php
                                                                    $StartIdleTimeString1 = $bookEnd;
                                                                    @endphp
                                                                    {{-- booking to waybill travel idle interval calculation --}}
                                                                    @php
                                                                    $EndIdleTimeString1 = $start2;
                                                                
                                                                    $StartIdleTime1 = new DateTime($StartIdleTimeString1);
                                                                    $EndIdleTime1 = new DateTime($EndIdleTimeString1);
                                                                
                                                                    $idleTimeDifferenceInMinutes1 = $EndIdleTime1->diff($StartIdleTime1)->format('%i');
                                                                    $totalIdleDuration1 = 60; // minutes
                                                                
                                                                    $idleTimeWidthPercentage1 = ($idleTimeDifferenceInMinutes1 / $totalIdleDuration1) * 100;
                                                                @endphp
                                                                {{-- @dd($idleTimeWidthPercentage1) --}}
                                                                @if ($idleTimeWidthPercentage1 > 0)
                                                                <button style="width: {{ $idleTimeWidthPercentage1 }}%;margin-left:-3px"
                                                                    class="font-bold text-white bg-red-500 border border-red-700 rounded hover:bg-red-700">
                                                                </button>
                                                                @endif
                                                                    {{-- waybill travel time calculation --}}
                                                                    @php
                                                                        $waybillActualStartTravelTimeString = $start2;
                                                                        $waybillActualEndTravelTimeString = $end2;
                                                                    
                                                                        $waybillActualStartTravelTime = new DateTime($waybillActualStartTravelTimeString);
                                                                        $waybillActualEndTravelTime = new DateTime($waybillActualEndTravelTimeString);
                                                                    
                                                                        $waybillTravelTimeDifferenceInMinutes = $waybillActualEndTravelTime->diff($waybillActualStartTravelTime)->format('%i');
                                                                        $totalWaybillTravelTimeDuration = 60; // minutes
                                                                    
                                                                        $waybillTravelTimeWidthPercentage = ($waybillTravelTimeDifferenceInMinutes / $totalWaybillTravelTimeDuration) * 100;
                                                                    @endphp                                                                
                                                                    <button style="width: {{ $waybillTravelTimeWidthPercentage }}%;background-color:aqua"
                                                                        class="py-0.5 font-bold text-white static cursor-pointer"
                                                                        onmouseover="showTraveltime1('hoverC{{ $d  }}')"
                                                                        onmouseout="hideTraveltime1('hoverC{{ $d  }}')">
                                                                        {{-- @dd($d ) --}}
                                                                        {{-- travel time --}}
                                                                        <div class="absolute hidden z-20 col-span-4 mt-2 p-2.5 ml-8 text-black bg-white border border-gray-400 rounded-sm "
                                                                            id="hoverC{{ $d }}">
                                                                            <div
                                                                                class="col-span-4 text-base font-medium text-left">
                                                                                Travel Time
                                                                            </div>

                                                                            <div id='showTraveltime'
                                                                                class="flex col-span-4 mt-2">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Estimated
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    32mins.</span>
                                                                            </div>

                                                                            <div class="flex col-span-2 space-x-10">

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">Start
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                        1 AM</span>
                                                                                </div>

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">End
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                        1 AM</span>
                                                                                </div>

                                                                            </div>

                                                                            <hr
                                                                                class="mt-2 text-gray-300 border border-gray-300">

                                                                            <div class="flex col-span-4 mt-2">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Actual
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    45mins.</span>
                                                                            </div>

                                                                            <div class="flex col-span-2 space-x-10">

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">Start
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    {{ date('H:i:s a', strtotime($start2 ))}}
                                                                                        </span>
                                                                                </div>

                                                                                <div class="flex ">
                                                                                    <span
                                                                                        class="text-xs font-normal text-gray-500 whitespace-nowrap">End
                                                                                        :</span>
                                                                                    <span
                                                                                        class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    {{ date('H:i:s a', strtotime($end2 ))}}

                                                                                        </span>
                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                    </button>

                                                                        {{-- waybill idle interval calculation --}}
                                                                        @php
                                                                        $waybillStartIdleTimeString = $end2;
                                                                        $waybillEndIdleTimeString = $waybillStart;
                                                                    
                                                                        $waybillStartIdleTime = new DateTime($waybillStartIdleTimeString);
                                                                        $waybillEndIdleTime = new DateTime($waybillEndIdleTimeString);
                                                                    
                                                                        $waybillIdleTimeDifferenceInMinutes = $waybillEndIdleTime->diff($waybillStartIdleTime)->format('%i');
                                                                        $waybillTotalIdleDuration = 60; // minutes
                                                                    
                                                                        $waybillIdleTimeWidthPercentage = ($waybillIdleTimeDifferenceInMinutes / $waybillTotalIdleDuration) * 100;
                                                                    @endphp
                                                                    @if ($waybillIdleTimeWidthPercentage > 0)
                                                                    <button style="width: {{ $waybillIdleTimeWidthPercentage }}%;margin-left:-3px"
                                                                        class="font-bold text-white bg-red-500 border border-red-700 rounded hover:bg-red-700">
                                                                    </button>
                                                                    @endif
                                                                        {{-- waybill interval calculation --}}
                                                                    @php
                                                                        $currentDateTime = new DateTime();
                                                                        $StartDeliveryTimeString = $waybillStart;
                                                                        $EndDeliveryTimeString = $waybillEnd;
                                                                    
                                                                        $StartDeliveryTime = new DateTime($StartDeliveryTimeString);
                                                                        $EndDeliveryTime = new DateTime($EndDeliveryTimeString);
                                                                    
                                                                        $deliveryTimeDifferenceInMinutes = $EndDeliveryTime->diff($StartDeliveryTime)->format('%i');
                                                                        $totalDeliveryDuration = 60; // minutes
                                                                    
                                                                        $deliveryTimeWidthPercentage = ($deliveryTimeDifferenceInMinutes / $totalDeliveryDuration) * 100;

                                                                        if ($currentDateTime < $StartDeliveryTime) {
                                                                            $backgroundColorW = '#C1FFB9';
                                                                        } elseif ($currentDateTime >= $StartDeliveryTime && $currentDateTime <= $EndDeliveryTime) {
                                                                            $backgroundColorW = '#55DE45';
                                                                        } else {
                                                                            $backgroundColorW = '#6BAF92';
                                                                        }

                                                                    @endphp
                                                                    <button
                                                                        style="width: {{ $deliveryTimeWidthPercentage }}%;margin-left:-3px;background-color:{{ $backgroundColorW }}"
                                                                        class="py-2.5 font-bold text-white static rounded "
                                                                        onmouseover="showWN1('hoverD{{ $d  }}')" 
                                                                        onmouseout="hideWN1('hoverD{{ $d  }}')">

                                                                        {{-- completed delivery (waybill) --}}
                                                                        <div class="absolute z-20 hidden col-span-4 mt-2.5 text-black bg-white border border-gray-400 rounded-sm max-w-lg"
                                                                            id="hoverD{{$d}}" style="">
                                                                            <div class="col-span-4 mt-1.5">
                                                                                <span class="block w-auto p-2 text-xs font-normal text-white bg-blue-800">{{ $waybillNo }}</span>
                                                                                </span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-4">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Shipper
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    {{ $shipperRefName }}
                                                                                </span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">

                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Consignee
                                                                                    :</span>
                                                                                <span class="ml-1 text-sm font-medium">
                                                                                    {{ $consignee }}
                                                                                </span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span class="text-xs font-normal text-gray-500">Delivery Address :</span>
                                                                                <span class="ml-1 text-sm font-medium flex w-full whitespace-pre-wrap text-left">{{$consigneeAdd}}</span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Instruction
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                </span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                {{-- @dd($details->edtrDetails[0]) --}}

                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Start
                                                                                    Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    {{ date('H:i:s a', strtotime($waybillStart ))}}
                                                                                    </span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">End
                                                                                    Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    {{ date('H:i:s a', strtotime($waybillEnd ))}}
                                                                                </span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Estimated
                                                                                    Completion Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    15 mins.</span>
                                                                            </div>

                                                                            <div class="flex col-span-4 px-2 mt-1">
                                                                                <span
                                                                                    class="text-xs font-normal text-gray-500 whitespace-nowrap">Actual
                                                                                    Completion Time
                                                                                    :</span>
                                                                                <span
                                                                                    class="ml-1 text-sm font-medium whitespace-nowrap">
                                                                                    16 mins.</span>
                                                                            </div>
                                                                        </div>
                                                                    </button>
                                                        <button style="width: 40%;margin-left:-3px;background-color:aqua"
                                                            class="py-0.5 font-bold text-white ">
                                                        </button>
                                                        <button style="width: 35%;margin-left:-3px"
                                                            class="py-2.5 font-bold text-white bg-blue-500 border border-blue-700 rounded hover:bg-blue-700">
                                                        </button>
                                                        <button style="width: 40%;margin-left:-3px;background-color:aqua"
                                                            class="py-0.5 font-bold text-white ">
                                                        </button>
                                                        <button style="width: 20%;margin-left:-3px"
                                                            class="py-2.5 font-bold text-white bg-gray-400 border border-gray-400 rounded hover:bg-gray-740000">
                                                        </button>
                                                        <button style="width: 30%;margin-left:-3px;background-color:aqua"
                                                            class="py-0.5 font-bold text-white ">
                                                        </button>
                                                        <button style="width: 50%;margin-left:-3px"
                                                            class="py-2.5 font-bold text-white bg-blue-500 border border-blue-700 rounded hover:bg-blue-700">
                                                        </button>
                                                        <button style="width: 30%;margin-left:-3px"
                                                            class="font-bold text-white bg-red-500 border border-red-700 rounded hover:bg-red-700">
                                                        </button>
                                                        <button style="width: 60%;margin-left:-3px;background-color:aqua"
                                                            class="py-0.5 font-bold text-white ">
                                                        </button>
                                                        {{-- @php
                                                        $currentDateTime1 = new DateTime();
                                                        $StartBreakTimeString1 = $breakStart;
                                                        $EndBreakTimeString1 = $breakEnd;
                                                    
                                                        $StartBreakTime1 = new DateTime($StartBreakTimeString1);
                                                        $EndBreakTime1 = new DateTime($EndBreakTimeString1);
                                                    
                                                        $breakTimeDifferenceInMinutes1 = $EndBreakTime1->diff($StartBreakTime1)->format('%i');
                                                        $totalBreakDuration1 = 60; // minutes
                                                    
                                                        $breakTimeWidthPercentage1 = ($breakTimeDifferenceInMinutes1 / $totalBreakDuration1) * 100;

                                                    @endphp
                                                        <button
                                                            style="width: {{ $breakTimeWidthPercentage1 }}%;margin-left:-3px;margin-top:8px;background-color:#c084fc;vertical-align:middle"
                                                            class="inline-flex items-center h-4 py-2.5 font-bold text-white rounded">
                                                            <div style="display:block;margin-left: 40%">

                                                                <svg xmlns="http://www.w3.org/2000/svg" width="15"
                                                                    height="15" viewBox="0 0 17.833 17.055">
                                                                    <g id="Icon_feather-coffee"
                                                                        data-name="Icon feather-coffee"
                                                                        transform="translate(0.75 0.75)">
                                                                        <path id="Path_81" data-name="Path 81"
                                                                            d="M27,12h.778a3.111,3.111,0,0,1,0,6.222H27"
                                                                            transform="translate(-14.556 -6.556)"
                                                                            fill="none" stroke="currentColor"
                                                                            stroke-linecap="round"
                                                                            stroke-linejoin="round"
                                                                            stroke-width="1.5" />
                                                                        <path id="Path_82" data-name="Path 82"
                                                                            d="M3,12H15.444v7a3.111,3.111,0,0,1-3.111,3.111H6.111A3.111,3.111,0,0,1,3,19Z"
                                                                            transform="translate(-3 -6.556)"
                                                                            fill="none" stroke="currentColor"
                                                                            stroke-linecap="round"
                                                                            stroke-linejoin="round"
                                                                            stroke-width="1.5" />
                                                                        <path id="Path_83" data-name="Path 83"
                                                                            d="M9,1.5V3.833"
                                                                            transform="translate(-5.889 -1.5)"
                                                                            fill="none" stroke="currentColor"
                                                                            stroke-linecap="round"
                                                                            stroke-linejoin="round"
                                                                            stroke-width="1.5" />
                                                                        <path id="Path_84" data-name="Path 84"
                                                                            d="M15,1.5V3.833"
                                                                            transform="translate(-8.778 -1.5)"
                                                                            fill="none" stroke="currentColor"
                                                                            stroke-linecap="round"
                                                                            stroke-linejoin="round"
                                                                            stroke-width="1.5" />
                                                                        <path id="Path_85" data-name="Path 85"
                                                                            d="M21,1.5V3.833"
                                                                            transform="translate(-11.667 -1.5)"
                                                                            fill="none" stroke="currentColor"
                                                                            stroke-linecap="round"
                                                                            stroke-linejoin="round"
                                                                            stroke-width="1.5" />
                                                                    </g>
                                                                </svg>
                                                            </div>
                                                        </button> --}}
                                                        <button style="width: 30%;margin-left:-3px;background-color:green"
                                                            class="py-2.5 font-bold text-white  rounded ">
                                                        </button>
                                                        <button style="width: 60%;margin-left:-3px;background-color:aqua"
                                                            class="py-0.5 font-bold text-white ">
                                                        </button>

                                                    </td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                    <td class="border border-slate-300" width=6.67%></td>
                                                </tr>
                                                @endforeach
                                                @break
                                                @endforeach
                                            </x-slot>
                                </x-table.table>
                            </div>
                        </div>
                    </span>
                </div>

            </div>


    </x-slot>



</x-form>
<script type="text/javascript">
    function updateRedLine() {
        var timestampElement = document.getElementById('redLine');
    }
    updateRedLine();
    setInterval(updateRedLine, 60000);
    // ==========================================================
    function updateTimestamp() {
        var timestampElement = document.getElementById('timestamp');
        var currentTime = new Date();
        var startHour = 7;
        var endHour = 19;
        var totalHours = endHour - startHour;
        
        var elapsedHours = currentTime.getHours() - startHour + currentTime.getMinutes() / 60;
        var percentageElapsed = ((elapsedHours / totalHours) * 100);

        timestampElement.innerHTML = currentTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
    }

    updateTimestamp();
    setInterval(updateTimestamp, 1000);

    function updateTimestamp1() {
        var timestampElement = document.getElementById('timestamp1');
        var currentTime = new Date();
        var startHour = 7;
        var endHour = 19;
        var totalHours = endHour - startHour;
        
        var elapsedHours = currentTime.getHours() - startHour + currentTime.getMinutes() / 60;
        var percentageElapsed = ((elapsedHours / totalHours) * 100);

        timestampElement.innerHTML = currentTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
    }

    updateTimestamp1();
    setInterval(updateTimestamp1, 1000);
// =================================================
    function showSweeper(id) {
        $(`#${id}`).removeClass('hidden');
    }

    function hideSweeper(id) {
        $(`#${id}`).addClass('hidden');
    }


    function showTraveltime(id) {
        $(`#${id}`).removeClass('hidden');
    }

    function hideTraveltime(id) {
        $(`#${id}`).addClass('hidden');
    }

    function showBRN(id) {
        $(`#${id}`).removeClass('hidden');
    }

    function hideBRN(id) {
        $(`#${id}`).addClass('hidden');
    }

    function showWN(id) {
        $(`#${id}`).removeClass('hidden');
    }

    function hideWN(id) {
        $(`#${id}`).addClass('hidden');
    }

    function showTraveltime1(id) {
        $(`#${id}`).removeClass('hidden');
    }

    function hideTraveltime1(id) {
        $(`#${id}`).addClass('hidden');
    }

    function showBRN1(id) {
        $(`#${id}`).removeClass('hidden');
    }

    function hideBRN1(id) {
        $(`#${id}`).addClass('hidden');
    }

    function showWN1(id) {
        $(`#${id}`).removeClass('hidden');
    }

    function hideWN1(id) {
        $(`#${id}`).addClass('hidden');
    }
    </script>
