<x-form wire:init="" x-data="{
    {{-- create_modal: '{{ $create_modal }}', --}}
    {{-- edit_modal: '{{ $edit_modal }}', --}}
    {{-- print_modal: '{{ $print_modal }}', --}}
    {{-- share_modal: '{{ $share_modal }}', --}}
}">

    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
    </x-slot>

    <x-slot name="header_title" style="font-family-poppins: Poppins;">Vehicle Utilization</x-slot>

    <x-slot name="body">

        <div class="grid w-full grid-cols-7 gap-3 text-sm " style="margin-top: 2%;">
            <div class="w-full">
                <x-transparent.input style="font-size: 12px" type="text" onfocus="(this.type='date')"
                    onblur="(this.type='text')" label="" placeholder="MM/DD/YY" name=""
                    wire:model.defer="" />
            </div>

            <div class="w-full" wire:init="">
                <x-transparent.select value="" label="Branch/Location" name="" wire:model.defer="">
                    <option value=""></option>
                    {{-- @foreach ($routeReferences as $routeReference)
                    <option value="{{ $routeReference->id }}">
                        {{ $routeReference->name ?? '-' }}
                    </option>
                @endforeach --}}
                </x-transparent.select>
            </div>

            <div class="w-full" wire:init="">
                <x-transparent.select value="" label="Plate Number" name="" wire:model.defer="">
                    <option value=""></option>
                    {{-- @foreach ($plateReferences as $plateReference)
                    <option value="{{ $plateReference->id }}">
                        {{ $plateReference->plate_no }}
                    </option>
                @endforeach --}}
                </x-transparent.select>
            </div>

            {{-- <div class="w-full" wire:init="teamBranchReference">
            <x-transparent.select value="" label="Area" name="branch_id" wire:model.defer="branch_id">
                <option value=""></option>
                @foreach ($teamBranchReferences as $teamBranchReference)
                    <option value="{{ $teamBranchReference->id }}">
                        {{ $teamBranchReference->name }}
                    </option>
                @endforeach
            </x-transparent.select>
            </div>

            <div class="w-full" wire:init="driverReference">
            <x-transparent.select value="" label="Driver" name="driver_id" wire:model.defer="driver_id">
                <option value=""></option>
                @foreach ($driverReferences as $driverReference)
                    <option value="{{ $driverReference->user_id }}">
                        {{ $driverReference->first_name . ' ' . $driverReference->last_name }}
                    </option>
                @endforeach
            </x-transparent.select>
            </div> --}}

            <div class="w-full" wire:init="">
                <x-transparent.select value="" label="Vehicle Type" name="" wire:model.defer="">
                    <option value=""></option>
                    {{-- @foreach ($checkerReferences as $checkerReference)
                    <option value="{{ $checkerReference->user_id }}">
                        {{ $checkerReference->first_name . ' ' . $checkerReference->last_name }}
                    </option>
                @endforeach --}}
                </x-transparent.select>
            </div>



            <div class="w-full">
                <x-button type="button" wire:click="search" title="Search"
                    class="px-3 py-1.5 text-white bg-blue hover:bg-blue-800" />
            </div>
        </div>


        <div class="grid grid-cols-12 gap-x-4">
            <div class="col-span-2 p-2 bg-white rounded-t-md">
                <div class="grid justify-center">
                    <h1 class="text-3xl font-semibold text-center text-blue">100%</h1>
                    <h1 class="text-base font-semibold text-center">METRO MANILA</h1>
                </div>
            </div>

            <div class="col-span-2 p-2 bg-white rounded-t-md">
                <div class="grid justify-center">
                    <h1 class="text-3xl font-semibold text-center text-blue">98%</h1>
                    <h1 class="text-base font-semibold text-center">LUZON</h1>
                </div>
            </div>

            <div class="col-span-2 p-2 bg-white rounded-t-md">
                <div class="grid justify-center">
                    <h1 class="text-3xl font-semibold text-center text-blue">95%</h1>
                    <h1 class="text-base font-semibold text-center">VISAYAS</h1>
                </div>
            </div>

            <div class="col-span-2 p-2 bg-white rounded-t-md">
                <div class="grid justify-center">
                    <h1 class="text-3xl font-semibold text-center text-blue">95%</h1>
                    <h1 class="text-base font-semibold text-center">MINDANAO</h1>
                </div>
            </div>
            <div class="col-span-2 p-2 bg-white rounded-t-md">
                <div class="grid justify-center">
                    <h1 class="text-3xl font-semibold text-center text-blue">97%</h1>
                    <h1 class="text-base font-semibold text-center">Overall Ave. Utilization</h1>
                </div>
            </div>
            <div class="col-span-2 p-2">
                <div class="grid justify-center">
                </div>
            </div>



        </div>


        <div class="bg-white rounded-lg">
            <div class="w-full mx-auto">
                <div class="w-full overflow-x-auto">
                    <table class="w-full ">
                        <thead>
                            <tr>
                                <th class="left-0 py-1 text-xs font-medium tracking-wider text-left text-gray-500 bg-white border-r-2 border-gray-300 "
                                    colspan="">
                                </th>
                                <th class="px-3 bg-white py-2 text-center text-[10px] font-medium text-black  tracking-wider border-r-2 border-gray-300"
                                    style="background-color: #E0E0E0;" colspan="3">
                                    VEHICLE DETAILS</th>
                                <th class="px-3 bg-white py-2 text-center text-[10px] font-medium text-black  tracking-wider border-r-2 border-gray-300"
                                    style="background-color: #E0E0E0;" colspan="2">
                                    CAPACITY</th>
                                <th class="px-3 bg-white py-2 text-center text-[10px] font-medium text-black  tracking-wider border-r-2 border-gray-300"
                                    style="background-color: #E0E0E0;" colspan="2">
                                    ACTUAL PICK-UP UTILIZATION</th>

                                <th class="px-3 py-1 text-xs font-medium tracking-wider text-center" colspan="">
                                </th>
                            </tr>
                            <tr>
                                <th
                                    class="px-3 bg-white py-2 text-left text-[10px] font-normal text-black  tracking-wider border-r-2 border-gray-300">
                                    No.</th>
                                <th class="px-3 py-2 text-[10px] font-normal tracking-wider text-left text-black "
                                    style=" left:100px;">
                                    Plate Number</th>
                                <th
                                    class="px-3 py-2 text-[10px] font-normal whitespace-nowrap tracking-wider text-left text-black ">
                                    Vehicle Type</th>
                                <th class="px-3 bg-white py-2 text-left text-[10px] font-normal text-black  tracking-wider border-r-2 border-gray-300">
                                    Branch/Location</th>
                                <th
                                    class="px-3 py-2 text-[10px] font-normal whitespace-nowrap tracking-wider text-left text-black ">
                                    Weight Capacity (tons)</th>
                                <th class="px-3 bg-white py-2 text-left text-[10px] font-normal text-black  tracking-wider border-r-2 border-gray-300">
                                    Weight Capacity (cbm)</th>
                                <th class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                    Actual Weight (tons)</th>
                                <th class="px-3 bg-white py-2 text-left text-[10px] font-normal text-black  tracking-wider border-r-2 border-gray-300">
                                    Actual Volume (cbm)</th>
                                <th class="px-3 py-2 text-[10px] font-normal tracking-wider text-center text-black ">
                                    Utilization Performance %</th>
                            </tr>
                        </thead>
                        <tbody class="divide-y-4 divide-white ">
                            {{-- @foreach ($edtr_lists as $o => $edtr_list) --}}

                            <tr>
                                <td class="py-2 pl-4 text-xs font-medium border-r-2 border-gray-300 whitespace-nowrap"
                                    style="background-color: #F7F7F7; min-width:100px;">
                                    {{-- {{ ($edtr_lists->currentPage() - 1) * $edtr_lists->links()->paginator->perPage() + $loop->iteration }}. --}}1.
                                </td>
                                <td class="py-2 px-3 text-xs font-medium text-[#003399] underline whitespace-nowrap"
                                    style=" left:100px; background-color: #F7F7F7;">
                                    <span class="cursor-pointer" {{-- wire:click="actionedtrr({'id':{{ $edtr_list->id }}},'edtrr') }}" --}}>
                                        ALK 7122
                                        {{-- {{$edtr_list->reference_no}} --}}
                                    </span>
                                </td>
                                <td class="px-3 py-2 text-xs font-medium whitespace-nowrap"
                                    style="background-color: #F7F7F7;">
                                    4W Canter
                                    {{-- {{date('m/d/Y', strtotime($edtr_list->dispatch_date))}} --}}
                                </td>
                                <td class="px-3 py-2 text-xs font-medium border-r-2 border-gray-300"
                                    style="background-color: #F7F7F7;">
                                    MANILA
                                    {{-- {{$edtr_list->edtrBranch['name']}} --}}
                                </td>
                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                    style="background-color: #F7F7F7;">
                                    2.5
                                    {{-- {{$edtr_list->Traref->teamIdReference['name']}} --}}
                                </td>
                                <td class="px-3 py-2 text-xs font-medium text-center border-r-2 border-gray-300 whitespace-nowrap"
                                    style="background-color: #F7F7F7; min-width:100px;">
                                    8
                                    {{-- {{$edtr_list->edtr_details_count}} --}}
                                </td>
                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                    style="background-color: #F7F7F7; min-width:120px;">
                                    2.5

                                </td>
                                <td class="px-3 py-2 text-xs font-medium text-center border-r-2 border-gray-300 whitespace-nowrap"
                                    style="background-color: #F7F7F7; min-width:100px;">
                                    <span class="cursor-pointer" wire:click="actionpua({}, 'pua')">
                                        {{-- 58 --}}
                                        8
                                    </span>
                                </td>
                                {{-- @dd(count($edtr_list->edtrDetails)) --}}
                                <td class="px-3 py-2 text-xs font-medium text-center whitespace-nowrap"
                                    style="background-color: #F7F7F7; min-width:100px;">
                                    {{-- 5 --}}
                                    100%
                                </td>
                            </tr>
                            {{-- @endforeach --}}
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </x-slot>

</x-form>
