<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>
    @if($confirmation_modal)
    <x-modal id="confirmation_modal" size="w-auto" hasClose="1">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to submit this new Service Area Coverage?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
    @endif

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="col-span-12 py-1 mt-8 border-b-2 border-blue-800 ">
            <span class="px-12 py-2 text-sm text-white rounded-tr-lg bg-blue" style="border-top-right-radius: 100px">
                Address
            </span>
        </div>
        <div class="p-2 mt-5 space-y-3">
            <div class="grid grid-cols-12 gap-6">

                <div class="col-span-6">
                    <x-label for="isl" value="Island Group" :required="true" />
                    <div wire:init="island" class="flex items-center">
                        <x-select class="w-full h-10 rounded-md" style="cursor: pointer;" name="isl"
                            wire:model='isl'>
                            <option value="">Select</option>
                            @foreach ($island_references as $island_reference)
                                <option value="{{ $island_reference->id }}">
                                    {{ $island_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error class="absolute" for="isl" />
                </div>

                <div class="col-span-6">
                    <x-label for="reg" value="Region" :required="true" />
                    <div wire:init="region" class="flex items-center">
                        <x-select class="w-full h-10 rounded-md" style="cursor: pointer;" name="reg"
                            wire:model='reg'>
                            <option value="">Select</option>
                            @if ($isl != '')
                                @foreach ($region_references as $region_reference)
                                    <option value="{{ $region_reference->id }}">
                                        {{ $region_reference->name }}
                                    </option>
                                @endforeach
                            @endif
                        </x-select>
                    </div>
                    <x-input-error class="absolute" for="reg" />
                </div>

                <div class="col-span-6">
                    <x-label for="prov" value="Province" :required="true" />
                    <div wire:init="province" class="flex items-center">
                        <x-select class="w-full h-10 rounded-md" style="cursor: pointer;" name="prov"
                            wire:model='prov'>
                            <option value="">Select</option>
                            @if ($reg != '')
                                @foreach ($province_references as $province_reference)
                                    <option value="{{ $province_reference->id }}">
                                        {{ $province_reference->name }}
                                    </option>
                                @endforeach
                            @endif
                        </x-select>
                    </div>
                    <x-input-error class="absolute" for="prov" />
                </div>

                <div class="col-span-6">
                    <x-label for="mun" value="Municipality" :required="true" />
                    <div wire:init="municipal" class="flex items-center">
                        <x-select class="w-full h-10 rounded-md" style="cursor: pointer;" name="mun"
                            wire:model='mun'>
                            <option value="">Select</option>
                            @if ($prov != '')
                                @foreach ($municipal_references as $municipal_reference)
                                    <option value="{{ $municipal_reference->id }}">
                                        {{ $municipal_reference->name }}
                                    </option>
                                @endforeach
                            @endif
                        </x-select>

                    </div>
                    <x-input-error class="absolute" for="mun" />
                </div>

                <div class="col-span-6">
                    <x-label for="bar" value="Barangay" :required="true" />
                    <div wire:init="barangay" class="flex items-center">
                        <x-select class="w-full h-10 rounded-md" style="cursor: pointer;" name="bar"
                            wire:model='bar'>
                            <option value="">Select</option>
                            @if ($mun != '')
                                @foreach ($barangay_references as $barangay_reference)
                                    <option value="{{ $barangay_reference->id }}">
                                        {{ $barangay_reference->name }}
                                    </option>
                                @endforeach
                            @endif
                        </x-select>
                    </div>
                    <x-input-error class="absolute" for="bar" />
                </div>

                <div class="col-span-6">
                    <x-label for="zip" value="Zipcode" />
                    <x-input disabled type="text" name="zip" wire:model.defer='zip'></x-input>
                    <x-input-error class="absolute" for="zip" />
                </div>


                <div class="col-span-12">
                    <span class="flex justify-center py-2">
                        <hr class="w-4/5 text-black border-2 border-gray-300">
                    </span>
                </div>

                <div x-data="{ open: false }" class="relative col-span-12 mb-2 rounded-md" @click.away="open = false">
                    {{-- <x-label for="ptc" value="Port to Cater" :required="true" />
                    <div class="relative">
                        <div class="absolute inset-y-0 right-0 flex items-center pr-3 pointer-events-none">
                            <svg class="w-5 h-5 font-semibold cursor-pointer text-blue" aria-hidden="true" focusable="false"
                                data-prefix="far" data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 576 512">
                                <path fill="currentColor"
                                    d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z">
                                </path>
                            </svg>
                        </div>
                        <x-input type="text" name="ptc" wire:model.defer='ptc'></x-input>
                        <x-input-error class="absolute" for="ptc" />
                    </div> --}}
                    <div>
                        <x-label for="ptc" value="Port to Cater" :required="true" />
                        <div class="absolute inset-y-0 right-0 flex items-center pr-3 mt-6 pointer-events-none">
                            <svg class="w-5 h-5 font-semibold cursor-pointer text-blue" aria-hidden="true"
                                focusable="false" data-prefix="far" data-icon="edit" role="img"
                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                <path fill="currentColor"
                                    d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z">
                                </path>
                            </svg>
                        </div>
                        <x-input class="block w-full pr-10 rounded-md h-9" style="cursor: pointer;" type="text"
                            name="ptc" wire:model='ptc' @click="open = !open">
                        </x-input>
                        <x-input-error class="absolute" for="ptc" />

                    </div>
                    <div x-show="open" x-cloak
                        class="absolute w-full p-2 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96"
                        style="">
                        <ul class="list-reset">
                            @foreach ($ports as $i => $port)
                                <li @click="open = !open" wire:key="{{ 'ptc' . $i }}"
                                    wire:click="getPortDetails({{ $port->id }})"
                                    class="p-1 px-2 text-black cursor-pointer hover:bg-gray-200">
                                    <p>
                                        {{ $port->name }}
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="col-span-12">
                    <x-label for="serv" value="Serviceability" :required="true" />
                    <div wire:init="serviceability" class="flex items-center">
                        <x-select class="w-full h-10 rounded-md" style="cursor: pointer;" name="serv"
                            wire:model='serv'>
                            <option value="">Select</option>
                            @foreach ($service_references as $service_reference)
                                <option value="{{ $service_reference->id }}">
                                    {{ $service_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error class="absolute" for="serv" />
                </div>

                {{-- <div class="col-span-12">
                    <x-label for="odaopa" value="OPA/ODA/Outsource Cost Charge" />
                    <x-input type="text" name="odaopa" wire:model.defer='odaopa'></x-input>
                </div> --}}

            </div>
            <div class="flex justify-end gap-3 pt-8 space-x-3">
                <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                    class="px-12 bg-white text-blue hover:bg-gray-100" />
                <x-button type="submit" title="Submit" class="px-12 bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>
    </form>
</div>
