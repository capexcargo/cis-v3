<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>
    @if($confirmation_modal)
    <x-modal id="confirmation_modal" size="w-auto" hasClose="1">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to save this new Province?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
    @endif

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="p-2 pr-8 mt-5 space-y-3">
            <div class="grid grid-cols-12 gap-3 pr-3">
                <div class="col-span-11">
                    <x-label for="name" value="Province" :required="true" />
                    <input class="block w-full h-10 rounded-md" type="text" name="name"
                        wire:model.defer='name'></input>
                    <x-input-error for="name" />
                </div>
                <div class="col-span-1"></div>

                <div class="col-span-6">
                    <x-label for="reg" value="Region" :required="true" />
                    <div wire:init="regiReference" class="flex items-center">
                        <x-select class="w-full h-10 rounded-md" style="cursor: pointer;" name="reg"
                            wire:model='reg'>
                            <option value="">Select</option>
                            @foreach ($regReferences as $regReference)
                                <option value="{{ $regReference->id }}">
                                    {{ $regReference->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="reg" />
                </div>
                <div class="col-span-5">
                    <x-label for="isl" value="Island Group" />
                    <div wire:init="isleReference" class="flex items-center">
                        <x-select disabled class="w-full h-10 rounded-md" style="cursor: pointer;" name="isl"
                            wire:model='isl'>
                            <option value="">Select</option>
                            @foreach ($islReferences as $islReference)
                                <option value="{{ $islReference->id }}">
                                    {{ $islReference->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="isl" />
                </div>
                <div class="col-span-1"></div>

                <?php $i = 0; ?>
                @foreach ($provs as $a => $prov)
                    @if (!$prov['is_deleted'])
                        <div class="col-span-11">
                            @if ($min == $a)
                                <x-label for="provs.{{ $a }}.mun" value="Municipality" :required="true" />
                            @else
                            @endif
                            <div wire:init="muniReference" class="flex items-center">
                                <x-select class="w-full h-10 rounded-md" style="cursor: pointer;"
                                    name="provs.{{ $a }}.mun" wire:model='provs.{{ $a }}.mun'>
                                    <option value="">Select</option>
                                    @foreach ($munReferences as $munReference)
                                        <option value="{{ $munReference->id }}">
                                            {{ $munReference->name }}
                                        </option>
                                    @endforeach
                                </x-select>
                            </div>
                            <x-input-error for="provs.{{ $a }}.mun" />
                        </div>
                        <div class="col-span-1">
                            <div class="grid grid-cols-2">
                                @if ($nearest > $min)
                                    @if ($min == $a)
                                        <div class="col-span-1 mt-6"></div>
                                    @else
                                        <div class="col-span-1"></div>
                                    @endif
                                    <x-label for="process" value="" />
                                    <svg wire:click="removeProv({'a': {{ $a }}})"
                                        class="z-10 w-8 h-6 cursor-pointer text-red" aria-hidden="true"
                                        focusable="false" data-prefix="fas" data-icon="circle-minus" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                        <path fill="currentColor"
                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z">
                                        </path>
                                    </svg>
                                @endif

                                {{-- @if (count($provs) == 1)
                                    <div class="col-span-1 pr-4 mt-6">
                                        <x-label for="TellNo" value="" />
                                        <svg wire:click="addProv({{ $a + 1 }})"
                                            class="w-8 h-6 cursor-pointer text-blue" aria-hidden="true"
                                            focusable="false" data-prefix="fas" data-icon="trash-alt" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                            </path>
                                        </svg>
                                    </div>
                                @endif

                                @if ($i != 0)
                                    @if (count($provs) == $i + 1)
                                        <div class="col-span-1 ">
                                            <svg wire:click="addProv({{ $a + 1 }})"
                                                class="w-8 h-6 ml-6 cursor-pointer text-blue" aria-hidden="true"
                                                focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                role="img" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                </path>
                                            </svg>
                                        </div>
                                    @endif
                                @endif --}}
                                @if ($nearest == $a)
                                    @if ($min == $a)
                                        <div class="col-span-1 pr-4 mt-6">
                                            <x-label for="TellNo" value="" />
                                            <svg wire:click="addProv()" class="w-8 h-6 cursor-pointer text-blue"
                                                aria-hidden="true" focusable="false" data-prefix="fas"
                                                data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                </path>
                                            </svg>
                                        </div>
                                    @else
                                        <div class="col-span-1">
                                            <svg wire:click="addProv()" class="w-8 h-6 ml-6 cursor-pointer text-blue"
                                                aria-hidden="true" focusable="false" data-prefix="fas"
                                                data-icon="trash-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                </path>
                                            </svg>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    @endif
                    <?php $i++; ?>
                @endforeach
            </div>
        </div>
        <div class="flex justify-end gap-3 mt-6 space-x-3 " style="padding-right: 18%;">
            <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                class="px-12 bg-white text-blue hover:bg-gray-100" />
            <x-button type="submit" title="Save" class="px-12 bg-blue text-white hover:bg-[#002161]" />
        </div>
    </form>
</div>
