<x-form wire:init="load" x-data="{
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    reactivate_modal: '{{ $reactivate_modal }}',
    deactivate_modal: '{{ $deactivate_modal }}',
    view_modal: '{{ $view_modal }}',
}">

    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @if ($create_modal)
            @can('oims_order_management_t_e_dropdown_mgmt_branch_mgmt_add')
                <x-modal id="create_modal" size="w-1/3" hasClose="1">
                    <x-slot name="title">Add Branch</x-slot>
                    <x-slot name="body">
                        @livewire('oims.order-management.t-e-dropdown-mgmt.branch-mgmt.create')
                    </x-slot>
                </x-modal>
            @endcan
        @endif
        @if ($edit_modal)
            @can('oims_order_management_t_e_dropdown_mgmt_branch_mgmt_edit')
                @if ($branch_id && $edit_modal)
                    <x-modal id="edit_modal" size="w-1/3" hasClose="1">
                        <x-slot name="title">Edit branch</x-slot>
                        <x-slot name="body">
                            @livewire('oims.order-management.t-e-dropdown-mgmt.branch-mgmt.edit', ['id' => $branch_id])
                        </x-slot>
                    </x-modal>
                @endif
            @endcan
        @endif
        @if ($view_modal)
            @can('oims_order_management_t_e_dropdown_mgmt_branch_mgmt_view')
                @if ($branch_id && $view_modal)
                    <x-modal id="view_modal" size="w-2/5" hasClose="1">
                        <x-slot name="body">
                            @livewire('oims.order-management.t-e-dropdown-mgmt.branch-mgmt.view', ['id' => $branch_id])
                        </x-slot>
                    </x-modal>
                @endif
            @endcan
        @endif

        @can('oims_order_management_t_e_dropdown_mgmt_branch_mgmt_deactivate')
            @if ($reactivate_modal)
                <x-modal id="reactivate_modal" size="w-auto" hasClose="1">
                    <x-slot name="body">
                        <div class="flex flex-col items-center justify-center">
                            <h2 class="text-xl text-center">
                                Are you sure you want to reactivate this branch?
                            </h2>
                            <div class="flex justify-center space-x-3">
                                <button wire:click="$set('reactivate_modal', false)"
                                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">No</button>
                                <button wire:click="updateStatus('{{ $branch_id }}', 1)"
                                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">Reactivate</button>
                            </div>
                        </div>
                    </x-slot>
                </x-modal>
            @endif

            @if ($deactivate_modal)
                <x-modal id="deactivate_modal" size="w-auto" hasClose="1">
                    <x-slot name="body">
                        <div class="flex flex-col items-center justify-center">
                            <h2 class="text-xl text-center">
                                Are you sure you want to deactivate this branch?
                            </h2>
                            <div class="flex justify-center space-x-3">
                                <button wire:click="$set('deactivate_modal', false)"
                                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">No</button>
                                <button wire:click="updateStatus('{{ $branch_id }}', 2)"
                                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-red">Deactivate</button>
                            </div>
                        </div>
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>

    <x-slot name="header_title">
        Transaction Entry Dropdown Management
    </x-slot>
    <x-slot name="header_button">
        @can('oims_order_management_t_e_dropdown_mgmt_branch_mgmt_add')
            <button wire:click="action({}, 'add')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Add Branch
                </div>
            </button>
        @endcan

    </x-slot>


    <x-slot name="body">
        <div class="grid grid-cols-12">
            <div class="col-span-4">
                <x-label class="text-base text-black" for="" value="Transaction Entry" />
                <x-select class="w-5/6 h-10 text-sm font-normal rounded-sm" style="cursor: pointer;" name=""
                    wire:model='transaction_entry'>
                    <option value="1">Origin, Destination and Transhipment</option>
                    <option value="2">Branch Management</option>
                    <option value="3">Transport Mode Management</option>
                    <option value="4">Paymode Management</option>
                    <option value="5">Service Mode Management</option>
                    <option value="6">ODA and OPA Management</option>
                    <option value="7">Service branch Coverage Management</option>
                    <option value="8">Dangerous Goods Management</option>
                    <option value="9">Crating Category Management</option>
                    <option value="10">Commodity Type Management</option>
                    <option value="11">Type of Packaging Management</option>
                    <option value="12">Reason Management</option>
                    <option value="13">Putaway Management</option>
                    <option value="14">Remarks Management</option>
                    <option value="15">Cargo Status Management</option>
                </x-select>
            </div>
        </div>
        <div class="py-6">
            <div class="flex items-center justify-start">
                <ul class="flex mt-8">
                    @foreach ($status_header_cards as $i => $status_card)
                        <li
                            class="px-4 py-1 text-xs {{ $status_header_cards[$i]['class'] }} {{ $stats == $status_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                            <button
                                wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                {{ $status_header_cards[$i]['title'] }}
                            </button>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="No." style="padding-left:2%;" />
                        <x-table.th name="Region" style="padding-left:%;" />
                        <x-table.th name="Branch Name" style="padding-left:%;" />
                        <x-table.th name="Address" style="padding-left:%;" />
                        <x-table.th name="Telephone No" style="padding-left:%;" />
                        <x-table.th name="Contact No" style="padding-left:%;" />
                        <x-table.th name="Action" style="padding-left:3%;" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($branch_lists as $i => $branch_list)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-normal">
                                <td class="w-1/12 p-3 whitespace-nowrap" style="padding-left:2%;">
                                    <p class="w-24">
                                        {{ ($branch_lists->currentPage() - 1) * $branch_lists->links()->paginator->perPage() + $loop->iteration }}
                                    </p>
                                </td>
                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $branch_list->RegionRefs->name }}
                                </td>
                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $branch_list->name }}
                                </td>
                                <td class="w-1/3 p-3 whitespace-pre-line" style="padding-left:;">
                                    {{ $branch_list->address }}
                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    <table class="capitalize ">
                                        @foreach ($branch_list->OBRTelephoneHasMany as $c => $branch_list1)
                                            <tr>
                                                <td class="">
                                                    {{ $branch_list1->telephone }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    <table class="capitalize ">
                                        @foreach ($branch_list->OBRContactHasMany as $c => $branch_list2)
                                            <tr>
                                                <td class="">
                                                    {{ $branch_list2->contact_no }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>
                                <td class="p-3 whitespace-nowrap" style="padding-left:;">
                                    <div class="flex space-x-1">
                                        @can('oims_order_management_t_e_dropdown_mgmt_branch_mgmt_view')
                                            <span title="View Map">
                                                <svg wire:click="action({'id': {{ $branch_list->id }}}, 'view')"
                                                    class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="far" data-icon="view" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M565.6 36.2C572.1 40.7 576 48.1 576 56V392c0 10-6.2 18.9-15.5 22.4l-168 64c-5.2 2-10.9 2.1-16.1 .3L192.5 417.5l-160 61c-7.4 2.8-15.7 1.8-22.2-2.7S0 463.9 0 456V120c0-10 6.1-18.9 15.5-22.4l168-64c5.2-2 10.9-2.1 16.1-.3L383.5 94.5l160-61c7.4-2.8 15.7-1.8 22.2 2.7zM48 136.5V421.2l120-45.7V90.8L48 136.5zM360 422.7V137.3l-144-48V374.7l144 48zm48-1.5l120-45.7V90.8L408 136.5V421.2z">
                                                    </path>
                                                </svg>
                                            </span>
                                        @endcan
                                        @can('oims_order_management_t_e_dropdown_mgmt_branch_mgmt_edit')
                                            <span title="Edit">
                                                <svg wire:click="action({'id': {{ $branch_list->id }}}, 'edit')"
                                                    class="w-5 h-5 ml-2 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="far" data-icon="edit" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                                    </path>
                                                </svg>
                                            </span>
                                        @endcan
                                        @can('oims_order_management_t_e_dropdown_mgmt_branch_mgmt_deactivate')
                                            <span title="Deactivate">
                                                <svg x-cloak x-show="'{{ $branch_list->status == 1 }}'"
                                                    wire:click="action({'id': {{ $branch_list->id }}, 'status': 2}, 'update_status')"
                                                    class="w-8 h-5 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="fas" data-icon="user-slash" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                                    <path fill="currentColor"
                                                        d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                    </path>
                                                </svg>
                                            </span>
                                            <span title="Reactivate">
                                                <svg x-cloak x-show="'{{ $branch_list->status == 2 }}'"
                                                    wire:click="action({'id': {{ $branch_list->id }}, 'status': 1}, 'update_status')"
                                                    class="w-8 h-5 pl-2 text-gray-400 rotate-180 " aria-hidden=" true"
                                                    focusable="false" data-prefix="fas" data-icon="user" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                                    <path fill="currentColor"
                                                        d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zm192 96a96 96 0 1 1 0 192 96 96 0 1 1 0-192z">
                                                    </path>
                                                </svg>
                                            </span>
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $branch_lists->links() }}
                </div>
            </div>

    </x-slot>
</x-form>
