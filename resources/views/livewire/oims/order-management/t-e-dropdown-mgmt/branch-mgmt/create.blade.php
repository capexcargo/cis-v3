<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>
    @if($confirmation_modal)
    <x-modal id="confirmation_modal" size="w-auto" hasClose="1">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to submit this new Branch?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
    @endif


    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="mt-5 ">
            <div class="grid grid-cols-12 gap-3 pl-2 pr-8">
                <div class="col-span-11">
                    <x-label for="r_name" value="Region" :required="true" />
                    <div wire:init="oimsRegionReference" class="flex items-center">
                        <x-select class="w-full h-10 rounded-md" style="cursor: pointer;" name="r_name"
                            wire:model.defer='r_name'>
                            <option value="">Select</option>
                            @foreach ($oBRReferences as $oBRReference)
                                <option value="{{ $oBRReference->id }}">
                                    {{ $oBRReference->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="r_name" />
                </div>
                <div class="col-span-1"></div>


                <div class="col-span-11">
                    <x-label for="b_name" value="Branch Name" :required="true" />
                    <x-input type="text" name="b_name" wire:model.defer='b_name'></x-input>
                    <x-input-error for="b_name" />
                </div>
                <div class="col-span-1">

                </div>

                <div class="col-span-11">
                    <x-label for="adrs" value="Address" :required="true" />
                    <x-input type="text" name="adrs" id="adrs" wire:model.defer='adrs'></x-input>
                    <x-input type="hidden" name="lat" id="lat" wire:model.defer='lat'></x-input>
                    <x-input type="hidden" name="long" id="long" wire:model.defer='long'></x-input>
                    <x-input-error for="adrs" />
                </div>

                <div class="col-span-1 ml-2">
                    <span class="ml-4">
                        <svg class="mt-1 cursor-pointer w-7 h-7 text-blue" aria-hidden="true" focusable="false"
                            data-prefix="far" data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 576 512">
                            <path fill="currentColor"
                                d="M215.7 499.2C267 435 384 279.4 384 192C384 86 298 0 192 0S0 86 0 192c0 87.4 117 243 168.3 307.2c12.3 15.3 35.1 15.3 47.4 0zM192 128a64 64 0 1 1 0 128 64 64 0 1 1 0-128z">
                            </path>
                        </svg>
                    </span>
                </div>

                <?php $i = 0; ?>
                @foreach ($tellnos as $a => $tellno)
                    <div class="col-span-11">
                        @if ($i == 0)
                            <x-label for="tellnos.{{ $a }}.tell_no" value="Telephone No" :required="true" />
                        @else
                        @endif
                        <div class="relative">
                            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <svg class="w-4 h-4 text-gray-400 cursor-pointer" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M164.9 24.6c-7.7-18.6-28-28.5-47.4-23.2l-88 24C12.1 30.2 0 46 0 64C0 311.4 200.6 512 448 512c18 0 33.8-12.1 38.6-29.5l24-88c5.3-19.4-4.6-39.7-23.2-47.4l-96-40c-16.3-6.8-35.2-2.1-46.3 11.6L304.7 368C234.3 334.7 177.3 277.7 144 207.3L193.3 167c13.7-11.2 18.4-30 11.6-46.3l-40-96z">
                                    </path>
                                </svg>
                            </div>
                            <x-input class="block w-full h-10 pl-10 rounded-md" type="number"
                                name="tellnos.{{ $a }}.tell_no"
                                wire:model.defer='tellnos.{{ $a }}.tell_no'></x-input>
                        </div>
                        <x-input-error for="tellnos.{{ $a }}.tell_no" />
                    </div>
                    <div class="col-span-1">
                        <div class="grid grid-cols-2">
                            @if (count($tellnos) > 1)
                                @if ($i == 0)
                                    <div class="col-span-1 mt-6"></div>
                                @else
                                    <div class="col-span-1"></div>
                                @endif
                                <x-label for="process" value="" />
                                <svg wire:click="removeTellNo({'a': {{ $a }}})"
                                    class="h-6 cursor-pointer w-7 text-red" aria-hidden="true" focusable="false"
                                    data-prefix="fas" data-icon="circle-minus" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z">
                                    </path>
                                </svg>
                            @endif

                            @if (count($tellnos) == 1)
                                <div class="col-span-1 pr-4 mt-6">
                                    <x-label for="TellNo" value="" />
                                    <svg wire:click="addTellNo({{ $a + 1 }})"
                                        class="h-6 cursor-pointer w-7 text-blue" aria-hidden="true" focusable="false"
                                        data-prefix="fas" data-icon="trash-alt" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                        <path fill="currentColor"
                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                        </path>
                                    </svg>
                                </div>
                            @endif

                            @if ($i != 0)
                                @if (count($tellnos) == $i + 1)
                                    <div class="col-span-1 pl-2 pr-2 ">
                                        <svg wire:click="addTellNo({{ $a + 1 }})"
                                            class="h-6 ml-3 cursor-pointer w-7 text-blue" aria-hidden="true"
                                            focusable="false" data-prefix="fas" data-icon="trash-alt" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                            </path>
                                        </svg>
                                    </div>
                                @endif
                            @endif
                        </div>
                    </div>
                    <?php $i++; ?>
                @endforeach

                <?php $x = 0; ?>
                @foreach ($contacts as $b => $contact)
                    <div class="col-span-11">
                        @if ($x == 0)
                            <x-label for="contacts.{{ $b }}.cont" value="Contact No" :required="true" />
                        @else
                        @endif
                        <div class="relative">
                            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <svg class="w-4 h-4 text-gray-400 cursor-pointer" aria-hidden="true"
                                    focusable="false" data-prefix="far" data-icon="edit" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M16 64C16 28.7 44.7 0 80 0H304c35.3 0 64 28.7 64 64V448c0 35.3-28.7 64-64 64H80c-35.3 0-64-28.7-64-64V64zM224 448a32 32 0 1 0 -64 0 32 32 0 1 0 64 0zM304 64H80V384H304V64z">
                                    </path>
                                </svg>
                            </div>
                            <x-input class="block w-full h-10 pl-10 rounded-md" type="number"
                                name="contacts.{{ $b }}.cont"
                                wire:model.defer='contacts.{{ $b }}.cont'>
                            </x-input>
                        </div>
                        <x-input-error for="contacts.{{ $b }}.cont" />
                    </div>
                    <div class="col-span-1">
                        <div class="grid grid-cols-2">
                            @if (count($contacts) > 1)
                                @if ($x == 0)
                                    <div class="col-span-1 mt-6"></div>
                                @else
                                    <div class="col-span-1"></div>
                                @endif
                                <x-label for="process" value="" />
                                <svg wire:click="removeContactNo({'a': {{ $b }}})"
                                    class="h-6 cursor-pointer w-7 text-red" aria-hidden="true" focusable="false"
                                    data-prefix="fas" data-icon="circle-minus" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z">
                                    </path>
                                </svg>
                            @endif

                            @if (count($contacts) == 1)
                                <div class="col-span-1 pr-4 mt-6">
                                    <x-label for="TellNo" value="" />
                                    <svg wire:click="addContactNo({{ $b + 1 }})"
                                        class="h-6 cursor-pointer w-7 text-blue" aria-hidden="true" focusable="false"
                                        data-prefix="fas" data-icon="trash-alt" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                        <path fill="currentColor"
                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                        </path>
                                    </svg>
                                </div>
                            @endif

                            @if ($x != 0)
                                @if (count($contacts) == $x + 1)
                                    <div class="col-span-1 pl-2 pr-2 ">
                                        <svg wire:click="addContactNo({{ $b + 1 }})"
                                            class="h-6 ml-3 cursor-pointer w-7 text-blue" aria-hidden="true"
                                            focusable="false" data-prefix="fas" data-icon="trash-alt" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                            </path>
                                        </svg>
                                    </div>
                                @endif
                            @endif
                        </div>
                    </div>
                    <?php $x++; ?>
                @endforeach

                <div class="col-span-11">
                    <x-label for="orig" value="Port To Cater" :required="true" />
                    <div wire:init="origReference" class="flex items-center">
                        <x-select class="w-full h-10 rounded-md" style="cursor: pointer;" name="orig"
                            wire:model.defer='orig'>
                            <option value="">Select</option>
                            @foreach ($origReferences as $origReference)
                                <option value="{{ $origReference->id }}">
                                    {{ $origReference->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="orig" />
                </div>
            </div>
        </div>

        <div class="flex justify-end gap-3 mt-6 space-x-3">
            <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                class="px-12 bg-white text-blue hover:bg-gray-100" />
            <x-button type="submit" title="Submit" class="px-12 bg-blue text-white hover:bg-[#002161]" />
        </div>
    </form>
</div>

{{-- @push('scripts') --}}
    <script>
        const auto_complete_options = {
            componentRestrictions: {
                country: "ph"
            },
        };
        var autocomplete = new google.maps.places.Autocomplete(document.getElementById("adrs"),
            auto_complete_options);

            autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            console.log(place)
            @this.set('adrs', place.formatted_address);
            @this.set('lat', place.geometry['location'].lat());
            @this.set('long', place.geometry['location'].lng());
        });
    </script>
{{-- @endpush --}}
