<div class="flex-grow w-full lg:flex lg:items-center lg:w-auto"
    :class="{ 'block shadow-3xl': isNavMenuOpenDesktop, 'hidden': !isNavMenuOpenDesktop }"
    @click.away="closeNavMenuDesktop" x-show.transition="true">
    <ul class="items-center justify-end flex-1 pt-6 font-medium lg:pt-0 list-reset lg:flex">
        <li class="mr-3">
            <a class="{{ Session::get('menu') == 1 ? 'border-b-4 border-blue-900 bg-blue-100 text-blue-900' : '' }} inline-block text-gray-700 py-6 px-4 w-full"
                href="javascript:void(0)" @click="closeNavMenuDesktop" wire:click="action(1)">ADMIN
            </a>
        </li>
        <li class="mr-3">
            <a class="{{ Session::get('menu') == 2 ? 'border-b-4 border-blue-900 bg-blue-100 text-blue-900' : '' }} inline-block text-gray-700 py-6 px-4 w-full"
                href="javascript:void(0)" @click="closeNavMenuDesktop" wire:click="action(2)">ACCTNG
            </a>
        </li>
        <li class="mr-3">
            <a class="{{ Session::get('menu') == 3 ? 'border-b-4 border-blue-900 bg-blue-100 text-blue-900' : '' }} inline-block py-6 px-4 w-full"
                href="javascript:void(0)" @click="closeNavMenuDesktop" wire:click="action(3)">FIMS
            </a>
        </li>
        <li class="mr-3">
            <a class="{{ Session::get('menu') == 4 ? 'border-b-4 border-blue-900 bg-blue-100 text-blue-900' : '' }} inline-block text-gray-700 py-6 px-4 w-full"
                href="javascript:void(0)" @click="closeNavMenuDesktop" wire:click="action(4)">CLAIMS
            </a>
        </li>
        <li class="mr-3">
            <a class="{{ Session::get('menu') == 5 ? 'border-b-4 border-blue-900 bg-blue-100 text-blue-900' : '' }} inline-block text-gray-700 py-6 px-4 w-full"
                href="javascript:void(0)" @click="closeNavMenuDesktop" wire:click="action(5)">OIMS
            </a>
        </li>
        <li class="mr-3">
            <a class="{{ Session::get('menu') == 6 ? 'border-b-4 border-blue-900 bg-blue-100 text-blue-900' : '' }} inline-block text-gray-700 py-6 px-4 w-full"
                href="javascript:void(0)" @click="closeNavMenuDesktop" wire:click="action(6)">CRM
            </a>
        </li>
        <li class="mr-3">
            <a class="{{ Session::get('menu') == 7 ? 'border-b-4 border-blue-900 bg-blue-100 text-blue-900' : '' }} inline-block text-gray-700 py-6 px-4 w-full"
                href="javascript:void(0)" @click="closeNavMenuDesktop" wire:click="action(7)">HRIMS
            </a>
        </li>
        <li class="mr-3">
            <a class="{{ Session::get('menu') == 8 ? 'border-b-4 border-blue-900 bg-blue-100 text-blue-900' : '' }} inline-block text-gray-700 py-6 px-4 w-full"
                href="javascript:void(0)" @click="closeNavMenuDesktop" wire:click="action(8)">TICKETS
            </a>
        </li>
    </ul>
</div>
