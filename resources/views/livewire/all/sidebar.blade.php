<ul class="mt-3" style="color: #3a3a3a; font-weight:600" x-data="{ where: '{{ $where }}' }">
    <x-loading />
    @foreach ($sidebars as $sidebar)
        @if ($sidebar['type'] == 'single')
            @can($sidebar['role'])
                <li
                    class="px-4 py-2 {{ Request::is($sidebar['request_is']) ? 'border-l-4 border-blue-900 text-blue px-3' : 'px-4' }}">
                    <a class="flex items-center text-sm font-semibold" href="{{ $sidebar['link'] }}">
                        {!! $sidebar['icon'] ?? '' !!}
                        <span class="ml-3">{{ $sidebar['title'] }}</span>
                    </a>
                </li>
            @endcan
        @elseif($sidebar['type'] == 'multiple')
            @can($sidebar['role'])
                <li class="relative text-sm">
                    <button
                        class="{{ Request::is($sidebar['request_is']) ? 'border-l-4 border-blue-900 text-blue px-3' : 'px-4' }} w-full flex items-center justify-between  py-2 font-semibold"
                        wire:click="$set('where','{{ $sidebar['request_is'] }}')" aria-haspopup="true">
                        <span class="flex items-center justify-start">
                            {!! $sidebar['icon'] ?? '' !!}
                            <span class="ml-3">{{ $sidebar['title'] }}</span>
                        </span>
                        <svg x-cloak x-show="!open" class="w-4 h-4" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 256 512">
                            <path fill="currentColor"
                                d="M137.4 406.6l-128-127.1C3.125 272.4 0 264.2 0 255.1s3.125-16.38 9.375-22.63l128-127.1c9.156-9.156 22.91-11.9 34.88-6.943S192 115.1 192 128v255.1c0 12.94-7.781 24.62-19.75 29.58S146.5 415.8 137.4 406.6z" />
                        </svg>
                        <svg x-cloak x-show="open" class="w-4 h-4" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 320 512">
                            <path fill="currentColor"
                                d="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z" />
                        </svg>
                    </button>
                    <div class="px-6">
                        <template x-if="where == '{{ $sidebar['request_is'] }}' ">
                            <ul x-transition:enter="transition-all ease-in-out duration-300"
                                x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl"
                                x-transition:leave="transition-all ease-in-out duration-300"
                                x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0"
                                class="overflow-hidden " aria-label="submenu">
                                @foreach ($sidebar['sub_sidebars'] as $sub_sidebar)
                                    @can($sub_sidebar['role'])
                                        <li class="{{ Request::is($sub_sidebar['request_is']) ? 'text-blue' : '' }} py-1 ml-12">
                                            <a class="font-medium"
                                                href="{{ $sub_sidebar['link'] }}">{{ $sub_sidebar['title'] }}</a>
                                        </li>
                                    @endcan
                                @endforeach
                            </ul>
                        </template>
                    </div>
                </li>
            @endcan
        @endif
    @endforeach
</ul>
