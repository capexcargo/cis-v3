<div class="flex items-center h-screen">
    <div class="w-1/4 ml-24 space-y-6">
        <div class="flex flex-col text-4xl font-semibold text-center text-gray-800">
            <span>CaPEx</span>
            <span>Integrated System</span>
        </div>
     
        <div>
            <x-jet-validation-errors class="mb-4" />
            @if (session('status'))
                <div class="mb-4 text-sm font-medium text-green-600">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="space-y-2">
                    <label for="email" class="text-base font-medium text-gray-700">Email</label>
                    <input type="email" id="email" name="email"
                        class="block w-full px-3 py-5 bg-gray-200 border-0 rounded-md" placeholder="Email"
                        value="{{ old('email') }}" required>
                </div>
                <div class="mt-4 space-y-2">
                    <label for="password" class="text-base font-medium text-gray-700">Password</label>
                    <input type="password" id="password" name="password"
                        class="block w-full px-3 py-5 bg-gray-200 border-0 rounded-md" placeholder="Password"
                        value="{{ old('password') }}" required>
                </div>
                <div class="flex items-center justify-between mt-4">
                    <div class="block">
                        <label for="remember_me" class="flex items-center">
                            <x-jet-checkbox id="remember_me" name="remember" />
                            <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                        </label>
                    </div>
                    <a class="text-sm text-blue-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        Forgot your password?
                    </a>
                </div>
                <div class="flex items-center justify-end mt-8">
                    <button type="submit" class="w-full bg-[#003399] rounded-md py-4 text-white text-lg">LOG IN</button>
                </div>
            </form>
        </div>
    </div>
</div>
