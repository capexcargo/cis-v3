<div x-data="{show_notifications: '{{ $show_notifications }}'}" class="relative">
    <span wire:click="$set('show_notifications', '{{ !$show_notifications }}')"
        @keydown.escape="$wire.set('show_notifications', false)" class="relative inline-block">
        <svg class="w-5 h-5" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
            <path
                d="M10 2a6 6 0 00-6 6v3.586l-.707.707A1 1 0 004 14h12a1 1 0 00.707-1.707L16 11.586V8a6 6 0 00-6-6zM10 18a3 3 0 01-3-3h6a3 3 0 01-3 3z">
            </path>
        </svg>
        @if (auth()->user()->unreadNotifications->count() > 0)
            <span
                class="absolute top-0 right-0 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full">{{ auth()->user()->unreadNotifications->count() > 99? '99+': auth()->user()->unreadNotifications->count() }}</span>
        @endif
    </span>
    <div x-cloak x-show="show_notifications" x-transition:leave="transition ease-in duration-150"
        x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0"
        wire:click="$set('show_notifications', true)" @keydown.escape="$wire.set('show_notifications', false)"
        class="absolute right-0 p-2 mt-2 space-y-2 text-gray-600 bg-white border border-gray-100 rounded-md shadow-md w-80 dark:text-gray-300 dark:border-gray-700 dark:bg-gray-700"
        aria-label="submenu">
        <div class="flex items-center justify-between space-x-3 text-xs">
            <div wire:click="notifications('all')" class="underline cursor-pointer text-blue">All</div>
            <div wire:click="notifications('unread')" class="underline cursor-pointer text-red">Unread</div>
            <div wire:click="notifications('read')" class="underline cursor-pointer text-green">Read</div>
        </div>
        <div class="space-y-2 overflow-auto h-52">
            @forelse ($notifications as $notification)
                <div wire:click="markAsRead('{{ $notification->id }}', '{{$notification->read_at}}')"
                    class="flex border {{ $notification->read_at ? 'border-green-600' : 'border-red-600' }} rounded-lg shadow-lg cursor-pointer">
                    <div class="w-full px-3 py-2">
                        <div class="flex flex-col">
                            <div class="flex items-center justify-between text-sm font-bold">
                                <p>{{ $notification->data['title'] }}</p>
                                <p>{{ $notification->created_at->diffForHumans() }}</p>
                            </div>
                            <p class="text-xs text-justify">{{ $notification->data['message'] }}</p>
                        </div>
                    </div>
                </div>
            @empty
                <div class="flex border border-blue-600 rounded-lg shadow-lg cursor-pointer">
                    <div class="w-full px-3 py-2">
                        <div class="flex flex-col">
                            <div class="flex items-center justify-between text-sm font-bold">
                                No Notification Found.
                            </div>
                            <p class="text-xs text-justify"></p>
                        </div>
                    </div>
                </div>
            @endforelse
        </div>

        <div class="flex items-center justify-between w-full">
            <p wire:click="markAsReadAll" class="underline cursor-pointer text-blue">Mark As Read All</p>
            <p wire:click="loadMore" class="underline cursor-pointer text-blue">Load More</p>
        </div>
    </div>
</div>
