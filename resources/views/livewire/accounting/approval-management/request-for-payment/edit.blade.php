<div wire:init="load">
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-4 gap-3">
                <div>
                    <x-label for="parent_reference_number" value="Parent Reference Number" :required="true" />
                    <x-input type="text" name="parent_reference_number" value="{{ $parent_reference_number }}" disabled>
                    </x-input>
                    <x-input-error for="parent_reference_number" />
                </div>
                <div>
                    <x-label for="reference_number" value="Reference Number" :required="true" />
                    <x-input type="text" name="reference_number" value="{{ $reference_number }}" disabled></x-input>
                    <x-input-error for="reference_number" />
                </div>
            </div>
            <div class="grid grid-cols-4 gap-3">
                <div>
                    <x-label for="division" value="Requesting Division" :required="true" />
                    <x-select name="division" wire:model='division' disabled>
                        <option value=""></option>
                        @foreach ($division_references as $division_reference)
                            <option value="{{ $division_reference->id }}">
                                {{ $division_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="division" />
                </div>
                <div>
                    <x-label for="budget_source" value="Budget Source" :required="true" />
                    <x-select name="budget_source" wire:model='budget_source' disabled>
                        <option value=""></option>
                        @foreach ($budget_source_references as $budget_source_reference)
                            <option value="{{ $budget_source_reference->id }}">
                                {{ $budget_source_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="budget_source" />
                </div>
                <div>
                    <x-label for="chart_of_accounts" value="Chart Of Accounts" :required="true" />
                    <x-select name="chart_of_accounts" wire:model='chart_of_accounts' disabled>
                        <option value=""></option>
                        @foreach ($budget_chart_references as $budget_chart_reference)
                            <option value="{{ $budget_chart_reference->id }}">
                                {{ $budget_chart_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="chart_of_accounts" />
                </div>
                <div>
                    <x-label for="coa_category" value="Coa Category" :required="true" />
                    <x-select name="coa_category" wire:model='coa_category' disabled>
                        <option value=""></option>
                        @foreach ($budget_plan_references as $budget_plan_reference)
                            <option value="{{ $budget_plan_reference->id }}">
                                {{ $budget_plan_reference->item }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="coa_category" />
                </div>
            </div>
            <div class="grid grid-cols-4 gap-3">
                <div>
                    <x-label for="priority" value="Priority" :required="true" />
                    <x-select name="priority" wire:model.defer='priority' disabled>
                        <option value=""></option>
                        @foreach ($priority_references as $priority_reference)
                            <option value="{{ $priority_reference->id }}">
                                {{ $priority_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="priority" />
                </div>
                <div>
                    <x-label for="date_needed" value="Date Needed" :required="true" />
                    <x-input type="date" name="date_needed" wire:model.defer='date_needed' disabled></x-input>
                    <x-input-error for="date_needed" />
                </div>
                <div>
                    <div class="flex items-center justify-between">
                        <x-label for="payee" value="Payee" :required="true" />
                        <div class="flex space-x-3">
                            <input type="checkbox" class="form-checkbox" name="is_payee_contact_person" value="1"
                                wire:model="is_payee_contact_person" @if ($is_payee_contact_person) checked @endif
                                disabled />
                            <x-label value="Display Payee Contact Person" />
                        </div>
                    </div>
                    <x-select name="payee" wire:model='payee' disabled>
                        <option value=""></option>
                        @foreach ($supplier_references as $supplier_reference)
                            <option value="{{ $supplier_reference->id }}">
                                {{ $is_payee_contact_person ? $supplier_reference->first_name . ' ' . $supplier_reference->last_name : $supplier_reference->company }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="payee" />
                </div>
                <div>
                    <x-label for="branch" value="Branch" :required="true" />
                    <x-select name="branch" wire:model.defer='branch' disabled>
                        <option value=""></option>
                        @foreach ($branch_references as $branch_reference)
                            <option value="{{ $branch_reference->id }}">
                                {{ $branch_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="branch" />
                </div>
                @if ($this->accounting_account_type != 1 && $this->payment_type != 2)
                    <div>
                        <x-label for="date_of_transaction_from" value="Date Of Transaction From" :required="true" />
                        <x-input type="date" name="date_of_transaction_from"
                            wire:model.defer='date_of_transaction_from' disabled>
                        </x-input>
                        <x-input-error for="date_of_transaction_from" />
                    </div>
                    <div>
                        <x-label for="date_of_transaction_to" value="Date Of Transaction To" :required="true" />
                        <x-input type="date" name="date_of_transaction_to" wire:model.defer='date_of_transaction_to'
                            disabled>
                        </x-input>
                        <x-input-error for="date_of_transaction_to" />
                    </div>
                @endif
                <div>
                    <x-label for="canvasser" value="Canvasser" />
                    <x-input type="text" name="canvasser" wire:model.defer='canvasser' disabled></x-input>
                    <x-input-error for="canvasser" />
                </div>
                <div>
                    <x-label for="opex_type" value="Opex Type" :required="true" />
                    <x-select name="opex_type" wire:model.defer='opex_type' disabled>
                        <option value=""></option>
                        @foreach ($opex_type_references as $opex_type_reference)
                            <option value="{{ $opex_type_reference->id }}">
                                {{ $opex_type_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="opex_type" />
                </div>
                <div>
                    <x-label for="description" value="Description" :required="true" />
                    <x-textarea type="text" name="description" wire:model.defer='description' disabled />
                    <x-input-error for="description" />
                </div>
                <div>
                    <x-label for="remarks" value="Remarks" />
                    <x-textarea type="text" name="remarks" wire:model.defer='remarks' disabled />
                    <x-input-error for="remarks" />
                </div>
            </div>
            @if ($rfp_type == 1)
                <div>
                    <div class="text-xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>Request For Payment</span>
                        </div>
                    </div>
                    <div class="grid grid-cols-4 gap-3">
                        <div>
                            <x-label for="po_reference_no" value="PO Reference No." :required="true" />
                            <x-input type="text" list="cavassing_supplier_suggestions" name="po_reference_no"
                                wire:model.debounce.500ms="po_reference_no" disabled></x-input>
                            <datalist id="cavassing_supplier_suggestions">
                                @foreach ($canvassing_supplier_references as $canvassing_supplier_reference)
                                    <option value="{{ $canvassing_supplier_reference->po_reference_no }}">
                                        {{ $canvassing_supplier_reference->po_reference_no . ' - ' . $canvassing_supplier_reference->supplier->company }}
                                    </option>
                                @endforeach
                            </datalist>
                            <x-input-error for="po_reference_no" />
                        </div>
                    </div>
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="Particular / Description" />
                            <x-table.th name="Plate Number" />
                            <x-table.th name="Labor Cost" />
                            <x-table.th name="Unit Cost" />
                            <x-table.th name="Quantity" />
                            <x-table.th name="Amount" />
                        </x-slot>
                        <x-slot name="tbody">
                            @forelse ($request_for_payments as $i => $request_for_payment)
                                @if (!$request_for_payment['is_deleted'])
                                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text"
                                                name="request_for_payments.{{ $i }}.particulars"
                                                wire:model.defer="request_for_payments.{{ $i }}.particulars"
                                                disabled />
                                            <x-input-error
                                                for="request_for_payments.{{ $i }}.particulars" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text"
                                                name="request_for_payments.{{ $i }}.plate_no"
                                                wire:model.defer="request_for_payments.{{ $i }}.plate_no"
                                                disabled />
                                            <x-input-error for="request_for_payments.{{ $i }}.plate_no" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number" step="0.01"
                                                name="request_for_payments.{{ $i }}.labor_cost"
                                                wire:model.debounce.500ms="request_for_payments.{{ $i }}.labor_cost"
                                                disabled />
                                            <x-input-error
                                                for="request_for_payments.{{ $i }}.labor_cost" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ number_format($request_for_payments[$i]['unit_cost'], 2) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $request_for_payments[$i]['quantity'] }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ number_format($request_for_payments[$i]['amount'], 2) }}
                                        </td>
                                    </tr>
                                @endif
                            @empty
                                <tr>
                                    <td colspan="6">
                                        <p class="text-center">Empty.</p>
                                    </td>
                                </tr>
                            @endforelse
                        </x-slot>
                    </x-table.table>
                    <x-input-error for="request_for_payments" />
                </div>
            @elseif($rfp_type == 2)
                <div>
                    <div class="text-xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>Freights</span>
                        </div>
                    </div>
                    <div class="grid grid-cols-4 gap-3">
                        <div>

                            <x-label for="accounting_account_type" value="Account Type" :required="true" />
                            <x-select name="accounting_account_type" wire:model='accounting_account_type' disabled>
                                <option value=""></option>

                                @foreach ($accounting_account_type_references as $accounting_account_type_reference)
                                    <option value="{{ $accounting_account_type_reference->id }}">
                                        {{ $accounting_account_type_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="accounting_account_type" />
                        </div>
                        <div>
                            <x-label for="loader_search" value="Loaders" :required="true" />
                            <x-input type="text" list="loaders_suggestions" name="loader_search"
                                wire:model.debounce.500ms="loader_search"></x-input>
                            <datalist id="loaders_suggestions">
                                @foreach ($loaders_references as $loaders_reference)
                                    <option>
                                        {{ $loaders_reference->name }}
                                    </option>
                                @endforeach
                            </datalist>
                            <x-input-error for="loader_search" />
                        </div>
                    </div>
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="Freight Reference No" />
                            <x-table.th name="Freight Reference Type" />
                            <x-table.th name="SOA No." />
                            <x-table.th name="Trucking Type" />
                            <x-table.th name="Trucking Amount" />
                            <x-table.th name="Freight Usage" />
                            <x-table.th name="Transaction Date" />
                            <x-table.th name="Allowance" />
                            <x-table.th name="Amount" />
                            @if ($rfp_type == 2 && $accounting_account_type == 1)
                                <x-table.th name="Remaining Budget" />
                            @endif
                            <x-table.th name="Action" />
                        </x-slot>
                        <x-slot name="tbody">
                            @forelse ($freights as $i => $freight)
                                @if (!$freight['is_deleted'])
                                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text"
                                                name="freights.{{ $i }}.freight_reference_no"
                                                wire:model.defer="freights.{{ $i }}.freight_reference_no"
                                                disabled />
                                            <x-input-error for="freights.{{ $i }}.freight_reference_no" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-select name="freights.{{ $i }}.freight_reference_type"
                                                wire:model.defer='freights.{{ $i }}.freight_reference_type'
                                                disabled>
                                                <option value=""></option>
                                                @foreach ($freight_reference_type_references as $freight_reference_type_reference)
                                                    <option value="{{ $freight_reference_type_reference->id }}">
                                                        {{ $freight_reference_type_reference->display }}
                                                    </option>
                                                @endforeach
                                            </x-select>
                                            <x-input-error
                                                for="freights.{{ $i }}.freight_reference_type" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text" name="freights.{{ $i }}.soa_no"
                                                wire:model.defer="freights.{{ $i }}.soa_no" disabled />
                                            <x-input-error for="freights.{{ $i }}.soa_no" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-select name="freights.{{ $i }}.trucking_type"
                                                wire:model.defer='freights.{{ $i }}.trucking_type'
                                                disabled>
                                                <option value=""></option>
                                                @foreach ($trucking_type_references as $trucking_type_reference)
                                                    <option value="{{ $trucking_type_reference->id }}">
                                                        {{ $trucking_type_reference->display }}
                                                    </option>
                                                @endforeach
                                            </x-select>
                                            <x-input-error for="freights.{{ $i }}.trucking_type" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number" step="0.01"
                                                name="freights.{{ $i }}.trucking_amount"
                                                wire:model="freights.{{ $i }}.trucking_amount" disabled />
                                            <x-input-error for="freights.{{ $i }}.trucking_amount" />
                                        </td>

                                        <td class="p-3 whitespace-nowrap">
                                            <x-select name="freights.{{ $i }}.freight_usage"
                                                wire:model.defer='freights.{{ $i }}.freight_usage'
                                                disabled>
                                                <option value=""></option>
                                                @foreach ($freight_usage_references as $freight_usage_reference)
                                                    <option value="{{ $freight_usage_reference->id }}">
                                                        {{ $freight_usage_reference->display }}
                                                    </option>
                                                @endforeach
                                            </x-select>
                                            <x-input-error for="freights.{{ $i }}.freight_usage" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="date"
                                                name="freights.{{ $i }}.transaction_date"
                                                wire:model.defer="freights.{{ $i }}.transaction_date"
                                                disabled />
                                            <x-input-error for="freights.{{ $i }}.transaction_date" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number" step="0.01"
                                                name="freights.{{ $i }}.allowance"
                                                wire:model="freights.{{ $i }}.allowance" disabled />
                                            <x-input-error for="freights.{{ $i }}.allowance" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number" step="0.01"
                                                name="freights.{{ $i }}.amount"
                                                value="{{ $freights[$i]['amount'] }}" disabled />
                                            <x-input-error for="freights.{{ $i }}.amount" />
                                        </td>
                                        @if ($rfp_type == 2 && $accounting_account_type == 1)
                                            <td class="p-3 whitespace-nowrap">
                                                <x-input type="number" step="0.01"
                                                    name="freights.{{ $i }}.balance_budget"
                                                    wire:model="freights.{{ $i }}.balance_budget"
                                                    disabled />
                                                <x-input-error for="freights.{{ $i }}.balance_budget" />
                                            </td>
                                        @endif
                                        <td class="p-3 whitespace-nowrap">
                                            <div class="flex items-center justify-center">
                                                @if ($freights[$i]['freight_reference_type'] == 3)
                                                    @if ($request->requestForPaymentDetails[0]->loaders)
                                                        <a href="https://cis2.capex.com.ph/index.php?r=acRfPayment/getwingvanPAforprint&data=1&tranID={{ $freights[$i]['freight_reference_no'] }}&wingv={{ $request->requestForPaymentDetails[0]->loaders->code }}"
                                                            target="_blank">
                                                            <svg class="w-5 h-5 text-blue"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 640 512">
                                                                <path fill="currentColor"
                                                                    d="M64 48C64 21.49 85.49 0 112 0H368C394.5 0 416 21.49 416 48V256H608V352C625.7 352 640 366.3 640 384C640 401.7 625.7 416 608 416H576C576 469 533 512 480 512C426.1 512 384 469 384 416H256C256 469 213 512 160 512C106.1 512 64 469 64 416V288H208C216.8 288 224 280.8 224 272C224 263.2 216.8 256 208 256H16C7.164 256 0 248.8 0 240C0 231.2 7.164 224 16 224H240C248.8 224 256 216.8 256 208C256 199.2 248.8 192 240 192H48C39.16 192 32 184.8 32 176C32 167.2 39.16 160 48 160H272C280.8 160 288 152.8 288 144C288 135.2 280.8 128 272 128H16C7.164 128 0 120.8 0 112C0 103.2 7.164 96 16 96H64V48zM160 464C186.5 464 208 442.5 208 416C208 389.5 186.5 368 160 368C133.5 368 112 389.5 112 416C112 442.5 133.5 464 160 464zM480 368C453.5 368 432 389.5 432 416C432 442.5 453.5 464 480 464C506.5 464 528 442.5 528 416C528 389.5 506.5 368 480 368zM466.7 160H400V96H466.7C483.7 96 499.1 102.7 512 114.7L589.3 192C601.3 204 608 220.3 608 237.3V288H544V237.3L466.7 160z" />
                                                            </svg>
                                                        </a>
                                                    @endif
                                                @endif
                                                @if ($freights[$i]['freight_reference_type'] == 4)
                                                    @if ($freights[$i]['soa_no'])
                                                        <a href="https://cis2.capex.com.ph/index.php?r=acRfPayment/getairPAforprint&data=1&tranID={{ $freights[$i]['soa_no'] }}"
                                                            target="_blank">
                                                            <svg class="w-5 h-5 text-blue"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 576 512">
                                                                <path fill="currentColor"
                                                                    d="M482.3 192C516.5 192 576 221 576 256C576 292 516.5 320 482.3 320H365.7L265.2 495.9C259.5 505.8 248.9 512 237.4 512H181.2C170.6 512 162.9 501.8 165.8 491.6L214.9 320H112L68.8 377.6C65.78 381.6 61.04 384 56 384H14.03C6.284 384 0 377.7 0 369.1C0 368.7 .1818 367.4 .5398 366.1L32 256L.5398 145.9C.1818 144.6 0 143.3 0 142C0 134.3 6.284 128 14.03 128H56C61.04 128 65.78 130.4 68.8 134.4L112 192H214.9L165.8 20.4C162.9 10.17 170.6 0 181.2 0H237.4C248.9 0 259.5 6.153 265.2 16.12L365.7 192H482.3z" />
                                                            </svg>
                                                        </a>
                                                    @endif
                                                @endif
                                                @if ($freights[$i]['freight_reference_type'] == 2)
                                                    @if ($freights[$i]['freight_reference_no'])
                                                        <a href="https://cis2.capex.com.ph/capexsystem/includes/AirwayBillPrint.php?awbNo={{ $freights[$i]['freight_reference_no'] }}"
                                                            target="_blank">
                                                            <svg class="w-5 h-5 text-blue"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 384 512">
                                                                <path fill="currentColor"
                                                                    d="M256 0v128h128L256 0zM224 128L224 0H48C21.49 0 0 21.49 0 48v416C0 490.5 21.49 512 48 512h288c26.51 0 48-21.49 48-48V160h-127.1C238.3 160 224 145.7 224 128zM272 416h-160C103.2 416 96 408.8 96 400C96 391.2 103.2 384 112 384h160c8.836 0 16 7.162 16 16C288 408.8 280.8 416 272 416zM272 352h-160C103.2 352 96 344.8 96 336C96 327.2 103.2 320 112 320h160c8.836 0 16 7.162 16 16C288 344.8 280.8 352 272 352zM288 272C288 280.8 280.8 288 272 288h-160C103.2 288 96 280.8 96 272C96 263.2 103.2 256 112 256h160C280.8 256 288 263.2 288 272z" />
                                                            </svg>
                                                        </a>
                                                    @endif
                                                @endif
                                                @if ($freights[$i]['freight_reference_type'] == 1)
                                                    <a href="https://cis2.capex.com.ph/index.php?r=acRfPayment/getseaPAforprint&data=1&tranID={{ $freights[$i]['freight_reference_no'] }}"
                                                        target="_blank">
                                                        <svg class="w-5 h-5 text-blue"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                            <path fill="currentColor"
                                                                d="M192 32C192 14.33 206.3 0 224 0H352C369.7 0 384 14.33 384 32V64H432C458.5 64 480 85.49 480 112V240L524.4 254.8C547.6 262.5 553.9 292.3 535.9 308.7L434.9 401.4C418.7 410.7 400.2 416.5 384 416.5C364.4 416.5 343.2 408.8 324.8 396.1C302.8 380.6 273.3 380.6 251.2 396.1C234 407.9 213.2 416.5 192 416.5C175.8 416.5 157.3 410.7 141.1 401.3L40.09 308.7C22.1 292.3 28.45 262.5 51.59 254.8L96 239.1V111.1C96 85.49 117.5 63.1 144 63.1H192V32zM160 218.7L267.8 182.7C280.9 178.4 295.1 178.4 308.2 182.7L416 218.7V128H160V218.7zM384 448C410.9 448 439.4 437.2 461.4 421.9L461.5 421.9C473.4 413.4 489.5 414.1 500.7 423.6C515 435.5 533.2 444.6 551.3 448.8C568.5 452.8 579.2 470.1 575.2 487.3C571.2 504.5 553.1 515.2 536.7 511.2C512.2 505.4 491.9 494.6 478.5 486.2C449.5 501.7 417 512 384 512C352.1 512 323.4 502.1 303.6 493.1C297.7 490.5 292.5 487.8 288 485.4C283.5 487.8 278.3 490.5 272.4 493.1C252.6 502.1 223.9 512 192 512C158.1 512 126.5 501.7 97.5 486.2C84.12 494.6 63.79 505.4 39.27 511.2C22.06 515.2 4.853 504.5 .8422 487.3C-3.169 470.1 7.532 452.8 24.74 448.8C42.84 444.6 60.96 435.5 75.31 423.6C86.46 414.1 102.6 413.4 114.5 421.9L114.6 421.9C136.7 437.2 165.1 448 192 448C219.5 448 247 437.4 269.5 421.9C280.6 414 295.4 414 306.5 421.9C328.1 437.4 356.5 448 384 448H384z" />
                                                        </svg>
                                                    </a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @empty
                                <tr>
                                    <td colspan="11">
                                        <p class="text-center">Empty.</p>
                                    </td>
                                </tr>
                            @endforelse
                        </x-slot>
                    </x-table.table>
                </div>
            @elseif($rfp_type == 3)
                <div class="space-y-3">
                    <div class="text-xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>Payables</span>
                        </div>
                    </div>
                    <div class="grid grid-cols-5 gap-3">
                        <div>
                            <x-label for="account_no" value="Account No." :required="true" />
                            <x-input type="text" name="account_no" wire:model.defer='account_no' disabled>
                            </x-input>
                            <x-input-error for="account_no" />
                        </div>
                        <div>
                            <x-label for="payment_type" value="Payment Type" :required="true" />
                            <x-select name="payment_type" wire:model.defer='payment_type' disabled>
                                <option value=""></option>
                                @foreach ($payment_type_references as $payment_type_reference)
                                    <option value="{{ $payment_type_reference->id }}">
                                        {{ $payment_type_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="payment_type" />
                        </div>
                        <div>
                            <x-label for="terms" value="Type of Terms" :required="true" />
                            <x-select name="terms" wire:model.defer='terms' disabled>
                                <option value=""></option>
                                @foreach ($terms_references as $terms_reference)
                                    <option value="{{ $terms_reference->id }}">
                                        {{ $terms_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="terms" />
                        </div>
                        <div>
                            <x-label for="pdc_from" value="Duration From" :required="true" />
                            <x-input type="date" name="pdc_from" wire:model.defer='pdc_from' disabled></x-input>
                            <x-input-error for="pdc_from" />
                        </div>
                        <div>
                            <x-label for="pdc_to" value="Duration To" :required="true" />
                            <x-input type="date" name="pdc_to" wire:model.defer='pdc_to' disabled></x-input>
                            <x-input-error for="pdc_to" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-table.table>
                                <x-slot name="thead">
                                    <x-table.th name="Invoice Number" />
                                    <x-table.th name="Invoice Amount" />
                                    @if ($payment_type == 2)
                                        <x-table.th name="Date Of Transaction" />
                                    @endif
                                </x-slot>
                                <x-slot name="tbody">
                                    @forelse ($payables as $i => $payable)
                                        @if (!$payable['is_deleted'])
                                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                                <td class="p-3 whitespace-nowrap">
                                                    <x-input type="text"
                                                        name="payables.{{ $i }}.invoice_no"
                                                        wire:model.defer="payables.{{ $i }}.invoice_no"
                                                        disabled />
                                                    <x-input-error for="payables.{{ $i }}.invoice_no" />
                                                </td>

                                                <td class="p-3 whitespace-nowrap">
                                                    <x-input type="number" step="0.01"
                                                        name="payables.{{ $i }}.invoice_amount"
                                                        wire:model="payables.{{ $i }}.invoice_amount"
                                                        disabled />
                                                    <x-input-error
                                                        for="payables.{{ $i }}.invoice_amount" />
                                                </td>
                                                @if ($payment_type == 2)
                                                    <td class="p-3 whitespace-nowrap">
                                                        <x-input type="date"
                                                            name="payables.{{ $i }}.date_of_transaction"
                                                            wire:model="payables.{{ $i }}.date_of_transaction"
                                                            disabled />
                                                        <x-input-error
                                                            for="payables.{{ $i }}.date_of_transaction" />
                                                    </td>
                                                @endif
                                            </tr>
                                        @endif
                                    @empty
                                        <tr>
                                            <td colspan="3">
                                                <p class="text-center">Empty.</p>
                                            </td>
                                        </tr>
                                    @endforelse
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                </div>
            @elseif ($rfp_type == 4)
                <div>
                    <div class="text-xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>Cash Advance</span>
                        </div>
                    </div>
                    <div class="grid grid-cols-5 gap-3">
                        <div>
                            <x-label for="ca_no" value="CA No." />
                            <x-input type="text" name="ca_no" wire:model.defer="ca_no" disabled></x-input>
                            <x-input-error for="ca_no" />
                        </div>
                        <div>
                            <x-label for="ca_reference_type" value="CA Reference Type" />
                            <x-select name="ca_reference_type" wire:model='ca_reference_type' disabled>
                                <option value=""></option>
                                @foreach ($ca_reference_types_references as $ca_reference_types_reference)
                                    <option value="{{ $ca_reference_types_reference->id }}">
                                        {{ $ca_reference_types_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="ca_reference_type" />
                        </div>
                    </div>
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="Particular / Description" />
                            <x-table.th name="{{ $ca_reference_type_display ?? 'CA Reference No' }}" />
                            <x-table.th name="Unit Cost" />
                            <x-table.th name="Quantity" />
                            <x-table.th name="Amount" />
                        </x-slot>
                        <x-slot name="tbody">
                            @forelse ($cash_advances as $i => $cash_advance)
                                @if (!$cash_advance['is_deleted'])
                                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text"
                                                name="cash_advances.{{ $i }}.particulars"
                                                wire:model.defer="cash_advances.{{ $i }}.particulars"
                                                disabled />
                                            <x-input-error for="cash_advances.{{ $i }}.particulars" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text"
                                                name="cash_advances.{{ $i }}.ca_reference_no"
                                                wire:model.defer="cash_advances.{{ $i }}.ca_reference_no"
                                                disabled />
                                            <x-input-error for="cash_advances.{{ $i }}.ca_reference_no" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number" step="0.01"
                                                name="cash_advances.{{ $i }}.unit_cost"
                                                wire:model.debounce.500ms="cash_advances.{{ $i }}.unit_cost"
                                                disabled />
                                            <x-input-error for="cash_advances.{{ $i }}.unit_cost" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number"
                                                name="cash_advances.{{ $i }}.quantity"
                                                wire:model.debounce.500ms="cash_advances.{{ $i }}.quantity"
                                                disabled />
                                            <x-input-error for="cash_advances.{{ $i }}.quantity" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ number_format($cash_advances[$i]['amount'], 2) }}
                                        </td>
                                    </tr>
                                @endif
                            @empty
                                <tr>
                                    <td colspan="6">
                                        <p class="text-center">Empty.</p>
                                    </td>
                                </tr>
                            @endforelse
                        </x-slot>
                    </x-table.table>
                    <x-input-error for="cash_advances" />
                </div>
            @endif
            <div class="text-xl font-semibold text-gray-800">
                @if ($rfp_type == 2 && $accounting_account_type == 1)
                @else
                    <div class="flex justify-center">
                        <div class="text-2xl">{{ date('Y', strtotime($date_of_transaction_from)) }} BUDGET</div>
                    </div>
                    <hr>
                    <br>
                @endif
                <div class="flex items-center justify-between space-x-3">
                    <span>Approvers</span>
                    {{-- @if ($budget_plan)
                        @php
                            $budget_plan_availed = $budget_plan->availment_sum_amount + $budget_plan->transfer_budget_from_sum_amount;
                            $budget_plan_total_amount = $budget_plan->total_amount + $budget_plan->transfer_budget_to_sum_amount;
                            $budget_availed_percent = round($budget_plan_availed && $budget_plan_total_amount ? ($budget_plan_availed / $budget_plan_total_amount) * 100 : 0, 2);
                        @endphp
                        <span>Current Coa Category Budget
                            {{ number_format($budget_plan_total_amount - $budget_plan_availed, 2) }}</span>
                        <span>Availed
                            {{ number_format($budget_plan_availed, 2) }}
                            (<span class="@if ($budget_availed_percent < 80) text-green @else text-red @endif">
                                {{ $budget_availed_percent }}%
                            </span>)
                        </span>
                    @endif --}}

                    {{-- @if ($budget_plan)
                        @php
                            $budget_plan_availed = $budget_plan->availment_sum_amount + $budget_plan->transfer_budget_from_sum_amount;
                            $budget_plan_total_amount = $budget_plan[$month] + $budget_plan->transfer_budget_to_sum_amount;
                            $budget_availed_percent = round($budget_plan_availed && $budget_plan_total_amount ? ($budget_plan_availed / $budget_plan_total_amount) * 100 : 0, 2);
                        @endphp
                        <span>Coa Category Approved ({{ strtoupper($month) }}) :
                            {{ number_format($budget_plan_total_amount, 2) }}</span>
                        <span>Availed ({{ strtoupper($month) }}) :
                            {{ number_format($budget_plan_availed, 2) }}
                        </span>
                    @endif --}}
                    @if ($budget_plan)
                        {{-- if(budget_plan_total_amount == 0){

    $budget_plan_total_amount = $budget_plan[ $month];
}else{
    $budget_plan_total_amount = $budget_plan->chart['budget_plan_sum_' . $month] + $budget_plan->chart->transfer_budget_to_sum_amount - $budget_plan->chart->transfer_budget_from_sum_amount;

} --}}
                        @php
                            $budget_plan_availed = $budget_plan->chart->availment_sum_amount;
                            $budget_plan_total_amount = ($budget_plan->chart['budget_plan_sum_' . $month] == 0 ? $budget_plan[$month] : $budget_plan->chart['budget_plan_sum_' . $month]) + $budget_plan->chart->transfer_budget_to_sum_amount - $budget_plan->chart->transfer_budget_from_sum_amount;
                            $budget_availed_percent = round($budget_plan_availed && $budget_plan_total_amount ? ($budget_plan_availed / $budget_plan_total_amount) * 100 : 0, 2);
                            // dd()
                            if ($is_approved_2 == 1) {
                                $budget_req_diff = $budget_plan_total_amount - $budget_plan_total_amount * 0.97;
                                $budget_needed = $budget_plan_availed + $total_amount - $budget_plan_total_amount + $budget_req_diff - $total_amount;
                            } else {
                                $budget_req_diff = $budget_plan_total_amount - $budget_plan_total_amount * 0.97;
                                $budget_needed = $budget_plan_availed + $total_amount - $budget_plan_total_amount + $budget_req_diff;
                            }
                            
                        @endphp
                        {{-- {{ dd($budget_plan->chart['budget_plan_sum_' . $month])}} --}}
                        @if ($rfp_type == 2 && $accounting_account_type == 1)
                        @else
                            <span style="font-weight: normal">Chart Of Accounts Approved ({{ strtoupper($month) }}) :
                                <strong><span>{{ number_format($budget_plan_total_amount, 2) }}</span></strong></span>

                            <span style="font-weight: normal">Availed ({{ strtoupper($month) }}) :
                                <strong><span>
                                        {{ number_format($budget_plan_availed, 2) . ' | ' . $budget_availed_percent . '%' }}</span></strong></span>

                            @if ($is_approved_2 == 1)
                                <span style="font-weight: normal"> Remaining Budget:
                                    @if ($budget_plan_total_amount != 0)
                                        @if (100 - round(($budget_plan_availed / $budget_plan_total_amount) * 100, 2) <= 0)
                                            <strong> <span
                                                    style="color:red">{{ number_format($budget_plan_total_amount - $budget_plan_availed, 2) . ' (' . (100 - round(($budget_plan_availed / $budget_plan_total_amount) * 100, 2)) . '%)' }}</span>
                                            </strong>
                                        @else
                                            <strong><span
                                                    style="color:green">{{ number_format($budget_plan_total_amount - $budget_plan_availed, 2) . ' (' . (100 - round(($budget_plan_availed / $budget_plan_total_amount) * 100, 2)) . '%)' }}</span></strong>
                                        @endif
                                    @else
                                        <strong> <span style="color:red">0.00 (0.00%)</span>
                                        </strong>
                                    @endif
                                </span>
                            @else
                                <span style="font-weight: normal"> Availment After Approval:
                                    {{-- {{ dd(($budget_plan_availed + $total_amount) / 0) }} --}}
                                    @if ($budget_plan_total_amount != 0)
                                        @if (round((($budget_plan_availed + $total_amount) / $budget_plan_total_amount) * 100, 2) > 98.3)
                                            <strong> <span
                                                    style="color:red">{{ number_format($budget_plan_availed + $total_amount, 2) . ' (' . round((($budget_plan_availed + $total_amount) / $budget_plan_total_amount) * 100, 2) . '%)' }}</span>
                                            </strong>
                                        @else
                                            <strong><span
                                                    style="color:green">{{ number_format($budget_plan_availed + $total_amount, 2) . ' (' . round((($budget_plan_availed + $total_amount) / $budget_plan_total_amount) * 100, 2) . '%)' }}</span></strong>
                                        @endif
                                    @else
                                        <strong> <span
                                                style="color:red">{{ number_format($budget_plan_availed + $total_amount, 2) . ' (100+%)' }}</span>
                                        </strong>
                                    @endif

                                </span>
                            @endif

                            @if ($is_approved_2 != 1)
                                <span style="font-weight: normal">Budget Needed :
                                    @if ($budget_needed <= 0)
                                        <strong><span>0.00</span></strong>
                                    @else
                                        <strong><span>{{ number_format($budget_needed, 2) }}</span></strong>
                                    @endif

                                </span>
                            @else
                                <span style="font-weight: normal">Budget Needed :
                                    <strong><span>-</span></strong>
                                </span>
                            @endif



                        @endif
                    @endif
                </div>
            </div>
            <div class="grid grid-cols-4 gap-3">
                <div class="space-y-3">
                    <div>
                        <div class="flex items-center justify-between">
                            <x-label for="approver_1_id" value="Approver 1" :required="true" />
                            <div class="flex space-x-3">
                                <input type="checkbox" class="form-checkbox" name="is_set_approver_1" value="1"
                                    wire:model="is_set_approver_1" @if ($is_set_approver_1) checked @endif
                                    disabled />
                                <x-label value="Set Approver 1" />
                            </div>
                        </div>
                        <select name="approver_1_id" disabled wire:model='approver_1_id'
                            class="text-gray-600 block w-full mt-1 border border-gray-300 p-[2px] rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 @error('approver_1_id') border-red-600 @enderror">
                            <option value=""></option>
                            @foreach ($approvers as $approver)
                                <option value="{{ $approver->id }}">
                                    {{ $approver->name }}
                                </option>
                            @endforeach
                        </select>
                        <x-input-error for="approver_1_id" />
                    </div>
                    @if ($request->approver_1_id)
                        @php
                            $is_disabled_approver1 = true;
                            if ($request->approver_1_id == auth()->user()->id || (auth()->user()->level_id == 5 && $request->approver_1_id)) {
                                $is_disabled_approver1 = false;
                            }
                        @endphp
                        <div class="flex justify-between">
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_1" wire:model.defer="is_approved_1"
                                    value="1" @if ($is_disabled_approver1) disabled @endif>
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_1">Approve</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_1" wire:model.defer="is_approved_1"
                                    value="0" @if ($is_disabled_approver1) disabled @endif>
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_1">Disapprove</label>
                            </div>
                        </div>
                        <div>
                            <x-label for="approver_remarks_1" value="Remarks" />
                            <textarea name="approver_remarks_1" wire:model.defer='approver_remarks_1'
                                class="text-gray-600 block w-full mt-1 border border-gray-300 p-[2px] rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 @error('approver_remarks_1') border-red-600 @enderror"
                                @if ($is_disabled_approver1) disabled @endif></textarea>
                            <x-input-error for="approver_remarks_1" />
                        </div>
                    @endif
                </div>
                <div class="space-y-3">
                    <div>
                        <x-label for="approver_2_id" value="Approver 2" />
                        <x-input type="text" name="approver_2_id" value="{{ $approver_2_name }}" disabled>
                        </x-input>
                        <x-input-error for="approver_2_id" />
                    </div>
                    @if ($request->approver_2_id)
                        @php
                            $is_disabled_approver2 = true;
                            if ($request->approver_2_id == auth()->user()->id || (auth()->user()->level_id == 5 && $request->approver_2_id)) {
                                $is_disabled_approver2 = false;
                            }
                        @endphp
                        <div class="flex justify-between">
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_2" wire:model.defer="is_approved_2"
                                    value="1" @if ($is_disabled_approver2) disabled @endif>
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_2">Approve</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_2" wire:model.defer="is_approved_2"
                                    value="0" @if ($is_disabled_approver2) disabled @endif>
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_2">Disapprove</label>
                            </div>
                        </div>
                        <div>
                            <x-label for="approver_remarks_2" value="Remarks" />
                            <textarea name="approver_remarks_2" wire:model.defer='approver_remarks_2'
                                class="text-gray-600 block w-full mt-1 border border-gray-300 p-[2px] rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 @error('approver_remarks_2') border-red-600 @enderror"
                                @if ($is_disabled_approver2) disabled @endif></textarea>
                            <x-input-error for="approver_remarks_2" />
                        </div>
                    @endif
                </div>
                <div class="space-y-3">
                    <div>
                        <x-label for="approver_3_id" value="Approver 3" />
                        <x-input type="text" name="approver_3_id" value="{{ $approver_3_name }}" disabled>
                        </x-input>
                        <x-input-error for="approver_3_id" />
                    </div>
                    @if ($request->approver_3_id)
                        @php
                            $is_disabled_approver3 = true;
                            if ($request->approver_3_id == auth()->user()->id || (auth()->user()->level_id == 5 && $request->approver_3_id)) {
                                $is_disabled_approver3 = false;
                            }
                        @endphp
                        <div class="flex justify-between">
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_3" wire:model.defer="is_approved_3"
                                    value="1" @if ($is_disabled_approver3) disabled @endif>
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_3">Approve</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_3" wire:model.defer="is_approved_3"
                                    value="0" @if ($is_disabled_approver3) disabled @endif>
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_3">Disapprove</label>
                            </div>
                        </div>
                        <div>
                            <x-label for="approver_remarks_3" value="Remarks" />
                            <textarea name="approver_remarks_3" wire:model.defer='approver_remarks_3'
                                class="text-gray-600 block w-full mt-1 border border-gray-300 p-[2px] rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 @error('approver_remarks_3') border-red-600 @enderror"
                                @if ($is_disabled_approver3) disabled @endif></textarea>
                            <x-input-error for="approver_remarks_3" />
                        </div>
                    @endif
                </div>
            </div>
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Attachments</span>
                </div>
            </div>
            <div class="grid grid-cols-4 gap-3">
                <div class="flex flex-col space-y-3">
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="File" />
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($request->attachments as $i => $attachment)
                                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                    <td class="flex items-center justify-center p-2 whitespace-nowrap">
                                        @if (in_array($attachment->extension, config('filesystems.image_type')))
                                            <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                <a href="{{ Storage::disk('accounting_gcs')->url($attachment->path . $attachment->name) }}"
                                                    target="_blank"><img
                                                        class="w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                        src="{{ Storage::disk('accounting_gcs')->url($attachment->path . $attachment->name) }}"></a>

                                            </div>
                                        @elseif(in_array($attachment->extension, config('filesystems.file_type')))
                                            <svg wire:click="download({{ $attachment->id }})"
                                                class="w-20 h-20 mx-auto border border-gray-500 rounded-lg"
                                                aria-hidden="true" focusable="false" data-prefix="fas"
                                                data-icon="file-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                                <path fill="currentColor"
                                                    d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z">
                                                </path>
                                            </svg>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </x-slot>
                    </x-table.table>
                </div>
            </div>
            <div class="flex items-center justify-end space-x-3 text-3xl font-extrabold">
                <span class="">Total:</span>
                <span class="text-blue">{{ number_format($total_amount, 2) }}</span>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Proceed</button>
            </div>
        </div>
    </form>
</div>
