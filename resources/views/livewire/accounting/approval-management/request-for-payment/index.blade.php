<x-form wire:init="load" x-data="{ search_form: false, edit_modal: '{{ $edit_modal }}', view_modal: '{{ $view_modal }}', check_voucher_view_modal: '{{ $check_voucher_view_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @if ($request_id)
            <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                <x-slot name="title">Approve Request</x-slot>
                <x-slot name="body">
                    @livewire('accounting.approval-management.request-for-payment.edit', ['id' => $request_id])
                </x-slot>
            </x-modal>
        @endif
        @if ($parent_reference_no)
            <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                <x-slot name="title">View Request</x-slot>
                <x-slot name="body">
                    @livewire('accounting.request-management.view', ['parent_reference_no' => $parent_reference_no])
                </x-slot>
            </x-modal>
        @endif
        @if ($check_voucher_id)
            <x-modal id="check_voucher_view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-6/12">
                <x-slot name="title">View Check Voucher</x-slot>
                <x-slot name="body">
                    @livewire('accounting.check-voucher.view', ['id' => $check_voucher_id])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">Request Approval</x-slot>
    <x-slot name="header_button">
        {{-- @if (auth()->user()->role->code == 'super_admin')
                <div class="flex space-x-3">
                    <button type="button" wire:click="createAvailmendAll"
                        class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Add Availment All
                        Aproved</button>
                </div>
            @endif --}}
    </x-slot>
    <x-slot name="header_card">
        @forelse ($header_cards as $index => $card)
            <x-card.header wire:click="$set('{{ $card['action'] }}', {{ $card['id'] }})"
                wire:key="{{ $index }}" :card="$card"></x-card.header>
        @empty
            <x-card.header-loading count="5"></x-card.header-loading>
        @endforelse
    </x-slot>
    <x-slot name="search_form">
        @if (auth()->user()->level_id >= 5)
            <div>
                <x-label for="division" value="Budget Division" />
                <x-select name="division" wire:model='division'>
                    <option value=""></option>
                    @foreach ($division_references as $division_reference)
                        <option value="{{ $division_reference->id }}">
                            {{ $division_reference->name }}
                        </option>
                    @endforeach
                </x-select>
                <x-input-error for="division" class="mt-2" />
            </div>
        @endif
        <div>
            <x-label for="reference_no" value="Reference No" />
            <x-input type="text" name="reference_no" wire:model.debounce.500ms="reference_no" />
            <x-input-error for="reference_no" class="mt-2" />
        </div>
        <div>
            <x-label for="request_type" value="Request Type" />
            <x-select name="request_type" wire:model='request_type'>
                <option value=""></option>
                @foreach ($request_for_payment_type_references as $request_for_payment_type_reference)
                    <option value="{{ $request_for_payment_type_reference->id }}">
                        {{ $request_for_payment_type_reference->display }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="request_type" class="mt-2" />
        </div>
        <div>
            <x-label for="payee" value="Payee" />
            <x-input type="text" name="payee" wire:model.debounce.500ms="payee" />
            <x-input-error for="payee" class="mt-2" />
        </div>
        <div>
            <x-label for="opex_type" value="Opex Type" />
            <x-select name="opex_type" wire:model.debounce.500ms='opex_type'>
                <option value=""></option>
                @foreach ($opex_type_references as $opex_type_reference)
                    <option value="{{ $opex_type_reference->id }}">
                        {{ $opex_type_reference->name }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="opex_type" class="mt-2" />
        </div>
        <div>
            <x-label for="requester" value="Requester" />
            <x-input type="text" name="requester" wire:model.debounce.500ms="requester" />
            <x-input-error for="requester" class="mt-2" />
        </div>
        <div>
            <x-label for="description" value="Description" />
            <x-input type="text" name="description" wire:model.debounce.500ms="description" />
            <x-input-error for="description" class="mt-2" />
        </div>
        <div>
            <x-label for="date_created" value="Date Created From" />
            <x-input type="date" name="date_created" wire:model.debounce.500ms="date_created" />
            <x-input-error for="date_created" class="mt-2" />
        </div>
        <div>
            <x-label for="date_created_to" value="Date Created To" />
            <x-input type="date" name="date_created_to" wire:model.debounce.500ms="date_created_to" />
            <x-input-error for="date_created_to" class="mt-2" />
        </div>
        <div>
            <x-label for="status" value="Status" />
            <x-select name="status" wire:model='status'>
                <option value=""></option>
                @foreach ($status_references as $status_reference)
                    <option value="{{ $status_reference->id }}">
                        {{ $status_reference->display }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="status" class="mt-2" />
        </div>
        <div>
            <x-label for="approver" value="Approver" />
            <x-select name="approver" wire:model="approver">
                <option value=""></option>
                <option value="1">Approver 1</option>
                <option value="2">Approver 2</option>
                <option value="3">Approver 3</option>
            </x-select>
            <x-input-error for="naapproverme" class="mt-2" />
        </div>
        <div>
            <x-label for="priority" value="Priority" />
            <x-select name="priority" wire:model='priority'>
                <option value=""></option>
                @foreach ($priority_references as $priority_reference)
                    <option value="{{ $priority_reference->id }}">
                        {{ $priority_reference->display }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="priority" class="mt-2" />
        </div>
        <div>
            <x-label for="date_needed" value="Date Needed From" />
            <x-input type="date" name="date_needed" wire:model.debounce.500ms="date_needed" />
            <x-input-error for="date_needed" class="mt-2" />
        </div>
        <div>
            <x-label for="date_needed_to" value="Date Needed To" />
            <x-input type="date" name="date_needed_to" wire:model.debounce.500ms="date_needed_to" />
            <x-input-error for="date_needed_to" class="mt-2" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="mb-6 text-3xl font-medium">
                <div class="flex items-center space-x-3">
                    <span>Pending For Approval</span>
                </div>
            </div>
            <div class="grid grid-cols-4 gap-3 mb-6" cloak>
                @foreach ($users as $i => $user)
                    <div wire:click="checkbox('{{ $user_id }}', '{{ $user['id'] }}')"
                        class="{{ $user_id == $user['id'] ? 'bg-blue-900' : 'bg-white' }} flex flex-col py-3 space-y-3 text-center   rounded-lg shadow-lg cursor-pointer">
                        <p class="uppercase">{{ $user['name'] }}</p>
                        <div class="text-4xl font-extrabold text-blue">
                            {{ $user['rfp_first_approver_count'] + $user['rfp_second_approver_count'] + $user['rfp_third_approver_count'] }}
                        </div>
                        <div class="grid grid-cols-3 gap-3 text-xs">
                            <div class="flex flex-col">
                                <span>{{ $user['rfp_first_approver_count'] }}</span>
                                <span>Approver 1</span>
                            </div>
                            <div class="flex flex-col">
                                <span>{{ $user['rfp_second_approver_count'] }}</span>
                                <span>Approver 2</span>
                            </div>
                            <div class="flex flex-col">
                                <span>{{ $user['rfp_third_approver_count'] }}</span>
                                <span>Approver 3</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Priority" />
                        <x-table.th name="Reference No." wire:click="sortBy('reference_id')">
                            <x-slot name="sort">
                                <x-table.sort-icon field="reference_id" sortField="{{ $sortField }}"
                                    sortAsc="{{ $sortAsc }}">
                                </x-table.sort-icon>
                            </x-slot>
                        </x-table.th>
                        <x-table.th name="Payee" />
                        <x-table.th name="Request Type" />
                        <x-table.th name="Amount" />
                        <x-table.th name="Requested By" />
                        <x-table.th name="Date Needed" wire:click="sortBy('date_needed')">
                            <x-slot name="sort">
                                <x-table.sort-icon field="date_needed" sortField="{{ $sortField }}"
                                    sortAsc="{{ $sortAsc }}">
                                </x-table.sort-icon>
                            </x-slot>
                        </x-table.th>
                        <x-table.th name="Approver 1" />
                        <x-table.th name="Approver 2" />
                        <x-table.th name="Approver 3" />
                        <x-table.th name="Approver 1 Date" />
                        <x-table.th name="Approver 2 Date" />
                        <x-table.th name="Approver 3 Date" />
                        <x-table.th name="Status" />
                        <x-table.th name="Remarks 1" />
                        <x-table.th name="Remarks 2" />
                        <x-table.th name="Remarks 3" />
                        <x-table.th name="Check Voucher" />
                        <x-table.th name="Release Date" />
                        <x-table.th name="Created At" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($request_for_payments as $request_for_payment)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->priority->display }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($request_for_payment->parent_reference_no)
                                        <p wire:click="action({'parent_reference_no': '{{ $request_for_payment->parent_reference_no }}'}, 'view')"
                                            class="underline underline-offset-4 text-blue">
                                            {{ $request_for_payment->reference_id }}</p>
                                    @else
                                        <a href="https://cis2.capex.com.ph/index.php?r=acRfPayment/ViewApprovalDetails&AcRfPayment%5Brfpa_ref_id%5D={{ $request_for_payment->reference_id }}"
                                            target="_blank"
                                            class="underline underline-offset-4 text-blue">{{ $request_for_payment->reference_id }}</a>
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->payee->company }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->type->display ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($request_for_payment->amount, 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->user->name ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->date_needed }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->approver_1_id ? (is_null($request_for_payment->is_approved_1) ? 'Pending' : ($request_for_payment->is_approved_1 ? 'Approved' : 'Reject')) : 'N/A' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->approver_2_id ? (is_null($request_for_payment->is_approved_2) ? 'Pending' : ($request_for_payment->is_approved_2 ? 'Approved' : 'Reject')) : 'N/A' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->approver_3_id ? (is_null($request_for_payment->is_approved_3) ? 'Pending' : ($request_for_payment->is_approved_3 ? 'Approved' : 'Reject')) : 'N/A' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->approver_1_id ? (is_null($request_for_payment->is_approved_1) ? '-' : ($request_for_payment->is_approved_1 ? $request_for_payment->approver_date_1 : $request_for_payment->approver_date_1)) : 'N/A' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->approver_2_id ? (is_null($request_for_payment->is_approved_2) ? '-' : ($request_for_payment->is_approved_2 ? $request_for_payment->approver_date_2 : $request_for_payment->approver_date_2)) : 'N/A' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->approver_3_id ? (is_null($request_for_payment->is_approved_3) ? '-' : ($request_for_payment->is_approved_3 ? $request_for_payment->approver_date_3 : $request_for_payment->approver_date_3)) : 'N/A' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->status->display ?? 'Not Set' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->approver_remarks_1 ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->approver_remarks_2 ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->approver_remarks_3 ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($request_for_payment->checkVoucher)
                                        <p wire:click="action({'check_voucher_id': '{{ $request_for_payment->checkVouchers[0]->id }}'}, 'view_voucher')"
                                            class="underline underline-offset-4 text-blue">
                                            {{ $request_for_payment->checkVouchers[0]->reference_no }}</p>
                                    @else
                                        Not Set
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->checkVoucher[0]->release_date ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('M. d, Y', strtotime($request_for_payment->created_at)) }}
                                </td>

                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        <svg wire:click="action({'id': {{ $request_for_payment->id }}}, 'edit')"
                                            class="w-5 h-5 text-blue" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 512 512">
                                            <path fill="currentColor"
                                                d="M128 447.1V223.1c0-17.67-14.33-31.1-32-31.1H32c-17.67 0-32 14.33-32 31.1v223.1c0 17.67 14.33 31.1 32 31.1h64C113.7 479.1 128 465.6 128 447.1zM512 224.1c0-26.5-21.48-47.98-48-47.98h-146.5c22.77-37.91 34.52-80.88 34.52-96.02C352 56.52 333.5 32 302.5 32c-63.13 0-26.36 76.15-108.2 141.6L178 186.6C166.2 196.1 160.2 210 160.1 224c-.0234 .0234 0 0 0 0L160 384c0 15.1 7.113 29.33 19.2 38.39l34.14 25.59C241 468.8 274.7 480 309.3 480H368c26.52 0 48-21.47 48-47.98c0-3.635-.4805-7.143-1.246-10.55C434 415.2 448 397.4 448 376c0-9.148-2.697-17.61-7.139-24.88C463.1 347 480 327.5 480 304.1c0-12.5-4.893-23.78-12.72-32.32C492.2 270.1 512 249.5 512 224.1z" />
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $request_for_payments->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
