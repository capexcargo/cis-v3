<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-3 gap-3">
                <div>
                    <x-label for="reference_no" value="Voucher Reference No." :required="true" />
                    <x-input type="text" name="reference_no" value="{{ $check_voucher->reference_no }}" disabled />
                    <x-input-error for="reference_no" />

                </div>
                <div>
                    <x-label for="rfp_reference_no" value="RFP Reference No." :required="true" />
                    <x-input type="text" name="rfp_reference_no"
                        value="{{ $check_voucher->requestForPayment->reference_id }}" disabled />
                    <x-input-error for="rfp_reference_no" />
                </div>
                <div>
                    <x-label for="cv_no" value="CV No" :required="true" />
                    <x-input type="text" name="cv_no" value="{{ $check_voucher->cv_no }}" disabled />
                    <x-input-error for="cv_no" />
                </div>
            </div>
            <div class="grid grid-cols-3 gap-3">
                <div>
                    <x-label for="payee_id" value="Pay To" :required="true" />
                    <x-input type="text" name="payee_id"
                        value="{{ $check_voucher->requestForPayment->payee->company }}" disabled />
                    <x-input-error for="payee_id" />

                </div>
                <div>
                    <x-label for="opex_type_id" value="Opex Type" :required="true" />
                    <x-input type="text" name="opex_type_id"
                        value="{{ $check_voucher->requestForPayment->opex->name }}" disabled />
                    <x-input-error for="opex_type_id" />
                </div>
                <div>
                    <x-label for="total_amount" value="Total Amount" :required="true" />
                    <x-input type="text" name="total_amount" value="{{ $check_voucher->requestForPayment->amount }}"
                        disabled />
                    <x-input-error for="total_amount" />
                </div>
            </div>
            <div class="grid grid-cols-3 gap-3">
                <div>
                    <x-label for="description" value="Description" :required="true" />
                    <x-textarea type="text" name="description" disabled>{{ $check_voucher->description }}</x-textarea>
                    <x-input-error for="description" />
                </div>
            </div>
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Check Details</span>
                </div>
            </div>
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="Date" />
                    <x-table.th name="Check No." />
                    <x-table.th name="Description" />
                    <x-table.th name="Amount" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($check_voucher->checkVoucherDetails as $a => $check_voucher_details)
                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                            <td class="p-3 whitespace-nowrap">
                                {{ date('M. d, Y', strtotime($check_voucher_details->date)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $check_voucher_details->voucher_no }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $check_voucher_details->description }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ number_format($check_voucher_details->amount, 2) }}
                            </td>
                        </tr>
                    @endforeach
                </x-slot>
            </x-table.table>
            <x-input-error for="vouchers" />
            <div class="flex flex-col items-end justify-end">
                {{ number_format($check_voucher->total_amount, 2) }}
                <x-input-error for="total_voucher_amount" />
            </div>
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Approver</span>
                </div>
            </div>
            <div class="grid grid-cols-2 gap-3">
                <div class="space-y-3">
                    <div>
                        <div>
                            <x-label for="approver_1_id" value="Approver 1" :required="true" />
                            <x-input type="text" name="approver_1_id" value="{{ $approver_1_name }}" disabled>
                            </x-input>
                            <x-input-error for="approver_1_id" />
                        </div>
                        <x-input-error for="approver_1_id" />
                    </div>
                    @if ($check_voucher->approver_1_id == auth()->user()->id || (auth()->user()->level_id == 5 && $check_voucher->approver_1_id))
                        <div class="flex justify-between">
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_1" wire:model.defer="is_approved_1" value="1">
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_1">Approve</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_1" wire:model.defer="is_approved_1" value="0">
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_1">Disapprove</label>
                            </div>
                        </div>
                        <div>
                            <x-label for="approver_remarks_1" value="Remarks" />
                            <x-textarea type="text" name="approver_remarks_1" wire:model.defer='approver_remarks_1' />
                            <x-input-error for="approver_remarks_1" />
                        </div>
                    @endif
                </div>
                <div class="space-y-3">
                    <div>
                        <x-label for="approver_2_id" value="Approver 2" />
                        <x-input type="text" name="approver_2_id" value="{{ $approver_2_name }}" disabled></x-input>
                        <x-input-error for="approver_2_id" />
                    </div>
                    @if ($check_voucher->approver_2_id == auth()->user()->id || (auth()->user()->level_id == 5 && $check_voucher->approver_2_id))
                        <div class="flex justify-between">
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_2" wire:model.defer="is_approved_2" value="1">
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_2">Approve</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_2" wire:model.defer="is_approved_2" value="0">
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_2">Disapprove</label>
                            </div>
                        </div>
                        <div>
                            <x-label for="approver_remarks_2" value="Remarks" />
                            <x-textarea type="text" name="approver_remarks_2" wire:model.defer='approver_remarks_2' />
                            <x-input-error for="approver_remarks_2" />
                        </div>
                    @endif
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Proceed</button>
            </div>
        </div>
    </form>
</div>
