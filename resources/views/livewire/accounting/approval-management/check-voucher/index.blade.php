<x-form wire:init="load" x-data="{ search_form: false, edit_modal: '{{ $edit_modal }}', view_modal: '{{ $view_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @if ($check_voucher_id)
            <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-6/12">
                <x-slot name="title">Approve Check Voucher</x-slot>
                <x-slot name="body">
                    @livewire('accounting.approval-management.check-voucher.edit', ['id' => $check_voucher_id])
                </x-slot>
            </x-modal>
        @endif
        @if ($parent_reference_no)
            <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                <x-slot name="title">View Request</x-slot>
                <x-slot name="body">
                    @livewire('accounting.request-management.view', ['parent_reference_no' => $parent_reference_no])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">Check Voucher Approval</x-slot>
    <x-slot name="header_card">
        @forelse ($header_cards as $index => $card)
            <x-card.header wire:click="$set('{{ $card['action'] }}', {{ $card['id'] }})"
                wire:key="{{ $index }}" :card="$card"></x-card.header>
        @empty
            <x-card.header-loading count="4"></x-card.header-loading>
        @endforelse
    </x-slot>
    <x-slot name="search_form">
        <div>
            <x-label for="rfp_reference_no" value="RFP Reference No." />
            <x-input type="text" name="rfp_reference_no" wire:model.debounce.500ms="rfp_reference_no" />
            <x-input-error for="rfp_reference_no" class="mt-2" />
        </div>
        <div>
            <x-label for="check_reference_no" value="Check Reference No." />
            <x-input type="text" name="check_reference_no" wire:model.debounce.500ms="check_reference_no" />
            <x-input-error for="check_reference_no" class="mt-2" />
        </div>
        <div>
            <x-label for="cv_no" value="CV No." />
            <x-input type="text" name="cv_no" wire:model.debounce.500ms="cv_no" />
            <x-input-error for="cv_no" class="mt-2" />
        </div>
        <div>
            <x-label for="check_no" value="Check No." />
            <x-input type="text" name="check_no" wire:model.debounce.500ms="check_no" />
            <x-input-error for="check_no" class="mt-2" />
        </div>
        <div>
            <x-label for="created_by" value="Created By" />
            <x-input type="text" name="created_by" wire:model.debounce.500ms="created_by" />
            <x-input-error for="created_by" class="mt-2" />
        </div>
        <div>
            <x-label for="received_by" value="Received By" />
            <x-input type="text" name="received_by" wire:model.debounce.500ms="received_by" />
            <x-input-error for="received_by" class="mt-2" />
        </div>
        <div>
            <x-label for="received_date_from" value="Received Date From" />
            <x-input type="date" name="received_date_from" wire:model.debounce.500ms="received_date_from" />
            <x-input-error for="received_date_from" class="mt-2" />
        </div>
        <div>
            <x-label for="received_date_to" value="Received Date To" />
            <x-input type="date" name="received_date_to" wire:model.debounce.500ms="received_date_to" />
            <x-input-error for="received_date_to" class="mt-2" />
        </div>
        <div>
            <x-label for="created_date_from" value="Created Date From" />
            <x-input type="date" name="created_date_from" wire:model.debounce.500ms="created_date_from" />
            <x-input-error for="created_date_from" class="mt-2" />
        </div>
        <div>
            <x-label for="created_date_to" value="Created Date To" />
            <x-input type="date" name="created_date_to" wire:model.debounce.500ms="created_date_to" />
            <x-input-error for="created_date_to" class="mt-2" />
        </div>
        <div>
            <x-label for="released_date_from" value="Released Date From" />
            <x-input type="date" name="released_date_from" wire:model.debounce.500ms="released_date_from" />
            <x-input-error for="released_date_from" class="mt-2" />
        </div>
        <div>
            <x-label for="released_date_to" value="Released Date To" />
            <x-input type="date" name="released_date_to" wire:model.debounce.500ms="released_date_to" />
            <x-input-error for="released_date_to" class="mt-2" />
        </div>
        <div>
            <x-label for="status" value="Status" />
            <x-select name="status" wire:model='status'>
                <option value=""></option>
                @foreach ($status_references as $status_reference)
                    <option value="{{ $status_reference->id }}">
                        {{ $status_reference->display }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="status" class="mt-2" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Select" />
                        <x-table.th name="RFP Reference No." />
                        <x-table.th name="Reference No." />
                        <x-table.th name="CV No" />
                        <x-table.th name="Payee" />
                        <x-table.th name="Amount" />
                        <x-table.th name="Prepared By" />
                        <x-table.th name="Date Prepared" />
                        <x-table.th name="Approver 1" />
                        <x-table.th name="Approver 2" />
                        <x-table.th name="Status" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($request_for_payments as $request_for_payment)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    <input type="checkbox" class="form-checkbox" wire:model="selected">
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($request_for_payment->parent_reference_no)
                                        <p wire:click="action({'parent_reference_no': '{{ $request_for_payment->parent_reference_no }}'}, 'view')"
                                            class="underline underline-offset-4 text-blue">
                                            {{ $request_for_payment->reference_id }}</p>
                                    @else
                                        <a href="https://cis2.capex.com.ph/index.php?r=acRfPayment/ViewApprovalDetails&AcRfPayment%5Brfpa_ref_id%5D={{ $request_for_payment->reference_id }}"
                                            target="_blank"
                                            class="underline underline-offset-4 text-blue">{{ $request_for_payment->reference_id }}</a>
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->checkVoucher->reference_no }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->checkVoucher->cv_no }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->payee->company }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->checkVoucher->total_amount }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->checkVoucher->createdBy->name }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('M. d, Y', strtotime($request_for_payment->checkVoucher->created_at)) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->checkVoucher->approver_1_id ? (is_null($request_for_payment->checkVoucher->is_approved_1) ? 'Pending' : ($request_for_payment->checkVoucher->is_approved_1 ? 'Approved' : 'Reject')) : 'Not Set' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->checkVoucher->approver_2_id ? (is_null($request_for_payment->checkVoucher->is_approved_2) ? 'Pending' : ($request_for_payment->checkVoucher->is_approved_2 ? 'Approved' : 'Reject')) : 'Not Set' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->checkVoucher->status->display }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        <svg wire:click="action({'id': {{ $request_for_payment->checkVoucher->id }}}, 'edit')"
                                            class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                            data-prefix="far" data-icon="edit" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                            </path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $request_for_payments->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
