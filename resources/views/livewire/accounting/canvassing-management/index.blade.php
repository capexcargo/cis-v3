<x-form x-data="{ search_form: false, create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('accounting_canvassing_management_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                <x-slot name="title">Create Canvassing</x-slot>
                <x-slot name="body">
                    @livewire('accounting.canvassing-management.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('accounting_canvassing_management_edit')
            @if ($canvassing_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-9/12">
                    <x-slot name="title">Edit Canvassing</x-slot>
                    <x-slot name="body">
                        @livewire('accounting.canvassing-management.edit', ['id' => $canvassing_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>
    <x-slot name="header_title">Canvassing List</x-slot>
    <x-slot name="header_button">
        @can('accounting_canvassing_management_add')
            <button wire:click="action({}, 'create')"
                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Create Canvassing</button>
        @endcan
    </x-slot>
    <x-slot name="search_form">
        <div>
            <x-label for="reference_number" value="Reference Number" />
            <x-input type="text" name="reference_number" wire:model.debounce.500ms="reference_number" />
            <x-input-error for="reference_number" class="mt-2" />
        </div>
        <div>
            <x-label for="company" value="Company" />
            <x-input type="text" name="company" wire:model.debounce.500ms="company" />
            <x-input-error for="company" class="mt-2" />
        </div>
        <div>
            <x-label for="item_name" value="Item Name" />
            <x-input type="text" name="item_name" wire:model.debounce.500ms="item_name" />
            <x-input-error for="item_name" class="mt-2" />
        </div>
        <div>
            <x-label for="assigned" value="Assigned" />
            <x-select name="assigned" wire:model="assigned">
                <option></option>
                <option value="yes">Yes</option>
                <option value="no">No</option>
            </x-select>
            <x-input-error for="assigned" class="mt-2" />
        </div>
        <div>
            <x-label for="first_name" value="First Name" />
            <x-input type="text" name="first_name" wire:model.debounce.500ms="first_name" />
            <x-input-error for="first_name" class="mt-2" />
        </div>
        <div>
            <x-label for="middle_name" value="Middle Name" />
            <x-input type="text" name="middle_name" wire:model.debounce.500ms="middle_name" />
            <x-input-error for="middle_name" class="mt-2" />
        </div>
        <div>
            <x-label for="last_name" value="Last Name" />
            <x-input type="text" name="last_name" wire:model.debounce.500ms="last_name" />
            <x-input-error for="last_name" class="mt-2" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Reference Number" />
                        <x-table.th name="Created By" />
                        <x-table.th name="PO Reference No." />
                        <x-table.th name="Company" />
                        <x-table.th name="Is Assigned" />
                        <x-table.th name="Created At" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($canvassings as $canvassing)
                            @foreach ($canvassing->canvassingSupplier as $canvassing_supplier)
                                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                    @if ($loop->first)
                                        <td rowspan="{{ $canvassing->canvassingSupplier->count() }}"
                                            class="p-3 whitespace-nowrap">
                                            {{ $canvassing->reference_id }}
                                        </td>
                                        <td rowspan="{{ $canvassing->canvassingSupplier->count() }}"
                                            class="p-3 whitespace-nowrap">
                                            {{ $canvassing->createdBy->name ?? ""}}
                                        </td>
                                    @endif
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $canvassing_supplier->po_reference_no }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $canvassing_supplier->supplier->company }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <span
                                            class="{{ $canvassing_supplier->requestForPayment ? 'text-green' : 'text-red' }}">{{ $canvassing_supplier->requestForPayment ? 'Yes' : 'No' }}</span>
                                    </td>
                                    @if ($loop->first)
                                        <td rowspan="{{ $canvassing->canvassingSupplier->count() }}"
                                            class="p-3 whitespace-nowrap">
                                            {{ date('M. d, Y', strtotime($canvassing->created_at)) }}
                                        </td>
                                        <td rowspan="{{ $canvassing->canvassingSupplier->count() }}"
                                            class="p-3 whitespace-nowrap">
                                            <div class="flex space-x-3">
                                                @can('accounting_canvassing_management_edit')
                                                    <svg wire:click="action({'id': {{ $canvassing->id }}}, 'edit')"
                                                        class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                        data-prefix="far" data-icon="edit" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                        </path>
                                                    </svg>
                                                @endcan
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $canvassings->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
