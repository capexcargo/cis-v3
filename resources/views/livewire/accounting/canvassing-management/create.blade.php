<div x-data="{ confirmation_modal: '{{ $confirmation_modal }}' }">
    <x-loading />
    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            {{ $confirmation_message }}
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">No</button>
                <button type="button" wire:click="remove"
                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Yes</button>
            </div>
        </x-slot>
    </x-modal>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3" wire:init="load">
            <div class="grid grid-cols-3 gap-3">
                <div>
                    <x-label for="reference_number" value="Reference Number" :required="true" />
                    <x-input type="text" name="reference_number" value="{{ $reference_number }}" disabled></x-input>
                    <x-input-error for="reference_number" />
                </div>
            </div>
            @foreach ($suppliers as $a => $supplier)
                <div class="text-xl font-semibold text-gray-800">
                    <div class="flex items-center justify-between space-x-3">
                        <span>{{ $a + 1 }}. Supplier</span>
                        @if (count($suppliers) > 1)
                            <svg wire:click="action({'a': {{ $a }}}, 'remove_supplier')"
                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false" data-prefix="fas"
                                data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 448 512">
                                <path fill="currentColor"
                                    d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                </path>
                            </svg>
                        @endif
                    </div>
                </div>
                <div class="grid grid-cols-3 gap-3">
                    <div>
                        <x-label for="suppliers.{{ $a }}.company" value="Company" :required="true" />
                        <x-input type="text" list="supplier_suggestions"
                            wire:click="action({'a': {{ $a }}}, 'suggestions_supplier')"
                            name="suppliers.{{ $a }}.company"
                            wire:model.debounce.500ms='suppliers.{{ $a }}.company'></x-input>
                        <datalist id="supplier_suggestions">
                            @foreach ($supplier_references as $supplier_reference)
                                <option>{{ $supplier_reference->company }}</option>
                            @endforeach
                        </datalist>
                        <x-input-error for="suppliers.{{ $a }}.company" />
                    </div>
                    <div>
                        <x-label for="suppliers.{{ $a }}.industry" value="Industry" :required="true" />
                        <x-select name="suppliers.{{ $a }}.industry"
                            wire:model.defer='suppliers.{{ $a }}.industry'>
                            <option value=""></option>
                            @foreach ($supplier_industry_references as $supplier_industry_reference)
                                <option value="{{ $supplier_industry_reference->id }}">
                                    {{ $supplier_industry_reference->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="suppliers.{{ $a }}.industry" />
                    </div>
                    <div>
                        <x-label for="suppliers.{{ $a }}.type" value="Type" :required="true" />
                        <x-select name="suppliers.{{ $a }}.type"
                            wire:model.defer='suppliers.{{ $a }}.type'>
                            <option value=""></option>
                            @foreach ($supplier_type_references as $supplier_type_reference)
                                <option value="{{ $supplier_type_reference->id }}">
                                    {{ $supplier_type_reference->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="suppliers.{{ $a }}.type" />
                    </div>
                </div>
                <div class="text-xl font-semibold text-gray-800">
                    <div class="flex items-center space-x-3">
                        <span>Contact Person</span>
                    </div>
                </div>
                <div class="grid grid-cols-3 gap-3">
                    <div>
                        <x-label for="suppliers.{{ $a }}.first_name" value="First Name" :required="true" />
                        <x-input type="text" name="suppliers.{{ $a }}.first_name"
                            wire:model.defer='suppliers.{{ $a }}.first_name'></x-input>
                        <x-input-error for="suppliers.{{ $a }}.first_name" />
                    </div>
                    <div>
                        <x-label for="suppliers.{{ $a }}.middle_name" value="Middle Name"
                            :required="true" />
                        <x-input type="text" name="suppliers.{{ $a }}.middle_name"
                            wire:model.defer='suppliers.{{ $a }}.middle_name'></x-input>
                        <x-input-error for="suppliers.{{ $a }}.middle_name" />
                    </div>
                    <div>
                        <x-label for="suppliers.{{ $a }}.last_name" value="Last Name" :required="true" />
                        <x-input type="text" name="suppliers.{{ $a }}.last_name"
                            wire:model.defer='suppliers.{{ $a }}.last_name'></x-input>
                        <x-input-error for="suppliers.{{ $a }}.last_name" />
                    </div>
                    <div>
                        <x-label for="suppliers.{{ $a }}.email" value="Email" :required="true" />
                        <x-input type="text" name="suppliers.{{ $a }}.email"
                            wire:model.defer='suppliers.{{ $a }}.email'></x-input>
                        <x-input-error for="suppliers.{{ $a }}.email" />
                    </div>
                    <div>
                        <x-label for="suppliers.{{ $a }}.mobile_number" value="Mobile Number"
                            :required="true" />
                        <x-input type="number" name="suppliers.{{ $a }}.mobile_number"
                            wire:model.defer='suppliers.{{ $a }}.mobile_number'></x-input>
                        <x-input-error for="suppliers.{{ $a }}.mobile_number" />
                    </div>
                    <div>
                        <x-label for="suppliers.{{ $a }}.telephone_number" value="Telephone Number"
                            :required="true" />
                        <x-input type="number" name="suppliers.{{ $a }}.telephone_number"
                            wire:model.defer='suppliers.{{ $a }}.telephone_number'></x-input>
                        <x-input-error for="suppliers.{{ $a }}.telephone_number" />
                    </div>
                    <div>
                        <x-label for="suppliers.{{ $a }}.branch" value="Branch" :required="true" />
                        <x-select name="suppliers.{{ $a }}.branch"
                            wire:model.defer='suppliers.{{ $a }}.branch'>
                            <option value=""></option>
                            @foreach ($branch_references as $branch_reference)
                                <option value="{{ $branch_reference->id }}">
                                    {{ $branch_reference->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="suppliers.{{ $a }}.branch" />
                    </div>
                    <div>
                        <x-label for="suppliers.{{ $a }}.address" value="Address" :required="true" />
                        <x-input type="text" name="suppliers.{{ $a }}.address"
                            wire:model.defer='suppliers.{{ $a }}.address'></x-input>
                        <x-input-error for="suppliers.{{ $a }}.address" />
                    </div>
                    <div>
                        <x-label for="suppliers.{{ $a }}.terms" value="Terms" :required="true" />
                        <x-input type="number" name="suppliers.{{ $a }}.terms"
                            wire:model.defer='suppliers.{{ $a }}.terms'></x-input>
                        <x-input-error for="suppliers.{{ $a }}.terms" />
                    </div>
                </div>
                <div class="text-xl font-semibold text-gray-800">
                    <div class="flex items-center space-x-3">
                        <span>Items</span>
                    </div>
                </div>
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Name" />
                        <x-table.th name="Unit Cost" />
                        <x-table.th name="Quantity" />
                        <x-table.th name="Total Amount" />
                        <x-table.th name="Remove" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($supplier['items'] as $b => $item)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    <x-input type="text" list="supplier_item_suggestions"
                                        wire:click="action({'a': {{ $a }}, 'b': {{ $b }}}, 'suggestions_supplier_item_name')"
                                        name="suppliers.{{ $a }}.items.{{ $b }}.name"
                                        wire:model.debounce.500ms='suppliers.{{ $a }}.items.{{ $b }}.name'>
                                    </x-input>
                                    <datalist id="supplier_item_suggestions">
                                        @foreach ($supplier_item_references as $supplier_item_reference)
                                            <option>{{ $supplier_item_reference->name }}</option>
                                        @endforeach
                                    </datalist>
                                    <x-input-error
                                        for="suppliers.{{ $a }}.items.{{ $b }}.name" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <x-input type="number"
                                        wire:click="action({'a': {{ $a }}, 'b': {{ $b }}}, 'suggestions_supplier_item_unit_cost')"
                                        name="suppliers.{{ $a }}.items.{{ $b }}.unit_cost"
                                        wire:model.debounce.500ms='suppliers.{{ $a }}.items.{{ $b }}.unit_cost'
                                        step="0.01">
                                    </x-input>
                                    <x-input-error
                                        for="suppliers.{{ $a }}.items.{{ $b }}.unit_cost" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <x-input type="number"
                                        name="suppliers.{{ $a }}.items.{{ $b }}.quantity"
                                        wire:model.debounce.500ms='suppliers.{{ $a }}.items.{{ $b }}.quantity'
                                        step="0.01">
                                    </x-input>
                                    <x-input-error
                                        for="suppliers.{{ $a }}.items.{{ $b }}.quantity" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <x-input type="number"
                                        name="suppliers.{{ $a }}.items.{{ $b }}.total_amount"
                                        wire:model.defer='suppliers.{{ $a }}.items.{{ $b }}.total_amount'
                                        step="0.01" disabled>
                                    </x-input>
                                    <x-input-error
                                        for="suppliers.{{ $a }}.items.{{ $b }}.total_amount" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        @if (count($suppliers[$a]['items']) > 1)
                                            <svg wire:click="action({'a': {{ $a }}, 'b': {{ $b }}}, 'remove_item')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="fas" data-icon="trash-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                                </path>
                                            </svg>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <x-input-error for="suppliers.{{ $a }}.items" />
                <div class="flex justify-end">
                    <span class="text-lg font-semibold">{{ $suppliers[$a]['total_amount'] }}</span>
                </div>
                <div class="flex justify-end space-x-3">
                    <button type="button" wire:click="addSupplierItem({{ $a }})"
                        class="flex-none px-3 py-1 text-sm text-white rounded-full bg-green">
                        Add Item</button>
                </div>
            @endforeach
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="addSupplier"
                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Add Supplier</button>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Create</button>
            </div>
        </div>
    </form>
</div>
