<div x-data="{ confirmation_modal: '{{ $confirmation_modal }}' }">
    <x-loading></x-loading>
    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            {{ $confirmation_message }}
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">No</button>
                <button type="button" wire:click="remove"
                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Yes</button>
            </div>
        </x-slot>
    </x-modal>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3" wire:init="load">
            <div class="grid grid-cols-3 gap-3">
                <div>
                    <x-label for="reference_number" value="Reference Number" :required="true" />
                    <x-input type="text" name="reference_number" value="{{ $reference_number }}" disabled></x-input>
                    <x-input-error for="reference_number" />
                </div>
            </div>
            @foreach ($suppliers as $a => $supplier)
                @if (!$supplier['is_deleted'])
                    <div class="text-xl font-semibold text-gray-800">
                        <div class="flex items-center justify-between space-x-3">
                            <span>{{ $a + 1 }}. Supplier</span>
                            @if ($canvassing->canvassingSupplier[$a]->requestForPayment)
                                <button type="button" wire:click="action({'a': {{ $a }}}, 'untagged_PO')"
                                    class="px-3 py-1 text-sm flex bg-[#003399] text-white rounded-full space-x-1">
                                    <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 640 512">
                                        <path fill="currentColor"
                                            d="M172.5 131.1C228.1 75.51 320.5 75.51 376.1 131.1C426.1 181.1 433.5 260.8 392.4 318.3L391.3 319.9C381 334.2 361 337.6 346.7 327.3C332.3 317 328.9 297 339.2 282.7L340.3 281.1C363.2 249 359.6 205.1 331.7 177.2C300.3 145.8 249.2 145.8 217.7 177.2L105.5 289.5C73.99 320.1 73.99 372 105.5 403.5C133.3 431.4 177.3 435 209.3 412.1L210.9 410.1C225.3 400.7 245.3 404 255.5 418.4C265.8 432.8 262.5 452.8 248.1 463.1L246.5 464.2C188.1 505.3 110.2 498.7 60.21 448.8C3.741 392.3 3.741 300.7 60.21 244.3L172.5 131.1zM467.5 380C411 436.5 319.5 436.5 263 380C213 330 206.5 251.2 247.6 193.7L248.7 192.1C258.1 177.8 278.1 174.4 293.3 184.7C307.7 194.1 311.1 214.1 300.8 229.3L299.7 230.9C276.8 262.1 280.4 306.9 308.3 334.8C339.7 366.2 390.8 366.2 422.3 334.8L534.5 222.5C566 191 566 139.1 534.5 108.5C506.7 80.63 462.7 76.99 430.7 99.9L429.1 101C414.7 111.3 394.7 107.1 384.5 93.58C374.2 79.2 377.5 59.21 391.9 48.94L393.5 47.82C451 6.731 529.8 13.25 579.8 63.24C636.3 119.7 636.3 211.3 579.8 267.7L467.5 380z" />
                                    </svg>
                                    <span>Untagged</span>
                                </button>
                            @else
                                @if (count($suppliers) > 1)
                                    <svg wire:click="action({'a': {{ $a }}}, 'remove_supplier')"
                                        class="w-5 h-5 text-blue" aria-hidden="true" focusable="false" data-prefix="fas"
                                        data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 448 512">
                                        <path fill="currentColor"
                                            d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                        </path>
                                    </svg>
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-3">
                        <div>
                            <x-label for="suppliers.{{ $a }}.company" value="Company" :required="true" />
                            <x-input type="text" list="supplier_suggestions"
                                wire:click="action({'a': {{ $a }}}, 'suggestions_supplier')"
                                name="suppliers.{{ $a }}.company"
                                wire:model.debounce.500ms='suppliers.{{ $a }}.company'></x-input>
                            <datalist id="supplier_suggestions">
                                @foreach ($supplier_references as $supplier_reference)
                                    <option>{{ $supplier_reference->company }}</option>
                                @endforeach
                            </datalist>
                            <x-input-error for="suppliers.{{ $a }}.company" />
                        </div>
                        <div>
                            <x-label for="suppliers.{{ $a }}.industry" value="Industry" :required="true" />
                            <x-select name="suppliers.{{ $a }}.industry"
                                wire:model.defer='suppliers.{{ $a }}.industry'>
                                <option value=""></option>
                                @foreach ($supplier_industry_references as $supplier_industry_reference)
                                    <option value="{{ $supplier_industry_reference->id }}">
                                        {{ $supplier_industry_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="suppliers.{{ $a }}.industry" />
                        </div>
                        <div>
                            <x-label for="suppliers.{{ $a }}.type" value="Type" :required="true" />
                            <x-select name="suppliers.{{ $a }}.type"
                                wire:model.defer='suppliers.{{ $a }}.type'>
                                <option value=""></option>
                                @foreach ($supplier_type_references as $supplier_type_reference)
                                    <option value="{{ $supplier_type_reference->id }}">
                                        {{ $supplier_type_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="suppliers.{{ $a }}.type" />
                        </div>
                    </div>
                    <div class="text-xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>Contact Person</span>
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-3">
                        <div>
                            <x-label for="suppliers.{{ $a }}.first_name" value="First Name"
                                :required="true" />
                            <x-input type="text" name="suppliers.{{ $a }}.first_name"
                                wire:model.defer='suppliers.{{ $a }}.first_name'></x-input>
                            <x-input-error for="suppliers.{{ $a }}.first_name" />
                        </div>
                        <div>
                            <x-label for="suppliers.{{ $a }}.middle_name" value="Middle Name"
                                :required="true" />
                            <x-input type="text" name="suppliers.{{ $a }}.middle_name"
                                wire:model.defer='suppliers.{{ $a }}.middle_name'></x-input>
                            <x-input-error for="suppliers.{{ $a }}.middle_name" />
                        </div>
                        <div>
                            <x-label for="suppliers.{{ $a }}.last_name" value="Last Name"
                                :required="true" />
                            <x-input type="text" name="suppliers.{{ $a }}.last_name"
                                wire:model.defer='suppliers.{{ $a }}.last_name'></x-input>
                            <x-input-error for="suppliers.{{ $a }}.last_name" />
                        </div>
                        <div>
                            <x-label for="suppliers.{{ $a }}.email" value="Email" :required="true" />
                            <x-input type="text" name="suppliers.{{ $a }}.email"
                                wire:model.defer='suppliers.{{ $a }}.email'></x-input>
                            <x-input-error for="suppliers.{{ $a }}.email" />
                        </div>
                        <div>
                            <x-label for="suppliers.{{ $a }}.mobile_number" value="Mobile Number"
                                :required="true" />
                            <x-input type="number" name="suppliers.{{ $a }}.mobile_number"
                                wire:model.defer='suppliers.{{ $a }}.mobile_number'></x-input>
                            <x-input-error for="suppliers.{{ $a }}.mobile_number" />
                        </div>
                        <div>
                            <x-label for="suppliers.{{ $a }}.telephone_number" value="Telephone Number"
                                :required="true" />
                            <x-input type="number" name="suppliers.{{ $a }}.telephone_number"
                                wire:model.defer='suppliers.{{ $a }}.telephone_number'></x-input>
                            <x-input-error for="suppliers.{{ $a }}.telephone_number" />
                        </div>
                        <div>
                            <x-label for="suppliers.{{ $a }}.branch" value="Branch" :required="true" />
                            <x-select name="suppliers.{{ $a }}.branch"
                                wire:model.defer='suppliers.{{ $a }}.branch'>
                                <option value=""></option>
                                @foreach ($branch_references as $branch_reference)
                                    <option value="{{ $branch_reference->id }}">
                                        {{ $branch_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="suppliers.{{ $a }}.branch" />
                        </div>
                        <div>
                            <x-label for="suppliers.{{ $a }}.address" value="Address" :required="true" />
                            <x-input type="text" name="suppliers.{{ $a }}.address"
                                wire:model.defer='suppliers.{{ $a }}.address'></x-input>
                            <x-input-error for="suppliers.{{ $a }}.address" />
                        </div>
                        <div>
                            <x-label for="suppliers.{{ $a }}.terms" value="Terms" :required="true" />
                            <x-input type="number" name="suppliers.{{ $a }}.terms"
                                wire:model.defer='suppliers.{{ $a }}.terms'></x-input>
                            <x-input-error for="suppliers.{{ $a }}.terms" />
                        </div>
                    </div>
                    <div class="text-xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>Items</span>
                        </div>
                    </div>
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="Name" />
                            <x-table.th name="Unit Cost" />
                            <x-table.th name="Quantity" />
                            <x-table.th name="Total Amount" />
                            <x-table.th name="Rec. Status" />
                            <x-table.th name="Final Status" />
                            @if (!$canvassing->canvassingSupplier[$a]->requestForPayment)
                                <x-table.th name="Rec. Status" />
                                <x-table.th name="Final Status" />
                                <x-table.th name="Remove" />
                            @endif
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($supplier['items'] as $b => $item)
                                @if (!$item['is_deleted'])
                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text" list="supplier_item_suggestions"
                                                wire:click="action({'a': {{ $a }}, 'b': {{ $b }}}, 'suggestions_supplier_item_name')"
                                                name="suppliers.{{ $a }}.items.{{ $b }}.name"
                                                wire:model.debounce.500ms='suppliers.{{ $a }}.items.{{ $b }}.name'>
                                            </x-input>
                                            <datalist id="supplier_item_suggestions">
                                                @foreach ($supplier_item_references as $supplier_item_reference)
                                                    <option>{{ $supplier_item_reference->name }}</option>
                                                @endforeach
                                            </datalist>
                                            <x-input-error
                                                for="suppliers.{{ $a }}.items.{{ $b }}.name" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number"
                                                wire:click="action({'a': {{ $a }}, 'b': {{ $b }}}, 'suggestions_supplier_item_unit_cost')"
                                                name="suppliers.{{ $a }}.items.{{ $b }}.unit_cost"
                                                wire:model.debounce.500ms='suppliers.{{ $a }}.items.{{ $b }}.unit_cost'
                                                step="0.01">
                                            </x-input>
                                            <x-input-error
                                                for="suppliers.{{ $a }}.items.{{ $b }}.unit_cost" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number"
                                                name="suppliers.{{ $a }}.items.{{ $b }}.quantity"
                                                wire:model.debounce.500ms='suppliers.{{ $a }}.items.{{ $b }}.quantity'
                                                step="0.01">
                                            </x-input>
                                            <x-input-error
                                                for="suppliers.{{ $a }}.items.{{ $b }}.quantity" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number"
                                                name="suppliers.{{ $a }}.items.{{ $b }}.total_amount"
                                                wire:model.defer='suppliers.{{ $a }}.items.{{ $b }}.total_amount'
                                                step="0.01" disabled>
                                            </x-input>
                                            <x-input-error
                                                for="suppliers.{{ $a }}.items.{{ $b }}.total_amount" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <span
                                                class="py-1 px-3 rounded-full text-xs {{ $item['recommended_status'] ? 'text-green bg-green-300' : 'text-red bg-red-300' }}">{{ $item['recommended_status'] ? 'Approved' : 'Not Approved' }}</span>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <span
                                                class="py-1 px-3 rounded-full text-xs {{ $item['final_status'] ? 'text-green bg-green-300' : 'text-red bg-red-300' }}">{{ $item['final_status'] ? 'Approved' : 'Not Approved' }}</span>
                                        </td>
                                        @if (!$canvassing->canvassingSupplier[$a]->requestForPayment)
                                            <td class="p-3 whitespace-nowrap">
                                                <div class="flex space-x-3">
                                                    @if ($item['recommended_status'])
                                                        <svg wire:click="updateItemStatus({{ $a }}, {{ $b }}, 0, 'recommended')"
                                                            class="w-5 h-5 text-red" aria-hidden="true"
                                                            focusable="false" data-prefix="far" data-icon="times-circle"
                                                            role="img" xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 512 512">
                                                            <path fill="currentColor"
                                                                d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z">
                                                            </path>
                                                        </svg>
                                                    @else
                                                        <svg wire:click="updateItemStatus({{ $a }}, {{ $b }}, 1, 'recommended')"
                                                            class="w-5 h-5 text-green" aria-hidden="true"
                                                            focusable="false" data-prefix="fas" data-icon="thumbs-up"
                                                            role="img" xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 512 512">
                                                            <path fill="currentColor"
                                                                d="M104 224H24c-13.255 0-24 10.745-24 24v240c0 13.255 10.745 24 24 24h80c13.255 0 24-10.745 24-24V248c0-13.255-10.745-24-24-24zM64 472c-13.255 0-24-10.745-24-24s10.745-24 24-24 24 10.745 24 24-10.745 24-24 24zM384 81.452c0 42.416-25.97 66.208-33.277 94.548h101.723c33.397 0 59.397 27.746 59.553 58.098.084 17.938-7.546 37.249-19.439 49.197l-.11.11c9.836 23.337 8.237 56.037-9.308 79.469 8.681 25.895-.069 57.704-16.382 74.757 4.298 17.598 2.244 32.575-6.148 44.632C440.202 511.587 389.616 512 346.839 512l-2.845-.001c-48.287-.017-87.806-17.598-119.56-31.725-15.957-7.099-36.821-15.887-52.651-16.178-6.54-.12-11.783-5.457-11.783-11.998v-213.77c0-3.2 1.282-6.271 3.558-8.521 39.614-39.144 56.648-80.587 89.117-113.111 14.804-14.832 20.188-37.236 25.393-58.902C282.515 39.293 291.817 0 312 0c24 0 72 8 72 81.452z">
                                                            </path>
                                                        </svg>
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                <div class="flex space-x-3">
                                                    @if ($item['final_status'])
                                                        <svg wire:click="updateItemStatus({{ $a }}, {{ $b }}, 0, 'final')"
                                                            class="w-5 h-5 text-red" aria-hidden="true"
                                                            focusable="false" data-prefix="far" data-icon="times-circle"
                                                            role="img" xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 512 512">
                                                            <path fill="currentColor"
                                                                d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z">
                                                            </path>
                                                        </svg>
                                                    @else
                                                        <svg wire:click="updateItemStatus({{ $a }}, {{ $b }}, 1, 'final')"
                                                            class="w-5 h-5 text-green" aria-hidden="true"
                                                            focusable="false" data-prefix="fas" data-icon="thumbs-up"
                                                            role="img" xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 512 512">
                                                            <path fill="currentColor"
                                                                d="M104 224H24c-13.255 0-24 10.745-24 24v240c0 13.255 10.745 24 24 24h80c13.255 0 24-10.745 24-24V248c0-13.255-10.745-24-24-24zM64 472c-13.255 0-24-10.745-24-24s10.745-24 24-24 24 10.745 24 24-10.745 24-24 24zM384 81.452c0 42.416-25.97 66.208-33.277 94.548h101.723c33.397 0 59.397 27.746 59.553 58.098.084 17.938-7.546 37.249-19.439 49.197l-.11.11c9.836 23.337 8.237 56.037-9.308 79.469 8.681 25.895-.069 57.704-16.382 74.757 4.298 17.598 2.244 32.575-6.148 44.632C440.202 511.587 389.616 512 346.839 512l-2.845-.001c-48.287-.017-87.806-17.598-119.56-31.725-15.957-7.099-36.821-15.887-52.651-16.178-6.54-.12-11.783-5.457-11.783-11.998v-213.77c0-3.2 1.282-6.271 3.558-8.521 39.614-39.144 56.648-80.587 89.117-113.111 14.804-14.832 20.188-37.236 25.393-58.902C282.515 39.293 291.817 0 312 0c24 0 72 8 72 81.452z">
                                                            </path>
                                                        </svg>
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                <div class="flex space-x-3">
                                                    @if (count($suppliers[$a]['items']) > 1)
                                                        <svg wire:click="action({'a': {{ $a }}, 'b': {{ $b }}}, 'remove_item')"
                                                            class="w-5 h-5 text-blue" aria-hidden="true"
                                                            focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                            role="img" xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 448 512">
                                                            <path fill="currentColor"
                                                                d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                                            </path>
                                                        </svg>
                                                    @endif
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                            @endforeach
                        </x-slot>
                    </x-table.table>
                    <x-input-error for="suppliers.{{ $a }}.items" />
                    <div class="flex justify-end">
                        <span class="text-lg font-semibold">{{ $suppliers[$a]['total_amount'] }}</span>
                    </div>
                    @if (!$canvassing->canvassingSupplier[$a]->requestForPayment)
                        <div class="flex justify-end space-x-3">
                            <button type="button" wire:click="addSupplierItem({{ $a }})"
                                class="flex-none px-3 py-1 text-sm text-white rounded-full bg-green">
                                Add Item</button>
                        </div>
                    @endif
                @endif
            @endforeach
            @if (!$canvassing->canvassingSupplier[$a]->requestForPayment)
                <div class="flex justify-end space-x-3">
                    <button type="button" wire:click="addSupplier"
                        class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                        Add Supplier</button>
                </div>
            @endif
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                @if (!$canvassing->canvassingSupplier[$a]->requestForPayment)
                    <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                        Update</button>
                @endif
            </div>
        </div>
    </form>
</div>
