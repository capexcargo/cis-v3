<x-form wire:init="load" x-data="{ search_form: false, confirmation_modal: '{{ $confirmation_modal }}', view_modal: '{{ $view_modal }}', snapshot_modal: '{{ $snapshot_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                {{ $confirmation_message }}
                <div class="flex justify-end space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click="remove"
                        class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>
        <x-modal id="snapshot_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                <form wire:submit.prevent="submitSnapshot" autocomplete="off">
                    <div class="space-y-3">
                        <div class="grid grid-cols-2 gap-3">
                            <div>
                                <x-label for="current_balance" value="Current Balance" />
                                <x-input type="number" step="0.01" name="current_balance"
                                    wire:model.defer="current_balance" />
                                <x-input-error for="current_balance" class="mt-2" />
                            </div>
                            <div>
                                <x-label for="floating_balance" value="Floating Balance" />
                                <x-input type="number" step="0.01" name="floating_balance"
                                    wire:model.defer="floating_balance" />
                                <x-input-error for="floating_balance" class="mt-2" />
                            </div>
                        </div>
                        <div class="flex justify-end space-x-3">
                            <button type="button" wire:click="$set('snapshot_modal', false)"
                                class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                            <button type="submit"
                                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                                Save</button>
                        </div>
                    </div>
                </form>
            </x-slot>
        </x-modal>
        @if ($parent_reference_no)
            <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                <x-slot name="title">View Request</x-slot>
                <x-slot name="body">
                    @livewire('accounting.request-management.view', ['parent_reference_no' => $parent_reference_no])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">Cash Flow List</x-slot>
    <x-slot name="header_card">
        @forelse ($header_cards as $index => $card)
            <x-card.header wire:click="action({}, 'snapshot')" wire:key="{{ $index }}" :card="$card">
            </x-card.header>
        @empty
            <x-card.header-loading count="2"></x-card.header-loading>
        @endforelse
    </x-slot>
    <x-slot name="search_form">
        <div>
            <x-label for="cv_date_from" value="CV Date From" />
            <x-input type="date" name="cv_date_from" wire:model="cv_date_from" />
            <x-input-error for="cv_date_from" class="mt-2" />
        </div>
        <div>
            <x-label for="cv_date_to" value="CV Date To" />
            <x-input type="date" name="cv_date_to" wire:model="cv_date_to" />
            <x-input-error for="cv_date_to" class="mt-2" />
        </div>
        <div>
            <x-label for="encashment_date_from" value="Encashment Date From" />
            <x-input type="date" name="encashment_date_from" wire:model="encashment_date_from" />
            <x-input-error for="encashment_date_from" class="mt-2" />
        </div>
        <div>
            <x-label for="encashment_date_to" value="Encashment Date To" />
            <x-input type="date" name="encashment_date_to" wire:model="encashment_date_to" />
            <x-input-error for="encashment_date_to" class="mt-2" />
        </div>
        <div>
            <x-label for="check_no" value="Check No" />
            <x-input type="number" name="check_no" wire:model.debounce.500ms="check_no" />
            <x-input-error for="check_no" class="mt-2" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Encashment Date" />
                        <x-table.th name="Check No" />
                        <x-table.th name="CV Date" />
                        <x-table.th name="Payee" />
                        <x-table.th name="Check Description" />
                        <x-table.th name="Encashment Amount" />
                        <x-table.th name="Locked Amount" />
                        <x-table.th name="Status" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($cash_flow_details as $cash_flow_detail)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $cash_flow_detail->encashment_date ? date('M. d, Y', strtotime($cash_flow_detail->encashment_date)) : 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($cash_flow_detail->checkVoucherDetails->checkVoucher->requestForPayment->parent_reference_no)
                                        <p wire:click="action({'parent_reference_no': '{{ $cash_flow_detail->checkVoucherDetails->checkVoucher->requestForPayment->parent_reference_no }}'}, 'view')"
                                            class="underline underline-offset-4 text-blue">
                                            {{ $cash_flow_detail->check_no }}</p>
                                    @else
                                        <a href="https://cis2.capex.com.ph/index.php?r=acRfPayment/ViewApprovalDetails&AcRfPayment%5Brfpa_ref_id%5D={{ $cash_flow_detail->checkVoucherDetails->checkVoucher->requestForPayment->reference_id }}"
                                            target="_blank"
                                            class="underline underline-offset-4 text-blue">{{ $cash_flow_detail->check_no }}</a>
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('M. d, Y', strtotime($cash_flow_detail->cv_date)) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $cash_flow_detail->payee->company }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $cash_flow_detail->check_description }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($cash_flow_detail->encashment_amount, 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($cash_flow_detail->locked_amount, 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($cash_flow_detail->status_id == 1)
                                        <p wire:click="action({'cash_flow_details_id': '{{ $cash_flow_detail->id }}', 'status_id': 2}, 'status')"
                                            class="underline underline-offset-4 text-blue">
                                            {{ $cash_flow_detail->status->display }}</p>
                                    @elseif($cash_flow_detail->status_id == 2)
                                        <p wire:click="action({'cash_flow_details_id': '{{ $cash_flow_detail->id }}', 'status_id': 1}, 'status')"
                                            class="underline underline-offset-4 text-blue">
                                            {{ $cash_flow_detail->status->display }}</p>
                                    @elseif($cash_flow_detail->status_id == 3)
                                        {{ $cash_flow_detail->status->display }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $cash_flow_details->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
