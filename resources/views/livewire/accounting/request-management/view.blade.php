<div>
    <x-loading />
    @foreach ($requests as $i => $request)
        <div class="space-y-3">
            <div class="grid grid-cols-4 gap-3">
                <div>
                    <x-label for="parent_reference_number" value="Parent Reference Number" :required="true" />
                    <x-input wire:key="parent_reference_number{{ $i }}" type="text"
                        name="parent_reference_number" value="{{ $request->parent_reference_no }}" disabled />
                    <x-input-error for="parent_reference_number" />
                </div>
                <div>
                    <x-label for="reference_number" value="Reference Number" :required="true" />
                    <x-input wire:key="reference_number{{ $i }}" type="text" name="reference_number"
                        value="{{ $request->reference_id }}" disabled />
                    <x-input-error for="reference_number" />
                </div>
            </div>
            <div class="grid grid-cols-4 gap-3">
                <div>
                    <x-label for="division" value="Requesting Division" :required="true" />
                    <x-input wire:key="division{{ $i }}" type="text" name="division"
                        value="{{ $request->budget->division->name }}" disabled />
                    <x-input-error for="division" />
                </div>
                <div>
                    <x-label for="budget_source" value="Budget Source" :required="true" />
                    <x-input wire:key="budget_source{{ $i }}" type="text" name="budget_source"
                        value="{{ $request->budget->source->name }}" disabled />
                    <x-input-error for="budget_source" />
                </div>
                <div>
                    <x-label for="chart_of_accounts" value="Chart Of Accounts" :required="true" />
                    <x-input wire:key="chart_of_accounts{{ $i }}" type="text" name="chart_of_accounts"
                        value="{{ $request->budget->chart->name }}" disabled />
                    <x-input-error for="chart_of_accounts" />
                </div>
                <div>
                    <x-label for="coa_category" value="Coa Category" :required="true" />
                    <x-input wire:key="budget_source{{ $i }}" type="text" name="budget_source"
                        value="{{ $request->budget->item }}" disabled />
                    <x-input-error for="coa_category" />
                </div>
            </div>
            <div class="grid grid-cols-4 gap-3">
                <div>
                    <x-label for="priority" value="Priority" :required="true" />
                    <x-input wire:key="priority{{ $i }}" type="text" name="priority"
                        value="{{ $request->priority->display }}" disabled />
                    <x-input-error for="priority" />
                </div>
                <div>
                    <x-label for="date_needed" value="Date Needed" :required="true" />
                    <x-input wire:key="date_needed{{ $i }}" type="date" name="date_needed"
                        value="{{ $request->date_needed }}" disabled></x-input>
                    <x-input-error for="date_needed" />
                </div>
                <div>
                    <x-label for="payee" value="Payee" :required="true" />
                    <x-input wire:key="payee{{ $i }}" type="text" name="payee"
                        value="{{ $request->payee->company }}" disabled />
                    <x-input-error for="payee" />
                </div>
                <div>
                    <x-label for="branch" value="Branch" :required="true" />
                    <x-input wire:key="branch{{ $i }}" type="text" name="branch"
                        value="{{ $request->branch->display }}" disabled />
                    <x-input-error for="branch" />
                </div>
                @if ($request->account_type_id != 1 && $request->requestForPaymentDetails[0]->payment_type_id != 2)
                    <div>
                        <x-label for="date_of_transaction_from" value="Date Of Transaction From" :required="true" />
                        <x-input wire:key="date_of_transaction_from{{ $i }}" type="date"
                            name="date_of_transaction_from" value="{{ $request->date_of_transaction_from }}"
                            disabled />
                        <x-input-error for="date_of_transaction_from" />
                    </div>
                    <div>
                        <x-label for="date_of_transaction_to" value="Date Of Transaction To" :required="true" />
                        <x-input wire:key="date_of_transaction_to{{ $i }}" type="date"
                            name="date_of_transaction_to" value="{{ $request->date_of_transaction_to }}" disabled>
                        </x-input>
                        <x-input-error for="date_of_transaction_to" />
                    </div>
                @endif
                <div>
                    <x-label for="canvasser" value="Canvasser" />
                    <x-input wire:key="canvasser{{ $i }}" type="text" name="canvasser"
                        value="{{ $request->canvasser }}" disabled></x-input>
                    <x-input-error for="canvasser" />
                </div>
                <div>
                    <x-label for="opex_type" value="Opex Type" :required="true" />
                    <x-input wire:key="opex_type{{ $i }}" type="text" name="opex_type"
                        value="{{ $request->opex->name }}" disabled></x-input>
                    <x-input-error for="opex_type" />
                </div>
                <div>
                    <x-label for="description" value="Description" :required="true" />
                    <x-textarea wire:key="description{{ $i }}" type="text" name="description" disabled>
                        {{ $request->description }}
                    </x-textarea>
                    <x-input-error for="description" />
                </div>
                <div>
                    <x-label for="remarks" value="Remarks" />
                    <x-textarea wire:key="remarks{{ $i }}" type="text" name="remarks" disabled>
                        {{ $request->remarks }}
                    </x-textarea>
                    <x-input-error for="remarks" />
                </div>
            </div>
            @if ($request->type_id == 1)
                <div>
                    <div class="text-2xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>Request For Payment</span>
                        </div>
                    </div>
                    <div class="grid grid-cols-4 gap-3">
                        <div>
                            <x-label for="po_reference_no" value="PO Reference No." :required="true" />
                            <x-input wire:key="po_reference_no{{ $i }}" type="text" name="po_reference_no"
                                value="{{ $request->canvassingSupplier->po_reference_no }}" disabled></x-input>
                            <x-input-error for="po_reference_no" />
                        </div>
                    </div>
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="Particular / Description" />
                            <x-table.th name="Plate Number" />
                            <x-table.th name="Labor Cost" />
                            <x-table.th name="Unit Cost" />
                            <x-table.th name="Quantity" />
                            <x-table.th name="Amount" />
                        </x-slot>
                        <x-slot name="tbody">
                            @forelse ($request->requestForPaymentDetails as $request_for_payment_details)
                                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="particulars{{ $i }}" type="text"
                                            name="particulars"
                                            value="{{ $request_for_payment_details->particulars }}" disabled />
                                        <x-input-error for="particulars" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="plate_no{{ $i }}" type="text" name="plate_no"
                                            value="{{ $request_for_payment_details->plate_no }}" disabled />
                                        <x-input-error for="plate_no" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="labor_cost{{ $i }}" type="number" step="0.01"
                                            name="labor_cost" value="{{ $request_for_payment_details->labor_cost }}"
                                            disabled />
                                        <x-input-error for="labor_cost" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ number_format($request_for_payment_details->unit_cost, 2) }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $request_for_payment_details->quantity }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ number_format($request_for_payment_details->amount, 2) }}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">
                                        <p class="text-center">Empty.</p>
                                    </td>
                                </tr>
                            @endforelse
                        </x-slot>
                    </x-table.table>
                    <x-input-error for="request_for_payments" />
                </div>
            @elseif($request->type_id == 2)
                <div>
                    <div class="text-2xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>Freights</span>
                        </div>
                    </div>
                    <div class="grid grid-cols-4 gap-3">
                        <div>
                            <x-label for="accounting_account_type" value="Account Type" :required="true" />
                            <x-input wire:key="accounting_account_type{{ $i }}" type="text"
                                name="accounting_account_type" value="{{ $request->accountType->display }}"
                                disabled />
                            <x-input-error for="accounting_account_type" />
                        </div>
                        <div>
                            <x-label for="loader_search" value="Loaders" :required="true" />
                            @if ($request->requestForPaymentDetails[0]->loaders)
                                <x-input type="text" list="loaders_suggestions" name="loader_search"
                                    value="{{ $request->requestForPaymentDetails[0]->loaders->name }}" disabled>
                                </x-input>
                            @else
                                <x-input type="text" list="loaders_suggestions" name="loader_search" value="" disabled>
                                </x-input>
                            @endif
                            <x-input-error for="loader_search" />
                            <x-input-error for="loader" />
                        </div>
                    </div>
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="Freight Reference No" />
                            <x-table.th name="Freight Reference Type" />
                            <x-table.th name="SOA No." />
                            <x-table.th name="Trucking Type" />
                            <x-table.th name="Trucking Amount" />
                            <x-table.th name="Freight Amount" />
                            <x-table.th name="Freight Usage" />
                            <x-table.th name="Transaction Date" />
                            <x-table.th name="Allowance" />
                            <x-table.th name="Amount" />
                            <x-table.th name="View PA" />
                        </x-slot>
                        <x-slot name="tbody">
                            @forelse ($request->requestForPaymentDetails as $request_for_payment_details)
                                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="freight_reference_no{{ $i }}" type="text"
                                            name="freight_reference_no"
                                            value="{{ $request_for_payment_details->freight_reference_no }}"
                                            disabled />
                                        <x-input-error for="freight_reference_no" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="freight_reference_type{{ $i }}" type="text"
                                            name="freight_reference_type"
                                            value="{{ $request_for_payment_details->freightReferenceType->display }}"
                                            disabled />
                                        <x-input-error for="freight_reference_type" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="soa_no{{ $i }}" type="text" name="soa_no"
                                            value="{{ $request_for_payment_details->soa_no }}" disabled />
                                        <x-input-error for="soa_no" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="trucking_type{{ $i }}" type="text"
                                            name="trucking_type"
                                            value="{{ $request_for_payment_details->truckingType->display }}"
                                            disabled />
                                        <x-input-error for="trucking_type" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="trucking_amount{{ $i }}" type="number"
                                            step="0.01" name="trucking_amount"
                                            value="{{ $request_for_payment_details->trucking_amount }}" disabled />
                                        <x-input-error for="trucking_amount" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="freight_amount{{ $i }}" type="number" step="0.01"
                                            name="freight_amount"
                                            value="{{ $request_for_payment_details->freight_amount }}" disabled />
                                        <x-input-error for="freight_amount" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="freight_usage{{ $i }}" type="text"
                                            name="freight_usage"
                                            value="{{ $request_for_payment_details->freightUsage->display }}"
                                            disabled />
                                        <x-input-error for="freight_usage" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="transaction_date{{ $i }}" type="date"
                                            name="transaction_date"
                                            value="{{ $request_for_payment_details->transaction_date }}" disabled />
                                        <x-input-error for="transaction_date" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="allowance{{ $i }}" type="number" step="0.01"
                                            name="allowance" value="{{ $request_for_payment_details->allowance }}"
                                            disabled />
                                        <x-input-error for="allowance" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="amount{{ $i }}" type="number" step="0.01"
                                            name="amount" value="{{ $request_for_payment_details->amount }}"
                                            disabled />
                                        <x-input-error for="amount" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-center">
                                            @if ($request_for_payment_details->freightReferenceType->id == 3)
                                                @if ($request->requestForPaymentDetails[0]->loaders)
                                                    <a href="https://cis2.capex.com.ph/index.php?r=acRfPayment/getwingvanPAforprint&data=1&tranID={{ $request_for_payment_details->freight_reference_no }}&wingv={{ $request->requestForPaymentDetails[0]->loaders->code }}"
                                                        target="_blank">
                                                        <svg class="w-5 h-5 text-blue"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                                            <path fill="currentColor"
                                                                d="M64 48C64 21.49 85.49 0 112 0H368C394.5 0 416 21.49 416 48V256H608V352C625.7 352 640 366.3 640 384C640 401.7 625.7 416 608 416H576C576 469 533 512 480 512C426.1 512 384 469 384 416H256C256 469 213 512 160 512C106.1 512 64 469 64 416V288H208C216.8 288 224 280.8 224 272C224 263.2 216.8 256 208 256H16C7.164 256 0 248.8 0 240C0 231.2 7.164 224 16 224H240C248.8 224 256 216.8 256 208C256 199.2 248.8 192 240 192H48C39.16 192 32 184.8 32 176C32 167.2 39.16 160 48 160H272C280.8 160 288 152.8 288 144C288 135.2 280.8 128 272 128H16C7.164 128 0 120.8 0 112C0 103.2 7.164 96 16 96H64V48zM160 464C186.5 464 208 442.5 208 416C208 389.5 186.5 368 160 368C133.5 368 112 389.5 112 416C112 442.5 133.5 464 160 464zM480 368C453.5 368 432 389.5 432 416C432 442.5 453.5 464 480 464C506.5 464 528 442.5 528 416C528 389.5 506.5 368 480 368zM466.7 160H400V96H466.7C483.7 96 499.1 102.7 512 114.7L589.3 192C601.3 204 608 220.3 608 237.3V288H544V237.3L466.7 160z" />
                                                        </svg>
                                                    </a>
                                                @endif
                                            @endif
                                            @if ($request_for_payment_details->freightReferenceType->id == 4)
                                                @if ($request_for_payment_details->soa_no)
                                                    <a href="https://cis2.capex.com.ph/index.php?r=acRfPayment/getairPAforprint&data=1&tranID={{ $request_for_payment_details->soa_no }}"
                                                        target="_blank">
                                                        <svg class="w-5 h-5 text-blue"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                            <path fill="currentColor"
                                                                d="M482.3 192C516.5 192 576 221 576 256C576 292 516.5 320 482.3 320H365.7L265.2 495.9C259.5 505.8 248.9 512 237.4 512H181.2C170.6 512 162.9 501.8 165.8 491.6L214.9 320H112L68.8 377.6C65.78 381.6 61.04 384 56 384H14.03C6.284 384 0 377.7 0 369.1C0 368.7 .1818 367.4 .5398 366.1L32 256L.5398 145.9C.1818 144.6 0 143.3 0 142C0 134.3 6.284 128 14.03 128H56C61.04 128 65.78 130.4 68.8 134.4L112 192H214.9L165.8 20.4C162.9 10.17 170.6 0 181.2 0H237.4C248.9 0 259.5 6.153 265.2 16.12L365.7 192H482.3z" />
                                                        </svg>
                                                    </a>
                                                @endif
                                            @endif
                                            @if ($request_for_payment_details->freightReferenceType->id == 2)
                                                @if ($request_for_payment_details->soa_no)
                                                    <a href="https://cis2.capex.com.ph/capexsystem/includes/AirwayBillPrint.php?awbNo={{ $request_for_payment_details->freight_reference_no }}"
                                                        target="_blank">
                                                        <svg class="w-5 h-5 text-blue"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                                            <path fill="currentColor"
                                                                d="M256 0v128h128L256 0zM224 128L224 0H48C21.49 0 0 21.49 0 48v416C0 490.5 21.49 512 48 512h288c26.51 0 48-21.49 48-48V160h-127.1C238.3 160 224 145.7 224 128zM272 416h-160C103.2 416 96 408.8 96 400C96 391.2 103.2 384 112 384h160c8.836 0 16 7.162 16 16C288 408.8 280.8 416 272 416zM272 352h-160C103.2 352 96 344.8 96 336C96 327.2 103.2 320 112 320h160c8.836 0 16 7.162 16 16C288 344.8 280.8 352 272 352zM288 272C288 280.8 280.8 288 272 288h-160C103.2 288 96 280.8 96 272C96 263.2 103.2 256 112 256h160C280.8 256 288 263.2 288 272z" />
                                                        </svg>
                                                    </a>
                                                @endif
                                            @endif
                                            @if ($request_for_payment_details->freightReferenceType->id == 1)
                                                <a href="https://cis2.capex.com.ph/index.php?r=acRfPayment/getseaPAforprint&data=1&tranID={{ $request_for_payment_details->freight_reference_no }}"
                                                    target="_blank">
                                                    <svg class="w-5 h-5 text-blue" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M192 32C192 14.33 206.3 0 224 0H352C369.7 0 384 14.33 384 32V64H432C458.5 64 480 85.49 480 112V240L524.4 254.8C547.6 262.5 553.9 292.3 535.9 308.7L434.9 401.4C418.7 410.7 400.2 416.5 384 416.5C364.4 416.5 343.2 408.8 324.8 396.1C302.8 380.6 273.3 380.6 251.2 396.1C234 407.9 213.2 416.5 192 416.5C175.8 416.5 157.3 410.7 141.1 401.3L40.09 308.7C22.1 292.3 28.45 262.5 51.59 254.8L96 239.1V111.1C96 85.49 117.5 63.1 144 63.1H192V32zM160 218.7L267.8 182.7C280.9 178.4 295.1 178.4 308.2 182.7L416 218.7V128H160V218.7zM384 448C410.9 448 439.4 437.2 461.4 421.9L461.5 421.9C473.4 413.4 489.5 414.1 500.7 423.6C515 435.5 533.2 444.6 551.3 448.8C568.5 452.8 579.2 470.1 575.2 487.3C571.2 504.5 553.1 515.2 536.7 511.2C512.2 505.4 491.9 494.6 478.5 486.2C449.5 501.7 417 512 384 512C352.1 512 323.4 502.1 303.6 493.1C297.7 490.5 292.5 487.8 288 485.4C283.5 487.8 278.3 490.5 272.4 493.1C252.6 502.1 223.9 512 192 512C158.1 512 126.5 501.7 97.5 486.2C84.12 494.6 63.79 505.4 39.27 511.2C22.06 515.2 4.853 504.5 .8422 487.3C-3.169 470.1 7.532 452.8 24.74 448.8C42.84 444.6 60.96 435.5 75.31 423.6C86.46 414.1 102.6 413.4 114.5 421.9L114.6 421.9C136.7 437.2 165.1 448 192 448C219.5 448 247 437.4 269.5 421.9C280.6 414 295.4 414 306.5 421.9C328.1 437.4 356.5 448 384 448H384z" />
                                                    </svg>
                                                </a>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="11">
                                        <p class="text-center">Empty.</p>
                                    </td>
                                </tr>
                            @endforelse
                        </x-slot>
                    </x-table.table>
                </div>
            @elseif($request->type_id == 3)
                <div class="space-y-3">
                    <div class="text-2xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>Payables</span>
                        </div>
                    </div>
                    <div class="grid grid-cols-5 gap-3">
                        <div>
                            <x-label for="account_no" value="Account No." :required="true" />
                            <x-input wire:key="account_no{{ $i }}" type="text" name="account_no"
                                value="{{ $request->requestForPaymentDetails[0]->account_no }}" disabled></x-input>
                            <x-input-error for="account_no" />
                        </div>
                        <div>
                            <x-label for="payment_type" value="Payment Type" :required="true" />
                            <x-input wire:key="payment_type{{ $i }}" type="text" name="payment_type"
                                value="{{ $request->requestForPaymentDetails[0]->paymentType->display }}" disabled>
                            </x-input>
                            <x-input-error for="payment_type" />
                        </div>
                        <div>
                            <x-label for="terms" value="Type of Terms" :required="true" />
                            <x-input wire:key="terms{{ $i }}" type="text" name="terms"
                                value="{{ $request->requestForPaymentDetails[0]->terms->display }}" disabled>
                            </x-input>
                            <x-input-error for="terms" />
                        </div>
                        <div>
                            <x-label for="pdc_from" value="Duration From" :required="true" />
                            <x-input wire:key="pdc_from{{ $i }}" type="date" name="pdc_from"
                                value="{{ $request->requestForPaymentDetails[0]->pdc_from }}" disabled></x-input>
                            <x-input-error for="pdc_from" />
                        </div>
                        <div>
                            <x-label for="pdc_to" value="Duration To" :required="true" />
                            <x-input wire:key="pdc_to{{ $i }}" type="date" name="pdc_to"
                                value="{{ $request->requestForPaymentDetails[0]->pdc_to }}" disabled></x-input>
                            <x-input-error for="pdc_to" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-table.table>
                                <x-slot name="thead">
                                    <x-table.th name="Invoice Number" />
                                    <x-table.th name="Invoice Amount" />
                                    @if ($request->requestForPaymentDetails[0]->payment_type_id == 2)
                                        <x-table.th name="Date Of Transaction" />
                                    @endif
                                </x-slot>
                                <x-slot name="tbody">
                                    @forelse ($request->requestForPaymentDetails as  $request_for_payment_details)
                                        <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                            <td class="p-3 whitespace-nowrap">
                                                <x-input wire:key="invoice_no{{ $i }}" type="text"
                                                    name="invoice_no"
                                                    value="{{ $request_for_payment_details->invoice }}" disabled />
                                                <x-input-error for="invoice_no" />
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                <x-input wire:key="invoice_amount{{ $i }}" type="number"
                                                    step="0.01" name="invoice_amount"
                                                    value="{{ $request_for_payment_details->invoice_amount }}"
                                                    disabled />
                                                <x-input-error for="invoice_amount" />
                                            </td>
                                            @if ($request_for_payment_details->payment_type_id == 2)
                                                <td class="p-3 whitespace-nowrap">
                                                    <x-input wire:key="date_of_transaction{{ $i }}"
                                                        type="date" name="date_of_transaction"
                                                        value="{{ $request_for_payment_details->transaction_date }}"
                                                        disabled />
                                                    <x-input-error for="date_of_transaction" />
                                                </td>
                                            @endif
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3">
                                                <p class="text-center">Empty.</p>
                                            </td>
                                        </tr>
                                    @endforelse
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                </div>
            @elseif ($request->type_id == 4)
                <div>
                    <div class="text-2xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>Cash Advance</span>
                        </div>
                    </div>
                    <div class="grid grid-cols-5 gap-3">
                        <div>
                            <x-label for="ca_no" value="CA No." />
                            <x-input wire:key="ca_no{{ $i }}" type="text" name="ca_no"
                                value="{{ $request->ca_no }}" disabled></x-input>
                            <x-input-error for="ca_no" />
                        </div>
                        <div>
                            <x-label for="ca_reference_type" value="CA Reference Type" />
                            <x-input wire:key="ca_reference_type{{ $i }}" type="text"
                                name="ca_reference_type"
                                value="{{ $request->CAReferenceType ? $request->CAReferenceType->display : '' }}"
                                disabled></x-input>
                            <x-input-error for="ca_reference_type" />
                        </div>
                    </div>
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="Particular / Description" />
                            <x-table.th name="{{ $ca_reference_type_display ?? 'CA Reference No' }}" />
                            <x-table.th name="Unit Cost" />
                            <x-table.th name="Quantity" />
                            <x-table.th name="Amount" />
                        </x-slot>
                        <x-slot name="tbody">
                            @forelse ($request->requestForPaymentDetails as  $request_for_payment_details)
                                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="particulars{{ $i }}" type="text"
                                            name="particulars"
                                            value="{{ $request_for_payment_details->particulars }}" disabled />
                                        <x-input-error for="particulars" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="ca_reference_no{{ $i }}" type="text"
                                            name="ca_reference_no"
                                            value="{{ $request_for_payment_details->ca_reference_no }}" disabled />
                                        <x-input-error for="ca_reference_no" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="unit_cost{{ $i }}" type="number" step="0.01"
                                            name="unit_cost" value="{{ $request_for_payment_details->unit_cost }}"
                                            disabled />
                                        <x-input-error for="unit_cost" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input wire:key="quantity{{ $i }}" type="number" name="quantity"
                                            value="{{ $request_for_payment_details->quantity }}" disabled />
                                        <x-input-error for="quantity" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ number_format($request_for_payment_details->amount, 2) }}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">
                                        <p class="text-center">Empty.</p>
                                    </td>
                                </tr>
                            @endforelse
                        </x-slot>
                    </x-table.table>
                    <x-input-error for="cash_advances" />
                </div>
            @endif
            <div class="text-2xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Approvers</span>
                </div>
            </div>
            <div class="grid grid-cols-4 gap-3">
                <div class="space-y-3">
                    <div>
                        <div class="flex items-center justify-between">
                            <x-label for="approver_1_id" value="Approver 1" :required="true" />
                            <div class="flex space-x-3">
                                <input wire:key="is_set_approver_1{{ $i }}" type="checkbox"
                                    class="form-checkbox" name="is_set_approver_1" value="1"
                                    wire:model="is_set_approver_1" @if ($request->is_set_approver_1) checked @endif
                                    disabled />
                                <x-label value="Set Approver 1" />
                            </div>
                        </div>
                        <x-input wire:key="approver_1_id{{ $i }}" type="text" name="approver_1_id"
                            value="{{ $request->approver1 ? $request->approver1->name : '' }}" disabled></x-input>
                        <x-input-error for="approver_1_id" />
                    </div>
                    @if ($request->approver_1_id)
                        <div class="flex justify-between">
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_1{{ $request->id }}"
                                    {{ $request->is_approved_1 ? 'checked' : '' }} disabled>
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_1{{ $request->id }}">Approve</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_1{{ $request->id }}"
                                    {{ $request->is_approved_1 === 0 ? 'checked' : '' }} disabled>
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_1{{ $request->id }}">Disapprove</label>
                            </div>
                        </div>
                        <div>
                            <x-label for="approver_remarks_1" value="Remarks" />
                            <x-textarea type="text" name="approver_remarks_1" disabled>
                                {{ $request->approver_remarks_1 }}</x-textarea>
                            <x-input-error for="approver_remarks_1" />
                        </div>
                    @endif
                </div>
                <div class="space-y-3">
                    <div>
                        <x-label for="approver_2_id" value="Approver 2" />
                        <x-input wire:key="approver_2_id{{ $i }}" type="text" name="approver_2_id"
                            value="{{ $request->approver2 ? $request->approver2->name : '' }}" disabled></x-input>
                        <x-input-error for="approver_2_id" />
                    </div>
                    @if ($request->approver_2_id)
                        <div class="flex justify-between">
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_2{{ $request->id }}"
                                    {{ $request->is_approved_2 ? 'checked' : '' }} disabled>
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_2{{ $request->id }}">Approve</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_2{{ $request->id }}"
                                    {{ $request->is_approved_2 === 0 ? 'checked' : '' }} disabled>
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_2{{ $request->id }}">Disapprove</label>
                            </div>
                        </div>
                        <div>
                            <x-label for="approver_remarks_2" value="Remarks" />
                            <x-textarea type="text" name="approver_remarks_2" disabled>
                                {{ $request->approver_remarks_2 }}</x-textarea>
                            <x-input-error for="approver_remarks_2" />
                        </div>
                    @endif
                </div>
                <div class="space-y-3">
                    <div>
                        <x-label for="approver_3_id" value="Approver 3" />
                        <x-input wire:key="approver_3_id{{ $i }}" type="text" name="approver_3_id"
                            value="{{ $request->approver3 ? $request->approver3->name : '' }}" disabled></x-input>
                        <x-input-error for="approver_3_id" />
                    </div>
                    @if ($request->approver_3_id)
                        <div class="flex justify-between">
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_3{{ $request->id }}"
                                    {{ $request->is_approved_3 ? 'checked' : '' }} disabled>
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_3{{ $request->id }}">Approve</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input
                                    class="float-left w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-full appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                                    type="radio" name="is_approved_3{{ $request->id }}"
                                    {{ $request->is_approved_3 === 0 ? 'checked' : '' }} disabled>
                                <label class="inline-block text-gray-800 form-check-label"
                                    for="is_approved_3{{ $request->id }}">Disapprove</label>
                            </div>
                        </div>
                        <div>
                            <x-label for="approver_remarks_3" value="Remarks" />
                            <x-textarea type="text" name="approver_remarks_3" disabled>
                                {{ $request->approver_remarks_3 }}</x-textarea>
                            <x-input-error for="approver_remarks_3" />
                        </div>
                    @endif
                </div>
            </div>
            <div class="text-2xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Attachments</span>
                </div>
            </div>
            <div class="grid grid-cols-4 gap-3">
                <div class="flex flex-col space-y-3">
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="File" />
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($request->attachments as $i => $attachment)
                                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                    <td class="flex items-center justify-center p-2 whitespace-nowrap">
                                        @if (in_array($attachment->extension, config('filesystems.image_type')))
                                            <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                <a href="{{ Storage::disk('accounting_gcs')->url($attachment->path . $attachment->name) }}"
                                                    target="_blank"><img
                                                        class="w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                        src="{{ Storage::disk('accounting_gcs')->url($attachment->path . $attachment->name) }}"></a>
                                            </div>
                                        @elseif(in_array($attachment->extension, config('filesystems.file_type')))
                                            <svg wire:click="download({{ $request->id }}, {{ $attachment->id }})"
                                                class="w-20 h-20 mx-auto border border-gray-500 rounded-lg"
                                                aria-hidden="true" focusable="false" data-prefix="fas"
                                                data-icon="file-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 384 512">
                                                <path fill="currentColor"
                                                    d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z">
                                                </path>
                                            </svg>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </x-slot>
                    </x-table.table>
                </div>
            </div>
            <div class="flex items-center justify-end space-x-3 text-lg font-extrabold">
                <span>Requested By: {{$request->user->name}}</span> <br><br>
            </div>
            <div class="flex items-center justify-end space-x-3 text-3xl font-extrabold">
                <span class="">Total:</span>
                <span class="text-blue">{{ number_format($request->amount, 2) }}</span>
            </div>
        </div>
    @endforeach
    <div class="flex items-center justify-end space-x-3 text-4xl font-extrabold">
        <span class="">All Request Total:</span>
        <span class="text-blue">{{ number_format($requests->sum('amount'), 2) }}</span>
    </div>
    <div class="flex justify-end space-x-3">
        <button type="button" wire:click="$emit('close_modal', 'view')"
            class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
    </div>
</div>
