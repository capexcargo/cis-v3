<x-form wire:init="load" x-data="{ search_form: false, confirmation_modal: '{{ $confirmation_modal }}', create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}', view_modal: '{{ $view_modal }}', import_modal: '{{ $import_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                {{ $confirmation_message }}
                <div class="flex justify-end space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click="remove"
                        class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>
        <x-modal id="import_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="title">Import Request For Payment</x-slot>
            <x-slot name="body">
                <div class="space-y-3">
                    <div>
                        <x-label for="import" value="Import" />
                        <x-input type="file" name="import" wire:model='import'></x-input>
                        <x-input-error for="import" />
                    </div>
                    <div class="flex items-center justify-end">
                        <button type="button" wire:click="import"
                            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Import</button>

                    </div>
                </div>
            </x-slot>
        </x-modal>
        @can('accounting_request_management_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                <x-slot name="title">Create Request</x-slot>
                <x-slot name="body">
                    @livewire('accounting.request-management.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('accounting_request_management_edit')
            @if ($request_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                    <x-slot name="title">Edit Request</x-slot>
                    <x-slot name="body">
                        @livewire('accounting.request-management.edit', ['id' => $request_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @if ($parent_reference_no)
            <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                <x-slot name="title">View Request</x-slot>
                <x-slot name="body">
                    @livewire('accounting.request-management.view', ['parent_reference_no' => $parent_reference_no])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>

    <x-slot name="header_title">RFP Monitoring
    </x-slot>
    <x-slot name="header_button">
        <a target="_blank"
            href="{{ route('accounting.request-management.print-request-for-payment', ['selected' => json_encode($selected)]) }}"
            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Print Request For Payment</a>
        <button type="button" wire:click="export"
            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Export to Excel</button>
    </x-slot>
    <x-slot name="search_form">
        {{-- @if (auth()->user()->level_id >= 5) --}}
        <div>
            <x-label for="division" value="Budget Division" />
            <x-select name="division" wire:model='division'>
                <option value=""></option>
                @foreach ($division_references as $division_reference)
                    <option value="{{ $division_reference->id }}">
                        {{ $division_reference->name }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="division" class="mt-2" />
        </div>
        {{-- @endif --}}
        <div>
            <x-label for="reference_no" value="Reference No" />
            <x-input type="text" name="reference_no" wire:model.debounce.500ms="reference_no" />
            <x-input-error for="reference_no" class="mt-2" />
        </div>
        <div>
            <x-label for="request_type" value="Request Type" />
            <x-select name="request_type" wire:model='request_type'>
                <option value=""></option>
                @foreach ($request_for_payment_type_references as $request_for_payment_type_reference)
                    <option value="{{ $request_for_payment_type_reference->id }}">
                        {{ $request_for_payment_type_reference->display }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="request_type" class="mt-2" />
        </div>
        <div>
            <x-label for="payee" value="Payee" />
            <x-input type="text" name="payee" wire:model.debounce.500ms="payee" />
            <x-input-error for="payee" class="mt-2" />
        </div>
        <div>
            <x-label for="opex_type" value="Opex Type" />
            <x-select name="opex_type" wire:model.debounce.500ms='opex_type'>
                <option value=""></option>
                @foreach ($opex_type_references as $opex_type_reference)
                    <option value="{{ $opex_type_reference->id }}">
                        {{ $opex_type_reference->name }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="opex_type" class="mt-2" />
        </div>
        <div>
            <x-label for="requester" value="Requester" />
            <x-input type="text" name="requester" wire:model.debounce.500ms="requester" />
            <x-input-error for="requester" class="mt-2" />
        </div>
        <div>
            <x-label for="description" value="Description" />
            <x-input type="text" name="description" wire:model.debounce.500ms="description" />
            <x-input-error for="description" class="mt-2" />
        </div>
        <div>
            <x-label for="date_created" value="Date Created From" />
            <x-input type="date" name="date_created" wire:model.debounce.500ms="date_created" />
            <x-input-error for="date_created" class="mt-2" />
        </div>
        <div>
            <x-label for="date_created_to" value="Date Created To" />
            <x-input type="date" name="date_created_to" wire:model.debounce.500ms="date_created_to" />
            <x-input-error for="date_created_to" class="mt-2" />
        </div>
        <div>
            <x-label for="status" value="Status" />
            <x-select name="status" wire:model='status'>
                <option value=""></option>
                @foreach ($status_references as $status_reference)
                    <option value="{{ $status_reference->id }}">
                        {{ $status_reference->display }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="status" class="mt-2" />
        </div>
        @can('accounting_request_management_delete')
            <div>
                <x-label for="trashed" value="Trashed" />
                <x-select name="trashed" wire:model='trashed'>
                    <option value=""></option>
                    <option value="with_trashed">With Trashed</option>
                    <option value="only_trashed">Only Trashed</option>
                </x-select>
                <x-input-error for="trashed" class="mt-2" />
            </div>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Select" />
                        <x-table.th name="Reference No." wire:click="sortBy('employee_number')">
                            <x-slot name="sort">
                                <x-table.sort-icon field="employee_number" sortField="{{ $sortField }}"
                                    sortAsc="{{ $sortAsc }}">
                                </x-table.sort-icon>
                            </x-slot>
                        </x-table.th>
                        <x-table.th name="Request Type" />
                        <x-table.th name="Payee" />
                        <x-table.th name="Priority" />
                        <x-table.th name="Description" />
                        <x-table.th name="Branch" />
                        <x-table.th name="Date Needed" />
                        <x-table.th name="Opex Type" />
                        <x-table.th name="Amount" />
                        <x-table.th name="Requested By" />
                        <x-table.th name="Approver 1 Status" />
                        <x-table.th name="Approver 2 Status" />
                        <x-table.th name="Approver 3 Status" />
                        <x-table.th name="Approver 1 Date" />
                        <x-table.th name="Approver 2 Date" />
                        <x-table.th name="Approver 3 Date" />
                        <x-table.th name="Status" />
                        <x-table.th name="Created At" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($request_for_payments as $request_for_payment)
                            <tr
                                class="border-0 cursor-pointer @if ($request_for_payment->trashed()) bg-red-100 @else hover:text-white hover:bg-[#4068b8] @endif">
                                <td class="p-3 whitespace-nowrap">
                                    <input type="checkbox" class="form-checkbox"
                                        name="{{ $request_for_payment->reference_id }}"
                                        value="{{ $request_for_payment->reference_id }}" wire:model="selected">
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($request_for_payment->parent_reference_no)
                                        <p wire:click="action({'parent_reference_no': '{{ $request_for_payment->parent_reference_no }}'}, 'view')"
                                            class="underline underline-offset-4 text-blue">
                                            {{ $request_for_payment->reference_id }}</p>
                                    @else
                                        <a href="https://cis2.capex.com.ph/index.php?r=acRfPayment/ViewApprovalDetails&AcRfPayment%5Brfpa_ref_id%5D={{ $request_for_payment->reference_id }}"
                                            target="_blank"
                                            class="underline underline-offset-4 text-blue">{{ $request_for_payment->reference_id }}</a>
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->type->display ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->payee->company ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->priority->display }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->description ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->branch->display ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->date_needed ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->opex->name ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($request_for_payment->amount, 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->user->name ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->approver_1_id ? (is_null($request_for_payment->is_approved_1) ? 'Pending' : ($request_for_payment->is_approved_1 ? 'Approved' : 'Reject')) : 'N/A' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->approver_2_id ? (is_null($request_for_payment->is_approved_2) ? 'Pending' : ($request_for_payment->is_approved_2 ? 'Approved' : 'Reject')) : 'N/A' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->approver_3_id ? (is_null($request_for_payment->is_approved_3) ? 'Pending' : ($request_for_payment->is_approved_3 ? 'Approved' : 'Reject')) : 'N/A' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->approver_1_id ? (is_null($request_for_payment->is_approved_1) ? '-' : ($request_for_payment->is_approved_1 ? $request_for_payment->approver_date_1 : $request_for_payment->approver_date_1)) : 'N/A' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->approver_2_id ? (is_null($request_for_payment->is_approved_2) ? '-' : ($request_for_payment->is_approved_2 ? $request_for_payment->approver_date_2 : $request_for_payment->approver_date_2)) : 'N/A' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->approver_3_id ? (is_null($request_for_payment->is_approved_3) ? '-' : ($request_for_payment->is_approved_3 ? $request_for_payment->approver_date_3 : $request_for_payment->approver_date_3)) : 'N/A' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->status->display ?? 'Not Set' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('M. d, Y', strtotime($request_for_payment->created_at)) }}
                                </td>

                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $request_for_payments->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
