<div wire:init="load">
    <x-loading />
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-4 gap-3">
                <div>
                    <x-label for="parent_reference_number" value="Parent Reference Number" :required="true" />
                    <x-input type="text" name="parent_reference_number" wire:model.defer="parent_reference_number">
                    </x-input>
                    <x-input-error for="parent_reference_number" />
                </div>
                <div>
                    <x-label for="reference_number" value="Reference Number" :required="true" />
                    <x-input type="text" name="reference_number" value="{{ $reference_number }}" disabled></x-input>
                    <x-input-error for="reference_number" />
                </div>
            </div>
            <div class="grid grid-cols-4 gap-3">
                <div>
                    <x-label for="division" value="Requesting Division" :required="true" />
                    <x-select name="division" wire:model='division'>
                        <option value=""></option>
                        @foreach ($division_references as $division_reference)
                            <option value="{{ $division_reference->id }}">
                                {{ $division_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="division" />
                </div>
                <div>
                    <x-label for="budget_source" value="Budget Source" :required="true" />
                    <x-select name="budget_source" wire:model='budget_source'>
                        <option value=""></option>
                        @foreach ($budget_source_references as $budget_source_reference)
                            <option value="{{ $budget_source_reference->id }}">
                                @if ($division == 7)
                                    {{ date('Y', strtotime($budget_source_reference->created_at)) }} -
                                    {{ $budget_source_reference->name }}
                                @else
                                    {{ $budget_source_reference->name }}
                                @endif
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="budget_source" />
                </div>
                <div>
                    <x-label for="chart_of_accounts_search" value="Chart Of Accounts" :required="true" />
                    <x-input type="text" list="budget_chart_suggestions_edit" name="chart_of_accounts_search"
                        wire:model.debounce.500ms="chart_of_accounts_search"></x-input>
                    <datalist id="budget_chart_suggestions_edit">
                        @foreach ($budget_chart_references as $budget_chart_reference)
                            <option>
                                {{ $budget_chart_reference->name }}
                            </option>
                        @endforeach
                    </datalist>
                    <x-input-error for="chart_of_accounts_search" />
                    <x-input-error for="chart_of_accounts" />
                </div>
                <div>
                    <x-label for="coa_category" value="Coa Category" :required="true" />
                    <x-select name="coa_category" wire:model='coa_category'>
                        <option value=""></option>
                        @foreach ($budget_plan_references as $budget_plan_reference)
                            <option value="{{ $budget_plan_reference->id }}">
                                {{ date('Y', strtotime($budget_plan_reference->created_at)) }} -
                                {{ $budget_plan_reference->item }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="coa_category" />
                </div>
            </div>
            <div class="grid grid-cols-4 gap-3">
                <div>
                    <x-label for="priority" value="Priority" :required="true" />
                    <x-select name="priority" wire:model.defer='priority'>
                        <option value=""></option>
                        @foreach ($priority_references as $priority_reference)
                            <option value="{{ $priority_reference->id }}">
                                {{ $priority_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="priority" />
                </div>
                <div>
                    <x-label for="date_needed" value="Date Needed" :required="true" />
                    <x-input type="date" name="date_needed" wire:model.defer='date_needed'></x-input>
                    <x-input-error for="date_needed" />
                </div>
                <div x-data="{ open: false }" class="relative mb-2 rounded-md" @click.away="open = false">
                    <div>
                        <div class="flex items-center justify-between">
                            <x-label for="payee" value="Payee" :required="true" />
                            <div class="flex space-x-3">
                                <input type="checkbox" class="form-checkbox" name="is_payee_contact_person"
                                    value="1" wire:model="is_payee_contact_person"
                                    @if ($is_payee_contact_person) checked @endif />
                                <x-label value="Display Payee Contact Person" />
                            </div>
                        </div>
                        <x-input type="text" @click="open = !open" name="supplier_search"
                            wire:model.debounce.500ms='supplier_search'></x-input>
                        <x-input-error for="supplier_search" />
                        <x-input-error for="payee" />
                    </div>
                    <div x-show="open" x-cloak
                        class="absolute w-full p-2 my-1 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96">
                        <ul class="list-reset">
                            @forelse ($supplier_references as $i => $supplier_reference)
                                <li @click="open = !open" wire:click="getSupplier({{ $supplier_reference->id }},1)"
                                    wire:key="{{ $i }}"
                                    class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                    <p>
                                        {{ $is_payee_contact_person ? $supplier_reference->first_name . ' ' . $supplier_reference->last_name : $supplier_reference->company }}
                                    </p>
                                </li>
                            @empty
                                <li>
                                    <p class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                        No Supplier Found.
                                    </p>
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
                <div>
                    <div class="flex items-center justify-between">
                        <x-label for="is_subpayee" value="Sub-Payee" />
                        {{-- <div class="flex space-x-3">
                            <input type="checkbox" class="form-checkbox" name="is_subpayee" value="1"
                                wire:model="is_subpayee" />
                            <x-label value="Display Sub-Payee in Voucher" />
                        </div> --}}
                    </div>
                    <x-input type="text" name="subpayee" wire:model.defer='subpayee'></x-input>
                    <x-input-error for="subpayee" />
                </div>
                <div>
                    <x-label for="branch" value="Branch" :required="true" />
                    <x-select name="branch" wire:model.defer='branch'>
                        <option value=""></option>
                        @foreach ($branch_references as $branch_reference)
                            <option value="{{ $branch_reference->id }}">
                                {{ $branch_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="branch" />
                </div>
                @if ($this->accounting_account_type != 1 && $this->payment_type != 2)
                    <div>
                        <x-label for="date_of_transaction_from" value="Date Of Transaction From" :required="true" />
                        <x-input type="date" name="date_of_transaction_from"
                            wire:model.defer='date_of_transaction_from'>
                        </x-input>
                        <x-input-error for="date_of_transaction_from" />
                    </div>
                    <div>
                        <x-label for="date_of_transaction_to" value="Date Of Transaction To" :required="true" />
                        <x-input type="date" name="date_of_transaction_to"
                            wire:model.defer='date_of_transaction_to'>
                        </x-input>
                        <x-input-error for="date_of_transaction_to" />
                    </div>
                @endif
                <div>
                    <x-label for="canvasser" value="Canvasser" />
                    <x-input type="text" name="canvasser" wire:model.defer='canvasser'></x-input>
                    <x-input-error for="canvasser" />
                </div>
                <div>
                    <x-label for="opex_type" value="Opex Type" :required="true" />
                    <x-select name="opex_type" wire:model.defer='opex_type'>
                        <option value=""></option>
                        @foreach ($opex_type_references as $opex_type_reference)
                            <option value="{{ $opex_type_reference->id }}">
                                {{ $opex_type_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="opex_type" />
                </div>
                <div>
                    <x-label for="description" value="Description" :required="true" />
                    <x-textarea type="text" name="description" wire:model.defer='description' />
                    <x-input-error for="description" />
                </div>
                <div>
                    <x-label for="remarks" value="Remarks" />
                    <x-textarea type="text" name="remarks" wire:model.defer='remarks' />
                    <x-input-error for="remarks" />
                </div>
            </div>
            @if ($rfp_type == 1)
                <div>
                    <div class="text-xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>Request For Payment</span>
                        </div>
                    </div>
                    <div class="grid grid-cols-4 gap-3">
                        <div>
                            <x-label for="po_reference_no" value="PO Reference No." :required="true" />
                            <x-input type="text" list="cavassing_supplier_suggestions" name="po_reference_no"
                                wire:model.debounce.500ms="po_reference_no"></x-input>
                            <datalist id="cavassing_supplier_suggestions">
                                @foreach ($canvassing_supplier_references as $canvassing_supplier_reference)
                                    <option value="{{ $canvassing_supplier_reference->po_reference_no }}">
                                        {{ $canvassing_supplier_reference->po_reference_no . ' - ' . $canvassing_supplier_reference->supplier->company }}
                                    </option>
                                @endforeach
                            </datalist>
                            <x-input-error for="po_reference_no" />
                        </div>
                    </div>
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="Particular / Description" />
                            <x-table.th name="Plate Number" />
                            <x-table.th name="Labor Cost" />
                            <x-table.th name="Unit Cost" />
                            <x-table.th name="Quantity" />
                            <x-table.th name="Amount" />
                        </x-slot>
                        <x-slot name="tbody">
                            @forelse ($request_for_payments as $i => $request_for_payment)
                                @if (!$request_for_payment['is_deleted'])
                                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text"
                                                name="request_for_payments.{{ $i }}.particulars"
                                                wire:model.defer="request_for_payments.{{ $i }}.particulars" />
                                            <x-input-error
                                                for="request_for_payments.{{ $i }}.particulars" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text"
                                                name="request_for_payments.{{ $i }}.plate_no"
                                                wire:model.defer="request_for_payments.{{ $i }}.plate_no" />
                                            <x-input-error for="request_for_payments.{{ $i }}.plate_no" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number" step="0.01"
                                                name="request_for_payments.{{ $i }}.labor_cost"
                                                wire:model.debounce.500ms="request_for_payments.{{ $i }}.labor_cost" />
                                            <x-input-error
                                                for="request_for_payments.{{ $i }}.labor_cost" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ number_format($request_for_payments[$i]['unit_cost'], 2) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $request_for_payments[$i]['quantity'] }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ number_format($request_for_payments[$i]['amount'], 2) }}
                                        </td>
                                    </tr>
                                @endif
                            @empty
                                <tr>
                                    <td colspan="6">
                                        <p class="text-center">Empty.</p>
                                    </td>
                                </tr>
                            @endforelse
                        </x-slot>
                    </x-table.table>
                    <x-input-error for="request_for_payments" />
                </div>
            @elseif($rfp_type == 2)
                <div>
                    <div class="text-xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>Freights</span>
                        </div>
                    </div>
                    <div class="grid grid-cols-4 gap-3">
                        <div>
                            <x-label for="accounting_account_type" value="Account Type" :required="true" />
                            <x-select name="accounting_account_type" wire:model='accounting_account_type'>
                                <option value=""></option>
                                @foreach ($accounting_account_type_references as $accounting_account_type_reference)
                                    <option value="{{ $accounting_account_type_reference->id }}">
                                        {{ $accounting_account_type_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="accounting_account_type" />
                        </div>
                        <div>
                            <x-label for="loader_search" value="Loaders" :required="true" />
                            <x-input type="text" list="loaders_suggestions" name="loader_search"
                                wire:model.debounce.500ms="loader_search"></x-input>
                            <datalist id="loaders_suggestions">
                                @foreach ($loaders_references as $loaders_reference)
                                    <option>
                                        {{ $loaders_reference->name }}
                                    </option>
                                @endforeach
                            </datalist>
                            <x-input-error for="loader_search" />
                            <x-input-error for="loader" />
                        </div>
                    </div>
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="Freight Reference No" />
                            <x-table.th name="Freight Reference Type" />
                            <x-table.th name="SOA No." />
                            <x-table.th name="Trucking Type" />
                            <x-table.th name="Trucking Amount" />
                            <x-table.th name="Freight Amount" />
                            <x-table.th name="Freight Usage" />
                            <x-table.th name="Transaction Date" />
                            <x-table.th name="Allowance" />
                            <x-table.th name="Amount" />
                            <x-table.th name="Action" />
                        </x-slot>
                        <x-slot name="tbody">
                            @forelse ($freights as $i => $freight)
                                @if (!$freight['is_deleted'])
                                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text"
                                                name="freights.{{ $i }}.freight_reference_no"
                                                wire:model.defer="freights.{{ $i }}.freight_reference_no" />
                                            <x-input-error for="freights.{{ $i }}.freight_reference_no" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-select name="freights.{{ $i }}.freight_reference_type"
                                                wire:model.defer='freights.{{ $i }}.freight_reference_type'>
                                                <option value=""></option>
                                                @foreach ($freight_reference_type_references as $freight_reference_type_reference)
                                                    <option value="{{ $freight_reference_type_reference->id }}">
                                                        {{ $freight_reference_type_reference->display }}
                                                    </option>
                                                @endforeach
                                            </x-select>
                                            <x-input-error
                                                for="freights.{{ $i }}.freight_reference_type" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text" name="freights.{{ $i }}.soa_no"
                                                wire:model.defer="freights.{{ $i }}.soa_no" />
                                            <x-input-error for="freights.{{ $i }}.soa_no" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-select name="freights.{{ $i }}.trucking_type"
                                                wire:model.defer='freights.{{ $i }}.trucking_type'>
                                                <option value=""></option>
                                                @foreach ($trucking_type_references as $trucking_type_reference)
                                                    <option value="{{ $trucking_type_reference->id }}">
                                                        {{ $trucking_type_reference->display }}
                                                    </option>
                                                @endforeach
                                            </x-select>
                                            <x-input-error for="freights.{{ $i }}.trucking_type" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number" step="0.01"
                                                name="freights.{{ $i }}.trucking_amount"
                                                wire:model="freights.{{ $i }}.trucking_amount" />
                                            <x-input-error for="freights.{{ $i }}.trucking_amount" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number" step="0.01"
                                                name="freights.{{ $i }}.freight_amount"
                                                wire:model="freights.{{ $i }}.freight_amount" />
                                            <x-input-error for="freights.{{ $i }}.freight_amount" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-select name="freights.{{ $i }}.freight_usage"
                                                wire:model.defer='freights.{{ $i }}.freight_usage'>
                                                <option value=""></option>
                                                @foreach ($freight_usage_references as $freight_usage_reference)
                                                    <option value="{{ $freight_usage_reference->id }}">
                                                        {{ $freight_usage_reference->display }}
                                                    </option>
                                                @endforeach
                                            </x-select>
                                            <x-input-error for="freights.{{ $i }}.freight_usage" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="date"
                                                name="freights.{{ $i }}.transaction_date"
                                                wire:model.defer="freights.{{ $i }}.transaction_date" />
                                            <x-input-error for="freights.{{ $i }}.transaction_date" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number" step="0.01"
                                                name="freights.{{ $i }}.allowance"
                                                wire:model="freights.{{ $i }}.allowance" />
                                            <x-input-error for="freights.{{ $i }}.allowance" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number" step="0.01"
                                                name="freights.{{ $i }}.amount"
                                                value="{{ $freights[$i]['amount'] }}" disabled />
                                            <x-input-error for="freights.{{ $i }}.amount" />
                                        </td>
                                        <td class="flex p-3 space-x-3 whitespace-nowrap">
                                            <svg wire:click="removeFreights({{ $i }})"
                                                class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                data-prefix="fas" data-icon="times-circle" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                </path>
                                            </svg>
                                            @if ($request->requestForPaymentDetails[0]->loaders)
                                                <a href="https://cis2.capex.com.ph/index.php?r=acRfPayment/getwingvanPAforprint&data=1&tranID={{ $freights[$i]['freight_reference_no'] }}&wingv={{ $request->requestForPaymentDetails[0]->loaders->code }}"
                                                    target="_blank">
                                                    <svg class="w-5 h-5 text-blue" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 576 512">
                                                        <path
                                                            d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                                    </svg>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @empty
                                <tr>
                                    <td colspan="11">
                                        <p class="text-center">Empty.</p>
                                    </td>
                                </tr>
                            @endforelse
                        </x-slot>
                    </x-table.table>
                    <div class="flex items-center justify-end">
                        <button type="button" wire:click="addFreights"
                            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                            Add Item</button>
                    </div>
                </div>
            @elseif($rfp_type == 3)
                <div class="space-y-3">
                    <div class="text-xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>Payables</span>
                        </div>
                    </div>
                    <div class="grid grid-cols-5 gap-3">
                        <div>
                            <x-label for="account_no" value="Account No." :required="true" />
                            <x-input type="text" name="account_no" wire:model.defer='account_no'></x-input>
                            <x-input-error for="account_no" />
                        </div>
                        <div>
                            <x-label for="payment_type" value="Payment Type" :required="true" />
                            <x-select name="payment_type" wire:model='payment_type'>
                                <option value=""></option>
                                @foreach ($payment_type_references as $payment_type_reference)
                                    <option value="{{ $payment_type_reference->id }}">
                                        {{ $payment_type_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="payment_type" />
                        </div>
                        <div>
                            <x-label for="terms" value="Type of Terms" :required="true" />
                            <x-select name="terms" wire:model.defer='terms'>
                                <option value=""></option>
                                @foreach ($terms_references as $terms_reference)
                                    <option value="{{ $terms_reference->id }}">
                                        {{ $terms_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="terms" />
                        </div>
                        <div>
                            <x-label for="pdc_from" value="Duration From" :required="true" />
                            <x-input type="date" name="pdc_from" wire:model.defer='pdc_from'></x-input>
                            <x-input-error for="pdc_from" />
                        </div>
                        <div>
                            <x-label for="pdc_to" value="Duration To" :required="true" />
                            <x-input type="date" name="pdc_to" wire:model.defer='pdc_to'></x-input>
                            <x-input-error for="pdc_to" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-table.table>
                                <x-slot name="thead">
                                    <x-table.th name="Invoice Number" />
                                    <x-table.th name="Invoice Amount" />
                                    @if ($payment_type == 2)
                                        <x-table.th name="Date Of Transaction" />
                                    @endif
                                    <x-table.th name="Action" />
                                </x-slot>
                                <x-slot name="tbody">
                                    @forelse ($payables as $i => $payable)
                                        @if (!$payable['is_deleted'])
                                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                                <td class="p-3 whitespace-nowrap">
                                                    <x-input type="text"
                                                        name="payables.{{ $i }}.invoice_no"
                                                        wire:model.defer="payables.{{ $i }}.invoice_no" />
                                                    <x-input-error for="payables.{{ $i }}.invoice_no" />
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    <x-input type="number" step="0.01"
                                                        name="payables.{{ $i }}.invoice_amount"
                                                        wire:model="payables.{{ $i }}.invoice_amount" />
                                                    <x-input-error
                                                        for="payables.{{ $i }}.invoice_amount" />
                                                </td>
                                                @if ($payment_type == 2)
                                                    <td class="p-3 whitespace-nowrap">
                                                        <x-input type="date"
                                                            name="payables.{{ $i }}.date_of_transaction"
                                                            wire:model="payables.{{ $i }}.date_of_transaction" />
                                                        <x-input-error
                                                            for="payables.{{ $i }}.date_of_transaction" />
                                                    </td>
                                                @endif
                                                <td class="p-3 whitespace-nowrap">
                                                    <svg wire:click="removePayables({{ $i }})"
                                                        class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="times-circle" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                            d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                        </path>
                                                    </svg>
                                                </td>
                                            </tr>
                                        @endif
                                    @empty
                                        <tr>
                                            <td colspan="3">
                                                <p class="text-center">Empty.</p>
                                            </td>
                                        </tr>
                                    @endforelse
                                </x-slot>
                            </x-table.table>
                            <div class="flex items-center justify-end">
                                <button type="button" wire:click="addPayables"
                                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                                    Add Item</button>
                            </div>
                        </div>
                    </div>
                </div>
            @elseif ($rfp_type == 4)
                <div>
                    <div class="text-xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>Cash Advance</span>
                        </div>
                    </div>
                    <div class="grid grid-cols-5 gap-3">
                        <div>
                            <x-label for="ca_no" value="CA No." />
                            <x-input type="text" name="ca_no" wire:model.defer="ca_no" disabled></x-input>
                            <x-input-error for="ca_no" />
                        </div>
                        <div>
                            <x-label for="ca_reference_type" value="CA Reference Type" />
                            <x-select name="ca_reference_type" wire:model='ca_reference_type'>
                                <option value=""></option>
                                @foreach ($ca_reference_types_references as $ca_reference_types_reference)
                                    <option value="{{ $ca_reference_types_reference->id }}">
                                        {{ $ca_reference_types_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="ca_reference_type" />
                        </div>
                    </div>
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="Particular / Description" />
                            <x-table.th name="{{ $ca_reference_type_display ?? 'CA Reference No' }}" />
                            <x-table.th name="Unit Cost" />
                            <x-table.th name="Quantity" />
                            <x-table.th name="Amount" />
                            <x-table.th name="Action" />
                        </x-slot>
                        <x-slot name="tbody">
                            @forelse ($cash_advances as $i => $cash_advance)
                                @if (!$cash_advance['is_deleted'])
                                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text"
                                                name="cash_advances.{{ $i }}.particulars"
                                                wire:model.defer="cash_advances.{{ $i }}.particulars" />
                                            <x-input-error for="cash_advances.{{ $i }}.particulars" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text"
                                                name="cash_advances.{{ $i }}.ca_reference_no"
                                                wire:model.defer="cash_advances.{{ $i }}.ca_reference_no" />
                                            <x-input-error for="cash_advances.{{ $i }}.ca_reference_no" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number" step="0.01"
                                                name="cash_advances.{{ $i }}.unit_cost"
                                                wire:model.debounce.500ms="cash_advances.{{ $i }}.unit_cost" />
                                            <x-input-error for="cash_advances.{{ $i }}.unit_cost" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="number"
                                                name="cash_advances.{{ $i }}.quantity"
                                                wire:model.debounce.500ms="cash_advances.{{ $i }}.quantity" />
                                            <x-input-error for="cash_advances.{{ $i }}.quantity" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ number_format($cash_advances[$i]['amount'], 2) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <svg wire:click="removeCashAdvances({{ $i }})"
                                                class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                data-prefix="fas" data-icon="times-circle" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                </path>
                                            </svg>
                                        </td>
                                    </tr>
                                @endif
                            @empty
                                <tr>
                                    <td colspan="6">
                                        <p class="text-center">Empty.</p>
                                    </td>
                                </tr>
                            @endforelse
                        </x-slot>
                    </x-table.table>
                    <x-input-error for="cash_advances" />
                    <div class="flex items-center justify-end">
                        <button type="button" wire:click="addCashAdvances"
                            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                            Add Item</button>
                    </div>
                </div>
            @endif
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Approvers</span>
                </div>
            </div>
            <div class="grid grid-cols-4 gap-3">
                <div>
                    <div class="flex items-center justify-between">
                        <x-label for="approver_1_id" value="Approver 1" :required="true" />
                        <div class="flex space-x-3">
                            <input type="checkbox" class="form-checkbox" name="is_set_approver_1" value="1"
                                wire:model="is_set_approver_1" @if ($is_set_approver_1) checked @endif />
                            <x-label value="Set Approver 1" />
                        </div>
                    </div>
                    <select name="approver_1_id" @if (!$is_set_approver_1) disabled @endif
                        wire:model='approver_1_id'
                        class="text-gray-600 block w-full mt-1 border border-gray-300 p-[2px] rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 @error('approver_1_id') border-red-600 @enderror">
                        <option value=""></option>
                        @foreach ($approvers as $approver)
                            <option value="{{ $approver->id }}">
                                {{ $approver->name }}
                            </option>
                        @endforeach
                    </select>
                    <x-input-error for="approver_1_id" />
                </div>
                <div>
                    <x-label for="approver_2_id" value="Approver 2" />
                    <x-input type="text" name="approver_2_id" value="{{ $approver_2_name }}" disabled></x-input>
                    <x-input-error for="approver_2_id" />
                </div>
                <div>
                    <x-label for="approver_3_id" value="Approver 3" />
                    <x-input type="text" name="approver_3_id" value="{{ $approver_3_name }}" disabled></x-input>
                    <x-input-error for="approver_3_id" />
                </div>
            </div>
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Attachments</span>
                </div>
            </div>
            <div class="grid grid-cols-4 gap-3">
                <div class="flex flex-col space-y-3">
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="File" />
                            <x-table.th name="Action" />
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($attachments as $a => $attachment)
                                @if (!$attachment['is_deleted'])
                                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="flex items-center justify-center p-2 whitespace-nowrap">
                                            <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                <div class="relative z-0 ">
                                                    <div class="absolute top-0 left-0">
                                                        @if ($attachment['id'] && !$attachments[$a]['attachment'])
                                                            @if (in_array($attachment['extension'], config('filesystems.image_type')))
                                                                <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                                    <a href="{{ Storage::disk('accounting_gcs')->url($attachment['path'] . $attachment['name']) }}"
                                                                        target="_blank"><img
                                                                            class="w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                            src="{{ Storage::disk('accounting_gcs')->url($attachment['path'] . $attachment['name']) }}"></a>
                                                                </div>
                                                            @elseif(in_array($attachment['extension'], config('filesystems.file_type')))
                                                                <svg wire:click="download({{ $attachment['id'] }})"
                                                                    class="w-20 h-20 mx-auto border border-gray-500 rounded-lg"
                                                                    aria-hidden="true" focusable="false"
                                                                    data-prefix="fas" data-icon="file-alt"
                                                                    role="img" xmlns="http://www.w3.org/2000/svg"
                                                                    viewBox="0 0 384 512">
                                                                    <path fill="currentColor"
                                                                        d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z">
                                                                    </path>
                                                                </svg>
                                                            @endif
                                                        @else
                                                            @if (!$attachments[$a]['attachment'])
                                                                <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                    src="{{ $attachments[$a]['attachment'] ? $attachments[$a]['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                            @elseif (in_array($attachments[$a]['attachment']->extension(), config('filesystems.image_type')))
                                                                <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                    src="{{ $attachments[$a]['attachment'] ? $attachments[$a]['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                            @else
                                                                <svg class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg"
                                                                    aria-hidden="true" focusable="false"
                                                                    data-prefix="fas" data-icon="file-alt"
                                                                    role="img" xmlns="http://www.w3.org/2000/svg"
                                                                    viewBox="0 0 384 512">
                                                                    <path fill="currentColor"
                                                                        d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z">
                                                                    </path>
                                                                </svg>
                                                            @endif
                                                        @endif
                                                    </div>
                                                    <input type="file"
                                                        name="attachments.{{ $a }}.attachment"
                                                        wire:model="attachments.{{ $a }}.attachment"
                                                        class="relative z-50 block w-20 h-20 opacity-0 cursor-pointer">
                                                    <x-input-error
                                                        for="attachments.{{ $a }}.attachment" />
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="flex items-center justify-center space-x-3">
                                                @if (count(collect($attachments)->where('is_deleted', false)) > 1)
                                                    <svg wire:click="removeAttachments({{ $a }})"
                                                        class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="times-circle" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                            d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                        </path>
                                                    </svg>
                                                @endif
                                                @if (in_array($attachment['extension'], config('filesystems.image_type')))
                                                    <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                        <a href="{{ Storage::disk('accounting_gcs')->url($attachment['path'] . $attachment['name']) }}"
                                                            target="_blank">
                                                            <svg class="w-5 h-5 text-blue"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 576 512" fill="currentColor">
                                                                <path
                                                                    d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                                            </svg>
                                                        </a>
                                                    </div>
                                                @elseif(in_array($attachment['extension'], config('filesystems.file_type')))
                                                    <svg wire:click="download({{ $attachment['id'] }})"
                                                        class="w-5 h-5 text-blue" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 512 512" fill="currentColor">
                                                        <path
                                                            d="M480 352h-133.5l-45.25 45.25C289.2 409.3 273.1 416 256 416s-33.16-6.656-45.25-18.75L165.5 352H32c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h448c17.67 0 32-14.33 32-32v-96C512 366.3 497.7 352 480 352zM432 456c-13.2 0-24-10.8-24-24c0-13.2 10.8-24 24-24s24 10.8 24 24C456 445.2 445.2 456 432 456zM233.4 374.6C239.6 380.9 247.8 384 256 384s16.38-3.125 22.62-9.375l128-128c12.49-12.5 12.49-32.75 0-45.25c-12.5-12.5-32.76-12.5-45.25 0L288 274.8V32c0-17.67-14.33-32-32-32C238.3 0 224 14.33 224 32v242.8L150.6 201.4c-12.49-12.5-32.75-12.5-45.25 0c-12.49 12.5-12.49 32.75 0 45.25L233.4 374.6z" />
                                                    </svg>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </x-slot>
                    </x-table.table>
                    <x-input-error for="attachments" />
                    <div class="flex items-center justify-end">
                        <button type="button" wire:click="addAttachments"
                            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                            Add Attachment</button>
                    </div>
                </div>
            </div>
            <div class="flex items-center justify-end space-x-3 text-3xl font-extrabold">
                <span class="">Total:</span>
                <span class="text-blue">{{ number_format($total_amount, 2) }}</span>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                @if (in_array($request->status_id, [1, 3]) && !$request->is_approved_2)
                    <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                        Update</button>
                @endif
            </div>
        </div>
    </form>
</div>
