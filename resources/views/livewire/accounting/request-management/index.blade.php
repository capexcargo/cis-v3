<x-form wire:init="load" x-data="{ search_form: false, confirmation_modal: '{{ $confirmation_modal }}', create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}', view_modal: '{{ $view_modal }}', import_modal: '{{ $import_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                {{ $confirmation_message }}
                <div class="flex justify-end space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click="remove"
                        class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>
        <x-modal id="import_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="title">Import Request For Payment</x-slot>
            <x-slot name="body">
                <div class="space-y-3">
                    <div>
                        <x-label for="import" value="Import" />
                        <x-input type="file" name="import" wire:model='import'></x-input>
                        <x-input-error for="import" />
                    </div>
                    <div class="flex items-center justify-end">
                        <button type="button" wire:click="import"
                            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Import</button>
                    </div>
                </div>
            </x-slot>
        </x-modal>
        @can('accounting_request_management_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                <x-slot name="title">Create Request</x-slot>
                <x-slot name="body">
                    @livewire('accounting.request-management.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('accounting_request_management_edit')
            @if ($request_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                    <x-slot name="title">Edit Request</x-slot>
                    <x-slot name="body">
                        @livewire('accounting.request-management.edit', ['id' => $request_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @if ($parent_reference_no)
            <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                <x-slot name="title">View Request</x-slot>
                <x-slot name="body">
                    @livewire('accounting.request-management.view', ['parent_reference_no' => $parent_reference_no])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>

    <x-slot name="header_title">Request List</x-slot>
    <x-slot name="header_button">
        <a target="_blank"
            href="{{ route('accounting.request-management.print-request-for-payment', ['selected' => json_encode($selected)]) }}"
            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Print Request For Payment</a>
        @can('accounting_request_management_import')
            <button wire:click="$set('import_modal', true)"
                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Import</button>
        @endcan
        @can('accounting_request_management_add')
            <button wire:click="action({}, 'create')"
                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Create Request</button>
        @endcan
    </x-slot>
    <x-slot name="search_form">
        @if (auth()->user()->level_id >= 5)
            <div>
                <x-label for="division" value="Budget Division" />
                <x-select name="division" wire:model='division'>
                    <option value=""></option>
                    @foreach ($division_references as $division_reference)
                        <option value="{{ $division_reference->id }}">
                            {{ $division_reference->name }}
                        </option>
                    @endforeach
                </x-select>
                <x-input-error for="division" class="mt-2" />
            </div>
        @endif
        <div>
            <x-label for="reference_no" value="Reference No" />
            <x-input type="text" name="reference_no" wire:model.debounce.500ms="reference_no" />
            <x-input-error for="reference_no" class="mt-2" />
        </div>
        <div>
            <x-label for="request_type" value="Request Type" />
            <x-select name="request_type" wire:model='request_type'>
                <option value=""></option>
                @foreach ($request_for_payment_type_references as $request_for_payment_type_reference)
                    <option value="{{ $request_for_payment_type_reference->id }}">
                        {{ $request_for_payment_type_reference->display }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="request_type" class="mt-2" />
        </div>
        <div>
            <x-label for="payee" value="Payee" />
            <x-input type="text" name="payee" wire:model.debounce.500ms="payee" />
            <x-input-error for="payee" class="mt-2" />
        </div>
        <div>
            <x-label for="opex_type" value="Opex Type" />
            <x-select name="opex_type" wire:model.debounce.500ms='opex_type'>
                <option value=""></option>
                @foreach ($opex_type_references as $opex_type_reference)
                    <option value="{{ $opex_type_reference->id }}">
                        {{ $opex_type_reference->name }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="opex_type" class="mt-2" />
        </div>
        <div>
            <x-label for="requester" value="Requester" />
            <x-input type="text" name="requester" wire:model.debounce.500ms="requester" />
            <x-input-error for="requester" class="mt-2" />
        </div>
        <div>
            <x-label for="description" value="Description" />
            <x-input type="text" name="description" wire:model.debounce.500ms="description" />
            <x-input-error for="description" class="mt-2" />
        </div>
        <div>
            <x-label for="date_created" value="Date Created" />
            <x-input type="date" name="date_created" wire:model.debounce.500ms="date_created" />
            <x-input-error for="date_created" class="mt-2" />
        </div>
        <div>
            <x-label for="status" value="Status" />
            <x-select name="status" wire:model='status'>
                <option value=""></option>
                @foreach ($status_references as $status_reference)
                    <option value="{{ $status_reference->id }}">
                        {{ $status_reference->display }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="status" class="mt-2" />
        </div>
        @can('accounting_request_management_delete')
            <div>
                <x-label for="trashed" value="Trashed" />
                <x-select name="trashed" wire:model='trashed'>
                    <option value=""></option>
                    <option value="with_trashed">With Trashed</option>
                    <option value="only_trashed">Only Trashed</option>
                </x-select>
                <x-input-error for="trashed" class="mt-2" />
            </div>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Select" />
                        <x-table.th name="Reference No." wire:click="sortBy('reference_id')">
                            <x-slot name="sort">
                                <x-table.sort-icon field="reference_id" sortField="{{ $sortField }}"
                                    sortAsc="{{ $sortAsc }}">
                                </x-table.sort-icon>
                            </x-slot>
                        </x-table.th>
                        <x-table.th name="Request Type" />
                        <x-table.th name="Payee" />
                        <x-table.th name="Opex Type" />
                        <x-table.th name="Amount" />
                        <x-table.th name="Requested By" />
                        <x-table.th name="Status" />
                        <x-table.th name="Accounting Status" />
                        <x-table.th name="Release Schedule" />
                        <x-table.th name="Created At" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($request_for_payments as $request_for_payment)
                            <tr
                                class="border-0 cursor-pointer @if ($request_for_payment->trashed()) bg-red-100 @else hover:text-white hover:bg-[#4068b8] @endif">
                                <td class="p-3 whitespace-nowrap">
                                    <input type="checkbox" class="form-checkbox"
                                        name="{{ $request_for_payment->reference_id }}"
                                        value="{{ $request_for_payment->reference_id }}" wire:model="selected">
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($request_for_payment->parent_reference_no)
                                        <p wire:click="action({'parent_reference_no': '{{ $request_for_payment->parent_reference_no }}'}, 'view')"
                                            class="underline underline-offset-4 text-blue">
                                            {{ $request_for_payment->reference_id }}</p>
                                    @else
                                        <a href="https://cis2.capex.com.ph/index.php?r=acRfPayment/ViewApprovalDetails&AcRfPayment%5Brfpa_ref_id%5D={{ $request_for_payment->reference_id }}"
                                            target="_blank"
                                            class="underline underline-offset-4 text-blue">{{ $request_for_payment->reference_id }}</a>
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->type->display ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->payee->company ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->opex->name ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($request_for_payment->amount, 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment->user->name ?? 'Not Set' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->status->display ?? 'Not Set' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if (isset($request_for_payment->checkVoucher))
                                        @if ($request_for_payment->checkVoucher->schedule_release_date != '')
                                            <span>{{ 'For Releasing' }}</span>
                                        @elseif ($request_for_payment->checkVoucher->status_id == 5)
                                            <span>{{ 'For Schedule of Release' }}</span>
                                        @elseif ($request_for_payment->checkVoucher->status_id != 5)
                                            <span>{{ 'Pending Voucher Approval' }}</span>
                                        @endif
                                    @else
                                        @if ($request_for_payment->status_id != '2')
                                            <span>{{ 'Pending RFP Final Approval' }}</span>
                                        @else
                                            <span>{{ 'Pending Creation of Voucher' }}</span>
                                        @endif
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span>{{ $request_for_payment->checkVoucher->schedule_release_date ?? '-' }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('M. d, Y', strtotime($request_for_payment->created_at)) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        @can('accounting_request_management_edit')
                                            <svg wire:click="action({'id': {{ $request_for_payment->id }}}, 'edit')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                </path>
                                            </svg>
                                        @endcan
                                        @can('accounting_request_management_delete')
                                            @if ($request_for_payment->trashed())
                                                <svg wire:click="action({'id': {{ $request_for_payment->id }}}, 'confirmation_restore')"
                                                    class="w-5 h-5 text-blue" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 448 512">
                                                    <path fill="currentColor"
                                                        d="M284.2 0C296.3 0 307.4 6.848 312.8 17.69L320 32H416C433.7 32 448 46.33 448 64C448 81.67 433.7 96 416 96H32C14.33 96 0 81.67 0 64C0 46.33 14.33 32 32 32H128L135.2 17.69C140.6 6.848 151.7 0 163.8 0H284.2zM31.1 128H416V448C416 483.3 387.3 512 352 512H95.1C60.65 512 31.1 483.3 31.1 448V128zM207 199L127 279C117.7 288.4 117.7 303.6 127 312.1C136.4 322.3 151.6 322.3 160.1 312.1L199.1 273.9V408C199.1 421.3 210.7 432 223.1 432C237.3 432 248 421.3 248 408V273.9L287 312.1C296.4 322.3 311.6 322.3 320.1 312.1C330.3 303.6 330.3 288.4 320.1 279L240.1 199C236.5 194.5 230.4 191.1 223.1 191.1C217.6 191.1 211.5 194.5 207 199V199z" />
                                                </svg>
                                            @else
                                                <svg wire:click="action({'id': {{ $request_for_payment->id }}}, 'confirmation_delete')"
                                                    class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="fas" data-icon="trash-alt" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                    <path fill="currentColor"
                                                        d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                                    </path>
                                                </svg>
                                            @endif
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $request_for_payments->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
