<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>
    @if ($confirmation_modal)
        <x-modal id="confirmation_modal" size="w-auto" hasClose="1">
            <x-slot name="body">
                <span class="relative block">
                    <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                        wire:click="$set('confirmation_modal', false)">
                    </span>
                </span>
                <h2 class="text-xl text-center">
                    Are you sure you want to submit details?
                </h2>
                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                        No
                    </button>
                    <button type="button" wire:click="submit"
                        class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                        Yes
                    </button>
                </div>
            </x-slot>
        </x-modal>
    @endif

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="mt-5 space-y-3 overflow-x-auto">
            <div class="grid grid-cols-12 gap-3 p-2 overflow-x-auto border-2 border-gray-200">
                <div class="col-span-12">
                    <x-label for="name" value="Name " />
                    <x-input type="text" name="name" wire:model.defer='name'></x-input>
                    <x-input-error class="absolute" for="name" />
                </div>
                <div class="col-span-12">
                    <x-label for="email_address" value="Email Address" />
                    <x-input type="text" name="email_address" wire:model.defer='email_address'></x-input>
                    <x-input-error class="absolute" for="email_address" />
                </div>
                <div class="col-span-6">
                    <x-label for="mobile_number" value="Mobile Number" />
                    <x-input type="text" name="mobile_number" wire:model.defer='mobile_number'></x-input>
                    <x-input-error class="absolute" for="mobile_number" />
                </div>
                <div class="col-span-6">
                    <x-label for="tel_number" value="Telephone Number (Optional)" />
                    <x-input type="text" name="tel_number" wire:model.defer='tel_number'></x-input>
                    <x-input-error class="absolute" for="tel_number" />
                </div>
                <div class="col-span-6">
                    <x-label for="agency" value="Company/Agency" />
                    <x-input type="text" name="agency" wire:model.defer='agency'></x-input>
                    <x-input-error class="absolute" for="agency" />
                </div>
                <div class="col-span-6">
                    <x-label for="tin" value="Tin Number" />
                    <x-input type="text" name="tin" wire:model.defer='tin'></x-input>
                    <x-input-error class="absolute" for="tin" />
                </div>
            </div>

            <div class="flex justify-end gap-3 mt-6 space-x-3">
                <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                    class="px-12 bg-white text-blue hover:bg-gray-100" />
                <x-button type="submit" title="Submit" class="px-12 bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>
    </form>
</div>
