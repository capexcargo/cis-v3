<x-form wire:init='load' x-data="{
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    purref_modal: '{{ $purref_modal }}'
}">

    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @if ($create_modal)
            <x-modal id="create_modal" size="w-11/12">
                <x-slot name="title">Create Purchase Requisition</x-slot>
                <x-slot name="body">
                    @livewire('accounting.purchase-req.create')
                </x-slot>
            </x-modal>
        @endif
        @if ($edit_modal)
            <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12" hasClose="1">
                <x-slot name="body">
                    @livewire('accounting.purchase-req.edit', ['id' => $reqid])
                </x-slot>
            </x-modal>
        @endif
        @if ($purref_modal)
            <x-modal id="purref_modal" size="w-3/4" hasClose="1">
                <x-slot name="purchase">
                    {{-- <div class="w-full text-center text-white bg-blue"> --}}
                    PURCHASE REQUISITION
                    {{-- </div> --}}
                </x-slot>

                <x-slot name="body">

                    <form>
                        <div class="grid grid-cols-12 gap-2 p-4">
                            <div class="flex col-span-4">
                                <table>
                                    <tr>
                                        <td>PRn No .:</td>
                                    </tr>
                                    <tr>
                                        <td>Date Created :</td>
                                    </tr>
                                    <tr>
                                        <td>Delivery Date :</td>
                                    </tr>
                                    <tr>
                                        <td>Priority Level :</td>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td>3</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-span-12">
                                <div class="bg-white border rounded-lg shadow-md">
                                    <x-table.table class="overflow-hidden">
                                        <x-slot name="thead">
                                            <x-table.th name="Item Category" style="padding-left:2%;" />
                                            <x-table.th name="Item Description" style="padding-left:%;" />
                                            <x-table.th name="Quantity" style="padding-left:%;" />
                                            <x-table.th name="Unit" style="padding-left:;" />
                                            <x-table.th name="Estimated Total" style="padding-left:;" />
                                            <x-table.th name="Purpose" style="padding-left:;" />
                                            <x-table.th name="Benificiary Branch" style="padding-left:;" />
                                            <x-table.th name="Preferred Supplier" style="padding-left:;" />
                                            <x-table.th name="Remarks" style="padding-left:;" />
                                            <x-table.th name="Action" style="padding-left:;" />
                                        </x-slot>
                                        <x-slot name="tbody">
                                            {{-- @foreach ($purchase_req_lists as $i => $purchase_req_list) --}}
                                            <tr class="font-normal border-0">
                                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:2%;">
                                                    <p class="w-24">
                                                        {{-- <input type="checkbox" class="ml-12 rounded-sm form-checkbox"
                                                            id="samp_prod"
                                                            name="purchase_forms.{{ $a }}.samp_prod"
                                                            value="1"
                                                            wire:model="purchase_forms.{{ $a }}.samp_prod"> --}}
                                                        {{-- {{ ($purchase_req_lists->currentPage() - 1) * $purchase_req_lists->links()->paginator->perPage() + $loop->iteration }} --}}
                                                    </p>
                                                </td>
                                                <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                                    {{-- <span class="underline cursor-pointer text-blue"
                                                            wire:click="actionpurchref({'id':{{ $purchase_req_list->id }}},'pref') }}">
                                                            {{ $purchase_req_list->reference_no }}
                                                        </span> --}}
                                                </td>
                                                <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                                    {{-- {{ $purchase_req_list->PurchReqDetailsPriorityy->name }} --}}
                                                </td>
                                                <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                                    {{-- {{ $purchase_req_list->expected_purchase_delivery_date }} --}}
                                                </td>
                                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:2%;">
                                                    <p class="w-24">
                                                        {{-- {{ ($purchase_req_lists->currentPage() - 1) * $purchase_req_lists->links()->paginator->perPage() + $loop->iteration }} --}}
                                                    </p>
                                                </td>
                                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:2%;">
                                                    <p class="w-24">
                                                        {{-- {{ ($purchase_req_lists->currentPage() - 1) * $purchase_req_lists->links()->paginator->perPage() + $loop->iteration }} --}}
                                                    </p>
                                                </td>
                                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:2%;">
                                                    <p class="w-24">
                                                        {{-- {{ ($purchase_req_lists->currentPage() - 1) * $purchase_req_lists->links()->paginator->perPage() + $loop->iteration }} --}}
                                                    </p>
                                                </td>
                                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:2%;">
                                                    <p class="w-24">
                                                        {{-- {{ ($purchase_req_lists->currentPage() - 1) * $purchase_req_lists->links()->paginator->perPage() + $loop->iteration }} --}}
                                                    </p>
                                                </td>
                                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:2%;">
                                                    <p class="w-24">
                                                        {{-- {{ ($purchase_req_lists->currentPage() - 1) * $purchase_req_lists->links()->paginator->perPage() + $loop->iteration }} --}}
                                                    </p>
                                                </td>
                                                <td>
                                                    {{-- <span title="Edit">
                                                            <svg wire:click="action({'id': {{ $purchase_req_list->id }}}, 'edit')"
                                                                class="w-5 h-5 ml-2 text-blue" aria-hidden="true" focusable="false"
                                                                data-prefix="far" data-icon="edit" role="img"
                                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                                <path fill="currentColor"
                                                                    d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                                                </path>
                                                            </svg>
                                                        </span> --}}
                                                </td>
                                            </tr>
                                            {{-- @endforeach --}}
                                        </x-slot>
                                    </x-table.table>
                                    <div class="flex justify-end px-2 pb-2 space-x-2">
                                        <div>Grand Total : </div>
                                        <div class="underline text-blue">P 2,100.00</div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-span-12">
                                <div class="bg-white border rounded-lg shadow-md">
                                    <x-table.table class="overflow-hidden">
                                        <x-slot name="thead">
                                            <x-table.th colspan="4" name="REQUESTER'S INFORMATION"
                                                class="text-center" style="padding-left:42.5%;" />
                                        </x-slot>
                                        <x-slot name="tbody">
                                            {{-- @foreach ($purchase_req_lists as $i => $purchase_req_list) --}}
                                            <tr class="font-normal border-0">
                                                <td class="w-1/4 p-3 border-r border-black whitespace-nowrap"
                                                    style="padding-left:;">
                                                    <div class="flex">
                                                        <table>
                                                            <tr>
                                                                <td>Name : </td>
                                                            </tr>
                                                        </table>
                                                        <table>
                                                            <tr>
                                                                <td>asdasd</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                                <td class="w-1/4 p-3 border-r border-black whitespace-nowrap"
                                                    style="padding-left:;">
                                                    <div class="flex">
                                                        <table>
                                                            <tr>
                                                                <td>Positions : </td>
                                                            </tr>
                                                        </table>
                                                        <table>
                                                            <tr>
                                                                <td>asdasd</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                                <td class="w-1/4 p-3 border-r border-black whitespace-nowrap"
                                                    style="padding-left:;">
                                                    <div class="flex">
                                                        <table>
                                                            <tr>
                                                                <td>Department : </td>
                                                            </tr>
                                                        </table>
                                                        <table>
                                                            <tr>
                                                                <td>asdasd</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                                    <div class="flex">
                                                        <table>
                                                            <tr>
                                                                <td>Branch : </td>
                                                            </tr>
                                                        </table>
                                                        <table>
                                                            <tr>
                                                                <td>asdasd</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            {{-- @endforeach --}}
                                        </x-slot>
                                    </x-table.table>
                                </div>
                            </div>

                            <div class="col-span-12">
                                <div class="bg-white border rounded-lg shadow-md">
                                    <x-table.table class="overflow-hidden">
                                        <x-slot name="thead">
                                            <tr>
                                                <x-table.th colspan="5" name="BUDGET DETAILS" class="text-center"
                                                    style="padding-left:45%;" />
                                            </tr>
                                            <tr class="border-t">
                                                <x-table.th name="Budget Source" class="text-center"
                                                    style="padding-left:; text-align: center;" />
                                                <x-table.th name="Category" class="text-center"
                                                    style="padding-left:;" />
                                                <x-table.th name="OPEX" class="text-center"
                                                    style="padding-left:;" />
                                                <x-table.th name="Chart of Accounts" class="text-center"
                                                    style="padding-left:;" />
                                                <x-table.th name="Sub Account" class="text-center"
                                                    style="padding-left:;" />
                                            </tr>
                                        </x-slot>
                                        <x-slot name="tbody">
                                            {{-- @foreach ($purchase_req_lists as $i => $purchase_req_list) --}}
                                            <tr class="font-normal border-0">
                                                <td class="w-1/4 p-3 border-r border-black whitespace-nowrap"
                                                    style="padding-left:;">

                                                </td>
                                                <td class="w-1/4 p-3 border-r border-black whitespace-nowrap"
                                                    style="padding-left:;">

                                                </td>
                                                <td class="w-1/4 p-3 border-r border-black whitespace-nowrap"
                                                    style="padding-left:;">

                                                </td>
                                                <td class="w-1/4 p-3 border-r whitespace-nowrap"
                                                    style="padding-left:;">

                                                </td>
                                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">

                                                </td>
                                            </tr>
                                            {{-- @endforeach --}}
                                        </x-slot>
                                    </x-table.table>
                                </div>
                            </div>
                        </div>
                    </form>
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">
        <div class="flex space-x-2">
            <div>
                Purchase Requisition
            </div>
        </div>
    </x-slot>
    <x-slot name="header_button">
        <button wire:click="action({}, 'add')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
            <div class="flex items-start justify-between">
                <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                    data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path fill="currentColor"
                        d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                </svg>
                Create Purchase Requisition
            </div>
        </button>

    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-6 text-sm" style="margin-top: 4%;">
            <div class="w-5/6">
                <div>
                    <x-transparent.input type="text" label="Prn No" name="reference_no"
                        wire:model.defer="reference_no" />
                </div>
            </div>
            <div class="">
                <x-button type="button" wire:click="search" title="Search"
                    class="px-2 py-1 mt-2 text-white bg-blue hover:bg-blue-800" />
            </div>
        </div>

        <div>
            <div class="flex items-center justify-start">
                <ul class="flex">
                    @foreach ($status_header_cards as $i => $status_card)
                        <li
                            class="px-4 py-1 text-xs {{ $status_header_cards[$i]['class'] }} {{ $stats == $status_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                            <button
                                wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                {{ $status_header_cards[$i]['title'] }}
                                <span class="ml-2 text-sm">{{ $status_header_cards[$i]['value'] }}</span>
                            </button>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="No." style="padding-left:2%;" />
                        <x-table.th name="PRn No." style="padding-left:%;" />
                        <x-table.th name="Priority Level" style="padding-left:%;" />
                        <x-table.th name="Expected Delivery Date" style="padding-left:;" />
                        <x-table.th name="Action" style="padding-left:;" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($purchase_req_lists as $i => $purchase_req_list)
                            <tr class="font-normal border-0">
                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:2%;">
                                    <p class="w-24">
                                        {{ ($purchase_req_lists->currentPage() - 1) * $purchase_req_lists->links()->paginator->perPage() + $loop->iteration }}
                                    </p>
                                </td>
                                <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                    <span class="underline cursor-pointer text-blue"
                                        wire:click="actionpurchref({'id':{{ $purchase_req_list->id }}},'pref') }}">
                                        {{ $purchase_req_list->reference_no }}
                                    </span>
                                </td>
                                <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $purchase_req_list->PurchReqDetailsPriorityy->name }}
                                </td>
                                <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $purchase_req_list->expected_purchase_delivery_date }}
                                </td>
                                <td>
                                    <span title="Edit">
                                        <svg wire:click="action({'id': {{ $purchase_req_list->id }}}, 'edit')"
                                            class="w-5 h-5 ml-2 text-blue" aria-hidden="true" focusable="false"
                                            data-prefix="far" data-icon="edit" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                            </path>
                                        </svg>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $purchase_req_lists->links() }}
                </div>
            </div>
        </div>
    </x-slot>

</x-form>
