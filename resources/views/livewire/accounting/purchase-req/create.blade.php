<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>
    @if ($confirmation_modal)
        <x-modal id="confirmation_modal" size="w-auto" hasClose="1">
            <x-slot name="body">
                <span class="relative block">
                    <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                        wire:click="$set('confirmation_modal', false)">
                    </span>
                </span>
                <h2 class="text-xl text-center">
                    Are you sure you want to submit this Purchase Request?
                </h2>

                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                        No
                    </button>
                    <button type="button" wire:click="submit"
                        class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                        Yes
                    </button>
                </div>
            </x-slot>
        </x-modal>
    @endif

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="mt-5 space-y-3 overflow-x-auto">
            <div class="flex">
                    <div class="mt-2">
                        Delivery Date :
                    </div>
                    <div class="w-1/5 ml-4">
                        <x-input class="w-full text-sm rounded-md h-9" style=" width: 80%" type="date"
                            name="del_date" wire:model.defer='del_date'></x-input>
                        <x-input-error class="absolute ml-4 text-xs" for="del_date" />
                    </div>
            </div>
            <?php $i = 0; ?>
            @foreach ($purchase_forms as $a => $purchase_form)
                <div class="grid grid-cols-12 gap-4 p-2 overflow-x-auto border-2 border-gray-200">
                    <div class="col-span-2">
                        <x-label for="item_category_id" value="Item Category" :required="true" />
                        <div wire:init="ItemCategoryReference" class="flex items-center">
                            <x-select style="cursor: pointer;"
                                name="purchase_forms.{{ $a }}.item_category_id"
                                wire:model='purchase_forms.{{ $a }}.item_category_id'>
                                <option value="">Select</option>
                                @foreach ($item_category_references as $item_category_reference)
                                    <option value="{{ $item_category_reference->id }}">
                                        {{ $item_category_reference->name }}
                                    </option>
                                @endforeach
                            </x-select>
                        </div>
                        <x-input-error for="item_category_id" />
                    </div>
                    <div class="col-span-2">
                        <x-label for="item_description_id" value="Item Description" :required="true" />
                        <div class="flex items-center" wire:init="">
                            <x-select style="cursor: pointer;"
                                name="purchase_forms.{{ $a }}.item_description_id"
                                wire:model.defer='purchase_forms.{{ $a }}.item_description_id'>
                                <option value="">Select</option>
                                @if(isset($item_description_references[$a]))
                                {{-- @dd($item_description_references) --}}
                                @foreach ($item_description_references[$a] as $q => $item_description_reference)
                                    <option value="{{ $item_description_references[$a][$q]['id']}}">
                                        {{ $item_description_references[$a][$q]['description']}}
                                    </option>
                                @endforeach
                                @endif
                            </x-select>
                        </div>
                        <x-input-error for="item_description_id" />
                    </div>
                    <div>
                        <x-label for="qty" value="Qty" />
                        <x-input type="text" name="purchase_forms.{{ $a }}.qty"
                            wire:model.defer='purchase_forms.{{ $a }}.qty'></x-input>
                        <x-input-error class="absolute" for="qty" />
                    </div>
                    <div>
                        <x-label for="unit" value="Unit" />
                        <div class="flex items-center">
                            <x-select style="cursor: pointer;" name="purchase_forms.{{ $a }}.unit"
                                wire:init="" wire:model.defer='purchase_forms.{{ $a }}.unit'>
                                <option value="">Select</option>
                                <option value="1">pads</option>
                                <option value="2">pcs</option>
                            </x-select>
                        </div>
                        <x-input-error for="unit" />
                    </div>
                    <div class="col-span-2">
                        <x-label for="estimated_price" value="Estimated Price" />
                        <x-input type="text" name="purchase_forms.{{ $a }}.estimated_price"
                            wire:model.defer='purchase_forms.{{ $a }}.estimated_price'></x-input>
                        <x-input-error class="absolute" for="estimated_price" />
                    </div>
                    <div class="col-span-2">
                        <x-label for="estimated_total" value="Estimated Total" />
                        <x-input type="text" name="purchase_forms.{{ $a }}.estimated_total"
                            wire:model.defer='purchase_forms.{{ $a }}.estimated_total'></x-input>
                        <x-input-error class="absolute" for="estimated_total" />
                    </div>
                    <div class="col-span-2">
                        <x-label for="purpose" value="Purpose" />
                        <div class="flex items-center">
                            <x-select style="cursor: pointer;" name="purchase_forms.{{ $a }}.purpose"
                                wire:init="PurposeReference" wire:model.defer='purchase_forms.{{ $a }}.purpose'>
                                <option value="">Select</option>
                                @foreach ($purpose_references as $purpose_reference)
                                <option value="{{ $purpose_reference->id }}">
                                    {{ $purpose_reference->name }}
                                </option>
                            @endforeach
                            </x-select>
                        </div>
                        <x-input-error for="purpose" />
                    </div>
                    <div class="col-span-2">
                        <x-label for="branch_id" value="Benificiary Branch" :required="true" />
                        <div class="flex items-center">
                            <x-select style="cursor: pointer;" name="purchase_forms.{{ $a }}.branch_id"
                                wire:init="loadBranchReference"
                                wire:model.defer='purchase_forms.{{ $a }}.branch_id'>
                                <option value="">Select</option>
                                @foreach ($branch_references as $branch_reference)
                                    <option value="{{ $branch_reference->id }}">
                                        {{ $branch_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                        </div>
                        <x-input-error for="branch_id" />
                    </div>
                    <div class="flex col-span-2 space-x-2">
                        <div>
                            <x-label for="sfrom" value="Series" class="text-right" />
                            <x-input type="text" name="purchase_forms.{{ $a }}.sfrom"
                                wire:model.defer='purchase_forms.{{ $a }}.sfrom'></x-input>
                            <x-input-error class="absolute" for="sfrom" />
                        </div>
                        <div>
                            <x-label for="sto" value="Number" />
                            <x-input type="text" name="purchase_forms.{{ $a }}.sto"
                                wire:model.defer='purchase_forms.{{ $a }}.sto'></x-input>
                            <x-input-error class="absolute" for="sto" />
                        </div>
                    </div>
                    <div class="col-span-2">
                        <x-label for="pref_supp" value="Preferred Supplier" :required="true" />
                        <div class="flex items-center">
                            <x-select style="cursor: pointer;" name="purchase_forms.{{ $a }}.pref_supp"
                                wire:init="PrefReference" wire:model.defer='purchase_forms.{{ $a }}.pref_supp'>
                                <option value="">Select</option>
                                @foreach ($pref_references as $pref_reference)
                                <option value="{{ $pref_reference->id }}">
                                    {{ $pref_reference->company }}
                                </option>
                            @endforeach
                            </x-select>
                        </div>
                        <x-input-error for="pref_supp" />
                    </div>
                    <div class="col-span-2">
                        <x-label for="rmks" value="Remarks" />
                        <x-input type="text" name="purchase_forms.{{ $a }}.rmks"
                            wire:model.defer='purchase_forms.{{ $a }}.rmks'></x-input>
                        <x-input-error class="absolute" for="rmks" />
                    </div>
                    <div class="col-span-2 ">
                        <x-label for="samp_prod" value="Sample Product" />
                            <input type="checkbox" class="ml-12 rounded-sm form-checkbox" id="samp_prod"
                                name="purchase_forms.{{ $a }}.samp_prod" value="1"
                                wire:model="purchase_forms.{{ $a }}.samp_prod">
                    </div>
                    <div class="flex -mt-2">
                        @if (count($purchase_forms) > 1)
                            <button type="button" title="Remove"
                                wire:click="removepurchase_forms({'a': {{ $a }}})">
                                <svg class="w-6 h-5 mt-1 mr-2 text-red" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="print-alt" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z" />
                                </svg>
                            </button>
                        @endif
                        @if (count($purchase_forms) == 1)
                            <button type="button" title="Adds"
                                wire:click="addpurchase_forms({{ $a + 1 }})">
                                <svg class="w-6 h-5 mt-1 mr-5 text-blue" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="print-alt" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path fill="currentColor"
                                        d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                </svg>
                            </button>
                        @endif

                        @if ($i != 0)
                            @if (count($purchase_forms) == $i + 1)
                                <button type="button" title="Add"
                                    wire:click="addpurchase_forms({{ $a + 1 }})">
                                    <svg class="w-6 h-5 mt-1 mr-5 text-blue" aria-hidden="true" focusable="false"
                                        data-prefix="far" data-icon="print-alt" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                        <path fill="currentColor"
                                            d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                                    </svg>
                                </button>
                            @endif
                        @endif
                    </div>

                </div>
                <?php $i++; ?>
            @endforeach

            <div class="flex justify-end gap-3 mt-6 space-x-3">
                <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                    class="px-12 bg-white text-blue hover:bg-gray-100" />
                <x-button type="submit" title="Submit" class="px-12 bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>
    </form>
</div>
