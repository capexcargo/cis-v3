<div wire:init="load">
    <x-loading />
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-3 gap-3">
                <div>
                    <x-label for="reference_no" value="Voucher Reference No." :required="true" />
                    <x-input type="text" name="reference_no" value="{{ $reference_no }}" disabled />
                    <x-input-error for="reference_no" />

                </div>
                <div>
                    <x-label for="rfp_reference_no" value="RFP Reference No." :required="true" />
                    <x-input type="text" name="rfp_reference_no" value="{{ $request_for_payment->reference_id }}"
                        disabled />
                    <x-input-error for="rfp_reference_no" />
                </div>
                <div>
                    <x-label for="cv_no" value="CV No" :required="true" />
                    <x-input type="text" name="cv_no" wire:model.defer='cv_no' disabled />
                    <x-input-error for="cv_no" />
                </div>
            </div>
            <div class="grid grid-cols-3 gap-3">
                <div x-data="{ open: false }" class="relative mb-2 rounded-md" @click.away="open = false">
                    <div>
                        <x-label for="supplier_search" value="Pay To" :required="true" />
                        <input type="text" @click="open = !open" name="pay_to" wire:model="supplier_search"
                            @if ($request_for_payment->parent_reference_no) disabled @endif
                            class="text-gray-600 block w-full mt-1 border border-gray-300 p-[2px] rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 @error('pay_to') border-red-600 @enderror">
                        <x-input-error for="supplier_search" />
                        <x-input-error for="pay_to" />
                    </div>
                    <div x-show="open" x-cloak
                        class="absolute w-full p-2 my-1 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96">
                        <ul class="list-reset">
                            @forelse ($supplier_references as $i => $supplier_reference)
                                <li @click="open = !open" wire:click="getSupplier({{ $supplier_reference->id }},1)"
                                    wire:key="{{ $i }}"
                                    class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                    <p>
                                        {{ $supplier_reference->company }}
                                    </p>
                                </li>
                            @empty
                                <li>
                                    <p class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                        No Supplier Found.
                                    </p>
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
                <div>
                    <x-label for="opex_type_id" value="Opex Type" :required="true" />
                    <x-input type="text" name="opex_type_id" value="{{ $request_for_payment->opex->name }}"
                        disabled />
                    <x-input-error for="opex_type_id" />
                </div>
                <div>
                    <x-label for="total_amount" value="Total Amount" :required="true" />
                    <x-input type="text" name="total_amount" value="{{ $request_for_payment->amount }}" disabled />
                    <x-input-error for="total_amount" />
                </div>
            </div>
            <div class="grid grid-cols-3 gap-3">
                <div>
                    <x-label for="description" value="Description" :required="true" />
                    <x-textarea type="text" name="description" wire:model.defer='description' />
                    <x-input-error for="description" />
                </div>
            </div>
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Check Details</span>
                </div>
            </div>
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="Date" />
                    <x-table.th name="Check No." />
                    <x-table.th name="Description" />
                    <x-table.th name="Amount" />
                    <x-table.th name="Action" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($vouchers as $a => $voucher)
                        @if (!$voucher['is_deleted'])
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    <x-input type="date" name="vouchers.{{ $a }}.date"
                                        wire:model.defer='vouchers.{{ $a }}.date'>
                                    </x-input>
                                    <x-input-error for="vouchers.{{ $a }}.date" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <x-input type="text" name="vouchers.{{ $a }}.voucher_no"
                                        wire:model.defer='vouchers.{{ $a }}.voucher_no'>
                                    </x-input>
                                    <x-input-error for="vouchers.{{ $a }}.voucher_no" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <x-input type="text" name="vouchers.{{ $a }}.description"
                                        wire:model.defer='vouchers.{{ $a }}.description'>
                                    </x-input>
                                    <x-input-error for="vouchers.{{ $a }}.description" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <x-input type="number" name="vouchers.{{ $a }}.amount"
                                        wire:model.debounce.500ms='vouchers.{{ $a }}.amount' step="0.01">
                                    </x-input>
                                    <x-input-error for="vouchers.{{ $a }}.amount" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        @if (count($vouchers[$a]) > 1)
                                            <svg wire:click="remove({'a': {{ $a }}})"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="fas" data-icon="trash-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                                </path>
                                            </svg>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </x-slot>
            </x-table.table>
            <x-input-error for="vouchers" />
            <div class="flex flex-col items-end justify-end">
                {{ number_format($total_voucher_amount, 2) }}
                <x-input-error for="total_voucher_amount" />
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="addVoucherItem()"
                    class="flex-none px-3 py-1 text-sm text-white rounded-full bg-green">
                    Add Check</button>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Save</button>
            </div>
        </div>
    </form>
</div>
