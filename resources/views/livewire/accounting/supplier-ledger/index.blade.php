<x-form wire:init="load" x-data="{ search_form: false, view_modal: '{{ $view_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @if ($supplier_id)
            <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/4">
                <x-slot name="title">View Supplier Ledger</x-slot>
                <x-slot name="body">
                    @livewire('accounting.supplier-ledger.view', ['id' => $supplier_id])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">Supplier Ledger List</x-slot>
    <x-slot name="header_button">
        <a target="_blank"
            href="{{ route('accounting.supplier-ledger.print-supplier-ledger', ['selected' => json_encode($selected)]) }}"
            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Print Supplier Ledger</a>
    </x-slot>
    <x-slot name="search_form">
        <div>
            <x-label for="company" value="Company" />
            <x-input type="text" list="supplier_suggestions" name="company" wire:model.debounce.500ms="company" />
            <datalist id="supplier_suggestions">
                @foreach ($supplier_references as $supplier_reference)
                    <option value="{{ $supplier_reference->company }}">
                        {{ $supplier_reference->company }}
                    </option>
                @endforeach
            </datalist>
            <x-input-error for="company" class="mt-2" />
        </div>
        <div>
            <x-label for="reference_no" value="Reference No" />
            <x-input type="text" name="reference_no" wire:model.debounce.500ms="reference_no" />
            <x-input-error for="reference_no" class="mt-2" />
        </div>
        <div>
            <x-label for="requested_from" value="Requested Form" />
            <x-input type="date" name="requested_from" wire:model.debounce.500ms="requested_from" />
            <x-input-error for="requested_from" class="mt-2" />
        </div>
        <div>
            <x-label for="requested_to" value="Requested To" />
            <x-input type="date" name="requested_to" wire:model.debounce.500ms="requested_to" />
            <x-input-error for="requested_to" class="mt-2" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <th colspan="4" class="p-3 tracking-wider whitespace-nowrap ">
                            <span class="flex justify-center font-medium">
                            </span>
                        </th>
                        <th colspan="2" class="p-3 tracking-wider whitespace-nowrap ">
                            <span class="flex justify-center font-medium">
                                Payee Transaction
                            </span>
                        </th>
                        <th colspan="2" class="p-3 tracking-wider whitespace-nowrap ">
                            <span class="flex justify-center font-medium">
                                Canvassing Supplier Transaction
                            </span>
                        </th>
                    </x-slot>
                    <x-slot name="thead1">
                        <x-table.th name="Select" />
                        <x-table.th name="Supplier/Company" />
                        <x-table.th name="Contact Person" />
                        <x-table.th name="Address" />
                        <x-table.th name="No. of Transaction" />
                        <x-table.th name="Total Amount" />
                        <x-table.th name="No. of Transaction" />
                        <x-table.th name="Total Amount" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($suppliers as $supplier)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    <input type="checkbox" class="form-checkbox" name="{{ $supplier->id }}"
                                        value="{{ $supplier->id }}" wire:model="selected">
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $supplier->company }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $supplier->first_name . ' ' . $supplier->last_name }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $supplier->address }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $supplier->payee_request_for_payments_count }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($supplier->payee_request_for_payments_sum_amount, 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $supplier->canvassing_supplier_request_for_payments_count }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($supplier->canvassing_supplier_request_for_payments_sum_amount, 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        <svg wire:click="action({'id': {{ $supplier->id }}}, 'view')"
                                            class="w-5 h-5 text-blue" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 576 512" fill="currentColor">
                                            <path
                                                d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $suppliers->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
