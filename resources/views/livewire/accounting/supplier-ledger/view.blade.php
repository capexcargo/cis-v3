<div x-data="{ view_modal: '{{ $view_modal }}', purchase_order_management_view_modal: '{{ $purchase_order_management_view_modal }}' }">
    <x-loading></x-loading>
    @if ($parent_reference_no)
        <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
            <x-slot name="title">View Request</x-slot>
            <x-slot name="body">
                @livewire('accounting.request-management.view', ['parent_reference_no' => $parent_reference_no])
            </x-slot>
        </x-modal>
    @endif
    @if ($canvassing_supplier_id)
        <x-modal id="purchase_order_management_view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
            <x-slot name="title">View Purchase Order</x-slot>
            <x-slot name="body">
                @livewire('accounting.purchase-order-management.view', ['id' => $canvassing_supplier_id])
            </x-slot>
        </x-modal>
    @endif
    <div class="space-y-3">
        <div class="grid grid-cols-2 gap-3">
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="name" value="Name" />
                    <x-input type="text" name="name" value="{{ $supplier->first_name . ' ' . $supplier->last_name }}"
                        disabled>
                    </x-input>
                    <x-input-error for="name" />
                </div>
                <div>
                    <x-label for="company" value="Company" />
                    <x-input type="text" name="company" value="{{ $supplier->company }}" disabled>
                    </x-input>
                    <x-input-error for="company" />
                </div>
                <div>
                    <x-label for="address" value="Address" />
                    <x-input type="text" name="address" value="{{ $supplier->address }}" disabled>
                    </x-input>
                    <x-input-error for="address" />
                </div>
            </div>
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="contact_no" value="Contact No." />
                    <x-input type="text" name="contact_no"
                        value="{{ $supplier->mobile_number . ' ' . $supplier->telephone_number }}" disabled>
                    </x-input>
                    <x-input-error for="contact_no" />
                </div>
                <div>
                    <x-label for="business_type" value="Business Type" />
                    <x-input type="text" name="business_type" value="{{ $supplier->type->display }}" disabled>
                    </x-input>
                    <x-input-error for="business_type" />
                </div>
                <div>
                    <x-label for="industry" value="Industry" />
                    <x-input type="text" name="industry" value="{{ $supplier->industry->display }}" disabled>
                    </x-input>
                    <x-input-error for="industry" />
                </div>
            </div>
        </div>
        <div class="text-xl font-semibold text-gray-800">
            <div class="flex items-center space-x-3">
                <span>Payee Request For Payment</span>
            </div>
        </div>
        <x-table.table>
            <x-slot name="thead">
                <x-table.th name="Reference No." />
                <x-table.th name="Requester" />
                <x-table.th name="Total Amount" />
                <x-table.th name="Created At" />
            </x-slot>
            <x-slot name="tbody">
                @foreach ($supplier->payeeRequestForPayments as $request_for_payment)
                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                        <td class="p-3 whitespace-nowrap">
                            <p wire:click="action({'parent_reference_no': '{{ $request_for_payment->parent_reference_no }}'}, 'view')"
                                class="underline underline-offset-4 text-blue">{{ $request_for_payment->reference_id }}
                            </p>
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ $request_for_payment->user->name }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ number_format($request_for_payment->amount, 2) }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ date('M. d, Y', strtotime($request_for_payment->created_at)) }}
                        </td>
                    </tr>
                @endforeach
            </x-slot>
        </x-table.table>
        <div class="flex items-center justify-end">
            <p>Total Amount: Php {{ number_format($supplier->payee_request_for_payments_sum_amount, 2) }}</p>
        </div>
        <div class="text-xl font-semibold text-gray-800">
            <div class="flex items-center space-x-3">
                <span>Canvassing Supplier Request For Payment</span>
            </div>
        </div>
        <x-table.table>
            <x-slot name="thead">
                <x-table.th name="Reference No." />
                <x-table.th name="PO Reference No." />
                <x-table.th name="Requester" />
                <x-table.th name="Total Amount" />
                <x-table.th name="Created At" />
            </x-slot>
            <x-slot name="tbody">
                @foreach ($supplier->canvassingSupplierRequestForPayments as $request_for_payment)
                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                        <td class="p-3 whitespace-nowrap">
                            <p wire:click="action({'parent_reference_no': '{{ $request_for_payment->parent_reference_no }}'}, 'view')"
                                class="underline underline-offset-4 text-blue">
                                {{ $request_for_payment->reference_id }}</p>
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            <p wire:click="action({'canvassing_supplier_id': '{{ $request_for_payment->canvassing_supplier_id }}'}, 'purchase_order_view')"
                                class="underline underline-offset-4 text-blue">
                                {{ $request_for_payment->canvassingSupplier->po_reference_no }}</p>
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ $request_for_payment->user->name }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ number_format($request_for_payment->amount, 2) }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ date('M. d, Y', strtotime($request_for_payment->created_at)) }}
                        </td>
                    </tr>
                @endforeach
            </x-slot>
        </x-table.table>
        <div class="flex items-center justify-end">
            <p>Total Amount: Php
                {{ number_format($supplier->canvassing_supplier_request_for_payments_sum_amount, 2) }}</p>
        </div>
        <div class="flex justify-end space-x-3">
            <button type="button" wire:click="$set('view_modal', false)"
                class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
        </div>
    </div>
</div>
