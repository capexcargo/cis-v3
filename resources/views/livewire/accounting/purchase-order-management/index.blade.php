<x-form x-data="{ search_form: false, purchase_order_management_view_modal: '{{ $purchase_order_management_view_modal }}', attachment_modal: '{{ $attachment_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @if ($canvassing_supplier_id)
            <x-modal id="purchase_order_management_view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                <x-slot name="title">View Purchase Order</x-slot>
                <x-slot name="body">
                    @livewire('accounting.purchase-order-management.view', ['id' => $canvassing_supplier_id])
                </x-slot>
            </x-modal>
        @endif
        @if ($canvassing_supplier_id)
            <x-modal id="attachment_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Add PO Attachment</x-slot>
                <x-slot name="body">
                    @livewire('accounting.purchase-order-management.attachment', ['id' => $canvassing_supplier_id])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">Purchase Order List</x-slot>
    <x-slot name="header_button">
        <a target="_blank"
            href="{{ route('accounting.purchase-order-management.print-purchase-order', ['selected' => json_encode($selected)]) }}"
            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Print Purchase Order</a>
    </x-slot>
    <x-slot name="search_form">
        <div>
            <x-label for="canvas_reference_no" value="Canvas Reference No" />
            <x-input type="text" name="canvas_reference_no" wire:model.debounce.500ms="canvas_reference_no" />
            <x-input-error for="canvas_reference_no" class="mt-2" />
        </div>
        <div>
            <x-label for="po_reference_no" value="PO Reference No" />
            <x-input type="text" name="po_reference_no" wire:model.debounce.500ms="po_reference_no" />
            <x-input-error for="po_reference_no" class="mt-2" />
        </div>
        <div>
            <x-label for="company_name" value="Company Name" />
            <x-input type="text" name="company_name" wire:model.debounce.500ms="company_name" />
            <x-input-error for="company_name" class="mt-2" />
        </div>
        <div>
            <x-label for="item_name" value="Item Name" />
            <x-input type="text" name="item_name" wire:model.debounce.500ms="item_name" />
            <x-input-error for="item_name" class="mt-2" />
        </div>
        <div>
            <x-label for="created_from" value="Created From" />
            <x-input type="date" name="created_from" wire:model.debounce.500ms="created_from" />
            <x-input-error for="created_from" class="mt-2" />
        </div>
        <div>
            <x-label for="created_to" value="Created To" />
            <x-input type="date" name="created_to" wire:model.debounce.500ms="created_to" />
            <x-input-error for="created_to" class="mt-2" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Select" />
                        <x-table.th name="CVS Reference Number" />
                        <x-table.th name="PO Reference Number" />
                        <x-table.th name="Supplier / Company" />
                        <x-table.th name="Total Amount" />
                        <x-table.th name="Created At" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($purchase_orders as $purchase_order)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    <input type="checkbox" class="form-checkbox"
                                        name="{{ $purchase_order->po_reference_no }}"
                                        value="{{ $purchase_order->po_reference_no }}" wire:model="selected">
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $purchase_order->canvassing->reference_id }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $purchase_order->po_reference_no }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $purchase_order->supplier->company }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($purchase_order->canvassing_supplier_item_sum_total_amount, 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('M. d, Y', strtotime($purchase_order->created_at)) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        <svg wire:click="action({'id': {{ $purchase_order->id }}}, 'edit')"
                                            class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                            data-prefix="far" data-icon="edit" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                            </path>
                                        </svg>
                                        <svg wire:click="action({'id': {{ $purchase_order->id }}}, 'attachment')"
                                            class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                            data-prefix="fas" data-icon="paperclip" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M43.246 466.142c-58.43-60.289-57.341-157.511 1.386-217.581L254.392 34c44.316-45.332 116.351-45.336 160.671 0 43.89 44.894 43.943 117.329 0 162.276L232.214 383.128c-29.855 30.537-78.633 30.111-107.982-.998-28.275-29.97-27.368-77.473 1.452-106.953l143.743-146.835c6.182-6.314 16.312-6.422 22.626-.241l22.861 22.379c6.315 6.182 6.422 16.312.241 22.626L171.427 319.927c-4.932 5.045-5.236 13.428-.648 18.292 4.372 4.634 11.245 4.711 15.688.165l182.849-186.851c19.613-20.062 19.613-52.725-.011-72.798-19.189-19.627-49.957-19.637-69.154 0L90.39 293.295c-34.763 35.56-35.299 93.12-1.191 128.313 34.01 35.093 88.985 35.137 123.058.286l172.06-175.999c6.177-6.319 16.307-6.433 22.626-.256l22.877 22.364c6.319 6.177 6.434 16.307.256 22.626l-172.06 175.998c-59.576 60.938-155.943 60.216-214.77-.485z">
                                            </path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $purchase_orders->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
