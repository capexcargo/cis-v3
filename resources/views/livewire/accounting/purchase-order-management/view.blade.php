<div>
    <x-loading />
    <div class="space-y-3">
        <div class="grid grid-cols-3 gap-3">
            <div>
                <x-label for="reference_number" value="Reference Number" />
                <x-input type="text" name="reference_number" value="{{ $canvassing_supplier->po_reference_no }}"
                    disabled></x-input>
                <x-input-error for="reference_number" />
            </div>
        </div>
        <div class="text-xl font-semibold text-gray-800">
            <div class="flex items-center justify-between space-x-3">
                <span>Supplier</span>
            </div>
        </div>
        <div class="grid grid-cols-3 gap-3">
            <div>
                <x-label for="company" value="Company" />
                <x-input type="text" name="company" value="{{ $canvassing_supplier->supplier->company }}" disabled>
                </x-input>
                <x-input-error for="company" />
            </div>
            <div>
                <x-label for="industry" value="Industry" />
                <x-input type="text" name="industry" value="{{ $canvassing_supplier->supplier->industry->display }}"
                    disabled>
                </x-input>
                <x-input-error for="industry" />
            </div>
            <div>
                <x-label for="type" value="Type" />
                <x-input type="text" name="type" value="{{ $canvassing_supplier->supplier->type->display }}"
                    disabled>
                </x-input>
                <x-input-error for="type" />
            </div>
        </div>
        <div class="text-xl font-semibold text-gray-800">
            <div class="flex items-center space-x-3">
                <span>Contact Person</span>
            </div>
        </div>
        <div class="grid grid-cols-3 gap-3">
            <div>
                <x-label for="first_name" value="First Name" />
                <x-input type="text" name="type" value="{{ $canvassing_supplier->supplier->first_name }}"
                    disabled>
                </x-input>
                <x-input-error for="first_name" />
            </div>
            <div>
                <x-label for="middle_name" value="Middle Name" />
                <x-input type="text" name="type" value="{{ $canvassing_supplier->supplier->middle_name }}"
                    disabled>
                </x-input>
                <x-input-error for="middle_name" />
            </div>
            <div>
                <x-label for="last_name" value="Last Name" />
                <x-input type="text" name="type" value="{{ $canvassing_supplier->supplier->last_name }}" disabled>
                </x-input>
                <x-input-error for="last_name" />
            </div>
            <div>
                <x-label for="email" value="Email" />
                <x-input type="text" name="type" value="{{ $canvassing_supplier->supplier->email }}" disabled>
                </x-input>
                <x-input-error for="email" />
            </div>
            <div>
                <x-label for="mobile_number" value="Mobile Number" />
                <x-input type="text" name="type" value="{{ $canvassing_supplier->supplier->mobile_number }}"
                    disabled>
                </x-input>
                <x-input-error for="mobile_number" />
            </div>
            <div>
                <x-label for="telephone_number" value="Telephone Number" />
                <x-input type="text" name="type" value="{{ $canvassing_supplier->supplier->telephone_number }}"
                    disabled>
                </x-input>
                <x-input-error for="telephone_number" />
            </div>
            <div>
                <x-label for="branch" value="Branch" />
                <x-input type="text" name="type" value="{{ $canvassing_supplier->supplier->branch->display }}"
                    disabled>
                </x-input>
                <x-input-error for="branch" />
            </div>
            <div>
                <x-label for="address" value="Address" />
                <x-input type="text" name="type" value="{{ $canvassing_supplier->supplier->address }}" disabled>
                </x-input>
                <x-input-error for="address" />
            </div>
            <div>
                <x-label for="terms" value="Terms" />
                <x-input type="text" name="type" value="{{ $canvassing_supplier->supplier->terms }}" disabled>
                </x-input>
                <x-input-error for="terms" />
            </div>
        </div>
        <div class="text-xl font-semibold text-gray-800">
            <div class="flex items-center space-x-3">
                <span>Items</span>
            </div>
        </div>
        <x-table.table>
            <x-slot name="thead">
                <x-table.th name="Name" />
                <x-table.th name="Unit Cost" />
                <x-table.th name="Quantity" />
                <x-table.th name="Total Amount" />
                <x-table.th name="Rec. Status" />
                <x-table.th name="Final Status" />
            </x-slot>
            <x-slot name="tbody">
                @foreach ($canvassing_supplier->canvassingSupplierItem as $canvassing_supplier_item)
                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                        <td class="p-3 whitespace-nowrap">
                            {{ $canvassing_supplier_item->supplierItem->name }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ number_format($canvassing_supplier_item->unit_cost, 2) }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ $canvassing_supplier_item->quantity }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ number_format($canvassing_supplier_item->total_amount, 2) }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            <span
                                class="py-1 px-3 rounded-full text-xs {{ $canvassing_supplier_item->recommended_status ? 'text-green bg-green-300' : 'text-red bg-red-300' }}">{{ $canvassing_supplier_item->recommended_status ? 'Approved' : 'Not Approved' }}</span>
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            <span
                                class="py-1 px-3 rounded-full text-xs {{ $canvassing_supplier_item->final_status ? 'text-green bg-green-300' : 'text-red bg-red-300' }}">{{ $canvassing_supplier_item->final_status ? 'Approved' : 'Not Approved' }}</span>
                        </td>
                    </tr>
                @endforeach
            </x-slot>
        </x-table.table>
        <div class="text-xl font-semibold text-gray-800">
            <div class="flex items-center space-x-3">
                <span>Waybill Series</span>
            </div>
        </div>
        <x-table.table>
            <x-slot name="thead">
                <x-table.th name="Branch" />
                <x-table.th name="Waybill From" />
                <x-table.th name="Waybill To" />
                <x-table.th name="Remove" />
            </x-slot>
            <x-slot name="tbody">
                @foreach ($waybills as $i => $waybill)
                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                        <td class="p-3 whitespace-nowrap">
                            <x-input type="text" name="waybills.{{ $i }}.branch_id"
                                wire:model="waybills.{{ $i }}.branch_id" maxlength="3" />
                            <x-input-error for="waybills.{{ $i }}.branch_id" />
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            <x-input type="text" name="waybills.{{ $i }}.waybill_from"
                                wire:model.defer="waybills.{{ $i }}.waybill_from" disabled />
                            <x-input-error for="waybills.{{ $i }}.waybill_from" />
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            <x-input type="text" name="waybills.{{ $i }}.waybill_to"
                                wire:model.defer="waybills.{{ $i }}.waybill_to" />
                            <x-input-error for="waybills.{{ $i }}.waybill_to" />
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            <svg wire:click="removeWaybill({{ $i }})" class="w-5 h-5 text-red"
                                aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times-circle"
                                role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                <path fill="currentColor"
                                    d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                </path>
                            </svg>
                        </td>
                    </tr>
                @endforeach
            </x-slot>
        </x-table.table>
        <div class="flex items-center justify-end">
            <button type="button" wire:click="addWaybillSeries"
                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                Add Waybill</button>&nbsp;
            <button type="button" wire:click="submitWaybillSeries()"
                class="flex-none px-3 py-1 text-sm text-black bg-green-300 rounded-full">
                Submit Waybill Series</button>
        </div>
        <div class="grid grid-cols-4 gap-3">
            <div class="flex flex-col space-y-3">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="File" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($canvassing_supplier->attachments as $i => $attachment)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="flex items-center justify-center p-2 whitespace-nowrap">
                                    @if (in_array($attachment->extension, config('filesystems.image_type')))
                                        <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                            <a href="{{ Storage::disk('accounting_gcs')->url($attachment->path . $attachment->name) }}"
                                                target="_blank"><img
                                                    class="w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                    src="{{ Storage::disk('accounting_gcs')->url($attachment->path . $attachment->name) }}"></a>
                                        </div>
                                    @elseif(in_array($attachment->extension, config('filesystems.file_type')))
                                        <svg wire:click="download({{ $attachment->id }})"
                                            class="w-20 h-20 mx-auto border border-gray-500 rounded-lg"
                                            aria-hidden="true" focusable="false" data-prefix="fas"
                                            data-icon="file-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 384 512">
                                            <path fill="currentColor"
                                                d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z">
                                            </path>
                                        </svg>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
            </div>
        </div>
        <div class="flex justify-end space-x-3">
            <button type="button" wire:click="$set('purchase_order_management_view_modal', false)"
                class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
        </div>
    </div>
</div>
