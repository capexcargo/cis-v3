<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="attachment" value="Attachment" />
                    <x-input type="file" name="attachment" wire:model.defer='attachment'></x-input>
                    <x-input-error for="attachment" />
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$set('attachment_modal', false)"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Save</button>
            </div>
        </div>
    </form>
</div>
