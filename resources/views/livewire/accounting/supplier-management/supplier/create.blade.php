<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-3 gap-3">
                <div>
                    <x-label for="company" value="Company" :required="true" />
                    <x-input type="text" name="company" wire:model.defer='company'></x-input>
                    <x-input-error for="company" />
                </div>
                <div wire:init="loadSupplierIndustryReference">
                    <x-label for="industry" value="Industry" :required="true" />
                    <x-select name="industry" wire:model.defer='industry'>
                        <option value=""></option>
                        @foreach ($supplier_industry_references as $supplier_industry_reference)
                            <option value="{{ $supplier_industry_reference->id }}">
                                {{ $supplier_industry_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="industry" />
                </div>
                <div wire:init="loadSupplierTypeReference">
                    <x-label for="type" value="Type" :required="true" />
                    <x-select name="type" wire:model.defer='type'>
                        <option value=""></option>
                        @foreach ($supplier_type_references as $supplier_type_reference)
                            <option value="{{ $supplier_type_reference->id }}">
                                {{ $supplier_type_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="type" />
                </div>
            </div>
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Contact Person</span>
                </div>
            </div>
            <div class="grid grid-cols-3 gap-3">
                <div>
                    <x-label for="first_name" value="First Name" :required="true" />
                    <x-input type="text" name="first_name" wire:model.defer='first_name'></x-input>
                    <x-input-error for="first_name" />
                </div>
                <div>
                    <x-label for="middle_name" value="Middle Name" :required="true" />
                    <x-input type="text" name="middle_name" wire:model.defer='middle_name'></x-input>
                    <x-input-error for="middle_name" />
                </div>
                <div>
                    <x-label for="last_name" value="Last Name" :required="true" />
                    <x-input type="text" name="last_name" wire:model.defer='last_name'></x-input>
                    <x-input-error for="last_name" />
                </div>
                <div>
                    <x-label for="email" value="Email" :required="true" />
                    <x-input type="text" name="email" wire:model.defer='email'></x-input>
                    <x-input-error for="email" />
                </div>
                <div>
                    <x-label for="mobile_number" value="Mobile Number" :required="true" />
                    <x-input type="number" name="mobile_number" wire:model.defer='mobile_number'></x-input>
                    <x-input-error for="mobile_number" />
                </div>
                <div>
                    <x-label for="telephone_number" value="Telephone Number" :required="true" />
                    <x-input type="number" name="telephone_number" wire:model.defer='telephone_number'></x-input>
                    <x-input-error for="telephone_number" />
                </div>
                <div wire:init="loadBranchReference">
                    <x-label for="branch" value="Branch" :required="true" />
                    <x-select name="branch" wire:model.defer='branch'>
                        <option value=""></option>
                        @foreach ($branch_references as $branch_reference)
                            <option value="{{ $branch_reference->id }}">
                                {{ $branch_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="branch" />
                </div>
                <div>
                    <x-label for="address" value="Address" :required="true" />
                    <x-input type="text" name="address" wire:model.defer='address'></x-input>
                    <x-input-error for="address" />
                </div>
                <div>
                    <x-label for="terms" value="Terms" :required="true" />
                    <x-input type="number" name="terms" wire:model.defer='terms'></x-input>
                    <x-input-error for="terms" />
                </div>
            </div>

            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Create</button>
            </div>
        </div>
    </form>
</div>
