<div x-data="{ create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}' }">
    <x-loading></x-loading>
    <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="title">Create Item</x-slot>
        <x-slot name="body">
            <form wire:submit.prevent="createItem" autocomplete="off">
                <div class="space-y-3">
                    <div class="grid grid-cols-1 gap-3">
                        <div>
                            <x-label for="name" value="Name" :required="true" />
                            <x-input type="text" name="name" wire:model.defer='name'></x-input>
                            <x-input-error for="name" />
                        </div>
                    </div>
                    <div class="flex justify-end space-x-3">
                        <button type="button" wire:click="$set('create_modal', false)"
                            class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                        <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                            Create</button>
                    </div>
                </div>
            </form>
        </x-slot>
    </x-modal>
    @if ($supplier_item)
        <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="title">Edit Item</x-slot>
            <x-slot name="body">
                <form wire:submit.prevent="editItem" autocomplete="off">
                    <div class="space-y-3">
                        <div class="grid grid-cols-1 gap-3">
                            <div>
                                <x-label for="name" value="Name" :required="true" />
                                <x-input type="text" name="name" wire:model.defer='name'></x-input>
                                <x-input-error for="name" />
                            </div>
                        </div>
                        <div class="flex justify-end space-x-3">
                            <button type="button" wire:click="$set('edit_modal', false)"
                                class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                            <button type="submit"
                                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                                Update</button>
                        </div>
                    </div>
                </form>
            </x-slot>
        </x-modal>
    @endif
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-3 gap-3">
                <div>
                    <x-label for="company" value="Company" :required="true" />
                    <x-input type="text" name="company" wire:model.defer='company'></x-input>
                    <x-input-error for="company" />
                </div>
                <div wire:init="loadSupplierIndustryReference">
                    <x-label for="industry" value="Industry" :required="true" />
                    <x-select name="industry" wire:model.defer='industry'>
                        <option value=""></option>
                        @foreach ($supplier_industry_references as $supplier_industry_reference)
                            <option value="{{ $supplier_industry_reference->id }}">
                                {{ $supplier_industry_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="industry" />
                </div>
                <div wire:init="loadSupplierTypeReference">
                    <x-label for="type" value="Type" :required="true" />
                    <x-select name="type" wire:model.defer='type'>
                        <option value=""></option>
                        @foreach ($supplier_type_references as $supplier_type_reference)
                            <option value="{{ $supplier_type_reference->id }}">
                                {{ $supplier_type_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="type" />
                </div>
            </div>
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Contact Person</span>
                </div>
            </div>
            <div class="grid grid-cols-3 gap-3">
                <div>
                    <x-label for="first_name" value="First Name" :required="true" />
                    <x-input type="text" name="first_name" wire:model.defer='first_name'></x-input>
                    <x-input-error for="first_name" />
                </div>
                <div>
                    <x-label for="middle_name" value="Middle Name" :required="true" />
                    <x-input type="text" name="middle_name" wire:model.defer='middle_name'></x-input>
                    <x-input-error for="middle_name" />
                </div>
                <div>
                    <x-label for="last_name" value="Last Name" :required="true" />
                    <x-input type="text" name="last_name" wire:model.defer='last_name'></x-input>
                    <x-input-error for="last_name" />
                </div>
                <div>
                    <x-label for="email" value="Email" :required="true" />
                    <x-input type="text" name="email" wire:model.defer='email'></x-input>
                    <x-input-error for="email" />
                </div>
                <div>
                    <x-label for="mobile_number" value="Mobile Number" :required="true" />
                    <x-input type="number" name="mobile_number" wire:model.defer='mobile_number'></x-input>
                    <x-input-error for="mobile_number" />
                </div>
                <div>
                    <x-label for="telephone_number" value="Telephone Number" :required="true" />
                    <x-input type="number" name="telephone_number" wire:model.defer='telephone_number'></x-input>
                    <x-input-error for="telephone_number" />
                </div>
                <div wire:init="loadBranchReference">
                    <x-label for="branch" value="Branch" :required="true" />
                    <x-select name="branch" wire:model.defer='branch'>
                        <option value=""></option>
                        @foreach ($branch_references as $branch_reference)
                            <option value="{{ $branch_reference->id }}">
                                {{ $branch_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="branch" />
                </div>
                <div>
                    <x-label for="address" value="Address" :required="true" />
                    <x-input type="text" name="address" wire:model.defer='address'></x-input>
                    <x-input-error for="address" />
                </div>
                <div>
                    <x-label for="terms" value="Terms" :required="true" />
                    <x-input type="number" name="terms" wire:model.defer='terms'></x-input>
                    <x-input-error for="terms" />
                </div>
            </div>

            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$set('create_modal', true)"
                    class="flex-none px-3 py-1 text-sm text-white rounded-full bg-green">
                    Add Item</button>
            </div>

            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center justify-between space-x-3">
                    <span>Items</span>
                    <div class="w-1/4">
                        <x-input type="search" name="search" wire:model.debounce.500ms='search'></x-input>
                    </div>
                </div>
            </div>
            <div x-data="{ open: true }">
                @if (session()->has('message'))
                    <div class="relative px-4 py-3 text-green-700 bg-green-100 border border-green-400 rounded"
                        :class="open ? '' : 'hidden'" role="alert">
                        <span class="block sm:inline">{{ session('message') }}</span>
                        <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
                            <svg @click="open = false" class="w-6 h-6 text-green-500 fill-current" role="button"
                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                <title>Close</title>
                                <path
                                    d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" />
                            </svg>
                        </span>
                    </div>
                @endif
            </div>
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="Name" />
                    <x-table.th name="Action" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($items as $item)
                        <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                            <td class="p-3 whitespace-nowrap">
                                {{ $item->name }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex space-x-3">
                                    <svg wire:click="action({'id': {{ $item->id }}}, 'edit')"
                                        class="w-5 h-5 text-blue" aria-hidden="true" focusable="false" data-prefix="far"
                                        data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                        </path>
                                    </svg>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </x-slot>
            </x-table.table>
            <div class="px-1 pb-2">
                {{ $items->links() }}
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Update</button>
            </div>
        </div>
    </form>
</div>
