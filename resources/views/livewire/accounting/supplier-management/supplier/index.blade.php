<x-form wire:init="load" x-data="{ search_form: false, create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('accounting_supplier_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                <x-slot name="title">Create Supplier</x-slot>
                <x-slot name="body">
                    @livewire('accounting.supplier-management.supplier.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('accounting_supplier_edit')
            @if ($supplier_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                    <x-slot name="title">Edit Supplier</x-slot>
                    <x-slot name="body">
                        @livewire('accounting.supplier-management.supplier.edit', ['id' => $supplier_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>
    <x-slot name="header_title">Supplier List</x-slot>
    <x-slot name="header_button">
        @can('accounting_supplier_add')
            <button wire:click="$set('create_modal', true)"
                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Create Supplier</button>
        @endcan
    </x-slot>
    <x-slot name="search_form">
        <div>
            <x-label for="industry" value="Industry" :required="true" />
            <x-select name="industry" wire:model='industry'>
                <option value=""></option>
                @foreach ($supplier_industry_references as $supplier_industry_reference)
                    <option value="{{ $supplier_industry_reference->id }}">
                        {{ $supplier_industry_reference->display }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="industry" />
        </div>
        <div>
            <x-label for="type" value="Type" :required="true" />
            <x-select name="type" wire:model='type'>
                <option value=""></option>
                @foreach ($supplier_type_references as $supplier_type_reference)
                    <option value="{{ $supplier_type_reference->id }}">
                        {{ $supplier_type_reference->display }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="type" />
        </div>
        <div>
            <x-label for="branch" value="Branch" :required="true" />
            <x-select name="branch" wire:model='branch'>
                <option value=""></option>
                @foreach ($branch_references as $branch_reference)
                    <option value="{{ $branch_reference->id }}">
                        {{ $branch_reference->display }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="branch" />
        </div>
        <div>
            <x-label for="company" value="Company" :required="true" />
            <x-input type="text" name="company" wire:model.debounce.500ms='company'></x-input>
            <x-input-error for="company" />
        </div>
        <div>
            <x-label for="email" value="Email" :required="true" />
            <x-input type="text" name="email" wire:model.debounce.500ms='email'></x-input>
            <x-input-error for="email" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Industry" />
                        <x-table.th name="Type" />
                        <x-table.th name="Branch" />
                        <x-table.th name="Company" />
                        <x-table.th name="Email" />
                        <x-table.th name="Mobile Number" />
                        <x-table.th name="Telephone Number" />
                        <x-table.th name="Created At" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($suppliers as $supplier)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $supplier->industry->display }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $supplier->type->display }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $supplier->branch->display ?? 'N/A' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $supplier->company }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $supplier->email }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $supplier->mobile_number }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $supplier->telephone_number }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('M. d, Y', strtotime($supplier->created_at)) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        @can('accounting_supplier_edit')
                                            <svg wire:click="action({'id': {{ $supplier->id }}}, 'edit')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                </path>
                                            </svg>
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $suppliers->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
