<div wire:init="load" x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>
    @if ($confirmation_modal)
        <x-modal id="confirmation_modal" size="w-auto" hasClose="1">
            <x-slot name="body">
                <span class="relative block">
                    <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                        wire:click="$set('confirmation_modal', false)">
                    </span>
                </span>
                <h2 class="text-xl text-center">
                    Are you sure you want to submit details?
                </h2>
                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                        No
                    </button>
                    <button type="button" wire:click="submit"
                        class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                        Yes
                    </button>
                </div>
            </x-slot>
        </x-modal>
    @endif

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="mt-5 space-y-3 overflow-x-auto">
            <div class="grid grid-cols-12 gap-3 p-2 overflow-x-auto border-2 border-gray-200">
                <div class="col-span-2">
                    <x-label for="service_category" value="Service Category" />
                    <x-input type="text" name="service_category" wire:model.defer='service_category'></x-input>
                    <x-input-error class="absolute" for="service_category" />
                </div>
                <div class="1">
                    <x-label for="recurring" style="margin-left: 15%" value="Recurring" />
                    <input type="checkbox" style="margin-left: 40%" class="rounded-sm form-checkbox" id="recurring"
                        name="recurring" value="1" wire:model="recurring"
                        @if ($recurring == 1) checked @endif>
                    <x-input-error class="text-center" for="recurring" />
                </div>
                <div class="col-span-2">
                    <x-label for="budget_source" value="Budget Source" :required="true" />
                    <div wire:init="BudgetSourceReference" class="flex items-center">
                        <x-select style="cursor: pointer;" name="budget_source" wire:model.defer='budget_source'>
                            <option value="">Select</option>
                            @foreach ($budget_source_references as $budget_source_reference)
                                <option value="{{ $budget_source_reference->id }}">
                                    {{ $budget_source_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="budget_source" />
                </div>
                <div class="col-span-2">
                    <x-label for="opex_category" value="OpEx Category" :required="true" />
                    <div wire:init="OpexCategoryReference" class="flex items-center">
                        <x-select style="cursor: pointer;" name="opex_category" wire:model='opex_category'>
                            <option value="">Select</option>
                            @foreach ($opex_category_references as $opex_category_reference)
                                <option value="{{ $opex_category_reference->id }}">
                                    {{ $opex_category_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="opex_category" />
                </div>
                <div class="col-span-1">
                    <x-label for="opex_type" value="OpEx Type" :required="true" />
                    <div wire:init="" class="flex items-center">
                        <x-select style="cursor: pointer;" name="opex_type" wire:model.defer='opex_type' disabled>
                            {{-- <option value="">Select</option> --}}
                            @foreach ($opex_type_references as $opex_type_reference)
                                <option value="{{ $opex_type_reference->id }}">
                                    {{ $opex_type_reference->opex_name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="opex_type" />
                </div>
                <div class="col-span-2">
                    <x-label for="chart_of_accounts" value="Chart of Accounts" :required="true" />
                    <div wire:init="ChartOfAccountsReference" class="flex items-center">
                        <x-select style="cursor: pointer;" name="chart_of_accounts"
                            wire:model.defer='chart_of_accounts'>
                            <option value="">Select</option>
                            @foreach ($chart_of_accounts_references as $chart_of_accounts_reference)
                                <option value="{{ $chart_of_accounts_reference->id }}">
                                    {{ $chart_of_accounts_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="chart_of_accounts" />
                </div>
                <div class="col-span-2">
                    <x-label for="sub_accounts" value="Sub Accounts" :required="true" />
                    <div wire:init="SubAccountsReference" class="flex items-center">
                        <x-select style="cursor: pointer;" name="sub_accounts" wire:model.defer='sub_accounts'>
                            <option value="">Select</option>
                            @foreach ($sub_accounts_references as $sub_accounts_reference)
                                <option value="{{ $sub_accounts_reference->id }}">
                                    {{ $sub_accounts_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="sub_accounts" />
                </div>
            </div>

            <div class="flex justify-end gap-3 mt-6 space-x-3">
                <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                    class="px-12 bg-white text-blue hover:bg-gray-100" />
                <x-button type="submit" title="Submit" class="px-12 bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>
    </form>
</div>
