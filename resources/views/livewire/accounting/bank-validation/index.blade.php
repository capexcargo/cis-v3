<x-form wire:init="load" x-data="{ search_form: false, import_modal: '{{ $import_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('accounting_bank_validation_import')
            <x-modal id="import_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Import Bank Validation</x-slot>
                <x-slot name="body">
                    <div class="space-y-3">
                        <div>
                            <x-label for="import" value="Import" />
                            <x-input type="file" name="import" wire:model='import'></x-input>
                            <x-input-error for="import" />
                        </div>
                        <div class="flex items-center justify-end">
                            <button type="button" wire:click="import()"
                                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Import</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
    </x-slot>
    <x-slot name="header_title">Bank Validation List</x-slot>
    <x-slot name="header_button">
        @can('accounting_bank_validation_import')
            <button wire:click="$set('import_modal', true)"
                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Import</button>
        @endcan
    </x-slot>
    <x-slot name="header_card">
        @forelse ($header_cards as $index => $card)
            <x-card.header wire:key="{{ $index }}" :card="$card"></x-card.header>
        @empty
            <x-card.header-loading count="2"></x-card.header-loading>
        @endforelse
    </x-slot>
    <x-slot name="search_form">
        <div>
            <x-label for="transaction_date_from" value="Transaction Date From" />
            <x-input type="date" name="transaction_date_from" wire:model="transaction_date_from" />
            <x-input-error for="transaction_date_from" class="mt-2" />
        </div>
        <div>
            <x-label for="transaction_date_to" value="Transaction Date To" />
            <x-input type="date" name="transaction_date_to" wire:model="transaction_date_to" />
            <x-input-error for="transaction_date_to" class="mt-2" />
        </div>
        <div>
            <x-label for="month" value="Month" />
            <x-select name="month" wire:model='month'>
                <option value=""></option>
                @foreach ($month_references as $month_reference)
                    <option value="{{ $month_reference->id }}">{{ $month_reference->display }}</option>
                @endforeach
            </x-select>
            <x-input-error for="month" />
        </div>
        <div>
            <x-label for="year" value="Year" />
            <x-input type="number" name="year" wire:model.debounce.500ms="year" />
            <x-input-error for="year" class="mt-2" />
        </div>
        <div>
            <x-label for="account_no" value="Account No" />
            <x-input type="text" name="account_no" wire:model.debounce.500ms="account_no" />
            <x-input-error for="account_no" class="mt-2" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Transaction Date" />
                        <x-table.th name="Check No" />
                        <x-table.th name="Description" />
                        <x-table.th name="Debit Amount" />
                        <x-table.th name="Credit Amount" />
                        <x-table.th name="Balance Amount" />
                        <x-table.th name="Branch" />
                        <x-table.th name="MB Branch" />
                        <x-table.th name="Account No" />
                        <x-table.th name="Paymode" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($bank_validations as $bank_validation)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $bank_validation->transaction_date }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $bank_validation->check_no }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $bank_validation->description }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($bank_validation->debit_amount, 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($bank_validation->credit_amount, 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($bank_validation->balance_amount, 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $bank_validation->branch }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $bank_validation->mb_branch }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $bank_validation->account_no }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $bank_validation->paymentType->display }}
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $bank_validations->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
