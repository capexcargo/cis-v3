<x-form wire:init="load" x-data="{ search_form: false, create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('accounting_budget_chart_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Create Budget Chart</x-slot>
                <x-slot name="body">
                    @livewire('accounting.budget-management.budget-chart.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('accounting_budget_chart_edit')
            @if ($budget_chart_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Edit Budget Chart</x-slot>
                    <x-slot name="body">
                        @livewire('accounting.budget-management.budget-chart.edit', ['id' => $budget_chart_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>
    <x-slot name="header_title">Budget Chart List</x-slot>
    <x-slot name="header_button">
        @can('accounting_budget_chart_add')
            <div class="flex items-center justify-between space-x-3">
                <button wire:click="$set('create_modal', true)"
                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Create Budget Chart</button>
            </div>
        @endcan
    </x-slot>
    <x-slot name="header_card">
        @forelse ($header_cards as $index => $card)
            <x-card.header wire:click="$set('{{ $card['action'] }}', {{ $card['id'] }})"
                wire:key="{{ $index }}" :card="$card"></x-card.header>
        @empty
            <x-card.header-loading count="9"></x-card.header-loading>
        @endforelse
    </x-slot>
    <x-slot name="search_form">
        <div>
            <x-label for="division" value="Division" />
            <x-select name="division" wire:model='division'>
                <option value=""></option>
                @foreach ($division_references as $division_reference)
                    <option value="{{ $division_reference->id }}">{{ $division_reference->name }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="division" />
        </div>
        <div>
            <x-label for="source" value="Source" />
            <x-select name="source" wire:model='source'>
                <option value=""></option>
                @foreach ($budget_source_references as $budget_source_reference)
                    <option value="{{ $budget_source_reference->id }}">{{ $budget_source_reference->name }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="source" />
        </div>
        <div>
            <x-label for="name" value="Name" />
            <x-input type="text" name="name" wire:model='name'></x-input>
            <x-input-error for="name" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="ID" />
                        <x-table.th name="Division" />
                        <x-table.th name="Budget Source" />
                        <x-table.th name="Name" />
                        <x-table.th name="Created At" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($budget_charts as $budget_chart)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $budget_chart->id }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $budget_chart->division->name }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $budget_chart->budgetSource->name }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $budget_chart->name }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('M. d, Y', strtotime($budget_chart->created_at)) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        @can('accounting_budget_chart_edit')
                                            <svg wire:click="action({'id': {{ $budget_chart->id }}}, 'edit')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                </path>
                                            </svg>
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $budget_charts->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
