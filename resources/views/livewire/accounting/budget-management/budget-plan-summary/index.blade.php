<x-form wire:init="load" x-data="{ search_form: false }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="header_title">Budget Plan Summary List</x-slot>
    <x-slot name="header_card">
        @forelse ($header_cards as $i => $card)
            <div wire:click="checkbox({{ $card['id'] }})" wire:key="{{ $i }}"
                class="w-full transition-all duration-500 ease-out delay-75 transform">
                <div
                    class="{{ in_array($card['id'], $seleted) ? 'bg-blue-300' : 'bg-white' }} flex flex-row justify-center p-3 space-x-3 duration-500 transform shadow-md cursor-pointer rounded-3xl hover:scale-105">
                    <div class="flex flex-col w-full text-center">
                        <span class="text-4xl font-bold ">{{ $card['approved_value'] }}</span>
                        <div class="flex items-center justify-between">
                            <span>Availed: {{ $card['availed_value'] }}</span>
                            <span>Balanced: {{ $card['current_value'] }}</span>
                        </div>
                        <span
                            class="text-sm {{ $card['icon'] ?? $card['color'] . ' font-extrabold text-lg' }}">{{ $card['title'] }}</span>
                    </div>
                </div>
            </div>
        @empty
            <x-card.header-loading count="8" :icon="false"></x-card.header-loading>
        @endforelse
    </x-slot>
    <x-slot name="search_form">
        <div>
            <x-label for="year" value="Year" />
            <x-input type="text" name="year" wire:model.debounce.500ms="year" />
            <x-input-error for="year" class="mt-2" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <th class="font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap"></th>
                        <th class="font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap"></th>
                        <th class="font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap"></th>
                        <th class="font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap"></th>
                        <th class="font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">January</th>
                        <th class="font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">February</th>
                        <th class="font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">March</th>
                        <th class="font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">April</th>
                        <th class="font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">May</th>
                        <th class="font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">June</th>
                        <th class="font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">July</th>
                        <th class="font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">August</th>
                        <th class="font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">September</th>
                        <th class="font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">October</th>
                        <th class="font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">November</th>
                        <th class="font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">December</th>
                    </x-slot>
                    <x-slot name="tbody">
                        <tr class="text-xs bg-white">
                            <td class="p-3 whitespace-nowrap">Account Code</td>
                            <td class="p-3 whitespace-nowrap">Total Approved</td>
                            <td class="p-3 whitespace-nowrap">Total Availed</td>
                            <td class="p-3 whitespace-nowrap">Total Balance</td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-between space-x-3">
                                    <div>Approved</div>
                                    <div>Availed</div>
                                    <div>Balanced</div>
                                </div>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-between space-x-3">
                                    <div>Approved</div>
                                    <div>Availed</div>
                                    <div>Balanced</div>
                                </div>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-between space-x-3">
                                    <div>Approved</div>
                                    <div>Availed</div>
                                    <div>Balanced</div>
                                </div>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-between space-x-3">
                                    <div>Approved</div>
                                    <div>Availed</div>
                                    <div>Balanced</div>
                                </div>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-between space-x-3">
                                    <div>Approved</div>
                                    <div>Availed</div>
                                    <div>Balanced</div>
                                </div>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-between space-x-3">
                                    <div>Approved</div>
                                    <div>Availed</div>
                                    <div>Balanced</div>
                                </div>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-between space-x-3">
                                    <div>Approved</div>
                                    <div>Availed</div>
                                    <div>Balanced</div>
                                </div>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-between space-x-3">
                                    <div>Approved</div>
                                    <div>Availed</div>
                                    <div>Balanced</div>
                                </div>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-between space-x-3">
                                    <div>Approved</div>
                                    <div>Availed</div>
                                    <div>Balanced</div>
                                </div>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-between space-x-3">
                                    <div>Approved</div>
                                    <div>Availed</div>
                                    <div>Balanced</div>
                                </div>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-between space-x-3">
                                    <div>Approved</div>
                                    <div>Availed</div>
                                    <div>Balanced</div>
                                </div>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-between space-x-3">
                                    <div>Approved</div>
                                    <div>Availed</div>
                                    <div>Balanced</div>
                                </div>
                            </td>
                        </tr>
                        @foreach ($divisions as $division)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $division->name }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($division->budget_plan_sum_total_amount + $division->transfer_budget_to_sum_amount, 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($division->transfer_budget_from_sum_amount + $division->availment_sum_amount, 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($division->budget_plan_sum_total_amount + $division->transfer_budget_to_sum_amount - ($division->transfer_budget_from_sum_amount + $division->availment_sum_amount), 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @php
                                        $div_january_approved =
                                            $division->budget_plan_sum_january +
                                            ($division->transferBudgetTo
                                                ? $division
                                                    ->transferBudgetTo()
                                                    ->where('month_to', 'january')
                                                    ->sum('amount')
                                                : 0) -
                                            ($division->transferBudgetFrom
                                                ? $division
                                                    ->transferBudgetFrom()
                                                    ->where('month_from', 'january')
                                                    ->sum('amount')
                                                : 0);
                                        $div_january_availed = $division->availment
                                            ? $division
                                                ->availment()
                                                ->where('month', 'january')
                                                ->sum('amount')
                                            : 0;
                                        
                                        $div_persent_january_availed = $div_january_availed && $div_january_approved ? ($div_january_availed / $div_january_approved) * 100 : 0;
                                    @endphp
                                    <div class="flex items-center justify-between space-x-3">
                                        <div>
                                            {{ number_format($div_january_approved, 2) }}
                                        </div>
                                        <div>
                                            {{ number_format($div_january_availed, 2) . '(' . round($div_persent_january_availed, 2) . ' %)' }}
                                        </div>
                                        <div>
                                            {{ number_format($div_january_approved - $div_january_availed, 2) . '(' . round(100 - $div_persent_january_availed, 2) . ' %)' }}
                                        </div>
                                    </div>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @php
                                        $div_february_approved =
                                            $division->budget_plan_sum_february +
                                            ($division->transferBudgetTo
                                                ? $division
                                                    ->transferBudgetTo()
                                                    ->where('month_to', 'february')
                                                    ->sum('amount')
                                                : 0) -
                                            ($division->transferBudgetFrom
                                                ? $division
                                                    ->transferBudgetFrom()
                                                    ->where('month_from', 'february')
                                                    ->sum('amount')
                                                : 0);
                                        $div_february_availed = $division->availment
                                            ? $division
                                                ->availment()
                                                ->where('month', 'february')
                                                ->sum('amount')
                                            : 0;
                                        
                                        $div_persent_february_availed = $div_february_availed && $div_february_approved ? ($div_february_availed / $div_february_approved) * 100 : 0;
                                    @endphp
                                    <div class="flex items-center justify-between space-x-3">
                                        <div>
                                            {{ number_format($div_february_approved, 2) }}
                                        </div>
                                        <div>
                                            {{ number_format($div_february_availed, 2) . '(' . round($div_persent_february_availed, 2) . ' %)' }}
                                        </div>
                                        <div>
                                            {{ number_format($div_february_approved - $div_february_availed, 2) . '(' . round(100 - $div_persent_february_availed, 2) . ' %)' }}
                                        </div>
                                    </div>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @php
                                        $div_march_approved =
                                            $division->budget_plan_sum_march +
                                            ($division->transferBudgetTo
                                                ? $division
                                                    ->transferBudgetTo()
                                                    ->where('month_to', 'march')
                                                    ->sum('amount')
                                                : 0) -
                                            ($division->transferBudgetFrom
                                                ? $division
                                                    ->transferBudgetFrom()
                                                    ->where('month_from', 'march')
                                                    ->sum('amount')
                                                : 0);
                                        $div_march_availed = $division->availment
                                            ? $division
                                                ->availment()
                                                ->where('month', 'march')
                                                ->sum('amount')
                                            : 0;
                                        
                                        $div_persent_march_availed = $div_march_availed && $div_march_approved ? ($div_march_availed / $div_march_approved) * 100 : 0;
                                    @endphp
                                    <div class="flex items-center justify-between space-x-3">
                                        <div>
                                            {{ number_format($div_march_approved, 2) }}
                                        </div>
                                        <div>
                                            {{ number_format($div_march_availed, 2) . '(' . round($div_persent_march_availed, 2) . ' %)' }}
                                        </div>
                                        <div>
                                            {{ number_format($div_march_approved - $div_march_availed, 2) . '(' . round(100 - $div_persent_march_availed, 2) . ' %)' }}
                                        </div>
                                    </div>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @php
                                        $div_april_approved =
                                            $division->budget_plan_sum_april +
                                            ($division->transferBudgetTo
                                                ? $division
                                                    ->transferBudgetTo()
                                                    ->where('month_to', 'april')
                                                    ->sum('amount')
                                                : 0) -
                                            ($division->transferBudgetFrom
                                                ? $division
                                                    ->transferBudgetFrom()
                                                    ->where('month_from', 'april')
                                                    ->sum('amount')
                                                : 0);
                                        $div_april_availed = $division->availment
                                            ? $division
                                                ->availment()
                                                ->where('month', 'april')
                                                ->sum('amount')
                                            : 0;
                                        
                                        $div_persent_april_availed = $div_april_availed && $div_april_approved ? ($div_april_availed / $div_april_approved) * 100 : 0;
                                    @endphp
                                    <div class="flex items-center justify-between space-x-3">
                                        <div>
                                            {{ number_format($div_april_approved, 2) }}
                                        </div>
                                        <div>
                                            {{ number_format($div_april_availed, 2) . '(' . round($div_persent_april_availed, 2) . ' %)' }}
                                        </div>
                                        <div>
                                            {{ number_format($div_april_approved - $div_april_availed, 2) . '(' . round(100 - $div_persent_april_availed, 2) . ' %)' }}
                                        </div>
                                    </div>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @php
                                        $div_may_approved =
                                            $division->budget_plan_sum_may +
                                            ($division->transferBudgetTo
                                                ? $division
                                                    ->transferBudgetTo()
                                                    ->where('month_to', 'may')
                                                    ->sum('amount')
                                                : 0) -
                                            ($division->transferBudgetFrom
                                                ? $division
                                                    ->transferBudgetFrom()
                                                    ->where('month_from', 'may')
                                                    ->sum('amount')
                                                : 0);
                                        $div_may_availed = $division->availment
                                            ? $division
                                                ->availment()
                                                ->where('month', 'may')
                                                ->sum('amount')
                                            : 0;
                                        
                                        $div_persent_may_availed = $div_may_availed && $div_may_approved ? ($div_may_availed / $div_may_approved) * 100 : 0;
                                    @endphp
                                    <div class="flex items-center justify-between space-x-3">
                                        <div>
                                            {{ number_format($div_may_approved, 2) }}
                                        </div>
                                        <div>
                                            {{ number_format($div_may_availed, 2) . '(' . round($div_persent_may_availed, 2) . ' %)' }}
                                        </div>
                                        <div>
                                            {{ number_format($div_may_approved - $div_may_availed, 2) . '(' . round(100 - $div_persent_may_availed, 2) . ' %)' }}
                                        </div>
                                    </div>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @php
                                        $div_june_approved =
                                            $division->budget_plan_sum_june +
                                            ($division->transferBudgetTo
                                                ? $division
                                                    ->transferBudgetTo()
                                                    ->where('month_to', 'june')
                                                    ->sum('amount')
                                                : 0) -
                                            ($division->transferBudgetFrom
                                                ? $division
                                                    ->transferBudgetFrom()
                                                    ->where('month_from', 'june')
                                                    ->sum('amount')
                                                : 0);
                                        $div_june_availed = $division->availment
                                            ? $division
                                                ->availment()
                                                ->where('month', 'june')
                                                ->sum('amount')
                                            : 0;
                                        
                                        $div_persent_june_availed = $div_june_availed && $div_june_approved ? ($div_june_availed / $div_june_approved) * 100 : 0;
                                    @endphp
                                    <div class="flex items-center justify-between space-x-3">
                                        <div>
                                            {{ number_format($div_june_approved, 2) }}
                                        </div>
                                        <div>
                                            {{ number_format($div_june_availed, 2) . '(' . round($div_persent_june_availed, 2) . ' %)' }}
                                        </div>
                                        <div>
                                            {{ number_format($div_june_approved - $div_june_availed, 2) . '(' . round(100 - $div_persent_june_availed, 2) . ' %)' }}
                                        </div>
                                    </div>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @php
                                        $div_july_approved =
                                            $division->budget_plan_sum_july +
                                            ($division->transferBudgetTo
                                                ? $division
                                                    ->transferBudgetTo()
                                                    ->where('month_to', 'july')
                                                    ->sum('amount')
                                                : 0) -
                                            ($division->transferBudgetFrom
                                                ? $division
                                                    ->transferBudgetFrom()
                                                    ->where('month_from', 'july')
                                                    ->sum('amount')
                                                : 0);
                                        $div_july_availed = $division->availment
                                            ? $division
                                                ->availment()
                                                ->where('month', 'july')
                                                ->sum('amount')
                                            : 0;
                                        
                                        $div_persent_july_availed = $div_july_availed && $div_july_approved ? ($div_july_availed / $div_july_approved) * 100 : 0;
                                    @endphp
                                    <div class="flex items-center justify-between space-x-3">
                                        <div>
                                            {{ number_format($div_july_approved, 2) }}
                                        </div>
                                        <div>
                                            {{ number_format($div_july_availed, 2) . '(' . round($div_persent_july_availed, 2) . ' %)' }}
                                        </div>
                                        <div>
                                            {{ number_format($div_july_approved - $div_july_availed, 2) . '(' . round(100 - $div_persent_july_availed, 2) . ' %)' }}
                                        </div>
                                    </div>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @php
                                        $div_august_approved =
                                            $division->budget_plan_sum_august +
                                            ($division->transferBudgetTo
                                                ? $division
                                                    ->transferBudgetTo()
                                                    ->where('month_to', 'august')
                                                    ->sum('amount')
                                                : 0) -
                                            ($division->transferBudgetFrom
                                                ? $division
                                                    ->transferBudgetFrom()
                                                    ->where('month_from', 'august')
                                                    ->sum('amount')
                                                : 0);
                                        $div_august_availed = $division->availment
                                            ? $division
                                                ->availment()
                                                ->where('month', 'august')
                                                ->sum('amount')
                                            : 0;
                                        
                                        $div_persent_august_availed = $div_august_availed && $div_august_approved ? ($div_august_availed / $div_august_approved) * 100 : 0;
                                    @endphp
                                    <div class="flex items-center justify-between space-x-3">
                                        <div>
                                            {{ number_format($div_august_approved, 2) }}
                                        </div>
                                        <div>
                                            {{ number_format($div_august_availed, 2) . '(' . round($div_persent_august_availed, 2) . ' %)' }}
                                        </div>
                                        <div>
                                            {{ number_format($div_august_approved - $div_august_availed, 2) . '(' . round(100 - $div_persent_august_availed, 2) . ' %)' }}
                                        </div>
                                    </div>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @php
                                        $div_september_approved =
                                            $division->budget_plan_sum_september +
                                            ($division->transferBudgetTo
                                                ? $division
                                                    ->transferBudgetTo()
                                                    ->where('month_to', 'september')
                                                    ->sum('amount')
                                                : 0) -
                                            ($division->transferBudgetFrom
                                                ? $division
                                                    ->transferBudgetFrom()
                                                    ->where('month_from', 'september')
                                                    ->sum('amount')
                                                : 0);
                                        $div_september_availed = $division->availment
                                            ? $division
                                                ->availment()
                                                ->where('month', 'september')
                                                ->sum('amount')
                                            : 0;
                                        
                                        $div_persent_september_availed = $div_september_availed && $div_september_approved ? ($div_september_availed / $div_september_approved) * 100 : 0;
                                    @endphp
                                    <div class="flex items-center justify-between space-x-3">
                                        <div>
                                            {{ number_format($div_september_approved, 2) }}
                                        </div>
                                        <div>
                                            {{ number_format($div_september_availed, 2) . '(' . round($div_persent_september_availed, 2) . ' %)' }}
                                        </div>
                                        <div>
                                            {{ number_format($div_september_approved - $div_september_availed, 2) . '(' . round(100 - $div_persent_september_availed, 2) . ' %)' }}
                                        </div>
                                    </div>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @php
                                        $div_october_approved =
                                            $division->budget_plan_sum_october +
                                            ($division->transferBudgetTo
                                                ? $division
                                                    ->transferBudgetTo()
                                                    ->where('month_to', 'october')
                                                    ->sum('amount')
                                                : 0) -
                                            ($division->transferBudgetFrom
                                                ? $division
                                                    ->transferBudgetFrom()
                                                    ->where('month_from', 'october')
                                                    ->sum('amount')
                                                : 0);
                                        $div_october_availed = $division->availment
                                            ? $division
                                                ->availment()
                                                ->where('month', 'october')
                                                ->sum('amount')
                                            : 0;
                                        
                                        $div_persent_october_availed = $div_october_availed && $div_october_approved ? ($div_october_availed / $div_october_approved) * 100 : 0;
                                    @endphp
                                    <div class="flex items-center justify-between space-x-3">
                                        <div>
                                            {{ number_format($div_october_approved, 2) }}
                                        </div>
                                        <div>
                                            {{ number_format($div_october_availed, 2) . '(' . round($div_persent_october_availed, 2) . ' %)' }}
                                        </div>
                                        <div>
                                            {{ number_format($div_october_approved - $div_october_availed, 2) . '(' . round(100 - $div_persent_october_availed, 2) . ' %)' }}
                                        </div>
                                    </div>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @php
                                        $div_november_approved =
                                            $division->budget_plan_sum_november +
                                            ($division->transferBudgetTo
                                                ? $division
                                                    ->transferBudgetTo()
                                                    ->where('month_to', 'november')
                                                    ->sum('amount')
                                                : 0) -
                                            ($division->transferBudgetFrom
                                                ? $division
                                                    ->transferBudgetFrom()
                                                    ->where('month_from', 'november')
                                                    ->sum('amount')
                                                : 0);
                                        $div_november_availed = $division->availment
                                            ? $division
                                                ->availment()
                                                ->where('month', 'november')
                                                ->sum('amount')
                                            : 0;
                                        
                                        $div_persent_november_availed = $div_november_availed && $div_november_approved ? ($div_november_availed / $div_november_approved) * 100 : 0;
                                    @endphp
                                    <div class="flex items-center justify-between space-x-3">
                                        <div>
                                            {{ number_format($div_november_approved, 2) }}
                                        </div>
                                        <div>
                                            {{ number_format($div_november_availed, 2) . '(' . round($div_persent_november_availed, 2) . ' %)' }}
                                        </div>
                                        <div>
                                            {{ number_format($div_november_approved - $div_november_availed, 2) . '(' . round(100 - $div_persent_november_availed, 2) . ' %)' }}
                                        </div>
                                    </div>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @php
                                        $div_december_approved =
                                            $division->budget_plan_sum_december +
                                            ($division->transferBudgetTo
                                                ? $division
                                                    ->transferBudgetTo()
                                                    ->where('month_to', 'december')
                                                    ->sum('amount')
                                                : 0) -
                                            ($division->transferBudgetFrom
                                                ? $division
                                                    ->transferBudgetFrom()
                                                    ->where('month_from', 'december')
                                                    ->sum('amount')
                                                : 0);
                                        $div_december_availed = $division->availment
                                            ? $division
                                                ->availment()
                                                ->where('month', 'december')
                                                ->sum('amount')
                                            : 0;
                                        
                                        $div_persent_december_availed = $div_december_availed && $div_december_approved ? ($div_december_availed / $div_december_approved) * 100 : 0;
                                    @endphp
                                    <div class="flex items-center justify-between space-x-3">
                                        <div>
                                            {{ number_format($div_december_approved, 2) }}
                                        </div>
                                        <div>
                                            {{ number_format($div_december_availed, 2) . '(' . round($div_persent_december_availed, 2) . ' %)' }}
                                        </div>
                                        <div>
                                            {{ number_format($div_december_approved - $div_december_availed, 2) . '(' . round(100 - $div_persent_december_availed, 2) . ' %)' }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $divisions->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
