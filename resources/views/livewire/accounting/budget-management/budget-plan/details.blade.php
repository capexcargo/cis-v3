<div x-data="{ view_modal: '{{ $view_modal }}' }">
    @if ($parent_reference_no)
        <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
            <x-slot name="title">View Request</x-slot>
            <x-slot name="body">
                @livewire('accounting.request-management.view', ['parent_reference_no' => $parent_reference_no])
            </x-slot>
        </x-modal>
    @endif
    <div class="flex items-center justify-between">
        <div class="w-32">
            <x-select id="paginate" name="paginate" wire:model="paginate">
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
            </x-select>
        </div>
    </div>
    <x-table.table>
        <x-slot name="thead">
            <x-table.th name="Reference No" />
            <x-table.th name="Division" />
            <x-table.th name="Budget Source" />
            <x-table.th name="Budget Chart" />
            <x-table.th name="Budget Plan" />
            <x-table.th name="Amount" />
            <x-table.th name="Month" />
        </x-slot>
        <x-slot name="tbody">
            @foreach ($budget_availments as $budget_availment)
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        @if ($budget_availment->requestForPayment->parent_reference_no)
                            <p wire:click="action({'parent_reference_no': '{{ $budget_availment->requestForPayment->parent_reference_no }}'}, 'view')"
                                class="underline underline-offset-4 text-blue">
                                {{ $budget_availment->requestForPayment->reference_id }}</p>
                        @else
                            <a href="https://cis2.capex.com.ph/index.php?r=acRfPayment/ViewApprovalDetails&AcRfPayment%5Brfpa_ref_id%5D={{ $budget_availment->requestForPayment->reference_id }}"
                                target="_blank"
                                class="underline underline-offset-4 text-blue">{{ $budget_availment->requestForPayment->reference_id }}</a>
                        @endif
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        {{ $budget_availment->division->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        {{ $budget_availment->source->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        {{ $budget_availment->chart->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        {{ $budget_availment->budgetPlan->item }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        {{ number_format($budget_availment->amount, 2) }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        {{ $budget_availment->month }}
                    </td>
                </tr>
            @endforeach
        </x-slot>
    </x-table.table>
    <div class="px-1 pb-2">
        {{ $budget_availments->links() }}
    </div>
</div>
