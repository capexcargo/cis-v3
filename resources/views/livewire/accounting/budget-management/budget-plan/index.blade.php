<x-form wire:init="load" x-data="{ search_form: false, create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}', details_modal: '{{ $details_modal }}', transfer_budget: '{{ $transfer_budget }}', import_modal: '{{ $import_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('accounting_budget_plan_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                <x-slot name="title">Create Budget Plan</x-slot>
                <x-slot name="body">
                    @livewire('accounting.budget-management.budget-plan.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('accounting_budget_plan_edit')
            @if ($budget_plan_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                    <x-slot name="title">Edit Budget Plan</x-slot>
                    <x-slot name="body">
                        @livewire('accounting.budget-management.budget-plan.edit', ['id' => $budget_plan_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @if ($budget_plan_id)
            <x-modal id="details_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                <x-slot name="title">Budget Plan Details</x-slot>
                <x-slot name="body">
                    @livewire('accounting.budget-management.budget-plan.details', ['id' => $budget_plan_id, 'month' =>
                    $month])
                </x-slot>
            </x-modal>
        @endif
        @can('accounting_budget_plan_transfer')
            @if ($budget_plan_id)
                <x-modal id="transfer_budget" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/2">
                    <x-slot name="title">Transfer Budget Plan</x-slot>
                    <x-slot name="body">
                        @livewire('accounting.budget-management.budget-plan.transfer-budget', ['id' => $budget_plan_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('accounting_budget_plan_import')
            <x-modal id="import_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Import Budget Plan</x-slot>
                <x-slot name="body">
                    <div class="space-y-3">
                        <div>
                            <x-label for="import" value="Import" />
                            <x-input type="file" name="import" wire:model='import'></x-input>
                            <x-input-error for="import" />
                        </div>
                        <div class="flex items-center justify-end">
                            <button type="button" wire:click="import"
                                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Import</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>

           
        @endcan
    </x-slot>
    <x-slot name="header_title">Budget Plan Lists</x-slot>
    <x-slot name="header_button">
        @can('accounting_budget_plan_import')
            <button wire:click="$set('import_modal', true)"
                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Import</button>
        @endcan
        @can('accounting_budget_plan_add')
            <button wire:click="$set('create_modal', true)"
                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Add COA Category</button>
        @endcan
        <button type="button" wire:click="export"
            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Export to Excel</button>
    </x-slot>
    <x-slot name="search_form">
        @if (auth()->user()->level_id == 5)
            <div>
                <x-label for="division" value="Division" />
                <x-select name="division" wire:model="division">
                    <option value=""></option>
                    @foreach ($division_references as $division_reference)
                        <option value="{{ $division_reference->id }}">
                            {{ $division_reference->name }}</option>
                    @endforeach
                </x-select>
                <x-input-error for="division" class="mt-2" />
            </div>
        @endif
        <div>
            <x-label for="budget_source" value="Budget Source" />
            <x-select name="budget_source" wire:model="budget_source">
                <option value=""></option>
                @foreach ($budget_source_references as $budget_source_reference)
                    <option value="{{ $budget_source_reference->id }}">
                        {{ $budget_source_reference->name }}</option>
                @endforeach
            </x-select>
            <x-input-error for="budget_source" class="mt-2" />
        </div>
        <div>
            <x-label for="budget_chart" value="Budget Chart" />
            <x-select name="budget_chart" wire:model="budget_chart">
                <option value=""></option>
                @foreach ($budget_chart_references as $budget_chart_reference)
                    <option value="{{ $budget_chart_reference->id }}">
                        {{ $budget_chart_reference->name }}</option>
                @endforeach
            </x-select>
            <x-input-error for="budget_chart" class="mt-2" />
        </div>
        <div>
            <x-label for="budget_plan" value="Budget Plan" />
            <x-select name="budget_plan" wire:model="budget_plan">
                <option value=""></option>
                @foreach ($budget_plan_references as $budget_plan_reference)
                    <option value="{{ $budget_plan_reference->id }}">
                        {{ $budget_plan_reference->item }}</option>
                @endforeach
            </x-select>
            <x-input-error for="budget_plan" class="mt-2" />
        </div>
        <div>
            <x-label for="year" value="Year" />
            <x-input type="text" name="year" wire:model="year" value="{{ $year }}" />
            <x-input-error for="year" class="mt-2" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="mt-2 bg-white rounded-lg shadow-md">
                <div class="flex flex-col h-screen">
                    <div class="flex-grow overflow-auto rounded-lg">
                        <table class="relative min-w-full font-medium divide-y-2">
                            <thead class="text-xs text-gray-700 capitalize bg-white border-0 cursor-pointer">
                                <tr>
                                    <th
                                        class="text-left sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap">
                                        Division</th>
                                    <th
                                        class="text-left sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap">
                                        Account Code
                                    </th>
                                    <th
                                        class="text-left sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap">
                                        Total Approved
                                    </th>
                                    <th
                                        class="text-left sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap">
                                        Total Availed
                                    </th>
                                    <th
                                        class="text-left sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap">
                                        Total Balance
                                    </th>
                                    <th
                                        class="text-left sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap">
                                        Opex</th>
                                    <th
                                        class="text-left sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[10rem] whitespace-nowrap">
                                        Year</th>

                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        January</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        February</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        March</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        April</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        May
                                    </th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        June
                                    </th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        July
                                    </th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        August</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        September</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        October</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        November</th>
                                    <th
                                        class="sticky top-0 bg-white font-medium p-3 tracking-wider min-w-[30rem] whitespace-nowrap">
                                        December</th>
                                    <th class="sticky top-0 p-3 font-medium bg-white whitespace-nowrap">
                                        Created At</th>
                                    <th class="sticky top-0 p-3 font-medium bg-white whitespace-nowrap">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody class="text-sm text-gray-800 bg-gray-100 border-0 divide-y-4 divide-white">
                                <tr class="text-xs bg-white">
                                    <td class="p-3 whitespace-nowrap"></td>
                                    <td class="p-3 whitespace-nowrap"></td>
                                    <td class="p-3 whitespace-nowrap"></td>
                                    <td class="p-3 whitespace-nowrap"></td>
                                    <td class="p-3 whitespace-nowrap"></td>
                                    <td class="p-3 whitespace-nowrap"></td>
                                    <td class="p-3 whitespace-nowrap"></td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced</div>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex items-center justify-between space-x-3">
                                            <div>Approved</div>
                                            <div>Availed</div>
                                            <div>Balanced </div>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                @foreach ($divisions as $division)
                                    @php
                                        $div_approved = $division->budget_plan_sum_total_amount + $division->transfer_budget_to_sum_amount - $division->transfer_budget_from_sum_amount;
                                        $div_availed = $division->availment_sum_amount;
                                        $div_persent_availed = $div_approved && $div_availed ? ($div_availed / $div_approved) * 100 : 0;
                                    @endphp
                                    <tr
                                        class="border-0 cursor-pointer bg-[#003399] text-white hover:text-white hover:bg-[#4068b8]">
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            Total
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ number_format($div_approved, 2) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ number_format($div_availed, 2) . '(' . round($div_persent_availed, 2) . ' %)' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ number_format($div_approved - $div_availed, 2) . '(' . round(100 - $div_persent_availed, 2) . ' %)' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_january_approved =
                                                    $division->budget_plan_sum_january +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'january')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'january')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0);
                                                $div_january_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'january')
                                                        ->where('year', $year)
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_january_availed = $div_january_availed && $div_january_approved ? ($div_january_availed / $div_january_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_january_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_january_availed, 2) . '(' . round($div_persent_january_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_january_approved - $div_january_availed, 2) . '(' . round(100 - $div_persent_january_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_february_approved =
                                                    $division->budget_plan_sum_february +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'february')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'february')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0);
                                                $div_february_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'february')
                                                        ->where('year', $year)
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_february_availed = $div_february_availed && $div_february_approved ? ($div_february_availed / $div_february_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_february_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_february_availed, 2) . '(' . round($div_persent_february_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_february_approved - $div_february_availed, 2) . '(' . round(100 - $div_persent_february_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_march_approved =
                                                    $division->budget_plan_sum_march +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'march')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'march')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0);
                                                $div_march_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'march')
                                                        ->where('year', $year)
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_march_availed = $div_march_availed && $div_march_approved ? ($div_march_availed / $div_march_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_march_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_march_availed, 2) . '(' . round($div_persent_march_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_march_approved - $div_march_availed, 2) . '(' . round(100 - $div_persent_march_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_april_approved =
                                                    $division->budget_plan_sum_april +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'april')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'april')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0);
                                                $div_april_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'april')
                                                        ->where('year', $year)
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_april_availed = $div_april_availed && $div_april_approved ? ($div_april_availed / $div_april_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_april_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_april_availed, 2) . '(' . round($div_persent_april_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_april_approved - $div_april_availed, 2) . '(' . round(100 - $div_persent_april_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_may_approved =
                                                    $division->budget_plan_sum_may +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'may')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'may')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0);
                                                $div_may_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'may')
                                                        ->where('year', $year)
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_may_availed = $div_may_availed && $div_may_approved ? ($div_may_availed / $div_may_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_may_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_may_availed, 2) . '(' . round($div_persent_may_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_may_approved - $div_may_availed, 2) . '(' . round(100 - $div_persent_may_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_june_approved =
                                                    $division->budget_plan_sum_june +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'june')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'june')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0);
                                                $div_june_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'june')
                                                        ->where('year', $year)
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_june_availed = $div_june_availed && $div_june_approved ? ($div_june_availed / $div_june_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_june_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_june_availed, 2) . '(' . round($div_persent_june_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_june_approved - $div_june_availed, 2) . '(' . round(100 - $div_persent_june_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_july_approved =
                                                    $division->budget_plan_sum_july +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'july')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'july')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0);
                                                $div_july_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'july')
                                                        ->where('year', $year)
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_july_availed = $div_july_availed && $div_july_approved ? ($div_july_availed / $div_july_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_july_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_july_availed, 2) . '(' . round($div_persent_july_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_july_approved - $div_july_availed, 2) . '(' . round(100 - $div_persent_july_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_august_approved =
                                                    $division->budget_plan_sum_august +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'august')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'august')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0);
                                                $div_august_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'august')
                                                        ->where('year', $year)
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_august_availed = $div_august_availed && $div_august_approved ? ($div_august_availed / $div_august_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_august_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_august_availed, 2) . '(' . round($div_persent_august_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_august_approved - $div_august_availed, 2) . '(' . round(100 - $div_persent_august_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_september_approved =
                                                    $division->budget_plan_sum_september +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'september')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'september')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0);
                                                $div_september_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'september')
                                                        ->where('year', $year)
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_september_availed = $div_september_availed && $div_september_approved ? ($div_september_availed / $div_september_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_september_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_september_availed, 2) . '(' . round($div_persent_september_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_september_approved - $div_september_availed, 2) . '(' . round(100 - $div_persent_september_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_october_approved =
                                                    $division->budget_plan_sum_october +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'october')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'october')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0);
                                                $div_october_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'october')
                                                        ->where('year', $year)
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_october_availed = $div_october_availed && $div_october_approved ? ($div_october_availed / $div_october_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_october_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_october_availed, 2) . '(' . round($div_persent_october_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_october_approved - $div_october_availed, 2) . '(' . round(100 - $div_persent_october_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_november_approved =
                                                    $division->budget_plan_sum_november +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'november')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'november')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0);
                                                $div_november_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'november')
                                                        ->where('year', $year)
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_november_availed = $div_november_availed && $div_november_approved ? ($div_november_availed / $div_november_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_november_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_november_availed, 2) . '(' . round($div_persent_november_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_november_approved - $div_november_availed, 2) . '(' . round(100 - $div_persent_november_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @php
                                                $div_december_approved =
                                                    $division->budget_plan_sum_december +
                                                    ($division->transferBudgetTo
                                                        ? $division
                                                            ->transferBudgetTo()
                                                            ->where('month_to', 'december')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0) -
                                                    ($division->transferBudgetFrom
                                                        ? $division
                                                            ->transferBudgetFrom()
                                                            ->where('month_from', 'december')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0);
                                                $div_december_availed = $division->availment
                                                    ? $division
                                                        ->availment()
                                                        ->where('month', 'december')
                                                        ->where('year', $year)
                                                        ->sum('amount')
                                                    : 0;
                                                
                                                $div_persent_december_availed = $div_december_availed && $div_december_approved ? ($div_december_availed / $div_december_approved) * 100 : 0;
                                            @endphp
                                            <div class="flex items-center justify-between space-x-3">
                                                <div>
                                                    {{ number_format($div_december_approved, 2) }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_december_availed, 2) . '(' . round($div_persent_december_availed, 2) . ' %)' }}
                                                </div>
                                                <div>
                                                    {{ number_format($div_december_approved - $div_december_availed, 2) . '(' . round(100 - $div_persent_december_availed, 2) . ' %)' }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                    </tr>
                                    @foreach ($division->budgetSource as $budget_source)
                                        @php
                                            $source_approved = $budget_source->budget_plan_sum_total_amount + $budget_source->transfer_budget_to_sum_amount - $budget_source->transfer_budget_from_sum_amount;
                                            $source_availed = $budget_source->availment_sum_amount;
                                            $source_persent_availed = $source_approved && $source_availed ? ($source_availed / $source_approved) * 100 : 0;
                                        @endphp
                                        <tr
                                            class="border-0 cursor-pointer bg-[#003399] text-white hover:text-white hover:bg-[#4068b8]">
                                            <td class="p-3 whitespace-nowrap">
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $budget_source->name }}
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ number_format($source_approved, 2) }}
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ number_format($source_availed, 2) . '(' . round($source_persent_availed, 2) . ' %)' }}
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ number_format($source_approved - $source_availed, 2) . '(' . round(100 - $source_persent_availed, 2) . ' %)' }}
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_january_approved =
                                                        $budget_source->budget_plan_sum_january +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'january')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'january')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_january_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'january')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_january_availed = $source_january_availed && $source_january_approved ? ($source_january_availed / $source_january_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_january_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_january_availed, 2) . '(' . round($source_persent_january_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_january_approved - $source_january_availed, 2) . '(' . round(100 - $source_persent_january_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_february_approved =
                                                        $budget_source->budget_plan_sum_february +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'february')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'february')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_february_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'february')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_february_availed = $source_february_availed && $source_february_approved ? ($source_february_availed / $source_february_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_february_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_february_availed, 2) . '(' . round($source_persent_february_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_february_approved - $source_february_availed, 2) . '(' . round(100 - $source_persent_february_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_march_approved =
                                                        $budget_source->budget_plan_sum_march +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'march')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'march')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_march_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'march')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_march_availed = $source_march_availed && $source_march_approved ? ($source_march_availed / $source_march_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_march_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_march_availed, 2) . '(' . round($source_persent_march_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_march_approved - $source_march_availed, 2) . '(' . round(100 - $source_persent_march_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_april_approved =
                                                        $budget_source->budget_plan_sum_april +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'april')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'april')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_april_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'april')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_april_availed = $source_april_availed && $source_april_approved ? ($source_april_availed / $source_april_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_april_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_april_availed, 2) . '(' . round($source_persent_april_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_april_approved - $source_april_availed, 2) . '(' . round(100 - $source_persent_april_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_may_approved =
                                                        $budget_source->budget_plan_sum_may +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'may')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'may')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_may_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'may')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_may_availed = $source_may_availed && $source_may_approved ? ($source_may_availed / $source_may_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_may_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_may_availed, 2) . '(' . round($source_persent_may_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_may_approved - $source_may_availed, 2) . '(' . round(100 - $source_persent_may_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_june_approved =
                                                        $budget_source->budget_plan_sum_june +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'june')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'june')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_june_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'june')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_june_availed = $source_june_availed && $source_june_approved ? ($source_june_availed / $source_june_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_june_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_june_availed, 2) . '(' . round($source_persent_june_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_june_approved - $source_june_availed, 2) . '(' . round(100 - $source_persent_june_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_july_approved =
                                                        $budget_source->budget_plan_sum_july +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'july')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'july')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_july_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'july')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_july_availed = $source_july_availed && $source_july_approved ? ($source_july_availed / $source_july_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_july_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_july_availed, 2) . '(' . round($source_persent_july_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_july_approved - $source_july_availed, 2) . '(' . round(100 - $source_persent_july_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_august_approved =
                                                        $budget_source->budget_plan_sum_august +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'august')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'august')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_august_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'august')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_august_availed = $source_august_availed && $source_august_approved ? ($source_august_availed / $source_august_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_august_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_august_availed, 2) . '(' . round($source_persent_august_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_august_approved - $source_august_availed, 2) . '(' . round(100 - $source_persent_august_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_september_approved =
                                                        $budget_source->budget_plan_sum_september +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'september')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'september')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_september_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'september')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_september_availed = $source_september_availed && $source_september_approved ? ($source_september_availed / $source_september_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_september_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_september_availed, 2) . '(' . round($source_persent_september_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_september_approved - $source_september_availed, 2) . '(' . round(100 - $source_persent_september_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_october_approved =
                                                        $budget_source->budget_plan_sum_october +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'october')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'october')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_october_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'october')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_october_availed = $source_october_availed && $source_october_approved ? ($source_october_availed / $source_october_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_october_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_october_availed, 2) . '(' . round($source_persent_october_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_october_approved - $source_october_availed, 2) . '(' . round(100 - $source_persent_october_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_november_approved =
                                                        $budget_source->budget_plan_sum_november +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'november')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'november')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_november_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'november')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_november_availed = $source_november_availed && $source_november_approved ? ($source_november_availed / $source_november_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_november_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_november_availed, 2) . '(' . round($source_persent_november_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_november_approved - $source_november_availed, 2) . '(' . round(100 - $source_persent_november_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @php
                                                    $source_december_approved =
                                                        $budget_source->budget_plan_sum_december +
                                                        ($budget_source->transferBudgetTo
                                                            ? $budget_source
                                                                ->transferBudgetTo()
                                                                ->where('month_to', 'december')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0) -
                                                        ($budget_source->transferBudgetFrom
                                                            ? $budget_source
                                                                ->transferBudgetFrom()
                                                                ->where('month_from', 'december')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0);
                                                    $source_december_availed = $budget_source->availment
                                                        ? $budget_source
                                                            ->availment()
                                                            ->where('month', 'december')
                                                            ->where('year', $year)
                                                            ->sum('amount')
                                                        : 0;
                                                    
                                                    $source_persent_december_availed = $source_december_availed && $source_december_approved ? ($source_december_availed / $source_december_approved) * 100 : 0;
                                                @endphp
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        {{ number_format($source_december_approved, 2) }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_december_availed, 2) . '(' . round($source_persent_december_availed, 2) . ' %)' }}
                                                    </div>
                                                    <div>
                                                        {{ number_format($source_december_approved - $source_december_availed, 2) . '(' . round(100 - $source_persent_december_availed, 2) . ' %)' }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                            </td>
                                        </tr>
                                        @foreach ($budget_source->budgetChart as $budget_chart)
                                            @php
                                                $chart_approved = $budget_chart->budget_plan_sum_total_amount + $budget_chart->transfer_budget_to_sum_amount - $budget_chart->transfer_budget_from_sum_amount;
                                                $chart_availed = $budget_chart->availment_sum_amount;
                                                $chart_persent_availed = $chart_approved && $chart_availed ? ($chart_availed / $chart_approved) * 100 : 0;
                                            @endphp
                                            <tr
                                                class="text-sm  border-0 cursor-pointer bg-[#1656d5] text-white hover:text-white hover:bg-[#4068b8]">
                                                <td class="p-3 whitespace-nowrap">
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    {{ $budget_chart->name }}
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    {{ number_format($chart_approved, 2) }}
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    {{ number_format($chart_availed, 2) . '(' . round($chart_persent_availed, 2) . ' %)' }}
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    {{ number_format($chart_approved - $chart_availed, 2) . '(' . round(100 - $chart_persent_availed, 2) . ' %)' }}
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_january_approved =
                                                            $budget_chart->budget_plan_sum_january +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'january')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'january')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_january_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'january')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_january_availed = $chart_january_availed && $chart_january_approved ? ($chart_january_availed / $chart_january_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_january_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_january_availed, 2) . '(' . round($chart_persent_january_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_january_approved - $chart_january_availed, 2) . '(' . round(100 - $chart_persent_january_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_february_approved =
                                                            $budget_chart->budget_plan_sum_february +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'february')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'february')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_february_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'february')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_february_availed = $chart_february_availed && $chart_february_approved ? ($chart_february_availed / $chart_february_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_february_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_february_availed, 2) . '(' . round($chart_persent_february_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_february_approved - $chart_february_availed, 2) . '(' . round(100 - $chart_persent_february_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_march_approved =
                                                            $budget_chart->budget_plan_sum_march +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'march')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'march')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_march_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'march')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_march_availed = $chart_march_availed && $chart_march_approved ? ($chart_march_availed / $chart_march_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_march_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_march_availed, 2) . '(' . round($chart_persent_march_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_march_approved - $chart_march_availed, 2) . '(' . round(100 - $chart_persent_march_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_april_approved =
                                                            $budget_chart->budget_plan_sum_april +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'april')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'april')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_april_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'april')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_april_availed = $chart_april_availed && $chart_april_approved ? ($chart_april_availed / $chart_april_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_april_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_april_availed, 2) . '(' . round($chart_persent_april_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_april_approved - $chart_april_availed, 2) . '(' . round(100 - $chart_persent_april_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_may_approved =
                                                            $budget_chart->budget_plan_sum_may +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'may')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'may')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_may_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'may')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_may_availed = $chart_may_availed && $chart_may_approved ? ($chart_may_availed / $chart_may_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_may_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_may_availed, 2) . '(' . round($chart_persent_may_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_may_approved - $chart_may_availed, 2) . '(' . round(100 - $chart_persent_may_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_june_approved =
                                                            $budget_chart->budget_plan_sum_june +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'june')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'june')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_june_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'june')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_june_availed = $chart_june_availed && $chart_june_approved ? ($chart_june_availed / $chart_june_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_june_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_june_availed, 2) . '(' . round($chart_persent_june_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_june_approved - $chart_june_availed, 2) . '(' . round(100 - $chart_persent_june_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_july_approved =
                                                            $budget_chart->budget_plan_sum_july +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'july')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'july')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_july_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'july')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_july_availed = $chart_july_availed && $chart_july_approved ? ($chart_july_availed / $chart_july_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_july_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_july_availed, 2) . '(' . round($chart_persent_july_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_july_approved - $chart_july_availed, 2) . '(' . round(100 - $chart_persent_july_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_august_approved =
                                                            $budget_chart->budget_plan_sum_august +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'august')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'august')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_august_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'august')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_august_availed = $chart_august_availed && $chart_august_approved ? ($chart_august_availed / $chart_august_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_august_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_august_availed, 2) . '(' . round($chart_persent_august_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_august_approved - $chart_august_availed, 2) . '(' . round(100 - $chart_persent_august_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_september_approved =
                                                            $budget_chart->budget_plan_sum_september +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'september')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'september')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_september_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'september')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_september_availed = $chart_september_availed && $chart_september_approved ? ($chart_september_availed / $chart_september_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_september_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_september_availed, 2) . '(' . round($chart_persent_september_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_september_approved - $chart_september_availed, 2) . '(' . round(100 - $chart_persent_september_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_october_approved =
                                                            $budget_chart->budget_plan_sum_october +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'october')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'october')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_october_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'october')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_october_availed = $chart_october_availed && $chart_october_approved ? ($chart_october_availed / $chart_october_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_october_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_october_availed, 2) . '(' . round($chart_persent_october_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_october_approved - $chart_october_availed, 2) . '(' . round(100 - $chart_persent_october_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_november_approved =
                                                            $budget_chart->budget_plan_sum_november +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'november')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'november')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_november_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'november')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_november_availed = $chart_november_availed && $chart_november_approved ? ($chart_november_availed / $chart_november_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_november_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_november_availed, 2) . '(' . round($chart_persent_november_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_november_approved - $chart_november_availed, 2) . '(' . round(100 - $chart_persent_november_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    @php
                                                        $chart_december_approved =
                                                            $budget_chart->budget_plan_sum_december +
                                                            ($budget_chart->transferBudgetTo
                                                                ? $budget_chart
                                                                    ->transferBudgetTo()
                                                                    ->where('month_to', 'december')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0) -
                                                            ($budget_chart->transferBudgetFrom
                                                                ? $budget_chart
                                                                    ->transferBudgetFrom()
                                                                    ->where('month_from', 'december')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0);
                                                        $chart_december_availed = $budget_chart->availment
                                                            ? $budget_chart
                                                                ->availment()
                                                                ->where('month', 'december')
                                                                ->where('year', $year)
                                                                ->sum('amount')
                                                            : 0;
                                                        
                                                        $chart_persent_december_availed = $chart_december_availed && $chart_december_approved ? ($chart_december_availed / $chart_december_approved) * 100 : 0;
                                                    @endphp
                                                    <div class="flex items-center justify-between space-x-3">
                                                        <div>
                                                            {{ number_format($chart_december_approved, 2) }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_december_availed, 2) . '(' . round($chart_persent_december_availed, 2) . ' %)' }}
                                                        </div>
                                                        <div>
                                                            {{ number_format($chart_december_approved - $chart_december_availed, 2) . '(' . round(100 - $chart_persent_december_availed, 2) . ' %)' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                </td>
                                            </tr>
                                            @foreach ($budget_chart->budgetPlan as $budget_plan)
                                                @php
                                                    $plan_approved = $budget_plan->total_amount + $budget_plan->transfer_budget_to_sum_amount - $budget_plan->transfer_budget_from_sum_amount;
                                                    $plan_availed = $budget_plan->availment_sum_amount;
                                                    $plan_persent_availed = $plan_approved && $plan_availed ? ($plan_availed / $plan_approved) * 100 : 0;
                                                @endphp
                                                @if($budget_plan->year == $this->year)
                                                <tr
                                                    class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ $division->name }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ $budget_plan->item }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ number_format($plan_approved, 2) }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ number_format($plan_availed, 2) . '(' . round($plan_persent_availed, 2) . ' %)' }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ number_format($plan_approved - $plan_availed, 2) . '(' . round(100 - $plan_persent_availed, 2) . ' %)' }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ $budget_plan->opexType ? $budget_plan->opexType->name : '' }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ $budget_plan->year }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'january'}, 'details')">
                                                        @php
                                                            $plan_january_approved =
                                                                $budget_plan->january +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'january')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'january')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_january_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'january')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0;
                                                            
                                                            $plan_persent_january_availed = $plan_january_availed && $plan_january_approved ? ($plan_january_availed / $plan_january_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_january_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_january_availed, 2) . '(' . round($plan_persent_january_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_january_approved - $plan_january_availed, 2) . '(' . round(100 - $plan_persent_january_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'february'}, 'details')">
                                                        @php
                                                            $plan_february_approved =
                                                                $budget_plan->february +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'february')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'february')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_february_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'february')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_february_availed = $plan_february_availed && $plan_february_approved ? ($plan_february_availed / $plan_february_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_february_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_february_availed, 2) . '(' . round($plan_persent_february_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_february_approved - $plan_february_availed, 2) . '(' . round(100 - $plan_persent_february_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'march'}, 'details')">
                                                        @php
                                                            $plan_march_approved =
                                                                $budget_plan->march +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'march')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'march')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_march_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'march')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_march_availed = $plan_march_availed && $plan_march_approved ? ($plan_march_availed / $plan_march_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_march_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_march_availed, 2) . '(' . round($plan_persent_march_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_march_approved - $plan_march_availed, 2) . '(' . round(100 - $plan_persent_march_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'april'}, 'details')">
                                                        @php
                                                            $plan_april_approved =
                                                                $budget_plan->april +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'april')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'april')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_april_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'april')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_april_availed = $plan_april_availed && $plan_april_approved ? ($plan_april_availed / $plan_april_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_april_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_april_availed, 2) . '(' . round($plan_persent_april_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_april_approved - $plan_april_availed, 2) . '(' . round(100 - $plan_persent_april_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'may'}, 'details')">
                                                        @php
                                                            $plan_may_approved =
                                                                $budget_plan->may +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'may')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'may')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_may_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'may')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_may_availed = $plan_may_availed && $plan_may_approved ? ($plan_may_availed / $plan_may_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_may_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_may_availed, 2) . '(' . round($plan_persent_may_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_may_approved - $plan_may_availed, 2) . '(' . round(100 - $plan_persent_may_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'june'}, 'details')">
                                                        @php
                                                            $plan_june_approved =
                                                                $budget_plan->june +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'june')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'june')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_june_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'june')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_june_availed = $plan_june_availed && $plan_june_approved ? ($plan_june_availed / $plan_june_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_june_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_june_availed, 2) . '(' . round($plan_persent_june_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_june_approved - $plan_june_availed, 2) . '(' . round(100 - $plan_persent_june_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'july'}, 'details')">
                                                        @php
                                                            $plan_july_approved =
                                                                $budget_plan->july +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'july')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'july')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_july_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'july')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_july_availed = $plan_july_availed && $plan_july_approved ? ($plan_july_availed / $plan_july_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_july_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_july_availed, 2) . '(' . round($plan_persent_july_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_july_approved - $plan_july_availed, 2) . '(' . round(100 - $plan_persent_july_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'august'}, 'details')">
                                                        @php
                                                            $plan_august_approved =
                                                                $budget_plan->august +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'august')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'august')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_august_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'august')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_august_availed = $plan_august_availed && $plan_august_approved ? ($plan_august_availed / $plan_august_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_august_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_august_availed, 2) . '(' . round($plan_persent_august_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_august_approved - $plan_august_availed, 2) . '(' . round(100 - $plan_persent_august_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'september'}, 'details')">
                                                        @php
                                                            $plan_september_approved =
                                                                $budget_plan->september +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'september')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'september')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_september_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'september')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_september_availed = $plan_september_availed && $plan_september_approved ? ($plan_september_availed / $plan_september_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_september_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_september_availed, 2) . '(' . round($plan_persent_september_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_september_approved - $plan_september_availed, 2) . '(' . round(100 - $plan_persent_september_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'october'}, 'details')">
                                                        @php
                                                            $plan_october_approved =
                                                                $budget_plan->october +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'october')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'october')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_october_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'october')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_october_availed = $plan_october_availed && $plan_october_approved ? ($plan_october_availed / $plan_october_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_october_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_october_availed, 2) . '(' . round($plan_persent_october_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_october_approved - $plan_october_availed, 2) . '(' . round(100 - $plan_persent_october_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'november'}, 'details')">
                                                        @php
                                                            $plan_november_approved =
                                                                $budget_plan->november +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'november')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'november')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_november_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'november')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_november_availed = $plan_november_availed && $plan_november_approved ? ($plan_november_availed / $plan_november_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_november_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_november_availed, 2) . '(' . round($plan_persent_november_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_november_approved - $plan_november_availed, 2) . '(' . round(100 - $plan_persent_november_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap"
                                                        wire:click="action({'id': {{ $budget_plan->id }}, 'month': 'december'}, 'details')">
                                                        @php
                                                            $plan_december_approved =
                                                                $budget_plan->december +
                                                                ($budget_plan->transferBudgetTo
                                                                    ? $budget_plan
                                                                        ->transferBudgetTo()
                                                                        ->where('month_to', 'december')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0) -
                                                                ($budget_plan->transferBudgetFrom
                                                                    ? $budget_plan
                                                                        ->transferBudgetFrom()
                                                                        ->where('month_from', 'december')
                                                                        ->where('year', $year)
                                                                        ->sum('amount')
                                                                    : 0);
                                                            $plan_december_availed = $budget_plan->availment
                                                                ? $budget_plan
                                                                    ->availment()
                                                                    ->where('month', 'december')
                                                                    ->where('year', $year)
                                                                    ->sum('amount')
                                                                : 0;
                                                            $plan_persent_december_availed = $plan_december_availed && $plan_december_approved ? ($plan_december_availed / $plan_december_approved) * 100 : 0;
                                                        @endphp
                                                        <div class="flex items-center justify-between space-x-3">
                                                            <div>
                                                                {{ number_format($plan_december_approved, 2) }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_december_availed, 2) . '(' . round($plan_persent_december_availed, 2) . ' %)' }}
                                                            </div>
                                                            <div>
                                                                {{ number_format($plan_december_approved - $plan_december_availed, 2) . '(' . round(100 - $plan_persent_december_availed, 2) . ' %)' }}
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ date('M/d/Y', strtotime($budget_plan->created_at)) }}
                                                    </td>
                                                    @can('accounting_budget_plan_action')
                                                        <td class="p-3 whitespace-nowrap">
                                                            <div class="flex space-x-3">
                                                                @can('accounting_budget_plan_edit')
                                                                    <svg wire:click="action({'id': {{ $budget_plan->id }}}, 'edit')"
                                                                        class="w-5 h-5 text-blue" aria-hidden="true"
                                                                        focusable="false" data-prefix="far" data-icon="edit"
                                                                        role="img" xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 576 512">
                                                                        <path fill="currentColor"
                                                                            d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                                        </path>
                                                                    </svg>
                                                                @endcan
                                                                @can('accounting_budget_plan_transfer')
                                                                    <svg wire:click="action({'id': {{ $budget_plan->id }}}, 'transfer_budget')"
                                                                        class="w-5 h-5 text-blue" aria-hidden="true"
                                                                        focusable="false" data-prefix="fas"
                                                                        data-icon="hand-holding-usd" role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 576 512">
                                                                        <path fill="currentColor"
                                                                            d="M271.06,144.3l54.27,14.3a8.59,8.59,0,0,1,6.63,8.1c0,4.6-4.09,8.4-9.12,8.4h-35.6a30,30,0,0,1-11.19-2.2c-5.24-2.2-11.28-1.7-15.3,2l-19,17.5a11.68,11.68,0,0,0-2.25,2.66,11.42,11.42,0,0,0,3.88,15.74,83.77,83.77,0,0,0,34.51,11.5V240c0,8.8,7.83,16,17.37,16h17.37c9.55,0,17.38-7.2,17.38-16V222.4c32.93-3.6,57.84-31,53.5-63-3.15-23-22.46-41.3-46.56-47.7L282.68,97.4a8.59,8.59,0,0,1-6.63-8.1c0-4.6,4.09-8.4,9.12-8.4h35.6A30,30,0,0,1,332,83.1c5.23,2.2,11.28,1.7,15.3-2l19-17.5A11.31,11.31,0,0,0,368.47,61a11.43,11.43,0,0,0-3.84-15.78,83.82,83.82,0,0,0-34.52-11.5V16c0-8.8-7.82-16-17.37-16H295.37C285.82,0,278,7.2,278,16V33.6c-32.89,3.6-57.85,31-53.51,63C227.63,119.6,247,137.9,271.06,144.3ZM565.27,328.1c-11.8-10.7-30.2-10-42.6,0L430.27,402a63.64,63.64,0,0,1-40,14H272a16,16,0,0,1,0-32h78.29c15.9,0,30.71-10.9,33.25-26.6a31.2,31.2,0,0,0,.46-5.46A32,32,0,0,0,352,320H192a117.66,117.66,0,0,0-74.1,26.29L71.4,384H16A16,16,0,0,0,0,400v96a16,16,0,0,0,16,16H372.77a64,64,0,0,0,40-14L564,377a32,32,0,0,0,1.28-48.9Z">
                                                                        </path>
                                                                    </svg>
                                                                @endcan
                                                            </div>
                                                        </td>
                                                    @endcan
                                                </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
