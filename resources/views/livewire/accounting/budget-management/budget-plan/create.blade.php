<div wire:init="load">
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-3 gap-3">
                <div>
                    <x-label for="item" value="Item" :required="true" />
                    <x-input type="text" name="item" wire:model.defer='item'></x-input>
                    <x-input-error for="item" />
                </div>
                <div>
                    <x-label for="year" value="Year" :required="true" />
                    <x-input type="number" name="year" wire:model.defer='year'></x-input>
                    <x-input-error for="year" />
                </div>
                <div>
                    <x-label for="opex_type" value="Opex Type" :required="true" />
                    <x-select name="opex_type" wire:model.defer='opex_type'>
                        <option value=""></option>
                        @foreach ($opex_type_references as $opex_type_reference)
                            <option value="{{ $opex_type_reference->id }}">{{ $opex_type_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="opex_type" />
                </div>
                <div>
                    <x-label for="division" value="Division" :required="true" />
                    <x-select name="division" wire:model='division'>
                        <option value=""></option>
                        @foreach ($division_references as $division_reference)
                            <option value="{{ $division_reference->id }}">{{ $division_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="division" />
                </div>
                <div>
                    <x-label for="source" value="Source" :required="true" />
                    <x-select name="source" wire:model='source'>
                        <option value=""></option>
                        @foreach ($source_references as $source_reference)
                            <option value="{{ $source_reference->id }}">{{ $source_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="source" />
                </div>
                <div>
                    <x-label for="chart" value="Chart" :required="true" />
                    <x-select name="chart" wire:model.defer='chart'>
                        <option value=""></option>
                        @foreach ($chart_references as $chart_reference)
                            <option value="{{ $chart_reference->id }}">{{ $chart_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="chart" />
                </div>
                <div>
                    <x-label for="request_type" value="Request Type" :required="true" />
                    <x-select name="request_type" wire:model.defer='request_type'>
                        <option value=""></option>
                        @foreach ($request_for_pament_type_references as $request_for_pament_type_reference)
                            <option value="{{ $request_for_pament_type_reference->id }}">
                                {{ $request_for_pament_type_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="request_type" />
                </div>
            </div>
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Budget Months</span>
                </div>
            </div>
            <div class="grid grid-cols-4 gap-3">
                <div>
                    <x-label for="january" value="January" :required="true" />
                    <x-input type="number" step="0.01" name="january" wire:model.defer='january'></x-input>
                    <x-input-error for="january" />
                </div>
                <div>
                    <x-label for="february" value="February" :required="true" />
                    <x-input type="number" step="0.01" name="february" wire:model.defer='february'></x-input>
                    <x-input-error for="february" />
                </div>
                <div>
                    <x-label for="march" value="March" :required="true" />
                    <x-input type="number" step="0.01" name="march" wire:model.defer='march'></x-input>
                    <x-input-error for="march" />
                </div>
                <div>
                    <x-label for="april" value="April" :required="true" />
                    <x-input type="number" step="0.01" name="april" wire:model.defer='april'></x-input>
                    <x-input-error for="april" />
                </div>
                <div>
                    <x-label for="may" value="May" :required="true" />
                    <x-input type="number" step="0.01" name="may" wire:model.defer='may'></x-input>
                    <x-input-error for="may" />
                </div>
                <div>
                    <x-label for="june" value="June" :required="true" />
                    <x-input type="number" step="0.01" name="june" wire:model.defer='june'></x-input>
                    <x-input-error for="june" />
                </div>
                <div>
                    <x-label for="july" value="July" :required="true" />
                    <x-input type="number" step="0.01" name="july" wire:model.defer='july'></x-input>
                    <x-input-error for="july" />
                </div>
                <div>
                    <x-label for="august" value="August" :required="true" />
                    <x-input type="number" step="0.01" name="august" wire:model.defer='august'></x-input>
                    <x-input-error for="august" />
                </div>
                <div>
                    <x-label for="september" value="September" :required="true" />
                    <x-input type="number" step="0.01" name="september" wire:model.defer='september'></x-input>
                    <x-input-error for="september" />
                </div>
                <div>
                    <x-label for="october" value="October" :required="true" />
                    <x-input type="number" step="0.01" name="october" wire:model.defer='october'></x-input>
                    <x-input-error for="october" />
                </div>
                <div>
                    <x-label for="november" value="November" :required="true" />
                    <x-input type="number" step="0.01" name="november" wire:model.defer='november'></x-input>
                    <x-input-error for="november" />
                </div>
                <div>
                    <x-label for="december" value="December" :required="true" />
                    <x-input type="number" step="0.01" name="december" wire:model.defer='december'></x-input>
                    <x-input-error for="december" />
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Create</button>
            </div>
        </div>
    </form>
</div>
