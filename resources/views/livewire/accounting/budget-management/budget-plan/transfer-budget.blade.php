<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <hr class="space-y-3" wire:init="load">
        <div class="grid grid-cols-1 gap-3">
            <div wire:init="loadTransferTypeReference">
                <x-label for="transfer_type" value="Transfer Type" :required="true" />
                <x-select name="transfer_type" wire:model='transfer_type'>
                    <option value=""></option>
                    @foreach ($transfer_type_references as $transfer_type_reference)
                        @if (auth()->user()->level_id == 5)
                            <option value="{{ $transfer_type_reference->id }}">
                                {{ $transfer_type_reference->display }}
                            </option>
                        @else
                            @if ($transfer_type_reference->id == 2)
                                <option value="{{ $transfer_type_reference->id }}">
                                    {{ $transfer_type_reference->display }}
                                </option>
                            @endif
                        @endif
                    @endforeach
                </x-select>
                <x-input-error for="transfer_type" />
            </div>
        </div>
        @if ($transfer_type == 2)
            {{-- <br>
            <div class="grid grid-cols-3 gap-3">
                <div>
                    <div class="text-xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>APPROVED : {{ number_format($approvedbudgetplan, 2) }}</span>
                        </div>
                    </div>
                </div>
                
                <div>
                    <div class="text-xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            <span>AVAILED : {{ number_format($availedbudgetplan, 2) }}</span>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="text-xl font-semibold text-gray-800">
                        <div class="flex items-center space-x-3">
                            @if ($balancebudgetplan < 0)
                                <span>BALANCE : <span style="color:red">{{ number_format($balancebudgetplan, 2) }}</span></span>
                            @else
                                <span>BALANCE : <span style="color:green">{{ number_format($balancebudgetplan, 2) }}</span></span>
                            @endif
                        </div>
                    </div>
                </div>
            </div><br>
            <hr style="border: 1px solid grey">
            </hr> --}}
            <br>
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Budget Plan From</span>
                </div>
            </div>
            <div class="grid grid-cols-3 gap-3">
                <div wire:init="loadDivisionReference">
                    <x-label for="division_from" value="Division" />
                    <select name="division_from" wire:model='division_from'
                        {{ auth()->user()->level_id == 5 ? '' : 'disabled' }}
                        class="text-gray-600 block w-full mt-1 border border-gray-300 p-[2px] rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 @error('chart_from') border-red-600 @enderror">
                        >
                        <option value=""></option>
                        @foreach ($division_references as $division_reference)
                            <option value="{{ $division_reference->id }}">{{ $division_reference->name }}
                            </option>
                        @endforeach
                    </select>
                    <x-input-error for="division_from" />
                </div>
                <div>
                    <x-label for="source_from" value="Source" />
                    <select name="source_from" wire:model='source_from'
                        {{ auth()->user()->level_id == 5 ? '' : 'disabled' }}
                        class="text-gray-600 block w-full mt-1 border border-gray-300 p-[2px] rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 @error('chart_from') border-red-600 @enderror">

                        >
                        <option value=""></option>
                        @foreach ($budget_source_references as $budget_source_reference)
                            <option value="{{ $budget_source_reference->id }}">
                                {{ $budget_source_reference->name }}
                            </option>
                        @endforeach
                    </select>
                    <x-input-error for="source_from" />
                </div>
                <div>
                    <x-label for="chart_from" value="Chart" />
                    <select name="chart_from" wire:model='chart_from'
                        {{ auth()->user()->level_id == 5 ? '' : 'disabled' }}
                        class="text-gray-600 block w-full mt-1 border border-gray-300 p-[2px] rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 @error('chart_from') border-red-600 @enderror">
                        <option value=""></option>
                        @foreach ($budget_chart_references as $budget_chart_reference)
                            <option value="{{ $budget_chart_reference->id }}">
                                {{ $budget_chart_reference->name }}
                            </option>
                        @endforeach
                    </select>
                    <x-input-error for="chart_from" />
                </div>
                <div>
                    <x-label for="budget_plan_from" value="Budget Plan" :required="true" />
                    <x-select name="budget_plan_from" wire:model='budget_plan_from'>
                        <option value=""></option>
                        @foreach ($budget_plan_references as $budget_plan_reference)
                            <option value="{{ $budget_plan_reference->id }}">
                                {{ $budget_plan_reference->item }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="budget_plan_from" />
                </div>
                <div>
                    <x-label for="month_from" value="Month" :required="true" />
                    <x-select name="month_from" wire:model='month_from'>
                        <option value=""></option>
                        @foreach ($month_references as $month_reference)
                            @if ($year == date('Y'))
                                @if (intval(date('m')) >= $month_reference->id)
                                    <option value="{{ $month_reference->code }}">{{ $month_reference->display }}
                                    </option>
                                @endif
                            @else
                                <option value="{{ $month_reference->code }}">{{ $month_reference->display }}
                                </option>
                            @endif
                        @endforeach
                    </x-select>
                    <x-input-error for="month_from" />
                </div>
                <div>
                    <x-label for="available_amount" value="Available Amount" :required="true" />
                    <x-input type="number" name="available_amount" value="{{ $available_amount }}" disabled>
                    </x-input>
                    <x-input-error for="available_amount" />
                </div>
            </div>
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Budget Plan To</span>
                </div>
            </div>
        @endif
        <div class="grid grid-cols-3 gap-3">
            <div>
                <x-label for="division_to" value="Division" />
                <x-input type="text" name="division_to" value="{{ $division_to }}" disabled></x-input>
                <x-input-error for="division_to" />
            </div>
            <div>
                <x-label for="source_to" value="Source" />
                <x-input type="text" name="source_to" value="{{ $source_to }}" disabled></x-input>
                <x-input-error for="source_to" />
            </div>
            <div>
                <x-label for="chart_to" value="Chart" />
                <x-input type="text" name="chart_to" value="{{ $chart_to }}" disabled></x-input>
                <x-input-error for="chart_to" />
            </div>
            <div>
                <x-label for="budget_plan_to" value="Budget Plan" />
                <x-input type="text" name="budget_plan_to" value="{{ $budget_plan_to }}" disabled></x-input>
                <x-input-error for="budget_plan_to" />
            </div>
            <div>
                <x-label for="month_to" value="Month" :required="true" />
                <x-select name="month_to" wire:model.defer='month_to'>
                    <option value=""></option>
                    @foreach ($month_references as $month_reference)
                        <option value="{{ $month_reference->code }}">{{ $month_reference->display }}
                        </option>
                    @endforeach
                </x-select>
                <x-input-error for="month_to" />
            </div>
            <div>
                <x-label for="amount" value="Amount" :required="true" />
                <x-input type="number" name="amount" step="0.01" wire:model.defer='amount'></x-input>
                <x-input-error for="amount" />
            </div>
        </div>


        <div class="flex justify-end space-x-3">
            <button type="button" wire:click="$emit('close_modal', 'transfer_budget')"
                class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
            <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                Submit</button>
        </div>
</div>
</form>
</div>
