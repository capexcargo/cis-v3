<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div wire:init="loadDivisionReference">
                    <x-label for="division" value="Division" />
                    <x-select name="division" wire:model.defer='division'>
                        <option value=""></option>
                        @foreach ($division_references as $division_reference)
                            <option value="{{ $division_reference->id }}">{{ $division_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="division" />
                </div>
                <div>
                    <x-label for="name" value="Name" />
                    <x-input type="text" name="name" wire:model.defer='name'></x-input>
                    <x-input-error for="name" />
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Update</button>
            </div>
        </div>
    </form>
</div>
