<div wire:init="load">
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-2 gap-3">
                <div>
                    <x-label for="division" value="Division" :required="true" />
                    <x-select name="division" wire:model='division'>
                        <option value=""></option>
                        @foreach ($division_references as $division_reference)
                            <option value="{{ $division_reference->id }}">{{ $division_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="division" />
                </div>
                <div>
                    <x-label for="budget_source" value="Budget Source" :required="true" />
                    <x-select name="budget_source" wire:model='budget_source'>
                        <option value=""></option>
                        @foreach ($budget_source_references as $budget_source_reference)
                            <option value="{{ $budget_source_reference->id }}">{{ $budget_source_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="budget_source" />
                </div>
                <div>
                    <x-label for="budget_chart" value="Budget Chart" :required="true" />
                    <x-select name="budget_chart" wire:model='budget_chart'>
                        <option value=""></option>
                        @foreach ($budget_chart_references as $budget_chart_reference)
                            <option value="{{ $budget_chart_reference->id }}">{{ $budget_chart_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="budget_chart" />
                </div>
                <div>
                    <x-label for="budget_plan" value="Budget Plan" :required="true" />
                    <x-select name="budget_plan" wire:model='budget_plan'>
                        <option value=""></option>
                        @foreach ($budget_plan_references as $budget_plan_reference)
                            @if ($budget_plan_reference->budget_loa_count < 3)
                                <option value="{{ $budget_plan_reference->id }}">{{ $budget_plan_reference->item }}
                                </option>
                            @endif
                        @endforeach
                    </x-select>
                    <x-input-error for="budget_plan" />
                </div>
                <div>
                    <x-label for="limit_min_amount" value="Limit Min Amount" :required="true" />
                    <x-input type="number" step="0.01" name="limit_min_amount" wire:model.defer='limit_min_amount'
                        disabled></x-input>
                    <x-input-error for="limit_min_amount" />
                </div>
                <div>
                    <x-label for="limit_max_amount" value="Limit Max Amount" :required="true" />
                    <x-input type="number" step="0.01" name="limit_max_amount" wire:model.defer='limit_max_amount'>
                    </x-input>
                    <x-input-error for="limit_max_amount" />
                </div>
            </div>
            <div class="grid grid-cols-3 gap-3">
                <div x-data="{open: false }" class="relative mb-2 rounded-md" @click.away="open = false">
                    <div>
                        <x-label for="first_approver_search" value="First Approver" :required="true" />
                        <x-input type="text" @click="open = !open" name="first_approver"
                            wire:model="first_approver_search" />
                        <x-input-error for="first_approver_search" />
                        <x-input-error for="first_approver" />
                    </div>
                    <div x-show="open" x-cloak
                        class="absolute w-full p-2 my-1 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96">
                        <ul class="list-reset">
                            @forelse ($first_approver_references as $i => $first_approver_reference)
                                <li @click="open = !open" wire:click="getApproverName({{ $first_approver_reference->id }},1)"
                                    wire:key="{{ 'first_approver'.$i }}"
                                    class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                    <p>
                                        {{ $first_approver_reference->name }}
                                    </p>
                                </li>
                            @empty
                                <li>
                                    <p class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                        No User Found.
                                    </p>
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
                <div x-data="{open: false }" class="relative mb-2 rounded-md" @click.away="open = false">
                    <div>
                        <x-label for="second_approver_search" value="Second Approver" />
                        <x-input type="text" @click="open = !open" name="second_approver"
                            wire:model="second_approver_search" />
                        <x-input-error for="second_approver_search" />
                        <x-input-error for="second_approver" />
                    </div>
                    <div x-show="open" x-cloak
                        class="absolute w-full p-2 my-1 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96">
                        <ul class="list-reset">
                            @forelse ($second_approver_references as $i => $second_approver_reference)
                                <li @click="open = !open" wire:click="getApproverName({{ $second_approver_reference->id }}, 2)"
                                    wire:key="{{ 'second_approver'.$i }}"
                                    class="block p-2 text-black cursor-pointer hover:bg-gray-200">
                                    <p>
                                        {{ $second_approver_reference->name }}
                                    </p>
                                </li>
                            @empty
                                <li>
                                    <p class="block p-2 text-black cursor-pointer hover:bg-gray-200">
                                        No User Found.
                                    </p>
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
                <div x-data="{open: false }" class="relative mb-2 rounded-md" @click.away="open = false">
                    <div>
                        <x-label for="third_approver_search" value="Third Approver" />
                        <x-input type="text" @click="open = !open" name="third_approver"
                            wire:model="third_approver_search" />
                        <x-input-error for="third_approver_search" />
                        <x-input-error for="third_approver" />
                    </div>
                    <div x-show="open" x-cloak
                        class="absolute w-full p-2 my-1 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96">
                        <ul class="list-reset">
                            @forelse ($third_approver_references as $i => $third_approver_reference)
                                <li  @click="open = !open" wire:click="getApproverName({{ $third_approver_reference->id }}, 3)"
                                    wire:key="{{ 'third_approver'.$i }}"
                                    class="block p-2 text-black cursor-pointer hover:bg-gray-200">
                                    <p>
                                        {{ $third_approver_reference->name }}
                                    </p>
                                </li>
                            @empty
                                <li>
                                    <p class="block p-2 text-black cursor-pointer hover:bg-gray-200">
                                        No User Found.
                                    </p>
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Create</button>
            </div>
        </div>
    </form>
</div>
