<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="name" value="Name" />
                    <x-input type="text" name="name" wire:model.defer='name'></x-input>
                    <x-input-error for="name" />
                </div>
                <div>
                    <x-label for="description" value="Description" />
                    <x-input type="text" name="description" wire:model.defer='description'></x-input>
                    <x-input-error for="description" />
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-6 py-2 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-md hover:text-white hover:bg-red-400">Cancel</button>
                <button type="submit" class="px-6 py-2 text-sm flex-none bg-blue text-white rounded-md">
                    Submit</button>
            </div>
        </div>
    </form>
</div>
