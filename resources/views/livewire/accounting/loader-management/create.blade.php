<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div wire:init="loaderTypeReference">
                    <x-label for="loaders_type_id" value="Loader Type" :required="true" />
                    <x-select name="loaders_type_id" wire:model='loaders_type_id'>
                        <option value=""></option>
                        @foreach ($loader_type_references as $loader_type_reference)
                                    <option value="{{ $loader_type_reference->id }}">{{ $loader_type_reference->display }}
                                    </option>
                                @endforeach
                    </x-select>
                    <x-input-error for="loaders_type_id" />
                </div>
            </div>
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="name" value="Loader Name" />
                    <x-input type="text" name="name" wire:model.defer='name'></x-input>
                    <x-input-error for="name" />
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Create</button>
            </div>
        </div>
    </form>
</div>
