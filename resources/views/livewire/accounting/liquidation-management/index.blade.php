<x-form wire:init="load" x-data="{ search_form: false, confirmation_modal: '{{ $confirmation_modal }}', create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}', view_modal: '{{ $view_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                {{ $confirmation_message }}
                <div class="flex justify-end space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click="updateLiquidationStatus"
                        class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>
        @can('accounting_liquidation_management_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                <x-slot name="title">Create Liquidation</x-slot>
                <x-slot name="body">
                    @livewire('accounting.liquidation-management.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('accounting_liquidation_management_edit')
            @if ($liquidation_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                    <x-slot name="title">Edit Liquidation</x-slot>
                    <x-slot name="body">
                        @livewire('accounting.liquidation-management.edit', ['id' => $liquidation_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @if ($parent_reference_no)
            <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                <x-slot name="title">View Request</x-slot>
                <x-slot name="body">
                    @livewire('accounting.request-management.view', ['parent_reference_no' => $parent_reference_no])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">Liquidation List</x-slot>
    <x-slot name="header_button">
        @can('accounting_liquidation_management_add')
            <button wire:click="action({}, 'create')"
                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Create Liquidation</button>
        @endcan
    </x-slot>
    <x-slot name="search_form">
        <div>
            <x-label for="division" value="Division" />
            <x-select name="division" wire:model='division'>
                <option value=""></option>
                @foreach ($division_references as $division_reference)
                    <option value="{{ $division_reference->id }}">
                        {{ $division_reference->name }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="division" class="mt-2" />
        </div>
        <div>
            <x-label for="rfp_reference_no" value="RFP Reference No." />
            <x-input type="text" name="rfp_reference_no" wire:model.debounce.500ms="rfp_reference_no" />
            <x-input-error for="rfp_reference_no" class="mt-2" />
        </div>
        <div>
            <x-label for="cv_no" value="CV No." />
            <x-input type="text" name="cv_no" wire:model.debounce.500ms="cv_no" />
            <x-input-error for="cv_no" class="mt-2" />
        </div>
        <div>
            <x-label for="liquidation_reference_no" value="Liquidation Reference No." />
            <x-input type="text" name="liquidation_reference_no" wire:model.debounce.500ms="liquidation_reference_no" />
            <x-input-error for="liquidation_reference_no" class="mt-2" />
        </div>
        <div>
            <x-label for="payee" value="Payee" />
            <x-input type="text" name="payee" wire:model.debounce.500ms="payee" />
            <x-input-error for="payee" class="mt-2" />
        </div>
        <div>
            <x-label for="opex_type" value="Opex Type" />
            <x-select name="opex_type" wire:model='opex_type'>
                <option value=""></option>
                @foreach ($opex_type_references as $opex_type_reference)
                    <option value="{{ $opex_type_reference->id }}">
                        {{ $opex_type_reference->name }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="opex_type" class="mt-2" />
        </div>
        <div>
            <x-label for="rfp_requester" value="RFP Requester" />
            <x-input type="text" name="rfp_requester" wire:model.debounce.500ms="rfp_requester" />
            <x-input-error for="rfp_requester" class="mt-2" />
        </div>
        <div>
            <x-label for="liquidated_by" value="Liquidated By" />
            <x-input type="text" name="liquidated_by" wire:model.debounce.500ms="liquidated_by" />
            <x-input-error for="liquidated_by" class="mt-2" />
        </div>
        <div>
            <x-label for="status" value="Status" />
            <x-select name="status" wire:model='status'>
                <option value=""></option>
                @foreach ($liquidation_status_references as $liquidation_status_reference)
                    <option value="{{ $liquidation_status_reference->id }}">
                        {{ $liquidation_status_reference->display }}
                    </option>
                @endforeach
            </x-select>
            <x-input-error for="status" class="mt-2" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Liquidation Reference No." />
                        <x-table.th name="Liquidation Total Amount" />
                        <x-table.th name="Status" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($liquidations as $liquidation)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $liquidation->liquidation_reference_no }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <p class="font-bold @if ($liquidation->status_id == 1) text-red @endif">
                                        {{ number_format($liquidation->liquidation_total_amount, 2) }}
                                    </p>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $liquidation->status->display }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        @can('accounting_liquidation_management_edit')
                                            <svg wire:click="action({'id': {{ $liquidation->id }}}, 'edit')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                </path>
                                            </svg>
                                        @endcan
                                        @if (auth()->user()->level_id == 5 && $liquidation)
                                            @if ($liquidation->status_id == 2)
                                                <svg wire:click="action({'id': {{ $liquidation->id }}, 'status_id': 3}, 'update_liquidation_status')"
                                                    class="w-5 h-5 text-red" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 512 512" fill="currentColor">
                                                    <path
                                                        d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
                                                </svg>
                                            @elseif($liquidation->status_id == 3)
                                                <svg wire:click="action({'id': {{ $liquidation->id }}, 'status_id': 2}, 'update_liquidation_status')"
                                                    class="w-5 h-5 text-green" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 512 512" fill="currentColor">
                                                    <path
                                                        d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM371.8 211.8C382.7 200.9 382.7 183.1 371.8 172.2C360.9 161.3 343.1 161.3 332.2 172.2L224 280.4L179.8 236.2C168.9 225.3 151.1 225.3 140.2 236.2C129.3 247.1 129.3 264.9 140.2 275.8L204.2 339.8C215.1 350.7 232.9 350.7 243.8 339.8L371.8 211.8z" />
                                                </svg>
                                            @endif
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $liquidations->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
