<div x-data="{ confirmation_modal: '{{ $confirmation_modal }}', request_for_payment_modal: '{{ $request_for_payment_modal }}', liquidation_details_attachments_modal: '{{ $liquidation_details_attachments_modal }}', liquidation_details_request_for_payment_modal: '{{ $liquidation_details_request_for_payment_modal }}' }">
    <x-loading />
    @if (isset($a) && ($liquidation_details[$a] ?? false))
        <x-modal id="liquidation_details_attachments_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="title">Attachments List</x-slot>
            <x-slot name="body">
                <div class="flex flex-col space-y-3">
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="File" />
                            <x-table.th name="Action" />
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($liquidation_details[$a]['attachments'] as $b => $attachment)
                                @if (!$attachment['is_deleted'])
                                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="flex items-center justify-center p-2 whitespace-nowrap">
                                            <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                <div class="relative z-0 ">
                                                    <div class="absolute top-0 left-0">
                                                        @if ($attachment['id'] && !$attachment['attachment'])
                                                            @if (in_array($attachment['extension'], config('filesystems.image_type')))
                                                                <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                                    <a href="{{ Storage::disk('accounting_gcs')->url($attachment['path'] . $attachment['name']) }}"
                                                                        target="_blank"><img
                                                                            class="w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                            src="{{ Storage::disk('accounting_gcs')->url($attachment['path'] . $attachment['name']) }}"></a>
                                                                </div>
                                                            @elseif(in_array($attachment['extension'], config('filesystems.file_type')))
                                                                <svg wire:click="download({{ $attachment['id'] }})"
                                                                    class="w-20 h-20 mx-auto border border-gray-500 rounded-lg"
                                                                    aria-hidden="true" focusable="false"
                                                                    data-prefix="fas" data-icon="file-alt" role="img"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    viewBox="0 0 384 512">
                                                                    <path fill="currentColor"
                                                                        d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z">
                                                                    </path>
                                                                </svg>
                                                            @endif
                                                        @else
                                                            @if (!$attachment['attachment'])
                                                                <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                    src="{{ $attachment['attachment'] ? $attachment['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                            @elseif (in_array($attachment['attachment']->extension(), config('filesystems.image_type')))
                                                                <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                    src="{{ $attachment['attachment'] ? $attachment['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                            @else
                                                                <svg class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg"
                                                                    aria-hidden="true" focusable="false"
                                                                    data-prefix="fas" data-icon="file-alt" role="img"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    viewBox="0 0 384 512">
                                                                    <path fill="currentColor"
                                                                        d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z">
                                                                    </path>
                                                                </svg>
                                                            @endif
                                                        @endif
                                                    </div>
                                                    <input type="file"
                                                        name="liquidation_details.{{ $a }}.attachments.{{ $b }}.attachment"
                                                        wire:model="liquidation_details.{{ $a }}.attachments.{{ $b }}.attachment"
                                                        class="relative z-50 block w-20 h-20 opacity-0 cursor-pointer">
                                                    <x-input-error
                                                        for="liquidation_details.{{ $a }}.attachments.{{ $b }}.attachment" />
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="flex items-center justify-center space-x-3">
                                                @if (count(collect($liquidation_details[$a]['attachments'])->where('is_deleted', false)) > 1)
                                                    <svg wire:click="action({'a': {{ $a }}, 'b': {{ $b }}}, 'liquidation_details_attachments')"
                                                        class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="times-circle" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                            d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                        </path>
                                                    </svg>
                                                @endif
                                                @if (in_array($attachment['extension'], config('filesystems.image_type')))
                                                    <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                        <a href="{{ Storage::disk('accounting_gcs')->url($attachment['path'] . $attachment['name']) }}"
                                                            target="_blank">
                                                            <svg class="w-5 h-5 text-blue"
                                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"
                                                                fill="currentColor">
                                                                <path
                                                                    d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                                            </svg>
                                                        </a>
                                                    </div>
                                                @elseif(in_array($attachment['extension'], config('filesystems.file_type')))
                                                    <svg wire:click="download({{ $attachment['id'] }})"
                                                        class="w-5 h-5 text-blue" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 512 512" fill="currentColor">
                                                        <path
                                                            d="M480 352h-133.5l-45.25 45.25C289.2 409.3 273.1 416 256 416s-33.16-6.656-45.25-18.75L165.5 352H32c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h448c17.67 0 32-14.33 32-32v-96C512 366.3 497.7 352 480 352zM432 456c-13.2 0-24-10.8-24-24c0-13.2 10.8-24 24-24s24 10.8 24 24C456 445.2 445.2 456 432 456zM233.4 374.6C239.6 380.9 247.8 384 256 384s16.38-3.125 22.62-9.375l128-128c12.49-12.5 12.49-32.75 0-45.25c-12.5-12.5-32.76-12.5-45.25 0L288 274.8V32c0-17.67-14.33-32-32-32C238.3 0 224 14.33 224 32v242.8L150.6 201.4c-12.49-12.5-32.75-12.5-45.25 0c-12.49 12.5-12.49 32.75 0 45.25L233.4 374.6z" />
                                                    </svg>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </x-slot>
                    </x-table.table>
                    <x-input-error for="liquidation_details.{{ $a }}.attachments" />
                    <div class="flex items-center justify-end">
                        <button type="button" wire:click="addLiquidationDetailAttachments"
                            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                            Add Attachment</button>
                    </div>
                </div>
            </x-slot>
        </x-modal>
    @endif
    @if (isset($a) && ($liquidation_details[$a] ?? false))
        <x-modal id="liquidation_details_request_for_payment_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-4/5">
            <x-slot name="title">Requests List</x-slot>
            <x-slot name="body">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Reference No" />
                        <x-table.th name="Pay To" />
                        <x-table.th name="Opex Type" />
                        <x-table.th name="Requested By" />
                        <x-table.th name="Amount" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($liquidation_details[$a]['request_for_payments'] as $b => $request_for_payment)
                            @if (!$request_for_payment['is_deleted'])
                                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $request_for_payment['reference_id'] }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $request_for_payment['pay_to'] }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $request_for_payment['opex_type'] }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $request_for_payment['requested_by'] }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ number_format($request_for_payment['amount'], 2) }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        @if (count(collect($liquidation_details[$a]['request_for_payments'])->where('is_deleted', false)) > 1)
                                            <svg wire:click="action({'a': {{ $a }}, 'b': {{ $b }}}, 'liquidation_details_request_for_payment')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="fas" data-icon="trash-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                                </path>
                                            </svg>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </x-slot>
                </x-table.table>
                <x-input-error for="liquidation_details.{{ $a }}.request_for_payments" />
                <div class="flex items-center justify-end">
                    <button type="button" wire:click="showRequestForPaymentReference('where_in')"
                        class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                        Add Request</button>
                </div>
            </x-slot>
        </x-modal>
    @endif
    <x-modal id="request_for_payment_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="title">Select Requests</x-slot>
        <x-slot name="body">
            <div x-data="{ open: false }" class="relative mb-2 rounded-md" @click.away="open = false">
                <div>
                    <x-label for="request_for_payment_search" value="Requests" :required="true" />
                    <x-input type="text" @click="open = !open" name="request_for_payment"
                        wire:model="request_for_payment_search" />
                    <x-input-error for="request_for_payment_search" />
                    <x-input-error for="request_for_payment" />
                </div>
                <div x-show="open" x-cloak
                    class="absolute w-full p-2 my-1 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96">
                    <ul class="list-reset">
                        @forelse ($request_for_payment_references as $i => $request_for_payment_reference)
                            <li @click="open = !open"
                                wire:click="addRequestForPayment({{ $request_for_payment_reference->id }},1)"
                                wire:key="{{ 'request_for_payment' . $i }}"
                                class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                <p>
                                    {{ $request_for_payment_reference->reference_id }}
                                </p>
                            </li>
                        @empty
                            <li>
                                <p class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                    No Request Found.
                                </p>
                            </li>
                        @endforelse
                    </ul>
                </div>
            </div>
        </x-slot>
    </x-modal>
    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            {{ $confirmation_message }}
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">No</button>
                <button type="button" wire:click="remove"
                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Yes</button>
            </div>
        </x-slot>
    </x-modal>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3" wire:init="load">
            <div class="grid grid-cols-3 gap-3">
                <div>
                    <x-label for="liquidation_reference_no" value="Liquidation Reference No." :required="true" />
                    <x-input type="text" name="liquidation_reference_no" value="{{ $liquidation_reference_no }}"
                        disabled />
                    <x-input-error for="liquidation_reference_no" />
                </div>
                @if (auth()->user()->level_id == 5)
                    <div>
                        <x-label for="status" value="Status" :required="true" />
                        <x-select name="status" wire:model.defer='status'>
                            <option value=""></option>
                            @foreach ($liquidation_status_references as $liquidation_status_reference)
                                <option value="{{ $liquidation_status_reference->id }}">
                                    {{ $liquidation_status_reference->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="status" />
                    </div>
                @endif
            </div>
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Request List</span>
                </div>
            </div>
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="Reference No" />
                    <x-table.th name="Pay To" />
                    <x-table.th name="Opex Type" />
                    <x-table.th name="Requested By" />
                    <x-table.th name="Amount" />
                    <x-table.th name="Action" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($request_for_payments as $a => $request_for_payment)
                        @if (!$request_for_payment['is_deleted'])
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment['reference_id'] }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment['pay_to'] }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment['opex_type'] }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $request_for_payment['requested_by'] }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($request_for_payment['amount'], 2) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if (count(collect($request_for_payments)->where('is_deleted', false)) > 1)
                                        <svg wire:click="action({'a': {{ $a }}}, 'request_for_payment')"
                                            class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                            data-prefix="fas" data-icon="trash-alt" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                            </path>
                                        </svg>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </x-slot>
            </x-table.table>
            <div class="flex flex-col items-end justify-center">
                <p>Total: {{ number_format($request_total_amount, 2) }}</p>
                <x-input-error for="request_for_payments" />
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="showRequestForPaymentReference('where_not_in')"
                    class="flex-none px-3 py-1 text-sm text-white rounded-full bg-green">
                    Add Request</button>
            </div>
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Liquidation Details</span>
                </div>
            </div>
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="Or No." />
                    <x-table.th name="Transaction Date" />
                    <x-table.th name="Amount" />
                    <x-table.th name="Cash On Hand" />
                    <x-table.th name="Description" />
                    <x-table.th name="Remarks" />
                    <x-table.th name="Action" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($liquidation_details as $a => $liquidation_detail)
                        @if (!$liquidation_detail['is_deleted'])
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    <x-input type="text" name="liquidation_details.{{ $a }}.or_no"
                                        wire:model.defer='liquidation_details.{{ $a }}.or_no'>
                                    </x-input>
                                    <x-input-error for="liquidation_details.{{ $a }}.or_no" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <x-input type="date"
                                        name="liquidation_details.{{ $a }}.transaction_date"
                                        wire:model.defer='liquidation_details.{{ $a }}.transaction_date'>
                                    </x-input>
                                    <x-input-error for="liquidation_details.{{ $a }}.transaction_date" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <x-input type="text" name="liquidation_details.{{ $a }}.amount"
                                        wire:model.debounce.500ms='liquidation_details.{{ $a }}.amount'>
                                    </x-input>
                                    <x-input-error for="liquidation_details.{{ $a }}.amount" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <x-input type="text" name="liquidation_details.{{ $a }}.cash_on_hand"
                                        wire:model.debounce.500ms='liquidation_details.{{ $a }}.cash_on_hand'>
                                    </x-input>
                                    <x-input-error for="liquidation_details.{{ $a }}.cash_on_hand" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <x-input type="text" name="liquidation_details.{{ $a }}.description"
                                        wire:model.defer='liquidation_details.{{ $a }}.description'>
                                    </x-input>
                                    <x-input-error for="liquidation_details.{{ $a }}.description" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <x-input type="text" name="liquidation_details.{{ $a }}.remarks"
                                        wire:model.defer='liquidation_details.{{ $a }}.remarks'>
                                    </x-input>
                                    <x-input-error for="liquidation_details.{{ $a }}.remarks" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        <span
                                            wire:click="action({'a': {{ $a }}}, 'liquidation_details_attachments_list')"
                                            class="relative inline-block">
                                            <svg class="w-5 h-5 text-blue" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M147.8 192H480V144C480 117.5 458.5 96 432 96h-160l-64-64h-160C21.49 32 0 53.49 0 80v328.4l90.54-181.1C101.4 205.6 123.4 192 147.8 192zM543.1 224H147.8C135.7 224 124.6 230.8 119.2 241.7L0 480h447.1c12.12 0 23.2-6.852 28.62-17.69l96-192C583.2 249 567.7 224 543.1 224z" />
                                            </svg>
                                            @if (count(collect($liquidation_detail['attachments'])->where('is_deleted', false)) > 0)
                                                <span
                                                    class="absolute top-0 right-0 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full">{{ count(collect($liquidation_detail['attachments'])->where('is_deleted', false)) > 99 ? '99+' : count(collect($liquidation_detail['attachments'])->where('is_deleted', false)) }}</span>
                                            @endif
                                        </span>
                                        <span
                                            wire:click="action({'a': {{ $a }}}, 'liquidation_details_request_for_payment_list')"
                                            class="relative inline-block">
                                            <svg class="w-5 h-5 text-blue" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M88 48C101.3 48 112 58.75 112 72V120C112 133.3 101.3 144 88 144H40C26.75 144 16 133.3 16 120V72C16 58.75 26.75 48 40 48H88zM480 64C497.7 64 512 78.33 512 96C512 113.7 497.7 128 480 128H192C174.3 128 160 113.7 160 96C160 78.33 174.3 64 192 64H480zM480 224C497.7 224 512 238.3 512 256C512 273.7 497.7 288 480 288H192C174.3 288 160 273.7 160 256C160 238.3 174.3 224 192 224H480zM480 384C497.7 384 512 398.3 512 416C512 433.7 497.7 448 480 448H192C174.3 448 160 433.7 160 416C160 398.3 174.3 384 192 384H480zM16 232C16 218.7 26.75 208 40 208H88C101.3 208 112 218.7 112 232V280C112 293.3 101.3 304 88 304H40C26.75 304 16 293.3 16 280V232zM88 368C101.3 368 112 378.7 112 392V440C112 453.3 101.3 464 88 464H40C26.75 464 16 453.3 16 440V392C16 378.7 26.75 368 40 368H88z" />
                                            </svg>
                                            @if (count(collect($liquidation_detail['request_for_payments'])->where('is_deleted', false)) > 0)
                                                <span
                                                    class="absolute top-0 right-0 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full">{{ count(collect($liquidation_detail['request_for_payments'])->where('is_deleted', false)) > 99 ? '99+' : count(collect($liquidation_detail['request_for_payments'])->where('is_deleted', false)) }}</span>
                                            @endif
                                        </span>
                                        @if (count(collect($liquidation_details)->where('is_deleted', false)) > 1)
                                            <svg wire:click="action({'a': {{ $a }}}, 'liquidation_details')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="fas" data-icon="trash-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                                </path>
                                            </svg>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </x-slot>
            </x-table.table>
            <x-input-error for="liquidation_details" />
            <x-input-error for="liquidation_details.*.attachments" />
            <x-input-error for="liquidation_details.*.attachments.*.attachment" />
            <x-input-error for="liquidation_details.*.request_for_payments" />
            <div class="flex flex-col items-end justify-center">
                <p>Total: {{ number_format($liquidation_total_amount, 2) }}</p>
                <x-input-error for="liquidation_total_amount" />
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="addLiquidationDetail"
                    class="flex-none px-3 py-1 text-sm text-white rounded-full bg-green">
                    Add Detail</button>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Create</button>
            </div>
        </div>
    </form>
</div>
