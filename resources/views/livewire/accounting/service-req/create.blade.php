<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>
    @if($confirmation_modal)
    <x-modal id="confirmation_modal" size="w-auto" hasClose="1">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to submit this Service Request?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
    @endif

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
    <div class="p-2 mt-5 space-y-3">
            <div class="grid grid-cols-10 gap-6">
            <div class="col-span-1">
            <x-label for="expected_start_date" value="Expected Start Date :" />
            </div>
            <div class="col-span-2">
            <x-input type="date" name="expected_start_date" wire:model.defer="expected_start_date" />
            <x-input-error for="expected_start_date" class="mt-2" />
            </div>
            </div>
    </div>
    <div class="p-2 mt-5 space-y-3">
            <div class="grid grid-cols-10 gap-6">
            <div class="col-span-1">
            <x-label for="expected_end_date" value="Expected End Date : " />
            </div>
            <div class="col-span-2">
            <x-input type="date" name="expected_end_date" wire:model.defer="expected_end_date" />
            <x-input-error for="expected_end_date" class="mt-2" />
            </div>
            </div>
    </div>
        <div class="p-2 mt-5 space-y-3">
            <div class="grid grid-cols-10 gap-6">
            <div class="col-span-2">
                    <x-label for="service_category_id" value="Service Category" :required="true" />
                    <div wire:init="ServiceCategoryReference" class="flex items-center">
                        <x-select style="cursor: pointer;" name="service_category_id"
                            wire:model='service_category_id'>
                            <option value="">Select</option>
                            @foreach ($service_category_references as $service_category_reference)
                                <option value="{{ $service_category_reference->id }}">
                                    {{ $service_category_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="service_category_id" />
                </div>
                <div class="col-span-2">
                    <x-label for="service_description_id" value="Service Description" :required="true" />
                    <div class="flex items-center">
                        <x-select style="cursor: pointer;" name="service_description_id"
                            wire:model.defer='service_description_id'>
                            <option value="">Select</option>
                            @foreach ($service_description_references as $service_description_reference)
                                <option value="{{ $service_description_reference->id }}">
                                    {{ $service_description_reference->description }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="service_description_id" />
                </div>
                <div>
                    <x-label for="number_of_workers" value="Number of Workers" />
                    <x-input type="text" name="number_of_workers" wire:model.defer='number_of_workers'></x-input>
                    <x-input-error class="absolute" for="number_of_workers" />
                </div>
                <div>
                    <x-label for="estimated_rate" value="Estimated Rate" />
                    <x-input type="text" name="estimated_rate" wire:model.defer='estimated_rate'></x-input>
                    <x-input-error class="absolute" for="estimated_rate" />
                </div>
                <div>
                    <x-label for="preferred_worker" value="Preferred Worker" />
                    <div class="flex items-center">
                        <x-select style="cursor: pointer;" name="preferred_worker"
                        wire:init="ManpowerReference"
                            wire:model.defer='preferred_worker'>
                            <option value="">Select</option>
                            @foreach ($manpower_references as $manpower_reference)
                                <option value="{{ $manpower_reference->id }}">
                                    {{ $manpower_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="preferred_worker" />
                </div>
                <div>
                    <x-label for="purpose" value="Purpose" />
                    <div class="flex items-center">
                        <x-select style="cursor: pointer;" name="purpose"
                        wire:init="PurposeReference"
                            wire:model.defer='purpose'>
                            <option value="">Select</option>
                            @foreach ($purpose_references as $purpose_reference)
                                <option value="{{ $purpose_reference->id }}">
                                    {{ $purpose_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="purpose" />
                </div>
                <div>
                    <x-label for="location_id" value="Location" :required="true"/>
                    <div class="flex items-center">
                        <x-select style="cursor: pointer;" name="location_id"
                        wire:init="loadBranchReference"
                            wire:model.defer='location_id'>
                            <option value="">Select</option>
                            @foreach ($branch_references as $branch_reference)
                                <option value="{{ $branch_reference->id }}">
                                    {{ $branch_reference->display }}
                                </option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="location_id" />
                </div>
                <div>
                    <x-label for="comments" value="Comments" />
                    <x-input type="text" name="comments" wire:model.defer='comments'></x-input>
                    <x-input-error class="absolute" for="comments" />
                </div>
            </div>

        </div>
        <div class="flex justify-end gap-3 mt-6 space-x-3">
            <x-button type="button" wire:click="closecreatemodal" title="Cancel"
            class="px-12 bg-white text-blue hover:bg-gray-100" />
            <x-button type="submit" title="Submit" 
            class="px-12 bg-blue text-white hover:bg-[#002161]" />
        </div>
    </form>
</div>
