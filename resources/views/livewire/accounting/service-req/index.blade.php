
<x-form x-data="{ create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}' }">

    <x-slot name="loading">
        <x-loading />
    </x-slot>   
    <x-slot name="modals">
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                <x-slot name="title">Create Service Requisition</x-slot>
                <x-slot name="body">
                @livewire('accounting.service-req.create')
                </x-slot>
            </x-modal>
        @if ($edit_modal)
            <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12" hasClose="1">
                        <x-slot name="body">
                            @livewire('accounting.service-req.edit', ['id' => $reqid])
                        </x-slot>
            </x-modal>
        @endif

    </x-slot>
    <x-slot name="header_title">
        <div class="flex space-x-2">
            <div>
                Service Requisition
            </div>
        </div>
    </x-slot>
    <x-slot name="header_button">
            <button wire:click="action({}, 'add')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Create SRn
                </div>
            </button>

    </x-slot>
    <x-slot name="body">
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="No." style="padding-left:2%;" />
                        <x-table.th name="SR No." style="padding-left:%;" />
                        <x-table.th name="Priority Level" style="padding-left:%;" />
                        <x-table.th name="Expected Start Date" style="padding-left:;" />
                        <x-table.th name="Action" style="padding-left:;" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($service_req_lists as $i => $service_req_list)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-normal">
                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:2%;">
                                    <p class="w-24">
                                        {{ ($service_req_lists->currentPage() - 1) * $service_req_lists->links()->paginator->perPage() + $loop->iteration }}
                                    </p>
                                </td>
                                <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $service_req_list->reference_no }}
                                </td>
                                <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $service_req_list->ServReqDetailsPriorityy->name }}
                                </td>
                                <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $service_req_list->expected_req_start_date }}
                                </td>
                                <td>
                                <span title="Edit">
                                                <svg wire:click="action({'id': {{ $service_req_list->id }}}, 'edit')"
                                                    class="w-5 h-5 ml-2 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="far" data-icon="edit" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                                    </path>
                                                </svg>
                                            </span>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $service_req_lists->links() }}
                </div>
            </div>
    </x-slot>

</x-form>
