<x-form wire:init="load" x-data="{ search_form: false, create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}', reactivate_modal: '{{ $reactivate_modal }}', deactivate_modal: '{{ $deactivate_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('accounting_account_management_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                <x-slot name="title">Create Account</x-slot>
                <x-slot name="body">
                    @livewire('accounting.account-management.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('accounting_account_management_edit')
            @if ($user_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                    <x-slot name="title">Edit Account</x-slot>
                    <x-slot name="body">
                        @livewire('accounting.account-management.edit', ['id' => $user_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('accounting_account_management_deactivate')
            <x-modal id="reactivate_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span
                                class="text-blue">Reactivate</span>
                            this Account?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('reactivate_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateStatus('{{ $user_id }}', 1)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#003399] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="deactivate_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span
                                class="text-red">Deactivate</span>
                            this Account?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('deactivate_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateStatus('{{ $user_id }}', 2)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#CC0000] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
    </x-slot>
    <x-slot name="header_title">Account List</x-slot>
    <x-slot name="header_button">
        @can('accounting_account_management_add')
            <button wire:click="$set('create_modal', true)"
                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Create an Account</button>
        @endcan
    </x-slot>
    <x-slot name="header_card">
        @forelse ($header_cards as $index => $card)
            <x-card.header wire:click="$set('{{ $card['action'] }}', {{ $card['id'] }})"
                wire:key="{{ $index }}" :card="$card"></x-card.header>
        @empty
            <x-card.header-loading count="4"></x-card.header-loading>
        @endforelse
    </x-slot>
    <x-slot name="search_form">
        <div>
            <x-label for="name" value="Name" />
            <x-input type="text" name="name" wire:model="name" />
            <x-input-error for="name" class="mt-2" />
        </div>
        <div>
            <x-label for="role" value="Roles" />
            <x-select name="role" wire:model="role">
                <option value=""></option>
                @foreach ($division_roles_references as $division_roles_reference)
                    <option value="{{ $division_roles_reference->id }}">
                        {{ $division_roles_reference->display }}</option>
                @endforeach
            </x-select>
            <x-input-error for="role" class="mt-2" />
        </div>
        <div>
            <x-label for="status" value="Status" />
            <x-select name="status" wire:model="status">
                <option value=""></option>
                @foreach ($account_status_references as $account_status_reference)
                    <option value="{{ $account_status_reference->id }}">
                        {{ $account_status_reference->display }}</option>
                @endforeach
            </x-select>
            <x-input-error for="status" class="mt-2" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Employee Number" wire:click="sortBy('employee_number')">
                            <x-slot name="sort">
                                <x-table.sort-icon field="employee_number" sortField="{{ $sortField }}"
                                    sortAsc="{{ $sortAsc }}">
                                </x-table.sort-icon>
                            </x-slot>
                        </x-table.th>
                        <x-table.th name="Role" />
                        <x-table.th name="Full Name" />
                        <x-table.th name="Email" />
                        <x-table.th name="Status" />
                        <x-table.th name="Created At" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($users as $user)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $user->userDetails->employee_number }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $user->role->display }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $user->name }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $user->email }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span
                                        class="{{ $user->status->text_color }}">{{ $user->status->display }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('M. d, Y', strtotime($user->created_at)) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        @can('accounting_account_management_edit')
                                            <svg wire:click="action({'id': {{ $user->id }}}, 'edit')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                </path>
                                            </svg>
                                        @endcan
                                        @can('accounting_account_management_deactivate')
                                            <svg x-cloak x-show="'{{ $user->status_id == 1 }}'"
                                                wire:click="action({'id': {{ $user->id }}, 'status_id': 2}, 'update_status')"
                                                class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                data-prefix="fas" data-icon="user-slash" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                                <path fill="currentColor"
                                                    d="M633.8 458.1L362.3 248.3C412.1 230.7 448 183.8 448 128 448 57.3 390.7 0 320 0c-67.1 0-121.5 51.8-126.9 117.4L45.5 3.4C38.5-2 28.5-.8 23 6.2L3.4 31.4c-5.4 7-4.2 17 2.8 22.4l588.4 454.7c7 5.4 17 4.2 22.5-2.8l19.6-25.3c5.4-6.8 4.1-16.9-2.9-22.3zM96 422.4V464c0 26.5 21.5 48 48 48h350.2L207.4 290.3C144.2 301.3 96 356 96 422.4z">
                                                </path>
                                            </svg>
                                            <svg x-cloak x-show="'{{ $user->status_id == 2 }}'"
                                                wire:click="action({'id': {{ $user->id }}, 'status_id': 1}, 'update_status')"
                                                class="w-5 h-5 text-green" aria-hidden=" true" focusable="false"
                                                data-prefix="fas" data-icon="user" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z">
                                                </path>
                                            </svg>
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
