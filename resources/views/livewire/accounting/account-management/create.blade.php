<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-2 gap-3">
                <div>
                    <div class="grid grid-cols-1 gap-3">
                        <div>
                            <x-label for="employee_number" value="Employee Number" :required="true" />
                            <x-input type="number" name="employee_number" wire:model.defer='employee_number'></x-input>
                            <x-input-error for="employee_number" />
                        </div>
                        <div>
                            <x-label for="first_name" value="First Name" :required="true" />
                            <x-input type="text" name="first_name" wire:model.defer='first_name'></x-input>
                            <x-input-error for="first_name" />
                        </div>
                        <div>
                            <x-label for="middle_name" value="Middle Name" />
                            <x-input type="text" name="middle_name" wire:model.defer='middle_name'></x-input>
                            <x-input-error for="middle_name" />
                        </div>
                        <div>
                            <x-label for="last_name" value="Last Name" :required="true" />
                            <x-input type="text" name="last_name" wire:model.defer='last_name'></x-input>
                            <x-input-error for="last_name" />
                        </div>
                        <div>
                            <x-label for="mobile_number" value="Mobile Number" :required="true" />
                            <x-input type="text" name="mobile_number" wire:model.defer='mobile_number'></x-input>
                            <x-input-error for="mobile_number" />
                        </div>
                        <div>
                            <x-label for="telephone_number" value="Telephone Number" />
                            <x-input type="text" name="telephone_number" wire:model.defer='telephone_number'></x-input>
                            <x-input-error for="telephone_number" />
                        </div>
                        <div wire:init="loadSuffixReference">
                            <x-label for="suffix" value="Suffix" />
                            <x-select name="suffix" wire:model.defer='suffix'>
                                <option value=""></option>
                                @foreach ($suffix_references as $suffix_reference)
                                    <option value="{{ $suffix_reference->id }}">{{ $suffix_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="suffix" />
                        </div>
                    </div>
                </div>
                <div>
                    <div class="grid grid-cols-1 gap-3">
                        <div wire:init="loadLevelReference">
                            <x-label for="level" value="Level" :required="true" />
                            <x-select name="level" wire:model='level'>
                                <option value=""></option>
                                @foreach ($level_references as $level_reference)
                                    <option value="{{ $level_reference->id }}">{{ $level_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="level" />
                        </div>
                        <div>
                            <x-label for="role" value="Role" :required="true" />
                            <x-select name="role" wire:model.defer='role'>
                                <option value=""></option>
                                @foreach ($role_references as $role_reference)
                                    <option value="{{ $role_reference->id }}">{{ $role_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="role" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Account</span>
                </div>
            </div>
            <div class="grid grid-cols-2 gap-3">
                <div>
                    <x-label for="email" value="Email" :required="true" />
                    <x-input type="email" name="email" wire:model.defer='email'></x-input>
                    <x-input-error for="email" />
                </div>
                <div></div>
                <div>
                    <x-label for="password" value="Password" :required="true" />
                    <x-input type="password" name="password" wire:model.defer='password'></x-input>
                    <x-input-error for="password" />
                </div>
                <div>
                    <x-label for="password_confirmation" value="Confirm Password" :required="true" />
                    <x-input type="password" name="password_confirmation" wire:model.defer='password_confirmation'>
                    </x-input>
                    <x-input-error for="password_confirmation" />
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Create</button>
            </div>
        </div>
    </form>
</div>
