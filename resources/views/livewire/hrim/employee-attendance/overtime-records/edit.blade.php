<div wire:init="load">
    <x-loading />
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-6">
            <div class="grid grid-cols-2 gap-6">
                <div class="col-span-2">
                    <div class="grid grid-cols-3 gap-6">
                        <div>
                            <x-label value="Month" :required="true" />
                            <x-select name="month" wire:model='month'>
                                @foreach ($month_references as $month_reference)
                                    <option value="{{ $month_reference->id }}">
                                        {{ $month_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="month" />
                        </div>
                        <div>
                            <x-label value="Cut Off" :required="true" />
                            <x-select name="cut_off" wire:model='cut_off'>
                                <option value="first">First</option>
                                <option value="second">Second</option>
                            </x-select>
                            <x-input-error for="cut_off" />
                        </div>
                        <div>
                            <x-label value="Year" :required="true" />
                            <x-input type="number" name="year" wire:model='year' />
                            <x-input-error for="year" />
                        </div>
                    </div>
                </div>
                <div>
                    <x-label value="Date Time In" :required="true" />
                    <x-select name="date_time_in" wire:model='date_time_in'>
                        <option value=""></option>
                        @foreach ($overtimed_date_references as $overtimed_date_references)
                            <option value="{{ $overtimed_date_references->date }}">
                                {{ date('M. d, Y', strtotime($overtimed_date_references->date)) }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="date_time_in" />
                </div>
                <div>
                    <x-label value="Date Time Out" :required="true" />
                    <x-input type="date" name="date_time_out" wire:model.defer='date_time_out' disabled />
                    <x-input-error for="date_time_out" />
                </div>
                <div>
                    <x-label value="Time In" :required="true" />
                    <x-input type="time" name="time_in" wire:model.defer='time_in' disabled />
                    <x-input-error for="time_in" />
                </div>
                <div>
                    <x-label value="Time Out" :required="true" />
                    <x-input type="time" name="time_out" wire:model.defer='time_out' disabled />
                    <x-input-error for="time_out" />
                </div>
                <div>
                    <x-label value="Work Schedule" :required="true" />
                    <x-input type="text" name="work_schedule" wire:model.defer='work_schedule' disabled />
                    <x-input-error for="work_schedule" />
                </div>
                <div>
                    <x-label value="Rendered OT Hours" :required="true" />
                    <x-input type="text" name="rendered_ot_hours" wire:model.defer='rendered_ot_hours' disabled />
                    <x-input-error for="rendered_ot_hours" />
                </div>
                @if ($time_log)
                    <div class="col-span-2">
                        <p class="text-lg font-semibold text-gray-800">Actual Overtime</p>
                    </div>
                    <div class="col-span-2">
                        <x-label value="Type of OT" :required="true" />
                        <x-select name="type_of_ot" wire:model='type_of_ot' disabled>
                            <option value=""></option>
                            @foreach ($date_category_references as $date_category_reference)
                                <option value="{{ $date_category_reference->id }}">
                                    {{ $date_category_reference->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="type_of_ot" />
                    </div>
                    <div>
                        <x-label value="Date From" :required="true" />
                        <x-input type="date" name="actual_date_from" wire:model.defer='actual_date_from' disabled />
                        <x-input-error for="actual_date_from" />
                    </div>
                    <div>
                        <x-label value="Date To" :required="true" />
                        <x-input type="date" name="actual_date_to" wire:model='actual_date_to' />
                        <x-input-error for="actual_date_to" />
                    </div>
                    <div>
                        <x-label value="From" :required="true" />
                        <x-input type="time" name="from" wire:model.defer='from' step="60" disabled />
                        <x-input-error for="from" />
                    </div>
                    <div>
                        <x-label value="To" :required="true" />
                        <x-input type="time" name="to" wire:model.debounce='to' step="60" />
                        <x-input-error for="to" />
                    </div>
                    <div>
                        <x-label value="Overtime Request" :required="true" />
                        <x-input type="text" name="overtime_request" wire:model.defer='overtime_request' disabled />
                        <x-input-error for="overtime_request" />
                    </div>
                    <div class="col-span-2">
                        <x-label value="Reason" :required="true" />
                        <x-textarea name="reason" wire:model.defer='reason'></x-textarea>
                        <x-input-error for="reason" />
                    </div>
                @endif
            </div>
            <div class="flex justify-end space-x-3">
                <x-button type="button" wire:click="$emit('close_modal', 'create')" title="Cancel"
                    class="bg-white text-blue hover:bg-gray-100" />
                @if (!$overtime_record || $overtime_record->final_status_id <= 1)
                    <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
                @endif
            </div>
        </div>
    </form>
</div>
