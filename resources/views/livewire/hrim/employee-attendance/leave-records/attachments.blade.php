<div x-data="{ view_modal: '{{ $view_modal }}' }" class="grid h-full grid-cols-12 gap-6">
    <x-loading />
    @if ($leave->attachments->isNotEmpty())
        <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
            <x-slot name="body">
                <img
                    src="{{ Storage::disk('hrim_gcs')->url($leave->attachments[$active]->path . $leave->attachments[$active]->name) }}">
            </x-slot>
        </x-modal>
        <div class="col-span-3 overflow-auto h-[500px]">
            <div class="flex flex-col space-y-3">
                @foreach ($leave->attachments as $index => $attachment)
                    @if (in_array($attachment->extension, config('filesystems.image_type')))
                        <div wire:click="$set('active', {{ $index }})"
                            class="w-full bg-center bg-no-repeat bg-cover border border-gray-400 rounded-md cursor-pointer h-28"
                            style="background-image: url({{ Storage::disk('hrim_gcs')->url($attachment->path . $attachment->name) }})">
                        </div>
                    @else
                        <div wire:click="$set('active', {{ $index }})"
                            class="flex items-center justify-center w-full p-2 border border-gray-400 rounded-md cursor-pointer h-28">
                            <svg class="h-24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                <path fill="currentColor"
                                    d="M256 0v128h128L256 0zM224 128L224 0H48C21.49 0 0 21.49 0 48v416C0 490.5 21.49 512 48 512h288c26.51 0 48-21.49 48-48V160h-127.1C238.3 160 224 145.7 224 128zM272 416h-160C103.2 416 96 408.8 96 400C96 391.2 103.2 384 112 384h160c8.836 0 16 7.162 16 16C288 408.8 280.8 416 272 416zM272 352h-160C103.2 352 96 344.8 96 336C96 327.2 103.2 320 112 320h160c8.836 0 16 7.162 16 16C288 344.8 280.8 352 272 352zM288 272C288 280.8 280.8 288 272 288h-160C103.2 288 96 280.8 96 272C96 263.2 103.2 256 112 256h160C280.8 256 288 263.2 288 272z" />
                            </svg>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
        <div class="h-full col-span-9">
            @if (in_array($leave->attachments[$active]->extension, config('filesystems.image_type')))
                <div wire:click="$set('view_modal', true)"
                    class="h-[500px] bg-center bg-no-repeat bg-cover border border-gray-400 rounded-md cursor-pointer"
                    style="background-image: url({{ Storage::disk('hrim_gcs')->url($leave->attachments[$active]->path . $leave->attachments[$active]->name) }})">
                </div>
            @else
                <div wire:click="download"
                    class="flex items-center justify-center w-full px-6 py-2 border border-gray-400 rounded-md cursor-pointer h-[500px]">
                    <svg class="h-[400px]" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                        <path fill="currentColor"
                            d="M256 0v128h128L256 0zM224 128L224 0H48C21.49 0 0 21.49 0 48v416C0 490.5 21.49 512 48 512h288c26.51 0 48-21.49 48-48V160h-127.1C238.3 160 224 145.7 224 128zM272 416h-160C103.2 416 96 408.8 96 400C96 391.2 103.2 384 112 384h160c8.836 0 16 7.162 16 16C288 408.8 280.8 416 272 416zM272 352h-160C103.2 352 96 344.8 96 336C96 327.2 103.2 320 112 320h160c8.836 0 16 7.162 16 16C288 344.8 280.8 352 272 352zM288 272C288 280.8 280.8 288 272 288h-160C103.2 288 96 280.8 96 272C96 263.2 103.2 256 112 256h160C280.8 256 288 263.2 288 272z" />
                    </svg>
                </div>
            @endif
            <div class="text-lg text-center">{{ $leave->attachments[$active]->original_name }}</div>
            <div class="flex items-center justify-between mt-3">
                <span wire:click="previous"
                    class="{{ $active == 0 ? 'text-gray-500' : 'underline text-blue' }} text-xs cursor-pointer">Previous</span>
                <span wire:click="next"
                    class="{{ $active == $total_attachments ? 'text-gray-500' : 'underline text-blue' }} text-xs cursor-pointer">Next</span>
            </div>
        </div>
    @endif
</div>
