<div>
    @if ($user_leaves)
        <div class="flex items-center justify-between px-3 py-2 mt-3 mb-3 text-sm bg-gray-100 rounded-full ">
            <p>Remaining SL Credits: <span
                    class="font-medium text-blue">{{ number_format($user_leaves['sick_leave'], 1) }}</span>
            </p>
            <p>Remaining VL Credits: <span
                    class="font-medium text-blue">{{ number_format($user_leaves['vacation_leave'], 1) }}</span>
            </p>
        </div>
    @endif
    <div class="grid grid-cols-12 text-sm gap-y-1">
        <div class="col-span-4 ">
            <p class="text-gray-400">Inclusive Date:</p>
        </div>
        <div class="col-span-8">
            <p class="font-medium">
                {{ date('F d, Y', strtotime($data['inclusive_date_from'])) . ' - ' . date('F d, Y', strtotime($data['inclusive_date_to'])) }}
            </p>
        </div>
        <div class="col-span-4">
            <p class="text-gray-400">Date Issued:</p>
        </div>
        <div class="col-span-8">
            <p class="font-medium">{{ date('F d, Y', strtotime(now())) }}</p>
        </div>
        <div class="col-span-4">
            <p class="text-gray-400">Resume of Work:</p>
        </div>
        <div class="col-span-8">
            <p class="font-medium">{{ date('F d, Y', strtotime($data['resume_of_work'])) }}</p>
        </div>
        <div class="col-span-4">
            <p class="text-gray-400">Type of Leave:</p>
        </div>
        <div class="col-span-8">
            <p class="font-medium">{{ $leave_type }}
                @if ($data['is_with_medical_certificate'] === '1')
                    <i class="text-xs font-normal">(With Medical Certificate)</i>
                @elseif ($data['is_with_medical_certificate'] === '0')
                    <i class="text-xs font-normal">(Without Medical Certificate)</i>
                @endif
            </p>
        </div>
        <div class="col-span-4">
            <p class="text-gray-400">Apply For:</p>
        </div>
        <div class="col-span-8">
            <p class="font-medium">{{ $apply_for }}
                <i class="text-xs font-normal">({{ $data['is_with_pay'] ? 'With Pay' : 'Without Pay' }})</i>
            </p>
        </div>
        <div class="col-span-4">
            <p class="text-gray-400">Reason:</p>
        </div>
        <div class="col-span-8">
            <p class="font-medium">{{ $data['reason'] }}</p>
        </div>
        <div class="col-span-4">
            <p class="text-gray-400">Reliever:</p>
        </div>
        <div class="col-span-8">
            <p class="font-medium">{{ $reliever }}</p>
        </div>
    </div>

    <div class="flex justify-end mt-3 space-x-3">
        <x-button type="button" wire:click="$emit('close_modal', 'perview')" title="Back"
            class="bg-white text-blue hover:bg-gray-100" />
        <x-button type="button" wire:click="submit" title="Confirm" class="bg-blue text-white hover:bg-[#002161]" />
    </div>
</div>
