<div wire:init="load" x-data="{ perview_summary_modal: '{{ $perview_summary_modal }}' }">
    <x-loading />
    @if ($perview_summary_modal)
        <x-modal id="perview_summary_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
            <x-slot name="title">
                Leave Summary
            </x-slot>
            <x-slot name="body">
                @livewire('hrim.employee-attendance.leave-records.perview-summary', ['data' => $this->getRequest(), 'action_type' => 'edit'])
            </x-slot>
        </x-modal>
    @endif
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-6">
            <div class="grid grid-cols-2 gap-6">
                <div>
                    <x-label value="Inclusive Date From" :required="true" />
                    <x-input type="date" name="inclusive_date_from" wire:model.defer='inclusive_date_from' />
                    <x-input-error for="inclusive_date_from" />
                </div>
                <div>
                    <x-label value="Inclusive Date To" :required="true" />
                    <x-input type="date" name="inclusive_date_to" wire:model.defer='inclusive_date_to' />
                    <x-input-error for="inclusive_date_to" />
                </div>
                <div class="col-span-2">
                    <x-label value="Resume of Work" :required="true" />
                    <x-input type="date" name="resume_of_work" wire:model.defer='resume_of_work' />
                    <x-input-error for="resume_of_work" />
                </div>
                <div class="col-span-2">
                    <x-label value="Type of Leave" :required="true" />
                    <x-select name="type_of_leave" wire:model='type_of_leave'>
                        <option value=""></option>
                        @foreach ($leave_type_references as $leave_type_reference)
                            <option value="{{ $leave_type_reference->id }}">
                                {{ $leave_type_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="type_of_leave" />
                    @if ($type_of_leave_message)
                        <div class="mt-2 text-sm text-red">Note: {{ $type_of_leave_message }}</div>
                    @endif
                </div>
                @if ($type_of_leave == 1)
                    <div class="col-span-2">
                        <x-input-radio label="With Medical Certificate" name="is_with_medical_certificate"
                            value="1" wire:model.defer="is_with_medical_certificate" />
                        <x-input-radio label="Without Medical Certificate" name="is_with_medical_certificate"
                            value="0" wire:model.defer="is_with_medical_certificate" />
                        <x-input-error for="is_with_medical_certificate" />
                    </div>
                @elseif ($type_of_leave == 4)
                    <div class="col-span-2">
                        <x-label value="Others (Type of leave)" class="text-xs" :required="true" />
                        <x-input type="text" name="other" wire:model.defer='other' />
                        <x-input-error for="other" />
                    </div>
                @endif
                <div class="col-span-2">
                    <x-label value="Apply For" :required="true" />
                    <x-select name="apply_for" wire:model.defer='apply_for'>
                        <option value=""></option>
                        @foreach ($leave_day_type_references as $leave_day_type_reference)
                            <option value="{{ $leave_day_type_reference->id }}">
                                {{ $leave_day_type_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="apply_for" />
                </div>
                <div class="col-span-2">
                    <x-input-radio label="With Pay" name="is_with_pay" value="1" wire:model.defer="is_with_pay" />
                    <x-input-radio label="Without Pay" name="is_with_pay" value="0"
                        wire:model.defer="is_with_pay" />
                    <x-input-error for="is_with_pay" />
                </div>
                <div class="col-span-2">
                    <x-label value="Reason" :required="true" />
                    <x-textarea name="reason" wire:model.defer='reason'></x-textarea>
                    <x-input-error for="reason" />
                </div>
                <div class="col-span-2">
                    <x-label value="Reliever" />
                    <x-select name="reliever" wire:model.defer='reliever'>
                        <option value=""></option>
                        @foreach ($reliever_references as $reliever_reference)
                            <option value="{{ $reliever_reference->id }}">
                                {{ $reliever_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="reliever" />
                </div>
                <div class="col-span-2">
                    <x-label value="Attachment" />
                    <div class="flex flex-col items-start justify-start w-full text-1xs">
                        <button type="button"
                            class="relative flex items-start justify-start px-3 py-1 text-gray-600 bg-gray-300 border border-gray-600 rounded cursor-pointer hover:bg-gray-400">
                            <input type="file" name="attachment" wire:model="attachment"
                                class="absolute w-full h-full opacity-0  cursor-pointer text-[0px]">
                            <svg class="w-3 h-3 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                <path fill="currentColor"
                                    d="M256 0v128h128L256 0zM224 128L224 0H48C21.49 0 0 21.49 0 48v416C0 490.5 21.49 512 48 512h288c26.51 0 48-21.49 48-48V160h-127.1C238.3 160 224 145.7 224 128zM272 416h-160C103.2 416 96 408.8 96 400C96 391.2 103.2 384 112 384h160c8.836 0 16 7.162 16 16C288 408.8 280.8 416 272 416zM272 352h-160C103.2 352 96 344.8 96 336C96 327.2 103.2 320 112 320h160c8.836 0 16 7.162 16 16C288 344.8 280.8 352 272 352zM288 272C288 280.8 280.8 288 272 288h-160C103.2 288 96 280.8 96 272C96 263.2 103.2 256 112 256h160C280.8 256 288 263.2 288 272z" />
                            </svg>
                            <span>Upload File</span>
                        </button>
                        <x-input-error for="attachment" />
                    </div>
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <x-button type="button" wire:click="$emit('close_modal', 'edit')" title="Cancel"
                    class="bg-white text-blue hover:bg-gray-100" />
                @if (!$leave || $leave->final_status_id <= 1)
                    <x-button type="button" wire:click="action({}, 'perview')" title="Submit"
                        class="bg-blue text-white hover:bg-[#002161]" />
                @endif
            </div>
        </div>
    </form>
</div>
