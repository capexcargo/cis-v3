<x-form x-data="{ create_modal: '{{ $create_modal }}', view_modal: '{{ $view_modal }}', edit_modal: '{{ $edit_modal }}', attachment_modal: '{{ $attachment_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_employee_leave_records_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="title">
                    Apply for a leave
                </x-slot>
                <x-slot name="body">
                    @if ($user_leaves)
                        <div class="flex items-center justify-between px-3 py-2 mt-3 mb-3 text-sm bg-gray-100 rounded-full ">
                            <p>Remaining SL Credits: <span
                                    class="font-medium text-blue">{{ number_format($user_leaves['sick_leave'], 1) }}</span>
                            </p>
                            <p>Remaining VL Credits: <span
                                    class="font-medium text-blue">{{ number_format($user_leaves['vacation_leave'], 1) }}</span>
                            </p>
                        </div>
                    @endif
                    @livewire('hrim.employee-attendance.leave-records.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_employee_leave_records_edit')
            @if ($edit_modal && $leave_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                    <x-slot name="title">
                        Edit Leave
                    </x-slot>
                    <x-slot name="body">
                        @if ($user_leaves)
                            <div
                                class="flex items-center justify-between px-3 py-2 mt-3 mb-3 text-sm bg-gray-100 rounded-full ">
                                <p>Remaining SL Credits: <span
                                        class="font-medium text-blue">{{ number_format($user_leaves['sick_leave'], 1) }}</span>
                                </p>
                                <p>Remaining VL Credits: <span
                                        class="font-medium text-blue">{{ number_format($user_leaves['vacation_leave'], 1) }}</span>
                                </p>
                            </div>
                        @endif
                        @livewire('hrim.employee-attendance.leave-records.edit', ['id' => $leave_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @if ($view_modal && $leave_id)
            <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="body">
                    @livewire('hrim.employee-attendance.leave-records.view', ['id' => $leave_id])
                </x-slot>
            </x-modal>
        @endif
        @if ($attachment_modal && $leave_id)
            <x-modal id="attachment_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="body">
                    @livewire('hrim.employee-attendance.leave-records.attachments', ['id' => $leave_id])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">My Leave Records</x-slot>
    <x-slot name="header_button">
        @if ($user_leaves)
            <div class="flex mr-6 space-x-6 text-sm">
                <p>Remaining SL Credit : <span
                        class="font-medium text-blue">{{ number_format($user_leaves['sick_leave'], 1) }}</span>
                </p>
                <p>Remaining VL Credit : <span
                        class="font-medium text-blue">{{ number_format($user_leaves['vacation_leave'], 1) }}</span>
                </p>
            </div>
        @endif
        <x-button type="button" wire:click="$set('create_modal', true)" title="Apply for a Leave"
            class="bg-blue text-white hover:bg-[#002161]">
        </x-button>
    </x-slot>
    <x-slot name="body">
        <div class="bg-white rounded-lg shadow-md">
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="Date Filed" />
                    <th colspan="2" class="p-2 tracking-wider whitespace-nowrap ">
                        <span class="flex font-medium">
                            Inclusive Dates
                        </span>
                    </th>
                    <x-table.th name="Resume of Work" />
                    <x-table.th name="Type of Leave" />
                    <x-table.th name="Apply for" />
                    <x-table.th name="Reason" />
                    <x-table.th name="Reliever" />
                    <x-table.th name="Status" />
                    <x-table.th name="Attachment" />
                    <x-table.th name="Action" />
                </x-slot>
                <x-slot name="thead1">
                    <x-table.th name="" />
                    <x-table.th name="From" />
                    <x-table.th name="To" />
                    <x-table.th name="" />
                    <x-table.th name="" />
                    <x-table.th name="" />
                    <x-table.th name="" />
                    <x-table.th name="" />
                    <x-table.th name="" />
                    <x-table.th name="" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($leaves as $leave)
                        <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                            <td class="p-3 whitespace-nowrap">
                                {{ date('M. d, Y', strtotime($leave->created_at)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ date('M. d, Y', strtotime($leave->inclusive_date_from)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ date('M. d, Y', strtotime($leave->inclusive_date_to)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ date('M. d, Y', strtotime($leave->resume_date)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                @if ($leave->leave_type_id != 4)
                                    {{ $leave->leaveType->display }}
                                @else
                                    <span class="text-center whitespace-normal">
                                        {{ $leave->leaveType->display }}
                                        <span class="text-xs">
                                            ({{ $leave->other }})
                                        </span>
                                    </span>
                                @endif
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $leave->leaveDayType->display }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $leave->reason }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $leave->relieverUser->name ?? 'N/A' }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <span
                                    class="px-6 py-1 text-xs rounded-full {{ $leave->finalStatus->code }}">{{ $leave->finalStatus->display }}</span>
                            </td>
                            <td class="flex items-center justify-center p-3 whitespace-nowrap">
                                <span title="Attachments">
                                    <svg wire:click="action({id: '{{ $leave->id }}'}, 'attachment')"
                                        class="w-5 h-5 text-blue" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M147.8 192H480V144C480 117.5 458.5 96 432 96h-160l-64-64h-160C21.49 32 0 53.49 0 80v328.4l90.54-181.1C101.4 205.6 123.4 192 147.8 192zM543.1 224H147.8C135.7 224 124.6 230.8 119.2 241.7L0 480h447.1c12.12 0 23.2-6.852 28.62-17.69l96-192C583.2 249 567.7 224 543.1 224z" />
                                    </svg>
                                </span>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-center space-x-3">
                                    <span title="View">
                                        <svg wire:click="action({id: '{{ $leave->id }}'}, 'view')"
                                            class="w-5 h-5 text-blue" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                        </svg>
                                    </span>
                                    @if ($leave->final_status_id == 1)
                                        @can('hrim_employee_leave_records_edit')
                                            <span title="Edit">
                                                <svg wire:click="action({id: '{{ $leave->id }}'}, 'edit')"
                                                    class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="far" data-icon="edit" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                    </path>
                                                </svg>
                                            </span>
                                        @endcan
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </x-slot>
            </x-table.table>
            <div class="px-1 pb-2">
                {{ $leaves ? $leaves->links() : '' }}
            </div>
        </div>
    </x-slot>
</x-form>
