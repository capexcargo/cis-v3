<x-form wire:init="load" x-data="{
    search_form: false,
    confirmation_modal: '{{ $confirmation_modal }}',
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    view_modal: '{{ $view_modal }}'
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/5">
            <x-slot name="body">
                <div class="space-y-3">
                    <p class="text-left">{{ $confirmation_message }}</p>
                    <div class="flex items-center justify-center space-x-3">
                        <x-button type="button" wire:click="$set('confirmation_modal', false)" title="No"
                            class="py-1 bg-white text-blue hover:bg-gray-100" />
                        <x-button type="button" wire:click="confirm" title="Yes"
                            class="bg-blue text-white hover:bg-[#002161] py-1" />
                    </div>
                </div>
            </x-slot>
        </x-modal>
        @can('hrim_employee_overtime_records_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/4">
                <x-slot name="title">Apply Schedule Adjustment Request</x-slot>
                <x-slot name="body">
                    @livewire('hrim.employee-attendance.schedule-adjustment.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_employee_overtime_records_edit')
            @if ($edit_modal && $schedule_adjustment_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/4">
                    <x-slot name="title">Edit Schedule Adjustment Request</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.employee-attendance.schedule-adjustment.edit', ['id' => $schedule_adjustment_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @if ($view_modal && $schedule_adjustment_id)
            <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="body">
                    @livewire('hrim.employee-attendance.schedule-adjustment.view', ['id' => $schedule_adjustment_id])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">Schedule Adjustment Request</x-slot>
    <x-slot name="header_button">
        @can('hrim_employee_overtime_records_add')
            <x-button type="button" wire:click="$set('create_modal', true)" title="Apply Schedule Adjustment"
                class="bg-blue text-white hover:bg-[#002161]">
            </x-button>
        @endcan
    </x-slot>
    {{-- <x-slot name="search_form">
        <x-transparent.input type="text" label="Employee Name" name="employee_name"
            wire:model.debounce.500ms="employee_name" />
        <x-transparent.input type="date" label="Date Filed" name="date_filed"
            wire:model.debounce.500ms="date_filed" />
        <x-transparent.select value="{{ $status }}" label="Status" name="status" wire:model="status">
            <option value=""></option>
            @foreach ($status_references as $status_reference)
                <option value="{{ $status_reference->id }}">
                    {{ $status_reference->display }}
                </option>
            @endforeach
        </x-transparent.select>
    </x-slot> --}}
    <x-slot name="body">
        <div class="grid grid-cols-2 gap-4 sm:grid-cols-2 md:grid-cols-5 lg:grid-cols-5 xl:grid-cols-5">
            <x-transparent.input type="text" label="Employee Name" name="employee_name"
                wire:model.debounce.500ms="employee_name" />
            <x-transparent.input type="date" label="Date Filed" name="date_filed"
                wire:model.debounce.500ms="date_filed" />
            <x-transparent.select value="{{ $status }}" label="Status" name="status" wire:model="status">
                <option value=""></option>
                @foreach ($status_references as $status_reference)
                    <option value="{{ $status_reference->id }}">
                        {{ $status_reference->display }}
                    </option>
                @endforeach
            </x-transparent.select>
        </div>
        <div class="bg-white rounded-lg shadow-md">
            <x-table.table>
                <x-slot name="thead">
                    <th colspan="3" class="p-2 tracking-wider whitespace-nowrap ">
                        <span class="flex justify-center font-medium">

                        </span>
                    </th>
                    <th colspan="4" class="p-2 tracking-wider bg-gray-100 whitespace-nowrap">
                        <span class="flex justify-center font-medium">
                            Current Schedule
                        </span>
                    </th>
                    <th colspan="4" class="p-2 tracking-wider bg-gray-200 whitespace-nowrap ">
                        <span class="flex justify-center font-medium">
                            New Schedule
                        </span>
                    </th>
                    <th colspan="2" class="p-2 tracking-wider bg-gray-100 whitespace-nowrap ">
                        <span class="flex justify-center font-medium">
                            Inclusive Dates
                        </span>
                    </th>
                </x-slot>
                <x-slot name="thead1">
                    <x-table.th name="No." />
                    <x-table.th name="Date Filed" />
                    <x-table.th name="Employee Name" />

                    <x-table.th class="border-l-2 border-gray-200" name="Schedule Category" />
                    <x-table.th name="Day" />
                    <x-table.th name="Time" />
                    <x-table.th name="Work Mode" />

                    <x-table.th class="border-l-2 border-gray-200" name="Schedule Category" />
                    <x-table.th name="Day" />
                    <x-table.th name="Time" />
                    <x-table.th name="Work Mode" />

                    <x-table.th class="border-l-2 border-gray-200" name="Start" />
                    <x-table.th class="border-r-2 border-gray-200" name="End" />
                    <x-table.th name="Reason" />
                    <x-table.th name="Status" />
                    <x-table.th name="Action" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($schedule_adjustments as $i => $schedule_adjustment)
                        <tr class="border-0 cursor-pointer hover:bg-[#4068b8]  hover:text-black">
                            <td class="p-3 whitespace-nowrap">
                                {{ $i + 1 }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ date('M. d, Y', strtotime($schedule_adjustment->created_at)) }}
                            </td>
                            <td class="flex items-center justify-start px-3 py-2 space-x-3 whitespace-nowrap"
                                style="position: sticky; left: 0; z-index: 1; background-color: rgba(243, 244, 246, var(--tw-bg-opacity));">
                                <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                    style="background-image: url({{ Storage::disk('hrim_gcs')->url($schedule_adjustment->user->photo_path . $schedule_adjustment->user->photo_name) }})">
                                </div>
                                <p>{{ $schedule_adjustment->user->name }}</p>
                            </td>
                            <td class="p-3 border-l-2 border-gray-200 whitespace-nowrap">
                                {{ $schedule_adjustment->currentWorkSchedule->name }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <span x-cloak x-show="{{ $schedule_adjustment->currentWorkSchedule->monday }}"
                                    class="text-1xs">Monday</span>
                                <span x-cloak x-show="{{ $schedule_adjustment->currentWorkSchedule->tuesday }}"
                                    class="text-1xs">Tuesday</span>
                                <span x-cloak x-show="{{ $schedule_adjustment->currentWorkSchedule->wednesday }}"
                                    class="text-1xs">Wednesday</span>
                                <span x-cloak x-show="{{ $schedule_adjustment->currentWorkSchedule->thursday }}"
                                    class="text-1xs">Thursday</span>
                                <span x-cloak x-show="{{ $schedule_adjustment->currentWorkSchedule->friday }}"
                                    class="text-1xs">Friday</span>
                                <span x-cloak x-show="{{ $schedule_adjustment->currentWorkSchedule->saturday }}"
                                    class="text-1xs">Saturday</span>
                                <span x-cloak x-show="{{ $schedule_adjustment->currentWorkSchedule->sunday }}"
                                    class="text-1xs">Sunday</span>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ date('h:i A', strtotime($schedule_adjustment->currentWorkSchedule->time_from)) . ' - ' . date('h:i A', strtotime($schedule_adjustment->currentWorkSchedule->time_to)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $schedule_adjustment->currentWorkSchedule->workMode->display }}
                            </td>

                            <td class="p-3 border-l-2 border-gray-200 whitespace-nowrap">
                                {{ $schedule_adjustment->newWorkSchedule->name }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <span x-cloak x-show="{{ $schedule_adjustment->newWorkSchedule->monday }}"
                                    class="text-1xs">Monday</span>
                                <span x-cloak x-show="{{ $schedule_adjustment->newWorkSchedule->tuesday }}"
                                    class="text-1xs">Tuesday</span>
                                <span x-cloak x-show="{{ $schedule_adjustment->newWorkSchedule->wednesday }}"
                                    class="text-1xs">Wednesday</span>
                                <span x-cloak x-show="{{ $schedule_adjustment->newWorkSchedule->thursday }}"
                                    class="text-1xs">Thursday</span>
                                <span x-cloak x-show="{{ $schedule_adjustment->newWorkSchedule->friday }}"
                                    class="text-1xs">Friday</span>
                                <span x-cloak x-show="{{ $schedule_adjustment->newWorkSchedule->saturday }}"
                                    class="text-1xs">Saturday</span>
                                <span x-cloak x-show="{{ $schedule_adjustment->newWorkSchedule->sunday }}"
                                    class="text-1xs">Sunday</span>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ date('h:i A', strtotime($schedule_adjustment->newWorkSchedule->time_from)) . ' - ' . date('h:i A', strtotime($schedule_adjustment->newWorkSchedule->time_to)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $schedule_adjustment->newWorkSchedule->workMode->display }}
                            </td>
                            <td class="p-3 border-l-2 border-gray-200 whitespace-nowrap">
                                {{ date('M. d, Y', strtotime($schedule_adjustment->inclusive_date_from)) }}
                            </td>
                            <td class="p-3 border-r-2 border-gray-200 whitespace-nowrap">
                                {{ date('M. d, Y', strtotime($schedule_adjustment->inclusive_date_to)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap ">
                                {{ $schedule_adjustment->reason }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <span
                                    class="px-6 py-1 text-xs rounded-full {{ $schedule_adjustment->finalStatus->code }}">{{ $schedule_adjustment->finalStatus->display }}</span>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-center space-x-3">
                                    <svg wire:click="action({id: '{{ $schedule_adjustment->id }}'}, 'view')"
                                        class="w-5 h-5 text-blue" data-toggle="tooltip" title="View"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                    </svg>
                                    @can('hrim_employee_schedule_adjustment_edit')
                                        <svg wire:click="action({id: '{{ $schedule_adjustment->id }}'}, 'edit')"
                                            class="w-5 h-5 text-blue" data-toggle="tooltip" title="Edit"
                                            aria-hidden="true" focusable="false" data-prefix="far" data-icon="edit"
                                            role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                            </path>
                                        </svg>
                                    @endcan
                                    @if ($schedule_adjustment->final_status_id == 1)
                                        <svg wire:click="action({id: '{{ $schedule_adjustment->id }}'}, 'delete')"
                                            class="w-5 h-5 text-red" data-toggle="tooltip" title="Delete"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                            <path fill="currentColor"
                                                d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
                                        </svg>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </x-slot>
            </x-table.table>
            <div class="px-1 pb-2">
                {{ $schedule_adjustments ? $schedule_adjustments->links() : '' }}
            </div>
        </div>
    </x-slot>
</x-form>
