<div wire:init="load">
    <x-loading />
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-6">
            <div class="grid grid-cols-2 gap-6">
                <div>
                    <x-label for="date_from" value="Date From" :required="true" />
                    <x-input type="date" name="date_from" wire:model.defer='date_from' disabled />
                    <x-input-error for="date" />
                </div>
                <div>
                    <x-label for="date_to" value="Date To" :required="true" />
                    <x-input type="date" name="date_to" wire:model.defer='date_to' />
                    <x-input-error for="date_to" />
                </div>
                <div>
                    <x-label for="actual_time_in" value="Actual Time In" :required="true" />
                    <x-input type="time" name="actual_time_in" wire:model.defer='actual_time_in' />
                    <x-input-error for="actual_time_in" />
                </div>
                <div>
                    <x-label for="actual_time_out" value="Actual Time Out" :required="true" />
                    <x-input type="time" name="actual_time_out" wire:model.defer='actual_time_out' />
                    <x-input-error for="actual_time_out" />
                </div>
                <div class="col-span-2">
                    <x-label for="reason_of_adjustment" value="Reason of Adjustment" :required="true" />
                    <x-select name="reason_of_adjustment" wire:model.defer='reason_of_adjustment'>
                        <option value=""></option>
                        @foreach ($tar_reason_references as $tar_reason_reference)
                            <option value="{{ $tar_reason_reference->id }}">
                                {{ $tar_reason_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="reason_of_adjustment" />
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <x-button type="button" wire:click="$emit('close_modal', 'create')" title="Cancel"
                    class="bg-white text-blue hover:bg-gray-100" />
                @if (!$tar || $tar->final_status_id <= 1)
                    <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
                @endif
            </div>
        </div>
    </form>
</div>
