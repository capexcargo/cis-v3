<div wire:init="load">
    <x-loading />
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3 x-2">
            <div class="grid grid-cols-1 gap-3">
                <div class="grid gap-4 grid-cols">
                    <div>
                        <x-label value="Division" :required="true" />
                        <x-select name="division" wire:model='division'>
                            <option value=""></option>
                            @foreach ($division_references as $division_reference)
                                <option value="{{ $division_reference->id }}">
                                    {{ $division_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="division" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols">
                    <div>
                        <x-label value="Department" :required="true" />
                        <x-select name="department_id" wire:model='department_id'>
                            <option value=""></option>
                            @foreach ($department_references as $department_reference)
                                <option value="{{ $department_reference->id }}">
                                    {{ $department_reference->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="department_id" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols">
                    <div>
                        <x-label value="Branch" :required="true" />
                        <x-select name="branch" wire:model='branch'>
                            <option value=""></option>
                            @foreach ($branch_references as $branch_reference)
                                <option value="{{ $branch_reference->id }}">
                                    {{ $branch_reference->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="branch" />
                    </div>
                </div>
                <div>
                    <x-label value="Type" :required="true" />
                    <x-select name="type" wire:model='type'>
                        <option value=""></option>
                        @foreach ($type_references as $type_reference)
                            <option value="{{ $type_reference->id }}">
                                {{ $type_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="type" />
                </div>
                <div>
                    <x-label value="First Approver" :required="true" />
                    <x-select name="first_approver" wire:model.defer='first_approver'>
                        <option value=""></option>
                        @foreach ($first_approver_references as $first_approver_reference)
                            <option value="{{ $first_approver_reference->id }}">
                                {{ $first_approver_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="first_approver" />
                </div>
                <div>
                    <x-label value="Second Approver" />
                    <x-select name="second_approver" wire:model.defer='second_approver'>
                        <option value=""></option>
                        @foreach ($second_approver_references as $second_approver_reference)
                            <option value="{{ $second_approver_reference->id }}">
                                {{ $second_approver_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="second_approver" />
                </div>
                <div>
                    <x-label value="Third Approver" />
                    <x-select name="third_approver" wire:model.defer='third_approver'>
                        <option value=""></option>
                        @foreach ($third_approver_references as $third_approver_reference)
                            <option value="{{ $third_approver_reference->id }}">
                                {{ $third_approver_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="third_approver" />
                </div>
                <div class="flex justify-end mt-6 space-x-3">
                    <x-button type="button" wire:click="$emit('close_modal', 'create')" title="Cancel"
                        class="bg-white text-blue hover:bg-gray-100" />
                    <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
