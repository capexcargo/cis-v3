<x-form x-data="{ search_form: false, create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_loa_management_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Create Loa</x-slot>
                <x-slot name="body">
                    @livewire('hrim.loa-management.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_loa_management_edit')
            @if ($edit_modal && $loa_management_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Edit Loa</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.loa-management.edit', ['id' => $loa_management_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>
    <x-slot name="header_title">Loa Management</x-slot>
    <x-slot name="header_button">
        @can('hrim_loa_management_add')
            <x-button type="button" wire:click="$set('create_modal', true)" title="Create Loa"
                class="bg-blue text-white hover:bg-[#002161]">
                <x-slot name="icon">
                    <svg class="w-3 h-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                </x-slot>
            </x-button>
        @endcan
    </x-slot>
    <x-slot name="search_form">
        <div>
            <x-label for="work_schedule" value="Work Schedule" />
            <x-input type="text" name="work_schedule" wire:model.debounce.500ms="work_schedule" />
            <x-input-error for="work_schedule" class="mt-2" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="hidden w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Division" />
                        <x-table.th name="Department" />
                        <x-table.th name="Branch" />
                        <x-table.th name="Type" />
                        <x-table.th name="First Approver" />
                        <x-table.th name="Second Approver" />
                        <x-table.th name="Third Approver" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($loa_managements as $loa_management)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loa_management->division->name }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loa_management->departments->display ?? ''}}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loa_management->branch->display }}

                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loa_management->loaType->display }}

                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loa_management->firstApprover->name }}

                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loa_management->secondApprover->name ?? 'Not Set' }}

                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loa_management->thirdApprover->name ?? 'Not Set' }}

                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        @can('hrim_loa_management_edit')
                                            <svg wire:click="action({'id': {{ $loa_management->id }}}, 'edit')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                </path>
                                            </svg>
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $loa_managements->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
