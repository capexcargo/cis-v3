<x-modal-form wire:init="loansAndLedgerReferences" x-data="{
    confirmation_approval_modal: '{{ $confirmation_approval_modal }}',
    confirmation_decline_modal: '{{ $confirmation_decline_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        <x-modal id="confirmation_approval_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                <h2 class="mb-3 text-lg text-left text-gray-900">
                    {{ $confirmation_message }}
                </h2>
                <div>
                    <x-label for="approved_amount" value="Approved Amount" />
                    <x-input type="number" name="approved_amount" wire:model.defer='approved_amount'>
                    </x-input>
                    <x-input-error for="approved_amount" />
                </div>
                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('confirmation_approval_modal', false)"
                        class="cursor-pointer px-12 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-md hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click="confirm({}, 'approve_loan')"
                        class="cursor-pointer flex-none px-12 py-1 mt-4 text-sm text-white rounded-md bg-blue">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>
        <x-modal id="confirmation_decline_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                <h2 class="mb-3 text-lg text-left text-gray-900">
                    {{ $confirmation_message }}
                </h2>
                <div>
                    <x-label for="decline_reason" value="Decline Reason" :required="true" />
                    <x-input type="text" name="decline_reason" wire:model.defer='decline_reason'>
                    </x-input>
                    <x-input-error for="decline_reason" />
                </div>
                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('confirmation_decline_modal', false)"
                        class="cursor-pointer px-12 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-md hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click="confirm({}, 'decline_loan')"
                        class="cursor-pointer flex-none px-12 py-1 mt-4 text-sm text-white rounded-md bg-blue">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>
    </x-slot>
    <x-slot name="body">
        {{-- @if ($loan_user_id == $user_id) --}}
        <div class="flex justify-center -mt-2">
            <div class="text-2xl font-semibold text-gray-800">{{ $employee_name }}<span>'s Loan
                    Summary</span>
            </div>
        </div>
        <div class="bg-white rounded-lg shadow-md">
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="No." />
                    <x-table.th name="Reference Number" />
                    <x-table.th name="Type" />
                    <x-table.th name="Principal Amount" />
                    <x-table.th name="Total Interest" hidden />
                    <x-table.th name="Terms" />
                    <x-table.th name="Term Type" />
                    <x-table.th name="Payment Start Date" />
                    <x-table.th name="Payment End Date" />
                    <x-table.th name="Purpose" />
                    <x-table.th name="Remaining Deduction Count" />
                    <x-table.th name="Total Loan Amount" />
                    <x-table.th name="Status" />
                    <x-table.th name="Approved Amount" />
                    <x-table.th name="Approver" />
                    <x-table.th name="By-Pass Approver" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($loans_and_ledgers as $i => $loans)
                        @foreach ($loans->loans as $i => $loans_and_ledger)
                            <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                <td class="p-3 whitespace-nowrap">{{ $i + 1 }}
                                </td>
                                <td class="p-3 whitespace-nowrap w-90">
                                    {{ $loans_and_ledger->reference_no }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loans_and_ledger->loanType->name }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($loans_and_ledger->principal_amount) }}
                                </td>
                                <td hidden class="p-3 whitespace-nowrap">500.00
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loans_and_ledger->terms }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loans_and_ledger->term_type == 1 ? 'Bi Monthly' : 'Monthly' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('M. d, Y', strtotime($loans_and_ledger->start_date)) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('M. d, Y', strtotime($loans_and_ledger->end_date)) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="w-50">
                                        {{ $loans_and_ledger->purpose }}
                                    </div>
                                </td>
                                <td class="w-12 p-3 whitespace-nowrap">
                                    {{ $loans_and_ledger->loanDetails->where('status', '=', 1)->sum('status') > 0 ? $loans_and_ledger->loanDetails->where('status', '=', 1)->sum('status') : $loans_and_ledger->terms }}
                                    / {{ $loans_and_ledger->terms }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($loans_and_ledger->principal_amount) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span
                                        class="{{ $loans_and_ledger->final_status == 1 ||
                                        ($loans_and_ledger->final_status == 3 &&
                                            $loans_and_ledger->third_status == 1 &&
                                            $loans_and_ledger->third_approver != '')
                                            ? 'text-orange bg-orange-light'
                                            : ($loans_and_ledger->final_status == 3
                                                ? 'text-blue bg-blue-300'
                                                : ($loans_and_ledger->final_status == 4
                                                    ? 'text-red bg-red-300'
                                                    : '')) }} 
                                                        text-xs rounded-full p-1 px-4">
                                        {{ $loans_and_ledger->finalStatus->display }}
                                        {{ $loans_and_ledger->final_status == 1 ||
                                        ($loans_and_ledger->final_status == 3 &&
                                            $loans_and_ledger->third_status == 1 &&
                                            $loans_and_ledger->third_approver != '')
                                            ? 'Requested'
                                            : ($loans_and_ledger->final_status == 3
                                                ? 'Approved'
                                                : ($loans_and_ledger->final_status == 4
                                                    ? 'Declined'
                                                    : '')) }}
                                    </span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loans_and_ledger->approved_amount != '' ? number_format($loans_and_ledger->approved_amount) : number_format($loans_and_ledger->principal_amount) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex gap-3">
                                        @if (!$loans_and_ledger->overrule_approver)
                                            @if ($loans_and_ledger->final_status == 1 ||
                                                ($loans_and_ledger->final_status == 3 &&
                                                    $loans_and_ledger->third_status == 1 &&
                                                    $loans_and_ledger->third_approver != ''))
                                                <svg wire:click="action({'id': {{ $loans_and_ledger->id }}, approver: 'is_approver'}, 'confirm_approve')"
                                                    class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700"
                                                    data-prefix="far" data-icon="approve" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM371.8 211.8C382.7 200.9 382.7 183.1 371.8 172.2C360.9 161.3 343.1 161.3 332.2 172.2L224 280.4L179.8 236.2C168.9 225.3 151.1 225.3 140.2 236.2C129.3 247.1 129.3 264.9 140.2 275.8L204.2 339.8C215.1 350.7 232.9 350.7 243.8 339.8L371.8 211.8z" />
                                                </svg>

                                                <svg wire:click="action({'id': {{ $loans_and_ledger->id }}, approver: 'is_approver'}, 'confirm_decline')"
                                                    class="w-4 h-4 cursor-pointer text-red hover:text-red-700"
                                                    data-prefix="far" data-icon="cancel-alt" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
                                                </svg>
                                            @endif
                                        @endif
                                    </div>
                                </td>
                                {{-- By-pass Approver --}}
                                <td class="p-3 whitespace-nowrap">
                                    @if ($divsionhead_id == Auth::user()->id && in_array($loans_and_ledger->overrule_approver, $seg_id) == false)
                                        <div class="flex gap-3 items-center">
                                            <svg wire:click="action({'id': {{ $loans_and_ledger->id }}, approver: 'is_bypass'}, 'confirm_approve')"
                                                class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700"
                                                data-prefix="far" data-icon="approve" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM371.8 211.8C382.7 200.9 382.7 183.1 371.8 172.2C360.9 161.3 343.1 161.3 332.2 172.2L224 280.4L179.8 236.2C168.9 225.3 151.1 225.3 140.2 236.2C129.3 247.1 129.3 264.9 140.2 275.8L204.2 339.8C215.1 350.7 232.9 350.7 243.8 339.8L371.8 211.8z" />
                                            </svg>
                                            <svg wire:click="action({'id': {{ $loans_and_ledger->id }}, approver: 'is_bypass'}, 'confirm_decline')"
                                                class="w-4 h-4 cursor-pointer text-red hover:text-red-700"
                                                data-prefix="far" data-icon="cancel-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
                                            </svg>
                                        </div>
                                    @elseif(in_array(Auth::user()->id, $seg_id))
                                        <div class="flex gap-3 items-center">
                                            <svg wire:click="action({'id': {{ $loans_and_ledger->id }}, approver: 'is_bypass'}, 'confirm_approve')"
                                                class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700"
                                                data-prefix="far" data-icon="approve" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM371.8 211.8C382.7 200.9 382.7 183.1 371.8 172.2C360.9 161.3 343.1 161.3 332.2 172.2L224 280.4L179.8 236.2C168.9 225.3 151.1 225.3 140.2 236.2C129.3 247.1 129.3 264.9 140.2 275.8L204.2 339.8C215.1 350.7 232.9 350.7 243.8 339.8L371.8 211.8z" />
                                            </svg>
                                            <svg wire:click="action({'id': {{ $loans_and_ledger->id }}, approver: 'is_bypass'}, 'confirm_decline')"
                                                class="w-4 h-4 cursor-pointer text-red hover:text-red-700"
                                                data-prefix="far" data-icon="cancel-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
                                            </svg>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                </x-slot>
            </x-table.table>
        </div>
        {{-- @else
            <div class="flex justify-center -mt-2">
                <div class="text-2xl font-semibold text-gray-800"><span>No Existing Loan</span>
                </div>
            </div>
        @endif --}}
    </x-slot>
</x-modal-form>
