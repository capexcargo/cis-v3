<div>
    <div class="flex justify-center -mt-2">
        <div class="text-2xl font-semibold text-gray-800">Payment Schedule</div>
    </div>
    <div class="flex justify-center text-md font-normal text-gray-800">
        {{ $remaining > 0 ? $remaining : $terms }}/{{ $terms }}
        Remaining
    </div>

    <div class="flex justify-start text-sm font-normal text-gray-800">Date Approved:</div>
    <div class="bg-white rounded-lg shadow-md">
        <x-table.table>
            <x-slot name="thead">
                <th class="w-4 p-3 whitespace-nowrap border-2 border-b-0">No.</th>
                <th class="p-3 whitespace-nowrap border-2 border-b-0">Due Date</th>
                <th class="p-3 whitespace-nowrap border-2 border-b-0">Amount</th>
                <th class="p-3 whitespace-nowrap border-2 border-b-0">Interest</th>
                <th class="p-3 whitespace-nowrap border-2 border-b-0">Total</th>
                <th class="p-3 whitespace-nowrap border-2 border-b-0">Status</th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($loan_payments_references as $i => $loan_payment)
                    <tr>
                        <td class="w-4 p-3 whitespace-nowrap border-2 border-b-0">
                            {{ $i + 1 }}
                        </td>
                        <td class="p-3 whitespace-nowrap border-2 border-b-0">
                            {{ date('M.', mktime(0, 0, 0, $loan_payment->month, 10)) }}
                            {{ $loan_payment->cutoff == 1 ? '10' : '25' }},
                            {{ $loan_payment->year }}
                        </td>
                        <td class="p-3 whitespace-nowrap border-2 border-b-0">
                            {{ $loan_payment->amount }}
                        </td>
                        <td class="p-3 whitespace-nowrap border-2 border-b-0">
                            {{ $loan_payment->interest }}
                        </td>
                        <td class="p-3 whitespace-nowrap border-2 border-b-0">
                            {{ $loan_payment->amount + $loan_payment->interest }}
                        </td>
                        <td class="p-3 whitespace-nowrap border-2 border-b-0">
                            <span
                                class="{{ $loan_payment->status == 1 ? 'text-orange bg-orange-light px-4' : 'text-green bg-green-100 px-6' }} 
                                            text-xs rounded-full p-1">
                                {{-- {{ $loan_payment->status }} --}}
                                {{ $loan_payment->status == 1 ? 'Unpaid' : 'Paid' }}
                            </span>
                        </td>
                    </tr>
                @endforeach
            </x-slot>
        </x-table.table>
    </div>
</div>
