<x-form x-data="{
    search_form: false,
    view_loan_modal: '{{ $view_loan_modal }}',
    view_approval_modal: '{{ $view_approval_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @if ($loans_and_ledger_id && $view_loan_modal)
            <x-modal id="view_loan_modal" size="w-10/12 xs:w-10/12 sm:w-10/12 md:w-4/5">
                <x-slot name="title"></x-slot>
                <x-slot name="body">
                    @livewire('hrim.compensation-and-benefits.loans-and-ledger-monitoring.view', ['id' => $loans_and_ledger_id])
                </x-slot>
            </x-modal>
        @endif
        @if ($loans_and_ledger_id && $view_approval_modal)
            <x-modal id="view_approval_modal" size="w-10/12 xs:w-10/12 sm:w-10/12 md:w-1/4">
                <x-slot name="title"></x-slot>
                <x-slot name="body">
                    @livewire('hrim.compensation-and-benefits.loans-and-ledger-monitoring.view-approval', ['id' => $loans_and_ledger_id])
                </x-slot>
            </x-modal>
        @endcan
</x-slot>
<x-slot name="header_title">Loans and Ledger Monitoring</x-slot>
<x-slot name="body">
    <div class="grid grid-cols-5 gap-2 text-sm md:w-3/4" wire:init="">
        <div class="w-40">
            <div>
                <x-transparent.input type="number" label="Payout Year" name="payout_year"
                    wire:model.debounce.500ms="payout_year" />
            </div>
        </div>
        <div class="w-40">
            <div>
                <x-transparent.input type="date" label="Payout Date" name="payout_date"
                    wire:model.debounce.500ms="payout_date" />
            </div>
        </div>
        <div class="w-40">
            <div wire:init="branchReferences">
                <x-transparent.select value="{{ $branch }}" label="Branch" name="branch" wire:model="branch">
                    <option value=""></option>
                    @foreach ($branch_references as $branch_ref)
                        <option value="{{ $branch_ref->id }}">
                            {{ $branch_ref->display }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div>
        </div>
    </div>
    <div class="grid grid-cols-5 gap-2 text-sm md:w-3/4" wire:init="">
        <div class="w-40">
            <div>
                <x-transparent.input type="text" label="Employee ID" name="employee_id"
                    wire:model.debounce.500ms="employee_id" />
            </div>
        </div>
        <div class="w-40">
            <div>
                <x-transparent.input type="text" label="Employee Name" name="employee_name"
                    wire:model.debounce.500ms="employee_name" />
            </div>
        </div>
    </div>

    <div class="bg-white rounded-lg shadow-md">
        <x-table.table>
            <x-slot name="thead">
                <x-table.th name="No." />
                <x-table.th name="Name" />
                <x-table.th name="Employee ID" />
                <x-table.th name="Branch" />
                <x-table.th name="Total Loan Amount" />
                <x-table.th name="Unpaid Loan Amount" />
                <x-table.th name="Payment End Date" />
                <x-table.th name="Action" />
            </x-slot>
            <x-slot name="tbody">
                @foreach ($loans_and_ledger as $i => $loan_and_ledger)
                    @if ($loan_and_ledger)
                        <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                            <td class="p-3 whitespace-nowrap">
                                <p class="flex-initial ">{{ $i + 1 }}.
                            </td>
                            <td class="p-3 whitespace-nowrap w-90">
                                <div class="flex space-x-2 w-50">
                                    <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                        style="background-image: url({{ Storage::disk('hrim_gcs')->url($loan_and_ledger->photo_path . $loan_and_ledger->photo_name) }})">
                                    </div>
                                    <div class="flex items-center text-sm">
                                        <div>
                                            <p class="flex-initial font-semibold">
                                                {{ $loan_and_ledger->name }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </td>

                            <td class="p-3 whitespace-nowrap">
                                {{ $loan_and_ledger->userDetails->employee_number }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                @if ($loan_and_ledger->userDetails->branch_id != '')
                                    {{ $loan_and_ledger->userDetails->branch->display }}
                                @endif
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                @if ($loan_and_ledger->loans[$i]->third_appover != '')
                                    {{ $loan_and_ledger->loans->where('second_status', 3)->where('third_status', 1)->where('final_status', 3)->sum('principal_amount') }}
                                @else
                                    {{ $loan_and_ledger->loans->where('final_status', 3)->sum('principal_amount') }}
                                @endif
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{-- @if ($loan_and_ledger->loans[$i]->third_appover != '')
                                        {{ $loan_and_ledger->loans->where('second_status', 3)->where('third_status', 1)->where('final_status', 3)->sum('principal_amount') }}
                                    @else
                                        {{ $loan_and_ledger->loans->where('final_status', 3)->sum('principal_amount') }}
                                    @endif --}}
                                0
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ date('M. d, Y', strtotime($loan_and_ledger->loans[$i]->end_date)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex gap-3">
                                    {{-- @can('hrim_kpi_management_edit') --}}
                                    <svg wire:click="action({'id': {{ $loan_and_ledger->id }}}, 'view_loan')"
                                        class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700"
                                        data-prefix="far" data-icon="approve" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                    </svg>
                                    {{-- @endcan --}}
                                    <svg wire:click="action({'id': {{ $loan_and_ledger->loans[$i]->id }}}, 'view_approval')"
                                        class="w-4 h-4 cursor-pointer text-blue hover:text-blue-800 "
                                        data-prefix="far" data-icon="view-alt" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 512">
                                        <path fill="currentColor"
                                            d="M64 360C94.93 360 120 385.1 120 416C120 446.9 94.93 472 64 472C33.07 472 8 446.9 8 416C8 385.1 33.07 360 64 360zM64 200C94.93 200 120 225.1 120 256C120 286.9 94.93 312 64 312C33.07 312 8 286.9 8 256C8 225.1 33.07 200 64 200zM64 152C33.07 152 8 126.9 8 96C8 65.07 33.07 40 64 40C94.93 40 120 65.07 120 96C120 126.9 94.93 152 64 152z">
                                        </path>
                                    </svg>
                                </div>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </x-slot>
        </x-table.table>
        <div class="px-1 pb-2">
            {{-- {{ $loans_and_ledger->links() }} --}}
        </div>
    </div>
</x-slot>
</x-form>
