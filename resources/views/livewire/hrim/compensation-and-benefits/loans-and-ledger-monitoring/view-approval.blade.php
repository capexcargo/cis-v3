<div>
    <div class="bg-white rounded-lg shadow-md">
        <x-table.table>
            <x-slot name="thead">
                <th class="p-3 whitespace-nowrap border-2 border-b-0 text-gray-400">Level of Approval</th>
                <th class="p-3 whitespace-nowrap border-2 border-b-0 text-gray-400">Status</th>
            </x-slot>
            <x-slot name="tbody">
                <tr>
                    <td class="p-3 whitespace-nowrap border-2 border-b-0">
                        Division Head
                    </td>
                    <td class="p-3 whitespace-nowrap border-2 border-b-0" wire:init="">
                        <span
                            class="p-1 {{ $final_status == 1 || ($final_status == 3 && $third_status == 1 && $third_approver != '')
                                ? 'text-orange '
                                : ($final_status == 3
                                    ? 'text-blue '
                                    : ($final_status == 4
                                        ? 'text-red '
                                        : '')) }}">
                            {{ $final_status == 1 || ($final_status == 3 && $third_status == 1 && $third_approver != '')
                                ? 'Requested'
                                : ($final_status == 3
                                    ? 'Approved'
                                    : ($final_status == 4
                                        ? 'Declined'
                                        : '')) }}
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="p-3 whitespace-nowrap border-2 border-b-0">
                        Hr Admin
                    </td>
                    <td class="p-3 whitespace-nowrap border-2 border-b-0 text-orange">Requested
                    </td>
                </tr>
            </x-slot>
        </x-table.table>
    </div>
</div>
