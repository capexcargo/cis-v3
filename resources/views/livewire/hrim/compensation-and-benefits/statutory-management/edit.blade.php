<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">

                <div class="grid grid-cols-1 gap-4">
                    <div>
                        <x-label for="statutor_benefit_type" value="Type of Statutory Benefits" :required="true" />
                        <x-input type="text" name="statutor_benefit_type" wire:model.defer='statutor_benefit_type'>
                        </x-input>
                        <x-input-error for="statutor_benefit_type" />
                    </div>
                    <div>
                        <x-label for="description" value="Benefit Description" :required="true" />
                        <x-textarea type="description" name="description" wire:model.defer='description'>
                        </x-textarea>
                        <x-input-error for="description" />
                    </div>
                </div>


                <div class="flex justify-end mt-6 space-x-3">
                    <button type="button" wire:click="$emit('close_modal', 'edit')"
                        class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
