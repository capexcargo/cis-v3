<x-form wire:init="load" x-data="{
    search_form: false,
    generate_payroll_modal: '{{ $generate_payroll_modal }}',
    confirmation_modal_initial: '{{ $confirmation_modal_initial }}',
    confirmation_modal_final: '{{ $confirmation_modal_final }}',
    generate_payslip_modal: '{{ $generate_payslip_modal }}',
    checkboxes: '{{ true }}',
    selectall: false,
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @if ($generate_payroll_modal)
            <x-modal id="generate_payroll_modal" size="md:inset-0 h-modal ">
                <x-slot name="body">
                    <h2 class="mb-3 text-lg text-left text-gray-900">
                        Generate Payroll
                    </h2>
                    <div class="flex justify-center space-x-3">
                        <button type="button" wire:click="export"
                            class="flex-none px-5 mt-4 text-sm text-white rounded-lg bg-blue">
                            Download</button>
                        <button type="button" wire:click="closePayroll"
                            class="flex-none px-5 mt-4 text-sm text-white rounded-lg bg-blue">
                            Email Payroll Report/Close Payroll</button>
                    </div>
                </x-slot>
            </x-modal>
        @endif
        @can('hrim_payroll_generatepayslip')
            @if ($generate_payslip_modal)
                <x-modal id="generate_payslip_modal" size="md:inset-0 h-modal ">
                    <x-slot name="body">
                        <h2 class="mb-3 text-lg text-left text-gray-900">
                            Generate Payslip
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button type="button" wire:click="action({}, 'generate_payslip_initial')"
                                class="flex-none px-5 py-1 mt-4 text-sm text-white rounded-lg bg-blue">
                                Initial Payslip</button>
                            <button type="button" wire:click="action({}, 'generate_payslip_final')"
                                class="flex-none px-5 py-1 mt-4 text-sm text-white rounded-lg bg-blue">
                                Final Payslip</button>
                        </div>
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @if ($confirmation_modal_initial)
            <x-modal id="confirmation_modal_initial" size="w-10/12 xs:w-10/12 sm:w-2/3 lg:w-1/2 md:w-1/4">
                <x-slot name="body">
                    <h2 class="mb-3 text-lg text-center text-gray-900">
                        {{ $confirmation_message_initial }}
                    </h2>
                    <div class="flex justify-center space-x-3">
                        <button type="button" wire:click="$set('confirmation_modal_initial', false)"
                            class="px-5 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                        <button type="button" wire:click="generatePayslipInitial"
                            class="flex-none px-5 mt-4 text-sm text-white rounded-lg bg-blue">
                            Yes</button>
                    </div>
                </x-slot>
            </x-modal>
        @endif
        @if ($confirmation_modal_final)
            <x-modal id="confirmation_modal_final" size="w-10/12 xs:w-10/12 sm:w-2/3 lg:w-1/2 md:w-1/4">
                <x-slot name="body">
                    <h2 class="mb-3 text-lg text-center text-gray-900">
                        {{ $confirmation_message_final }}
                    </h2>
                    <div class="flex justify-center space-x-3">
                        <button type="button" wire:click="$set('confirmation_modal_final', false)"
                            class="px-5 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                        <button type="button" wire:click="generatePayslipFinal"
                            class="flex-none px-5 mt-4 text-sm text-white rounded-lg bg-blue">
                            Yes</button>
                    </div>
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">CaPEx Payroll</x-slot>
    <x-slot name="header_button">
        @can('hrim_payroll_generatepayroll')
            <button wire:click="action({}, 'generate_payroll')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg hidden class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Generate Payroll
                </div>
            </button>
        @endcan
        @can('hrim_payroll_generatebankfile')
            <button wire:click="action({}, 'generate_bankfile')"
                class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg hidden class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Generate Bank File
                </div>
            </button>
        @endcan
        @can('hrim_payroll_generatepayslip')
            <button wire:click="action({}, 'generate_payslip')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg hidden class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Generate Payslip
                </div>
            </button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-5 gap-2 text-sm md:w-3/4" wire:init="">
            <div class="w-40">
                <div>
                    <x-transparent.input value="{{ $year }}" type="number" label="Year" name="year"
                        wire:model.debounce.500ms="year" />
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.select value="{{ $month }}" label="Month" name="month" wire:model="month">
                        <option value=""></option>
                        @foreach ($month_references as $month_reference)
                            <option value="{{ $month_reference->id }}">
                                {{ $month_reference->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.select value="{{ $cut_off }}" label="Cut Off" name="cut_off"
                        wire:model="cut_off">
                        <option value=""></option>
                        <option value=1>First</option>
                        <option value=2>Second</option>
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div wire:init="branchReferences">
                    <x-transparent.select value="{{ $branch }}" label="Branch" name="branch"
                        wire:model="branch">
                        <option value=""></option>
                        @foreach ($branch_references as $branch_ref)
                            <option value="{{ $branch_ref->id }}">
                                {{ $branch_ref->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div wire:init="employeeCategoryReferences">
                    <x-transparent.select value="{{ $employee_category }}" label="Employee Category"
                        name="employee_category" wire:model="employee_category">
                        <option value=""></option>
                        @foreach ($employee_category_references as $employee_category)
                            <option value="{{ $employee_category->id }}">
                                {{ $employee_category->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
        </div>
        <div class="grid grid-cols-5 gap-2 text-sm md:w-3/4" wire:init="">
            <div class="w-40">
                <div>
                    <x-transparent.input type="text" label="Employee Name" name="employee_name"
                        wire:model.debounce.500ms="employee_name" />
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.input type="text" label="Employee ID" name="employee_id"
                        wire:model.debounce.500ms="employee_id" />
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.input type="text" label="Employee Code" name="employee_code"
                        wire:model.debounce.500ms="employee_code" />
                </div>
            </div>
            <div class="w-40">
                <div wire:init="divisionReferences">
                    <x-transparent.select value="{{ $division }}" label="Division" name="division"
                        wire:model="division">
                        <option value=""></option>
                        @foreach ($division_references as $division)
                            <option value="{{ $division->id }}">
                                {{ $division->name }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div wire:init="jobRankReferences">
                    <x-transparent.select value="{{ $job_rank }}" label="Job Rank" name="job_rank"
                        wire:model="job_rank">
                        <option value=""></option>
                        @foreach ($job_rank_references as $job_rank)
                            <option value="{{ $job_rank->id }}">
                                {{ $job_rank->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
        </div>
        <div class="overflow-auto tableFixHead">
            <div class="relative block bg-white rounded-lg shadow-md table-wrp">
                <table class="min-w-full font-medium">
                    <thead class="text-xs text-gray-700 capitalize bg-white border-0 cursor-pointer">
                        <tr class="text-xs font-light text-left text-gray-500 bg-white border-0 cursor-pointer">
                            <th class="relative px-2 border-2 border-gray-900 border-x-4">
                                <div class="w-12 text-xs font-normal text-blue">Send Payslip to</div>
                                <div class="mt-4">
                                    <div class="flex gap-2 mt-2 flex-nowrap">
                                        <span class="w-20 text-gray-700">
                                            {{-- <input type="checkbox" class="form-checkbox" x-model="enable_all"
                                                id="enable_all" name="enable_all" value=""
                                                wire:model=""><span class="ml-2"> --}}
                                            {{-- <input class="form-check-input" type="checkbox"
                                                @click="selectall=!selectall"> All</span>
                                                 --}}

                                            <input wire:model="selectAll" type="checkbox" />
                                        </span>
                                        <span class="w-12 text-gray-700">No.</span>
                                        <span class="w-48 mr-4 text-gray-700">Name</span>
                                    </div>
                                </div>
                            </th>
                            <th class="text-center border-2 border-gray-900 border-x-4">
                                <div class="text-lg font-normal border-b-2 border-gray-900 text-blue">Employee Payroll
                                    Information
                                </div>
                                <div class="px-2 mt-6 font-semibold">
                                    <div class="flex gap-2 mt-2 flex-nowrap">
                                        <span class="w-20 text-gray-700">Employee ID</span>
                                        <span class="text-gray-700 w-28">Employee Code</span>
                                        <span class="text-gray-700 w-44">Payroll Bank Account No.</span>
                                    </div>
                                </div>
                            </th>
                            <th class="text-center border-2 border-gray-900 border-x-4">
                                <div class="text-lg font-normal border-b-2 border-gray-900 text-blue">Employee
                                    Information</div>
                                <div class="px-2 mt-6 font-semibold">
                                    <div class="flex mt-2 text-left flex-nowrap">
                                        <span class="text-gray-700 w-28">Date Hired</span>
                                        <span class="w-40 text-gray-700">Employment Status</span>
                                        <span class="w-40 text-gray-700 whitespace-pre-line">Job Rank</span>
                                        <span class="w-40 text-gray-700 whitespace-pre-line">Position</span>
                                        <span class="w-20 text-gray-700">Branch</span>
                                    </div>
                                </div>
                            </th>
                            <th class="w-1/2 text-center border-2 border-gray-900 border-x-4">
                                <div class="text-lg font-normal border-b-2 text-blue">Salary Details</div>
                                <div class="flex flex-nowrap">
                                    <div class="font-semibold text-left">
                                        <div class="text-sm font-normal text-center bg-gray-200">Earnings</div>
                                        <div class="flex justify-between gap-2 px-2 mt-2">
                                            <span class="w-20 text-gray-700">Basic Pay</span>
                                            <span class="w-20 text-gray-700">COLA</span>
                                            <span class="w-20 text-gray-700">Gross Pay</span>
                                            <span class="w-20 text-gray-700">Holiday Pay</span>
                                            <span class="w-20 text-gray-700">ND Pay</span>
                                            <span class="w-20 text-gray-700">OT Pay</span>
                                            <span class="w-20 text-gray-700">Rest Day Pay</span>
                                            <span class="w-20 text-gray-700">Leave w/ Pay</span>
                                        </div>
                                    </div>
                                    <div class="p-0 m-0 border border-gray-900"></div>
                                    <div class="font-semibold text-left">
                                        <div class="text-sm font-normal text-center bg-gray-200">Deductions</div>
                                        <div class="flex justify-between gap-2 px-2 mt-2">
                                            <span class="w-20 text-gray-700">Leave w/o Pay</span>
                                            <span class="w-20 text-gray-700">Tardiness</span>
                                            <span class="w-20 text-gray-700">Undertime</span>
                                            <span class="w-20 text-gray-700">Absences</span>
                                            <span class="text-gray-700 w-44">Government Contributions</span>
                                            <span class="w-40 text-gray-700">Government Loans</span>
                                            <span class="w-40 text-gray-700">Withholding Tax</span>
                                            <span class="w-40 text-gray-700">Other Loans & Deductions</span>
                                        </div>
                                    </div>

                                </div>
                            </th>
                            <th class="text-center border-2 border-gray-900 border-x-4">
                                <div class="text-lg font-normal border-b-2 border-gray-900 text-blue">&nbsp;</div>
                                <div class="px-2 mt-6 font-semibold">
                                    <div class="flex gap-2 mt-2 flex-nowrap">
                                        <span class="mr-2 text-gray-700 w-28 whitespace-nowrap">Admin
                                            Adjustments</span>
                                    </div>
                                </div>
                            </th>
                            <th class="text-center border-2 border-gray-900 border-x-4">
                                <div class="text-lg font-normal border-b-2 border-gray-900 text-blue">&nbsp;</div>
                                <div class="px-2 mt-6 font-semibold">
                                    <div class="flex gap-2 mt-2 flex-nowrap">
                                        <span class="w-24 text-gray-700">DLB</span>
                                    </div>
                                </div>
                            </th>
                            <th class="text-center border-2 border-gray-900 border-x-4">
                                <div class="text-lg font-normal border-b-2 border-gray-900 text-blue">&nbsp;</div>
                                <div class="px-2 mt-6 font-semibold">
                                    <div class="flex gap-2 mt-2 flex-nowrap">
                                        <span class="w-24 text-gray-700">Net Pay</span>
                                    </div>
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="text-sm bg-gray-100 border-0">
                        @foreach ($payrolls as $i => $payroll)
                            <tr>
                                <td class="w-12 px-2 py-2 border-2 border-gray-900 border-x-4">
                                    <div class="flex gap-2 mt-2 flex-nowrap">
                                        <span class="w-20">
                                            {{-- <input type="checkbox" class="form-checkbox" x-model="enable"
                                                id="enable" name="enable" value="" wire:model=""> --}}
                                            {{-- <input class="item-select-input" type="checkbox"
                                                name="{{ $payroll->employee_number }}"
                                                value="{{ $payroll->employee_number }}" checked wire:model="selected.{{ $payroll->employee_number }}" > --}}


                                            <input wire:model="selected" type="checkbox"
                                                name={{ $payroll->employee_number }}
                                                value="{{ $payroll->employee_number }}">
                                        </span>

                                        <span class="w-12"> {{ $loop->index + 1 }}</span>
                                        <span class="">
                                            <div class="flex space-x-2 w-50">
                                                <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                                    style="background-image: url({{ Storage::disk('hrim_gcs')->url($payroll->photo_path . $payroll->photo_name) }})">
                                                </div>
                                                <div class="flex items-center w-40 text-sm">
                                                    <p class="flex-initial">{{ $payroll->first_name }}
                                                        {{ $payroll->last_name }}</p>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </td>
                                <td class="px-4 text-left border-2 border-gray-900 border-x-4">
                                    <div class="flex gap-2 mt-2 flex-nowrap">
                                        <span class="w-20">{{ $payroll->employee_number }}</span>
                                        <span class="w-28"></span>
                                        <span class="w-40" style="margin-left: 2px"></span>
                                    </div>
                                </td>
                                <td class="px-2 text-left border-2 border-gray-900 border-x-4">
                                    <div class="flex py-2 mt-2 flex-nowrap">
                                        <span class="w-28">{{ $payroll->hired_date }}</span>
                                        <span class="w-40">{{ $payroll->emp_status }}</span>
                                        <span class="w-40 whitespace-pre-line">{{ $payroll->job_rank }}</span>
                                        <span class="w-40 whitespace-pre-line">{{ $payroll->position }}</span>
                                        <span class="w-20" style="margin-left: 2px">{{ $payroll->branch }}</span>
                                    </div>
                                </td>
                                <td class="w-1/2 text-center border-2 border-gray-900 border-x-4">
                                    <div class="flex flex-nowrap">
                                        <div class="py-2 text-left">
                                            <div class="flex justify-between gap-2 px-2 mt-2">
                                                <span
                                                    class="w-20">{{ number_format($payroll->basic_pay / 2, 2) }}</span>
                                                <span class="w-20">{{ number_format($payroll->cola / 2, 2) }}</span>
                                                <span
                                                    class="w-20">{{ number_format($payroll->gross / 2, 2) }}</span>
                                                <span class="w-20">{{ number_format($payroll->holidayp, 2) }}</span>
                                                <span class="w-20">{{ number_format($payroll->ndval, 2) }}</span>
                                                <span class="w-20">{{ number_format($payroll->ot_pay, 2) }}</span>
                                                <span
                                                    class="w-20">{{ number_format(round($payroll->restdaypays, 2), 2) }}</span>
                                                <span
                                                    class="w-20">{{ number_format(round($payroll->leaveswtpay * ($payroll->basic_pay / 26), 2), 2) }}</span>
                                            </div>
                                        </div>
                                        <div class="p-0 m-0 border border-gray-900"></div>
                                        <div class="py-2 text-left">
                                            <div class="flex justify-between gap-2 px-2 mt-2">
                                                <span
                                                    class="w-20">{{ number_format(round($payroll->leaves * ($payroll->basic_pay / 26), 2), 2) }}</span>
                                                <span
                                                    class="w-20">{{ number_format($payroll->tardiness, 2) }}</span>
                                                <span
                                                    class="w-20">{{ number_format($payroll->undertime, 2) }}</span>
                                                <span
                                                    class="w-20">{{ number_format(round($payroll->absentcount * ($payroll->basic_pay / 26), 2), 2) }}</span>
                                                <span
                                                    class="w-44">{{ $cut_off == 1 ? number_format($payroll->philhealth + $payroll->pagibig, 2) : 0 }}</span>
                                                <span
                                                    class="w-40">{{ number_format($payroll->loansgovt, 2) }}</span>
                                                <span class="w-40">0.00</span>
                                                <span class="w-40"
                                                    style="margin-left: -3px">{{ number_format($payroll->loans, 2) }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="w-1/4 px-3 text-center border-2 border-gray-900 border-x-4">
                                    <div class="flex mr-2 flex-nowrap" style="margin-left: 4px">
                                        {{ number_format($payroll->aadpay, 2) }}
                                    </div>
                                </td>
                                <td class="w-1/4 px-2 text-center border-2 border-gray-900 border-x-4">
                                    <div class="flex flex-nowrap">
                                        {{ number_format($payroll->dlbpay, 2) }}
                                    </div>
                                </td>
                                <td class="w-1/4 px-2 ml-3 text-center border-2 border-gray-900 border-x-4">
                                    <div class="flex flex-nowrap" style="margin-right: 2px">
                                        {{ number_format(
                                            $payroll->gross / 2 +
                                                $payroll->ot_pay +
                                                $payroll->holidayp +
                                                $payroll->ndval +
                                                round($payroll->restdaypays, 2) +
                                                $payroll->dlbpay +
                                                $payroll->aadpay +
                                                round($payroll->leaveswtpay * ($payroll->basic_pay / 26), 2) -
                                                (round($payroll->leaves * ($payroll->basic_pay / 26), 2) +
                                                    $payroll->tardiness +
                                                    $payroll->undertime +
                                                    (round($payroll->absentcount * ($payroll->basic_pay / 26), 2) +
                                                        ($cut_off == 1 ? $payroll->philhealth + $payroll->pagibig : $payroll->loansgovt)) +
                                                    $payroll->loans),
                                            2,
                                        ) }}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </x-slot>
</x-form>

@push('heads')
    <style>
        .tableFixHead {
            table-layout: fixed;
            border-collapse: collapse;
            margin-bottom: 20%;
        }

        .tableFixHead tbody {
            display: block;
            width: 100%;
            overflow: auto;
            height: 500px;
        }

        .tableFixHead thead tr {
            display: block;
            overflow-y: auto;
        }
    </style>
@endpush
