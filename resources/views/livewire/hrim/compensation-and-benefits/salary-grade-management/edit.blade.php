<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">

                <div class="grid grid-cols-1 gap-4">
                    <div>
                        <x-label for="grade" value="Salary Grade" :required="true" />
                        <x-input type="text" name="grade" wire:model.defer='grade'>
                        </x-input>
                        <x-input-error for="grade" />
                    </div>
                    <div>
                        <x-label for="minimun_amount" value="Minimum Amount" :required="true" />
                        <x-input type="minimun_amount" name="minimun_amount" wire:model.defer='minimun_amount'>
                        </x-input>
                        <x-input-error for="minimun_amount" />
                    </div>
                    <div>
                        <x-label for="maximum_amount" value="Maximum Amount" :required="true" />
                        <x-input type="maximum_amount" name="maximum_amount" wire:model.defer='maximum_amount'>
                        </x-input>
                        <x-input-error for="maximum_amount" />
                    </div>
                </div>


                <div class="flex justify-end mt-6 space-x-3">
                    <button type="button" wire:click="$emit('close_modal', 'edit')"
                        class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
