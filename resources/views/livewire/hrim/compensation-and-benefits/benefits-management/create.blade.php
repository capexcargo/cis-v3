<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}'
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <h2 class="text-xl text-left ">
                Are you sure you want to submit this new Benefits?
            </h2>

            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    NO
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    YES
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">


                <div class="grid grid-cols-1 gap-4">
                    <div>
                        <x-label for="benefit_type" value="Type of Benefits" :required="true" />
                        <x-input type="text" name="benefit_type" wire:model.defer='benefit_type'>
                        </x-input>
                        <x-input-error for="benefit_type" />
                    </div>
                    <div>
                        <x-label for="description" value="Benefit Description" :required="true" />
                        <x-textarea type="description" name="description" wire:model.defer='description'>
                        </x-textarea>
                        <x-input-error for="description" />
                    </div>
                </div>

                <div class="flex justify-end mt-6 space-x-3">
                    <button type="button" wire:click="$emit('close_modal', 'create')"
                        class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
