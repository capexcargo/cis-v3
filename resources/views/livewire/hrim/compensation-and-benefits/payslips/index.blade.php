<x-form wire:init="load" x-data="{
    search_form: false,
    view_modal: '{{ $view_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @if ($view_modal && $user_id)
            <x-modal id="view_modal" size="w-10/12 xs:w-10/12 sm:w-2/3 md:w-3/4 lg:w-3/4">
                <x-slot name="title"></x-slot>
                <x-slot name="body">
                    @livewire('hrim.compensation-and-benefits.payslips.view', ['id' => $user_id, 'request' => $this->getRequest()])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">Payslips</x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-5 gap-2 text-sm md:w-3/4" wire:init="">
            <div class="w-40">
                <div>
                    <x-transparent.input value="{{ $year }}" type="number" label="Year" name="year"
                    wire:model.debounce.500ms="year" />
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.select label="Month" name="month" wire:model="month">
                        <option value=""></option>
                        @foreach ($month_references as $month_reference)
                            <option value="{{ $month_reference->id }}">
                                {{ $month_reference->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.select label="Cut Off" name="cut_off" wire:model="cut_off">
                        <option value=""></option>
                        <option value=1>First</option>
                        <option value=2>Second</option>
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div wire:init="branchReferences">
                    <x-transparent.select value="{{ $branch }}" label="Branch" name="branch" wire:model="branch">
                        <option value=""></option>
                        @foreach ($branch_references as $branch_ref)
                            <option value="{{ $branch_ref->id }}">
                                {{ $branch_ref->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div wire:init="employeeCategoryReferences">
                    <x-transparent.select value="{{ $employee_category }}" label="Employee Category"
                        name="employee_category" wire:model="employee_category">
                        <option value=""></option>
                        @foreach ($employee_category_references as $employee_category)
                        <option value="{{ $employee_category->id }}">
                            {{ $employee_category->display }}
                        </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
        </div>
        <div class="grid grid-cols-5 gap-2 text-sm md:w-3/4" wire:init="">
            <div class="w-40">
                <div>
                    <x-transparent.input type="text" label="Employee Name" name="employee_name"
                        wire:model.debounce.500ms="employee_name" />
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.input type="text" label="Employee ID" name="employee_id"
                        wire:model.debounce.500ms="employee_id" />
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.input type="text" label="Employee Code" name="employee_code"
                        wire:model.debounce.500ms="employee_code" />
                </div>
            </div>
            <div class="w-40">
                <div wire:init="divisionReferences">
                    <x-transparent.select value="{{ $division }}" label="Division" name="division"
                        wire:model="division">
                        <option value=""></option>
                        @foreach ($division_references as $division)
                            <option value="{{ $division->id }}">
                                {{ $division->name }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div wire:init="jobRankReferences">
                    <x-transparent.select value="{{ $job_rank }}" label="Job Rank" name="job_rank"
                        wire:model="job_rank">
                        <option value=""></option>
                        @foreach ($job_rank_references as $job_rank)
                            <option value="{{ $job_rank->id }}">
                                {{ $job_rank->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
        </div>

        <div class="bg-white rounded-lg shadow-md">
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="No." />
                    <x-table.th name="Name" />
                    <x-table.th name="Employee ID" />
                    <x-table.th name="Branch" />
                    <x-table.th name="Employee Category" />
                    <x-table.th name="Division" />
                    <x-table.th name="Job Rank" />
                    <x-table.th name="Action" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($payrolls as $payroll)
                    <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                        <td class="p-3 whitespace-nowrap">
                            <p class="flex-initial ">{{$loop->index + 1}}</p>
                                {{-- {{ ($payrolls->currentPage() - 1) * $payrolls->links()->paginator->perPage() + $loop->iteration }} --}}
                        </td>
                        <td class="p-3 whitespace-nowrap w-90">
                            <div class="flex space-x-2 w-50">
                                <div class="relative hidden w-10 h-10 rounded-full md:block">
                                    <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                    style="background-image: url({{ Storage::disk('hrim_gcs')->url($payroll->photo_path . $payroll->photo_name) }})">
                                </div>
                                </div>
                                <div class="flex items-center text-sm">
                                    <div>
                                    <div class="flex items-center w-40 text-sm">
                                        <p class="flex-initial">{{ $payroll->first_name }}
                                            {{ $payroll->last_name }}</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </td>

                        <td class="p-3 whitespace-nowrap">{{ $payroll->employee_number }}</td>
                        <td class="p-3 whitespace-nowrap">Delta Office</td>
                        <td class="p-3 whitespace-nowrap">Direct</td>
                        <td class="p-3 whitespace-nowrap">{{$payroll->divname}}</td>
                        <td class="p-3 whitespace-nowrap">{{$payroll->job_rank}}</td>
                        <td class="p-3 whitespace-nowrap">
                            <div class="flex gap-3">
                                @can('hrim_payroll_view')
                                    <svg wire:click="action({'id':  {{ $payroll->user_id }}}, 'view_payslip')"
                                        class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700" data-prefix="far"
                                        data-icon="approve" role="img" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                    </svg>
                                @endcan
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </x-slot>
            </x-table.table>
            <div class="px-1 pb-2">
            </div>
        </div>
    </x-slot>
</x-form>
