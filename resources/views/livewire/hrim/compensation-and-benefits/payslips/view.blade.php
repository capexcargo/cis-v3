<div class="px-2 w-100 whitespace-nowrap">
    @foreach ($details as $payroll)
        <div class="flex justify-between gap-3 mt-8 md:flex">
            <div class="text-3xl font-medium">{{ $payroll->first_name . ' ' . $payroll->last_name . "'s " }}Payslip
                Details
            </div>
            <div class="inline-block">
                <table class='min-w-full font-medium divide-y-2'>
                    <tr>
                        <td class="mr-2 text-xs font-semibold text-gray-400 no-wrap">Payout Date :
                        </td>
                        <td class="no-wrap">{{ $payroll->payoutdatemonth }}</td>
                    </tr>
                    <tr>
                        <td class="mr-2 text-xs font-semibold text-gray-400 no-wrap">Payout Period :</td>
                        <td class="no-wrap"> {{ $payroll->period == 1 ? 'First Cutoff' : 'Second Cutoff' }}
                        </td>
                    </tr>
                </table>
            </div>
            <div class="flex justify-end gap-4 md:flex mt-6">
                <button
                    class="px-4 py-1 text-xs text-black bg-gray-200 border border-black rounded-sm shadow-md hover:bg-blue-100">
                    <div class="flex items-start justify-between md:flex">
                        <svg wire:click="action({'id': ''}, '')" class="w-4 h-4 mr-4 text-gray-400" aria-hidden="true"
                            focusable="false" data-prefix="far" data-icon="print-alt" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path fill="currentColor"
                                d="M448 192H64C28.65 192 0 220.7 0 256v96c0 17.67 14.33 32 32 32h32v96c0 17.67 14.33 32 32 32h320c17.67 0 32-14.33 32-32v-96h32c17.67 0 32-14.33 32-32V256C512 220.7 483.3 192 448 192zM384 448H128v-96h256V448zM432 296c-13.25 0-24-10.75-24-24c0-13.27 10.75-24 24-24s24 10.73 24 24C456 285.3 445.3 296 432 296zM128 64h229.5L384 90.51V160h64V77.25c0-8.484-3.375-16.62-9.375-22.62l-45.25-45.25C387.4 3.375 379.2 0 370.8 0H96C78.34 0 64 14.33 64 32v128h64V64z" />
                        </svg>Download
                    </div>
                </button>
                <button
                    class="px-4 py-1 text-xs text-black bg-gray-200 border border-black rounded-sm shadow-md hover:bg-blue-100">
                    <div class="flex items-start justify-between md:flex">
                        <svg wire:click="action({'id': ''}, '')" class="w-4 h-4 mr-4 text-gray-400" aria-hidden="true"
                            focusable="false" data-prefix="far" data-icon="print-alt" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path fill="currentColor"
                                d="M448 192H64C28.65 192 0 220.7 0 256v96c0 17.67 14.33 32 32 32h32v96c0 17.67 14.33 32 32 32h320c17.67 0 32-14.33 32-32v-96h32c17.67 0 32-14.33 32-32V256C512 220.7 483.3 192 448 192zM384 448H128v-96h256V448zM432 296c-13.25 0-24-10.75-24-24c0-13.27 10.75-24 24-24s24 10.73 24 24C456 285.3 445.3 296 432 296zM128 64h229.5L384 90.51V160h64V77.25c0-8.484-3.375-16.62-9.375-22.62l-45.25-45.25C387.4 3.375 379.2 0 370.8 0H96C78.34 0 64 14.33 64 32v128h64V64z" />
                        </svg>Print
                    </div>
                </button>
            </div>
        </div>
        <div class="mb-4 rounded-lg shadow-lg position-relative">
            <div class="flex justify-between gap-4 mt-6 border-2 border-b-0 border-gray-400 md:flex"
                style="border-left: 0; border-right:0">
                <div class="px-6 py-2">
                    <div class="text-blue">Earnings</div>
                    <div class="px-10 text-sm">
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Basic Pay :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format($payroll->basic_pay / 2, 2) }}</div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">COLA :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format($payroll->cola / 2, 2) }}</div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Gross Pay :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format($payroll->gross / 2, 2) }}</div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Holiday Pay :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format($payroll->holidayp / 2, 2) }}</div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">ND Pay :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format($payroll->ndval / 2, 2) }}</div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">OT Pay :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format($payroll->ot_pay, 2) }}</div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Rest Day Pay :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format(round($payroll->restdaypays, 2), 2) }}</div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Leave with Pay :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format(round($payroll->leaveswtpay * ($payroll->basic_pay / 26), 2), 2) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="justify-start px-10 py-2 border-l-2 border-gray-400 deductionsDiv"
                    style="border-style: dashed;">
                    <div class="text-blue">Deductions</div>
                    <div class="px-10 text-sm">
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Leave :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format(round($payroll->leaves * ($payroll->basic_pay / 26), 2), 2) }}</div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Tardiness :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format($payroll->tardiness, 2) }}</div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Undertime :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format($payroll->undertime, 2) }}</div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Absences :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format(round($payroll->absentcount * ($payroll->basic_pay / 26), 2), 2) }}
                            </div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Government Contributions :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format($payroll->cutoffidentify == 1 ? $payroll->philhealth + $payroll->pagibig : 0, 2) }}
                            </div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Government Loans :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format($payroll->loansgovt, 2) }}</div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Withholding Tax :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">-</div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Other Loans & Deductions :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format($payroll->loans, 2) }}</div>
                        </div>
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Admin Adjustments :</div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-gray-700">
                                {{ number_format($payroll->aadpay, 2) }}</div>
                        </div>
                    </div>
                </div>
                <div class=""></div>
            </div>

            <div class="flex justify-between gap-4 border-2 border-gray-400 shadow-lg md:flex"
                style="border-left: 0; border-right:0">
                <div class="px-6 py-2">
                    <div class="px-10 text-sm">
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Total Gross Income :</div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-blue">
                                {{ number_format(
                                    $payroll->gross / 2 +
                                        $payroll->ot_pay +
                                        $payroll->holidayp +
                                        $payroll->ndval +
                                        round($payroll->restdaypays, 2) +
                                        $payroll->dlbpay +
                                        $payroll->aadpay +
                                        round($payroll->leaveswtpay * ($payroll->basic_pay / 26), 2),
                                    2,
                                ) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""></div>
                <div class="justify-start px-10 py-2">
                    <div class="px-10 text-sm">
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Total Deductions :</div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-blue">
                                ({{ number_format(
                                    round($payroll->leaves * ($payroll->basic_pay / 26), 2) +
                                        $payroll->tardiness +
                                        $payroll->undertime +
                                        (round($payroll->absentcount * ($payroll->basic_pay / 26), 2) +
                                            ($payroll->cutoffidentify == 1 ? $payroll->philhealth + $payroll->pagibig : $payroll->loansgovt)) +
                                        $payroll->loans,
                                    2,
                                ) }})
                            </div>
                        </div>
                    </div>
                    <div></div>
                </div>
                <div class=""></div>
                <div class=""></div>
            </div>

            <div class="flex justify-between gap-4 md:flex">
                <div class="px-6 py-2">
                    <div class="px-10 text-sm">
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">Total Net Pay :</div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div class="text-for-adjust text-lg font-medium text-right text-blue"></div>
                        </div>
                    </div>
                </div>
                <div class=""></div>
                <div class="justify-start px-10 py-2">
                    <div class="px-10 text-sm">
                        <div class="flex justify-between gap-6 pt-4 md:flex">
                            <div class="mt-1">
                                <div class="text-2xl font-medium text-right text-blue total-net-text">
                                    {{ number_format(
                                        $payroll->gross / 2 +
                                            $payroll->ot_pay +
                                            $payroll->holidayp +
                                            $payroll->ndval +
                                            round($payroll->restdaypays, 2) +
                                            $payroll->dlbpay +
                                            $payroll->aadpay +
                                            round($payroll->leaveswtpay * ($payroll->basic_pay / 26), 2) -
                                            (round($payroll->leaves * ($payroll->basic_pay / 26), 2) +
                                                $payroll->tardiness +
                                                $payroll->undertime +
                                                (round($payroll->absentcount * ($payroll->basic_pay / 26), 2) +
                                                    ($payroll->cutoffidentify == 1 ? $payroll->philhealth + $payroll->pagibig : $payroll->loansgovt)) +
                                                $payroll->loans),
                                        2,
                                    ) }}
                                </div>
                            </div>
                        </div>
                        <div></div>
                    </div>
                    <div class=""></div>
                    <div class=""></div>
                </div>
                <div class="watermark-container">
                    <div id="watermark" class="whitespace-nowrap font-semibold opacity-25 text-blue">
                        This is not an official printable payslip
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <style>
        .watermark-container {
            position: relative;
            width: 100%;
            height: 100%;
        }

        #watermark {
            position: absolute;
            opacity: 0.25;
            font-size: 4.2rem;
            width: 100%;
            text-align: center;
            z-index: 1000;
            -webkit-transform: rotate(-23deg);
            margin-top: -20%;
            margin-left: -54rem;
        }

        .text-for-adjust {
            margin-left: 180px;
        }

        .total-net-text {
            margin-right: 80px;
        }

        @media (min-width: 769px) and (max-width: 1280px) {
            #watermark {
                position: absolute;
                opacity: 0.25;
                font-size: 3rem;
                width: 100%;
                text-align: center;
                z-index: 1000;
                -webkit-transform: rotate(-30deg);
                margin-top: -29%;
                margin-left: -3%;
            }

            .deductionsDiv {
                padding-right: 20px;
            }

            .text-for-adjust {
                margin-left: 0;
            }

            .total-net-text {
                margin-right: -18px;
                font-size: 20px;
            }
        }
    </style>
</div>
