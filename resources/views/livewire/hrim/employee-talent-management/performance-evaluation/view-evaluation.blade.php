<x-form autocomplete="off" wire:init="load" x-data="{
    search_form: false,
    view_jd_modal: '{{ $view_jd_modal }}',
    view_justification_modal: '{{ $view_justification_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        {{-- @can('hrim_position_view') --}}
        @if ($position_id && $view_jd_modal)
            <x-modal id="view_jd_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/5">
                <x-slot name="body">
                    @livewire('hrim.workforce.position-management.view', ['id' => $position_id])
                </x-slot>
            </x-modal>
        @endif
        @if ($eval_id && $view_justification_modal)
            <x-modal id="view_justification_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="body">
                    @livewire('hrim.talent-management.performance-evaluation.view-justification', ['id' => $eval_id])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">
        <span>
            <div class="flex items-start justify-between" wire:click="action({}, 'back')">
                <svg class="w-8 h-8 mt-1 mr-4 text-blue-800 cursor-pointer" aria-hidden="true" focusable="false"
                    data-prefix="far" data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 448 512">
                    <path fill="currentColor"
                        d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" />
                </svg>
                Performance Evaluation Scoring
            </div>
        </span>
    </x-slot>
    <x-slot name="body">
        <div class="">
            <div class="grid grid-cols-2 gap-12 mb-4 text-sm md:w-full">
                <div class="flex gap-4">
                    <div class="relative hidden w-40 h-40 rounded-full md:block">
                        <img class="flex-initial object-cover w-full h-full rounded-full"
                            src="https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixlib=rb-1.2.1&amp;q=80&amp;fm=jpg&amp;crop=entropy&amp;cs=tinysrgb&amp;w=200&amp;fit=max&amp;ixid=eyJhcHBfaWQiOjE3Nzg0fQ"
                            alt="" loading="lazy">
                    </div>
                    <div class="flex mt-5 text-sm">
                        @foreach ($performance_evaluation_references as $i => $performance_evaluation)
                            <div>
                                <p class="flex-initial text-2xl font-semibold uppercase">
                                    {{ $performance_evaluation->user->name }}
                                </p>
                                <p class="flex-initial font-semibold">
                                    {{ $performance_evaluation->position->display }}
                                </p>
                                <p class="flex-initial font-semibold">
                                    {{ $performance_evaluation->division->description }}
                                </p>
                                <p class="flex-initial font-semibold">
                                    {{ $performance_evaluation->jobLevel->display }}
                                </p>
                                <p class="flex-initial mt-2 text-xs font-normal text-blue-800">
                                    <span class="flex-initial underline cursor-pointer"
                                        wire:click="action({'id': {{ $performance_evaluation->position_id }}},'view_jd')">Job
                                        Description
                                    </span>
                                </p>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="mt-4 ml-24 text-sm">
                    <div class="">
                        <p class="flex-initial ml-24 text-lg font-normal">
                            Rating Scale :
                        </p>
                        <div class="flex gap-4 text-md">
                            <div class="mr-4">
                                <p class="flex-initial font-normal">
                                    <span class="font-bold text-blue">5.00</span> - Outstanding
                                </p>
                                <p class="flex-initial font-normal">
                                    <span class="font-bold text-blue">4.00</span> - Excellent
                                </p>
                                <p class="flex-initial font-normal">
                                    <span class="font-bold text-blue">3.00</span> - Good
                                </p>
                            </div>
                            <div class="text-justify">
                                <p class="flex-initial font-normal">
                                    <span class="font-bold text-blue">2.00</span> - Satisfactory
                                </p>
                                <p class="flex-initial font-normal">
                                    <span class="font-bold text-blue">1.00</span> - Needs Improvement
                                </p>
                            </div>
                        </div>
                    </div><br>
                    <div class="grid grid-cols-1">
                        <table class="table-fixed font-normal">
                            <thead>
                                <th class="p-1  border-2 border-b-0 text-gray-500 font-normal"></th>
                                <th class="p-1  border-2 border-b-0 text-gray-500 font-normal">Total
                                    Scoring Points</th>
                                <th class="p-1  border-2 border-b-0 text-gray-500 font-normal">Total
                                    Core Values Points</th>
                                <th class="p-1  border-2 border-b-0 text-gray-500 font-normal">Total
                                    Leadership Points</th>
                                <th class="p-1  border-2 border-b-0 text-gray-500 font-normal">Overall
                                    Total</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="p-2 whitespace-nowrap border-2 border-b-0">
                                        First Evaluator
                                    </td>
                                    <td class="p-2 whitespace-nowrap border-2 border-b-0 text-center">
                                        {{ number_format(($breakdown_pct_goals / 100) * $total_first_points_eval_scoring, 1, '.', '') }}
                                    </td>
                                    <td class="p-2 whitespace-nowrap border-2 border-b-0 text-center">
                                        {{ number_format(($breakdown_pct_core_values / 100) * $total_first_points_core_value, 1, '.', '') }}
                                    </td>
                                    <td class="p-2 whitespace-nowrap border-2 border-b-0 text-center">
                                        {{ number_format(($breakdown_pct_leadership_competencies / 100) * $total_first_points_leader_comp, 1, '.', '') }}
                                    </td>
                                    <td
                                        class="p-2 whitespace-nowrap border-2 border-b-0 text-blue text-md text-center font-bold">
                                        {{ number_format(($breakdown_pct_goals / 100) * $total_first_points_eval_scoring, 1, '.', '') +
                                            number_format(($breakdown_pct_core_values / 100) * $total_first_points_core_value, 1, '.', '') +
                                            number_format(($breakdown_pct_leadership_competencies / 100) * $total_first_points_leader_comp, 1, '.', '') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="p-2 whitespace-nowrap border-2 border-b-0">
                                        Second Evaluator
                                    </td>
                                    <td class="p-2 whitespace-nowrap border-2 border-b-0 text-center">
                                        {{ number_format(($breakdown_pct_goals / 100) * $total_second_points_eval_scoring, 1, '.', '') }}
                                    </td>
                                    <td class="p-2 whitespace-nowrap border-2 border-b-0 text-center">
                                        {{ number_format(($breakdown_pct_core_values / 100) * $total_second_points_core_value, 1, '.', '') }}
                                    </td>
                                    <td class="p-2 whitespace-nowrap border-2 border-b-0 text-center">
                                        {{ number_format(($breakdown_pct_leadership_competencies / 100) * $total_second_points_leader_comp, 1, '.', '') }}
                                    </td>
                                    <td
                                        class="p-2 whitespace-nowrap border-2 border-b-0 text-blue text-md text-center font-bold">
                                        {{ number_format(($breakdown_pct_goals / 100) * $total_second_points_eval_scoring, 1, '.', '') +
                                            number_format(($breakdown_pct_core_values / 100) * $total_second_points_core_value, 1, '.', '') +
                                            number_format(($breakdown_pct_leadership_competencies / 100) * $total_second_points_leader_comp, 1, '.', '') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="p-2 whitespace-nowrap border-2">
                                        Third Evaluator
                                    </td>
                                    <td class="p-2 whitespace-nowrap border-2 text-center">
                                        {{ number_format(($breakdown_pct_goals / 100) * $total_third_points_eval_scoring, 1, '.', '') }}
                                    </td>
                                    <td class="p-2 whitespace-nowrap border-2 text-center">
                                        {{ number_format(($breakdown_pct_core_values / 100) * $total_third_points_core_value, 1, '.', '') }}
                                    </td>
                                    <td class="p-2 whitespace-nowrap border-2 text-center">
                                        {{ number_format(($breakdown_pct_leadership_competencies / 100) * $total_third_points_leader_comp, 1, '.', '') }}
                                    </td>
                                    <td class="p-2 whitespace-nowrap border-2 text-blue text-md text-center font-bold">
                                        {{ number_format(($breakdown_pct_goals / 100) * $total_third_points_eval_scoring, 1, '.', '') +
                                            number_format(($breakdown_pct_core_values / 100) * $total_third_points_core_value, 1, '.', '') +
                                            number_format(($breakdown_pct_leadership_competencies / 100) * $total_third_points_leader_comp, 1, '.', '') }}
                                    </td>
                                </tr>
                                <tr class="text-md font-semibold">
                                    <td class="p-2 whitespace-nowrap">
                                        Final Evaluation Points :
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="p-2 whitespace-nowrap text-blue text-lg text-center">
                                        <div class="p-2">
                                            @if ($performance_evaluators[$i]['third_evaluator'] == null)
                                                {{ number_format(($breakdown_pct_goals / 100) * $total_second_points_eval_scoring, 1, '.', '') +
                                                    number_format(($breakdown_pct_core_values / 100) * $total_second_points_core_value, 1, '.', '') +
                                                    number_format(($breakdown_pct_leadership_competencies / 100) * $total_second_points_leader_comp, 1, '.', '') }}
                                            @else
                                                {{ number_format(($breakdown_pct_goals / 100) * $total_third_points_eval_scoring, 1, '.', '') +
                                                    number_format(($breakdown_pct_core_values / 100) * $total_third_points_core_value, 1, '.', '') +
                                                    number_format(($breakdown_pct_leadership_competencies / 100) * $total_third_points_leader_comp, 1, '.', '') }}
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            <tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="mt-8">
                <div class="flex px-4 py-2 text-white rounded-md shadow-md bg-blue justify-between">
                    <span>Performance Evaluation Scoring</span>
                    <span>({{ $breakdown_pct_goals }}%)</span>
                </div>
                <div class="-mt-2 bg-white rounded-lg shadow-md">
                    <div class="my-2 overflow-auto rounded-lg">
                        <div class="inline-block min-w-full align-middle">
                            <table class="w-full divide-y-2">
                                <thead>
                                    <tr
                                        class="text-xs font-normal text-left text-gray-500 bg-white border-0 cursor-pointer">
                                        <th class="w-1/5 px-2 py-1">Key Results Area</th>
                                        <th class="px-4 py-1">Key Performance Indicator</th>
                                        <th class="px-2 py-1">Target</th>
                                        <th class="px-2 py-1">% Score Weight</th>
                                        <th class="w-1/4 px-2 py-1 text-center border-x-4">
                                            <span class="w-24">Score
                                            </span>
                                            <div class="grid grid-cols-3 gap-2 mt-2 bg-gray-200">
                                                <span class="text-gray-400">1st Evaluator</span>
                                                <span class="text-gray-400">2nd Evaluator</span>
                                                <span class="text-gray-400">3rd Evaluator</span>
                                            </div>
                                        </th>
                                        <th class="px-2 py-1 text-center">Justification</th>
                                        <th class="w-1/4 px-2 py-1 text-center border-x-4">
                                            <span class="w-24">Weighted Score
                                            </span>
                                            <div class="grid grid-cols-3 gap-2 mt-2 bg-gray-200">
                                                <span class="text-gray-400">1st Evaluator</span>
                                                <span class="text-gray-400">2nd Evaluator</span>
                                                <span class="text-gray-400">3rd Evaluator</span>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="text-sm bg-gray-100 divide-white">
                                    @foreach ($performance_scorings as $i => $kpi_tagged)
                                        <tr>
                                            <td class="w-64 px-2 py-2">
                                                {{ $performance_scorings[$i]['kra'] }}
                                            </td>
                                            <td class="w-1/4 px-4 py-1">
                                                {{ $performance_scorings[$i]['kpi'] }}
                                            </td>
                                            <td class="px-2 py-2">
                                                <p class="flex-initial w-28">
                                                    {{ $performance_scorings[$i]['target'] }}
                                                </p>
                                            </td>
                                            <td class="px-2 py-2">
                                                <p class="flex-initial text-center w-28">
                                                    {{ $performance_scorings[$i]['points'] }}%
                                                </p>
                                            </td>
                                            <td class="px-2 py-2 font-normal text-center text-blue">
                                                <div class="grid grid-cols-3 gap-2 mt-2 text-2xl">
                                                    <span class="">
                                                        @if ($performance_scorings[$i]['first_evaluator'] == Auth::user()->id &&
                                                            ($performance_scorings[$i]['first_status'] == 1 &&
                                                                $performance_scorings[$i]['second_status'] == 1 &&
                                                                $performance_scorings[$i]['third_status'] == 1))
                                                            <x-select
                                                                name="performance_scorings.{{ $i }}.first_evaluator_points"
                                                                wire:model.defer='performance_scorings.{{ $i }}.first_evaluator_points'
                                                                wire:change="first_compute_eval_scoring">
                                                                <option value="">Select
                                                                </option>
                                                                @foreach ($evaluation_points_references as $evaluation_points)
                                                                    <option value="{{ $evaluation_points->id }}">
                                                                        {{ $evaluation_points->points }}
                                                                    </option>
                                                                @endforeach
                                                            </x-select>
                                                            <x-input-error
                                                                for="performance_scorings.{{ $i }}.first_evaluator_points" />
                                                        @else
                                                            {{ $performance_scorings[$i]['first_points'] }}
                                                        @endif
                                                    </span>
                                                    <span class="">
                                                        @if ($performance_scorings[$i]['second_evaluator'] == Auth::user()->id &&
                                                            ($performance_scorings[$i]['first_status'] == 2 &&
                                                                $performance_scorings[$i]['second_status'] == 1 &&
                                                                $performance_scorings[$i]['third_status'] == 1))
                                                            <x-select
                                                                name="performance_scorings.{{ $i }}.second_evaluator_points"
                                                                wire:model.defer='performance_scorings.{{ $i }}.second_evaluator_points'
                                                                wire:change="second_compute_eval_scoring">
                                                                <option value="">Select
                                                                </option>
                                                                @foreach ($evaluation_points_references as $evaluation_points)
                                                                    <option value="{{ $evaluation_points->id }}">
                                                                        {{ $evaluation_points->points }}
                                                                    </option>
                                                                @endforeach
                                                            </x-select>
                                                            <x-input-error
                                                                for="performance_scorings.{{ $i }}.second_evaluator_points" />
                                                        @else
                                                            {{ $performance_scorings[$i]['second_points'] }}
                                                        @endif
                                                    </span>
                                                    <span class="">
                                                        @if ($performance_scorings[$i]['third_evaluator'] == Auth::user()->id &&
                                                            ($performance_scorings[$i]['first_status'] == 2 &&
                                                                $performance_scorings[$i]['second_status'] == 2 &&
                                                                $performance_scorings[$i]['third_status'] == 1))
                                                            <x-select
                                                                name="performance_scorings.{{ $i }}.third_evaluator_points"
                                                                wire:model.defer='performance_scorings.{{ $i }}.third_evaluator_points'
                                                                wire:change="third_compute_eval_scoring">
                                                                <option value="">Select
                                                                </option>
                                                                @foreach ($evaluation_points_references as $evaluation_points)
                                                                    <option value="{{ $evaluation_points->id }}">
                                                                        {{ $evaluation_points->points }}
                                                                    </option>
                                                                @endforeach
                                                            </x-select>
                                                            <x-input-error
                                                                for="performance_scorings.{{ $i }}.third_evaluator_points" />
                                                        @else
                                                            {{ $performance_scorings[$i]['third_points'] }}
                                                        @endif
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="px-2 py-2 text-sm"><span
                                                    class="ml-6 underline cursor-pointer text-blue"
                                                    wire:click="action({'id': {{ $performance_scorings[$i]['id'] }} }, 'view_justification')">view</span>
                                            </td>
                                            <td class="px-2 py-2 font-normal text-center text-blue">
                                                <div class="grid grid-cols-3 gap-2 mt-2 text-2xl">
                                                    <span class="">
                                                        {{ $performance_scorings[$i]['first_evaluator_points'] }}
                                                    </span>
                                                    <span class="">
                                                        {{ $performance_scorings[$i]['second_evaluator_points'] }}
                                                    </span>
                                                    <span class="">
                                                        {{ $performance_scorings[$i]['third_evaluator_points'] }}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <thead>
                                    <tr class="text-sm font-normal bg-white border-0 cursor-pointer">
                                        <th class="w-1/5 px-2 py-4 text-left">Average Score :</th>
                                        <th class="px-2 py-4"></th>
                                        <th class="px-2 py-4"></th>
                                        <th class="px-2 py-4"></th>
                                        <th class="px-4 py-4 text-sm text-center">
                                            <div class="grid grid-cols-3 gap-4 mt-2 text-2xl">
                                                <span
                                                    class="{{ $total_first_points_eval_scoring == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_first_points_eval_scoring }}</span>
                                                <span
                                                    class="{{ $total_second_points_eval_scoring == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_second_points_eval_scoring }}</span>
                                                <span
                                                    class="{{ $total_third_points_eval_scoring == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_third_points_eval_scoring }}</span>
                                            </div>
                                        </th>
                                        <th class="px-2 py-4"></th>
                                        <th class="px-4 py-4 text-sm text-center">
                                            <div class="grid grid-cols-3 gap-4 mt-2 text-2xl">
                                                <span
                                                    class="{{ $total_first_points_eval_scoring == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_first_points_eval_scoring }}</span>
                                                <span
                                                    class="{{ $total_second_points_eval_scoring == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_second_points_eval_scoring }}</span>
                                                <span
                                                    class="{{ $total_third_points_eval_scoring == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_third_points_eval_scoring }}</span>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-12">
                <div class="flex px-4 py-2 text-white rounded-md shadow-md bg-blue justify-between">
                    <span>Core Value Assessment</span>
                    <span>({{ $breakdown_pct_core_values }}%)</span>
                </div>
                <div class="-mt-2 bg-white rounded-lg shadow-md">
                    <div class="my-2 overflow-auto rounded-lg">
                        <div class="inline-block min-w-full align-middle">
                            <table class="w-full divide-y-2">
                                <thead>
                                    <tr
                                        class="w-full text-xs font-normal text-left text-gray-500 bg-white border-0 cursor-pointer">
                                        <th class="w-12 px-2 py-1">No.</th>
                                        <th class="w-1/2 px-4 py-1">Core Values</th>
                                        <th class="w-40 px-2 py-1 text-center">Evaluatee's Score</th>
                                        <th class="w-1/4 px-2 py-1 text-center border-x-4">
                                            <span class="w-24">Evaluator's Score
                                            </span>
                                            <div class="grid grid-cols-3 gap-2 mt-2 bg-gray-200">
                                                <span class="text-gray-400">1st Evaluator</span>
                                                <span class="text-gray-400">2nd Evaluator</span>
                                                <span class="text-gray-400">3rd Evaluator</span>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="text-sm bg-gray-100 divide-white">
                                    @foreach ($core_value_assessments as $i => $core_val_tagged)
                                        <tr>
                                            <td class="px-2 py-1">
                                                <p class="flex-initial">{{ $i + 1 }}.
                                                </p>
                                            </td>
                                            <td class="px-4 py-1">
                                                <p class="">
                                                    {{ $core_value_assessments[$i]['description'] }}
                                                </p>
                                            </td>
                                            <td class="px-2 py-2 font-normal text-center">
                                                <span class="text-2xl text-blue">
                                                    {{ $core_value_assessments[$i]['employee_self_points'] }}
                                                </span>
                                            </td>
                                            <td class="px-2 py-2 font-normal text-center text-blue">
                                                <div class="grid grid-cols-3 gap-2 mt-2 text-2xl">
                                                    <span class="">
                                                        @if ($core_value_assessments[$i]['first_evaluator'] == Auth::user()->id &&
                                                            ($core_value_assessments[$i]['first_status'] == 1 &&
                                                                $core_value_assessments[$i]['second_status'] == 1 &&
                                                                $core_value_assessments[$i]['third_status'] == 1))
                                                            <x-select
                                                                name="core_value_assessments.{{ $i }}.first_evaluator_points"
                                                                wire:model.defer='core_value_assessments.{{ $i }}.first_evaluator_points'
                                                                wire:change="first_compute_core_value">
                                                                <option value="">Select</option>

                                                                @foreach ($evaluation_points_references as $evaluation_points)
                                                                    <option value="{{ $evaluation_points->id }}">
                                                                        {{ $evaluation_points->points }}
                                                                    </option>
                                                                @endforeach
                                                            </x-select>
                                                            <x-input-error
                                                                for="core_value_assessments.{{ $i }}.first_evaluator_points" />
                                                        @else
                                                            {{ $core_value_assessments[$i]['first_points'] }}
                                                        @endif
                                                    </span>
                                                    <span class="">
                                                        @if ($core_value_assessments[$i]['second_evaluator'] == Auth::user()->id &&
                                                            ($core_value_assessments[$i]['first_status'] == 2 &&
                                                                $core_value_assessments[$i]['second_status'] == 1 &&
                                                                $core_value_assessments[$i]['third_status'] == 1))
                                                            <x-select
                                                                name="core_value_assessments.{{ $i }}.second_evaluator_points"
                                                                wire:model.defer='core_value_assessments.{{ $i }}.second_evaluator_points'
                                                                wire:change="second_compute_core_value">
                                                                <option value="">Select</option>
                                                                @foreach ($evaluation_points_references as $evaluation_points)
                                                                    <option value="{{ $evaluation_points->id }}">
                                                                        {{ $evaluation_points->points }}
                                                                    </option>
                                                                @endforeach
                                                            </x-select>
                                                            <x-input-error
                                                                for="core_value_assessments.{{ $i }}.second_evaluator_points" />
                                                        @else
                                                            {{ $core_value_assessments[$i]['second_points'] }}
                                                        @endif
                                                    </span>
                                                    <span class="">
                                                        @if ($core_value_assessments[$i]['third_evaluator'] == Auth::user()->id &&
                                                            ($core_value_assessments[$i]['first_status'] == 2 &&
                                                                $core_value_assessments[$i]['second_status'] == 2 &&
                                                                $core_value_assessments[$i]['third_status'] == 1))
                                                            <x-select
                                                                name="core_value_assessments.{{ $i }}.third_evaluator_points"
                                                                wire:model.defer='core_value_assessments.{{ $i }}.third_evaluator_points'
                                                                wire:change="third_compute_core_value">
                                                                <option value="">Select</option>
                                                                @foreach ($evaluation_points_references as $evaluation_points)
                                                                    <option value="{{ $evaluation_points->id }}">
                                                                        {{ $evaluation_points->points }}
                                                                    </option>
                                                                @endforeach
                                                            </x-select>
                                                            <x-input-error
                                                                for="core_value_assessments.{{ $i }}.third_evaluator_points" />
                                                        @else
                                                            {{ $core_value_assessments[$i]['third_points'] }}
                                                        @endif
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>

                                        @if ($core_value_assessments[$i]['employee_self_points'] > 3)
                                            <tr>
                                                <td class="px-2 py-1">
                                                </td>
                                                <td class="px-4 py-1">
                                                    <span class="text-l font-medium text-blue">
                                                        Employee Actual Critical Incident :
                                                    </span>
                                                    <span class="font-semibold">
                                                        {{ $core_value_assessments[$i]['emp_critical_remarks'] }}
                                                    </span>
                                                </td>
                                                <td class="px-2 py-2 font-normal text-center">

                                                </td>
                                                <td class="px-2 py-2 font-normal text-center text-blue">

                                                </td>
                                            </tr>
                                        @endif

                                        @if ($core_value_assessments[$i]['first_evaluator_points'] > 3 &&
                                            ($core_value_assessments[$i]['first_status'] == 1 &&
                                                $core_value_assessments[$i]['second_status'] == 1 &&
                                                $core_value_assessments[$i]['third_status'] == 1))
                                            <tr>
                                                <td class="px-2 py-1">
                                                </td>
                                                <td class="px-4 py-1">
                                                    <div class="grid grid-cols-1 justify-start">
                                                        <span class="text-l font-semibold">
                                                            <x-label for="core_value"
                                                                value="Actual Critical Incident :" :required="true" />
                                                        </span>
                                                        <span>
                                                            <x-input
                                                                name="core_value_assessments.{{ $i }}.evaluator_1st_critical_remarks"
                                                                wire:model.defer='core_value_assessments.{{ $i }}.evaluator_1st_critical_remarks'
                                                                wire:change="" disabled>
                                                            </x-input>
                                                            <x-input-error
                                                                for="core_value_assessments.{{ $i }}.evaluator_1st_critical_remarks" />
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="px-2 py-1">
                                                </td>
                                                <td class="px-2 py-1">
                                                </td>
                                            </tr>
                                        @else
                                            @if ($core_value_assessments[$i]['evaluator_1st_critical_remarks'] != null)
                                                <tr>
                                                    <td class="px-2 py-1">
                                                    </td>
                                                    <td class="px-4 py-1">
                                                        <span class="text-l font-medium text-blue">
                                                            First Evaluator Critical Incident :
                                                        </span>
                                                        <span class="font-semibold">
                                                            {{ $core_value_assessments[$i]['evaluator_1st_critical_remarks'] }}
                                                        </span>
                                                    </td>
                                                    <td class="px-2 py-2 font-normal text-center">

                                                    </td>
                                                    <td class="px-2 py-2 font-normal text-center text-blue">

                                                    </td>
                                                </tr>
                                            @endif
                                        @endif

                                        @if ($core_value_assessments[$i]['second_evaluator_points'] > 3 &&
                                            ($core_value_assessments[$i]['first_status'] == 2 &&
                                                $core_value_assessments[$i]['second_status'] == 1 &&
                                                $core_value_assessments[$i]['third_status'] == 1))
                                            <tr>
                                                <td class="px-2 py-1">
                                                </td>
                                                <td class="px-4 py-1">
                                                    <div class="grid grid-cols-1 justify-start">
                                                        <span class="text-l font-semibold">
                                                            <x-label for="core_value"
                                                                value="Actual Critical Incident :" :required="true" />
                                                        </span>
                                                        <span>
                                                            <x-input
                                                                name="core_value_assessments.{{ $i }}.evaluator_2nd_critical_remarks"
                                                                wire:model.defer='core_value_assessments.{{ $i }}.evaluator_2nd_critical_remarks'
                                                                wire:change="" disabled>
                                                            </x-input>
                                                            <x-input-error
                                                                for="core_value_assessments.{{ $i }}.evaluator_2nd_critical_remarks" />
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="px-2 py-1">
                                                </td>
                                                <td class="px-2 py-1">
                                                </td>
                                            </tr>
                                        @else
                                            @if ($core_value_assessments[$i]['evaluator_2nd_critical_remarks'] != null)
                                                <tr>
                                                    <td class="px-2 py-1">
                                                    </td>
                                                    <td class="px-4 py-1">
                                                        <span class="text-l font-medium text-blue">
                                                            Second Evaluator Critical Incident :
                                                        </span>
                                                        <span class="font-semibold">
                                                            {{ $core_value_assessments[$i]['evaluator_2nd_critical_remarks'] }}
                                                        </span>
                                                    </td>
                                                    <td class="px-2 py-2 font-normal text-center">

                                                    </td>
                                                    <td class="px-2 py-2 font-normal text-center text-blue">

                                                    </td>
                                                </tr>
                                            @endif
                                        @endif

                                        @if ($core_value_assessments[$i]['third_evaluator_points'] > 3 &&
                                            ($core_value_assessments[$i]['first_status'] == 2 &&
                                                $core_value_assessments[$i]['second_status'] == 2 &&
                                                $core_value_assessments[$i]['third_status'] == 1))
                                            <tr>
                                                <td class="px-2 py-1">
                                                </td>
                                                <td class="px-4 py-1">
                                                    <div class="grid grid-cols-1 justify-start">
                                                        <span class="text-l font-semibold">
                                                            <x-label for="core_value"
                                                                value="Actual Critical Incident :" :required="true" />
                                                        </span>
                                                        <span>
                                                            <x-input
                                                                name="core_value_assessments.{{ $i }}.evaluator_2nd_critical_remarks"
                                                                wire:model.defer='core_value_assessments.{{ $i }}.evaluator_2nd_critical_remarks'
                                                                wire:change="" disabled>
                                                            </x-input>
                                                            <x-input-error
                                                                for="core_value_assessments.{{ $i }}.evaluator_2nd_critical_remarks" />
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="px-2 py-1">
                                                </td>
                                                <td class="px-2 py-1">
                                                </td>
                                            </tr>
                                        @else
                                            @if ($core_value_assessments[$i]['evaluator_3rd_critical_remarks'] != null)
                                                <tr>
                                                    <td class="px-2 py-1">
                                                    </td>
                                                    <td class="px-4 py-1">
                                                        <span class="text-l font-medium text-blue">
                                                            Third Evaluator Critical Incident :
                                                        </span>
                                                        <span class="font-semibold">
                                                            {{ $core_value_assessments[$i]['evaluator_3rd_critical_remarks'] }}
                                                        </span>
                                                    </td>
                                                    <td class="px-2 py-2 font-normal text-center">

                                                    </td>
                                                    <td class="px-2 py-2 font-normal text-center text-blue">

                                                    </td>
                                                </tr>
                                            @endif
                                        @endif
                                    @endforeach
                                </tbody>
                                <thead>
                                    <tr class="text-sm font-normal text-left bg-white border-0 cursor-pointer">
                                        <th class="w-40 px-2 py-4">Average Score :</th>
                                        <th class="px-2 py-4"></th>
                                        <th class="px-2 py-4 text-2xl text-center">
                                            <span
                                                class=" {{ $total_employee_points_core_value == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_employee_points_core_value }}</span>
                                        </th>
                                        <th class="w-1/4 px-2 py-4 text-center border-x-4">
                                            <div class="grid grid-cols-3 text-2xl">
                                                <span
                                                    class="{{ $total_first_points_core_value == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_first_points_core_value }}</span>
                                                <span
                                                    class="{{ $total_second_points_core_value == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_second_points_core_value }}</span>
                                                <span
                                                    class="{{ $total_third_points_core_value == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_third_points_core_value }}</span>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            @if (count($leadership_competencies) > 0)
                <div class="mt-12">
                    <div class="flex px-4 py-2 text-white rounded-md shadow-md bg-blue justify-between">
                        <span>Leadership Competencies</span>
                        <span>({{ $breakdown_pct_leadership_competencies }}%)</span>
                    </div>
                    <div class="-mt-2 bg-white rounded-lg shadow-md">
                        <div class="my-2 overflow-auto rounded-lg">
                            <div class="inline-block min-w-full align-middle">
                                <table class="w-full divide-y-2">
                                    <thead>
                                        <tr
                                            class="text-xs font-normal text-left text-gray-500 bg-white border-0 cursor-pointer">
                                            <th class="w-20 px-2 py-1">No.</th>
                                            <th class="w-1/2 px-4 py-1">Leadership Competencies</th>
                                            <th class="w-40 px-2 py-1 text-center">Evaluatee's Score</th>
                                            <th class="w-1/4 px-2 py-1 text-center border-x-4">Evaluator's Score<br>
                                                <div class="grid grid-cols-3 gap-2 mt-2 bg-gray-200">
                                                    <span class="text-gray-400">1st Evaluator</span>
                                                    <span class="text-gray-400">2nd Evaluator</span>
                                                    <span class="text-gray-400">3rd Evaluator</span>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-sm bg-gray-100 divide-white">
                                        @foreach ($leadership_competencies as $i => $leadership_comp_tagged)
                                            <tr>
                                                <td class="px-2 py-1">
                                                    <p class="flex-initial">{{ $i + 1 }}.
                                                    </p>
                                                </td>
                                                <td class="px-4 py-1">
                                                    <p class="flex-initial">
                                                        {{ $leadership_competencies[$i]['description'] }}
                                                    </p>
                                                </td>
                                                <td class="px-2 py-2 font-normal text-center">
                                                    <span class="text-2xl text-blue">
                                                        {{ $leadership_competencies[$i]['employee_self_points'] }}
                                                    </span>
                                                </td>
                                                <td class="px-2 py-2 font-normal text-center text-blue">
                                                    <div class="grid grid-cols-3 gap-2 mt-2 text-2xl">
                                                        <span class="">
                                                            @if ($leadership_competencies[$i]['first_evaluator'] == Auth::user()->id &&
                                                                ($leadership_competencies[$i]['first_status'] == 1 &&
                                                                    $leadership_competencies[$i]['second_status'] == 1 &&
                                                                    $leadership_competencies[$i]['third_status'] == 1))
                                                                <x-select
                                                                    name="leadership_competencies.{{ $i }}.first_evaluator_points"
                                                                    wire:model.defer='leadership_competencies.{{ $i }}.first_evaluator_points'
                                                                    wire:change="first_compute_leader_comp">
                                                                    <option value="">Select</option>

                                                                    @foreach ($evaluation_points_references as $evaluation_points)
                                                                        <option value="{{ $evaluation_points->id }}">
                                                                            {{ $evaluation_points->points }}
                                                                        </option>
                                                                    @endforeach
                                                                </x-select>
                                                                <x-input-error
                                                                    for="leadership_competencies.{{ $i }}.first_evaluator_points" />
                                                            @else
                                                                {{ $leadership_competencies[$i]['first_points'] }}
                                                            @endif
                                                        </span>
                                                        <span class="">
                                                            @if ($leadership_competencies[$i]['second_evaluator'] == Auth::user()->id &&
                                                                ($leadership_competencies[$i]['first_status'] == 2 &&
                                                                    $leadership_competencies[$i]['second_status'] == 1 &&
                                                                    $leadership_competencies[$i]['third_status'] == 1))
                                                                <x-select
                                                                    name="leadership_competencies.{{ $i }}.second_evaluator_points"
                                                                    wire:model.defer='leadership_competencies.{{ $i }}.second_evaluator_points'
                                                                    wire:change="second_compute_leader_comp">
                                                                    <option value="">Select</option>
                                                                    @foreach ($evaluation_points_references as $evaluation_points)
                                                                        <option value="{{ $evaluation_points->id }}">
                                                                            {{ $evaluation_points->points }}
                                                                        </option>
                                                                    @endforeach
                                                                </x-select>
                                                                <x-input-error
                                                                    for="leadership_competencies.{{ $i }}.second_evaluator_points" />
                                                            @else
                                                                {{ $leadership_competencies[$i]['second_points'] }}
                                                            @endif
                                                        </span>
                                                        <span class="">
                                                            @if ($leadership_competencies[$i]['third_evaluator'] == Auth::user()->id &&
                                                                ($leadership_competencies[$i]['first_status'] == 2 &&
                                                                    $leadership_competencies[$i]['second_status'] == 2 &&
                                                                    $leadership_competencies[$i]['third_status'] == 1))
                                                                <x-select
                                                                    name="leadership_competencies.{{ $i }}.third_evaluator_points"
                                                                    wire:model.defer='leadership_competencies.{{ $i }}.third_evaluator_points'
                                                                    wire:change="third_compute_leader_comp">
                                                                    <option value="">Select</option>
                                                                    @foreach ($evaluation_points_references as $evaluation_points)
                                                                        <option value="{{ $evaluation_points->id }}">
                                                                            {{ $evaluation_points->points }}
                                                                        </option>
                                                                    @endforeach
                                                                </x-select>
                                                                <x-input-error
                                                                    for="leadership_competencies.{{ $i }}.third_evaluator_points" />
                                                            @else
                                                                {{ $leadership_competencies[$i]['third_points'] }}
                                                            @endif
                                                        </span>
                                                    </div>
                                                </td>
                                            </tr>

                                            @if ($leadership_competencies[$i]['employee_self_points'] > 3)
                                                <tr>
                                                    <td class="px-2 py-1">
                                                    </td>
                                                    <td class="px-4 py-1">
                                                        <span class="text-l font-medium text-blue">
                                                            Employee Actual Critical Incident :
                                                        </span>
                                                        <span class="font-semibold">
                                                            {{ $leadership_competencies[$i]['emp_critical_remarks'] }}
                                                        </span>
                                                    </td>
                                                    <td class="px-2 py-2 font-normal text-center">

                                                    </td>
                                                    <td class="px-2 py-2 font-normal text-center text-blue">

                                                    </td>
                                                </tr>
                                            @endif

                                            @if ($leadership_competencies[$i]['first_evaluator_points'] > 3 &&
                                                ($leadership_competencies[$i]['first_status'] == 1 &&
                                                    $leadership_competencies[$i]['second_status'] == 1 &&
                                                    $leadership_competencies[$i]['third_status'] == 1))
                                                <tr>
                                                    <td class="px-2 py-1">
                                                    </td>
                                                    <td class="px-4 py-1">
                                                        <div class="grid grid-cols-1 justify-start">
                                                            <span class="text-l font-semibold">
                                                                <x-label for="core_value"
                                                                    value="Actual Critical Incident :" />
                                                            </span>
                                                            <span>
                                                                <x-input
                                                                    name="leadership_competencies.{{ $i }}.evaluator_1st_critical_remarks"
                                                                    wire:model.defer='leadership_competencies.{{ $i }}.evaluator_1st_critical_remarks'
                                                                    wire:change="" disabled>
                                                                </x-input>
                                                                <x-input-error
                                                                    for="leadership_competencies.{{ $i }}.evaluator_1st_critical_remarks" />
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                    </td>
                                                    <td class="px-2 py-1">
                                                    </td>
                                                </tr>
                                            @else
                                                @if ($core_value_assessments[$i]['evaluator_1st_critical_remarks'] != null)
                                                    <tr>
                                                        <td class="px-2 py-1">
                                                        </td>
                                                        <td class="px-4 py-1">
                                                            <span class="text-l font-medium text-blue">
                                                                First Evaluator Critical Incident :
                                                            </span>
                                                            <span class="font-semibold">
                                                                {{ $core_value_assessments[$i]['evaluator_1st_critical_remarks'] }}
                                                            </span>
                                                        </td>
                                                        <td class="px-2 py-2 font-normal text-center">

                                                        </td>
                                                        <td class="px-2 py-2 font-normal text-center text-blue">

                                                        </td>
                                                    </tr>
                                                @endif
                                            @endif

                                            @if ($leadership_competencies[$i]['second_evaluator_points'] > 3 &&
                                                ($leadership_competencies[$i]['first_status'] == 2 &&
                                                    $leadership_competencies[$i]['second_status'] == 1 &&
                                                    $leadership_competencies[$i]['third_status'] == 1))
                                                <tr>
                                                    <td class="px-2 py-1">
                                                    </td>
                                                    <td class="px-4 py-1">
                                                        <div class="grid grid-cols-1 justify-start">
                                                            <span class="text-l font-semibold">
                                                                <x-label for="core_value"
                                                                    value="Actual Critical Incident :" />
                                                            </span>
                                                            <span>
                                                                <x-input
                                                                    name="leadership_competencies.{{ $i }}.evaluator_2nd_critical_remarks"
                                                                    wire:model.defer='leadership_competencies.{{ $i }}.evaluator_2nd_critical_remarks'
                                                                    wire:change="" disabled>
                                                                </x-input>
                                                                <x-input-error
                                                                    for="leadership_competencies.{{ $i }}.evaluator_2nd_critical_remarks" />
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                    </td>
                                                    <td class="px-2 py-1">
                                                    </td>
                                                </tr>
                                            @else
                                                @if ($core_value_assessments[$i]['evaluator_2nd_critical_remarks'] != null)
                                                    <tr>
                                                        <td class="px-2 py-1">
                                                        </td>
                                                        <td class="px-4 py-1">
                                                            <span class="text-l font-medium text-blue">
                                                                Second Evaluator Critical Incident :
                                                            </span>
                                                            <span class="font-semibold">
                                                                {{ $core_value_assessments[$i]['evaluator_2nd_critical_remarks'] }}
                                                            </span>
                                                        </td>
                                                        <td class="px-2 py-2 font-normal text-center">

                                                        </td>
                                                        <td class="px-2 py-2 font-normal text-center text-blue">

                                                        </td>
                                                    </tr>
                                                @endif
                                            @endif

                                            @if ($leadership_competencies[$i]['third_evaluator_points'] > 3 &&
                                                ($leadership_competencies[$i]['first_status'] == 2 &&
                                                    $leadership_competencies[$i]['second_status'] == 2 &&
                                                    $leadership_competencies[$i]['third_status'] == 1))
                                                <tr>
                                                    <td class="px-2 py-1">
                                                    </td>
                                                    <td class="px-4 py-1">
                                                        <div class="grid grid-cols-1 justify-start">
                                                            <span class="text-l font-semibold">
                                                                <x-label for="core_value"
                                                                    value="Actual Critical Incident :" />
                                                            </span>
                                                            <span>
                                                                <x-input
                                                                    name="leadership_competencies.{{ $i }}.evaluator_3rd_critical_remarks"
                                                                    wire:model.defer='leadership_competencies.{{ $i }}.evaluator_3rd_critical_remarks'
                                                                    wire:change="" disabled>
                                                                </x-input>
                                                                <x-input-error
                                                                    for="leadership_competencies.{{ $i }}.evaluator_3rd_critical_remarks" />
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                    </td>
                                                    <td class="px-2 py-1">
                                                    </td>
                                                </tr>
                                            @else
                                                @if ($core_value_assessments[$i]['evaluator_3rd_critical_remarks'] != null)
                                                    <tr>
                                                        <td class="px-2 py-1">
                                                        </td>
                                                        <td class="px-4 py-1">
                                                            <span class="text-l font-medium text-blue">
                                                                Third Evaluator Critical Incident :
                                                            </span>
                                                            <span class="font-semibold">
                                                                {{ $core_value_assessments[$i]['evaluator_3rd_critical_remarks'] }}
                                                            </span>
                                                        </td>
                                                        <td class="px-2 py-2 font-normal text-center">

                                                        </td>
                                                        <td class="px-2 py-2 font-normal text-center text-blue">

                                                        </td>
                                                    </tr>
                                                @endif
                                            @endif
                                        @endforeach
                                    </tbody>
                                    <thead>
                                        <tr class="text-sm font-normal text-left bg-white border-0 cursor-pointer">
                                            <th class="w-40 px-2 py-4">Average Score :</th>
                                            <th class="px-2 py-4"></th>
                                            <th class="px-2 py-4 text-2xl text-center">
                                                <span
                                                    class=" {{ $total_employee_points_leader_comp == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_employee_points_leader_comp }}</span>
                                            </th>
                                            <th class="w-1/4 px-2 py-4 text-center border-x-4">
                                                <div class="grid grid-cols-3 text-2xl">
                                                    <span
                                                        class="{{ $total_first_points_leader_comp == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_first_points_leader_comp }}</span>
                                                    <span
                                                        class="{{ $total_second_points_leader_comp == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_second_points_leader_comp }}</span>
                                                    <span
                                                        class="{{ $total_third_points_leader_comp == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_third_points_leader_comp }}</span>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if (($performance_scorings[0]['first_status'] == 1 &&
                $performance_scorings[0]['first_evaluator'] == Auth::user()->id) ||
                ($performance_scorings[0]['first_status'] == 2 &&
                    $performance_scorings[0]['second_evaluator'] == Auth::user()->id) ||
                ($performance_scorings[0]['second_status'] == 2 && $performance_scorings[0]['third_evaluator'] == Auth::user()->id))
                <div class="mt-12">
                    <div class="px-4 py-2 text-white rounded-md shadow-md bg-blue">
                        Remarks
                    </div>
                    <div class="-mt-2 bg-white rounded-lg shadow-md">
                        <div class="my-2 overflow-auto rounded-lg">
                            <div class="inline-block min-w-full align-middle">
                                <table class="w-full divide-y-2">
                                    <thead>
                                        <tr
                                            class="text-xs font-normal text-left text-gray-500 bg-white border-0 cursor-pointer">
                                            <th class="w-20 px-2 py-1">No.</th>
                                            <th class="w-40 px-4 py-1">Evaluator Name</th>
                                            <th class="px-4 py-1">Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody class="py-2 text-sm bg-gray-100 divide-white">
                                        @foreach ($performance_evaluators as $i => $evaluator)
                                            @if ($performance_evaluators[$i]['first_status'] == 1 &&
                                                $performance_evaluators[$i]['second_status'] == 1 &&
                                                $performance_evaluators[$i]['third_status'] == 1)
                                                <tr>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">1.</p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial" wire:init="loadEvaluators">
                                                            @foreach ($evaluator_references as $evaluator)
                                                                {{ $performance_evaluators[$i]['first_evaluator'] == $evaluator->id ? $evaluator->name : '' }}
                                                            @endforeach
                                                        </p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">
                                                            <x-textarea
                                                                name="performance_evaluators.{{ $i }}.first_remarks"
                                                                wire:model.defer='performance_evaluators.{{ $i }}.first_remarks'>
                                                            </x-textarea>
                                                        </p>
                                                    </td>
                                                </tr>
                                            @endif
                                            @if ($performance_evaluators[$i]['first_status'] == 2 &&
                                                $performance_evaluators[$i]['second_status'] == 1 &&
                                                $performance_evaluators[$i]['third_status'] == 1)
                                                <tr>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">1.</p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial" wire:init="loadEvaluators">
                                                            @foreach ($evaluator_references as $evaluator)
                                                                {{ $performance_evaluators[$i]['first_evaluator'] == $evaluator->id ? $evaluator->name : '' }}
                                                            @endforeach
                                                        </p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">
                                                            <x-textarea
                                                                name="performance_evaluators.{{ $i }}.first_remarks"
                                                                wire:model.defer='performance_evaluators.{{ $i }}.first_remarks'
                                                                disabled>
                                                            </x-textarea>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">2.</p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial" wire:init="loadEvaluators">
                                                            @foreach ($evaluator_references as $evaluator)
                                                                {{ $performance_evaluators[$i]['second_evaluator'] == $evaluator->id ? $evaluator->name : '' }}
                                                            @endforeach
                                                        </p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">
                                                            <x-textarea
                                                                name="performance_evaluators.{{ $i }}.second_remarks"
                                                                wire:model.defer='performance_evaluators.{{ $i }}.second_remarks'>
                                                            </x-textarea>
                                                        </p>
                                                    </td>
                                                </tr>
                                            @endif
                                            @if ($performance_evaluators[$i]['first_status'] == 2 &&
                                                $performance_evaluators[$i]['second_status'] == 2 &&
                                                $performance_evaluators[$i]['third_status'] == 1)
                                                <tr>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">1.</p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial" wire:init="loadEvaluators">
                                                            @foreach ($evaluator_references as $evaluator)
                                                                {{ $performance_evaluators[$i]['first_evaluator'] == $evaluator->id ? $evaluator->name : '' }}
                                                            @endforeach
                                                        </p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">
                                                            <x-textarea
                                                                name="performance_evaluators.{{ $i }}.first_remarks"
                                                                wire:model.defer='performance_evaluators.{{ $i }}.first_remarks'
                                                                disabled>
                                                            </x-textarea>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">2.</p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial" wire:init="loadEvaluators">
                                                            @foreach ($evaluator_references as $evaluator)
                                                                {{ $performance_evaluators[$i]['second_evaluator'] == $evaluator->id ? $evaluator->name : '' }}
                                                            @endforeach
                                                        </p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">
                                                            <x-textarea
                                                                name="performance_evaluators.{{ $i }}.second_remarks"
                                                                wire:model.defer='performance_evaluators.{{ $i }}.second_remarks'
                                                                disabled>
                                                            </x-textarea>
                                                        </p>
                                                    </td>
                                                </tr>
                                                @if ($performance_evaluators[$i]['third_evaluator'] != null)
                                                    <tr>
                                                        <td class="px-2 py-1">
                                                            <p class="flex-initial">3.</p>
                                                        </td>
                                                        <td class="px-2 py-1">
                                                            <p class="flex-initial" wire:init="loadEvaluators">
                                                                @foreach ($evaluator_references as $evaluator)
                                                                    {{ $performance_evaluators[$i]['third_evaluator'] == $evaluator->id ? $evaluator->name : '' }}
                                                                @endforeach
                                                            </p>
                                                        </td>
                                                        <td class="px-2 py-1">
                                                            <p class="flex-initial">
                                                                <x-textarea
                                                                    name="performance_evaluators.{{ $i }}.third_remarks"
                                                                    wire:model.defer='performance_evaluators.{{ $i }}.third_remarks'>
                                                                </x-textarea>
                                                            </p>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endif
                                            @if ($performance_evaluators[$i]['first_status'] == 2 &&
                                                $performance_evaluators[$i]['second_status'] == 2 &&
                                                $performance_evaluators[$i]['third_status'] == 2)
                                                <tr>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">1.</p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial" wire:init="loadEvaluators">
                                                            @foreach ($evaluator_references as $evaluator)
                                                                {{ $performance_evaluators[$i]['first_evaluator'] == $evaluator->id ? $evaluator->name : '' }}
                                                            @endforeach
                                                        </p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">
                                                            <x-textarea
                                                                name="performance_evaluators.{{ $i }}.first_remarks"
                                                                wire:model.defer='performance_evaluators.{{ $i }}.first_remarks'
                                                                disabled>
                                                            </x-textarea>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">2.</p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial" wire:init="loadEvaluators">
                                                            @foreach ($evaluator_references as $evaluator)
                                                                {{ $performance_evaluators[$i]['second_evaluator'] == $evaluator->id ? $evaluator->name : '' }}
                                                            @endforeach
                                                        </p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">
                                                            <x-textarea
                                                                name="performance_evaluators.{{ $i }}.second_remarks"
                                                                wire:model.defer='performance_evaluators.{{ $i }}.second_remarks'
                                                                disabled>
                                                            </x-textarea>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">3.</p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial" wire:init="loadEvaluators">
                                                            @foreach ($evaluator_references as $evaluator)
                                                                {{ $performance_evaluators[$i]['third_evaluator'] == $evaluator->id ? $evaluator->name : '' }}
                                                            @endforeach
                                                        </p>
                                                    </td>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">
                                                            <x-textarea
                                                                name="performance_evaluators.{{ $i }}.third_remarks"
                                                                wire:model.defer='performance_evaluators.{{ $i }}.third_remarks'
                                                                disabled>
                                                            </x-textarea>
                                                        </p>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                @if (($performance_evaluators[$i]['first_status'] == 2 &&
                    $performance_evaluators[$i]['second_status'] == 2 &&
                    $performance_evaluators[$i]['third_status'] == 2) ||
                    ($performance_evaluators[$i]['first_status'] == 2 &&
                        $performance_evaluators[$i]['second_status'] == 2 &&
                        $performance_evaluators[$i]['third_status'] == 1 &&
                        $performance_evaluators[$i]['third_evaluator'] == null))
                    <div class="grid justify-between grid-cols-2 gap-4">
                        <button type="button" wire:click=""
                            class="px-6 py-2 text-sm font-medium transition-all duration-300 border text-white bg-gray-300 rounded-md focus:outline-none"
                            disabled>
                            Save Evaluation
                        </button>
                        <button type="button" wire:click=""
                            class="px-6 py-2 text-sm flex-none bg-blue-300 text-white rounded-md focus:outline-none"
                            disabled>
                            Close Evaluation
                        </button>
                    </div>
                @else
                    <div class="grid justify-between grid-cols-2 gap-4">
                        <button type="button" wire:click="action({}, 'save')"
                            class="px-6 py-2 text-sm font-medium text-blue-800 transition-all duration-300 border border-blue-800 rounded-md hover:bg-blue-800 hover:text-white">
                            Save Evaluation
                        </button>
                        <button type="button" wire:click="action({}, 'close')"
                            class="px-6 py-2 text-sm flex-none bg-[#003399] text-white rounded-md hover:bg-blue-700">
                            Close Evaluation
                        </button>
                    </div>
                @endif
            @endif
        </div>
    </x-slot>
</x-form>
