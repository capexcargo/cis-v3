<div>
    <form autocomplete="off">
        <div class="bg-white rounded-lg shadow-md">
            <x-table.table>
                <x-slot name="thead">
                    <th class="p-3 whitespace-nowrap border-2 border-b-0 text-gray-400">Year</th>
                    <th class="p-3 whitespace-nowrap border-2 border-b-0 text-gray-400">Quarter</th>
                    <th class="p-3 whitespace-nowrap border-2 border-b-0 text-gray-400">Action</th>
                </x-slot>
                <x-slot name="tbody">
                    <x-loading></x-loading>
                    @forelse($previous_evaluations as $i => $evaluation)
                        <tr>
                            <td class="p-3 whitespace-nowrap border-2 border-b-0">
                                {{ $evaluation->year }}
                            </td>
                            <td class="p-3 whitespace-nowrap border-2 border-b-0">
                                Quarter {{ $evaluation->quarter }}
                            </td>
                            <td class="p-3 whitespace-nowrap border-2 border-b-0">
                                @if (($evaluation->third_evaluator == '' && $evaluation->second_status == 2) ||
                                    ($evaluation->third_evaluator != '' && $evaluation->third_status == 2))
                                    <span class="text-sm text-blue underline cursor-pointer"
                                        wire:click="action({'id':{{ $evaluation->user_id }},'quarter':{{ $evaluation->quarter }}, 'year':{{ $evaluation->year }}}, 'view')">view</span>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <div class="mt-12 text-md">
                            <span class="italic bg-gray-200 p-2">There's no evaluation yet.</span>
                        </div>
                    @endforelse
                </x-slot>
            </x-table.table>
        </div>
    </form>
</div>
