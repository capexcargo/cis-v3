<x-form wire:init="loadEmployee" x-data="{
    search_form: false,
    view_previous_modal: '{{ $view_previous_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @if ($employee_id && $view_previous_modal)
        <x-modal id="view_previous_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                @livewire('hrim.employee-talent-management.performance-evaluation.view-previous-evaluation', ['id' => $employee_id])
            </x-slot>
        </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">
        Competency Review
    </x-slot>
    <x-slot name="header_button">
        <button wire:click="action({'id':'{{ Auth::user()->id }}'}, 'view_previou_eval')"
            class="p-2 px-4 mr-3 text-sm text-white rounded-md bg-blue">
            <div class="flex items-start justify-between">
                Previous Evaluation
            </div>
        </button>
    </x-slot>
    <x-slot name="body">
        <div class="">
            <div class="grid grid-cols-5 gap-4 text-sm md:w-3/4">
                <div class="w-40">
                    <div>
                        <x-transparent.input type="date" label="Evaluation Date" name="evaluation_date"
                            wire:model.debounce.300ms="evaluation_date" />
                    </div>
                </div>
                <div class="w-40">
                    <div>
                        <x-transparent.select value="{{ $quarter }}" label="Quarter" name="quarter"
                            wire:model.debounce.300ms="quarter">
                            <option value=""></option>
                            <option value="1">1st Quarter</option>
                            <option value="2">2nd Quarter</option>
                            <option value="3">3rd Quarter</option>
                            <option value="4">4th Quarter</option>
                        </x-transparent.select>
                    </div>
                </div>
                <div class="w-40">
                    <div>
                        <x-transparent.select value="{{ $year }}" label="Year" name="year"
                            wire:model.debounce.300ms="year">
                            <option value=""></option>
                            @for ($i = 2011; $i <= date('Y'); $i++)
                                <option value="{{ $i }}">
                                    {{ $i }}
                                </option>
                            @endfor
                        </x-transparent.select>
                    </div>
                </div>
            </div>

            <div class="grid grid-cols-3 items-stretch mb-4 text-md">
                <div></div>
                <div class="self-auto">
                    <div class="text-lg ml-24 font-normal">
                        Rating Scale :
                    </div>
                    <div class="flex mt-4 text-md">
                        <div class="flex gap-6 text-md text-between">
                            <div class="">
                                <p class="flex-initial font-normal">
                                    <span class="font-bold text-blue">5.00</span> - Outstanding
                                </p>
                                <p class="flex-initial font-normal">
                                    <span class="font-bold text-blue">4.00</span> - Excellent
                                </p>
                                <p class="flex-initial font-normal">
                                    <span class="font-bold text-blue">3.00</span> - Good
                                </p>
                            </div>
                            <div class="">
                                <p class="flex-initial font-normal">
                                    <span class="font-bold text-blue">2.00</span> - Satisfactory
                                </p>
                                <p class="flex-initial font-normal">
                                    <span class="font-bold text-blue">1.00</span> - Needs Improvement
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div></div>
            </div>

            @if ($core_values != null || $leadership_comps != null || $employee_evals != null)
                @if (count($core_values) > 0)
                    <div class="mt-12">
                        <div class="px-4 py-2 text-white rounded-md shadow-md bg-blue">
                            A. Core Value Assessment
                        </div>
                        <div class="-mt-2 bg-white rounded-lg shadow-md">
                            <div class="my-2 overflow-auto rounded-lg">
                                <div class="inline-block min-w-full align-middle">
                                    <table class="w-full divide-y-2">
                                        <thead>
                                            <tr
                                                class="w-full text-xs font-normal text-left text-gray-500 bg-white border-0 cursor-pointer">
                                                <th class="w-1/6 px-2 py-1">No.</th>
                                                <th class="w-3/5 px-4 py-1">Core Values</th>
                                                <th class="w-40 px-2 py-1 text-center">Self Review</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-sm bg-gray-100 divide-white">
                                            @foreach ($core_values as $i => $core_value)
                                                <tr>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">{{ $i + 1 }}.
                                                        </p>
                                                    </td>
                                                    <td class="px-4 py-1">
                                                        <p class="">
                                                            {{ $core_values[$i]['description'] }}
                                                        </p>
                                                    </td>
                                                    <td class="px-2 py-2 font-normal text-center">
                                                        <div class="grid grid-cols-3 gap-2 mt-2 text-2xl">
                                                            <span></span>
                                                            <span class="text-2xl text-blue">
                                                                @if ($core_values[$i]['employee_self_points'] == '')
                                                                    <x-select
                                                                        name="core_values.{{ $i }}.employee_self_eval_points"
                                                                        wire:model.defer='core_values.{{ $i }}.employee_self_eval_points'
                                                                        wire:change="employee_self_eval_core_value">
                                                                        <option value="">Select</option>
                                                                        @foreach ($evaluation_points_references as $evaluation_points)
                                                                            <option
                                                                                value="{{ $evaluation_points->id }}">
                                                                                {{ $evaluation_points->points }}
                                                                            </option>
                                                                        @endforeach
                                                                    </x-select>
                                                                    <x-input-error
                                                                        for="core_values.{{ $i }}.employee_self_eval_points" />
                                                                @else
                                                                    {{ $core_values[$i]['employee_self_points'] }}
                                                                @endif
                                                            </span>
                                                            <span></span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @if ($core_values[$i]['employee_self_eval_points'] > 3)
                                                    <tr>
                                                        <td class="px-2 py-1">
                                                        </td>
                                                        <td class="px-4 py-1">
                                                            <div class="grid grid-cols-1 justify-start">
                                                                <span class="text-l font-semibold">
                                                                    <x-label for="core_value"
                                                                        value="Actual Critical Incident :"
                                                                        :required="true" />
                                                                </span>
                                                                @if ($core_values[$i]['critical_remarks'] == '')
                                                                    <span>
                                                                        <x-input
                                                                            name="core_values.{{ $i }}.employee_critical_remarks"
                                                                            wire:model.defer='core_values.{{ $i }}.employee_critical_remarks'>
                                                                        </x-input>
                                                                        <x-input-error
                                                                            for="core_values.{{ $i }}.employee_critical_remarks" />
                                                                    </span>
                                                                @else
                                                                    <span class="">
                                                                        <x-input name=""
                                                                            value="{{ $core_values[$i]['critical_remarks'] }}"
                                                                            disabled>
                                                                        </x-input>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </td>
                                                        <td class="px-2 py-2 font-normal text-center">

                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                        <thead>
                                            <tr
                                                class="text-sm font-normal text-left bg-white border-0 cursor-pointer text-blue">
                                                <th class="w-1/6 px-2 py-4">Average Score :</th>
                                                <th class="w-3/5 px-2 py-4"></th>
                                                <th class="px-2 py-4 text-2xl text-center">
                                                    <div class="grid grid-cols-3 gap-4 mt-2 text-2xl">
                                                        <span></span>
                                                        <span
                                                            class="{{ $total_employee_points_core_value == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_employee_points_core_value }}</span>
                                                        <span></span>
                                                    </div>
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if (count($leadership_comps) > 0)
                    <div class="mt-12">
                        <div class="px-4 py-2 text-white rounded-md shadow-md bg-blue">
                            B. Leadership Competencies
                        </div>
                        <div class="-mt-2 bg-white rounded-lg shadow-md">
                            <div class="my-2 overflow-auto rounded-lg">
                                <div class="inline-block min-w-full align-middle">
                                    <table class="w-full divide-y-2">
                                        <thead>
                                            <tr
                                                class="text-xs font-normal text-left text-gray-500 bg-white border-0 cursor-pointer">
                                                <th class="w-1/6 px-2 py-1">No.</th>
                                                <th class="w-3/5 px-4 py-1">Leadership Competencies</th>
                                                <th class="w-40 px-2 py-1 text-center">Self Review</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-sm bg-gray-100 divide-white">
                                            @foreach ($leadership_comps as $i => $leadership_comp)
                                                <tr>
                                                    <td class="px-2 py-1">
                                                        <p class="flex-initial">{{ $i + 1 }}.
                                                        </p>
                                                    </td>
                                                    <td class="px-4 py-1">
                                                        <p class="flex-initial">
                                                            {{ $leadership_comps[$i]['description'] }}
                                                        </p>
                                                    </td>
                                                    <td class="px-2 py-2 font-normal text-center">
                                                        <div class="grid grid-cols-3 gap-2 mt-2 text-2xl">
                                                            <span></span>
                                                            <span class="text-2xl text-blue">
                                                                @if ($leadership_comps[$i]['employee_self_points'] == '')
                                                                    <x-select
                                                                        name="leadership_comps.{{ $i }}.employee_self_eval_points"
                                                                        wire:model.defer='leadership_comps.{{ $i }}.employee_self_eval_points'
                                                                        wire:change="employee_self_eval_leader_comp">
                                                                        <option value="">Select</option>
                                                                        @foreach ($evaluation_points_references as $evaluation_points)
                                                                            <option
                                                                                value="{{ $evaluation_points->id }}">
                                                                                {{ $evaluation_points->points }}
                                                                            </option>
                                                                        @endforeach
                                                                    </x-select>
                                                                    <x-input-error
                                                                        for="leadership_comps.{{ $i }}.employee_self_eval_points" />
                                                                @else
                                                                    {{ $leadership_comps[$i]['employee_self_points'] }}
                                                                @endif
                                                            </span>
                                                            <span></span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @if ($leadership_comps[$i]['employee_self_eval_points'] > 3)
                                                    <tr>
                                                        <td class="px-2 py-1">
                                                        </td>
                                                        <td class="px-4 py-1">
                                                            <div class="grid grid-cols-1 justify-start">
                                                                <span class="text-l font-semibold">
                                                                    <x-label for="core_value"
                                                                        value="Actual Critical Incident :"
                                                                        :required="true" />
                                                                </span>
                                                                @if ($leadership_comps[$i]['critical_remarks'] == '')
                                                                    <span>
                                                                        <x-input
                                                                            name="leadership_comps.{{ $i }}.employee_critical_remarks"
                                                                            wire:model.defer='leadership_comps.{{ $i }}.employee_critical_remarks'>
                                                                        </x-input>
                                                                        <x-input-error
                                                                            for="leadership_comps.{{ $i }}.employee_critical_remarks" />
                                                                    </span>
                                                                @else
                                                                    <span class="">
                                                                        <x-input name=""
                                                                            value="{{ $leadership_comps[$i]['critical_remarks'] }}"
                                                                            disabled>
                                                                        </x-input>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </td>
                                                        <td class="px-2 py-2 font-normal text-center">

                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                        <thead>
                                            <tr
                                                class="text-sm font-normal text-left bg-white border-0 cursor-pointer text-blue">
                                                <th class="w-1/6 px-2 py-4">Average Score :</th>
                                                <th class="w-3/5 px-2 py-4"></th>
                                                <th class="px-2 py-4 text-2xl text-center">
                                                    <div class="grid grid-cols-3 gap-4 mt-2 text-2xl">
                                                        <span></span>
                                                        <span
                                                            class="{{ $total_employee_points_leader_comp == 0 ? 'text-gray-200' : 'text-blue' }}">{{ $total_employee_points_leader_comp }}</span>
                                                        <span></span>
                                                    </div>
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if (count($employee_evals) > 0)
                    @foreach ($employee_evals as $i => $employee_eval)
                        <div class="mt-10">
                            <div class=" py-2 text-blue">
                                Employee Comment
                            </div>
                            <div class="-mt-2 bg-white rounded-lg shadow-md">
                                @if ($employee_evals[$i]['employee_eval_status'] == 1)
                                    <x-textarea name="employee_remarks" wire:model.defer='employee_remarks'>
                                    </x-textarea>
                                @else
                                    <x-textarea name="employee_remarks" disabled>
                                        {{ $core_values[0]['employee_remarks'] }}
                                    </x-textarea>
                                @endif
                            </div>
                        </div>
                        <div class="grid grid-cols-1 mt-10">
                            @if ($core_values != null)
                                @if ($employee_evals[$i]['employee_eval_status'] == 1)
                                    <button type="button" wire:key="submit" wire:click="action({}, 'save')"
                                        class="px-6 py-3 text-sm flex-none bg-[#003399] text-white rounded-md">
                                        Save
                                    </button>
                                @else
                                    <button type="button"
                                        class="px-6 py-3 bg-blue-300 text-white rounded-md focus:outline-non ">
                                        Save
                                    </button>
                                @endif
                            @endif
                        </div>
                    @endforeach
                @endif
            @else
                <div class="mt-12 text-md">
                    <span class="italic bg-gray-200 p-2">There's no available evaluation for the selected quarter
                        or year.</span>
                </div>
            @endif
        </div>
    </x-slot>
</x-form>
