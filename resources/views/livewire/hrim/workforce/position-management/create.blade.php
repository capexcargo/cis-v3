<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="position" value="Position" :required="true" />
                    <x-input type="text" name="position" wire:model.defer='position'></x-input>
                    <x-input-error for="position" />
                </div>
                <div wire:init="loadDivision">
                    <x-label for="division" value="Division" :required="true" />
                    <x-select name="division" wire:model='division'>
                        <option value=""></option>
                        @foreach ($divisions as $division)
                            <option value="{{ $division->id }}">
                                {{ $division->description }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="division" />
                </div>
                <div wire:init="loadDepartment">
                    <x-label for="department" value="Department" :required="true" />
                    <x-select name="department" wire:model='department'>
                        <option value=""></option>
                        @foreach ($departments as $department)
                            <option value="{{ $department->id }}">
                                {{ $department->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="department" />
                </div>
                <div wire:init="loadJobLevel">
                    <x-label for="job_level" value="Job Level" :required="true" />
                    <x-select name="job_level" wire:model='job_level'>
                        <option value=""></option>
                        @foreach ($job_levels as $job_level)
                            <option value="{{ $job_level->id }}">
                                {{ $job_level->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="job_level" />
                </div>
                <div>
                    <x-label for="job_overview" value="Job Overview" :required="true" />
                    <x-textarea type="text" name="job_overview" wire:model.defer='job_overview'></x-textarea>
                    <x-input-error for="job_overview" />
                </div>

                <div class="text-xl">
                    <x-label for="responsibilities" value="Responsibilities and Duties" :required="true" />
                    @foreach ($responsibilities as $j => $responsibility)
                        <div class="relative z-0 ">
                            <x-textarea type="text" name="responsibilities.{{ $j }}.duties"
                                wire:model.defer='responsibilities.{{ $j }}.duties'>
                            </x-textarea>
                        </div>
                        <div class="relative w-full mb-2">
                            <div class="absolute right-0 w-18">
                                @if (count($responsibilities) > 1)
                                    <svg wire:click="removeResponsibilities({{ $j }})"
                                        class="w-3 h-3 cursor-pointer text-red"
                                        style="margin-top: -4rem; margin-right: -3px;" aria-hidden="true"
                                        focusable="false" data-prefix="fas" data-icon="times-circle" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                        </path>
                                    </svg>
                                @endif
                            </div>
                            <x-input-error for="responsibilities.{{ $j }}.duties" />
                        </div>
                    @endforeach

                    <div class="flex items-center justify-start">
                        <button type="button" wire:click="addResponsibilities" title="Add Job Responsibilities Field"
                            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                            +</button>
                    </div>
                </div>

                <div class="text-xl">
                    <x-label for="requirements" value="Job Requirements" :required="true" />
                    @foreach ($requirements as $i => $requirement)
                        <div class="relative z-0 ">
                            <x-textarea type="text" name="requirements.{{ $i }}.code"
                                wire:model.defer='requirements.{{ $i }}.code'>
                            </x-textarea>
                        </div>
                        <div class="relative w-full mb-2">
                            <div class="absolute right-0 w-18">
                                @if (count($requirements) > 1)
                                    <svg wire:click="removeRequirements({{ $i }})"
                                        class="w-3 h-3 cursor-pointer text-red"
                                        style="margin-top: -4rem; margin-right: -3px;" aria-hidden="true"
                                        focusable="false" data-prefix="fas" data-icon="times-circle" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                        </path>
                                    </svg>
                                @endif
                            </div>
                            <x-input-error for="requirements.{{ $i }}.code" />
                        </div>
                    @endforeach

                    <div class="flex items-center justify-start">
                        <button type="button" wire:click="addRequirements" title="Add Job Responsibilities Field"
                            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                            +</button>
                    </div>
                </div>
            </div>
            <div class="flex justify-end gap-3 pt-5 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-md hover:text-white hover:bg-red-400">Cancel</button>
                <button type="submit" class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-md">
                    Submit</button>
            </div>
        </div>
    </form>
</div>
