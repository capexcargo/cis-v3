<div wire:init="load">
    <p class="text-xl -mt-4 font-medium">Add <span class="text-blue">{{ $division }}</span> Organization Chart</p>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="flex flex-col space-y-3">
            <div class="py-2 whitespace-nowrap">
                <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                    <div class="relative z-0 ">
                        <div class="absolute top-0 left-0 right-0">
                            @if ($attachment_id && !$attachment)
                                @if (in_array($attachment_extension, config('filesystems.image_type')))
                                    <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                        <a href="{{ Storage::disk('hrim_gcs')->url($attachment_path . $attachment_name) }}"
                                            target="_blank"><img
                                                class="w-48 h-48 mx-auto border border-gray-500 rounded-lg "
                                                src="{{ Storage::disk('hrim_gcs')->url($attachment_path . $attachment_name) }}"></a>
                                    </div>
                                @endif
                            @else
                                <img class="object-contain w-48 h-48 mx-auto border border-gray-500 rounded-lg "
                                    src="{{ $attachment ? $attachment->temporaryUrl() : asset('images/form/add-image.png') }}">
                            @endif
                        </div>
                        <input type="file" name="attachment" wire:model="attachment"
                            class="relative z-50 block w-48 h-48 opacity-0 cursor-pointer">
                        <x-input-error for="attachment" />
                    </div>
                </div>
            </div>
        </div>
        <div class="grid grid-cols-2 gap-4 mt-4">
            <x-button type="button" wire:click="$emit('close_modal', 'upload')" title="Cancel"
                class="py-1 px-4 bg-white text-blue hover:bg-gray-100" />
            <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161] py-1 px-4" />
        </div>
    </form>
</div>
