<div wire:init="load" x-data="{ confirmation_modal: @entangle('confirmation_modal') }">
    <x-loading />
    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <div class="space-y-3">
                <p class="text-center">{{ $confirmation_message }}</p>
                <div class="flex items-center justify-center space-x-3">
                    <x-button type="button" wire:click="$set('confirmation_modal', false)" title="No"
                        class="py-1 bg-white text-blue hover:bg-gray-100" />
                    <x-button type="button" wire:click="confirm" title="Yes"
                        class="bg-blue text-white hover:bg-[#002161] py-1" />
                </div>
            </div>
        </x-slot>
    </x-modal>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-4">
            <div class="grid grid-cols-4 gap-3">
                <div>
                    <x-label for="division" value="Division" />
                    <x-select name="division" wire:model='division'>
                        <option value=""></option>
                        @foreach ($division_references as $division_reference)
                            <option value="{{ $division_reference->id }}">
                                {{ $division_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="division" />
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th class="w-4/12" name="Role" />
                        <x-table.th class="w-4/12" name="Reports To" />
                        <x-table.th class="w-4/12" name="Department" />
                        {{-- <x-table.th class="w-1/4" name="Section" /> --}}
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($teams as $i => $team)
                            <tr class="cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    <x-select name="teams.{{ $i }}.role"
                                        wire:model.defer='teams.{{ $i }}.role'>
                                        <option value=""></option>
                                        @foreach ($position_references as $position_reference)
                                            <option value="{{ $position_reference->id }}">
                                                {{ $position_reference->display }}
                                            </option>
                                        @endforeach
                                    </x-select>
                                    <x-input-error for="teams.{{ $i }}.role" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <x-select name="teams.{{ $i }}.reports_to"
                                        wire:model.defer='teams.{{ $i }}.reports_to'>
                                        <option value=""></option>
                                        @foreach ($position_references as $position_reference)
                                            <option value="{{ $position_reference->id }}">
                                                {{ $position_reference->display }}
                                            </option>
                                        @endforeach
                                    </x-select>
                                    <x-input-error for="teams.{{ $i }}.reports_to" />
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <x-select name="teams.{{ $i }}.department"
                                        wire:model.defer='teams.{{ $i }}.department'>
                                        <option value=""></option>
                                        @foreach ($department_references as $department_reference)
                                            <option value="{{ $department_reference->id }}">
                                                {{ $department_reference->display }}
                                            </option>
                                        @endforeach
                                    </x-select>
                                    <x-input-error for="teams.{{ $i }}.department" />
                                </td>
                                {{-- <td class="p-3 whitespace-nowrap">
                                    <x-select name="teams.{{ $i }}.section"
                                        wire:model.defer='teams.{{ $i }}.section'>
                                        <option value=""></option>
                                        @foreach ($section_references as $section_reference)
                                            <option value="{{ $section_reference->id }}">
                                                {{ $section_reference->display }}
                                            </option>
                                        @endforeach
                                    </x-select>
                                    <x-input-error for="teams.{{ $i }}.section" />
                                </td> --}}
                                <td class="p-3 whitespace-nowrap">
                                    @if (count(collect($teams)->where('is_deleted', false)) > 1)
                                        <svg wire:click="action({index:'{{ $i }}'},'remove')"
                                            class="w-5 h-5 text-red" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M135.2 17.69C140.6 6.848 151.7 0 163.8 0H284.2C296.3 0 307.4 6.848 312.8 17.69L320 32H416C433.7 32 448 46.33 448 64C448 81.67 433.7 96 416 96H32C14.33 96 0 81.67 0 64C0 46.33 14.33 32 32 32H128L135.2 17.69zM31.1 128H416V448C416 483.3 387.3 512 352 512H95.1C60.65 512 31.1 483.3 31.1 448V128zM111.1 208V432C111.1 440.8 119.2 448 127.1 448C136.8 448 143.1 440.8 143.1 432V208C143.1 199.2 136.8 192 127.1 192C119.2 192 111.1 199.2 111.1 208zM207.1 208V432C207.1 440.8 215.2 448 223.1 448C232.8 448 240 440.8 240 432V208C240 199.2 232.8 192 223.1 192C215.2 192 207.1 199.2 207.1 208zM304 208V432C304 440.8 311.2 448 320 448C328.8 448 336 440.8 336 432V208C336 199.2 328.8 192 320 192C311.2 192 304 199.2 304 208z" />
                                        </svg>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <x-input-error for="teams" />
            </div>
            <div class="flex justify-end">
                <x-button type="button" wire:click="add" title="Add Row"
                    class="bg-blue text-white hover:bg-[#002161] py-1" />
            </div>
            <div class="flex justify-end space-x-3">
                <x-button type="button" wire:click="$emit('close_modal', 'create')" title="Cancel"
                    class="py-1 bg-white text-blue hover:bg-gray-100" />
                <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161] py-1" />
            </div>
        </div>
    </form>
</div>
