<x-form x-data="{
    search_form: false,
    confirmation_modal: '{{ $confirmation_modal }}',
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    upload_modal: '{{ $upload_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                <div class="space-y-3">
                    <p class="text-center">{{ $confirmation_message }}</p>
                    <div class="flex items-center justify-center space-x-3">
                        <x-button type="button" wire:click="$set('confirmation_modal', false)" title="No"
                            class="py-1 bg-white text-blue hover:bg-gray-100" />
                        <x-button type="button" wire:click="confirm" title="Yes"
                            class="bg-blue text-white hover:bg-[#002161] py-1" />
                    </div>
                </div>
            </x-slot>
        </x-modal>

        @can('hrim_work_schedule_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/4">
                <x-slot name="title">Add a New Team</x-slot>
                <x-slot name="body">
                    @livewire('hrim.workforce.teams.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_work_schedule_edit')
            @if ($edit_modal && $division_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/4">
                    <x-slot name="title">Edit Team</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.workforce.teams.edit', ['id' => $division_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @if ($upload_modal && $division_id)
            <x-modal id="upload_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/5">
                <x-slot name="body">
                    @livewire('hrim.workforce.teams.uploadfile', ['id' => $division_id])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">Teams</x-slot>
    <x-slot name="header_button">
        @can('hrim_work_schedule_add')
            <x-button type="button" wire:click="$set('create_modal', true)" title="Create a New Team"
                class="bg-blue text-white hover:bg-[#002161]">
                <x-slot name="icon">
                    <svg class="w-3 h-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                </x-slot>
            </x-button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="mx-auto">
            <ul class="divide-y-8 divide-transparent" x-data="{ selected: null }">
                @foreach ($divisions as $i => $division)
                    <li class="relative">
                        <button type="button"
                            class="w-full px-8 py-3 text-left bg-white rounded-md shadow-md hover:bg-gray-200"
                            @click="selected !== {{ $i }} ? selected = {{ $i }} : selected = null">
                            <div
                                class="flex items-center justify-between text-base font-medium text-left text-gray-600 uppercase ">
                                <span
                                    class="{{ $division->teams_status ? 'text-gray-600' : 'text-red-500' }}">{{ $division->description }}</span>
                                <span>
                                    <svg x-cloak x-show="selected != '{{ $i }}'" class="w-7 h-7"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                                        <path fill="currentColor"
                                            d="M137.4 406.6l-128-127.1C3.125 272.4 0 264.2 0 255.1s3.125-16.38 9.375-22.63l128-127.1c9.156-9.156 22.91-11.9 34.88-6.943S192 115.1 192 128v255.1c0 12.94-7.781 24.62-19.75 29.58S146.5 415.8 137.4 406.6z" />
                                    </svg>
                                    <svg x-cloak x-show="selected == '{{ $i }}'" class="w-7 h-7"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                        <path fill="currentColor"
                                            d="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z" />
                                    </svg>
                                </span>
                            </div>
                        </button>
                        <div class="relative overflow-hidden transition-all duration-700 max-h-0" style=""
                            x-ref="container{{ $i }}"
                            x-bind:style="selected == {{ $i }} ? 'max-height: ' + $refs.container{{ $i }}
                                .scrollHeight + 'px' : ''">
                            <div class="py-3 space-y-3">
                                <div class="flex items-center justify-end space-x-3">
                                    @can('hrim_work_schedule_add')
                                        <x-button type="button" wire:click="action({'id': {{ $division->id }}}, 'upload')"
                                            title="Add File" class="bg-blue text-white hover:bg-blue-900 py-1">
                                            <x-slot name="icon">
                                                <svg class="w-4 h-4 " xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 448 512">
                                                    <path fill="currentColor"
                                                        d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm160-14.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z" />
                                                </svg>
                                            </x-slot>
                                        </x-button>
                                        @if ($division->teams_status)
                                            <x-button type="button"
                                                wire:click="action({'id': {{ $division->id }}}, 'deactivate')"
                                                title="Deactivate" class="py-1 bg-white text-blue hover:bg-gray-100" />
                                        @else
                                            <x-button type="button"
                                                wire:click="action({'id': {{ $division->id }}}, 'reactivate')"
                                                title="Reactivate" class="py-1 bg-white text-blue hover:bg-gray-100" />
                                        @endif
                                        <x-button type="button" wire:click="action({'id': {{ $division->id }}}, 'edit')"
                                            title="Edit" class="bg-blue text-white hover:bg-[#002161] py-1" />
                                    @endcan
                                </div>
                                <div class="px-6">
                                    <div class="bg-white rounded-lg shadow-md">
                                        <x-table.table>
                                            <x-slot name="thead">
                                                <x-table.th class="w-1/4" name="Role" />
                                                <x-table.th class="w-1/4" name="Reports To" />
                                                <x-table.th class="w-1/4" name="Department" />
                                                {{-- <x-table.th class="w-1/4" name="Section" /> --}}
                                            </x-slot>
                                            <x-slot name="tbody">
                                                @foreach ($division->teams as $i => $team)
                                                    <tr class="cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                                        <td class="p-3 whitespace-nowrap">
                                                            {{ $team->position->display }}
                                                        </td>
                                                        <td class="p-3 whitespace-nowrap">
                                                            {{ $team->reportsTo->display }}
                                                        </td>
                                                        <td class="p-3 whitespace-nowrap">
                                                            {{ $team->department->display }}
                                                        </td>
                                                        {{-- <td class="p-3 whitespace-nowrap">
                                                            {{ $team->section->display }}
                                                        </td> --}}
                                                    </tr>
                                                @endforeach
                                            </x-slot>
                                        </x-table.table>
                                    </div>
                                </div>
                                <div class="px-6 pt-8">
                                    @foreach ($division->attachments as $attachment)
                                        <div class="flex-shrink-0 mb-1 mr-1">
                                            <a href="{{ Storage::disk('hrim_gcs')->url($attachment->path . $attachment->name) }}"
                                                target="_blank"><img class="w-100 h-96 mx-auto rounded-lg "
                                                    src="{{ Storage::disk('hrim_gcs')->url($attachment->path . $attachment->name) }}"></a>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="flex items-center justify-end pr-1">
                                    <i class="text-xs {{ $division->teams_status ?: 'text-red-500 ' }}">{{ $division->teams_status ? 'Last Updated: ' : 'Deactivated: ' }}
                                        {{ date('F d, Y', strtotime($division->updated_at)) }}</i>
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </x-slot>
</x-form>
