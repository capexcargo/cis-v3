<div class="space-y-4 cursor-pointer">
    <div class="grid grid-cols-2">
        <div class="text-sm text-gray-400">Work Schedule :</div>
        <div>{{ $data['work_schedule'] }}</div>
    </div>
    <div class="grid grid-cols-2">
        <div class="text-sm text-gray-400">Inclusive Days :</div>
        <div>{{ ucwords(strtolower(implode(', ', $data['inclusive_days']))) ?? null }}</div>
    </div>
    <div class="grid grid-cols-2">
        <div class="text-sm text-gray-400">Time :</div>
        <div>{{ date('h:i A', strtotime($data['time_from'])) }} - {{ date('h:i A', strtotime($data['time_to'])) }}</div>
    </div>
    <div class="grid grid-cols-2">
        <div class="text-sm text-gray-400">Break :</div>
        <div>{{ date('h:i A', strtotime($data['break_from'])) }} - {{ date('h:i A', strtotime($data['break_to'])) }}
        </div>
    </div>
    <div class="grid grid-cols-2">
        <div class="text-sm text-gray-400">OT Break :</div>
        <div>{{ date('h:i A', strtotime($data['ot_break_from'])) }} -
            {{ date('h:i A', strtotime($data['ot_break_to'])) }}</div>
    </div>
    <div class="flex justify-center space-x-4">
        {{-- <button type="submit" wire:click="closePreview"
            class="px-10 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-red-400">
            Cancel
        </button> --}}
        <button type="button" wire:click="submit"
            class="px-10 py-2 text-sm font-medium bg-blue text-white hover:bg-[#002161] border border-[#003399] rounded-lg">
            Confirm
        </button>
    </div>
</div>
