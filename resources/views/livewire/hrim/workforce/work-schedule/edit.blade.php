<div wire:init="load">
    <x-loading />
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-6">
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label value="Work Schedule" :required="true" />
                    <x-input type="text" name="work_schedule" wire:model.defer='work_schedule' />
                    <x-input-error for="work_schedule" />
                </div>
                <div>
                    <x-label value="Work Mode" :required="true" />
                    <x-select name="work_mode" wire:model.defer='work_mode'>
                        <option value=""></option>
                        @foreach ($work_mode_references as $work_mode_reference)
                            <option value="{{ $work_mode_reference->id }}">
                                {{ $work_mode_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="work_mode" />
                </div>
                <div>
                    <x-label value="Shift" :required="true" />
                    <x-select name="shift" wire:model='shift'>
                        <option value=""></option>
                        <option value="day_shift">Day Shift</option>
                        <option value="mid_shift">Mid Shift</option>
                        <option value="night_shift">Night Shift</option>
                    </x-select>
                    <x-input-error for="shift" />
                    @if ($shift == 'night_shift')
                        <p class="mt-2 text-sm text-red-600">*Note: All Time Out and Overtime dates will be on the next
                            day
                            automatically.</p>
                    @endif
                </div>
            </div>
            <div class="grid grid-cols-2 gap-3">
                <div class="col-span-5">
                    <x-label value="Inclusive Days" :required="true" />
                </div>
                <div>
                    <x-input-checkbox label="Monday" name="monday" value="monday" wire:model.defer="inclusive_days" />
                    <x-input-checkbox label="Tuesday" name="tuesday" value="tuesday"
                        wire:model.defer="inclusive_days" />
                    <x-input-checkbox label="Wednesday" name="wednesday" value="wednesday"
                        wire:model.defer="inclusive_days" />
                    <x-input-checkbox label="Thursday" name="thursday" value="thursday"
                        wire:model.defer="inclusive_days" />
                </div>
                <div>
                    <x-input-checkbox label="Friday" name="friday" value="friday" wire:model.defer="inclusive_days" />
                    <x-input-checkbox label="Saturday" name="saturday" value="saturday"
                        wire:model.defer="inclusive_days" />
                    <x-input-checkbox label="Sunday" name="sunday" value="sunday" wire:model.defer="inclusive_days" />
                </div>
                <div class="col-span-5">
                    <x-input-error for="inclusive_days" />
                </div>
            </div>
            <div class="grid grid-cols-2 gap-3">
                <div>
                    <x-label value="Time From" :required="true" />
                    <x-input type="time" name="time_from" wire:model.defer='time_from' placeholder="From" />
                    <x-input-error for="time_from" />
                </div>
                <div>
                    <x-label value="Time To" :required="true" />
                    <x-input type="time" name="time_to" wire:model.defer='time_to' />
                    <x-input-error for="time_to" />
                </div>
            </div>
            <div class="grid grid-cols-2 gap-3">
                <div>
                    <x-label value="Break From" :required="true" />
                    <x-input type="time" name="break_from" wire:model.defer='break_from' />
                    <x-input-error for="break_from" />
                </div>
                <div>
                    <x-label value="Break To" :required="true" />
                    <x-input type="time" name="break_to" wire:model.defer='break_to' />
                    <x-input-error for="break_to" />
                </div>
            </div>
            <div class="grid grid-cols-2 gap-3">
                <div>
                    <x-label value="OT Break From" :required="true" />
                    <x-input type="time" name="ot_break_from" wire:model.defer='ot_break_from' />
                    <x-input-error for="ot_break_from" />
                </div>
                <div>
                    <x-label value="OT Break To" :required="true" />
                    <x-input type="time" name="ot_break_to" wire:model.defer='ot_break_to' />
                    <x-input-error for="ot_break_to" />
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <x-button type="button" wire:click="$emit('close_modal', 'edit')" title="Cancel"
                    class="bg-white text-blue hover:bg-gray-100" />
                <x-button type="submit" title="Save" class="bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>
    </form>
</div>
