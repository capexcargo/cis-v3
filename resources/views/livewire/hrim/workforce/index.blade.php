<x-form x-data="{ search_form: false">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="header_title">Organizational Structure</x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-6 gap-4">
            <button wire:click="action({}, 'redirectToPosition')"
                class="p-1 w-50 text-gray-700 bg-white border border-blue border-solid rounded-md hover:bg-blue-100">
                <div class="items-center px-6 py-3">
                    <span class="text-left text-md font-medium inline-block">POSITION<br>MANAGEMENT</span>
                </div>
            </button>
            <button wire:click="action({}, 'redirectToJobLevel')"
                class="p-1 w-50 text-gray-700 bg-white border border-blue border-solid rounded-md hover:bg-blue-100">
                <div class="items-center px-6 py-3">
                    <span class="text-left text-md font-medium inline-block">JOB LEVEL<br>MANAGEMENT</span>
                </div>
            </button>
            <button wire:click="action({}, 'redirectToDepartment')"
                class="p-1 w-50 text-gray-700 bg-white border border-blue border-solid rounded-md hover:bg-blue-100">
                <div class="items-center px-6 py-3">
                    <span class="text-left text-md font-medium inline-block">DEPARTMENT<br>MANAGEMENT</span>
                </div>
            </button>
            <button wire:click="action({}, 'redirectToDivision')"
                class="p-1 w-50 text-gray-700 bg-white border border-blue border-solid rounded-md hover:bg-blue-100">
                <div class="items-center px-6 py-3">
                    <span class="text-left text-md font-medium inline-block">DIVISION<br>MANAGEMENT</span>
                </div>
            </button>
        </div>
    </x-slot>
</x-form>
