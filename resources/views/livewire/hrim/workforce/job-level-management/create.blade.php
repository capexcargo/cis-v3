<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                {{-- <div>
                    <x-label for="code" value="Code" :required="true" />
                    <x-input type="text" name="code" wire:model.defer='code'></x-input>
                    <x-input-error for="code" />
                </div> --}}
                <div>
                    <x-label for="display" value="Job Level" :required="true" />
                    <x-input type="text" name="display" wire:model.defer='display'></x-input>
                    <x-input-error for="display" />
                </div>
            </div>
            <div class="flex justify-end space-x-3 gap-3 pt-5">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">Cancel</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                    Submit</button>
            </div>
        </div>
    </form>
</div>
