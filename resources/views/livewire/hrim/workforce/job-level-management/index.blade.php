<x-form x-data="{
    search_form: false,
    create_job_level_modal: '{{ $create_job_level_modal }}',
    edit_job_level_modal: '{{ $edit_job_level_modal }}',
    delete_job_level_modal: '{{ $delete_job_level_modal }}'
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_job_level_add')
            <x-modal id="create_job_level_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="title">Add New Job Level</x-slot>
                <x-slot name="body">
                    @livewire('hrim.workforce.job-level-management.create')
                </x-slot>
            </x-modal>
        @endcan

        @can('hrim_job_level_edit')
            @if ($job_level_id && $edit_job_level_modal)
                <x-modal id="edit_job_level_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                    <x-slot name="title">Edit Job Level</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.workforce.job-level-management.edit', ['id' => $job_level_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        <x-modal id="delete_job_level_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
            <x-slot name="body">
                <h2 class="mb-3 text-lg text-center text-gray-900">
                    {{ $confirmation_message }}
                </h2>
                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('delete_job_level_modal', false)"
                        class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click="deleteJobLevel"
                        class="flex-none px-5 py-1 mt-4 text-sm text-white rounded-lg bg-blue">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>
    </x-slot>

    <x-slot name="header_title">Organizational Structure</x-slot>
    <x-slot name="header_button">
        @can('hrim_job_level_add')
            <button wire:click="action({}, 'create_job_level')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Add New Job Level
                </div>
            </button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="flex gap-6">
            <button wire:click="action({}, 'redirectToPosition')"
                class="flex items-center justify-center w-40 py-4 text-gray-700 bg-white border border-solid rounded-md 4ext-gray-700 border-blue hover:bg-blue-100">
                <span class="font-medium text-justify text-md">POSITION<br>MANAGEMENT</span>
            </button>
            <button wire:click="action({}, 'redirectToJobLevel')"
                class="flex items-center justify-center w-40 py-4 text-gray-700 bg-white border-2 border-solid rounded-md border-blue hover:bg-blue-100">
                <span class="font-medium text-justify text-md">JOB LEVEL<br>MANAGEMENT</span>
            </button>
            <button wire:click="action({}, 'redirectToDepartment')"
                class="flex items-center justify-center w-40 py-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <span class="font-medium text-justify text-md">DEPARTMENT<br>MANAGEMENT</span>
            </button>
            <button wire:click="action({}, 'redirectToDivision')"
                class="flex items-center justify-center w-40 py-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <span class="font-medium text-justify text-md">DIVISION<br>MANAGEMENT</span>
            </button>
        </div>

        <div class="grid grid-cols-5 gap-4 text-sm md:w-3/4">
            <div class="w-40">
                <div>
                    <x-transparent.input value="" label="Job Level" name="job_level" wire:model="job_level">
                    </x-transparent.input>
                </div>
            </div>
        </div>
        <div class="grid grid-cols-3 gap-4">
            <div class="col-span-2">
                <div class="bg-white rounded-lg shadow-md">
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="No." />
                            <x-table.th name="Job Level" />
                            <x-table.th name="Created At" hidden />
                            <x-table.th name="Action" />
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($joblevels as $i => $joblevel)
                                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                    <td class="p-3 whitespace-nowrap md:w-24">
                                        {{ ($joblevels->currentPage() - 1) * $joblevels->links()->paginator->perPage() + $loop->iteration }}.
                                    </td>
                                    <td class="p-3 whitespace-nowrap md:w-3/4">
                                        {{ $joblevel->display }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap" hidden>
                                        {{ date('M. d, Y', strtotime($joblevel->created_at)) }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex space-x-3">
                                            @can('hrim_job_level_edit')
                                                <svg wire:click="action({'id': {{ $joblevel->id }}}, 'edit_job_level')"
                                                    class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="far" data-icon="edit" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                    </path>
                                                </svg>
                                            @endcan


                                            @can('hrim_job_level_delete')
                                                <svg wire:click="action({'id': {{ $joblevel->id }}}, 'delete_job_level')"
                                                    class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                    data-prefix="fas" data-icon="trash-alt" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                    <path fill="currentColor"
                                                        d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                                    </path>
                                                </svg>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </x-slot>
                    </x-table.table>
                    <div class="px-1 pb-2">
                        {{ $joblevels->links() }}
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
