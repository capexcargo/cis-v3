<x-form wire:init="load" x-data="{
    search_form: false,
    view_modal: '{{ $view_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @if ($loan_govt_id && $view_modal)
            <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/4">
                <x-slot name="title">View Status</x-slot>
                <x-slot name="body">
                    @livewire('hrim.reports.loan-government-deduction.view', ['id' => $loan_govt_id])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">Loans/Government Deductions</x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-5 gap-4 md:w-3/4 text-sm">
            <div class="w-40">
                <div>
                    <x-transparent.input type="text" label="Employee Name" name="employee_name"
                        wire:model.debounce.500ms="employee_name" />
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.input type="text" label="Employee ID" name="employee_id"
                        wire:model.debounce.500ms="employee_id" />
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.select value="{{ $division }}" label="Division" name="division"
                        wire:model="division">
                        <option value=""></option>
                        @foreach ($division_references as $division_reference)
                            <option value="{{ $division_reference->id }}">
                                {{ $division_reference->name }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.select value="{{ $department }}" label="Department" name="department"
                        wire:model="department">
                        <option value=""></option>
                        @foreach ($department_references as $department_reference)
                            <option value="{{ $department_reference->id }}">
                                {{ $department_reference->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
        </div>

        <div class="grid gap-4">
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="No." />
                        <x-table.th name="Employee Name" />
                        <x-table.th name="Employee ID" />
                        <x-table.th name="Division" />
                        <x-table.th name="Department" />
                        <x-table.th name="Loan/Deduction Count" />
                        <x-table.th name="Balance" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($loans_govt_deductions as $i => $loan_govt)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ ($loans_govt_deductions->currentPage() - 1) * $loans_govt_deductions->links()->paginator->perPage() + $loop->iteration }}.
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loan_govt->name }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loan_govt->userDetails->employee_number }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loan_govt->division->name }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loan_govt->userDetails->department->display }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loan_govt->approved_loans }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loan_govt->remaining }}/
                                    {{ $loan_govt->remaining + $loan_govt->paid }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        {{-- @can('hrim_loan_edit') --}}
                                        <svg wire:click="action({'id': {{ $loan_govt->id }}}, 'view_loan_status')"
                                            class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                            data-prefix="far" data-icon="edit" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"
                                            fill="currentColor">
                                            <path
                                                d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                        </svg>
                                        {{-- @endcan --}}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $loans_govt_deductions->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
