<div class="grid gap-4">
    <div class="bg-white rounded-lg shadow-md">
        <x-table.table>
            <x-slot name="thead">
                <x-table.th name="No." />
                <x-table.th name="Payment Date" />
                <x-table.th name="Loan Reference" />
                <x-table.th name="Loan Type" />
                <x-table.th name="Loan Deduction Particular" />
                <x-table.th name="Amount" />
                <x-table.th name="Status" />
            </x-slot>
            <x-slot name="tbody">
                @foreach ($loans_govt_deductions as $i => $loan_govt)
                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                        <td class="p-3 whitespace-nowrap">
                            {{ $i + 1 }}.
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ date('M. d, Y', strtotime($loan_govt->payment_date)) }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ $loan_govt->loan->reference_no }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ $loan_govt->loan->loanType->name }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ $loan_govt->loan->purpose }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ $loan_govt->amount }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            <span
                                class="{{ $loan_govt->status == 1 ? 'text-orange bg-orange-light px-4' : 'text-green bg-green-100 px-6' }} 
                                        text-xs rounded-full p-1">
                                {{ $loan_govt->status == 1 ? 'Unpaid' : 'Paid' }}
                            </span>
                        </td>
                    </tr>
                @endforeach
            </x-slot>
        </x-table.table>
        <div class="px-1 pb-2">
        </div>
    </div>
</div>
