<x-form wire:init="load" x-data="{
    search_form: false,
    edit_onboarding_modal: '{{ $edit_onboarding_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_onboarding_edit')
            @if ($onboarding_id && $edit_onboarding_modal)
                <x-modal id="edit_onboarding_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Edit Onboarding Status</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.recruitment-and-hiring.onboarding.edit', ['id' => $onboarding_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>
    <x-slot name="header_title">Onboarding</x-slot>
    <x-slot name="body">
        <div class="flex gap-3">
            @foreach ($header_cards as $index => $card)
                <button wire:click="$set('{{ $card['action'] }}', {{ $card['id'] }})"
                    class="flex flex-col px-3 py-5 text-gray-700 bg-white border-blue-800 border-solid rounded-md items-left justify-left "
                    :class="$wire.{{ $card['action'] }} ==
                        '{{ $card['id'] }}' ?
                        'border-2' : 'border'">
                    <div class="flex justify-between w-full items-left">
                        <span class="flex-1 -mt-1 font-medium text-md">{{ $card['title'] }}<span
                                class="ml-6 text-3xl font-bold">{{ $card['value'] }}</span></span>
                    </div>
                </button>
            @endforeach
        </div>

        <div class="grid grid-cols-5 gap-4 md:w-3/4">
            <div class="w-40">
                <div>
                    <div>
                        <input id="" wire:model.debounce.500ms="fullname" name="fullname" placeholder="Name"
                            class="w-full py-2 text-sm text-left bg-transparent border-0 border-b-2 border-gray-400 ring-0 focus:ring-0 dark:border-gray-500 focus:border-gray-500 focus:outline-none ">
                    </div>
                </div>
            </div>
        </div>
        <div class="grid grid-cols-10 gap-6">
            <div class="col-span-9">
                <div class="grid grid-cols-10 gap-6 -mb-6">
                    <div class="col-span-9">
                        <ul class="flex mt-2">
                            @foreach ($status_header_cards as $i => $status_card)
                                <li class="px-3 py-1 text-xs {{ $status_header_cards[$i]['class'] }}"
                                    :class="$wire.{{ $status_header_cards[$i]['action'] }} ==
                                        '{{ $status_header_cards[$i]['id'] }}' ?
                                        'bg-blue text-white' : 'text-gray-500'">
                                    <button
                                        wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                        {{ $status_header_cards[$i]['title'] }}
                                        <span class="ml-6 text-sm">{{ $status_header_cards[$i]['value'] }}</span>
                                    </button>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-white rounded-lg shadow-md md:w-3/4">
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="No." />
                    <x-table.th name="Name" />
                    <x-table.th name="Position" />
                    <x-table.th name="Division" />
                    <x-table.th name="Onboarding Status" />
                    <x-table.th name="Created At" hidden />
                    <x-table.th name="Action" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($onboardings as $onboarding)
                        <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                            <td class="p-3 whitespace-nowrap">
                                {{ $onboarding->id ?? null}}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $onboarding->firstname . ' ' . $onboarding->middlename . ' ' . $onboarding->lastname  ?? null}}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $onboarding->erfReference->position->display  ?? null}}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $onboarding->erfReference->division->name  ?? null}}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $onboarding->onboardingStatus->display  ?? null}}
                            </td>
                            <td class="p-3 whitespace-nowrap" hidden>
                                {{ date('M. d, Y', strtotime($onboarding->erfReference->created_at)) ?? null }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex space-x-3">
                                    @can('hrim_onboarding_edit')
                                        <svg wire:click="action({'id': {{ $onboarding->id }}}, 'edit_onboarding')"
                                            class="w-5 h-5 text-blue" aria-hidden="true" focusable="false" data-prefix="far"
                                            data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                            </path>
                                        </svg>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </x-slot>
            </x-table.table>
            <div class="px-1 pb-2">
                {{ $onboardings->links() }}
            </div>
        </div>
    </x-slot>
</x-form>
