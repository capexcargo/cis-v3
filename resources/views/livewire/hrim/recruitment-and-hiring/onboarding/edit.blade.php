<div>
    <x-loading></x-loading>
    <form autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div wire:init="onboardingStatusReference">
                    <x-label for="onboarding_status" value="Status of application" :required="true" />
                    <x-select name="onboarding_status" wire:model='onboarding_status'>
                        <option value="">Select</option>
                        @foreach ($onboarding_status_reference as $onboarding_status_ref)
                            <option value="{{ $onboarding_status_ref->id }}">
                                {{ $onboarding_status_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="onboarding_status" />
                </div>
                <div class="flex justify-end space-x-3 mt-5">
                    <button type="button" wire:click="$emit('close_modal', 'edit')"
                        class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <button type="button" wire:click="submit"
                        class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                        Save
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
