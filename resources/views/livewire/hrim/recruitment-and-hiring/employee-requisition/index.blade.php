<x-form wire:init="load" x-data="{
    search_form: false,
    confirmation_modal: '{{ $confirmation_modal }}',
    view_employee_requisition_modal: '{{ $view_employee_requisition_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
            <x-slot name="body">
                <h2 class="mb-3 text-lg text-center text-gray-900">
                    {{ $confirmation_message }}
                </h2>
                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click="confirm"
                        class="flex-none px-5 py-1 mt-4 text-sm text-white rounded-lg bg-blue">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>

        @if ($employee_requisition_id && $view_employee_requisition_modal)
            <x-modal id="view_employee_requisition_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                <x-slot name="body">
                    @livewire('hrim.employee-recruitment-and-hiring.employee-requisition.view', ['id' => $employee_requisition_id])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>

    <x-slot name="header_title">Employee Requisition</x-slot>

    <x-slot name="body">
        <div class="grid grid-cols-5 gap-4 text-sm md:w-3/4" wire:init="branchReference">
            <div class="w-40 text-sm">
                <x-transparent.select value="" label="Branch" name="branch" wire:model="branch">
                    <option value=""></option>
                    @foreach ($branch_reference as $branch_ref)
                        <option value="{{ $branch_ref->id }}">
                            {{ $branch_ref->display ?? '' }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div>
            <div class="w-40 text-sm" wire:init="divisionReference">
                <x-transparent.select value="{{ $division }}" label="Division" name="division"
                    wire:model="division">
                    <option value=""></option>
                    @foreach ($division_reference as $division_ref)
                        <option value="{{ $division_ref->id }}">
                            {{ $division_ref->name }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div>
            <div class="w-40 text-sm" wire:init="requestReasonReference">
                <x-transparent.select value="{{ $request_reason }}" label="Reason for Request" name="request_reason"
                    wire:model="request_reason">
                    <option value=""></option>
                    @foreach ($request_reason_reference as $request_reason_ref)
                        <option value="{{ $request_reason_ref->id }}">
                            {{ $request_reason_ref->display ?? '' }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div>
            {{-- <div class="w-40 text-sm" wire:init="positionReference">
                <x-transparent.select value="{{ $position }}" label="Position" name="position"
                    wire:model="position">
                    <option value=""></option>
                    @foreach ($position_reference as $position_ref)
                        <option value="{{ $position_ref->id }}">
                            {{ $position_ref->display ?? '' }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div> --}}

            <div class="w-40">
                <x-transparent.input type="name" label="Position" name="position_search"
                    wire:model="position_search" />
            </div>
        </div>
        @foreach ($header_cards as $index => $card)
            <button wire:click="$set('{{ $card['action'] }}', {{ $card['id'] }})"
                :class="$wire.{{ $card['action'] }} ==
                    '{{ $card['id'] }}' ?
                    'border-2 border-blue' : ''"
                class="flex-none w-40 px-3 py-3 bg-white border border-gray-200 rounded-lg shadow-md hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                <h5 class="mb-2 text-4xl font-semibold tracking-tight text-blue-900 dark:text-white">
                    {{ $card['value'] }}
                </h5>
                <p class="font-semibold text-gray-700 dark:text-gray-400">{{ $card['title'] }}</p>
            </button>
        @endforeach

        <div class="grid grid-cols-1 gap-4">
            <div class="col-span-2">
                <div class="grid grid-cols-10 gap-6">
                    <div class="col-span-9">
                        <ul class="flex mt-8">
                            @foreach ($status_header_cards as $i => $status_card)
                                <li class="px-3 py-1 text-xs {{ $status_header_cards[$i]['class'] }}"
                                    :class="$wire.{{ $status_header_cards[$i]['action'] }} ==
                                        '{{ $status_header_cards[$i]['id'] }}' ?
                                        'bg-blue text-white' : 'text-gray-500'">
                                    <button
                                        wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                        {{ $status_header_cards[$i]['title'] }}
                                        <span class="ml-6 text-sm">{{ $status_header_cards[$i]['value'] }}</span>
                                    </button>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="hidden col-span-1">
                        <div class="right-0 h-16 w-15">
                            <button
                                class="px-6 py-2 mt-6 text-xs text-black bg-gray-200 border border-black rounded-sm shadow-sm hover:bg-blue-100">
                                <div class="flex items-start justify-between">
                                    <svg wire:click="action({'id': ''}, '')" class="w-4 h-4 mr-2 text-gray-500"
                                        aria-hidden="true" focusable="false" data-prefix="far" data-icon="print-alt"
                                        role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M448 192H64C28.65 192 0 220.7 0 256v96c0 17.67 14.33 32 32 32h32v96c0 17.67 14.33 32 32 32h320c17.67 0 32-14.33 32-32v-96h32c17.67 0 32-14.33 32-32V256C512 220.7 483.3 192 448 192zM384 448H128v-96h256V448zM432 296c-13.25 0-24-10.75-24-24c0-13.27 10.75-24 24-24s24 10.73 24 24C456 285.3 445.3 296 432 296zM128 64h229.5L384 90.51V160h64V77.25c0-8.484-3.375-16.62-9.375-22.62l-45.25-45.25C387.4 3.375 379.2 0 370.8 0H96C78.34 0 64 14.33 64 32v128h64V64z" />
                                    </svg>
                                    Print
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="bg-white rounded-lg shadow-md">
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="No." />
                            <x-table.th name="ERF Reference" />
                            <x-table.th name="Date Requested" />
                            <x-table.th name="Branch" />
                            <x-table.th name="Division" />
                            <x-table.th name="Job Title" />
                            <x-table.th name="Job Level" />
                            <x-table.th name="Employment Status" />
                            <x-table.th name="Project Name" />
                            <x-table.th name="Duration" />
                            <x-table.th name="Reason for Request" />
                            <x-table.th name="Target Hire Date" />
                            <x-table.th name="Requested By" />
                            <x-table.th name="Approver 1" />
                            <x-table.th name="Approver 2" />
                            <x-table.th name="Status" />
                            <x-table.th name="Attachment" />
                            <x-table.th name="Action" />
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($employee_requisitions as $i => $employee_requisition)
                                @if ($employee_requisition->id)
                                    <tr class="border-0 cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                        <td class="p-3 whitespace-nowrap">{{ $i + 1 }}.
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->erf_reference_no }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ date('M. d, Y', strtotime($employee_requisition->created_at)) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->branch->display ?? '' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->division->description ?? '' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->position->display ?? '' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->job_level->display ?? '' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->employment_category->display ?? '' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->project_name == null ? '-' : $employee_requisition->project_name }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->employment_category->id == 2 ? $employee_requisition->employment_category_type->display : '-' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->requisition_reason->display ?? '' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">{{ $employee_requisition->target_hire }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">{{ $employee_requisition->user->name }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->firstApprover->name ?? '' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->secondApprover->name ?? '' }}
                                        </td>

                                        <td class="p-3 whitespace-nowrap">
                                            <span
                                                class="{{ $employee_requisition->final_status == 1
                                                    ? 'text-orange bg-orange-light px-4'
                                                    : ($employee_requisition->final_status == 2
                                                        ? 'text-orange bg-red-100 px-6'
                                                        : ($employee_requisition->final_status == 3
                                                            ? 'text-blue bg-blue-100 px-5'
                                                            : ($employee_requisition->final_status == 4
                                                                ? 'text-red bg-red-100 px-6'
                                                                : ''))) }} 
                                                text-xs rounded-full p-1">
                                                {{ $employee_requisition->status->display ?? '' }}</span>
                                        </td>

                                        <td class="p-3 whitespace-nowrap">
                                            <svg wire:click="action({'id': {{ $employee_requisition->id }}}, 'view_employee_requisition')"
                                                class="w-4 h-4 ml-8 cursor-pointer text-blue hover:text-blue-700"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M256 0v128h128L256 0zM224 128L224 0H48C21.49 0 0 21.49 0 48v416C0 490.5 21.49 512 48 512h288c26.51 0 48-21.49 48-48V160h-127.1C238.3 160 224 145.7 224 128zM272 416h-160C103.2 416 96 408.8 96 400C96 391.2 103.2 384 112 384h160c8.836 0 16 7.162 16 16C288 408.8 280.8 416 272 416zM272 352h-160C103.2 352 96 344.8 96 336C96 327.2 103.2 320 112 320h160c8.836 0 16 7.162 16 16C288 344.8 280.8 352 272 352zM288 272C288 280.8 280.8 288 272 288h-160C103.2 288 96 280.8 96 272C96 263.2 103.2 256 112 256h160C280.8 256 288 263.2 288 272z" />
                                            </svg>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <div class="flex gap-3">
                                                @if (
                                                    ($employee_requisition->final_status == 1 && $employee_requisition->first_approver == Auth::user()->id) ||
                                                        ($employee_requisition->final_status == 2 && $employee_requisition->second_approver == Auth::user()->id))
                                                    <svg wire:click="action({'id': {{ $employee_requisition->id }}}, 'approve_employee_requisition')"
                                                        class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700"
                                                        data-prefix="far" data-icon="approve" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM371.8 211.8C382.7 200.9 382.7 183.1 371.8 172.2C360.9 161.3 343.1 161.3 332.2 172.2L224 280.4L179.8 236.2C168.9 225.3 151.1 225.3 140.2 236.2C129.3 247.1 129.3 264.9 140.2 275.8L204.2 339.8C215.1 350.7 232.9 350.7 243.8 339.8L371.8 211.8z" />
                                                    </svg>
                                                    <svg wire:click="action({'id': {{ $employee_requisition->id }}}, 'decline_employee_requisition')"
                                                        class="w-4 h-4 cursor-pointer text-red hover:text-red-700"
                                                        data-prefix="far" data-icon="cancel-alt" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
                                                    </svg>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </x-slot>
                    </x-table.table>
                </div>
            </div>
        </div>

    </x-slot>
</x-form>
