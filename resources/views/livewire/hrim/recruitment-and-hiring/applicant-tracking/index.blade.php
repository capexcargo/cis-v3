<x-form wire:init="load" x-data="{
    search_form: false,
    confirmation_modal: '{{ $confirmation_modal }}',
    create_applicant_tracking_modal: '{{ $create_applicant_tracking_modal }}',
    edit_applicant_tracking_modal: '{{ $edit_applicant_tracking_modal }}',
    view_applicant_tracking_modal: '{{ $view_applicant_tracking_modal }}',
    view_applicant_tracking_notes_modal: '{{ $view_applicant_tracking_notes_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_applicant_tracking_add')
            <x-modal id="create_applicant_tracking_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Add a new applicant</x-slot>
                <x-slot name="body">
                    @livewire('hrim.recruitment-and-hiring.applicant-tracking.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_applicant_tracking_edit')
            @if ($applicant_tracking_id && $edit_applicant_tracking_modal)
                <x-modal id="edit_applicant_tracking_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="body">
                        @livewire('hrim.recruitment-and-hiring.applicant-tracking.edit', ['id' => $applicant_tracking_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan

        @can('hrim_applicant_tracking_view')
            @if ($applicant_tracking_id && $view_applicant_tracking_modal)
                <x-modal id="view_applicant_tracking_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                    <x-slot name="body">
                        @livewire('hrim.recruitment-and-hiring.applicant-tracking.view', ['id' => $applicant_tracking_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan

        @if ($applicant_tracking_id && $view_applicant_tracking_notes_modal)
            <x-modal id="view_applicant_tracking_notes_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="body">
                    <div class="mt-4 mb-2">
                        <div class="grid gap-2 mb-4">
                            <h5 class="mb-2 font-semibold tracking-tight text-blue-900 text-md dark:text-white">
                                Notes
                            </h5>
                            <div class="flex flex-col space-y-3 text-xs text-left">
                                <table class="divide-y divide-gray-300 text-md ">
                                    <thead class="bg-gray-100 border-2">
                                        <th class="px-4 py-2 text-left border-0">Position</th>
                                        <th class="px-4 py-2 text-left border-2 border-b-0">Notes</th>
                                    </thead>
                                    <tbody class="bg-white" wire:init="loadApplicantDetails">
                                        @foreach ($applicant_details as $i => $applicant_det)
                                            @if ($applicant_det->id == $applicant_tracking_id)
                                                <tr class="cursor-pointer text-md even:bg-white ">
                                                    <td class="p-4 text-left border-2 border-b-0">
                                                        HR
                                                    </td>
                                                    <td
                                                        class="p-4 font-semibold text-left text-black border-2 border-b-0">
                                                        {{ $applicant_det->hr_notes }}
                                                    </td>
                                                </tr>
                                                <tr class="cursor-pointer text-md even:bg-white ">
                                                    <td class="p-4 text-left border-2 border-b-0">
                                                        Hiring Manager
                                                    </td>
                                                    <td
                                                        class="p-4 font-semibold text-left text-black border-2 border-b-0">
                                                        {{ $applicant_det->hiring_manager_notes }}
                                                    </td>
                                                </tr>
                                                <tr class="cursor-pointer text-md even:bg-white ">
                                                    <td class="p-6 text-left border-2 border-b-0">
                                                        GM / SEG
                                                    </td>
                                                    <td
                                                        class="p-6 font-semibold text-left text-black border-2 border-b-0">
                                                        {{ $applicant_det->gm_notes }}
                                                    </td>
                                                </tr>
                                                <tr class="cursor-pointer text-md even:bg-white ">
                                                    <td class="p-6 text-left border-2">
                                                        Final Remarks
                                                    </td>
                                                    <td class="p-6 font-semibold text-left text-black border-2">
                                                        {{ $applicant_det->remarks }}
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="grid gap-2 mt-6">
                            <h5 class="mb-2 font-semibold tracking-tight text-blue-900 text-md dark:text-white">
                                Application History
                            </h5>
                            <div class="flex flex-col space-y-3 text-xs text-left">
                                <table class="divide-y divide-gray-300 text-md ">
                                    <thead class="bg-gray-100 border-2">
                                        <th class="px-4 py-2 text-left border-0">Status</th>
                                        <th class="px-4 py-2 text-left border-2 border-b-0">Date</th>
                                        <th class="px-4 py-2 text-left border-2 border-b-0">HR Remarks</th>
                                    </thead>
                                    <tbody class="bg-white" wire:init="loadApplicantDetails">
                                        {{-- @foreach ($applicant_details as $i => $applicant_det)
                                            @if ($applicant_det->id == $applicant_tracking_id) --}}
                                        <tr class="cursor-pointer text-md even:bg-white">
                                            <td class="p-4 text-left border-2 border-b-0 md:w-1/3">
                                                For HR Interview
                                            </td>
                                            <td class="p-4 font-semibold text-left text-black border-2 border-b-0">
                                                {{-- {{ $applicant_det->hr_notes }} --}}
                                            </td>
                                            <td class="p-4 font-semibold text-left text-black border-2 border-b-0">
                                                {{-- {{ $applicant_det->hr_notes }} --}}
                                            </td>
                                        </tr>
                                        <tr class="cursor-pointer text-md even:bg-white ">
                                            <td class="p-4 text-left border-2 md:w-1/3">
                                                For Assessment
                                            </td>
                                            <td class="p-4 font-semibold text-left text-black border-2">
                                                {{-- {{ $applicant_det->hiring_manager_notes }} --}}
                                            </td>
                                            <td class="p-4 font-semibold text-left text-black border-2">
                                                {{-- {{ $applicant_det->hr_notes }} --}}
                                            </td>
                                        </tr>
                                        <tr class="cursor-pointer text-md even:bg-white ">
                                            <td class="p-4 text-left border-2 md:w-1/3">
                                                For HR Manager Interview
                                            </td>
                                            <td class="p-4 font-semibold text-left text-black border-2">
                                                {{-- {{ $applicant_det->gm_notes }} --}}
                                            </td>
                                            <td class="p-4 font-semibold text-left text-black border-2">
                                                {{-- {{ $applicant_det->gm_notes }} --}}
                                            </td>
                                        </tr>
                                        {{-- @endif
                                        @endforeach --}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">Applicant Tracking</x-slot>
    <x-slot name="header_button">
        @can('hrim_applicant_tracking_add')
            <button wire:click="action({}, 'create_applicant_tracking')"
                class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg hidden class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Add a New Applicant
                </div>
            </button>
        @endcan

    </x-slot>

    <x-slot name="body">
        <div class="flex gap-4 text-sm md:w-3/4">
            <div class="w-40">
                <div>
                    <x-transparent.input type="text" label="Applicant Name" name="fullname"
                        wire:model.debounce.500ms="fullname" />
                </div>
            </div>
            <div class="w-40">
                <div wire:init="positionReference">
                    <x-transparent.select value="{{ $position }}" label="Position applied for" name="position"
                        wire:model="position">
                        <option value=""></option>
                        @foreach ($position_reference as $position_reference)
                            <option value="{{ $position_reference->id }}">
                                {{ $position_reference->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div wire:init="branchReference">
                    <x-transparent.select value="{{ $branch }}" label="Branch" name="branch"
                        wire:model="branch">
                        <option value=""></option>
                        @foreach ($branch_reference as $branch_ref)
                            <option value="{{ $branch_ref->id }}">
                                {{ $branch_ref->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                {{-- <div>
                    <x-transparent.input type="date" label="Date Applied" name="date_applied"
                        wire:model.debounce.500ms="date_applied" />
                </div> --}}

                <div>
                    <x-transparent.input style="font-size: 14px;color: #8d8d8d !important;" type="text"
                        onfocus="(this.type='date')" onblur="(this.type='text')" label=""
                        placeholder="&#128197; Date Applied" name="date_applied"
                        wire:model.defer="date_applied" />
                </div>
            </div>
            <div class="w-40">
                <div wire:init="applicantStatusReference">
                    <x-transparent.select value="" label="Status" name="status_of_application"
                        wire:model="status_of_application">
                        <option value=""></option>
                        @foreach ($applicant_status_reference as $applicant_status_ref)
                            <option value="{{ $applicant_status_ref->id }}">
                                {{ $applicant_status_ref->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
        </div>

        @foreach ($header_cards as $index => $card)
            <button wire:click="$set('{{ $card['action'] }}', {{ $card['id'] }})"
                :class="$wire.{{ $card['action'] }} ==
                    '{{ $card['id'] }}' ?
                    'border-2 border-blue' : ''"
                class="flex-none w-40 px-3 py-3 bg-white border-2 border-gray-200 rounded-lg shadow-md hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                <h5 class="mb-2 text-4xl font-semibold tracking-tight text-blue-900 dark:text-white">
                    {{ $card['value'] }}
                </h5>
                <p class="font-semibold text-gray-700 dark:text-gray-400">{{ $card['title'] }}</p>
            </button>
        @endforeach

        <div class="grid grid-cols-1 gap-4">
            <div class="col-span-2">
                <div class="bg-white rounded-lg shadow-md">
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="No." />
                            <x-table.th name="ERF Reference" />
                            <x-table.th name="Applicant Name" />
                            <x-table.th name="Position applied for" />
                            <x-table.th name="Branch" />
                            <x-table.th name="Date applied" />
                            <x-table.th name="Requesting Manager" />
                            <x-table.th name="Requesting Division" />
                            <x-table.th name="Status" />
                            <x-table.th name="Attachment" />
                            <x-table.th name="Action" />
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($applicant_trackings as $i => $applicant_tracking)
                                <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                    <td class="p-3 whitespace-nowrap">{{ $i + 1 }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $applicant_tracking->erfReference->erf_reference_no }}
                                    </td>
                                    <td class="p-3 capitalize whitespace-nowrap">
                                        {{ $applicant_tracking->firstname . ' ' . $applicant_tracking->middlename . ' ' . $applicant_tracking->lastname }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $applicant_tracking->erfReference->position->display }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $applicant_tracking->erfReference->branch->display }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ date('M. d, Y', strtotime($applicant_tracking->erfReference->created_at)) }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $applicant_tracking->erfReference->user->name }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $applicant_tracking->erfReference->division->name }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $applicant_tracking->applicantStatus->display }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <svg wire:click="action({'id': {{ $applicant_tracking->id }}}, 'view_applicant_tracking')"
                                            class="w-4 h-4 ml-8 cursor-pointer text-blue hover:text-blue-700"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M256 0v128h128L256 0zM224 128L224 0H48C21.49 0 0 21.49 0 48v416C0 490.5 21.49 512 48 512h288c26.51 0 48-21.49 48-48V160h-127.1C238.3 160 224 145.7 224 128zM272 416h-160C103.2 416 96 408.8 96 400C96 391.2 103.2 384 112 384h160c8.836 0 16 7.162 16 16C288 408.8 280.8 416 272 416zM272 352h-160C103.2 352 96 344.8 96 336C96 327.2 103.2 320 112 320h160c8.836 0 16 7.162 16 16C288 344.8 280.8 352 272 352zM288 272C288 280.8 280.8 288 272 288h-160C103.2 288 96 280.8 96 272C96 263.2 103.2 256 112 256h160C280.8 256 288 263.2 288 272z" />
                                        </svg>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex gap-3">
                                            <svg wire:click="action({'id': {{ $applicant_tracking->id }}}, 'view_applicant_tracking_notes')"
                                                class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700"
                                                data-prefix="far" data-icon="approve" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                            </svg>
                                            @can('hrim_applicant_tracking_edit')
                                                <svg wire:click="action({'id': {{ $applicant_tracking->id }}}, 'edit_applicant_tracking')"
                                                    class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700"
                                                    data-prefix="far" data-icon="cancel-alt" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                </svg>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </x-slot>
                    </x-table.table>
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
