<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}'
}" wire:init="load">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                Confirm Applicant details?

                <table class="mt-6 table-auto">
                    <tbody class="text-sm font-light text-gray-900">
                        <tr>
                            <td class="whitespace-nowrap"><span class="mr-2 text-xs font-semibold text-gray-400">ERF
                                    Reference : </span></td>
                            <td class="mr-2"><span class="mr-2 font-semibold">{{ $erf_reference_search }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span
                                    class="mr-2 text-xs font-semibold text-gray-400">Applicant Name : </span></td>
                            <td class="mr-2"><span
                                    class="mr-2 font-semibold">{{ $first_name . ' ' . $middle_name . ' ' . $last_name }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span
                                    class="mr-2 text-xs font-semibold text-gray-400">Position : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @foreach ($position_reference as $position_ref)
                                        {{ $position_ref->id == $position ? $position_ref->display : '' }}
                                    @endforeach
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span class="mr-2 text-xs font-semibold text-gray-400">Branch
                                    : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @foreach ($branch_reference as $branch_ref)
                                        {{ $branch_ref->id == $branch ? $branch_ref->display : '' }}
                                    @endforeach
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span class="mr-2 text-xs font-semibold text-gray-400">Date
                                    Applied : </span></td>
                            <td class="mr-2"><span class="mr-2 font-semibold">{{ $date_applied }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span
                                    class="mr-2 text-xs font-semibold text-gray-400">Requesting Manager : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @foreach ($requesting_manager_reference as $requesting_manager_ref)
                                        {{ $requesting_manager_ref->id == $requesting_manager ? $requesting_manager_ref->name : '' }}
                                    @endforeach
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span
                                    class="mr-2 text-xs font-semibold text-gray-400">Requesting Division : </span>
                            </td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @foreach ($division_reference as $division_ref)
                                        {{ $division_ref->id == $requesting_division ? $division_ref->name : '' }}
                                    @endforeach
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span class="mr-2 text-xs font-semibold text-gray-400">HR
                                    Notes : </span></td>
                            <td class="mr-2"><span class="mr-2 font-semibold">{{ $hr_notes }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span class="mr-2 text-xs font-semibold text-gray-400">Hiring
                                    Manager Notes : </span>
                            </td>
                            <td class="mr-2"><span class="mr-2 font-semibold">{{ $hiring_manager_notes }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span
                                    class="mr-2 text-xs font-semibold text-gray-400">GM/SEG's Notes : </span></td>
                            <td class="mr-2"><span class="mr-2 font-semibold">{{ $gm_segs_notes }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span class="mr-2 text-xs font-semibold text-gray-400">Status
                                    of application : </span>
                            </td>
                            <td class="mr-2"><span class="mr-2 font-semibold">
                                    @foreach ($applicant_status_reference as $applicant_status_ref)
                                        {{ $applicant_status_ref->id == $status_of_application ? $applicant_status_ref->display : '' }}
                                    @endforeach
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span class="mr-2 text-xs font-semibold text-gray-400">Final
                                    Remarks : </span></td>
                            <td class="mr-2"><span class="mr-2 font-semibold">{{ $final_remarks }}</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </h2>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="confirm"
                    class="px-8 py-1 mt-4 text-sm flex-none bg-[#003399] text-white rounded-md">
                    Confirm</button>
            </div>
        </x-slot>
    </x-modal>
    <form autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                {{-- <div>
                    <x-label for="erf_reference" value="ERF Reference" />
                    <x-input type="text" name="erf_reference" wire:model.defer='erf_reference'></x-input>
                    <x-input-error for="erf_reference" />
                </div> --}}
                <div>
                    <div x-data="{ open: false }" class="relative mb-2 rounded-md" @click.away="open = false">
                        <div>
                            <x-label for="erf_reference_search" value="ERF Reference" :required="true" />
                            <span class="relative block">
                                <span class="absolute inset-y-0 left-0 flex items-center ml-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-4 text-blue" aria-hidden="true"
                                        focusable="false" data-prefix="far" data-icon="edit" role="img"
                                        viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M500.3 443.7l-119.7-119.7c27.22-40.41 40.65-90.9 33.46-144.7C401.8 87.79 326.8 13.32 235.2 1.723C99.01-15.51-15.51 99.01 1.724 235.2c11.6 91.64 86.08 166.7 177.6 178.9c53.8 7.189 104.3-6.236 144.7-33.46l119.7 119.7c15.62 15.62 40.95 15.62 56.57 0C515.9 484.7 515.9 459.3 500.3 443.7zM79.1 208c0-70.58 57.42-128 128-128s128 57.42 128 128c0 70.58-57.42 128-128 128S79.1 278.6 79.1 208z" />
                                    </svg>
                                </span>
                                <input
                                    class="block w-full py-2 pr-3 bg-white border rounded-md shadow-sm placeholder:placeholder:text-slate-400 px-9 border-slate-300 focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1"
                                    type="text" @click="open = !open" wire:model='erf_reference_search'
                                    name="erf_reference" />
                            </span>

                            <x-input-error for="erf_reference_search" />
                            <x-input-error for="erf_reference" />
                        </div>
                        <div x-show="open" x-cloak
                            class="absolute w-full p-2 my-1 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96">
                            <ul class="list-reset">
                                @forelse ($erf_references as $i => $erf_ref)
                                    <li @click="open = !open" wire:click="getErfDetails({{ $erf_ref->id }})"
                                        wire:key="{{ 'erf_reference' . $i }}"
                                        class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                        <p>
                                            {{ $erf_ref->erf_reference_no }}
                                        </p>
                                    </li>
                                @empty
                                    <li>
                                        <p class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                            No ERF Found.
                                        </p>
                                    </li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
                {{-- <div>
                    <x-label for="applicant_name" value="Applicant Name" :required="true" />
                    <x-input type="text" name="applicant_name" wire:model.defer='applicant_name'></x-input>
                    <x-input-error for="applicant_name" />
                </div> --}}
                {{-- <x-label for="" value="Applicant Name" />
                <hr> --}}
                <div class="grid grid-cols-3 gap-4 -mt-2">
                    <div>
                        <x-label for="first_name" value="First Name" :required="true" />
                        <x-input type="text" name="first_name" wire:model.defer='first_name'></x-input>
                        <x-input-error for="first_name" />
                    </div>
                    <div>
                        <x-label for="middle_name" value="Middle Name" />
                        <x-input type="text" name="middle_name" wire:model.defer='middle_name'></x-input>
                        <x-input-error for="middle_name" />
                    </div>
                    <div>
                        <x-label for="last_name" value="Last Name" :required="true" />
                        <x-input type="text" name="last_name" wire:model.defer='last_name'></x-input>
                        <x-input-error for="last_name" />
                    </div>
                </div>
                <div wire:init="positionReference">
                    <x-label for="position" value="Position" :required="true" />
                    <x-select name="position" wire:model='position'>
                        <option value="">Select</option>
                        @foreach ($position_reference as $position_ref)
                            <option value="{{ $position_ref->id }}">
                                {{ $position_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="position" />
                </div>
                <div class="grid grid-cols-2 gap-4">
                    <div wire:init="branchReference">
                        <x-label for="branch" value="Branch" :required="true" />
                        <x-select name="branch" wire:model='branch'>
                            <option value="">Select</option>
                            @foreach ($branch_reference as $branch_ref)
                                <option value="{{ $branch_ref->id }}">
                                    {{ $branch_ref->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="branch" />
                    </div>
                    <div>
                        <x-label for="date_applied" value="Date applied" :required="true" />
                        <x-input type="date" name="date_applied" wire:model='date_applied'>
                        </x-input>
                        <x-input-error for="date_applied" />
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-4">
                    <div wire:init="requestingManagerReference">
                        <x-label for="requesting_manager" value="Requesting Manager" :required="true" />
                        <x-select name="requesting_manager" wire:model='requesting_manager'>
                            <option value="">Select</option>
                            @foreach ($requesting_manager_reference as $requesting_manager_ref)
                                <option value="{{ $requesting_manager_ref->id }}">
                                    {{ $requesting_manager_ref->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="requesting_manager" />
                    </div>
                    <div wire:init="divisionReference">
                        <x-label for="requesting_division" value="Requesting Division" :required="true" />
                        <x-select name="requesting_division" wire:model='requesting_division'>
                            <option value="">Select</option>
                            @foreach ($division_reference as $division_ref)
                                <option value="{{ $division_ref->id }}">
                                    {{ $division_ref->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="requesting_division" />
                    </div>
                </div>
                <div>
                    <x-label for="hr_notes" value="HR Notes" />
                    <x-textarea type="text" name="hr_notes" wire:model.defer='hr_notes'></x-textarea>
                    <x-input-error for="hr_notes" />
                </div>
                <div>
                    <x-label for="hiring_manager_notes" value="Hiring Manager Notes" />
                    <x-textarea type="text" name="hiring_manager_notes" wire:model.defer='hiring_manager_notes'>
                    </x-textarea>
                    <x-input-error for="hiring_manager_notes" />
                </div>
                <div>
                    <x-label for="gm_segs_notes" value="GM/SEG's Notes" />
                    <x-textarea type="text" name="gm_segs_notes" wire:model.defer='gm_segs_notes'></x-textarea>
                    <x-input-error for="gm_segs_notes" />
                </div>
                <div wire:init="applicantStatusReference">
                    <x-label for="status_of_application" value="Status of application" :required="true" />
                    <x-select name="status_of_application" wire:model='status_of_application'>
                        <option value="">Select</option>
                        @foreach ($applicant_status_reference as $applicant_status_ref)
                            <option value="{{ $applicant_status_ref->id }}">
                                {{ $applicant_status_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="status_of_application" />
                </div>
                <div>
                    <x-label for="final_remarks" value="Final Remarks" />
                    <x-textarea type="text" name="final_remarks" wire:model.defer='final_remarks'></x-textarea>
                    <x-input-error for="final_remarks" />
                </div>

                <div>
                    <div class="text-2xl font-bold text-blue">
                        <div class="flex items-center space-x-3">
                            <x-label for="attachment" value="Attachment" :required="true" />
                        </div>
                    </div>
                    <div class="grid gap-2">
                        <div class="flex flex-col overflow-hidden">
                            <table class="w-full">
                                <thead>
                                </thead>
                                <tbody>
                                    @forelse ($attachments as $i => $attachment)
                                        <tr class="text-xs border-0 cursor-pointer even:bg-white">
                                            <td class="flex p-1 items-left justify-left whitespace-nowrap">
                                                <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap ">
                                                    <div class="relative z-0">
                                                        <input type="file"
                                                            name="attachments.{{ $i }}.attachment"
                                                            wire:model="attachments.{{ $i }}.attachment"
                                                            class="absolute top-0 left-0 z-50 opacity-0">

                                                        @if ($attachments[$i]['attachment'])
                                                            <label for="attachments.{{ $i }}.attachment"
                                                                class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 rounded-lg cursor-pointer">
                                                                <span class="flex gap-1">
                                                                    <svg class="w-3 h-3" aria-hidden="true"
                                                                        focusable="false" data-prefix="fas"
                                                                        data-icon="file-alt" role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                        </path>
                                                                    </svg>{{ $attachments[$i]['attachment']->getClientOriginalName() }}
                                                                </span>
                                                            </label>
                                                        @else
                                                            <label for="attachments.{{ $i }}.attachment"
                                                                class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 rounded-lg cursor-pointer">
                                                                <span class="flex gap-1">
                                                                    <svg class="w-3 h-3" aria-hidden="true"
                                                                        focusable="false" data-prefix="fas"
                                                                        data-icon="file-alt" role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                        </path>
                                                                    </svg>Choose Attachment
                                                                </span>
                                                            </label>
                                                        @endif

                                                        <x-input-error
                                                            for="attachments.{{ $i }}.attachment" />
                                                    </div>

                                                </div>
                                            </td>
                                            <td class="p-2 whitespace-nowrap">
                                                @if (count($attachments) > 1)
                                                    <a wire:click="removeAttachments({{ $i }})">
                                                        <p class="text-red-600 underline underline-offset-2">Remove</p>
                                                    </a>
                                                    <svg hidden wire:click="removeAttachments({{ $i }})"
                                                        class="w-5 text-red" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="times-circle" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                            d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                        </path>
                                                    </svg>
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                    @endforelse
                                </tbody>
                            </table>
                            <div class="flex items-center justify-start">
                                <button type="button" title="Add Attachment" wire:click="addAttachments"
                                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                    +</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    Cancel
                </button>
                <button type="button" wire:click="action({}, 'submit')"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Submit
                </button>
            </div>
        </div>
</div>
</form>
</div>
