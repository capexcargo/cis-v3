<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
<div class="swiper mySwiper">
    @foreach ($requests as $i => $request)
        <div class="swiper-wrapper">
            @foreach ($request->attachments as $i => $attachment)
                {{-- <div class="swiper-slide">
                    <img class="min-h-screen object-fit min-w-screen"
                        src="{{ Storage::disk('hrim_gcs')->url($attachment->path . $attachment->name) }}"
                        alt="image" />
                </div> --}}
                <div class="swiper-slide">
                    @if (in_array($attachment->extension, config('filesystems.image_type')))
                        <img class="min-h-screen object-fit min-w-screen"
                            src="{{ Storage::disk('hrim_gcs')->url($attachment->path . $attachment->name) }}">
                    @endif
                    @if (in_array($attachment->extension, config('filesystems.file_type')))
                        <iframe src="{{ Storage::disk('hrim_gcs')->url($attachment->path . $attachment->name) }}"
                            type="application/pdf" class="min-h-screen object-fit min-w-screen" width="100%"
                            height="600px">
                        </iframe>
                    @endif
                </div>
            @endforeach
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-pagination"></div>
    @endforeach

</div>

<!-- Swiper JS -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script>
    var swiper = new Swiper(".mySwiper", {
        cssMode: true,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el: ".swiper-pagination",
        },
        mousewheel: true,
        keyboard: true,
    });
</script>
