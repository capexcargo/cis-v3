<x-form wire:init="load" x-data="{
    search_form: false,
    confirmation_modal: '{{ $confirmation_modal }}',
    create_employee_requisition_modal: '{{ $create_employee_requisition_modal }}',
    edit_employee_requisition_modal: '{{ $edit_employee_requisition_modal }}',
    view_employee_requisition_modal: '{{ $view_employee_requisition_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">

        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
            <x-slot name="body">
                <h2 class="mb-3 text-lg text-center text-gray-900">
                    {{ $confirmation_message }}
                </h2>
                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click="cancelRequest"
                        class="flex-none px-5 py-1 mt-4 text-sm text-white rounded-lg bg-blue">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>

        @can('hrim_employee_requisition_add')
            <x-modal id="create_employee_requisition_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">New Employee Requisition</x-slot>
                <x-slot name="body">
                    @livewire('hrim.employee-recruitment-and-hiring.employee-requisition.create')
                </x-slot>
            </x-modal>
        @endcan

        @can('hrim_employee_requisition_edit')
            @if ($employee_requisition_id && $edit_employee_requisition_modal)
                <x-modal id="edit_employee_requisition_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Edit Employee Requisition</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.employee-recruitment-and-hiring.employee-requisition.edit', ['id' => $employee_requisition_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan

        @if ($employee_requisition_id && $view_employee_requisition_modal)
            <x-modal id="view_employee_requisition_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                <x-slot name="body">
                    @livewire('hrim.employee-recruitment-and-hiring.employee-requisition.view', ['id' => $employee_requisition_id])
                </x-slot>
            </x-modal>
        @endif

    </x-slot>

    <x-slot name="header_title">Employee Requisition</x-slot>
    <x-slot name="header_button">
        @can('hrim_employee_requisition_add')
            <button wire:click="action({}, 'create_employee_requisition')"
                class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg hidden class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    New Employee Requisition
                </div>
            </button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-5 gap-4 md:w-3/4">
            <div class="w-40">
                <x-transparent.input type="date" label="Date Requested" name="date_created"
                    wire:model.debounce.500ms="date_created" />
            </div>
            {{-- <div class="w-40" wire:init="positionReference">
                <x-transparent.select value="{{ $position }}" label="Position" name="position"
                    wire:model="position">
                    <option value=""></option>
                    @foreach ($position_reference as $position_ref)
                        <option value="{{ $position_ref->id }}">
                            {{ $position_ref->display ?? '' }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div> --}}
            <div class="w-40">
                <x-transparent.input type="name" label="Position" name="position_search"
                    wire:model="position_search" />
            </div>
        </div>

        <div class="grid grid-cols-1 gap-4 mt-4">
            <div class="col-span-2">
                <div class="grid grid-cols-10 gap-6">
                    <div class="col-span-9">
                        <ul class="flex mt-8">
                            @foreach ($status_header_cards as $i => $status_card)
                                <li class="px-3 py-1 text-xs {{ $status_header_cards[$i]['class'] }} "
                                    :class="$wire.{{ $status_header_cards[$i]['action'] }} ==
                                        '{{ $status_header_cards[$i]['id'] }}' ?
                                        'bg-blue text-white' : 'text-gray-500'">
                                    <button
                                        wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                        {{ $status_header_cards[$i]['title'] }}
                                        <span class="ml-6 text-sm">{{ $status_header_cards[$i]['value'] }}</span>
                                    </button>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="hidden col-span-1">
                        <div class="right-0 h-16 w-15">
                            <button
                                class="px-6 py-2 mt-6 text-xs text-black bg-gray-200 border border-black rounded-sm shadow-sm hover:bg-blue-100">
                                <div class="flex items-start justify-between">
                                    <svg wire:click="action({'id': ''}, '')" class="w-4 h-4 mr-2 text-gray-500"
                                        aria-hidden="true" focusable="false" data-prefix="far" data-icon="print-alt"
                                        role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M448 192H64C28.65 192 0 220.7 0 256v96c0 17.67 14.33 32 32 32h32v96c0 17.67 14.33 32 32 32h320c17.67 0 32-14.33 32-32v-96h32c17.67 0 32-14.33 32-32V256C512 220.7 483.3 192 448 192zM384 448H128v-96h256V448zM432 296c-13.25 0-24-10.75-24-24c0-13.27 10.75-24 24-24s24 10.73 24 24C456 285.3 445.3 296 432 296zM128 64h229.5L384 90.51V160h64V77.25c0-8.484-3.375-16.62-9.375-22.62l-45.25-45.25C387.4 3.375 379.2 0 370.8 0H96C78.34 0 64 14.33 64 32v128h64V64z" />
                                    </svg>
                                    Print
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="bg-white rounded-lg shadow-md">
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="No." />
                            <x-table.th name="ERF Reference" />
                            <x-table.th name="Date Requested" />
                            <x-table.th name="Branch" />
                            <x-table.th name="Division" />
                            <x-table.th name="Job Title" />
                            <x-table.th name="Job Level" />
                            <x-table.th name="Employment Status" />
                            <x-table.th name="Project Name" />
                            <x-table.th name="Duration" />
                            <x-table.th name="Reason for Request" />
                            <x-table.th name="Target Hire Date" />
                            <x-table.th name="Requested By" />
                            <x-table.th name="Approved By" />
                            <x-table.th name="Status" />
                            <x-table.th name="Attachment" />
                            <x-table.th name="Action" />
                        </x-slot>
                        <x-slot name="tbody">

                            @foreach ($employee_requisitions as $i => $employee_requisition)
                                @if ($employee_requisition)
                                    <tr class="border-0 cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                        <td class="p-3 whitespace-nowrap">{{ $i + 1 }}.
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->erf_reference_no }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ date('M. d, Y', strtotime($employee_requisition->created_at)) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->branch->display ?? '' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->division->description }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->position->display ?? '' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->job_level->display ?? '' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->employment_category->display ?? '' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->project_name == null ? '-' : $employee_requisition->project_name }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            @if ($employee_requisition->employment_category->id == 2)
                                                {{ date('M. d, Y', strtotime($employee_requisition->duration_from)) }}
                                                -<br>
                                                {{ date('M. d, Y', strtotime($employee_requisition->duration_to)) }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->requisition_reason->display ?? '' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ date('M. d, Y', strtotime($employee_requisition->target_hire)) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">{{ $employee_requisition->user->name ?? '' }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $employee_requisition->second_status != 2 ? $employee_requisition->firstApprover->name ?? '' : $employee_requisition->secondApprover->name }}
                                        </td>

                                        <td class="p-3 whitespace-nowrap">
                                            <span
                                                class="{{ $employee_requisition->final_status == 1
                                                    ? 'text-orange bg-orange-light px-4'
                                                    : ($employee_requisition->final_status == 2
                                                        ? 'text-orange bg-red-100 px-6'
                                                        : ($employee_requisition->final_status == 3
                                                            ? 'text-blue bg-blue-100 px-5'
                                                            : ($employee_requisition->final_status == 4
                                                                ? 'text-red bg-red-100 px-6'
                                                                : ''))) }} 
                                            text-xs rounded-full p-1">
                                                {{ $employee_requisition->status->display ?? '' }}</span>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <svg wire:click="action({'id': {{ $employee_requisition->id }}}, 'view_employee_requisition')"
                                                data-toggle="tooltip"
                                                class="w-4 h-4 ml-8 cursor-pointer text-blue hover:text-blue-700"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M256 0v128h128L256 0zM224 128L224 0H48C21.49 0 0 21.49 0 48v416C0 490.5 21.49 512 48 512h288c26.51 0 48-21.49 48-48V160h-127.1C238.3 160 224 145.7 224 128zM272 416h-160C103.2 416 96 408.8 96 400C96 391.2 103.2 384 112 384h160c8.836 0 16 7.162 16 16C288 408.8 280.8 416 272 416zM272 352h-160C103.2 352 96 344.8 96 336C96 327.2 103.2 320 112 320h160c8.836 0 16 7.162 16 16C288 344.8 280.8 352 272 352zM288 272C288 280.8 280.8 288 272 288h-160C103.2 288 96 280.8 96 272C96 263.2 103.2 256 112 256h160C280.8 256 288 263.2 288 272z" />
                                            </svg>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <div class="flex gap-3">
                                                @if ($employee_requisition->first_status == 1)
                                                    @can('hrim_employee_requisition_edit')
                                                        <svg wire:click="action({'id':  {{ $employee_requisition->id }}}, 'edit_employee_requisition')"
                                                            class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700"
                                                            data-prefix="far" data-icon="approve" role="img"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                            <path fill="currentColor"
                                                                d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                        </svg>
                                                    @endcan
                                                    <svg wire:click="action({'id': {{ $employee_requisition->id }}}, 'cancel_employee_requisition')"
                                                        class="w-4 h-4 cursor-pointer text-red hover:text-red-700"
                                                        data-prefix="far" data-icon="cancel-alt" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
                                                    </svg>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </x-slot>
                    </x-table.table>
                </div>
            </div>
        </div>

    </x-slot>
</x-form>
