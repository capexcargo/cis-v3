<div wire:init="load" x-data="{
    confirmation_modal: '{{ $confirmation_modal }}'
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                New Employee Requisition Summary
                <table class="mt-6 table-auto">
                    <tbody class="text-sm font-light text-gray-900">
                        <tr>
                            <td><span class="mr-2 text-xs font-semibold text-gray-400">ERF Reference : </span></td>
                            <td class="mr-2"><span class="mr-2 font-semibold">{{ $erf_reference }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="mr-2 text-xs font-semibold text-gray-400">Branch : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @foreach ($branch_reference as $branch_ref)
                                        {{ $branch_ref->id == $branch ? $branch_ref->display : '' }}
                                    @endforeach
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="mr-2 text-xs font-semibold text-gray-400">Division : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @foreach ($division_reference as $division_ref)
                                        {{ $division_ref->id == $division ? $division_ref->name : '' }}
                                    @endforeach
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="mr-2 text-xs font-semibold text-gray-400">Job Title : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @foreach ($position_reference as $position_ref)
                                        {{ $position_ref->id == $position ? $position_ref->display : '' }}
                                    @endforeach
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="mr-2 text-xs font-semibold text-gray-400">Job Level : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @foreach ($job_level_reference as $job_level_ref)
                                        {{ $job_level_ref->id == $job_level ? $job_level_ref->display : '' }}
                                    @endforeach
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="mr-2 text-xs font-semibold text-gray-400">Employment Status : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @foreach ($employment_category_reference as $employment_category_ref)
                                        {{ $employment_category_ref->id == $employment_cat ? $employment_category_ref->display : '' }}
                                    @endforeach
                                </span>
                            </td>
                        </tr>
                        @if ($employment_cat == 2)
                            <tr>
                                <td><span class="mr-2 text-xs font-semibold text-gray-400">Non-Regular Type : </span>
                                </td>
                                <td class="mr-2">
                                    <span class="mr-2 font-semibold">
                                        @foreach ($employment_category_type_reference as $employment_category_type_ref)
                                            {{ $employment_category_type_ref->id == $emp_cat_type ? $employment_category_type_ref->display : '' }}
                                        @endforeach
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="mr-2 text-xs font-semibold text-gray-400">Project Name : </span>
                                </td>
                                <td class="mr-2"><span class="mr-2 font-semibold">{{ $project_name }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="mr-2 text-xs font-semibold text-gray-400">Duration : </span>
                                </td>
                                <td class="mr-2"><span
                                        class="mr-2 font-semibold">{{ date('M. d, Y', strtotime($duration_from)) }} to
                                        {{ date('M. d, Y', strtotime($duration_to)) }}</span>
                                </td>
                            </tr>
                        @endif
                        <tr>
                            <td><span class="mr-2 text-xs font-semibold text-gray-400">Reason for Request : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @foreach ($request_reason_reference as $request_reason_ref)
                                        {{ $request_reason_ref->id == $request_reason ? $request_reason_ref->display : '' }}
                                    @endforeach
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="mr-2 text-xs font-semibold text-gray-400">Target Hire Date : </span></td>
                            <td class="mr-2"><span
                                    class="mr-2 font-semibold">{{ date('M. d, Y', strtotime($target_hire_date)) }}</span>
                            </td>
                        </tr>
                        {{-- <tr>
                            <td><span class="mr-2 text-xs font-semibold text-gray-400">Attachment : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @foreach ($attachments as $a => $attachment)
                                        {{ basename($attachments[$a]['attachment']) }}
                                    @endforeach
                                </span>
                            </td>
                        </tr> --}}
                        <br>
                        <tr>
                            <td><span class="mr-2 text-xs font-semibold text-gray-400">Requested By : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    {{ Auth::user()->name }}
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="mr-2 text-xs font-semibold text-gray-400">Approved By : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    {{ $approver->firstApprover->name ?? '' }}
                                    {{ $approver->secondApprover->name ?? '' != '' ? '&' : '' }}<br>
                                    {{ $approver->secondApprover->name ?? '' }}
                                    {{-- @foreach ($approvedBy as $approver)
                                        @foreach ($approved_by_reference as $approved_by)
                                            {{ $approved_by->id == ($approver->division_id == $division && $approver->branch_id == $branch ? $approver->first_approver : '') ? $approved_by->name : '' }}
                                        @endforeach
                                    @endforeach --}}
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </h2>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="confirm"
                    class="px-8 py-1 mt-4 text-sm flex-none bg-[#003399] text-white rounded-md">
                    Confirm</button>
            </div>
        </x-slot>
    </x-modal>
    <form autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="erf_reference" value="ERF Reference" />
                    <x-input type="text" name="erf_reference" wire:model.defer='erf_reference' disabled></x-input>
                    <x-input-error for="erf_reference" />
                </div>

                <div class="grid grid-cols-2 gap-4">
                    <div>
                        <x-label for="branch" value="Branch" :required="true" />
                        <x-select name="branch" wire:model='branch'>
                            <option value="">Select</option>
                            @foreach ($branch_reference as $branch_ref)
                                <option value="{{ $branch_ref->id }}">
                                    {{ $branch_ref->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="branch" />
                    </div>
                    <div>
                        <x-label for="division" value="Division" :required="true" />
                        <x-select name="division" wire:model='division'>
                            <option value="">Select</option>
                            @foreach ($division_reference as $division_ref)
                                <option value="{{ $division_ref->id }}">
                                    {{ $division_ref->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="division" />
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-4">
                    <div class="text-sm">
                        <x-label for="position" value="Position" :required="true" />
                        <x-select name="position" wire:model='position'>
                            <option value="">Select</option>
                            @foreach ($position_reference as $position_ref)
                                <option value="{{ $position_ref->id }}">
                                    {{ $position_ref->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="position" />
                    </div>
                    <div>
                        <x-label for="job_level" value="Job Level" :required="true" />
                        <x-select name="job_level" wire:model='job_level'>
                            <option value="">Select</option>
                            @foreach ($job_level_reference as $job_level_ref)
                                <option value="{{ $job_level_ref->id }}">
                                    {{ $job_level_ref->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="job_level" />
                    </div>
                </div>
                <div class="-mb-2">
                    <x-label for="employment_cat" value="Employment Status" :required="true" />
                    <x-select name="employment_cat" wire:model='employment_cat'>
                        <option value="employment_cat">Select</option>
                        @foreach ($employment_category_reference as $request_category_ref)
                            <option value="{{ $request_category_ref->id }}">
                                {{ $request_category_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="employment_cat" />
                </div>
                @if ($employment_cat == 2)
                    <div>
                        @foreach ($employment_category_type_reference as $employment_category_type_ref)
                            <div class="pt-1 pr-1 text-xs form-check form-check-inline">
                                <input type="radio" class="default:ring-2" name="emp_cat_type"
                                    wire:model="emp_cat_type" value="{{ $employment_category_type_ref->id }}">
                                <label class="text-gray-800 form-check-label"
                                    for="">{{ $employment_category_type_ref->display }}</label>
                            </div>
                        @endforeach
                        <x-input-error for="emp_cat_type" />
                    </div>

                    <div>
                        <x-label for="project_name" value="Project Name" :required="true" />
                        <x-input name="project_name" wire:model.defer='project_name'></x-input>
                        <x-input-error for="project_name" />
                    </div>

                    <x-label for="" value="Duration From" :required="true" class="-mb-2" />
                    <div class="grid grid-cols-2 gap-4">
                        <div>
                            <x-input type="date" name="duration_from" wire:model='duration_from'></x-input>
                            <x-input-error for="duration_from" />
                        </div>

                        <div>
                            <x-input type="date" name="duration_to" wire:model='duration_to'></x-input>
                            <x-input-error for="duration_to" />
                        </div>
                    </div>
                @endif
                <div>
                    <x-label for="request_reason" value="Reason for Request" :required="true" />
                    <x-select name="request_reason" wire:model='request_reason'>
                        <option value="request_reason">Select</option>
                        @foreach ($request_reason_reference as $request_reason_ref)
                            <option value="{{ $request_reason_ref->id }}">
                                {{ $request_reason_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="request_reason" />
                </div>
                <div>
                    <x-label for="target_hire_date" value="Target Hire Date" :required="true" />
                    <x-input type="date" name="target_hire_date" wire:model='target_hire_date'></x-input>
                    <x-input-error for="target_hire_date" />
                </div>

                <div>
                    <div class="text-2xl font-bold text-blue">
                        <div class="flex items-center -mb-2 space-x-3">
                            <x-label for="attachment" value="Attach 3D Profile" :required="true" />
                        </div>
                        <x-table.table>
                            <x-slot name="thead">
                                {{-- <x-table.th name="File" />
                            <x-table.th name="Action" /> --}}
                            </x-slot>
                            <x-slot name="tbody">
                                @forelse ($attachments as $i => $attachment)
                                    <tr class="text-sm border-0 cursor-pointer">
                                        <td class="flex p-2 items-left justify-left whitespace-nowrap">
                                            <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                <div class="relative z-0 ">
                                                    <div class="absolute top-0 left-0" hidden>
                                                        @if (!$attachments[$i]['attachment'])
                                                            <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                src="{{ $attachments[$i]['attachment'] ? $attachments[$i]['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                        @elseif (in_array($attachments[$i]['attachment']->extension(), config('filesystems.image_type')))
                                                            <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                src="{{ $attachments[$i]['attachment'] ? $attachments[$i]['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                        @else
                                                            <svg class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg"
                                                                aria-hidden="true" focusable="false"
                                                                data-prefix="fas" data-icon="file-alt" role="img"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 384 512">
                                                                <path fill="currentColor"
                                                                    d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z">
                                                                </path>
                                                            </svg>
                                                        @endif
                                                    </div>
                                                    <input type="file"
                                                        name="attachments.{{ $i }}.attachment"
                                                        wire:model="attachments.{{ $i }}.attachment"
                                                        class="text-xs focus:outline-none focus:content-none">
                                                </div>
                                                <x-input-error for="attachments.{{ $i }}.attachment" />
                                            </div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            @if (count($attachments) > 1)
                                                <a wire:click="removeAttachments({{ $i }})">
                                                    <p class="text-red-600 underline underline-offset-2">Remove</p>
                                                </a>
                                                <svg hidden wire:click="removeAttachments({{ $i }})"
                                                    class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                    data-prefix="fas" data-icon="times-circle" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                    <path fill="currentColor"
                                                        d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                    </path>
                                                </svg>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3">
                                            <p class="text-center">Empty.</p>
                                        </td>
                                    </tr>
                                @endforelse
                            </x-slot>
                        </x-table.table>
                        <div class="flex items-center justify-start">
                            <button type="button" title="Add Attachment" wire:click="addAttachments"
                                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                +</button>
                        </div>
                    </div>
                </div>

                <div class="mt-4 mb-2">
                    <div class="grid gap-2">
                        <div class="flex flex-col space-y-3 text-xs text-gray-600">
                            <x-table.table>
                                <x-slot name="thead">
                                    <x-table.th name="Approvals" />
                                    <x-table.th name="Name" />
                                </x-slot>
                                <x-slot name="tbody">
                                    <tr
                                        class="border-0 cursor-pointer text-md even:bg-white hover:text-white hover:bg-blue-200">
                                        <td class="flex p-2 items-left justify-left whitespace-nowrap">
                                            <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                <div class="relative z-0 ">
                                                    Requested By
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-2 text-black whitespace-nowrap">
                                            {{ Auth::user()->name }}
                                        </td>
                                    </tr>
                                    <tr
                                        class="border-0 cursor-pointer text-md even:bg-white hover:text-white hover:bg-blue-200">
                                        <td class="flex p-2 items-left justify-left whitespace-nowrap">
                                            <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                <div class="relative z-0 ">
                                                    Approved By
                                                </div>
                                            </div>
                                        </td>
                                        <td class="p-2 text-black whitespace-nowrap">
                                            {{ $approver->firstApprover->name ?? '' }}
                                            {{ $approver->secondApprover->name ?? '' != '' ? '&' : '' }}<br>
                                            {{ $approver->secondApprover->name ?? '' }}
                                            {{-- @foreach ($approvedBy as $approver)
                                                @foreach ($approved_by_reference as $approved_by)
                                                    {{ $approved_by->id == ($approver->division_id == $division && $approver->branch_id == $branch ? $approver->first_approver : '') ? $approved_by->name : '' }}
                                                @endforeach
                                            @endforeach --}}
                                        </td>
                                    </tr>
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                </div>
                <div class="flex justify-end mt-6 space-x-3">
                    <button type="button" wire:click="$emit('close_modal', 'create')"
                        class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <button type="button" wire:click="action({}, 'submit')"
                        class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                        Submit
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
