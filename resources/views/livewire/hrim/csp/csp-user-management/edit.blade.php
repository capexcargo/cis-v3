<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div class="grid gap-4 grid-cols">
                    <div wire:init="agencyReference">
                        <x-label for="agency_id" value="Agency Name" :required="true" />
                        <x-select name="agency_id" wire:model.defer='agency_id'>
                            <option value="">-Select-</option>
                            @foreach ($agency_reference as $agency_ref)
                                <option value="{{ $agency_ref->id }}">
                                    {{ $agency_ref->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="agency_id" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols">
                    <div wire:init="branchReference">
                        <x-label for="branch_id" value="Branch" :required="true" />
                        <x-select name="branch_id" wire:model.defer='branch_id'>
                            <option value="">-Select-</option>
                            @foreach ($branch_reference as $branch_ref)
                                <option value="{{ $branch_ref->id }}">
                                    {{ $branch_ref->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="branch_id" />
                    </div>
                </div>
                <div>
                    <x-label for="agency_contact_person" value="Contact Person" :required="true" />
                    <x-input type="text" name="agency_contact_person" wire:model.defer='agency_contact_person'>
                    </x-input>
                    <x-input-error for="agency_contact_person" />
                </div>
                <div>
                    <x-label for="agency_contact_no" value="Contact No." :required="true" />
                    <x-input type="text" name="agency_contact_no" wire:model.defer='agency_contact_no'>
                    </x-input>
                    <x-input-error for="agency_contact_no" />
                </div>
                <div>
                    <x-label for="agency_position" value="Position" :required="true" />
                    <x-input type="text" name="agency_position" wire:model.defer='agency_position'>
                    </x-input>
                    <x-input-error for="agency_position" />
                </div>
                <div>
                    <x-label for="personal_email" value="Email Address" :required="true" />
                    <x-input type="text" name="personal_email" wire:model.defer='personal_email'>
                    </x-input>
                    <x-input-error for="personal_email" />
                </div>
                <div>
                    <x-label for="password" value="Password" :required="true" />
                    <x-input type="password" name="password" wire:model.defer='password'>
                    </x-input>
                    <x-input-error for="password" />
                </div>
                <div>
                    <x-label for="password_confirmation" value="Confirm Password" :required="true" />
                    <x-input type="password" name="password_confirmation" wire:model.defer='password_confirmation'>
                    </x-input>
                    <x-input-error for="password_confirmation" />
                </div>

                <div class="flex justify-end mt-6 space-x-3">
                    <button type="button" wire:click="$emit('close_modal', 'create')"
                        class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
