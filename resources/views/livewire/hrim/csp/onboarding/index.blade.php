<x-form wire:init="load" x-data="{ search_form: true, confirmation_modal: '{{ $confirmation_modal }}', create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                <div class="space-y-3">
                    <p class="text-center">{{ $confirmation_message }}</p>
                    <div class="flex items-center justify-center space-x-3">
                        <x-button type="button" wire:click="$set('confirmation_modal', false)" title="No"
                            class="py-1 bg-white text-blue hover:bg-gray-100" />
                        <x-button type="button" wire:click="confirm" title="Yes"
                            class="bg-blue text-white hover:bg-[#002161] py-1" />
                    </div>
                </div>
            </x-slot>
        </x-modal>
        @can('hrim_csp_onboarding_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/4">
                <x-slot name="title">Add Employee</x-slot>
                <x-slot name="body">
                    @livewire('hrim.csp.onboarding.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_csp_onboarding_edit')
            @if ($edit_modal && $user_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/4">
                    <x-slot name="title">Edit Employee</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.csp.onboarding.edit', ['id' => $user_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>
    <x-slot name="search_form">
        <x-transparent.input type="text" label="Employee Name" name="employee_name"
            wire:model.debounce.500ms="employee_name" />
        <x-transparent.select value="{{ $position }}" label="Position" name="position" wire:model="position">
            <option value=""></option>
            @foreach ($position_references as $position_reference)
                <option value="{{ $position_reference->id }}">
                    {{ $position_reference->display }}
                </option>
            @endforeach
        </x-transparent.select>
        <x-transparent.select value="{{ $status }}" label="Status" name="status" wire:model="status">
            <option value=""></option>
            @foreach ($status_references as $status_reference)
                <option value="{{ $status_reference->id }}">
                    {{ $status_reference->display }}
                </option>
            @endforeach
        </x-transparent.select>
    </x-slot>
    <x-slot name="header_title">Onboarding</x-slot>
    <x-slot name="header_button">
        @can('hrim_csp_onboarding_add')
            <x-button type="button" wire:click="$set('create_modal', true)" title="Add CSP Employee"
                class="bg-blue text-white hover:bg-[#002161]">
                <x-slot name="icon">
                    <svg class="w-3 h-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                </x-slot>
            </x-button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Name" />
                        <x-table.th name="Position" />
                        <x-table.th name="Employment Category" />
                        <x-table.th name="Contact Number" />
                        <x-table.th name="Email Address" />
                        <x-table.th name="Status" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($employee_informations as $employee_information)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="flex items-center justify-start px-3 py-1 space-x-3 whitespace-nowrap">
                                    <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                        style="background-image: url({{ Storage::disk('hrim_gcs')->url($employee_information->user->photo_path . $employee_information->user->photo_name) }})">
                                    </div>
                                    <p class="capitalize ">
                                        {{ $employee_information->last_name . ', ' . $employee_information->first_name . ' ' . $employee_information->middle_name }}
                                    </p>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $employee_information->department->display ?? 'N/A' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $employee_information->employmentCategory->display ?? 'N/A' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $employee_information->company_mobile_number }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($employee_information->user->email_verification_code)
                                        <x-button type="button" title="Request User Account"
                                            class="py-1 bg-white text-blue hover:bg-gray-200 text-1xs">
                                        </x-button>
                                    @else
                                        {{ $employee_information->user->email }}
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($employee_information->user->status_id == 1)
                                        <p class="uppercase text-blue">
                                            {{ $employee_information->user->status->display }}
                                        </p>
                                    @else
                                        <p class="uppercase text-red">
                                            {{ $employee_information->user->status->display }}
                                        </p>
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        @can('hrim_csp_onboarding_edit')
                                            <svg wire:click="action({'id': {{ $employee_information->user_id }}}, 'edit')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                </path>
                                            </svg>
                                        @endcan
                                        @if ($employee_information->status_id == 1)
                                            <svg wire:click="action({'id': {{ $employee_information->user_id }}}, 'confirmation_deactivate')"
                                                class="w-5 h-5 text-red" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 640 512">
                                                <path fill="currentColor"
                                                    d="M95.1 477.3c0 19.14 15.52 34.67 34.66 34.67h378.7c5.625 0 10.73-1.65 15.42-4.029L264.9 304.3C171.3 306.7 95.1 383.1 95.1 477.3zM630.8 469.1l-277.1-217.9c54.69-14.56 95.18-63.95 95.18-123.2C447.1 57.31 390.7 0 319.1 0C250.2 0 193.7 55.93 192.3 125.4l-153.4-120.3C34.41 1.672 29.19 0 24.03 0C16.91 0 9.845 3.156 5.127 9.187c-8.187 10.44-6.375 25.53 4.062 33.7L601.2 506.9c10.5 8.203 25.56 6.328 33.69-4.078C643.1 492.4 641.2 477.3 630.8 469.1z" />
                                            </svg>
                                        @elseif($employee_information->status_id == 3)
                                            <svg wire:click="action({'id': {{ $employee_information->user_id }}}, 'confirmation_reactivate')"
                                                class="w-5 h-5 text-red" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 640 512">
                                                <path fill="currentColor"
                                                    d="M95.1 477.3c0 19.14 15.52 34.67 34.66 34.67h378.7c5.625 0 10.73-1.65 15.42-4.029L264.9 304.3C171.3 306.7 95.1 383.1 95.1 477.3zM630.8 469.1l-277.1-217.9c54.69-14.56 95.18-63.95 95.18-123.2C447.1 57.31 390.7 0 319.1 0C250.2 0 193.7 55.93 192.3 125.4l-153.4-120.3C34.41 1.672 29.19 0 24.03 0C16.91 0 9.845 3.156 5.127 9.187c-8.187 10.44-6.375 25.53 4.062 33.7L601.2 506.9c10.5 8.203 25.56 6.328 33.69-4.078C643.1 492.4 641.2 477.3 630.8 469.1z" />
                                            </svg>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $employee_informations ? $employee_informations->links() : '' }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
