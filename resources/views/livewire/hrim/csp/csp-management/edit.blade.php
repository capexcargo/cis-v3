<div>
    <x-loading></x-loading>


    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">


                <div class="grid gap-4">
                    <div>
                        <x-label for="name" value="Agency Name" :required="true" />
                        <x-input type="text" name="name" wire:model.defer='name'>
                        </x-input>
                        <x-input-error for="name" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols">
                    <div wire:init="branchReference">
                        <x-label for="branch_id" value="Branch" :required="true" />
                        <x-select name="branch_id" wire:model.defer='branch_id'>
                            <option value="">Select</option>
                            @foreach ($branch_reference as $branch_ref)
                                <option value="{{ $branch_ref->id }}">
                                    {{ $branch_ref->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="branch_id" />
                    </div>
                </div>



                <div class="flex justify-end mt-6 space-x-3">
                    <button type="button" wire:click="$emit('close_modal', 'create')"
                        class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
