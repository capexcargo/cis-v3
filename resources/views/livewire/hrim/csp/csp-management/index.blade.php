<x-form x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    delete_modal: '{{ $delete_modal }}',
    activate_modal: '{{ $activate_modal }}',
    deactivate_modal: '{{ $deactivate_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_csp_management_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Add CSP</x-slot>
                <x-slot name="body">
                    @livewire('hrim.csp.csp-management.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_csp_management_edit')
            @if ($csp_management_id && $edit_modal)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Edit CSP</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.csp.csp-management.edit', ['id' => $csp_management_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('hrim_csp_management_delete')
            <x-modal id="delete_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="body">
                    <h2 class="mb-3 text-lg text-left text-gray-900">
                        {{ $confirmation_message }}
                    </h2>
                    <div class="flex justify-center space-x-3">
                        <button type="button" wire:click="$set('confirmation_modal', false)"
                            class="px-12 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-md cursor-pointer hover:text-white hover:bg-red-400">No</button>
                        <button type="button" wire:click="confirm({}, 'delete')"
                            class="flex-none px-12 py-1 mt-4 text-sm text-white rounded-md cursor-pointer bg-blue">
                            Yes</button>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_csp_management_activate')
            <x-modal id="activate_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            {{ $confirmation_message }}
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('activate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                            <button wire:click="updateStatus({{ $csp_management_id }}, 1)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">YES</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="deactivate_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            {{ $confirmation_message }}
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('deactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                            <button wire:click="updateStatus({{ $csp_management_id }}, 2)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">YES</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
    </x-slot>
    <x-slot name="header_title">CSP Management</x-slot>
    <x-slot name="header_button">
        @can('hrim_csp_management_add')
            <button wire:click="action({}, 'create')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Add CSP
                </div>
            </button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-2 gap-4">
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="No." />
                        <x-table.th name="Agency Name" />
                        <x-table.th name="Office Branch" />
                        <x-table.th name="Status" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($csp_managements as $i => $csp_management)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ ($csp_managements->currentPage() - 1) * $csp_managements->links()->paginator->perPage() + $loop->iteration }}.
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $csp_management->name }}

                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $csp_management->branch->display }}

                                </td>
                                <td
                                    class="p-3 whitespace-nowrap w-32 {{ $csp_management->is_active == 1 ? 'text-green' : 'text-red' }}">
                                    {{ $csp_management->is_active == 1 ? 'Active' : 'Inactive' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        @can('hrim_csp_management_edit')
                                            <svg wire:click="action({'id': {{ $csp_management->id }}}, 'edit')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                </path>
                                            </svg>
                                        @endcan
                                        @can('hrim_csp_management_delete')
                                            <svg wire:click="action({'id': {{ $csp_management->id }}}, 'delete')"
                                                class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                data-prefix="fas" data-icon="trash-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                                </path>
                                            </svg>
                                        @endcan

                                        @can('hrim_csp_management_activate')
                                            @if ($csp_management->is_active == 1)
                                                <svg wire:click="action({'id': {{ $csp_management->id }}, 'status': 2}, 'update_status')"
                                                    class="w-10 h-6 rounded-full text-green" aria-hidden="true"
                                                    focusable="false" data-prefix="fas" data-icon="user-slash"
                                                    role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 680 510">
                                                    <path fill="currentColor"
                                                        d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                    </path>
                                                </svg>
                                            @else
                                                <svg wire:click="action({'id': {{ $csp_management->id }}, 'status': 1}, 'update_status')"
                                                    class="w-10 h-6 rounded-full text-red" aria-hidden="true"
                                                    focusable="false" data-prefix="fas" data-icon="user-slash"
                                                    role="img" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 680 510">
                                                    <path fill="currentColor"
                                                        d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                                    </path>
                                                </svg>
                                            @endif
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
            </div>
        </div>
    </x-slot>
</x-form>
