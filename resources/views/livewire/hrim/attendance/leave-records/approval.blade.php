<div wire:init="load">
    <x-loading />
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-6">
            <div class="grid grid-cols-2 gap-6">
                <div>
                    <x-label value="Inclusive Date From" :required="true" />
                    <x-input type="date" name="inclusive_date_from" wire:model.defer='inclusive_date_from' disabled />
                    <x-input-error for="inclusive_date_from" />
                </div>
                <div>
                    <x-label value="Inclusive Date To" :required="true" />
                    <x-input type="date" name="inclusive_date_to" wire:model.defer='inclusive_date_to' disabled />
                    <x-input-error for="inclusive_date_to" />
                </div>
                <div class="col-span-2">
                    <x-label value="Resume of Work" :required="true" />
                    <x-input type="date" name="resume_of_work" wire:model.defer='resume_of_work' disabled />
                    <x-input-error for="resume_of_work" />
                </div>
                <div class="col-span-2">
                    <x-label value="Type of Leave" :required="true" />
                    <x-select name="type_of_leave" wire:model='type_of_leave' disabled>
                        <option value=""></option>
                        @foreach ($leave_type_references as $leave_type_reference)
                            <option value="{{ $leave_type_reference->id }}">
                                {{ $leave_type_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="type_of_leave" />
                </div>
                @if ($type_of_leave == 1)
                    <div class="col-span-2">
                        <x-input-radio label="With Medical Certificate" name="is_with_medical_certificate"
                            value="1" wire:model.defer="is_with_medical_certificate" disabled />
                        <x-input-radio label="Without Medical Certificate" name="is_with_medical_certificate"
                            value="0" wire:model.defer="is_with_medical_certificate" disabled />
                        <x-input-error for="is_with_medical_certificate" />
                    </div>
                @endif
                <div class="col-span-2">
                    <x-label value="Apply For" :required="true" />
                    <x-select name="apply_for" wire:model.defer='apply_for' disabled>
                        <option value=""></option>
                        @foreach ($leave_day_type_references as $leave_day_type_reference)
                            <option value="{{ $leave_day_type_reference->id }}">
                                {{ $leave_day_type_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="apply_for" />
                </div>
                <div class="col-span-2">
                    <x-input-radio label="With Pay" name="is_with_pay" value="1" wire:model.defer="is_with_pay"
                        disabled />
                    <x-input-radio label="Without Pay" name="is_with_pay" value="0" wire:model.defer="is_with_pay"
                        disabled />
                    <x-input-error for="is_with_pay" />
                </div>
                <div class="col-span-2">
                    <x-label value="Reason" :required="true" />
                    <x-textarea name="reason" wire:model.defer='reason' disabled></x-textarea>
                    <x-input-error for="reason" />
                </div>
                <div class="col-span-2">
                    <x-label value="Reliever" />
                    <x-select name="reliever" wire:model.defer='reliever' disabled>
                        <option value=""></option>
                        @foreach ($reliever_references as $reliever_reference)
                            <option value="{{ $reliever_reference->id }}">
                                {{ $reliever_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="reliever" />
                </div>
            </div>

            {{-- <div class="grid grid-cols-3 gap-3">
                <div class="space-y-3">
                    <div>
                        <div>
                            <x-label value="First Approver" :required="true" />
                            <x-input type="text" name="first_approver_id" value="{{ $leave->firstApprover->name }}"
                                disabled>
                            </x-input>
                            <x-input-error for="first_approver_id" />
                        </div>
                    </div>
                    @if ($leave->first_approver == auth()->user()->id || (auth()->user()->level_id == 5 && $leave->first_approver))
                        <div class="flex justify-between">
                            <x-input-radio label="Approve" name="is_first_approved" wire:model.defer="is_first_approved"
                                value="1" />
                            <x-input-radio label="Disapprove" name="is_first_approved"
                                wire:model.defer="is_first_approved" value="0" />
                        </div>
                        <div>
                            <x-label value="Remarks" />
                            <x-textarea type="text" name="first_approver_remarks"
                                wire:model.defer='first_approver_remarks' />
                            <x-input-error for="first_approver_remarks" />
                        </div>
                    @endif
                </div>
                <div class="space-y-3">
                    @if ($leave->second_approver == auth()->user()->id || (auth()->user()->level_id == 5 && $leave->second_approver))
                        <div>
                            <div>
                                <x-label value="Second Approver" :required="true" />
                                <x-input type="text" name="second_approver_id"
                                    value="{{ $leave->secondApprover->name }}" disabled>
                                </x-input>
                                <x-input-error for="second_approver_id" />
                            </div>
                        </div>
                        <div class="flex justify-between">
                            <x-input-radio label="Approve" name="is_second_approved"
                                wire:model.defer="is_second_approved" value="1" />
                            <x-input-radio label="Disapprove" name="is_second_approved"
                                wire:model.defer="is_second_approved" value="0" />
                        </div>
                        <div>
                            <x-label value="Remarks" />
                            <x-textarea type="text" name="second_approver_remarks"
                                wire:model.defer='second_approver_remarks' />
                            <x-input-error for="second_approver_remarks" />
                        </div>
                    @endif
                </div>
                <div class="space-y-3">
                    @if ($leave->third_approver == auth()->user()->id || (auth()->user()->level_id == 5 && $leave->third_approver))
                        <div>
                            <div>
                                <x-label value="Third Approver" :required="true" />
                                <x-input type="text" name="third_approver_id"
                                    value="{{ $leave->thirdApprover->name }}" disabled>
                                </x-input>
                                <x-input-error for="third_approver_id" />
                            </div>
                        </div>
                        <div class="flex justify-between">
                            <x-input-radio label="Approve" name="is_third_approved"
                                wire:model.defer="is_third_approved" value="1" />
                            <x-input-radio label="Disapprove" name="is_third_approved"
                                wire:model.defer="is_third_approved" value="0" />
                        </div>
                        <div>
                            <x-label value="Remarks" />
                            <x-textarea type="text" name="third_approver_remarks"
                                wire:model.defer='third_approver_remarks' />
                            <x-input-error for="third_approver_remarks" />
                        </div>
                    @endif
                </div>
            </div> --}}
            <div class="flex justify-end space-x-3">
                <x-button type="button" wire:click="$emit('close_modal', 'approval')" title="Cancel"
                    class="bg-white text-blue hover:bg-gray-100" />
                {{-- @if ($leave->final_status_id != 3)
                    <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
                @endif --}}
            </div>
        </div>
    </form>
</div>
