<div>
    <x-table.table>
        <x-slot name="thead">
            <x-table.th name="Approvers" />
            <x-table.th name="Status" />
        </x-slot>
        <x-slot name="tbody">
            @if ($leave->firstApprover)
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        {{ $leave->firstApprover->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if (is_null($leave->first_status))
                            <span class="text-requested">Requested</span>
                        @elseif($leave->first_status === 0)
                            <span class="text-declined">Declined</span>
                        @elseif($leave->first_status === 1)
                            <span class="text-approved">Approved</span>
                        @endif
                    </td>
                </tr>
            @endif
            @if ($leave->secondApprover)
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        {{ $leave->secondApprover->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if (is_null($leave->second_status))
                            <span class="text-requested">Requested</span>
                        @elseif($leave->second_status === 0)
                            <span class="text-declined">Declined</span>
                        @elseif($leave->second_status === 1)
                            <span class="text-approved">Approved</span>
                        @endif
                    </td>
                </tr>
            @endif
            @if ($leave->thirdApprover)
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        {{ $leave->thirdApprover->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if (is_null($leave->third_status))
                            <span class="text-requested">Requested</span>
                        @elseif($leave->third_status === 0)
                            <span class="text-declined">Declined</span>
                        @elseif($leave->third_status === 1)
                            <span class="text-approved">Approved</span>
                        @endif
                    </td>
                </tr>
            @endif
        </x-slot>
    </x-table.table>
</div>
