<x-form wire:init="load" x-data="{ status: '{{ $status }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="header_title">Leave Summary</x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-6 gap-6">
            <x-transparent.select label="Month" name="month" wire:model="month">
                @foreach ($month_references as $month_reference)
                    <option value="{{ $month_reference->id }}">
                        {{ $month_reference->display }}
                    </option>
                @endforeach
            </x-transparent.select>
            <x-transparent.select label="Cut Off" name="cut_off" wire:model="cut_off">
                <option value="first">First</option>
                <option value="second">Second</option>
            </x-transparent.select>
            <x-transparent.input type="number" label="Year" name="year" wire:model.debounce.500ms="year" />
            {{-- <x-transparent.input type="date" label="Payout Date" name="payout_date"
                wire:model.debounce.500ms="payout_date" /> --}}
            <x-transparent.select label="Branch" name="branch" wire:model="branch">
                <option value="">All</option>
                @foreach ($branch_references as $branch_reference)
                    <option value="{{ $branch_reference->id }}">
                        {{ $branch_reference->display }}
                    </option>
                @endforeach
            </x-transparent.select>
            <x-transparent.select label="Employment Category" name="employment_category"
                wire:model="employment_category">
                <option value="">All</option>
                @foreach ($employment_category_references as $employment_category_reference)
                    <option value="{{ $employment_category_reference->id }}">
                        {{ $employment_category_reference->display }}
                    </option>
                @endforeach
            </x-transparent.select>
            <x-transparent.input type="text" label="Employee ID" name="employee_id"
                wire:model.debounce.500ms="employee_id" />
            <x-transparent.input type="text" label="Employee Name" name="employee_name"
                wire:model.debounce.500ms="employee_name" />
        </div>
        <div>
            <div class="flex items-center justify-start ">
                <div class="flex items-center justify-between px-3 py-2 text-sm border border-gray-400"
                    :class="status == 'all' ? 'bg-blue text-white' : ''" wire:click="$set('status', 'all')">
                    <span>All</span>
                    <span class="ml-10">{{ $users_leave_records_all }}</span>
                </div>
                <div class="flex items-center justify-between px-3 py-2 text-sm bg-white border border-gray-400"
                    :class="status == 'for_approval' ? 'bg-blue text-white' : ''"
                    wire:click="$set('status', 'for_approval')">
                    <span>For Approval</span>
                    <span class="ml-10">{{ $users_leave_records_for_approval }}</span>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>

                    <x-slot name="thead">
                        <x-table.th name="No." />
                        <x-table.th name="Name" />
                        <x-table.th name="Employee ID" />
                        <x-table.th name="Employment Category" />
                        <x-table.th name="Branch" />
                        <x-table.th name="Employment Status" />

                        <x-table.th colspan="2" name="Total Filed Leave" />
                        <x-table.th colspan="2" name="Total Approved Leave" />

                        <x-table.th name="Unused Leave" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="thead1">
                        <th colspan="6" class="p-2 tracking-wider whitespace-nowrap ">
                            <span class="flex justify-center font-medium">

                            </span>
                        </th>
                        <th class="p-2 tracking-wider bg-gray-100 whitespace-nowrap">
                            <span class="flex justify-center font-medium">
                                With Pay
                            </span>
                        </th>
                        <th class="p-2 tracking-wider bg-gray-200 whitespace-nowrap">
                            <span class="flex justify-center font-medium">
                                Without Pay
                            </span>
                        </th>
                        <th class="p-2 tracking-wider bg-gray-100 whitespace-nowrap">
                            <span class="flex justify-center font-medium">
                                With Pay
                            </span>
                        </th>
                        <th class="p-2 tracking-wider bg-gray-200 whitespace-nowrap">
                            <span class="flex justify-center font-medium">
                                Without Pay
                            </span>
                        </th>
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($users as $i => $user)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $i + 1 }}
                                </td>
                                <td class="flex items-center justify-start px-3 py-1 space-x-3 whitespace-nowrap">
                                    <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                        style="background-image: url({{ Storage::disk('hrim_gcs')->url($user->photo_path . $user->photo_name) }})">
                                    </div>
                                    <p>{{ $user->name }}</p>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $user->userDetails->employee_number }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $user->userDetails->employmentCategory->display ?? 'N/A' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $user->userDetails->branch->display ?? 'N/A' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $user->userDetails->employmentStatus->display ?? 'N/A' }}
                                </td>
                                <td class="p-3 border-l-2 border-gray-200 whitespace-nowrap">
                                    {{ $user->total_filed_leave_with_pay ?? 0 }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $user->total_filed_leave_without_pay }}
                                </td>
                                <td class="p-3 border-l-2 border-gray-200 whitespace-nowrap">
                                    {{ $user->total_approved_leave_with_pay ?? 0 }}
                                </td>
                                <td class="p-3 border-r-2 border-gray-200 whitespace-nowrap">
                                    {{ $user->total_approved_leave_without_pay }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <?php
                                    $date1 = new DateTime($user->userDetails->hired_date);
                                    $date2 = new DateTime(date('Y-m-d'));
                                    $interval = $date1->diff($date2);
                                    $vacation_leave = 0;
                                    if ($user->userDetails->employment_status_id == 1) {
                                        if ($interval->y <= 5) {
                                            $vacation_leave = 5;
                                        }
                                        if ($interval->y == 6) {
                                            $vacation_leave = 6;
                                        }
                                        if ($interval->y == 7) {
                                            $vacation_leave = 7;
                                        }
                                        if ($interval->y == 8) {
                                            $vacation_leave = 8;
                                        }
                                        if ($interval->y == 9) {
                                            $vacation_leave = 9;
                                        }
                                        if ($interval->y >= 10) {
                                            $vacation_leave = 10;
                                        }
                                    } ?>
                                    @if ($user->userDetails->employment_status_id)
                                        {{ 5 + $vacation_leave - $user->total_points ?? 0 }}
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex items-center justify-center space-x-3">
                                        <a
                                            href="{{ route('hrim.attendance.leave-records.view', ['id' => $user->id]) }}">
                                            <svg class="w-5 h-5 text-blue" data-toggle="tooltip" title="View"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                            </svg>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $users ? $users->links() : '' }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
