<x-form wire:init="load" x-data="{ search_form: true, status: '{{ $status }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="header_title">Schedule Management</x-slot>
    {{-- <x-slot name="search_form">
        <x-transparent.select label="Month" name="month" wire:model="month">
            @foreach ($month_references as $month_reference)
                <option value="{{ $month_reference->id }}">
                    {{ $month_reference->display }}
                </option>
            @endforeach
        </x-transparent.select>
        <x-transparent.select label="Cut Off" name="cut_off" wire:model="cut_off">
            <option value="first">First</option>
            <option value="second">Second</option>
        </x-transparent.select>
        <x-transparent.input type="number" label="Year" name="year" wire:model.debounce.500ms="year" />
        <x-transparent.select label="Branch" name="branch" wire:model="branch">
            <option value="">All</option>
            @foreach ($branch_references as $branch_reference)
                <option value="{{ $branch_reference->id }}">
                    {{ $branch_reference->display }}
                </option>
            @endforeach
        </x-transparent.select>
        <x-transparent.select label="Employment Category" name="employment_category" wire:model="employment_category">
            <option value="">All</option>
            @foreach ($employment_category_references as $employment_category_reference)
                <option value="{{ $employment_category_reference->id }}">
                    {{ $employment_category_reference->display }}
                </option>
            @endforeach
        </x-transparent.select>
        <x-transparent.input type="text" label="Employee ID" name="employee_id"
            wire:model.debounce.500ms="employee_id" />
        <x-transparent.input type="text" label="Employee Name" name="employee_name"
            wire:model.debounce.500ms="employee_name" />
    </x-slot> --}}

    <x-slot name="body">
        <div class="grid grid-cols-2 gap-4 sm:grid-cols-2 md:grid-cols-5 lg:grid-cols-5 xl:grid-cols-5">
            <x-transparent.select label="Month" name="month" wire:model="month">
                @foreach ($month_references as $month_reference)
                    <option value="{{ $month_reference->id }}">
                        {{ $month_reference->display }}
                    </option>
                @endforeach
            </x-transparent.select>
            <x-transparent.select label="Cut Off" name="cut_off" wire:model="cut_off">
                <option value="first">First</option>
                <option value="second">Second</option>
            </x-transparent.select>
            <x-transparent.input type="number" label="Year" name="year" wire:model.debounce.500ms="year" />
            <x-transparent.select label="Branch" name="branch" wire:model="branch">
                <option value="">All</option>
                @foreach ($branch_references as $branch_reference)
                    <option value="{{ $branch_reference->id }}">
                        {{ $branch_reference->display }}
                    </option>
                @endforeach
            </x-transparent.select>
            <x-transparent.select label="Employment Category" name="employment_category"
                wire:model="employment_category">
                <option value="">All</option>
                @foreach ($employment_category_references as $employment_category_reference)
                    <option value="{{ $employment_category_reference->id }}">
                        {{ $employment_category_reference->display }}
                    </option>
                @endforeach
            </x-transparent.select>
            <x-transparent.input type="text" label="Employee ID" name="employee_id"
                wire:model.debounce.500ms="employee_id" />
            <x-transparent.input type="text" label="Employee Name" name="employee_name"
                wire:model.debounce.500ms="employee_name" />
        </div>
        <div>
            <div class="flex items-center justify-start ">
                <div class="flex items-center justify-between px-3 py-2 text-sm border border-gray-400 cursor-pointer"
                    :class="status == 'tar_all' ? 'bg-blue text-white' : ''" wire:click="$set('status', 'tar_all')">
                    <span>All TAR</span>
                    <span class="ml-10">{{ $result['users_tar_all'] }}</span>
                </div>
                <div class="flex items-center justify-between px-3 py-2 text-sm bg-white border border-gray-400 cursor-pointer"
                    :class="status == 'tar_for_approval' ? 'bg-blue text-white' : ''"
                    wire:click="$set('status', 'tar_for_approval')">
                    <span>TAR - For Approval</span>
                    <span class="ml-10">{{ $result['users_tar_for_approval'] }}</span>
                </div>
                <div class="flex items-center justify-between px-3 py-2 text-sm bg-white border border-gray-400 cursor-pointer"
                    :class="status == 'sar_all' ? 'bg-blue text-white' : ''" wire:click="$set('status', 'sar_all')">
                    <span>All SAR</span>
                    <span class="ml-10">{{ $result['users_sar_all'] }}</span>
                </div>
                <div class="flex items-center justify-between px-3 py-2 text-sm bg-white border border-gray-400 cursor-pointer"
                    :class="status == 'sar_for_approval' ? 'bg-blue text-white' : ''"
                    wire:click="$set('status', 'sar_for_approval')">
                    <span>SAR - For Approval</span>
                    <span class="ml-10">{{ $result['users_sar_for_approval'] }}</span>
                </div>
            </div>
            @if ($status == 'tar_all' || $status == 'tar_for_approval')
                @livewire('hrim.attendance.tar.index', ['request' => $request])
            @elseif($status == 'sar_all' || $status == 'sar_for_approval')
                @livewire('hrim.attendance.schedule-adjustment.index', ['request' => $request])
            @endif
        </div>
    </x-slot>
</x-form>
