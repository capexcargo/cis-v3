<x-form x-data="{ search_form: true }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="header_title">Team Status</x-slot>
    <x-slot name="body">
      
        <div class="mx-auto">
            <ul class="divide-y-8 divide-transparent" x-data="{ selected: null }">
                @foreach ($divisions as $i => $division)
                    <li class="relative">
                        <a type="button" href="{{ route('hrim.attendance.team-status.view', ['id' => $division->id]) }}"
                            class="w-full px-8 py-3 text-left bg-white rounded-md shadow-md hover:bg-gray-200"
                            @click="selected !== {{ $i }} ? selected = {{ $i }} : selected = null">
                            <div
                                class="flex items-center justify-between text-base font-medium text-left text-gray-600 uppercase ">
                                <span
                                    class="{{ $division->teams_status ? 'text-gray-600' : 'text-red-500' }}">{{ $division->description }}</span>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </x-slot>

</x-form>
