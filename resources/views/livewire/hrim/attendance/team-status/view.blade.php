<x-form x-data="{
    search_form: true,
    view_modal: '{{ $view_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        {{-- @can('hrim_employee_information_view') --}}
        @if ($view_modal && $user_id)
            <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                {{-- <x-slot name="title">View Employee</x-slot> --}}
                <x-slot name="body">
                    @livewire('hrim.employee-management.employee-information.view', ['id' => $user_id])
                </x-slot>
            </x-modal>
        @endif
        {{-- @endcan --}}
    </x-slot>

    <x-slot name="header_title">
        <div class="flex items-center justify-start space-x-3">
            <a href="{{ route('hrim.attendance.team-status.index') }}">
                <svg class="w-8 h-8 text-blue" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path fill="currentColor"
                        d="M447.1 256C447.1 273.7 433.7 288 416 288H109.3l105.4 105.4c12.5 12.5 12.5 32.75 0 45.25C208.4 444.9 200.2 448 192 448s-16.38-3.125-22.62-9.375l-160-160c-12.5-12.5-12.5-32.75 0-45.25l160-160c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25L109.3 224H416C433.7 224 447.1 238.3 447.1 256z" />
                </svg>
            </a>
            <span>{{ $divisionname ?? '' }}, </span>
            <p class="italic text-blue-600">{{ date('F d, Y') }}</p>
        </div>
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-6 gap-6">
            <x-transparent.input type="text" label="Employee ID" name="employee_id"
                wire:model.debounce.500ms="employee_id" />
            <x-transparent.input type="text" label="Employee Name" name="employee_name"
                wire:model.debounce.500ms="employee_name" />
        </div>
        <div class="bg-white rounded-lg shadow-md">
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="Name" />
                    <x-table.th name="ID Number" />
                    <x-table.th name="Position" />
                    <x-table.th name="Time In" />
                    <x-table.th name="Time Out" />
                    <x-table.th name="Action" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($team_records as $team_record)
                        <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                            <td class="flex items-center justify-start px-3 py-1 space-x-3 whitespace-nowrap">
                                <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                    style="background-image: url({{ Storage::disk('hrim_gcs')->url($team_record->user->photo_path . $team_record->user->photo_name) }})">
                                </div>
                                <p>
                                    {{ ucwords($team_record->last_name) . ', ' . ucwords($team_record->first_name) . ' ' . $team_record->middle_name }}
                                </p>
                            </td>
                            <td>
                                {{ $team_record->employee_number }}
                            </td>
                            <td>
                                {{ $team_record->position->display }}
                            </td>
                            <td <?php echo $team_record->timeLog[0]->late ?? '' > 0 ? "class='text-red-600'" : ''; ?> }}>
                                {{ $team_record->timeLog[0]->time_in ?? '' }}
                            </td>
                            <td>
                                {{ $team_record->timeLog[0]->time_out ?? '' }}
                            </td>
                            <td>
                                <div class="flex ml-6">
                                    <svg wire:click="action({'id': {{ $team_record->id }}}, 'view')"
                                        class="w-4 h-4 cursor-pointer text-gray-400 hover:text-[#003399] "
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 512">
                                        <path fill="currentColor"
                                            d="M64 360C94.93 360 120 385.1 120 416C120 446.9 94.93 472 64 472C33.07 472 8 446.9 8 416C8 385.1 33.07 360 64 360zM64 200C94.93 200 120 225.1 120 256C120 286.9 94.93 312 64 312C33.07 312 8 286.9 8 256C8 225.1 33.07 200 64 200zM64 152C33.07 152 8 126.9 8 96C8 65.07 33.07 40 64 40C94.93 40 120 65.07 120 96C120 126.9 94.93 152 64 152z">
                                        </path>
                                    </svg>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </x-slot>
            </x-table.table>
        </div>
    </x-slot>
</x-form>
