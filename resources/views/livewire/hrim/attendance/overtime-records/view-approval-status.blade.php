<div>
    <x-table.table>
        <x-slot name="thead">
            <x-table.th name="Approvers" />
            <x-table.th name="Status" />
            <x-table.th name="Reason for decline" />
        </x-slot>
        <x-slot name="tbody">
            @if ($overtime_record->firstApprover)
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        {{ $overtime_record->firstApprover->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if (is_null($overtime_record->first_status))
                            <span class="text-requested">Requested</span>
                        @elseif($overtime_record->first_status === 0)
                            <span class="text-declined">Declined</span>
                        @elseif($overtime_record->first_status === 1)
                            <span class="text-approved">Approved</span>
                        @endif
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if ($overtime_record->first_status === 0)
                            {{ $overtime_record->first_approver_remarks }}
                        @endif
                    </td>
                </tr>
            @endif
            @if ($overtime_record->secondApprover)
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        {{ $overtime_record->secondApprover->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if (is_null($overtime_record->second_status))
                            <span class="text-requested">Requested</span>
                        @elseif($overtime_record->second_status === 0)
                            <span class="text-declined">Declined</span>
                        @elseif($overtime_record->second_status === 1)
                            <span class="text-approved">Approved</span>
                        @endif
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if ($overtime_record->second_status === 0)
                            {{ $overtime_record->second_approver_remarks }}
                        @endif
                    </td>
                </tr>
            @endif
            @if ($overtime_record->thirdApprover)
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        {{ $overtime_record->thirdApprover->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if (is_null($overtime_record->third_status))
                            <span class="text-requested">Requested</span>
                        @elseif($overtime_record->third_status === 0)
                            <span class="text-declined">Declined</span>
                        @elseif($overtime_record->third_status === 1)
                            <span class="text-approved">Approved</span>
                        @endif
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if ($overtime_record->third_status === 0)
                            {{ $overtime_record->third_approver_remarks }}
                        @endif
                    </td>
                </tr>
            @endif
            @if ($overtime_record->adminApprover)
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        {{ $overtime_record->adminApprover->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if ($overtime_record->final_status_id === 3)
                            <span class="text-approved">Approved</span>
                        @elseif($overtime_record->final_status_id === 4)
                            <span class="text-declined">Declined</span>
                        @endif
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if ($overtime_record->final_status_id === 4)
                            {{ $overtime_record->admin_approver_remarks }}
                        @endif
                    </td>
                </tr>
            @endif
        </x-slot>
    </x-table.table>
</div>
