<x-form wire:init="load" x-data="{ search_form: true, status: '{{ $status }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="header_title">Overtime Summary</x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-6 gap-6">
            <x-transparent.select label="Month" name="month" wire:model="month">
                @foreach ($month_references as $month_reference)
                    <option value="{{ $month_reference->id }}">
                        {{ $month_reference->display }}
                    </option>
                @endforeach
            </x-transparent.select>
            <x-transparent.select label="Cut Off" name="cut_off" wire:model="cut_off">
                <option value="first">First</option>
                <option value="second">Second</option>
            </x-transparent.select>
            <x-transparent.input type="number" label="Year" name="year" wire:model.debounce.500ms="year" />
            <x-transparent.select label="Branch" name="branch" wire:model="branch">
                <option value="">All</option>
                @foreach ($branch_references as $branch_reference)
                    <option value="{{ $branch_reference->id }}">
                        {{ $branch_reference->display }}
                    </option>
                @endforeach
            </x-transparent.select>
            <x-transparent.select label="Employment Category" name="employment_category"
                wire:model="employment_category">
                <option value="">All</option>
                @foreach ($employment_category_references as $employment_category_reference)
                    <option value="{{ $employment_category_reference->id }}">
                        {{ $employment_category_reference->display }}
                    </option>
                @endforeach
            </x-transparent.select>
            {{-- @if ($employment_category == 2)
                <x-transparent.select label="Employment Category Type" name="employment_category_type"
                    wire:model="employment_category_type">
                    <option value=""></option>
                    @foreach ($employment_category_type_references as $employment_category_type_reference)
                        <option value="{{ $employment_category_type_reference->id }}">
                            {{ $employment_category_type_reference->display }}
                        </option>
                    @endforeach
                </x-transparent.select>
            @endif --}}
        </div>
        <div class="grid grid-cols-6 gap-6">
            <x-transparent.input type="text" label="Employee ID" name="employee_id"
                wire:model.debounce.500ms="employee_id" />
            <x-transparent.input type="text" label="Employee Name" name="employee_name"
                wire:model.debounce.500ms="employee_name" />
        </div>
        
        <div class="grid grid-cols-2 gap-6 md:grid-cols-5 lg:grid-cols-5">
            <div
                class="flex flex-col items-center justify-center px-3 py-4 text-gray-700 bg-white border border-blue-800 border-solid rounded-md ">
                <div class="flex items-center justify-between w-full">
                    <span class="text-xs font-medium uppercase ">Total No. Of Filed Overtime</span>
                    <span class="text-2xl font-semibold text-right ">{{ $users->count() }}</span>
                </div>
            </div>
            <div
                class="flex flex-col items-center justify-center px-3 py-4 text-gray-700 bg-white border border-blue-800 border-solid rounded-md ">
                <div class="flex items-center justify-between w-full">
                    <span class="text-xs font-medium uppercase ">Total Filed Overtime Hours</span>
                    <span
                        class="text-2xl font-semibold text-right ">{{ number_format($users->sum('total_filed_ot_hours'), 1) }}</span>
                </div>
            </div>
            <div
                class="flex flex-col items-center justify-center px-3 py-4 text-gray-700 bg-white border border-blue-800 border-solid rounded-md ">
                <div class="flex items-center justify-between w-full">
                    <span class="text-xs font-medium uppercase ">Total Approved Overtime Hours</span>
                    <span
                        class="text-2xl font-semibold text-right ">{{ number_format($users->sum('total_approved_ot_hours'), 1) }}</span>
                </div>
            </div>
        </div>
        <div>
            <div class="flex items-center justify-start ">
                <div class="flex items-center justify-between px-3 py-2 text-sm border border-gray-400 "
                    :class="status == 'all' ? 'bg-blue text-white' : ''" wire:click="$set('status', 'all')">
                    <span>All</span>
                    <span class="ml-10">{{ $users_overtime_records_all }}</span>
                </div>
                <div class="flex items-center justify-between px-3 py-2 text-sm bg-white border border-gray-400"
                    :class="status == 'for_approval' ? 'bg-blue text-white' : ''"
                    wire:click="$set('status', 'for_approval')">
                    <span>For Approval</span>
                    <span class="ml-10">{{ $users_overtime_records_for_approval }}</span>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>

                    <x-slot name="thead">
                        <x-table.th name="No." />
                        <x-table.th name="Name" />
                        <x-table.th name="Employee ID" />
                        <x-table.th name="Employment Category" />
                        <x-table.th name="Branch" />
                        <x-table.th name="Payout Date" />

                        <x-table.th name="Total Filed OT Hours" />
                        <x-table.th name="Total Approved OT Hours" />

                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($users as $i => $user)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $i + 1 }}
                                </td>
                                <td class="flex items-center justify-start px-3 py-1 space-x-3 whitespace-nowrap">
                                    <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                        style="background-image: url({{ Storage::disk('hrim_gcs')->url($user->photo_path . $user->photo_name) }})">
                                    </div>
                                    <p>{{ $user->name }}</p>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $user->userDetails->employee_number }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $user->userDetails->employmentCategory->display ?? 'N/A' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $user->userDetails->branch->display ?? 'N/A' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('F', strtotime($year . '-' . $month . '-' . '1')) }}
                                    <span class="capitalize">{{ $cut_off . ' off' . ' ' . $year }}
                                    </span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($user->total_filed_ot_hours, 1) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ number_format($user->total_approved_ot_hours, 1) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex items-center justify-center space-x-3">
                                        <a
                                            href="{{ route('hrim.attendance.overtime-records.view', ['id' => $user->id]) }}">
                                            <svg class="w-5 h-5 text-blue" data-toggle="tooltip" title="View"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                                <path fill="currentColor"
                                                    d="M256 0v128h128L256 0zM224 128L224 0H48C21.49 0 0 21.49 0 48v416C0 490.5 21.49 512 48 512h288c26.51 0 48-21.49 48-48V160h-127.1C238.3 160 224 145.7 224 128zM272 416h-160C103.2 416 96 408.8 96 400C96 391.2 103.2 384 112 384h160c8.836 0 16 7.162 16 16C288 408.8 280.8 416 272 416zM272 352h-160C103.2 352 96 344.8 96 336C96 327.2 103.2 320 112 320h160c8.836 0 16 7.162 16 16C288 344.8 280.8 352 272 352zM288 272C288 280.8 280.8 288 272 288h-160C103.2 288 96 280.8 96 272C96 263.2 103.2 256 112 256h160C280.8 256 288 263.2 288 272z" />
                                            </svg>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $users ? $users->links() : '' }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
