<div>
    <x-table.table>
        <x-slot name="thead">
            <x-table.th name="Approvers" />
            <x-table.th name="Status" />
            <x-table.th name="Reason for decline" />
        </x-slot>
        <x-slot name="tbody">
            @if ($tar->firstApprover)
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        {{ $tar->firstApprover->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if (is_null($tar->first_status))
                            <span class="text-requested">Requested</span>
                        @elseif($tar->first_status === 0)
                            <span class="text-declined">Declined</span>
                        @elseif($tar->first_status === 1)
                            <span class="text-approved">Approved</span>
                        @endif
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        {{ $tar->first_approver_remarks }}
                    </td>
                </tr>
            @endif
            @if ($tar->secondApprover)
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        {{ $tar->secondApprover->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if (is_null($tar->second_status))
                            <span class="text-requested">Requested</span>
                        @elseif($tar->second_status === 0)
                            <span class="text-declined">Declined</span>
                        @elseif($tar->second_status === 1)
                            <span class="text-approved">Approved</span>
                        @endif
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        {{ $tar->second_approver_remarks }}
                    </td>
                </tr>
            @endif
            @if ($tar->thirdApprover)
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        {{ $tar->thirdApprover->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if (is_null($tar->third_status))
                            <span class="text-requested">Requested</span>
                        @elseif($tar->third_status === 0)
                            <span class="text-declined">Declined</span>
                        @elseif($tar->third_status === 1)
                            <span class="text-approved">Approved</span>
                        @endif
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        {{ $tar->third_approver_remarks }}
                    </td>
                </tr>
            @endif
            @if ($tar->adminApprover)
                <td class="p-3 whitespace-nowrap">
                    {{ $tar->adminApprover->name }}
                </td>
                <td class="p-3 whitespace-nowrap">
                    @if ($tar->final_status_id === 3)
                        <span class="text-approved">Approved</span>
                    @elseif($tar->final_status_id === 4)
                        <span class="text-declined">Declined</span>
                    @endif
                </td>
                <td class="p-3 whitespace-nowrap">
                    {{ $tar->admin_approver_remarks }}
                </td>
            @endif
        </x-slot>
    </x-table.table>
</div>
