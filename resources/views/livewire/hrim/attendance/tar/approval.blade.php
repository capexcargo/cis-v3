<div wire:init="load">
    <x-loading />
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-6">
            <div class="grid grid-cols-2 gap-6">
                <div class="col-span-2">
                    <x-label for="date" value="Date" :required="true" />
                    <x-input type="date" name="date" wire:model.defer='date' disabled />
                    <x-input-error for="date" />
                </div>
                <div>
                    <x-label for="request_time_in" value="Requested Time In" :required="true" />
                    <x-input type="time" name="request_time_in" wire:model.defer='request_time_in' disabled />
                    <x-input-error for="request_time_in" />
                </div>
                <div>
                    <x-label for="request_time_out" value="Requested Time Out" :required="true" />
                    <x-input type="time" name="request_time_out" wire:model.defer='request_time_out' disabled />
                    <x-input-error for="request_time_out" />
                </div>
                <div class="col-span-2">
                    <x-label for="reason_of_adjustment" value="Reason of Adjustment" :required="true" />
                    <x-select name="reason_of_adjustment" wire:model.defer='reason_of_adjustment' disabled>
                        <option value=""></option>
                        @foreach ($tar_reason_references as $tar_reason_reference)
                            <option value="{{ $tar_reason_reference->id }}">
                                {{ $tar_reason_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="reason_of_adjustment" />
                </div>
            </div>

            {{-- <div class="grid grid-cols-3 gap-3">
                <div class="space-y-3">
                    <div>
                        <div>
                            <x-label value="First Approver" :required="true" />
                            <x-input type="text" name="first_approver_id" value="{{ $tar->firstApprover->name }}"
                                disabled>
                            </x-input>
                            <x-input-error for="first_approver_id" />
                        </div>
                    </div>
                    @if ($tar->first_approver == auth()->user()->id || (auth()->user()->level_id == 5 && $tar->first_approver))
                        <div class="flex justify-between">
                            <x-input-radio label="Approve" name="is_first_approved" wire:model.defer="is_first_approved"
                                value="1" />
                            <x-input-radio label="Dispprove" name="is_first_approved"
                                wire:model.defer="is_first_approved" value="0" />
                        </div>
                        <div>
                            <x-label value="Remarks" />
                            <x-textarea type="text" name="first_approver_remarks"
                                wire:model.defer='first_approver_remarks' />
                            <x-input-error for="first_approver_remarks" />
                        </div>
                    @endif
                </div>
                <div class="space-y-3">
                    @if ($tar->second_approver == auth()->user()->id || (auth()->user()->level_id == 5 && $tar->second_approver))
                        <div>
                            <div>
                                <x-label value="Second Approver" :required="true" />
                                <x-input type="text" name="second_approver_id"
                                    value="{{ $tar->secondApprover->name }}" disabled>
                                </x-input>
                                <x-input-error for="second_approver_id" />
                            </div>
                        </div>
                        <div class="flex justify-between">
                            <x-input-radio label="Approve" name="is_second_approved"
                                wire:model.defer="is_second_approved" value="1" />
                            <x-input-radio label="Dispprove" name="is_second_approved"
                                wire:model.defer="is_second_approved" value="0" />
                        </div>
                        <div>
                            <x-label value="Remarks" />
                            <x-textarea type="text" name="second_approver_remarks"
                                wire:model.defer='second_approver_remarks' />
                            <x-input-error for="second_approver_remarks" />
                        </div>
                    @endif
                </div>
                <div class="space-y-3">
                    @if ($tar->third_approver == auth()->user()->id || (auth()->user()->level_id == 5 && $tar->third_approver))
                        <div>
                            <div>
                                <x-label value="Third Approver" :required="true" />
                                <x-input type="text" name="third_approver_id"
                                    value="{{ $tar->thirdApprover->name }}" disabled>
                                </x-input>
                                <x-input-error for="third_approver_id" />
                            </div>
                        </div>
                        <div class="flex justify-between">
                            <x-input-radio label="Approve" name="is_third_approved" wire:model.defer="is_third_approved"
                                value="1" />
                            <x-input-radio label="Dispprove" name="is_third_approved"
                                wire:model.defer="is_third_approved" value="0" />
                        </div>
                        <div>
                            <x-label value="Remarks" />
                            <x-textarea type="text" name="third_approver_remarks"
                                wire:model.defer='third_approver_remarks' />
                            <x-input-error for="third_approver_remarks" />
                        </div>
                    @endif
                </div>
            </div> --}}
            <div class="flex justify-end space-x-3">
                <x-button type="button" wire:click="$emit('close_modal', 'approval')" title="Cancel"
                    class="bg-white text-blue hover:bg-gray-100" />
                {{-- @if ($tar->final_status_id != 3)
                    <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
                @endif --}}
            </div>
        </div>
    </form>
</div>
