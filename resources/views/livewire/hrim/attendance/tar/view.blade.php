<x-form x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
    approval_modal: '{{ $approval_modal }}',
    view_approval_status_modal: '{{ $view_approval_status_modal }}'
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                <div class="space-y-3">
                    <p class="text-left">{{ $confirmation_message }}</p>
                    <x-input-error for="approver" />
                    @if ($action_type == 'decline')
                        <div class="grid grid-cols-12">
                            <div class="col-span-3">
                                <x-label value="Remarks" :required="true" />
                            </div>
                            <div class="col-span-9">
                                <x-textarea name="remarks" wire:model.debounce.500ms="remarks"></x-textarea>
                            </div>
                            <div class="col-span-12">
                                <x-input-error for="remarks" />
                            </div>
                        </div>
                        <div class="flex items-end justify-end space-x-3">
                            <x-button type="button" wire:click="confirm" title="Decline"
                                class="border-0 bg-red text-white hover:bg-[#7c1818] py-1" />
                        </div>
                    @else
                        <div class="flex items-center justify-center space-x-3">
                            <x-button type="button" wire:click="$set('confirmation_modal', false)" title="No"
                                class="py-1 bg-white text-blue hover:bg-gray-100" />
                            <x-button type="button" wire:click="confirm" title="Yes"
                                class="bg-blue text-white hover:bg-[#002161] py-1" />
                        </div>
                    @endif
                </div>
            </x-slot>
        </x-modal>
        @if ($approval_modal && $tar_id)
            <x-modal id="approval_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="title">Approve Time Adjustment Request</x-slot>
                <x-slot name="body">
                    @livewire('hrim.attendance.tar.approval', ['id' => $tar_id, 'user_id' => $user_id])
                </x-slot>
            </x-modal>
        @endif
        @if ($view_approval_status_modal && $tar_id)
            <x-modal id="view_approval_status_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="body">
                    @livewire('hrim.attendance.tar.view-approval-status', ['id' => $tar_id, 'user_id' => $user_id])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">
        <div class="flex items-center justify-start space-x-3">
            <a href="{{ route('hrim.attendance.schedules.index') }}">
                <svg class="w-8 h-8 text-blue" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path fill="currentColor"
                        d="M447.1 256C447.1 273.7 433.7 288 416 288H109.3l105.4 105.4c12.5 12.5 12.5 32.75 0 45.25C208.4 444.9 200.2 448 192 448s-16.38-3.125-22.62-9.375l-160-160c-12.5-12.5-12.5-32.75 0-45.25l160-160c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25L109.3 224H416C433.7 224 447.1 238.3 447.1 256z" />
                </svg>
            </a>
            <span>{{ $user->name }}'s Time Adjustment Request Summary</span>
        </div>
    </x-slot>
    <x-slot name="body">
        <div class="bg-white rounded-lg shadow-md">
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="No" />
                    <x-table.th name="Date From" />
                    <x-table.th name="Date To" />
                    <x-table.th name="Day" />
                    <x-table.th name="Actual Time In" />
                    <x-table.th name="Actual Time Out" />
                    <x-table.th name="Requested Time In" />
                    <x-table.th name="Requested Time Out" />
                    <x-table.th name="Total Hours Rendered" />
                    <x-table.th name="Work Mode" />
                    <x-table.th name="Reason" />
                    <x-table.th name="Status" />
                    <x-table.th name="1st Approver" />
                    <x-table.th name="1st Approver Remarks" />
                    <x-table.th name="2nd Approver" />
                    <x-table.th name="2nd Approver Remarks" />
                    <x-table.th name="3rd Approver" />
                    <x-table.th name="3rd Approver Remarks" />
                    <x-table.th name="By-Pass Approver" />
                    <x-table.th name="By-Pass Approver Remarks" />
                    <x-table.th name="Action" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($tars as $i => $tar)
                        <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                            <td class="p-3 whitespace-nowrap">
                                {{ $i + 1 }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ date('M. d, Y', strtotime($tar->date_from)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ date('M. d, Y', strtotime($tar->date_to)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ date('l', strtotime($tar->date_from)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ ($tar->timeLogs->time_in ?? '') == '' ? '-' : date('h:i A', strtotime($tar->timeLogs->time_in)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ ($tar->timeLogs->time_out ?? '') == '' ? '-' : date('h:i A', strtotime($tar->timeLogs->time_out)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ date('h:i A', strtotime($tar->actual_time_in)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ date('h:i A', strtotime($tar->actual_time_out)) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ ($tar->timeLogs->rendered_time ?? '') == '' ? '-' : $tar->timeLogs->rendered_time }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ ($tar->timeLogs->workSchedule ?? '') == '' ? '-' : $tar->timeLogs->workSchedule->workMode->display }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $tar->tarReason->display }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <span
                                    class="px-6 py-1 text-xs rounded-full {{ $tar->finalStatus->code }}">{{ $tar->finalStatus->display }}</span>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                @if (!$tar->admin_approver)
                                    @if (
                                        ($tar->first_approver && $tar->first_approver == Auth::user()->id) ||
                                            (Auth::user()->level_id == 5 && $tar->first_approver))
                                        <div class="flex items-center justify-center space-x-3">
                                            <svg wire:click="action({id: '{{ $tar->id }}', approver: 'first'}, 'approve')"
                                                class="w-5 h-5 text-blue" data-toggle="tooltip" title="Approved"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM371.8 211.8C382.7 200.9 382.7 183.1 371.8 172.2C360.9 161.3 343.1 161.3 332.2 172.2L224 280.4L179.8 236.2C168.9 225.3 151.1 225.3 140.2 236.2C129.3 247.1 129.3 264.9 140.2 275.8L204.2 339.8C215.1 350.7 232.9 350.7 243.8 339.8L371.8 211.8z" />
                                            </svg>
                                            <svg wire:click="action({id: '{{ $tar->id }}', approver: 'first'}, 'decline')"
                                                class="w-5 h-5 text-red" data-toggle="tooltip" title="Declined"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
                                            </svg>
                                        </div>
                                    @endif
                                @endif
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $tar->first_approver_remarks }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                @if (!$tar->admin_approver)
                                    @if (
                                        ($tar->second_approver && $tar->second_approver == Auth::user()->id) ||
                                            (Auth::user()->level_id == 5 && $tar->second_approver))
                                        <div class="flex items-center justify-center space-x-3">
                                            <svg wire:click="action({id: '{{ $tar->id }}', approver: 'second'}, 'approve')"
                                                class="w-5 h-5 text-blue" data-toggle="tooltip" title="Approved"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM371.8 211.8C382.7 200.9 382.7 183.1 371.8 172.2C360.9 161.3 343.1 161.3 332.2 172.2L224 280.4L179.8 236.2C168.9 225.3 151.1 225.3 140.2 236.2C129.3 247.1 129.3 264.9 140.2 275.8L204.2 339.8C215.1 350.7 232.9 350.7 243.8 339.8L371.8 211.8z" />
                                            </svg>
                                            <svg wire:click="action({id: '{{ $tar->id }}', approver: 'second'}, 'decline')"
                                                class="w-5 h-5 text-red" data-toggle="tooltip" title="Declined"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
                                            </svg>
                                        </div>
                                    @endif
                                @endif
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $tar->second_approver_remarks }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                @if (!$tar->admin_approver)
                                    @if (
                                        ($tar->third_approver && $tar->third_approver == Auth::user()->id) ||
                                            (Auth::user()->level_id == 5 && $tar->third_approver))
                                        <div class="flex items-center justify-center space-x-3">
                                            <svg wire:click="action({id: '{{ $tar->id }}', approver: 'third'}, 'approve')"
                                                class="w-5 h-5 text-blue" data-toggle="tooltip" title="Approved"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM371.8 211.8C382.7 200.9 382.7 183.1 371.8 172.2C360.9 161.3 343.1 161.3 332.2 172.2L224 280.4L179.8 236.2C168.9 225.3 151.1 225.3 140.2 236.2C129.3 247.1 129.3 264.9 140.2 275.8L204.2 339.8C215.1 350.7 232.9 350.7 243.8 339.8L371.8 211.8z" />
                                            </svg>
                                            <svg wire:click="action({id: '{{ $tar->id }}', approver: 'third'}, 'decline')"
                                                class="w-5 h-5 text-red" data-toggle="tooltip" title="Declined"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
                                            </svg>
                                        </div>
                                    @endif
                                @endif
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $tar->third_approver_remarks }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                @if ($divsionhead_id == Auth::user()->id && in_array($tar->admin_approver, $seg_id) == false)
                                    <div class="flex items-center justify-center space-x-3">
                                        <svg wire:click="action({id: '{{ $tar->id }}', approver: 'bypass'}, 'approve')"
                                            class="w-5 h-5 text-blue" data-toggle="tooltip" title="Approved"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                            <path fill="currentColor"
                                                d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM371.8 211.8C382.7 200.9 382.7 183.1 371.8 172.2C360.9 161.3 343.1 161.3 332.2 172.2L224 280.4L179.8 236.2C168.9 225.3 151.1 225.3 140.2 236.2C129.3 247.1 129.3 264.9 140.2 275.8L204.2 339.8C215.1 350.7 232.9 350.7 243.8 339.8L371.8 211.8z" />
                                        </svg>
                                        <svg wire:click="action({id: '{{ $tar->id }}', approver: 'bypass'}, 'decline')"
                                            class="w-5 h-5 text-red" data-toggle="tooltip" title="Declined"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                            <path fill="currentColor"
                                                d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
                                        </svg>
                                    </div>
                                @elseif(in_array(Auth::user()->id, $seg_id))
                                    <div class="flex items-center justify-center space-x-3">
                                        <div class="flex items-center justify-center space-x-3">
                                            <svg wire:click="action({id: '{{ $tar->id }}', approver: 'bypass'}, 'approve')"
                                                class="w-5 h-5 text-blue" data-toggle="tooltip" title="Approved"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM371.8 211.8C382.7 200.9 382.7 183.1 371.8 172.2C360.9 161.3 343.1 161.3 332.2 172.2L224 280.4L179.8 236.2C168.9 225.3 151.1 225.3 140.2 236.2C129.3 247.1 129.3 264.9 140.2 275.8L204.2 339.8C215.1 350.7 232.9 350.7 243.8 339.8L371.8 211.8z" />
                                            </svg>
                                            <svg wire:click="action({id: '{{ $tar->id }}', approver: 'bypass'}, 'decline')"
                                                class="w-5 h-5 text-red" data-toggle="tooltip" title="Declined"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
                                            </svg>
                                        </div>

                                    </div>
                                @endif
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $tar->admin_approver_remarks }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-center space-x-3">
                                    <svg wire:click="action({id: '{{ $tar->id }}'}, 'approval')"
                                        class="w-5 h-5 text-blue" data-toggle="tooltip" title="View"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                    </svg>
                                    <svg wire:click="action({id: '{{ $tar->id }}'}, 'view_approval_status')"
                                        class="w-5 h-5 text-blue" data-toggle="tooltip" title="View Approval Status"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 512">
                                        <path fill="currentColor"
                                            d="M64 360C94.93 360 120 385.1 120 416C120 446.9 94.93 472 64 472C33.07 472 8 446.9 8 416C8 385.1 33.07 360 64 360zM64 200C94.93 200 120 225.1 120 256C120 286.9 94.93 312 64 312C33.07 312 8 286.9 8 256C8 225.1 33.07 200 64 200zM64 152C33.07 152 8 126.9 8 96C8 65.07 33.07 40 64 40C94.93 40 120 65.07 120 96C120 126.9 94.93 152 64 152z" />
                                    </svg>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </x-slot>
            </x-table.table>
            <div class="px-1 pb-2">
                {{ $tars ? $tars->links() : '' }}
            </div>
        </div>
    </x-slot>
</x-form>
