<x-form wire:init="load" x-data="{ search_form: true }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="header_title">Daily Time Records,
        <i class="font-normal text-blue">{{ date('F d, Y l', strtotime(now())) }}</i>
    </x-slot>
    <x-slot name="search_form">
        <x-transparent.input type="date" label="Date" name="date" wire:model.debounce.500ms="date" />
        <x-transparent.select label="Branch" name="branch" wire:model="branch">
            <option value="">All</option>
            @foreach ($branch_references as $branch_reference)
                <option value="{{ $branch_reference->id }}">
                    {{ $branch_reference->display }}
                </option>
            @endforeach
        </x-transparent.select>
        <x-transparent.select label="Employment Category" name="employment_category" wire:model="employment_category">
            <option value="">All</option>
            @foreach ($employment_category_references as $employment_category_reference)
                <option value="{{ $employment_category_reference->id }}">
                    {{ $employment_category_reference->display }}
                </option>
            @endforeach
        </x-transparent.select>
        <x-transparent.input type="text" label="Employee ID" name="employee_id"
            wire:model.debounce.500ms="employee_id" />
        <x-transparent.input type="text" label="Employee Name" name="employee_name"
            wire:model.debounce.500ms="employee_name" />
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="No." />
                        <x-table.th name="Employee Name" />
                        <x-table.th name="Employment Category" />
                        <x-table.th name="Date" />
                        <x-table.th name="Day" />
                        <x-table.th name="Time In" />
                        <x-table.th name="Time Out" />
                        <x-table.th name="Late" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($time_logs as $i => $time_log)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $i + 1 }}
                                </td>
                                <td class="flex items-center justify-start px-3 py-1 space-x-3 whitespace-nowrap">
                                    <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                    style="background-image: url({{ Storage::disk('hrim_gcs')->url($time_log->user->photo_path . $time_log->user->photo_name) }})">
                                </div>
                                <p>
                                    {{ $time_log->user->name }}
                                </p>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $time_log->user->userDetails->employmentCategory->display }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($time_log->date)
                                        {{ date('F d, Y', strtotime($time_log->date)) }}
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($time_log->date)
                                        {{ date('l', strtotime($time_log->date)) }}
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($time_log->time_in)
                                        {{ date('h:i A', strtotime($time_log->time_in)) }}
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($time_log->time_out != "00:00:00" && $time_log->time_out != null)
                                        {{ date('h:i A', strtotime($time_log->time_out)) }}
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($time_log->late)
                                        {{ $time_log->late }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $time_logs->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
