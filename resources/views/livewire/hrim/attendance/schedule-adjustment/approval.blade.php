<div wire:init="load">
    <x-loading />
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-6">
            <div class="grid grid-cols-2 gap-6">
                <div x-data="{ open: false }" class="relative mb-2 rounded-md" @click.away="open = false">
                    <div>
                        <x-label value="Employee Name" :required="true" />
                        <x-input type="text" @click="open = !open" name="employee_name"
                            wire:model.debounce.500ms='employee_name' disabled />
                        <x-input-error for="employee_name" />
                    </div>
                    <div x-show="open" x-cloak
                        class="absolute w-full p-2 my-1 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96">
                        <ul class="list-reset">
                            @forelse ($employee_references as $i => $employee_reference)
                                <li @click="open = !open"
                                    wire:click="getEmployeeInformation({{ $employee_reference->id }})"
                                    wire:key="{{ $i }}"
                                    class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                    <p>
                                        {{ $employee_reference->name }}
                                    </p>
                                </li>
                            @empty
                                <li>
                                    <p class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                        No Employee Found.
                                    </p>
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>

            <div class="grid grid-cols-2 divide-x-2 divide-dashed">
                <div>
                    <div class="grid grid-cols-2 gap-6 pr-3">
                        <div class="col-span-2">
                            <x-label value="Current Schedule" :required="true" />
                            <x-select name="current_schedule" wire:model='current_schedule' disabled>
                                <option value=""></option>
                                @foreach ($work_schedule_references as $work_schedule_reference)
                                    <option value="{{ $work_schedule_reference->id }}">
                                        {{ $work_schedule_reference->name }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="current_schedule" />
                        </div>
                        <div class="col-span-2">
                            <x-label value="Days" :required="true" />
                            <x-input type="text" name="current_days" wire:model.defer='current_days' disabled />
                            <x-input-error for="current_days" />
                        </div>
                        <div class="col-span-2 -mb-3">
                            <p class="text-sm font-semibold">Time</p>
                        </div>
                        <div>
                            <x-label value="From" :required="true" />
                            <x-input type="text" name="current_time_from" wire:model.defer='current_time_from'
                                disabled />
                            <x-input-error for="current_time_from" />
                        </div>
                        <div>
                            <x-label value="To" :required="true" />
                            <x-input type="text" name="current_time_to" wire:model.defer='current_time_to'
                                disabled />
                            <x-input-error for="current_time_to" />
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-6 pl-3">
                    <div class="col-span-2">
                        <x-label value="New Schedule" :required="true" />
                        <x-select name="new_schedule" wire:model='new_schedule' disabled>
                            <option value=""></option>
                            @foreach ($work_schedule_references as $work_schedule_reference)
                                <option value="{{ $work_schedule_reference->id }}">
                                    {{ $work_schedule_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="new_schedule" />
                    </div>
                    <div class="col-span-2">
                        <x-label value="Days" :required="true" />
                        <x-input type="text" name="new_days" wire:model.defer='new_days' disabled />
                        <x-input-error for="new_days" />
                    </div>
                    <div class="col-span-2 -mb-3">
                        <p class="text-sm font-semibold">Time</p>
                    </div>
                    <div>
                        <x-label value="From" :required="true" />
                        <x-input type="text" name="new_time_from" wire:model.defer='new_time_from' disabled />
                        <x-input-error for="new_time_from" />
                    </div>
                    <div>
                        <x-label value="To" :required="true" />
                        <x-input type="text" name="new_time_to" wire:model.defer='new_time_to' disabled />
                        <x-input-error for="new_time_to" />
                    </div>
                    <div class="col-span-2">
                        <x-label value="Work Mode" :required="true" />
                        <x-input type="text" name="new_work_mode" wire:model.defer='new_work_mode' disabled />
                        <x-input-error for="new_work_mode" />
                    </div>
                    <div class="col-span-2 -mb-3">
                        <p class="text-sm font-semibold">Inclusive Dates <span class="text-red">*</span></p>
                    </div>
                    <div>
                        <x-label value="Start Date" :required="true" />
                        <x-input type="date" name="new_inclusive_date_start_date"
                            wire:model.defer='new_inclusive_date_start_date' disabled />
                        <x-input-error for="new_inclusive_date_start_date" />
                    </div>
                    @if ($set_an_end_date)
                        <div>
                            <x-label value="End Date" :required="true" />
                            <x-input type="date" name="new_inclusive_date_end_date"
                                wire:model.defer='new_inclusive_date_end_date' disabled />
                            <x-input-error for="new_inclusive_date_end_date" />
                        </div>
                    @endif

                    <div class="col-span-2 -mt-3">
                        <x-input-checkbox label="Set an end date" name="set_an_end_date" value="1"
                            wire:model="set_an_end_date" disabled />
                    </div>
                    <div class="col-span-2">
                        <x-label value="Reason" :required="true" />
                        <x-textarea name="reason" wire:model.defer='reason' disabled />
                        <x-input-error for="reason" />
                    </div>
                </div>
            </div>
            <div class="grid grid-cols-3 gap-3">
                <div class="space-y-3">
                    <div>
                        <div>
                            <x-label value="First Approver" :required="true" />
                            <x-input type="text" name="first_approver_id"
                                value="{{ $schedule_adjustment->firstApprover->name }}" disabled>
                            </x-input>
                            <x-input-error for="first_approver_id" />
                        </div>
                    </div>
                    @if ($schedule_adjustment->first_approver == auth()->user()->id || (auth()->user()->level_id == 5 && $schedule_adjustment->first_approver))
                        <div class="flex justify-between">
                            <x-input-radio label="Approve" name="is_first_approved"
                                wire:model.defer="is_first_approved" value="1" />
                            <x-input-radio label="Dispprove" name="is_first_approved"
                                wire:model.defer="is_first_approved" value="0" />
                        </div>
                        <div>
                            <x-label value="Remarks" />
                            <x-textarea type="text" name="first_approver_remarks"
                                wire:model.defer='first_approver_remarks' />
                            <x-input-error for="first_approver_remarks" />
                        </div>
                    @endif
                </div>
                <div class="space-y-3">
                    @if ($schedule_adjustment->second_approver == auth()->user()->id || (auth()->user()->level_id == 5 && $schedule_adjustment->second_approver))
                        <div>
                            <div>
                                <x-label value="Second Approver" :required="true" />
                                <x-input type="text" name="second_approver_id"
                                    value="{{ $schedule_adjustment->secondApprover->name }}" disabled>
                                </x-input>
                                <x-input-error for="second_approver_id" />
                            </div>
                        </div>
                        <div class="flex justify-between">
                            <x-input-radio label="Approve" name="is_second_approved"
                                wire:model.defer="is_second_approved" value="1" />
                            <x-input-radio label="Dispprove" name="is_second_approved"
                                wire:model.defer="is_second_approved" value="0" />
                        </div>
                        <div>
                            <x-label value="Remarks" />
                            <x-textarea type="text" name="second_approver_remarks"
                                wire:model.defer='second_approver_remarks' />
                            <x-input-error for="second_approver_remarks" />
                        </div>
                    @endif
                </div>
                <div class="space-y-3">
                    @if ($schedule_adjustment->third_approver == auth()->user()->id || (auth()->user()->level_id == 5 && $schedule_adjustment->third_approver))
                        <div>
                            <div>
                                <x-label value="Third Approver" :required="true" />
                                <x-input type="text" name="third_approver_id"
                                    value="{{ $schedule_adjustment->thirdApprover->name }}" disabled>
                                </x-input>
                                <x-input-error for="third_approver_id" />
                            </div>
                        </div>
                        <div class="flex justify-between">
                            <x-input-radio label="Approve" name="is_third_approved"
                                wire:model.defer="is_third_approved" value="1" />
                            <x-input-radio label="Dispprove" name="is_third_approved"
                                wire:model.defer="is_third_approved" value="0" />
                        </div>
                        <div>
                            <x-label value="Remarks" />
                            <x-textarea type="text" name="third_approver_remarks"
                                wire:model.defer='third_approver_remarks' />
                            <x-input-error for="third_approver_remarks" />
                        </div>
                    @endif
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <x-button type="button" wire:click="$emit('close_modal', 'approval')" title="Cancel"
                    class="bg-white text-blue hover:bg-gray-100" />
                @if ($schedule_adjustment->final_status_id != 3)
                    <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
                @endif
            </div>
        </div>
    </form>
</div>
