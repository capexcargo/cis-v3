<div>
    <x-table.table>
        <x-slot name="thead">
            <x-table.th name="Approvers" />
            <x-table.th name="Status" />
        </x-slot>
        <x-slot name="tbody">
            @if ($schedule_adjustment->firstApprover)
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        {{ $schedule_adjustment->firstApprover->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if (is_null($schedule_adjustment->first_status))
                            <span class="text-requested">Requested</span>
                        @elseif($schedule_adjustment->first_status === 0)
                            <span class="text-declined">Declined</span>
                        @elseif($schedule_adjustment->first_status === 1)
                            <span class="text-approved">Approved</span>
                        @endif
                    </td>
                </tr>
            @endif
            @if ($schedule_adjustment->secondApprover)
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        {{ $schedule_adjustment->secondApprover->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if (is_null($schedule_adjustment->second_status))
                            <span class="text-requested">Requested</span>
                        @elseif($schedule_adjustment->second_status === 0)
                            <span class="text-declined">Declined</span>
                        @elseif($schedule_adjustment->second_status === 1)
                            <span class="text-approved">Approved</span>
                        @endif
                    </td>
                </tr>
            @endif
            @if ($schedule_adjustment->thirdApprover)
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        {{ $schedule_adjustment->thirdApprover->name }}
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        @if (is_null($schedule_adjustment->third_status))
                            <span class="text-requested">Requested</span>
                        @elseif($schedule_adjustment->third_status === 0)
                            <span class="text-declined">Declined</span>
                        @elseif($schedule_adjustment->third_status === 1)
                            <span class="text-approved">Approved</span>
                        @endif
                    </td>
                </tr>
            @endif
        </x-slot>
    </x-table.table>
</div>
