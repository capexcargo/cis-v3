@push('heads')
    <style>
        /* Large desktops and laptops */
        @media (min-width: 1200px) {
            video {
                width: 300px !important;
                height: 230px !important;
                border-width: 3px;
                border-color: #003399;
                border-radius: 0.375rem;
            }

            #camera[style],
            #snap_shot {
                width: 300px !important;
                height: 230px !important;
            }

        }

        /* Landscape tablets and medium desktops */
        @media (min-width: 992px) and (max-width: 1199px) {
            video {
                width: 240px !important;
                height: 170px !important;
                border-width: 3px;
                border-color: #003399;
                border-radius: 0.375rem;
            }

            #camera[style],
            #snap_shot {
                width: 240px !important;
                height: 170px !important;
            }
        }

        /* Portrait tablets and small desktops */
        @media (min-width: 768px) and (max-width: 991px) {
            video {
                width: 240px !important;
                height: 170px !important;
                border-width: 3px;
                border-color: #003399;
                border-radius: 0.375rem;
            }

            #camera[style],
            #snap_shot {
                width: 240px !important;
                height: 170px !important;
            }
        }

        /* Landscape phones and portrait tablets */
        @media (max-width: 767px) {
            video {
                width: 210px !important;
                height: 140px !important;
                border-width: 3px;
                border-color: #003399;
                border-radius: 0.375rem;
            }

            #camera[style],
            #snap_shot {
                width: 210px !important;
                height: 140px !important;
            }
        }

        /* Portrait phones and smaller */
        @media (max-width: 480px) {
            video {
                width: 210px !important;
                height: 140px !important;
                border-width: 3px;
                border-color: #003399;
                border-radius: 0.375rem;
            }

            #camera[style],
            #snap_shot {
                width: 210px !important;
                height: 140px !important;
            }
        }
    </style>
@endpush


<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}"
    class="flex items-center justify-center h-full text-lg text-center text-white bg-gradient-to-b from-[#86ACF7] to-[#CCDBF8]">

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <h2 class="mb-3 text-xl font-normal text-center text-black">
                Are you sure you want to time out?
            </h2>
            <div class="flex justify-center mt-6 space-x-6">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-10 py-2 text-xs font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    NO
                </button>
                <button type="button" onclick="timeOut()"
                    class="px-10 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    YES
                </button>
            </div>
        </x-slot>
    </x-modal>

    <div class="flex flex-col items-center justify-center space-y-2 -mt-30 md:space-y-4 lg:space-y-6">
        {{-- <div id="image_shot" wire:ignore>
        </div> --}}
        <div id="camera" class="flex items-center justify-center" wire:ignore>
        </div>

        <p class="text-base md:text-lg lg:text-2xl">{{ date('l, F d, Y', strtotime(now())) }}</p>
        {{-- <p class="font-semibold text-7xl">{{ date('h : i : s A', strtotime(now())) }}</p> --}}
        {{-- <p class="text-2xl">
            <span id="day"></span>
            <span id="date"></span>

        </p> --}}
        <p class="text-3xl font-semibold md:text-5xl lg:text-7xl" id="time"></p>

        <div class="flex items-start justify-between space-x-20">
            <div>
                <button type="button" onclick="timeIn()"
                    class="w-16 h-16 text-lg font-medium text-white bg-blue-900 rounded-full shadow-lg md:w-24 lg:w-32 md:h-24 lg:h-32 md:text-2xl lg:text-3xl">IN</button>
                @if ($time_log)
                    @if ($time_log->time_in)
                        <p class="mt-3">{{ date('h : i : s A', strtotime($time_log->time_in)) }}</p>
                    @endif
                @endif
            </div>
            <div>
                <button type="button" wire:click="confirm"
                    class="w-16 h-16 text-lg font-medium text-blue-900 bg-white border-2 border-blue-900 rounded-full shadow-lg md:w-24 lg:w-32 md:h-24 lg:h-32 md:text-2xl lg:text-3xl">OUT</button>
                @if ($time_log)
                    @if ($time_log->time_out)
                        <p class="mt-3">{{ date('h : i : s A', strtotime($time_log->time_out)) }}</p>
                    @endif
                @endif
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>

    <script>
        Webcam.set({
            width: 300,
            height: 230,
            image_format: 'jpeg',
            jpeg_quality: 90
        });

        Webcam.attach('#camera');

        function timeIn() {
            Webcam.snap(function(data_uri) {
                document.getElementById('camera').innerHTML =
                    '<img id="snap_shot" src="' + data_uri + '" />';
                Livewire.emit('time_in', data_uri)
            });
        }

        function timeOut() {
            Webcam.snap(function(data_uri) {
                document.getElementById('camera').innerHTML =
                    '<img id="snap_shot" src="' + data_uri + '" />';
                Livewire.emit('time_out', data_uri)
            });
        }

        const getCurrentTimeDate = () => {
            let currentTimeDate = new Date();

            var weekday = new Array(7);
            weekday[0] = "Sunday";
            weekday[1] = "Monday";
            weekday[2] = "Tuesday";
            weekday[3] = "Wednesday";
            weekday[4] = "Thursday";
            weekday[5] = "Friday";
            weekday[6] = "Saturday";

            const month = [
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            ]

            var hours = currentTimeDate.getHours();

            var minutes = currentTimeDate.getMinutes();
            var seconds = currentTimeDate.getSeconds();
            minutes = minutes < 10 ? '0' + minutes : minutes;
            seconds = seconds < 10 ? '0' + seconds : seconds;

            var AMPM = hours >= 12 ? 'PM' : 'AM';

            if (hours === 12) {
                hours = 12;
            } else {
                hours = hours % 12;
            }

            var currentTime = `${hours} : ${minutes} : ${seconds} ${AMPM}`;
            var currentDay = weekday[currentTimeDate.getDay()];

            var currentDate = currentTimeDate.getDate();
            var currentMonth = month[currentTimeDate.getMonth()];
            var CurrentYear = currentTimeDate.getFullYear();

            var fullDate = `${currentDate} ${currentMonth} ${CurrentYear}`;


            document.getElementById("time").innerHTML = currentTime;
            // document.getElementById("day").innerHTML = currentDay;
            // document.getElementById("date").innerHTML = fullDate;

            setTimeout(getCurrentTimeDate, 500);
        }
        getCurrentTimeDate();
    </script>
@endpush
