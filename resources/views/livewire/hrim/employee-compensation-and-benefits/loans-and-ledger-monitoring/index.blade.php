<x-form x-data="{
    search_form: false,
    confirmation_modal: '{{ $confirmation_modal }}',
    create_loan_modal: '{{ $create_loan_modal }}',
    edit_loan_modal: '{{ $edit_loan_modal }}',
    view_payment_modal: '{{ $view_payment_modal }}',
    view_approval_modal: '{{ $view_approval_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        <x-modal id="create_loan_modal" size="w-10/12 xs:w-10/12 sm:w-10/12 md:w-1/4">
            <x-slot name="title">File Loan</x-slot>
            <x-slot name="body">
                @livewire('hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.create')
            </x-slot>
        </x-modal>
        @if ($loans_and_ledger_id && $view_payment_modal)
            <x-modal id="view_payment_modal" size="w-10/12 xs:w-10/12 sm:w-10/12 md:w-1/2">
                <x-slot name="title"></x-slot>
                <x-slot name="body">
                    @livewire('hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.view-loan-pay-schedule', ['id' => $loans_and_ledger_id])
                </x-slot>
            </x-modal>
        @endif
        @if ($loans_and_ledger_id && $view_approval_modal)
            <x-modal id="view_approval_modal" size="w-10/12 xs:w-10/12 sm:w-10/12 md:w-1/4">
                <x-slot name="title"></x-slot>
                <x-slot name="body">
                    @livewire('hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.view-approval', ['id' => $loans_and_ledger_id])
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_loans_and_ledger_monitoring_edit')
            @if ($loans_and_ledger_id && $edit_loan_modal)
                <x-modal id="edit_loan_modal" size="w-10/12 xs:w-10/12 sm:w-10/12 md:w-1/4">
                    <x-slot name="title">Edit Loan</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.edit', ['id' => $loans_and_ledger_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan

        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                <h2 class="mb-3 text-lg text-center text-gray-900">
                    {{ $confirmation_message }}
                </h2>
                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-12 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-md hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click="cancelLoanRequest"
                        class="flex-none px-12 py-1 mt-4 text-sm text-white rounded-md bg-blue">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>

</x-slot>
<x-slot name="header_title">Loans and Ledger </x-slot>
<x-slot name="header_button">
    @can('hrim_loans_and_ledger_monitoring_add')
        <button wire:click="action({}, 'apply_loan')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
            <div class="flex items-start justify-between">
                File Loan
            </div>
        </button>
    @endcan
</x-slot>
<x-slot name="body">
    <div class="flex justify-end">
        <button hidden
            class="mt-2 px-6 py-2 text-xs bg-gray-200 border border-black shadow-sm text-black rounded-sm hover:bg-blue-100">
            <div class="flex items-start justify-between">
                <svg wire:click="action({'id': ''}, '')" class="w-4 h-4 mr-2 text-gray-500" aria-hidden="true"
                    focusable="false" data-prefix="far" data-icon="print-alt" role="img"
                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path fill="currentColor"
                        d="M448 192H64C28.65 192 0 220.7 0 256v96c0 17.67 14.33 32 32 32h32v96c0 17.67 14.33 32 32 32h320c17.67 0 32-14.33 32-32v-96h32c17.67 0 32-14.33 32-32V256C512 220.7 483.3 192 448 192zM384 448H128v-96h256V448zM432 296c-13.25 0-24-10.75-24-24c0-13.27 10.75-24 24-24s24 10.73 24 24C456 285.3 445.3 296 432 296zM128 64h229.5L384 90.51V160h64V77.25c0-8.484-3.375-16.62-9.375-22.62l-45.25-45.25C387.4 3.375 379.2 0 370.8 0H96C78.34 0 64 14.33 64 32v128h64V64z" />
                </svg>
                Print ATD
            </div>
        </button>
    </div>
    <div class="bg-white rounded-lg shadow-md">

        <x-table.table>
            <x-slot name="thead">
                <x-table.th name="" />
                <x-table.th name="No." />
                <x-table.th name="Reference Number" />
                <x-table.th name="Type" />
                <x-table.th name="Principal Amount" />
                <x-table.th name="Total Interest" />
                <x-table.th name="Terms" />
                <x-table.th name="Payment Terms" />
                <x-table.th name="Payment Start Date" />
                <x-table.th name="Payment End Date" />
                <x-table.th name="Purpose" />
                <x-table.th name="Remaining Deduction Count" />
                <x-table.th name="Total Loan Amount" hidden />
                <x-table.th name="Status" />
                <x-table.th name="Remarks(Decline Reason)" />
                <x-table.th name="Action" />
                <x-table.th name="" />
                <x-table.th name="" />
            </x-slot>
            <x-slot name="tbody">
                @foreach ($loans_and_ledger as $i => $loan_and_ledger)
                    @if ($loan_and_ledger)
                        <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                            <td class="p-3 whitespace-nowrap">
                                @if ($loan_and_ledger->final_status == 3)
                                    <a target="_blank"
                                        href="{{ route('hrim.employee-compensation-and-benefits.print-atd', ['id' => $loan_and_ledger->id]) }}"
                                        class="">
                                        <div class="flex items-start justify-between md:flex">
                                            <svg class="w-5 h-5 mr-2 text-gray-500 hover:text-blue-800"
                                                aria-hidden="true" focusable="false" data-prefix="far"
                                                data-icon="print-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M448 192H64C28.65 192 0 220.7 0 256v96c0 17.67 14.33 32 32 32h32v96c0 17.67 14.33 32 32 32h320c17.67 0 32-14.33 32-32v-96h32c17.67 0 32-14.33 32-32V256C512 220.7 483.3 192 448 192zM384 448H128v-96h256V448zM432 296c-13.25 0-24-10.75-24-24c0-13.27 10.75-24 24-24s24 10.73 24 24C456 285.3 445.3 296 432 296zM128 64h229.5L384 90.51V160h64V77.25c0-8.484-3.375-16.62-9.375-22.62l-45.25-45.25C387.4 3.375 379.2 0 370.8 0H96C78.34 0 64 14.33 64 32v128h64V64z" />
                                            </svg>
                                        </div>
                                    </a>
                                @endif
                                {{-- <input type="checkbox" class="form-checkbox" id="enable" name="enable"
                                        value="" wire:model="{{ $i }}"> --}}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $i + 1 }}
                            </td>
                            <td class="p-3 whitespace-nowrap w-90">
                                {{ $loan_and_ledger->reference_no }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $loan_and_ledger->loanType->name }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ number_format($loan_and_ledger->principal_amount) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">{{ $loan_and_ledger->interest }}
                            </td>
                            <td class="p-3 whitespace-nowrap">{{ $loan_and_ledger->terms }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $loan_and_ledger->term_type == 1 ? 'Bi Monthly' : 'Monthly' }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ date('M. d, Y', strtotime($loan_and_ledger->start_date)) }}<br>
                                <span class="text-xs">
                                    @foreach ($payout_date_start as $i => $payout_start)
                                        @if ($loan_and_ledger->id == $payout_start['start_id'])
                                            ({{ date('M. d, Y', strtotime($payout_start['start_date'])) }})
                                        @endif
                                    @endforeach
                                </span>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ date('M. d, Y', strtotime($loan_and_ledger->end_date)) }}<br>
                                <span class="text-xs">
                                    @foreach ($payout_date_end as $i => $payout_end)
                                        @if ($loan_and_ledger->id == $payout_end['end_id'])
                                            ({{ date('M. d, Y', strtotime($payout_end['end_date'])) }})
                                        @endif
                                    @endforeach
                                </span>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="w-50">{{ $loan_and_ledger->purpose }}</div>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $loan_and_ledger->loanDetails->where('status', '=', 1)->sum('status') > 0 ? $loan_and_ledger->loanDetails->where('status', '=', 1)->sum('status') : $loan_and_ledger->terms }}
                                / {{ $loan_and_ledger->terms }}
                            </td>
                            <td class="p-3 whitespace-nowrap" hidden>{{ $loan_and_ledger->purpose }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <span
                                    class="{{ $loan_and_ledger->final_status == 1 ||
                                    ($loan_and_ledger->final_status == 3 && $loan_and_ledger->third_status == 1 && $loan_and_ledger->third_approver != '')
                                        ? 'text-orange bg-orange-light'
                                        : ($loan_and_ledger->final_status == 3
                                            ? 'text-blue bg-blue-300'
                                            : ($loan_and_ledger->final_status == 4
                                                ? 'text-red bg-red-300'
                                                : '')) }} 
                                                    text-xs rounded-full p-1 px-4">
                                    {{-- {{ $loan_and_ledger->finalStatus->display }} --}}
                                    {{ $loan_and_ledger->final_status == 1 ||
                                    ($loan_and_ledger->final_status == 3 && $loan_and_ledger->third_status == 1 && $loan_and_ledger->third_approver != '')
                                        ? 'Requested'
                                        : ($loan_and_ledger->final_status == 3
                                            ? 'Approved'
                                            : ($loan_and_ledger->final_status == 4
                                                ? 'Declined'
                                                : '')) }}
                                </span>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $loan_and_ledger->decline_reason }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex gap-3">
                                    @if ($loan_and_ledger->final_status == 1 ||
                                        ($loan_and_ledger->final_status == 3 &&
                                            $loan_and_ledger->third_status == 1 &&
                                            $loan_and_ledger->third_approver != ''))
                                        @can('hrim_loans_and_ledger_monitoring_edit')
                                            <svg wire:click="action({'id': {{ $loan_and_ledger->id }}}, 'edit_loan')"
                                                class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700"
                                                data-prefix="far" data-icon="cancel-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                            </svg>
                                        @endcan
                                        @can('hrim_loans_and_ledger_monitoring_delete')
                                            <svg wire:click="action({'id': {{ $loan_and_ledger->id }}}, 'delete_loan')"
                                                class="w-4 h-4 cursor-pointer text-red hover:text-red-700"
                                                data-prefix="far" data-icon="cancel-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
                                            </svg>
                                        @endcan
                                    @endif
                                </div>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <svg wire:click="action({'id': {{ $loan_and_ledger->id }}}, 'view_approval')"
                                    class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700" data-prefix="far"
                                    data-icon="approve" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                </svg>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                @if ($loan_and_ledger->final_status == 3)
                                    <span
                                        wire:click="action({'id': {{ $loan_and_ledger->id }}}, 'view_payment_schedule')"
                                        class="underline text-blue text-xs cursor-pointer">View Payment
                                        Schedule</span>
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
            </x-slot>
        </x-table.table>
        <div class="px-1 pb-2">
            {{ $loans_and_ledger->links() }}
        </div>
</x-slot>
</x-form>
