<div wire:init="load" x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
    create_view_payment_modal: '{{ $create_view_payment_modal }}'
}">
    <x-loading></x-loading>
    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            {{ $confirmation_message }}
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">No</button>
                <button type="button" wire:click="submit"
                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Yes</button>
            </div>
        </x-slot>
    </x-modal>
    {{-- @if ($loans_and_ledger_id && $view_payment_modal) --}}
    <x-modal id="create_view_payment_modal" size="w-10/12 xs:w-10/12 sm:w-10/12 md:w-1/2">
        <x-slot name="title"></x-slot>
        <x-slot name="body">
            @livewire('hrim.employee-compensation-and-benefits.loans-and-ledger-monitoring.view-create-loan')
        </x-slot>
    </x-modal>
    {{-- @endif --}}
    <form autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div wire:init="generateRefernenceNo">
                    <x-label for="reference_number" value="Reference Number" />
                    <x-input type="text" name="reference_number" wire:model.defer='reference_number' disabled>
                    </x-input>
                    <x-input-error for="reference_number" />
                </div>
                <div wire:init="">
                    <x-label for="type" value="Type" :required="true" />
                    <x-select name="type" wire:model='type'>
                        <option value="">Select</option>
                        @foreach ($loan_type_references as $loan_type)
                            <option value="{{ $loan_type->id }}">
                                {{ $loan_type->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="type" />
                </div>
                <div>
                    <x-label for="principal_amount" value="Principal Amount" :required="true" />
                    <x-input type="text" name="principal_amount" wire:model.defer='principal_amount'></x-input>
                    <x-input-error for="principal_amount" />
                </div>
                <div hidden>
                    <x-label for="total_interest" value="Total Interest" />
                    <x-input type="text" name="total_interest" wire:model.defer='total_interest' disabled></x-input>
                    <x-input-error for="total_interest" />
                </div>
                <div class="grid grid-cols-2 gap-4">
                    <div>
                        <x-label for="terms" value="Terms" :required="true" />
                        <x-input type="text" name="terms" wire:model.debounce.100ms='terms'>
                        </x-input>
                        <x-input-error for="terms" />
                    </div>
                    <div>
                        <x-label for="term_type" value="Term Type" :required="true" />
                        <x-select name="term_type" wire:model='term_type'>
                            <option value="">Select</option>
                            @if ($loan_type_category != 1)
                                <option value="1">Bi Monthly</option>
                            @endif
                            <option value="2" {{ $loan_type_category == 1 ? 'selected' : '' }}>Monthly</option>
                        </x-select>
                        <x-input-error for="term_type" />
                    </div>
                </div>
                <div>
                    <x-label for="payment_start_date" value="Payment Start Date" :required="true" />
                    <div class="grid grid-cols-2 gap-4">
                        {{-- <div>
                        <x-label for="payment_start_date" value="Payment Start Date" :required="true" />
                        <x-input type="date" name="payment_start_date" wire:model='payment_start_date'></x-input>
                        <x-input-error for="payment_start_date" />
                    </div> --}}
                        <div>
                            <x-label for="year" value="Year" />
                            <x-input type="text" name="year" wire:model='year'></x-input>
                            <x-input-error for="year" />
                        </div>
                        <div>
                            <x-label for="month_day" value="Month Day" />
                            <x-select name="month_day" wire:model='month_day'>
                                <option value="">Select</option>
                                @foreach ($month_day_references as $i => $month_day_ref)
                                    @if (($loan_type_category == 1 ? '30' : '') == $month_day_ref['day'])
                                        <option value="{{ $month_day_ref['value'] }}">
                                            {{ $month_day_ref['name'] }}
                                        </option>
                                    @elseif (!($loan_type_category == 1 ? '30' : '') == $month_day_ref['day'])
                                        <option value="{{ $month_day_ref['value'] }}">
                                            {{ $month_day_ref['name'] }}
                                        </option>
                                    @endif
                                @endforeach
                            </x-select>
                            <x-input-error for="month_day" />
                        </div>
                    </div>
                </div>
                <div>
                    <x-label for="payment_end_date" value="Payment End Date" :required="true" />
                    <x-input type="date" name="payment_end_date" wire:model='payment_end_date' disabled>
                    </x-input>
                    <x-input-error for="payment_end_date" />
                </div>
                <div>
                    <x-label for="purpose" value="Purpose" />
                    <x-textarea type="text" name="purpose" wire:model.defer='purpose'>
                    </x-textarea>
                    <x-input-error for="purpose" />
                </div>
                <div class="flex justify-end mt-6 text-xs space-x-3">
                    <span hidden wire:click="action({}, 'create_view_payment_schedule')"
                        class="underline text-blue text-left pt-4 mr-4 text-sm cursor-pointer">View Payment
                        Schedule</span>
                    <button type="button" wire:click="action({}, 'confirm_submit')"
                        class="px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                        Submit
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
