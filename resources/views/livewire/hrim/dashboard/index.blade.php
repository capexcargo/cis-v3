<x-form>
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="header_title">Dashboard</x-slot>
    <x-slot name="body" size="w-10/12 xs:w-10/12 sm:w-10/12 md:w-10/12">
        <div class="grid grid-cols-10 gap-5">
            <div class="col-span-6">
                <div class="bg-white rounded-lg shadow-lg">
                    <div class="flex justify-between">
                        <div class="min-w-0 p-3 px-6">
                            <div class="font-semibold text-gray-500">
                                <h4 class="text-[16px]">
                                    Active Employees | CaPEx-wide
                                </h4>
                                <p class="text-sm font-normal">236 Employees</p>
                            </div>
                        </div>
                        <div class="min-w-0 p-3 px-6 font-semibold">
                            <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                wire:click="action({},'see_details_emp_info')">
                                See Details
                            </h4>
                        </div>
                    </div>

                    <div class="grid grid-cols-3 gap-3 p-3 px-6">
                        <button wire:click=""
                            class="flex justify-between px-4 py-6 text-gray-700 bg-white border border-blue-600 rounded-md">
                            <div class="font-medium text-left text-md">PROBATIONARY<br>EMPLOYEES</div>
                            <div class="mt-2 text-3xl font-bold">{{ $status_probationary }}</div>
                        </button>
                        <button wire:click=""
                            class="flex justify-between px-4 py-6 text-gray-700 bg-white border border-blue-600 rounded-md ">
                            <div class="font-medium text-left text-md">CONTRACTUAL<br>EMPLOYEES</div>
                            <div class="mt-2 text-3xl font-bold">{{ $status_contractual }}</div>
                        </button>
                        <button wire:click=""
                            class="flex justify-between px-4 py-6 text-gray-700 bg-white border border-blue-600 rounded-md ">
                            <div class="font-medium text-left text-md">REGULAR<br>EMPLOYEES</div>
                            <div class="mt-2 text-3xl font-bold">{{ $status_regular }}</div>
                        </button>
                    </div>

                    {{-- <div class="p-3 px-6 overflow-auto" style="height: 32vh">
                        <div class="bg-white rounded-lg shadow-md">
                            <x-table.table style="overflow: hidden">
                                <x-slot name="thead">
                                    <x-table.th name="Branches" />
                                    <x-table.th name="Probationary" />
                                    <x-table.th name="Contractual" />
                                    <x-table.th name="Regular" />
                                    <x-table.th name="Total" />
                                </x-slot>
                                <x-slot name="tbody">

                                    @foreach ($get_status_by_branchs_res as $get_status_by_branch)
                                        <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                            <td class="p-3 whitespace-nowrap">{{ $get_status_by_branch['code'] }}</td>
                                            <td class="p-3 whitespace-nowrap">{{ $get_status_by_branch['probi'] }}</td>
                                            <td class="p-3 whitespace-nowrap">{{ $get_status_by_branch['contractual'] }}
                                            </td>
                                            <td class="p-3 whitespace-nowrap">{{ $get_status_by_branch['regular'] }}
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $get_status_by_branch['regular'] + $get_status_by_branch['probi'] + $get_status_by_branch['contractual'] }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr class="font-semibold stext-lg text-blue ">
                                        <td class="p-3 text-gray-400 whitespace-nowrap">Total</td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ array_sum($get_status_by_branchs_res_footer_probi) }}</td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ array_sum($get_status_by_branchs_res_footer_contractual) }}</td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ array_sum($get_status_by_branchs_res_footer_reg) }}</td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ array_sum($get_status_by_branchs_res_footer_probi) +
                                                array_sum($get_status_by_branchs_res_footer_contractual) +
                                                array_sum($get_status_by_branchs_res_footer_reg) }}
                                        </td>
                                    </tr>
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div> --}}
                    <div>
                        <div class="px-6" style=" z-index: 1;">
                            <div class="bg-white rounded-t-lg shadow-lg" style="width: 98%;">
                                <table class="min-w-full font-medium">
                                    <thead class="text-xs text-gray-700 capitalize bg-white border-0 cursor-pointer">
                                        <tr>
                                            <th class="p-3 tracking-wider whitespace-nowrap">
                                                <span class="flex w-10 text-sm font-medium">
                                                    Branches
                                                </span>
                                            </th>
                                            <th class="p-3 tracking-wider whitespace-nowrap" scope="col">
                                                <span class="flex w-12 px-1 ml-6 font-medium">
                                                    Probationary
                                                </span>
                                            </th>
                                            <th class="p-3 tracking-wider whitespace-nowrap" scope="col">
                                                <span class="flex w-12 mr-1 font-medium">
                                                    Contractual
                                                </span>
                                            </th>
                                            <th class="p-3 tracking-wider whitespace-nowrap" scope="col">
                                                <span class="flex w-10 px-4 font-medium">
                                                    Regular
                                                </span>
                                            </th>
                                            <th class="p-3 tracking-wider whitespace-nowrap" scope="col">
                                                <span class="flex w-6 mr-1 font-medium">
                                                    Total
                                                </span>
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="p-3 px-6 -mt-2 overflow-auto" style="height: 50vh;">
                            <div class="bg-white rounded-b-lg shadow-md">
                                <table class="min-w-full font-medium divide-y-2">
                                    <tbody class="text-sm bg-gray-100 border-0 divide-y-4 divide-white">
                                        @foreach ($get_status_by_branchs_res as $get_status_by_branch)
                                            <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                                <th class="p-3 tracking-wider whitespace-nowrap">
                                                    <span class="flex w-10 text-sm font-medium">
                                                        {{ $get_status_by_branch['code'] }}
                                                    </span>
                                                </th>
                                                <th class="p-3 tracking-wider whitespace-nowrap" scope="col">
                                                    <span class="flex w-12 px-1 ml-6 font-medium">
                                                        {{ $get_status_by_branch['probi'] }}
                                                    </span>
                                                </th>
                                                <th class="p-3 tracking-wider whitespace-nowrap" scope="col">
                                                    <span class="flex w-12 mr-1 font-medium">
                                                        {{ $get_status_by_branch['contractual'] }}
                                                    </span>
                                                </th>
                                                <th class="p-3 tracking-wider whitespace-nowrap" scope="col">
                                                    <span class="flex w-10 px-4 font-medium">
                                                        {{ $get_status_by_branch['regular'] }}
                                                    </span>
                                                </th>
                                                <th class="p-3 tracking-wider whitespace-nowrap" scope="col">
                                                    <span class="flex w-6 mr-1 font-medium">
                                                        {{ $get_status_by_branch['regular'] + $get_status_by_branch['probi'] + $get_status_by_branch['contractual'] }}

                                                    </span>
                                                </th>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="px-6 overflow-auto">
                            <div style="width: 98%">
                                <div class="overflow-auto rounded-lg">
                                    <div class="inline-block min-w-full align-middle">
                                        <table class="min-w-full font-medium divide-y-2">
                                            <thead class="cursor-pointer text-md">
                                                <tr>
                                                    <th class="px-3 tracking-wider text-gray-400 whitespace-nowrap">
                                                        <span class="flex w-10 text-sm font-medium">
                                                            Total
                                                        </span>
                                                    </th>
                                                    <th class="px-3 tracking-wider whitespace-nowrap text-[#003399]"
                                                        scope="col">
                                                        <span class="flex w-12 px-1 ml-6 font-medium">
                                                            {{ array_sum($get_status_by_branchs_res_footer_probi) }}
                                                        </span>
                                                    </th>
                                                    <th class="px-3 tracking-wider whitespace-nowrap text-[#003399]"
                                                        scope="col">
                                                        <span class="flex w-12 mr-1 font-medium">
                                                            {{ array_sum($get_status_by_branchs_res_footer_contractual) }}
                                                        </span>
                                                    </th>
                                                    <th class="px-3 tracking-wider whitespace-nowrap text-[#003399]"
                                                        scope="col">
                                                        <span class="flex w-10 px-4 font-medium">
                                                            {{ array_sum($get_status_by_branchs_res_footer_reg) }}
                                                        </span>
                                                    </th>
                                                    <th class="px-3 tracking-wider whitespace-nowrap text-[#003399]"
                                                        scope="col">
                                                        <span class="flex w-6 mr-1 font-medium">
                                                            {{ array_sum($get_status_by_branchs_res_footer_probi) +
                                                                array_sum($get_status_by_branchs_res_footer_contractual) +
                                                                array_sum($get_status_by_branchs_res_footer_reg) }}
                                                        </span>
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
                <div class="mt-3 bg-white rounded-lg shadow-lg">
                    <div class="flex justify-between">
                        <div class="min-w-0 p-3 px-6">
                            <div class="font-semibold text-gray-500">
                                <h4 class="text-[16px]">
                                    Age Distribution Report
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="flex overflow-hidden" style="width: 100%; height:400;">
                        <canvas class="p-10" id="chartLine"></canvas>
                        <p class="mb-2 text-sm text-center text-gray-400" style="margin-top: -4%">Age</p>
                    </div>
                </div>
                <div class="mt-3 bg-white rounded-lg shadow-lg">
                    <div class="flex justify-between">
                        <div class="min-w-0 p-3 px-6">
                            <div class="font-semibold text-gray-500">
                                <h4 class="text-[16px]">
                                    Salary Grade
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="flex overflow-hidden" style="width: 100%; height:400;">
                        <canvas class="p-10" id="chartBar"></canvas>
                        <p class="mb-2 text-sm text-center text-gray-400" style="margin-top: -4%">Salary Grade</p>
                    </div>
                </div>
                <div class="mt-3 bg-white rounded-lg shadow-lg">
                    <div class="flex justify-between">
                        <div class="min-w-0 p-3 px-6">
                            <div class="font-semibold text-gray-500">
                                <h4 class="text-[16px]">
                                    Recruitment
                                </h4>
                                <p class="text-sm font-normal">{{ count($get_emp_requisition) }} Total Employee
                                    Requests</p>
                            </div>
                        </div>

                        <div class="flex gap-4 p-3 px-4">
                            <div wire:init=""class="w-24">
                                <x-select value="" label="" name="" wire:model="">
                                    <option value="">All</option>
                                </x-select>
                            </div>
                            <div wire:init=""class="w-24">
                                <x-select value="" label="" name="" wire:model="">
                                    <option value="1">Quarter</option>
                                    {{-- <option value="2">2nd Quarter</option>
                                    <option value="3">3rd Quarter</option>
                                    <option value="4">4th Quarter</option> --}}
                                </x-select>
                            </div>
                            <div class="items-end min-w-0 p-3 px-6 font-semibold">
                                <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                    wire:click="action({},'see_details_employee_req')">
                                    See Details
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="p-3 px-6 overflow-auto" style="height: 20vh">
                        <div class="bg-white rounded-lg shadow-md">
                            <x-table.table style="overflow: hidden">
                                <x-slot name="thead">
                                    <x-table.th name="Positions" />
                                    <x-table.th name="Date Requested" />
                                    <x-table.th name="Target Hire Date" />
                                    <x-table.th name="Actual Date Hired" />
                                </x-slot>
                                <x-slot name="tbody">
                                    @forelse ($get_emp_requisition as $emp_req)
                                        <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                            <td class="p-3">{{ $emp_req->position->display }}</td>
                                            <td class="p-3">{{ date('Y-m-d', strtotime($emp_req->created_at)) }}
                                            </td>
                                            <td class="p-3">{{ $emp_req->target_hire }}</td>
                                            <td class="p-3 text-red-600"></td>
                                        </tr>
                                    @empty
                                    @endforelse
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                    <div class="flex gap-12 p-3 px-6">
                        <div class="grid min-w-0 grid-cols-1 p-3 mt-4 font-semibold">
                            <div class="font-semibold text-gray-500">
                                <h4 class="text-[16px]">
                                    Overall Recruitment Score
                                </h4>
                            </div>
                            <div class="w-56">
                                <h4 class="flex text-sm">
                                    <div class="w-4 h-4 mr-2 bg-red-600 rounded-md"></div>
                                    <p>Delayed</p>
                                </h4>
                                <p class="ml-6 text-xs font-normal">1 Total Delayed Recruitment</p><br>
                                <h4 class="flex text-sm">
                                    <div class="w-4 h-4 mr-2 bg-green-600 rounded-md"></div>
                                    <p>On Time</p>
                                </h4>
                                <p class="ml-6 text-xs font-normal">2 Total On Time Recruitments</p><br>
                                <h4 class="flex text-sm">
                                    <div class="w-4 h-4 mr-2 rounded-md bg-blue-pie"></div>
                                    <p>Ahead</p>
                                </h4>
                                <p class="ml-6 text-xs font-normal">2 Total Early Recruitments</p>
                            </div>
                        </div>
                        <div class="overflow-hidden h-80 w-80 ">
                            <canvas class="p-10" id="chartDoughnut"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            {{-- // --}}
            <div class="col-span-4">
                <div class="bg-white rounded-lg shadow-lg">
                    <div class="flex justify-between">
                        <div class="min-w-0 px-6 py-3 text-left">
                            <div class="">
                                <h4 class="font-semibold text-gray-500 text-[16px]">
                                    Employment Category
                                </h4>
                            </div>
                        </div>
                        <div class="min-w-0 p-3 font-semibold">
                            <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                wire:click="action({},'see_details_emp_info')">
                                See Details
                            </h4>
                        </div>
                    </div>
                    <div class="flex gap-10 p-3 px-6">
                        <div class="grid min-w-0 grid-cols-1 p-3 mt-12 font-semibold">
                            <div class="w-56">
                                <h4 class="flex text-sm">
                                    <div class="w-4 h-4 mr-2 rounded-md bg-blue-pie"></div>
                                    <p>Direct Employees</p>
                                </h4>
                                <p class="ml-6 text-xs font-normal">{{ $direct_emp }} Direct Employees</p><br>
                                <h4 class="flex text-sm">
                                    <div class="w-4 h-4 mr-2 rounded-md bg-sky-blue-pie"></div>
                                    <p>CSP Employees</p>
                                </h4>
                                <p class="ml-6 text-xs font-normal">{{ $csp_emp }} CSP Employees</p>
                            </div>
                        </div>
                        <div class="overflow-hidden h-72 w-72">
                            <canvas class="p-6" id="chartPie"></canvas>
                        </div>
                    </div>
                </div>
                <div class="mt-4 bg-white rounded-lg shadow-lg">
                    <div class="flex justify-between">
                        <div class="min-w-0 px-6 py-3 text-left">
                            <div class="">
                                <h4 class="font-semibold text-gray-500 text-[16px]">
                                    Gender
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="flex justify-between p-3 px-8">
                        <div class="grid min-w-0 grid-cols-1 p-3 font-semibold">
                            <div class="flex gap-2">
                                <div class="w-24 h-24 p-2 rounded-full bg-blue-pie">
                                    <svg class="w-20 h-20 text-white cursor-pointer " data-prefix="far"
                                        data-icon="cancel-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 320 512">
                                        <path fill="currentColor"
                                            d="M208 48C208 74.51 186.5 96 160 96C133.5 96 112 74.51 112 48C112 21.49 133.5 0 160 0C186.5 0 208 21.49 208 48zM152 352V480C152 497.7 137.7 512 120 512C102.3 512 88 497.7 88 480V256.9L59.43 304.5C50.33 319.6 30.67 324.5 15.52 315.4C.3696 306.3-4.531 286.7 4.573 271.5L62.85 174.6C80.2 145.7 111.4 128 145.1 128H174.9C208.6 128 239.8 145.7 257.2 174.6L315.4 271.5C324.5 286.7 319.6 306.3 304.5 315.4C289.3 324.5 269.7 319.6 260.6 304.5L232 256.9V480C232 497.7 217.7 512 200 512C182.3 512 168 497.7 168 480V352L152 352z" />
                                    </svg>
                                </div>
                                <div>
                                    <p class="mt-6 text-2xl">{{ $male_emp }}</p>
                                    <p class="text-xs">Male</p>
                                </div>
                            </div>
                        </div>
                        <div class="grid min-w-0 grid-cols-1 p-3 font-semibold">
                            <div class="flex gap-2">
                                <div class="w-24 h-24 p-2 rounded-full bg-pink">
                                    <svg class="w-20 h-20 text-white cursor-pointer " data-prefix="far"
                                        data-icon="cancel-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 320 512">
                                        <path fill="currentColor"
                                            d="M112 48C112 21.49 133.5 0 160 0C186.5 0 208 21.49 208 48C208 74.51 186.5 96 160 96C133.5 96 112 74.51 112 48zM88 384H70.2C59.28 384 51.57 373.3 55.02 362.9L93.28 248.1L59.43 304.5C50.33 319.6 30.67 324.5 15.52 315.4C.3696 306.3-4.531 286.7 4.573 271.5L58.18 182.3C78.43 148.6 114.9 128 154.2 128H165.8C205.1 128 241.6 148.6 261.8 182.3L315.4 271.5C324.5 286.7 319.6 306.3 304.5 315.4C289.3 324.5 269.7 319.6 260.6 304.5L226.7 248.1L264.1 362.9C268.4 373.3 260.7 384 249.8 384H232V480C232 497.7 217.7 512 200 512C182.3 512 168 497.7 168 480V384H152V480C152 497.7 137.7 512 120 512C102.3 512 88 497.7 88 480L88 384z">
                                    </svg>
                                </div>
                                <div>
                                    <p class="mt-6 text-2xl">{{ $female_emp }}</p>
                                    <p class="text-xs">Female</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-3 bg-white rounded-lg shadow-lg">
                    <div class="flex justify-between">
                        <div class="min-w-0 p-3 px-6 text-left">
                            <div class="font-semibold text-gray-500 ">
                                <h4 class="text-[16px]">
                                    Today's Total Late Employees
                                </h4>
                                <p class="text-sm font-normal">{{ count($get_emp_late) }} Employees</p>
                            </div>

                        </div>
                        <div class="min-w-0 p-3 font-semibold">
                            <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                wire:click="action({},'see_details_total_late')">
                                See Details
                            </h4>
                        </div>
                    </div>
                    <div class="p-3 px-6 overflow-auto" style="height: 20vh">
                        <div class="bg-white rounded-lg shadow-md">
                            <x-table.table style="overflow: hidden">
                                <x-slot name="thead">
                                    <x-table.th name="No." />
                                    <x-table.th name="Name" />
                                    <x-table.th name="Time In" />
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($get_emp_late as $i => $late)
                                        <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                            <td class="p-3 whitespace-nowrap">{{ $i + 1 }}.</td>
                                            <td class="flex p-2 w-90">
                                                <div class="flex space-x-2 w-50">
                                                    <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                                        style="background-image: url({{ Storage::disk('hrim_gcs')->url($late->user->photo_path . $late->user->photo_name) }})">
                                                    </div>
                                                    <div class="flex items-center text-sm">
                                                        <p class="flex-initial" style="overflow-wrap: break-word;">
                                                            {{ $late->user->name }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">{{ $late->time_in }}</td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                    <br>
                </div>
                <div class="mt-3 bg-white rounded-lg shadow-lg">
                    <div class="flex justify-between">
                        <div class="min-w-0 p-3 px-6 text-left">
                            <div class="font-semibold text-gray-500 ">
                                <h4 class="text-[16px]">
                                    Today's Total Employees on Leave
                                </h4>
                                <p class="text-sm font-normal">{{ count($get_emp_on_leave) }} Employees</p>
                            </div>
                        </div>
                        <div class="min-w-0 p-3 font-semibold">
                            <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                wire:click="action({},'see_details_leave_rec')">
                                See Details
                            </h4>
                        </div>
                    </div>
                    <div class="p-3 px-6 overflow-auto" style="height: 32vh">
                        <div class="bg-white rounded-lg shadow-md">
                            <x-table.table style="overflow: hidden">
                                <x-slot name="thead">
                                    <x-table.th name="No." />
                                    <x-table.th name="Name" />
                                    <x-table.th name="Type of Leave" />
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($get_emp_on_leave as $i => $on_leave_today)
                                        <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                            <td class="p-3 whitespace-nowrap">{{ $i + 1 }}</td>
                                            <td class="flex p-2 w-90">
                                                <div class="flex space-x-2 w-50">
                                                    <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                                        style="background-image: url({{ Storage::disk('hrim_gcs')->url($on_leave_today->user->photo_path . $on_leave_today->user->photo_name) }})">
                                                    </div>
                                                    <div class="flex items-center text-sm">
                                                        <p class="flex-initial" style="overflow-wrap: break-word;">
                                                            {{ $on_leave_today->user->name ?? null }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $on_leave_today->leaveType->display ?? null }}</td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                    <br>
                </div>
                <div class="mt-3 bg-white rounded-lg shadow-lg">
                    <div class="flex justify-between">
                        <div class="min-w-0 p-3 px-6 text-left">
                            <div class="font-semibold text-gray-500 ">
                                <h4 class="text-[16px]">
                                    TAR Report
                                </h4>
                                <p class="text-sm font-normal">{{ count($tar_report) }} Employees</p>
                            </div>
                        </div>
                        <div class="min-w-0 p-3 font-semibold">
                            <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                wire:click="action({},'see_details_tar')">
                                See Details
                            </h4>
                        </div>
                    </div>
                    <div class="p-3 px-6 overflow-auto" style="height: 20vh">
                        <div class="bg-white rounded-lg shadow-md">
                            <x-table.table style="overflow: hidden">
                                <x-slot name="thead">
                                    <x-table.th name="No." />
                                    <x-table.th name="Name" />
                                    <x-table.th name="Status" />
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($tar_report as $i => $tar)
                                        <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff] text-sm">
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $i + 1 }}.
                                            </td>
                                            <td class="flex p-2 w-90">
                                                <div class="flex space-x-2 w-44">
                                                    <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                                        style="background-image: url({{ Storage::disk('hrim_gcs')->url($tar->user->photo_path . $tar->user->photo_name) }})">
                                                    </div>
                                                    <div class="flex items-center">
                                                        <p class="flex-initial" style="overflow-wrap: break-word;">
                                                            {{ $tar->user->name }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                @if (is_null($tar->finalStatus->first_status))
                                                    <span class="text-requested">Requested</span>
                                                @elseif($tar->finalStatus->first_status === 0)
                                                    <span class="text-declined">Declined</span>
                                                @elseif($tar->finalStatus->first_status === 1)
                                                    <span class="text-approved">Approved</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                    <br>
                </div>
                <div class="mt-3 bg-white rounded-lg shadow-lg">
                    <div class="flex justify-between">
                        <div class="min-w-0 p-3 px-6 text-left">
                            <div class="font-semibold text-gray-500 ">
                                <h4 class="text-[16px]">
                                    Today's Total Absences
                                </h4>
                                <p class="text-sm font-normal">{{ count($get_emp_today_absent) }} Employees</p>
                            </div>
                        </div>
                    </div>
                    <div class="p-3 px-6 overflow-auto" style="height: 28vh">
                        <div class="bg-white rounded-lg shadow-md">
                            <x-table.table style="overflow: hidden">
                                <x-slot name="thead">
                                    <x-table.th name="No." />
                                    <x-table.th name="Name" />
                                    <x-table.th name="Division" />
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($get_emp_today_absent as $i => $absent_today)
                                        <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                            <td class="p-3 whitespace-nowrap">{{ $i + 1 }}.</td>
                                            <td class="p-2 whitespace-nowrap w-90">
                                                <div class="flex space-x-2 w-50">
                                                    <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                                        style="background-image: url({{ Storage::disk('hrim_gcs')->url($absent_today->user->photo_path . $absent_today->user->photo_name) }})">
                                                    </div>
                                                    <div class="flex items-center text-sm">
                                                        <div>
                                                            <p class="flex-initial">
                                                                {{ $absent_today->user->name }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $absent_today->user->division->name }}</td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </x-slot>
</x-form>

@push('scripts')
    <!-- Required chart.js -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        // Distribution Chart
        const labels = ["18-24", "25-34", "35-44", "45-54", "55-64", "65+"];
        const dataLine = {
            labels: labels,
            datasets: [{
                label: "Age Distribution Chart",
                backgroundColor: "#003399",
                borderColor: "#003399",
                data: [
                    {{ count($age1824) }},
                    {{ count($age2534) }},
                    {{ count($age3544) }},
                    {{ count($age4554) }},
                    {{ count($age5564) }},
                    {{ count($age64up) }},
                ],
            }, ],
        };

        const configLineChart = {
            type: "line",
            data: dataLine,
            options: {},
        };

        var chartLine = new Chart(
            document.getElementById("chartLine"),
            configLineChart
        );

        // Salary Grade
        const labelsBarChart = [
            "A1", "A2", "A3", "B1", "B2", "B3", "C1", "C2", "C3", "D1", "D2"
        ];

        const dataBarChart = {
            labels: labelsBarChart,
            datasets: [{
                label: "Salary Grade Chart",
                backgroundColor: [
                    "#389AEB", "#389AEB", "#389AEB",
                    "#3BDD46", "#3BDD46", "#3BDD46",
                    "#2364E6", "#2364E6", "#2364E6",
                    "#FF4040", "#FF4040"
                ],
                borderColor: "",
                data: [
                    {{ $salary_grade_a1 }},
                    {{ $salary_grade_a2 }},
                    {{ $salary_grade_a3 }},
                    {{ $salary_grade_b1 }},
                    {{ $salary_grade_b2 }},
                    {{ $salary_grade_b3 }},
                    {{ $salary_grade_c1 }},
                    {{ $salary_grade_c2 }},
                    {{ $salary_grade_c3 }},
                    {{ $salary_grade_d1 }},
                    {{ $salary_grade_d2 }},
                ],
            }, ],
        };

        const configBarChart = {
            type: "bar",
            data: dataBarChart,
            options: {},
        };

        var chartBar = new Chart(
            document.getElementById("chartBar"),
            configBarChart
        );

        // Employment Category
        const dataPie = {
            datasets: [{
                label: "Employment Category",
                data: [{{ $direct_emp }}, {{ $csp_emp }}],
                backgroundColor: [
                    "#1F78B4",
                    "#A6CEE3",
                ],
                hoverOffset: 4,
            }, ],
        };

        const configPie = {
            type: "pie",
            data: dataPie,
            options: {},
        };

        var chartBar = new Chart(document.getElementById("chartPie"), configPie);


        // Recruitment
        const dataDoughnut = {
            datasets: [{
                label: "Recruitment",
                data: [20, 40, 40],
                backgroundColor: [
                    "#FF0000",
                    "#3CFC2E",
                    "#1F78B4",
                ],
                hoverOffset: 4,
            }, ],
        };

        const configDoughnut = {
            type: "doughnut",
            data: dataDoughnut,
            options: {},
        };

        var chartBar = new Chart(
            document.getElementById("chartDoughnut"),
            configDoughnut
        );
    </script>
@endpush
