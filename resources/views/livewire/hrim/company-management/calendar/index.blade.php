<x-form x-data="{ confirmation_modal: '{{ $confirmation_modal }}', create_modal: '{{ $create_modal }}', view_modal: '{{ $view_modal }}', edit_modal: '{{ $edit_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                <div class="space-y-3">
                    <p class="text-center">{{ $confirmation_message }}</p>
                    <div class="flex items-center justify-center space-x-3">
                        <x-button type="button" wire:click="$set('confirmation_modal', false)" title="No"
                            class="py-1 bg-white text-blue hover:bg-gray-100" />
                        <x-button type="button" wire:click="confirm" title="Yes"
                            class="bg-blue text-white hover:bg-[#002161] py-1" />
                    </div>
                </div>
            </x-slot>
        </x-modal>
        @can('hrim_calendar_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Create Event</x-slot>
                <x-slot name="body">
                    @livewire('hrim.company-management.calendar.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_calendar_edit')
            @if ($edit_modal && $calendar_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Edit Event</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.company-management.calendar.edit', ['id' => $calendar_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @if ($view_modal && $date)
            <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                <x-slot name="title">View Event</x-slot>
                <x-slot name="body">
                    @livewire('hrim.company-management.calendar.view', ['date' => $date])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">
        Calendar
    </x-slot>
    <x-slot name="header_button">
        @can('hrim_calendar_add')
            <x-button type="button" wire:click="$set('create_modal', true)" title="Create Event"
                class="bg-blue text-white hover:bg-[#002161]">
                <x-slot name="icon">
                    <svg class="w-3 h-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                </x-slot>
            </x-button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-12 gap-3 text-gray-800">
            <div class="col-span-9">
                <div x-data="app($wire)" x-init="[getNoOfDays()]" x-cloak>
                    <div class="overflow-hidden bg-white rounded-lg shadow">
                        <div class="flex items-center justify-between px-6 py-6">
                            <button type="button"
                                class="inline-flex items-center p-1 leading-none transition duration-100 ease-in-out rounded-lg cursor-pointer hover:bg-gray-200"
                                @click=" previous()">
                                <svg class="inline-flex w-6 h-6 leading-none text-gray-500" fill="none"
                                    viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M15 19l-7-7 7-7" />
                                </svg>
                            </button>
                            <div>
                                <span x-text="MONTH_NAMES[month]" class="text-lg font-semibold text-gray-600"></span>
                                <span x-text="year" class="ml-1 text-lg font-semibold text-gray-600"></span>
                            </div>
                            <button type="button"
                                class="inline-flex items-center p-1 leading-none transition duration-100 ease-in-out rounded-lg cursor-pointer hover:bg-gray-200"
                                @click=" next()">
                                <svg class="inline-flex w-6 h-6 leading-none text-gray-500" fill="none"
                                    viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M9 5l7 7-7 7" />
                                </svg>
                            </button>
                        </div>
                        <div class="-mx-1 -mb-1">
                            <div class="flex flex-wrap" style="margin-bottom: -40px;">
                                <template x-for="(day, index) in DAYS" :key="index">
                                    <div style="width: 14.26%" class="px-2 py-2">
                                        <div x-text="day"
                                            class="text-sm font-bold tracking-wide text-center text-gray-600 uppercase">
                                        </div>
                                    </div>
                                </template>
                            </div>

                            <div class="flex flex-wrap border-t border-l">
                                <template x-for="blankday in blankdays">
                                    <div style="width: 14.28%; height: 100px"
                                        class="px-4 pt-2 text-center border-b border-r">
                                    </div>
                                </template>
                                <template x-for="(date, dateIndex) in no_of_days" :key="dateIndex">
                                    <div style="width: 14.28%; height: 100px"
                                        class="relative px-4 pt-2 border-b border-r cursor-pointer"
                                        @click="showEventModal(date)"
                                        :class="{
                                            'bg-blue text-white': isToday(date) ==
                                                true,
                                            'text-blue hover:bg-blue-200/50': isToday(date) ==
                                                false
                                        }">
                                        <div x-text="date"
                                            class="inline-flex items-center justify-center w-6 h-6 text-2xl font-semibold leading-none text-center transition duration-100 ease-in-out rounded-full cursor-pointer"
                                            :class="{
                                                'bg-blue text-white': isToday(date) ==
                                                    true,
                                                'text-gray-700 hover:bg-blue-200': isToday(date) ==
                                                    false
                                            }">
                                        </div>
                                        <div style="height: 30px;" class="mt-1 overflow-hidden">
                                            <template
                                                x-for="(event, index) in events.filter(e => new Date(e.event_date_time).toDateString() ===  new Date(year, month, date).toDateString() )">
                                                <div x-show="index < 2" class="overflow-hidden capitalize">
                                                    <p x-text="event.title" class="truncate text-1xs"
                                                        :class="{ 'line-through': event.deleted_at }">>
                                                    </p>
                                                </div>
                                            </template>
                                        </div>
                                        <div x-cloak
                                            x-show="(events.filter(e => new Date(e.event_date_time).toDateString() ===  new Date(year, month, date).toDateString())).length >= 3"
                                            class="mt-1 overflow-hidden font-medium ">
                                            <p class="text-xs truncate"
                                                x-text="'+' + (events.filter(e => new Date(e.event_date_time).toDateString() ===  new Date(year, month, date).toDateString()).length - 2) + ' others'">
                                            </p>
                                        </div>
                                    </div>
                                </template>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-span-3">
                <div class="flex flex-col justify-between h-full">
                    <div>
                        <p class="text-4xl font-semibold uppercase whitespace-nowrap"><span
                                class="text-[#003399]">{{ date('F', strtotime(now())) }}</span>
                            {{ date('d', strtotime(now())) }}</p>
                        <p class="text-2xl font-semibold text-gray-700">{{ date('l', strtotime(now())) }}</p>
                    </div>
                    <div class="flex flex-col bg-white rounded-md h-60">
                        <div class="w-full overflow-y-auto">
                            <table
                                class="w-full text-xs font-light text-left border-b border-gray-900 divide-y divide-gray-900 rounded-md ">
                                <thead>
                                    <tr>
                                        <th class="sticky top-0 p-2 font-light bg-gray-200">
                                            Company Events
                                        </th>
                                        <th class="sticky top-0 p-2 font-light bg-gray-200"></th>
                                    </tr>
                                </thead>
                                <tbody class="divide-y divide-gray-900">
                                    @foreach ($calendar_event_references as $i => $calendar_event_reference)
                                        <tr class="{{ $calendar_event_reference['deleted_at'] ? 'bg-red-100' : '' }}">
                                            <td class="p-2">
                                                <p
                                                    class="font-semibold truncate w-[210px] whitespace-nowrap capitalize {{ $calendar_event_reference['deleted_at'] ? 'line-through' : '' }}">
                                                    {{ $calendar_event_reference['title'] }} |<span
                                                        class="{{ $calendar_event_reference['deleted_at'] ? 'text-black' : 'text-[#003399]' }}">
                                                        {{ date('M d', strtotime($calendar_event_reference['event_date_time'])) }}
                                                    </span>
                                                </p>
                                                <p>{{ $calendar_event_reference['description'] }}</p>
                                            </td>
                                            <td class="p-2">
                                                <div class="flex items-center justify-between space-x-3">
                                                    <div>
                                                        @if (!$calendar_event_reference['deleted_at'])
                                                            @can('hrim_calendar_edit')
                                                                <span title="Edit">
                                                                    <svg wire:click="action({id: {{ $calendar_event_reference['id'] }}}, 'edit')"
                                                                        title="Edit"
                                                                        class="w-4 h-4 text-gray-300 hover:text-blue-900 "
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 512 512">
                                                                        <path fill="currentColor"
                                                                            d="M490.3 40.4C512.2 62.27 512.2 97.73 490.3 119.6L460.3 149.7L362.3 51.72L392.4 21.66C414.3-.2135 449.7-.2135 471.6 21.66L490.3 40.4zM172.4 241.7L339.7 74.34L437.7 172.3L270.3 339.6C264.2 345.8 256.7 350.4 248.4 353.2L159.6 382.8C150.1 385.6 141.5 383.4 135 376.1C128.6 370.5 126.4 361 129.2 352.4L158.8 263.6C161.6 255.3 166.2 247.8 172.4 241.7V241.7zM192 63.1C209.7 63.1 224 78.33 224 95.1C224 113.7 209.7 127.1 192 127.1H96C78.33 127.1 64 142.3 64 159.1V416C64 433.7 78.33 448 96 448H352C369.7 448 384 433.7 384 416V319.1C384 302.3 398.3 287.1 416 287.1C433.7 287.1 448 302.3 448 319.1V416C448 469 405 512 352 512H96C42.98 512 0 469 0 416V159.1C0 106.1 42.98 63.1 96 63.1H192z" />
                                                                    </svg>
                                                                </span>
                                                            @endcan
                                                        @endif
                                                    </div>
                                                    @can('hrim_calendar_delete')
                                                        @if ($calendar_event_reference['deleted_at'])
                                                            <span title="Restore">
                                                                <svg wire:click="action({id: {{ $calendar_event_reference['id'] }}}, 'restore')"
                                                                    title="Restore"
                                                                    class="w-4 h-4 text-gray-300 hover:text-green-500"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    viewBox="0 0 448 512">
                                                                    <path fill="currentColor"
                                                                        d="M284.2 0C296.3 0 307.4 6.848 312.8 17.69L320 32H416C433.7 32 448 46.33 448 64C448 81.67 433.7 96 416 96H32C14.33 96 0 81.67 0 64C0 46.33 14.33 32 32 32H128L135.2 17.69C140.6 6.848 151.7 0 163.8 0H284.2zM31.1 128H416V448C416 483.3 387.3 512 352 512H95.1C60.65 512 31.1 483.3 31.1 448V128zM207 199L127 279C117.7 288.4 117.7 303.6 127 312.1C136.4 322.3 151.6 322.3 160.1 312.1L199.1 273.9V408C199.1 421.3 210.7 432 223.1 432C237.3 432 248 421.3 248 408V273.9L287 312.1C296.4 322.3 311.6 322.3 320.1 312.1C330.3 303.6 330.3 288.4 320.1 279L240.1 199C236.5 194.5 230.4 191.1 223.1 191.1C217.6 191.1 211.5 194.5 207 199V199z" />
                                                                </svg>
                                                            </span>
                                                        @else
                                                            <span title="Delete">
                                                                <svg wire:click="action({id: {{ $calendar_event_reference['id'] }}}, 'delete')"
                                                                    title="Delete"
                                                                    class="w-4 h-4 text-gray-300 hover:text-red-500"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    viewBox="0 0 448 512">
                                                                    <path fill="currentColor"
                                                                        d="M160 400C160 408.8 152.8 416 144 416C135.2 416 128 408.8 128 400V192C128 183.2 135.2 176 144 176C152.8 176 160 183.2 160 192V400zM240 400C240 408.8 232.8 416 224 416C215.2 416 208 408.8 208 400V192C208 183.2 215.2 176 224 176C232.8 176 240 183.2 240 192V400zM320 400C320 408.8 312.8 416 304 416C295.2 416 288 408.8 288 400V192C288 183.2 295.2 176 304 176C312.8 176 320 183.2 320 192V400zM317.5 24.94L354.2 80H424C437.3 80 448 90.75 448 104C448 117.3 437.3 128 424 128H416V432C416 476.2 380.2 512 336 512H112C67.82 512 32 476.2 32 432V128H24C10.75 128 0 117.3 0 104C0 90.75 10.75 80 24 80H93.82L130.5 24.94C140.9 9.357 158.4 0 177.1 0H270.9C289.6 0 307.1 9.358 317.5 24.94H317.5zM151.5 80H296.5L277.5 51.56C276 49.34 273.5 48 270.9 48H177.1C174.5 48 171.1 49.34 170.5 51.56L151.5 80zM80 432C80 449.7 94.33 464 112 464H336C353.7 464 368 449.7 368 432V128H80V432z" />
                                                                </svg>
                                                            </span>
                                                        @endif
                                                    @endcan
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    {{-- @foreach ($holiday_references as $x => $holiday_reference)
                                        {{ $holiday_reference['name'] }}
                                    @endforeach --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="flex flex-col bg-white rounded-md h-60">
                        <div class="flex-grow overflow-auto">
                            <table
                                class="relative w-full text-xs font-light text-left border-b border-gray-900 divide-y divide-gray-900 rounded-md">
                                <thead>
                                    <tr>
                                        <th class="sticky top-0 p-2 font-light bg-gray-200">Birthdays</th>
                                        <th
                                            class="sticky top-0 p-2 font-light text-right text-blue-900 underline bg-gray-200">
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="divide-y divide-gray-900">
                                    @foreach ($birth_month_references as $birth_month_reference)
                                        <tr>
                                            <td class="p-2 capitalize">
                                                <p class="font-semibold">
                                                    {{ $birth_month_reference->first_name . ' ' . $birth_month_reference->last_name }}
                                                </p>
                                                <p>{{ date('F d', strtotime($birth_month_reference->birth_date)) }}
                                                </p>
                                            </td>
                                            <td class="p-2 whitespace-nowrap">
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="flex flex-col bg-white rounded-md h-60">
                        <div class="flex-grow overflow-auto">
                            <table
                                class="relative w-full text-xs font-light text-left border-b border-gray-900 divide-y divide-gray-900 rounded-md">
                                <thead>
                                    <tr>
                                        <th class="sticky top-0 p-2 font-light bg-gray-200">Holidays</th>
                                        <th
                                            class="sticky top-0 p-2 font-light text-right text-blue-900 underline bg-gray-200">
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="divide-y divide-gray-900">
                                    @foreach ($holiday_references as $holiday_reference)
                                        <tr>
                                            <td class="p-2">
                                                <p class="font-semibold">
                                                    {{ $holiday_reference->name }}
                                                </p>
                                                <p>{{ date('F d', strtotime($holiday_reference->date)) }}
                                                </p>
                                            </td>
                                            <td class="p-2 whitespace-nowrap">
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-form>

@push('scripts')
    <script>
        const MONTH_NAMES = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
            'September',
            'October', 'November', 'December'
        ];
        const DAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

        function app($wire) {
            return {
                month: $wire.month - 1,
                year: $wire.year,
                no_of_days: [],
                blankdays: [],
                days: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                events: @entangle('calendar_event_references'),
                themes: [{
                        value: "blue",
                        label: "Blue Theme"
                    },
                    {
                        value: "red",
                        label: "Red Theme"
                    },
                    {
                        value: "yellow",
                        label: "Yellow Theme"
                    },
                    {
                        value: "green",
                        label: "Green Theme"
                    },
                    {
                        value: "purple",
                        label: "Purple Theme"
                    }
                ],

                isToday(date) {
                    const today = new Date();
                    const d = new Date(this.year, this.month, date);

                    return today.toDateString() === d.toDateString() ? true : false;
                },

                showEventModal(date) {
                    let check_events = this.events.filter(e => new Date(e.event_date_time).toDateString() === new Date(this
                        .year, this.month, date).toDateString());
                    if (check_events.length) {
                        $wire.action({
                            date: new Date(this.year, this.month, date).toDateString()
                        }, 'view')
                    }
                },

                getNoOfDays() {
                    let daysInMonth = new Date(this.year, this.month + 1, 0).getDate();

                    // find where to start calendar day of week
                    let dayOfWeek = new Date(this.year, this.month).getDay();
                    let blankdaysArray = [];
                    for (var i = 1; i <= dayOfWeek; i++) {
                        blankdaysArray.push(i);
                    }

                    let daysArray = [];
                    for (var i = 1; i <= daysInMonth; i++) {
                        daysArray.push(i);
                    }

                    this.blankdays = blankdaysArray;
                    this.no_of_days = daysArray;
                },

                previous() {
                    this.month--;
                    if (this.month < 0) {
                        this.month = 11;
                        this.year -= 1;
                    }
                    $wire.monthCalendarEvent(new Date(this.year, this.month).toDateString())
                    this.getNoOfDays();
                },

                next() {
                    this.month++;
                    if (this.month > 11) {
                        this.month = 0;
                        this.year += 1;
                    }
                    $wire.monthCalendarEvent(new Date(this.year, this.month).toDateString())
                    this.getNoOfDays();
                },
            }
        }
    </script>
@endpush
