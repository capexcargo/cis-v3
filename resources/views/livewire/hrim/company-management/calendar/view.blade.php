<div x-data="{ confirmation_modal: '{{ $confirmation_modal }}', edit_modal: '{{ $edit_modal }}' }">
    <x-loading />
    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <div class="space-y-3">
                <p class="text-center">{{ $confirmation_message }}</p>
                <div class="flex items-center justify-center space-x-3">
                    <x-button type="button" wire:click="$set('confirmation_modal', false)" title="No"
                        class="py-1 bg-white text-blue hover:bg-gray-100" />
                    <x-button type="button" wire:click="confirm" title="Yes"
                        class="bg-blue text-white hover:bg-[#002161] py-1" />
                </div>
            </div>
        </x-slot>
    </x-modal>
    @can('accounting_account_management_add')
        @if ($edit_modal && $calendar_id)
            <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Edit Event</x-slot>
                <x-slot name="body">
                    @livewire('hrim.company-management.calendar.edit', ['id' => $calendar_id])
                </x-slot>
            </x-modal>
        @endif
    @endcan
    <x-table.table>
        <x-slot name="thead">
            {{-- <x-table.th name="Type" /> --}}
            <x-table.th class="w-[15%]" name="Title" />
            <x-table.th class="w-[20%]" name="Date and Time" />
            <x-table.th class="w-[20%]" name="Created On" />
            <x-table.th class="w-[10%]" name="Branch" />
            <x-table.th class="w-[25%]" name="Description" />
            @can('hrim_calendar_action')
                <x-table.th class="w-[10%]" name="Action" />
            @endcan
        </x-slot>
        <x-slot name="tbody">
            @foreach ($calendar_event_references as $calendar_event_reference)
                <tr
                    class="{{ $calendar_event_reference->deleted_at ? 'bg-red-100' : '' }} border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    {{-- <td class="p-3 whitespace-nowrap">
                        {{ $calendar_event_reference->eventType->display }}
                    </td> --}}
                    <td class="p-3 whitespace-nowrap w-[15%]">
                        <p class="truncate w-[150px]">{{ $calendar_event_reference->title }}</p>
                    </td>
                    <td class="p-3 whitespace-nowrap w-[20%]">
                        {{ date('M. d, Y h:i A', strtotime($calendar_event_reference->event_date_time)) }}
                    </td>
                    <td class="p-3 whitespace-nowrap w-[20%]">
                        {{ date('M. d, Y h:i A', strtotime($calendar_event_reference->created_at)) }}
                    </td>
                    <td class="p-3 whitespace-nowrap w-[10%]">
                        {{ $calendar_event_reference->branch->display }}
                    </td>
                    <td class="p-3 whitespace-nowrap w-[25%]">
                        <p class="truncate w-[180px]">{{ $calendar_event_reference->description }}</p>
                    </td>
                    @can('hrim_calendar_action')
                        <td class="p-3 whitespace-nowrap w-[10%]">
                            <div class="flex items-center justify-between space-x-3">
                                <div>
                                    @if (!$calendar_event_reference['deleted_at'])
                                        @can('hrim_calendar_edit')
                                            <svg wire:click="action({id: {{ $calendar_event_reference->id }}}, 'edit')"
                                                data-toggle="tooltip" title="Edit"
                                                class="w-4 h-4 text-gray-300 hover:text-blue-900 "
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M490.3 40.4C512.2 62.27 512.2 97.73 490.3 119.6L460.3 149.7L362.3 51.72L392.4 21.66C414.3-.2135 449.7-.2135 471.6 21.66L490.3 40.4zM172.4 241.7L339.7 74.34L437.7 172.3L270.3 339.6C264.2 345.8 256.7 350.4 248.4 353.2L159.6 382.8C150.1 385.6 141.5 383.4 135 376.1C128.6 370.5 126.4 361 129.2 352.4L158.8 263.6C161.6 255.3 166.2 247.8 172.4 241.7V241.7zM192 63.1C209.7 63.1 224 78.33 224 95.1C224 113.7 209.7 127.1 192 127.1H96C78.33 127.1 64 142.3 64 159.1V416C64 433.7 78.33 448 96 448H352C369.7 448 384 433.7 384 416V319.1C384 302.3 398.3 287.1 416 287.1C433.7 287.1 448 302.3 448 319.1V416C448 469 405 512 352 512H96C42.98 512 0 469 0 416V159.1C0 106.1 42.98 63.1 96 63.1H192z" />
                                            </svg>
                                        @endcan
                                    @endif
                                </div>
                                @can('hrim_calendar_delete')
                                    @if ($calendar_event_reference['deleted_at'])
                                        <svg wire:click="action({id: {{ $calendar_event_reference->id }}}, 'restore')"
                                            data-toggle="tooltip" title="Restore"
                                            class="w-4 h-4 text-gray-300 hover:text-green-500"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M284.2 0C296.3 0 307.4 6.848 312.8 17.69L320 32H416C433.7 32 448 46.33 448 64C448 81.67 433.7 96 416 96H32C14.33 96 0 81.67 0 64C0 46.33 14.33 32 32 32H128L135.2 17.69C140.6 6.848 151.7 0 163.8 0H284.2zM31.1 128H416V448C416 483.3 387.3 512 352 512H95.1C60.65 512 31.1 483.3 31.1 448V128zM207 199L127 279C117.7 288.4 117.7 303.6 127 312.1C136.4 322.3 151.6 322.3 160.1 312.1L199.1 273.9V408C199.1 421.3 210.7 432 223.1 432C237.3 432 248 421.3 248 408V273.9L287 312.1C296.4 322.3 311.6 322.3 320.1 312.1C330.3 303.6 330.3 288.4 320.1 279L240.1 199C236.5 194.5 230.4 191.1 223.1 191.1C217.6 191.1 211.5 194.5 207 199V199z" />
                                        </svg>
                                    @else
                                        <svg wire:click="action({id: {{ $calendar_event_reference->id }}}, 'delete')"
                                            data-toggle="tooltip" title="Delete"
                                            class="w-4 h-4 text-gray-300 hover:text-red-500" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M160 400C160 408.8 152.8 416 144 416C135.2 416 128 408.8 128 400V192C128 183.2 135.2 176 144 176C152.8 176 160 183.2 160 192V400zM240 400C240 408.8 232.8 416 224 416C215.2 416 208 408.8 208 400V192C208 183.2 215.2 176 224 176C232.8 176 240 183.2 240 192V400zM320 400C320 408.8 312.8 416 304 416C295.2 416 288 408.8 288 400V192C288 183.2 295.2 176 304 176C312.8 176 320 183.2 320 192V400zM317.5 24.94L354.2 80H424C437.3 80 448 90.75 448 104C448 117.3 437.3 128 424 128H416V432C416 476.2 380.2 512 336 512H112C67.82 512 32 476.2 32 432V128H24C10.75 128 0 117.3 0 104C0 90.75 10.75 80 24 80H93.82L130.5 24.94C140.9 9.357 158.4 0 177.1 0H270.9C289.6 0 307.1 9.358 317.5 24.94H317.5zM151.5 80H296.5L277.5 51.56C276 49.34 273.5 48 270.9 48H177.1C174.5 48 171.1 49.34 170.5 51.56L151.5 80zM80 432C80 449.7 94.33 464 112 464H336C353.7 464 368 449.7 368 432V128H80V432z" />
                                        </svg>
                                    @endif
                                @endcan
                            </div>
                        </td>
                    @endcan
                </tr>
            @endforeach
        </x-slot>
    </x-table.table>
</div>
