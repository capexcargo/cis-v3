<div wire:init="load">
    <x-loading />
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            {{-- <div>
                <x-label value="Event Type" :required="true"/>
                <x-select name="event_type" wire:model.defer='event_type'>
                    <option value=""></option>
                    @foreach ($event_type_references as $event_type_reference)
                        <option value="{{ $event_type_reference->id }}">
                            {{ $event_type_reference->display }}
                        </option>
                    @endforeach
                </x-select>
                <x-input-error for="event_type" />
            </div> --}}
            <div>
                <x-label value="Event Title" :required="true"/>
                <x-input type="text" name="event_title" wire:model.debounce.500ms='event_title' />
                <x-input-error for="event_title" />
            </div>
            <div>
                <x-label value="Date And Time" :required="true"/>
                <x-input type="datetime-local" name="date_and_time" wire:model.debounce.500ms='date_and_time' />
                <x-input-error for="date_and_time" />
            </div>
            <div>
                <x-label value="Branch" :required="true"/>
                <x-select name="branch" wire:model='branch'>
                    <option value=""></option>
                    @foreach ($branch_references as $branch_reference)
                        <option value="{{ $branch_reference->id }}">
                            {{ $branch_reference->display }}
                        </option>
                    @endforeach
                </x-select>
                <x-input-error for="branch" />
            </div>
            <div>
                <x-label value="Description" :required="true"/>
                <x-textarea name="description" wire:model.debounce.500ms='description' rows="5"/>
                <x-input-error for="description" />
            </div>
            <div class="flex justify-end space-x-3">
                <x-button type="button" wire:click="$emit('close_modal', 'edit')" title="Cancel"
                    class="bg-white text-blue hover:bg-gray-100" />
                <x-button type="submit" title="Save" class="bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>
    </form>
</div>
