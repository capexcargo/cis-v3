<div wire:init="load" x-data="{ message_modal: '{{ $message_modal }}' }">
    <x-loading />

    <div wire:key="message_modal" x-show="message_modal" x-cloak
        class="fixed inset-0 z-40 w-full bg-gray-900 bg-opacity-60"
        @keydown.escape.window="$wire.set('message_modal', false)">
        <div class="flex items-center justify-center h-screen py-10 overflow-auto">
            <div class="relative w-11/12 p-4 m-auto bg-white rounded-md shadow-md xs:w-11/12 sm:w-11/12 md:w-1/4">
                <div class="mt-3">
                    <h2 class="mb-3 text-2xl font-normal text-center">
                        {{ $message }}
                    </h2>

                    <div class="flex justify-center mt-6 space-x-6">
                        <button type="button" wire:click="okAction()"
                            class="px-10 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                            OK
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            {{-- <div>
                <x-label value="Event Type" :required="true"/>
                <x-select name="event_type" wire:model.defer='event_type'>
                    <option value=""></option>
                    @foreach ($event_type_references as $event_type_reference)
                        <option value="{{ $event_type_reference->id }}">
                            {{ $event_type_reference->display }}
                        </option>
                    @endforeach
                </x-select>
                <x-input-error for="event_type" />
            </div> --}}
            <div>
                <x-label value="Event Title" :required="true" />
                <x-input type="text" name="event_title" wire:model.defer='event_title' />
                <x-input-error for="event_title" />
            </div>
            <div>
                <x-label value="Date And Time" :required="true" />
                <x-input type="datetime-local" name="date_and_time" wire:model.defer='date_and_time' />
                <x-input-error for="date_and_time" />
            </div>
            <div>
                <x-label value="Branch" :required="true" />
                <x-select name="branch" wire:model.defer='branch'>
                    <option value=""></option>
                    @foreach ($branch_references as $branch_reference)
                        <option value="{{ $branch_reference->id }}">
                            {{ $branch_reference->display }}
                        </option>
                    @endforeach
                </x-select>
                <x-input-error for="branch" />
            </div>
            <div>
                <x-label value="Description" :required="true" />
                <x-textarea name="description" wire:model.defer='description' rows="5" />
                <x-input-error for="description" />
            </div>
            <div class="flex justify-end space-x-3">
                <x-button type="button" wire:click="$emit('close_modal', 'create')" title="Cancel"
                    class="bg-white text-blue hover:bg-gray-100" />
                <x-button type="submit" title="Save" class="bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>
    </form>
</div>
