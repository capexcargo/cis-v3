<x-form>
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="header_title">Table of Organization</x-slot>
    <x-slot name="header_button">
        <x-button type="button" wire:click="$set('create_modal', true)" title="Update Table Of Organization"
            class="bg-blue text-white hover:bg-[#002161]">
            <x-slot name="icon">
                <svg class="w-3 h-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path fill="currentColor"
                        d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                </svg>
            </x-slot>
        </x-button>
    </x-slot>
    <x-slot name="body">
        <img src="{{ asset('images/table-of-organization.jpg') }}" alt="table-of-organization">
    </x-slot>
</x-form>
