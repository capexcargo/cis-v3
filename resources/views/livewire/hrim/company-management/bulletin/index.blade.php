<x-form x-data="{
    searh_form: false,
    confirmation_modal: '{{ $confirmation_modal }}',
    delete_announcement_modal: '{{ $delete_announcement_modal }}',
    publish_announcement_modal: '{{ $publish_announcement_modal }}',
    create_announcement_modal: '{{ $create_announcement_modal }}',
    edit_announcement_modal: '{{ $edit_announcement_modal }}',
    view_announcement_modal: '{{ $view_announcement_modal }}',
    delete_memo_modal: '{{ $delete_memo_modal }}',
    create_memo_modal: '{{ $create_memo_modal }}',
    edit_memo_modal: '{{ $edit_memo_modal }}',
    view_memo_modal: '{{ $view_memo_modal }}'
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">

        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
            <x-slot name="body">
                <h2 class="mb-3 text-lg text-center text-gray-900">
                    {{ $confirmation_message }}
                </h2>
                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click="confirm"
                        class="px-5 py-1 mt-4 text-sm flex-none bg-[#003399] text-white rounded-lg">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>

        <x-modal id="publish_announcement_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
            <x-slot name="body">
                <h2 class="mb-3 text-lg text-center text-gray-900">
                    {{ $confirmation_message_publish }}
                </h2>
                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('publish_announcement_modal', false)"
                        class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click=""
                        class="px-5 py-1 mt-4 text-sm flex-none bg-[#003399] text-white rounded-lg">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>

        @can('hrim_announcement_add')
            <x-modal id="create_announcement_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="title">Create Announcement</x-slot>
                <x-slot name="body">
                    @livewire('hrim.company-management.bulletin.announcement.create')
                </x-slot>
            </x-modal>
        @endcan

        @can('hrim_announcement_edit')
            @if ($announcement_id && $edit_announcement_modal)
                <x-modal id="edit_announcement_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                    <x-slot name="title">Edit Announcement</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.company-management.bulletin.announcement.edit', ['id' => $announcement_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('hrim_announcement_view')
            @if ($announcement_no && $view_announcement_modal)
                <x-modal id="view_announcement_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                    <x-slot name="body">
                        @livewire('hrim.company-management.bulletin.announcement.view', ['announcement_no' => $announcement_no])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('hrim_memo_add')
            <x-modal id="create_memo_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="title">Create Memo</x-slot>
                <x-slot name="body">
                    @livewire('hrim.company-management.bulletin.memo.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_memo_edit')
            @if ($memo_id && $edit_memo_modal)
                <x-modal id="edit_memo_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                    <x-slot name="title">Edit Memo</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.company-management.bulletin.memo.edit', ['id' => $memo_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @if ($memorandum_no && $view_memo_modal)
            <x-modal id="view_memo_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                <x-slot name="body">
                    @livewire('hrim.company-management.bulletin.memo.view', ['memorandum_no' => $memorandum_no])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">Company Bulletin</x-slot>
    <x-slot name="header_button">
        @can('hrim_announcement_add')
            <button wire:click="action({}, 'create_announcement')"
                class="mr-3 px-3 p-2 text-sm bg-[#003399] text-white rounded-md">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Create Announcement
                </div>
            </button>
        @endcan
        @can('hrim_memo_add')
            <button wire:click="action({}, 'create_memo')"
                class="mr-3 px-3 p-2 text-sm bg-white text-[#003399] rounded-md border border-blue-800 border-solid">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    New Memo
                </div>
            </button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-12 gap-6">
            <div class="col-span-8">
                @can('hrim_announcement_view')
                    <div class="bg-gray-100 border-2 border-gray-300 rounded-lg">
                        <div class="min-w-0 p-3 px-6 text-left">
                            <div class="">
                                <h4 class="text-xl font-semibold text-gray-800">
                                    Announcements
                                </h4>
                            </div>
                        </div>
                        <div></div>
                    </div>
                    <div class="bg-white shadow-xs ">
                        <div class="min-w-0 bg-white shadow-lg">
                            @foreach ($announcements as $announcement)
                                @if ($announcement->announcement_no)
                                    <div class="relative px-6 py-3 border border-gray-500 for-hover">
                                        <div class="absolute right-0 flex mr-2 space-x-2 top-2">
                                            @can('hrim_announcement_edit')
                                                <svg wire:click="action({'id': {{ $announcement->id }}}, 'edit_announcement')"
                                                    data-toggle="tooltip"
                                                    class="w-4 h-4 cursor-pointer text-gray-400 hover:text-[#003399]"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                    <path fill="currentColor"
                                                        d="M490.3 40.4C512.2 62.27 512.2 97.73 490.3 119.6L460.3 149.7L362.3 51.72L392.4 21.66C414.3-.2135 449.7-.2135 471.6 21.66L490.3 40.4zM172.4 241.7L339.7 74.34L437.7 172.3L270.3 339.6C264.2 345.8 256.7 350.4 248.4 353.2L159.6 382.8C150.1 385.6 141.5 383.4 135 376.1C128.6 370.5 126.4 361 129.2 352.4L158.8 263.6C161.6 255.3 166.2 247.8 172.4 241.7V241.7zM192 63.1C209.7 63.1 224 78.33 224 95.1C224 113.7 209.7 127.1 192 127.1H96C78.33 127.1 64 142.3 64 159.1V416C64 433.7 78.33 448 96 448H352C369.7 448 384 433.7 384 416V319.1C384 302.3 398.3 287.1 416 287.1C433.7 287.1 448 302.3 448 319.1V416C448 469 405 512 352 512H96C42.98 512 0 469 0 416V159.1C0 106.1 42.98 63.1 96 63.1H192z" />
                                                </svg>
                                            @endcan
                                            @can('hrim_announcement_delete')
                                                <svg wire:click="action({'id': {{ $announcement->id }}}, 'delete_announcement')"
                                                    data-toggle="tooltip"
                                                    class="w-4 h-4 text-gray-400 cursor-pointer hover:text-red-500"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                    <path fill="currentColor"
                                                        d="M160 400C160 408.8 152.8 416 144 416C135.2 416 128 408.8 128 400V192C128 183.2 135.2 176 144 176C152.8 176 160 183.2 160 192V400zM240 400C240 408.8 232.8 416 224 416C215.2 416 208 408.8 208 400V192C208 183.2 215.2 176 224 176C232.8 176 240 183.2 240 192V400zM320 400C320 408.8 312.8 416 304 416C295.2 416 288 408.8 288 400V192C288 183.2 295.2 176 304 176C312.8 176 320 183.2 320 192V400zM317.5 24.94L354.2 80H424C437.3 80 448 90.75 448 104C448 117.3 437.3 128 424 128H416V432C416 476.2 380.2 512 336 512H112C67.82 512 32 476.2 32 432V128H24C10.75 128 0 117.3 0 104C0 90.75 10.75 80 24 80H93.82L130.5 24.94C140.9 9.357 158.4 0 177.1 0H270.9C289.6 0 307.1 9.358 317.5 24.94H317.5zM151.5 80H296.5L277.5 51.56C276 49.34 273.5 48 270.9 48H177.1C174.5 48 171.1 49.34 170.5 51.56L151.5 80zM80 432C80 449.7 94.33 464 112 464H336C353.7 464 368 449.7 368 432V128H80V432z" />
                                                </svg>
                                            @endcan
                                        </div>
                                        <div class="items-start cursor-pointer"
                                            wire:click="action({'announcement_no': '{{ $announcement->announcement_no }}'}, 'view_announcement')">
                                            <h1 class="mb-1 text-xl font-semibold text-blue-600">
                                                {{ $announcement->title }}
                                            </h1>
                                            <p class="mb-3 text-gray-600 text-md dark:text-gray-400">
                                                {{ $announcement->details }}
                                            </p>

                                            <table class="table-auto">
                                                <tbody class="text-sm font-light text-gray-900">
                                                    <tr>
                                                        <td><span class="mr-2">Announcement #: </span></td>
                                                        <td class="mr-2"><span
                                                                class="mr-2 font-semibold">{{ $announcement->announcement_no }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><span class="mr-2">Date Posted: </span></td>
                                                        <td class="mr-2"><span
                                                                class="mr-2 font-semibold">{{ $announcement->date_posted }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><span class="mr-2">Posted By: </span></td>
                                                        <td class="mr-2"><span
                                                                class="mr-2 font-semibold">{{ $announcement->postedBy->name }}</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    {{ $announcements->links() }}
                @else
                    unauthorized
                @endcan
            </div>
            <div class="col-span-4">
                @can('hrim_memo_view')
                    <div class="bg-gray-100 border-2 border-gray-300 rounded-lg shadow-xs">
                        <div class="min-w-0 p-3 px-6 text-left">
                            <div class="">
                                <h4 class="text-xl font-semibold text-gray-800">
                                    Memos
                                </h4>
                            </div>
                        </div>
                    </div>
                    @foreach ($memos as $memo)
                        @if ($memo->memorandum_no)
                            <div class="px-6 py-4 mb-3 bg-white rounded-lg shadow-lg for-hover">
                                <div class="items-start cursor-pointer"
                                    wire:click="action({'memorandum_no': '{{ $memo->memorandum_no }}'}, 'view_memo')">
                                    <h1 class="mb-1 text-xl font-semibold text-blue-600">
                                        {{ $memo->title }}
                                    </h1>

                                    <table class="table-auto">
                                        <tbody class="text-sm font-light text-gray-900">
                                            <tr>
                                                <td><span class="mr-2">Memorandum No: </span></td>
                                                <td class="mr-2"><span
                                                        class="mr-2 font-semibold uppercase">{{ $memo->memorandum_no }}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="mr-2">Date Posted: </span></td>
                                                <td class="mr-2"><span
                                                        class="mr-2 font-semibold">{{ $memo->date_posted }}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="mr-2">Attention To: </span></td>
                                                <td class="mr-2"><span class="mr-2 font-semibold">All
                                                        Employees</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="flex justify-end -mr-2 space-x-2">
                                    @can('hrim_memo_edit')
                                        <svg wire:click="action({'id': {{ $memo->id }}}, 'edit_memo')"
                                            data-toggle="tooltip"
                                            class="w-4 h-4 cursor-pointer text-gray-400 hover:text-[#003399] "
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                            <path fill="currentColor"
                                                d="M490.3 40.4C512.2 62.27 512.2 97.73 490.3 119.6L460.3 149.7L362.3 51.72L392.4 21.66C414.3-.2135 449.7-.2135 471.6 21.66L490.3 40.4zM172.4 241.7L339.7 74.34L437.7 172.3L270.3 339.6C264.2 345.8 256.7 350.4 248.4 353.2L159.6 382.8C150.1 385.6 141.5 383.4 135 376.1C128.6 370.5 126.4 361 129.2 352.4L158.8 263.6C161.6 255.3 166.2 247.8 172.4 241.7V241.7zM192 63.1C209.7 63.1 224 78.33 224 95.1C224 113.7 209.7 127.1 192 127.1H96C78.33 127.1 64 142.3 64 159.1V416C64 433.7 78.33 448 96 448H352C369.7 448 384 433.7 384 416V319.1C384 302.3 398.3 287.1 416 287.1C433.7 287.1 448 302.3 448 319.1V416C448 469 405 512 352 512H96C42.98 512 0 469 0 416V159.1C0 106.1 42.98 63.1 96 63.1H192z" />
                                        </svg>
                                    @endcan
                                    @can('hrim_announcement_delete')
                                        <svg wire:click="action({'id': {{ $memo->id }}}, 'delete_memo')"
                                            data-toggle="tooltip"
                                            class="w-4 h-4 text-gray-400 cursor-pointer hover:text-red-500"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M160 400C160 408.8 152.8 416 144 416C135.2 416 128 408.8 128 400V192C128 183.2 135.2 176 144 176C152.8 176 160 183.2 160 192V400zM240 400C240 408.8 232.8 416 224 416C215.2 416 208 408.8 208 400V192C208 183.2 215.2 176 224 176C232.8 176 240 183.2 240 192V400zM320 400C320 408.8 312.8 416 304 416C295.2 416 288 408.8 288 400V192C288 183.2 295.2 176 304 176C312.8 176 320 183.2 320 192V400zM317.5 24.94L354.2 80H424C437.3 80 448 90.75 448 104C448 117.3 437.3 128 424 128H416V432C416 476.2 380.2 512 336 512H112C67.82 512 32 476.2 32 432V128H24C10.75 128 0 117.3 0 104C0 90.75 10.75 80 24 80H93.82L130.5 24.94C140.9 9.357 158.4 0 177.1 0H270.9C289.6 0 307.1 9.358 317.5 24.94H317.5zM151.5 80H296.5L277.5 51.56C276 49.34 273.5 48 270.9 48H177.1C174.5 48 171.1 49.34 170.5 51.56L151.5 80zM80 432C80 449.7 94.33 464 112 464H336C353.7 464 368 449.7 368 432V128H80V432z" />
                                        </svg>
                                    @endcan
                                </div>
                            </div>
                        @endif
                    @endforeach
                    {{ $memos->links() }}
                @else
                    unauthorized
                @endcan
            </div>
        </div>
    </x-slot>
</x-form>

@push('heads')
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />

    <style>
        .for-hover:hover {
            border: 2px solid #003399;
        }
    </style>
@endpush

@push('scripts')
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script>
        var swiper = new Swiper(".mySwiper", {
            cssMode: true,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            pagination: {
                el: ".swiper-pagination",
            },
            mousewheel: true,
            keyboard: true,
        });
    </script>
@endpush
