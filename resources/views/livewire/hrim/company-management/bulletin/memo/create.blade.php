<div {{-- wire:init="load" --}} x-data="{
    confirmation_modal: '{{ $confirmation_modal }}'
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
        <x-slot name="body">
            <h2 class="mb-3 text-lg text-center text-gray-900">
                {{ $confirmation_message }}
            </h2>
            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                <button type="button" wire:click="confirm"
                    class="px-5 py-1 mt-4 text-sm flex-none bg-[#003399] text-white rounded-lg">
                    Yes</button>
            </div>
        </x-slot>
    </x-modal>

    <form autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="memorandum_no" value="Memorandum No." :required="true" />
                    <input type="text" name="memorandum_no" wire:model.defer='memorandum_no'
                        class="uppercase text-gray-600 block w-full 
                        border p-[5px] rounded-md shadow-sm 
                        focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50">
                    <x-input-error for="memorandum_no" />
                </div>
                <div>
                    <x-label for="attention_to" value="Attention To" :required="true" />
                    <x-select name="attention_to" wire:model='attention_to' disabled>
                        <option value="">All Employees</option>
                    </x-select>
                    <x-input-error for="attention_to" />
                </div>
                <div>
                    <x-label for="title" value="Title" :required="true" />
                    <x-input type="text" name="title" wire:model.defer='title'></x-input>
                    <x-input-error for="title" />
                </div>
                <div>
                    <x-label for="details" value="Brief Description" :required="true" />
                    <x-textarea type="text" name="details" wire:model.defer='details'></x-textarea>
                    <x-input-error for="details" />
                </div>
                {{-- <div>
                    <div class="text-2xl font-bold text-blue">
                        <div class="flex items-center space-x-3">
                            <x-label for="attachment" value="Attachment" :required="true" />
                        </div>
                    </div>
                    <div class="grid gap-2">
                        <div class="flex flex-col space-y-3">
                            <x-table.table>
                                <x-slot name="thead">
                                </x-slot>
                                <x-slot name="tbody">
                                    @forelse ($attachments as $i => $attachment)
                                        <tr
                                            class="text-xs border-0 cursor-pointer even:bg-white hover:text-white hover:bg-blue-200">
                                            <td class="flex p-1 items-left justify-left whitespace-nowrap">
                                                <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                    <div class="relative z-0">
                                                        <input type="file"
                                                            name="attachments.{{ $i }}.attachment"
                                                            wire:model="attachments.{{ $i }}.attachment"
                                                            class="absolute top-0 left-0 z-50 opacity-0">

                                                        @if ($attachments[$i]['attachment'])
                                                            <label for="attachments.{{ $i }}.attachment"
                                                                class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 rounded-lg cursor-pointer">
                                                                <span class="flex gap-1">
                                                                    <svg class="w-3 h-3" aria-hidden="true"
                                                                        focusable="false" data-prefix="fas"
                                                                        data-icon="file-alt" role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                        </path>
                                                                    </svg>{{ $attachments[$i]['attachment']->getClientOriginalName() }}
                                                                </span>
                                                            </label>
                                                        @else
                                                            <label for="attachments.{{ $i }}.attachment"
                                                                class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 rounded-lg cursor-pointer">
                                                                <span class="flex gap-1">
                                                                    <svg class="w-3 h-3" aria-hidden="true"
                                                                        focusable="false" data-prefix="fas"
                                                                        data-icon="file-alt" role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                        </path>
                                                                    </svg>Choose Attachment
                                                                </span>
                                                            </label>
                                                        @endif

                                                        <x-input-error
                                                            for="attachments.{{ $i }}.attachment" />
                                                    </div>

                                                </div>
                                            </td>
                                            <td class="p-2 whitespace-nowrap">
                                                @if (count($attachments) > 1)
                                                    <a wire:click="removeAttachments({{ $i }})">
                                                        <p class="text-red-600 underline underline-offset-2">Remove</p>
                                                    </a>
                                                    <svg hidden wire:click="removeAttachments({{ $i }})"
                                                        class="w-5 text-red" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="times-circle" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                            d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                        </path>
                                                    </svg>
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3">
                                                <p class="text-center">Empty.</p>
                                            </td>
                                        </tr>
                                    @endforelse
                                </x-slot>
                            </x-table.table>
                            <div class="flex items-center justify-start">
                                <button type="button" title="Add Attachment" wire:click="addAttachments"
                                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                    +</button>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div>
                    <div class="text-2xl font-bold text-blue">
                        <div class="flex items-center space-x-3">
                            <x-label for="attachment" value="Attachment" :required="true" />
                        </div>
                    </div>
                    <div class="grid gap-2">
                        <div class="flex flex-col overflow-hidden">
                            <table class="w-full">
                                <thead>
                                </thead>
                                <tbody>
                                    @forelse ($attachments as $i => $attachment)
                                        <tr class="text-xs border-0 cursor-pointer even:bg-white">
                                            <td class="flex p-1 items-left justify-left whitespace-nowrap">
                                                <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap ">
                                                    <div class="relative z-0">
                                                        <input type="file"
                                                            name="attachments.{{ $i }}.attachment"
                                                            wire:model="attachments.{{ $i }}.attachment"
                                                            class="absolute top-0 left-0 z-50 opacity-0">

                                                        @if ($attachments[$i]['attachment'])
                                                            <label for="attachments.{{ $i }}.attachment"
                                                                class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 rounded-lg cursor-pointer">
                                                                <span class="flex gap-1">
                                                                    <svg class="w-3 h-3" aria-hidden="true"
                                                                        focusable="false" data-prefix="fas"
                                                                        data-icon="file-alt" role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                        </path>
                                                                    </svg>{{ $attachments[$i]['attachment']->getClientOriginalName() }}
                                                                </span>
                                                            </label>
                                                        @else
                                                            <label for="attachments.{{ $i }}.attachment"
                                                                class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 rounded-lg cursor-pointer">
                                                                <span class="flex gap-1">
                                                                    <svg class="w-3 h-3" aria-hidden="true"
                                                                        focusable="false" data-prefix="fas"
                                                                        data-icon="file-alt" role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                        </path>
                                                                    </svg>Choose Attachment
                                                                </span>
                                                            </label>
                                                        @endif

                                                        <x-input-error
                                                            for="attachments.{{ $i }}.attachment" />
                                                    </div>

                                                </div>
                                            </td>
                                            <td class="p-2 whitespace-nowrap">
                                                @if (count($attachments) > 1)
                                                    <a wire:click="removeAttachments({{ $i }})">
                                                        <p class="text-red-600 underline underline-offset-2">Remove</p>
                                                    </a>
                                                    <svg hidden wire:click="removeAttachments({{ $i }})"
                                                        class="w-5 text-red" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="times-circle" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                            d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                        </path>
                                                    </svg>
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3">
                                                <p class="text-center">Empty.</p>
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            <div class="flex items-center justify-start">
                                <button type="button" title="Add Attachment" wire:click="addAttachments"
                                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                    +</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    Cancel
                </button>
                <button type="button" wire:click="action({}, 'submit')"
                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                    Submit
                </button>
            </div>
        </div>
    </form>
</div>

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
    <script>
        jQuery(document).ready(function() {
            $('form input').focus(function() {
                $(this).siblings(".text-red-600").hide();
            });

            $('form textarea').focus(function() {
                $(this).siblings(".text-red-600").hide();
            });
        });
    </script>
@endpush
