<table>
    <thead>
        <tr>
            <div class="min-w-0 p-4 bg-white shadow-xs rounded-xs dark:bg-gray-800">
                <h4 class="font-semibold text-gray-600 dark:text-gray-300">
                    Memos
                </h4>
            </div>
        </tr>
    </thead>
    <tbody>
        @foreach ($memos as $memo)
            <tr>
                <td>
                    <div class="min-w-0 p-4 bg-white shadow-xs rounded-sx dark:bg-gray-800">
                        <h4 class="mb-3 font-bold text-gray-900 dark:text-gray-300">
                            <div class="">{{ $memo->title }}</div>
                        </h4>
                        <h4 class="mb-3 font-light text-gray-900 dark:text-gray-300">
                            <span class="text-sm">Memo No. : </span><span
                                class="font-bold">{{ $memo->memorandum_no }}</span>
                        </h4>
                        <h4 class="mb-3 font-light text-gray-900 dark:text-gray-300">
                            <span class="text-sm">Date Posted : </span><span
                                class="font-bold">{{ $memo->date_posted }}</span>
                        </h4>
                        <h4 class="mb-3 font-bold text-gray-900 dark:text-gray-300">
                            <span class="text-sm">Attention To : </span><span
                                class="font-bold">{{ $memo->attention_to }}</span>
                        </h4>
                        <p class="text-sm text-gray-600 dark:text-gray-400">
                            {{ $memo->details }}
                        </p>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
