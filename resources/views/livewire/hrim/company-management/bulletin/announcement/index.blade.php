<div x-data="{ searh_form: false, delete_announcement_modal: '{{ $delete_announcement_modal }}', create_announcement_modal: '{{ $create_announcement_modal }}', edit_announcement_modal: '{{ $edit_announcement_modal }}', delete_memo_modal: '{{ $delete_memo_modal }}', create_memo_modal: '{{ $create_memo_modal }}', edit_memo_modal: '{{ $edit_memo_modal }}' }">
    <x-loading />
    <x-modal id="delete_announcement_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
        <x-slot name="body">
            <h2 class="mb-3 text-lg text-center text-gray-900">
                {{ $confirmation_message_ann }}
            </h2>
            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('delete_announcement_modal', false)"
                    class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">No</button>
                <button type="button" wire:click="deleteAnnouncement"
                    class="px-5 py-1 mt-4 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Yes</button>
            </div>
        </x-slot>
    </x-modal>
    @can('hrim_announcement_add')
        <x-modal id="create_announcement_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
            <x-slot name="title">Create Announcement</x-slot>
            <x-slot name="body">
                @livewire('hrim.company-management.bulletin.announcement.create')
            </x-slot>
        </x-modal>
    @endcan
    @can('hrim_announcement_edit')
        @if ($announcement_id && $edit_announcement_modal)
            <x-modal id="edit_announcement_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="title">Edit Announcement</x-slot>
                <x-slot name="body">
                    @livewire('hrim.company-management.bulletin.announcement.edit', ['id' => $announcement_id])
                </x-slot>
            </x-modal>
        @endif
    @endcan
    <x-modal id="delete_memo_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
        <x-slot name="body">
            <h2 class="mb-3 text-lg text-center text-gray-900">
                {{ $confirmation_message_memo }}
            </h2>
            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('delete_memo_modal', false)"
                    class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">No</button>
                <button type="button" wire:click="deleteMemo"
                    class="px-5 py-1 mt-4 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Yes</button>
            </div>
        </x-slot>
    </x-modal>
    @can('hrim_memo_add')
        <x-modal id="create_memo_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
            <x-slot name="title">Create Memo</x-slot>
            <x-slot name="body">
                @livewire('hrim.company-management.bulletin.memo.create')
            </x-slot>
        </x-modal>
    @endcan
    @can('hrim_memo_edit')
        @if ($memo_id && $edit_memo_modal)
            <x-modal id="edit_memo_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="title">Edit Memo</x-slot>
                <x-slot name="body">
                    @livewire('hrim.company-management.bulletin.memo.edit', ['id' => $memo_id])
                </x-slot>
            </x-modal>
        @endif
    @endcan

    <div class="flex items-center justify-between">
        <h2 class="flex items-center my-6 text-2xl font-black text-[#003399] space-x-6">
            <span>
                Company Bulletin
            </span>
            <svg @click="searh_form = !searh_form" class="w-5 h-5 " aria-hidden="true" focusable="false"
                data-prefix="fas" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512">
                <path fill="currentColor"
                    d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z">
                </path>
            </svg>
        </h2>
        <div class="flex items-center justify-between space-x-3">
            @can('hrim_announcement_add')
                <button wire:click="action({}, 'create_announcement')"
                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Add Announcements</button>
            @endcan
            @can('hrim_memo_add')
                <button wire:click="action({}, 'create_memo')"
                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Add Memos</button>
            @endcan
        </div>
    </div>
    <div class="grid grid-cols-12 gap-3">
        <div class="col-span-8">
            @can('hrim_announcement_view')
                <div class="flex items-center justify-between">
                    <div>
                        <select id="paginate" name="paginate" wire:model="paginateAnnouncement"
                            class="text-xs bg-transparent border-0 border-b-2 border-gray-300 ring-0 focus:ring-0 dark:border-gray-500 dark:focus:border-green-500 focus:border-green-500 ">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                        </select>
                    </div>
                </div>
                <table class="w-full bg-white divide-y shadow-xs rounded-xs">
                    <thead>
                        <tr>
                            <th class="min-w-0 p-4 px-6 text-left">
                                <div>
                                    <h4 class="font-semibold text-gray-600">
                                        Announcements
                                    </h4>
                                </div>
                            </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="min-w-0 p-4 bg-white divide-y-2 shadow-xs rounded-sx">
                        @foreach ($announcements as $announcement)
                            {{-- <tr wire:click="action({'announcement_no': '{{ $announcement->announcement_no }}'}, 'view')"
                                class="text-blue"> --}}
                            <tr class="">
                                <td class="px-6 py-3">
                                    <div class="items-start">
                                        <h4 class="mb-3 font-bold text-gray-900">
                                            <div class="">Announcement #
                                                {{ $announcement->announcement_no }}</div>
                                        </h4>
                                        <h4 class="mb-3 font-light text-gray-900">
                                            <span class="text-sm">Date Posted : </span><span
                                                class="font-bold">{{ $announcement->date_posted }}</span>
                                        </h4>
                                        <h4 class="mb-4 font-light text-gray-900">
                                            <span class="text-sm">Posted By : </span><span
                                                class="font-bold">{{ $announcement->postedBy->name }}</span>
                                        </h4>
                                        <h4 class="mb-3 font-bold text-gray-900">
                                            {{ $announcement->title }}
                                        </h4>
                                        <p class="text-sm text-gray-600 dark:text-gray-400">
                                            {{ $announcement->details }}
                                        </p>
                                    </div>
                                </td>
                                <td class="grid px-3 py-3 place-items-start">
                                    <div class="items-end">
                                        <div class="flex space-x-3">
                                            @can('hrim_announcement_edit')
                                                <svg wire:click="action({'id': {{ $announcement->id }}}, 'edit_announcement')"
                                                    class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="far" data-icon="edit" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                    </path>
                                                </svg>
                                            @endcan
                                            @can('hrim_announcement_delete')
                                                <svg wire:click="action({'id': {{ $announcement->id }}}, 'delete_announcement')"
                                                    class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="fas" data-icon="trash-alt" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                    <path fill="currentColor"
                                                        d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                                    </path>
                                                </svg>
                                            @endcan
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $announcements->links() }}
            @else
                unauthorized
            @endcan
        </div>
        <div class="col-span-4">
            @can('hrim_memo_view')
                <div class="flex items-center justify-between">
                    <div>
                        <select id="paginate" name="paginate" wire:model="paginateMemo"
                            class="text-xs bg-transparent border-0 border-b-2 border-gray-300 ring-0 focus:ring-0 dark:border-gray-500 dark:focus:border-green-500 focus:border-green-500 ">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                        </select>
                    </div>
                </div>
                <table class="w-full bg-white divide-y shadow-xs rounded-xs">
                    <thead>
                        <tr>
                            <th class="min-w-0 p-4 px-6 text-left">
                                <div>
                                    <h4 class="font-semibold text-gray-600">
                                        Memos
                                    </h4>
                                </div>
                            </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="min-w-0 p-4 bg-white divide-y-8 shadow-xs rounded-sx">
                        @foreach ($memos as $memo)
                            {{-- <tr wire:click="action({'memo_no': '{{ $memo->memorandum_no }}'}, 'view')"
                            class="text-blue"> --}}
                            <tr class="">
                                <td class="px-6 py-6">
                                    <div class="items-start">
                                        <h1 class="mb-3 font-bold text-gray-900">
                                            {{ $memo->title }}
                                        </h1>
                                        <h5 class="mb-3 font-light text-gray-900">
                                            <span class="text-sm">Memorandum No : </span><span
                                                class="text-sm font-bold">{{ $memo->memorandum_no }}</span>
                                        </h5>
                                        <h5 class="mb-3 font-light text-gray-900">
                                            <span class="text-sm">Date Posted : </span><span
                                                class="text-sm font-bold">{{ $memo->date_posted }}</span>
                                        </h5>
                                        <h5 class="mb-3 font-light text-gray-900">
                                            <span class="text-sm">Attention To : </span><span
                                                class="text-sm font-bold">All Employees</span>
                                        </h5>
                                    </div>
                                </td>
                                <td class="px-3 py-3">
                                    <div class="flex space-x-3">
                                        @can('hrim_memo_edit')
                                            <svg wire:click="action({'id': {{ $memo->id }}}, 'edit_memo')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false" data-prefix="far"
                                                data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                </path>
                                            </svg>
                                        @endcan
                                        @can('hrim_announcement_delete')
                                            <svg wire:click="action({'id': {{ $memo->id }}}, 'delete_memo')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false" data-prefix="fas"
                                                data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                                </path>
                                            </svg>
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $memos->links() }}
            @else
                unauthorized
            @endcan
        </div>
    </div>
</div>
