<x-form x-data="{ search_form: false, confirmation_modal: '{{ $confirmation_modal }}', view_modal: '{{ $view_modal }}', create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                <div class="space-y-3">
                    <p class="text-center">{{ $confirmation_message }}</p>
                    <div class="flex items-center justify-center space-x-3">
                        <x-button type="button" wire:click="$set('confirmation_modal', false)" title="No"
                            class="py-1 bg-white text-blue hover:bg-gray-100" />
                        <x-button type="button" wire:click="confirm" title="Yes"
                            class="bg-blue text-white hover:bg-[#002161] py-1" />
                    </div>
                </div>
            </x-slot>
        </x-modal>
        @if ($employer_compliance_id && $view_modal)
            <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/5">
                <x-slot name="body">
                    @livewire('hrim.company-management.employer-compliance.view', ['id' => $employer_compliance_id])
                </x-slot>
            </x-modal>
        @endif
        @can('hrim_employer_compliance_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="title">Create Employer Compliance</x-slot>
                <x-slot name="body">
                    @livewire('hrim.company-management.employer-compliance.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_employer_compliance_edit')
            @if ($employer_compliance_id && $edit_modal)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                    <x-slot name="title">Edit Employer Compliance</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.company-management.employer-compliance.edit', ['id' => $employer_compliance_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>
    <x-slot name="header_title">Employer Compliance</x-slot>
    <x-slot name="header_button">
        @can('hrim_employer_compliance_add')
            <x-button type="button" wire:click="$set('create_modal', true)" title="Add Employer Compliance"
                class="bg-blue text-white hover:bg-[#002161]">
                <x-slot name="icon">
                    <svg class="w-3 h-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                </x-slot>
            </x-button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="px-12 py-6 pb-12 text-sm bg-white rounded-lg shadow-md">
            <div class="grid grid-cols-3 gap-12 text-left" style="color:#3a3a3a;">
                @foreach ($employer_compliances as $employer_compliance)
                    <div
                        class="{{ $employer_compliance->deleted_at ? 'bg-red-100' : '' }} flex-none space-y-2 rounded-md border-2 border-gray-300 for-hover -mb-6">
                        <div class="flex flex-col">
                            <div class="flex justify-between gap-2 mx-4 mt-2">
                                <div class="text-lg capitalize text-[#003399] font-semibold h-11 overflow-hidden">
                                    <div class="line-clamp-2"
                                        style="line-height: 1.2; word-wrap: break-word; hyphens: auto;">
                                        {{ $employer_compliance->title }}
                                    </div>
                                </div>
                                <div class="flex items-center gap-2" style="margin-top: -24px">
                                    @can('hrim_employer_compliance_edit')
                                        <span title="Edit">
                                            <svg wire:click="action({'id': {{ $employer_compliance->id }}}, 'edit')"
                                                data-toggle="tooltip"
                                                class="w-4 h-4 cursor-pointer text-[#003399] hover:text-blue"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M490.3 40.4C512.2 62.27 512.2 97.73 490.3 119.6L460.3 149.7L362.3 51.72L392.4 21.66C414.3-.2135 449.7-.2135 471.6 21.66L490.3 40.4zM172.4 241.7L339.7 74.34L437.7 172.3L270.3 339.6C264.2 345.8 256.7 350.4 248.4 353.2L159.6 382.8C150.1 385.6 141.5 383.4 135 376.1C128.6 370.5 126.4 361 129.2 352.4L158.8 263.6C161.6 255.3 166.2 247.8 172.4 241.7V241.7zM192 63.1C209.7 63.1 224 78.33 224 95.1C224 113.7 209.7 127.1 192 127.1H96C78.33 127.1 64 142.3 64 159.1V416C64 433.7 78.33 448 96 448H352C369.7 448 384 433.7 384 416V319.1C384 302.3 398.3 287.1 416 287.1C433.7 287.1 448 302.3 448 319.1V416C448 469 405 512 352 512H96C42.98 512 0 469 0 416V159.1C0 106.1 42.98 63.1 96 63.1H192z" />
                                            </svg>
                                        </span>
                                    @endcan
                                    @can('hrim_employer_compliance_delete')
                                        @if ($employer_compliance->deleted_at)
                                            <button type="button"
                                                wire:click="action({'id': {{ $employer_compliance->id }}}, 'confirmation_restore')"
                                                class="px-3 py-2 font-medium text-white bg-green-600 rounded-md">Restore</button>
                                        @else
                                            <span title="Delete">
                                                <svg wire:click="action({'id': {{ $employer_compliance->id }}}, 'confirmation_delete')"
                                                    data-toggle="tooltip"
                                                    class="w-4 h-4 text-red-500 cursor-pointer hover:text-red-300"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                    <path fill="currentColor"
                                                        d="M160 400C160 408.8 152.8 416 144 416C135.2 416 128 408.8 128 400V192C128 183.2 135.2 176 144 176C152.8 176 160 183.2 160 192V400zM240 400C240 408.8 232.8 416 224 416C215.2 416 208 408.8 208 400V192C208 183.2 215.2 176 224 176C232.8 176 240 183.2 240 192V400zM320 400C320 408.8 312.8 416 304 416C295.2 416 288 408.8 288 400V192C288 183.2 295.2 176 304 176C312.8 176 320 183.2 320 192V400zM317.5 24.94L354.2 80H424C437.3 80 448 90.75 448 104C448 117.3 437.3 128 424 128H416V432C416 476.2 380.2 512 336 512H112C67.82 512 32 476.2 32 432V128H24C10.75 128 0 117.3 0 104C0 90.75 10.75 80 24 80H93.82L130.5 24.94C140.9 9.357 158.4 0 177.1 0H270.9C289.6 0 307.1 9.358 317.5 24.94H317.5zM151.5 80H296.5L277.5 51.56C276 49.34 273.5 48 270.9 48H177.1C174.5 48 171.1 49.34 170.5 51.56L151.5 80zM80 432C80 449.7 94.33 464 112 464H336C353.7 464 368 449.7 368 432V128H80V432z" />
                                                </svg>
                                            </span>
                                        @endif
                                    @endcan
                                </div>
                            </div>
                            <span class="mx-4 mt-2 text-base font-semibold">Description :</span>
                            <div
                                class="h-10 mb-1 ml-4 mr-2 overflow-hidden sm:text-xs md:text-xs lg:text-xs xl:text-sm 2xl:text-sm">
                                <div class="line-clamp-2"
                                    style="line-height: 1.3; word-wrap: break-word; hyphens: auto;">
                                    @php
                                        $max_length = 65;
                                        $ellipsis = '...';
                                        if (strlen($employer_compliance->description) > $max_length) {
                                            $description = substr($employer_compliance->description, 0, $max_length) . $ellipsis;
                                        } else {
                                            $description = substr($employer_compliance->description, 0, $max_length);
                                        }
                                    @endphp
                                    {{ $description }}
                                </div>
                            </div>
                            <div wire:click="action({'id': {{ $employer_compliance->id }}}, 'view')"
                                class="bg-center bg-no-repeat bg-cover border border-gray-200 rounded-md shadow-lg cursor-pointer"
                                style="background-image: url({{ Storage::disk('hrim_gcs')->url($employer_compliance['path'] . $employer_compliance['name']) }}); height:18vh">
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </x-slot>
</x-form>

@push('heads')
    <style>
        .for-hover:hover {
            border: 3px solid #003399;
        }
    </style>
@endpush
