<div>
    <x-loading />
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="flex flex-col items-center justify-center">
                <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                    <div class="relative z-0 ">
                        <div class="absolute top-0 left-0">
                            <img class="object-contain w-40 h-40 mx-auto border border-gray-500 rounded-lg "
                                src="{{ $image ? $image->temporaryUrl() : Storage::disk('hrim_gcs')->url($this->employer_compliance->path . $this->employer_compliance->name) }}">
                        </div>
                        <input type="file" name="image" wire:model="image"
                            class="relative z-50 block w-40 h-40 opacity-0 cursor-pointer">
                        <x-input-error for="image" />
                    </div>
                </div>
            </div>
            <div>
                <x-label for="title" value="Title" />
                <x-input type="text" name="title" wire:model.defer='title' />
                <x-input-error for="title" />
            </div>
            <div>
                <x-label for="description" value="Description" />
                <x-textarea name="description" wire:model.defer='description' rows="8" />
                <x-input-error for="description" />
            </div>
            <div class="flex justify-end space-x-3">
                <x-button type="button" wire:click="$emit('close_modal', 'edit')" title="Cancel"
                    class="bg-white text-blue hover:bg-gray-100" />
                <x-button type="submit" title="Save" class="bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>
    </form>
</div>
