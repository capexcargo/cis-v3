<div>
    <div class="mb-3 -mt-3 text-2xl font-semibold text-gray-800">
        <div class="flex items-center space-x-3">
            <span>{{ $employer_compliance->title }} </span>
        </div>
    </div>
    <div class="flex items-center justify-center">
        <img src="{{ Storage::disk('hrim_gcs')->url($employer_compliance['path'] . $employer_compliance['name']) }}"
            alt="{{ $employer_compliance->title }}">
    </div>
</div>
