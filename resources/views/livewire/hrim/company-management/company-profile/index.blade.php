<div>
    <div class="w-full">
        <img src="{{ asset('images/form/company-profile.png') }}" class="w-full" alt="">
    </div>
    <div class="px-6 py-3 space-y-6">
        <div class="text-3xl text-[#003399] font-medium">Cargo Padala Express Forwarding Services Corporation</div>

        <div class="flex flex-col space-y-6">
            <p>Better known as CaPEx, our registered trademark, is the realization of a dream of an innovative young
                entrepreneur of a world-class freight forwarding company.</p>
            <p>CaPEx was established on May 4, 2011 and is now a fast-growing organization.</p>
            <p>CaPEx is registered with the Securities and Exchange Commission (SEC), has legally secured government
                permits,
                and is fully authorized to operate as a cargo freight forwarder by the Civil Aeronautics Board (CAB) for
                its
                airfreight as well as the Philippine Shipper’s Bureau (PSB) for its sea freight business transactions.
            </p>
            <p>CaPEx
                offers freight services nationwide through its offices in various provinces.</p>
            <p>CaPEx, through its leaders, ensure
                that the company is footed on this foundational principle:</p>
        </div>

        <div class="text-lg font-semibold text-[#003399]">
            "TO PROVIDE OUR CLIENTELE WITH NO LESS THAN EXCELLENT CUSTOMER CARE AND EXPERIENCE!"
        </div>

        <p>This is the thrust of our tagline: <i class="font-black text-red-700">We Create Connections!</i></p>

        <div class="rounded-t-[3rem] rounded-bl-[3rem] px-12 py-10 space-y-3 bg-white shadow-lg text-[#003399]">
            <p class="text-4xl font-semibold">OUR VISION</p>
            <p class="text-lg font-semibold">By 2025, CaPEx is an industry leader, a fully digital logistics
                platform Company with global presence providing relational service for every customer.</p>
        </div>

        <div class="rounded-t-[3rem] rounded-bl-[3rem] px-12 py-10 space-y-3 bg-white shadow-lg text-[#003399]">
            <p class="text-4xl font-semibold">OUR MISSION</p>

            <div class="flex flex-col space-y-6">
                <div class="text-lg">
                    <p class="font-semibold">Technology & Digital Platforms</p>
                    <p>We are a technology-driven organization offering total logistics solutions.</p>
                </div>
                <div class="text-lg">
                    <p class="font-semibold">Process</p>
                    <p>We streamline processes to achieve higher efficiencies for better services.</p>
                </div>
                <div class="text-lg">
                    <p class="font-semibold">People</p>
                    <p>We put premium on developing people for meaningful engagement.</p>
                </div>
                <div class="text-lg">
                    <p class="font-semibold">Social Impact</p>
                    <p>We provide programs that contributes to socio-economic and political progress.</p>
                </div>
                <div class="text-lg">
                    <p class="font-semibold">Environment</p>
                    <p>We ensure environmental sustainability by conducting business responsibly.</p>
                </div>
                <div class="text-lg">
                    <p class="font-semibold">Market Share</p>
                    <p>We deliver optimum customer experience through exceptional client management.</p>
                </div>
                <div class="text-lg">
                    <p class="font-semibold">Finance</p>
                    <p>We drive for higher profitability and growth for robust financial position.</p>
                </div>
            </div>
        </div>

        <div class="rounded-t-[3rem] rounded-bl-[3rem] px-12 py-10 space-y-3 bg-white shadow-lg text-[#003399]">
            <p class="text-4xl font-semibold">OUR CORE VALUES</p>
            <p class="text-5xl font-semibold text-center">I AM</p>
            <div class="grid content-center grid-cols-6 gap-20 text-6xl font-bold justify-items-start">
                <p>C</p>
                <p>A</p>
                <p>P</p>
                <p>E</p>
                <p>X</p>
            </div>

            <div class="grid items-center grid-cols-6 gap-20 text-lg justify-items-start">
                <p>Customer Obsessed</p>
                <p>Accountable</p>
                <p>Passionate</p>
                <p>Engaged</p>
                <p>Excellent</p>
            </div>
        </div>
    </div>
</div>
