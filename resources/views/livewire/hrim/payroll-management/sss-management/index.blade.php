<x-form x-data="{ search_form: false, import_modal: '{{ $import_modal }}', status: '{{ $status }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_sss_management_import')
            <x-modal id="import_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Import SSS Rate</x-slot>
                <x-slot name="body">
                    <div class="space-y-3">
                        <div>
                            <x-label for="import" value="Import" />
                            <x-input type="file" name="import" wire:model='import'></x-input>
                            <x-input-error for="import" />
                        </div>
                        <div class="flex items-center justify-end">
                            <button type="button" wire:click="import()"
                                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Import</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
    </x-slot>
    <x-slot name="header_title">Payroll Management</x-slot>
    <x-slot name="header_button">
        @can('hrim_sss_management_import')
            <button wire:click="$set('import_modal', true)" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg hidden class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Import SSS Rate
                </div>
            </button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-6 gap-4">
            <button wire:click="redirectTo({}, 'redirectToHolidayMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">HOLIDAY<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToSssMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border-2 border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">GOV'T CONTRIBUTION<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectTo13thMonth')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">13TH<br>MONTH PAY</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToDLB')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue">
                <span class="inline-block font-medium text-md">DLB</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToPayrollCutOff')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">PAYROLL CUT-OFF<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToAdminAdjustment')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">ADMIN<br>ADJUSTMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToLoaAdjustmentMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">LOA<br>ADJUSTMENT</span>
            </button>
        </div>
        <div class="text-3xl font-medium">
            New SSS Contribution Table Effective January 2021
        </div>
        <div class="grid grid-cols-6 gap-2 text-sm md:w-3/4" wire:init="">
            <div class="w-40">
                <div wire:init="">
                    <x-transparent.select value="" label="Salary" name="salary" wire:model="salary">
                        <option value=""></option>
                        <option value="1">Below 3,250</option>
                        {{-- @foreach ($salary_references as $salary_ref)
                            <option value="{{ $salary_ref->id }}">
                                {{ $salary_ref->display }}
                            </option>
                        @endforeach --}}
                    </x-transparent.select>
                </div>
            </div>
        </div>
        <div class="py-2">
            <div class="flex items-center justify-start">
                <div wire:click="redirectTo({}, 'redirectToSssMgmt')"
                    class="flex items-center justify-between px-8 py-1 text-sm text-white border border-gray-400 cursor-pointer rounded-l-md bg-blue">
                    <span>SSS</span>
                    <span class="ml-10"></span>
                </div>
                <div wire:click="redirectTo({}, 'redirectToPhilhealthMgmt')"
                    class="flex items-center justify-between px-8 py-1 text-sm bg-white border border-gray-400 cursor-pointer ">
                    <span>Philhealth</span>
                    <span class="ml-10"></span>
                </div>
                <div wire:click="redirectTo({}, 'redirectToPagibigMgmt')"
                    class="flex items-center justify-between px-8 py-1 text-sm bg-white border border-gray-400 cursor-pointer rounded-r-md">
                    <span>Pagibig</span>
                    <span class="ml-10"></span>
                </div>
                {{-- <div class="flex items-center justify-between px-8 py-1 text-sm bg-white border border-gray-400 cursor-pointer rounded-r-md"
                    :class="status == 'pagibig' ? 'bg-blue text-white' : ''" wire:click="$set('status', 'pagibig')">
                    <span>Pagibig</span>
                    <span class="ml-10"></span>
                </div> --}}
            </div>
            <div class="overflow-x-scroll bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <th colspan="2" class="p-2 text-center border-0">
                            <span class="justify-center text-sm font-semibold text-blue">Range of
                                Compensation</span>
                        </th>
                        <th colspan="3" class="p-2 text-center border-2 border-b-0">
                            <span class="justify-center text-sm font-semibold text-blue">Monthly Salary
                                Credit</span>
                        </th>
                        <th colspan="12" class="p-2 text-center border-2 border-b-0">
                            <span class="justify-center text-sm font-semibold text-blue">Amount of
                                Contributions</span>
                        </th>
                    </x-slot>
                    <x-slot name="thead1">
                        <x-table.th name="No." />
                        <x-table.th name="" />
                        <x-table.th name="Regular Social Security" />
                        <x-table.th name="Mandatory Prominent Fund" />
                        <x-table.th name="Total" />
                        <th class="font-medium text-center bg-gray-200">
                            <span class="">REGULAR SOCIAL SECURITY</span><br>
                            <div class="grid grid-cols-3 gap-5 py-1 mt-1 bg-white">
                                <span class="text-gray-400">ER</span>
                                <span class="text-gray-400">EE</span>
                                <span class="text-gray-400">Total</span>
                            </div>
                        </th>
                        <th class="font-medium text-center bg-gray-200">EMPLOYEE'S COMPENSATION<br>
                            <div class="grid grid-cols-3 gap-5 py-1 mt-1 bg-white">
                                <span class="text-gray-400">ER</span>
                                <span class="text-gray-400">EE</span>
                                <span class="text-gray-400">Total</span>
                            </div>
                        </th>
                        <th class="font-medium text-center bg-gray-200">MANDATORY PROVIDENT FUND<br>
                            <div class="grid grid-cols-3 gap-5 py-1 mt-1 bg-white">
                                <span class="text-gray-400">ER</span>
                                <span class="text-gray-400">EE</span>
                                <span class="text-gray-400">Total</span>
                            </div>
                        </th>
                        <th class="font-medium text-center bg-gray-200">TOTAL<br>
                            <div class="grid grid-cols-3 gap-5 py-1 mt-1 bg-white">
                                <span class="text-gray-400">ER</span>
                                <span class="text-gray-400">EE</span>
                                <span class="text-gray-400">Total</span>
                            </div>
                        </th>
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($sss_managements as $sss_management)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loop->index + 1 }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $sss_management->grosspay_min }} - {{ $sss_management->grosspay_max }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $sss_management->regular_er }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $sss_management->regular_ee }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $sss_management->regular_er + $sss_management->regular_ee }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $sss_management->compensation_er }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $sss_management->compensation_ee }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $sss_management->compensation_er + $sss_management->compensation_ee }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $sss_management->mandatory_er }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $sss_management->mandatory_ee }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $sss_management->mandatory_er + $sss_management->mandatory_ee }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $sss_management->regular_er + $sss_management->compensation_er + $sss_management->mandatory_er }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $sss_management->regular_ee + $sss_management->compensation_ee + $sss_management->mandatory_ee }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $sss_management->regular_er +
                                        $sss_management->compensation_er +
                                        $sss_management->mandatory_er +
                                        $sss_management->regular_ee +
                                        $sss_management->compensation_ee +
                                        $sss_management->mandatory_ee }}
                                </td>




                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
            </div>
        </div>
    </x-slot>
</x-form>
