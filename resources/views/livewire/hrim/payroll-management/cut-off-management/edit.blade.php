<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div class="grid gap-4 grid-cols">
                    <div>
                        <x-label value="Category" :required="true" />
                        <x-select name="category" wire:model.defer='category'>
                            <option value="">Select</option>
                            <option value="1">Salary and OT</option>
                            {{-- @foreach ($category_references as $category_reference)
                                <option value="{{ $category_reference->id }}">
                                    {{ $category_reference->display }}
                                </option>
                            @endforeach --}}
                        </x-select>
                        <x-input-error for="category" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols-2 mt-1">
                    <div>
                        <x-label for="cutoff_date_from" value="Cut-off Date (From)" :required="true" />
                        <x-input type="date" name="cutoff_date_from" wire:model.defer='cutoff_date_from'></x-input>
                        <x-input-error for="cutoff_date_from" />
                    </div>
                    <div>
                        <x-label for="cutoff_date_to" value="Cut-off Date (To)" :required="true" />
                        <x-input type="date" name="cutoff_date_to" wire:model.defer='cutoff_date_to'></x-input>
                        <x-input-error for="cutoff_date_to" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols mt-1">
                    <div>
                        <x-label for="payout_date" value="Payout Date" :required="true" />
                        <x-input type="date" name="payout_date" wire:model.defer='payout_date' disabled></x-input>
                        <x-input-error for="payout_date" />
                    </div>
                </div>
                <div class="flex justify-end mt-6 space-x-3 text-xs">
                    <button type="button" wire:click="$emit('close_modal', 'edit')"
                        class="px-12 py-2 font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <x-button type="submit" title="Save" class="bg-blue text-xs text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
