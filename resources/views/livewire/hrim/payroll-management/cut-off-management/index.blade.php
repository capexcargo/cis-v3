<x-form x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_cut_off_management_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Add Payroll Cut-off</x-slot>
                <x-slot name="body">
                    @livewire('hrim.payroll-management.cut-off-management.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_cut_off_management_edit')
            @if ($cutoff_id && $edit_modal)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Edit Payroll Cut-off</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.payroll-management.cut-off-management.edit', ['id' => $cutoff_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>
    <x-slot name="header_title">Payroll Management</x-slot>
    <x-slot name="header_button">
        @can('hrim_cut_off_management_add')
            <button wire:click="action({}, 'create_cutoff')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far" data-icon="print-alt"
                        role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Add Payroll Cut-off
                </div>
            </button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-6 gap-4">
            <button wire:click="redirectTo({}, 'redirectToHolidayMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">HOLIDAY<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToSssMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">GOV'T CONTRIBUTION<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectTo13thMonth')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">13TH<br>MONTH PAY</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToDLB')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue">
                <span class="inline-block font-medium text-md">DLB</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToPayrollCutOff')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border-2 border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">PAYROLL CUT-OFF<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToAdminAdjustment')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">ADMIN<br>ADJUSTMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToLoaAdjustmentMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">LOA<br>ADJUSTMENT</span>
            </button>
        </div>
        <div class="overflow-auto">
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="No." />
                        <x-table.th name="Date" />
                        <x-table.th name="Payout Month" />
                        <x-table.th name="Payout Year" />
                        <x-table.th name="Cutoff" />
                        <x-table.th name="Category" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @forelse ($cutoff_managements as $i => $cutoff)
                            <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $i + 1 }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('M d, Y (l)', strtotime($cutoff->cutoff_date)) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('M', strtotime($cutoff->payout_month)) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('Y', strtotime($cutoff->payout_year)) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ ($cutoff->payout_cutoff == 1 ? "First Cutoff" : "Second Cutoff") }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    Salary and OT
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @can('hrim_cut_off_management_edit')
                                        <span>
                                            <svg wire:click="action({id: '2'}, 'edit_cutoff')"
                                                class="w-4 h-4 cursor-pointer text-[#003399] hover:text-blue-700 "
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M490.3 40.4C512.2 62.27 512.2 97.73 490.3 119.6L460.3 149.7L362.3 51.72L392.4 21.66C414.3-.2135 449.7-.2135 471.6 21.66L490.3 40.4zM172.4 241.7L339.7 74.34L437.7 172.3L270.3 339.6C264.2 345.8 256.7 350.4 248.4 353.2L159.6 382.8C150.1 385.6 141.5 383.4 135 376.1C128.6 370.5 126.4 361 129.2 352.4L158.8 263.6C161.6 255.3 166.2 247.8 172.4 241.7V241.7zM192 63.1C209.7 63.1 224 78.33 224 95.1C224 113.7 209.7 127.1 192 127.1H96C78.33 127.1 64 142.3 64 159.1V416C64 433.7 78.33 448 96 448H352C369.7 448 384 433.7 384 416V319.1C384 302.3 398.3 287.1 416 287.1C433.7 287.1 448 302.3 448 319.1V416C448 469 405 512 352 512H96C42.98 512 0 469 0 416V159.1C0 106.1 42.98 63.1 96 63.1H192z" />
                                            </svg>
                                        </span>
                                    @endcan
                                </td>
                            </tr>
                        @empty
                        @endforelse
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $cutoff_managements->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
