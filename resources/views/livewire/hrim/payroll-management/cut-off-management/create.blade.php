<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}'
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <h2 class="mb-2 mt-4 text-md font-semibold text-left text-blue">
                Are you sure you want to submit this new payroll cutoff?
            </h2>
            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div class="grid gap-4 grid-cols">
                    <div>
                        <x-label value="Category" :required="true" />
                        <x-select name="category" wire:model.defer='category'>
                            <option value="">Select</option>
                            <option value="1">Salary and OT</option>
                            {{-- @foreach ($category_references as $category_reference)
                                <option value="{{ $category_reference->id }}">
                                    {{ $category_reference->display }}
                                </option>
                            @endforeach --}}
                        </x-select>
                        <x-input-error for="category" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols-2 mt-1">
                    <div>
                        <x-label for="cutoff_date_from" value="Cut-off Date (From)" :required="true" />
                        <x-input type="date" name="cutoff_date_from" wire:model.defer='cutoff_date_from'></x-input>
                        <x-input-error for="cutoff_date_from" />
                    </div>
                    <div>
                        <x-label for="cutoff_date_to" value="Cut-off Date (To)" :required="true" />
                        <x-input type="date" name="cutoff_date_to" wire:model='cutoff_date_to'
                            wire:change="getPayoutDate"></x-input>
                        <x-input-error for="cutoff_date_to" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols mt-1">
                    <div>
                        <x-label for="payout_date" value="Payout Date" :required="true" />
                        <x-input type="date" name="payout_date" wire:model.defer='payout_date'></x-input>
                        <x-input-error for="payout_date" />
                    </div>
                </div>
                <div class="flex justify-end mt-6 space-x-3 text-xs">
                    <button type="button" wire:click="$emit('close_modal', 'create')"
                        class="px-12 py-2 font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <x-button type="submit" title="Submit" class="bg-blue text-xs text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
