<div wire:init="load" x-data="{
    confirmation_modal: '{{ $confirmation_modal }}'
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 text-red" aria-hidden="true" focusable="false"
                        data-prefix="far" data-icon="edit" role="img" viewBox="0 0 576 512">
                        <path fill="currentColor"
                            d="M310.6 361.4c12.5 12.5 12.5 32.75 0 45.25C304.4 412.9 296.2 416 288 416s-16.38-3.125-22.62-9.375L160 301.3L54.63 406.6C48.38 412.9 40.19 416 32 416S15.63 412.9 9.375 406.6c-12.5-12.5-12.5-32.75 0-45.25l105.4-105.4L9.375 150.6c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0L160 210.8l105.4-105.4c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25l-105.4 105.4L310.6 361.4z" />
                    </svg>
                </span>
            </span>
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                Are you sure you want to submit this new DLB?
            </h2>


            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    Cancel
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Submit
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="space-y-5">
            <div class="grid grid-cols-1 gap-3">
                <div class="grid gap-8 grid-cols">
                    <div>
                        <x-label for="user_id" value="Employee" :required="true" />
                        <x-select name="user_id" wire:model.defer='user_id'>
                            <option value="">Select</option>
                            @foreach ($employees as $employee)
                                <option value="{{ $employee->id }}">
                                    {{ $employee->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="user_id" />
                    </div>
                </div>
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="Year" />
                            <x-table.th name="Month" />
                            <x-table.th name="Cut-Off" />
                            <x-table.th name="Amount" />
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($dlbs as $a => $dlb)
                                @if (!$dlb['is_deleted'])
                                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text" name="dlbs.{{ $a }}.year"
                                                wire:model.defer='dlbs.{{ $a }}.year'>
                                            </x-input>
                                            <x-input-error for="dlbs.{{ $a }}.year" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-select name="dlbs.{{ $a }}.month"
                                                wire:model.defer='dlbs.{{ $a }}.month'>
                                                <option value="">Select</option>
                                                @foreach ($months as $month)
                                                    <option value="{{ $month->id }}">
                                                        {{ $month->display }}
                                                    </option>
                                                @endforeach
                                            </x-select>
                                            <x-input-error for="dlbs.{{ $a }}.month" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-select name="dlbs.{{ $a }}.payout_cutoff"
                                                wire:model.defer='dlbs.{{ $a }}.payout_cutoff'>
                                                <option value="">Select</option>
                                                <option value="1">First</option>
                                                <option value="2">Second</option>
                                            </x-select>
                                            <x-input-error for="dlbs.{{ $a }}.payout_cutoff" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <x-input type="text" name="dlbs.{{ $a }}.amount"
                                                wire:model.defer='dlbs.{{ $a }}.amount'>
                                            </x-input>
                                            <x-input-error for="dlbs.{{ $a }}.amount" />
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <div class="flex space-x-3">
                                                @if (count($dlbs) > 1)
                                                    <svg wire:click="removeDlb({'a': {{ $a }}})"
                                                        class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="trash-alt" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                        <path fill="currentColor"
                                                            d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                                        </path>
                                                    </svg>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </x-slot>
                    </x-table.table>
                    <x-input-error for="dlbs" />


                <div class="flex justify-end space-x-3">
                    <button type="button" wire:click="addDlb()"
                        class="flex-none px-3 py-1 text-sm text-white rounded-full bg-green">
                        Add DLB</button>
                </div>

                <div class="flex justify-end mt-6 space-x-3">
                    <button type="button" wire:click="$emit('close_modal', 'create')"
                        class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
