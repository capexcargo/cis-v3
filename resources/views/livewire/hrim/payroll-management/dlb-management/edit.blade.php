<div wire:init="load">
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-5">
            <div class="grid grid-cols-1 gap-3">
                <div class="grid gap-8 grid-cols">
                    <div>
                        <x-label for="user_id" value="Employee" :required="true" />
                        <x-select name="user_id" wire:model.defer='user_id'>
                            <option value="">Select</option>
                            @foreach ($employees as $employee)
                                <option value="{{ $employee->id }}">
                                    {{ $employee->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="user_id" />
                    </div>
                </div>
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="Year" />
                        <x-table.th name="Month" />
                        <x-table.th name="Cut-Off" />
                        <x-table.th name="Amount" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($dlbs as $a => $dlb)
                            @if (!$dlb['is_deleted'])
                                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input type="text" name="dlbs.{{ $a }}.year"
                                            wire:model.defer='dlbs.{{ $a }}.year'>
                                        </x-input>
                                        <x-input-error for="dlbs.{{ $a }}.year" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-select name="dlbs.{{ $a }}.month"
                                            wire:model.defer='dlbs.{{ $a }}.month'>
                                            <option value="">Select</option>
                                            @foreach ($months as $month)
                                                <option value="{{ $month->id }}">
                                                    {{ $month->display }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                        <x-input-error for="dlbs.{{ $a }}.month" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-select name="dlbs.{{ $a }}.payout_cutoff"
                                            wire:model.defer='dlbs.{{ $a }}.payout_cutoff'>
                                            <option value="">Select</option>
                                            <option value="1">First</option>
                                            <option value="2">Second</option>
                                        </x-select>
                                        <x-input-error for="dlbs.{{ $a }}.payout_cutoff" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <x-input type="text" name="dlbs.{{ $a }}.amount"
                                            wire:model.defer='dlbs.{{ $a }}.amount'>
                                        </x-input>
                                        <x-input-error for="dlbs.{{ $a }}.amount" />
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex space-x-3">
                                            @if (count($dlbs) > 1)
                                                <svg wire:click="removeDlb({'a': {{ $a }}})"
                                                    class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="fas" data-icon="trash-alt" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                    <path fill="currentColor"
                                                        d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z">
                                                    </path>
                                                </svg>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </x-slot>
                </x-table.table>
                <x-input-error for="dlbs" />


                <div class="flex justify-end space-x-3">
                    <button type="button" wire:click="addDlb"
                        class="flex-none px-3 py-1 text-sm text-white rounded-full bg-green">
                        Add DLB</button>
                </div>

                <div class="flex justify-end mt-6 space-x-3">
                    <button type="button" wire:click="$emit('close_modal', 'edit')"
                        class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <x-button type="submit" title="Save" class="bg-blue text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
