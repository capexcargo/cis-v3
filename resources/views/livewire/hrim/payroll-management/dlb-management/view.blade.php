<div>
    <div class="bg-white rounded-lg shadow-md">
        <x-table.table>
            <x-slot name="thead">
                <th class="p-3 whitespace-nowrap border-2 border-b-0 text-gray-400">Cut-Off Date</th>
                <th class="p-3 whitespace-nowrap border-2 border-b-0 text-gray-400">DLB Amount</th>
            </x-slot>
            <x-slot name="tbody">
                <tr>
                    <td class="p-3 whitespace-nowrap border-2 border-b-0">
                        01/25/2022
                    </td>
                    <td class="p-3 whitespace-nowrap border-2 border-b-0">5,000.00
                    </td>
                </tr>
                <tr>
                    <td class="p-3 whitespace-nowrap border-2 border-b-0">
                        02/10/2022
                    </td>
                    <td class="p-3 whitespace-nowrap border-2 border-b-0">5,000.00
                    </td>
                </tr>
                <tr>
                    <td class="p-3 whitespace-nowrap border-2 border-b-0">
                        Total
                    </td>
                    <td class="p-3 whitespace-nowrap border-2 border-b-0 text-blue">10,000.00
                    </td>
                </tr>
            </x-slot>
        </x-table.table>
    </div>
</div>
