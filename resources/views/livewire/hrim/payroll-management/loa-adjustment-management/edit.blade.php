<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off" wire:init="load">
        <div class="space-y-6">
            <div class="grid grid-cols-3 gap-6">
                <div>
                    <x-label value="Minimum Amount" :required="true" />
                    <x-input type="number" name="min_amount" wire:model.defer='min_amount'>
                    </x-input>
                    <x-input-error for="min_amount" />
                </div>
                <div>
                    <x-label value="Maximum Amount" :required="true" />
                    <x-input type="number" name="max_amount" wire:model.defer='max_amount'>
                    </x-input>
                    <x-input-error for="max_amount" />
                </div>
                <div>
                    <x-label value="Category" :required="true" />
                    <x-select name="category" wire:model.defer='category'>
                        <option value=""></option>
                        @foreach ($category_references as $category_reference)
                            <option value="{{ $category_reference->id }}">
                                {{ $category_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="category" />
                </div>
                <div>
                    <x-label value="Approver" :required="true" />
                    <x-select name="approver" wire:model.defer='approver'>
                        <option value=""></option>
                        @foreach ($approver_references as $approver_reference)
                            <option value="{{ $approver_reference->id }}">
                                {{ $approver_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="approver" />
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <x-button type="button" wire:click="$emit('close_modal', 'edit')" title="Cancel"
                    class="bg-white text-blue hover:bg-gray-100" />
                <x-button type="submit" title="Save" class="bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>
    </form>
</div>
