<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}'
}" wire:init="load">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
        <x-slot name="body">
            <h2 class="text-center mb-2 mt-4 text-md font-semibold text-blue">
                Are you sure you want to submit this new LOA adjustment?
            </h2>
            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="space-y-6">
            <div class="grid grid-cols-3 gap-6">
                <div>
                    <x-label value="Minimum Amount" :required="true" />
                    <x-input type="number" name="min_amount" wire:model.defer='min_amount'>
                    </x-input>
                    <x-input-error for="min_amount" />
                </div>
                <div>
                    <x-label value="Maximum Amount" :required="true" />
                    <x-input type="number" name="max_amount" wire:model.defer='max_amount'>
                    </x-input>
                    <x-input-error for="max_amount" />
                </div>
                <div>
                    <x-label value="Category" :required="true" />
                    <x-select name="category" wire:model.defer='category'>
                        <option value=""></option>
                        @foreach ($category_references as $category_reference)
                            <option value="{{ $category_reference->id }}">
                                {{ $category_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="category" />
                </div>
                <div>
                    <x-label value="Approver" :required="true" />
                    <x-select name="approver" wire:model.defer='approver'>
                        <option value=""></option>
                        @foreach ($approver_references as $approver_reference)
                            <option value="{{ $approver_reference->id }}">
                                {{ $approver_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="approver" />
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <x-button type="button" wire:click="$emit('close_modal', 'create')" title="Cancel"
                    class="bg-white text-blue hover:bg-gray-100" />
                <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>
    </form>
</div>
