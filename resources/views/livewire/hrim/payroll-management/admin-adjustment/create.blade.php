<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}'
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <h2 class="mb-2 mt-4 text-md font-semibold text-left text-blue">
                Are you sure you want to submit this new adjustment?
            </h2>
            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off" wire:init="load">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div class="grid gap-4 grid-cols">
                    <div>
                        {{-- <x-label for="employee" value="Employee" :required="true" />
                        <x-input type="text" name="employee" wire:model.defer='employee'>
                        </x-input>
                        <x-input-error for="employee" /> --}}
                        <div x-data="{ open: false }" class="relative mb-2 rounded-md" @click.away="open = false">
                            <div>
                                <x-label for="employee_search" value="Employee" :required="true" />
                                <span class="relative block">
                                    <span class="absolute inset-y-0 left-0 flex items-center ml-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-4 text-blue"
                                            aria-hidden="true" focusable="false" data-prefix="far" data-icon="edit"
                                            role="img" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M500.3 443.7l-119.7-119.7c27.22-40.41 40.65-90.9 33.46-144.7C401.8 87.79 326.8 13.32 235.2 1.723C99.01-15.51-15.51 99.01 1.724 235.2c11.6 91.64 86.08 166.7 177.6 178.9c53.8 7.189 104.3-6.236 144.7-33.46l119.7 119.7c15.62 15.62 40.95 15.62 56.57 0C515.9 484.7 515.9 459.3 500.3 443.7zM79.1 208c0-70.58 57.42-128 128-128s128 57.42 128 128c0 70.58-57.42 128-128 128S79.1 278.6 79.1 208z" />
                                        </svg>
                                    </span>
                                    <input
                                        class="placeholder:placeholder:text-slate-400 block bg-white w-full px-9 border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1"
                                        type="text" @click="open = !open" wire:model='employee_search'
                                        name="employee" />
                                </span>

                                <x-input-error for="employee_search" />
                                <x-input-error for="employee" />
                            </div>
                            <div x-show="open" x-cloak
                                class="absolute w-full p-2 my-1 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96">
                                <ul class="list-reset">
                                    @forelse ($employee_references as $i => $employee_ref)
                                        <li @click="open = !open"
                                            wire:click="getEmployeeDetails({{ $employee_ref->id }})"
                                            wire:key="{{ 'employee' . $i }}"
                                            class=" p-2 text-black cursor-pointer hover:bg-gray-200">
                                            <p>
                                                {{ $employee_ref->name }}
                                            </p>
                                        </li>
                                    @empty
                                        <li>
                                            <p class=" p-2 text-black cursor-pointer hover:bg-gray-200">
                                                No Employee Found.
                                            </p>
                                        </li>
                                    @endforelse
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid gap-4 grid-cols">
                    <div>
                        <x-label value="Category" :required="true" />
                        <x-select name="category" wire:model.defer='category'>
                            <option value="">Select</option>
                            @foreach ($category_references as $category_reference)
                                <option value="{{ $category_reference->id }}">
                                    {{ $category_reference->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="category" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols">
                    <div>
                        <x-label for="description" value="Description" />
                        <x-textarea type="text" name="description" wire:model.defer='description'></x-textarea>
                        <x-input-error for="description" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols-2">
                    <div>
                        <x-label for="amount" value="Amount" :required="true" />
                        <x-input type="number" name="amount" wire:model.defer='amount'></x-input>
                        <x-input-error for="amount" />
                    </div>
                    <div>
                        <x-label for="year" value="Year" :required="true" />
                        <x-input type="number" name="year" wire:model.defer='year'></x-input>
                        <x-input-error for="year" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols-2">
                    <div wire:init="">
                        <x-label for="month" value="Month" :required="true" />
                        <x-select name="month" wire:model.defer='month'>
                            <option value="">Select</option>
                            @foreach ($months as $month)
                                <option value="{{ $month->id }}">
                                    {{ $month->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="month" />
                    </div>
                    <div wire:init="">
                        <x-label for="cutoff" value="Cutoff" :required="true" />
                        <x-select name="cutoff" wire:model.defer='cutoff'>
                            <option value="">Select</option>
                            <option value="1">First</option>
                            <option value="2">Second</option>
                        </x-select>
                        <x-input-error for="cutoff" />
                    </div>
                </div>
                <div class="flex justify-end mt-6 space-x-3 text-xs">
                    <button type="button" wire:click="$emit('close_modal', 'create')"
                        class="px-12 py-2 font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <x-button type="submit" title="Submit" class="bg-blue text-xs text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
