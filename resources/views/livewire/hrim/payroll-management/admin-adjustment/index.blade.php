<x-form x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    delete_modal: '{{ $delete_modal }}',
    approve_modal: '{{ $approve_modal }}'
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_admin_adjustment_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Add Adjustment</x-slot>
                <x-slot name="body">
                    @livewire('hrim.payroll-management.admin-adjustment.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_admin_adjustment_edit')
            @if ($adjustment_id && $edit_modal)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Edit Adjustment</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.payroll-management.admin-adjustment.edit', ['id' => $adjustment_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('hrim_admin_adjustment_delete')
            <x-modal id="delete_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="body">
                    <h2 class="mb-3 text-lg text-center text-gray-900">
                        {{ $confirmation_message }}
                    </h2>
                    <div class="flex justify-center space-x-3">
                        <button type="button" wire:click="$set('delete_modal', false)"
                            class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                        <button type="button" wire:click="delete"
                            class="flex-none px-5 py-1 mt-4 text-sm text-white rounded-lg bg-blue">
                            Yes</button>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
        <x-modal id="approve_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
            <x-slot name="body">
                <h2 class="mb-1 text-lg text-center text-gray-900">
                    {{ $confirmation_message }}
                </h2>
                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('approve_modal', false)"
                        class="px-5 py-1 mt-3 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click="approve"
                        class="flex-none px-5 py-1 mt-3 text-sm text-white rounded-lg bg-blue">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>
    </x-slot>
    <x-slot name="header_title">Payroll Management</x-slot>
    <x-slot name="header_button">
        @can('hrim_admin_adjustment_add')
            <button wire:click="action({}, 'create_adjustment')"
                class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Add Adjustment
                </div>
            </button>
        @endcan
    </x-slot>

    <x-slot name="body">
        <div class="grid grid-cols-6 gap-4">
            <button wire:click="redirectTo({}, 'redirectToHolidayMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">HOLIDAY<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToSssMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">GOV'T CONTRIBUTION<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectTo13thMonth')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">13TH<br>MONTH PAY</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToDLB')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue">
                <span class="inline-block font-medium text-md">DLB</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToPayrollCutOff')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">PAYROLL CUT-OFF<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToAdminAdjustment')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border-2 border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">ADMIN<br>ADJUSTMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToLoaAdjustmentMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">LOA<br>ADJUSTMENT</span>
            </button>
        </div>
        <div class="overflow-auto">
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="No." />
                        <x-table.th name="Employee Name" />
                        <x-table.th name="Category" />
                        <x-table.th name="Description" />
                        <x-table.th name="Amount" />
                        <x-table.th name="Year" />
                        <x-table.th name="Month" />
                        <x-table.th name="Cutoff" />
                        <x-table.th name="Approver" />
                        <x-table.th name="Status" />
                        <x-table.th name="Remarks" />
                        <x-table.th name="Approved Date" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($admin_adjustments as $i => $adjustment)
                            <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $i + 1 }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $adjustment->user->name }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $adjustment->categoryRef->display }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $adjustment->description }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $adjustment->amount }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $adjustment->year }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $adjustment->monthRef->display }}
                                </td>
                                <td class="p-3 whitespace-nowrap w-90">
                                    {{ $adjustment->payout_cutoff == 1 ? 'First' : 'Second' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $adjustment->approver->name }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <span
                                        class="{{ $adjustment->first_status == 1
                                            ? 'text-orange bg-orange-light px-4'
                                            : ($adjustment->first_status == 2
                                                ? 'text-orange bg-red-100 px-6'
                                                : ($adjustment->first_status == 3
                                                    ? 'text-blue bg-blue-100 px-5'
                                                    : ($adjustment->first_status == 4
                                                        ? 'text-red bg-red-100 px-6'
                                                        : ''))) }} 
                                                text-xs rounded-full p-1">
                                        {{ $adjustment->status->display }}</span>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $adjustment->first_remarks ?? '-' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $adjustment->first_date != '' ? date('M. d, Y', strtotime($adjustment->first_date)) : '-' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($adjustment->first_status != 3)
                                        <div class="flex justify-start gap-3">
                                            @if ($adjustment->first_approver == Auth::user()->id)
                                                <span>
                                                    <svg wire:click="action({id:{{ $adjustment->id }}}, 'approve_adjustment')"
                                                        class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700"
                                                        data-prefix="far" data-icon="approve" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM371.8 211.8C382.7 200.9 382.7 183.1 371.8 172.2C360.9 161.3 343.1 161.3 332.2 172.2L224 280.4L179.8 236.2C168.9 225.3 151.1 225.3 140.2 236.2C129.3 247.1 129.3 264.9 140.2 275.8L204.2 339.8C215.1 350.7 232.9 350.7 243.8 339.8L371.8 211.8z" />
                                                    </svg>
                                                </span>
                                            @endif
                                            <span>
                                                <svg wire:click="action({id:{{ $adjustment->id }}}, 'edit_adjustment')"
                                                    class="w-4 h-4 cursor-pointer text-[#003399] hover:text-blue-700 "
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                    <path fill="currentColor"
                                                        d="M490.3 40.4C512.2 62.27 512.2 97.73 490.3 119.6L460.3 149.7L362.3 51.72L392.4 21.66C414.3-.2135 449.7-.2135 471.6 21.66L490.3 40.4zM172.4 241.7L339.7 74.34L437.7 172.3L270.3 339.6C264.2 345.8 256.7 350.4 248.4 353.2L159.6 382.8C150.1 385.6 141.5 383.4 135 376.1C128.6 370.5 126.4 361 129.2 352.4L158.8 263.6C161.6 255.3 166.2 247.8 172.4 241.7V241.7zM192 63.1C209.7 63.1 224 78.33 224 95.1C224 113.7 209.7 127.1 192 127.1H96C78.33 127.1 64 142.3 64 159.1V416C64 433.7 78.33 448 96 448H352C369.7 448 384 433.7 384 416V319.1C384 302.3 398.3 287.1 416 287.1C433.7 287.1 448 302.3 448 319.1V416C448 469 405 512 352 512H96C42.98 512 0 469 0 416V159.1C0 106.1 42.98 63.1 96 63.1H192z" />
                                                </svg>
                                            </span>
                                            <span>
                                                <svg wire:click="action({id:{{ $adjustment->id }}}, 'delete_adjustment')"
                                                    class="w-4 h-4 cursor-pointer text-red hover:text-red-500"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                    <path fill="currentColor"
                                                        d="M160 400C160 408.8 152.8 416 144 416C135.2 416 128 408.8 128 400V192C128 183.2 135.2 176 144 176C152.8 176 160 183.2 160 192V400zM240 400C240 408.8 232.8 416 224 416C215.2 416 208 408.8 208 400V192C208 183.2 215.2 176 224 176C232.8 176 240 183.2 240 192V400zM320 400C320 408.8 312.8 416 304 416C295.2 416 288 408.8 288 400V192C288 183.2 295.2 176 304 176C312.8 176 320 183.2 320 192V400zM317.5 24.94L354.2 80H424C437.3 80 448 90.75 448 104C448 117.3 437.3 128 424 128H416V432C416 476.2 380.2 512 336 512H112C67.82 512 32 476.2 32 432V128H24C10.75 128 0 117.3 0 104C0 90.75 10.75 80 24 80H93.82L130.5 24.94C140.9 9.357 158.4 0 177.1 0H270.9C289.6 0 307.1 9.358 317.5 24.94H317.5zM151.5 80H296.5L277.5 51.56C276 49.34 273.5 48 270.9 48H177.1C174.5 48 171.1 49.34 170.5 51.56L151.5 80zM80 432C80 449.7 94.33 464 112 464H336C353.7 464 368 449.7 368 432V128H80V432z" />
                                                </svg>
                                            </span>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
            </div>
        </div>
    </x-slot>
</x-form>
