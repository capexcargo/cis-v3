<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off" wire:init="load">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">

                <div class="grid gap-4 grid-cols">
                    <div>
                        <x-label for="employee" value="Employee" :required="true" />
                        <x-input type="text" name="employee" wire:model.defer='employee' disabled>
                        </x-input>
                        <x-input-error for="employee" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols">
                    <div>
                        <x-label value="Category" :required="true" />
                        <x-select name="category" wire:model.defer='category'>
                            <option value="">Select</option>
                            @foreach ($category_references as $category_reference)
                                <option value="{{ $category_reference->id }}">
                                    {{ $category_reference->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="category" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols">
                    <div>
                        <x-label for="description" value="Description" />
                        <x-textarea type="text" name="description" wire:model.defer='description'></x-textarea>
                        <x-input-error for="description" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols-2">
                    <div>
                        <x-label for="amount" value="Amount" :required="true" />
                        <x-input type="number" name="amount" wire:model.defer='amount'></x-input>
                        <x-input-error for="amount" />
                    </div>
                    <div>
                        <x-label for="year" value="Year" :required="true" />
                        <x-input type="number" name="year" wire:model.defer='year'></x-input>
                        <x-input-error for="year" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols-2">
                    <div wire:init="">
                        <x-label for="month" value="Month" :required="true" />
                        <x-select name="month" wire:model.defer='month'>
                            <option value="">Select</option>
                            @foreach ($months as $month)
                                <option value="{{ $month->id }}">
                                    {{ $month->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="month" />
                    </div>
                    <div wire:init="">
                        <x-label for="cutoff" value="Cutoff" :required="true" />
                        <x-select name="cutoff" wire:model.defer='cutoff'>
                            <option value="">Select</option>
                            <option value="1">First</option>
                            <option value="2">Second</option>
                        </x-select>
                        <x-input-error for="cutoff" />
                    </div>
                </div>
                <div class="flex justify-end mt-6 space-x-3 text-xs">
                    <button type="button" wire:click="$emit('close_modal', 'edit')"
                        class="px-12 py-2 font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <x-button type="submit" title="Save" class="bg-blue text-xs text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
