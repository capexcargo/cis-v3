<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}'
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 text-red" aria-hidden="true" focusable="false"
                        data-prefix="far" data-icon="edit" role="img" viewBox="0 0 576 512">
                        <path fill="currentColor"
                            d="M310.6 361.4c12.5 12.5 12.5 32.75 0 45.25C304.4 412.9 296.2 416 288 416s-16.38-3.125-22.62-9.375L160 301.3L54.63 406.6C48.38 412.9 40.19 416 32 416S15.63 412.9 9.375 406.6c-12.5-12.5-12.5-32.75 0-45.25l105.4-105.4L9.375 150.6c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0L160 210.8l105.4-105.4c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25l-105.4 105.4L310.6 361.4z" />
                    </svg>
                </span>
            </span>
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                Are you sure you want to submit this new premium rate?
            </h2>


            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    Cancel
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Submit
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">


                <div class="grid grid-cols-2 gap-4">
                    <div>
                        <x-label for="year" value="Year" />
                        <x-input type="text" name="year" wire:model.defer='year'>
                        </x-input>
                        <x-input-error for="year" />
                    </div>
                    <div>
                        <x-label for="premium_rate" value="Premium Rate" />
                        <x-input type="premium_rate" name="premium_rate" wire:model.defer='premium_rate'></x-input>
                        <x-input-error for="premium_rate" />
                    </div>
                </div>

                <div class="flex justify-end mt-6 space-x-3">
                    <button type="button" wire:click="$emit('close_modal', 'create')"
                        class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
