<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">

                <div class="grid grid-cols-2 gap-4">
                    <div>
                        <x-label for="year" value="Year" />
                        <x-input type="text" name="year" wire:model.defer='year'>
                        </x-input>
                        <x-input-error for="year" />
                    </div>
                    <div>
                        <x-label for="premium_rate" value="Premium Rate" />
                        <x-input type="text" name="premium_rate" wire:model.defer='premium_rate'></x-input>
                        <x-input-error for="premium_rate" />
                    </div>
                </div>


                <div class="flex justify-end mt-6 space-x-3">
                    <button type="button" wire:click="$emit('close_modal', 'edit')"
                        class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <x-button type="submit" title="Submit" class="bg-blue text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
