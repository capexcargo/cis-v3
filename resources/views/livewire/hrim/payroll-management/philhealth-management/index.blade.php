<x-form x-data="{ search_form: false, create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_philhealth_management_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Add Premium Rate</x-slot>
                <x-slot name="body">
                    @livewire('hrim.payroll-management.philhealth-management.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_philhealth_management_edit')
            @if ($philhealth_management_id && $edit_modal)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Edit Premium Rate</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.payroll-management.philhealth-management.edit', ['id' => $philhealth_management_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>
    <x-slot name="header_title">Payroll Management</x-slot>
    <x-slot name="header_button">
        @can('hrim_philhealth_management_add')
            <button wire:click="action({}, 'create')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg hidden class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Add Premium Rate
                </div>
            </button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-6 gap-4">
            <button wire:click="redirectTo({}, 'redirectToHolidayMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">HOLIDAY<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToSssMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border-2 border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">GOV'T CONTRIBUTION<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectTo13thMonth')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">13TH<br>MONTH PAY</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToDLB')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue">
                <span class="inline-block font-medium text-md">DLB</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToPayrollCutOff')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">PAYROLL CUT-OFF<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToAdminAdjustment')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">ADMIN<br>ADJUSTMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToLoaAdjustmentMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">LOA<br>ADJUSTMENT</span>
            </button>
        </div>
        <div class="py-2">
            <div class="mt-12">
            </div>
        </div>
        <div class="py-2">
            <div class="flex items-center justify-start mt-12">
                <div wire:click="redirectTo({}, 'redirectToSssMgmt')" s
                    class="flex items-center justify-between px-8 py-1 text-sm border border-gray-400 cursor-pointer rounded-l-md">
                    <span>SSS</span>
                    <span class="ml-10"></span>
                </div>
                <div wire:click="redirectTo({}, 'redirectToPhilhealthMgmt')"
                    class="flex items-center justify-between px-8 py-1 text-sm text-white bg-white border border-gray-400 cursor-pointer bg-blue">
                    <span>Philhealth</span>
                    <span class="ml-10"></span>
                </div>
                <div wire:click="redirectTo({}, 'redirectToPagibigMgmt')"
                    class="flex items-center justify-between px-8 py-1 text-sm bg-white border border-gray-400 cursor-pointer rounded-r-md">
                    <span>Pagibig</span>
                    <span class="ml-10"></span>
                </div>
            </div>

            <div class="overflow-x-scroll bg-white rounded-lg shadow-md">
                <x-table.table>

                    <x-slot name="thead">
                        <x-table.th name="No." />
                        <x-table.th name="Year" />
                        <x-table.th name="Premium Rate" />
                        <x-table.th name="Monthly Premium" hidden />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($philhealth_managements as $philhealth_management)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $loop->index + 1 }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $philhealth_management->year }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $philhealth_management->premium_rate }}
                                </td>
                                <td>
                                    <div class="flex space-x-3">
                                        @can('hrim_holiday_management_edit')
                                            <svg wire:click="action({'id': {{ $philhealth_management->id }}}, 'edit')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                </path>
                                            </svg>
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
            </div>
        </div>
    </x-slot>
</x-form>
