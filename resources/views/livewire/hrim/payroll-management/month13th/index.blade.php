<x-form wire:init="load" x-data="{
    search_form: false,
    generate_13th_month_modal: '{{ $generate_13th_month_modal }}',
    generate_bankfile_modal: '{{ $generate_bankfile_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @if ($generate_13th_month_modal)
            <x-modal id="generate_13th_month_modal" size="md:inset-0 h-modal ">
                <x-slot name="body">
                    <h2 class="mb-2 text-lg text-left text-gray-900">
                        Generate 13th Month
                    </h2>
                    <div class="flex justify-center space-x-6">
                        <button type="button" wire:click="download"
                            class="flex-none px-5 py-1 mt-2 text-sm text-white rounded-md cursor-pointer bg-blue">
                            Download</button>
                        <button type="button" wire:click="email_13th_month"
                            class="flex-none px-5 py-1 mt-2 text-sm text-white rounded-md cursor-pointer bg-blue">
                            Email 13th Month</button>
                    </div>
                </x-slot>
            </x-modal>
        @endif
        @if ($generate_bankfile_modal)
            <x-modal id="generate_bankfile_modal" size="md:inset-0 h-modal ">
                <x-slot name="body">
                    <h2 class="mb-3 text-lg text-left text-gray-900">
                        Are you sure you want to download a bank file?
                    </h2>
                    <div class="flex justify-center space-x-6">
                        <button type="button" wire:click="$set('generate_bankfile_modal', false)"
                            class="flex-none px-5 py-1 mt-4 text-sm text-[#003399] border border-[#003399] rounded-md bg-white cursor-pointer">
                            NO</button>
                        <button type="button" wire:click="generate"
                            class="flex-none px-5 py-1 mt-4 text-sm text-white rounded-md bg-[#003399] cursor-pointer">
                            YES</button>
                    </div>
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">Payroll Management</x-slot>
    <x-slot name="header_button">
        <div class="flex justify-between">
            <button wire:click="action('generate_13th_month')"
                class="p-2 px-3 mr-3 text-sm text-white rounded-md 
                {{ !($selectAll || count($selected) > 0) ? 'bg-gray-500 cursor-not-allowed' : 'bg-blue' }}"
                @if (!($selectAll || count($selected) > 0)) disabled readonly @endif>
                <div class="flex justify-between">
                    Generate 13th Month Payroll
                </div>
            </button>
            <button wire:click="action('generate_bank_file')"
                class="p-2 px-3 mr-3 text-sm text-white rounded-md
                {{ !($selectAll || count($selected) > 0) ? 'bg-gray-500 cursor-not-allowed' : 'bg-blue' }}"
                @if (!($selectAll || count($selected) > 0)) disabled readonly @endif>
                <div class="flex justify-between">
                    Generate Bank File
                </div>
            </button>
            {{-- <button wire:click="email_13th_month" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex justify-between">
                    Email 13th Month
                </div>
            </button> --}}
        </div>
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-6 gap-4">
            <button wire:click="redirectTo({}, 'redirectToHolidayMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">HOLIDAY<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToSssMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">GOV'T CONTRIBUTION<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectTo13thMonth')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border-2 border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">13TH<br>MONTH PAY</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToDLB')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue">
                <span class="inline-block font-medium text-md">DLB</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToPayrollCutOff')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">PAYROLL CUT-OFF<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToAdminAdjustment')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">ADMIN<br>ADJUSTMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToLoaAdjustmentMgmt')"
                class="flex items-center justify-center px-1 py-4 text-left text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="inline-block font-medium text-md">LOA<br>ADJUSTMENT</span>
            </button>
        </div>
        <div class="flex gap-4 px-1 py-1 text-sm md:w-3/4">
            <div class="w-40">
                <div>
                    <x-transparent.input type="number" label="Payroll Year" name="year" wire:model="year" />
                </div>
            </div>
            <div class="w-40">
                <div wire:init="">
                    <x-transparent.select value="{{ $cut_off }}" label="Cut-Off" name="cut_off" .
                        wire:model="cut_off" wire:model="cut_off">
                        <option value=""></option>
                        <option value=1>First</option>
                        <option value=2>Second</option>
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div wire:init="">
                    <x-transparent.select value="" label="Branch" name="branch_id" wire:model="branch_id">
                        <option value=""></option>
                        @foreach ($branch_references as $branch_ref)
                            <option value="{{ $branch_ref->id }}">
                                {{ $branch_ref->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div wire:init="">
                    <x-transparent.select value="" label="Employment Category" name="employment_category"
                        wire:model="employment_category">
                        <option value=""></option>
                        @foreach ($employee_category_references as $employee_category)
                            <option value="{{ $employee_category->id }}">
                                {{ $employee_category->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
        </div>
        <div class="flex gap-4 px-1 py-1 text-sm md:w-3/4">
            <div class="w-40">
                <div>
                    <x-transparent.input type="number" label="Employee ID" name="employee_id"
                        wire:model="employee_id" />
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.input type="text" label="Employee Name" name="employee_name"
                        wire:model="employee_name" />
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.input type="text" label="Employee Code" name="employee_code"
                        wire:model="employee_code" />
                </div>
            </div>
            <div class="w-40">
                <div wire:init="">
                    <x-transparent.select value="" label="Division" name="division" wire:model="division">
                        <option value=""></option>
                        @foreach ($division_references as $division)
                            <option value="{{ $division->id }}">
                                {{ $division->name }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div wire:init="">
                    <x-transparent.select value="" label="Job Rank" name="job_rank" wire:model="job_rank">
                        <option value=""></option>
                        @foreach ($job_rank_references as $job_rank)
                            <option value="{{ $job_rank->id }}">
                                {{ $job_rank->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
        </div>

        <div class="overflow-auto tableFixHead">
            <div class="relative block bg-white rounded-lg shadow-md table-wrp">
                <table class="min-w-full font-medium">
                    <thead class="text-xs text-gray-700 capitalize bg-white border-0 cursor-pointer">
                        <tr class="text-xs font-light text-left text-gray-500 bg-white border-0 cursor-pointer">
                            <th class="relative px-2 border-2 border-gray-900 border-x-4">
                                <div class="w-12 text-xs font-normal text-blue">Email 13th Month to</div>
                                <div class="mt-4">
                                    <div class="flex gap-2 mt-2 flex-nowrap">
                                        <span class="w-20 text-gray-700">
                                            <input wire:model="selectAll" type="checkbox" />
                                        </span>
                                        <span class="w-12 text-gray-700">No.</span>
                                        <span class="w-48 mr-4 text-gray-700">Name</span>
                                    </div>
                                </div>
                            </th>
                            <th class="text-center border-2 border-gray-900 border-x-4">
                                <div class="text-lg font-normal border-b-2 border-gray-900 text-blue">Employee Payroll
                                    Information
                                </div>
                                <div class="px-2 mt-6 font-semibold">
                                    <div class="flex gap-2 mt-2 flex-nowrap">
                                        <span class="w-20 text-gray-700">Employee ID</span>
                                        <span class="text-gray-700 w-28">Employee Code</span>
                                        <span class="text-gray-700 w-44">Payroll Bank Account No.</span>
                                    </div>
                                </div>
                            </th>
                            <th class="text-center border-2 border-gray-900 border-x-4">
                                <div class="text-lg font-normal border-b-2 border-gray-900 text-blue">Employee
                                    Information</div>
                                <div class="px-2 mt-6 font-semibold">
                                    <div class="flex mt-2 text-left flex-nowrap">
                                        <span class="text-gray-700 w-28">Date Hired</span>
                                        <span class="w-40 text-gray-700">Employment Status</span>
                                        <span class="w-40 text-gray-700 whitespace-pre-line">Job Rank</span>
                                        <span class="w-40 text-gray-700 whitespace-pre-line">Position</span>
                                        <span class="w-20 text-gray-700">Branch</span>
                                    </div>
                                </div>
                            </th>
                            <th class="w-1/2 text-center border-2 border-gray-900 border-x-4">
                                <div class="text-lg font-normal border-b-2 text-blue">Salary Details</div>
                                <div class="flex flex-nowrap">
                                    <div class="font-semibold text-left">
                                        <div class="text-sm font-normal text-center bg-gray-200">Earnings</div>
                                        <div class="flex justify-between gap-2 px-2 mt-2">
                                            <span class="w-20 text-gray-700">Basic Pay</span>
                                            <span class="w-20 text-gray-700">COLA</span>
                                            <span class="w-20 text-gray-700">Gross Pay</span>
                                            <span class="w-20 text-gray-700">Holiday Pay</span>
                                            <span class="w-20 text-gray-700">ND Pay</span>
                                            <span class="w-20 text-gray-700">OT Pay</span>
                                            <span class="w-20 text-gray-700">Rest Day Pay</span>
                                        </div>
                                    </div>
                                    <div class="p-0 m-0 border border-gray-900"></div>
                                    <div class="font-semibold text-left">
                                        <div class="text-sm font-normal text-center bg-gray-200">Deductions</div>
                                        <div class="flex justify-between gap-2 px-2 mt-2">
                                            <span class="w-20 text-gray-700">Leave</span>
                                            <span class="w-20 text-gray-700">Tardiness</span>
                                            <span class="w-20 text-gray-700">Undertime</span>
                                            <span class="w-20 text-gray-700">Absences</span>
                                        </div>
                                    </div>

                                </div>
                            </th>
                            <th class="w-1/4 px-3 text-center border-2 border-gray-900 border-x-4">
                                <div class="px-2 mt-6 font-semibold">
                                    <div class="flex gap-2 mt-2 flex-nowrap">
                                        <span class="mr-2 text-gray-700 w-28 whitespace-nowrap">13th Month Pay</span>
                                    </div>
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="text-sm bg-gray-100 border-0">
                        @foreach ($month_13th_pays as $i => $pay)
                            <tr>
                                <td class="w-12 px-2 py-2 border-2 border-gray-900 border-x-4">
                                    <div class="flex gap-2 mt-2 flex-nowrap">
                                        <span class="w-20">
                                            {{-- <input type="checkbox" class="form-checkbox" x-model="enable"
                                                id="enable" name="enable" value="" wire:model=""> --}}
                                            {{-- <input class="item-select-input" type="checkbox"
                                                name="{{ $pay->employee_number }}"
                                                value="{{ $pay->employee_number }}" checked wire:model="selected.{{ $pay->employee_number }}" > --}}


                                            <input wire:model="selected" type="checkbox"
                                                name={{ $pay->employee_number }} value="{{ $pay->employee_number }}">
                                        </span>

                                        <span class="w-12"> {{ $loop->index + 1 }}</span>
                                        <span class="">
                                            <div class="flex space-x-2 w-50">
                                                <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                                    style="background-image: url({{ Storage::disk('hrim_gcs')->url($pay->photo_path . $pay->photo_name) }})">
                                                </div>
                                                <div class="flex items-center w-40 text-sm">
                                                    <p class="flex-initial">{{ $pay->first_name }}
                                                        {{ $pay->last_name }}</p>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </td>
                                <td class="px-4 text-left border-2 border-gray-900 border-x-4">
                                    <div class="flex gap-2 mt-2 flex-nowrap">
                                        <span class="w-20">{{ $pay->employee_number }}</span>
                                        <span class="w-28"></span>
                                        <span class="w-40" style="margin-left: 2px"></span>
                                    </div>
                                </td>
                                <td class="px-2 text-left border-2 border-gray-900 border-x-4">
                                    <div class="flex py-2 mt-2 flex-nowrap">
                                        <span class="w-28">{{ $pay->hired_date }}</span>
                                        <span class="w-40">{{ $pay->emp_status }}</span>
                                        <span class="w-40 whitespace-pre-line">{{ $pay->job_rank }}</span>
                                        <span class="w-40 whitespace-pre-line">{{ $pay->position }}</span>
                                        <span class="w-20" style="margin-left: 2px">{{ $pay->branch }}</span>
                                    </div>
                                </td>
                                <td class="w-1/2 text-center border-2 border-gray-900 border-x-4">
                                    <div class="flex flex-nowrap">
                                        <div class="py-2 text-left">
                                            <div class="flex justify-between gap-2 px-2 mt-2">
                                                <span
                                                    class="w-20">{{ number_format($pay->basic_pay / 2, 2) }}</span>
                                                <span class="w-20">{{ number_format($pay->cola / 2, 2) }}</span>
                                                <span class="w-20">{{ number_format($pay->gross / 2, 2) }}</span>
                                                <span class="w-20">{{ number_format($pay->holidayp, 2) }}</span>
                                                <span class="w-20">{{ number_format($pay->ndval, 2) }}</span>
                                                <span class="w-20">{{ number_format($pay->ot_pay, 2) }}</span>
                                                <span
                                                    class="w-20">{{ number_format(round($pay->restdaypays, 2), 2) }}</span>
                                            </div>
                                        </div>
                                        <div class="p-0 m-0 border border-gray-900"></div>
                                        <div class="py-2 text-left">
                                            <div class="flex justify-between gap-2 px-2 mt-2">
                                                <span
                                                    class="w-20">{{ number_format(round($pay->leaves * ($pay->basic_pay / 26), 2), 2) }}</span>
                                                <span class="w-20">{{ number_format($pay->tardiness, 2) }}</span>
                                                <span class="w-20">{{ number_format($pay->undertime, 2) }}</span>
                                                <span
                                                    class="w-7">{{ number_format(round($pay->absentcount * ($pay->basic_pay / 26), 2), 2) }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="w-1/4 px-3 text-center border-2 border-gray-900 border-x-4">
                                    <div class="flex flex-nowrap">
                                        {{ number_format((($pay->basic_pay / 2) * 6 - ($pay->tardiness + $pay->undertime + round($pay->absentcount * ($pay->basic_pay / 26), 2))) / 6, 2) }}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </x-slot>
</x-form>

@push('heads')
    <style>
        .tableFixHead {
            table-layout: fixed;
            border-collapse: collapse;
            margin-bottom: 20%;
        }

        .tableFixHead tbody {
            display: block;
            width: 100%;
            overflow: auto;
            height: 500px;
        }

        .tableFixHead thead tr {
            display: block;
            overflow-y: auto;
        }
    </style>
@endpush
