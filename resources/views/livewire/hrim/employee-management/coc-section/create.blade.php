<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}'
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
        <x-slot name="body">
            <h2 class="mb-3 text-lg text-center text-gray-900">
                {{ $confirmation_message }}
            </h2>
            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                <button type="button" wire:click="action({},'submit')"
                    class="px-5 py-1 mt-4 text-sm flex-none bg-[#003399] text-white rounded-lg">
                    Yes</button>
            </div>
        </x-slot>
    </x-modal>

    <form autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 mt-4 pt-4">
                <div>
                    <x-label for="display" value="COC Section" :required="true" />
                    <x-textarea type="text" name="display" wire:model.defer='display'></x-textarea>
                    <x-input-error for="display" />
                </div>
            </div>
            <div class="flex justify-end space-x-3 gap-3 pt-5">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-10 py-2 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-md hover:text-white hover:bg-red-400">Cancel</button>
                <button type="button" wire:click="submit"
                    class="px-10 py-2 text-sm flex-none bg-blue text-white rounded-md">
                    Submit</button>
            </div>
        </div>
    </form>
</div>
