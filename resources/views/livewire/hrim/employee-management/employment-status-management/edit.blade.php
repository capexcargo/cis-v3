<div
    x-data="{
        confirmation_modal: '{{ $confirmation_modal }}',
    }">
        <x-loading></x-loading>
    
        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                <span class="relative block">
                    <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                        wire:click="$set('confirmation_modal', false)">
                    </span>
                </span>
                <h2 class="mb-3 text-xl font-bold text-left text-blue">
                    Are you sure you want to submit this Status?
                </h2>
    
    
                <div class="flex justify-end mt-6 space-x-3">
                    <button type="button" wire:click="$emit('close_modal', 'edit')"
                        class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <button type="button" wire:click="submit"
                        class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                        Submit
                    </button>
                </div>
            </x-slot>
        </x-modal>

    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="status" value="Status" :required="true" />
                    <x-input type="text" name="status" wire:model.defer='status'></x-input>
                    <x-input-error for="status" />
                </div>
            </div>
            {{-- <div wire:init="categoryReferences">
                <x-label for="category" value="Category" :required="true" />
                <x-select type="text" name="category" wire:model.defer='category'>
                    <option value=""></option>
                    @foreach ($category_references as $category_ref)
                        <option value="{{ $category_ref->id }}">
                            {{ $category_ref->display }}
                        </option>
                    @endforeach
                </x-select>
                <x-input-error for="category" />
            </div> --}}
            <div class="flex justify-end gap-3 pt-5 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">Cancel</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                    Submit</button>
            </div>
        </div>
    </form>
</div>
