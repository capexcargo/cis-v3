<div wire:init="load" x-data="{
    confirmation_modal: @entangle('confirmation_modal'),
    current_tab: @entangle('current_tab'),
    perview_summary_modal: @entangle('perview_summary_modal')
}">
    <x-loading />
    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            {{ $confirmation_message }}
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">No</button>
                <button type="button" wire:click="remove"
                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Yes</button>
            </div>
        </x-slot>
    </x-modal>
    @if ($perview_summary_modal)
        <x-modal id="perview_summary_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
            <x-slot name="body">
                @livewire('hrim.employee-management.employee-information.perview-summary', ['data' => $this->getRequest(), 'action_type' => 'edit'])
            </x-slot>
        </x-modal>
    @endif
    <form wire:submit.prevent="submit" autocomplete="off">
        <div x-cloak x-show="current_tab == 1">
            <div class="space-y-6">
                <div class="flex flex-col items-center justify-center">
                    <div class="relative z-0 mx-auto">
                        <div class="p-16 bg-center bg-no-repeat bg-cover rounded-full w-44 h-44"
                            style="background-image: url({{ $photo ? $photo->temporaryUrl() : Storage::disk('hrim_gcs')->url($user->photo_path . $user->photo_name) }})">
                        </div>
                        <div
                            class="absolute top-0 flex items-center duration-500 bg-gray-900 rounded-full opacity-0 h-44 w-44 bg-opacity-60 hover:opacity-100">
                            <div class="flex justify-center w-full">
                                <div class="flex justify-between">
                                    <label
                                        class="flex items-center p-2 mx-1 text-white bg-blue-400 rounded-lg cursor-pointer hover:bg-blue-500 text-blue">
                                        <svg class="w-5 h-5" aria-hidden="true" focusable="false" data-prefix="fal"
                                            data-icon="arrow-alt-to-top" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                            <path fill="currentColor"
                                                d="M153.1 448c-8.8 0-16-7.2-16-16V288H43.3c-7.1 0-10.7-8.6-5.7-13.6l143.1-143.5c6.3-6.3 16.4-6.3 22.7 0l143.1 143.5c5 5 1.5 13.6-5.7 13.6h-93.9v144c0 8.8-7.2 16-16 16h-77.8m0 32h77.7c26.5 0 48-21.5 48-48V320h61.9c35.5 0 53.5-43 28.3-68.2L226 108.2c-18.8-18.8-49.2-18.8-68 0L14.9 251.8c-25 25.1-7.3 68.2 28.4 68.2h61.9v112c-.1 26.5 21.5 48 47.9 48zM0 44v8c0 6.6 5.4 12 12 12h360c6.6 0 12-5.4 12-12v-8c0-6.6-5.4-12-12-12H12C5.4 32 0 37.4 0 44z">
                                            </path>
                                        </svg>
                                        <input type='file' wire:model="photo" class="hidden" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <x-input-error for="photo" />
                </div>
                <div class="p-3 text-xl font-medium text-gray-800 bg-gray-200">
                    <div class="flex items-center space-x-3">
                        <span>Employee Information</span>
                    </div>
                </div>
                <div class="px-3 space-y-3">
                    <div class="grid grid-cols-3 gap-3">
                        <div x-data="{ open: false }" class="relative mb-2 rounded-md" @click.away="open = false">
                            <div>
                                <x-label for="erf_reference_search" value="ERF Reference" />
                                <span class="relative block">
                                    <span class="absolute inset-y-0 left-0 flex items-center ml-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-4 text-blue"
                                            aria-hidden="true" focusable="false" data-prefix="far" data-icon="edit"
                                            role="img" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M500.3 443.7l-119.7-119.7c27.22-40.41 40.65-90.9 33.46-144.7C401.8 87.79 326.8 13.32 235.2 1.723C99.01-15.51-15.51 99.01 1.724 235.2c11.6 91.64 86.08 166.7 177.6 178.9c53.8 7.189 104.3-6.236 144.7-33.46l119.7 119.7c15.62 15.62 40.95 15.62 56.57 0C515.9 484.7 515.9 459.3 500.3 443.7zM79.1 208c0-70.58 57.42-128 128-128s128 57.42 128 128c0 70.58-57.42 128-128 128S79.1 278.6 79.1 208z" />
                                        </svg>
                                    </span>
                                    <input
                                        class="block w-full py-2 pr-3 bg-white border rounded-md shadow-sm placeholder:placeholder:text-slate-400 px-9 border-slate-300 focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1"
                                        type="text" @click="open = !open" wire:model='erf_reference_search'
                                        name="erf_reference" />
                                </span>

                                <x-input-error for="erf_reference_search" />
                                <x-input-error for="erf_reference" />
                            </div>
                            <div x-show="open" x-cloak
                                class="absolute w-full p-2 my-1 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96">
                                <ul class="list-reset">
                                    @forelse ($erf_references as $i => $erf_ref)
                                        <li @click="open = !open" wire:click="getErfDetails({{ $erf_ref->id }})"
                                            wire:key="{{ 'erf_reference' . $i }}"
                                            class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                            <p>
                                                {{ $erf_ref->erf_reference_no }}
                                            </p>
                                        </li>
                                    @empty
                                        <li>
                                            <p class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                                No ERF Found.
                                            </p>
                                        </li>
                                    @endforelse
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-3">
                        <div>
                            <x-label for="employee_id" value="Employee ID" :required="true" />
                            <x-input type="text" name="employee_id" wire:model.defer='employee_id'></x-input>
                            <x-input-error for="employee_id" />
                        </div>
                        <div>
                            <x-label for="branch" value="Branch" :required="true" />
                            <x-select name="branch" wire:model.defer='branch'>
                                <option value=""></option>
                                @foreach ($branch_references as $branch_reference)
                                    <option value="{{ $branch_reference->id }}">{{ $branch_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="branch" />
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-3">
                        <div>
                            <x-label for="first_name" value="First Name" :required="true" />
                            <x-input type="text" name="first_name" wire:model.defer='first_name'></x-input>
                            <x-input-error for="first_name" />
                        </div>
                        <div>
                            <x-label for="middle_name" value="Middle Name" />
                            <x-input type="text" name="middle_name" wire:model.defer='middle_name'></x-input>
                            <x-input-error for="middle_name" />
                        </div>
                        <div>
                            <x-label for="last_name" value="Last Name" :required="true" />
                            <x-input type="text" name="last_name" wire:model.defer='last_name'></x-input>
                            <x-input-error for="last_name" />
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-3">
                        <div>
                            <x-label for="division" value="Division" :required="true" />
                            <x-select name="division" wire:model='division'>
                                <option value=""></option>
                                @foreach ($division_references as $division_reference)
                                    <option value="{{ $division_reference->id }}">
                                        {{ $division_reference->name }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="division" />
                        </div>
                        <div>
                            <x-label for="department" value="Department" :required="true" />
                            <x-select name="department" wire:model='department'>
                                <option value=""></option>
                                @foreach ($department_references as $department_reference)
                                    <option value="{{ $department_reference->id }}">
                                        {{ $department_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="department" />
                        </div>
                        <div>
                            <x-label for="position" value="Position" :required="true" />
                            <x-select name="position" wire:model.defer='position'>
                                <option value=""></option>
                                @foreach ($position_references as $position_reference)
                                    <option value="{{ $position_reference->id }}">{{ $position_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="position" />
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-3">
                        <div>
                            <x-label for="job_level" value="Job Level" :required="true" />
                            <x-select name="job_level" wire:model.defer='job_level'>
                                <option value=""></option>
                                @foreach ($job_level_references as $job_level_reference)
                                    <option value="{{ $job_level_reference->id }}">
                                        {{ $job_level_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="job_level" />
                        </div>
                        <div>
                            <x-label for="employement_category" value="Employment Category" :required="true" />
                            <x-select name="employement_category" wire:model.defer='employement_category'>
                                <option value=""></option>
                                @foreach ($employment_category_references as $employment_category_reference)
                                    <option value="{{ $employment_category_reference->id }}">
                                        {{ $employment_category_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="employement_category" />
                        </div>
                        <div>
                            <x-label for="employement_status" value="Employment Status" :required="true" />
                            <x-select name="employement_status" wire:model.defer='employement_status'>
                                <option value=""></option>
                                @foreach ($employment_status_references as $employment_status_reference)
                                    <option value="{{ $employment_status_reference->id }}">
                                        {{ $employment_status_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="employement_status" />
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-3">
                        <div>
                            <x-label for="gender" value="Gender" :required="true" />
                            <x-select name="gender" wire:model.defer='gender'>
                                <option value=""></option>
                                @foreach ($gender_references as $gender_reference)
                                    <option value="{{ $gender_reference->id }}">{{ $gender_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="gender" />
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-3">
                        <div>
                            <x-label for="date_of_birth" value="Date of Birth" :required="true" />
                            <x-input type="date" name="date_of_birth" wire:model.defer='date_of_birth'></x-input>
                            <x-input-error for="date_of_birth" />
                        </div>
                        <div>
                            <x-label for="date_hired" value="Date Hired" :required="true" />
                            <x-input type="date" name="date_hired" wire:model.defer='date_hired'></x-input>
                            <x-input-error for="date_hired" />
                        </div>

                    </div>
                    <div class="grid grid-cols-3 gap-3">
                        <div class="col-span-3">
                            <x-label for="current_address" value="Current Address" :required="true" />
                            <x-input type="text" name="current_address" wire:model.defer='current_address'>
                            </x-input>
                            <x-input-error for="current_address" />
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-3">
                        <div class="col-span-3">
                            <x-label for="permanent_address" value="Permanent Address" :required="true" />
                            <x-input type="text" name="permanent_address" wire:model.defer='permanent_address'>
                            </x-input>
                            <x-input-error for="permanent_address" />
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-3">
                        <div>
                            <x-label for="personal_mobile_number" value="Personal Mobile Number" :required="true" />
                            <x-input-icon.input type="text" name="personal_mobile_number"
                                wire:model.defer='personal_mobile_number' data-format="****-***-****"
                                data-mask="####-###-####">
                                <x-slot name="icon_left">
                                    <svg class="w-5 h-5 text-gray-400" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M511.2 387l-23.25 100.8c-3.266 14.25-15.79 24.22-30.46 24.22C205.2 512 0 306.8 0 54.5c0-14.66 9.969-27.2 24.22-30.45l100.8-23.25C139.7-2.602 154.7 5.018 160.8 18.92l46.52 108.5c5.438 12.78 1.77 27.67-8.98 36.45L144.5 207.1c33.98 69.22 90.26 125.5 159.5 159.5l44.08-53.8c8.688-10.78 23.69-14.51 36.47-8.975l108.5 46.51C506.1 357.2 514.6 372.4 511.2 387z" />
                                    </svg>
                                </x-slot>
                            </x-input-icon.input>
                            <x-input-error for="personal_mobile_number" />
                        </div>
                        <div>
                            <x-label for="personal_telephone_number" value="Personal Telephone Number" />
                            <x-input-icon.input type="number" name="personal_telephone_number"
                                wire:model.defer='personal_telephone_number'>
                                <x-slot name="icon_left">
                                    <svg class="w-5 h-5 text-gray-400" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M511.2 387l-23.25 100.8c-3.266 14.25-15.79 24.22-30.46 24.22C205.2 512 0 306.8 0 54.5c0-14.66 9.969-27.2 24.22-30.45l100.8-23.25C139.7-2.602 154.7 5.018 160.8 18.92l46.52 108.5c5.438 12.78 1.77 27.67-8.98 36.45L144.5 207.1c33.98 69.22 90.26 125.5 159.5 159.5l44.08-53.8c8.688-10.78 23.69-14.51 36.47-8.975l108.5 46.51C506.1 357.2 514.6 372.4 511.2 387z" />
                                    </svg>
                                </x-slot>
                            </x-input-icon.input>
                            <x-input-error for="personal_telephone_number" />
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-3">
                        <div>
                            <x-label for="personal_email_address" value="Personal Email Address" :required="true" />
                            <x-input-icon.input type="text" name="personal_email_address"
                                wire:model.defer='personal_email_address'>
                                <x-slot name="icon_left">
                                    <svg class="w-5 h-5 text-gray-400" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M464 64C490.5 64 512 85.49 512 112C512 127.1 504.9 141.3 492.8 150.4L275.2 313.6C263.8 322.1 248.2 322.1 236.8 313.6L19.2 150.4C7.113 141.3 0 127.1 0 112C0 85.49 21.49 64 48 64H464zM217.6 339.2C240.4 356.3 271.6 356.3 294.4 339.2L512 176V384C512 419.3 483.3 448 448 448H64C28.65 448 0 419.3 0 384V176L217.6 339.2z" />
                                    </svg>
                                </x-slot>
                            </x-input-icon.input>
                            <x-input-error for="personal_email_address" />
                        </div>
                    </div>
                    <div class="grid grid-cols-3 gap-3">
                        <div>
                            <x-label for="company_mobile_number" value="Company Mobile Number" />
                            <x-input-icon.input type="text" name="company_mobile_number"
                                wire:model.defer='company_mobile_number' data-format="****-***-****"
                                data-mask="####-###-####">
                                <x-slot name="icon_left">
                                    <svg class="w-5 h-5 text-gray-400" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M511.2 387l-23.25 100.8c-3.266 14.25-15.79 24.22-30.46 24.22C205.2 512 0 306.8 0 54.5c0-14.66 9.969-27.2 24.22-30.45l100.8-23.25C139.7-2.602 154.7 5.018 160.8 18.92l46.52 108.5c5.438 12.78 1.77 27.67-8.98 36.45L144.5 207.1c33.98 69.22 90.26 125.5 159.5 159.5l44.08-53.8c8.688-10.78 23.69-14.51 36.47-8.975l108.5 46.51C506.1 357.2 514.6 372.4 511.2 387z" />
                                    </svg>
                                </x-slot>
                            </x-input-icon.input>
                            <x-input-error for="company_mobile_number" />
                        </div>
                        <div>
                            <x-label for="company_telephone_number" value="Company Telephone Number" />
                            <x-input-icon.input type="number" name="company_telephone_number"
                                wire:model.defer='company_telephone_number'>
                                <x-slot name="icon_left">
                                    <svg class="w-5 h-5 text-gray-400" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M511.2 387l-23.25 100.8c-3.266 14.25-15.79 24.22-30.46 24.22C205.2 512 0 306.8 0 54.5c0-14.66 9.969-27.2 24.22-30.45l100.8-23.25C139.7-2.602 154.7 5.018 160.8 18.92l46.52 108.5c5.438 12.78 1.77 27.67-8.98 36.45L144.5 207.1c33.98 69.22 90.26 125.5 159.5 159.5l44.08-53.8c8.688-10.78 23.69-14.51 36.47-8.975l108.5 46.51C506.1 357.2 514.6 372.4 511.2 387z" />
                                    </svg>
                                </x-slot>
                            </x-input-icon.input>
                            <x-input-error for="company_telephone_number" />
                        </div>
                    </div>
                </div>
                <div class="flex justify-end space-x-3">
                    <x-button type="button" wire:click="$emit('close_modal', 'edit')" title="Cancel"
                        class="bg-white text-blue hover:bg-gray-100" />
                    <x-button type="button" wire:click="action({},'edit_next')" title="Next"
                        class="bg-blue text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
        <div x-cloak x-show="current_tab == 2">
            <div class="space-y-6">

                <div class="p-3 text-xl font-medium text-gray-800 bg-gray-200">
                    <div class="flex items-center space-x-3">
                        <span>Account Information</span>
                    </div>
                </div>
                <div class="px-3 space-y-3">
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="company_email_address" value="Email" :required="true" />
                            <x-input type="text" name="company_email_address"
                                wire:model.defer='company_email_address'>
                            </x-input>
                            <x-input-error for="company_email_address" />
                        </div>
                    </div>
                </div>
                <div class="p-3 text-xl font-medium text-gray-800 bg-gray-200">
                    <div class="flex items-center space-x-3">
                        <span>Schedule</span>
                    </div>
                </div>
                <div class="px-3 space-y-3">
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="work_schedule" value="Work Schedule" :required="true" />
                            <x-select name="work_schedule" wire:model='work_schedule'>
                                <option value=""></option>
                                @foreach ($work_schedule_references as $work_schedule_references)
                                    <option value="{{ $work_schedule_references->id }}">
                                        {{ $work_schedule_references->name }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="work_schedule" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="inclusive_days" value="Inclusive Days" />
                            <x-input type="text" name="inclusive_days" wire:model.defer='inclusive_days' disabled>
                            </x-input>
                            <x-input-error for="inclusive_days" />
                        </div>
                        <div>
                            <x-label for="time_schedule" value="Time Schedule" />
                            <x-input type="text" name="time_schedule" wire:model.defer='time_schedule' disabled>
                            </x-input>
                            <x-input-error for="time_schedule" />
                        </div>
                    </div>
                </div>
                <div class="p-3 text-xl font-medium text-gray-800 bg-gray-200">
                    <div class="flex items-center space-x-3">
                        <span>Government ID Information</span>
                    </div>
                </div>
                <div class="px-3 space-y-3">
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="sss" value="SSS" />
                            <x-input type="text" name="sss" wire:model.defer='sss'></x-input>
                            <x-input-error for="sss" />
                        </div>
                        <div>
                            <x-label for="pagibig_number" value="PAGIBIG NO." />
                            <x-input type="text" name="pagibig_number" wire:model.defer='pagibig_number'>
                            </x-input>
                            <x-input-error for="pagibig_number" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="philhealth_number" value="PHILHEALTH NO." />
                            <x-input type="text" name="philhealth_number" wire:model.defer='philhealth_number'>
                            </x-input>
                            <x-input-error for="philhealth_number" />
                        </div>
                        <div>
                            <x-label for="tin_number" value="TIN NO." />
                            <x-input type="text" name="tin_number" wire:model.defer='tin_number'></x-input>
                            <x-input-error for="tin_number" />
                        </div>
                    </div>
                </div>
                <div class="p-3 text-xl font-medium text-gray-800 bg-gray-200">
                    <div class="flex items-center space-x-3">
                        <span>Educational Attainment</span>
                    </div>
                </div>
                <div class="px-3 space-y-3">
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="educational_level" value="Level" :required="true" />
                            <x-select name="educational_level" wire:model.defer='educational_level'>
                                <option value=""></option>
                                @foreach ($educational_level_references as $educational_level_reference)
                                    <option value="{{ $educational_level_reference->id }}">
                                        {{ $educational_level_reference->display }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="educational_level" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="educational_institution" value="Educational Institution"
                                :required="true" />
                            <x-input type="text" name="educational_institution"
                                wire:model.defer='educational_institution'>
                            </x-input>
                            <x-input-error for="educational_institution" />
                        </div>
                        <div>
                            <x-label for="education_inclusive_years" value="Inclusive Years" :required="true" />
                            <div class="grid grid-cols-2 gap-3">
                                <div>
                                    <x-input type="text" name="educational_inclusive_year_from"
                                        wire:model.defer='educational_inclusive_year_from' maxlength="4"
                                        placeholder="From" />
                                    <x-input-error for="educational_inclusive_year_from" />
                                </div>
                                <div>
                                    <x-input type="text" name="educational_inclusive_year_to"
                                        wire:model.defer='educational_inclusive_year_to' maxlength="4"
                                        placeholder="To" />
                                    <x-input-error for="educational_inclusive_year_to" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-3 text-xl font-medium text-gray-800 bg-gray-200">
                    <div class="flex items-center space-x-3">
                        <span>Employment Record</span>
                    </div>
                </div>
                <div class="px-3 space-y-3">
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="employment_employer" value="Employer" />
                            <x-input type="text" name="employment_employer"
                                wire:model.defer='employment_employer'>
                            </x-input>
                            <x-input-error for="employment_employer" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="employment_position" value="Position" />
                            <x-input type="text" name="employment_position"
                                wire:model.defer='employment_position'>
                            </x-input>
                            <x-input-error for="employment_position" />
                        </div>
                        <div>
                            <x-label for="employment_inclusive_years" value="Inclusive Years" />
                            <div class="grid grid-cols-2 gap-3">
                                <div>
                                    <x-input type="text" name="employment_inclusive_year_from"
                                        wire:model.defer='employment_inclusive_year_from' maxlength="4"
                                        placeholder="From" />
                                    <x-input-error for="employment_inclusive_year_from" />
                                </div>
                                <div>
                                    <x-input type="text" name="employment_inclusive_year_to"
                                        wire:model.defer='employment_inclusive_year_to' maxlength="4"
                                        placeholder="To" />
                                    <x-input-error for="employment_inclusive_year_to" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-3 text-xl font-medium text-gray-800 bg-gray-200">
                    <div class="flex items-center space-x-3">
                        <span>Character Reference</span>
                    </div>
                </div>
                <div class="px-3 space-y-3">
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="character_reference_name" value="Name" />
                            <x-input type="text" name="character_reference_name"
                                wire:model.defer='character_reference_name'></x-input>
                            <x-input-error for="character_reference_name" />
                        </div>

                    </div>
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="character_reference_company" value="Company" />
                            <x-input type="text" name="character_reference_company"
                                wire:model.defer='character_reference_company'></x-input>
                            <x-input-error for="character_reference_company" />
                        </div>
                        <div>
                            <x-label for="character_reference_position" value="Position" />
                            <x-input type="text" name="character_reference_position"
                                wire:model.defer='character_reference_position'></x-input>
                            <x-input-error for="character_reference_position" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="character_reference_mobile_number" value="Mobile Number" />
                            <x-input-icon.input type="text" name="character_reference_mobile_number"
                                wire:model.defer='character_reference_mobile_number' data-format="****-***-****"
                                data-mask="####-###-####">
                                <x-slot name="icon_left">
                                    <svg class="w-5 h-5 text-gray-400" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 384 512">
                                        <path fill="currentColor"
                                            d="M320 0H64C37.5 0 16 21.5 16 48v416C16 490.5 37.5 512 64 512h256c26.5 0 48-21.5 48-48v-416C368 21.5 346.5 0 320 0zM240 447.1C240 456.8 232.8 464 224 464H159.1C151.2 464 144 456.8 144 448S151.2 432 160 432h64C232.8 432 240 439.2 240 447.1zM304 384h-224V64h224V384z" />
                                    </svg>
                                </x-slot>
                            </x-input-icon.input>
                            <x-input-error for="character_reference_mobile_number" />
                        </div>
                        <div>
                            <x-label for="character_reference_telephone_number" value="Telephone Number" />
                            <x-input-icon.input type="number" name="character_reference_telephone_number"
                                wire:model.defer='character_reference_telephone_number'>
                                <x-slot name="icon_left">
                                    <svg class="w-5 h-5 text-gray-400" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M511.2 387l-23.25 100.8c-3.266 14.25-15.79 24.22-30.46 24.22C205.2 512 0 306.8 0 54.5c0-14.66 9.969-27.2 24.22-30.45l100.8-23.25C139.7-2.602 154.7 5.018 160.8 18.92l46.52 108.5c5.438 12.78 1.77 27.67-8.98 36.45L144.5 207.1c33.98 69.22 90.26 125.5 159.5 159.5l44.08-53.8c8.688-10.78 23.69-14.51 36.47-8.975l108.5 46.51C506.1 357.2 514.6 372.4 511.2 387z" />
                                    </svg>
                                </x-slot>
                            </x-input-icon.input>
                            <x-input-error for="character_reference_telephone_number" />
                        </div>
                    </div>
                </div>

                <div class="p-3 text-xl font-medium text-gray-800 bg-gray-200">
                    <div class="flex items-center space-x-3">
                        <span>Emergency Contact Details</span>
                    </div>
                </div>
                <div class="px-3 space-y-3">
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="emergency_contact_name" value="Name" :required="true" />
                            <x-input type="text" name="emergency_contact_name"
                                wire:model.defer='emergency_contact_name'>
                            </x-input>
                            <x-input-error for="emergency_contact_name" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="emergency_contact_relationship" value="Relationship" :required="true" />
                            <x-input type="text" name="emergency_contact_relationship"
                                wire:model.defer='emergency_contact_relationship'>
                            </x-input>
                            {{-- <x-select name="emergency_contact_relationship"
                                wire:model.defer='emergency_contact_relationship'>
                                <option value=""></option>
                                @foreach ($ecd_relationship_references as $ecd_relationship_reference)
                                    <option value="{{ $ecd_relationship_reference->id }}">
                                        {{ $ecd_relationship_reference->display }}
                                    </option>
                                @endforeach
                            </x-select> --}}
                            <x-input-error for="emergency_contact_relationship" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-3">
                        <div class="col-span-3">
                            <x-label for="emergency_contact_address" value="Address" :required="true" />
                            <x-input type="text" name="emergency_contact_address"
                                wire:model.defer='emergency_contact_address'></x-input>
                            <x-input-error for="emergency_contact_address" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="emergency_contact_mobile_number" value="Mobile Number" />
                            <x-input-icon.input type="text" name="emergency_contact_mobile_number"
                                wire:model.defer='emergency_contact_mobile_number' data-format="****-***-****"
                                data-mask="####-###-####">
                                <x-slot name="icon_left">
                                    <svg class="w-5 h-5 text-gray-400" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 384 512">
                                        <path fill="currentColor"
                                            d="M320 0H64C37.5 0 16 21.5 16 48v416C16 490.5 37.5 512 64 512h256c26.5 0 48-21.5 48-48v-416C368 21.5 346.5 0 320 0zM240 447.1C240 456.8 232.8 464 224 464H159.1C151.2 464 144 456.8 144 448S151.2 432 160 432h64C232.8 432 240 439.2 240 447.1zM304 384h-224V64h224V384z" />
                                    </svg>
                                </x-slot>
                            </x-input-icon.input>
                            <x-input-error for="emergency_contact_mobile_number" />
                        </div>
                        <div>
                            <x-label for="emergency_contact_telephone_number" value="Telephone Number" />
                            <x-input-icon.input type="number" name="emergency_contact_telephone_number"
                                wire:model.defer='emergency_contact_telephone_number'>
                                <x-slot name="icon_left">
                                    <svg class="w-5 h-5 text-gray-400" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M511.2 387l-23.25 100.8c-3.266 14.25-15.79 24.22-30.46 24.22C205.2 512 0 306.8 0 54.5c0-14.66 9.969-27.2 24.22-30.45l100.8-23.25C139.7-2.602 154.7 5.018 160.8 18.92l46.52 108.5c5.438 12.78 1.77 27.67-8.98 36.45L144.5 207.1c33.98 69.22 90.26 125.5 159.5 159.5l44.08-53.8c8.688-10.78 23.69-14.51 36.47-8.975l108.5 46.51C506.1 357.2 514.6 372.4 511.2 387z" />
                                    </svg>
                                </x-slot>
                            </x-input-icon.input>
                            <x-input-error for="emergency_contact_telephone_number" />
                        </div>
                    </div>
                </div>

                <div class="p-3 text-xl font-medium text-gray-800 bg-gray-200">
                    <div class="flex items-center space-x-3">
                        <span>Family Information</span>
                    </div>
                </div>
                <div class="px-3 space-y-3">
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="family_mother_maiden_name" value="Mother's Maiden Name"
                                :required="true" />
                            <x-input type="text" name="family_mother_maiden_name"
                                wire:model.defer='family_mother_maiden_name'></x-input>
                            <x-input-error for="family_mother_maiden_name" />
                        </div>
                        <div>
                            <x-label for="family_father_name" value="Father's Name" :required="true" />
                            <x-input type="text" name="family_father_name" wire:model.defer='family_father_name'>
                            </x-input>
                            <x-input-error for="family_father_name" />
                        </div>
                    </div>
                    @foreach ($family_siblings as $i => $family_sibling)
                        <div class="grid grid-cols-2 gap-3">
                            <div class="col-span-2">
                                @if ($loop->first)
                                    <x-label for="family_siblings" value="Siblings" />
                                @endif
                            </div>
                            <div class="flex items-center justify-start">
                                <x-input type="text" name="family_siblings.{{ $i }}.name"
                                    wire:model.defer='family_siblings.{{ $i }}.name'>
                                </x-input>
                                <x-input-error for="family_siblings.{{ $i }}.name" />
                                @if ($loop->last)
                                    <x-input-error for="family_siblings" />
                                @endif
                            </div>
                            <div class="flex items-center justify-start space-x-3">
                                @if (count($family_siblings) > 1)
                                    <button type="button"
                                        wire:click="action({index: '{{ $i }}'}, 'remove_family_sibling')"
                                        class="p-1 rounded-full border-2 border-[#003399]">
                                        <svg class="w-5 h-5 text-blue" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M400 288h-352c-17.69 0-32-14.32-32-32.01s14.31-31.99 32-31.99h352c17.69 0 32 14.3 32 31.99S417.7 288 400 288z" />
                                        </svg>
                                    </button>
                                @endif
                                @if ($loop->last)
                                    <button type="button" wire:click="add('family_sibling')"
                                        class="p-1 rounded-full border-2 border-[#003399] bg-blue">
                                        <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                                        </svg>
                                    </button>
                                @endif
                            </div>
                        </div>
                    @endforeach
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="family_spouse" value="Spouse" />
                            <x-input type="text" name="family_spouse" wire:model.defer='family_spouse'></x-input>
                            <x-input-error for="family_spouse" />
                        </div>
                    </div>
                    @foreach ($family_childrens as $i => $family_children)
                        <div class="grid grid-cols-2 gap-3">
                            <div class="col-span-2">
                                @if ($loop->first)
                                    <x-label for="family_childrens" value="Children" />
                                @endif
                            </div>
                            <div class="flex items-center justify-start">
                                <x-input type="text" name="family_childrens.{{ $i }}.name"
                                    wire:model.defer='family_childrens.{{ $i }}.name'>
                                </x-input>
                                <x-input-error for="family_childrens.{{ $i }}.name" />
                                @if ($loop->last)
                                    <x-input-error for="family_childrens" />
                                @endif
                            </div>
                            <div class="flex items-center justify-start space-x-3">
                                @if (count($family_childrens) > 1)
                                    <button type="button"
                                        wire:click="action({index: '{{ $i }}'}, 'remove_family_children')"
                                        class="p-1 rounded-full border-2 border-[#003399]">
                                        <svg class="w-5 h-5 text-blue" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M400 288h-352c-17.69 0-32-14.32-32-32.01s14.31-31.99 32-31.99h352c17.69 0 32 14.3 32 31.99S417.7 288 400 288z" />
                                        </svg>
                                    </button>
                                @endif
                                @if ($loop->last)
                                    <button type="button" wire:click="add('family_children')"
                                        class="p-1 rounded-full border-2 border-[#003399] bg-blue">
                                        <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                                        </svg>
                                    </button>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="p-3 text-xl font-medium text-gray-800 bg-gray-200">
                    <div class="flex items-center space-x-3">
                        <span>Earnings</span>
                    </div>
                </div>
                <div class="px-3 space-y-3">
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="earnings_basic_pay" value="Basic Pay" :required="true" />
                            <x-input type="number" name="earnings_basic_pay"
                                wire:model.debounce.500ms='earnings_basic_pay'>
                            </x-input>
                            <x-input-error for="earnings_basic_pay" />
                        </div>
                        <div>
                            <x-label for="earnings_cola" value="COLA" />
                            <x-input type="number" name="earnings_cola" wire:model.debounce.500ms='earnings_cola'>
                            </x-input>
                            <x-input-error for="earnings_cola" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-3">
                        <div>
                            <x-label for="earnings_gross_pay" value="Gross Pay" />
                            <x-input type="number" name="earnings_gross_pay"
                                wire:model.debounce.500ms='earnings_gross_pay' disabled>
                            </x-input>
                            <x-input-error for="earnings_gross_pay" />


                        </div>
                    </div>
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <x-button type="button" wire:click="$set('current_tab', 1)" title="Back"
                    class="bg-white text-blue hover:bg-gray-100" />
                <x-button type="button" wire:click="action({},'edit_next_perview')" title="Next"
                    class="bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>
    </form>
</div>

<script>
    function doFormat(x, pattern, mask) {
        var strippedValue = x.replace(/[^0-9]/g, "");
        var chars = strippedValue.split('');
        var count = 0;

        var formatted = '';
        for (var i = 0; i < pattern.length; i++) {
            const c = pattern[i];
            if (chars[count]) {
                if (/\*/.test(c)) {
                    formatted += chars[count];
                    count++;
                } else {
                    formatted += c;
                }
            } else if (mask) {
                if (mask.split('')[i])
                    formatted += mask.split('')[i];
            }
        }
        return formatted;
    }

    document.querySelectorAll('[data-mask]').forEach(function(e) {
        function format(elem) {
            const val = doFormat(elem.value, elem.getAttribute('data-format'));
            elem.value = doFormat(elem.value, elem.getAttribute('data-format'), elem.getAttribute('data-mask'));

            if (elem.createTextRange) {
                var range = elem.createTextRange();
                range.move('character', val.length);
                range.select();
            } else if (elem.selectionStart) {
                elem.focus();
                elem.setSelectionRange(val.length, val.length);
            }
        }
        e.addEventListener('keyup', function() {
            format(e);
        });
        e.addEventListener('keydown', function() {
            format(e);
        });
        format(e)
    });
</script>
