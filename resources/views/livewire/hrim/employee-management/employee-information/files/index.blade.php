<div x-data="{ current_tab: 1, confirmation_modal: @entangle('confirmation_modal'), view_modal: @entangle('view_modal') }">
    <x-loading />

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <div class="space-y-3">
                <p class="text-center">{{ $confirmation_message }}</p>
                <div class="flex items-center justify-center space-x-3">
                    <x-button type="button" wire:click="$set('confirmation_modal', false)" title="No"
                        class="py-1 bg-white text-blue hover:bg-gray-100" />
                    <x-button type="button" wire:click="confirm" title="Yes"
                        class="bg-blue text-white hover:bg-[#002161] py-1" />
                </div>
            </div>
        </x-slot>
    </x-modal>
    @if ($view_modal && $hrim_201_files_reference_id && $user_id)
        <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/4">
            <x-slot name="body">
                @livewire('hrim.employee-management.employee-information.files.view', ['id' => $hrim_201_files_reference_id, 'user_id' => $user_id])
            </x-slot>
        </x-modal>
    @endif
    <div class="flex mb-3">
        <span @click="current_tab=1" class="px-4 py-1 text-sm border border-gray-600 shadow-lg "
            :class="current_tab == 1 ? 'text-white bg-blue' : 'text-gray-500 bg-white'">201 Files</span>
        <span @click="current_tab=2" class="px-4 py-1 text-sm border border-gray-600 shadow-lg"
            :class="current_tab == 2 ? 'text-white bg-blue' : 'text-gray-500 bg-white'">Disciplinary History</span>
    </div>
    <div x-cloak x-show="current_tab == 1">
        <x-table.table>
            <x-slot name="thead">
                <x-table.th class="w-[40%]" name="Files" />
                <x-table.th class="w-[20%]" name="Date Updated" />
                <x-table.th class="w-[20%]" name="Status" />
                <x-table.th class="w-[20%]" name="Action" />
            </x-slot>
            <x-slot name="tbody">
                @foreach ($hrim_201_files_references as $index_a => $hrim_201_files_reference)
                    <tr class="cursor-pointer ">
                        <td class="p-3 whitespace-nowrap">
                            {{ $hrim_201_files_reference['display'] }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            @if($hrim_201_files_reference['status'])
                            {{ $hrim_201_files_reference['date_updated'] }}
                            @endif
                        </td>
                        <td class="p-3 whitespace-nowrap text-1xs">
                            @if ($hrim_201_files_reference['status'])
                                <div class="flex items-center justify-start space-x-3 text-green">
                                    <svg class="w-3 h-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256z" />
                                    </svg>
                                    <span>Uploaded</span>
                                </div>
                            {{-- @else
                                <div class="flex items-center justify-start space-x-3 text-red">
                                    <svg class="w-3 h-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256z" />
                                    </svg>
                                    <span>No Status</span>
                                </div> --}}
                            @endif

                        </td>
                        <td class="p-3 whitespace-nowrap">
                            <div class="flex items-center justify-start space-x-3">
                                <div class="flex flex-col items-start justify-start w-full text-1xs">
                                    <button
                                        class="flex justify-start items-start relative px-2 py-1 mx-auto text-blue border border-[#003399] bg-white hover:bg-blue-100 rounded-full cursor-pointer">
                                        <input type="file" name="hrim_201_files_references.{{ $index_a }}.file"
                                            wire:model="hrim_201_files_references.{{ $index_a }}.file"
                                            class="absolute w-full h-full opacity-0  cursor-pointer text-[0px]">
                                        <svg class="w-3 h-3 mr-2" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                                        </svg>
                                        <span> Add Attachment</span>
                                    </button>
                                    <x-input-error for="hrim_201_files_references.{{ $index_a }}.file" />
                                </div>
                                <div class="flex w-16 space-x-3">
                                    @if ($hrim_201_files_reference['status'] && $hrim_201_files_reference['date_updated'])
                                        <svg wire:ignore
                                            wire:click="action({id: {{ $hrim_201_files_reference['id'] }}}, 'view')"
                                            data-toggle="tooltip" title="View"
                                            class="w-4 h-4 text-gray-300 hover:text-blue-900"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                        </svg>
                                    @endif
                                    @if ($hrim_201_files_reference['status'] && $hrim_201_files_reference['date_updated'])
                                        <svg wire:click="action({id: {{ $hrim_201_files_reference['id'] }}}, 'confirmation_delete')"
                                            data-toggle="tooltip" title="Delete"
                                            class="w-4 h-4 text-gray-300 hover:text-red-500"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M160 400C160 408.8 152.8 416 144 416C135.2 416 128 408.8 128 400V192C128 183.2 135.2 176 144 176C152.8 176 160 183.2 160 192V400zM240 400C240 408.8 232.8 416 224 416C215.2 416 208 408.8 208 400V192C208 183.2 215.2 176 224 176C232.8 176 240 183.2 240 192V400zM320 400C320 408.8 312.8 416 304 416C295.2 416 288 408.8 288 400V192C288 183.2 295.2 176 304 176C312.8 176 320 183.2 320 192V400zM317.5 24.94L354.2 80H424C437.3 80 448 90.75 448 104C448 117.3 437.3 128 424 128H416V432C416 476.2 380.2 512 336 512H112C67.82 512 32 476.2 32 432V128H24C10.75 128 0 117.3 0 104C0 90.75 10.75 80 24 80H93.82L130.5 24.94C140.9 9.357 158.4 0 177.1 0H270.9C289.6 0 307.1 9.358 317.5 24.94H317.5zM151.5 80H296.5L277.5 51.56C276 49.34 273.5 48 270.9 48H177.1C174.5 48 171.1 49.34 170.5 51.56L151.5 80zM80 432C80 449.7 94.33 464 112 464H336C353.7 464 368 449.7 368 432V128H80V432z" />
                                        </svg>
                                    @elseif($hrim_201_files_reference['status'] && $hrim_201_files_reference['date_updated'])
                                        <svg wire:click="action({id: {{ $hrim_201_files_reference['id'] }}}, 'confirmation_restore')"
                                            data-toggle="tooltip" title="Restore"
                                            class="w-4 h-4 text-gray-300 hover:text-green-500"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M284.2 0C296.3 0 307.4 6.848 312.8 17.69L320 32H416C433.7 32 448 46.33 448 64C448 81.67 433.7 96 416 96H32C14.33 96 0 81.67 0 64C0 46.33 14.33 32 32 32H128L135.2 17.69C140.6 6.848 151.7 0 163.8 0H284.2zM31.1 128H416V448C416 483.3 387.3 512 352 512H95.1C60.65 512 31.1 483.3 31.1 448V128zM207 199L127 279C117.7 288.4 117.7 303.6 127 312.1C136.4 322.3 151.6 322.3 160.1 312.1L199.1 273.9V408C199.1 421.3 210.7 432 223.1 432C237.3 432 248 421.3 248 408V273.9L287 312.1C296.4 322.3 311.6 322.3 320.1 312.1C330.3 303.6 330.3 288.4 320.1 279L240.1 199C236.5 194.5 230.4 191.1 223.1 191.1C217.6 191.1 211.5 194.5 207 199V199z" />
                                        </svg>
                                    @endif
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </x-slot>
        </x-table.table>
    </div>
    <div x-cloak x-show="current_tab == 2">
        <x-table.table>
            <x-slot name="thead">
                <x-table.th name="COC Section" />
                <x-table.th name="Violation count" />
                <x-table.th name="Action" />
            </x-slot>
            <x-slot name="tbody">
                @foreach ($disciplinaries as $disciplinaries)
                    <tr class="cursor-pointer ">
                        <td class="p-3 whitespace-nowrap">
                            {{ $disciplinaries->display }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            {{ $disciplinaries->violations_count }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            <div class="flex items-center justify-start space-x-3">
                                <a href="{{ route('hrim.employee-management.disciplinary-history.index') }}">
                                    <svg data-toggle="tooltip" title="View"
                                        class="w-4 h-4 text-gray-300 hover:text-blue-900"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                    </svg>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </x-slot>
        </x-table.table>
    </div>
</div>
@push('scripts')
    <script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>
    <script src="https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js"></script>
    <script>

        document.addEventListener('DOMContentLoaded', (event) => {
            var tooltipTriggerList = [].slice.call(
                document.querySelectorAll('[data-toggle="tooltip"]')
            );
            console.log('shit');
            var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
                return new Tooltip(tooltipTriggerEl, {
                    trigger: "hover",
                    placement: "top",
                });
            });
        })
    </script>
@endpush
