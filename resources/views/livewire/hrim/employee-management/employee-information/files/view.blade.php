<div x-data="{ confirmation_modal: @entangle('confirmation_modal'), view_modal: @entangle('view_modal') }">
    <x-loading />
    <div class="mb-3 -mt-3 text-2xl font-semibold text-gray-800">
        {{ $hrim_201_files->hrim201Reference->display }}
    </div>
    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <div class="space-y-3">
                <p class="text-center">{{ $confirmation_message }}</p>
                <div class="flex items-center justify-center space-x-3">
                    <x-button type="button" wire:click="$set('confirmation_modal', false)" title="No"
                        class="py-1 bg-white text-blue hover:bg-gray-100" />
                    <x-button type="button" wire:click="confirm" title="Yes"
                        class="bg-blue text-white hover:bg-[#002161] py-1" />
                </div>
            </div>
        </x-slot>
    </x-modal>
    @if ($view_modal && $hrim_201_files_attachment_id)
        <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/5">
            <x-slot name="body">
                @livewire('hrim.employee-management.employee-information.files.view-image', ['id' => $hrim_201_files_attachment_id])
            </x-slot>
        </x-modal>
    @endif
    <div class="grid items-center grid-cols-3 gap-6 text-left">
        @foreach ($hrim_201_files->attachments as $attachment)
            <div class="{{ $attachment->deleted_at ? 'bg-red-100' : '' }} px-3 space-y-2 py-2 rounded-md">
                <div class="relative flex flex-col">
                    <div wire:click="action({'id': {{ $attachment->id }}}, 'view')"
                        class="bg-center bg-no-repeat bg-cover border border-gray-200 rounded-md shadow-lg cursor-pointer h-28"
                        style="background-image: url({{ Storage::disk('hrim_gcs')->url($attachment->path . $attachment->name) }})">
                    </div>
                    @if ($attachment->deleted_at)
                        <svg wire:click="action({'id': {{ $attachment->id }}}, 'confirmation_restore')"
                            data-toggle="tooltip" title="Restore"
                            class="absolute w-5 h-5 p-1 text-white rounded-full -right-2 -top-2 bg-green"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path fill="currentColor"
                                d="M180.2 243.1C185 263.9 162.2 280.2 144.1 268.8L119.8 253.6l-50.9 81.43c-13.33 21.32 2.004 48.98 27.15 48.98h32.02c17.64 0 31.98 14.32 31.98 31.96c0 17.64-14.34 32.05-31.98 32.05H96.15c-75.36 0-121.3-82.84-81.47-146.8L65.51 219.8L41.15 204.5C23.04 193.1 27.66 165.5 48.48 160.7l91.43-21.15C148.5 137.7 157.2 142.9 159.2 151.6L180.2 243.1zM283.1 78.96l41.25 66.14l-24.25 15.08c-18.16 11.31-13.57 38.94 7.278 43.77l91.4 21.15c8.622 1.995 17.23-3.387 19.21-12.01l21.04-91.43c4.789-20.81-17.95-37.05-36.07-25.76l-24.36 15.2L337.4 45.14c-37.58-60.14-125.2-60.18-162.8-.0617L167.2 56.9C157.9 71.75 162.5 91.58 177.3 100.9c14.92 9.359 34.77 4.886 44.11-10.04l7.442-11.89C241.6 58.58 270.9 59.33 283.1 78.96zM497.3 301.3l-16.99-27.26c-9.336-14.98-29.06-19.56-44.04-10.21c-14.94 9.318-19.52 29.15-10.18 44.08l16.99 27.15c13.35 21.32-1.984 49-27.14 49h-95.99l.0234-28.74c0-21.38-25.85-32.09-40.97-16.97l-66.41 66.43c-6.222 6.223-6.222 16.41 .0044 22.63l66.42 66.34c15.12 15.1 40.95 4.386 40.95-16.98l-.0234-28.68h95.86C491.2 448.1 537.2 365.2 497.3 301.3z" />
                        </svg>
                    @else
                        <svg wire:click="action({'id': {{ $attachment->id }}}, 'confirmation_delete')"
                            data-toggle="tooltip" title="Delete"
                            class="absolute w-5 h-5 bg-white rounded-full -right-2 -top-2 text-red"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path fill="currentColor"
                                d="M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM175 208.1L222.1 255.1L175 303C165.7 312.4 165.7 327.6 175 336.1C184.4 346.3 199.6 346.3 208.1 336.1L255.1 289.9L303 336.1C312.4 346.3 327.6 346.3 336.1 336.1C346.3 327.6 346.3 312.4 336.1 303L289.9 255.1L336.1 208.1C346.3 199.6 346.3 184.4 336.1 175C327.6 165.7 312.4 165.7 303 175L255.1 222.1L208.1 175C199.6 165.7 184.4 165.7 175 175C165.7 184.4 165.7 199.6 175 208.1V208.1z" />
                        </svg>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
</div>
