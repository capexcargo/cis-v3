<div class="space-y-4 cursor-pointer">
    <div class="flex justify-between text-center">
        <div class="flex justify-center space-x-3 text-center">
            @if ($data['user'])
                <div class="w-10 h-10 p-16 bg-center bg-no-repeat bg-cover rounded-full"
                    style="background-image: url({{ Storage::disk('hrim_gcs')->url($data['user']['photo_path'] . $data['user']['photo_name']) }})">
                </div>
            @else
                <div class="w-10 h-10 p-16 bg-center bg-no-repeat bg-cover rounded-full"
                    style="background-image: url({{ $data['photo'] ? $data['photo']->temporaryUrl() : asset('images/form/blank-person.png') }})">
                </div>
            @endif
            <div class="flex flex-col items-start justify-center">
                <p class="text-3xl font-medium uppercase">
                    {{ $data['first_name'] . ' ' . $data['middle_name'] . ' ' . $data['last_name'] }}</p>
                <p class="text-base">{{ $position->display ?? 'N/A' }}</p>
            </div>
        </div>
    </div>
    <div class="grid grid-cols-12 gap-3">
        <div class="flex flex-col col-span-5 space-y-4">
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Employee Information</div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Employee ID:</p>
                        <p class="col-span-2">{{ $data['employee_id'] ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Branch:</p>
                        <p class="col-span-2">{{ $branch->display ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Date Hired:</p>
                        <p class="col-span-2">
                            {{ $data['date_hired'] ? date('F d, Y', strtotime($data['date_hired'])) : 'N/A' }}
                        </p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Position:</p>
                        <p class="col-span-2">{{ $position->display ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Job Level:</p>
                        <p class="col-span-2">{{ $job_level->display ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Department:</p>
                        <p class="col-span-2">{{ $department->display ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Division:</p>
                        <p class="col-span-2">{{ $division->name ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Gender:</p>
                        <p class="col-span-2">{{ $gender->display ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Date of Birth:</p>
                        <p class="col-span-2">
                            {{ $data['date_of_birth'] ? date('F d, Y', strtotime($data['date_of_birth'])) : 'N/A' }}
                        </p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Current Address:</p>
                        <p class="col-span-2">{{ ucwords(strtolower($data['current_address'])) ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Permanent Address:</p>
                        <p class="col-span-2">{{ ucwords(strtolower($data['permanent_address'])) ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Personal Mobile Number:</p>
                        <p class="col-span-2">{{ $data['personal_mobile_number'] ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Personal Telephone Number:</p>
                        <p class="col-span-2">{{ $data['personal_telephone_number'] ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Personal Email Address:</p>
                        <p class="col-span-2">{{ $data['personal_email_address'] ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Company Mobile Number:</p>
                        <p class="col-span-2">{{ $data['company_mobile_number'] ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Company Telephone Number:</p>
                        <p class="col-span-2">{{ $data['company_telephone_number'] ?? 'N/A' }}</p>
                    </div>
                </div>
            </div>
            <div class="grid grid-cols-2 gap-3">
                <div class="text-sm font-medium border border-gray-500 rounded-md">
                    <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Schedule
                    </div>
                    <div class="flex flex-col p-3 space-y-2">
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-gray-500 text-1xs">Work Schedule:</p>
                            <div class="flex flex-col col-span-2 space-y-1">
                                @if ($work_schedule)
                                    <p>{{ $work_schedule->name }}</p>
                                    <p class="text-1xs">
                                        {{ date('h:i A', strtotime($work_schedule->time_from)) . ' - ' . date('h:i A', strtotime($work_schedule->time_to)) }}
                                    </p>
                                    <p class="text-1xs">
                                        @if ($work_schedule->monday)
                                            Monday
                                        @endif
                                        @if ($work_schedule->tuesday)
                                            , Tuesday
                                        @endif
                                        @if ($work_schedule->wednesday)
                                            , Wednesday
                                        @endif
                                        @if ($work_schedule->thursday)
                                            , Thursday
                                        @endif
                                        @if ($work_schedule->friday)
                                            , Firday
                                        @endif
                                        @if ($work_schedule->saturday)
                                            , Saturday
                                        @endif
                                        @if ($work_schedule->sunday)
                                            , Sunday
                                        @endif
                                    </p>
                                @else
                                    N/A
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-sm font-medium border border-gray-500 rounded-md">
                    <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Earnings
                    </div>
                    <div class="flex flex-col p-3 space-y-2">
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-gray-500 text-1xs">Basic Pay:</p>
                            <p class="col-span-2 text-right">
                                {{ number_format($data['earnings_basic_pay'] ?? 0, 2) }}</p>
                        </div>
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-gray-500 text-1xs">COLA:</p>
                            <p class="col-span-2 text-right">
                                {{ number_format($data['earnings_cola'] ?? 0, 2) }}</p>
                        </div>
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-gray-500 text-1xs">Gross Pay:</p>
                            <p class="col-span-2 text-right">
                                {{ number_format($data['earnings_gross_pay'] ?? 0, 2) }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex flex-col col-span-3 space-y-4">
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Government ID Information
                </div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">SSS:</p>
                        <p class="col-span-2">{{ $data['sss'] ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">PHILHEALTH NO:</p>
                        <p class="col-span-2">{{ $data['pagibig_number'] ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">PAGIBIG NO:</p>
                        <p class="col-span-2">{{ $data['philhealth_number'] ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">TIN NO:</p>
                        <p class="col-span-2">{{ $data['tin_number'] ?? 'N/A' }}</p>
                    </div>
                </div>
            </div>
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Employment Record
                </div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Employer:</p>
                        <p class="col-span-2">{{ ucwords(strtolower($data['employment_employer'])) ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Position:</p>
                        <p class="col-span-2">{{ ucwords(strtolower($data['employment_position'])) ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Inclusive Years:</p>
                        <p class="col-span-2">
                            {{ $data['employment_inclusive_year_from'] . ' - ' . $data['employment_inclusive_year_to'] }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Family Information
                </div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="grid items-center grid-cols-2 gap-3">
                        <p class="text-gray-500 text-1xs">Mother's Maiden Name:</p>
                        <p>{{ ucwords(strtolower($data['family_mother_maiden_name'])) ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-2 gap-3">
                        <p class="text-gray-500 text-1xs">Father's Name:</p>
                        <p>{{ ucwords(strtolower($data['family_father_name'])) ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-2 gap-3">
                        <p class="text-gray-500 text-1xs">Siblings:</p>
                        <div>
                            @forelse($data['family_siblings'] as $sibling)
                                <p> {{ ucwords(strtolower($sibling['name'])) }}</p>
                            @empty
                                N/A
                            @endforelse
                        </div>
                    </div>
                    <div class="grid items-center grid-cols-2 gap-3">
                        <p class="text-gray-500 text-1xs">Spouse:</p>
                        <p>{{ ucwords(strtolower($data['family_spouse'])) ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-2 gap-3">
                        <p class="text-gray-500 text-1xs">Children:</p>
                        <div>
                            @forelse($data['family_childrens'] as $children)
                                <p> {{ ucwords(strtolower($children['name'])) }}</p>
                            @empty
                                N/A
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex flex-col col-span-4 space-y-4">
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Emergency Contact Details
                </div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Name:</p>
                        <p class="col-span-2">{{ ucwords(strtolower($data['emergency_contact_name'])) ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Relationship:</p>
                        <p class="col-span-2">
                            {{ ucwords(strtolower($data['emergency_contact_relationship'])) }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Address:</p>
                        <p class="col-span-2">{{ ucwords(strtolower($data['emergency_contact_address'])) ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Mobile Number:</p>
                        <p class="col-span-2">{{ $data['emergency_contact_mobile_number'] ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Telephone Number:</p>
                        <p class="col-span-2">{{ $data['emergency_contact_telephone_number'] ?? 'N/A' }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Character Reference
                </div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Name:</p>
                        <p class="col-span-2">{{ ucwords(strtolower($data['character_reference_name'])) ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Company:</p>
                        <p class="col-span-2">{{ ucwords(strtolower($data['character_reference_company'])) ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Position:</p>
                        <p class="col-span-2">{{ ucwords(strtolower($data['character_reference_position'])) ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Mobile Number:</p>
                        <p class="col-span-2">{{ $data['character_reference_mobile_number'] ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Telephone Number:</p>
                        <p class="col-span-2">{{ $data['character_reference_telephone_number'] ?? 'N/A' }}</p>
                    </div>
                </div>
            </div>
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Educational Attainment
                </div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Level:</p>
                        <p class="col-span-2">{{ $ed_attainment->display ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Educational Institution:</p>
                        <p class="col-span-2">
                            {{ ucwords(strtolower($data['educational_institution'])) ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Inclusive Years:</p>
                        <p class="col-span-2">
                            {{ $data['educational_inclusive_year_from'] . ' - ' . $data['educational_inclusive_year_to'] }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="flex justify-end space-x-3">
        <x-button type="button" wire:click="submit" title="Confirm"
            class="bg-blue text-white hover:bg-[#002161]" />
    </div>
</div>
