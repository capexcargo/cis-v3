<x-form x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    view_modal: '{{ $view_modal }}',
    resignation_modal: '{{ $resignation_modal }}'
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_employee_information_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/4">
                <x-slot name="title">Add Employee</x-slot>
                <x-slot name="body">
                    @livewire('hrim.employee-management.employee-information.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_employee_information_view')
            @if ($view_modal && $user_id)
                <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                    {{-- <x-slot name="title">View Employee</x-slot> --}}
                    <x-slot name="body">
                        @livewire('hrim.employee-management.employee-information.view', ['id' => $user_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @if ($resignation_modal && $user_id)
            <x-modal id="resignation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Resignation Reason</x-slot>
                <x-slot name="body">
                    @livewire('hrim.employee-management.employee-information.resignation-reason', ['id' => $user_id])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    {{-- <x-slot name="header_card">
        @forelse ($header_cards as $index => $card)
            <x-card.header wire:click="$set('{{ $card['action'] }}', {{ $card['id'] }})"
                wire:key="{{ $index }}" :card="$card"></x-card.header>
        @empty
            <x-card.header-loading count="3"></x-card.header-loading>
        @endforelse
    </x-slot> --}}
    <x-slot name="header_title">Employee Information</x-slot>
    <x-slot name="header_button">
        @can('hrim_employee_information_add')
            <x-button type="button" wire:click="$set('create_modal', true)" title="Add a New Employee"
                class="bg-blue text-white hover:bg-[#002161]">
                <x-slot name="icon">
                    <svg class="w-3 h-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                </x-slot>
            </x-button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex justify-between mb-4">
                <div class="flex space-x-6">
                    @forelse ($header_cards as $index => $card)
                        <x-card.header wire:click="$set('{{ $card['action'] }}', {{ $card['id'] }})"
                            wire:key="{{ $index }}" :card="$card"></x-card.header>
                    @empty
                        <x-card.header-loading count="3"></x-card.header-loading>
                    @endforelse
                </div>
                <div class="flex items-end">
                    <div class="relative rounded-full shadow-sm w-72">
                        <div class="absolute inset-y-0 left-0 flex items-center pl-4 pointer-events-none">
                            <svg class="w-5 h-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                <path fill="currentColor"
                                    d="M500.3 443.7l-119.7-119.7c27.22-40.41 40.65-90.9 33.46-144.7C401.8 87.79 326.8 13.32 235.2 1.723C99.01-15.51-15.51 99.01 1.724 235.2c11.6 91.64 86.08 166.7 177.6 178.9c53.8 7.189 104.3-6.236 144.7-33.46l119.7 119.7c15.62 15.62 40.95 15.62 56.57 0C515.9 484.7 515.9 459.3 500.3 443.7zM79.1 208c0-70.58 57.42-128 128-128s128 57.42 128 128c0 70.58-57.42 128-128 128S79.1 278.6 79.1 208z" />
                            </svg>
                        </div>
                        <input type="text" name="employee_name" placeholder="Search Employee Name"
                            wire:model.debounce.500ms="employee_name"
                            class="pl-10 block w-full p-[5px] shadow-sm border-gray-500 rounded-full focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50">
                    </div>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <th class="flex p-3 pl-4 ml-12 tracking-wider whitespace-nowrap" scope="col">
                            <span class="flex gap-1 font-medium">
                                <div class="">
                                    Name
                                </div>
                                <div class="grid grid-cols-1">
                                    <span title="">
                                        <svg wire:click="action({}, 'sortByName')" class="w-4 text-gray-500"
                                            aria-hidden="true" focusable="false" data-prefix="fas" data-icon="users-cog"
                                            role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                                        </svg>
                                    </span>
                                </div>
                            </span>
                        </th>
                        <th class="p-3 tracking-wider whitespace-nowrap" scope="col">
                            <span class="flex gap-1 font-medium">
                                <div class="">
                                    Position
                                </div>
                                <div class="grid grid-cols-1">
                                    <span title="">
                                        <svg wire:click="action({}, 'sortByPosition')" class="w-4 text-gray-500" aria-hidden="true"
                                            focusable="false" data-prefix="fas" data-icon="users-cog" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                                        </svg>
                                    </span>
                                </div>
                            </span>
                        </th> <x-table.th name="Employment Category" />
                        <x-table.th name="Contact Number" />
                        <x-table.th name="Email Address" />
                        <th class="p-3 tracking-wider whitespace-nowrap" scope="col">
                            <span class="flex gap-1 font-medium">
                                <div class="">
                                    Status
                                </div>
                                <div class="grid grid-cols-1">
                                    <span title="">
                                        <svg wire:click="action({}, 'sortByStatus')" class="w-4 text-gray-500" aria-hidden="true"
                                            focusable="false" data-prefix="fas" data-icon="users-cog" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                                        </svg>
                                    </span>
                                </div>
                            </span>
                        </th>
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($employee_informations as $employee_information)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="flex items-center justify-start px-3 py-1 space-x-3 whitespace-nowrap">
                                    <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                        style="background-image: url({{ Storage::disk('hrim_gcs')->url($employee_information->user->photo_path . $employee_information->user->photo_name) }})">
                                    </div>
                                    <p>
                                        {{ ucwords($employee_information->last_name) . ', ' . ucwords($employee_information->first_name) . ' ' . $employee_information->middle_name }}
                                    </p>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $employee_information->position->display ?? 'N/A' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $employee_information->employmentCategory->display ?? 'N/A' }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $employee_information->company_mobile_number == '' ? $employee_information->personal_mobile_number : $employee_information->company_mobile_number }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($employee_information->user->email_verification_code)
                                        <x-button type="button" title="Request User Account"
                                            class="py-1 bg-white text-blue hover:bg-gray-200 text-1xs">
                                        </x-button>
                                    @else
                                        {{ $employee_information->user->email }}
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @if ($employee_information->user->status_id == 1)
                                        <p class="uppercase text-blue">
                                            {{ $employee_information->user->status->display }}
                                        </p>
                                    @else
                                        <p class="uppercase text-red">
                                            {{ $employee_information->user->status->display }}
                                        </p>
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex space-x-3">
                                        @can('hrim_employee_information_view')
                                            <svg wire:click="action({'id': {{ $employee_information->user_id }}}, 'view')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                </path>
                                            </svg>
                                        @endcan
                                        <svg wire:click="action({'id': {{ $employee_information->user_id }}}, 'resignation')"
                                            class="w-5 h-5 text-red" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 640 512">
                                            <path fill="currentColor"
                                                d="M95.1 477.3c0 19.14 15.52 34.67 34.66 34.67h378.7c5.625 0 10.73-1.65 15.42-4.029L264.9 304.3C171.3 306.7 95.1 383.1 95.1 477.3zM630.8 469.1l-277.1-217.9c54.69-14.56 95.18-63.95 95.18-123.2C447.1 57.31 390.7 0 319.1 0C250.2 0 193.7 55.93 192.3 125.4l-153.4-120.3C34.41 1.672 29.19 0 24.03 0C16.91 0 9.845 3.156 5.127 9.187c-8.187 10.44-6.375 25.53 4.062 33.7L601.2 506.9c10.5 8.203 25.56 6.328 33.69-4.078C643.1 492.4 641.2 477.3 630.8 469.1z" />
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $employee_informations->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>

@push('scripts')
    <script>
        const MONTH_NAMES = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
            'September',
            'October', 'November', 'December'
        ];
        const DAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

        function app($wire) {
            return {
                month: $wire.month - 1,
                year: $wire.year,
                no_of_days: [],
                blankdays: [],
                days: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                events: [],
                // events: @entangle('events'),
                // user_id: @entangle('user_id'),
                themes: [{
                        value: "blue",
                        label: "Blue Theme"
                    },
                    {
                        value: "red",
                        label: "Red Theme"
                    },
                    {
                        value: "yellow",
                        label: "Yellow Theme"
                    },
                    {
                        value: "green",
                        label: "Green Theme"
                    },
                    {
                        value: "purple",
                        label: "Purple Theme"
                    }
                ],



                isToday(date) {
                    const today = new Date();
                    const d = new Date(this.year, this.month, date);

                    return today.toDateString() === d.toDateString() ? true : false;

                },

                showEventModal(date) {
                    // let check_events = this.events.filter(e => new Date(e.event_date_time).toDateString() === new Date(this
                    //     .year, this.month, date).toDateString());
                    // if (check_events.length) {
                    //     $wire.action({
                    //         date: new Date(this.year, this.month, date).toDateString()
                    //     }, 'view')
                    // }
                },

                getNoOfDays() {
                    // console.log(this.events);
                    let daysInMonth = new Date(this.year, this.month + 1, 0).getDate();

                    // find where to start calendar day of week
                    let dayOfWeek = new Date(this.year, this.month).getDay();
                    let blankdaysArray = [];
                    for (var i = 1; i <= dayOfWeek; i++) {
                        blankdaysArray.push(i);
                    }

                    let daysArray = [];
                    for (var i = 1; i <= daysInMonth; i++) {
                        daysArray.push(i);
                    }

                    this.blankdays = blankdaysArray;
                    this.no_of_days = daysArray;

                    // Livewire.emitTo('hrim.employee-management.employee-information.schedule',
                    //     'get_events', new Date(this
                    //         .year, this.month).toDateString())

                    // Livewire.on('updateEvents', data => {
                    //     this.events = data;
                    // })
                },

                previous() {
                    this.month--;
                    if (this.month < 0) {
                        this.month = 11;
                        this.year -= 1;
                    }

                    Livewire.emitTo('hrim.employee-management.employee-information.schedule',
                        'get_events', new Date(this
                            .year, this.month).toDateString())

                    Livewire.on('updateEvents', data => {
                        this.events = data;
                        this.getNoOfDays();
                    })
                },

                next() {
                    this.month++;
                    if (this.month > 11) {
                        this.month = 0;
                        this.year += 1;
                    }

                    Livewire.emitTo('hrim.employee-management.employee-information.schedule',
                        'get_events', new Date(this
                            .year, this.month).toDateString())
                    Livewire.on('updateEvents', data => {
                        this.events = data;
                        this.getNoOfDays();
                    })
                },
            }
        }
    </script>
@endpush
