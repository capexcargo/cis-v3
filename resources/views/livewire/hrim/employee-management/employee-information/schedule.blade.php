<div>
    <div x-data="app($wire)" x-init="[getNoOfDays()]" x-cloak>
        <div class="overflow-hidden bg-white rounded-lg shadow">
            <div class="flex items-center justify-between px-6 py-6">
                <button type="button"
                    class="inline-flex items-center p-1 leading-none transition duration-100 ease-in-out rounded-lg cursor-pointer hover:bg-gray-200"
                    @click=" previous()">
                    <svg class="inline-flex w-6 h-6 leading-none text-gray-500" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7" />
                    </svg>
                </button>
                <div>
                    <span x-text="MONTH_NAMES[month]" class="text-lg font-semibold text-gray-600"></span>
                    <span x-text="year" class="ml-1 text-lg font-semibold text-gray-600"></span>
                </div>
                <button type="button"
                    class="inline-flex items-center p-1 leading-none transition duration-100 ease-in-out rounded-lg cursor-pointer hover:bg-gray-200"
                    @click=" next()">
                    <svg class="inline-flex w-6 h-6 leading-none text-gray-500" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
                    </svg>
                </button>
            </div>
            <div class="-mx-1 -mb-1">
                <div class="flex flex-wrap" style="margin-bottom: -40px;">
                    <template x-for="(day, index) in DAYS" :key="index">
                        <div style="width: 14.26%" class="px-2 py-2">
                            <div x-text="day"
                                class="text-sm font-bold tracking-wide text-center text-gray-600 uppercase">
                            </div>
                        </div>
                    </template>
                </div>

                <div class="flex flex-wrap border-t border-l">
                    <template x-for="blankday in blankdays">
                        <div style="width: 14.28%; height: 100px" class="px-4 pt-2 text-center border-b border-r">
                        </div>
                    </template>
                    <template x-for="(date, dateIndex) in no_of_days" :key="dateIndex">
                        <div style="width: 14.28%; height: 100px"
                            class="relative px-4 pt-2 border-b border-r cursor-pointer" @click="showEventModal(date)"
                            :class="{
                                'bg-blue text-white': isToday(date) ==
                                    true,
                                'text-blue hover:bg-blue-200/50': isToday(date) ==
                                    false
                            }">
                            <div x-text="date"
                                class="inline-flex items-center justify-center w-6 h-6 text-2xl font-semibold leading-none text-center transition duration-100 ease-in-out rounded-full cursor-pointer"
                                :class="{
                                    'bg-blue text-white': isToday(date) ==
                                        true,
                                    'text-gray-700 hover:bg-blue-200': isToday(date) ==
                                        false
                                }">
                            </div>
                            <div style="height: 50px;" class="mt-1 overflow-hidden">
                                <template
                                    x-for="(event, index) in events.filter(e => new Date(e.event_date_time).toDateString() ===  new Date(year, month, date).toDateString() )">
                                    <div x-show="index < 1" class="overflow-hidden ">
                                        <br>
                                        <p x-text="event.work_schedule" class="truncate text-[9px]">
                                        </p>
                                        <p x-text="event.work_schedule_time_log" class="truncate text-[9px]">
                                        </p>
                                        <p x-text="event.time_log" class="truncate text-[8px]">
                                        </p>
                                    </div>
                                </template>
                            </div>
                            <div x-cloak
                                x-show="(events.filter(e => new Date(e.event_date_time).toDateString() ===  new Date(year, month, date).toDateString())).length >= 3"
                                class="mt-1 overflow-hidden font-medium ">
                                <p class="text-xs truncate"
                                    x-text="'+' + (events.filter(e => new Date(e.event_date_time).toDateString() ===  new Date(year, month, date).toDateString()).length - 2) + ' others'">
                                </p>
                            </div>
                        </div>
                    </template>
                </div>
            </div>
        </div>
    </div>

</div>
