<div class="space-y-4 cursor-pointer" x-data="{
    hrim_201_files_modal: @entangle('hrim_201_files_modal'),
    edit_modal: @entangle('edit_modal'),
    schedule_modal: @entangle('schedule_modal')
}">
    @can('hrim_employee_information_edit')
        @if ($hrim_201_files_modal && $user_id)
            <x-modal id="hrim_201_files_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                <x-slot name="body">
                    @livewire('hrim.employee-management.employee-information.files.index', ['id' => $user_id])
                </x-slot>
            </x-modal>
        @endif
        @if ($edit_modal && $user_id)
            <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-2/4">
                <x-slot name="title">Edit Employee</x-slot>
                <x-slot name="body">
                    @livewire('hrim.employee-management.employee-information.edit', ['id' => $user_id])
                </x-slot>
            </x-modal>
        @endif
    @endcan
    <x-modal id="schedule_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-7/12">
        <x-slot name="title">{{ $user->name }}'s Schedule</x-slot>
        <x-slot name="body">
            @livewire('hrim.employee-management.employee-information.schedule', ['user_id' => $user_id])
        </x-slot>
    </x-modal>
    <div class="flex justify-between text-center">
        <div class="flex justify-center space-x-3 text-center">
            <div class="w-10 h-10 p-16 bg-center bg-no-repeat bg-cover rounded-full"
                style="background-image: url({{ Storage::disk('hrim_gcs')->url($user->photo_path . $user->photo_name) }})">
            </div>
            <div class="flex flex-col items-start justify-center">
                <p class="text-3xl font-medium uppercase">{{ $user->name ?? 'N/A' }}</p>
                <p class="text-base">{{ $user->userDetails->position->display ?? 'N/A' }}</p>
            </div>
        </div>
        <div class="flex items-end justify-between space-x-3">
            @can('hrim_employee_information_edit')
                <button type="button" wire:click="action({'id': {{ $user->id }}}, '201_files')"
                    class="flex px-4 py-2 space-x-2 text-sm font-medium bg-gray-200 border border-gray-500 rounded shadow-md hover:bg-blue-100">
                    <svg class="w-5 h-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                        <path fill="currentColor"
                            d="M256 0v128h128L256 0zM224 128L224 0H48C21.49 0 0 21.49 0 48v416C0 490.5 21.49 512 48 512h288c26.51 0 48-21.49 48-48V160h-127.1C238.3 160 224 145.7 224 128zM272 416h-160C103.2 416 96 408.8 96 400C96 391.2 103.2 384 112 384h160c8.836 0 16 7.162 16 16C288 408.8 280.8 416 272 416zM272 352h-160C103.2 352 96 344.8 96 336C96 327.2 103.2 320 112 320h160c8.836 0 16 7.162 16 16C288 344.8 280.8 352 272 352zM288 272C288 280.8 280.8 288 272 288h-160C103.2 288 96 280.8 96 272C96 263.2 103.2 256 112 256h160C280.8 256 288 263.2 288 272z" />
                    </svg>
                    <span>View 201 File</span>
                </button>
            @endcan
            <button type="button"
                class="flex px-4 py-2 space-x-2 text-sm font-medium bg-gray-200 border border-gray-500 rounded shadow-md hover:bg-blue-100">
                <svg class="w-5 h-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path fill="currentColor"
                        d="M448 192H64C28.65 192 0 220.7 0 256v96c0 17.67 14.33 32 32 32h32v96c0 17.67 14.33 32 32 32h320c17.67 0 32-14.33 32-32v-96h32c17.67 0 32-14.33 32-32V256C512 220.7 483.3 192 448 192zM384 448H128v-96h256V448zM432 296c-13.25 0-24-10.75-24-24c0-13.27 10.75-24 24-24s24 10.73 24 24C456 285.3 445.3 296 432 296zM128 64h229.5L384 90.51V160h64V77.25c0-8.484-3.375-16.62-9.375-22.62l-45.25-45.25C387.4 3.375 379.2 0 370.8 0H96C78.34 0 64 14.33 64 32v128h64V64z" />
                </svg>
                <span>Print</span>
            </button>
            @can('hrim_employee_information_edit')
                <button type="button" wire:click="action({'id': {{ $user->id }}}, 'edit')"
                    class="flex px-4 py-2 space-x-2 text-sm font-medium bg-gray-200 border border-gray-500 rounded shadow-md hover:bg-blue-100">
                    <svg class="w-5 h-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path fill="currentColor"
                            d="M362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32zM421.7 220.3L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3z" />
                    </svg>
                    <span>Edit</span>
                </button>
            @endcan
        </div>
    </div>
    <div class="grid grid-cols-12 gap-3">
        <div class="flex flex-col col-span-5 space-y-4">
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Employee Information</div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Employee ID:</p>
                        <p class="col-span-2">{{ $user->userDetails->employee_number ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Branch:</p>
                        <p class="col-span-2">{{ $user->userDetails->branch->display ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Date Hired:</p>
                        <p class="col-span-2">
                            {{ $user->userDetails->hired_date ? date('F d, Y', strtotime($user->userDetails->hired_date)) : 'N/A' }}
                        </p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Position:</p>
                        <p class="col-span-2">{{ $user->userDetails->position->display ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Job Level:</p>
                        <p class="col-span-2">{{ $user->userDetails->jobLevel->display ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Department:</p>
                        <p class="col-span-2">{{ $user->userDetails->department->display ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Division:</p>
                        <p class="col-span-2">{{ $user->userDetails->division->name ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Gender:</p>
                        <p class="col-span-2">{{ $user->userDetails->gender->display ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Date of Birth:</p>
                        <p class="col-span-2">
                            {{ $user->userDetails->birth_date ? date('F d, Y', strtotime($user->userDetails->birth_date)) : 'N/A' }}
                        </p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Current Address:</p>
                        <p class="col-span-2">{{ $user->userDetails->current_address ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Permanent Address:</p>
                        <p class="col-span-2">{{ $user->userDetails->permanent_address ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Personal Mobile Number:</p>
                        <p class="col-span-2">{{ $user->userDetails->personal_mobile_number ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Personal Telephone Number:</p>
                        <p class="col-span-2">{{ $user->userDetails->personal_telephone_number ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Personal Email Address:</p>
                        <p class="col-span-2">{{ $user->userDetails->personal_email ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Company Mobile Number:</p>
                        <p class="col-span-2">{{ $user->userDetails->company_mobile_number ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Company Telephone Number:</p>
                        <p class="col-span-2">{{ $user->userDetails->company_telephone_number ?? 'N/A' }}</p>
                    </div>
                </div>
            </div>
            <div class="grid grid-cols-2 gap-3">
                <div class="text-sm font-medium border border-gray-500 rounded-md">
                    <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Schedule
                    </div>
                    <div class="flex flex-col p-3 space-y-2">
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-gray-500 text-1xs">Work Schedule:</p>
                            <div class="flex flex-col col-span-2 space-y-1">
                                @if ($user->userDetails->workSchedule)
                                    <p>{{ $user->userDetails->workSchedule->name }}</p>
                                    <p class="text-1xs">
                                        {{ date('h:i A', strtotime($user->userDetails->workSchedule->time_from)) . ' - ' . date('h:i A', strtotime($user->userDetails->workSchedule->time_to)) }}
                                    </p>
                                    <p class="text-1xs">
                                        @if ($user->userDetails->workSchedule->monday)
                                            Monday
                                        @endif
                                        @if ($user->userDetails->workSchedule->tuesday)
                                            , Tuesday
                                        @endif
                                        @if ($user->userDetails->workSchedule->wednesday)
                                            , Wednesday
                                        @endif
                                        @if ($user->userDetails->workSchedule->thursday)
                                            , Thursday
                                        @endif
                                        @if ($user->userDetails->workSchedule->friday)
                                            , Firday
                                        @endif
                                        @if ($user->userDetails->workSchedule->saturday)
                                            , Saturday
                                        @endif
                                        @if ($user->userDetails->workSchedule->sunday)
                                            , Sunday
                                        @endif
                                    </p>
                                @else
                                    N/A
                                @endif
                            </div>
                        </div>
                        <div>
                            <button type="button"  wire:click="$set('schedule_modal', true)" class="text-[#003399] underline text-1xs">View Schedule</button>
                        </div>
                    </div>
                </div>
                <div class="text-sm font-medium border border-gray-500 rounded-md">
                    <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Earnings
                    </div>
                    <div class="flex flex-col p-3 space-y-2">
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-gray-500 text-1xs">Basic Pay:</p>
                            <p class="col-span-2 text-right">
                                {{ number_format($user->earnings->basic_pay ?? 0, 2) }}</p>
                        </div>
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-gray-500 text-1xs">COLA:</p>
                            <p class="col-span-2 text-right">
                                {{ number_format($user->earnings->cola ?? 0, 2) }}</p>
                        </div>
                        <div class="grid items-center grid-cols-3 gap-3">
                            <p class="text-gray-500 text-1xs">Gross Pay:</p>
                            <p class="col-span-2 text-right">
                                {{ number_format($user->earnings->gross_pay ?? 0, 2) }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex flex-col col-span-3 space-y-4">
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Government ID Information
                </div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">SSS:</p>
                        <p class="col-span-2">{{ $user->governmentInformation->sss_no ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">PHILHEALTH NO:</p>
                        <p class="col-span-2">{{ $user->governmentInformation->pagibig_no ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">PAGIBIG NO:</p>
                        <p class="col-span-2">{{ $user->governmentInformation->philhealth_no ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">TIN NO:</p>
                        <p class="col-span-2">{{ $user->governmentInformation->tin_no ?? 'N/A' }}</p>
                    </div>
                </div>
            </div>
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Employment Record
                </div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Employer:</p>
                        <p class="col-span-2">{{ $user->employmentRecord->employer ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Position:</p>
                        <p class="col-span-2">{{ $user->employmentRecord->position ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Inclusive Years:</p>
                        @if ($user->employmentRecord)
                            <p class="col-span-2">
                                {{ $user->employmentRecord->inclusive_year_from . ' - ' . $user->employmentRecord->inclusive_year_to }}
                            </p>
                        @else
                            <p class="col-span-2"> N/A</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Family Information
                </div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="grid items-center grid-cols-2 gap-3">
                        <p class="text-gray-500 text-1xs">Mother's Maiden Name:</p>
                        <p>{{ $user->familyInformation->mothers_maiden_name ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-2 gap-3">
                        <p class="text-gray-500 text-1xs">Father's Name:</p>
                        <p>{{ $user->familyInformation->fathers_name ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-2 gap-3">
                        <p class="text-gray-500 text-1xs">Siblings:</p>
                        <div>
                            @if ($user->familyInformation)
                                @forelse($user->familyInformation->siblings as $sibling)
                                    <p> {{ $sibling->name }}</p>
                                @empty
                                    N/A
                                @endforelse
                            @else
                                N/A
                            @endif
                        </div>
                    </div>
                    <div class="grid items-center grid-cols-2 gap-3">
                        <p class="text-gray-500 text-1xs">Spouse:</p>
                        <p>{{ $user->familyInformation->spouse ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-2 gap-3">
                        <p class="text-gray-500 text-1xs">Children:</p>
                        <div>
                            @if ($user->familyInformation)
                                @forelse($user->familyInformation->childrens as $children)
                                    <p> {{ $children->name }}</p>
                                @empty
                                    N/A
                                @endforelse
                            @else
                                N/A
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex flex-col col-span-4 space-y-4">
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Emergency Contact Details
                </div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Name:</p>
                        <p class="col-span-2">{{ $user->emergencyContactDetails->name ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Relationship:</p>
                        <p class="col-span-2">
                            {{ $user->emergencyContactDetails->ecd_relationship_id ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Address:</p>
                        <p class="col-span-2">{{ $user->emergencyContactDetails->address ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Mobile Number:</p>
                        <p class="col-span-2">{{ $user->emergencyContactDetails->mobile_number ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Telephone Number:</p>
                        <p class="col-span-2">{{ $user->emergencyContactDetails->telephone_number ?? 'N/A' }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Character Reference
                </div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Name:</p>
                        <p class="col-span-2">{{ $user->characterReference->name ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Company:</p>
                        <p class="col-span-2">{{ $user->characterReference->company ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Position:</p>
                        <p class="col-span-2">{{ $user->characterReference->position ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Mobile Number:</p>
                        <p class="col-span-2">{{ $user->characterReference->mobile_number ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Telephone Number:</p>
                        <p class="col-span-2">{{ $user->characterReference->telephone_number ?? 'N/A' }}</p>
                    </div>
                </div>
            </div>
            <div class="text-sm font-medium border border-gray-500 rounded-md">
                <div class="px-3 py-2 text-lg bg-gray-200 border border-gray-500 rounded-md">Educational Attainment
                </div>
                <div class="flex flex-col p-3 space-y-2">
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Level:</p>
                        <p class="col-span-2">{{ $user->edutcationalAttainment->level->display ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Educational Institution:</p>
                        <p class="col-span-2">
                            {{ $user->edutcationalAttainment->educational_institution ?? 'N/A' }}</p>
                    </div>
                    <div class="grid items-center grid-cols-3 gap-3">
                        <p class="text-gray-500 text-1xs">Inclusive Years:</p>
                        @if ($user->employmentRecord)
                            <p class="col-span-2">
                                {{ $user->edutcationalAttainment->inclusive_year_from . ' - ' . $user->edutcationalAttainment->inclusive_year_to }}
                            </p>
                        @else
                            <p class="col-span-2"> N/A</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


