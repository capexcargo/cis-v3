<x-form x-data="{ search_form: false">
    <x-loading />
    <x-slot name="header_title">Disciplinary Record Management</x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-5 gap-4 md:w-3/4">
            <button wire:click="action({}, 'redirectToCocSection')"
                class="flex flex-col items-center justify-center p-4 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <div class="items-center px-6">
                    <span class="inline-block font-medium text-left text-md">COC<br>SECTION</span>
                </div>
            </button>
            <button wire:click="action({}, 'redirectToViolation')"
                class="flex flex-col items-center justify-center p-4 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <div class="items-center px-6">
                    <span class="inline-block font-medium text-left text-md">VIOLATION</span>
                </div>
            </button>
            <button wire:click="action({}, 'redirectToDiscRecord')"
                class="flex flex-col items-center justify-center p-4 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <div class="items-center px-6">
                    <span class="inline-block font-medium text-left text-md">DISCIPLINARY<br>RECORD</span>
                </div>
            </button>
            <button wire:click="action({}, 'redirectToSanction')"
                class="flex flex-col items-center justify-center p-4 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <div class="items-center px-6">
                    <span class="inline-block font-medium text-left text-md">SANCTION</span>
                </div>
            </button>
            <button wire:click="action({}, 'redirectToSanctionStat')"
                class="flex flex-col items-center justify-center p-4 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <div class="items-center px-6">
                    <span class="inline-block font-medium text-left text-md">SANCTION<br>STATUS</span>
                </div>
            </button>
        </div>
    </x-slot>
</x-form>
