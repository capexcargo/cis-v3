<div>
    <x-loading></x-loading>
    <form autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3 pt-4">
                <div wire:init="load">
                    <div x-data="{ open: false }" class="relative mb-2 rounded-md" @click.away="open = false">
                        <div>
                            <x-label for="employee_name_search" value="Employee Name" :required="true" />
                            <span class="relative block">
                                <span class="absolute inset-y-0 left-0 flex items-center ml-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-4 text-blue" aria-hidden="true"
                                        focusable="false" data-prefix="far" data-icon="edit" role="img"
                                        viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M500.3 443.7l-119.7-119.7c27.22-40.41 40.65-90.9 33.46-144.7C401.8 87.79 326.8 13.32 235.2 1.723C99.01-15.51-15.51 99.01 1.724 235.2c11.6 91.64 86.08 166.7 177.6 178.9c53.8 7.189 104.3-6.236 144.7-33.46l119.7 119.7c15.62 15.62 40.95 15.62 56.57 0C515.9 484.7 515.9 459.3 500.3 443.7zM79.1 208c0-70.58 57.42-128 128-128s128 57.42 128 128c0 70.58-57.42 128-128 128S79.1 278.6 79.1 208z" />
                                    </svg>
                                </span>
                                <input disabled
                                    class="placeholder:placeholder:text-slate-400 block bg-white w-full px-9 border border-slate-300 rounded-md py-2 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1"
                                    placeholder="Employee Name" type="text" @click="open = !open"
                                    wire:model='employee_name_search' name="employee_name" />
                            </span>

                            <x-input-error for="employee_name_search" />
                            <x-input-error for="employee_name" />
                        </div>
                        <div x-show="open" x-cloak
                            class="absolute w-full p-2 my-1 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96">
                            <ul class="list-reset">
                                @forelse ($employee_name_reference as $i => $employee_name_ref)
                                    <li @click="open = !open" wire:click="getEmployeeName({{ $employee_name_ref->id }})"
                                        wire:key="{{ 'employee_name' . $i }}"
                                        class=" p-2 text-black cursor-pointer hover:bg-gray-200">
                                        <p>
                                            {{ $employee_name_ref->name }}
                                        </p>
                                    </li>
                                @empty
                                    <li>
                                        <p class=" p-2 text-black cursor-pointer hover:bg-gray-200">
                                            No User Found.
                                        </p>
                                    </li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
                <div>
                    <x-label for="position" value="Position" />
                    <x-input type="text" name="position" wire:model.defer='position' disabled></x-input>
                    <x-input-error for="position" />
                </div>
                <div class="grid grid-cols-2 gap-4">
                    <div>
                        <x-label for="incident_date" value="Incident Date" :required="true" />
                        <x-input type="date" name="incident_date" wire:model.defer='incident_date'></x-input>
                        <x-input-error for="incident_date" />
                    </div>
                    <div>
                        <x-label for="incident_time" value="Incident Time" :required="true" />
                        <x-input type="time" name="incident_time" wire:model.defer='incident_time'></x-input>
                        <x-input-error for="incident_time" />
                    </div>
                </div>
                <div wire:init="loadIncidentLocation">
                    <x-label for="incident_location" value="Incident Location" :required="true" />
                    <x-select name="incident_location" wire:model='incident_location'>
                        <option value=""></option>
                        @foreach ($incident_location_reference as $incident_location_ref)
                            <option value="{{ $incident_location_ref->id }}">
                                {{ $incident_location_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="incident_location" />
                </div>
                <div>
                    <x-label for="description" value="Description" :required="true" />
                    <x-textarea type="text" name="description" wire:model.defer='description'></x-textarea>
                    <x-input-error for="description" />
                </div>
                <div wire:init="loadCocSection">
                    <x-label for="coc_section" value="COC Section" :required="true" />
                    <x-select name="coc_section" wire:model='coc_section'>
                        <option value=""></option>
                        @foreach ($coc_section_reference as $coc_section_ref)
                            <option value="{{ $coc_section_ref->id }}">
                                {{ $coc_section_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="coc_section" />
                </div>
                <div wire:init="loadViolation">
                    <x-label for="violation" value="Violation" :required="true" />
                    <x-select name="violation" wire:model='violation'>
                        <option value=""></option>
                        @foreach ($violation_reference as $violation_ref)
                            <option value="{{ $violation_ref->id }}">
                                {{ $violation_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="violation" />
                </div>
                <div wire:init="loadDisciplinaryRecord">
                    <x-label for="disciplinary_record" value="Disciplinary Record" :required="true" />
                    <x-select name="disciplinary_record" wire:model='disciplinary_record'>
                        <option value=""></option>
                        @foreach ($disciplinary_record_reference as $disciplinary_record_ref)
                            <option value="{{ $disciplinary_record_ref->id }}">
                                {{ $disciplinary_record_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="disciplinary_record" />
                </div>
                <div wire:init="loadSanction">
                    <x-label for="sanction" value="Sanction" :required="true" />
                    <x-select name="sanction" wire:model='sanction'>
                        <option value=""></option>
                        @foreach ($sanction_reference as $sanction_ref)
                            <option value="{{ $sanction_ref->id }}">
                                {{ $sanction_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="sanction" />
                </div>
                <div wire:init="loadSanctionStatus">
                    <x-label for="sanction_status" value="Sanction Status" :required="true" />
                    <x-select name="sanction_status" wire:model='sanction_status'>
                        <option value=""></option>
                        @foreach ($sanction_status_reference as $sanction_status_ref)
                            <option value="{{ $sanction_status_ref->id }}">
                                {{ $sanction_status_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="sanction_status" />
                </div>

                <div>
                    <div class="text-2xl font-bold text-blue">
                        <div class="flex items-center space-x-3">
                            <x-label for="attachment" value="Attachment" :required="true" />
                        </div>
                    </div>
                    <div class="grid gap-2">
                        <div class="flex flex-col space-y-3">
                            <x-table.table>
                                <x-slot name="thead">
                                    {{-- <x-table.th name="File" />
                                <x-table.th name="Action" /> --}}
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($attachments as $a => $attachment)
                                        @if (!$attachment['is_deleted'])
                                            <tr
                                                class="text-sm  border-0 cursor-pointer even:bg-white hover:text-white hover:bg-blue-200">
                                                <td class="flex items-left justify-left p-2 whitespace-nowrap">
                                                    <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                        <div class="relative z-0 ">
                                                            <div class="absolute top-0 left-0">
                                                                @if ($attachment['id'] && !$attachments[$a]['attachment'])
                                                                    @if (in_array($attachment['extension'], config('filesystems.image_type')))
                                                                        <div
                                                                            class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                                            <a href="{{ Storage::disk('hrim_gcs')->url($attachment['path'] . $attachment['name']) }}"
                                                                                target="_blank"><img
                                                                                    class="w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                                    src="{{ Storage::disk('hrim_gcs')->url($attachment['path'] . $attachment['name']) }}"></a>
                                                                        </div>
                                                                    @endif
                                                                @else
                                                                    @if (!$attachments[$a]['attachment'])
                                                                        <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                            src="{{ $attachments[$a]['attachment'] ? $attachments[$a]['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                                    @elseif (in_array($attachments[$a]['attachment']->extension(), config('filesystems.image_type')))
                                                                        <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                            src="{{ $attachments[$a]['attachment'] ? $attachments[$a]['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                                    @else
                                                                        <svg class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="fas" data-icon="file-alt"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 384 512">
                                                                            <path fill="currentColor"
                                                                                d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z">
                                                                            </path>
                                                                        </svg>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                            <input type="file"
                                                                name="attachments.{{ $a }}.attachment"
                                                                wire:model="attachments.{{ $a }}.attachment"
                                                                class="relative z-50 block w-20 h-20 opacity-0 cursor-pointer">
                                                            <x-input-error
                                                                for="attachments.{{ $a }}.attachment" />
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-2 whitespace-nowrap">
                                                    <div class="flex items-center justify-center space-x-3">
                                                        @if (in_array($attachment['extension'], config('filesystems.image_type')))
                                                            <a href="{{ Storage::disk('hrim_gcs')->url($attachment['path'] . $attachment['name']) }}"
                                                                target="_blank">
                                                                <p class="text-blue-600 underline underline-offset-2">
                                                                    View
                                                                </p>
                                                                <svg hidden class="w-5 h-5 text-blue"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    viewBox="0 0 576 512" fill="currentColor">
                                                                    <path
                                                                        d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                                                </svg>
                                                            </a>
                                                        @elseif(in_array($attachment['extension'], config('filesystems.file_type')))
                                                            <svg wire:click="download({{ $attachment['id'] }})"
                                                                class="w-5 h-5 text-blue"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 512 512" fill="currentColor">
                                                                <path
                                                                    d="M480 352h-133.5l-45.25 45.25C289.2 409.3 273.1 416 256 416s-33.16-6.656-45.25-18.75L165.5 352H32c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h448c17.67 0 32-14.33 32-32v-96C512 366.3 497.7 352 480 352zM432 456c-13.2 0-24-10.8-24-24c0-13.2 10.8-24 24-24s24 10.8 24 24C456 445.2 445.2 456 432 456zM233.4 374.6C239.6 380.9 247.8 384 256 384s16.38-3.125 22.62-9.375l128-128c12.49-12.5 12.49-32.75 0-45.25c-12.5-12.5-32.76-12.5-45.25 0L288 274.8V32c0-17.67-14.33-32-32-32C238.3 0 224 14.33 224 32v242.8L150.6 201.4c-12.49-12.5-32.75-12.5-45.25 0c-12.49 12.5-12.49 32.75 0 45.25L233.4 374.6z" />
                                                            </svg>
                                                        @endif
                                                        @if (count(collect($attachments)->where('is_deleted', false)) > 1)
                                                            {{-- <a wire:click="removeAttachments({{ $a }})">
                                                                <p class="text-red-600 underline underline-offset-2">
                                                                    Remove
                                                                </p>
                                                            </a> --}}
                                                            <svg wire:click="removeAttachments({{ $a }})"
                                                                class="w-3 h-3 text-red" data-toggle="tooltip" title="Remove"
                                                                aria-hidden="true" focusable="false" data-prefix="fas"
                                                                data-icon="times-circle" role="img"
                                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                                <path fill="currentColor"
                                                                    d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                                </path>
                                                            </svg>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                            <x-input-error for="attachments" />
                            <div class="flex items-center justify-start">
                                <button type="button" title="Add Attachment" wire:click="addAttachments"
                                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                    +</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex justify-end space-x-3 gap-3 pt-5">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-10 py-2 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-md hover:text-white hover:bg-red-400">Cancel</button>
                <button type="button" wire:click="submit"
                    class="px-10 py-2 text-sm flex-none bg-blue text-white rounded-md">
                    Submit</button>
            </div>
        </div>
    </form>
</div>
