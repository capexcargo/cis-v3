<x-form x-data="{
    search_form: false,
    confirmation_modal: '{{ $confirmation_modal }}',
    create_disciplinary_history_modal: '{{ $create_disciplinary_history_modal }}',
    edit_disciplinary_history_modal: '{{ $edit_disciplinary_history_modal }}',
    view_disciplinary_history_modal: '{{ $view_disciplinary_history_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_disciplinary_history_add')
            <x-modal id="create_disciplinary_history_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">File a Disciplinary Record</x-slot>
                <x-slot name="body">
                    @livewire('hrim.employee-management.disciplinary-history.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_disciplinary_history_edit')
            @if ($disciplinary_history_id && $edit_disciplinary_history_modal)
                <x-modal id="edit_disciplinary_history_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Edit Disciplinary Record</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.employee-management.disciplinary-history.edit', ['id' => $disciplinary_history_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan

        @can('hrim_disciplinary_history_view')
            @if ($disciplinary_history_id && $view_disciplinary_history_modal)
                <x-modal id="view_disciplinary_history_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                    <x-slot name="body">
                        @livewire('hrim.employee-management.disciplinary-history.view', ['id' => $disciplinary_history_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
            <x-slot name="body">
                <h2 class="mb-3 text-lg text-center text-gray-900">
                    {{ $confirmation_message }}
                </h2>
                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('confirmation_modal', false)"
                        class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click="confirm"
                        class="flex-none px-5 py-1 mt-4 text-sm text-white rounded-lg bg-blue">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>
    </x-slot>
    <x-slot name="header_title">Disciplinary History</x-slot>
    <x-slot name="header_button">
        @can('hrim_disciplinary_history_add')
            <button wire:click="action({}, 'create_disciplinary_history')"
                class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    File a Disciplinary Record
                </div>
            </button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="flex gap-6">
            <div class="w-40">
                <div>
                    <input placeholder="Employee Name" wire:model.debounce.500ms="employee_name_search"
                        name="employee_name_search"
                        class="text-sm w-full p-[5px] bg-transparent border-b border-black focus:border-b-2 focus:outline-none">
                </div>
            </div>
            <div class="w-40">
                <div>
                    <input placeholder="Date Filed" wire:model.debounce.500ms="date_created" name="date_created"
                        class="date text-sm w-full p-[5px] bg-transparent border-b border-black focus:border-b-2 focus:outline-none">
                </div>
            </div>
            <div class="w-40">
                <div>
                    <input placeholder="Incident Date" wire:model.debounce.500ms="incident_date" name="incident_date"
                        class="date text-sm w-full p-[5px] bg-transparent border-b border-black focus:border-b-2 focus:outline-none">
                </div>
            </div>
        </div>
        <br>
        <div class="bg-white rounded-lg shadow-md">
            <div class="my-2 overflow-auto rounded-lg">
                <div class="inline-block min-w-full align-middle">
                    <table class="w-full divide-y-2">
                        <thead>
                            <tr class="text-xs text-left text-gray-700 bg-white border-0 cursor-pointer">
                                <th class="px-2 py-1"></th>
                                <th class="px-2 py-1">Name</th>
                                <th class="px-2 py-1">Position</th>
                                <th class="px-2 py-1">Date Filed</th>
                                <th class="px-2 py-1">Incident Date and Time</th>
                                <th class="px-2 py-1">Incident Branch</th>
                                <th class="px-2 py-1">Description</th>
                                <th class="px-2 py-1">Type of Violation<br><span class="text-gray-400">COC
                                        Section</span></th>
                                <th class="px-2 py-1"><br><span class="text-gray-400">Violation</span></th>
                                <th class="px-2 py-1">Disciplinary Record</th>
                                <th class="px-2 py-1">Sanction</th>
                                <th class="px-2 py-1">Sanction Status</th>
                                <th class="px-2 py-1">Action</th>
                            </tr>
                        </thead>
                        <tbody class="text-sm bg-gray-100 border-0 divide-y-4 divide-white">
                            @foreach ($disciplinary_histories as $disciplinary_history)
                                @if ($disciplinary_history->user->division_id == Auth::user()->division_id)
                                    <tr>
                                        <td>
                                            <div class="relative hidden w-10 h-10 ml-2 rounded-full md:block">
                                                <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                                    style="background-image: url({{ Storage::disk('hrim_gcs')->url($disciplinary_history->user->photo_path . $disciplinary_history->user->photo_name) }})">
                                                </div>
                                            </div>
                                        </td>
                                        <td class="px-2 py-1 text-sm">
                                            <div class="flex items-center text-sm">
                                                <div>
                                                    <p class="flex-initial w-56 font-semibold">
                                                        {{ $disciplinary_history->user->name }}</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="px-2 py-1 text-sm">
                                            <p class="flex-initial w-44">
                                                {{ $disciplinary_history->userDetails->position->display }}</p>
                                        </td>
                                        <td class="px-2 py-1 text-sm">
                                            <p class="flex-initial w-32 ">
                                                {{ date('m/d/Y', strtotime($disciplinary_history->created_at)) }}</p>
                                        </td>
                                        <td class="px-2 py-1 text-sm">
                                            <p class="flex-initial w-40 ">
                                                {{ date('m/d/Y', strtotime($disciplinary_history->incident_date)) }}
                                                {{ date('H:m A', strtotime($disciplinary_history->incident_time)) }}
                                            </p>
                                        </td>
                                        <td class="px-2 py-1 text-sm">
                                            <p class="flex-initial w-32 ">
                                                {{ $disciplinary_history->branch->display }}</p>
                                        </td>
                                        <td class="px-2 py-1 text-sm">
                                            <p class="flex-initial w-40 ">{{ $disciplinary_history->description }}</p>
                                        </td>
                                        <td class="px-2 py-1 text-xs">
                                            <p class="flex-initial w-40 ">
                                                {{ $disciplinary_history->cocSection->display }}</p>
                                        </td>
                                        <td class="px-2 py-1 text-xs">
                                            <p class="flex-initial w-40 ">
                                                {{ $disciplinary_history->violation->display }}
                                            </p>
                                        </td>
                                        <td class="px-2 py-1 text-sm">
                                            <p class="flex-initial w-32 ">
                                                {{ $disciplinary_history->disciplinaryRecord->display }}</p>
                                        </td>
                                        <td class="px-2 py-1 text-sm">
                                            <p class="flex-initial w-32 ">
                                                {{ $disciplinary_history->sanction->display }}
                                            </p>
                                        </td>
                                        <td class="px-2 py-1 text-sm">
                                            <p class="flex-initial w-32 ">
                                                {{ $disciplinary_history->sanctionStatus->display }}</p>
                                        </td>
                                        <td class="px-2 py-1 text-sm">
                                            <div class="flex space-x-3">
                                                @can('hrim_disciplinary_history_edit')
                                                    <svg wire:click="action({'id': {{ $disciplinary_history->id }}}, 'edit_disciplinary_history')"
                                                        data-toggle="tooltip"
                                                        class="w-5 h-5 text-blue-800 cursor-pointer hover:text-blue-900"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                        </path>
                                                    </svg>
                                                @endcan

                                                @can('hrim_disciplinary_history_view')
                                                    <svg wire:click="action({'id': {{ $disciplinary_history->id }}}, 'view_disciplinary_history')"
                                                        data-toggle="tooltip" title="View Attachment"
                                                        class="w-5 h-5 text-gray-400 cursor-pointer hover:text-blue-700"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                        <path fill="currentColor"
                                                            d="M256 0v128h128L256 0zM224 128L224 0H48C21.49 0 0 21.49 0 48v416C0 490.5 21.49 512 48 512h288c26.51 0 48-21.49 48-48V160h-127.1C238.3 160 224 145.7 224 128zM272 416h-160C103.2 416 96 408.8 96 400C96 391.2 103.2 384 112 384h160c8.836 0 16 7.162 16 16C288 408.8 280.8 416 272 416zM272 352h-160C103.2 352 96 344.8 96 336C96 327.2 103.2 320 112 320h160c8.836 0 16 7.162 16 16C288 344.8 280.8 352 272 352zM288 272C288 280.8 280.8 288 272 288h-160C103.2 288 96 280.8 96 272C96 263.2 103.2 256 112 256h160C280.8 256 288 263.2 288 272z" />
                                                    </svg>
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
