<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}'
}" wire:init="load">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
        <x-slot name="body">
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                {{ $confirmation_message }}
                Disciplinary Record Summary

                <table class="mt-6 table-auto">
                    <tbody class="text-sm font-light text-gray-900">
                        <tr>
                            <td class=" whitespace-nowrap"><span class="mr-2 text-xs font-semibold text-gray-400">Name :
                                </span></td>
                            <td class="mr-2"><span class="mr-2 font-semibold">{{ $employee_name_search }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span
                                    class="mr-2 text-xs font-semibold text-gray-400">Position : </span></td>
                            <td class="mr-2"><span class="mr-2 font-semibold">{{ $position }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span
                                    class="mr-2 text-xs font-semibold text-gray-400">Incident Date and Time : </span>
                            </td>
                            <td class="mr-2"><span
                                    class="mr-2 font-semibold">{{ date('M. d, Y', strtotime($incident_date)) }}
                                    {{ $incident_time }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span
                                    class="mr-2 text-xs font-semibold text-gray-400">Incident Location : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @forelse ($incident_location_reference as $incident_location_ref)
                                        {{ $incident_location_ref->id == $incident_location ? $incident_location_ref->display : '' }}
                                    @empty
                                    @endforelse
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span
                                    class="mr-2 text-xs font-semibold text-gray-400">Description : </span></td>
                            <td class="mr-2"><span class="mr-2 font-semibold">{{ $description }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span class="mr-2 text-xs font-semibold text-gray-400">COC
                                    Section : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @forelse ($coc_section_reference as $coc_section_ref)
                                        {{ $coc_section_ref->id == $coc_section ? $coc_section_ref->display : '' }}
                                    @empty
                                    @endforelse
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span
                                    class="mr-2 text-xs font-semibold text-gray-400">Violation : </span></td>
                            <td class="mr-2">
                                <span class="flex-auto mr-2 font-semibold">
                                    @forelse ($violation_reference as $violation_ref)
                                        {{ $violation_ref->id == $violation ? $violation_ref->display : '' }}
                                    @empty
                                    @endforelse
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span
                                    class="mr-2 text-xs font-semibold text-gray-400">Disciplinary Record : </span>
                            </td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @forelse ($disciplinary_record_reference as $disciplinary_record_ref)
                                        {{ $disciplinary_record_ref->id == $disciplinary_record ? $disciplinary_record_ref->display : '' }}
                                    @empty
                                    @endforelse
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span
                                    class="mr-2 text-xs font-semibold text-gray-400">Sanction : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @forelse ($sanction_reference as $sanction_ref)
                                        {{ $sanction_ref->id == $sanction ? $sanction_ref->display : '' }}
                                    @empty
                                    @endforelse
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="whitespace-nowrap"><span
                                    class="mr-2 text-xs font-semibold text-gray-400">Sanction Status : </span></td>
                            <td class="mr-2">
                                <span class="mr-2 font-semibold">
                                    @forelse ($sanction_status_reference as $sanction_status_ref)
                                        {{ $sanction_status_ref->id == $sanction_status ? $sanction_status_ref->display : '' }}
                                    @empty
                                    @endforelse
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </h2>
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="confirm"
                    class="px-8 py-1 mt-4 text-sm flex-none bg-[#003399] text-white rounded-md">
                    Confirm</button>
            </div>
        </x-slot>
    </x-modal>

    <form autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3 pt-4">
                <div>
                    <div x-data="{ open: false }" class="relative mb-2 rounded-md" @click.away="open = false">
                        <div>
                            <x-label for="employee_name_search" value="Employee Name" :required="true" />
                            <span class="relative block">
                                <span class="absolute inset-y-0 left-0 flex items-center ml-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-4 text-blue" aria-hidden="true"
                                        focusable="false" data-prefix="far" data-icon="edit" role="img"
                                        viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M500.3 443.7l-119.7-119.7c27.22-40.41 40.65-90.9 33.46-144.7C401.8 87.79 326.8 13.32 235.2 1.723C99.01-15.51-15.51 99.01 1.724 235.2c11.6 91.64 86.08 166.7 177.6 178.9c53.8 7.189 104.3-6.236 144.7-33.46l119.7 119.7c15.62 15.62 40.95 15.62 56.57 0C515.9 484.7 515.9 459.3 500.3 443.7zM79.1 208c0-70.58 57.42-128 128-128s128 57.42 128 128c0 70.58-57.42 128-128 128S79.1 278.6 79.1 208z" />
                                    </svg>
                                </span>
                                <input
                                    class="block w-full py-2 pr-3 bg-white border rounded-md shadow-sm placeholder:placeholder:text-slate-400 px-9 border-slate-300 focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1"
                                    placeholder="Employee Name" type="text" @click="open = !open"
                                    wire:model='employee_name_search' name="employee_name" />
                            </span>

                            <x-input-error for="employee_name_search" />
                            <x-input-error for="employee_name" />
                        </div>
                        <div x-show="open" x-cloak
                            class="absolute w-full p-2 my-1 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96">
                            <ul class="list-reset">
                                @forelse ($employee_name_reference as $i => $employee_name_ref)
                                    <li @click="open = !open"
                                        wire:click="getEmployeeName({{ $employee_name_ref->id }})"
                                        wire:key="{{ 'employee_name' . $i }}"
                                        class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                        <p>
                                            {{ $employee_name_ref->name }}
                                        </p>
                                    </li>
                                @empty
                                    <li>
                                        <p class="p-2 text-black cursor-pointer hover:bg-gray-200">
                                            No User Found.
                                        </p>
                                    </li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
                <div>
                    <x-label for="position" value="Position" />
                    <x-input type="text" name="position" wire:model.defer='position' disabled></x-input>
                    <x-input-error for="position" />
                </div>
                <div class="grid grid-cols-2 gap-4">
                    <div>
                        <x-label for="incident_date" value="Incident Date" :required="true" />
                        <x-input type="date" name="incident_date" wire:model.defer='incident_date'></x-input>
                        <x-input-error for="incident_date" />
                    </div>
                    <div>
                        <x-label for="incident_time" value="Incident Time" :required="true" />
                        <x-input type="time" name="incident_time" wire:model.defer='incident_time'></x-input>
                        <x-input-error for="incident_time" />
                    </div>
                </div>
                <div wire:init="loadIncidentLocation">
                    <x-label for="incident_location" value="Incident Location" :required="true" />
                    <x-select name="incident_location" wire:model='incident_location'>
                        <option value=""></option>
                        @foreach ($incident_location_reference as $incident_location_ref)
                            <option value="{{ $incident_location_ref->id }}">
                                {{ $incident_location_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="incident_location" />
                </div>
                <div>
                    <x-label for="description" value="Description" :required="true" />
                    <x-textarea type="text" name="description" wire:model.defer='description'></x-textarea>
                    <x-input-error for="description" />
                </div>
                <div wire:init="loadCocSection">
                    <x-label for="coc_section" value="COC Section" :required="true" />
                    <x-select name="coc_section" wire:model='coc_section'>
                        <option value=""></option>
                        @foreach ($coc_section_reference as $coc_section_ref)
                            <option value="{{ $coc_section_ref->id }}">
                                {{ $coc_section_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="coc_section" />
                </div>
                <div wire:init="loadViolation">
                    <x-label for="violation" value="Violation" :required="true" />
                    <x-select name="violation" wire:model='violation'>
                        <option value=""></option>
                        @foreach ($violation_reference as $violation_ref)
                            <option value="{{ $violation_ref->id }}">
                                {{ $violation_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="violation" />
                </div>
                <div wire:init="loadDisciplinaryRecord">
                    <x-label for="disciplinary_record" value="Disciplinary Record" :required="true" />
                    <x-select name="disciplinary_record" wire:model='disciplinary_record'>
                        <option value=""></option>
                        @foreach ($disciplinary_record_reference as $disciplinary_record_ref)
                            <option value="{{ $disciplinary_record_ref->id }}">
                                {{ $disciplinary_record_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="disciplinary_record" />
                </div>
                <div wire:init="loadSanction">
                    <x-label for="sanction" value="Sanction" :required="true" />
                    <x-select name="sanction" wire:model='sanction'>
                        <option value=""></option>
                        @foreach ($sanction_reference as $sanction_ref)
                            <option value="{{ $sanction_ref->id }}">
                                {{ $sanction_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="sanction" />
                </div>
                <div wire:init="loadSanctionStatus">
                    <x-label for="sanction_status" value="Sanction Status" :required="true" />
                    <x-select name="sanction_status" wire:model='sanction_status'>
                        <option value=""></option>
                        @foreach ($sanction_status_reference as $sanction_status_ref)
                            <option value="{{ $sanction_status_ref->id }}">
                                {{ $sanction_status_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="sanction_status" />
                </div>

                <div>
                    <div class="text-2xl font-bold text-blue">
                        <div class="flex items-center space-x-3">
                            <x-label for="attachment" value="Attachment" :required="true" />
                        </div>
                    </div>
                    <div class="grid gap-2">
                        <div class="flex flex-col overflow-hidden">
                            <table class="w-full">
                                <thead>
                                </thead>
                                <tbody>
                                    @forelse ($attachments as $i => $attachment)
                                        <tr class="text-xs border-0 cursor-pointer even:bg-white">
                                            <td class="flex p-1 items-left justify-left whitespace-nowrap">
                                                <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap ">
                                                    <div class="relative z-0">
                                                        <input type="file"
                                                            name="attachments.{{ $i }}.attachment"
                                                            wire:model="attachments.{{ $i }}.attachment"
                                                            class="absolute top-0 left-0 z-50 opacity-0">

                                                        @if ($attachments[$i]['attachment'])
                                                            <label for="attachments.{{ $i }}.attachment"
                                                                class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 rounded-lg cursor-pointer">
                                                                <span class="flex gap-1">
                                                                    <svg class="w-3 h-3" aria-hidden="true"
                                                                        focusable="false" data-prefix="fas"
                                                                        data-icon="file-alt" role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                        </path>
                                                                    </svg>{{ $attachments[$i]['attachment']->getClientOriginalName() }}
                                                                </span>
                                                            </label>
                                                        @else
                                                            <label for="attachments.{{ $i }}.attachment"
                                                                class="relative z-30 block px-2 py-1 text-xs bg-gray-200 border border-gray-500 rounded-lg cursor-pointer">
                                                                <span class="flex gap-1">
                                                                    <svg class="w-3 h-3" aria-hidden="true"
                                                                        focusable="false" data-prefix="fas"
                                                                        data-icon="file-alt" role="img"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        viewBox="0 0 384 512">
                                                                        <path fill="currentColor"
                                                                            d="M364.2 83.8c-24.4-24.4-64-24.4-88.4 0l-184 184c-42.1 42.1-42.1 110.3 0 152.4s110.3 42.1 152.4 0l152-152c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-152 152c-64 64-167.6 64-231.6 0s-64-167.6 0-231.6l184-184c46.3-46.3 121.3-46.3 167.6 0s46.3 121.3 0 167.6l-176 176c-28.6 28.6-75 28.6-103.6 0s-28.6-75 0-103.6l144-144c10.9-10.9 28.7-10.9 39.6 0s10.9 28.7 0 39.6l-144 144c-6.7 6.7-6.7 17.7 0 24.4s17.7 6.7 24.4 0l176-176c24.4-24.4 24.4-64 0-88.4z">
                                                                        </path>
                                                                    </svg>Choose Attachment
                                                                </span>
                                                            </label>
                                                        @endif

                                                        <x-input-error
                                                            for="attachments.{{ $i }}.attachment" />
                                                    </div>

                                                </div>
                                            </td>
                                            <td class="p-2 whitespace-nowrap">
                                                @if (count($attachments) > 1)
                                                    <a wire:click="removeAttachments({{ $i }})">
                                                        <p class="text-red-600 underline underline-offset-2">Remove</p>
                                                    </a>
                                                    <svg hidden wire:click="removeAttachments({{ $i }})"
                                                        class="w-5 text-red" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="times-circle" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                            d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                        </path>
                                                    </svg>
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3">
                                                <p class="text-center">Empty.</p>
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            <div class="flex items-center justify-start">
                                <button type="button" title="Add Attachment" wire:click="addAttachments"
                                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                    +</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex justify-end gap-3 pt-5 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-10 py-2 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-md hover:text-white hover:bg-red-400">Cancel</button>
                <button type="button" wire:click="action({}, 'submit')"
                    class="flex-none px-10 py-2 text-sm text-white rounded-md bg-blue">
                    Submit</button>
            </div>
        </div>
    </form>
</div>
