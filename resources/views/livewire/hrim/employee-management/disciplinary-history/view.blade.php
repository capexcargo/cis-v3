<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
<div class="swiper mySwiper">
    @foreach ($requests as $i => $request)
        <div class="swiper-wrapper">
            @foreach ($request->attachments as $i => $attachment)
                <div class="swiper-slide">
                    <img class="object-fit min-w-screen min-h-screen"
                        src="{{ Storage::disk('hrim_gcs')->url($attachment->path . $attachment->name) }}"
                        alt="image" />
                </div>
            @endforeach
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-pagination"></div>
    @endforeach

</div>

<!-- Swiper JS -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script>
    var swiper = new Swiper(".mySwiper", {
        cssMode: true,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el: ".swiper-pagination",
        },
        mousewheel: true,
        keyboard: true,
    });
</script>
