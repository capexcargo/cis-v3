<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 mt-4 pt-4">
                <div>
                    <x-label for="display" value="Sanction Status" :required="true" />
                    <x-textarea type="text" name="display" wire:model.defer='display'></x-textarea>
                    <x-input-error for="display" />
                </div>
            </div>
            <div class="flex justify-end space-x-3 gap-3 pt-5">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-10 py-2 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-md hover:text-white hover:bg-red-400">Cancel</button>
                <button type="submit" class="px-10 py-2 text-sm flex-none bg-blue text-white rounded-md">
                    Save</button>
            </div>
        </div>
    </form>
</div>
