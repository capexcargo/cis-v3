<div>
    <x-loading></x-loading>
    <form autocomplete="off">
        <div class="space-y-3 mt-5">
            <div class="grid grid-cols-1 gap-3 mt-3">
                <div>
                    <x-label for="core_value" value="Core Value" :required="true" />
                    <x-textarea type="text" name="core_value" wire:model.defer='core_value'></x-textarea>
                    <x-input-error for="core_value" />
                </div>
            </div>
        </div>
        <div class="flex justify-end space-x-3 mt-8">
            <button type="button" wire:click="$emit('close_modal', 'create')"
                class="px-12 py-2 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                Cancel
            </button>
            <button type="button" wire:click="submit"
                class="px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                Submit
            </button>
        </div>
    </form>
</div>
