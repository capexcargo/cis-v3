<div>
    <form autocomplete="off">
        <div class="bg-white rounded-lg shadow-md">
            <x-table.table>
                <x-slot name="thead">
                    <th class="p-3 text-gray-400 border-2 border-b-0 whitespace-nowrap">Evaluator</th>
                    <th class="p-3 text-gray-400 border-2 border-b-0 whitespace-nowrap">Justification</th>
                    <th class="p-3 text-gray-400 border-2 border-b-0 whitespace-nowrap">Action</th>
                </x-slot>
                <x-slot name="tbody">
                    <x-loading></x-loading>
                    <tr x-data="{ show_1st_justification: 1 }">
                        <td class="p-3 border-2 border-b-0 whitespace-nowrap">
                            1st Evaluator
                        </td>
                        <td class="p-3 border-2 border-b-0 whitespace-nowrap">
                            <x-textarea name="first_justification" wire:model.defer='first_justification'
                                x-show="show_1st_justification === 0">
                            </x-textarea>
                            <div x-show="show_1st_justification === 1">{{ $first_justification }}</div>
                        </td>
                        <td class="p-3 border-2 border-b-0 whitespace-nowrap">
                            @if ($first_status == 1 && $third_status == 1 && $first_evaluator == Auth::user()->id)
                                <div class="flex gap-3">
                                    <svg x-show="show_1st_justification === 1" @click="show_1st_justification = 0"
                                        class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700" data-prefix="far"
                                        data-icon="cancel-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                    </svg>

                                    <button type="button" x-show="show_1st_justification === 0"
                                        wire:click="action({'id': {{ $eval_id }}}, 'save_1st_justification')"
                                        @click="show_1st_justification = 1"
                                        class="px-4 py-1 text-xs flex-none bg-[#003399] text-white rounded-md hover:bg-blue-700">
                                        Save
                                    </button>
                                </div>
                            @else
                                <svg class="w-4 h-4 text-gray-300 cursor-pointer" data-prefix="far"
                                    data-icon="cancel-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                </svg>
                            @endif
                        </td>
                    </tr>
                    <tr x-data="{ show_2nd_justification: 1 }">
                        <td class="p-3 border-2 border-b-0 whitespace-nowrap">
                            2nd Evaluator
                        </td>
                        <td class="p-3 border-2 border-b-0 whitespace-nowrap">
                            <x-textarea name="second_justification" wire:model.defer='second_justification'
                                x-show="show_2nd_justification === 0">
                            </x-textarea>
                            <div x-show="show_2nd_justification === 1">{{ $second_justification }}</div>
                        </td>
                        <td class="p-3 border-2 border-b-0 whitespace-nowrap">
                            @if (($second_status == 1 &&
                                $third_status == 1 &&
                                $third_evaluator != '' &&
                                $second_evaluator == Auth::user()->id) ||
                                ($second_status == 1 && $third_status == 1 && $third_evaluator == '' && $second_evaluator == Auth::user()->id))
                                <div class="flex gap-3">
                                    <svg x-show="show_2nd_justification === 1" @click="show_2nd_justification = 0"
                                        class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700" data-prefix="far"
                                        data-icon="cancel-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                    </svg>

                                    <button type="button" x-show="show_2nd_justification === 0"
                                        wire:click="action({'id': {{ $eval_id }}}, 'save_2nd_justification')"
                                        @click="show_2nd_justification = 1"
                                        class="px-4 py-1 text-xs flex-none bg-[#003399] text-white rounded-md hover:bg-blue-700">
                                        Save
                                    </button>
                                </div>
                            @else
                                <svg class="w-4 h-4 text-gray-300 cursor-pointer" data-prefix="far"
                                    data-icon="cancel-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                </svg>
                            @endif
                        </td>
                    </tr>
                    <tr x-data="{ show_3rd_justification: 1 }">
                        <td class="p-3 border-2 border-b-0 whitespace-nowrap">
                            3rd Evaluator
                        </td>
                        <td class="p-3 border-2 border-b-0 whitespace-nowrap">
                            <x-textarea name="third_justification" wire:model.defer='third_justification'
                                x-show="show_3rd_justification === 0">
                            </x-textarea>
                            <div x-show="show_3rd_justification === 1">{{ $third_justification }}</div>
                        </td>
                        <td class="p-3 border-2 border-b-0 whitespace-nowrap">
                            @if ($third_status == 1 && $third_evaluator != '' && $third_evaluator == Auth::user()->id)
                                <div class="flex gap-3">
                                    <svg x-show="show_3rd_justification === 1" @click="show_3rd_justification = 0"
                                        class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700" data-prefix="far"
                                        data-icon="cancel-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                    </svg>

                                    <button type="button" x-show="show_3rd_justification === 0"
                                        wire:click="action({'id': {{ $eval_id }}}, 'save_3rd_justification')"
                                        @click="show_3rd_justification = 1"
                                        class="px-4 py-1 text-xs flex-none bg-[#003399] text-white rounded-md hover:bg-blue-700">
                                        Save
                                    </button>
                                </div>
                            @else
                                <svg class="w-4 h-4 text-gray-300 cursor-pointer" data-prefix="far"
                                    data-icon="cancel-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                </svg>
                            @endif
                        </td>
                    </tr>
                </x-slot>
            </x-table.table>
        </div>
    </form>
</div>
