<x-form wire:init="load">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="header_title">Performance Evaluation</x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-5 gap-4 text-sm md:w-3/4">
            <div class="w-40">
                <div>
                    <x-transparent.input type="date" label="Date Evaluated (From)" name="date_evaluated_from"
                        wire:model.debounce.500ms="date_evaluated_from" />
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.input type="date" label="Date Evaluated (To)" name="date_evaluated_to"
                        wire:model.debounce.500ms="date_evaluated_to" />
                </div>
            </div>

            <div class="w-40">
                <div>
                    <x-transparent.select value="{{ $quarter }}" label="Quarter" name="quarter"
                        wire:model="quarter">
                        <option value=""></option>
                        <option value="1">1st Quarter</option>
                        <option value="2">2nd Quarter</option>
                        <option value="3">3rd Quarter</option>
                        <option value="4">4th Quarter</option>
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.select value="{{ $year }}" label="Year" name="year" wire:model="year">
                        <option value=""></option>
                        @for ($i = 2011; $i <= date('Y'); $i++)
                            <option value="{{ $i }}">
                                {{ $i }}
                            </option>
                        @endfor
                    </x-transparent.select>
                </div>
            </div>
        </div>
        <div class="grid grid-cols-5 gap-4 text-sm md:w-3/4">
            <div class="w-40">
                <div>
                    <x-transparent.input type="text" label="Employee Name" name="employee_name"
                        wire:model.debounce.500ms="employee_name" />
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.select value="{{ $position }}" label="Position" name="position"
                        wire:model="position">
                        <option value=""></option>
                        @foreach ($position_references as $position_reference)
                            <option value="{{ $position_reference->id }}">
                                {{ $position_reference->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>

            <div class="w-40">
                <div>
                    <x-transparent.select value="{{ $division }}" label="Division" name="division"
                        wire:model="division">
                        <option value=""></option>
                        @foreach ($division_references as $division_reference)
                            <option value="{{ $division_reference->id }}">
                                {{ $division_reference->name }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.select value="{{ $branch }}" label="Branch" name="branch"
                        wire:model="branch">
                        <option value=""></option>
                        @foreach ($branch_references as $branch_reference)
                            <option value="{{ $branch_reference->id }}">
                                {{ $branch_reference->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.select value="{{ $evaluator }}" label="Evaluator" name="evaluator"
                        wire:model="evaluator">
                        <option value=""></option>
                        @foreach ($evaluator_references as $evaluator_reference)
                            @if (
                                $division == ($evaluator_reference->userDetails->division_id ?? null) &&
                                    $branch == ($evaluator_reference->userDetails->branch_id ?? null) &&
                                    $division != '')
                                <option value="{{ $evaluator_reference->id }}">
                                    {{ $evaluator_reference->name }}
                                </option>
                            @endif
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
        </div>
        <div class="bg-white rounded-lg shadow-md">
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="No." />
                    <x-table.th name="Name" />
                    <x-table.th name="Position" />
                    <x-table.th name="Division" />
                    <x-table.th name="Branch" />
                    <x-table.th name="Date Evaluated" />
                    <x-table.th name="Quarter" />
                    <x-table.th name="Year" />
                    <x-table.th name="Final Score" />
                    <x-table.th name="Status" />
                    <x-table.th name="Action" />
                </x-slot>
                <x-slot name="tbody">
                    @forelse ($performance_evaluations as $i => $performance_evaluation)
                        <tr class="font-semibold ">
                            <td class="px-2 py-2 text-sm">
                                <p class="flex-initial">{{ $i + 1 }}.
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <div class="flex space-x-2 w-50">
                                    <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                        style="background-image: url({{ Storage::disk('hrim_gcs')->url($performance_evaluation->user->photo_path . $performance_evaluation->user->photo_name) }})">
                                    </div>
                                    <div class="flex items-center text-sm">
                                        <div>
                                            <p class="flex-initial">
                                                {{ $performance_evaluation->user->name }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $performance_evaluation->userDetails->position->display ?? null }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $performance_evaluation->userDetails->division->name }}

                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $performance_evaluation->userDetails->branch->display }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $performance_evaluation['first_status'] == 1 &&
                                $performance_evaluation['second_status'] == 1 &&
                                $performance_evaluation['third_status'] == 1
                                    ? ''
                                    : ($performance_evaluation['first_status'] == 2 &&
                                    $performance_evaluation['second_status'] == 1 &&
                                    $performance_evaluation['third_status'] == 1
                                        ? $performance_evaluation['first_eval_date']
                                        : ($performance_evaluation['first_status'] == 2 &&
                                        $performance_evaluation['second_status'] == 2 &&
                                        $performance_evaluation['third_status'] == 1
                                            ? $performance_evaluation['second_eval_date']
                                            : $performance_evaluation['third_eval_date'])) }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $performance_evaluation->quarter }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $performance_evaluation->year }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $performance_evaluation->final_score ?? '-' }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                <span class="px-2 py-1 text-xs bg-blue-300 rounded-full text-blue">
                                    {{ $performance_evaluation['first_status'] == 1 &&
                                    $performance_evaluation['second_status'] == 1 &&
                                    $performance_evaluation['third_status'] == 1
                                        ? 'For 1st Evaluation'
                                        : ($performance_evaluation['first_status'] == 2 &&
                                        $performance_evaluation['second_status'] == 1 &&
                                        $performance_evaluation['third_status'] == 1
                                            ? 'For 2nd Evaluation'
                                            : ($performance_evaluation['first_status'] == 2 &&
                                            $performance_evaluation['second_status'] == 2 &&
                                            $performance_evaluation['third_status'] == 1 &&
                                            $performance_evaluation['third_evaluator'] != ''
                                                ? 'For 3rd Evaluation'
                                                : 'Evaluated')) }}

                                </span>
                            </td>
                            <td class="w-20 p-3 whitespace-nowrap">
                                <div class="flex ml-2">
                                    <svg wire:click="action({'id': {{ $performance_evaluation['user_id'] }}, 'quarter': {{ $performance_evaluation['quarter'] }}, 'years': {{ $performance_evaluation['year'] }} }, 'view_performance')"
                                        class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700" data-prefix="far"
                                        data-icon="approve" role="img" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                    </svg>
                                </div>
                            </td>
                        </tr>
                    @empty
                    @endforelse
                </x-slot>
            </x-table.table>
            <div class="px-1 pb-2">
                {{ $performance_evaluations->links() }}
            </div>
        </div>
    </x-slot>
</x-form>
