<div wire:init="load" x-data="{
    confirmation_modal: '{{ $confirmation_modal }}'
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <h2 class="mb-3 text-lg text-center text-gray-900">
                {{ $confirmation_message }}
            </h2>
            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                <button type="button" wire:click="submit"
                    class="flex-none px-5 py-1 mt-4 text-sm text-white rounded-lg bg-blue">
                    Yes</button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="mt-5 space-y-3">
            <div class="grid grid-cols-1 gap-3 mt-3">
                <div>
                    <x-label for="title" value="Title" :required="true" />
                    <x-input type="text" name="title" wire:model.defer='title'></x-input>
                    <x-input-error for="title" />
                </div>

                <div>
                    <x-label for="description" value="Description" :required="true" />
                    <x-textarea type="text" name="description" wire:model.defer='description'></x-textarea>
                    <x-input-error for="description" />
                </div>
                <div class="grid grid-cols-2 gap-3 mt-3">
                    <div>
                        <x-label for="position" value="Tag Position/s" />
                        <x-select type="text" name="is_all_tagged" wire:model='is_all_tagged'
                            wire:change="loadPositions">
                            <option value="">Select</option>
                            <option value="1">All Positions</option>
                        </x-select>
                        <x-input-error for="position" />
                    </div>
                    <div>
                        <x-label for="division" value="Tag Division" />
                        <x-select type="text" name="division" wire:model='division' wire:init="loadDivisions"
                            wire:change="loadDivisions">
                            <option value="">Select</option>
                            @foreach ($division_references as $division)
                                <option value="{{ $division->id }}">{{ $division->name }}</option>
                            @endforeach
                        </x-select>
                    </div>
                    <x-input-error for="division" />
                </div>
                <div>
                    <x-input-error for="tag_atleast_one" />
                </div>
                <div wire:init="loadPositions" class="text-sm -mt-3">
                    @foreach ($position_references as $i => $position)
                        <div class="flex">
                            <span class="inline-block mb-1">
                                <input type="checkbox" class="form-checkbox" value="{{ $position->id }}"
                                    wire:model="positions.{{ $i }}.position">
                                {{ $position->display }}
                            </span>
                        </div>
                    @endforeach
                    <x-input-error for="positions" />
                </div>
                <div class="grid grid-cols-1">
                    <div>
                        <div class="overflow-hidden text-2xl text-blue">
                            <div class="flex items-center -mb-1 space-x-3">
                                <x-label for="attachment" value="Video File" :required="true" />
                            </div>
                            <table>
                                <tbody class="bg-transparent">
                                    @foreach ($attachments as $i => $attachment)
                                        <tr class="text-sm border-0">
                                            <td class="flex gap-3 items-left justify-left cursor-pointer">
                                                <div class="relative z-0 bg-transparent">
                                                    <input type="file"
                                                        name="attachments.{{ $i }}.attachment"
                                                        wire:model="attachments.{{ $i }}.attachment"
                                                        class="absolute top-0 left-0 z-50 opacity-0 w-32">
                                                    <label for="attachment"
                                                        class="relative z-30 block px-10 py-1 bg-gray-200 border border-gray-500 rounded-md cursor-pointer">
                                                        <span class="flex gap-1">
                                                            Upload
                                                        </span>
                                                    </label>
                                                </div>
                                                @if ($attachments[$i]['attachment'])
                                                    <div class="mt-1 underline">
                                                        {{ $attachments[$i]['attachment']->getClientOriginalName() }}
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                        <x-input-error for="attachments.{{ $i }}.attachment" />
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="flex justify-end mt-8 space-x-3">
                    <button type="button" wire:click="$emit('close_modal', 'create')"
                        class="px-12 py-2 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <button type="submit" title="Submit"
                        class="px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                        Upload
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
