<div>
    <div class="-mt-2 text-2xl font-semibold">{{ $title }}</div>
    <div class="flex gap-4 py-4 cursor-pointer">
        <div class="w-2/3" >
            <video controls class="w-full rounded-md shadow-lg" style="height: 360px;" autoplay>
                <source src="{{ Storage::disk('hrim_gcs')->url($attachments[0]['path'] . $attachments[0]['name']) }}"
                    type="video/mp4">
            </video>
        </div>
        <div class="w-1/3">
            <div class="mb-3"> {{ $description }}
            </div>
            <div class="flex py-2 gap-2">
                <p class="whitespace-nowrap text-xs text-gray-400 w-1/6">
                    Tag To:
                </p>
                <span class="text-gray-700 text-xs w-5/6">
                    @if ($is_all_tagged == 1)
                        All Positions
                    @else
                        <div class="grid grid-cols-1">
                            @foreach ($get_tagged_position as $i => $position)
                                @if ($video_ref == $position->video_ref)
                                    <p class="text-sm">{{ $position->taggedPosition->display }}
                                    </p>
                                @endif
                            @endforeach
                        </div>
                    @endif
                </span>
            </div>
            <div class="flex justify-start gap-6">
                <p class="whitespace-nowrap mt-1 text-xs text-gray-400 w-1/6">File Name : </p>
                <p class="font-semibold text-blue text-sm w-5/6 underline">{{ $attachments[0]['original_filename'] }} </p>
            </div>
        </div>
    </div>
</div>
