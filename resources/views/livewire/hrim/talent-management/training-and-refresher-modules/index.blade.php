<x-form x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    play_modal: '{{ $play_modal }}',
    edit_modal: '{{ $edit_modal }}',
    view_modal: '{{ $view_modal }}',
    confirmation_modal: '{{ $confirmation_modal }}',
}" wire:init="load">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_training_and_refresher_modules_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Upload Video</x-slot>
                <x-slot name="body">
                    @livewire('hrim.talent-management.training-and-refresher-modules.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('hrim_training_and_refresher_modules_play')
            @if ($train_and_ref_id && $play_modal)
                <x-modal id="play_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/2">
                    <x-slot name="body">
                        @livewire('hrim.talent-management.training-and-refresher-modules.play', ['id' => $train_and_ref_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('hrim_training_and_refresher_modules_edit')
            @if ($train_and_ref_id && $edit_modal)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Edit Video Details</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.talent-management.training-and-refresher-modules.edit', ['id' => $train_and_ref_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('hrim_training_and_refresher_modules_view')
            @if ($train_and_ref_id && $view_modal)
                <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                    <x-slot name="body">
                        @livewire('hrim.talent-management.training-and-refresher-modules.view', ['id' => $train_and_ref_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('hrim_training_and_refresher_modules_delete')
            <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="body">
                    <h2 class="text-lg text-center text-gray-900">
                        {{ $confirmation_message }}
                    </h2>
                    <div class="flex justify-center space-x-3">
                        <button type="button" wire:click="$set('confirmation_modal', false)"
                            class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                        <button type="button" wire:click="confirm"
                            class="flex-none px-5 py-1 mt-4 text-sm text-white rounded-lg bg-blue">
                            Yes</button>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
    </x-slot>
    <x-slot name="header_title">Training & Refresher Modules</x-slot>
    <x-slot name="header_button">
        @can('hrim_training_and_refresher_modules_add')
            <button wire:click="action({}, 'create')" class="p-2 px-6 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg hidden class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Upload
                </div>
            </button>
        @endcan
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-5 gap-4 text-sm md:w-3/4">
            <div class="w-40">
                {{-- <div>
                    <x-transparent.input type="text" label="Employee Name" name="employee_name"
                        wire:model.debounce.500ms="employee_name" />
                </div> --}}

                <div x-data="{ open: false }" class="relative mb-2 rounded-md" @click.away="open = false">
                    <div>
                        <span class="relative block">
                            <div class="relative z-0 w-full mb-5">
                                <input type="text" @click="open = !open" wire:model='employee_name_search'
                                    name="employee_name" placeholder=" "
                                    class="pt-2 pl-2 pb-1 block w-full mt-0 bg-transparent border-0 border-b-2 appearance-none focus:outline-none focus:ring-0 focus:border-[#003399] border-gray-600" />
                                <label :class="{ 'whitespace-nowrap': open }"
                                    class="absolute text-gray-500 duration-300 form-control focus:text-blue focus:whitespace-nowrap top-3 -z-1 origin-0">
                                    <span
                                        class="@if ($employee_name_search != '') whitespace-nowrap @endif leading-6">Employee
                                        Name</span>
                                </label>
                            </div>
                        </span>
                    </div>
                    <div x-show="open" x-cloak
                        class="absolute z-50 w-full p-2 overflow-hidden overflow-y-auto bg-gray-100 rounded shadow max-h-96"
                        style="margin:-16px 0 0 0">
                        <ul class="list-reset">
                            @if ($employee_name_search != '')
                                @foreach ($employee_references as $i => $employee_data)
                                    <li @click="open = !open" wire:click="getEmployeeDetails({{ $employee_data->id }})"
                                        wire:key="{{ 'employee_name' . $i }}"
                                        class="px-2 text-black cursor-pointer hover:bg-gray-200">
                                        <p>
                                            {{ $employee_data->name }}
                                        </p>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.select value="{{ $division }}" label="Division" name="division"
                        wire:model="division">
                        <option value=""></option>
                        @foreach ($division_references as $division_reference)
                            <option value="{{ $division_reference->id }}">
                                {{ $division_reference->name }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.select value="{{ $position }}" label="Position" name="position"
                        wire:model="position">
                        <option value=""></option>
                        @foreach ($position_references as $position_reference)
                            <option value="{{ $position_reference->id }}">
                                {{ $position_reference->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
        </div>
        <div class="bg-white rounded-md shadow-md">
            <div class="px-6 pt-4 text-2xl font-semibold text-blue">Latest Modules</div>
            <div class="grid grid-cols-3 gap-6 p-6">
                @foreach ($training_and_refreshers as $a => $training_and_refresher)
                    <div class="px-6 py-4 overflow-hidden border border-gray-400 rounded shadow-lg relative">
                        <div class="cursor-pointer"
                            wire:click="action({'id': {{ $training_and_refresher->id }}}, 'play')">
                            <div class="video-container">
                                <video controls class="w-full rounded-md shadow-lg" style="height: 320px;"
                                    wire:click="">
                                    <source
                                        src="{{ Storage::disk('hrim_gcs')->url($training_and_refresher->path . $training_and_refresher->name) }}"
                                        type="video/mp4">
                                </video>

                                <button class="play-button">
                                    <svg class="text-white h-14 w-12 ml-2" xmlns="http://www.w3.org/2000/svg"
                                        height="1em" viewBox="0 0 384 512">
                                        <path fill="currentColor"
                                            d="M73 39c-14.8-9.1-33.4-9.4-48.5-.9S0 62.6 0 80V432c0 17.4 9.4 33.4 24.5 41.9s33.7 8.1 48.5-.9L361 297c14.3-8.7 23-24.2 23-41s-8.7-32.2-23-41L73 39z" />
                                    </svg>
                                </button>
                            </div>
                            <div class="py-4">
                                <div class="mb-2 font-bold text-md">
                                    {{ $training_and_refresher->trainingRef->title }}
                                </div>
                                <p class="text-sm text-justify text-gray-700">
                                    {{ $training_and_refresher->trainingRef->description }}
                                </p>
                                <div class="py-2 flex gap-2">
                                    <p class="text-xs text-gray-400">
                                        Tag To:
                                    </p>
                                    <span class="text-gray-700 text-xs">
                                        @if ($training_and_refresher->trainingRef->is_tag_all_position == 1)
                                            All Positions
                                        @else
                                            <div class="grid grid-cols-1">
                                                @foreach ($get_tagged_position as $i => $position)
                                                    @if ($training_and_refresher->trainingRef->video_ref == $position->video_ref)
                                                        <p>{{ $position->taggedPosition->display }}
                                                        </p>
                                                    @endif
                                                @endforeach
                                            </div>
                                        @endif
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="icons-container absolute bottom-0 right-0 p-4 flex gap-3 mt-2">
                            @can('hrim_training_and_refresher_modules_view')
                                <span>
                                    <svg wire:click="action({'id': {{ $training_and_refresher->id }}}, 'view')"
                                        data-toggle="tooltip"
                                        class="w-4 h-4 cursor-pointer text-gray-400 hover:text-[#003399] "
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 512">
                                        <path fill="currentColor"
                                            d="M64 360C94.93 360 120 385.1 120 416C120 446.9 94.93 472 64 472C33.07 472 8 446.9 8 416C8 385.1 33.07 360 64 360zM64 200C94.93 200 120 225.1 120 256C120 286.9 94.93 312 64 312C33.07 312 8 286.9 8 256C8 225.1 33.07 200 64 200zM64 152C33.07 152 8 126.9 8 96C8 65.07 33.07 40 64 40C94.93 40 120 65.07 120 96C120 126.9 94.93 152 64 152z">
                                        </path>
                                    </svg>
                                </span>
                            @endcan
                            @can('hrim_training_and_refresher_modules_edit')
                                <span>
                                    <svg wire:click="action({'id': {{ $training_and_refresher->id }}}, 'edit')"
                                        data-toggle="tooltip"
                                        class="w-4 h-4 cursor-pointer text-gray-400 hover:text-[#003399] "
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M490.3 40.4C512.2 62.27 512.2 97.73 490.3 119.6L460.3 149.7L362.3 51.72L392.4 21.66C414.3-.2135 449.7-.2135 471.6 21.66L490.3 40.4zM172.4 241.7L339.7 74.34L437.7 172.3L270.3 339.6C264.2 345.8 256.7 350.4 248.4 353.2L159.6 382.8C150.1 385.6 141.5 383.4 135 376.1C128.6 370.5 126.4 361 129.2 352.4L158.8 263.6C161.6 255.3 166.2 247.8 172.4 241.7V241.7zM192 63.1C209.7 63.1 224 78.33 224 95.1C224 113.7 209.7 127.1 192 127.1H96C78.33 127.1 64 142.3 64 159.1V416C64 433.7 78.33 448 96 448H352C369.7 448 384 433.7 384 416V319.1C384 302.3 398.3 287.1 416 287.1C433.7 287.1 448 302.3 448 319.1V416C448 469 405 512 352 512H96C42.98 512 0 469 0 416V159.1C0 106.1 42.98 63.1 96 63.1H192z" />
                                    </svg>
                                </span>
                            @endcan
                            @can('hrim_training_and_refresher_modules_delete')
                                <span>
                                    <svg wire:click="action({'id': {{ $training_and_refresher->id }}}, 'delete')"
                                        data-toggle="tooltip"
                                        class="w-4 h-4 text-gray-400 cursor-pointer hover:text-red-500"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                        <path fill="currentColor"
                                            d="M160 400C160 408.8 152.8 416 144 416C135.2 416 128 408.8 128 400V192C128 183.2 135.2 176 144 176C152.8 176 160 183.2 160 192V400zM240 400C240 408.8 232.8 416 224 416C215.2 416 208 408.8 208 400V192C208 183.2 215.2 176 224 176C232.8 176 240 183.2 240 192V400zM320 400C320 408.8 312.8 416 304 416C295.2 416 288 408.8 288 400V192C288 183.2 295.2 176 304 176C312.8 176 320 183.2 320 192V400zM317.5 24.94L354.2 80H424C437.3 80 448 90.75 448 104C448 117.3 437.3 128 424 128H416V432C416 476.2 380.2 512 336 512H112C67.82 512 32 476.2 32 432V128H24C10.75 128 0 117.3 0 104C0 90.75 10.75 80 24 80H93.82L130.5 24.94C140.9 9.357 158.4 0 177.1 0H270.9C289.6 0 307.1 9.358 317.5 24.94H317.5zM151.5 80H296.5L277.5 51.56C276 49.34 273.5 48 270.9 48H177.1C174.5 48 171.1 49.34 170.5 51.56L151.5 80zM80 432C80 449.7 94.33 464 112 464H336C353.7 464 368 449.7 368 432V128H80V432z" />
                                    </svg>
                                </span>
                            @endcan
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </x-slot>
</x-form>

@push('styles')
    <style>
        .video-container {
            position: relative;
            /* display: inline-block; */
            pointer-events: none;
            /* Disable pointer events on the whole container */
        }

        .video-player {
            display: block;
            width: 100%;
            border-radius: 0.25rem;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }

        .play-button {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            padding: .5rem;
            background-color: #003399;
            color: white;
            border: none;
            border-radius: 100%;
            cursor: pointer;
            z-index: 1;
        }

        .play-button:hover::before {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.1);
            /* Grayish background with opacity */
            z-index: -1;
            /* Place the background behind the button */
        }

        /* Enable pointer events on the play button */
        .play-button:hover {
            pointer-events: auto;
        }
    </style>
@endpush
