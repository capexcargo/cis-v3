<div>
    <div class="mb-3 -mt-2 text-2xl font-semibold">{{ $title }}</div>
    <div class="mb-3">{{ $description }}</div>
    <div class="flex justify-start gap-2">
        <p class="mt-1 text-xs text-gray-500">Tag to : </p>
        <p class="text-md">
            <span class="text-gray-700 text-xs w-5/6">
                @if ($is_all_tagged == 1)
                    All Positions
                @else
                    <div class="grid grid-cols-1">
                        @foreach ($get_tagged_position as $i => $position)
                            @if ($video_ref == $position->video_ref)
                                <p class="text-sm">{{ $position->taggedPosition->display }}
                                </p>
                            @endif
                        @endforeach
                    </div>
                @endif
            </span>
        </p>
    </div>
    <div class="flex justify-start gap-2">
        <p class="mt-1 text-xs text-gray-500">File Name : </p>
        <p class="font-semibold text-blue text-md">{{ $attachments[0]['original_filename'] }}</p>
    </div>
</div>
