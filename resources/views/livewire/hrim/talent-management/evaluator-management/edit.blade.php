<div>
    <x-loading></x-loading>
    <form autocomplete="off">
        <div class="mt-5 space-y-3">
            <div class="grid grid-cols-1 gap-3 mt-3 text-xs">
                <div>
                    <table class="-mt-3 table-auto">
                        <tbody class="font-light">
                            <tr>
                                <td><span class="mr-4 font-semibold text-[#003399]">Employee Name:</span></td>
                                <td class="mr-2"><span
                                        class="ml-4 font-semibold text-gray-500">{{ $employee_name }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="mr-4 font-semibold text-[#003399]">Division:</span></td>
                                <td class="mr-2"><span
                                        class="ml-4 font-semibold text-gray-500">{{ $division }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="mr-4 font-semibold text-[#003399]">Branch:</span></td>
                                <td class="mr-2"><span
                                        class="ml-4 font-semibold text-gray-500">{{ $branch }}</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div wire:init="firstEvaluatorReference">
                    <label for="first_evaluator" class="block font-medium text-[#003399] mb-1">1st
                        Evaluator <span class="text-red">*</span>
                    </label>
                    <select name="first_evaluator" wire:model='first_evaluator'
                        class="text-sm text-gray-600 block w-full mt-1 border border-gray-500 p-[5px] rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50">
                        <option value="">Select</option>
                        @foreach ($first_evaluator_reference as $first_evaluator_ref)
                            @if ($first_evaluator_ref->division_id == $division_id)
                                <option value="{{ $first_evaluator_ref->id }}">
                                    {{ $first_evaluator_ref->name }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                    <x-input-error for="first_evaluator" />
                </div>
                <div wire:init="secondEvaluatorReference">
                    <label for="second_evaluator" class="block font-medium text-[#003399] mb-1">2nd
                        Evaluator <span class="text-red">*</span>
                    </label>
                    <select name="second_evaluator" wire:model='second_evaluator'
                        class="text-sm text-gray-600 block w-full mt-1 border border-gray-500 p-[5px] rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50">
                        <option value="">Select</option>
                        @foreach ($second_evaluator_reference as $second_evaluator_ref)
                            @if ($second_evaluator_ref->division_id == $division_id)
                                <option value="{{ $second_evaluator_ref->id }}">
                                    {{ $second_evaluator_ref->name }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                    <x-input-error for="second_evaluator" />
                </div>
                <div wire:init="thirdEvaluatorReference">
                    <label for="third_evaluator" class="block font-medium text-[#003399] mb-1">3rd
                        Evaluator</label>
                    <select name="third_evaluator" wire:model='third_evaluator'
                        class="text-sm text-gray-600 block w-full mt-1 border border-gray-500 p-[5px] rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50">
                        <option value="">Select</option>
                        @foreach ($third_evaluator_reference as $third_evaluator_ref)
                            @if ($third_evaluator_ref->division_id == $division_id)
                                <option value="{{ $third_evaluator_ref->id }}">
                                    {{ $third_evaluator_ref->name }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="flex justify-end mt-8 space-x-3 text-xs">
            <button type="button" wire:click="$emit('close_modal', 'edit')"
                class="px-12 py-2 font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                Cancel
            </button>
            <button type="button" wire:click="submit" class="px-12 py-2 flex-none bg-[#003399] text-white rounded-lg">
                Save
            </button>
        </div>
    </form>
</div>
