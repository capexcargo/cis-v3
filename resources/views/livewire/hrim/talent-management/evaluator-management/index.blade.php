<x-form x-data="{
    search_form: false,
    edit_evaluator_modal: '{{ $edit_evaluator_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_evaluator_management_edit')
            @if ($evaluator_id && $edit_evaluator_modal)
                <x-modal id="edit_evaluator_modal" size="w-10/12 xs:w-10/12 sm:w-10/12 md:w-1/4">
                    <x-slot name="title">Edit Evaluator</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.talent-management.evaluator-management.edit', ['id' => $evaluator_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>
    <x-slot name="header_title">Performance Management</x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-6 gap-4">
            <button wire:click="redirectTo({}, 'redirectToKraMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">KRA<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToKpiMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">KPI<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToKpiTagging')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">KPI<br>TAGGING</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToEvaluatorMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border-2 border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">EVALUATOR<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToCoreValueMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">CORE VALUE<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToLeadershipCompetenciesMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">LEADERSHIP<br>COMPETENCIES<br>MANAGEMENT</span>
            </button>
            @can('hrim_percentage_management_view')
                <button wire:click="redirectTo({}, 'redirectToAdminpercentage')"
                    class="flex flex-col items-center justify-center p-4 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                    <span class="font-medium text-left text-md">PERCENTAGE<br>MANAGEMENT</span>
                </button>
            @endcan
        </div>
        <div class="flex gap-4 text-sm md:w-3/4" wire:init="">
            <div class="w-40">
                <div>
                    <x-transparent.input type="text" label="Employee Name" name="employee_name"
                        wire:model.debounce.500ms="employee_name" />
                </div>
            </div>
            <div class="w-40">
                <div wire:init="divisionReference">
                    <x-transparent.select value="{{ $division }}" label="Division" name="division"
                        wire:model="division">
                        <option value=""></option>
                        @foreach ($division_reference as $division_ref)
                            <option class="text-sm" value="{{ $division_ref->id }}">
                                {{ $division_ref->name }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div wire:init="branchReference">
                    <x-transparent.select value="{{ $branch }}" label="Branch" name="branch" wire:model="branch">
                        <option value=""></option>
                        @foreach ($branch_reference as $branch_ref)
                            <option class="text-sm" value="{{ $branch_ref->id }}">
                                {{ $branch_ref->display }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>
        </div>
        <div class="bg-white rounded-lg shadow-md">
            <div class="my-2 overflow-auto rounded-lg">
                <div class="inline-block min-w-full align-middle">
                    <table class="w-full divide-y-2">
                        <thead>
                            <tr class="text-xs font-normal text-left text-gray-500 bg-white border-0 cursor-pointer">
                                <th class="px-2 py-1">No.</th>
                                <th class="px-4 py-1">Employee Name</th>
                                <th class="px-2 py-1">Division</th>
                                <th class="px-2 py-1">Branch</th>
                                <th class="px-2 py-1 text-center border-x-4">Evaluator<br>
                                    <div class="grid grid-cols-3 gap-5 mt-2">
                                        <span class="text-gray-400">1st</span>
                                        <span class="text-gray-400">2nd</span>
                                        <span class="text-gray-400">3rd</span>
                                    </div>
                                </th>
                                <th class="px-2 py-1 text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody class="text-sm bg-gray-100 border-0 divide-y-4 divide-white">
                            @foreach ($employees as $i => $employee)
                                @if ($employee->division_id == Auth::user()->division_id || Auth::user()->level_id == 5)
                                    <tr>
                                        <td class="px-2 py-2 text-sm">
                                            <p class="flex-initial ">{{ $i + 1 }}.
                                        </td>
                                        <td class="px-2 py-1 text-sm">
                                            <div class="flex space-x-2 w-50">
                                                <div class="relative hidden w-10 h-10 rounded-full md:block">
                                                    <img class="flex-initial object-cover w-full h-full rounded-full"
                                                        src="https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixlib=rb-1.2.1&amp;q=80&amp;fm=jpg&amp;crop=entropy&amp;cs=tinysrgb&amp;w=200&amp;fit=max&amp;ixid=eyJhcHBfaWQiOjE3Nzg0fQ"
                                                        alt="" loading="lazy">
                                                </div>
                                                <div class="flex items-center text-sm">
                                                    <div>
                                                        <p class="flex-initial font-semibold">
                                                            {{ $employee->user->name }}
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="px-2 py-2 text-sm">
                                            <p class="flex-initial w-28">
                                                {{ $employee->division->name ?? '' }}
                                            </p>
                                        </td>
                                        <td class="px-2 py-2 text-sm">
                                            <p class="flex-initial w-28">
                                                {{-- @foreach ($branches as $branch)
                                                    {{ $employee->userDetails->branch_id == $branch->id ? $branch->display : '' }}
                                                @endforeach --}}
                                                {{ $employee->branch->display ?? '' }}
                                            </p>
                                        </td>
                                        <td class="px-2 py-2 text-sm text-center border-2 border-b-0 w-72">
                                            <div class="grid flex-shrink-0 grid-cols-3 gap-5">
                                                <span class="w-20">
                                                    @foreach ($evaluators as $evaluator)
                                                        {{ $evaluator->user_id == $employee->id ? $evaluator->firstEvaluator->name ?? '' : '' }}
                                                    @endforeach
                                                </span>
                                                <span class="w-20">
                                                    @foreach ($evaluators as $evaluator)
                                                        {{ $evaluator->user_id == $employee->id ? $evaluator->secondEvaluator->name ?? '' : '' }}
                                                    @endforeach
                                                </span>
                                                <span class="w-20">
                                                    @foreach ($evaluators as $evaluator)
                                                        {{ $evaluator->user_id == $employee->id ? $evaluator->thirdEvaluator->name ?? '' : '' }}
                                                    @endforeach
                                                </span>
                                            </div>
                                        </td>
                                        <td class="w-20 px-2 py-2 text-sm">
                                            @can('hrim_evaluator_management_edit')
                                                <div class="flex ml-2">

                                                    <svg wire:click="action({'id': {{ $employee->id }}}, 'edit_evaluator')"
                                                        data-toggle="tooltip"
                                                        class="w-5 h-5 text-blue-800 cursor-pointer hover:text-blue-900"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                        </path>
                                                    </svg>
                                                </div>
                                            @endcan
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    <div class="px-1 pt-2 pb-2">
                        {{ $employees->links() }}
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
