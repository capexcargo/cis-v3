<x-form x-data="{
    {{-- search_form: false,
    edit_performance_modal: '{{ $edit_performance_modal }}', --}}
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    {{-- <x-slot name="modals">
        @can('hrim_performance_edit')
            @if ($performance_id && $edit_performance_modal)
                <x-modal id="edit_performance_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Edit Performance Status</x-slot>
                    <x-slot name="body">
                        @livewire('hrim.talent-management.performance.edit', ['id' => $performance_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot> --}}
    <x-slot name="header_title">Performance Management</x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-6 gap-4">
            <button wire:click="redirectTo({}, 'redirectToKraMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">KRA<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToKpiMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">KPI<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToKpiTagging')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">KPI<br>TAGGING</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToEvaluatorMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">EVALUATOR<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToCoreValueMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">CORE VALUE<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToLeadershipCompetenciesMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">LEADERSHIP<br>COMPETENCIES<br>MANAGEMENT</span>
            </button>
            @can('hrim_percentage_management_view')
                <button wire:click="redirectTo({}, 'redirectToAdminpercentage')"
                    class="flex flex-col items-center justify-center p-4 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                    <span class="font-medium text-left text-md">PERCENTAGE<br>MANAGEMENT</span>
                </button>
            @endcan
        </div>

        <div class="bg-white rounded-lg shadow-md">
            <x-table.table>
            </x-table.table>
        </div>
    </x-slot>
</x-form>
