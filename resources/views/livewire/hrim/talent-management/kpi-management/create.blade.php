<div>
    <x-loading></x-loading>
    <form autocomplete="off">
        <div class="mt-5 space-y-3">
            <div class="grid grid-cols-1 gap-3 mt-3">
                <div>
                    <x-label for="kpi_name" value="KPI Name" :required="true" />
                    <x-input type="text" name="" wire:model.defer='kpi_name'></x-input>
                    <x-input-error for="kpi_name" />
                </div>
            </div>
            <div class="grid grid-cols-2 gap-4 mt-3">
                <div wire:init="">
                    <x-label for="quarter" value="Quarter" :required="true" />
                    <x-select name="quarter" wire:model='quarter'>
                        <option value="">Select</option>
                        <option value="1">1st Quarter</option>
                        <option value="2">2nd Quarter</option>
                        <option value="3">3rd Quarter</option>
                        <option value="4">4th Quarter</option>
                    </x-select>
                    <x-input-error for="quarter" />
                </div>
                <div wire:init="loadYear">
                    <x-label for="year" value="Year" :required="true" />
                    <x-input type="text" name="year" wire:model='year' disabled>
                    </x-input>
                    <x-input-error for="year" />
                </div>
            </div>
        </div>
        <div class="grid grid-cols-1 gap-3 mt-3">
            <div>
                <x-label for="target" value="Target"/>
                <x-input type="text" name="target" wire:model.defer='target'></x-input>
                <x-input-error for="target" />
            </div>
        </div>
        <div class="grid grid-cols-1 gap-3 mt-3">
            <div>
                <x-label for="points" value="Points" :required="true" />
                <x-input type="number" name="points" wire:model.defer='points'></x-input>
                <x-input-error for="points" />
            </div>
        </div>
        <div class="grid grid-cols-1 gap-3 mt-3">
            <div>
                <x-label for="remarks" value="Remarks"/>
                <x-textarea type="text" name="remarks" wire:model.defer='remarks'></x-textarea>
                <x-input-error for="remarks" />
            </div>
        </div>
        <div class="flex justify-end mt-8 space-x-3">
            <button type="button" wire:click="$emit('close_modal', 'create')"
                class="px-12 py-2 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                Cancel
            </button>
            <button type="button" wire:click="submit"
                class="px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                Submit
            </button>
        </div>
    </form>
</div>
