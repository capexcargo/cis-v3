<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div class="grid gap-4 grid-cols">
                    <div wire:init="loadJobLevels">
                        <x-label value="Job Level" :required="true" />
                        <x-select name="job_level" wire:model.defer='job_level' disabled>
                            <option value="">Select</option>
                            @foreach ($job_level_references as $job_level)
                                <option value="{{ $job_level->id }}">
                                    {{ $job_level->display }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="job_level" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols">
                    <div>
                        <x-label for="goal_percentage" value="Goal Percentage" :required="true" />
                        <x-input type="number" name="goal_percentage" wire:model.defer='goal_percentage'></x-input>
                        <x-input-error for="goal_percentage" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols">
                    <div>
                        <x-label for="core_value_percentage" value="Core Value Percentage" :required="true" />
                        <x-input type="number" name="core_value_percentage" wire:model.defer='core_value_percentage'></x-input>
                        <x-input-error for="core_value_percentage" />
                    </div>
                </div>
                <div class="grid gap-4 grid-cols">
                    <div>
                        <x-label for="lead_com_percentage" value="Lead Comp Percentage" :required="true" />
                        <x-input type="number" name="lead_com_percentage" wire:model.defer='lead_com_percentage'></x-input>
                        <x-input-error for="lead_com_percentage" />
                    </div>
                </div>
                <div class="flex justify-end mt-6 space-x-3 text-xs">
                    <button type="button" wire:click="$emit('close_modal', 'edit')"
                        class="px-12 py-2 font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <x-button type="submit" title="Save" class="bg-blue text-xs text-white hover:bg-[#002161]" />
                </div>
            </div>
        </div>
    </form>
</div>
