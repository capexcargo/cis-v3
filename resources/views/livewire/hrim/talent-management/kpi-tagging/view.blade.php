<div class="-mt-2">
    <h3 class="text-lg font-semibold">{{ $position }}</h3>
</div>
<div class="bg-white rounded-lg shadow-md">
    <div class="my-2 overflow-auto rounded-lg">
        <div class="inline-block min-w-full align-middle">
            <table class="w-full divide-y-2">
                <thead>
                    <tr class="text-sm font-normal text-left text-gray-500 bg-white border-0 cursor-pointer">
                        <th class="w-1/5 px-2 py-1">KRA</th>
                        <th class="px-4 py-1">KPI</th>
                        <th class="px-2 py-1">Target</th>
                        <th class="px-2 py-1">Points</th>
                        <th class="px-2 py-1">Remarks</th>
                    </tr>
                </thead>
                <tbody class="text-sm bg-gray-100 divide-white">
                    @foreach ($kpi_taggings as $i => $kpi_tagged)
                        <tr>
                            <td class="w-64 px-2 py-2">
                                {{ $kpi_taggings[$i]['kra'] }}
                            </td>
                            <td class="w-1/4 px-4 py-1">
                                {{ $kpi_taggings[$i]['kpi'] }}
                            </td>
                            <td class="px-2 py-2">
                                <p class="flex-initial w-28">
                                    {{ $kpi_taggings[$i]['target'] }}
                                </p>
                            </td>
                            <td class="px-2 py-2">
                                <p class="flex-initial text-center w-28">
                                    {{ $kpi_taggings[$i]['points'] }}%
                                </p>
                            </td>
                            <td class="px-2 py-2">
                                <p class="flex-initial text-center w-28">
                                    {{ $kpi_taggings[$i]['remarks'] }}
                                </p>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="mt-4">
    <h3 class="text-lg font-semibold">Job Description</h3>
    <p class="text-sm font-normal ml-6 mt-1">{{ $job_description }}</p>
</div>
