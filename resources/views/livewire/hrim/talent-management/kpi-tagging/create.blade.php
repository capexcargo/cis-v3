<div wire:init="load">
    <x-loading></x-loading>
    <form autocomplete="off">
        <div class="mt-5 space-y-3">
            <div class="grid grid-cols-3 gap-3 mt-4">
                <div>
                    <x-label for="position" value="Position" :required="true" />
                    <x-select name="position" wire:model='position'>
                        <option value="">Select</option>
                        @foreach ($position_reference as $position_ref)
                            <option value="{{ $position_ref->id }}">
                                {{ $position_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="position" />
                </div>
            </div>
            <div class="grid grid-cols-3 gap-3 mt-3">
                <div class="grid grid-cols-2 gap-4">
                    <div>
                        <x-label for="quarter" value="Quarter" :required="true" />
                        <x-select name="quarter" wire:model='quarter'>
                            <option value="">Select</option>
                            <option value="1">1st Quarter</option>
                            <option value="2">2nd Quarter</option>
                            <option value="3">3rd Quarter</option>
                            <option value="4">4th Quarter</option>
                        </x-select>
                        <x-input-error for="quarter" />
                    </div>
                    <div>
                        <x-label for="year" value="Year" :required="true" />
                        <x-input type="text" name="year" wire:model='year' disabled>
                        </x-input>
                        <x-input-error for="year" />
                    </div>
                </div>
            </div>

            <label class="block font-medium text-sm text-[#003399] t-8">Search</label>
            <div class="grid grid-cols-2 gap-3">
                <div class="grid grid-cols-3 gap-4">
                    <div wire:init="">
                        <x-transparent.select value="{{ '$quarter' }}" label="KPI Quarter" name=""
                            wire:model="">
                            <option value=""></option>
                            <option value="1">1st Quarter</option>
                            <option value="2">2nd Quarter</option>
                            <option value="3">3rd Quarter</option>
                            <option value="4">4th Quarter</option>
                        </x-transparent.select>
                    </div>
                    <div wire:init="">
                        <x-transparent.input value="" label="KPI Year" name="" wire:model="">
                        </x-transparent.input>
                    </div>
                    {{-- <div>
                        <button type="button" wire:click=""
                            class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                            Search
                        </button>
                    </div> --}}
                </div>
            </div>
        </div>

        <div class="bg-white rounded-lg shadow-md">
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="Select" />
                    <x-table.th name="No." />
                    <x-table.th name="KPI Name" />
                    <x-table.th name="Tag KRA" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($kpi_reference as $i => $kpi_ref)
                        <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]" x-data="{ enable{{ $i }}: false }">
                            <td class="w-12 p-3 whitespace-nowrap">
                                <input type="checkbox" class="form-checkbox" x-model="enable{{ $i }}"
                                    id="enable{{ $i }}" name="enable{{ $i }}"
                                    value="{{ $kpi_ref->id }}" wire:model="kpis">
                            </td>
                            <td class="w-12 p-3 whitespace-nowrap">{{ $i + 1 }}
                            </td>
                            <td class="p-3 whitespace-nowrap w-80">
                                <span class="md:w-80">
                                    {{ $kpi_ref->name }}
                                </span>
                            </td>
                            <td class="w-20 p-3 whitespace-nowrap">
                                <div class="md:w-80">
                                    <select name="kras.{{ $i }}" wire:model='kras.{{ $i }}'
                                        x-bind:disabled="!enable{{ $i }}"
                                        class="text-gray-600 block w-full mt-1 border border-gray-500 p-[5px] rounded-md shadow-sm 
                                            focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50">
                                        <option value="">Select
                                        </option>
                                        @foreach ($kra_reference as $kra_ref)
                                            <option value="{{ $kra_ref->id }}">
                                                {{ $kra_ref->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </x-slot>
            </x-table.table>
        </div>

        <div class="flex justify-end mt-8 space-x-3">
            <button type="button" wire:click="$emit('close_modal', 'create')"
                class="px-12 py-2 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                Cancel
            </button>
            <button type="button" wire:click="submit"
                class="px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                Save
            </button>
        </div>
    </form>
</div>
