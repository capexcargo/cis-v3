<x-form x-data="{
    search_form: false,
    create_kpi_tagging_modal: '{{ $create_kpi_tagging_modal }}',
    view_kpi_tagging_modal: '{{ $view_kpi_tagging_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('hrim_kpi_tagging_view')
            <x-modal id="create_kpi_tagging_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/4">
                <x-slot name="title">Tag KPI</x-slot>
                <x-slot name="body">
                    @livewire('hrim.talent-management.kpi-tagging.create')
                </x-slot>
            </x-modal>
        @endcan
        @if ($kpi_tagging_id && $view_kpi_tagging_modal)
            <x-modal id="view_kpi_tagging_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/5">
                <x-slot name="body">
                    @livewire('hrim.talent-management.kpi-tagging.view', ['id' => $kpi_tagging_id, 'quarter' => $kpi_quarter, 'year' => $kpi_year])
                </x-slot>
            </x-modal>
        @endif
    </x-slot>
    <x-slot name="header_title">Performance Management</x-slot>
    <x-slot name="header_button">
        {{-- @can('hrim_kpi_tagging_add') --}}
        <button wire:click="action({}, 'create_kpi_tagging')"
            class="p-2 px-10 mr-3 text-sm text-white rounded-md bg-blue">
            <div class="flex items-start justify-between">
                <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far" data-icon="print-alt"
                    role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path fill="currentColor"
                        d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                </svg>
                Tag KPI
            </div>
        </button>
        {{-- @endcan --}}
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-6 gap-4">
            <button wire:click="redirectTo({}, 'redirectToKraMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">KRA<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToKpiMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">KPI<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToKpiTagging')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border-2 border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">KPI<br>TAGGING</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToEvaluatorMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">EVALUATOR<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToCoreValueMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">CORE VALUE<br>MANAGEMENT</span>
            </button>
            <button wire:click="redirectTo({}, 'redirectToLeadershipCompetenciesMgmt')"
                class="flex flex-col items-center justify-center p-1 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                <span class="font-medium text-left text-md">LEADERSHIP<br>COMPETENCIES<br>MANAGEMENT</span>
            </button>
            @can('hrim_percentage_management_view')
                <button wire:click="redirectTo({}, 'redirectToAdminpercentage')"
                    class="flex flex-col items-center justify-center p-4 text-gray-700 bg-white border border-solid rounded-md w-50 border-blue hover:bg-blue-100">
                    <span class="font-medium text-left text-md">PERCENTAGE<br>MANAGEMENT</span>
                </button>
            @endcan
        </div>
        <div class="flex gap-4 text-sm md:w-3/4">
            <div class="w-40">
                <div>
                    <x-transparent.input type="text" label="Position" name="position"
                        wire:model.debounce.500ms="position" />
                </div>
            </div>
            <div class="w-40">
                <div wire:init="">
                    <x-transparent.select value="{{ $quarter }}" label="Quarter" name="quarter"
                        wire:model="quarter">
                        <option value=""></option>
                        <option value="1">1st Quarter</option>
                        <option value="2">2nd Quarter</option>
                        <option value="3">3rd Quarter</option>
                        <option value="4">4th Quarter</option>
                    </x-transparent.select>
                </div>
            </div>
            <div class="w-40">
                <div>
                    <x-transparent.select value="{{ $year }}" label="Year" name="year" wire:model="year">
                        <option value=""></option>
                        @for ($i = 2011; $i <= 2022; $i++)
                            <option value="{{ $i }}">
                                {{ $i }}
                            </option>
                        @endfor
                    </x-transparent.select>
                </div>
            </div>
        </div>
        <div class="bg-white rounded-lg shadow-md">
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="No." />
                    <x-table.th name="Position" />
                    <x-table.th name="Quarter" />
                    <x-table.th name="Year" />
                    <x-table.th name="Action" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($kpi_tags as $i => $kpi_tag)
                        @if ($kpi_tag)
                            <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                <td class="p-3 whitespace-nowrap">{{ $no++ }}
                                </td>
                                <td class="p-3 whitespace-nowrap">{{ $kpi_tag->position->display }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $kpi_tag->quarter == 1
                                        ? '1st Quarter'
                                        : ($kpi_tag->quarter == 2
                                            ? '2nd Quarter'
                                            : ($kpi_tag->quarter == 3
                                                ? '3rd Quarter'
                                                : ($kpi_tag->quarter == 4
                                                    ? '4th Quarter'
                                                    : ''))) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">{{ $kpi_tag->year }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex gap-3">
                                        <svg wire:click="action({'id': {{ $kpi_tag->position_id }}, 'quarter': {{ $kpi_tag->quarter }}, 'year': {{ $kpi_tag->year }}}, 'view_kpi_tagging')"
                                            class="w-4 h-4 cursor-pointer text-blue hover:text-blue-700"
                                            data-prefix="far" data-icon="approve" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </x-slot>
            </x-table.table>
        </div>
    </x-slot>
</x-form>
