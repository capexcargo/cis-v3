<div>
    <x-loading></x-loading>
    <form autocomplete="off">
        <div class="space-y-3 mt-5">
            <div class="grid grid-cols-1 gap-3 mt-3">
                <div>
                    <x-label for="leadership_competencies" value="Leadership Competency" :required="true" />
                    <x-textarea type="text" name="leadership_competencies" wire:model.defer='leadership_competencies'></x-textarea>
                    <x-input-error for="leadership_competencies" />
                </div>
            </div>
        </div>
        <div class="flex justify-end space-x-3 mt-8">
            <button type="button" wire:click="$emit('close_modal', 'edit')"
                class="px-12 py-2 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                Cancel
            </button>
            <button type="button" wire:click="submit"
                class="px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                Save
            </button>
        </div>
    </form>
</div>
