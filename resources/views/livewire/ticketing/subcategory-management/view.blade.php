<h2 class="mb-3 text-xl text-blue">
    <table class="table-auto">
        <tbody class="text-lg font-light text-gray-900">
            <tr>
                <td><span class="mr-6 font-normal text-gray-500">Category : </span></td>
                <td class="mr-2"><span class="mr-2 font-semibold">{{ $category }}</span></td>
            </tr>
            <tr>
                <td><span class="mr-6 font-normal text-gray-500">Subcategory : </span></td>
                <td class="mr-2"><span class="mr-2 font-semibold">{{ $subcategory }}</span></td>
            </tr>
            <tr>
                <td><span class="mr-6 font-normal text-gray-500">SLA : </span></td>
                <td class="mr-2"><span class="mr-2 font-semibold">{{ $sla }}</span></td>
            </tr>
            <tr>
                <td><span class="mr-6 font-normal text-gray-500">Severity : </span></td>
                <td class="mr-2"><span class="mr-2 font-semibold">{{ $severity }}</span></td>
            </tr>
            <tr>
                <td><span class="mr-6 font-normal text-gray-500">Division : </span></td>
                <td class="mr-2"><span class="mr-2 font-semibold">{{ $division }}</span></td>
            </tr>
            <tr>
                <td><span class="mr-6 font-normal text-gray-500">Task Holders : </span></td>
                <td class="mr-2">
                    <span class="mr-2 font-semibold">
                        @foreach ($taskholders as $i => $taskholder)
                            {{ $taskholder['task_holder'] }}<br>
                        @endforeach
                    </span>
                </td>
            </tr>
        </tbody>
    </table>
</h2>
