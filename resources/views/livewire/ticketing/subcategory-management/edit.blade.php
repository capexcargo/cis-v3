<div>
    <x-loading></x-loading>
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-4">
            <div class="grid grid-cols-6 gap-4">
                <div wire:init="categoryReferences">
                    <x-label for="category" value="Category" :required="true" />
                    <x-select type="text" name="category" wire:model.defer='category'>
                        <option value=""></option>
                        @foreach ($category_references as $category_ref)
                            <option value="{{ $category_ref->id }}">
                                {{ $category_ref->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="category" />
                </div>
                <div>
                    <x-label for="subcategory" value="Subcategory Name" :required="true" />
                    <x-input type="text" name="subcategory" wire:model.defer='subcategory'></x-input>
                    <x-input-error for="subcategory" />
                </div>
                <div>
                    <x-label for="sla" value="SLA" :required="true" />
                    <x-input type="number" name="sla" wire:model.defer='sla'></x-input>
                    <x-input-error for="sla" />
                </div>
                <div>
                    <x-label for="severity" value="Severity" :required="true" />
                    <x-select type="text" name="severity" wire:model.defer='severity'>
                        <option value=""></option>
                        <option value="1">Low</option>
                        <option value="2">Medium</option>
                        <option value="3">High</option>
                    </x-select>
                    <x-input-error for="severity" />
                </div>
                <div wire:init="divisionReferences">
                    <x-label for="division" value="Division" :required="true" />
                    <x-select type="text" name="division" wire:model='division'>
                        <option value=""></option>
                        @foreach ($division_references as $division_ref)
                            <option value="{{ $division_ref->id }}">
                                {{ $division_ref->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="division" />
                </div>
                <div wire:init="departmentReferences">
                    <x-label for="department" value="Department" :required="true" />
                    <x-select type="text" name="department" wire:model='department'>
                        <option value=""></option>
                        @foreach ($department_references as $department_ref)
                            <option value="{{ $department_ref->id }}">
                                {{ $department_ref->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="department" />
                </div>
            </div>
            <div class="grid grid-cols-6 gap-4">
                @forelse ($taskholders as $i => $taskholder)
                    <div class="col-span-1">
                        <div class="relative z-0" wire:init="taskHolderReferences">
                            <x-label for="task_holder" value="Task Holder" :required="true" />
                            <x-select type="text" name="taskholders.{{ $i }}.task_holder"
                                wire:model.defer='taskholders.{{ $i }}.task_holder'>
                                <option value=""></option>
                                @foreach ($task_holder_references as $task_holder_ref)
                                    <option value="{{ $task_holder_ref->id }}">
                                        {{ $task_holder_ref->user->name }}
                                    </option>
                                @endforeach
                            </x-select>
                        </div>
                        <div class="relative w-full">
                            <div class="absolute right-0 w-18">
                                @if (count($taskholders) > 1)
                                    <svg wire:click="removeTaskholders({{ $i }})"
                                        class="w-3 h-3 text-red cursor-pointer"
                                        style="margin-top: -40px; margin-right: -4px;" aria-hidden="true"
                                        focusable="false" data-prefix="fas" data-icon="times-circle" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                            d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                        </path>
                                    </svg>
                                @endif
                            </div>
                        </div>
                        <x-input-error for="taskholders.{{ $i }}.task_holder" />
                    </div>
                @empty
                    <div class="text-sm">
                        click "+" to add new task holder</div>
                @endforelse
            </div>

            <div class="flex justify-start">
                <button type="button" title="Add Task Holder" wire:click="addTaskholders"
                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                    +</button>
            </div>
            <div class="flex justify-end gap-3 pt-5 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">Cancel</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                    Save</button>
            </div>
        </div>
    </form>
</div>
