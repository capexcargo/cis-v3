<div class="grid grid-cols-1 gap-4 mt-6">
    <h4 class="text-blue-800 text-2xl font-semibold">Total Tickets</h4>
    <div class="col-span-2">
        <div class="bg-white rounded-lg shadow-md">
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="No." />
                    <x-table.th name="Reference" />
                    <x-table.th name="Subject" />
                    <x-table.th name="Message" />
                    <x-table.th name="Category" />
                    <x-table.th name="Subcategory" />
                    <x-table.th name="To Concerned Division" />
                    <x-table.th name="Date Created" />
                    <x-table.th name="Assignment Date" />
                    <x-table.th name="SLA" />
                    <x-table.th name="Target Completion" />
                    <x-table.th name="Actual Target Date" />
                    <x-table.th name="Date Closed" />
                    <x-table.th name="Assignment Ageing" />
                    <x-table.th name="Task Holder" />
                    <x-table.th name="Concerned By" />
                    <x-table.th name="Ticket Status" />
                    <x-table.th name="Remarks" />
                    <x-table.th name="Concerned User Status" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($tickets_management as $ticket_management)
                        <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                            <td class="p-3 whitespace-nowrap">
                                {{ $loop->index + 1 }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->ticket_reference_no }}

                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->subject }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->message }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->category->name }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->subcategory->name }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->division->name }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->created_at != '' ? date('M. d, Y', strtotime($ticket_management->created_at)) : '-' }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->assignment_date != '' ? date('M. d, Y', strtotime($ticket_management->assignment_date)) : '-' }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->subcategory->sla == 1 ? $ticket_management->subcategory->sla . ' Day' : $ticket_management->subcategory->sla . ' Days' }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->target_end_date != '' ? date('M. d, Y', strtotime($ticket_management->target_end_date)) : '-' }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->actual_end_date != '' ? date('M. d, Y', strtotime($ticket_management->actual_end_date)) : '-' }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->final_status_date != '' ? date('M. d, Y', strtotime($ticket_management->final_status_date)) : '-' }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                @if ($ticket_management->assignment_date == null)
                                    {{ date_diff(date_create($ticket_management->created_at), date_create(date('Y-m-d')))->format('%a Day/s') }}
                                @else
                                    {{ date_diff(date_create($ticket_management->created_at), date_create($ticket_management->assignment_date))->format('%a Day/s') }}
                                @endif
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->taskholder_name->name }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->requestedBy->name }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                @if ($ticket_management->final_status == 1 || $ticket_management->final_status == 2)
                                    <span
                                        class="text-orange bg-orange-light px-4 text-xs rounded-full p-1">Pending</span>
                                @else
                                    <span class="text-blue bg-blue-100 px-4 text-xs rounded-full p-1">Closed</span>
                                @endif
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $ticket_management->subcategory->remarks }}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                @if ($ticket_management->requester_status == 1 || $ticket_management->requester_status == 2)
                                    <span
                                        class="text-orange bg-orange-light px-4 text-xs rounded-full p-1">Pending</span>
                                @else
                                    <span class="text-blue bg-blue-100 px-4 text-xs rounded-full p-1">Closed</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </x-slot>
            </x-table.table>
            <div class="px-1 pb-2">
                {{-- {{ $tickets_management->links() }} --}}
            </div>
        </div>
    </div>
</div>
