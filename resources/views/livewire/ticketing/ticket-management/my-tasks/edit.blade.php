<div>
    <x-loading></x-loading>

    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div>
                <x-label for="actual_start_date" value="Actual Start Date" :required="true" />
                <x-input type="date" name="actual_start_date" wire:model.defer='actual_start_date'>
                </x-input>
                <x-input-error for="actual_start_date" />
            </div>
            <div>
                <x-label for="remarks" value="Remarks" :required="true" />
                <x-textarea type="date" name="remarks" wire:model.defer='remarks'>
                </x-textarea>
                <x-input-error for="remarks" />
            </div>
            <div class="flex justify-end gap-3 pt-5 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">Cancel</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                    Save</button>
            </div>
        </div>
    </form>
</div>
