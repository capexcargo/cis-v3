<div wire:init="load" x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    view_modal: '{{ $view_modal }}',
    confirmation_modal: '{{ $confirmation_modal }}'
}">
    <x-loading />
    @can('ticket_management_my_tickets_add')
        <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="title">Create New Ticket</x-slot>
            <x-slot name="body">
                @livewire('ticketing.ticket-management.my-tickets.create')
            </x-slot>
        </x-modal>
    @endcan

    @can('ticket_management_my_tasks_edit')
        @if ($ticket_id && $edit_modal)
            <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/5">
                <x-slot name="title">Edit</x-slot>
                <x-slot name="body">
                    @livewire('ticketing.ticket-management.my-tasks.edit', ['id' => $ticket_id])
                </x-slot>
            </x-modal>
        @endif
    @endcan

    @if ($ticket_id && $view_modal)
        <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/5">
            <x-slot name="title">Remarks</x-slot>
            <x-slot name="body">
                @livewire('ticketing.ticket-management.my-tasks.view', ['id' => $ticket_id])
            </x-slot>
        </x-modal>
    @endif

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                {{ $confirmation_message }}
            </h2>

            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'close')"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    No
                </button>
                <button type="button" wire:click="submit({{ $ticket_id }})"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
    <div class="px-6 py-8 mb-8">
        <div class="flex items-center justify-between">
            <div class="grid grid-cols-1 gap-4">
                <div class="col-span-2">
                    <div class="grid grid-cols-10 gap-6">
                        <div class="col-span-4">
                            <ul class="flex">
                                <li
                                    class="px-3 py-1 text-xs text-gray-500 bg-gray-100 border border-gray-600 shadow-sm rounded-l-md">
                                    <button wire:click="redirectTo({}, 'redirectToDashboard')">
                                        <span class="ml-2 mr-2 text-sm">Dashboard</span>
                                    </button>
                                </li>
                                <li
                                    class="px-3 py-1 text-xs text-gray-500 bg-gray-100 border border-gray-600 shadow-sm">
                                    <button wire:click="redirectTo({}, 'redirectToAllTickets')">
                                        <span class="ml-2 mr-2 text-sm">All Tickets</span>
                                    </button>
                                </li>
                                <li
                                    class="px-3 py-1 text-xs text-gray-500 bg-gray-100 border border-gray-600 shadow-sm ">
                                    <button wire:click="redirectTo({}, 'redirectToMyTickets')">
                                        <span class="ml-2 mr-2 text-sm">My Tickets</span>
                                    </button>
                                </li>
                                <li
                                    class="px-3 py-1 text-xs text-white border border-gray-600 shadow-sm bg-blue rounded-r-md">
                                    <button wire:click="redirectTo({}, 'redirectToMyTasks')">
                                        <span class="ml-2 mr-2 text-sm">My Tasks</span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex items-center justify-between space-x-3">
                @can('ticket_management_my_tickets_add')
                    <button wire:click="action({}, 'create')"
                        class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue whitespace-nowrap">
                        <div class="flex items-start justify-between">
                            <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                                data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 448 512">
                                <path fill="currentColor"
                                    d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                            </svg>
                            Create New Ticket
                        </div>
                    </button>
                @endcan
            </div>
        </div>

        <div class="grid grid-cols-6 gap-3 mt-8">
            <button wire:click=""
                class="flex justify-between px-4 py-6 text-gray-700 bg-white border border-blue-600 rounded-md ">
                <div class="mt-2 font-medium text-left text-md">ALL TICKETS</div>
                <div class="text-3xl font-bold">{{ $tickets_management->total() }}</div>
            </button>
            <button wire:click=""
                class="flex justify-between px-4 py-6 text-gray-700 bg-white border border-blue-600 rounded-md ">
                <div class="font-medium text-left text-md">IN PROGRESS<br>TICKETS</div>
                <div class="text-3xl font-bold">{{ count($in_progress_tickets) }}</div>
            </button>
            <button wire:click=""
                class="flex justify-between px-4 py-6 text-gray-700 bg-white border border-blue-600 rounded-md ">
                <div class="font-medium text-left text-md">OVERDUE TICKETS</div>
                <div class="text-3xl font-bold mr-1">{{ count($count_overdue) }}</div>
            </button>
        </div>

        <div class="grid grid-cols-6 mt-8 text-sm">
            <div class="w-40">
                <x-transparent.input type="name" label="Reference No." name="ticket_ref_no"
                    wire:model.debounce.500ms="ticket_ref_no" />
            </div>
            <div class="w-40">
                <x-transparent.input type="name" label="Subject" name="subject"
                    wire:model.debounce.500ms="subject" />
            </div>
            <div class="w-40">
                <x-transparent.input type="date" label="Date Created" name="date_created"
                    wire:model.debounce.500ms="date_created" />
            </div>
            <div class="w-40">
                <x-transparent.input type="date" label="Date Closed" name="date_closed"
                    wire:model.debounce.500ms="date_closed" />
            </div>
        </div>
        <div class="grid grid-cols-6 text-sm">
            <div class="w-40">
                <x-transparent.select value="{{ $category }}" label="Category" name="category"
                    wire:model="category">
                    <option value=""></option>
                    @foreach ($categories_references as $category_ref)
                        <option value="{{ $category_ref->id }}">
                            {{ $category_ref->name }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div>
            <div class="w-40">
                <x-transparent.select value="" label="Ticket Status" name="ticket_status"
                    wire:model="ticket_status">
                    <option value=""></option>
                    <option value="1">Pending</option>
                    <option value="2">In Progress</option>
                    <option value="3">Closed</option>
                </x-transparent.select>
            </div>
        </div>

        <div class="grid grid-cols-1 gap-4 mt-6">
            <div class="col-span-2">
                <div class="bg-white rounded-lg shadow-md">
                    <x-table.table>
                        <x-slot name="thead">
                            <x-table.th name="No." />
                            <x-table.th name="Reference" />
                            <x-table.th name="Subject" />
                            <x-table.th name="Message" />
                            <x-table.th name="Category" />
                            <x-table.th name="Subcategory" />
                            <x-table.th name="To Concerned Division" />
                            <x-table.th name="Date Created" />
                            <x-table.th name="Assignment Date" />
                            <x-table.th name="SLA" />
                            <th class="px-2 py-1 text-center bg-gray-200 border-x-4 text-normal md:w-1/2">
                                <div class="">Target</div>
                                <div class="flex justify-between gap-12 mt-2 whitespace-nowrap">
                                    <span class="text-gray-400 bg-gray-0">Start Date</span>
                                    <span class="text-gray-400 bg-none">End Date</span>
                                </div>
                            </th>
                            <th class="px-2 py-1 text-center bg-gray-300 border-x-4 text-normal whitespace-nowrap">
                                <div class="">Actual</div>
                                <div class="flex justify-between gap-12 mt-2">
                                    <span class="text-gray-400">Start Date</span>
                                    <span class="text-gray-400">End Date</span>
                                </div>
                            </th>
                            <x-table.th name="Ticket Ageing" />
                            <x-table.th name="Assignment Ageing" />
                            <x-table.th name="Date Closed" />
                            <x-table.th name="Ticket Status" />
                            <x-table.th name="Remarks" />
                            <x-table.th name="Concerned User Status" />
                            <x-table.th name="Action" />
                        </x-slot>
                        <x-slot name="tbody">
                            @forelse ($tickets_management as $ticket_management)
                                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                    <td class="p-3 whitespace-nowrap">
                                        {{ ($tickets_management->currentPage() - 1) * $tickets_management->links()->paginator->perPage() + $loop->iteration }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $ticket_management->ticket_reference_no }}

                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $ticket_management->subject }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $ticket_management->message }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $ticket_management->category->name }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $ticket_management->subcategory->name }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $ticket_management->division->name }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ date('M. d, Y', strtotime($ticket_management->created_at)) }}
                                    </td>
                                    <td class="p-3 text-center whitespace-nowrap">
                                        {{ $ticket_management->assignment_date != '' ? date('M. d, Y', strtotime($ticket_management->assignment_date)) : '-' }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $ticket_management->subcategory->sla == 1 ? $ticket_management->subcategory->sla . ' Day' : $ticket_management->subcategory->sla . ' Days' }}
                                    </td>
                                    <td class="px-2 text-center border-x-4 text-normal whitespace-nowrap">
                                        <div class="flex justify-between gap-12 whitespace-nowrap">
                                            <span
                                                class="">{{ $ticket_management->target_start_date != '' ? date('M. d, Y', strtotime($ticket_management->target_start_date)) : null }}</span>
                                            <span
                                                class="">{{ $ticket_management->target_end_date != '' ? date('M. d, Y', strtotime($ticket_management->target_end_date)) : null }}</span>
                                        </div>
                                    </td>
                                    <td class="px-2 text-center border-x-4 text-normal">
                                        <div class="flex justify-between gap-12 whitespace-nowrap">
                                            <span
                                                class="">{{ $ticket_management->actual_start_date != '' ? date('M. d, Y', strtotime($ticket_management->actual_start_date)) : null }}</span>
                                            <span
                                                class="">{{ $ticket_management->actual_end_date != '' ? date('M. d, Y', strtotime($ticket_management->actual_end_date)) : null }}</span>
                                        </div>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        @if ($ticket_management->actual_end_date != null)
                                            {{ date_diff(date_create($ticket_management->actual_end_date), date_create($ticket_management->target_end_date))->format('%a Day/s') }}
                                        @else
                                            {{ date_diff(date_create($ticket_management->target_end_date), date_create(date('Y-m-d')))->format('%R%a') < 0 ? '0 Day/s' : date_diff(date_create($ticket_management->target_end_date), date_create(date('Y-m-d')))->format('%a Day/s') }}
                                        @endif
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        @if ($ticket_management->assignment_date == null)
                                            {{ date_diff(date_create(date('Y-m-d', strtotime($ticket_management->created_at))), date_create(date('Y-m-d')))->format('%a Day/s') }}
                                        @else
                                            {{ date_diff(date_create(date('Y-m-d', strtotime($ticket_management->created_at))), date_create($ticket_management->assignment_date))->format('%a Day/s') }}
                                        @endif
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $ticket_management->task_holder_status_date != '' ? date('M. d, Y', strtotime($ticket_management->task_holder_status_date)) : null }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        @if (($ticket_management->task_holder_status == 1 || $ticket_management->task_holder_status == 2) &&
                                            ($ticket_management->final_status == 1 || $ticket_management->final_status == 2))
                                            <span
                                                @if ($ticket_management->assignment_date != null && $ticket_management->actual_start_date != null) wire:click="action({'id': {{ $ticket_management->id }}, closer: 'task_holder'}, 'close')" @endif
                                                class="flex pr-2 text-xs rounded-full p-1 border-2 md:w-24 {{ $ticket_management->actual_start_date == null ? 'text-gray-500 border-gray-500' : 'text-blue border-blue' }}">
                                                <div
                                                    class="h-4 w-4 rounded-full mr-1 {{ $ticket_management->actual_start_date == null ? 'bg-gray-500' : 'bg-blue' }}">
                                                </div>
                                                Pending
                                            </span>
                                        @else
                                            <span
                                                class="p-1 px-4 text-xs bg-blue-100 rounded-full text-blue">Closed</span>
                                        @endif
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        @if ($ticket_management->remarks != '')
                                            <span class="underline text-blue"
                                                wire:click="action({'id': {{ $ticket_management->id }}}, 'view_remarks')">View
                                            </span>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        @if ($ticket_management->requester_status == 1 || $ticket_management->requester_status == 2)
                                            <span class="text-xs ">
                                                Pending
                                            </span>
                                        @else
                                            <span class="text-xs">Closed</span>
                                        @endif
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="flex space-x-3">
                                            @if ($ticket_management->final_status != 3 && $ticket_management->target_start_date != null)
                                                @can('ticket_management_my_tasks_edit')
                                                    <svg wire:click="action({'id': {{ $ticket_management->id }}}, 'edit')"
                                                        class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                        data-prefix="far" data-icon="edit" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                            d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                                        </path>
                                                    </svg>
                                                @endcan
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @empty
                            @endforelse
                        </x-slot>
                    </x-table.table>
                    <div class="px-1 pb-2">
                        {{ $tickets_management->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
