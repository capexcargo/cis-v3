<div>
    <x-loading></x-loading>

    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div>
                <x-label for="task_holder" value="Task Holder" :required="true" />
                <x-select type="text" name="task_holder" wire:model.defer='task_holder'>
                    <option value=""></option>
                    @foreach ($taskholder_references as $taskholder_ref)
                        <option value="{{ $taskholder_ref->user->id }}">
                            {{ $taskholder_ref->user->name }}
                        </option>
                    @endforeach
                </x-select>
                <x-input-error for="task_holder" />
            </div>
            <div class="flex justify-end gap-3 pt-5 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">Cancel</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                    Save</button>
            </div>
        </div>
    </form>
</div>
