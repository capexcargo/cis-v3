<div wire:init="loadReferences" x-data="{
    confirmation_modal: '{{ $confirmation_modal }}'
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                Are you sure you want to submit this ticket?
            </h2>
            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    Cancel
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Submit
                </button>
            </div>
        </x-slot>
    </x-modal>
    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-2 gap-3">
                <div>
                    <x-label for="ticket_ref_no" value="Ticket Reference No." />
                    <x-input type="text" name="ticket_ref_no" wire:model.defer='ticket_ref_no' disabled></x-input>
                    <x-input-error for="ticket_ref_no" />
                </div>
                <div>
                    <x-label for="division" value="To Concern Division" :required="true" />
                    <x-select type="text" name="division" wire:model='division'>
                        <option value=""></option>
                        @foreach ($division_references as $division_ref)
                            <option value="{{ $division_ref->id }}">
                                {{ $division_ref->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="division" />
                </div>
            </div>
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="subject" value="Subject" :required="true" />
                    <x-input type="text" name="subject" wire:model.defer='subject'></x-input>
                    <x-input-error for="subject" />
                </div>
            </div>
            <div class="grid grid-cols-2 gap-3">
                <div>
                    <x-label for="category" value="Category" :required="true" />
                    <x-select type="text" name="category" wire:model='category'>
                        <option value=""></option>
                        @foreach ($category_references as $category_ref)
                            <option value="{{ $category_ref->id }}">
                                {{ $category_ref->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="category" />
                </div>
                <div>
                    <x-label for="subcategory" value="Subcategory" :required="true" />
                    <x-select type="text" name="subcategory" wire:model='subcategory'>
                        <option value=""></option>
                        @foreach ($subcategory_references as $subcategory_ref)
                            <option value="{{ $subcategory_ref->id }}">
                                {{ $subcategory_ref->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="subcategory" />
                </div>
            </div>
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="message" value="Message" />
                    <x-textarea type="text" name="message" wire:model.defer='message'></x-textarea>
                    <x-input-error for="message" />
                </div>
            </div>
            @if ($division == 1)
                <div class="grid grid-cols-1 gap-3">
                    <div>
                        <x-label for="concern_reference" value="Concern Reference" :required="true" />
                        <x-input type="text" name="concern_reference" wire:model.defer='concern_reference'>
                        </x-input>
                        <x-input-error for="concern_reference" />
                    </div>
                </div>
                <div class="grid grid-cols-1 gap-3">
                    <div>
                        <x-label for="concern_ref_url" value="Concern Reference URL" :required="true" />
                        <x-input type="text" name="concern_ref_url" wire:model.defer='concern_ref_url'></x-input>
                        <x-input-error for="concern_ref_url" />
                    </div>
                </div>
            @endif

            <div>
                <div class="text-2xl font-bold text-blue">
                    <div class="flex items-center -mb-1 space-x-3">
                        <x-label for="attachment" value="Attachment"/>
                    </div>
                    <x-table.table>
                        <x-slot name="thead">
                        </x-slot>
                        <x-slot name="tbody">
                            @forelse ($attachments as $i => $attachment)
                                <tr class="text-sm border-0 cursor-pointer">
                                    <td class="flex p-2 items-left justify-left whitespace-nowrap">
                                        <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                            <div class="relative z-0 ">
                                                <div class="absolute top-0 left-0" hidden>
                                                    @if (!$attachments[$i]['attachment'])
                                                        <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                            src="{{ $attachments[$i]['attachment'] ? $attachments[$i]['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                    @elseif (in_array($attachments[$i]['attachment']->extension(), config('filesystems.image_type')))
                                                        <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                            src="{{ $attachments[$i]['attachment'] ? $attachments[$i]['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                    @else
                                                        <svg class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg"
                                                            aria-hidden="true" focusable="false" data-prefix="fas"
                                                            data-icon="file-alt" role="img"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                                            <path fill="currentColor"
                                                                d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z">
                                                            </path>
                                                        </svg>
                                                    @endif
                                                </div>
                                                <input type="file"
                                                    name="attachments.{{ $i }}.attachment"
                                                    wire:model="attachments.{{ $i }}.attachment"
                                                    class="text-xs focus:outline-none focus:content-none">
                                            </div>
                                            <x-input-error for="attachments.{{ $i }}.attachment" />
                                        </div>
                                    </td>
                                    <td class="p-2 whitespace-nowrap">
                                        @if (count($attachments) > 1)
                                            <a wire:click="removeAttachments({{ $i }})">
                                                <p class="text-red-500 underline underline-offset-2">Remove</p>
                                            </a>
                                            <svg hidden wire:click="removeAttachments({{ $i }})"
                                                class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                data-prefix="fas" data-icon="times-circle" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="currentColor"
                                                    d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                </path>
                                            </svg>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">
                                        <p class="text-center">Empty.</p>
                                    </td>
                                </tr>
                            @endforelse
                        </x-slot>
                    </x-table.table>
                    <div class="flex items-center justify-start">
                        <button type="button" title="Add Attachment" wire:click="addAttachments"
                            class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                            +</button>
                    </div>
                </div>
            </div>
            <div class="flex justify-end gap-3 pt-5 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'create')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">Cancel</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                    Submit</button>
            </div>
        </div>
    </form>
</div>
