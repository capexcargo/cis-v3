<div wire:init="load" x-data="{
    search_form: false,
    view_modal: '{{ $view_modal }}',
    create_modal: '{{ $create_modal }}',
    confirmation_modal: '{{ $confirmation_modal }}'
}">
    <x-loading />
    @can('ticket_management_my_tickets_add')
        <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="title">Create New Ticket</x-slot>
            <x-slot name="body">
                @livewire('ticketing.ticket-management.my-tickets.create')
            </x-slot>
        </x-modal>
    @endcan
    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                {{ $confirmation_message }}
            </h2>

            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'close')"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    No
                </button>
                <button type="button" wire:click="confirm({{ $ticket_id }})"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
    <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-4/5">
        <x-slot name="body">
            @livewire('ticketing.ticket-management.view-tickets')
        </x-slot>
    </x-modal>
    <div class="px-6 py-8">
        <div class="flex items-center justify-between">
            <div class="grid grid-cols-1 gap-4">
                <div class="col-span-2">
                    <div class="grid grid-cols-10 gap-6">
                        <div class="col-span-4">
                            <ul class="flex">
                                <li
                                    class="px-3 py-1 text-xs text-white bg-gray-100 border border-gray-600 shadow-sm bg-blue rounded-l-md">
                                    <button wire:click="redirectTo({}, 'redirectToDashboard')">
                                        <span class="ml-2 mr-2 text-sm">Dashboard</span>
                                    </button>
                                </li>
                                <li
                                    class="px-3 py-1 text-xs text-gray-500 bg-gray-100 border border-gray-600 shadow-sm ">
                                    <button wire:click="redirectTo({}, 'redirectToAllTickets')">
                                        <span class="ml-2 mr-2 text-sm">All Tickets</span>
                                    </button>
                                </li>
                                <li
                                    class="px-3 py-1 text-xs text-gray-500 bg-gray-100 border border-gray-600 shadow-sm ">
                                    <button wire:click="redirectTo({}, 'redirectToMyTickets')">
                                        <span class="ml-2 mr-2 text-sm">My Tickets</span>
                                    </button>
                                </li>
                                <li
                                    class="px-3 py-1 text-xs text-gray-500 bg-gray-100 border border-gray-600 shadow-sm rounded-r-md">
                                    <button wire:click="redirectTo({}, 'redirectToMyTasks')">
                                        <span class="ml-2 mr-2 text-sm">My Tasks</span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex items-center justify-between space-x-3">
                @can('ticket_management_my_tickets_add')
                    <button wire:click="action({}, 'create')"
                        class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue whitespace-nowrap">
                        <div class="flex items-start justify-between">
                            <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                                data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 448 512">
                                <path fill="currentColor"
                                    d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                            </svg>
                            Create New Ticket
                        </div>
                    </button>
                @endcan
            </div>
        </div>
        <div class="grid grid-cols-4 gap-3 mt-8">
            <button wire:click="action({}, 'view_total_tickets')"
                class="flex justify-center px-4 py-6 text-xl text-gray-700 bg-white border rounded-md shadow-md hover:bg-gray-100">
                <div class="px-8 text-3xl font-bold">
                    <svg class="w-20 h-20 p-4 bg-blue-100 rounded-full text-blue" aria-hidden="true" focusable="false"
                        data-prefix="far" data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 576 512">
                        <path fill="currentColor"
                            d="M64 64C28.7 64 0 92.7 0 128v80c26.5 0 48 21.5 48 48s-21.5 48-48 48v80c0 35.3 28.7 64 64 64H512c35.3 0 64-28.7 64-64V304c-26.5 0-48-21.5-48-48s21.5-48 48-48V128c0-35.3-28.7-64-64-64H64zm64 96l0 192H448V160H128zm-32 0c0-17.7 14.3-32 32-32H448c17.7 0 32 14.3 32 32V352c0 17.7-14.3 32-32 32H128c-17.7 0-32-14.3-32-32V160z" />
                    </svg>
                </div>
                <div class="font-semibold text-left uppercase text-md"><span class="whitespace-normal">Total
                        Tickets</span><br><span
                        class="text-2xl font-normal text-gray-500">{{ count($total_tickets) }}</span></div>
            </button>
            <button wire:click=""
                class="flex justify-center px-4 py-6 text-xl text-gray-700 bg-white border rounded-md shadow-md hover:bg-gray-100">
                <div class="px-8 text-3xl font-bold">
                    <svg class="w-20 h-20 px-6 pr-4 rounded-full text-orange bg-orange-light" aria-hidden="true"
                        focusable="false" data-prefix="far" data-icon="edit" role="img"
                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                        <path fill="currentColor"
                            d="M32 0C14.3 0 0 14.3 0 32S14.3 64 32 64V75c0 42.4 16.9 83.1 46.9 113.1L146.7 256 78.9 323.9C48.9 353.9 32 394.6 32 437v11c-17.7 0-32 14.3-32 32s14.3 32 32 32H64 320h32c17.7 0 32-14.3 32-32s-14.3-32-32-32V437c0-42.4-16.9-83.1-46.9-113.1L237.3 256l67.9-67.9c30-30 46.9-70.7 46.9-113.1V64c17.7 0 32-14.3 32-32s-14.3-32-32-32H320 64 32zM288 437v11H96V437c0-25.5 10.1-49.9 28.1-67.9L192 301.3l67.9 67.9c18 18 28.1 42.4 28.1 67.9z" />
                    </svg>
                </div>
                <div class="font-semibold text-left uppercase text-md"><span class="whitespace-normal">Open
                        Tickets</span><br><span class="text-2xl font-normal text-gray-500">{{ $open_tickets }}</span>
                </div>
            </button>
            <button wire:click=""
                class="flex justify-center px-4 py-6 text-xl text-gray-700 bg-white border rounded-md shadow-md hover:bg-gray-100">
                <div class="px-8 text-3xl font-bold">
                    <svg class="w-20 h-20 p-4 bg-green-100 rounded-full text-green" aria-hidden="true" focusable="false"
                        data-prefix="far" data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 576 512">
                        <path fill="currentColor"
                            d="M243.8 339.8C232.9 350.7 215.1 350.7 204.2 339.8L140.2 275.8C129.3 264.9 129.3 247.1 140.2 236.2C151.1 225.3 168.9 225.3 179.8 236.2L224 280.4L332.2 172.2C343.1 161.3 360.9 161.3 371.8 172.2C382.7 183.1 382.7 200.9 371.8 211.8L243.8 339.8zM512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256zM256 48C141.1 48 48 141.1 48 256C48 370.9 141.1 464 256 464C370.9 464 464 370.9 464 256C464 141.1 370.9 48 256 48z" />
                    </svg>
                </div>
                <div class="font-semibold text-left uppercase text-md"><span class="whitespace-normal">Close
                        Tickets</span><br><span
                        class="text-2xl font-normal text-gray-500">{{ count($closed_tickets) }}</span></div>
            </button>
            <button wire:click=""
                class="flex justify-center px-4 py-6 text-xl text-gray-700 bg-white border rounded-md shadow-md hover:bg-gray-100">
                <div class="px-8 text-3xl font-bold">
                    <svg class="w-20 h-20 p-4 bg-red-100 rounded-full text-red" aria-hidden="true" focusable="false"
                        data-prefix="far" data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 576 512">
                        <path fill="currentColor"
                            d="M232 120C232 106.7 242.7 96 256 96C269.3 96 280 106.7 280 120V243.2L365.3 300C376.3 307.4 379.3 322.3 371.1 333.3C364.6 344.3 349.7 347.3 338.7 339.1L242.7 275.1C236 271.5 232 264 232 255.1L232 120zM256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256C0 114.6 114.6 0 256 0zM48 256C48 370.9 141.1 464 256 464C370.9 464 464 370.9 464 256C464 141.1 370.9 48 256 48C141.1 48 48 141.1 48 256z" />
                    </svg>
                </div>
                <div class="font-semibold text-left uppercase text-md"><span class="whitespace-normal">Overdue
                        Tickets</span><br><span
                        class="text-2xl font-normal text-gray-500">{{ count($count_overdue) }}</span></div>
            </button>
        </div>
        <div class="flex gap-8 mt-10">
            <div class="w-40">
                <x-transparent.input type="date" label="Date From" name="date_from"
                    wire:model.defer="date_from" />
            </div>
            <div class="w-40">
                <x-transparent.input type="date" label="Date To" name="date_to" wire:model.defer="date_to" />
            </div>
            <div>
                <button wire:click="searchDateRange"
                    class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue whitespace-nowrap">
                    <div class="font-semibold text-md"><span class="whitespace-normal">Search
                    </div>
                </button>
            </div>
        </div>

        <div class="grid grid-cols-10 gap-5 mt-3">
            <div class="col-span-6">
                <div class="bg-white rounded-lg shadow-lg">
                    <div class="flex justify-between">
                        <div class="min-w-0 p-3 px-6">
                            <div class="font-semibold text-gray-500">
                                <h4 class="text-[16px]">
                                    Ticket Volume
                                </h4>
                                <div hidden class="mt-4 text-sm">
                                    <span class="flex p-1 text-blue whitespace-nowrap">
                                        <div class="w-3 h-3 mt-1 mr-2 rounded-full bg-blue"></div>Total
                                    </span>
                                    <div class="flex p-1 text-green whitespace-nowrap">
                                        <div class="w-3 h-3 mt-1 mr-2 rounded-full bg-green"></div>Closed
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="min-w-0 p-3 font-semibold" x-data="{ open: false }">
                            <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4">
                                <svg @click="open = !open" class="w-5 h-5" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="edit" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M128 0c17.7 0 32 14.3 32 32V64H288V32c0-17.7 14.3-32 32-32s32 14.3 32 32V64h48c26.5 0 48 21.5 48 48v48H0V112C0 85.5 21.5 64 48 64H96V32c0-17.7 14.3-32 32-32zM0 192H448V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V192zm64 80v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H80c-8.8 0-16 7.2-16 16zm128 0v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H208c-8.8 0-16 7.2-16 16zm144-16c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H336zM64 400v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H80c-8.8 0-16 7.2-16 16zm144-16c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H208zm112 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H336c-8.8 0-16 7.2-16 16z" />
                                </svg>
                                <div class="relative" style="margin-top: 0;">
                                    <ul class="absolute text-gray-600 bg-white rounded shadow"
                                        style="margin-left: -59px;" x-show="open" @click.away="open = false">
                                        <li class="px-3 py-1 border-b hover:bg-indigo-100" x-data="{ open: false }">
                                            <a wire:click="action({},'') }}">Daily</a>
                                        </li>
                                        <li class="px-3 py-1 border-b hover:bg-indigo-100" x-data="{ open: false }">
                                            <a wire:click="action({},'') }}">Weekly</a>
                                        </li>
                                        <li class="px-3 py-1 border-b hover:bg-indigo-100" x-data="{ open: false }">
                                            <a wire:click="action({},'') }}">Monthly</a>
                                        </li>
                                    </ul>
                                </div>
                            </h4>
                        </div>
                    </div>
                    <div class="flex overflow-hidden" style="width: 100%; height:400;">
                        <canvas class="p-10" wire:ignore id="chartLine" wire:model="chartline"></canvas>
                        <p class="mb-2 text-sm text-center text-gray-400" style="margin-top: -4%">Date</p>
                    </div>
                </div>
            </div>
            <div class="col-span-4 space-y-4">
                <div class="bg-white rounded-lg shadow-lg">
                    <div class="flex justify-between">
                        <div class="min-w-0 px-6 py-3 text-left">
                            <div class="">
                                <h4 class="font-semibold text-gray-500 text-[16px]">
                                    Ticket Category
                                </h4>
                            </div>
                        </div>

                        <div class="min-w-0 p-3 font-semibold" x-data="{ open: false }">
                            <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4">
                                <svg @click="open = !open" class="w-5 h-5" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="edit" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M128 0c17.7 0 32 14.3 32 32V64H288V32c0-17.7 14.3-32 32-32s32 14.3 32 32V64h48c26.5 0 48 21.5 48 48v48H0V112C0 85.5 21.5 64 48 64H96V32c0-17.7 14.3-32 32-32zM0 192H448V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V192zm64 80v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H80c-8.8 0-16 7.2-16 16zm128 0v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H208c-8.8 0-16 7.2-16 16zm144-16c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H336zM64 400v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H80c-8.8 0-16 7.2-16 16zm144-16c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H208zm112 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H336c-8.8 0-16 7.2-16 16z" />
                                </svg>
                                <div class="relative" style="margin-top: 0;">
                                    <ul class="absolute text-gray-600 bg-white rounded shadow"
                                        style="margin-left: -59px;" x-show="open" @click.away="open = false">
                                        <li class="px-3 py-1 border-b hover:bg-indigo-100" x-data="{ open: false }">
                                            <a wire:click="action({},'') }}">Daily</a>
                                        </li>
                                        <li class="px-3 py-1 border-b hover:bg-indigo-100" x-data="{ open: false }">
                                            <a wire:click="action({},'') }}">Weekly</a>
                                        </li>
                                        <li class="px-3 py-1 border-b hover:bg-indigo-100" x-data="{ open: false }">
                                            <a wire:click="action({},'') }}">Monthly</a>
                                        </li>
                                    </ul>
                                </div>
                            </h4>
                        </div>
                    </div>
                    <div class="flex h-full gap-10 p-2 px-6">
                        <div class="grid min-w-0 grid-cols-1 p-2 mt-12 font-semibold">
                            <div class="w-56">
                                <h4 class="flex text-sm">
                                    <div class="w-4 h-4 mr-2 rounded-md bg-blue-pie"></div>
                                    <p>Help Desk</p>
                                </h4>
                                <p class="ml-6 text-xs font-normal">{{ $help_desk }}</p><br>
                                <h4 class="flex text-sm">
                                    <div class="w-4 h-4 mr-2 rounded-md bg-sky-blue-pie"></div>
                                    <p>Technical Support</p>
                                </h4>
                                <p class="ml-6 text-xs font-normal">{{ $tech_supp }}</p>
                            </div>
                        </div>
                        <div class="flex pb-2 overflow-hidden" style="width: 35%; height:35%;">
                            <canvas wire:ignore id="chartPie" wire:model="chartPie"></canvas>
                        </div>
                    </div>
                </div>
                <div class="h-full bg-white rounded-lg shadow-lg ">
                    <div class="flex justify-between">
                        <div class="min-w-0 px-6 py-3 text-left">
                            <div class="">
                                <h4 class="font-semibold text-gray-500 text-[16px]">
                                    SLA Compliance
                                </h4>
                            </div>
                        </div>
                        <div class="min-w-0 p-3 font-semibold" x-data="{ open: false }">
                            <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4">
                                <svg @click="open = !open" class="w-5 h-5" aria-hidden="true" focusable="false"
                                    data-prefix="far" data-icon="edit" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                    <path fill="currentColor"
                                        d="M128 0c17.7 0 32 14.3 32 32V64H288V32c0-17.7 14.3-32 32-32s32 14.3 32 32V64h48c26.5 0 48 21.5 48 48v48H0V112C0 85.5 21.5 64 48 64H96V32c0-17.7 14.3-32 32-32zM0 192H448V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V192zm64 80v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H80c-8.8 0-16 7.2-16 16zm128 0v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H208c-8.8 0-16 7.2-16 16zm144-16c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H336zM64 400v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H80c-8.8 0-16 7.2-16 16zm144-16c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H208zm112 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H336c-8.8 0-16 7.2-16 16z" />
                                </svg>
                                <div class="relative" style="margin-top: 0;">
                                    <ul class="absolute text-gray-600 bg-white rounded shadow"
                                        style="margin-left: -59px;" x-show="open" @click.away="open = false">
                                        <li class="px-3 py-1 border-b hover:bg-indigo-100" x-data="{ open: false }">
                                            <a wire:click="action({},'') }}">Daily</a>
                                        </li>
                                        <li class="px-3 py-1 border-b hover:bg-indigo-100" x-data="{ open: false }">
                                            <a wire:click="action({},'') }}">Weekly</a>
                                        </li>
                                        <li class="px-3 py-1 border-b hover:bg-indigo-100" x-data="{ open: false }">
                                            <a wire:click="action({},'') }}">Monthly</a>
                                        </li>
                                    </ul>
                                </div>
                            </h4>
                        </div>
                    </div>
                    <div class="flex h-full gap-10 p-2 px-6">
                        <div class="grid min-w-0 grid-cols-1 p-2 mt-12 font-semibold">
                            <div class="w-56">
                                <h4 class="flex text-sm">
                                    <div class="w-4 h-4 mr-2 bg-[#3BDD46] rounded-md"></div>
                                    <p>Within SLA</p>
                                </h4>
                                <p class="ml-6 text-xs font-normal">{{ $within_sla }}</p><br>
                                <h4 class="flex text-sm">
                                    <div class="w-4 h-4 mr-2 bg-[#FF4040] rounded-md"></div>
                                    <p>Beyond SLA</p>
                                </h4>
                                <p class="ml-6 text-xs font-normal">{{ $beyond_sla }}</p>
                            </div>
                        </div>
                        <div class="flex overflow-hidden" style="width: 38%; height:38%;">
                            <canvas wire:ignore id="chartPie2" wire:model="chartPie2"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <!-- Required chart.js -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        var lineChart = null;
        var pieChart = null;
        var pieChart2 = null;

        // Ticket Volume
        function updateTicketVolume(days_in_tickets, total_weekly_tickets, total_closed_weekly_tickets) {
            if (lineChart) {
                lineChart.destroy();
            }

            var dataFirst = {
                label: "Total",
                data: total_weekly_tickets,
                lineTension: 0,
                fill: false,
                backgroundColor: "blue",
                borderColor: 'blue'
            };

            var dataSecond = {
                label: "Closed",
                data: total_closed_weekly_tickets,
                lineTension: 0,
                fill: false,
                backgroundColor: "green",
                borderColor: 'green'
            };

            var tickedVolumeData = {
                labels: days_in_tickets,
                datasets: [dataFirst, dataSecond]
            };

            var chartOptions = {
                legend: {
                    display: true,
                    position: 'top',
                    labels: {
                        boxWidth: 80,
                        fontColor: 'black'
                    }
                }
            };

            lineChart = new Chart(document.getElementById("chartLine"), {
                type: 'line',
                data: tickedVolumeData,
                options: chartOptions
            });
        }

        // Ticket Category
        function updateTicketCatPieChart(data) {
            if (pieChart) {
                pieChart.destroy();
            }
            var ctx = document.getElementById("chartPie").getContext('2d');

            const dataPie = {
                datasets: [{
                    label: "Ticket Category",
                    data: data,
                    backgroundColor: [
                        "#1F78B4",
                        "#A6CEE3",
                    ],
                    hoverOffset: 4,
                }],
            };

            pieChart = new Chart(ctx, {
                label: "Employment Category",
                type: 'pie',
                data: dataPie,
                options: {
                    // maintainAspectRatio: false,
                }
            });
        }

        // SLA Compliance
        function updateSlaCompliancePieChart(data) {
            if (pieChart2) {
                pieChart2.destroy();
            }
            var ctx = document.getElementById("chartPie2").getContext('2d');

            const dataPie2 = {
                datasets: [{
                    label: "SLA Compliance",
                    data: data,
                    backgroundColor: [
                        "#3BDD46",
                        "#FF4040",
                    ],
                    hoverOffset: 4,
                }],
            };

            pieChart2 = new Chart(ctx, {
                label: "Employment Category",
                type: 'pie',
                data: dataPie2,
                options: {
                    // maintainAspectRatio: false,
                }
            });
        }

        document.addEventListener('livewire:load', function() {
            var chart_data_ticket_cat = [{{ $tech_supp }}, {{ $help_desk }}];
            updateTicketCatPieChart(chart_data_ticket_cat);

            var chart_data_sla_compliance = [{{ $within_sla }}, {{ $beyond_sla }}];
            updateSlaCompliancePieChart(chart_data_sla_compliance);

            var days_in_tickets = [
                "{{ $day1_tickets }}",
                "{{ $day2_tickets }}",
                "{{ $day3_tickets }}",
                "{{ $day4_tickets }}",
                "{{ $day5_tickets }}",
                "{{ $day6_tickets }}",
                "{{ $day7_tickets }}"
            ];

            var total_weekly_tickets = [
                {{ $weekly_total1_tickets }},
                {{ $weekly_total2_tickets }},
                {{ $weekly_total3_tickets }},
                {{ $weekly_total4_tickets }},
                {{ $weekly_total5_tickets }},
                {{ $weekly_total6_tickets }},
                {{ $weekly_total7_tickets }}
            ];

            var total_closed_weekly_tickets = [
                {{ $weekly_closed1_tickets }},
                {{ $weekly_closed2_tickets }},
                {{ $weekly_closed3_tickets }},
                {{ $weekly_closed4_tickets }},
                {{ $weekly_closed5_tickets }},
                {{ $weekly_closed6_tickets }},
                {{ $weekly_closed7_tickets }}
            ];

            updateTicketVolume(days_in_tickets, total_weekly_tickets, total_closed_weekly_tickets);

            Livewire.on('updatedCategories', function(data) {
                updateTicketCatPieChart(data.ticket_category_data);
                updateSlaCompliancePieChart(data.sla_compliance_data);

                updateTicketVolume(data.days_in_tickets, data.total_weekly_tickets, data
                    .total_closed_weekly_tickets);
            });
        });
    </script>
@endpush
