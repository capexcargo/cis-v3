<div>
    <x-loading></x-loading>

    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="space-y-3">
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="name" value="Category Name" :required="true" />
                    <x-input type="text" name="name" wire:model.defer='name'></x-input>
                    <x-input-error for="name" />
                </div>
            </div>
            <div wire:init="divisionReferences">
                <x-label for="division_id" value="Division" :required="true" />
                <x-select type="text" name="division_id" wire:model.defer='division_id'>
                    <option value=""></option>
                    @foreach ($division_references as $division_ref)
                        <option value="{{ $division_ref->id }}">
                            {{ $division_ref->name }}
                        </option>
                    @endforeach
                </x-select>
                <x-input-error for="division_id" />
            </div>
            <div class="flex justify-end gap-3 pt-5 space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">Cancel</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                    Submit</button>
            </div>
        </div>
    </form>
</div>
