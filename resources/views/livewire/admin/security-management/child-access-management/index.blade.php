<div class="flex flex-col" wire:init="load">
    <x-loading></x-loading>
    @if ($function == 'create')
        <form wire:submit.prevent="submitCreate" autocomplete="off">
            <div class="grid grid-cols-2 gap-3 py-3">
                <div class="col-span-2">
                    <x-label for="division" value="Account Type" />
                    <x-select name="division" wire:model="division">
                        <option></option>
                        @foreach ($division_references as $division_reference)
                            <option value="{{ $division_reference->id }}">{{ $division_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="division" class="mt-2" />
                </div>
                <div>
                    <x-label for="parent_name" value="Parent Name" />
                    <x-select name="parent_name" wire:model="parent_name">
                        <option></option>
                        @foreach ($parent_access_references as $parent_access_reference)
                            <option value="{{ $parent_access_reference->id }}">
                                {{ $parent_access_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="parent_name" class="mt-2" />
                </div>
                <div>
                    <x-label for="child_name" value="Child Access" />
                    <x-input type="text" name="child_name" wire:model.defer="child_name" />
                    <x-input-error for="child_name" class="mt-2" />
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Create</button>
            </div>
        </form>
    @elseif($function == 'update')
        <form wire:submit.prevent="submitUpdate" autocomplete="off">
            <div class="grid grid-cols-2 gap-3 py-3">
                <div class="col-span-2">
                    <x-label for="division" value="Account Type" />
                    <x-select name="division" wire:model="division">
                        <option></option>
                        @foreach ($division_references as $division_reference)
                            <option value="{{ $division_reference->id }}">{{ $division_reference->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="division" class="mt-2" />
                </div>
                <div>
                    <x-label for="parent_name" value="Parent Name" />
                    <x-select name="parent_name" wire:model="parent_name">
                        <option></option>
                        @foreach ($parent_access_references as $parent_access_reference)
                            <option value="{{ $parent_access_reference->id }}">
                                {{ $parent_access_reference->display }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="parent_name" class="mt-2" />
                </div>
                <div>
                    <x-label for="child_name" value="Child Access" />
                    <x-input type="text" name="child_name" wire:model.defer="child_name" />
                    <x-input-error for="child_name" class="mt-2" />
                </div>
            </div>
            <div class="flex justify-end space-x-3">
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Update</button>
                <button type="button" wire:click="action('','', 'create')"
                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Cancel</button>
            </div>
        </form>
    @endif
    <div x-data="{open: true}">
        @if (session()->has('message'))
            <div class="relative px-4 py-3 text-green-700 bg-green-100 border border-green-400 rounded"
                :class="open ? '' : 'hidden'" role="alert">
                <span class="block sm:inline">{{ session('message') }}</span>
                <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
                    <svg @click="open = false" class="w-6 h-6 text-green-500 fill-current" role="button"
                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <title>Close</title>
                        <path
                            d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" />
                    </svg>
                </span>
            </div>
        @endif
    </div>
    <div class="flex items-center justify-between">
        <div class="w-32">
            <x-select id="paginate" name="paginate" wire:model="paginate">
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
            </x-select>
        </div>
    </div>
    <x-table.table>
        <x-slot name="thead">
            <x-table.th name="Parent Name" wire:click="sortBy('display')">
                <x-slot name="sort">
                    <x-table.sort-icon field="display" sortField="{{ $sortField }}" sortAsc="{{ $sortAsc }}">
                    </x-table.sort-icon>
                </x-slot>
            </x-table.th>
            <x-table.th name="Child Name" wire:click="sortBy('display')">
                <x-slot name="sort">
                    <x-table.sort-icon field="display" sortField="{{ $sortField }}" sortAsc="{{ $sortAsc }}">
                    </x-table.sort-icon>
                </x-slot>
            </x-table.th>
            <x-table.th name="Status" wire:click="sortBy('is_visible')">
                <x-slot name="sort">
                    <x-table.sort-icon field="is_visible" sortField="{{ $sortField }}"
                        sortAsc="{{ $sortAsc }}">
                    </x-table.sort-icon>
                </x-slot>
            </x-table.th>
            <x-table.th name="Created At" wire:click="sortBy('created_at')">
                <x-slot name="sort">
                    <x-table.sort-icon field="created_at" sortField="{{ $sortField }}"
                        sortAsc="{{ $sortAsc }}">
                    </x-table.sort-icon>
                </x-slot>
            </x-table.th>
            <x-table.th name="Action" />
        </x-slot>
        <x-slot name="tbody">
            @foreach ($parent_accesses as $parent_access)
                @forelse ($parent_access->accesses as $child_access)
                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                        @if ($loop->first)
                            <td rowspan="{{ count($parent_access->accesses) }}" class="p-3 whitespace-nowrap ">
                                {{ $parent_access->display }}
                            </td>
                        @endif
                        <td class="p-3 whitespace-nowrap ">
                            {{ $child_access->display }}
                        </td>
                        <td class="p-3 whitespace-nowrap">
                            <div class="flex justify-between">
                                @if ($child_access->is_visible == 1)
                                    <span class="text-green-500">Active</span>
                                @else
                                    <span class="text-red-500">Inactive</span>
                                @endif
                                <div @click.away="open = false" class="relative" x-data="{ open: false }">
                                    <div>
                                        <button @click="open = !open"
                                            class="flex items-center max-w-xs text-sm transition-all duration-300 outline-none focus:outline-none focus:shadow-solid">
                                            <span :class="{ 'bg-blue-500': open === true,'': open === false }"
                                                class="flex items-center justify-center p-1 transition-all duration-300 rounded-full group hover:bg-blue-500">
                                                <svg :class="{ 'text-white': open === true,'text-[#8a82a1]': open === false }"
                                                    class="w-5 h-5 transition-all duration-300 group-hover:text-white"
                                                    xmlns="http://www.w3.org/2000/svg" class="w-5 h-5"
                                                    viewBox="0 0 20 20" fill="currentColor">
                                                    <path fill-rule="evenodd"
                                                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                                        clip-rule="evenodd" />
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                    <div x-show="open" x-transition:enter="transition ease-out duration-100"
                                        x-transition:enter-start="transform opacity-0 scale-95"
                                        x-transition:enter-end="transform opacity-100 scale-100"
                                        x-transition:leave="transition ease-in duration-75"
                                        x-transition:leave-start="transform opacity-100 scale-100"
                                        x-transition:leave-end="transform opacity-0 scale-95"
                                        class="absolute z-10 mt-1 -ml-10 origin-center rounded-md shadow-lg">
                                        <div class="w-full py-1 bg-white rounded-md shadow-xs">
                                            <a href="#" wire:click="updateStatus({{ $child_access->id }}, true)"
                                                @click="open = !open"
                                                class="block px-4 py-2 text-sm text-green-500 transition duration-150 ease-in-out hover:bg-blue-200">Active</a>
                                            <a href="#" wire:click="updateStatus({{ $child_access->id }}, false)"
                                                @click="open = !open"
                                                class="block px-4 py-2 text-sm text-red-500 transition duration-150 ease-in-out hover:bg-blue-200">Inactive</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="p-3 whitespace-nowrap ">
                            {{ date('M/d/Y', strtotime($child_access->created_at)) }}
                        </td>
                        <td class="relative p-3 whitespace-nowrap">
                            <div @click.away="open = false" x-data="{ open: false }">
                                <div>
                                    <button @click="open = !open"
                                        class="flex items-center max-w-xs text-sm transition-all duration-300 outline-none focus:outline-none focus:shadow-solid">
                                        <span :class="{ 'bg-blue-500': open === true,'': open === false }"
                                            class="flex items-center justify-center p-1 transition-all duration-300 rounded-full group hover:bg-blue-500">
                                            <svg :class="{ 'text-white': open === true,'text-[#8a82a1]': open === false }"
                                                class="w-5 h-5 transition-all duration-300 group-hover:text-white"
                                                aria-hidden="true" focusable="false" data-prefix="far"
                                                data-icon="ellipsis-v" role="img" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 128 512">
                                                <path fill="currentColor"
                                                    d="M64 208c26.5 0 48 21.5 48 48s-21.5 48-48 48-48-21.5-48-48 21.5-48 48-48zM16 104c0 26.5 21.5 48 48 48s48-21.5 48-48-21.5-48-48-48-48 21.5-48 48zm0 304c0 26.5 21.5 48 48 48s48-21.5 48-48-21.5-48-48-48-48 21.5-48 48z">
                                                </path>
                                            </svg>
                                        </span>
                                    </button>
                                </div>
                                <div x-show="open" x-transition:enter="transition ease-out duration-100"
                                    x-transition:enter-start="transform opacity-0 scale-95"
                                    x-transition:enter-end="transform opacity-100 scale-100"
                                    x-transition:leave="transition ease-in duration-75"
                                    x-transition:leave-start="transform opacity-100 scale-100"
                                    x-transition:leave-end="transform opacity-0 scale-95"
                                    class="absolute z-10 mt-1 -ml-10 origin-center rounded-md shadow-lg">
                                    <div class="py-1 bg-white rounded-md shadow-xs ">
                                        <a href="#"
                                            wire:click="action({{ $parent_access->id }}, {{ $child_access->id }}, 'update')"
                                            @click="open = !open"
                                            class="block px-4 py-2 text-sm text-gray-700 transition duration-150 ease-in-out hover:bg-blue-200">Edit</a>
                                        <a href="#"
                                            wire:click="actionDelete({{ $parent_access->id }}, {{ $child_access->id }})"
                                            @click="open = !open"
                                            class="block px-4 py-2 text-sm text-gray-700 transition duration-150 ease-in-out hover:bg-blue-200">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr class="text-sm  border-0 cursor-pointer even:bg-white hover:text-white hover:bg-[#4068b8]">
                        <td class="p-3 whitespace-nowrap ">
                            {{ $parent_access->display }}
                        </td>
                        <td colspan="4" class="p-3 whitespace-nowrap">
                            No child found.
                        </td>
                    </tr>
                @endforelse
            @endforeach
        </x-slot>
    </x-table.table>
    <div class="px-1 pb-2">
        {{ $parent_accesses->links() }}
    </div>
    <div class="flex justify-end mt-3 space-x-3">
        <button type="button" wire:click="$emit('close_modal', 'child')"
            class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
    </div>
</div>
