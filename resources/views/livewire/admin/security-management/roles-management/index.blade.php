<x-form wire:init="load" x-data="{ search_form: false, create_modal: '{{ $create_modal }}', edit_modal: '{{ $edit_modal }}' }">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        @can('admin_roles_management_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                <x-slot name="title">Create Roles</x-slot>
                <x-slot name="body">
                    @livewire('admin.security-management.roles-management.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('admin_roles_management_edit')
            @if ($role_id)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-11/12">
                    <x-slot name="title">Edit Roles</x-slot>
                    <x-slot name="body">
                        @livewire('admin.security-management.roles-management.edit', ['id' => $role_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>
    <x-slot name="header_title">Roles List</x-slot>
    <x-slot name="header_button">
        @can('admin_roles_management_add')
            <button wire:click="$set('create_modal', true)"
                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Create a Role</button>
        @endcan
    </x-slot>
    <x-slot name="header_card">
        @forelse ($header_cards as $index => $card)
            <x-card.header wire:click="$set('{{ $card['action'] }}', {{ $card['id'] }})"
                wire:key="{{ $index }}" :card="$card"></x-card.header>
        @empty
            <x-card.header-loading count="8"></x-card.header-loading>
        @endforelse
    </x-slot>
    <x-slot name="search_form">
        <div>
            <x-jet-label for="division" value="Division" />
            <x-select name="division" wire:model="division">
                <option></option>
                @foreach ($division_references as $division_reference)
                    <option value="{{ $division_reference->id }}">{{ $division_reference->name }}</option>
                @endforeach
            </x-select>
            <x-jet-input-error for="division" class="mt-2" />
        </div>
        <div>
            <x-jet-label for="role_name" value="Role Name" />
            <x-input type="text" name="role_name" wire:model="role_name"></x-input>
            <x-jet-input-error for="role_name" class="mt-2" />
        </div>
        <div>
            <x-jet-label for="status" value="Status" />
            <x-select name="status" wire:model="status">
                <option value=""></option>
                <option value="1">Active</option>
                <option value="false">Inactive</option>
            </x-select>
            <x-jet-input-error for="status" class="mt-2" />
        </div>
    </x-slot>
    <x-slot name="body">
        <div>
            <div class="flex items-center justify-between">
                <div class="w-32">
                    <x-select id="paginate" name="paginate" wire:model="paginate">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-select>
                </div>
            </div>
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table>
                    <x-slot name="thead">
                        <x-table.th name="division" wire:click="sortBy('division_id')">
                            <x-slot name="sort">
                                <x-table.sort-icon field="division_id" sortField="{{ $sortField }}"
                                    sortAsc="{{ $sortAsc }}">
                                </x-table.sort-icon>
                            </x-slot>
                        </x-table.th>
                        <x-table.th name="Level" wire:click="sortBy('level_id')">
                            <x-slot name="sort">
                                <x-table.sort-icon field="level_id" sortField="{{ $sortField }}"
                                    sortAsc="{{ $sortAsc }}">
                                </x-table.sort-icon>
                            </x-slot>
                        </x-table.th>
                        <x-table.th name="Role Name" wire:click="sortBy('display')">
                            <x-slot name="sort">
                                <x-table.sort-icon field="display" sortField="{{ $sortField }}"
                                    sortAsc="{{ $sortAsc }}">
                                </x-table.sort-icon>
                            </x-slot>
                        </x-table.th>
                        <x-table.th name="Status" />
                        <x-table.th name="Created At" />
                        <x-table.th name="Action" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($roles as $role)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                <td class="p-3 whitespace-nowrap">
                                    {{ $role->division->name }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $role->level->display }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ $role->display }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    <div class="flex items-center space-x-3">
                                        @if ($role->is_visible == 1)
                                            <svg wire:click="updateStatus({{ $role->id }}, false)"
                                                class="w-5 h-5 text-green" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="check-square" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M400 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V80c0-26.51-21.49-48-48-48zm0 400H48V80h352v352zm-35.864-241.724L191.547 361.48c-4.705 4.667-12.303 4.637-16.97-.068l-90.781-91.516c-4.667-4.705-4.637-12.303.069-16.971l22.719-22.536c4.705-4.667 12.303-4.637 16.97.069l59.792 60.277 141.352-140.216c4.705-4.667 12.303-4.637 16.97.068l22.536 22.718c4.667 4.706 4.637 12.304-.068 16.971z">
                                                </path>
                                            </svg>
                                            <span class="text-green">Active</span>
                                        @else
                                            <svg wire:click="updateStatus({{ $role->id }}, true)"
                                                class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="square" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-6 400H54c-3.3 0-6-2.7-6-6V86c0-3.3 2.7-6 6-6h340c3.3 0 6 2.7 6 6v340c0 3.3-2.7 6-6 6z">
                                                </path>
                                            </svg>
                                            <span class="text-red">Inactive</span>
                                        @endif
                                    </div>
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    {{ date('M/d/Y', strtotime($role->created_at)) }}
                                </td>
                                <td class="p-3 whitespace-nowrap">
                                    @can('admin_roles_management_edit')
                                        <svg wire:click="action({{ $role->id }})" class="w-5 h-5 text-blue"
                                            aria-hidden="true" focusable="false" data-prefix="far" data-icon="edit"
                                            role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z">
                                            </path>
                                        </svg>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $roles->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
