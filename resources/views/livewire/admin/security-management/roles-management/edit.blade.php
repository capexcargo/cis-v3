<div class="flex flex-col" wire:init="load">
    <x-loading></x-loading>
    <div x-data="{parent_access_modal: '{{ $parent_access_modal }}', child_access_modal: '{{ $child_access_modal }}'}"
        class="flex justify-end space-x-3">
        @can('admin_roles_management_add_parent')
            <x-modal id="parent_access_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-6/12">
                <x-slot name="title">Create Parent Access</x-slot>
                <x-slot name="body">
                    @livewire('admin.security-management.parent-access-management.index')
                </x-slot>
            </x-modal>
            <button wire:click="$set('parent_access_modal', true)"
                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Parent Access</button>
        @endcan
        @can('admin_roles_management_add_child')
            <x-modal id="child_access_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-6/12">
                <x-slot name="title">Create Child Access</x-slot>
                <x-slot name="body">
                    @livewire('admin.security-management.child-access-management.index')
                </x-slot>
            </x-modal>
            <button wire:click="$set('child_access_modal', true)"
                class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">Child Access</button>
        @endcan
    </div>
    <div class="mt-3">
        <form wire:submit.prevent="submit" autocomplete="off">
            <div class="grid grid-cols-3 gap-3 py-3">
                <div>
                    <x-label for="division" value="Division" />
                    <x-select name="division" wire:model="division">
                        <option></option>
                        @foreach ($division_references as $division_reference)
                            <option value="{{ $division_reference->id }}">{{ $division_reference->name }}</option>
                        @endforeach
                    </x-select>
                    <x-input-error for="division" class="mt-2" />
                </div>
                <div>
                    <x-label for="level" value="Level" />
                    <x-select name="level" wire:model.defer="level">
                        <option></option>
                        @foreach ($level_references as $level_reference)
                            <option value="{{ $level_reference->id }}">{{ $level_reference->display }}</option>
                        @endforeach
                    </x-select>
                    <x-input-error for="level" class="mt-2" />
                </div>
                <div>
                    <x-label for="role_name" value="Role Name" />
                    <x-input type="text" name="role_name" wire:model.defer="role_name" />
                    <x-input-error for="role_name" class="mt-2" />
                </div>
            </div>
            <div class="text-xl font-semibold text-gray-800">
                <div class="flex items-center space-x-3">
                    <span>Access</span>
                </div>
            </div>
            <div class="flex">
                <x-input type="text" name="search" wire:model.debounce.500ms="search" placeholder="Search Role" />
            </div>
            <div class="grid grid-cols-1 gap-3 p-3 mt-3 xs:grid-cols-1 sm:grid-cols-1 md:grid-cols-4">
                @forelse ($parent_accesses as $parent_access)
                    <div class="block">
                        <span class="text-lg font-bold text-gray-700 text-blue">{{ $parent_access->display }}</span>
                        <div class="mt-2">
                            @forelse ($parent_access->accesses as $child_access)
                                <div>
                                    <label class="inline-flex items-center space-x-3">
                                        <input type="checkbox" class="form-checkbox" name="{{ $child_access->code }}"
                                            value="{{ $child_access->id }}" wire:model.defer="selected_accesses">
                                        <x-label value="{{ $child_access->display }}" />
                                    </label>
                                </div>
                            @empty
                                <x-label value="No child found." />
                            @endforelse
                        </div>
                    </div>
                @empty
                @endforelse
            </div>
            {{ $parent_accesses->links() }}
            <div class="flex justify-end space-x-3">
                <button type="button" wire:click="$emit('close_modal', 'edit')"
                    class="px-3 py-1 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-full hover:text-white hover:bg-red-400">Close</button>
                <button type="submit" class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-full">
                    Update</button>
            </div>
        </form>
    </div>
</div>
