<div x-data="{
    {{-- confirmation_modal_meeting: '{{ $confirmation_modal_meeting }}', --}}
}">
    <x-loading></x-loading>

    {{-- <x-modal id="confirmation_modal_meeting" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal_meeting', false)">
                </span>
            </span>
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                Are you sure you want to submit Log a Meeting?
            </h2>


            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$set('confirmation_modal_meeting', false)"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    Cancel
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Submit
                </button>
            </div>
        </x-slot>
    </x-modal> --}}

    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="mt-5 space-y-6">
            <div class="grid grid-cols-1 gap-3">
                <div x-data="{ open: false }" @click.away="open = false">
                    <div>
                        <x-label for="customer_id_meeting" value="Customer Name" :required="true" />
                        <x-input class="w-full h-10 rounded-md" maxlength="50" style="cursor: pointer;" type="text"
                            name="customer_id_meeting" wire:model='customer_id_meeting' @click="open = !open">
                        </x-input>
                        <x-input-error for="customer_id_meeting" />

                    </div>
                    <div x-show="open" x-cloak
                        class="absolute w-11/12 overflow-hidden overflow-y-auto bg-gray-100 rounded-md shadow h-11 max-h-96"
                        style="">
                        <ul class="list-reset">
                            @foreach ($customer_nos as $i => $customer_no)
                                <li @click="open = !open" wire:key="{{ 'customer_no_2' . $i }}"
                                    wire:click="getCustomerDetailsMeeting({{ $customer_no->id }})"
                                    class="px-2 text-black cursor-pointer hover:bg-gray-200">
                                    <p>
                                        {{ $customer_no->fullname }}
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="attendees" value="Attendees" />
                    <x-input type="text" name="attendees" maxlength="50" wire:model.defer='attendees'></x-input>
                    <x-input-error for="attendees" />
                    {{-- <div x-data @tags-update="console.log('tags updated', $event.detail.tags)" data-tags='[]'
                        class="max-w-lg m-6 bg-gray-400">
                        <div x-data="tagSelect()" x-init="init('parentEl')" @click.away="clearSearch()"
                            @keydown.escape="clearSearch()">
                            <div class="relative" @keydown.enter.prevent="addTag(textInput)">
                                <input x-model="textInput" x-ref="textInput" @input="search($event.target.value)"
                                    class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                    name="attendees" placeholder="">
                                <div :class="[open ? 'block' : 'hidden']">
                                    <div class="absolute left-0 z-40 w-full mt-2">
                                        <div class="py-1 text-sm bg-white border border-gray-300 rounded shadow-lg">
                                            <a @click.prevent="addTag(textInput)"
                                                class="block px-5 py-1 cursor-pointer hover:bg-indigo-600 hover:text-white">Add
                                                tag "<span class="font-semibold" x-text="textInput"></span>"</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- selections -->
                                <template x-for="(tag, index) in tags">
                                    <div class="inline-flex items-center mt-2 mr-1 text-sm bg-indigo-100 rounded">
                                       
                                        <span class="max-w-xs ml-2 mr-1 leading-relaxed truncate" x-text="tag"
                                            wire:model="tagsasdasd"></span>
                                        <button @click.prevent="removeTag(index)"
                                            class="inline-block w-6 h-8 text-gray-500 align-middle hover:text-gray-600 focus:outline-none">
                                            <svg class="w-6 h-6 mx-auto fill-current" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 24 24">
                                                <path fill-rule="evenodd"
                                                    d="M15.78 14.36a1 1 0 0 1-1.42 1.42l-2.82-2.83-2.83 2.83a1 1 0 1 1-1.42-1.42l2.83-2.82L7.3 8.7a1 1 0 0 1 1.42-1.42l2.83 2.83 2.82-2.83a1 1 0 0 1 1.42 1.42l-2.83 2.83 2.83 2.82z" />
                                            </svg>
                                        </button>
                                    </div>
                                </template>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
            <div class="grid grid-cols-1 gap-3">
                <div>
                    {{-- <input type="text" id="asdasd"
                    class="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"> --}}
                    <x-label for="outcome" value="Outcome" :required="true" />
                    <x-select name="outcome" wire:model.defer='outcome'>
                        <option value="1"> None </option>
                        <option value="2"> Scheduled </option>
                        <option value="3"> Completed </option>
                        <option value="4"> Rescheduled </option>
                        <option value="5"> No Show </option>
                        <option value="6"> Canceled </option>
                    </x-select>
                    <x-input-error for="outcome" />
                </div>
            </div>
            <div class="grid grid-cols-2 gap-3">
                <div class="col-span-1">
                    <x-label for="datetime" value="Date" :required="true" />
                    <x-input type="date" name="datetime" wire:model.defer='datetime'>
                    </x-input>
                    <x-input-error for="datetime" />
                </div>
                <div class="col-span-1">

                    <x-label for="time" value="Time" :required="true" />
                    <x-input type="time" name="time" wire:model.defer='time'>
                    </x-input>
                    <x-input-error for="time" />
                </div>
            </div>
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="notes" value="Notes on the Meeting" :required="true" />
                    <x-textarea name="notes" wire:model.defer='notes'></x-textarea>
                    <x-input-error for="notes" />
                </div>
            </div>
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="sr_no" value="Associate With" />
                    <x-input type="text" name="sr_no" wire:model.defer='sr_no'></x-input>
                    <x-input-error for="sr_no" />
                </div>
            </div>

        </div>
        <div class="flex justify-end mt-8 space-x-6">
            {{-- <button type="button" wire:click="closecreatemodal"
                class="px-6 py-2 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                Cancel
            </button>
            <button type="submit" class="px-6 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                Submit
            </button> --}}
            <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                class="px-12 bg-white text-blue hover:bg-gray-100" />

            @if ($customer_id_meeting == '' || $outcome == '' || $datetime == '' || $notes == '' || $sr_no == '')
            <x-button disabled type="submit" title="Log Meeting" class="px-12 text-white bg-gray-300 border-0 cursor-pointer hover:bg-gray-400" />
            @else
                <x-button type="submit" title="Log Meeting" class="px-12 bg-blue text-white hover:bg-[#002161]" />
            @endif
        </div>
    </form>

</div>

<script>
    function tagSelect() {
        return {
            open: false,
            textInput: '',
            tags: [],

            init() {
                this.tags = JSON.parse(this.$el.parentNode.getAttribute('data-tags'));
            },
            addTag(tag) {
                tag = tag.trim()
                if (tag != "" && !this.hasTag(tag)) {
                    this.tags.push(tag)

                    console.log(this.tags)
                    $("#asdasd").val(tag)
                }


                this.clearSearch()
                this.$refs.textInput.focus()
                this.fireTagsUpdateEvent()
            },
            fireTagsUpdateEvent() {
                this.$el.dispatchEvent(new CustomEvent('tags-update', {
                    detail: {
                        tags: this.tags
                    },
                    bubbles: true,
                }));
            },
            hasTag(tag) {
                var tag = this.tags.find(e => {
                    return e.toLowerCase() === tag.toLowerCase()
                })
                return tag != undefined
            },
            removeTag(index) {
                this.tags.splice(index, 1)
                this.fireTagsUpdateEvent()
            },
            search(q) {
                if (q.includes(",")) {
                    q.split(",").forEach(function(val) {
                        this.addTag(val)
                    }, this)
                }
                this.toggleSearch()
            },
            clearSearch() {
                this.textInput = ''
                this.toggleSearch()
            },
            toggleSearch() {
                this.open = this.textInput != ''
            }
        }
    }
</script>
