<div x-data="{
    {{-- confirmation_modal_note: '{{ $confirmation_modal_note }}', --}}
}">
    <x-loading></x-loading>

    {{-- <x-modal id="confirmation_modal_note" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal_note', false)">
                </span>
            </span>
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                Are you sure you want to submit Notes?
            </h2>


            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$set('confirmation_modal_note', false)"
                    class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                    Cancel
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Submit
                </button>
            </div>
        </x-slot>
    </x-modal> --}}

    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="mt-5 space-y-6">
            <div class="grid grid-cols-1 gap-3">
                <div x-data="{ open: false }" @click.away="open = false">
                    <div>
                        <x-label for="customer_id_notes" value="Customer Name" :required="true" />
                        <x-input class="w-full h-10 rounded-md" maxlength="50" style="cursor: pointer;" type="text"
                            name="customer_id_notes" wire:model='customer_id_notes' @click="open = !open">
                        </x-input>
                        <x-input-error for="customer_id" />

                    </div>
                    <div x-show="open" x-cloak
                        class="absolute w-11/12 overflow-hidden overflow-y-auto bg-gray-100 rounded-md shadow h-11 max-h-96"
                        style="">
                        <ul class="list-reset">
                            @foreach ($customer_nos as $i => $customer_no)
                                <li @click="open = !open" wire:key="{{ 'customer_no_2' . $i }}"
                                    wire:click="getCustomerDetailsNotes({{ $customer_no->id }})"
                                    class="px-2 text-black cursor-pointer hover:bg-gray-200">
                                    <p>
                                        {{ $customer_no->fullname }}
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="title_notes" value="Title" :required="true" />
                    <x-input type="text" name="title_notes" maxlength="50" wire:model='title_notes'></x-input>
                    <x-input-error for="title" />
                </div>
            </div>
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="notes" value="Note" :required="true" />
                    <x-textarea name="notes" maxlength="50" wire:model='notes'></x-textarea>
                    <x-input-error for="notes" />
                </div>
            </div>
            <div class="grid grid-cols-1 gap-3">
                <div>
                    <x-label for="sr_no_notes" value="Associate With" />
                    <x-input type="text" name="sr_no_notes" wire:model='sr_no_notes'></x-input>
                    <x-input-error for="sr_no" />
                </div>
            </div>

        </div>
        <div class="flex justify-end mt-8 space-x-6">
            {{-- <button type="button" wire:click="closecreatemodal"
                class="px-6 py-2 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                Cancel
            </button>
            <button type="submit" class="px-6 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                Submit
            </button> --}}

            <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                    class="px-12 bg-white text-blue hover:bg-gray-100" />

            @if($customer_id_notes == '' || $title_notes == '' || $notes == '' || $sr_no_notes == '')
            <x-button disabled type="submit" title="Create" class="px-12 text-white bg-gray-300 border-0 cursor-pointer hover:bg-gray-400" />
            @else
                <x-button type="submit" title="Create" class="px-12 bg-blue text-white hover:bg-[#002161]" />
            @endif

        </div>
    </form>

</div>
