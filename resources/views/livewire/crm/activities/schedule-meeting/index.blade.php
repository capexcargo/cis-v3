<x-form x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    {{-- create_modal_task: '{{ $create_modal_task }}',
    create_modal_note: '{{ $create_modal_note }}',
    create_modal_log_meeting: '{{ $create_modal_log_meeting }}', --}}
    {{-- edit_modal: '{{ $edit_modal }}', --}}
    {{-- delete_modal: '{{ $delete_modal }}', --}}

}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_activities_schedule_meeting_add')
            <x-modal id="create_modal" size="w-max">
                <x-slot name="title">Add Schedule</x-slot>
                <x-slot name="body">
                    @livewire('crm.activities.schedule-meeting.create')
                </x-slot>
            </x-modal>
        @endcan
    </x-slot>
    
    <x-slot name="header_title">Schedule a Meeting</x-slot>
    
    <x-slot name="header_button">
        @can('crm_activities_schedule_meeting_add')
        <button wire:click="action({}, 'add')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
            <div class="flex items-start justify-between">
                <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                    data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path fill="currentColor"
                        d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                </svg>
                Add Schedule
            </div>
        </button>
        @endcan
    </x-slot>
    
    <x-slot name="body">
        <div class="p-6 bg-white rounded-lg shadow-md">
            <div class="p-2 whitespace-nowrap" id='calendar'></div>
        </div>
    </x-slot>

</x-form>

@push('scripts')
    <script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.8/index.global.min.js'></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'dayGridMonth',
                initialDate: new Date(),
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay'
                },
                // dateClick: function(info) {
                //     console.log(info);
                //         $('eventModal').modal('show');
                // }
            });
            calendar.render();
        });
    </script>
@endpush
