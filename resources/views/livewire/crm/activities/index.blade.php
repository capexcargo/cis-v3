<x-form x-data="{
    search_form: false,
    create_modal_task: '{{ $create_modal_task }}',
    create_modal_note: '{{ $create_modal_note }}',
    create_modal_log_meeting: '{{ $create_modal_log_meeting }}',
    {{-- edit_modal: '{{ $edit_modal }}', --}}
    {{-- delete_modal: '{{ $delete_modal }}', --}}

}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_activities_task_add')
            <x-modal id="create_modal_task" size="w-1/3">
                <x-slot name="title">Create a Task</x-slot>
                <x-slot name="body">
                    @livewire('crm.activities.task.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('crm_activities_notes_add')
            <x-modal id="create_modal_note" size="w-1/3">
                <x-slot name="title">Create A Note</x-slot>
                <x-slot name="body">
                    @livewire('crm.activities.notes.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('crm_activities_log_meeting_add')
            <x-modal id="create_modal_log_meeting" size="w-1/3">
                <x-slot name="title">Log a Meeting</x-slot>
                <x-slot name="body">
                    @livewire('crm.activities.log-meeting.create')
                </x-slot>
            </x-modal>
        @endcan
    </x-slot>

    <x-slot name="header_title">Activities</x-slot>
    <x-slot name="header_button">
        @can('crm_activities_add')
            <div class="text-blue whitespace-nowrap" x-data="{ open: false }">
                <button @click="open = !open" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                    <div class="flex items-start justify-between">
                        Add Activity
                        <svg class="w-3 h-3 mt-1 ml-2" aria-hidden="true" focusable="false" data-prefix="far"
                            data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                            <path fill="currentColor"
                                d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                        </svg>
                    </div>
                </button>

                <div class="absolute border rounded" style="margin-left: -3.5rem; margin-top:0.8%;">
                    <ul class="text-gray-600 bg-white rounded shadow" x-show="open" @click.away="open = false">
                        @can('crm_activities_task_add')
                            <li style="cursor:pointer" class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                    wire:click="action({}, 'add')">Create a
                                    Task</a>
                            </li>
                        @endcan
                        @can('crm_activities_notes_add')
                            <li style="cursor:pointer" class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                    wire:click="action({}, 'add_note')">Create a Note</a>
                            </li>
                        @endcan
                        <li style="cursor:pointer" class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                wire:click="action({},''}">Create an Email</a>
                        </li>
                        {{-- <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                wire:click="action({},''}">Quotation</a>
                        </li> --}}
                        @can('crm_activities_log_meeting_add')
                            <li style="cursor:pointer" class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                    wire:click="action({},'add_meeting')">Log
                                    a Meeting</a>
                            </li>
                        @endcan
                        <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                            wire:click="action({},'schedule_meeting')">Schedule a Meeting</a>
                        </li>
                    </ul>
                </div>
            </div>
        @endcan
    </x-slot>

    <x-slot name="body">

        <div class="flex gap-4">
            <button disabled wire:click="cardHeader(1)"
                class="
                {{-- {{ $card_header == 1 ? 'border-2' : '' }} --}}
                 flex justify-between items-center gap-12 px-4 py-4 bg-white border border-[#003399] rounded-lg">
                <h5 class="font-medium text-gray-700 uppercase dark:text-gray-40">ALL ACTIVITIES</h5>
                <h5 class="text-3xl font-medium text-gray-700 dark:text-gray-400">50
                    {{-- {{ $all_sr }} --}}
                </h5>
            </button>
            <button disabled wire:click="cardHeader(2)"
                class="
                {{-- {{ $card_header == 2 ? 'border-2' : '' }} --}}
                 flex justify-between items-center gap-12 px-4 py-4 bg-white border border-[#003399] rounded-lg ">
                <h5 class="font-medium text-gray-700 uppercase dark:text-gray-400">MY TEAM'S<br>ACTIVITIES</h5>
                <h5 class="text-3xl font-medium text-gray-700 dark:text-gray-400">25
                    {{-- {{ $closed_sr }} --}}
                </h5>
            </button>
            <button disabled wire:click="cardHeader(3)"
                class="
                {{-- {{ $card_header == 3 ? 'border-2' : '' }}  --}}
                flex justify-between items-center gap-10 px-4 py-4 bg-white border border-[#003399] rounded-lg ">
                <h5 class="font-medium text-left text-gray-700 uppercase dark:text-gray-400">MY ACTIVITIES</h5>
                <h5 class="text-3xl font-medium text-gray-700 dark:text-gray-400">25
                    {{-- {{ $teams_sr }} --}}
                </h5>
            </button>
        </div>

        <div class="grid grid-cols-6 gap-4 text-sm " style="margin-top: 2%;">
            <div class="w-5/6">
                {{-- <div>
                    <x-transparent.input type="date" label="Booking Date" name="name"
                        wire:model.defer="created_at" />
                </div> --}}
                <div>
                    <x-transparent.input style="font-size: 14px;" type="text" onfocus="(this.type='date')" onblur="(this.type='text')"
                        label="" placeholder="Booking Date" name="name" wire:model.defer="created_at" />
                </div>
            </div>

            <div class="w-5/6" wire:init="">
                <x-transparent.select value="" label="Employee Name" name=""
                    wire:model.defer="">
                    <option value=""></option>
                    {{-- @foreach ($final_referencesbk as $final_reference)
                        <option value="{{ $final_reference->id }}">
                            {{ $final_reference->name }}
                        </option>
                    @endforeach --}}
                </x-transparent.select>
            </div>

            <div class="w-5/6" wire:init="">
                <x-transparent.select value="" label="Activity Type" name=""
                    wire:model.defer="">
                    <option value=""></option>
                    {{-- @foreach ($bookingbranch_referencesbk as $bookingbranch_reference)
                        <option value="{{ $bookingbranch_reference->id }}">
                            {{ $bookingbranch_reference->code }}
                        </option>
                    @endforeach --}}
                </x-transparent.select>
            </div>

            <div class="w-5/6" wire:init="">
                
            </div>

            <div class="w-5/6" wire:init="">
                
            </div>
            <div class="w-5/6" wire:init="">
                
            </div>

            <div class="w-5/6" wire:init="">
                <x-transparent.select value="" label="Customer Name" name=""
                    wire:model.defer="">
                    <option value=""></option>
                    {{-- @foreach ($bookingbranch_referencesbk as $bookingbranch_reference)
                        <option value="{{ $bookingbranch_reference->id }}">
                            {{ $bookingbranch_reference->code }}
                        </option>
                    @endforeach --}}
                </x-transparent.select>
            </div>

            <div class="w-5/6" wire:init="">
                <x-transparent.select value="" label="SR Number" name=""
                    wire:model.defer="">
                    <option value=""></option>
                    {{-- @foreach ($bookingbranch_referencesbk as $bookingbranch_reference)
                        <option value="{{ $bookingbranch_reference->id }}">
                            {{ $bookingbranch_reference->code }}
                        </option>
                    @endforeach --}}
                </x-transparent.select>
            </div>

            <div class="w-5/6" wire:init="">
                <x-transparent.select value="" label="Quotation Ref. No" name=""
                    wire:model.defer="">
                    <option value=""></option>
                    {{-- @foreach ($bookingbranch_referencesbk as $bookingbranch_reference)
                        <option value="{{ $bookingbranch_reference->id }}">
                            {{ $bookingbranch_reference->code }}
                        </option>
                    @endforeach --}}
                </x-transparent.select>
            </div>

            <div class="">
                <x-button type="button" wire:click="search" title="Search"
                    class="px-3 py-1.5 text-white bg-blue hover:bg-blue-800" />
            </div>
        </div>

        <div class="py-12">
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="No." style="padding-left:2%;" />
                        <x-table.th name="Date Created" style="padding-left:;" />
                        <x-table.th name="Employee Name" style="padding-left:;" />
                        <x-table.th name="Activity Type" style="padding-left:;" />
                        <x-table.th name="Customer Name" style="padding-left:;" />
                        <x-table.th name="Description" style="padding-left:;" />
                        <x-table.th name="SR Number" style="padding-left:;" />
                        <x-table.th name="Quotation Ref. No" style="padding-left:;" />
                        <x-table.th name="Status" style="padding-left:;" />
                        <x-table.th name="Date & Time Completed" style="padding-left:;" />
                        <x-table.th name="Action" style="padding-left:;" />
                    </x-slot>
                    <x-slot name="tbody">
                        {{-- @foreach ($req_s as $i => $req) --}}
                            <tr class="font-semibold border-0 hover:text-white">
                                <td class="w-24 p-3 whitespace-nowrap" style="padding-left:2%;">
                                    <p class="w-24">
                                        {{-- {{ $i + 1 }}. --}}
                                    </p>
                                </td>

                                <td class="w-1/4 pb-3 whitespace-pre-line" style="padding-left:;">
                                    {{-- {{ $req->name }} --}}
                                </td>

                                <td class="w-1/4 pb-3 whitespace-pre-line" style="padding-left:;">
                                    {{-- {{ $req->name }} --}}
                                </td>

                                <td class="w-1/4 pb-3 whitespace-pre-line" style="padding-left:;">
                                    {{-- {{ $req->name }} --}}
                                </td>

                                <td class="w-1/4 pb-3 whitespace-pre-line" style="padding-left:;">
                                    {{-- {{ $req->name }} --}}
                                </td>

                                <td class="w-1/4 pb-3 whitespace-pre-line" style="padding-left:;">
                                    {{-- {{ $req->name }} --}}
                                </td>

                                <td class="w-1/4 pb-3 whitespace-pre-line" style="padding-left:;">
                                    {{-- {{ $req->name }} --}}
                                </td>

                                <td class="w-1/4 pb-3 whitespace-pre-line" style="padding-left:;">
                                    {{-- {{ $req->name }} --}}
                                </td>

                                <td class="w-1/4 pb-3 whitespace-pre-line" style="padding-left:;">
                                    {{-- {{ $req->name }} --}}
                                </td>

                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{-- <span
                                        class="{{ $req->status == 1
                                            ? 'text-green bg-green-100 px-8'
                                            : ($req->status == 2
                                                ? 'text-red bg-red-100 px-8'
                                                : '') }} 
                                                        text-xs rounded-full p-1">
                                        {{ $req->status == 1 ? 'Active' : 'Inactive' ?? '' }}
                                    </span> --}}
                                </td>

                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{-- <div class="flex space-x-3">
                                        @can('crm_contracts_requirements_mgmt_edit')
                                            <svg wire:click="action({'id': {{ $req->id }}}, 'edit')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                                </path>
                                            </svg>
                                        @endcan
                                        @can('crm_contracts_requirements_mgmt_delete')
                                            <svg wire:click="action({'id': {{ $req->id }}}, 'delete')"
                                                class="w-5 h-5 text-red" aria-hidden="true" focusable="false"
                                                data-prefix="fas" data-icon="trash-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M160 400C160 408.8 152.8 416 144 416C135.2 416 128 408.8 128 400V192C128 183.2 135.2 176 144 176C152.8 176 160 183.2 160 192V400zM240 400C240 408.8 232.8 416 224 416C215.2 416 208 408.8 208 400V192C208 183.2 215.2 176 224 176C232.8 176 240 183.2 240 192V400zM320 400C320 408.8 312.8 416 304 416C295.2 416 288 408.8 288 400V192C288 183.2 295.2 176 304 176C312.8 176 320 183.2 320 192V400zM317.5 24.94L354.2 80H424C437.3 80 448 90.75 448 104C448 117.3 437.3 128 424 128H416V432C416 476.2 380.2 512 336 512H112C67.82 512 32 476.2 32 432V128H24C10.75 128 0 117.3 0 104C0 90.75 10.75 80 24 80H93.82L130.5 24.94C140.9 9.357 158.4 0 177.1 0H270.9C289.6 0 307.1 9.358 317.5 24.94H317.5zM151.5 80H296.5L277.5 51.56C276 49.34 273.5 48 270.9 48H177.1C174.5 48 171.1 49.34 170.5 51.56L151.5 80zM80 432C80 449.7 94.33 464 112 464H336C353.7 464 368 449.7 368 432V128H80V432z">
                                                </path>
                                            </svg>
                                        @endcan
                                    </div> --}}
                                </td>
                            </tr>
                        {{-- @endforeach --}}
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{-- {{ $status_s->links() }} --}}
                </div>
            </div>
        </div>
    
    </x-slot>
</x-form>
