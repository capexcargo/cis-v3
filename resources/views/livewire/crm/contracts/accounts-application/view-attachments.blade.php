<div x-data="{
    search_form: false,
    view_image_modal: '{{ $view_image_modal }}',
    delete_modal: '{{ $delete_modal }}',
}">
    <x-loading />

    @can('crm_contracts_accounts_application_view_image')
        @if ($img_id && $view_image_modal)
            <x-modal id="view_image_modal" size="w-7/12">
                <x-slot name="title">View Image Attached</x-slot>
                <x-slot name="body">
                    @livewire('crm.contracts.accounts-application.view-image', ['id' => $img_id])
                </x-slot>
            </x-modal>
        @endif
    @endcan

    @can('crm_contracts_accounts_application_delete')
        <x-modal id="delete_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
            <x-slot name="body">
                <h2 class="mb-3 text-lg text-center text-gray-900">
                    {{ $confirmation_message }}
                </h2>
                <div class="flex justify-center space-x-3">
                    <button type="button" wire:click="$set('delete_modal', false)"
                        class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                    <button type="button" wire:click="confirm"
                        class="flex-none px-5 py-1 mt-4 text-sm text-white rounded-lg bg-blue">
                        Yes</button>
                </div>
            </x-slot>
        </x-modal>
    @endcan

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
        <x-slot name="body">
            <div class="space-y-3">
                <p class="text-center">{{ $confirmation_message }}</p>
                <div class="flex items-center justify-center space-x-3">
                    <x-button type="button" wire:click="$set('confirmation_modal', false)" title="No"
                        class="py-1 bg-white text-blue hover:bg-gray-100" />
                    <x-button type="button" wire:click="confirm" title="Yes"
                        class="bg-blue text-white hover:bg-[#002161] py-1" />
                </div>
            </div>
        </x-slot>
    </x-modal>

    <div class="grid grid-cols-12 gap-4 p-2">
        <div class="col-span-12">
            <div class="text-3xl font-medium text-blue whitespace-nowrap">{{ $fullname }}</div>
        </div>

        <div class="col-span-6">
            <table class="p-4">
                <tr class="p-6">
                    <td class="text-base font-medium text-gray-400">
                        Customer Number :
                    </td>
                    <td class="pl-4 font-semibold text-right">
                        {{ $accno }}
                    </td>
                </tr>
                <tr class="p-6">
                    <td class="text-base font-medium text-gray-400">
                        Industry :
                    </td>
                    <td class="pl-4 font-semibold text-right">
                        {{ $industry }}
                    </td>
                </tr>
                <tr class="p-6">
                    <td class="text-base font-medium text-gray-400">
                        Accounts Application No. :
                    </td>
                    <td class="pl-4 font-semibold text-right ">
                        {{ $accno }}
                    </td>
                </tr>
                <tr class="p-6">
                    <td class="text-base font-medium text-gray-400">
                        Primary Contact Person :
                    </td>
                    <td class="pl-4 font-semibold text-right ">
                        {{ $cfname }} {{ $cmname }} {{ $clname }}
                    </td>
                </tr>
                <tr class="p-6">
                    <td class="text-base font-medium text-gray-400">
                        Primary Contact Number :
                    </td>
                    <td class="pl-4 font-semibold text-right ">
                        {{ $cpnum }}
                    </td>
                </tr>
            </table>
        </div>

        <div class="col-span-6 pl-8">
            <table class="p-4">
                <tr class="p-6">
                    <td class="text-base font-medium text-gray-400">
                        TIN :
                    </td>
                    <td class="pl-4 font-semibold text-right">
                        {{ $tin }}
                    </td>
                </tr>
                <tr class="p-6">
                    <td class="text-base font-medium text-gray-400">
                        Date Onboarded :
                    </td>
                    <td class="pl-4 font-semibold text-right">
                        {{ date('M d,Y', strtotime($createdat)) }}
                    </td>
                </tr>
                <tr class="p-6">
                    <td class="text-base font-medium text-gray-400">
                        Payment Term :
                    </td>
                    <td class="pl-4 font-semibold text-right ">
                        -
                    </td>
                </tr>
                <tr class="p-6">
                    <td class="text-base font-medium text-gray-400">
                        Credit Limit :
                    </td>
                    <td class="pl-4 font-semibold text-right ">
                        -
                    </td>
                </tr>
                <tr class="p-6">
                    <td class="text-base font-medium text-gray-400">
                        Ageing :
                    </td>
                    <td class="pl-4 font-semibold text-right ">
                        -
                    </td>
                </tr>
            </table>
        </div>

        <div class="col-span-12">
            <x-table.table class="overflow-hidden">
                <x-slot name="thead">
                    <x-table.th class="w-[20%]" name="No" />
                    <x-table.th class="w-[20%]" name="Files" />
                    <x-table.th class="w-[10%]" style="padding-left: 10%" name="Attachments" />
                </x-slot>
                <x-slot name="tbody" class="border">
                    @foreach ($reqmgmts as $i => $reqmgmt)
                        <tr class="cursor-pointer ">
                            <td class="p-3 whitespace-nowrap">
                                {{ $i + 1 }}.
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{ $reqmgmt->name }}
                            </td>

                            <td class="p-3 whitespace-nowrap">
                                <div class="flex items-center justify-start space-x-3">
                                    <div class="flex flex-col items-start justify-start w-full text-1xs">
                                        <button
                                            class="flex justify-start items-start relative px-2 py-1 mx-auto text-black border border-[#003399] bg-white hover:bg-black-100 rounded-full cursor-pointer">
                                            <input type="file" name="applications.{{ $i }}.file"
                                                wire:model="applications.{{ $i }}.file"
                                                class="absolute w-full h-full opacity-0  cursor-pointer text-[0px]"
                                                wire:click="setid({{ $reqmgmt->id }})">
                                            <svg class="w-3 h-3 mr-2" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                                            </svg>
                                            <span> Add Attachment</span>
                                        </button>
                                        <x-input-error for="applications.{{ $i }}.file" />
                                    </div>
                                    <div class="flex w-16 space-x-3">
                                        @if (isset($reqmgmt->appRequirements->application_requirement_id))
                                        {{-- <p>{{ $reqmgmt->appRequirements->application_requirement_id ?? ''}}</p> --}}
                                        <svg wire:ignore wire:click="action({id: {{ $reqmgmt->id }}}, 'view-image')"
                                            data-toggle="tooltip" title="View"
                                            class="w-4 h-4 text-gray-300 hover:text-blue-900"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                        </svg>
                                        <svg wire:click="action({id: {{ $reqmgmt->id }}}, 'delete')"
                                            data-toggle="tooltip" title="Delete"
                                            class="w-4 h-4 text-gray-300 hover:text-red-500"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M160 400C160 408.8 152.8 416 144 416C135.2 416 128 408.8 128 400V192C128 183.2 135.2 176 144 176C152.8 176 160 183.2 160 192V400zM240 400C240 408.8 232.8 416 224 416C215.2 416 208 408.8 208 400V192C208 183.2 215.2 176 224 176C232.8 176 240 183.2 240 192V400zM320 400C320 408.8 312.8 416 304 416C295.2 416 288 408.8 288 400V192C288 183.2 295.2 176 304 176C312.8 176 320 183.2 320 192V400zM317.5 24.94L354.2 80H424C437.3 80 448 90.75 448 104C448 117.3 437.3 128 424 128H416V432C416 476.2 380.2 512 336 512H112C67.82 512 32 476.2 32 432V128H24C10.75 128 0 117.3 0 104C0 90.75 10.75 80 24 80H93.82L130.5 24.94C140.9 9.357 158.4 0 177.1 0H270.9C289.6 0 307.1 9.358 317.5 24.94H317.5zM151.5 80H296.5L277.5 51.56C276 49.34 273.5 48 270.9 48H177.1C174.5 48 171.1 49.34 170.5 51.56L151.5 80zM80 432C80 449.7 94.33 464 112 464H336C353.7 464 368 449.7 368 432V128H80V432z" />
                                        </svg>
                                        @endif


                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </x-slot>
            </x-table.table>
        </div>

    </div>
</div>
