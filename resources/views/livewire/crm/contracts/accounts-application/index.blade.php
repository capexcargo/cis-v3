<x-form wire:init="load" x-data="{
    search_form: false,
    reactivate_modal: '{{ $reactivate_modal }}',
    deactivate_modal: '{{ $deactivate_modal }}',
    approve1_modal: '{{ $approve1_modal }}',
    decline1_modal: '{{ $decline1_modal }}',
    approve2_modal: '{{ $approve2_modal }}',
    decline2_modal: '{{ $decline2_modal }}',
    approve3_modal: '{{ $approve3_modal }}',
    decline3_modal: '{{ $decline3_modal }}',
    approve4_modal: '{{ $approve4_modal }}',
    decline4_modal: '{{ $decline4_modal }}',
    view_modal: '{{ $view_modal }}',
    view_attachments_modal: '{{ $view_attachments_modal }}',

}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        {{-- @can('crm_sales_booking_mgmt_add')
            <x-modal id="create_modal" size="w-3/4">
                <x-slot name="body">
                    @livewire('crm.sales.booking-mgmt.create')
                </x-slot>
            </x-modal>
        @endcan --}}

        @can('crm_contracts_accounts_application_view_attachments')
        @if ($acc_id && $view_attachments_modal)
            <x-modal id="view_attachments_modal" size="w-7/12">
                {{-- <x-slot name="title">View Customer</x-slot> --}}
                <x-slot name="body">
                    @livewire('crm.contracts.accounts-application.view-attachments', ['id' => $acc_id])
                </x-slot>
            </x-modal>
        @endif
        @endcan

        @can('crm_contracts_accounts_application_view')
        @if ($acc_id && $view_modal)
            <x-modal id="view_modal" size="w-4/5">
                {{-- <x-slot name="title">View Customer</x-slot> --}}
                <x-slot name="body">
                    @livewire('crm.contracts.accounts-application.view', ['id' => $acc_id])
                </x-slot>
            </x-modal>
        @endif
        @endcan


        @can('crm_contracts_accounts_application_deactivate')
            <x-modal id="reactivate_modal" size="w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-blue">Reactivate</span>
                            this Accounts Application?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('reactivate_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateStatus('{{ $acc_id }}', 1)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#003399] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="deactivate_modal" size="w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-red">Deactivate</span>
                            this Accounts Application?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('deactivate_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateStatus('{{ $acc_id }}', 2)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#CC0000] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan

        @can('crm_contracts_accounts_application_approver_1')
            <x-modal id="approve1_modal" size="w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-blue">Approve</span>
                            this Account?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('approve1_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateApp1('{{ $acc_id }}', 1)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#003399] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="decline1_modal" size="w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-red">Decline</span>
                            this Account?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('decline1_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateApp1('{{ $acc_id }}', 2)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#CC0000] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan

        @can('crm_contracts_accounts_application_approver_2')
            <x-modal id="approve2_modal" size="w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-blue">Approve</span>
                            this Account?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('approve2_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateApp2('{{ $acc_id }}', 1)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#003399] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="decline2_modal" size="w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-red">Decline</span>
                            this Account?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('decline2_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateApp2('{{ $acc_id }}', 2)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#CC0000] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan

        @can('crm_contracts_accounts_application_approver_3')
            <x-modal id="approve3_modal" size="w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-blue">Approve</span>
                            this Account?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('approve3_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateApp3('{{ $acc_id }}', 1)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#003399] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="decline3_modal" size="w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-red">Decline</span>
                            this Account?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('decline3_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateApp3('{{ $acc_id }}', 2)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#CC0000] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan

        @can('crm_contracts_accounts_application_approver_4')
            <x-modal id="approve4_modal" size="w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-blue">Approve</span>
                            this Account?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('approve4_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateApp4('{{ $acc_id }}', 1)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#003399] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="decline4_modal" size="w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-red">Decline</span>
                            this Account?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('decline4_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateApp4('{{ $acc_id }}', 2)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#CC0000] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan

    </x-slot>

    <x-slot name="header_title">Accounts Application</x-slot>
    <x-slot name="header_button">
    </x-slot>
    <x-slot name="body">
        <div class="grid grid-cols-6 gap-4 ">
            <button wire:click="redirectTo({}, 'redirectToAFMgmt')"
            class="p-4 text-gray-700 bg-white rounded-md w-45 hover:bg-blue-100">
            <div class="grid grid-cols-6">
                <div class="col-span-4">
                    <span class="inline-block text-base font-semibold text-left">Total No of Applications</span>
                </div>
                <div class="h-10 col-span-2 mt-2">
                    <span class="inline-block h-10 text-3xl font-medium text-left">{{$countall}}</span>
                </div>
            </div>
            </button>
        </div>

        <div class="grid grid-cols-6 gap-4 text-sm " style="margin-top: 4%;">
            <div class="w-11/12">
                <div>
                    <x-transparent.input type="text" onfocus="(this.type='date')" onblur="(this.type='text')"
                        label="" placeholder="Date" name="name" wire:model.defer="created_at" />
                </div>
            </div>

            <div class="w-11/12">
                <div>
                    <x-transparent.input type="text" label="Customer No" name="name"
                        wire:model.defer="account_no" />
                </div>
            </div>

            <div class="w-11/12">
                <div>
                    <x-transparent.input type="text" label="Company Name" name="name"
                        wire:model.defer="company_name" />
                </div>
            </div>

            <div class="">
                <x-button type="button" wire:click="search" title="Search"
                    class="px-3 py-1.5 text-white bg-blue hover:bg-blue-800" />
            </div>
        </div>



        <div class="">
            <div class="flex items-center justify-start">
                <ul class="flex mt-8">
                    @foreach ($status_header_cards as $i => $status_card)
                        {{-- <li class="px-3 py-1 text-sm {{ $status_header_cards[$i]['class'] }}"
                            :class="$wire.{{ $status_header_cards[$i]['action'] }} ==
                                '{{ $status_header_cards[$i]['id'] }}' ?
                                'bg-blue text-white' : ''"> --}}
                        <li
                            class="px-3 py-1 text-xs {{ $status_header_cards[$i]['class'] }} {{ $stats == $status_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                            <button
                                wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                {{ $status_header_cards[$i]['title'] }}
                                <span class="ml-6 text-sm">{{ $status_header_cards[$i]['value'] }}</span>
                            </button>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="px-2 bg-white rounded-lg shadow-md">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="No." style="padding-left:%;" />
                        <x-table.th name="Account" style="padding-left:%;" />
                        <x-table.th name="Company Name" style="padding-left:%;" />
                        <x-table.th name="Company Email Address" style="padding-left:%;" />
                        <x-table.th name="Contact Owner" style="padding-left:%;" />
                        <x-table.th name="Date of" style="padding-left:%;" />
                        <x-table.th name="Accounts Application" style="padding-left:%;" />
                        <x-table.th name="Date Approved/" style="padding-left:%;" />
                        <x-table.th name="Approver 1" style="padding-left:%;" />
                        <x-table.th name="Approver 2" style="padding-left:%;" />
                        <x-table.th name="Approver 3" style="padding-left:%;" />
                        <x-table.th name="Approver 4" style="padding-left:%;" />
                        <x-table.th name="Account Status" style="padding-left:%;" />
                        <x-table.th name="Actions" style="padding-left:%;" />
                        <x-table.th name="Remarks" style="padding-left:%;" />
                        <tr>
                            <x-table.th name="" style="padding-left:%;" />
                            <x-table.th name="Application No." style="padding-left:%;" />
                            <x-table.th name="" style="padding-left:%;" />
                            <x-table.th name="" style="padding-left:%;" />
                            <x-table.th name="" style="padding-left:%;" />
                            <x-table.th name="Accounts Application" style="padding-left:%;" />
                            <x-table.th name="Status" style="padding-left:%;" />
                            <x-table.th name="Declined" style="padding-left:%;" />
                            <x-table.th name="" style="padding-left:%;" />
                            <x-table.th name="" style="padding-left:%;" />
                            <x-table.th name="" style="padding-left:%;" />
                            <x-table.th name="" style="padding-left:%;" />
                            <x-table.th name="" style="padding-left:%;" />
                            <x-table.th name="" style="padding-left:%;" />
                            <x-table.th name="" style="padding-left:%;" />
                        </tr>
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($acc_s as $i => $acc)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-semibold">
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    <p class="w-1/4">
                                        {{ ($acc_s->currentPage() - 1) * $acc_s->links()->paginator->perPage() + $loop->iteration }}.
                                    </p>
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    <p class="underline text-blue"
                                        wire:click="action({'id': {{ $acc->id }}}, 'view_attachments')">
                                        {{ $acc->account_no }}
                                    </p>
                                </td>

                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    <p class="underline text-blue"
                                        wire:click="action({'id': {{ $acc->id }}}, 'view')">
                                        {{ $acc->company_name }}
                                    </p>

                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    @if ($acc->company_email_address == null)
                                        <p class="text-red">-</p>
                                    @else
                                        {{ $acc->company_email_address }}
                                    @endif
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $acc->contactOwner->name }}
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ date('M d,Y', strtotime($acc->created_at)) }}
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    <span
                                        style="{{ $acc->final_status == 2
                                            ? 'color:red; background-color:#ffe6e6; padding-left:20px; padding-right:20px; padding-top:5px; padding-bottom:5px;'
                                            : ($acc->final_status == 1
                                                ? 'color:darkblue; background-color:#ACBDDD; padding-left:20px; padding-right:20px; padding-top:5px; padding-bottom:5px;'
                                                : ($acc->final_status == null
                                                    ? 'color:orange; background-color:#fff1cf; padding-left:20px; padding-right:20px; padding-top:5px; padding-bottom:5px;'
                                                    : '')) }} 
                                    border-radius:25px; font-size:13px;">
                                        {{ $acc->final_status == 1 ? 'Approved' : ($acc->final_status == 2 ? 'Declined' : 'For Approval') }}</span>
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    @if ($acc->approval_date == null)
                                        <p class="pl-6 text-red">-</p>
                                    @else
                                        {{ date('M d,Y', strtotime($acc->approval_date)) }}
                                    @endif
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    @if ($acc->approver1_status == null)
                                        <div class="flex gap-2">
                                            @can('crm_contracts_accounts_application_approver_1')
                                                <div class="">
                                                    <svg wire:click="action({'id':{{ $acc->id }}, 'approver1_status': 1},'update_app1') }}"
                                                        class="w-6 h-5 text-blue" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="trash-alt" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                                                        </path>
                                                    </svg>
                                                </div>
                                                <div class="" style="">
                                                    <svg wire:click="action({'id':{{ $acc->id }}, 'approver1_status': 2},'update_app1') }}"
                                                        class="w-6 h-5 text-red" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="circle-minus" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM175 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z">
                                                        </path>
                                                    </svg>
                                                </div>
                                            @endcan
                                        </div>
                                    @else
                                        <span
                                            style="{{ $acc->approver1_status == 2
                                                ? 'color:red; background-color:#ffe6e6; padding-left:20px; padding-right:20px; padding-top:5px; padding-bottom:5px;'
                                                : ($acc->approver1_status == 1
                                                    ? 'color:darkblue; background-color:#ACBDDD; padding-left:20px; padding-right:20px; padding-top:5px; padding-bottom:5px;'
                                                    : '') }} 
                                                        border-radius:25px; font-size:13px;">
                                            {{ $acc->approver1_status == 1 ? 'Approved' : 'Declined' }}
                                        </span>
                                    @endif
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    @if ($acc->approver1_status == 1)
                                        @if ($acc->approver2_status == null)
                                            <div class="flex gap-2">
                                                @can('crm_contracts_accounts_application_approver_2')
                                                    <div class="">
                                                        <svg wire:click="action({'id':{{ $acc->id }}, 'approver2_status': 1},'update_app2') }}"
                                                            class="w-6 h-5 text-blue" aria-hidden="true"
                                                            focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                            role="img" xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 448 512">
                                                            <path fill="currentColor"
                                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                                                            </path>
                                                        </svg>
                                                    </div>
                                                    <div class="" style="">
                                                        <svg wire:click="action({'id':{{ $acc->id }}, 'approver2_status': 2},'update_app2') }}"
                                                            class="w-6 h-5 text-red" aria-hidden="true" focusable="false"
                                                            data-prefix="fas" data-icon="circle-minus" role="img"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                            <path fill="currentColor"
                                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM175 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z">
                                                            </path>
                                                        </svg>
                                                    </div>
                                                @endcan
                                            </div>
                                        @else
                                            <span
                                                style="{{ $acc->approver2_status == 2
                                                    ? 'color:red; background-color:#ffe6e6; padding-left:20px; padding-right:20px; padding-top:5px; padding-bottom:5px;'
                                                    : ($acc->approver2_status == 1
                                                        ? 'color:darkblue; background-color:#ACBDDD; padding-left:20px; padding-right:20px; padding-top:5px; padding-bottom:5px;'
                                                        : '') }} 
                                                        border-radius:25px; font-size:13px;">
                                                {{ $acc->approver2_status == 1 ? 'Approved' : 'Declined' }}
                                            </span>
                                        @endif
                                    @else
                                        <p class="text-yellow">Pending</p>
                                    @endif
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    @if ($acc->approver2_status == 1)
                                        @if ($acc->approver3_status == null)
                                            <div class="flex gap-2">
                                                @can('crm_contracts_accounts_application_approver_3')
                                                    <div class="">
                                                        <svg wire:click="action({'id':{{ $acc->id }}, 'approver3_status': 1},'update_app3') }}"
                                                            class="w-6 h-5 text-blue" aria-hidden="true"
                                                            focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                            role="img" xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 448 512">
                                                            <path fill="currentColor"
                                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                                                            </path>
                                                        </svg>
                                                    </div>
                                                    <div class="" style="">
                                                        <svg wire:click="action({'id':{{ $acc->id }}, 'approver3_status': 2},'update_app3') }}"
                                                            class="w-6 h-5 text-red" aria-hidden="true" focusable="false"
                                                            data-prefix="fas" data-icon="circle-minus" role="img"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                            <path fill="currentColor"
                                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM175 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z">
                                                            </path>
                                                        </svg>
                                                    </div>
                                                @endcan
                                            </div>
                                        @else
                                            <span
                                                style="{{ $acc->approver3_status == 2
                                                    ? 'color:red; background-color:#ffe6e6; padding-left:20px; padding-right:20px; padding-top:5px; padding-bottom:5px;'
                                                    : ($acc->approver3_status == 1
                                                        ? 'color:darkblue; background-color:#ACBDDD; padding-left:20px; padding-right:20px; padding-top:5px; padding-bottom:5px;'
                                                        : '') }} 
                                                    border-radius:25px; font-size:13px;">
                                                {{ $acc->approver3_status == 1 ? 'Approved' : 'Declined' }}
                                            </span>
                                        @endif
                                    @else
                                        <p class="text-yellow">Pending</p>
                                    @endif
                                </td>
                                <td class="p-3 whitespace-nowrap" style="padding-left:;">
                                    @if ($acc->approver2_status == 1)
                                        @if ($acc->approver4_status == null)
                                            <div class="flex gap-2">
                                                @can('crm_contracts_accounts_application_approver_4')
                                                    <div class="">
                                                        <svg wire:click="action({'id':{{ $acc->id }}, 'approver4_status': 1},'update_app4') }}"
                                                            class="w-6 h-5 ml-2 text-blue" aria-hidden="true"
                                                            focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                            role="img" xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 448 512">
                                                            <path fill="currentColor"
                                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                                                            </path>
                                                        </svg>
                                                    </div>
                                                    <div class="" style="">
                                                        <svg wire:click="action({'id':{{ $acc->id }}, 'approver4_status': 2},'update_app4') }}"
                                                            class="w-6 h-5 text-red" aria-hidden="true" focusable="false"
                                                            data-prefix="fas" data-icon="circle-minus" role="img"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                            <path fill="currentColor"
                                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM175 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z">
                                                            </path>
                                                        </svg>
                                                    </div>
                                                @endcan
                                            </div>
                                        @else
                                            <span
                                                style="{{ $acc->approver4_status == 2
                                                    ? 'color:red; background-color:#ffe6e6; padding-left:20px; padding-right:20px; padding-top:5px; padding-bottom:5px;'
                                                    : ($acc->approver4_status == 1
                                                        ? 'color:darkblue; background-color:#ACBDDD; padding-left:20px; padding-right:20px; padding-top:5px; padding-bottom:5px;'
                                                        : '') }} 
                                                border-radius:25px; font-size:13px;">
                                                {{ $acc->approver4_status == 1 ? 'Approved' : 'Declined' }}
                                            </span>
                                        @endif
                                    @else
                                        <p class="text-yellow">Pending</p>
                                    @endif
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    <span
                                        class="{{ $acc->status == 2
                                            ? 'text-red text-sm bg-red-100 px-8'
                                            : ($acc->status == 1
                                                ? 'text-green text-sm bg-green-100 px-8'
                                                : '') }} 
                                    text-xs rounded-full p-1">
                                        {{ $acc->status == 1 ? 'Active' : 'Deactivated' }}</span>
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                    @can('crm_contracts_accounts_application_deactivate')
                                        <svg x-cloak x-show="'{{ $acc->status == 1 }}'"
                                            wire:click="action({'id': {{ $acc->id }}, 'status': 2}, 'update_status')"
                                            class="h-8 ml-2 w-11" style="color: #18EB00;" aria-hidden="true"
                                            focusable="false" data-prefix="fas" data-icon="user-slash" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                            <path fill="currentColor"
                                                d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                            </path>
                                        </svg>
                                        <svg x-cloak x-show="'{{ $acc->status == 2 }}'"
                                            wire:click="action({'id': {{ $acc->id }}, 'status': 1}, 'update_status')"
                                            class="h-8 w-11 " style="color: #FF0000;" aria-hidden=" true"
                                            focusable="false" data-prefix="fas" data-icon="user" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z">
                                            </path>
                                        </svg>
                                    @endcan
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap"
                                    style="padding-left:;"x-data="{ open: false }">
                                    <svg @click="open = !open"
                                        class="w-5 h-5 cursor-pointer text-grey hover:text-blue-800 "
                                        data-prefix="far" data-icon="approve" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 512">
                                        <path fill="currentColor"
                                            d="M64 360C94.93 360 120 385.1 120 416C120 446.9 94.93 472 64 472C33.07 472 8 446.9 8 416C8 385.1 33.07 360 64 360zM64 200C94.93 200 120 225.1 120 256C120 286.9 94.93 312 64 312C33.07 312 8 286.9 8 256C8 225.1 33.07 200 64 200zM64 152C33.07 152 8 126.9 8 96C8 65.07 33.07 40 64 40C94.93 40 120 65.07 120 96C120 126.9 94.93 152 64 152z">
                                        </path>
                                    </svg>
                                    <div class="" style="margin-top:-3%; margin-left:5%">
                                        <ul class="text-gray-600 bg-white rounded shadow" x-show="open"
                                            @click.away="open = false">
                                            <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }">
                                                @can('crm_sales_booking_mgmt_edit')
                                                <a {{-- wire:click="action({'id': {{ $book->id }}},'edit') }}">Edit</a> --}} @endcan </li>
                                            <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }">
                                                {{-- @can('crm_commercials_commercial_mgmt_view') --}}
                                                <a wire:click="action({'id': },'') }}">Redirect to Data Entry</a>
                                                {{-- @endcan --}}
                                            </li>
                                            <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }">
                                                {{-- @can('crm_commercials_commercial_mgmt_view') --}}
                                                <a wire:click="action({'id': },'view') }}">View</a>
                                                {{-- @endcan --}}
                                            </li>
                                            <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }">
                                                {{-- @can('crm_commercials_commercial_mgmt_view') --}}
                                                <a wire:click="action({'id': },'view') }}">Delete</a>
                                                {{-- @endcan --}}
                                            </li>
                                            <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }">
                                                {{-- <a --}}
                                                {{-- wire:click="actionCancelBk({'id':{{ $book->id }}},'cancelbk') }}">Cancel</a> --}}
                                            </li>
                                            <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }">
                                                {{-- @can('crm_commercials_commercial_mgmt_view') --}}
                                                {{-- <a
                                                    wire:click="actionrescheduleBk({'id':{{ $book->id }}},'reschedbk') }}">Reschedule</a> --}}
                                                {{-- @endcan --}}
                                            </li>
                                        </ul>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $acc_s->links() }}
                </div>
            </div>
        </div>

    </x-slot>
</x-form>
