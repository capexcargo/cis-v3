<div class="flex gap-4">
    <div class="w-1/5 pl-4 mt-3 text-lg font-medium border-r-2 border-gray-500 whitespace-nowrap">
        <div class="grid grid-cols-1 mt-6 text-blue">Contact</div>
        <div class="grid grid-cols-1 mt-6">My Tasks</div>
        <div class="grid grid-cols-1 mt-6">My Notes</div>
        <div class="grid grid-cols-1 mt-6">My Emails</div>
        <div class="grid grid-cols-1 mt-6">Quotation</div>
        <div class="grid grid-cols-1 mt-6">My Logged Meetings</div>
        <div class="grid grid-cols-1 mt-6 mb-6">My Scheduled Meetings</div>
    </div>
    <div class="w-4/5 px-6 font-medium">
        <div class="flex justify-between">
            <div class="text-3xl text-blue whitespace-nowrap">{{ $fullname }}</div>
            <div class="text-blue whitespace-nowrap" x-data="{ open: false }">
                <button @click="open = !open" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                    <div class="flex items-start justify-between">
                        Add Activity
                        <svg class="w-3 h-3 mt-1 ml-2" aria-hidden="true" focusable="false" data-prefix="far"
                            data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 448 512">
                            <path fill="currentColor"
                                d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
                        </svg>
                    </div>
                </button>

                <div class="absolute" style="margin-left: -3.5rem">
                    <ul class="text-gray-600 bg-white rounded shadow" x-show="open" @click.away="open = false">
                        <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a href="">Create New
                                Task</a>
                        </li>
                        <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                wire:click="action({},''}">Create a Note</a>
                        </li>
                        <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                wire:click="action({},''}">Create an Email</a>
                        </li>
                        <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                wire:click="action({},''}">Quotation</a>
                        </li>
                        <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                wire:click="action({},''}">Log a Meeting</a>
                        </li>
                        <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }"><a
                                wire:click="action({},''}">Schedule a Meeting</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="grid grid-cols-1 text-lg whitespace-nowrap">{{ $customer_number }}</div>
        <div class="grid grid-cols-2 gap-8 mt-6">
            <div class="grid grid-cols-1 space-y-2">
                <div class="grid grid-cols-2 whitespace-nowrap">
                    <div class="text-sm text-gray-400">
                        Email Address :
                    </div>
                    <div class="whitespace-wrap" style="margin-left: -20px">
                        @foreach ($emails as $i => $email)
                            <div class="flex gap-2">
                                {{ $email->email }}
                                @if ($email->is_primary == 1)
                                    <span>
                                        <svg class="w-4 h-4 mt-1 text-blue" aria-hidden="true" focusable="false"
                                            data-prefix="far" data-icon="approve" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                        </svg>
                                    </span>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="grid grid-cols-2 whitespace-nowrap">
                    <div class="text-sm text-gray-400">
                        Telephone No. :
                    </div>
                    <div class="whitespace-wrap" style="margin-left: -20px">
                        @foreach ($telephone_numbers as $i => $telephone_number)
                            <div class="flex gap-2">
                                {{ $telephone_number->telephone }}
                                @if ($telephone_number->is_primary == 1)
                                    <span>
                                        <svg class="w-4 h-4 mt-1 text-blue" aria-hidden="true" focusable="false"
                                            data-prefix="far" data-icon="approve" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                        </svg>
                                    </span>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="grid grid-cols-2 whitespace-nowrap">
                    <div class="text-sm text-gray-400">
                        Mobile No. :
                    </div>
                    <div class="whitespace-wrap" style="margin-left: -20px">
                        @foreach ($mobile_numbers as $i => $mobile_number)
                            <div class="flex gap-2">
                                {{ $mobile_number->mobile }}
                                @if ($mobile_number->is_primary == 1)
                                    <span>
                                        <svg class="w-4 h-4 mt-1 text-blue" aria-hidden="true" focusable="false"
                                            data-prefix="far" data-icon="approve" role="img"
                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                        </svg>
                                    </span>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
                @foreach ($addresses as $i => $address)
                    <div class="grid grid-cols-2">
                        <div class="text-sm text-gray-400">
                            Address {{ $i + 1 }} (<span
                                class="italic">{{ $address->address_label == 1 ? 'Home' : ($address->address_label == 2 ? 'Office' : 'Warehouse') }}</span>)
                            :
                        </div>
                        <div class="flex gap-2" style="margin-left: -20px">
                            {{ $address->address_line_1 }} <br>
                            {{ $address->address_line_2 }} <br>
                            {{ $address->barangay_id }}, {{ $address->city_id }} {{ $address->state_id }},
                            {{ $address->postal_id }} -
                            {{ $address->country_id }}
                            @if ($address->is_primary == 1)
                                <span>
                                    <svg class="w-4 h-4 mt-1 text-blue" aria-hidden="true" focusable="false"
                                        data-prefix="far" data-icon="approve" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" />
                                    </svg>
                                </span>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="grid grid-cols-2">
                <div class="grid grid-cols-1 space-y-2">
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm text-gray-400">Category :</div>
                        <div class="">
                            {{ $category }}
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm text-gray-400">Life Stage :</div>
                        <div class="">
                            {{ $life_stage }}
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm text-gray-400">Account Type :</div>
                        <div class="">
                            {{ $account_type }}
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm text-gray-400">Contact Owner :</div>
                        <div class="">John Doe</div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm text-gray-400">Onboarding Channel :</div>
                        <div class="">
                            CRM
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm text-gray-400">Marketing Channel :</div>
                        <div class="">
                            {{ $marketing_channel }}
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm text-gray-400">Date Onboarded :</div>
                        <div class="">
                            {{ $date_onboarded }}
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-24 whitespace-nowrap">
                        <div class="text-sm text-gray-400">Note :</div>
                        <div class="">
                            {{ $note }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
