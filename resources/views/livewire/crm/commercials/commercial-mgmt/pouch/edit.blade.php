<div x-data="{

    confirmation_modal: '{{ $confirmation_modal }}',
    current_tab: @entangle('current_tab'),

}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to submit this rate table?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
    <form wire:submit.prevent="submit" autocomplete="off">

        <div x-cloak x-show="current_tab == 1">
            {{-- <div class="-mt-4">
                @if ($rate == 'base')
                    <h1 class="mb-3 text-2xl font-semibold text-left text-black "> Create New Base Rate </h1>
                @else
                    <h1 class="mb-3 text-2xl font-semibold text-left text-black "> Add Pouch Rate</h1>
                @endif
            </div> --}}

            <div class="grid grid-cols-12 gap-6">

                <div class="col-span-6">
                    <x-label for="dates" value="Date Created" />
                    <x-input disabled type="text" name="dates" wire:model.defer='dates' placholder="dates">
                    </x-input>
                    <x-input-error for="" />
                </div>
                <div class="col-span-6">
                    <x-label for="created_by" value="Created By" />
                    <x-input disabled type="text" name="created_by" wire:model.defer='created_by'></x-input>
                    <x-input-error for="created_by" />
                </div>

                <div class="col-span-6">
                    <x-label for="name" value="Rate Name" :required="true" />
                    <x-input type="text" name="name" maxlength="30" wire:model.defer='name'></x-input>
                    <x-input-error for="name" />
                </div>
                <div class="col-span-6">
                    <x-label for="effectivity_date" value="Rate Effectivity Date" :required="true" />
                    <x-input type="date" name="effectivity_date" wire:model.defer='effectivity_date'></x-input>
                    <x-input-error for="effectivity_date" />
                </div>

                <div class="col-span-12">
                    <x-label for="description" value="Rate Description" :required="true" />
                    <x-textarea rows="3" cols="50" maxlength="100" type="text" name="description"
                        wire:model.defer='description'>
                    </x-textarea>
                    <x-input-error for="description" />
                </div>

                <div class="col-span-12">
                    <h1 class="text-2xl font-semibold text-left text-black ">Rate Details</h1>
                </div>

                <div class="col-span-6">
                    <div wire:init="RateTypeReference">
                        <x-label for="rate_type_id" value="Rate Type" :required="true" />
                        <x-select name="rate_type_id" wire:model='rate_type_id'>
                            <option value=""></option>
                            @foreach ($rate_type_references as $rate_type_reference)
                                <option value="{{ $rate_type_reference->id }}">
                                    {{ $rate_type_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="rate_type_id" />
                    </div>
                </div>
                <div class="col-span-6">
                    <x-label for="transport_mode_id" value="Transport Mode" :required="true" />
                    <x-input disabled type="text" name="" wire:model.defer='transport_mode_id'></x-input>
                    <x-input-error for="transport_mode_id" />
                </div>

                <div class="col-span-12">

                    <div wire:init="BookingReference">
                        <x-label for="booking_type_id" value="Booking Type" :required="true" />
                        <x-select name="booking_type_id" wire:model='booking_type_id'>
                            <option value=""></option>
                            @foreach ($booking_references as $booking_reference)
                                <option value="{{ $booking_reference->id }}">
                                    {{ $booking_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="booking_type_id" />
                    </div>
                </div>

                <div class="col-span-12">

                    <div wire:init="ApplyReference">
                        <x-label for="apply_for_id" value="Apply To" :required="true" />
                        <x-select name="apply_for_id" wire:model='apply_for_id'>
                            {{-- <option value=""></option> --}}
                            @foreach ($apply_references as $apply_reference)
                                <option value="{{ $apply_reference->id }}">
                                    {{ $apply_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="apply_for_id" />
                    </div>
                </div>

                <div class="col-span-12">

                    <div wire:init="PouchTypeReference">
                        <x-label for="pouch_type_id" value="Pouch Type" :required="true" />
                        <x-select name="pouch_type_id" wire:model='pouch_type_id'>
                            <option value=""></option>
                            @foreach ($pouch_type_references as $pouch_type_reference)
                                <option value="{{ $pouch_type_reference->id }}">
                                    {{ $pouch_type_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="pouch_type_id" />
                    </div>
                </div>
            </div>

            <div class="flex justify-end gap-3 mt-6 space-x-3">
                <x-button type="button" wire:click="$emit('close_modal', 'edit')" title="Cancel"
                    class="px-12 bg-white text-blue hover:bg-gray-100" />
                <x-button type="button" wire:click="action({},'update_next')" title="Next"
                    class="px-14 bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>

        <div x-cloak x-show="current_tab == 2">

            <div class="grid grid-cols-12 ">
                <div class="col-span-12">
                    <h1 class="text-2xl font-semibold text-left text-black ">Rate Details</h1>
                </div>
                {{-- <div class="col-span-12 mt-4 ml-4">
                    <input type="checkbox" class="rounded-sm form-checkbox" id="vice_versa" name="vice_versa"
                        value="1" wire:model="vice_versa"> Vice Versa
                </div> --}}
                <div class="col-span-12 text-center">
                    <div class="mt-4 bg-white border rounded-lg drop-shadow-lg">
                        <x-table.table class="overflow-hidden">
                            <x-slot name="thead">
                                <x-table.th name="Origin*" style="padding-left:2%;" />
                                <x-table.th name="Destination*" style="padding-left:2%;" />
                                <x-table.th name="" style="padding-left:4%;" />
                                <x-table.th name="Small (3kg)*" style="padding-left:4%;" />
                                <x-table.th name="Medium (5kg)*" style="padding-left:3%;" />
                                <x-table.th name="Large (10kg)*" style="padding-left:4%;" />
                                <x-table.th name="Action" style="padding-left:2%;" />
                            </x-slot>

                            <x-slot name="tbody">
                                @foreach ($rates as $a => $rate)
                                    @if (!$rate['is_deleted'])
                                        <tr class="font-semibold border-0">
                                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                                <div wire:init="OriginReference">
                                                    <x-label for="rates.{{ $a }}.origin_id"
                                                        value="" />
                                                    <x-select class="rounded-sm"
                                                        name="rates.{{ $a }}.origin_id"
                                                        wire:model='rates.{{ $a }}.origin_id'>
                                                        <option value=""></option>
                                                        @foreach ($origin_references as $origin_reference)
                                                            <option value="{{ $origin_reference->id }}">
                                                                {{ $origin_reference->code }}
                                                            </option>
                                                        @endforeach
                                                    </x-select>
                                                    <x-input-error class="font-normal"
                                                        for="rates.{{ $a }}.origin_id" />
                                                </div>
                                            </td>
                                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                                <div wire:init="DestinationReference">
                                                    <x-label for="destination_id" value="" />
                                                    <x-select class="rounded-sm"
                                                        name="rates.{{ $a }}.destination_id"
                                                        wire:model='rates.{{ $a }}.destination_id'>
                                                        <option value=""></option>
                                                        @foreach ($destination_references as $destination_reference)
                                                            <option value="{{ $destination_reference->id }}">
                                                                {{ $destination_reference->code }}
                                                            </option>
                                                        @endforeach
                                                    </x-select>
                                                    <x-input-error class="font-normal"
                                                        for="rates.{{ $a }}.destination_id" />
                                                </div>
                                            </td>

                                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                                {{-- {{ $vice_versa_display }} --}}
                                            </td>

                                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                                <x-label for="amount_small" value="" />
                                                <x-input class="w-32 rounded-sm" type="number"
                                                    name="rates.{{ $a }}.amount_small"
                                                    wire:model.defer='rates.{{ $a }}.amount_small'>
                                                </x-input>
                                                <x-input-error class="font-normal"
                                                    for="rates.{{ $a }}.amount_small" />
                                            </td>

                                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                                <x-label for="amount_medium" value="" />
                                                <x-input class="w-32 rounded-sm" type="number"
                                                    name="rates.{{ $a }}.amount_medium"
                                                    wire:model.defer='rates.{{ $a }}.amount_medium'>
                                                </x-input>
                                                <x-input-error class="font-normal"
                                                    for="rates.{{ $a }}.amount_medium" />
                                            </td>

                                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                                <x-label for="amount_large" value="" />
                                                <x-input class="w-32 rounded-sm" type="number"
                                                    name="rates.{{ $a }}.amount_large"
                                                    wire:model.defer='rates.{{ $a }}.amount_large'>
                                                </x-input>
                                                <x-input-error class="font-normal"
                                                    for="rates.{{ $a }}.amount_large" />
                                            </td>

                                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                                <div class="flex space-x-3">
                                                    <svg wire:click="addRates()" class="h-6 w-7 text-blue"
                                                        aria-hidden="true" focusable="false" data-prefix="fas"
                                                        data-icon="trash-alt" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                        </path>
                                                    </svg>

                                                    <svg wire:click="removeRates({'a': {{ $a }}})"
                                                        class="h-6 w-7 text-red" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="circle-minus" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z">
                                                        </path>
                                                    </svg>
                                                </div>

                                            </td>
                                        </tr>
                                    @endif
                                @endforeach

                            </x-slot>


                        </x-table.table>


                    </div>
                </div>

                <div class="col-span-12 mt-6">
                    <p class="text-sm font-semibold text-left text-blue ">Terms and Conditions / Notes</p>
                    <p class="text-sm font-medium text-left text-black ">1. Above rates are applicable for within
                        city limits pick-up and delivery only.</p>
                    <p class="text-sm font-medium text-left text-black ">2. Special handling, hauling & unloading,
                        man-power, lashing and incidental charges are not included.</p>
                    <p class="text-sm font-medium text-left text-black ">3. In case of cargo-cancelation, 5% of the
                        Total Charges will be applied or 10% of the Total Charges if CaPEx will return the cargo to the
                        pick-up area.</p>
                    <p class="text-sm font-medium text-left text-black ">4. Storage Fee is Php50.00 per cbm per day.
                    </p>

                </div>

            </div>
            <div class="flex justify-end gap-3 mt-6 space-x-3">
                <x-button type="button" wire:click="$set('current_tab', 1)" title="Back"
                    class="px-12 bg-white text-blue hover:bg-gray-100" />
                <x-button type="button" wire:click="action({},'update_next_perview')" title="Submit"
                    class="px-12 bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>
    </form>
</div>
