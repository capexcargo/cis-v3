<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',

}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to submit this Loa?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                No
                </button>
                <button type="button" wire:click="submit"
                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
    
    <form wire:submit.prevent="submit" autocomplete="off">
        <div class="mt-5 space-y-3">
            <div class="grid grid-cols-12 gap-8">
                <div class="col-span-12">
                    {{-- @if ($i == 0) --}}
                        <x-label for="rate_category" value="Rate Category" :required="true" />
                    {{-- @endif --}}
                    <x-select name="rate_category"
                        wire:model.defer='rate_category'>
                        <option value="">Select</option>
                        <option value="1">Air Freight</option>
                        <option value="2">Sea Freight</option>
                        <option value="3">Land Freight</option>
                        <option value="4">Air Freight Premium</option>
                        <option value="5">Pouch</option>
                        <option value="6">Box</option>
                        <option value="7">Crating</option>
                        <option value="8">Warehousing Service Rate</option>
                    </x-select>
                    <x-input-error for="rate_category" />
                </div>

                <div class="col-span-12">
                    <div wire:init="loadFirstApproverReference">
                        <x-label for="approver1_id" value="First Approver" :required="true" />
                        <x-select name="approver1_id" wire:model='approver1_id'>
                            <option value="">Select</option>
                            @foreach ($first_approver_references as $first_approver_references)
                                <option value="{{ $first_approver_references->id }}">
                                    {{ $first_approver_references->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="approver1_id" />
                    </div>
                </div>
                <div class="col-span-12">
                    <div wire:init="loadSecodeApproverReference">
                        <x-label for="approver2_id" value="Second Approver" :required="true" />
                        <x-select name="approver2_id" wire:model='approver2_id'>
                            <option value="">Select</option>
                            @foreach ($second_approver_references as $second_ref)
                                <option value="{{ $second_ref->id }}">
                                    {{ $second_ref->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="approver2_id" />
                    </div>
                </div>
            </div> 
            
        </div>
        <div class="flex justify-end gap-3 mt-6 space-x-3">
            <x-button type="button" wire:click="closecreatemodal" title="Cancel"
            class="px-12 bg-white text-blue hover:bg-gray-100" />
            <x-button type="button" wire:click="action({},'submit2')" title="Submit" 
            class="px-12 bg-blue text-white hover:bg-[#002161]" />
        </div>

        
    </form>
</div>
