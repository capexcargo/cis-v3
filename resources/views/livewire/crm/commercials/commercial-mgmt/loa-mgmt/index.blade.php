<x-form wire:init="load" x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    {{-- delete_modal: '{{ $delete_modal }}', --}}

}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_commercials_commercial_mgmt_loa_mgmt_add')
            <x-modal id="create_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="title">Add Loa</x-slot>
                <x-slot name="body">
                    @livewire('crm.commercials.commercial-mgmt.loa-mgmt.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('crm_commercials_commercial_mgmt_loa_mgmt_edit')
            @if ($loa_id && $edit_modal)
                <x-modal id="edit_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="title">Edit Loa</x-slot>
                    <x-slot name="body">
                        @livewire('crm.commercials.commercial-mgmt.loa-mgmt.edit', ['id' => $loa_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        {{-- @can('crm_service_request_sr_related_mgmt_delete')
            <x-modal id="delete_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="body">
                    <h2 class="mb-3 text-lg text-center text-gray-900">
                        {{ $confirmation_message }}
                    </h2>
                    <div class="flex justify-center space-x-3">
                        <button type="button" wire:click="$set('delete_modal', false)"
                            class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                        <button type="button" wire:click="confirm"
                            class="flex-none px-5 py-1 mt-4 text-sm text-white rounded-lg bg-blue">
                            Yes</button>
                    </div>
                </x-slot>
            </x-modal>
        @endcan --}}
    </x-slot>

    <x-slot name="header_title">Commercial Management</x-slot>
    <x-slot name="header_button">
        @can('crm_commercials_commercial_mgmt_loa_mgmt_add')
            <button wire:click="action({}, 'add')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far" data-icon="print-alt"
                        role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Add Loa
                </div>
            </button>
        @endcan

    </x-slot>
    <x-slot name="body">

        <div class="grid grid-cols-6 gap-4 ">
            <button wire:click="redirectTo({}, 'redirectToAFMgmt')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">AIR FREIGHT</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToAirFreightPremium')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">AIR FREIGHT<br>PREMIUM</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToBox')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">BOX</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToCrating')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">CRATING</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToLand')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">LAND FREIGHT</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToLoa')" class="w-auto py-4 text-gray-700 bg-white rounded-md"
                style="border: 4px solid darkblue;">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-center">LOA MANAGEMENT</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToPouch')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">POUCH</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToSea')"
                class="w-auto p-4 text-gray-700 bg-white border rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">SEA FREIGHT</span>
                    <span class="pr-4"></span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToWarehousing')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">WAREHOUSING<BR>SERVICE RATE</span>
                </div>
            </button>

        </div>

        <div class="grid grid-cols-5 text-sm font-semibold" style="margin-top: 4%;">
            <div class="w-5/6">
                <x--transparent.select value="{{ $rate_category }}" label="Rate Category" name="rate_category"
                    wire:model.defer='rate_category'>
                    <option value=""></option>
                    <option value="1">Air Freight</option>
                    <option value="2">Sea Freight</option>
                    <option value="3">Land Freight</option>
                    <option value="4">Air Freight Premium</option>
                    <option value="5">Pouch</option>
                    <option value="6">Box</option>
                    <option value="7">Crating</option>
                    <option value="8">Warehousing Service Rate</option>
                </x--transparent.select>
            </div>
            <div class="w-5/6">
                <div wire:init="loadFirstApproverReference">
                    <x-transparent.select value="{{ $approver1_id }}" label="1st Approver" name="approver1_id"
                        wire:model.defer="approver1_id">
                        <option value=""></option>
                        @foreach ($first_approver_references as $first_approver_references)
                            <option value="{{ $first_approver_references->id }}">
                                {{ $first_approver_references->name }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>

            <div class="w-5/6">
                <div wire:init="loadSecodeApproverReference">
                    <x-transparent.select value="{{ $approver2_id }}" label="2nd Approver" name="approver2_id"
                        wire:model.defer="approver2_id">
                        <option value=""></option>
                        @foreach ($second_approver_references as $second_ref)
                            <option value="{{ $second_ref->id }}">
                                {{ $second_ref->name }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>

            <div class="">
                <x-button type="button" wire:click="search" title="Search"
                    class="text-white bg-blue hover:bg-blue-800" />
            </div>
        </div>


        <div class="py-12">

            <div class="bg-white rounded-lg shadow-md">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="No." style="padding-left:2%;" />
                        <x-table.th name="Rate Category" style="padding-left:10%;" />
                        <x-table.th name="Approver 1" style="padding-left:10%;" />
                        <x-table.th name="Approver 2" style="padding-left:10%;" />
                        <x-table.th name="Action" style="padding-left:10%;" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($loa_s as $i => $loa)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-normal">
                                <td class="w-24 p-3 whitespace-nowrap" style="padding-left:2%;">
                                    <p class="w-24">
                                        {{ ($loa_s->currentPage() - 1) * $loa_s->links()->paginator->perPage() + $loop->iteration }}
                                    </p>
                                </td>
                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:10%;">

                                    {{ $loa->rate_category == 1
                                        ? 'Air Freight'
                                        : ($loa->rate_category == 2
                                            ? 'Sea Freight'
                                            : ($loa->rate_category == 3
                                                ? 'Land Freight'
                                                : ($loa->rate_category == 4
                                                    ? 'Air Freight Premium'
                                                    : ($loa->rate_category == 5
                                                        ? 'Pouch'
                                                        : ($loa->rate_category == 6
                                                            ? 'Box'
                                                            : ($loa->rate_category == 7
                                                                ? 'Crating'
                                                                : 'Warehousing Service Rate')))))) }}
                                </td>

                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:10%;">
                                    {{ $loa->App1->name }}
                                </td>

                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:10%;">
                                    {{ $loa->App2->name }}
                                </td>

                                <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:10%;">
                                    <div class="flex space-x-3">
                                        <span title="Edit">
                                            @can('crm_commercials_commercial_mgmt_loa_mgmt_edit')
                                                <svg wire:click="action({'id': {{ $loa->id }}}, 'edit')"
                                                    class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="far" data-icon="edit" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                                    </path>
                                                </svg>
                                            </span>
                                        @endcan

                                        {{-- @can('crm_service_request_sr_related_mgmt_delete') --}}
                                        <span title="Delete">
                                            <svg wire:click="action({'id':}, 'delete')" class="w-5 h-5 text-red"
                                                aria-hidden="true" focusable="false" data-prefix="fas"
                                                data-icon="trash-alt" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                <path fill="currentColor"
                                                    d="M160 400C160 408.8 152.8 416 144 416C135.2 416 128 408.8 128 400V192C128 183.2 135.2 176 144 176C152.8 176 160 183.2 160 192V400zM240 400C240 408.8 232.8 416 224 416C215.2 416 208 408.8 208 400V192C208 183.2 215.2 176 224 176C232.8 176 240 183.2 240 192V400zM320 400C320 408.8 312.8 416 304 416C295.2 416 288 408.8 288 400V192C288 183.2 295.2 176 304 176C312.8 176 320 183.2 320 192V400zM317.5 24.94L354.2 80H424C437.3 80 448 90.75 448 104C448 117.3 437.3 128 424 128H416V432C416 476.2 380.2 512 336 512H112C67.82 512 32 476.2 32 432V128H24C10.75 128 0 117.3 0 104C0 90.75 10.75 80 24 80H93.82L130.5 24.94C140.9 9.357 158.4 0 177.1 0H270.9C289.6 0 307.1 9.358 317.5 24.94H317.5zM151.5 80H296.5L277.5 51.56C276 49.34 273.5 48 270.9 48H177.1C174.5 48 171.1 49.34 170.5 51.56L151.5 80zM80 432C80 449.7 94.33 464 112 464H336C353.7 464 368 449.7 368 432V128H80V432z">
                                                </path>
                                            </svg>
                                        </span>
                                        {{-- @endcan --}}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $loa_s->links() }}
                </div>
            </div>







        </div>


    </x-slot>
</x-form>
