<form wire:submit.prevent="submit" autocomplete="off">
    <div class="grid grid-cols-12 px-2">
        <div class="col-span-5 text-left">
            <div class="mt-4 bg-white" style="">
                <x-table.table class="overflow-hidden border-none">
                    <x-slot name="thead">
                    </x-slot>
                    <x-slot name="tbody">
                        <tr class="font-semibold border-0 bg-white">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Rate Name :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $sea_s->name }}
                                </div>
                            </td>
                        </tr>
                        <tr class="font-semibold border-0 bg-white">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Commodity Type :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $sea_s->CommodityTypeReference->name }}
                                </div>
                            </td>
                        </tr>
                        <tr class="font-semibold border-0 bg-white">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Service Mode :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                </div>
                            </td>
                        </tr>
                    </x-slot>
                </x-table.table>
            </div>
        </div>
        <div class="col-span-2"></div>
        <div class="col-span-5 text-left">
            <div class="mt-4 bg-white" style="">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                    </x-slot>
                    <x-slot name="tbody">
                        <tr class="font-semibold border-0 bg-white">
                            <td class="w-1/6  text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Origin :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-right:;">
                                <div>
                                    @foreach ($sea_s->RateseafreightHasMany as $a => $sea)
                                    {{ $sea->OriginDetails->code }}
                                    @endforeach
                                </div>
                            </td>
                        </tr>
                        <tr class="font-semibold border-0 bg-white">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Vice Versa :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $vice_versa_status }}
                                </div>
                            </td>
                        </tr>
                        <tr class="font-semibold border-0 bg-white">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Loading Type :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $sea_s->ShipmentTypeReference->name }}
                                </div>
                            </td>
                        </tr>
                    </x-slot>
                </x-table.table>
            </div>
        </div>
    </div>

    {{-- {{dd($sea_s->shipment_type_id);}} --}}

    @if ($sea_s->shipment_type_id == 1)

        <div class="grid grid-cols-12 px-2">
            <div class="col-span-12 text-center">
                <div class="mt-4 bg-white border rounded-lg drop-shadow-lg" style="">
                    <x-table.table class="overflow-hidden">
                        <x-slot name="thead">
                            <x-table.th name="Origin" style="padding-left:2%;" />
                            <x-table.th name="Destination" style="padding-left:2%;" />
                            <x-table.th name="VV" style="padding-left:3.5%;" />
                            <x-table.th name="Size" style="padding-left:3.5%;" />
                            <x-table.th name="Pier-to-Pier" style="padding-left:;" />
                            <x-table.th name="Door-to-Pier" style="padding-left:;" />
                            <x-table.th name="Pier-to-Door" style="padding-left:%;" />
                            <x-table.th name="Door-to-Door" style="padding-left:%;" />
                        </x-slot>

                        <x-slot name="tbody">
                            @foreach ($sea_s->RateseafreightHasMany as $a => $sea)
                                <tr class="font-semibold border-0">
                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        <div>
                                            {{ $sea->OriginDetails->code }}
                                        </div>
                                    </td>
                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        <div>
                                            {{ $sea->DestinationDetails->code }}
                                        </div>
                                    </td>

                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $vice_versa_display }}
                                    </td>

                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $sea->size }}.00
                                    </td>

                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $sea->amount_servicemode_1 }}.00
                                    </td>

                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $sea->amount_servicemode_2 }}.00
                                    </td>

                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $sea->amount_servicemode_3 }}.00
                                    </td>

                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $sea->amount_servicemode_4 }}.00
                                    </td>
                                </tr>
                            @endforeach
                        </x-slot>
                    </x-table.table>
                </div>
            </div>

            <div class="col-span-12 pb-4 mt-6">
                <p class="p-2 text-sm font-semibold text-left text-blue ">TERMS AND CONDITIONS / NOTES :</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">1. Charges subject to 12% VAT.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">2. Rates apply to shipment of General Cargo
                    and SOC (Shipper's Own Container).</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">3. Valuation Charges of Php5.00 per 1,000 +
                    vat in excess of allowable maximum declared value: 10ftr - php</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">250,000; 20ftr - php500,000; 40ftr - 1 Million
                </p>
                <p class="pl-8 text-sm font-medium text-left text-black ">4. Maximum allowable Weight: 10footer - 9 tons
                    20footer - 18tons; 40footer - 25 tons</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">5. There will be 2 calendar days storage free
                    time at the destination port.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">6. Standard Fees shall be imposed exceeding
                    free time mentioned above.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">7. Detention (containers pulled out as empty
                    or released as laden but are overstaying at the customers or its</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">representative facility): 10ftr - 280; 20ftr -
                    php560; 40ftr - 1,120</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">8. Payment should be in cash, managers or
                    cashier's check unless with approved credit terms as assessed from</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">Finance.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">9. No 40 footer acceptance to ports of
                    Bacolod, Iloilo and Dumaguete unless with prior arrangement</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">10. Rates are subject to government and 3rd
                    part increases</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">11. Rates do not apply to Rolling Cargo and or
                    Dangerous Goods</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">12. Rates are not applicable for outside city
                    limits pick-up and delivery of origin and destination port.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">13. Loading and unloading of containers,
                    equipment rental, lashing, hauling, manpower and other special</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">handling is not included on above given rates.
                </p>
                <p class="pl-8 text-sm font-medium text-left text-black ">Above rates are subject to change with or
                    without prior notice.</p>
            </div>

        </div>
    @else
        <div class="grid grid-cols-12 px-2">
            <div class="col-span-12 text-center">
                <div class="mt-4 bg-white border rounded-lg drop-shadow-lg" style="">
                    <x-table.table class="overflow-hidden">
                        <x-slot name="thead">
                            <x-table.th name="Origin" style="padding-left:5.5%;" />
                            <x-table.th name="Destination" style="padding-left:3.5%;" />
                            <x-table.th name="VV" style="padding-left:5.8%;" />
                            <x-table.th name="Pier-to-Pier" style="padding-left:4%;" />
                            <x-table.th name="Door-to-Pier" style="padding-left:3%;" />
                            <x-table.th name="Pier-to-Door" style="padding-left:3%;" />
                            <x-table.th name="Door-to-Door" style="padding-left:%;" />
                        </x-slot>

                        <x-slot name="tbody">
                            @foreach ($sea_s->RateseafreightHasMany as $a => $sea)
                                <tr class="font-semibold border-0">
                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        <div>
                                            {{ $sea->OriginDetails->code }}
                                        </div>
                                    </td>
                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        <div>
                                            {{ $sea->DestinationDetails->code }}
                                        </div>
                                    </td>

                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $vice_versa_display }}
                                    </td>

                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $sea->amount_weight_1 }}.00
                                    </td>

                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $sea->amount_weight_2 }}.00
                                    </td>

                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $sea->amount_weight_3 }}.00
                                    </td>

                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $sea->amount_weight_4 }}.00
                                    </td>
                                </tr>
                            @endforeach
                        </x-slot>
                    </x-table.table>
                </div>
            </div>

            <div class="col-span-12 text-left">
                <div class="" style="width: 40vw;">
                    <x-table.table class="overflow-hidden">

                        <x-slot name="tbody">
                            <tr class="bg-white border-white font-sm">
                                <td class="w-1/6 text-blue whitespace-nowrap" style="padding-top:6%;">
                                    ANCILLARY CHARGES :
                                </td>
                                <td class="w-1/6 whitespace-nowrap" style="padding-left:5%; padding-top:6%;">
                                </td>
                            </tr>
                            <tr class="bg-white border-none font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    WAYBILL FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    Php50.00 per waybill
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    VALUATION FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    Php5.00 per Php 1,000.00 Declared Value (0.5%)
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    HANDLING FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    Php1.50 per kgs. for cargoes over 50kgs per piece.
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    DG FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    Php500.00 per commodity
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    ODA FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    Additional fee will apply for out of city limits delivery
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    VAT :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    12% of Sub total
                                </td>
                            </tr>

                        </x-slot>
                    </x-table.table>
                </div>
            </div>

            <div class="col-span-12 pb-4 mt-6">
                <p class="p-2 text-sm font-semibold text-left text-blue ">TERMS AND CONDITIONS / NOTES :</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">1. Above rates are applicable for within city
                    limits pick-up and delivery only.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">2. Special handling, hauling & unloading,
                    man-power, lashing and incidental charges are not included.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">3. In case of cargo-cancelation, 5% of the
                    Total Charges will be applied or 10% of the Total Charges if CaPEx will return the cargo to</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">the pick-up area.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">4. Storage Fee is Php50.00 per cbm per day.
                </p>
            </div>

        </div>

    @endif



</form>
