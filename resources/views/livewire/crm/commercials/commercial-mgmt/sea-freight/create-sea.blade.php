<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
    current_tab: {{ $current_tab }},

}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to create this rate table?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">

        <div x-cloak x-show="current_tab == 1">
            <h1 class="mb-3 text-2xl font-semibold text-left text-black "> Add SF Rate </h1>


            <div class="grid grid-cols-12 gap-6">

                <div class="col-span-6">
                    <x-label for="dates" value="Date Created" />
                    <x-input disabled type="text" name="dates" wire:model.defer='dates' placholder="dates">
                    </x-input>
                    <x-input-error for="" />
                </div>
                <div class="col-span-6">
                    <x-label for="created_by" value="Created By" />
                    <x-input disabled type="text" name="created_by" wire:model.defer='created_by'></x-input>
                    <x-input-error for="created_by" />
                </div>

                <div class="col-span-6">
                    <x-label for="name" value="Rate Name" :required="true" />
                    <x-input type="text" name="name" maxlength="30" wire:model.defer='name'></x-input>
                    <x-input-error for="name" />
                </div>
                <div class="col-span-6">
                    <x-label for="effectivity_date" value="Rate Effectivity Date" :required="true" />
                    <x-input type="date" name="effectivity_date" wire:model.defer='effectivity_date'></x-input>
                    <x-input-error for="effectivity_date" />
                </div>

                <div class="col-span-12">
                    <x-label for="description" value="Rate Description" :required="true" />
                    <x-textarea rows="3" cols="50" maxlength="100" type="text" name="description"
                        wire:model.defer='description'>
                    </x-textarea>
                    <x-input-error for="description" />
                </div>

                <div class="col-span-12">
                    <h1 class="text-2xl font-semibold text-left text-black ">Rate Details</h1>
                </div>

                <div class="col-span-6">
                    <x-label for="transport_mode_id" value="Transport Mode" :required="true" />
                    <x-input disabled type="text" name="" wire:model.defer='transport_mode_id'></x-input>
                    <x-input-error for="transport_mode_id" />
                </div>


                <div class="col-span-6">
                    <div wire:init="CommodityTypeReference">
                        <x-label for="commodity_type_id" value="Commodity Type" :required="true" />
                        <x-select style="cursor: pointer;" name="commodity_type_id" wire:model='commodity_type_id'>
                            <option value=""></option>
                            @foreach ($commodity_references as $commodity_reference)
                                <option value="{{ $commodity_reference->id }}">
                                    {{ $commodity_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="commodity_type_id" />
                    </div>
                </div>

                <div class="col-span-6">

                    <div wire:init="ApplyReference">
                        <x-label for="apply_for_id" value="Apply To" :required="true" />
                        <x-select style="cursor: pointer;" name="apply_for_id" wire:model='apply_for_id'>
                            <option value="">All</option>
                            @foreach ($apply_references as $apply_reference)
                                <option value="{{ $apply_reference->id }}">
                                    {{ $apply_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="apply_for_id" />
                    </div>
                </div>

                <div class="col-span-6">

                    <x-label for="base_rate_id" value="Base Rate" :required="true" />
                    <x-select style="cursor: pointer;" name="base_rate_id" wire:model='base_rate_id'>
                        <option value=""></option>
                        @foreach ($bases as $base)
                            <option value="{{ $base->id }}">
                                {{ $base->name }}
                            </option>
                        @endforeach
                    </x-select>
                    <x-input-error for="base_rate_id" />
                </div>

            </div>

            {{-- <div class="col-span-6">
                <x-label for="shipment_type_id" value="{{$shipment_type_id}}" :required="true" />
                <x-input disabled type="text" name="" wire:model.defer='shipment_type_id'></x-input>
                <x-input-error for="shipment_type_id" />
            </div> --}}

            <div class="flex justify-end gap-3 mt-6 space-x-3">
                <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                    class="px-12 bg-white text-blue hover:bg-gray-100" />
                <x-button type="button" wire:click="action({},'create_next')" title="Add"
                    class="px-14 bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>

        <div x-cloak x-show="current_tab == 2">
            {{-- {{dd($bases->['shipment_type_id']);}} --}}
            @if ($shipment_type_id == 1)
                <div class="grid grid-cols-12">
                    <div class="col-span-12">
                        <h1 class="text-2xl font-semibold text-left text-black ">Rate Settings</h1>
                    </div>

                    <div class="col-span-12 overflow-auto text-center" style="width: 26vw">
                        <div class="mt-2 ">
                            <x-table.table class="overflow-hidden">
                                <x-slot name="thead">
                                    <x-table.th name="" style="padding-left:2%;" />
                                    <x-table.th name="Size" style="padding-left:6%;" />
                                    <x-table.th name="Amount" style="padding-left:7%;" />
                                    <x-table.th name="Add" style="padding-left:9%;" />
                                    <x-table.th name="Less" style="padding-left:10%;" />
                                </x-slot>

                                <x-slot name="tbody">
                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            Pier-to-Pier
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            10
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_servicemode_1_ptp_10" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_servicemode_1_ptp_10"
                                                wire:model.defer='amount_servicemode_1_ptp_10'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_servicemode_1_ptp_10" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:7%;">
                                            <div class=""><input type="radio" name="aw1ptp10"
                                                    wire:model='aw1ptp10' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap"
                                            style="padding-left:10%; padding-right:10%;">
                                            <div class=""><input type="radio" name="aw1ptp10"
                                                    wire:model='aw1ptp10' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">

                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            20
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_servicemode_1_ptp_20" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_servicemode_1_ptp_20"
                                                wire:model.defer='amount_servicemode_1_ptp_20'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_servicemode_1_ptp_20" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:7%;">
                                            <div class=""><input type="radio" name="aw1ptp20"
                                                    wire:model='aw1ptp20' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap"
                                            style="padding-left:10%; padding-right:10%;">
                                            <div class=""><input type="radio" name="aw1ptp20"
                                                    wire:model='aw1ptp20' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">

                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            40
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_servicemode_1_ptp_40" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_servicemode_1_ptp_40"
                                                wire:model.defer='amount_servicemode_1_ptp_40'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_servicemode_1_ptp_40" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:7%;">
                                            <div class=""><input type="radio" name="aw1ptp40"
                                                    wire:model='aw1ptp40' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap"
                                            style="padding-left:10%; padding-right:10%;">
                                            <div class=""><input type="radio" name="aw1ptp40"
                                                    wire:model='aw1ptp40' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            Door-to-Pier
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            10
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_servicemode_2_ptp_10" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_servicemode_2_ptp_10"
                                                wire:model.defer='amount_servicemode_2_ptp_10'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_servicemode_2_ptp_10" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:7%;">
                                            <div class=""><input type="radio" name="aw2ptp10"
                                                    wire:model='aw2ptp10' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap"
                                            style="padding-left:10%; padding-right:10%;">
                                            <div class=""><input type="radio" name="aw2ptp10"
                                                    wire:model='aw2ptp10' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">

                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            20
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_servicemode_2_ptp_20" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_servicemode_2_ptp_20"
                                                wire:model.defer='amount_servicemode_2_ptp_20'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_servicemode_2_ptp_20" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:7%;">
                                            <div class=""><input type="radio" name="aw2ptp20"
                                                    wire:model='aw2ptp20' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap"
                                            style="padding-left:10%; padding-right:10%;">
                                            <div class=""><input type="radio" name="aw2ptp20"
                                                    wire:model='aw2ptp20' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">

                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            40
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_servicemode_2_ptp_40" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_servicemode_2_ptp_40"
                                                wire:model.defer='amount_servicemode_2_ptp_40'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_servicemode_2_ptp_40" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:7%;">
                                            <div class=""><input type="radio" name="aw2ptp40"
                                                    wire:model='aw2ptp40' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap"
                                            style="padding-left:10%; padding-right:10%;">
                                            <div class=""><input type="radio" name="aw2ptp40"
                                                    wire:model='aw2ptp40' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            Pier-to-Door
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            10
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_servicemode_3_ptp_10" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_servicemode_3_ptp_10"
                                                wire:model.defer='amount_servicemode_3_ptp_10'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_servicemode_3_ptp_10" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:7%;">
                                            <div class=""><input type="radio" name="aw3ptp10"
                                                    wire:model='aw3ptp10' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap"
                                            style="padding-left:10%; padding-right:10%;">
                                            <div class=""><input type="radio" name="aw3ptp10"
                                                    wire:model='aw3ptp10' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">

                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            20
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_servicemode_3_ptp_20" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_servicemode_3_ptp_20"
                                                wire:model.defer='amount_servicemode_3_ptp_20'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_servicemode_3_ptp_20" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:7%;">
                                            <div class=""><input type="radio" name="aw3ptp20"
                                                    wire:model='aw3ptp20' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap"
                                            style="padding-left:10%; padding-right:10%;">
                                            <div class=""><input type="radio" name="aw3ptp20"
                                                    wire:model='aw3ptp20' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">

                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            40
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_servicemode_3_ptp_40" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_servicemode_3_ptp_40"
                                                wire:model.defer='amount_servicemode_3_ptp_40'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_servicemode_3_ptp_40" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:7%;">
                                            <div class=""><input type="radio" name="aw3ptp40"
                                                    wire:model='aw3ptp40' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap"
                                            style="padding-left:10%; padding-right:10%;">
                                            <div class=""><input type="radio" name="aw3ptp40"
                                                    wire:model='aw3ptp40' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            Door-to-Door
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            10
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_servicemode_4_ptp_10" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_servicemode_4_ptp_10"
                                                wire:model.defer='amount_servicemode_4_ptp_10'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_servicemode_4_ptp_10" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:7%;">
                                            <div class=""><input type="radio" name="aw4ptp10"
                                                    wire:model='aw4ptp10' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap"
                                            style="padding-left:10%; padding-right:10%;">
                                            <div class=""><input type="radio" name="aw4ptp10"
                                                    wire:model='aw4ptp10' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">

                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            20
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_servicemode_4_ptp_20" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_servicemode_4_ptp_20"
                                                wire:model.defer='amount_servicemode_4_ptp_20'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_servicemode_4_ptp_20" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:7%;">
                                            <div class=""><input type="radio" name="aw4ptp20"
                                                    wire:model='aw4ptp20' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap"
                                            style="padding-left:10%; padding-right:10%;">
                                            <div class=""><input type="radio" name="aw4ptp20"
                                                    wire:model='aw4ptp20' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">

                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            40
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_servicemode_4_ptp_40" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_servicemode_4_ptp_40"
                                                wire:model.defer='amount_servicemode_4_ptp_40'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_servicemode_4_ptp_40" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:7%;">
                                            <div class=""><input type="radio" name="aw4ptp40"
                                                    wire:model='aw4ptp40' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap"
                                            style="padding-left:10%; padding-right:10%;">
                                            <div class=""><input type="radio" name="aw4ptp40"
                                                    wire:model='aw4ptp40' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                </div>
            @endif
            @if ($shipment_type_id == 2)

                <div class="grid grid-cols-12">
                    <div class="col-span-12">
                        <h1 class="text-2xl font-semibold text-left text-black ">Rate Settings</h1>
                    </div>

                    <div class="col-span-12 overflow-auto text-center" style="width: 22.5vw">
                        <div class="mt-2 ">
                            <x-table.table class="overflow-hidden">
                                <x-slot name="thead">
                                    <x-table.th name="" style="padding-left:2%;" />
                                    <x-table.th name="Amount*" style="padding-left:10%;" />
                                    <x-table.th name="Add*" style="padding-left:9%;" />
                                    <x-table.th name="Less*" style="padding-left:10%;" />
                                </x-slot>

                                <x-slot name="tbody">
                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            Pier-To-Pier
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_weight_1" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_weight_1"
                                                wire:model.defer='amount_weight_1'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_weight_1" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:4%;">
                                            <div class=""><input type="radio" name="aw1"
                                                    wire:model='aw1' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:10%;">
                                            <div class=""><input type="radio" name="aw1"
                                                    wire:model='aw1' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            Door-To-Pier
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_weight_2" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_weight_2"
                                                wire:model.defer='amount_weight_2'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_weight_2" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:4%;">
                                            <div class=""><input type="radio" name="aw2"
                                                    wire:model='aw2' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:10%;">
                                            <div class=""><input type="radio" name="aw2"
                                                    wire:model='aw2' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            Pier-To-Door
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_weight_3" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_weight_3"
                                                wire:model.defer='amount_weight_3'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_weight_3" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:4%;">
                                            <div class=""><input type="radio" name="aw3"
                                                    wire:model='aw3' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:10%;">
                                            <div class=""><input type="radio" name="aw3"
                                                    wire:model='aw3' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="font-semibold bg-white border-0">
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                            Door-To-Door
                                        </td>
                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                            <x-label for="amount_weight_4" value="" />
                                            <x-input class="w-28"
                                                style="border-top: none; border-left: none; border-right: none;"
                                                type="number" name="amount_weight_4"
                                                wire:model.defer='amount_weight_4'>
                                            </x-input>
                                            <x-input-error class="font-normal" for="amount_weight_4" />
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:4%;">
                                            <div class=""><input type="radio" name="aw4"
                                                    wire:model='aw4' value="1"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>

                                        <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:10%;">
                                            <div class=""><input type="radio" name="aw4"
                                                    wire:model='aw4' value="0"
                                                    class="w-5 h-5 border-2 border-grey">
                                            </div>
                                        </td>
                                    </tr>

                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>

                    <div class="col-span-12 mt-8">
                        <div wire:init="AncillaryReference" class="w-60">
                            <x-label for="ancillary_charge_id" value="Ancillary Charges" :required="true" />
                            <x-select style="cursor: pointer;" name="ancillary_charge_id"
                                wire:model='ancillary_charge_id'>
                                <option value=""></option>
                                @foreach ($ancillary_references as $ancillary_reference)
                                    <option value="{{ $ancillary_reference->id }}">
                                        {{ $ancillary_reference->name }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="ancillary_charge_id" />
                        </div>
                    </div>

                </div>

            @endif

            <div class="flex justify-end gap-3 mt-10 space-x-3">
                <x-button type="button" wire:click="$set('current_tab', 1)" title="Back"
                    class="px-12 bg-white text-blue hover:bg-gray-100" />
                <x-button type="button" wire:click="action({},'create_next_perview')" title="Submit"
                    class="px-12 bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>
    </form>
</div>
