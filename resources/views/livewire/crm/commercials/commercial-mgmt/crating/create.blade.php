<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
    current_tab: '{{ $current_tab }}',

}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to submit this rate table?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                No
                </button>
                <button type="button" wire:click="submit"
                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">

        <div x-cloak x-show="current_tab == 1">
            <h1 class="mb-3 text-2xl font-semibold text-left text-black "> Create New Base Rate </h1>


            <div class="grid grid-cols-12 gap-6 py-2">

                <div class="col-span-6">
                    <x-label for="dates" value="Date Created" />
                    <x-input disabled type="text" name="dates" wire:model.defer='dates' placholder="dates">
                    </x-input>
                    <x-input-error for="" />
                </div>
                <div class="col-span-6">
                    <x-label for="created_by" value="Created By" />
                    <x-input disabled type="text" name="created_by" wire:model.defer='created_by'></x-input>
                    <x-input-error for="created_by" />
                </div>

                <div class="col-span-6">
                    <x-label for="name" value="Rate Name" :required="true" />
                    <x-input type="text" name="name" maxlength="30" wire:model.defer='name'></x-input>
                    <x-input-error for="name" />
                </div>
                <div class="col-span-6">
                    <x-label for="effectivity_date" value="Rate Effectivity Date" :required="true" />
                    <x-input style="cursor: pointer;" type="date" name="effectivity_date"
                        wire:model.defer='effectivity_date'></x-input>
                    <x-input-error for="effectivity_date" />
                </div>

                <div class="col-span-12">
                    <x-label for="description" value="Rate Description" :required="true" />
                    <x-textarea rows="3" cols="50" maxlength="100" type="text" name="description"
                        wire:model.defer='description'>
                    </x-textarea>
                    <x-input-error for="description" />
                </div>

                <div class="col-span-12">
                    <h1 class="text-2xl font-semibold text-left text-black ">Rate Details</h1>
                </div>

                <div class="col-span-6">
                    <div wire:init="CrateTypeReference">
                        <x-label for="crating_type_id" value="Rate Type" :required="true" />
                        <x-select style="cursor: pointer;" name="crating_type_id" wire:model='crating_type_id'>
                            <option value=""></option>
                            @foreach ($crate_type_references as $crate_type_reference)
                                <option value="{{ $crate_type_reference->id }}">
                                    {{ $crate_type_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="crating_type_id" />
                    </div>
                </div>

                <div class="col-span-6">

                    <div wire:init="ApplyReference">
                        <x-label for="apply_for_id" value="Apply To" :required="true" />
                        <x-select style="cursor: pointer;" name="apply_for_id" wire:model='apply_for_id'>
                            <option value="">All</option>
                            @foreach ($apply_references as $apply_reference)
                                <option value="{{ $apply_reference->id }}">
                                    {{ $apply_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="apply_for_id" />
                    </div>
                </div>

            </div>

            <div class="flex justify-end gap-3 mt-6 space-x-3">
                <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                    class="px-12 bg-white text-blue hover:bg-gray-100" />
                <x-button type="button" wire:click="action({},'create_next')" title="Next"
                    class="px-14 bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>

        <div x-cloak x-show="current_tab == 2">

            <div class="grid grid-cols-12 py-2">
                <div class="col-span-12">
                    <h1 class="text-2xl font-semibold text-left text-black ">Rate Settings</h1>
                </div>

                <div class="col-span-12 overflow-auto text-left border-none" style="width: 25vw">
                    <div class="mt-2 border-none">
                        <x-table.table class="overflow-hidden">
                            {{-- <x-slot name="thead">
                               
                            </x-slot> --}}
                            <x-slot name="tbody">
                                {{-- @foreach ($rates as $a => $rate) --}}
                                <tr class="font-semibold bg-white border-none">
                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                        Rate Per Cubic Meter(CBM) :
                                    </td>
                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                        <x-label for="amount_per_cbm" value="" />
                                        <x-input class="w-36"
                                            style="border-top: none; border-left: none; border-right: none;"
                                            type="number" name="amount_per_cbm" wire:model.defer='amount_per_cbm'>
                                        </x-input>
                                        <x-input-error class="font-normal" for="amount_per_cbm" />
                                    </td>
                                </tr>
                                <tr class="font-semibold bg-white border-0">
                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                        Ancillary Charges :
                                    </td>
                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                        <x-label for="ancillary_charge" value="" />
                                        <x-input class="w-36"
                                            style="border-top: none; border-left: none; border-right: none;"
                                            type="number" name="ancillary_charge"
                                            wire:model.defer='ancillary_charge'>
                                        </x-input>
                                        <x-input-error class="font-normal" for="ancillary_charge" />
                                    </td>

                                </tr>

                                {{-- @endforeach --}}
                    </div>
                </div>
                </x-slot>


                </x-table.table>


            </div>
        </div>

        <div class="col-span-12 mt-6">
            <p class="p-2 text-sm font-semibold text-left text-blue ">Terms and Conditions / Notes</p>
            <p class="pl-8 text-sm font-medium text-left text-black ">1. Above rates are applicable for within city
                limits pick-up and delivery only.</p>
            <p class="pl-8 text-sm font-medium text-left text-black ">2. Special handling, hauling & unloading,
                man-power, lashing and incidental charges are not included.</p>
            <p class="pl-8 pr-4 text-sm font-medium text-left text-black ">3. In case of cargo-cancelation, 5% of the
                Total Charges will be applied or 10% of the Total Charges if CaPEx will return</p>
            <p class="pl-10 text-sm font-medium text-left text-black ">the cargo to the pick-up area.</p>
            <p class="pl-8 text-sm font-medium text-left text-black ">4. Storage Fee is Php50.00 per cbm per day.
            </p>
        </div>

</div>
<div class="flex justify-end gap-3 mt-6 space-x-3">
    <x-button type="button" wire:click="$set('current_tab', 1)" title="Back"
        class="px-12 bg-white text-blue hover:bg-gray-100" />
    <x-button type="button" wire:click="action({},'create_next_perview')" title="Submit"
        class="px-12 bg-blue text-white hover:bg-[#002161]" />
</div>
</div>
</form>
</div>
