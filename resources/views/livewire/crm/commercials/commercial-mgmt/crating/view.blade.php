<form wire:submit.prevent="submit" autocomplete="off">
    {{-- <div class="-pt-4">
        <h1 class="text-2xl font-semibold text-center text-blue "> {{$crate_s->description}} 
            </h1>
</div> --}}

<div class="grid grid-cols-12 px-2">
    <div class="col-span-5 text-left">
        <div class="mt-4 bg-white" style="">
            <x-table.table class="overflow-hidden border-none">
                <x-slot name="thead">
                </x-slot>
                <x-slot name="tbody">
                    <tr class="font-semibold border-0 bg-white">
                        <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                            <div>
                                Rate Name :
                            </div>
                        </td>
                        <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                            <div>
                                {{ $crate_s->name }}
                            </div>
                        </td>
                    </tr>
                    {{-- <tr class="font-semibold border-0 bg-white">
                        <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                            <div>
                                Commodity Type :
                            </div>
                        </td>
                        <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                            <div>
                                {{ $crate_s->CommodityTypeReference->name }}
                            </div>
                        </td>
                    </tr>
                    <tr class="font-semibold border-0 bg-white">
                        <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                            <div>
                                Service Mode :
                            </div>
                        </td>
                        <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                            <div>
                            </div>
                        </td>
                    </tr> --}}
                </x-slot>
            </x-table.table>
        </div>
    </div>
    <div class="col-span-2"></div>
    <div class="col-span-5 text-left">
        {{-- <div class="mt-4 bg-white" style="">
            <x-table.table class="overflow-hidden">
                <x-slot name="thead">
                </x-slot>
                <x-slot name="tbody">
                    <tr class="font-semibold border-0 bg-white">
                        <td class="w-1/6  text-gray-400 whitespace-nowrap" style="padding-left:;">
                            <div>
                                Origin :
                            </div>
                        </td>
                        <td class="w-1/6 whitespace-nowrap" style="padding-right:;">
                            <div>
                                @foreach ($crate_s->CratingtHasMany as $a => $sea)
                                {{ $sea->OriginDetails->code }}
                                @endforeach
                            </div>
                        </td>
                    </tr>
                    <tr class="font-semibold border-0 bg-white">
                        <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                            <div>
                                Vice Versa :
                            </div>
                        </td>
                        <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                            <div>
                                {{ $vice_versa_status }}
                            </div>
                        </td>
                    </tr>
                    <tr class="font-semibold border-0 bg-white">
                        <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                            <div>
                                Loading Type :
                            </div>
                        </td>
                        <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                            <div>
                                {{ $crate_s->ShipmentTypeReference->name }}
                            </div>
                        </td>
                    </tr>
                </x-slot>
            </x-table.table>
        </div> --}}
    </div>
</div>

<div class="grid grid-cols-12 px-2">
    <div class="col-span-12 text-left" >
        <div class="mt-4 bg-white border rounded-lg drop-shadow-lg">
            <x-table.table class="overflow-hidden">
                <x-slot name="thead">
                    <x-table.th name="No." style="padding-left:6%;" />
                    <x-table.th name="Rate Per Cubic Meter" style="padding-left:3.5%;" />
                    <x-table.th name="Ancillary Charge" style="padding-left:7.3%;" />
                </x-slot>

                <x-slot name="tbody">
                        <tr class="font-semibold border-0">
                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:6%;">
                                1.
                            </td>
                            
                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:5%;">
                                Php {{$crate_s->amount_per_cbm}}.00
                            </td>

                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:9%;">
                                {{$crate_s->ancillary_charge}}% VAT
                            </td>
                        </tr>
                </x-slot>
            </x-table.table>
        </div>
    </div>

    <div class="col-span-12 mt-6">
        <p class="p-1 text-sm font-semibold text-left text-blue whitespace-nowrap">TERMS AND CONDITIONS / NOTES :</p>
        <p class="text-sm font-medium text-left text-black whitespace-nowrap">1. Above rates are applicable for within city limits pick-up and delivery only.</p>
        <p class="text-sm font-medium text-left text-black whitespace-nowrap">2. Special handling, hauling & unloading, man-power, lashing and incidental charges are not included.</p>
        <p class="text-sm font-medium text-left text-black whitespace-nowrap">3. In case of cargo-cancelation, 5% of the Total Charges will be applied or 10% of the Total Charges if CaPEx will return</p>
        <p class="text-sm font-medium text-left text-black whitespace-nowrap">the cargo to the pick-up area.</p>
        <p class="text-sm font-medium text-left text-black whitespace-nowrap">4. Storage Fee is Php50.00 per cbm per day.</p>
    </div>

</div>
</form>