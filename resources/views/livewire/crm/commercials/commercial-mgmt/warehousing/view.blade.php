<form wire:submit.prevent="submit" autocomplete="off">
    {{-- <div class="-pt-4">
        <h1 class="text-2xl font-semibold text-center text-blue "> {{ $warehousing_s->description }}
        </h1>
    </div> --}}
    <div class="grid grid-cols-12 px-2">
        <div class="col-span-5 text-left">
            <div class="mt-4 bg-white" style="">
                <x-table.table class="overflow-hidden">
                    <x-slot name="tbody">
                        <tr class="font-semibold bg-white border-white">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Rate Name :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $warehousing_s->name }}
                                </div>
                            </td>
                        </tr>
                        {{-- <tr class="font-semibold bg-white border-none">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Commodity Type :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $warehousing_s->CommodityTypeReference->name }}
                                </div>
                            </td>
                        </tr> --}}
                        {{-- <tr class="font-semibold bg-white border-0">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Service Mode :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    
                                </div>
                            </td>
                        </tr> --}}
                    </x-slot>
                </x-table.table>
            </div>
        </div>
        <div class="col-span-2"></div>
        <div class="col-span-5 text-left">
            {{-- <div class="mt-4 bg-white" style="">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                    </x-slot>
                    <x-slot name="tbody">
                        <tr class="font-semibold bg-white border-0">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Origin :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-right:;">
                                <div>
                                    @foreach ($air_s->RateAirfreightHasMany as $a => $air)
                                    {{ $air->OriginDetails->code }}
                                    @endforeach
                                </div>
                            </td>
                        </tr>
                        <tr class="font-semibold bg-white border-0">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Vice Versa :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $vice_versa_status }}
                                </div>
                            </td>
                        </tr>
                        <tr class="font-semibold bg-white border-0">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Loading Type :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $air_s->ShipmentTypeReference->name }}
                                </div>
                            </td>
                        </tr>
                    </x-slot>
                </x-table.table>
            </div> --}}
        </div>
    </div>

    <div class="grid grid-cols-12 px-2">
        <div class="col-span-12 text-left">
            <div class="mt-4 bg-white border rounded-lg drop-shadow-lg">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="Options" style="padding-left:6%;" />
                        <x-table.th name="Rate per Unit" style="padding-left:5%;" />
                        <x-table.th name="Short-term Contract" style="padding-left:7.3%;" />
                        <x-table.th name="Long-term Contract" style="padding-left:7.3%;" />
                    </x-slot>

                    <x-slot name="tbody">
                        @foreach ($warehousing_s->RateWarehousingHasMany as $a => $warehouse)
                            <tr class="font-semibold border-0">
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:6%;">
                                    {{ $warehouse->WarehousingOptionDetails->option }}

                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:5%;">
                                    {{ $warehouse->WarehousingOptionDetails->rate_per_unit }}

                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:12%;">
                                    {{ $warehouse->amount_short_term }}

                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:12%;">
                                    {{ $warehouse->amount_long_term }}

                                </td>
                            </tr>
                        @endforeach

                    </x-slot>
                </x-table.table>
            </div>
        </div>

        <div class="grid grid-cols-12 px-2">
            <div class="col-span-12 text-left">
                <div style="width: 40vw">
                    <x-table.table class="overflow-hidden">
                        
                        <x-slot name="tbody" class="border-none">
                            <tr class="bg-white border-none font-sm">
                                <td class="w-1/6 text-blue whitespace-nowrap" style="padding-top:6%;">
                                    ANCILLARY CHARGES :
                                </td>

                                <td class="w-1/6 whitespace-nowrap" style="padding-left:5%; padding-top:6%;">

                                </td>
                            </tr>
                            <tr class="bg-white border-none font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    HANDLING IN :
                                </td>

                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="padding-left:2%;">
                                    Php120.00 / pallet of per cbm
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    HANDLING OUT :
                                </td>

                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="padding-left:2%;">
                                    Php120.00 / pallet of per cbm
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    STRIPPING FEE :
                                </td>

                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="padding-left:2%;">
                                    Php 4,000.00 per TEU
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    STUFFING FEE :
                                </td>

                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="padding-left:2%;">
                                    Php 4,000.00 per TEU
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    OVERTIME CHARGES :
                                </td>

                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="padding-left:2%;">
                                    Php 600.00 Per FTE per hour (Sundays and Holidays shall be subject to <br>
                                    additional applicable percentage)
                                </td>
                            </tr>
                        </x-slot>
                    </x-table.table>
                </div>
            </div>


            <div class="col-span-12 pb-4 mt-6">
                <p class="p-1 text-sm font-semibold text-left text-blue whitespace-nowrap">TERMS AND CONDITIONS / NOTES
                    :</p>
                <p class="text-sm font-medium text-left text-black whitespace-nowrap">1. Applicable for general cargo
                    only and non-temperature-controlled cargo.</p>
                <p class="text-sm font-medium text-left text-black whitespace-nowrap">2. Cargo that will require special
                    handling should be arranged with CaPEx and shall be subject to possible additional</p>
                <p class="pl-4 text-sm font-medium text-left text-black whitespace-nowrap">charges.</p>
                <p class="text-sm font-medium text-left text-black whitespace-nowrap">3. Basic warehousing report will
                    be provided by CaPEx based on the frequency required by the client.</p>
                <p class="text-sm font-medium text-left text-black whitespace-nowrap">4. Subject to other terms and
                    conditions of CaPEx as stated in its warehousing contract.</p>
            </div>

        </div>
</form>
