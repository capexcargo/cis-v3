<form wire:submit.prevent="submit" autocomplete="off">
    {{-- <div class="-pt-4">
        <h1 class="text-2xl font-semibold text-center text-blue "> {{ $air_s->description }}
        </h1>
    </div> --}}
    <div class="grid grid-cols-12 px-2">
        <div class="col-span-5 text-left">
            <div class="mt-4 bg-white" style="">
                <x-table.table class="overflow-hidden">
                    <x-slot name="tbody">
                        <tr class="font-semibold bg-white border-white">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Rate Name :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $air_s->name }}
                                </div>
                            </td>
                        </tr>
                        <tr class="font-semibold bg-white border-none">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Commodity Type :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $air_s->CommodityTypeReference->name }}
                                </div>
                            </td>
                        </tr>
                        <tr class="font-semibold bg-white border-0">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Service Mode :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    
                                </div>
                            </td>
                        </tr>
                    </x-slot>
                </x-table.table>
            </div>
        </div>
        <div class="col-span-2"></div>
        <div class="col-span-5 text-left">
            <div class="mt-4 bg-white" style="">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                    </x-slot>
                    <x-slot name="tbody">
                        <tr class="font-semibold bg-white border-0">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Origin :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-right:;">
                                <div>
                                    @foreach ($air_s->RateAirfreightHasMany as $a => $air)
                                    {{ $air->OriginDetails->code }}
                                    @endforeach
                                </div>
                            </td>
                        </tr>
                        <tr class="font-semibold bg-white border-0">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Vice Versa :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $vice_versa_status }}
                                </div>
                            </td>
                        </tr>
                        <tr class="font-semibold bg-white border-0">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Loading Type :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{-- {{ $air_s->ShipmentTypeReference->name }} --}}
                                </div>
                            </td>
                        </tr>
                    </x-slot>
                </x-table.table>
            </div>
        </div>
    </div>

    <div class="grid grid-cols-12 px-2">
        <div class="col-span-12 text-center">
            <div class="mt-4 bg-white border rounded-lg drop-shadow-lg" style="">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="Origin" style="padding-left:5.5%;" />
                        <x-table.th name="Destination" style="padding-left:3.5%;" />
                        <x-table.th name="VV" style="padding-left:5.3%;" />
                        <x-table.th name="0-5kgs" style="padding-left:5%;" />
                        <x-table.th name="6-49kgs" style="padding-left:4%;" />
                        <x-table.th name="50-249kgs" style="padding-left:4%;" />
                        <x-table.th name="250-999kgs" style="padding-left:%;" />
                        <x-table.th name="1000kgs-up" style="padding-left:%;" />
                    </x-slot>

                    <x-slot name="tbody">
                        @foreach ($air_s->RateAirfreightHasMany as $a => $air)
                            <tr class="font-semibold border-0">
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    <div>
                                        {{ $air->OriginDetails->code }}
                                    </div>
                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    <div>
                                        {{ $air->DestinationDetails->code }}
                                    </div>
                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $vice_versa_display }}
                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $air->amount_weight_1 }}.00
                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $air->amount_weight_2 }}.00
                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $air->amount_weight_3 }}.00

                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $air->amount_weight_4 }}.00

                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $air->amount_weight_5 }}.00

                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
            </div>
        </div>

            <div class="col-span-12 text-left">
                <div class="" style="width:auto;">
                    <x-table.table class="overflow-hidden">
                        <x-slot name="tbody">
                            <tr class="bg-white border-white font-sm">
                                <td class="w-1/6 text-blue whitespace-nowrap" style="padding-top:6%;">
                                    ANCILLARY CHARGES :
                                </td>
                                <td class="w-1/6 whitespace-nowrap" style="padding-left:5%; padding-top:6%;">
                                </td>
                            </tr>
                            <tr class="bg-white border-none font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    AWB FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    Php50.00 per air waybill
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    VALUATION FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    1% of Declared Value 
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    INSURANCE FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    Php2.00 / per Chargable Weight
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    HANDLINE FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    Php4.00/kgs (Visayas) Php5.00/kgs(Mindanao)
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    DELIVERY FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    Php5.00 / Chargable Weight
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    PERISHABLE FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    Php100.00 per air waybill (if applicable)
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    DG FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    Php100.00 per air waybill (if applicable)
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    COLLECT FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    Php30.00
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    ODA FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    Php1,500.00 first 5km, additional Php 40.00 per cbm/km for succeeding km<br>
                                    for 1cbm or less and Php20.00 percbm/k, for over 1cbm for succeeding km.
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    VAT :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="margin-left:-10%;">
                                    12% of Sub total
                                </td>
                            </tr>
                        </x-slot>
                    </x-table.table>
                </div>
            </div>

            <div class="col-span-12 pb-4 mt-6">
                <p class="p-2 text-sm font-medium text-left text-blue ">TERMS AND CONDITIONS / NOTES :</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">1. In Case of cargo-cancelation, 5% of the Total Cahrges will be applied or 10% of the Total Charges if CaPEx will</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">return the cargo to the pick-up area.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">2. Storage Fee is Php50.00 per cbm per day.</p>
            </div>

    </div>
</form>
