


<form wire:submit.prevent="submit" autocomplete="off">
    {{-- <div class="-pt-4">
        <h1 class="text-2xl font-semibold text-center text-blue "> {{$box_s->description}} 
            </h1>
</div> --}}
<div class="grid grid-cols-12 px-2">
    <div class="col-span-5 text-left">
        <div class="mt-4 bg-white" style="">
            <x-table.table class="overflow-hidden border-none">
                <x-slot name="thead">
                </x-slot>
                <x-slot name="tbody">
                    <tr class="font-semibold border-0 bg-white">
                        <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                            <div>
                                Rate Name :
                            </div>
                        </td>
                        <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                            <div>
                                {{ $box_s->name }}
                            </div>
                        </td>
                    </tr>
                    <tr class="font-semibold border-0 bg-white">
                        <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                            <div>
                                Commodity Type :
                            </div>
                        </td>
                        <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                            <div>
                                {{-- {{ $box_s->CommodityTypeReference->name }} --}}
                            </div>
                        </td>
                    </tr>
                    <tr class="font-semibold border-0 bg-white">
                        <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                            <div>
                                Service Mode :
                            </div>
                        </td>
                        <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                            <div>
                            </div>
                        </td>
                    </tr>
                </x-slot>
            </x-table.table>
        </div>
    </div>
    <div class="col-span-2"></div>
    <div class="col-span-5 text-left">
        <div class="mt-4 bg-white" style="">
            <x-table.table class="overflow-hidden">
                <x-slot name="thead">
                </x-slot>
                <x-slot name="tbody">
                    <tr class="font-semibold border-0 bg-white">
                        <td class="w-1/6  text-gray-400 whitespace-nowrap" style="padding-left:;">
                            <div>
                                Origin :
                            </div>
                        </td>
                        <td class="w-1/6 whitespace-nowrap" style="padding-right:;">
                            <div>
                                @foreach ($box_s->RateboxHasMany as $a => $box)
                                {{ $box->OriginDetails->code }}
                                @endforeach
                            </div>
                        </td>
                    </tr>
                    <tr class="font-semibold border-0 bg-white">
                        <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                            <div>
                                Vice Versa :
                            </div>
                        </td>
                        <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                            <div>
                                {{ $vice_versa_status }}
                            </div>
                        </td>
                    </tr>
                    <tr class="font-semibold border-0 bg-white">
                        <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                            <div>
                                Loading Type :
                            </div>
                        </td>
                        <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                            <div>
                                {{-- {{ $box_s->ShipmentTypeReference->name }} --}}
                            </div>
                        </td>
                    </tr>
                </x-slot>
            </x-table.table>
        </div>
    </div>
</div>

<div class="grid grid-cols-12 px-2">
    <div class="col-span-12 text-center" >
        <div class="mt-4 bg-white border rounded-lg drop-shadow-lg">
            <x-table.table class="overflow-hidden">
                <x-slot name="thead">
                    <x-table.th name="Origin" style="padding-left:6%;" />
                    <x-table.th name="Destination" style="padding-left:3.5%;" />
                    <x-table.th name="VV" style="padding-left:7.3%;" />
                    <x-table.th name="Small (3kg)" style="padding-left:4%;" />
                    <x-table.th name="Medium (5kg)" style="padding-left:3%;" />
                    <x-table.th name="Large (10kg)" style="padding-left:4%;" />
                </x-slot>

                <x-slot name="tbody">
              
                @foreach ($box_s->RateBoxHasMany as $a => $rate)
         
                        <tr class="font-semibold border-0">
                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{$rate->OriginDetails->code}}
                                </div>
                            </td>
                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                <div >
                                    {{$rate->DestinationDetails->code}}
                                </div>
                            </td>

                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                {{ $vice_versa_display }}
                            </td>

                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                {{$rate->amount_small}}.00
                            </td>

                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                {{$rate->amount_medium}}.00
                            </td>

                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                {{$rate->amount_large}}.00

                            </td>

                        </tr>
                @endforeach
                </x-slot>
            </x-table.table>
        </div>
    </div>

    <div class="col-span-12 mt-6">
        <p class="p-1 text-sm font-semibold text-left text-blue whitespace-nowrap">TERMS AND CONDITIONS / NOTES :</p>
        <p class="text-sm font-medium text-left text-black whitespace-nowrap">1. Maximum Declared Value per package is Php500.00</p>
        <p class="text-sm font-medium text-left text-black whitespace-nowrap">2. Declared Value higher than the above shall be subject to Valuation Fee of 1% for Express and 0.5% for </p>
        <p class="pl-4 text-sm font-medium text-left text-black whitespace-nowrap">Standard of Declared Value</p>
        <p class="text-sm font-medium text-left text-black whitespace-nowrap">3. Applicable for general cargo only.</p>
    </div>

</div>
</form>