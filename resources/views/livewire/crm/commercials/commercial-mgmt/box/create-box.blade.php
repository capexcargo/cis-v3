<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
    current_tab: '{{ $current_tab }}',

}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to submit this rate table?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                No
                </button>
                <button type="button" wire:click="submit"
                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                Yes
                </button>
            </div>
        </x-slot>
    </x-modal>
    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">

        <div x-cloak x-show="current_tab == 1">
            <h1 class="mb-3 text-2xl font-semibold text-left text-black "> Add Box Rate </h1>


            <div class="grid grid-cols-12 gap-6">

                <div class="col-span-6">
                    <x-label for="dates" value="Date Created" />
                    <x-input disabled type="text" name="dates" wire:model.defer='dates' placholder="dates">
                    </x-input>
                    <x-input-error for="" />
                </div>
                <div class="col-span-6">
                    <x-label for="created_by" value="Created By" />
                    <x-input disabled type="text" name="created_by" wire:model.defer='created_by'></x-input>
                    <x-input-error for="created_by" />
                </div>

                <div class="col-span-6">
                    <x-label for="name" value="Rate Name" :required="true" />
                    <x-input type="text" name="name" maxlength="30" wire:model.defer='name'></x-input>
                    <x-input-error for="name" />
                </div>
                <div class="col-span-6">
                    <x-label for="effectivity_date" value="Rate Effectivity Date" :required="true" />
                    <x-input type="date" name="effectivity_date" wire:model.defer='effectivity_date'></x-input>
                    <x-input-error for="effectivity_date" />
                </div>

                <div class="col-span-12">
                    <x-label for="description" value="Rate Description" :required="true" />
                    <x-textarea rows="3" cols="50" maxlength="100" type="text" name="description"
                        wire:model.defer='description'>
                    </x-textarea>
                    <x-input-error for="description" />
                </div>

                <div class="col-span-12">
                    <h1 class="text-2xl font-semibold text-left text-black ">Rate Details</h1>
                </div>

                <div class="col-span-6">
                    <div wire:init="RateTypeReference">
                        <x-label for="rate_type_id" value="Rate Type" :required="true" />
                        <x-select style="cursor: pointer;" name="rate_type_id" wire:model='rate_type_id'>
                            <option value=""></option>
                            @foreach ($rate_type_references as $rate_type_reference)
                                <option value="{{ $rate_type_reference->id }}">
                                    {{ $rate_type_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="rate_type_id" />
                    </div>
                </div>
                <div class="col-span-6">
                    <x-label for="transport_mode_id" value="Transport Mode" :required="true" />
                    <x-input disabled type="text" name="" wire:model.defer='transport_mode_id'></x-input>
                    <x-input-error for="transport_mode_id" />
                </div>

                <div class="col-span-12">

                    <div wire:init="BookingReference">
                        <x-label for="booking_type_id" value="Booking Type" :required="true" />
                        <x-select style="cursor: pointer;" name="booking_type_id" wire:model='booking_type_id'>
                            <option value=""></option>
                            @foreach ($booking_references as $booking_reference)
                                <option value="{{ $booking_reference->id }}">
                                    {{ $booking_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="booking_type_id" />
                    </div>
                </div>

                <div class="col-span-12">

                    <div wire:init="ApplyReference">
                        <x-label for="apply_for_id" value="Apply To" :required="true" />
                        <x-select style="cursor: pointer;" name="apply_for_id" wire:model='apply_for_id'>
                            <option value="">All</option>
                            @foreach ($apply_references as $apply_reference)
                                <option value="{{ $apply_reference->id }}">
                                    {{ $apply_reference->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="apply_for_id" />
                    </div>
                </div>

                <div class="col-span-12">

                        <x-label for="base_rate_id" value="Base Rate" :required="true" />
                        <x-select style="cursor: pointer;" name="base_rate_id" wire:model='base_rate_id'>
                            <option value=""></option>
                            @foreach ($bases as $base)
                                <option value="{{ $base->id }}">
                                    {{ $base->name }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="base_rate_id" />
                    </div>

            </div>

            <div class="flex justify-end gap-3 mt-6 space-x-3">
                <x-button type="button" wire:click="closecreatemodal" title="Cancel"
                    class="px-12 bg-white text-blue hover:bg-gray-100" />
                <x-button type="button" wire:click="action({},'create_next')" title="Next"
                    class="px-14 bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>

        <div x-cloak x-show="current_tab == 2">

            <div class="grid grid-cols-12">
                <div class="col-span-12">
                    <h1 class="text-2xl font-semibold text-left text-black ">Rate Settings</h1>
                </div>

                <div class="col-span-12 overflow-auto text-center" style="width: 22.5vw">
                    <div class="mt-2 ">
                        <x-table.table class="overflow-hidden">
                            <x-slot name="thead">
                                <x-table.th name="" style="padding-left:2%;" />
                                <x-table.th name="Amount*" style="padding-left:10%;" />
                                <x-table.th name="Add*" style="padding-left:9%;" />
                                <x-table.th name="Less*" style="padding-left:10%;" />
                            </x-slot>

                            <x-slot name="tbody">
                                <tr class="font-semibold bg-white border-0">
                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                        Small(3kg)
                                    </td>
                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                        <x-label for="amount_small" value="" />
                                        <x-input class="w-32"
                                            style="border-top: none; border-left: none; border-right: none;"
                                            type="number" name="amount_small" wire:model.defer='amount_small'>
                                        </x-input>
                                        <x-input-error class="font-normal" for="amount_small" />
                                    </td>

                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                        <div class=""><input type="radio" name="small"
                                                wire:model='small' value="1"
                                                class="w-5 h-5 border-2 border-grey">
                                        </div>
                                    </td>

                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:10%;">
                                        <div class=""><input type="radio" name="small"
                                                wire:model='small' value="0"
                                                class="w-5 h-5 border-2 border-grey">
                                        </div>
                                    </td>
                                </tr>

                                <tr class="font-semibold bg-white border-0">
                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                        Medium(5kg)
                                    </td>
                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                        <x-label for="amount_medium" value="" />
                                        <x-input class="w-32"
                                            style="border-top: none; border-left: none; border-right: none;"
                                            type="number" name="amount_medium" wire:model.defer='amount_medium'>
                                        </x-input>
                                        <x-input-error class="font-normal" for="amount_medium" />
                                    </td>

                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                        <div class=""><input type="radio" name="medium"
                                                wire:model='medium' value="1"
                                                class="w-5 h-5 border-2 border-grey">
                                        </div>
                                    </td>

                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:10%;">
                                        <div class=""><input type="radio" name="medium"
                                                wire:model='medium' value="0"
                                                class="w-5 h-5 border-2 border-grey">
                                        </div>
                                    </td>
                                </tr>

                                <tr class="font-semibold bg-white border-0">
                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                        Large(10kg)
                                    </td>
                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:;">
                                        <x-label for="amount_large" value="" />
                                        <x-input class="w-32"
                                            style="border-top: none; border-left: none; border-right: none;"
                                            type="number" name="amount_large" wire:model.defer='amount_large'>
                                        </x-input>
                                        <x-input-error class="font-normal" for="amount_large" />
                                    </td>

                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:2%;">
                                        <div class=""><input type="radio" name="large"
                                                wire:model='large' value="1"
                                                class="w-5 h-5 border-2 border-grey">
                                        </div>
                                    </td>

                                    <td class="w-1/6 p-1 whitespace-nowrap" style="padding-left:10%;">
                                        <div class=""><input type="radio" name="large"
                                                wire:model='large' value="0"
                                                class="w-5 h-5 border-2 border-grey">
                                        </div>
                                    </td>
                                </tr>


                            </x-slot>


                        </x-table.table>


                    </div>
                </div>

              

            </div>
            <div class="flex justify-end gap-3 mt-6 space-x-3">
                <x-button type="button" wire:click="$set('current_tab', 1)" title="Back"
                    class="px-12 bg-white text-blue hover:bg-gray-100" />
                <x-button type="button" wire:click="action({},'create_next_perview')" title="Submit"
                    class="px-12 bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>
    </form>
</div>
