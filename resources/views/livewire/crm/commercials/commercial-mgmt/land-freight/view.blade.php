<form wire:submit.prevent="submit" autocomplete="off">
    {{-- <div class="-pt-4">
        <h1 class="text-2xl font-semibold text-center text-blue "> {{ $land_s->description }}
        </h1>
    </div> --}}
    <div class="grid grid-cols-12 px-2">
        <div class="col-span-5 text-left">
            <div class="mt-4 bg-white" style="">
                <x-table.table class="overflow-hidden border-none">
                    <x-slot name="thead">
                    </x-slot>
                    <x-slot name="tbody">
                        <tr class="font-semibold border-0 bg-white">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Rate Name :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $land_s->name }}
                                </div>
                            </td>
                        </tr>
                        <tr class="font-semibold border-0 bg-white">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Commodity Type :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $land_s->CommodityTypeReference->name }}
                                </div>
                            </td>
                        </tr>
                        <tr class="font-semibold border-0 bg-white">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Service Mode :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                </div>
                            </td>
                        </tr>
                    </x-slot>
                </x-table.table>
            </div>
        </div>
        <div class="col-span-2"></div>
        <div class="col-span-5 text-left">
            <div class="mt-4 bg-white" style="">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                    </x-slot>
                    <x-slot name="tbody">
                        <tr class="font-semibold border-0 bg-white">
                            <td class="w-1/6  text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Origin :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-right:;">
                                <div>
                                    @foreach ($land_s->RatelandfreightHasMany as $a => $land)
                                    {{ $land->OriginDetails->code }}
                                    @endforeach
                                </div>
                            </td>
                        </tr>
                        <tr class="font-semibold border-0 bg-white">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Vice Versa :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $vice_versa_status }}
                                </div>
                            </td>
                        </tr>
                        <tr class="font-semibold border-0 bg-white">
                            <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    Loading Type :
                                </div>
                            </td>
                            <td class="w-1/6 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{-- {{ $land_s->ShipmentTypeReference->name }} --}}
                                </div>
                            </td>
                        </tr>
                    </x-slot>
                </x-table.table>
            </div>
        </div>
    </div>

    <div class="grid grid-cols-12 px-2">
        <div class="col-span-12 text-center">
            <div class="mt-4 bg-white border rounded-lg drop-shadow-lg" style="">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="Origin" style="padding-left:5.5%;" />
                        <x-table.th name="Destination" style="padding-left:3.5%;" />
                        <x-table.th name="VV" style="padding-left:6.3%;" />
                        <x-table.th name="Door-to-Door" style="padding-left:5%;" />
                        <x-table.th name="Door-to-Port" style="padding-left:5%;" />
                        <x-table.th name="Port-to-Door" style="padding-left:4%;" />
                        <x-table.th name="Port-to-Port" style="padding-left:%;" />
                    </x-slot>

                    <x-slot name="tbody">
                        @foreach ($land_s->RateLandfreightHasMany as $a => $land)
                            <tr class="font-semibold border-0">
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    <div>
                                        {{ $land->OriginDetails->code }}
                                    </div>
                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    <div>
                                        {{ $land->DestinationDetails->code }}
                                    </div>
                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $vice_versa_display }}
                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $land->amount_weight_1 }}.00
                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $land->amount_weight_2 }}.00
                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $land->amount_weight_3 }}.00

                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $land->amount_weight_4 }}.00

                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
            </div>
        </div>

            <div class="col-span-12 text-left">
                <div class="" style="width: 40vw; border: none">
                    <x-table.table class="overflow-hidden">
                        
                        <x-slot name="tbody">
                            <tr class="bg-white border-white font-sm">
                                <td class="w-1/6 text-blue whitespace-nowrap" style="padding-top:6%;">
                                    ANCILLARY CHARGES :
                                </td>
                                <td class="w-1/6 whitespace-nowrap" style="padding-left:5%; padding-top:6%;">
                                </td>
                            </tr>
                            <tr class="bg-white border-none font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    WAYBILL FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="padding-left:2%;">
                                    Php100.00 per waybill
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    VALUATION FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="padding-left:2%;">
                                    Php5.00 per Php 1,000.00 Declared Value (0.5%)
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    HANDLING FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="padding-left:2%;">
                                    Php1.50 per kgs. for cargoes over 50kgs per piece.
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    DG FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="padding-left:2%;">
                                    Php100.00 per commodity if applicable
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    COD FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="padding-left:2%;">
                                    Php30.00 per waybill
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    DOC FEE :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="padding-left:2%;">
                                    Php5.00 per waybill
                                </td>
                            </tr>
                            <tr class="bg-white border-0 font-sm">
                                <td class="w-1/6 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                    EVAT :
                                </td>
                                <td class="w-1/6 text-sm font-normal text-left text-black whitespace-nowrap"
                                    style="padding-left:2%;">
                                    12% of Sub total
                                </td>
                            </tr>
                        </x-slot>
                    </x-table.table>
                </div>
            </div>

            <div class="col-span-12 pb-4 mt-6">
                <p class="p-2 text-sm font-semibold text-left text-blue ">TERMS AND CONDITIONS / NOTES :</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">1. Minimum Base Freight of Php550.00 of
                    the above freight charge</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">2. Valuable Cargo is subject to an
                    additional Php500.00 per cbm of the base freight</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">3. Subject to weight or measurement rule
                    (W/M), per volumetric weight using 303 kilograms divisor or per</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">cubic meter whichever is higher</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">4. Applicable within city limits of origin
                    and destination ports only</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">5. Above rates are applicable for the
                    transportation of regular size and weight cargo which requires regular</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">handling only</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">6. Lashing, equipment rental, hauling,
                    additional manpower, equipment rental, and other special handling are</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">not included in the above rates.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">7. DG Cargo shall be required with MSDS
                    and shall be subject for approval If acceptable.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">8. Incidental cist is also not included in
                    the above charges.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">9. Subject to other terms and conditions
                    of Cargo Padala Express Forwarding Services Corp.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">10. Subject to Government and Third-Party
                    increases.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">11. Subject to change until further
                    notice.</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">12. Storage Fee of Php50.00 per cbm per
                    day shall apply for unclaimed cargoes after 48 hours from the time of</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">arrival subject to 12% VAT Pull-out Fee
                    of 5% of Total Freight Cost for cargoes to be picked-up at CaPEx</p>
                <p class="pl-8 text-sm font-medium text-left text-black ">office/warehouse or 10% of Total Freight
                    Cost for cargoes to be returned to the shipper (within city limits only)</p>
            </div>

    </div>
</form>
