<x-form wire:init="load" x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    create_land_modal: '{{ $create_land_modal }}',
    {{-- edit_modal: '{{ $edit_modal }}', --}}
    view_modal: '{{ $view_modal }}',
    reactivate_modal: '{{ $reactivate_modal }}',
    deactivate_modal: '{{ $deactivate_modal }}',
    approve1_modal: '{{ $approve1_modal }}',
    decline1_modal: '{{ $decline1_modal }}',
    approve2_modal: '{{ $approve2_modal }}',
    decline2_modal: '{{ $decline2_modal }}',

}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_commercials_commercial_mgmt_land_freight_add')
            @if ($create_modal)
                <x-modal id="create_modal" size="w-auto">
                    <x-slot name="body">
                        @livewire('crm.commercials.commercial-mgmt.land-freight.create')
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('crm_commercials_commercial_mgmt_land_freight_add_land')
            @if ($create_land_modal)
                <x-modal id="create_land_modal" size="w-auto">
                    <x-slot name="body">
                        @livewire('crm.commercials.commercial-mgmt.land-freight.create-land')
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        {{-- @can('crm_commercials_commercial_mgmt_pouch_edit')
            @if ($pouch_id && $edit_modal)
                <x-modal id="edit_modal" size="w-max">
                    <x-slot name="body">
                        @livewire('crm.commercials.commercial-mgmt.pouch.edit', ['id' => $pouch_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan --}}
        @can('crm_commercials_commercial_mgmt_land_freight_view')
            @if ($land_id && $view_modal)
                <x-modal id="view_modal" size="w-3/5">
                    <x-slot name="body">
                        @livewire('crm.commercials.commercial-mgmt.land-freight.view', ['id' => $land_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('crm_commercials_commercial_mgmt_land_freight_deactivate')
            <x-modal id="reactivate_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            Are you sure you want to approve this rate?
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('reactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">No</button>
                            <button wire:click="updateStatus('{{ $land_id }}', 2)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">Yes,
                                Approve</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="deactivate_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            Are you sure you want to deactivate this rate?
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('deactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">No</button>
                            <button wire:click="updateStatus('{{ $land_id }}', 4)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-red">Yes,
                                Deactivate</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan

        @can('crm_commercials_commercial_mgmt_land_freight_approver_1')
            <x-modal id="approve1_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            Are you sure you want to approve this rate?
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('approve1_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">No</button>
                            <button wire:click="updateApp1('{{ $land_id }}', 2)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">Yes,
                                Approve</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="decline1_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            Are you sure you want to decline this rate?
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('decline1_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">No</button>
                            <button wire:click="updateApp1('{{ $land_id }}', 3)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-red">Yes,
                                Decline</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan

        @can('crm_commercials_commercial_mgmt_land_freight_approver_2')
            <x-modal id="approve2_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            Are you sure you want to approve this rate?
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('approve2_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">No</button>
                            <button wire:click="updateApp2('{{ $land_id }}', 2)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">Yes,
                                Approve</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="decline2_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            Are you sure you want to decline this rate?
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('decline2_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">No</button>
                            <button wire:click="updateApp2('{{ $land_id }}', 3)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-red">Yes,
                                Decline</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan


    </x-slot>

    <x-slot name="header_title">Commercial Management</x-slot>
    <x-slot name="header_button">
        @can('crm_commercials_commercial_mgmt_land_freight_add')
            <button wire:click="action({}, 'add_rate')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Create New Base Rate
                </div>
            </button>
        @endcan


        @can('crm_commercials_commercial_mgmt_land_freight_add_land')
            <button wire:click="action({}, 'add_land')"
                class="p-2 px-3 mr-3 text-sm bg-white border-2 rounded-md text-blue border-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Add LF Rate
                </div>
            </button>
        @endcan


    </x-slot>
    <x-slot name="body">

        <div class="grid grid-cols-6 gap-4 ">
            <button wire:click="redirectTo({}, 'redirectToAFMgmt')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">AIR FREIGHT</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToAirFreightPremium')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">AIR FREIGHT<br>PREMIUM</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToBox')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">BOX</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToCrating')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">CRATING</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToLand')"
                class="w-auto py-4 text-gray-700 bg-white rounded-md" style="border: 4px solid darkblue;">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">LAND FREIGHT</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToLoa')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">LOA MANAGEMENT</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToPouch')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">POUCH</span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToSea')"
                class="w-auto p-4 text-gray-700 bg-white border rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">SEA FREIGHT</span>
                    <span class="pr-4"></span>
                </div>
            </button>
            <button wire:click="redirectTo({}, 'redirectToWarehousing')"
                class="w-auto p-4 text-gray-700 bg-white border border-solid rounded-md border-blue hover:bg-blue-100">
                <div class="items-center">
                    <span class="inline-block text-xl font-medium text-left">WAREHOUSING<BR>SERVICE RATE</span>
                </div>
            </button>

        </div>

        <div class="grid grid-cols-5 text-sm font-semibold" style="margin-top: 4%;">
            <div class="w-5/6">
                <div>
                    <x-transparent.input type="text" label="Rate Name" name="name"
                        wire:model.defer="name" />
                </div>
            </div>
            <div class="w-5/6">
                <div>
                    <x-transparent.input type="text" label="Rate Description" name="description"
                        wire:model.defer="description" />
                </div>
            </div>

            <div class="w-5/6">
                <div wire:init="statusReference">
                    <x-transparent.select value="{{ $final_status_id }}" label="Rate Status" name="final_status_id"
                        wire:model.defer="final_status_id">
                        <option value=""></option>
                        @foreach ($status_references as $status_references)
                            <option value="{{ $status_references->id }}">
                                {{ $status_references->name }}
                            </option>
                        @endforeach
                    </x-transparent.select>
                </div>
            </div>

            <div class="">
                <x-button type="button" wire:click="search" title="Search"
                    class="text-white bg-blue hover:bg-blue-800" />
            </div>
        </div>


        <div class="py-6">
            <div class="flex items-center justify-start">

                <ul class="flex mt-8">
                    @foreach ($status_header_cards as $i => $status_card)
                        <li class="px-3 py-1 text-sm {{ $status_header_cards[$i]['class'] }}"
                            :class="$wire.{{ $status_header_cards[$i]['action'] }} ==
                                '{{ $status_header_cards[$i]['id'] }}' ?
                                'bg-blue text-white' : ''">
                            <button
                                wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                {{ $status_header_cards[$i]['title'] }}
                                <span class="ml-6 text-sm">{{ $status_header_cards[$i]['value'] }}</span>
                            </button>
                        </li>
                    @endforeach
                </ul>

            </div>

            <div class="bg-white rounded-lg shadow-md">
                <x-table.table class="overflow-hidden">

                    @foreach ($land_s as $i => $land)
                        <x-slot name="thead">
                            <x-table.th name="No." style="padding-left:2%;" />
                            <x-table.th name="Rate Name" style="padding-left:%;" />
                            <x-table.th name="Rate Description" style="padding-left:%;" />
                            @if ($land->approver1_id == Auth::user()->id || Auth::user()->level_id == 5)
                                <x-table.th name="Status" style="padding-left:2%;" />
                            @endif
                            @if ($land->approver2_id == Auth::user()->id || Auth::user()->level_id == 5)
                                <x-table.th name="Status" style="padding-left:2%;" />
                            @endif
                            <x-table.th name="Rate Status" style="padding-left:;" />
                            @if ($land->approver1_id == Auth::user()->id || Auth::user()->level_id == 5)
                                <x-table.th name="Approver" style="padding-left:;" />
                            @endif
                            @if ($land->approver2_id == Auth::user()->id || Auth::user()->level_id == 5)
                                <x-table.th name="Approver" style="padding-left:;" />
                            @endif
                            <x-table.th name="Action" style="padding-left:-20%;" />
                        </x-slot>
                    @endforeach

                    <x-slot name="tbody">
                        @foreach ($land_s as $i => $land)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-normal">
                                <td class="w-24 p-3 whitespace-nowrap" style="padding-left:2%;">
                                    <p class="w-24">
                                        {{ ($land_s->currentPage() - 1) * $land_s->links()->paginator->perPage() + $loop->iteration }}
                                    </p>
                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $land->name }}
                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    <span wire:click="action({'id': {{ $land->id }}},'view') }}"
                                        class="underline cursor-pointer text-blue">{{ $land->description }}</span>
                                </td>

                                @if ($land->approver1_id == Auth::user()->id || Auth::user()->level_id == 5)
                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        <span
                                            class="{{ $land->approver1_status_id == 3
                                                ? 'text-orange bg-orange-light px-4'
                                                : ($land->approver1_status_id == 2
                                                    ? 'text-green bg-green-100 px-6'
                                                    : ($land->approver1_status_id == 1
                                                        ? 'text-blue bg-blue-100 px-5'
                                                        : ($land->approver1_status_id == 4
                                                            ? 'text-red bg-red-100 px-6'
                                                            : ''))) }} 
                                                            text-xs rounded-full p-1">
                                            {{ $land->App1StatusReference->name ?? '' }}
                                        </span>
                                    </td>
                                @endif

                                @if ($land->approver2_id == Auth::user()->id || Auth::user()->level_id == 5)
                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        <span
                                            class="{{ $land->approver2_status_id == 3
                                                ? 'text-orange bg-orange-light px-4'
                                                : ($land->approver2_status_id == 2
                                                    ? 'text-green bg-green-100 px-6'
                                                    : ($land->approver2_status_id == 1
                                                        ? 'text-blue bg-blue-100 px-5'
                                                        : ($land->approver2_status_id == 4
                                                            ? 'text-red bg-red-100 px-6'
                                                            : ''))) }} 
                                                                text-xs rounded-full p-1">
                                            {{ $land->App2StatusReference->name ?? '' }}
                                        </span>
                                    </td>
                                @endif

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    <span
                                        class="{{ $land->final_status_id == 3
                                            ? 'text-orange bg-orange-light px-4'
                                            : ($land->final_status_id == 2
                                                ? 'text-green bg-green-100 px-6'
                                                : ($land->final_status_id == 1
                                                    ? 'text-blue bg-blue-100 px-5'
                                                    : ($land->final_status_id == 4
                                                        ? 'text-red bg-red-100 px-6'
                                                        : ''))) }} 
                                                         text-xs rounded-full p-1">
                                        {{ $land->FinalStatusReference->name ?? '' }}</span>
                                </td>


                                @if ($land->approver1_id == Auth::user()->id || Auth::user()->level_id == 5)
                                    <td class="p-3" style="padding-left:;">
                                        <div class="flex gap-2">
                                            @if ($land->final_status_id != 4)
                                                @can('crm_commercials_commercial_mgmt_land_freight_approver_1')
                                                    <div title="Approve">
                                                        <svg wire:click="action({'id':{{ $land->id }}, 'final_status_id': 2},'update_app1') }}"
                                                            class="w-6 h-5 text-blue" aria-hidden="true"
                                                            focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                            role="img" xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 448 512">
                                                            <path fill="currentColor"
                                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                                                            </path>
                                                        </svg>
                                                    </div>
                                                    <div title="Decline">
                                                        <svg wire:click="action({'id':{{ $land->id }}, 'final_status_id': 3},'update_app1') }}"
                                                            class="w-6 h-5 text-red" aria-hidden="true" focusable="false"
                                                            data-prefix="fas" data-icon="circle-minus" role="img"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                            <path fill="currentColor"
                                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM175 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z">
                                                            </path>
                                                        </svg>
                                                    </div>
                                                @endcan
                                            @endif
                                        </div>
                                    </td>
                                @endif
                                @if ($land->approver2_id == Auth::user()->id || Auth::user()->level_id == 5)
                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        <div class="flex gap-2">
                                            @if ($land->final_status_id != 4)
                                                @can('crm_commercials_commercial_mgmt_land_freight_approver_2')
                                                    <div title="Approve">
                                                        <svg wire:click="action({'id':{{ $land->id }}, 'final_status_id': 2},'update_app2') }}"
                                                            class="w-6 h-5 text-blue" aria-hidden="true"
                                                            focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                            role="img" xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 448 512">
                                                            <path fill="currentColor"
                                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                                                            </path>
                                                        </svg>
                                                    </div>
                                                    <div title="Decline">
                                                        <svg wire:click="action({'id':{{ $land->id }}, 'final_status_id': 3},'update_app2') }}"
                                                            class="w-6 h-5 text-red" aria-hidden="true" focusable="false"
                                                            data-prefix="fas" data-icon="circle-minus" role="img"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                            <path fill="currentColor"
                                                                d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM175 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z">
                                                            </path>
                                                        </svg>
                                                    </div>
                                                @endcan
                                            @endif
                                        </div>
                                    </td>
                                @endif

                                <td class="w-1/6 p-3 whitespace-nowrap"
                                    style="padding-left:;"x-data="{ open: false }">
                                    <svg @click="open = !open"
                                        class="w-5 h-5 cursor-pointer text-grey hover:text-blue-800 "
                                        data-prefix="far" data-icon="approve" role="img"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 512">
                                        <path fill="currentColor"
                                            d="M64 360C94.93 360 120 385.1 120 416C120 446.9 94.93 472 64 472C33.07 472 8 446.9 8 416C8 385.1 33.07 360 64 360zM64 200C94.93 200 120 225.1 120 256C120 286.9 94.93 312 64 312C33.07 312 8 286.9 8 256C8 225.1 33.07 200 64 200zM64 152C33.07 152 8 126.9 8 96C8 65.07 33.07 40 64 40C94.93 40 120 65.07 120 96C120 126.9 94.93 152 64 152z">
                                        </path>
                                    </svg>
                                    <div class="" style="margin-top: -3%; margin-left:5%">
                                        <ul class="text-gray-600 bg-white rounded shadow" x-show="open"
                                            @click.away="open = false">
                                            <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }">
                                                @can('crm_commercials_commercial_mgmt_land_freight_view')
                                                    <a wire:click="action({'id': {{ $land->id }}},'view') }}">View
                                                        Details</a>
                                                @endcan
                                            </li>
                                            <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }">
                                                @can('crm_commercials_commercial_mgmt_land_freight_deactivate')
                                                    <a x-cloak x-show="'{{ $land->final_status_id == 2 }}'"
                                                        wire:click="action({'id':{{ $land->id }}, 'final_status_id': 4},'update_status') }}">Deacitavate</a>
                                                    <a x-cloak x-show="'{{ $land->final_status_id == 4 }}'"
                                                        wire:click="action({'id':{{ $land->id }}, 'final_status_id': 2},'update_status') }}">Activate</a>
                                                @endcan
                                            </li>
                                        </ul>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $land_s->links() }}
                </div>
            </div>

        </div>

    </x-slot>
</x-form>
