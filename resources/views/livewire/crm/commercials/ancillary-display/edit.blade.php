<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to update this ancillary charge item?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="grid grid-cols-12 gap-6" style="width:92%">
            <div class="col-span-8 w-52">
                <x-label for="name" value="Ancillary Charges Name" :required="true" />
                <x-input class="w-80" type="text" name="name" wire:model.defer='name'>
                </x-input>
                <x-input-error for="name" />
            </div>
        </div>
        <div class="col-span-12 text-center">

            <div class="mt-4 bg-white border rounded-lg drop-shadow-lg">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="No." style="padding-left:1%;" />
                        <x-table.th name="Ancillary Charges" style="padding-left:1%;" />
                        <x-table.th name="Description" style="padding-left:1%;" />
                        <x-table.th name="Action" style="padding-left:1%;" />
                    </x-slot>
                    <x-slot name="tbody">
                        @php $i = 1; @endphp
                        @foreach ($ancillarycharges as $a => $ancillarycharge)
                            @if (!$ancillarycharge['is_deleted'])
                                <tr>

                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $i }}
                                    </td>
                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        <div wire:init="AncillaryCharge">
                                            <x-select class="rounded-sm w-80" style="cursor: pointer;"
                                                name="ancillarycharges.{{ $a }}.ancillary_charge_id"
                                                wire:model='ancillarycharges.{{ $a }}.ancillary_charge_id'>
                                                <option value="">Select</option>
                                                @foreach ($ancillarychargeitems as $ancillarychargeitem)
                                                    <option value="{{ $ancillarychargeitem->id }}">
                                                        {{ $ancillarychargeitem->name }}
                                                    </option>
                                                @endforeach
                                            </x-select>
                                            <x-input-error class="font-normal"
                                                for="ancillarycharges.{{ $a }}.ancillary_charge_id" />
                                        </div>
                                    </td>
                                    <td class="p-3 w-80 whitespace-nowrap" style="padding-left:;">
                                        <div class="text-left rounded-sm w-80"
                                            name="ancillarycharges.{{ $a }}.description"
                                            wire:model.defer='ancillarycharges.{{ $a }}.description'>
                                            {{ $ancillarycharges[$a]['description'] }}</div>
                                    </td>
                                    <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                        <div class="flex space-x-2">
                                            @if ($max == $a)
                                                <div class="col-span-1">
                                                    <svg wire:click="addAncillaryDisplay()" class="h-6 w-7 text-blue"
                                                        aria-hidden="true" focusable="false" data-prefix="fas"
                                                        data-icon="trash-alt" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"
                                                        style="cursor: pointer">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                        </path>
                                                    </svg>
                                                </div>
                                            @else
                                                <div class="col-span-1">
                                                    <div class="h-6 w-7">&nbsp;</div>
                                                </div>
                                            @endif
                                            @if ($max > $min)
                                                <div class="col-span-1">
                                                    <svg wire:click="removeAncillary({'a': {{ $a }}})"
                                                        class="h-6 w-7 text-red" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="circle-minus" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"
                                                        style="cursor: pointer">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM184 232H328c13.3 0 24 10.7 24 24s-10.7 24-24 24H184c-13.3 0-24-10.7-24-24s10.7-24 24-24z">
                                                        </path>
                                                    </svg>
                                                </div>
                                            @endif



                                        </div>
                                    </td>
                                </tr>
                                @php
                                    $i++;
                                @endphp
                            @endif
                        @endforeach
                    </x-slot>
                </x-table.table>
            </div>
            <div class="flex justify-end gap-3 mt-6 space-x-3">
                <x-button type="button" wire:click="closecreatemodal" title="Back"
                    class="px-12 bg-white text-blue hover:bg-gray-100" />
                <x-button type="submit" title="Save"
                class="px-12 bg-blue text-white hover:bg-[#002161]" />
            </div>

        </div>
    </form>

</div>
