<div class="-pt-4">
    <h1 class="text-2xl font-semibold text-center text-blue "> {{ $name }}
    </h1>
</div>
<div class="grid grid-cols-12 px-2">
    <div class="col-span-12">
        <div class="mt-4 bg-white border rounded-lg drop-shadow-lg">
            <x-table.table class="overflow-hidden">
                <x-slot name="thead" class="flex">
                    <x-table.th name="Ancillary Charge" style="padding-left:;" class="w-2/5" />
                    <x-table.th name="Ancillary Description" style="padding-left:;" class="w-3/5" />
                </x-slot>

                <x-slot name="tbody">
                    @foreach ($ancillarydetails->AncillaryDisplayDetails as $a => $ancillarycharge)
                        <tr class="font-semibold border-0">
                            <td class="w-2/5 p-3 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $ancillarycharge->AncillaryCharge->name }}
                                </div>
                            </td>
                            <td class="w-3/5 p-3 whitespace-nowrap" style="padding-left:;">
                                <div>
                                    {{ $ancillarycharge->AncillaryCharge->description }}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </x-slot>
            </x-table.table>
        </div>
    </div>

</div>
