<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to update this ancillary charge?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes, Save
                </button>
            </div>
        </x-slot>
    </x-modal>


    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="col-span-12 text-center">
            <div class="mt-4 bg-white border rounded-lg drop-shadow-lg">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="Ancillary Charges name" style="padding-left:1%;" />
                        <x-table.th name="Charges Amount" style="padding-left:1%;" />
                        <x-table.th name="Charges Rate" style="padding-left:1%;" />
                        <x-table.th name="Description" style="padding-left:1%;" />
                    </x-slot>
                    <x-slot name="tbody">
                        <tr>
                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                <x-label for="name" value="" />
                                <x-input class="rounded-sm w-80" type="text" name="name"
                                    wire:model.defer='name'></x-input>
                                <x-input-error class="font-normal" for="name" />
                            </td>
                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                <x-label for="charges_amount" value="" />
                                <x-input class="w-32 rounded-sm" type="number" step="0.01" name="charges_amount"
                                    wire:model.defer='charges_amount' placeholder="0.00"></x-input>
                                <x-input-error class="font-normal" for="charges_amount" />
                            </td>
                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                <x-label for="charges_rate" value="" />
                                <x-input class="w-32 rounded-sm" type="number" step="0.001" name="charges_rate"
                                    wire:model.defer='charges_rate' placeholder="0.00%"></x-input>
                                <x-input-error class="font-normal" for="charges_rate" />
                            </td>
                            <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                <x-label for="description" value="" />
                                <x-input class="rounded-sm w-80" type="text" name="description"
                                    wire:model.defer='description'></x-input>
                                <x-input-error class="font-normal" for="description" />
                            </td>

                        </tr>

                    </x-slot>
                </x-table.table>


            </div>

            <div class="flex justify-end gap-3 mt-6 space-x-3">
                <x-button type="button" wire:click="closecreatemodal" title="Back"
                    class="px-12 bg-white text-blue hover:bg-gray-100" />
                <x-button type="submit" title="Save"
                class="px-12 bg-blue text-white hover:bg-[#002161]" />
            </div>
        </div>

    </form>

</div>
