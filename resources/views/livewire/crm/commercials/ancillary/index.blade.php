<x-form x-data="{
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    reactivate_modal: '{{ $reactivate_modal }}',
    deactivate_modal: '{{ $deactivate_modal }}',
}">

    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_commercials_ancillary_mgmt_add')
            <x-modal id="create_modal" size="w-max">
                <x-slot name="title">Add Ancillary Charges</x-slot>
                <x-slot name="body">
                    @livewire('crm.commercials.ancillary.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('crm_commercials_ancillary_mgmt_edit')
            @if ($ancillary_mgmt_id && $edit_modal)
                <x-modal id="edit_modal" size="w-max">
                    <x-slot name="title">Edit Ancillary Charges</x-slot>
                    <x-slot name="body">
                        @livewire('crm.commercials.ancillary.edit', ['id' => $ancillary_mgmt_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('crm_commercials_ancillary_mgmt_deactivate')
            <x-modal id="reactivate_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            Are you sure you want to reactivate this Ancillary Charge Name?
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('reactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">No</button>
                            <button wire:click="reactivate_stat('{{ $ancillary_mgmt_id }}', 1)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">Yes,
                                Reactivate</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="deactivate_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            Are you sure you want to deactivate this Ancillary Charge Name?
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('deactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">No</button>
                            <button wire:click="reactivate_stat('{{ $ancillary_mgmt_id }}', 2)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-red">Yes,
                                Deactivate</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
    </x-slot>



    <x-slot name="header_title">Ancillary Charges Management</x-slot>
    <x-slot name="header_button">
        @can('crm_commercials_ancillary_mgmt_add')
            <button wire:click="action({}, 'create')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Add Ancillary Charges
                </div>
            </button>
        @endcan

    </x-slot>

    <x-slot name="body">
        <div class="py-6">
            <div class="bg-white rounded-lg shadow-md">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="No." style="padding-left:2%;" />
                        <x-table.th name="Ancillary Charges Item" style="padding-left:%;" />
                        <x-table.th name="Charges Amount" style="padding-left:%;" />
                        <x-table.th name="Charges Rate" style="padding-left:;" />
                        <x-table.th name="Description" style="padding-left:;" />
                        <x-table.th name="Status" style="padding-left:;" />
                        <x-table.th name="Action" style="padding-left:-20%;" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($ancillary_charges as $i => $ancillary_charge)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-normal">
                                <td class="w-24 p-3 whitespace-nowrap" style="padding-left:2%;">
                                    <p class="w-24">
                                        {{ ($ancillary_charges->currentPage() - 1) * $ancillary_charges->links()->paginator->perPage() + $loop->iteration }}
                                    </p>
                                </td>
                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $ancillary_charge->name }}
                                </td>
                                <td class="w-1/6 p-3 text-right whitespace-nowrap" style="padding-right:5%;">
                                    @if($ancillary_charge->charges_amount == null)
                                        0
                                    @else
                                    {{ $ancillary_charge->charges_amount }}
                                    @endif
                                </td>
                                <td class="w-1/5 p-3 text-right whitespace-nowrap" style="padding-right:8.5%;">
                                    @if($ancillary_charge->charges_rate == null)
                                        0
                                    @else
                                    {{ $ancillary_charge->charges_rate }}
                                    @endif
                                </td>
                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $ancillary_charge->description }}
                                </td>
                                <td class="w-1/6 p-3 text-left whitespace-nowrap" style="padding-left:;">
                                    <span
                                        class="{{ $ancillary_charge->status == 1 ? 'text-green bg-green-100 px-6' : 'text-red bg-red-100 px-6' }} 
                                text-xs rounded-full p-1">
                                        {{ $ancillary_charge->status == 1 ? 'Active' : 'Deactivated' }} </span>
                                </td>
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:2%;"x-data="{ open: false }">
                                    <svg @click="open = !open"
                                        class="w-5 h-5 cursor-pointer text-grey hover:text-blue-800 " data-prefix="far"
                                        data-icon="approve" role="img" xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 128 512">
                                        <path fill="currentColor"
                                            d="M64 360C94.93 360 120 385.1 120 416C120 446.9 94.93 472 64 472C33.07 472 8 446.9 8 416C8 385.1 33.07 360 64 360zM64 200C94.93 200 120 225.1 120 256C120 286.9 94.93 312 64 312C33.07 312 8 286.9 8 256C8 225.1 33.07 200 64 200zM64 152C33.07 152 8 126.9 8 96C8 65.07 33.07 40 64 40C94.93 40 120 65.07 120 96C120 126.9 94.93 152 64 152z">
                                        </path>
                                    </svg>
                                    <div class="absolute" style="margin-top: -3.6%; margin-left: -6rem">
                                        <ul class="text-gray-600 bg-white rounded shadow" x-show="open"
                                            @click.away="open = false">
                                            <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }">
                                                @can('crm_commercials_ancillary_mgmt_edit')
                                                    <a
                                                        wire:click="action({'id': {{ $ancillary_charge->id }}},'edit') }}">Edit</a>
                                                @endcan
                                            </li>
                                            <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }">
                                                @can('crm_commercials_ancillary_mgmt_deactivate')
                                                    <a x-cloak x-show="'{{ $ancillary_charge->status == 1 }}'"
                                                        wire:click="action({'id':{{ $ancillary_charge->id }}, 'status': 2},'deactivate') }}">Deactivate</a>
                                                    <a x-cloak x-show="'{{ $ancillary_charge->status == 2 }}'"
                                                        wire:click="action({'id':{{ $ancillary_charge->id }}, 'status': 1},'deactivate') }}">Reactivate</a>
                                                @endcan
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $ancillary_charges->links() }}
                </div>
            </div>


    </x-slot>
</x-form>
