<x-crm.form x-data="{
    search_form: false,
    view_modal: '{{ $view_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        {{-- @can('crm_activity_reports_view') --}}
        @if ($agents_id && $view_modal)
            <x-modal id="view_modal" size="w-1/3">
                <x-slot name="title"></x-slot>
                <x-slot name="body">
                    @livewire('crm.reports.activity-reports.view', ['data' => $dataindexs])
                </x-slot>
            </x-modal>
        @endif
        {{-- @endcan --}}

    </x-slot>
    <x-slot name="header_title">Activity Reports</x-slot>
    <x-slot name="filter">
    </x-slot>

    <x-slot name="body">
        <div class="grid grid-cols-1 p-2 -mt-4">
            <div class="bg-white rounded-lg shadow-lg">
                <div class="grid grid-cols-1 gap-5 p-3">
                    <div class="grid grid-cols-2">
                        <div class="flex justify-between px-4">
                            <div class="text-2xl font-semibold text-blue">Team's Activities</div>
                        </div>
                        <div class="flex justify-end gap-8">
                            <div class="w-1/3">
                                <x-crm.input type="date" wire:model.defer="date_created_search"
                                    name="date_created_search" placeholder="">
                                </x-crm.input>
                            </div>
                            <div class="grid grid-cols-2">
                                <div>
                                    <x-crm.select wire:model.debounce.500ms="" name="" placeholder="">
                                        <option value="1">Daily</option>
                                        <option value="2">Weekly</option>
                                        <option value="3">Monthly</option>
                                    </x-crm.select>
                                </div>
                                <div class="ml-8">
                                    <x-crm.button type="button" wire:click="search" name="search" title="Search"
                                        class="text-white bg-blue hover:bg-blue-800" />
                                </div>
                            </div>
                            <div class="w-1/12 mt-2 font-semibold">
                                <h4 class="text-sm text-blue-800 underline cursor-pointer whitespace-nowrap underline-offset-4"
                                    wire:click="redirectTo({}, 'redirectToActivities')">
                                    See More
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="grid grid-cols-3 gap-6 p-6 mt-8">
                    @foreach ($agents as $a => $agent)
                        {{-- @if ($agents[$a]['service_req_count'] != 0) --}}
                        {{-- {{dd($agent->assignedTo->name);}} --}}
                        <div class="p-2 px-6 bg-white border rounded-lg shadow-lg ">
                            <div class="flex">
                                <div class="flex-none w-1/12 p-2 h-1/12">
                                    <div class="p-10 mt-4 bg-center bg-no-repeat bg-cover border rounded-full"
                                        style="background-image: url({{ Storage::disk('hrim_gcs')->url($agent->photo_path . $agent->photo_name) }})">
                                    </div>
                                </div>
                                <div class="flex-initial w-full mt-4 ml-2" style="padding-left: 25%">
                                    <ul class="list-none">
                                        <li class="text-lg font-semibold whitespace-normal">{{ $agent->name }}
                                        </li>
                                        <li class="mt-1 text-xs font-medium text-gray-400">Open Activities : <span
                                                class="font-semibold text-gray-600">{{ $agent->open }}</span></li>
                                        <li class="mt-1 text-xs font-medium text-gray-400">Closed Activities : <span
                                                class="font-semibold text-gray-600">{{ $agent->close }}</span></li>
                                        <li class="mt-1 text-xs font-medium text-gray-400">Total Activities : <span
                                                class="font-semibold text-gray-600">{{ $agent->total }}</span></li>
                                    </ul>
                                </div>
                                <div class="flex justify-end w-1/12 ">
                                    <h4 class="text-sm font-semibold text-blue-800 underline cursor-pointer underline-offset-4"
                                        wire:click="action({'id': {{ $agent->id }}},'view') }}">
                                        View
                                    </h4>
                                </div>
                            </div>

                        </div>
                        {{-- @endif --}}
                    @endforeach


                </div>

            </div>
        </div>


    </x-slot>

</x-crm.form>
