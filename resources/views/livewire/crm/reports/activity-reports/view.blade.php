<div x-data="{}">
    <x-loading></x-loading>

    <div class="mt-5 space-y-3">
        <div class="grid grid-cols-1">
            <div>
                <h6 class="-mt-4 text-xl font-semibold text-blue-800">{{ $name }}'s Activities</h6>


                <h6 class="mt-2 text-xs text-gray-400">
                    @if ($datefiltered != NULL)
                        {{ date('F d, Y', strtotime($datefiltered)) }}
                    @endif
                </h6>
            </div>
        </div>
        <div class="col-span-3 row-span-2 bg-white rounded-lg shadow-lg">
            <div class="overflow-auto text-center" style="height: auto">
                <div class="bg-white border border-gray-800 rounded-lg shadow-md">
                    <x-table.table class="overflow-hidden">
                        <x-slot name="thead">
                            <x-table.th name="Activities" />
                            <x-table.th name="Open" style="padding-left:4%;"/>
                            <x-table.th name="Closed" style="padding-left:4%;"/>
                            <x-table.th name="Total" style="padding-left:4%;" />
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($tasks as $v => $task)
                                <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                    <td class="p-3 text-left whitespace-nowrap">{{ $task->name }}</td>
                                    <td class="p-3 whitespace-nowrap">{{ $task->opened }}</td>
                                    <td class="p-3 whitespace-nowrap">{{ $task->closed }}</td>
                                    <td class="p-3 whitespace-nowrap">{{ $task->totals }}</td>
                                </tr>
                            @endforeach
                        </x-slot>
                    </x-table.table>
                </div>
            </div>
        </div>
    </div>


</div>
