<x-crm.form x-data="{
    search_form: false,
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
    </x-slot>
    <x-slot name="header_title">CDM Reports</x-slot>
    <x-slot name="body" size="w-10/12 xs:w-10/12 sm:w-10/12 md:w-10/12">
        <div class="grid grid-cols-1 gap-5 p-2">
            <div class="bg-white rounded-lg shadow-lg">
                <div class="grid grid-cols-1 gap-5 p-3">
                    <div class="grid grid-cols-2">
                        <div class="flex justify-between px-4">
                            <div class="text-2xl font-semibold text-blue">Overall Performance vs. Target</div>
                        </div>
                        <div class="flex justify-end gap-8">
                            <div class="w-56">
                                <x-crm.input type="date" wire:model.defer="date" name="date" placeholder="">
                                </x-crm.input>
                            </div>
                            <div class="grid grid-cols-2">
                                <div>
                                    <x-crm.select wire:model.defer="date_type" name="date_type" placeholder="date_type">
                                        <option value="">Daily</option>
                                        <option value="">Weekly</option>
                                        <option value="">Monthly</option>
                                        <option value="">Quarterly</option>
                                        <option value="">Yearly</option>
                                    </x-crm.select>
                                </div>
                                <div class="flex justify-start ml-8">
                                    <x-crm.button type="button" wire:click="" title="Search"
                                        class="text-white bg-blue hover:bg-blue-800" />
                                </div>
                            </div>
                            <div class="mt-2 font-semibold">
                                <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                    wire:click="action({},'')">
                                    See More
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="overflow-auto rounded-lg">
                    <div class="grid grid-cols-1 px-12">
                        <div class="mt-8 mb-6 overflow-hidden bg-white border border-gray-200 rounded-lg shadow-md">
                            <x-table.table class="overflow-hidden">
                                <x-slot name="thead">
                                    <tr class="text-xs text-left text-gray-500 bg-white cursor-pointer">
                                        <th class="border-r border-gray-400">
                                            <div class="px-8 font-normal text-gray-700">
                                                <div class="flex gap-2 mt-6 flex-nowrap">
                                                    Stakeholders Category
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="grid grid-cols-1 font-normal">
                                                <div class="grid grid-cols-12">
                                                    <div
                                                        class="col-span-4 text-sm text-center bg-gray-200 text-[#003399] py-1">
                                                        <p style="margin-right: 42px">TARGET</p>
                                                    </div>
                                                    <div
                                                        class="col-span-8 text-sm text-center bg-gray-200 text-[#003399] py-1">
                                                        OVERALL ACTUAL PERFORMANCE
                                                    </div>
                                                </div>
                                                <div class="grid grid-cols-12 py-1 text-black">
                                                    <div class="col-span-4">
                                                        <div class="flex justify-between px-12">
                                                            <div class="text-center whitespace-nowrap">
                                                                <p>Volume (Tons)</p>
                                                            </div>
                                                            <div class="text-center whitespace-nowrap">
                                                                <p style="margin-left:46px">Amount</p>
                                                            </div>
                                                            <div class="border-r border-gray-400"
                                                                style="margin: -36px 0 -5px 6px;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-span-8 px-4">
                                                        <div class="flex justify-between px-4">
                                                            <div class="whitespace-nowrap">
                                                                <p>Volume (Tons)</p>
                                                            </div>
                                                            <div class="whitespace-nowrap">
                                                                <p style="margin-left:4px">Percentage</p>
                                                            </div>
                                                            <div class="border-l border-gray-400"
                                                                style="margin: -4px -1px -5px -35px;">
                                                            </div>
                                                            <div class="whitespace-nowrap">
                                                                <p>Amount</p>
                                                            </div>
                                                            <div class="whitespace-nowrap">
                                                                <p class="">Percentage</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($stakeholder_categories as $i => $sh)
                                        <tr class="font-semibold">
                                            <td class="w-1/4 px-8 py-4 border-r border-gray-400">
                                                {{ $sh->stakeholder_category_name }}
                                            </td>
                                            <td>
                                                <div class="grid grid-cols-1">
                                                    <div class="grid grid-cols-12">
                                                        <div class="col-span-4">
                                                            <div class="flex justify-between px-12">
                                                                <div class="w-1/5 ml-4 whitespace-nowrap">
                                                                    <p>500</p>
                                                                </div>
                                                                <div class="w-4 border-l border-gray-400"
                                                                    style="margin: -16px 0 -16px 16px;">
                                                                </div>
                                                                <div class="w-1/5 whitespace-nowrap">
                                                                    <p>{{ number_format($sh->amnt, 2) }}</p>
                                                                </div>
                                                                <div class="border-r border-gray-400"
                                                                    style="margin: -16px 0 -16px 6px;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-span-8 px-4">
                                                            <div class="flex justify-between px-4">
                                                                <div class="w-12 whitespace-nowrap">
                                                                    <p>212.00</p>
                                                                </div>
                                                                <div class="w-6 ml-10 whitespace-nowrap">
                                                                    <p class="text-[#FF0000]">29%</p>
                                                                </div>
                                                                <div class="border-l border-gray-400"
                                                                    style="margin: -16px 8px -16px 9px;">
                                                                </div>
                                                                <div class="w-12 whitespace-nowrap">
                                                                    <p style="margin-left: -14px">
                                                                        {{ number_format($sh->act_amnt, 2) }}</p>
                                                                </div>
                                                                <div class="whitespace-nowrap">
                                                                    <p
                                                                        class="{{ ($sh->amnt / $sh->act_amnt) * 100 < 100 ? 'text-[#FF0000]' : 'text-green-400' }} mr-6">
                                                                        {{ number_format(($sh->amnt / $sh->act_amnt) * 100) }}%
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                                <x-slot name="tbody1">
                                    <tr class="text-sm font-medium text-left text-gray-500 bg-white cursor-pointer ">
                                        <th class="border-r border-gray-400">
                                            <div class="px-8 text-[#003399]">
                                                <div class="flex gap-2 py-3 flex-nowrap">
                                                    TOTAL :
                                                </div>
                                            </div>
                                        </th>
                                        <th>
                                            <div class="grid grid-cols-1 ">
                                                <div class="grid grid-cols-12 py-1 text-black">
                                                    <div class="col-span-4">
                                                        <div class="flex justify-between px-12">
                                                            <div class="w-1/5 ml-4 whitespace-nowrap">
                                                                <p>3,576.99</p>
                                                            </div>
                                                            <div class="w-4 border-l border-gray-400"
                                                                style="margin: -16px 0 -16px 16px;">
                                                            </div>
                                                            <div class="w-1/5 whitespace-nowrap">
                                                                <p>{{ number_format($total_target_amount, 2) }}</p>
                                                            </div>
                                                            <div class="border-r border-gray-400"
                                                                style="margin: -16px 0 -16px 6px;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-span-8 px-4">
                                                        <div class="flex justify-between px-4">
                                                            <div class="w-12 whitespace-nowrap">
                                                                <p>1,822.35</p>
                                                            </div>
                                                            <div class="w-4 whitespace-nowrap">
                                                                <p class="text-[#FF0000]"></p>
                                                            </div>
                                                            <div class="border-l border-gray-400"
                                                                style="margin: -16px -6px -16px -9px;">
                                                            </div>
                                                            <div class="w-12 whitespace-nowrap">
                                                                <p style="margin-left: -34px">
                                                                    {{ $actual_total_amount }}</p>
                                                            </div>
                                                            <div class="whitespace-nowrap">
                                                                <p class="text-[#FF0000]"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                </div>
            </div>
    </x-slot>
</x-crm.form>
