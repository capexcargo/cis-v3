<x-crm.form x-data="{
    search_form: false,
    view_total_resched_modal: '{{ $view_total_resched_modal }}'
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
        {{-- @can('crm_reports_booking_reports_view_total_resched') --}}
        @if ($resch_id && $view_total_resched_modal)
            <x-modal id="view_total_resched_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                <x-slot name="body">
                    @livewire('crm.reports.booking-reports.view-total-resched', ['data' => $resch_id])
                </x-slot>
            </x-modal>
        @endif
        {{-- @endcan --}}
    </x-slot>
    <x-slot name="header_title">Booking Reports</x-slot>
    <x-slot name="body" size="w-10/12 xs:w-10/12 sm:w-10/12 md:w-10/12">
        <div class="grid grid-cols-1 gap-5 p-2">
            <div class="bg-white rounded-lg shadow-lg">
                <div class="grid grid-cols-1 p-3">
                    <div class="grid grid-cols-2">
                        <div class="flex justify-between px-4">
                            <div class="text-2xl font-semibold text-blue">Booking History</div>
                        </div>

                        <div class="flex justify-end gap-8 px-2">
                            <div class="w-56">
                                <x-crm.input onchange="filterData()" type="date" wire:model.defer="BH_search"
                                    name="BH_search" placeholder="">
                                </x-crm.input>
                            </div>
                            <div class="grid grid-cols-2">
                                <div>
                                    <x-crm.select wire:model.defer="BH_searchs" name="BH_searchs" placeholder="">
                                        <option value="1">Daily</option>
                                        <option value="2">Weekly</option>
                                        <option value="3">Monthly</option>
                                    </x-crm.select>
                                </div>
                                <div class="flex justify-start ml-8">
                                    <x-crm.button type="button" wire:click="searchBH" title="Search"
                                        class="text-white bg-blue hover:bg-blue-800" />
                                </div>
                            </div>
                            <div class="mt-2 font-semibold">
                                <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                    wire:click="redirectTo({}, 'redirectToBooking')">
                                    See More
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-12 mt-6">
                        <div class="col-span-7 px-3">
                            <div class="grid grid-cols-2">
                                <div class="text-[#8D8D8D] text-lg font-semibold ml-1">
                                    <p>Booking Status</p>
                                    <p class="text-sm font-normal">TOTAL BOOKING : {{ $total }}</p>
                                </div>
                                <div class="flex gap-8">
                                    <div class="flex">
                                        <div class="w-4 h-4 mr-2 mt-1 rounded-sm bg-[#5485E5]"></div>Completed
                                    </div>
                                    <div class="flex">
                                        <div class="w-4 h-4 mr-2 mt-1 rounded-sm bg-[#6BCD60]"></div>Advanced
                                    </div>
                                    <div class="flex">
                                        <div class="w-4 h-4 mr-2 mt-1 rounded-sm bg-[#FFC0B6]"></div>Rescheduled
                                    </div>
                                    <div class="flex">
                                        <div class="w-4 h-4 mr-2 mt-1 rounded-sm bg-[#FA7A7A]"></div>Cancelled
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-1 p-6">
                        <div id="booking_history_chart" style="height: 32vh"></div>
                    </div>
                    <div class="grid grid-cols-1 p-3">
                        <div class="grid grid-cols-2">
                            <div class="text-[#8D8D8D] text-lg font-semibold ml-1">
                                <p>Booking Channels</p>
                                <p class="text-sm font-normal">Turn - Around Time</p>
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-12 pb-6" style="margin-bottom:-6.5rem">
                        <div class="col-span-3 px-3" style="margin-left: 20%">
                            {{-- <div class="flex"> --}}
                            {{-- <div class="text-[#8D8D8D] text-lg font-semibold ml-1">
                                    <canvas class="p-4" style="width: 200px; height: 320px; margin-left:10px; margin-top:-40px;"
                                        id="booking_channel_piechart"></canvas> --}}
                            {{-- <div id="booking_channel_piechart"
                                        style="width: 200px; height: 320px; margin-left:40px">
                                    </div> --}}
                            {{-- </div> --}}
                            <div class="w-60 h-60 -mt-4">
                                <canvas class="p-4" style="height:100% ; width:100%;"
                                    id="booking_channel_piechart"></canvas>
                            </div>
                            {{-- </div> --}}
                        </div>
                        <div class="col-span-5" style="margin-left: 15%">
                            <div class="grid grid-cols-2 py-10" style="margin: -5rem 0 4rem -5rem; gap: 6rem;">
                                <div class="whitespace-nowrap" style="margin-top: 3.2rem">
                                    <div class="flex" style="margin-left: -1.6rem">
                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#1F78B4]" style="margin-top: 8px">
                                        </div>

                                        <h1 class="text-xl">{{ $labelcis3 }}</h1>
                                    </div>
                                    <p class="text-sm font-normal text-gray-400">Total Bookings :
                                        <span class="font-semibold text-black" id="cis3">{{ $totalcis3 }}</span>
                                    </p>
                                    <p class="text-sm font-normal text-gray-400">Average Completion Time :
                                        <span class="font-semibold text-black"> {{ round($avgcis3) }}min
                                    </p>

                                </div>
                                <div class="whitespace-nowrap" style="margin-top: 3.2rem">
                                    <div class="flex" style="margin-left: -1.6rem">
                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#A6CEE3]" style="margin-top: 8px">
                                        </div>

                                        <h1 class="text-xl">{{ $labelweb }}</h1>
                                    </div>
                                    <p class="text-sm font-normal text-gray-400">Total Bookings :
                                        <span class="font-semibold text-black" id="web">{{ $totalweb }}</span>
                                    </p>
                                    <p class="text-sm font-normal text-gray-400">Average Completion Time :
                                        <span class="font-semibold text-black"> {{ round($avgweb) }}min
                                    </p>
                                </div>
                                <div class="whitespace-nowrap" style="margin-top: -3rem">
                                    <div class="flex" style="margin-left: -1.6rem">
                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#77D26D]" style="margin-top: 8px">
                                        </div>
                                        <h1 class="text-xl">{{ $labelmobile }}</h1>
                                    </div>
                                    <p class="text-sm font-normal text-gray-400">Total Bookings :
                                        <span class="font-semibold text-black" id="mobile">{{ $totalmobile }}</span>
                                    </p>
                                    <p class="text-sm font-normal text-gray-400">Average Completion Time :
                                        <span class="font-semibold text-black"> {{ round($avgmob) }}min
                                    </p>
                                </div>
                                <div class="whitespace-nowrap" style="margin-top: -3rem">
                                    <div class="flex" style="margin-left: -1.6rem">
                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#9CE993]" style="margin-top: 8px">
                                        </div>
                                        <h1 class="text-xl">{{ $labelvip }}</h1>
                                    </div>
                                    <p class="text-sm font-normal text-gray-400">Total Bookings :
                                        <span class="font-semibold text-black"
                                            id="vip">{{ $totalvip }}</span>
                                    </p>
                                    <p class="text-sm font-normal text-gray-400">Average Completion Time :
                                        <span class="font-semibold text-black"> {{ round($avgvip) }}min
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-cols-1 gap-5 p-2">
            <div class="bg-white rounded-lg shadow-lg">
                <div class="grid grid-cols-1 gap-5 p-3">
                    <div class="grid grid-cols-12">
                        <div class="col-span-3 px-4">
                            <div class="text-2xl font-semibold text-blue">Booking Status Reports</div>
                        </div>
                        <div class="col-span-9">
                            <div class="flex justify-end gap-8 px-2">
                                <div class="w-72">

                                    <div>
                                        <x-crm.input onchange="filterData()" type="text"
                                            wire:model.defer="RR_search" name="RR_search"
                                            placeholder="Booking Reference No.">
                                        </x-crm.input>
                                    </div>

                                </div>
                                <div class="w-56">
                                    <x-crm.input onchange="filterData()" type="date" wire:model.defer="CR_search"
                                        name="CR_search" placeholder="">
                                    </x-crm.input>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div>
                                        <x-crm.select wire:model.defer="CR_searchs" name="CR_searchs" placeholder="">
                                            <option value="1">Daily</option>
                                            <option value="2">Weekly</option>
                                            <option value="3">Monthly</option>
                                        </x-crm.select>
                                    </div>
                                    <div class="flex justify-start ml-8">
                                        <x-crm.button type="button" wire:click="searchCR" title="Search"
                                            class="text-white bg-blue hover:bg-blue-800" />
                                    </div>
                                </div>
                                <div class="mt-2 ml-4 font-semibold">
                                    <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                        wire:click="action({},'see_details_emp_info')">
                                        See More
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-12 gap-5 p-3">
                        <div class="col-span-5">
                            <div class="text-[#8D8D8D] text-lg font-semibold ml-1">
                                <p>Reschedule Report</p>
                                <p class="text-sm font-normal">TOTAL NO. OF RESCHEDULED REPORTS : {{ $totalresched }}
                                </p>
                            </div>
                            <div class="mt-3 overflow-auto drop-shadow-xl">
                                <div class="bg-white border border-gray-400 rounded-lg shadow-md"
                                    style="height: 36vh">
                                    <x-table.table>
                                        <x-slot name="thead">
                                            <x-table.th name="Booking Reference No." />
                                            <x-table.th name="Total Rescheduled" />
                                            <x-table.th name="Reason" />
                                        </x-slot>
                                        <x-slot name="tbody">
                                            @foreach ($rescheduled as $a => $reschedule)
                                                <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ $reschedule->booking_reference_no }}

                                                        {{-- @dd($rescheduled[$a]['booking_logs_has_many_b_k_count']) --}}
                                                    </td>
                                                    {{-- @dd(); --}}

                                                    <td class="p-3 whitespace-nowrap">
                                                        <h4 class="underline text-[#003399] text-center cursor-pointer"
                                                            wire:click="action({'id': {{ $reschedule->id }}}, 'view_total_resched')">

                                                            {{ $reschedule->booking_logs_has_many_b_k_count }}
                                                            {{-- {{ count($rescheduled[$a]['BookingLogsHasManyBK']) }} --}}

                                                        </h4>
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        @foreach ($rescheduled[$a]['BookingLogsHasManyBK'] as $b => $resch)
                                                            {{-- @dd($resch) --}}

                                                            {{ $resch->ReschedReasonReferenceLG->name }}
                                                            {{-- @dd($reschedule->BookingLogsHasManyBK[$a]['ReschedReasonReferenceLG']['name']) --}}
                                                            {{-- {{$reschedule->BookingLogsHasManyBK->ReschedReasonReferenceLG->name}} --}}
                                                        @endforeach
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </x-slot>
                                    </x-table.table>
                                </div>
                            </div>
                        </div>

                        <div class="col-span-7">
                            <div class="text-[#8D8D8D] text-lg font-semibold ml-1">
                                <p>Cancellation Report</p>
                                <p class="text-sm font-normal">TOTAL NO. OF CANCELLED REPORTS : {{ $totalcancelled }}
                                </p>
                            </div>
                            <div class="mt-3 overflow-hidden drop-shadow-xl">
                                <div class="bg-white border border-gray-400 rounded-lg shadow-md">
                                    <div class="grid grid-cols-12">
                                        <div class="col-span-3 px-3" style="margin-top: 50%">
                                            <div class="w-60 h-60 -mt-4">
                                                <canvas class="p-4" style="height:100% ; width:100%;"
                                                    id="cancellation_report_piechart"></canvas>
                                            </div>
                                        </div>
                                        <div class="col-span-9 ml-12 -mt-4">
                                            <div class="grid grid-cols-2 py-10">
                                                <div class="w-4/5">
                                                    <div class="flex mt-3">
                                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#1F78B4]"
                                                            style="margin-top: 12px">
                                                        </div>
                                                        <div class="grid grid-cols-1">
                                                            <div class="text-sm">{{ $reason1 }}</div>
                                                            <div class="text-sm" id="r1">{{ $reason1count }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="flex mt-3">
                                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#A6CEE3]"
                                                            style="margin-top: 12px">
                                                        </div>
                                                        <div class="grid grid-cols-1">
                                                            <div class="text-sm">{{ $reason2 }}</div>
                                                            <div class="text-sm" id="r2">{{ $reason2count }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="flex mt-3">
                                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#B78FFF]"
                                                            style="margin-top: 12px">
                                                        </div>
                                                        <div class="grid grid-cols-1">
                                                            <div class="text-sm">{{ $reason3 }}</div>
                                                            <div class="text-sm" id="r3">{{ $reason3count }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="flex mt-3">
                                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#D1B8FF]"
                                                            style="margin-top: 12px">
                                                        </div>
                                                        <div class="grid grid-cols-1">
                                                            <div class="text-sm">{{ $reason4 }}</div>
                                                            <div class="text-sm" id="r4">{{ $reason4count }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="flex mt-3">
                                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#FA7A7A]"
                                                            style="margin-top: 12px">
                                                        </div>
                                                        <div class="grid grid-cols-1">
                                                            <div class="text-sm">{{ $reason5 }}
                                                            </div>
                                                            <div class="text-sm" id="r5">{{ $reason5count }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="w-4/5">
                                                    <div class="flex mt-3">
                                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#FFC0B6]"
                                                            style="margin-top: 12px">
                                                        </div>
                                                        <div class="grid grid-cols-1">
                                                            <div class="text-sm">{{ $reason6 }}</div>
                                                            <div class="text-sm" id="r6">{{ $reason6count }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="flex mt-3">
                                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#FFB055]"
                                                            style="margin-top: 12px">
                                                        </div>
                                                        <div class="grid grid-cols-1">
                                                            <div class="text-sm">{{ $reason7 }}</div>
                                                            <div class="text-sm" id="r7">{{ $reason7count }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="flex mt-3">
                                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#FFD4A2]"
                                                            style="margin-top: 12px">
                                                        </div>
                                                        <div class="grid grid-cols-1">
                                                            <div class="text-sm">{{ $reason8 }}</div>
                                                            <div class="text-sm" id="r8">{{ $reason8count }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="flex mt-3">
                                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#FFE6A0]"
                                                            style="margin-top: 24px">
                                                        </div>
                                                        <div class="grid grid-cols-1">
                                                            <div class="text-sm">{{ $reason9 }}</div>
                                                            <div class="text-sm" id="r9">{{ $reason9count }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="flex mt-3">
                                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#77D26D]"
                                                            style="margin-top: 12px">
                                                        </div>
                                                        <div class="grid grid-cols-1">
                                                            <div class="text-sm">{{ $reason10 }}</div>
                                                            <div class="text-sm" id="r10">{{ $reason10count }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="flex mt-3">
                                                        <div class="w-3 h-3 mr-3 rounded-sm bg-[#9CE993]"
                                                            style="margin-top: 12px">
                                                        </div>
                                                        <div class="grid grid-cols-1">
                                                            <div class="text-sm">{{ $reason11 }}</div>
                                                            <div class="text-sm" id="r11">{{ $reason11count }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-12 gap-5 p-3">
                        <div class="col-span-6">
                            <div class="text-[#8D8D8D] text-lg font-semibold ml-1">
                                <p>Pick up Activity Completion Rate</p>
                                <p class="text-sm font-normal">TOTAL BOOKINGS : {{ $totalpickup }}</p>
                            </div>
                        </div>
                        <div class="col-span-6">
                            <div class="flex justify-end gap-8 px-2">
                                <div class="w-56">
                                    <x-crm.input onchange="filterData()" type="date"
                                        wire:model.defer="PACR_search" name="PACR_search" placeholder="">
                                    </x-crm.input>
                                </div>
                                <div class="grid grid-cols-2">
                                    <div>
                                        <x-crm.select wire:model.defer="" name="" placeholder="">
                                            <option value="">Daily</option>
                                            <option value="">Weekly</option>
                                            <option value="">Monthly</option>
                                        </x-crm.select>
                                    </div>
                                    <div class="flex justify-start ml-8">
                                        <x-crm.button type="button" wire:click="searchPACR" title="Search"
                                            class="text-white bg-blue hover:bg-blue-800" />
                                    </div>
                                </div>
                                <div class="mt-2 font-semibold">
                                    <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                        wire:click="action({},'see_details_emp_info')">
                                        See More
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-1">
                        <div id="pickup_activity_chart" style="width: 600px; height: 440px;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-cols-1 p-2">
            <div class="grid grid-cols-2 gap-6">
                <div class="grid grid-cols-1 bg-white rounded-lg shadow-lg">
                    <div class="grid grid-cols-2 px-6 py-2">
                        <div class="text-[#8D8D8D] text-lg font-semibold ml-1">
                            <p>Booking per Agent</p>
                            <p class="text-sm font-normal">TOTAL BOOKING : {{ $totalbook }}</p>
                        </div>
                        <div>
                            <div class="flex justify-end gap-4">
                                <div class="w-56">
                                    <x-crm.input onchange="filterData()" type="date" wire:model.defer="BPA_search"
                                        name="BPA_search" placeholder="">
                                    </x-crm.input>
                                </div>
                                <div class="font-semibold">
                                    <x-crm.button type="button" wire:click="searchBPA" title="Search"
                                        class="text-white bg-blue hover:bg-blue-800" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-1 px-4 pb-4">
                        <div class="overflow-auto drop-shadow-xl">
                            <div class="bg-white border border-gray-400 rounded-lg shadow-md" style="height: 30vh">
                                <x-table.table>
                                    <x-slot name="thead">
                                        <x-table.th name="Name" />
                                        <x-table.th name="New Customers" />
                                        <x-table.th name="Existing Customers" />
                                        <x-table.th name="Total Booking" />
                                    </x-slot>
                                    <x-slot name="tbody">
                                        @foreach ($bookingperagents as $c => $bookingperagent)
                                            @if ($bookingperagent->bookingscreated_count != 0)
                                                <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ $bookingperagent->name }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ $bookingperagent->bookingscreated_count }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        64
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        117
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </x-slot>
                                </x-table.table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-white rounded-lg shadow-lg">
                    <div class="grid grid-cols-2 px-8 py-2">
                        <div class="text-[#8D8D8D] text-lg font-semibold">
                            <p>Booking per Branch</p>
                            <p class="text-sm font-normal">TOTAL BOOKING : {{ $totalbookb }}</p>
                        </div>
                        <div>
                            <div class="flex justify-end gap-4">
                                <div class="w-56">
                                    <x-crm.input onchange="filterData()" type="date" wire:model.defer="BPB_search"
                                        name="BPB_search" placeholder="">
                                    </x-crm.input>
                                </div>
                                <div class="font-semibold">
                                    <x-crm.button type="button" wire:click="searchBPB" title="Search"
                                        class="text-white bg-blue hover:bg-blue-800" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-1 px-4">
                        <div class="overflow-auto drop-shadow-xl">
                            <div class="bg-white border border-gray-400 rounded-lg shadow-md" style="height: 30vh">
                                <x-table.table>
                                    <x-slot name="thead">
                                        <x-table.th name="Branch" />
                                        <x-table.th name="Total Booking" />
                                    </x-slot>
                                    <x-slot name="tbody">
                                        @foreach ($bookingperbranches as $v => $bookingperbranch)
                                            @if ($bookingperbranch->bookings_count != 0)
                                                <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ $bookingperbranch->code }}
                                                    </td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        {{ $bookingperbranch->bookings_count }}
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </x-slot>
                                </x-table.table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-cols-1 p-2">
            <div class="grid grid-cols-12 gap-6">
                <div class="col-span-7">
                    <div class="grid grid-cols-1">
                        <div class="grid grid-cols-1 px-6 py-2 bg-white rounded-lg shadow-lg">
                            <div class="text-[#8D8D8D] text-lg font-semibold ml-1">Booking Volume per Channel</div>
                            <div class="flex justify-end gap-4 mt-4">
                                <div class="w-64">
                                    <x-crm.input onchange="filterData()" type="date"
                                        wire:model.defer="BVPC_search" name="BVPC_search" placeholder="">
                                    </x-crm.input>
                                </div>
                                <div class="w-32">
                                    <x-crm.select wire:model.debounce.500ms="" name="" placeholder="">
                                        <option value="">All</option>
                                        <option value="">PP</option>
                                        <option value="">COD</option>
                                        <option value="">PTD</option>
                                        <option value="">CS</option>
                                        <option value="">CC</option>
                                    </x-crm.select>
                                </div>
                                <div class="w-32 mr-2">
                                    <x-crm.select wire:model.debounce.500ms="" name="" placeholder="">
                                        <option value="">All</option>
                                        <option value="">Bacolod</option>
                                        <option value="">Bambang</option>
                                        <option value="">Butuan</option>
                                        <option value="">Cagayan</option>
                                        <option value="">Cebu</option>
                                    </x-crm.select>
                                </div>
                                <div>
                                    <x-crm.button type="button" wire:click="searchBVPC" title="Search"
                                        class="text-white bg-blue hover:bg-blue-800" />
                                </div>
                            </div>
                            <div class="grid grid-cols-1 mt-6 mb-4">
                                <div class="overflow-auto bg-white rounded-lg shadow-lg">
                                    <div class="bg-white border border-gray-400 rounded-lg rounded-b-none shadow-md"
                                        style="height: 33vh">
                                        <x-table.table>
                                            <x-slot name="thead">
                                                <tr class="text-xs text-left text-gray-500 bg-white cursor-pointer">
                                                    <th class="w-1/2 border-r border-gray-400">
                                                        <div class="px-2 font-normal text-gray-700">
                                                            <div class="flex gap-20">
                                                                <span class="w-40 text-center"
                                                                    style="margin-left: -3rem">Channel</span>
                                                                <span class="w-12 text-center">No. of Bookings</span>
                                                                <span class="w-12 text-center">No. of Waybills</span>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th class="w-3/4">
                                                        <div class="grid grid-cols-1 font-normal text-gray-700">
                                                            <div class="text-center">Volume</div>
                                                            <div
                                                                class="flex justify-center gap-24 text-xs border-t border-gray-400">
                                                                <div class="flex whitespace-nowrap">
                                                                    <p>Air (CWT in kgs)</p>
                                                                </div>
                                                                <div class="flex whitespace-nowrap">
                                                                    <p>Sea (CBM)</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th class="w-1/3 border-l border-gray-400">
                                                        <div class="w-24 px-2 font-normal text-center text-gray-700">
                                                            <div>Total Volume (tons)</div>
                                                        </div>
                                                    </th>
                                                </tr>
                                            </x-slot>
                                            <x-slot name="tbody">
                                                <tr>
                                                    @foreach ($bookingvolumeperchannels as $p => $bookingvolumeperchannel)
                                                        <td class="w-1/4 border-r border-gray-400">
                                                            <div class="flex gap-20 py-4 mt-2 flex-nowrap">
                                                                <div class="ml-4 text-left w-28">
                                                                    <p>
                                                                        {{ $bookingvolumeperchannel->name }}</p>
                                                                </div>
                                                                <div class="w-12 text-left">
                                                                    <p>
                                                                        {{ $bookingvolumeperchannel->booking_channel_count }}
                                                                    </p>
                                                                </div>
                                                                <div class="w-12 text-left">
                                                                    <p>500</p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="w-1/2">
                                                            <div class="grid grid-cols-1 py-4">
                                                                <div class="flex justify-between px-12">
                                                                    <div class="flex whitespace-nowrap">
                                                                        <p>3,500</p>
                                                                    </div>
                                                                    <div class="flex whitespace-nowrap">
                                                                        <p>100</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="w-1/3 border-l border-gray-400">
                                                            <div
                                                                class="w-24 px-2 py-4 font-normal text-center text-gray-700">
                                                                <div>39</div>
                                                            </div>
                                                        </td>
                                                </tr>
                                                @endforeach

                                            </x-slot>
                                        </x-table.table>
                                    </div>
                                    <div
                                        class="flex text-sm font-semibold bg-white border border-gray-400 rounded-lg rounded-t-none shadow-md">
                                        <div class="border-r border-gray-400">
                                            <div class="flex gap-20 py-3 flex-nowrap">
                                                <div class="ml-4 text-left w-28 whitespace-nowrap">
                                                    <p>Total Bookings</p>
                                                </div>
                                                <div class="w-12 text-left whitespace-nowrap text-[#003399]">
                                                    <p>{{ $totalBVPC }}</p>
                                                </div>
                                                <div class="text-left whitespace-nowrap text-[#003399]"
                                                    style="width: 4.9rem">
                                                    <p>3,200</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-3/4">
                                            <div class="grid grid-cols-1 ">
                                                <div class="flex justify-between px-12 py-3">
                                                    <div class="flex whitespace-nowrap text-[#003399]">
                                                        <p>18,050</p>
                                                    </div>
                                                    <div class="flex whitespace-nowrap text-[#003399]">
                                                        <p>820</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-1/5 border-l border-gray-400 text-[#003399] ">
                                            <div class="px-2 py-3 text-center" style="width: 6.1rem">
                                                <div>307</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-span-5">
                    <div class="px-8 py-2 bg-white rounded-lg shadow-lg">
                        <div class="grid grid-cols-12">
                            <div class="col-span-4 text-[#8D8D8D] text-lg font-semibold">
                                <p>Customer Type Distribution</p>
                                <p class="text-sm font-normal">TOTAL : {{ $totalctd }}</p>
                            </div>
                            <div class="col-span-8">
                                <div class="flex justify-end gap-4">
                                    <div class="w-56">
                                        <x-crm.input onchange="filterData()" type="date"
                                            wire:model.defer="CTD_search" name="CTD_search" placeholder="">
                                        </x-crm.input>
                                    </div>
                                    <div class="font-semibold">
                                        <x-crm.button type="button" wire:click="searchCTD" title="Search"
                                            class="text-white bg-blue hover:bg-blue-800" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid grid-cols-1 mt-4 ml-12" style="margin-left:5rem">
                                <div class="-mt-2 h-80 w-80 ml-8">
                                    <canvas class="p-4 " style="height:100% ; width:100%;" id="customer_type_distribution_chart"></canvas>
                                </div>
                        </div>
                        <div class="flex justify-center gap-20 mb-4">
                            <div class="flex mt-3 mr-4">
                                <div class="w-3 h-3 mr-3 rounded-sm bg-[#1F78B4]" style="margin-top: 12px">
                                </div>
                                <div class="grid grid-cols-1">
                                    <div class="text-xs font-normal text-gray-400 uppercase">{{ $individual }}</div>
                                    <div class="text-lg font-semibold" id="ind">{{ $totalindividual }}</div>
                                </div>
                            </div>
                            <div class="flex mt-3 ml-4">
                                <div class="w-3 h-3 mr-3 rounded-sm bg-[#1F78B4]" style="margin-top: 12px">
                                </div>
                                <div class="grid grid-cols-1">
                                    <div class="text-xs font-normal text-gray-400 uppercase">{{ $corporate }}</div>
                                    <div class="text-lg font-semibold" id="corp">{{ $totalcorporate }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-cols-1 gap-5 p-2">
            <div class="bg-white rounded-lg shadow-lg">
                <div class="grid grid-cols-1 gap-5 p-3">
                    <div class="grid grid-cols-2">
                        <div class="flex justify-between px-4">
                            <div class="text-[#8D8D8D] text-lg font-semibold ml-1">Geographical Booking Heat Map
                            </div>
                        </div>
                        <div class="flex justify-end gap-8 px-2">
                            <div class="w-56">
                                <x-crm.input onchange="filterData()" type="date" wire:model.defer="HM_search"
                                    name="HM_search" placeholder="">
                                </x-crm.input>
                            </div>
                            <div class="grid grid-cols-2">
                                <div>
                                    <x-crm.select wire:model.defer="HM_searchs" name="HM_searchs" placeholder="">
                                        <option value="1">Daily</option>
                                        <option value="2">Weekly</option>
                                        <option value="3">Monthly</option>
                                    </x-crm.select>
                                </div>
                                <div class="flex justify-start ml-8">
                                    <x-crm.button type="button" wire:click="searchHM" title="Search"
                                        class="text-white bg-blue hover:bg-blue-800" />
                                </div>
                            </div>
                            <div class="mt-2 font-semibold">
                                <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                    wire:click="action({},'see_details_emp_info')">
                                    See More
                                </h4>
                            </div>
                        </div>
                    </div>

                    <div class="grid grid-cols-12 gap-6">
                        <div class="col-span-5">
                            <div class="grid grid-cols-1 px-2">
                                <div class="overflow-auto bg-white rounded-lg shadow-lg">
                                    <div class="bg-white border border-gray-400 rounded-lg rounded-b-none shadow-md"
                                        style="height: 40vh">
                                        <x-table.table>
                                            <x-slot name="thead">
                                                <tr>
                                                    <th class="p-3 tracking-wider whitespace-nowrap">
                                                        <span
                                                            class="flex justify-between ml-10 font-medium">Area</span>
                                                    </th>
                                                    <th class="p-3 tracking-wider whitespace-nowrap">
                                                        <span class="flex justify-between ml-2 font-medium">No. of
                                                            Bookings</span>
                                                    </th>
                                                    <th class="p-3 tracking-wider whitespace-nowrap">
                                                        <span class="flex justify-between ml-2 font-medium">Volume
                                                            (tons)</span>
                                                    </th>
                                                </tr>
                                            </x-slot>
                                            <x-slot name="tbody">

                                                @foreach ($heatmaps as $l => $heatmap)
                                                    @if ($heatmap->booking_city_count != 0)
                                                        <tr
                                                            class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                                            <td class="p-3 tracking-wider whitespace-nowrap">
                                                                <div class="flex gap-3 ml-5">
                                                                    @if ($heatmap->booking_city_count > 2)
                                                                        <div
                                                                            class="bg-[#FF7560] rounded-full h-2 w-2 mt-[6px]">
                                                                    @endif

                                                                    @if ($heatmap->booking_city_count == 2)
                                                                        <div
                                                                            class="bg-[#FFD04F] rounded-full h-2 w-2 mt-[6px]">
                                                                    @endif

                                                                    @if ($heatmap->booking_city_count < 2)
                                                                        <div
                                                                            class="bg-[#A2C1FF] rounded-full h-2 w-2 mt-[6px]">
                                                                    @endif

                                                                </div>
                                                                <div>{{ $heatmap->name }}</div>
                                    </div>
                                    </td>
                                    <td class="p-3 tracking-wider whitespace-nowrap">
                                        <span
                                            class="flex justify-between ml-2">{{ $heatmap->booking_city_count }}</span>
                                    </td>
                                    <td class="p-3 tracking-wider whitespace-nowrap">
                                        <span class="flex justify-between ml-2">35</span>
                                    </td>
                                    </tr>
                                    @endif
                                    @endforeach
    </x-slot>
    </x-table.table>
    </div>
    </div>
    <div class="flex text-sm font-medium bg-white border border-gray-400 rounded-lg rounded-t-none shadow-md">
        <div class="">
            <div class="flex gap-20 py-4 flex-nowrap">
                <div class="w-32 ml-12 text-left whitespace-nowrap">
                    <p>Total Bookings :</p>
                </div>
                <div class="w-24 text-left whitespace-nowrap text-[#003399] font-semibold text-[15px]">
                    <p>{{ $totalheatmap }}</p>
                </div>
                <div class="text-left whitespace-nowrap text-[#003399] font-semibold text-[15px]">
                    <p>216</p>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class="col-span-7">
        <div class="grid grid-cols-12 gap-4">
            <div class="col-span-9 border border-gray-500 rounded-lg">
                <div style="width:652px; height:435px;" id="map"></div>
            </div>
            <div class="col-span-3">
                <div class="grid grid-cols-1" style="margin: 9rem 0 0 2rem">
                    <div class="flex gap-2">
                        <div class="bg-[#FF7560] h-3 w-3 mt-[5px]"></div>
                        <div>HOT</div>
                    </div>
                    <div class="flex gap-2 mt-6">
                        <div class="bg-[#FFD04F] h-3 w-3 mt-[5px]"></div>
                        <div>MEDIUM</div>
                    </div>
                    <div class="flex gap-2 mt-6">
                        <div class="bg-[#A2C1FF] h-3 w-3 mt-[5px]"></div>
                        <div>COLD</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </x-slot>
</x-crm.form>


@push('scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>

    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API_KEY') }}&callback=initMap&libraries=visualization&v=weekly"
        defer></script>

    <script type="text/javascript">
        // Top Performing Sales/

        google.charts.load('current', {
            'packages': ['bar'],
        }).then(function() {
            var data = google.visualization.arrayToDataTable([
                ['', 'Completed', 'Advanced', 'Rescheduled', 'Cancelled'],
                <?php
                for ($x = 0; $x <= count($completeddates) - 1; $x++) {
                    echo str_replace('"', '', json_encode($completeddates2[$x])) . ',';
                }
                ?>
            ]);
            var options = {
                chart: {
                    title: '',
                    subtitle: '',
                },
                vAxes: {
                    0: {
                        title: 'Total no. of Bookings'
                    }
                },
                bar: {
                    groupWidth: "26%",
                },
                bars: 'vertical', // Required for Material Bar Charts.
                colors: [
                    "#5485E5", "#6BCD60", "#FFC0B6", "#FA7A7A"
                ],
                legend: {
                    position: "none"
                }
            };

            var chart = new google.charts.Bar(document.getElementById('booking_history_chart'));

            chart.draw(data, google.charts.Bar.convertOptions(options));
        });

        // Booking Channel
        const totalsPie = [{{ $totalcis3 }}, {{ $totalweb }}, {{ $totalmobile }}, {{ $totalvip }}];
        drawChartBilog(totalsPie);

        function drawChartBilog(data) {
            const dataPie = {
                datasets: [{
                    label: "Booking Channels",
                    data: data,
                    backgroundColor: [
                        "#1F78B4",
                        "#A6CEE3",
                        "#77D26D",
                        "#9CE993",
                    ],
                    borderWidth: 0,
                    hoverOffset: 4,
                }, ],
            };
            var options = {
                tooltips: {
                    enabled: false
                },
                events: [],

                plugins: {
                    datalabels: {
                        formatter: (value, booking_channel_piechart) => {

                            let sum = 0;
                            let dataArr = booking_channel_piechart.chart.data.datasets[0].data;
                            dataArr.map(data => {
                                sum += data;
                            });
                            let percentage = (value * 100 / sum).toFixed(0) + "%";
                            if (percentage == '0%') return '';
                            return percentage;

                        },
                        borderRadius: 0,
                        color: '#fff',
                        font: {
                            size: 16
                        },
                    }
                }
            };

            const configPie = {
                type: "pie",
                data: dataPie,
                options: options,
            };

            var chartBar = new Chart(document.getElementById("booking_channel_piechart"), configPie);
        }
        document.addEventListener('livewire:load', function() {
            let totals = {};
            window.livewire.on('totalsarray', function(value) {
                const totalarray = [];
                totals.cis3 = document.getElementById('cis3').textContent = value[0];
                totals.web = document.getElementById('web').textContent = value[1];
                totals.mobile = document.getElementById('mobile').textContent = value[2];
                totals.vip = document.getElementById('vip').textContent = value[3];
                for (const [key, value] of Object.entries(totals)) {
                    totalarray.push(value)
                }
                drawChartBilog(totalarray);
            });
        });

        // Cancellation Reports Pie
        const totalsPie2 = [{{ $reason1count }}, {{ $reason2count }}, {{ $reason3count }}, {{ $reason4count }},
            {{ $reason5count }}, {{ $reason6count }}, {{ $reason7count }}, {{ $reason8count }},
            {{ $reason9count }}, {{ $reason10count }}, {{ $reason11count }}
        ];
        drawChartBilog2(totalsPie2);

        function drawChartBilog2(data) {
            const dataPie = {
                datasets: [{
                    label: "Cancellation Report",
                    data: data,
                    backgroundColor: [
                        "#1F78B4",
                        "#A6CEE3",
                        "#B78FFF",
                        "#D1B8FF",
                        "#FA7A7A",
                        "#FFC0B6",
                        "#FFB055",
                        "#FFD4A2",
                        "#FFE6A0",
                        "#77D26D",
                        "#9CE993",
                    ],
                    borderWidth: 0,
                    hoverOffset: 4,
                }, ],
            };
            var options = {
                tooltips: {
                    enabled: false
                },
                events: [],

                plugins: {
                    datalabels: {
                        formatter: (value, cancellation_report_piechart) => {

                            let sum = 0;
                            let dataArr = cancellation_report_piechart.chart.data.datasets[0].data;
                            dataArr.map(data => {
                                sum += data;
                            });
                            let percentage = (value * 100 / sum).toFixed(0) + "%";
                            if (percentage == '0%') return '';
                            return percentage;

                        },
                        borderRadius: 0,
                        color: '#fff',
                        font: {
                            size: 16
                        },
                    }
                }
            };

            const configPie = {
                type: "pie",
                data: dataPie,
                options: options,
            };

            var chartBar = new Chart(document.getElementById("cancellation_report_piechart"), configPie);
        }
        document.addEventListener('livewire:load', function() {
            let totals = {};
            window.livewire.on('totalsarray2', function(value) {
                const totalarray2 = [];
                totals.r1 = document.getElementById('r1').textContent = value[0];
                totals.r2 = document.getElementById('r2').textContent = value[1];
                totals.r3 = document.getElementById('r3').textContent = value[2];
                totals.r4 = document.getElementById('r4').textContent = value[3];
                totals.r5 = document.getElementById('r5').textContent = value[4];
                totals.r6 = document.getElementById('r6').textContent = value[5];
                totals.r7 = document.getElementById('r7').textContent = value[6];
                totals.r8 = document.getElementById('r8').textContent = value[7];
                totals.r9 = document.getElementById('r9').textContent = value[8];
                totals.r10 = document.getElementById('r10').textContent = value[9];
                totals.r11 = document.getElementById('r11').textContent = value[10];
                for (const [key, value] of Object.entries(totals)) {
                    totalarray2.push(value)
                }
                drawChartBilog2(totalarray2);
            });
        });

        // Pick up Activity Completion Rate
        google.charts.load("current", {
            packages: ["corechart"],
        }).then(function() {
            var data = google.visualization.arrayToDataTable([
                ['Month', 'Percent', {
                    role: 'style'
                }],
                ['January', {{ $totaljan }}, 'point {fill-color: #003399; }'],
                ['February', {{ $totalfeb }}, 'point {fill-color: #003399; }'],
                ['March', {{ $totalmar }}, 'point {fill-color: #003399; }'],
                ['April', {{ $totalapr }}, 'point {fill-color: #003399; }'],
                ['May', {{ $totalmay }}, 'point {fill-color: #003399; }'],
                ['June', {{ $totaljun }}, 'point {fill-color: #003399; }'],
                ['July', {{ $totaljul }}, 'point {fill-color: #003399; }'],
                ['August', {{ $totalaug }}, 'point {fill-color: #003399; }'],
                ['September', {{ $totalsept }}, 'point {fill-color: #003399; }'],
                ['October', {{ $totaloct }}, 'point {fill-color: #003399; }'],
                ['November', {{ $totalnov }}, 'point {fill-color: #003399; }'],
                ['December', {{ $totaldec }}, 'point {fill-color: #003399; }']

            ]);

            var options = {
                legend: 'none',
                curveType: 'function',
                pointSize: 12,
                width: 1510,
                height: 400,
                chartArea: {
                    top: '5%',
                    right: '2%',
                    bottom: '5%',
                    left: '6%'
                },
                vAxes: {
                    0: {
                        title: 'Total no. of Bookings'
                    }
                },
                series: {
                    0: {
                        lineWidth: 2
                    },
                    1: {
                        color: 'red',
                        lineWidth: 0,
                        pointSize: 4
                    }
                },
                colors: [
                    "#16DA00"
                ],
            };

            var chart = new google.visualization.LineChart(document.getElementById('pickup_activity_chart'));
            chart
                .draw(data, options);
        });

        // Customer Type Distribution Pie
        const totalsPie3 = [{{ $totalindividual }}, {{ $totalcorporate }}];
        drawChartBilog3(totalsPie3);

        function drawChartBilog3(data) {
            const dataPie = {
                datasets: [{
                    label: "Customer Type Distribution",
                    data: data,
                    backgroundColor: [
                        "#1F78B4",
                        "#A6CEE3",
                    ],
                    borderWidth: 0,
                    hoverOffset: 4,
                }, ],
            };
            var options = {
                tooltips: {
                    enabled: false
                },
                events: [],

                plugins: {
                    datalabels: {
                        formatter: (value, customer_type_distribution_chart) => {

                            let sum = 0;
                            let dataArr = customer_type_distribution_chart.chart.data.datasets[0].data;
                            dataArr.map(data => {
                                sum += data;
                            });
                            let percentage = (value * 100 / sum).toFixed(0) + "%";
                            if (percentage == '0%') return '';
                            return percentage;

                        },
                        borderRadius: 0,
                        color: '#fff',
                        font: {
                            size: 16
                        },
                    }
                }
            };

            const configPie = {
                type: "pie",
                data: dataPie,
                options: options,
            };

            var chartBar = new Chart(document.getElementById("customer_type_distribution_chart"), configPie);
        }
        document.addEventListener('livewire:load', function() {
            let totals = {};
            window.livewire.on('totalsarray3', function(value) {
                const totalarray3 = [];
                totals.ind = document.getElementById('ind').textContent = value[0];
                totals.corp = document.getElementById('corp').textContent = value[1];
                for (const [key, value] of Object.entries(totals)) {
                    totalarray3.push(value)
                }
                drawChartBilog3(totalarray3);
            });
        });





        // Heatmap
        let map, heatmap;

        function initMap() {
            map = new google.maps.Map(document.getElementById("map"), {
                zoom: 6,
                center: {
                    lat: 11.6978352,
                    lng: 122.6217542
                },
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });
            heatmap = new google.maps.visualization.HeatmapLayer({
                data: getPoints(),
                map: map,
            });
        }

        function getPoints() {
            var latlngs = JSON.parse('{{ json_encode($latlngs) }}');
            const f = latlngs;
            var googlemaps = f.flatMap((x) => new google.maps.LatLng(x[0], x[1]));

            return googlemaps;
        }


        window.initMap = initMap;
    </script>
@endpush

@push('heads')
    <style>
        #map {
            height: 100%;
        }

        /*
                                                                                                                                                                                                                     * Optional: Makes the sample page fill the window.
                                                                                                                                                                                                                     */
        html,
        body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        #floating-panel {
            position: absolute;
            top: 10px;
            left: 25%;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border: 1px solid #999;
            text-align: center;
            font-family: "Roboto", "sans-serif";
            line-height: 30px;
            padding-left: 10px;
        }

        #floating-panel {
            background-color: #fff;
            border: 1px solid #999;
            left: 25%;
            padding: 5px;
            position: absolute;
            top: 10px;
            z-index: 5;
        }
    </style>
@endpush
