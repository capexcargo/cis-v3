<div>
    <div class="text-[#8D8D8D] text-lg font-semibold ml-1">
        <p class="text-[#003399] text-xl">FM61-6123-7233</p>
        <p class="text-sm font-normal">09/22/2022 - 09/28/2022</p>
    </div>
    <div class="mt-3 overflow-auto">
        <div class="bg-white border border-gray-400 rounded-lg shadow-md">
            <x-table.table>
                <x-slot name="thead">
                    <x-table.th name="No." />
                    <x-table.th name="Date" />
                    <x-table.th name="Reason for Reschedule" />
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($logs as $a => $log)
                        <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                            <td class="p-3 whitespace-nowrap">
                                {{ $a + 1 }}.
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{date_format($log->created_at,'m/d/Y')}}
                            </td>
                            <td class="p-3 whitespace-nowrap">
                                {{$log->ReschedReasonReferenceLG->name}}
                            </td>
                        </tr>
                    @endforeach

                </x-slot>
            </x-table.table>
        </div>
    </div>
</div>


