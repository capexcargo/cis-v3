<x-crm.form x-data="{
    search_form: false,
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
    </x-slot>
    <x-slot name="header_title">SR Reports</x-slot>
    <x-slot name="filter">
        <div class="grid w-3/4 grid-cols-4 gap-4 pb-2">
            <div>
                <x-crm.input onchange="filterData()" type="date" wire:model.defer="date_created_search"
                    name="date_created_search" placeholder="">
                </x-crm.input>
            </div>
            <div class="grid grid-cols-3">
                <div class="flex justify-start ml-2">
                    <x-crm.button type="button" wire:click="search" name="search" title="Search"
                        class="text-white bg-blue hover:bg-blue-800" />
                </div>
            </div>
        </div>
    </x-slot>
    <x-slot name="body" size="w-auto">

        <div class="grid grid-cols-12 gap-3">
            <div class="col-span-8 gap-4">
                <div class="grid grid-cols-12 gap-3">

                    <div class="col-span-6">
                        <div class="bg-white border rounded-lg shadow-lg">
                            <div class="flex justify-between">
                                <div class="px-6 py-3 text-left">
                                    <div class="">
                                        <h4 class="text-lg font-semibold text-gray-500">
                                            Service Request Report
                                        </h4>
                                        <P class="font-normal text-sm text-gray-400 text-[10px]">
                                            TOTAL : {{ $totalsrr }}
                                        </P>
                                    </div>
                                </div>
                                <div class="p-3 font-semibold">
                                    <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                        wire:click="redirectTo({}, 'redirectToService')">
                                        See More
                                    </h4>
                                </div>
                            </div>
                            <div class="grid grid-cols-12 -mt-4">
                                <div class="col-span-6 pt-2 pb-6" style="height:115% ; width:125%;">
                                    <canvas class="" style="height:100% ; width:100%;" id="chartPie"></canvas>
                                </div>

                                <div class="col-span-1 pr-2 mt-6 ml-6" style="">
                                    <div class="w-4 h-4 mt-4 ml-2 rounded-sm bg-blue-pie"></div>
                                    <div class="w-4 h-4 mt-16 ml-2 rounded-sm bg-sky-blue-pie">
                                    </div>
                                </div>
                                <div class="col-span-1 mt-6 ml-6" style="">
                                    <div class="mt-1 text-sm font-normal text-gray-400 uppercase">CLOSED</div>
                                    <div class="text-xl font-medium " id="closed">{{ $totalclose }}</div>

                                    <div class="mt-8 text-sm font-normal text-gray-400 uppercase">OPEN</div>
                                    <div class="text-xl font-medium " id="opened">{{ $totalopen }}</div>
                                </div>
                            </div>

                        </div>


                    </div>

                    <div class="col-span-6">
                        <div class="bg-white border rounded-lg shadow-lg">
                            <div class="flex justify-between">
                                <div class="px-6 py-3 text-left">
                                    <div class="">
                                        <h4 class="text-lg font-semibold text-gray-500">
                                            Severity
                                        </h4>
                                        <P class="font-normal text-sm text-gray-400 text-[10px]">
                                            TOTAL:{{ $totalsev }}
                                        </P>
                                    </div>
                                </div>
                                <div class="p-3 font-semibold">
                                    <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                        wire:click="redirectTo({}, 'redirectToService')">
                                        See More
                                    </h4>
                                </div>
                            </div>
                            <div class="grid grid-cols-12 -mt-4">
                                <div class="col-span-6 pt-2 pb-6" style="height:115% ; width:125%;">
                                    <canvas class="" style="height:100% ; width:100%;" id="chartPie2"></canvas>
                                </div>

                                <div class="col-span-1 mt-6 ml-6" style="">
                                    <div class="w-4 h-4 mt-4 ml-2 rounded-sm bg-[#67E66F]"></div>
                                    <div class="w-4 h-4 mt-16 ml-2 rounded-sm bg-[#FFD360]">
                                    </div>
                                </div>
                                <div class="col-span-1 mt-6 ml-6" style="">
                                    <div class="mt-1 text-sm font-normal text-gray-400 uppercase">Low</div>
                                    <div class="text-xl font-medium " id="lows">{{ $totallow }}</div>

                                    <div class="mt-8 text-sm font-normal text-gray-400 uppercase">Medium</div>
                                    <div class="text-xl font-medium " id="mediums">{{ $totalmedium }}</div>
                                </div>

                                <div class="col-span-1 mt-6 ml-2" style="">
                                    {{-- <div class="w-4 h-4 mt-4 ml-2 rounded-sm bg-[#FC9788]"></div> --}}

                                </div>
                               
                                <div class="col-span-1 mt-6 ml-2" style="">
                                    <div class="w-4 h-4 mt-4 ml-2 rounded-sm bg-[#FC9788]"></div>

                                </div>
                                <div class="col-span-1 mt-6 ml-2" style="">
                                    <div class="mt-1 text-sm font-normal text-gray-400 uppercase">High</div>
                                    <div class="text-xl font-medium " id="highs">{{ $totalhigh }}</div>

                                </div>
                            </div>

                        </div>


                    </div>

                    <div class="col-span-12">
                        <div class="bg-white border rounded-lg shadow-lg">
                            <div class="flex justify-between">
                                <div class="px-3 py-3 text-left ">
                                    <div class="">
                                        <h4 class="ml-2 text-lg font-semibold text-gray-500">
                                            Channel/SR Source
                                        </h4>
                                    </div>
                                </div>
                                <div class="p-3 font-semibold ">
                                    <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                        wire:click="redirectTo({}, 'redirectToService')">
                                        See More
                                    </h4>
                                </div>
                            </div>

                                {{-- <canvas class="p-6" style="width: 100%; height:340px;" id="chartBar"></canvas> --}}
                                <div id="chartBar" class="content-center" style="width:100%; height:350px;"></div>
                        </div>
                    </div>

                    <div class="col-span-12">
                        <div class="bg-white border rounded-lg shadow-lg">
                            <div class="flex justify-between">
                                <div class="min-w-0 px-3 py-3 text-left">
                                    <div class="">
                                        <h4 class="ml-2 text-lg font-semibold text-gray-500">
                                            Service Requirements
                                        </h4>
                                    </div>
                                </div>
                                <div class="min-w-0 p-3 font-semibold">
                                    <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                        wire:click="redirectTo({}, 'redirectToService')">
                                        See More
                                    </h4>
                                </div>
                            </div>
                                {{-- <canvas class="p-4" style="width: 100%; height:340px;" id="chartBar2"></canvas> --}}
                                <div id="chartBar2" class="content-center" style="width: 100%; height:350px;"></div>

                        </div>
                    </div>

                    <div class="col-span-12">
                        <div class="bg-white border rounded-lg shadow-lg">
                            <div class="flex justify-between">
                                <div class="min-w-0 px-6 py-3 text-left">
                                    <div class="">
                                        <h4 class="text-lg font-semibold text-gray-500">
                                            Agents
                                        </h4>
                                        <P class="font-normal text-sm text-gray-400 text-[10px]">
                                            {{ $totalagents }} CSM Agents
                                        </P>
                                    </div>
                                </div>
                                <div class="min-w-0 p-3 font-semibold">
                                    <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                        wire:click="redirectTo({}, 'redirectToService')">
                                        See More
                                    </h4>
                                </div>
                            </div>

                            <div class="p-3 px-6 overflow-auto" style="height:">
                                <div class="text-center bg-white border-gray-300 rounded-lg shadow-md">
                                    <x-table.table class="overflow-hidden">

                                        <x-slot name="thead">
                                            <tr>
                                                <x-table.th name="" class="" />

                                                <x-table.th name="" class="border-r border-gray-300 " />

                                                <x-table.th colspan="2" name="Open"
                                                    class="bg-gray-100 border border-gray-300 "
                                                    style="padding-left:9%;height:1px" />


                                                <x-table.th colspan="2" name="Closed"
                                                    class="bg-gray-200 border border-gray-300 "
                                                    style="padding-left:8%;height:1px" />

                                                <x-table.th name="" class="" />

                                            </tr>
                                            <tr>
                                                <x-table.th name="No." class="border-t border-gray-300 " />

                                                <x-table.th name="Name"
                                                    class="border-t border-r border-gray-300 " />
                                                <x-table.th name="Within SLA"
                                                    class="border-t border-r border-gray-300 " />
                                                <x-table.th name="Beyond SLA"
                                                    class="border-t border-r border-gray-300 " />


                                                <x-table.th name="Within SLA"
                                                    class="border-t border-r border-gray-300 " />
                                                <x-table.th name="Beyond SLA"
                                                    class="border-t border-r border-gray-300 " />
                                                <x-table.th name="Total SRs Received"
                                                    class="border-t " />
                                            </tr>
                                        </x-slot>
                                        <x-slot name="tbody">
                                            @foreach ($agents as $a => $agent)
                                                <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                                    <td class="p-3 whitespace-nowrap">{{ $a + 1 }}.</td>
                                                    <td class="p-3 whitespace-nowrap">
                                                        <div class="flex w-full space-x-2">
                                                            <div class="p-5 bg-center bg-no-repeat bg-cover border rounded-full"
                                                                style="background-image: url({{ Storage::disk('hrim_gcs')->url($agents[$a]['photo_path'] . $agents[$a]['photo_name']) }})">
                                                            </div>
                                                            <div class="flex items-center text-sm">
                                                                {{ $agents[$a]['name'] }}
                                                    </td>
                                                    <td class="p-3 whitespace-normal border-l border-gray-300">5</td>
                                                    <td class="p-3 whitespace-normal border-l border-gray-300">2</td>
                                                    <td class="p-3 whitespace-normal border-l border-gray-300">27</td>
                                                    <td class="p-3 whitespace-normal border-l border-gray-300">9</td>
                                                    <td
                                                        class="p-3 text-blue-600 whitespace-normal border-l border-gray-300">
                                                        43</td>
                                                </tr>
                                            @endforeach

                                        </x-slot>
                                    </x-table.table>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>

            <div class="col-span-4 gap-4">
                <div class="grid grid-cols-12 gap-4">
                    <div class="col-span-12 gap-3">
                        <div class="bg-white border rounded-lg shadow-lg">
                            <div class="flex justify-between">
                                <div class="min-w-0 px-6 py-3 text-left">
                                    <div class="">
                                        <h4 class="text-lg font-semibold text-gray-500">
                                            Service Request Type Distribution
                                        </h4>
                                    </div>
                                </div>
                                <div class="min-w-0 p-3 font-semibold">
                                    <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                        wire:click="redirectTo({}, 'redirectToService')">
                                        See More
                                    </h4>
                                </div>
                            </div>

                            <div class="grid grid-cols-12 gap-3 p-6 -mt-4">
                                @foreach ($serviceRTD as $x => $srsubat)
                                    <div class="col-span-6">
                                        <div class="bg-white border rounded-lg shadow-lg">
                                            <div class="flex justify-between">
                                                <div class="px-3 py-2 text-left">
                                                        <div class="text-base font-semibold text-gray-500 whitespace-nowrap">
                                                            {{ $srsubat->name }}
                                                        </div>
                                                </div>
                                                <div class="p-2 px-3 font-semibold ">
                                                        <div class="text-xl font-bold">
                                                            {{ $srsubat->service_requestsr_count }}
                                                        </div>
                                                </div>
                                            </div>

                                            @foreach ($srsubat->SrSubCateg as $v => $asd)
                                                <div class="px-2">
                                                    <div class="grid grid-cols-1 p-2 mt-2">
                                                        <div class="-mt-4">
                                                            <p class="text-sm font-normal text-gray-500 whitespace-normal">
                                                                {{ $asd->name }}</p>
                                                            <div class="flex gap-2">
                                                                <div
                                                                    class="w-full mt-1 border border-none rounded-lg h-[14px] mb-6">
                                                                    <div class="bg-[#389AEB] h-3 rounded-lg"
                                                                        style="width: {{ $srsubat->service_requestsr_count <= 0 ? 0 : round(($asd->service_request_count / $srsubat->service_requestsr_count) * 100, 2) }}%">
                                                                    </div>
                                                                </div>
                                                                <h4 class="-mt-1 font-semibold">
                                                                    {{ $asd->service_request_count }}

                                                                </h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="col-span-12">
                        <div class="bg-white border rounded-lg shadow-lg">
                            <div class="flex justify-between">
                                <div class="px-6 py-3 text-left">
                                    <div class="">
                                        <h4 class="text-lg font-semibold text-gray-500">
                                            First Response Efficency
                                        </h4>
                                        <P class="font-normal text-sm text-gray-400 text-[10px]">
                                            TOTAL : 101
                                        </P>
                                    </div>
                                </div>
                                <div class="p-3 font-semibold">
                                    <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                        wire:click="redirectTo({}, 'redirectToService')">
                                        See More
                                    </h4>
                                </div>
                            </div>
                            <div class="grid grid-cols-12 -mt-4">
                                <div class="col-span-6 pt-2 pb-6" style="height:115% ; width:125%;">
                                    <canvas class="" style="height:100% ; width:100%;" id="chartPie3"></canvas>
                                </div>

                                <div class="col-span-1 mt-6 ml-6" style="">
                                    <div class="w-4 h-4 mt-4 ml-2 rounded-sm bg-[#67E66F]"></div>
                                    <div class="w-4 h-4 mt-16 ml-2 rounded-sm bg-[#FC9788]">
                                    </div>
                                </div>
                                <div class="col-span-1 mt-6 ml-6" style="">
                                    <div class="mt-1 text-sm font-normal text-gray-400 uppercase whitespace-nowrap">ON TIME</div>
                                    <div class="text-xl font-medium " id="">48</div>

                                    <div class="mt-8 text-sm font-normal text-gray-400 uppercase">LATE</div>
                                    <div class="text-xl font-medium " id="">53</div>
                                </div>
                            </div>

                        </div>


                    </div>

                    <div class="col-span-12">
                        <div class="bg-white border rounded-lg shadow-lg">
                            <div class="flex justify-between">
                                <div class="px-6 py-3 text-left">
                                    <div class="">
                                        <h4 class="text-lg font-semibold text-gray-500">
                                            SR Resolution Efficency
                                        </h4>
                                        <P class="font-normal text-sm text-gray-400 text-[10px]">
                                            TOTAL : 101
                                        </P>
                                    </div>
                                </div>
                                <div class="p-3 font-semibold">
                                    <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                        wire:click="redirectTo({}, 'redirectToService')">
                                        See More
                                    </h4>
                                </div>
                            </div>
                            <div class="grid grid-cols-12 -mt-4">
                                <div class="col-span-6 pt-2 pb-6" style="height:115% ; width:125%;">
                                    <canvas class="" style="height:100% ; width:100%;" id="chartPie4"></canvas>
                                </div>

                                <div class="col-span-1 mt-6 ml-6" style="">
                                    <div class="w-4 h-4 mt-4 ml-2 rounded-sm bg-[#67E66F]"></div>
                                    <div class="w-4 h-4 mt-16 ml-2 rounded-sm bg-[#FC9788]">
                                    </div>
                                </div>
                                <div class="col-span-1 mt-6 ml-6" style="">
                                    <div class="mt-1 text-sm font-normal text-gray-400 uppercase whitespace-nowrap">ON TIME</div>
                                    <div class="text-xl font-medium " id="">57</div>

                                    <div class="mt-8 text-sm font-normal text-gray-400 uppercase">LATE</div>
                                    <div class="text-xl font-medium " id="">44</div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>



        </div>




    </x-slot>
</x-crm.form>

@push('scripts')
    <!-- Required chart.js -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
    <script type="text/javascript">
        const totalsPie = [{{ $totalclose }}, {{ $totalopen }}];
        drawChartBilog(totalsPie);

        function drawChartBilog(data) {
            const dataPie = {
                datasets: [{
                    label: "Service Request Report",
                    data: data,
                    backgroundColor: [
                        "#1F78B4",
                        "#A6CEE3",
                    ],
                    borderWidth: 0,
                    hoverOffset: 4,
                }, ],
            };
            var options = {
                tooltips: {
                    enabled: false
                },
                events: [],

                plugins: {
                    datalabels: {
                        formatter: (value, chartPie) => {

                            let sum = 0;
                            let dataArr = chartPie.chart.data.datasets[0].data;
                            dataArr.map(data => {
                                sum += data;
                            });
                            let percentage = (value * 100 / sum).toFixed(0) + "%";
                            if (percentage == '0%') return '';
                            return percentage;

                        },
                        borderRadius: 0,
                        color: '#fff',
                        font: {
                            size: 16
                        },
                    }
                }
            };

            const configPie = {
                type: "pie",
                data: dataPie,
                options: options,
            };

            var chartBar = new Chart(document.getElementById("chartPie"), configPie);
        }
        document.addEventListener('livewire:load', function() {
            let totals = {};
            window.livewire.on('totalsarray', function(value) {
                const totalarray = [];
                totals.closed = document.getElementById('closed').textContent = value[0];
                totals.opened = document.getElementById('opened').textContent = value[1];
                for (const [key, value] of Object.entries(totals)) {
                    totalarray.push(value)
                }
                drawChartBilog(totalarray);
            });
        });

        const totalsPie2 = [{{ $totallow }}, {{ $totalmedium }}, {{ $totalhigh }}];
        drawChartBilog2(totalsPie2);

        function drawChartBilog2(data) {

            const dataPie2 = {
                datasets: [{
                    label: "Severity",
                    data: data,
                    backgroundColor: [
                        "#67e66f",
                        "#f5d174",
                        "#fc9788",
                    ],
                    borderWidth: 0,
                    hoverOffset: 4,
                }, ],

            };

            var options = {
                tooltips: {
                    enabled: false
                },
                events: [],

                plugins: {
                    datalabels: {
                        formatter: (value, chartPie2) => {

                            let sum = 0;
                            let dataArr = chartPie2.chart.data.datasets[0].data;
                            dataArr.map(data => {
                                sum += data;
                            });
                            let percentage = (value * 100 / sum).toFixed(0) + "%";
                            if (percentage == '0%') return '';
                            return percentage;

                        },
                        color: '#fff',
                        font: {
                            size: 16
                        },
                    }
                }
            };

            const configPie2 = {
                type: "pie",
                data: dataPie2,
                options: options,
            };

            var chartBar = new Chart(document.getElementById("chartPie2"), configPie2);
        }
        document.addEventListener('livewire:load', function() {
            let totals2 = {};
            window.livewire.on('totalsarray2', function(value) {
                const totalarray2 = [];
                totals2.lows = document.getElementById('lows').textContent = value[0];
                totals2.mediums = document.getElementById('mediums').textContent = value[1];
                totals2.highs = document.getElementById('highs').textContent = value[2];
                for (const [key, value] of Object.entries(totals2)) {
                    totalarray2.push(value)
                }
                drawChartBilog2(totalarray2);
            });
        });


        const dataPie3 = {
            datasets: [{
                label: "First Response Efficency",
                data: [48, 52],
                backgroundColor: [
                    "#67e66f",
                    "#fc9788",
                ],
                borderWidth: 0,
                hoverOffset: 4,
            }, ],
        };

        var options = {
            tooltips: {
                enabled: false
            },
            plugins: {
                datalabels: {
                    formatter: (value, chartPie3) => {

                        let sum = 0;
                        let dataArr = chartPie3.chart.data.datasets[0].data;
                        dataArr.map(data => {
                            sum += data;
                        });
                        let percentage = (value * 100 / sum).toFixed(0) + "%";
                        return percentage;


                    },
                    color: '#fff',
                    font: {
                        size: 16
                    },
                }
            }
        };

        const configPie3 = {
            type: "pie",
            data: dataPie3,
            options: options,
        };

        var chartBar = new Chart(document.getElementById("chartPie3"), configPie3);

        const dataPie4 = {
            datasets: [{
                label: "SR Resolution Efficiency",
                data: [80, 20],
                backgroundColor: [
                    "#67e66f",
                    "#fc9788",
                ],
                borderWidth: 0,
                hoverOffset: 4,
            }, ],
        };

        var options = {
            tooltips: {
                enabled: false
            },
            plugins: {
                datalabels: {
                    formatter: (value, chartPie4) => {

                        let sum = 0;
                        let dataArr = chartPie4.chart.data.datasets[0].data;
                        dataArr.map(data => {
                            sum += data;
                        });
                        let percentage = (value * 100 / sum).toFixed(0) + "%";
                        return percentage;


                    },
                    color: '#fff',
                    font: {
                        size: 16
                    },
                }
            }
        };

        const configPie4 = {
            type: "pie",
            data: dataPie4,
            options: options,
        };

        var chartBar = new Chart(document.getElementById("chartPie4"), configPie4);


        google.charts.load('current', {
            packages: ['corechart']
        }).then(function() {
            var data = new google.visualization.arrayToDataTable([
                ['Channels/SR Source', 'Total', {
                    role: 'style'
                }],

                <?php
                
                for ($x = 0; $x <= count($totalchannels) - 1; $x++) {
                    echo json_encode($getdatachannels[$x]) . ',';
                }
                ?>

            ]);

            var view = new google.visualization.DataView(data);

            var options = {
                color: ['#82ABFF'],
                legend: {
                    position: 'none'
                },
                chart: {
                    title: '',
                    subtitle: ''
                },
                vAxes: {
                    0: {
                        title: 'Total no. of Requests'
                    }
                },
                bar: {
                    groupWidth: "20%"
                }
            };

            var chart = new google.visualization.ColumnChart(document.getElementById("chartBar"));
            chart.draw(view, options);
        });


        google.charts.load('current', {
            packages: ['corechart']
        }).then(function() {
            var data = new google.visualization.arrayToDataTable([
                ['Service Requirements', 'Total', {
                    role: 'style'
                }],
                <?php
                
                for ($x = 0; $x <= count($totalrequirements) - 1; $x++) {
                    echo json_encode($getdatarequirements[$x]) . ',';
                }
                ?>

            ]);

            var view = new google.visualization.DataView(data);

            var options = {
                legend: {
                    position: 'none'
                },
                chart: {
                    title: '',
                    subtitle: ''
                },
                vAxes: {
                    0: {
                        title: 'Total no. of SRs'
                    }
                },
                bar: {
                    groupWidth: "25%"
                }
            };

            var chart = new google.visualization.ColumnChart(document.getElementById("chartBar2"));
            chart.draw(view, options);
        });
    </script>
@endpush
