<x-crm.form x-data="{
    search_form: false,
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
    </x-slot>
    <x-slot name="header_title">Commercials Reports</x-slot>

    <x-slot name="body" size="w-10/12 xs:w-10/12 sm:w-10/12 md:w-10/12">

        <div class="grid grid-cols-12 gap-3 p-2">

            <div class="col-span-12">
                <div class="pb-6 -mt-4 bg-white border rounded-lg shadow-lg">
                    <div class="flex justify-between">
                        <div class="min-w-0 px-6 py-3 text-left">
                            <div class="">
                                <h4 class="text-lg font-semibold text-gray-500">
                                    Rate Usage Report
                                </h4>

                            </div>
                        </div>
                        <div class="min-w-0 p-3 mr-4 font-semibold">
                            <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                wire:click="redirectTo({}, 'redirectToCommercial')">
                                See More
                            </h4>
                        </div>
                    </div>

                    <div class="p-6 -mt-4 overflow-hidden" style="height: auto">
                        <div class="bg-white border rounded-lg shadow-md">
                            <x-table.table>
                                <x-slot name="thead">
                                    <x-table.th name="No." />
                                    <x-table.th name="Rate Name" />
                                    <x-table.th name="Rate Description" />
                                    <x-table.th name="No. of Transactions" />
                                    <x-table.th name="Volume" />
                                </x-slot>
                                <x-slot name="tbody">
                                    {{-- @dd($pages); --}}

                                    @foreach ($pages as $i => $commercialmgmt)
                                        <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                            <td class="p-3 whitespace-nowrap">{{ ($pages->currentPage() - 1) * $pages->links()->paginator->perPage() + $loop->iteration }}.
                                            </td>
                                            <td class="p-3 whitespace-nowrap">{{ $commercialmgmt['name'] ?? '' }}</td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $commercialmgmt['description'] ?? '' }}</td>
                                            <td class="p-3 whitespace-nowrap">155</td>
                                            <td class="p-3 whitespace-nowrap">10</td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                            <div class="px-1 pb-2">
                                    {{ $pages->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </x-slot>
</x-crm.form>
