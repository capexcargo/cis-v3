<x-crm.form x-data="{
    search_form: false,
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
    </x-slot>
    <x-slot name="header_title">CDM Reports</x-slot>
    <x-slot name="filter">
        <div class="grid grid-cols-4 gap-4 md:w-3/4">
            <div>
                <x-crm.input type="date" wire:model.defer="date_created_search" name="date_created_search">
                </x-crm.input>
            </div>
            <div class="grid grid-cols-3">
                <select wire:model.defer="date_range_search" name="date_range_search"
                    class="sm:w-24 md:w-24 lg:w-24 2xl:w-24 text-gray-600 block w-full border border-gray-500 py-[2px] px-2 
                    rounded-md shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50">
                    <option value="1">Daily</option>
                    <option value="2">Weekly</option>
                    <option value="3">Monthly</option>
                </select>
                <div class="flex justify-start sm:ml-16 md:ml-16 lg:ml-16 2xl:ml-8">
                    <x-crm.button type="button" wire:click="search" title="Search"
                        class="text-white bg-blue hover:bg-blue-800" />
                </div>
            </div>
        </div>
    </x-slot>
    <x-slot name="body" size="w-10/12 xs:w-10/12 sm:w-10/12 md:w-10/12">
        <div>
            {{-- Pie chart --}}
            <div class="grid grid-cols-12 gap-5 p-2">
                <div class="sm:col-span-6 md:col-span-6 lg:col-span-6 xl:col-span-6 2xl:col-span-3">
                    <div class="bg-white rounded-lg shadow-lg">
                        <div class="flex justify-between">
                            <div class="min-w-0 px-4 py-2 text-left">
                                <div class="">
                                    <h4 class="font-semibold text-gray-500">
                                        Customer Type
                                    </h4>
                                    <h5 class="text-xs font-normal text-gray-500">TOTAL :
                                        {{ $count_individual + $count_corporate }}</h5>
                                </div>
                            </div>
                            <div class="min-w-0 p-3 font-semibold">
                                <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                    wire:click="action({},'see_customer_type')">
                                    See More
                                </h4>
                            </div>
                        </div>
                        <div class="flex -mt-4">
                            <div class="w-56 h-56 xs:w-56 xs:h-56">
                                <canvas class="p-4" id="customer_type_pie_chart"></canvas>
                            </div>
                            <div class="grid grid-cols-1 p-2 mt-3">
                                <div class="w-56">
                                    <h4 class="flex text-sm">
                                        <div class="w-4 h-4 mt-3 mr-2 rounded-md bg-blue-pie"></div>
                                        <p class="font-normal text-gray-500 uppercase">Individuals</p>
                                    </h4>
                                    <p class="ml-6 -mt-2 text-xl font-normal">{{ $count_individual }}</p><br>
                                    <h4 class="flex text-sm">
                                        <div class="w-4 h-4 mt-3 mr-2 rounded-md bg-sky-blue-pie"></div>
                                        <p class="font-normal text-gray-500 uppercase">Corporate</p>
                                    </h4>
                                    <p class="ml-6 -mt-2 text-xl font-normal">{{ $count_corporate }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sm:col-span-6 md:col-span-6 lg:col-span-6 xl:col-span-6 2xl:col-span-3">
                    <div class="bg-white rounded-lg shadow-lg">
                        <div class="flex justify-between">
                            <div class="min-w-0 px-4 py-2 text-left">
                                <div class="">
                                    <h4 class="font-semibold text-gray-500">
                                        Account Status
                                    </h4>
                                    <h5 class="text-xs font-normal text-gray-500">TOTAL :
                                        {{ $count_as_active + $count_as_inactive }}</h5>
                                </div>
                            </div>
                            <div class="min-w-0 p-3 font-semibold">
                                <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                    wire:click="action({},'see_account_status')">
                                    See More
                                </h4>
                            </div>
                        </div>
                        <div class="flex -mt-4">
                            <div class="w-56 h-56 xs:w-56 xs:h-56">
                                <canvas class="p-6" id="account_status_pie_chart"></canvas>
                            </div>
                            <div class="grid grid-cols-1 p-2 mt-3">
                                <div class="w-56">
                                    <h4 class="flex text-sm">
                                        <div class="w-4 h-4 mr-2 mt-3 rounded-md bg-[#67E66F]"></div>
                                        <p class="font-normal text-gray-500 uppercase">Active</p>
                                    </h4>
                                    <p class="ml-6 -mt-2 text-xl font-normal">{{ $count_as_active }}</p><br>
                                    <h4 class="flex text-sm">
                                        <div class="w-4 h-4 mr-2 mt-3 rounded-md bg-[#FC9788]"></div>
                                        <p class="font-normal text-gray-500 uppercase">InActive</p>
                                    </h4>
                                    <p class="ml-6 -mt-2 text-xl font-normal">{{ $count_as_inactive }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sm:col-span-6 md:col-span-6 lg:col-span-6 xl:col-span-6 2xl:col-span-3">
                    <div class="bg-white rounded-lg shadow-lg">
                        <div class="flex justify-between">
                            <div class="min-w-0 px-4 py-2 text-left">
                                <div class="">
                                    <h4 class="font-semibold text-gray-500">
                                        Customer Category
                                    </h4>
                                    <h5 class="text-xs font-normal text-gray-500">TOTAL :
                                        {{ $count_cc_active + $count_cc_inactive + $count_cc_prospect }}</h5>
                                </div>
                            </div>
                            <div class="min-w-0 p-3 font-semibold">
                                <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                    wire:click="action({},'see_customer_category')">
                                    See More
                                </h4>
                            </div>
                        </div>
                        <div class="flex -mt-4">
                            <div class="w-56 h-56 xs:w-56 xs:h-56">
                                <canvas class="p-6" id="customer_category_pie_chart"></canvas>
                            </div>
                            <div class="grid grid-cols-1 p-2 mt-3">
                                <div class="w-56">
                                    <div class="">
                                        <h4 class="flex text-sm">
                                            <div class="w-4 h-4 mr-2 mt-3 rounded-md bg-[#67E66F]"></div>
                                            <p class="font-normal text-gray-500 uppercase">Active</p>
                                        </h4>
                                        <p class="ml-6 -mt-2 text-xl font-normal">{{ $count_cc_active }}</p><br>
                                    </div>
                                    <div class="-mt-4">
                                        <h4 class="flex text-sm">
                                            <div class="w-4 h-4 mr-2 mt-3 rounded-md bg-[#FFD360]"></div>
                                            <p class="font-normal text-gray-500 uppercase">Prospect</p>
                                        </h4>
                                        <p class="ml-6 -mt-2 text-xl font-normal">{{ $count_cc_prospect }}</p><br>
                                    </div>
                                    <div class="-mt-4">
                                        <h4 class="flex text-sm">
                                            <div class="w-4 h-4 mr-2 mt-3 rounded-md bg-[#FC9788]"></div>
                                            <p class="font-normal text-gray-500 uppercase">InActive</p>
                                        </h4>
                                        <p class="ml-6 -mt-2 text-xl font-normal">{{ $count_cc_inactive }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sm:col-span-6 md:col-span-6 lg:col-span-6 xl:col-span-6 2xl:col-span-3">
                    <div class="bg-white rounded-lg shadow-lg">
                        <div class="flex justify-between">
                            <div class="min-w-0 px-4 py-2 text-left">
                                <div class="">
                                    <h4 class="font-semibold text-gray-500">
                                        Contact Lifestage
                                    </h4>
                                    <h5 class="text-xs font-normal text-gray-500">TOTAL :
                                        {{ $count_ls_lead + $count_ls_customer }}</h5>
                                </div>
                            </div>
                            <div class="min-w-0 p-3 font-semibold">
                                <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                    wire:click="action({},'see_contact_lifestage')">
                                    See More
                                </h4>
                            </div>
                        </div>
                        <div class="flex -mt-4">
                            <div class="w-56 h-56 xs:w-56 xs:h-56">
                                <canvas class="p-6" id="contact_lifestage_pie_chart"></canvas>
                            </div>
                            <div class="grid grid-cols-1 p-2 mt-3">
                                <div class="w-56">
                                    <h4 class="flex text-sm">
                                        <div class="w-4 h-4 mt-3 mr-2 rounded-md bg-blue-pie"></div>
                                        <p class="font-normal text-gray-500 uppercase">Customers</p>
                                    </h4>
                                    <p class="ml-6 -mt-2 text-xl font-normal">{{ $count_ls_customer }}</p><br>
                                    <h4 class="flex text-sm">
                                        <div class="w-4 h-4 mt-3 mr-2 rounded-md bg-sky-blue-pie"></div>
                                        <p class="font-normal text-gray-500 uppercase">Leads</p>
                                    </h4>
                                    <p class="ml-6 -mt-2 text-xl font-normal">{{ $count_ls_lead }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Marketing Channels Chart --}}
            <div class="grid grid-cols-12 gap-5 p-2">
                <div class="sm:col-span-12 md:col-span-12 lg:col-span-12 2xl:col-span-8">
                    <div class="bg-white rounded-lg shadow-lg">
                        <div class="flex justify-between">
                            <div class="min-w-0 px-6 py-3 text-left">
                                <div class="">
                                    <h4 class="font-semibold text-gray-500">
                                        Marketing Channels
                                    </h4>
                                </div>
                            </div>
                            <div class="min-w-0 p-3 font-semibold">
                                <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                    wire:click="action({},'see_marketing_channel')">
                                    See More
                                </h4>
                            </div>
                        </div>
                        <div class="flex px-6 -mt-4">
                            <div id="chartBarMarketingChannels" style="width: 100%; height:340px;"></div>

                        </div>
                    </div>
                </div>
                <div class="sm:col-span-12 md:col-span-12 lg:col-span-12 2xl:col-span-4">
                    <div class="bg-white rounded-lg shadow-lg" style=" height:372px;">
                        <div class="flex justify-between">
                            <div class="min-w-0 px-6 py-3 text-left">
                                <div class="">
                                    <h4 class="font-semibold text-gray-500">
                                        Onboarding Channels
                                    </h4>
                                </div>
                            </div>
                            <div class="min-w-0 p-3 font-semibold">
                                <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                    wire:click="action({},'see_onboarding_channel')">
                                    See More
                                </h4>
                            </div>
                        </div>
                        <div class="px-6">
                            <div class="grid grid-cols-1 mt-4">
                                <div>
                                    <p class="text-sm font-normal text-gray-500">CRM</p>
                                    <div class="flex gap-2">
                                        <div class="w-full border border-[#7F7F7F] rounded-lg h-[13px] mb-6">
                                            <div class="bg-[#389AEB] h-3 rounded-lg" style="width: 25%"></div>
                                        </div>
                                        <h4 class="-mt-1 font-semibold">25%</h4>
                                    </div>
                                </div>
                                <div>
                                    <p class="text-sm font-normal text-gray-500">CaPEx Website</p>
                                    <div class="flex gap-2">
                                        <div class="w-full border border-[#7F7F7F] rounded-lg h-[13px] mb-6">
                                            <div class="bg-[#389AEB] h-3 rounded-lg" style="width: 30%"></div>
                                        </div>
                                        <h4 class="-mt-1 font-semibold">30%</h4>
                                    </div>
                                </div>
                                <div>
                                    <p class="text-sm font-normal text-gray-500">CaPEx Mobile Application</p>
                                    <div class="flex gap-2">
                                        <div class="w-full border border-[#7F7F7F] rounded-lg h-[13px] mb-6">
                                            <div class="bg-[#389AEB] h-3 rounded-lg" style="width: 10%"></div>
                                        </div>
                                        <h4 class="-mt-1 font-semibold">10%</h4>
                                    </div>
                                </div>
                                <div>
                                    <p class="text-sm font-normal text-gray-500">Service Request</p>
                                    <div class="flex gap-2">
                                        <div class="w-full border border-[#7F7F7F] rounded-lg h-[13px] mb-6">
                                            <div class="bg-[#389AEB] h-3 rounded-lg" style="width: 25%"></div>
                                        </div>
                                        <h4 class="-mt-1 font-semibold">25%</h4>
                                    </div>
                                </div>
                                <div>
                                    <p class="text-sm font-normal text-gray-500">FSC</p>
                                    <div class="flex gap-2">
                                        <div class="w-full border border-[#7F7F7F] rounded-lg h-[13px] mb-6">
                                            <div class="bg-[#389AEB] h-3 rounded-lg" style="width: 10%"></div>
                                        </div>
                                        <h4 class="-mt-1 font-semibold">10%</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Industry Chart --}}
            <div class="grid grid-cols-1 gap-5 p-2">
                <div class="grid grid-cols-12 bg-white rounded-lg shadow-lg">
                    <div class="sm:col-span-12 md:col-span-12 lg:col-span-12">
                        <div class="flex justify-between">
                            <div class="min-w-0 px-6 py-3 text-left">
                                <div class="">
                                    <h4 class="font-semibold text-gray-500">
                                        Industry
                                    </h4>
                                </div>
                            </div>
                            <div class="min-w-0 p-3 font-semibold">
                                <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                    wire:click="action({},'see_industry')">
                                    See More
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="sm:col-span-12 md:col-span-12 lg:col-span-12 2xl:col-span-8">
                        <div class="flex px-6 -mt-4">
                            <div id="chartBarIndustry" style="width: 100%; height:340px;"></div>
                        </div>
                    </div>
                    <div
                        class="sm:pb-4 md:pb-4 lg:pb-4 sm:px-4 md:px-4 lg:px-4 sm:col-span-12 md:col-span-12 lg:col-span-12 2xl:col-span-4">
                        <div class="grid grid-cols-12 gap-4 mt-6">
                            <div class="flex justify-center col-span-4 bg-white rounded-lg shadow-lg">
                                <div class="px-6 py-3 text-center">
                                    <h4 class="font-normal text-gray-500 whitespace-nowrap" style="font-size: 14px">
                                        Food Industry
                                    </h4>
                                    <div class="grid grid-cols-12">
                                        <div class="col-span-4"></div>
                                        <div class="col-span-4 text-lg font-semibold">
                                            {{ $prcntg_ind_food_industry }}%
                                        </div>
                                        <div
                                            class="col-span-4 whitespace-nowrap
                                        {{ $food_industry_total_percentage > 0
                                            ? 'text-green-500'
                                            : ($food_industry_total_percentage < 0
                                                ? 'text-red-500'
                                                : 'text-orange') }} ml-2">
                                            <span class="text-xs">{{ $food_industry_total_percentage }}%</span>
                                            @if ($food_industry_total_percentage > 0)
                                                &#42779;
                                            @elseif($food_industry_total_percentage < 0)
                                                &#42780;
                                            @else
                                                &#126;
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex justify-center col-span-4 bg-white rounded-lg shadow-lg">
                                <div class="px-6 py-3 text-center">
                                    <h4 class="font-normal text-gray-500 whitespace-nowrap" style="font-size: 14px">
                                        Agriculture
                                    </h4>
                                    <div class="grid grid-cols-12">
                                        <div class="col-span-4"></div>
                                        <div class="col-span-4 text-lg font-semibold">
                                            {{ $prcntg_ind_agriculture }}%
                                        </div>
                                        <div
                                            class="col-span-4 whitespace-nowrap
                                        {{ $agriculture_total_percentage > 0
                                            ? 'text-green-500'
                                            : ($agriculture_total_percentage < 0
                                                ? 'text-red-500'
                                                : 'text-orange') }} ml-2">
                                            <span class="text-xs">{{ $agriculture_total_percentage }}%</span>
                                            @if ($agriculture_total_percentage > 0)
                                                &#42779;
                                            @elseif($agriculture_total_percentage < 0)
                                                &#42780;
                                            @else
                                                &#126;
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex justify-center col-span-4 bg-white rounded-lg shadow-lg">
                                <div class="px-6 py-3 text-center">
                                    <h4 class="font-normal text-gray-500 whitespace-nowrap" style="font-size: 14px">
                                        Construction
                                    </h4>
                                    <div class="grid grid-cols-12">
                                        <div class="col-span-4"></div>
                                        <div class="col-span-4 text-lg font-semibold">
                                            {{ $prcntg_ind_construction }}%
                                        </div>
                                        <div
                                            class="col-span-4 whitespace-nowrap
                                        {{ $construction_total_percentage > 0
                                            ? 'text-green-500'
                                            : ($construction_total_percentage < 0
                                                ? 'text-red-500'
                                                : 'text-orange') }} ml-2">
                                            <span class="text-xs">{{ $construction_total_percentage }}%</span>
                                            @if ($construction_total_percentage > 0)
                                                &#42779;
                                            @elseif($construction_total_percentage < 0)
                                                &#42780;
                                            @else
                                                &#126;
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="grid grid-cols-12 gap-4 mt-6">
                            <div class="flex justify-center col-span-4 bg-white rounded-lg shadow-lg">
                                <div class="px-6 py-3 text-center">
                                    <h4 class="font-normal text-gray-500 whitespace-nowrap" style="font-size: 14px">
                                        Automotive
                                    </h4>
                                    <div class="grid grid-cols-12">
                                        <div class="col-span-4"></div>
                                        <div class="col-span-4 text-lg font-semibold">
                                            {{ $prcntg_ind_automotive }}%
                                        </div>
                                        <div
                                            class="col-span-4 whitespace-nowrap
                                        {{ $automotive_total_percentage > 0
                                            ? 'text-green-500'
                                            : ($automotive_total_percentage < 0
                                                ? 'text-red-500'
                                                : 'text-orange') }} ml-2">
                                            <span class="text-xs">{{ $automotive_total_percentage }}%</span>
                                            @if ($automotive_total_percentage > 0)
                                                &#42779;
                                            @elseif($automotive_total_percentage < 0)
                                                &#42780;
                                            @else
                                                &#126;
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex justify-center col-span-4 bg-white rounded-lg shadow-lg">
                                <div class="px-6 py-3 text-center">
                                    <h4 class="font-normal text-gray-500 whitespace-nowrap" style="font-size: 14px">
                                        Manufacturing
                                    </h4>
                                    <div class="grid grid-cols-12">
                                        <div class="col-span-4"></div>
                                        <div class="col-span-4 text-lg font-semibold">
                                            {{ $prcntg_ind_manufacturing }}%
                                        </div>
                                        <div
                                            class="col-span-4 whitespace-nowrap
                                        {{ $manufacturing_total_percentage > 0
                                            ? 'text-green-500'
                                            : ($manufacturing_total_percentage < 0
                                                ? 'text-red-500'
                                                : 'text-orange') }} ml-2">
                                            <span class="text-xs">{{ $manufacturing_total_percentage }}%</span>
                                            @if ($manufacturing_total_percentage > 0)
                                                &#42779;
                                            @elseif($manufacturing_total_percentage < 0)
                                                &#42780;
                                            @else
                                                &#126;
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex justify-center col-span-4 bg-white rounded-lg shadow-lg">
                                <div class="px-6 py-3 text-center">
                                    <h4 class="font-normal text-gray-500 whitespace-nowrap" style="font-size: 14px">
                                        Financial Services
                                    </h4>
                                    <div class="grid grid-cols-12">
                                        <div class="col-span-4"></div>
                                        <div class="col-span-4 text-lg font-semibold">
                                            {{ $prcntg_ind_financial_services }}%
                                        </div>
                                        <div
                                            class="col-span-4 whitespace-nowrap
                                        {{ $financial_services_total_percentage > 0
                                            ? 'text-green-500'
                                            : ($financial_services_total_percentage < 0
                                                ? 'text-red-500'
                                                : 'text-orange') }} ml-2">
                                            <span class="text-xs">{{ $financial_services_total_percentage }}%</span>
                                            @if ($financial_services_total_percentage > 0)
                                                &#42779;
                                            @elseif($financial_services_total_percentage < 0)
                                                &#42780;
                                            @else
                                                &#126;
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="grid grid-cols-12 gap-4 mt-6">
                            <div class="flex justify-center col-span-4 bg-white rounded-lg shadow-lg">
                                <div class="px-6 py-3 text-center">
                                    <h4 class="font-normal text-gray-500 whitespace-nowrap" style="font-size: 14px">
                                        Transport
                                    </h4>
                                    <div class="grid grid-cols-12">
                                        <div class="col-span-4"></div>
                                        <div class="col-span-4 text-lg font-semibold">
                                            {{ $prcntg_ind_transport }}%
                                        </div>
                                        <div
                                            class="col-span-4 whitespace-nowrap
                                        {{ $transport_total_percentage > 0
                                            ? 'text-green-500'
                                            : ($transport_total_percentage < 0
                                                ? 'text-red-500'
                                                : 'text-orange') }} ml-2">
                                            <span class="text-xs">{{ $transport_total_percentage }}%</span>
                                            @if ($transport_total_percentage > 0)
                                                &#42779;
                                            @elseif($transport_total_percentage < 0)
                                                &#42780;
                                            @else
                                                &#126;
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex justify-center col-span-4 bg-white rounded-lg shadow-lg">
                                <div class="px-6 py-3 text-center">
                                    <h4 class="font-normal text-gray-500 whitespace-nowrap" style="font-size: 14px">
                                        Logistics
                                    </h4>
                                    <div class="grid grid-cols-12">
                                        <div class="col-span-4"></div>
                                        <div class="col-span-4 text-lg font-semibold">
                                            {{ $prcntg_ind_logistics }}%
                                        </div>
                                        <div
                                            class="col-span-4 whitespace-nowrap
                                        {{ $logistics_total_percentage > 0
                                            ? 'text-green-500'
                                            : ($logistics_total_percentage < 0
                                                ? 'text-red-500'
                                                : 'text-orange') }} ml-2">
                                            <span class="text-xs">{{ $logistics_total_percentage }}%</span>
                                            @if ($logistics_total_percentage > 0)
                                                &#42779;
                                            @elseif($logistics_total_percentage < 0)
                                                &#42780;
                                            @else
                                                &#126;
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex justify-center col-span-4 bg-white rounded-lg shadow-lg">
                                <div class="px-6 py-3 text-center">
                                    <h4 class="font-normal text-gray-500 whitespace-nowrap" style="font-size: 14px">
                                        Commerce
                                    </h4>
                                    <div class="grid grid-cols-12">
                                        <div class="col-span-4"></div>
                                        <div class="col-span-4 text-lg font-semibold">
                                            {{ $prcntg_ind_commerces }}%
                                        </div>
                                        <div
                                            class="col-span-4 whitespace-nowrap
                                        {{ $commerces_total_percentage > 0
                                            ? 'text-green-500'
                                            : ($commerces_total_percentage < 0
                                                ? 'text-red-500'
                                                : 'text-orange') }} ml-2">
                                            <span class="text-xs">{{ $commerces_total_percentage }}%</span>
                                            @if ($commerces_total_percentage > 0)
                                                &#42779;
                                            @elseif($commerces_total_percentage < 0)
                                                &#42780;
                                            @else
                                                &#126;
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Contact Owner and Creation Report --}}
            <div class="grid grid-cols-12 gap-5 p-2">
                <div class="col-span-5">
                    <div class="bg-white rounded-lg shadow-lg">
                        <div class="flex justify-between">
                            <div class="min-w-0 p-3 px-6 text-left">
                                <div class="font-semibold text-gray-500 ">
                                    <h4 class="text-[16px]">
                                        Contact Owner
                                    </h4>
                                    <p class="text-xs font-normal">{{ count($contact_owners) }} CSM Agents</p>
                                </div>
                            </div>
                        </div>
                        <div class="p-3 px-6 overflow-auto" style="height: 32vh">
                            <div class="bg-white rounded-lg shadow-md">
                                <x-table.table>
                                    <x-slot name="thead">
                                        <x-table.th name="No." />
                                        <x-table.th name="Name" />
                                        <x-table.th name="Owned / Onboarded" />
                                    </x-slot>
                                    <x-slot name="tbody">
                                        @foreach ($contact_owners as $contact_owner)
                                            <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                                <td class="p-3 whitespace-nowrap">
                                                    {{ ($contact_owners->currentPage() - 1) * $contact_owners->links()->paginator->perPage() + $loop->iteration }}.
                                                </td>
                                                <td class="p-2 whitespace-nowrap w-90">
                                                    <div class="flex space-x-2 w-50">
                                                        <div class="p-5 bg-center bg-no-repeat bg-cover rounded-full"
                                                            style="background-image: url('https://i.ibb.co/rsT0hzM/rider.png')">
                                                        </div>
                                                        <div class="flex items-center text-sm">
                                                            <div>
                                                                <p class="flex-initial">
                                                                    {{ $contact_owner->name }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    {{ $contact_owner->count_contact_own }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </x-slot>
                                </x-table.table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-span-5">
                    <div class="bg-white rounded-lg shadow-lg">
                        <div class="flex justify-between">
                            <div class="min-w-0 p-3 px-6 text-left">
                                <div class="font-semibold text-gray-500 ">
                                    <h4 class="text-[16px]">
                                        Contact Creation Report
                                    </h4>
                                    <p class="text-xs font-normal">{{ date('F d, Y') }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="p-3 px-6 overflow-auto" style="height: 32vh">
                            <div class="bg-white rounded-lg shadow-md">
                                <x-table.table>
                                    <x-slot name="thead">
                                        <x-table.th name="No." />
                                        <x-table.th name="Date" />
                                        <x-table.th name="Contacts Created" />
                                    </x-slot>
                                    <x-slot name="tbody">
                                        @foreach ($contacts_creation_report as $ccr)
                                            <tr class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                                <td class="p-3 whitespace-nowrap">
                                                    {{ ($contacts_creation_report->currentPage() - 1) * $contacts_creation_report->links()->paginator->perPage() + $loop->iteration }}.
                                                </td>
                                                <td class="p-2 whitespace-nowrap w-90">
                                                    {{ date('F d, Y', strtotime($ccr->date)) }}
                                                </td>
                                                <td class="p-3 whitespace-nowrap">
                                                    {{ $ccr->count_contact_created }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </x-slot>
                                </x-table.table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-crm.form>
@push('scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script type="text/javascript">
        // Livewire.on('updateChart', (data) => {
        //     console.log(data);
        // Customer Type
        const dataPie = {
            datasets: [{
                label: "Customer Type",
                data: [{{ $count_individual }}, {{ $count_corporate }}],
                backgroundColor: [
                    "#1F78B4",
                    "#A6CEE3",
                ],
                hoverOffset: 4,
            }, ],
        };
        const configPie = {
            type: "pie",
            data: dataPie,
            options: {},
        };

        var chartBar = new Chart(document.getElementById("customer_type_pie_chart").getContext("2d"),
            configPie);

        // chartBar.destroy();
        // });



        // Account Status
        const dataPie2 = {
            datasets: [{
                label: "Account Status",
                data: [{{ $count_as_active }}, {{ $count_as_inactive }}],
                backgroundColor: [
                    "#67E66F",
                    "#FC9788",
                ],
                hoverOffset: 4,
            }, ],
        };
        const configPie2 = {
            type: "pie",
            data: dataPie2,
            options: {},
        };

        // Customer Category
        const dataPie3 = {
            datasets: [{
                label: "Customer Category",
                data: [{{ $count_cc_active }}, {{ $count_cc_inactive }}, {{ $count_cc_prospect }}],

                backgroundColor: [
                    "#67E66F",
                    "#FC9788",
                    "#FFD360",
                ],
                hoverOffset: 4,
            }, ],
        };
        const configPie3 = {
            type: "pie",
            data: dataPie3,
            options: {},
        };

        // Contact Lifestage
        const dataPie4 = {
            datasets: [{
                label: "Contact Lifestage",
                data: [{{ $count_ls_customer }}, {{ $count_ls_lead }}],
                backgroundColor: [
                    "#1F78B4",
                    "#A6CEE3",
                ],
                hoverOffset: 4,
            }, ],
        };
        const configPie4 = {
            type: "pie",
            data: dataPie4,
            options: {},
        };
        var chartBar = new Chart(document.getElementById("account_status_pie_chart"), configPie2);
        var chartBar = new Chart(document.getElementById("customer_category_pie_chart"), configPie3);
        var chartBar = new Chart(document.getElementById("contact_lifestage_pie_chart"), configPie4);

        // Marketing Channels
        google.charts.load('current', {
            packages: ['corechart']
        }).then(function() {
            var data = new google.visualization.arrayToDataTable([
                ['Marketing Channels', 'Total', {
                    role: 'style'
                }],
                ['Social Media', {{ $count_mc_social_media }}, '#389AEB'],
                ['Google Search', {{ $count_mc_google_search }}, '#A775FF'],
                ['Digital Advertisement', {{ $count_mc_digital_ads }}, '#3BDD46'],
                ['Email Marketing', {{ $count_mc_email_marketing }}, '#FE8E7D'],
                ['Catalog Direct/Flyers', {{ $count_mc_catalog_flyers }}, '#FFCE48'],
                ['Outdoor Ads', {{ $count_mc_outdoor_ads }}, '#FFA742'],
                ['CaPEx Truck', {{ $count_mc_capex_truck }}, '#FF4FF3'],
                ['Events', {{ $count_mc_events }}, '#D0DFFD'],
                ['Direct Selling', {{ $count_mc_direct_selling }}, '#7AFF6B']
            ]);

            var view = new google.visualization.DataView(data);

            var options = {
                legend: {
                    position: 'none'
                },
                chart: {
                    title: '',
                    subtitle: ''
                },
                vAxes: {
                    0: {
                        title: 'Total no. of Customers'
                    }
                },
                bar: {
                    groupWidth: "30%"
                }
            };

            var chart = new google.visualization.ColumnChart(document.getElementById(
                "chartBarMarketingChannels"));
            chart.draw(view, options);
        });

        // Industry
        google.charts.load('current', {
            packages: ['corechart']
        }).then(function() {
            var data = new google.visualization.arrayToDataTable([
                ['Industry', 'Value', {
                    role: 'style'
                }],
                ['Food Industry', {{ $count_ind_food_industry }}, '#82ABFF'],
                ['Agriculture', {{ $count_ind_agriculture }}, '#82ABFF'],
                ['Construction', {{ $count_ind_construction }}, '#82ABFF'],
                ['Automotive', {{ $count_ind_automotive }}, '#82ABFF'],
                ['Manufacturing', {{ $count_ind_manufacturing }}, '#82ABFF'],
                ['Financial Services', {{ $count_ind_financial_services }}, '#82ABFF'],
                ['Transport', {{ $count_ind_transport }}, '#82ABFF'],
                ['Logistics', {{ $count_ind_logistics }}, '#82ABFF'],
                ['Commerce', {{ $count_ind_commerces }}, '#82ABFF']
            ]);

            var view = new google.visualization.DataView(data);

            var options = {
                legend: {
                    position: 'none'
                },
                chart: {
                    title: '',
                    subtitle: ''
                },
                vAxes: {
                    0: {
                        title: 'Total no. of Customers'
                    }
                },
                bar: {
                    groupWidth: "30%"
                }
            };

            var chart = new google.visualization.ColumnChart(document.getElementById("chartBarIndustry"));
            chart.draw(view, options);
        });
    </script>
@endpush
