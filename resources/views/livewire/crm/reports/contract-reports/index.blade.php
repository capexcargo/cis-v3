<x-crm.form x-data="{
    search_form: false,
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
    </x-slot>
    <x-slot name="header_title">Contract Reports</x-slot>
    <x-slot name="filter">
        <div class="grid grid-cols-4 gap-4 md:w-3/4">
            <div>
                <x-crm.input onchange="filterData()" type="date" wire:model.defer="date_created_search"
                    name="date_created_search" placeholder="">
                </x-crm.input>
            </div>
            <div class="grid grid-cols-3">
                <div class="flex justify-start ml-2">
                    <x-crm.button type="button" wire:click="search" name="search" title="Search"
                        class="text-white bg-blue hover:bg-blue-800" />
                </div>
            </div>
        </div>
    </x-slot>
    <x-slot name="body">

        <div class="grid grid-cols-12 gap-4">
            <div class="col-span-6 gap-6">
                <div class="ml-2 bg-white border rounded-lg shadow-lg">
                    <div class="flex justify-between">
                        <div class="min-w-0 px-6 py-3 text-left">
                            <div class="">
                                <h4 class="text-xl font-semibold text-gray-500">
                                    Account Application Status
                                </h4>
                                <P class="font-normal text-base text-gray-400 text-[10px]">
                                    TOTAL : {{ $totalapps }}
                                </P>
                            </div>
                        </div>
                        <div class="min-w-0 p-3 mr-2 font-semibold">
                            <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                wire:click="redirectTo({}, 'redirectToAccountsApplication')">
                                See More
                            </h4>
                        </div>
                    </div>

                    <div class="flex justify-center -mt-4">
                        <div class="-mt-2 h-72 w-72">
                            <canvas class="p-4 " style="height:100% ; width:100%;" id="chartPie"></canvas>
                        </div>
                    </div>

                    <div class="flex justify-center mt-8 mb-6 ml-12">
                        <div class="w-56 ml-8">
                            <h4 class="flex text-sm">
                                <div class="w-4 h-4 mt-3 mr-4 bg-blue-400 rounded-sm"></div>
                                <p class="font-normal text-gray-400">APPROVED</p>
                            </h4>
                            <p class="ml-8 -mt-2 text-xl font-normal" id="approved">{{ $totalapproved }}</p><br>
                        </div>
                        <div class="w-56">
                            <h4 class="flex text-sm">
                                <div class="w-4 h-4 mr-4 mt-3 rounded-sm bg-[#FFD360]"></div>
                                <p class="font-normal text-gray-400">FOR APPROVAL</p>
                            </h4>
                            <p class="ml-8 -mt-2 text-xl font-normal" id="approval">{{ $totalforapps }}
                            <p>
                        </div>
                        <div class="w-56">
                            <h4 class="flex text-sm">
                                <div class="w-4 h-4 mr-4 mt-3 rounded-sm bg-[#FC9788]"></div>
                                <p class="font-normal text-gray-400">DECLINED</p>
                            </h4>
                            <p class="ml-8 -mt-2 text-xl font-normal" id="declined">{{ $totaldeclined }}</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </x-slot>
</x-crm.form>
@push('scripts')
    <!-- Required chart.js -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
    <script>
        const totalsPie = [{{ $totalapproved }}, {{ $totalforapps }}, {{ $totaldeclined }}];
        drawChartBilog(totalsPie);

        function drawChartBilog(data) {
            const dataPie = {
                datasets: [{
                    label: "Account Application Status",
                    data: data,
                    backgroundColor: [
                        "#6b9bfa",
                        "#FFD360",
                        "#FC9788",
                    ],
                    borderWidth: 0,
                    hoverOffset: 4,
                }, ],
            };

            var options = {
                tooltips: {
                    enabled: false
                },
                events: [],

                plugins: {
                    datalabels: {
                        formatter: (value, chartPie) => {

                            let sum = 0;
                            let dataArr = chartPie.chart.data.datasets[0].data;
                            dataArr.map(data => {
                                sum += data;
                            });
                            let percentage = (value * 100 / sum).toFixed(0) + "%";
                            if(percentage == '0%') return '';
                            return percentage;


                        },
                        borderRadius: 0,
                        color: '#fff',
                        font: {
                            size: 26
                        },
                    }
                }
            };

            const configPie = {
                type: "pie",
                data: dataPie,
                options: options,
            };

            var chartBar = new Chart(document.getElementById("chartPie"), configPie);
        }

        document.addEventListener('livewire:load', function() {
            let totals = {};
            window.livewire.on('totalsarray', function(value) {
            const totalarray = [];
                    totals.approved = document.getElementById('approved').textContent = value[0];
                    totals.approval = document.getElementById('approval').textContent = value[1];
                    totals.declined = document.getElementById('declined').textContent = value[2];
                    for (const [key, value] of Object.entries(totals)) {
                        totalarray.push(value)
                    }
                drawChartBilog(totalarray);
            });

        });
    </script>
@endpush
