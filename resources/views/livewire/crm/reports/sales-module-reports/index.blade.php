<x-crm.form x-data="{
    search_form: false,
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>
    <x-slot name="modals">
    </x-slot>
    <x-slot name="header_title">Sales Module Reports</x-slot>
    <x-slot name="body" size="w-10/12 xs:w-10/12 sm:w-10/12 md:w-10/12">
        <div class="grid grid-cols-1 p-2 -mt-4">
            <div class="bg-white rounded-lg shadow-lg">
                <div class="grid grid-cols-1 gap-5 p-3">
                    <div class="grid grid-cols-2">
                        <div class="flex justify-between px-4">
                            <div class="text-blue text-2xl font-semibold">Sales Campaign Summary</div>
                            <div>
                                <div class="flex gap-4">
                                    <div class="w-full">
                                        <x-crm.select wire:model.defer="quarter_search" name="quarter_search">
                                            <option value="1">1st Quarter</option>
                                            <option value="2">2nd Quarter</option>
                                            <option value="3">3rd Quarter</option>
                                            <option value="4">4th Quarter</option>
                                        </x-crm.select>
                                    </div>
                                    <div class="w-32">
                                        <x-crm.select wire:model.defer="year_search" name="year_search">
                                            <option value="1">2023</option>
                                            <option value="2">2022</option>
                                            <option value="3">2021</option>
                                            <option value="4">2020</option>
                                            <option value="5">2019</option>
                                        </x-crm.select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex justify-end gap-8">
                            <div class="grid grid-cols-2">
                                <x-crm.select wire:model.defer="month_search" name="month_search">
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </x-crm.select>
                                <div class="flex justify-start ml-8">
                                    <x-crm.button type="button" wire:click="search" title="Search"
                                        class="text-white bg-blue hover:bg-blue-800" />
                                </div>
                            </div>
                            <div class="font-semibold mt-2">
                                <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                    wire:click="action({},'see_sales_campaign')">
                                    See More
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-2 border-t border-gray-300">
                    <div class="grid grid-cols-1">
                        <div class="grid grid-cols-2 px-6 py-2">
                            <div class="text-[#8D8D8D] text-xl font-semibold ml-1">Quarterly Sales Campaign</div>
                            <div>
                                <div class="flex justify-end gap-12">
                                    <div class="flex">
                                        <div class="w-4 h-4 mr-2 mt-1 rounded-sm bg-[#5485E5]"></div>Sales
                                    </div>
                                    <div class="flex">
                                        <div class="w-4 h-4 mr-2 mt-1 rounded-sm bg-[#D6E4FF]"></div>Expense
                                    </div>
                                    <div class="flex">
                                        <div class="w-4 h-4 mr-2 mt-1 rounded-sm bg-[#88E27D]"></div>Profit
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="px-8 mt-2">
                            <div class="py-2 pr-4" id="quarterly_sales_chart" style="width: 740px; height: 450px;">
                            </div>
                        </div> --}}
                        <div class="">
                            <div class="" id="quarterly_sales_chart" style="height: 60vh">
                            </div>
                        </div>
                    </div>
                    <div class="px-8 py-2 border-l border-gray-300">
                        <div class="grid grid-cols-2">
                            <div class="text-[#8D8D8D] text-xl font-semibold">Top Performing Sales Campaign For January
                            </div>
                            <div class="flex justify-end gap-12 mt-1">
                                <div class="flex">
                                    <div class="w-4 h-4 mr-2 mt-1 rounded-sm bg-[#5485E5]"></div>Sales
                                </div>
                                <div class="flex">
                                    <div class="w-4 h-4 mr-2 mt-1 rounded-sm bg-[#D6E4FF]"></div>Discount
                                </div>
                                <div class="flex">
                                    <div class="w-4 h-4 mr-2 mt-1 rounded-sm bg-[#88E27D]"></div>Profit
                                </div>
                            </div>
                        </div>

                        <div class="mt-6">
                            <div class="" id="top_performing_sales_chart" style="height: 46vh">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-cols-1 p-2 -mt-4">
            <div class="bg-white rounded-lg shadow-lg">
                <div class="grid grid-cols-1 gap-5 p-3">
                    <div class="grid grid-cols-2">
                        <div class="flex justify-between px-4">
                            <div class="text-blue text-2xl font-semibold">Leads</div>
                        </div>
                        <div class="flex justify-end gap-8">
                            <div class="w-56">
                                <x-crm.input type="date" wire:model.defer="date_created_search_lead"
                                    name="date_created_search_lead">
                                </x-crm.input>
                            </div>
                            <div class="grid grid-cols-2">
                                <div>
                                    <x-crm.select wire:model.defer="date_range_search_lead"
                                        name="date_range_search_lead">
                                        <option value="1">Daily</option>
                                        <option value="2">Weekly</option>
                                        <option value="3">Monthly</option>
                                    </x-crm.select>
                                </div>
                                <div class="flex justify-start ml-8">
                                    <x-crm.button type="button" wire:click="search" title="Search"
                                        class="text-white bg-blue hover:bg-blue-800" />
                                </div>
                            </div>
                            <div class="font-semibold mt-2">
                                <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                    wire:click="action({},'see_leads')">
                                    See More
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-1 px-6">
                    <div class="text-[#8D8D8D] text-xl font-semibold ml-1 -mt-1">Status Report</div>
                </div>
                {{-- Table --}}
                <div class="grid grid-cols-1 px-5 mt-2 shadow-md">
                    <div class="overflow-auto">
                        <div class="bg-white rounded-lg rounded-b-none shadow-md border border-gray-400"
                            {{-- style="height: 33vh" --}}>
                            <x-table.table>
                                <x-slot name="thead">
                                    <tr class="text-xs text-left text-gray-500 bg-white cursor-pointer">
                                        <th class="w-2/5 border-r border-gray-400">
                                            <div class="px-4 font-normal text-gray-700">
                                                <div class="flex gap-2 flex-nowrap">
                                                    <span class="w-56">Name</span>
                                                    <span class="w-44">Customer Acquisition Category</span>
                                                    <span class="w-28">No. of Leads</span>
                                                </div>
                                            </div>
                                        </th>
                                        <th class="w-3/5">
                                            <div class="grid grid-cols-1 font-normal">
                                                <div class="text-sm text-center bg-gray-200">Leads</div>
                                                <div class="flex gap-12 px-4">
                                                    <div class="flex w-1/5 border-r border-gray-400">
                                                        <div class="flex gap-12 text-xs">
                                                            <p>Retired</p>
                                                            <p>%</p>
                                                        </div>
                                                    </div>
                                                    <div class="flex w-1/5 border-r border-gray-400">
                                                        <div class="flex gap-8 text-xs">
                                                            <p style="margin-left: -18px">Unqualified</p>
                                                            <p>%</p>
                                                        </div>
                                                    </div>
                                                    <div class="flex w-1/5 border-r border-gray-400">
                                                        <div class="flex gap-12 text-xs">
                                                            <p style="margin-left: -16px">Qualified</p>
                                                            <p>%</p>
                                                        </div>
                                                    </div>
                                                    <div class="flex w-1/4">
                                                        <div class="flex gap-12 text-xs">
                                                            <p style="margin-left: -24px">Converted to Opportunities
                                                            </p>
                                                            <p>%</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($leads as $i => $lead)
                                        <tr>
                                            <td class="w-12 px-2 py-2 border-r border-gray-400">
                                                <div class="flex gap-4 mt-2 flex-nowrap">
                                                    <div class="flex space-x-2 w-56">
                                                        <div class="p-5 w-4 h-4 bg-center bg-no-repeat bg-cover rounded-full"
                                                            style="background-image: url('https://i.ibb.co/rsT0hzM/rider.png')">
                                                        </div>
                                                        <div class="flex items-center w-40 text-sm">
                                                            <p class="flex-initial">{{ $lead->name }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="grid grid-cols-1 w-44">
                                                        <div class="flex items-center w-40 text-sm">
                                                            <p class="flex-initial">New</p>
                                                        </div>
                                                        <div class="flex items-center w-40 text-sm mt-6">
                                                            <p class="flex-initial">Existing</p>
                                                        </div>
                                                    </div>
                                                    <div class="flex w-28">
                                                        <div class="flex items-center w-40 text-sm">
                                                            <p class="flex-initial">{{ $lead->no_of_leads }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="grid grid-cols-1 font-normal">
                                                    <div class="flex gap-12 px-4">
                                                        <div class="grid grid-cols-1 w-1/5">
                                                            <div class="flex gap-16 text-sm ml-2">
                                                                <p>{{ $lead->retired_new_count ?? 0 }}</p>
                                                                <p>{{ $lead->retired_new_qual_score ?? 0 }}%</p>
                                                            </div>
                                                            <div class="flex gap-16 text-sm ml-2 mt-6">
                                                                <p>{{ $lead->retired_existimg_count ?? 0 }}</p>
                                                                <p>{{ $lead->retired_existing_qual_score ?? 0 }}%</p>
                                                            </div>
                                                        </div>
                                                        <div class="border-l border-gray-400"
                                                            style="margin: -12px 0 -12px -49px;">
                                                        </div>
                                                        <div class="grid grid-cols-1 w-1/5">
                                                            <div class="flex gap-16 text-sm ml-2">
                                                                <p>{{ $lead->unqualified_new_count ?? 0 }}</p>
                                                                <p>{{ $lead->unqualified_new_qual_score ?? 0 }}%</p>
                                                            </div>
                                                            <div class="flex gap-16 text-sm ml-2 mt-6">
                                                                <p>{{ $lead->unqualified_existimg_count ?? 0 }}</p>
                                                                <p>{{ $lead->unqualified_existing_qual_score ?? 0 }}%
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="border-l border-gray-400"
                                                            style="margin: -12px 0 -12px -49px;">
                                                        </div>
                                                        <div class="grid grid-cols-1 w-1/5">
                                                            <div class="flex gap-16 text-sm ml-2">
                                                                <p>{{ $lead->qualified_new_count ?? 0 }}</p>
                                                                <p>{{ $lead->qualified_new_qual_score ?? 0 }}%</p>
                                                            </div>
                                                            <div class="flex gap-16 text-sm ml-2 mt-6">
                                                                <p>{{ $lead->qualified_existimg_count ?? 0 }}</p>
                                                                <p>{{ $lead->qualified_existing_qual_score ?? 0 }}%</p>
                                                            </div>
                                                        </div>
                                                        <div class="border-l border-gray-400"
                                                            style="margin: -12px 0 -12px -49px;">
                                                        </div>
                                                        <div class="grid grid-cols-1 w-1/4">
                                                            <div class="flex gap-16 text-sm ml-2">
                                                                <p class="ml-12">
                                                                    {{ $lead->converted_new_count ?? 0 }}
                                                                </p>
                                                                <p class="ml-12">
                                                                    {{ $lead->converted_new_qual_score ?? 0 }}%
                                                                </p>
                                                            </div>
                                                            <div class="flex gap-16 text-sm ml-2 mt-6">
                                                                <p class="ml-12">
                                                                    {{ $lead->converted_existimg_count ?? 0 }}
                                                                </p>
                                                                <p class="ml-12">
                                                                    {{ $lead->converted_existing_qual_score ?? 0 }}%
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                        </div>
                        <div class="flex bg-white rounded-lg rounded-t-none shadow-md border border-gray-400">
                            <div class="w-2/5 px-4 font-normal text-gray-700 my-2">
                                <div class="flex gap-2 flex-nowrap font-semibold">
                                    <span class="w-56"></span>
                                    <span class="w-44 text-[#003399]">TOTAL</span>
                                    <span class="w-28 text-[#003399] ml-1">{{ $total_no_of_leads }}</span>
                                </div>
                            </div>
                            <div class="w-3/5 px-4 font-normal text-gray-700 border-l border-gray-400">
                                <div class="flex gap-12 my-2">
                                    <div class="grid grid-cols-1 w-1/5">
                                        <div class="flex flex-nowrap font-semibold">
                                            <div class="flex gap-16 text-[#003399]">
                                                <p class="ml-2">{{ $total_retired_leads }}</p>
                                                <p>{{ $total_retired_leads_qual_score }}%</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="border-l border-gray-400" style="margin: -8px 0 -8px -49px;">
                                    </div>
                                    <div class="grid grid-cols-1 w-1/5">
                                        <div class="flex flex-nowrap font-semibold">
                                            <div class="flex gap-16 text-[#003399]">
                                                <p class="ml-2">{{ $total_unqualified_leads }}</p>
                                                <p>{{ $total_unqualified_leads_qual_score }}%</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="border-l border-gray-400" style="margin: -8px 0 -8px -49px;">
                                    </div>
                                    <div class="grid grid-cols-1 w-1/5">
                                        <div class="flex flex-nowrap font-semibold">
                                            <div class="flex gap-16 text-[#003399]">
                                                <p class="ml-2">{{ $total_qualified_leads }}</p>
                                                <p>{{ $total_qualified_leads_qual_score }}%</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="border-l border-gray-400" style="margin: -8px 0 -8px -49px;">
                                    </div>
                                    <div class="grid grid-cols-1 w-1/4">
                                        <div class="flex flex-nowrap font-semibold">
                                            <div class="flex gap-16 text-[#003399] ml-2">
                                                <p class="ml-10">{{ $total_converted_leads }}</p>
                                                <p class="ml-8">{{ $total_converted_leads_qual_score }}%</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="grid grid-cols-12 gap-5 py-2 mt-2">
                        <div class="col-span-8">
                            <div class="grid grid-cols-1 gap-5">
                                <div class="grid grid-cols-12 bg-white rounded-lg shadow-lg">
                                    <div class="col-span-12">
                                        <div class="flex justify-between">
                                            <div class="min-w-0 px-6 py-3 text-left">
                                                <div class="">
                                                    <h4 class="font-semibold text-gray-500">
                                                        Industry
                                                    </h4>
                                                    <h5 class="font-normal text-gray-500 text-xs">TOTAL NO. OF LEADS:
                                                        {{ $industry_total_leads }}</h5>
                                                </div>
                                            </div>
                                            <div class="min-w-0 p-3 font-semibold">
                                                <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                                    wire:click="action({},'see_industry')">
                                                    See More
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-span-12">
                                        <div class="flex py-3">
                                            <div id="chartBarIndustry" style="width: 100%; height:300px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-span-4">
                            <div class="grid grid-cols-1 gap-5">
                                <div class="grid grid-cols-12 bg-white rounded-lg shadow-lg">
                                    <div class="col-span-12">
                                        <div class="flex justify-between">
                                            <div class="min-w-0 px-6 py-3 text-left">
                                                <div class="">
                                                    <h4 class="font-semibold text-gray-500">
                                                        Service Requirement
                                                    </h4>
                                                    <h5 class="font-normal text-gray-500 text-xs">TOTAL NO. OF LEADS:
                                                        {{ $service_requirement_total_leads }}
                                                    </h5>
                                                </div>
                                            </div>
                                            <div class="min-w-0 p-3 font-semibold">
                                                <h4 class="text-xs text-blue-800 underline cursor-pointer underline-offset-4"
                                                    wire:click="action({},'see_service_requirement')">
                                                    See More
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-span-8">
                                        <div class="px-6 flex -mt-4">
                                            <div class="flex mt-4" style=" height:324px;">
                                                <div class="h-56 w-56">
                                                    <canvas class="p-4"
                                                        id="service_requirement_pie_chart"></canvas>
                                                </div>
                                                <div class="grid grid-cols-1 p-2 mt-3">
                                                    <div class="w-56">
                                                        <h4 class="flex text-sm">
                                                            <div class="w-6 h-4 rounded-sm mr-2 mt-4 bg-blue-pie">
                                                            </div>
                                                            <p class="font-normal uppercase text-gray-500">
                                                                DOMESTIC FREIGHT FORWARDING SERVICES
                                                            </p>
                                                        </h4>
                                                        <p class="ml-6 -mt-2 text-xl font-normal">
                                                            {{ $count_domestic_freight }}</p><br>
                                                        <h4 class="flex text-sm">
                                                            <div class="w-6 h-4 rounded-sm mr-2 mt-4 bg-sky-blue-pie">
                                                            </div>
                                                            <p class="font-normal uppercase text-gray-500">
                                                                INTERNATIONAL FREIGHT FORWARDING SERVICES
                                                            </p>
                                                        </h4>
                                                        <p class="ml-6 -mt-2 text-xl font-normal">
                                                            {{ $count_international_freight }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="grid grid-cols-12 gap-5 py-2 mt-2 mb-2 bg-white rounded-lg shadow-lg">
                        <div class="col-span-8">
                            <div class="grid grid-cols-12">
                                <div class="col-span-12">
                                    <div class="flex justify-between">
                                        <div class="min-w-0 px-6 py-3 text-left">
                                            <div class="">
                                                <h4 class="font-semibold text-gray-500">
                                                    Channel Source
                                                </h4>
                                                <h5 class="font-normal text-gray-500 text-xs">TOTAL NO. OF LEADS:
                                                    {{ $total_leads }}
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ml-2 pl-4 col-span-12">
                                    <div class="flex mt-2 mb-2">
                                        <div class="overflow-auto">
                                            <div class="bg-white rounded-lg shadow-md border-2 border-gray-300"
                                                style="width:101vh">
                                                <x-table.table>
                                                    <x-slot name="thead">
                                                        <x-table.th name="Channel Source" />
                                                        <x-table.th name="No. of Leads" />
                                                        <x-table.th name="Converted to Opportunities" />
                                                        <x-table.th name="Qualified" />
                                                        <x-table.th name="Unqualified" />
                                                        <x-table.th name="Retired" />
                                                    </x-slot>
                                                    <x-slot name="tbody">
                                                        @foreach ($channel_sources as $i => $channel_source)
                                                            <tr
                                                                class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                                                <td class="p-3 whitespace-nowrap">
                                                                    {{ $channel_source->name }}
                                                                </td>
                                                                <td class="p-3 whitespace-nowrap">
                                                                    {{ $channel_source->leads_count }}
                                                                </td>
                                                                <td class="p-3 whitespace-nowrap">
                                                                    {{ $channel_source->converted_count }}
                                                                </td>
                                                                <td class="p-3 whitespace-nowrap">
                                                                    {{ $channel_source->qualified_count }}
                                                                </td>
                                                                <td class="p-3 whitespace-nowrap">
                                                                    {{ $channel_source->unqualified_count }}
                                                                </td>
                                                                <td class="p-3 whitespace-nowrap">
                                                                    {{ $channel_source->retired_count }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </x-slot>
                                                </x-table.table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-span-4">
                            <div class="grid grid-cols-12">
                                <div class="col-span-12">
                                    <div class="flex justify-between">
                                        <div class="min-w-0 px-2 py-3 text-left">
                                            <div class="">
                                                <h4 class="font-semibold text-gray-500">
                                                    Channel Source Ranking
                                                </h4>
                                                <h5 class="font-normal text-gray-500 text-xs">TOTAL NO. OF CONVERTED TO
                                                    OPPORTUNITIES : {{ $total_converted }}
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-span-12">
                                    <div class="px-2 flex mt-2 mb-2">
                                        <div class="overflow-auto">
                                            <div class="bg-white rounded-lg shadow-md border-2 border-gray-300"
                                                {{-- style="height: 20vh; width:48vh" --}}>
                                                <x-table.table>
                                                    <x-slot name="thead">
                                                        <x-table.th name="Rank" />
                                                        <x-table.th name="Channel Source" />
                                                        <x-table.th name="Converted to Opportunities" />
                                                    </x-slot>
                                                    <x-slot name="tbody">
                                                        @foreach ($channel_source_rankings as $i => $csr)
                                                            <tr
                                                                class="border cursor-pointer hover:text-black hover:bg-[#eff6ff]">
                                                                <td class="p-3 whitespace-nowrap">
                                                                    {{ ($channel_source_rankings->currentPage() - 1) * $channel_source_rankings->links()->paginator->perPage() + $loop->iteration }}.
                                                                </td>
                                                                <td class="p-2 whitespace-nowrap w-90">
                                                                    <div class="flex space-x-2 w-50">
                                                                        <div class="flex items-center text-sm">
                                                                            <div>
                                                                                <p class="flex-initial">
                                                                                    {{ $csr->name }}
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td class="p-3 whitespace-nowrap">
                                                                    {{ $csr->converted_to_opps_count }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </x-slot>
                                                </x-table.table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-cols-1 gap-5 p-2">
            <div class="bg-white rounded-lg shadow-lg">
                <div class="grid grid-cols-1 gap-5 p-3">
                    <div class="grid grid-cols-2">
                        <div class="flex justify-between px-4">
                            <div class="text-blue text-2xl font-semibold">Opportunities</div>
                        </div>
                        <div class="flex justify-end gap-8">
                            <div class="w-56">
                                <x-crm.input type="date" wire:model.defer="date_created_search_opportunity"
                                    name="date_created_search_opportunity">
                                </x-crm.input>
                            </div>
                            <div class="grid grid-cols-2">
                                <div>
                                    <x-crm.select wire:model.defer="date_range_search_opportunity"
                                        name="date_range_search_opportunity">
                                        <option value="1">Daily</option>
                                        <option value="2">Weekly</option>
                                        <option value="3">Monthly</option>
                                    </x-crm.select>
                                </div>
                                <div class="flex justify-start ml-8">
                                    <x-crm.button type="button" wire:click="search" title="Search"
                                        class="text-white bg-blue hover:bg-blue-800" />
                                </div>
                            </div>
                            <div class="font-semibold mt-2">
                                <h4 class="text-sm text-blue-800 underline cursor-pointer underline-offset-4"
                                    wire:click="action({},'see_details_emp_info')">
                                    See More
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-1 px-6">
                    <div class="text-[#8D8D8D] text-xl font-semibold ml-1 -mt-1">Status Report</div>
                </div>
                <div class="grid grid-cols-1 px-5 mt-2 mb-4">
                    <div class="overflow-auto bg-white rounded-lg shadow-lg">
                        <div class="bg-white rounded-lg shadow-md border border-gray-400" {{-- style="height: 31vh" --}}>
                            <x-table.table>
                                <x-slot name="thead">
                                    <tr class="text-xs text-left text-gray-500 bg-white cursor-pointer">
                                        <th class="w-1/2 border-r border-gray-400">
                                            <div class="px-4 font-normal text-gray-700">
                                                <div class="flex gap-2 flex-nowrap">
                                                    <span class="w-40">Sales Funnel</span>
                                                    <div class="border-l border-gray-400"
                                                        style="margin: -12px 0 -12px -1px;">
                                                    </div>
                                                    <span class="w-48"></span>
                                                    <div class="border-l border-gray-400"
                                                        style="margin: -12px 0 -12px 0;">
                                                    </div>
                                                    <span class="w-44 px-4">No. of Opportunities</span>
                                                    <div class="border-l border-gray-400"
                                                        style="margin: -12px 0 -12px -1px;">
                                                    </div>
                                                    <span class="w-2 px-4">Opportunity Amount</span>
                                                </div>
                                            </div>
                                        </th>
                                        <th class="w-1/2">
                                            <div class="grid grid-cols-1 font-normal">
                                                <div class="text-sm text-center bg-gray-200">
                                                    Customer Acquisition Category
                                                </div>
                                                <div class="flex gap-12 px-4 text-xs">
                                                    <div class="grid grid-cols-1 w-1/5">
                                                        <p>Initiation</p>
                                                    </div>
                                                    <div class="grid grid-cols-1 w-1/5">
                                                        <p>Quotation</p>
                                                    </div>
                                                    <div class="grid grid-cols-1 w-1/5">
                                                        <p style="">Negotiation</p>
                                                    </div>
                                                    <div class="grid grid-cols-1 w-1/5">
                                                        <p class="ml-6">Loss</p>
                                                    </div>
                                                    <div class="grid grid-cols-1 w-1/5">
                                                        <p class="ml-6">Won</p>
                                                    </div>
                                                    <div class="fgrid grid-cols-1 w-1/5 whitespace-nowrap">
                                                        <p style="">Conversion %</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($opportunities as $i => $opportunity)
                                        <tr>
                                            <td class="w-12 px-2 py-2 border-r border-gray-400">
                                                <div class="flex gap-4 flex-nowrap">
                                                    <div class="flex space-x-2 w-40">
                                                        <div class="mt-3 p-5 w-4 h-4 bg-center bg-no-repeat bg-cover rounded-full"
                                                            style="background-image: url('https://i.ibb.co/rsT0hzM/rider.png')">
                                                        </div>
                                                        <div class="flex items-center w-40 text-sm">
                                                            <p class="flex-initial">{{ $opportunity->name }}</p>
                                                        </div>
                                                    </div>
                                                    <div class=" border-l border-gray-400"
                                                        style="margin: -8px 0 -4px 0;">
                                                    </div>
                                                    <div class="grid grid-cols-1 w-44">
                                                        <div class="flex items-center w-40 text-sm">
                                                            <p class="flex-initial">No. of Opportunities</p>
                                                        </div>
                                                        <div class="border-b border-gray-400"
                                                            style="margin: 10px -16px -5px -16px;">
                                                        </div>
                                                        <div class="flex items-center w-40 text-sm mt-5">
                                                            <p class="flex-initial">Deal Size</p>
                                                        </div>
                                                    </div>
                                                    <div class=" border-l border-gray-400"
                                                        style="margin: -8px 0 -4px 0;">
                                                    </div>
                                                    <div class="flex w-40 px-4">
                                                        <div class="flex items-center w-40 text-sm">
                                                            <p class="flex-initial">
                                                                {{ $opportunity->no_of_opportunities }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class=" border-l border-gray-400"
                                                        style="margin: -8px 0 -4px -1px;">
                                                    </div>
                                                    <div class="flex w-40 px-4">
                                                        <div class="flex items-center w-40 text-sm">
                                                            <p class="flex-initial">
                                                                {{ $opportunity->total_opportunity_amount }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="grid grid-cols-1 ">
                                                    <div class="flex gap-12 px-4">
                                                        <div class="grid grid-cols-1 w-1/5">
                                                            <div>
                                                                <p class="text-center">
                                                                    {{ $opportunity->opportunities_initiation }}</p>
                                                            </div>
                                                            <div class="border-b border-gray-400"
                                                                style="margin: 10px -24px -5px -18px;">
                                                            </div>
                                                            <div class="mt-6 text-center">
                                                                <p>{{ number_format($opportunity->deal_size_initiation, 2) }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="grid grid-cols-1 w-1/5">
                                                            <div>
                                                                <p class="text-center">
                                                                    {{ $opportunity->opportunities_quotation }}</p>
                                                            </div>
                                                            <div class="border-b border-gray-400"
                                                                style="margin: 10px -24px -5px -24px;">
                                                            </div>
                                                            <div class="mt-6 text-center">
                                                                <p>{{ number_format($opportunity->deal_size_quotation, 2) }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="grid grid-cols-1 w-1/5">
                                                            <div>
                                                                <p class="text-center">
                                                                    {{ $opportunity->opportunities_negotation }}</p>
                                                            </div>
                                                            <div class="border-b border-gray-400"
                                                                style="margin: 10px -24px -5px -24px;">
                                                            </div>
                                                            <div class="mt-6 text-center">
                                                                <p>{{ number_format($opportunity->deal_size_negotation, 2) }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="grid grid-cols-1 w-1/5">
                                                            <div>
                                                                <p class="text-center">
                                                                    {{ $opportunity->opportunities_loss }}</p>
                                                            </div>
                                                            <div class="border-b border-gray-400"
                                                                style="margin: 10px -24px -5px -24px;">
                                                            </div>
                                                            <div class="mt-6 text-center">
                                                                <p>{{ number_format($opportunity->deal_size_loss, 2) }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="grid grid-cols-1 w-1/5">
                                                            <div>
                                                                <p class="text-center">
                                                                    {{ $opportunity->opportunities_won }}</p>
                                                            </div>
                                                            <div class="border-b border-gray-400"
                                                                style="margin: 10px -24px -5px -24px;">
                                                            </div>
                                                            <div class="mt-6 text-center">
                                                                <p>{{ number_format($opportunity->deal_size_won, 2) }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="grid grid-cols-1 w-1/5">
                                                            <div>
                                                                <p>{{ ($opportunity->opportunities_won / $opportunity->no_of_opportunities) * 100 }}%
                                                                </p>
                                                            </div>
                                                            <div class="border-b border-gray-400"
                                                                style="margin: 10px -16px -5px -24px;">
                                                            </div>
                                                            <div class="mt-6">
                                                                <p>{{ ($opportunity->deal_size_won / $opportunity->total_opportunity_amount) * 100 }}%
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-cols-1 gap-5 p-2">
            <div class="bg-white rounded-lg shadow-lg">
                <div class="grid grid-cols-1 gap-5 p-3">
                    <div class="grid grid-cols-2">
                        <div class="flex justify-between px-4">
                            <div class="text-blue text-2xl font-semibold">Sales Conversion Funnel</div>
                        </div>
                        <div class="flex justify-end gap-8">
                            <div class="w-56">
                                <x-crm.input type="date" wire:model.defer="" name="">
                                </x-crm.input>
                            </div>
                            <div class="grid grid-cols-2">
                                <div>
                                    <x-crm.select wire:model.defer="" name="">
                                        <option value="1">Daily</option>
                                        <option value="2">Weekly</option>
                                        <option value="3">Monthly</option>
                                    </x-crm.select>
                                </div>
                                <div class="flex justify-start ml-8">
                                    <x-crm.button type="button" wire:click="search" title="Search"
                                        class="text-white bg-blue hover:bg-blue-800" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-1 px-5 mt-2 mb-4">
                    <div class="overflow-auto bg-white rounded-lg shadow-lg">
                        <div class="bg-white rounded-lg shadow-md border border-gray-400" style="height: 24vh">
                            <x-table.table>
                                <x-slot name="thead">
                                    <tr class="text-xs text-left text-gray-500 bg-white cursor-pointer">
                                        <th class="w-2/5 border-r border-gray-400">
                                            <div class="px-4 font-normal text-gray-700">
                                                <div class="flex gap-2 flex-nowrap">
                                                    <span class="w-56">Sales Funnel</span>
                                                    <span class="w-44">Count</span>
                                                    <span class="w-28">%</span>
                                                </div>
                                            </div>
                                        </th>
                                        <th class="w-3/5">
                                            <div class="grid grid-cols-1 font-normal">
                                                <div class="text-sm text-center bg-gray-200">Customer Acquisition
                                                    Category</div>
                                                <div class="flex gap-12 px-4 text-xs">
                                                    <div class="flex w-1/5">
                                                        <p>New</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>%</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p style="">Amount</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>Existing</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>%</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p style="">Amount</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                </x-slot>
                                <x-slot name="tbody">
                                    <tr>
                                        <td class="w-12 px-2 py-2 border-r border-gray-400">
                                            <div class="flex gap-4 mt-2 flex-nowrap">
                                                <div class="flex w-56">
                                                    <p class="flex-initial ml-2">Service Request</p>
                                                </div>
                                                <div class="grid grid-cols-1 w-44">
                                                    <p class="ml-1">500</p>
                                                </div>
                                                <div class="flex w-28">
                                                    <p style="margin-left:-10px">55</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="grid grid-cols-1">
                                                <div class="flex gap-12 px-4">
                                                    <div class="flex w-1/5">
                                                        <p>200</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>68%</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p style="">12,050,000.00</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>146</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>42%</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p style="">-</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="w-12 px-2 py-2 border-r border-gray-400">
                                            <div class="flex gap-4 mt-2 flex-nowrap">
                                                <div class="flex w-56">
                                                    <p class="flex-initial ml-2">Lead</p>
                                                </div>
                                                <div class="grid grid-cols-1 w-44">
                                                    <p class="ml-1">500</p>
                                                </div>
                                                <div class="flex w-28">
                                                    <p style="margin-left:-10px">55</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="grid grid-cols-1">
                                                <div class="flex gap-12 px-4">
                                                    <div class="flex w-1/5">
                                                        <p>200</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>68%</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p style="">12,050,000.00</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>146</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>42%</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p style="">10,000,000.00</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="w-12 px-2 py-2 border-r border-gray-400">
                                            <div class="flex gap-4 mt-2 flex-nowrap">
                                                <div class="flex w-56">
                                                    <p class="flex-initial ml-2">Opportunity</p>
                                                </div>
                                                <div class="grid grid-cols-1 w-44">
                                                    <p class="ml-1">500</p>
                                                </div>
                                                <div class="flex w-28">
                                                    <p style="margin-left:-10px">55</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="grid grid-cols-1">
                                                <div class="flex gap-12 px-4">
                                                    <div class="flex w-1/5">
                                                        <p>200</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>68%</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p style="">12,050,000.00</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>146</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>32%</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p style="">10,000,000.00</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="w-12 px-2 py-2 border-r border-gray-400">
                                            <div class="flex gap-4 mt-2 flex-nowrap">
                                                <div class="flex w-56">
                                                    <p class="flex-initial ml-2">Won</p>
                                                </div>
                                                <div class="grid grid-cols-1 w-44">
                                                    <p class="ml-1">500</p>
                                                </div>
                                                <div class="flex w-28">
                                                    <p style="margin-left:-10px">55</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="grid grid-cols-1">
                                                <div class="flex gap-12 px-4">
                                                    <div class="flex w-1/5">
                                                        <p>200</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>68%</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p style="">12,050,000.00</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>146</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p>32%</p>
                                                    </div>
                                                    <div class="flex w-1/5">
                                                        <p style="">10,000,000.00</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-crm.form>

@push('scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <script type="text/javascript">
        // Quarterly Sales
        google.charts.load('current', {
            packages: ['corechart']
        }).then(function() {
            var data = google.visualization.arrayToDataTable([
                ['Month', 'Sales', 'Expenses', 'Profit'],
                ['January', 600000, 200000, 400000],
                ['February', 500000, 300000, 200000],
                ['March', 300000, 500000, -200000],
                ['April', 300000, 500000, 200000],
                ['May', 600000, 200000, 400000],
                ['June', 300000, 500000, -200000],
                ['July', 300000, 500000, 200000],
                ['August', 300000, 500000, -200000],
                ['September', 600000, 200000, 400000],
                ['October', 300000, 500000, 200000],
                ['November', 300000, 500000, -200000],
                ['December', 500000, 300000, 200000],
            ]);

            var ranges = [
                [-1000000, 0, '#FF7560']
            ];

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1, 2, 3, {
                calc: function(dt, row) {
                    var rowValue = dt.getValue(row, 3);
                    var color;
                    ranges.forEach(function(range, index) {
                        if ((rowValue >= range[0]) && ((rowValue < range[1]) || (range[
                                    1] ===
                                null))) {
                            color = range[2];
                        }
                    });
                    return color;
                },
                role: 'style',
                type: 'string'
            }]);


            var chart = new google.visualization.ColumnChart(document.getElementById('quarterly_sales_chart'));
            chart.draw(view, {
                chart: {
                    title: '',
                    subtitle: ''
                },
                colors: [
                    "#5485E5", "#D6E4FF", "#88E27D"
                ],
                legend: {
                    position: "none"
                },
                vAxes: {
                    0: {
                        title: 'Amount'
                    }
                },
            });
        });

        // Top Performing Sales 
        google.charts.load('current', {
            'packages': ['bar'],
        }).then(function() {
            var data = google.visualization.arrayToDataTable([
                ['Promotions', 'Sales', 'Expenses', 'Profit'],
                ['Bulto Panalo', 300000, 100000, 185000],
                ['January Discount', 150000, 25000, 100000],
                ['Cashback Offer', 75000, 25000, 50000],
                ['Holiday Special Offer', 50000, 35000, 45000],
                ['Tipid Padala', 25000, 15000, 20000]
            ]);


            var options = {
                chart: {
                    title: '',
                    subtitle: '',
                },
                bars: 'horizontal', // Required for Material Bar Charts.
                colors: [
                    "#5485E5", "#D6E4FF", "#88E27D"
                ],
                legend: {
                    position: "none"
                }
            };

            var chart = new google.charts.Bar(document.getElementById('top_performing_sales_chart'));

            chart.draw(data, google.charts.Bar.convertOptions(options));
        });

        // Industry
        google.charts.load('current', {
            packages: ['corechart']
        }).then(function() {
            var data = new google.visualization.arrayToDataTable([
                ['Industry', 'Value', {
                    role: 'style'
                }],
                ['Food Industry', {{ $count_ind_food_industry }}, '#82ABFF'],
                ['Agriculture', {{ $count_ind_agriculture }}, '#82ABFF'],
                ['Construction', {{ $count_ind_construction }}, '#82ABFF'],
                ['Automotive', {{ $count_ind_automotive }}, '#82ABFF'],
                ['Manufacturing', {{ $count_ind_manufacturing }}, '#82ABFF'],
                ['Financial Services', {{ $count_ind_financial_services }}, '#82ABFF'],
                ['Transport', {{ $count_ind_transport }}, '#82ABFF'],
                ['Logistics', {{ $count_ind_logistics }}, '#82ABFF'],
                ['Commerce', {{ $count_ind_commerces }}, '#82ABFF']
            ]);

            var view = new google.visualization.DataView(data);

            var options = {
                legend: {
                    position: 'none'
                },
                chart: {
                    title: '',
                    subtitle: ''
                },
                vAxes: {
                    // Adds titles to each axis.
                    0: {
                        title: 'Total no. of Leads'
                    }
                },
                bar: {
                    groupWidth: "30%",
                }
            };

            var chart = new google.visualization.ColumnChart(document.getElementById("chartBarIndustry"));
            chart.draw(view, options);
        });

        // Service Requirement
        const dataPie = {
            datasets: [{
                label: "Service Requirement",
                data: [{{ $count_domestic_freight }}, {{ $count_international_freight }}],
                backgroundColor: [
                    "#1F78B4",
                    "#A6CEE3",
                ],
                hoverOffset: 4,
            }, ],
        };
        const configPie = {
            type: "pie",
            data: dataPie,
            options: {},
        };
        var chartBar = new Chart(document.getElementById("service_requirement_pie_chart"), configPie);
    </script>
@endpush
