<div wire:init="load">
    <x-loading></x-loading>

    <form wire:submit.prevent="validateSubmit" autocomplete="off">
        <div class="grid grid-cols-1">
            <table>
                <tr>
                    <td colspan="2" class="pb-6">
                        {{-- <div class="grid grid-cols-1"> --}}
                        <x-label for="name" value="Segment Name" :required="true" />
                        <div class="flex items-center">

                            <x-input class="w-full rounded-md h-11" type="text" name="name"
                                wire:model.defer='name'>
                            </x-input>
                        </div>
                        <x-input-error for="name" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="pb-6">
                        <x-label for="filter_id" value="Filter" :required="true" />
                        <div class="flex items-center">
                            <x-select class="w-full rounded-md cursor-pointer h-11" type="text" name="filter_id"
                                wire:model='filter_id'>
                                <option value="">
                                    Select
                                </option>
                                <option value="1">
                                    Life Stage
                                </option>
                                <option value="2">
                                    Category
                                </option>
                                <option value="3">
                                    Account Type
                                </option>
                                <option value="4">
                                    Onboarding Channel
                                </option>
                                <option value="5">
                                    Marketing Channel
                                </option>
                                <option value="6">
                                    Address
                                </option>
                                <option value="7">
                                    Date Onboarded
                                </option>
                            </x-select>
                        </div>
                        <x-input-error for="filter_id" />
                    </td>
                </tr>
                @if ($filter_id == 1)
                    <?php $i = 0; ?>
                    @foreach ($this->positions as $a => $position)
                        @if (!$position['is_deleted'])
                            <tr>
                                <td colspan="2">
                                    @if ($min == $a)
                                        <x-label for="criteria_id" value="Criteria*" />
                                    @else
                                        <div class="text-white">.
                                        </div>
                                    @endif
                                    <div wire:init="lifeStageReference" class="flex items-center">
                                        <x-select class="w-full rounded-md h-11" style="cursor: pointer;"
                                            name="positions.{{ $a }}.criteria_id"
                                            wire:model.defer='positions.{{ $a }}.criteria_id'>
                                            <option value="">Select</option>
                                            @foreach ($lifeStage_references as $lifeStage_reference)
                                                <option value="{{ $lifeStage_reference->id }}">
                                                    {{ $lifeStage_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                    </div>
                                    <x-input-error for="positions.{{ $a }}.criteria_id" />
                                </td>
                                <td>
                                    <div class="col-span-1">
                                        <div class="grid grid-cols-10 mt-4 ml-3">
                                            @if ($nearest > $min)
                                                <div class="col-span-5">
                                                    <x-label for="criteria_name" value="" />
                                                    <svg wire:click="removePosition({'a': {{ $a }}})"
                                                        xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                                                        class="h-6 w-7 cursor-pointer text-red bi bi-x-circle-fill"
                                                        viewBox="0 0 16 16">
                                                        <path
                                                            d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4.5 7.5a.5.5 0 0 0 0 1h7a.5.5 0 0 0 0-1h-7z" />
                                                    </svg>
                                                </div>
                                            @endif

                                            @if ($nearest == $a)
                                                <div class="col-span-5">
                                                    <x-label for="criteria_name" value="" />
                                                    <svg wire:click="addPosition()"
                                                        class="h-6 w-7 cursor-pointer text-blue" aria-hidden="true"
                                                        focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                        role="img" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 448 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                        </path>
                                                    </svg>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    <?php $i++; ?>
                @elseif($filter_id == 2)
                    <?php $i = 0; ?>
                    @foreach ($this->positions as $a => $position)
                        @if (!$position['is_deleted'])
                            <tr>
                                <td colspan="2">
                                    @if ($min == $a)
                                        <x-label for="criteria_id" value="Criteria*" />
                                    @else
                                        <div class="text-white">.
                                        </div>
                                    @endif
                                    <div wire:init="customerStatusReference" class="flex items-center">
                                        <x-select class="w-full rounded-md h-11" style="cursor: pointer;"
                                            name="positions.{{ $a }}.criteria_id"
                                            wire:model.defer='positions.{{ $a }}.criteria_id'>
                                            <option value="">Select</option>
                                            @foreach ($customerStatus_references as $customerStatus_reference)
                                                <option value="{{ $customerStatus_reference->id }}">
                                                    {{ $customerStatus_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                    </div>
                                    <x-input-error for="positions.{{ $a }}.criteria_id" />
                                </td>
                                <td>
                                    <div class="col-span-1">
                                        <div class="grid grid-cols-10 mt-4 ml-3">
                                            @if ($nearest > $min)
                                                <div class="col-span-5">
                                                    <x-label for="criteria_name" value="" />
                                                    <svg wire:click="removePosition({'a': {{ $a }}})"
                                                        xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                                                        class="h-6 w-7 cursor-pointer text-red bi bi-x-circle-fill"
                                                        viewBox="0 0 16 16">
                                                        <path
                                                            d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4.5 7.5a.5.5 0 0 0 0 1h7a.5.5 0 0 0 0-1h-7z" />
                                                    </svg>
                                                </div>
                                            @endif

                                            @if ($nearest == $a)
                                                <div class="col-span-5">
                                                    <x-label for="criteria_name" value="" />
                                                    <svg wire:click="addPosition()"
                                                        class="h-6 w-7 cursor-pointer text-blue" aria-hidden="true"
                                                        focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                        role="img" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 448 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                        </path>
                                                    </svg>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    <?php $i++; ?>
                @elseif($filter_id == 3)
                    <?php $i = 0; ?>

                    @foreach ($this->positions as $a => $position)
                        @if (!$position['is_deleted'])
                            <tr>
                                <td colspan="2">
                                    @if ($min == $a)
                                        <x-label for="criteria_id" value="Criteria*" />
                                    @else
                                        <div class="text-white">.
                                        </div>
                                    @endif
                                    <div wire:init="accountTypeReference" class="flex items-center">
                                        <x-select class="w-full rounded-md h-11" style="cursor: pointer;"
                                            name="positions.{{ $a }}.criteria_id"
                                            wire:model.defer='positions.{{ $a }}.criteria_id'>
                                            <option value="">Select</option>
                                            @foreach ($accountType_references as $accountType_reference)
                                                <option value="{{ $accountType_reference->id }}">
                                                    {{ $accountType_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                    </div>
                                    <x-input-error for="positions.{{ $a }}.criteria_id" />
                                </td>
                                <td>
                                    <div class="col-span-1">
                                        <div class="grid grid-cols-10 mt-4 ml-3">
                                            @if ($nearest > $min)
                                                <div class="col-span-5">
                                                    <x-label for="criteria_name" value="" />
                                                    <svg wire:click="removePosition({'a': {{ $a }}})"
                                                        xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                                                        class="h-6 w-7 cursor-pointer text-red bi bi-x-circle-fill"
                                                        viewBox="0 0 16 16">
                                                        <path
                                                            d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4.5 7.5a.5.5 0 0 0 0 1h7a.5.5 0 0 0 0-1h-7z" />
                                                    </svg>
                                                </div>
                                            @endif

                                            @if ($nearest == $a)
                                                <div class="col-span-5">
                                                    <x-label for="criteria_name" value="" />
                                                    <svg wire:click="addPosition()"
                                                        class="h-6 w-7 cursor-pointer text-blue" aria-hidden="true"
                                                        focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                        role="img" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 448 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                        </path>
                                                    </svg>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    <?php $i++; ?>
                @elseif($filter_id == 4)
                    <?php $i = 0; ?>

                    @foreach ($this->positions as $a => $position)
                        @if (!$position['is_deleted'])
                            <tr>
                                <td colspan="2">
                                    @if ($min == $a)
                                        <x-label for="criteria_id" value="Criteria*" />
                                    @else
                                        <div class="text-white">.
                                        </div>
                                    @endif
                                    <div wire:init="channelSrReference" class="flex items-center">
                                        <x-select class="w-full rounded-md h-11" style="cursor: pointer;"
                                            name="positions.{{ $a }}.criteria_id"
                                            wire:model.defer='positions.{{ $a }}.criteria_id'>
                                            <option value="">Select</option>
                                            @foreach ($channelSr_references as $channelSr_reference)
                                                <option value="{{ $channelSr_reference->id }}">
                                                    {{ $channelSr_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                    </div>
                                    <x-input-error for="positions.{{ $a }}.criteria_id" />
                                </td>
                                <td>
                                    <div class="col-span-1">
                                        <div class="grid grid-cols-10 mt-4 ml-3">
                                            @if ($nearest > $min)
                                                <div class="col-span-5">
                                                    <x-label for="criteria_name" value="" />
                                                    <svg wire:click="removePosition({'a': {{ $a }}})"
                                                        xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                                                        class="h-6 w-7 cursor-pointer text-red bi bi-x-circle-fill"
                                                        viewBox="0 0 16 16">
                                                        <path
                                                            d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4.5 7.5a.5.5 0 0 0 0 1h7a.5.5 0 0 0 0-1h-7z" />
                                                    </svg>
                                                </div>
                                            @endif

                                            @if ($nearest == $a)
                                                <div class="col-span-5">
                                                    <x-label for="criteria_name" value="" />
                                                    <svg wire:click="addPosition()"
                                                        class="h-6 w-7 cursor-pointer text-blue" aria-hidden="true"
                                                        focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                        role="img" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 448 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                        </path>
                                                    </svg>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    <?php $i++; ?>
                @elseif($filter_id == 5)
                    <?php $i = 0; ?>

                    @foreach ($this->positions as $a => $position)
                        @if (!$position['is_deleted'])
                            <tr>
                                <td colspan="2">
                                    @if ($min == $a)
                                        <x-label for="criteria_id" value="Criteria*" />
                                    @else
                                        <div class="text-white">.
                                        </div>
                                    @endif
                                    <div wire:init="marketingChannelReference" class="flex items-center">
                                        <x-select class="w-full rounded-md h-11" style="cursor: pointer;"
                                            name="positions.{{ $a }}.criteria_id"
                                            wire:model.defer='positions.{{ $a }}.criteria_id'>
                                            <option value="">Select</option>
                                            @foreach ($marketingChannel_references as $marketingChannel_reference)
                                                <option value="{{ $marketingChannel_reference->id }}">
                                                    {{ $marketingChannel_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                    </div>
                                    <x-input-error for="positions.{{ $a }}.criteria_id" />
                                </td>
                                <td>
                                    <div class="col-span-1">
                                        <div class="grid grid-cols-10 mt-4 ml-3">
                                            @if ($nearest > $min)
                                                <div class="col-span-5">
                                                    <x-label for="criteria_name" value="" />
                                                    <svg wire:click="removePosition({'a': {{ $a }}})"
                                                        xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                                                        class="h-6 w-7 cursor-pointer text-red bi bi-x-circle-fill"
                                                        viewBox="0 0 16 16">
                                                        <path
                                                            d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4.5 7.5a.5.5 0 0 0 0 1h7a.5.5 0 0 0 0-1h-7z" />
                                                    </svg>
                                                </div>
                                            @endif

                                            @if ($nearest == $a)
                                                <div class="col-span-5">
                                                    <x-label for="criteria_name" value="" />
                                                    <svg wire:click="addPosition()"
                                                        class="h-6 w-7 cursor-pointer text-blue" aria-hidden="true"
                                                        focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                        role="img" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 448 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                        </path>
                                                    </svg>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    <?php $i++; ?>
                @elseif($filter_id == 6)
                    <?php $i = 0; ?>

                    @foreach ($this->positions as $a => $position)
                        @if (!$position['is_deleted'])
                            <tr>
                                <td colspan="2">
                                    @if ($min == $a)
                                        <x-label for="criteria_id" value="Criteria*" />
                                    @else
                                        <div class="text-white">.
                                        </div>
                                    @endif
                                    <div wire:init="cityReference" class="flex items-center">
                                        <x-select class="w-full rounded-md h-11" style="cursor: pointer;"
                                            name="positions.{{ $a }}.criteria_id"
                                            wire:model.defer='positions.{{ $a }}.criteria_id'>
                                            <option value="">Select</option>
                                            @foreach ($city_references as $city_reference)
                                                <option value="{{ $city_reference->id }}">
                                                    {{ $city_reference->name }}
                                                </option>
                                            @endforeach
                                        </x-select>
                                    </div>
                                    <x-input-error for="positions.{{ $a }}.criteria_id" />
                                </td>
                                <td>
                                    <div class="col-span-1">
                                        <div class="grid grid-cols-10 mt-4 ml-3">
                                            @if ($nearest > $min)
                                                <div class="col-span-5">
                                                    <x-label for="criteria_name" value="" />
                                                    <svg wire:click="removePosition({'a': {{ $a }}})"
                                                        xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                                                        class="h-6 w-7 cursor-pointer text-red bi bi-x-circle-fill"
                                                        viewBox="0 0 16 16">
                                                        <path
                                                            d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4.5 7.5a.5.5 0 0 0 0 1h7a.5.5 0 0 0 0-1h-7z" />
                                                    </svg>
                                                </div>
                                            @endif

                                            @if ($nearest == $a)
                                                <div class="col-span-5">
                                                    <x-label for="criteria_name" value="" />
                                                    <svg wire:click="addPosition()"
                                                        class="h-6 w-7 cursor-pointer text-blue" aria-hidden="true"
                                                        focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                        role="img" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 448 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z">
                                                        </path>
                                                    </svg>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    <?php $i++; ?>
                @elseif($filter_id == 7)
                    <td>
                        <x-label class="pl-1" for="onboarded_date_from" value="From" :required="true" />
                        <div class="flex items-center pr-2">
                            <x-input class="w-full rounded-md h-11" type="date" name="onboarded_date_from"
                                wire:model.defer='onboarded_date_from'>
                            </x-input>
                        </div>
                        <x-input-error for="onboarded_date_from" />
                    </td>
                    <td>
                        <x-label class="pl-2" for="onboarded_date_to" value="To" :required="true" />
                        <div class="flex items-center pl-2">
                            <x-input class="w-full rounded-md h-11" type="date" name="onboarded_date_to"
                                wire:model.defer='onboarded_date_to'>
                            </x-input>
                        </div>
                        <x-input-error class="pl-2" for="onboarded_date_to" />

                    </td>
                @endif
        </div>
        </table>
        <div class="grid grid-cols-1 pt-4">
            <div class="flex gap-4 mt-4">
                <div class="w-full col-span-11">
                    <div class="flex justify-end gap-4">
                        <button type="button" wire:click="$emit('close_modal', 'edit')"
                            class="px-10 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-blue hover:bg-gray-200">
                            Cancel
                        </button>
                        <button type="submit" {{-- wire:click="submit" --}}
                            class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>

</div>
</form>
</div>
