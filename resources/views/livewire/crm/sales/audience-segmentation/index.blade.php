<x-form x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    reactivate_modal: '{{ $reactivate_modal }}',
    deactivate_modal: '{{ $deactivate_modal }}',

}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_sales_audience_segmentation_add')
            <x-modal id="create_modal" size="w-1/3">
                <x-slot name="title">Add Target Segment</x-slot>
                <x-slot name="body">
                    @livewire('crm.sales.audience-segmentation.create')
                </x-slot>
            </x-modal>
        @endcan

        @can('crm_sales_audience_segmentation_edit')
            @if ($audience_segmentation_id && $edit_modal)
                <x-modal id="edit_modal" size="w-1/3">
                    <x-slot name="title">Edit Target Segment</x-slot>
                    <x-slot name="body">
                        @livewire('crm.sales.audience-segmentation.edit', ['id' => $audience_segmentation_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan

        {{-- @if ($audience_segmentation_id && $reactivate_modal)
            <x-modal id="reactivate_modal" size="w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-red">Deactivate</span>
                            this Segment?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('reactivate_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateStatus('{{ $audience_segmentation_id }}', 2)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#CC0000] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endif
        @if ($audience_segmentation_id && $deactivate_modal)
            <x-modal id="deactivate_modal" size="w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span
                                class="text-blue">Reactivate</span>
                            this Segment?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('deactivate_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateStatus('{{ $audience_segmentation_id }}', 1)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#003399] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endif --}}

        @can('crm_sales_audience_segmentation_deactivate')
            <x-modal id="reactivate_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center text-gray-900 ">
                            Are you sure you want to reactivate this audience segment?</h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('reactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                            <button wire:click="updateStatus('{{ $audience_segmentation_id }}', 1)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                                YES</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="deactivate_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center text-gray-900 ">
                            Are you sure you want to deactivate this audience segment?</h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('deactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                            <button wire:click="updateStatus('{{ $audience_segmentation_id }}', 2)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                                YES</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
    </x-slot>

    <x-slot name="header_title">
        <div class="grid grid-cols-12">
            <div class="col-span-1 pl-5">
                <svg wire:click="redirectTo({}, 'redirectToHome')" class="w-10 h-10 cursor-pointer text-blue"
                    aria-hidden="true" focusable="false" data-prefix="far" data-icon="edit" role="img"
                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                    <path fill="currentColor"
                        d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z">
                    </path>
                </svg>
            </div>
            <div class="col-span-11">
                <h1 class="mt-1 text-4xl font-medium text-left text-black ">Audience Segmentation Management
                </h1>
            </div>
        </div>
    </x-slot>
    <x-slot name="header_button">
        @can('crm_sales_audience_segmentation_add')
            <button wire:click="action({}, 'add')"
                class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue hover:bg-blue-900">
                <div class="flex items-start justify-between">
                    Add Target Segment
                </div>
            </button>
        @endcan

    </x-slot>
    <x-slot name="body">
        <div class="py-12">
            <div class="w-full bg-white rounded-lg shadow-md">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="No." style="padding-left:2%;" />
                        <x-table.th name="Segment Name" />
                        <x-table.th name="Segment" style="padding-left:;" />
                        <x-table.th name="Action" style="padding-left:;" />
                    </x-slot>
                    <x-slot name="tbody">

                        @foreach ($segment as $i => $segments)
                            <tr class="font-semibold border-0 cursor-pointer">
                                <td class="w-1/5 p-3 whitespace-nowrap" style="padding-left:2%;">
                                    <p class="w-24">
                                        {{ ($segment->currentPage() - 1) * $segment->links()->paginator->perPage() + $loop->iteration }}.

                                    </p>
                                </td>
                                <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:1%;">
                                    {{ $segments->name }}
                                </td>
                                @if ($segments->filter_id == 1)
                                    <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:1%;">
                                        Life Stage
                                    </td>
                                @elseif ($segments->filter_id == 2)
                                    <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:1%;">
                                        Category
                                    </td>
                                @elseif ($segments->filter_id == 3)
                                    <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:1%;">
                                        Account Type
                                    </td>
                                @elseif ($segments->filter_id == 4)
                                    <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:1%;">
                                        Onboarding Channel
                                    </td>
                                @elseif ($segments->filter_id == 5)
                                    <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:1%;">
                                        Marketing Channel
                                    </td>
                                @elseif ($segments->filter_id == 6)
                                    <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:1%;">
                                        Address
                                    </td>
                                @elseif ($segments->filter_id == 7)
                                    <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:1%;">
                                        Date Onboarded
                                    </td>
                                @endif

                                <td class="p-3 whitespace-nowrap" style="padding-left:1%;">
                                    <div class="flex space-x-3">

                                        @can('crm_sales_audience_segmentation_edit')
                                            <svg wire:click="action({'id': {{ $segments->id }}}, 'edit')"
                                                class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                data-prefix="far" data-icon="edit" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path fill="currentColor"
                                                    d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                                </path>
                                            </svg>
                                        @endcan

                                        {{-- @if ($segments->status_id == 2)
                                            <svg wire:click="action({'id': {{ $segments->id }}, 'status_id': 2}, 'update_status')"
                                                xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                fill="blue" class="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                                <path
                                                    d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                                            </svg>
                                        @elseif($segments->status_id == 1)
                                            <svg wire:click="action({'id': {{ $segments->id }}, 'status_id': 1}, 'update_status')"
                                                xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                fill="red" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                                                <path
                                                    d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z" />
                                            </svg>
                                        @elseif($segments->status_id == null)
                                            <p>&nbsp&nbsp&nbsp&nbsp&nbsp</p>
                                        @endif --}}
                                        @can('crm_sales_audience_segmentation_deactivate')
                                            @if ($segments->status_id == 1)
                                                <span title="Deactivate">
                                                    <svg x-cloak x-show="'{{ $segments->status_id == 1 }}'"
                                                        wire:click="action({'id': {{ $segments->id }}, 'status_id': 2}, 'update_status')"
                                                        class="w-8 h-5 text-blue" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="user-slash" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                                        <path fill="currentColor"
                                                            d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zM384 352c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96z">
                                                        </path>
                                                    </svg>
                                                </span>
                                            @else
                                                <span title="Reactivate">
                                                    <svg x-cloak x-show="'{{ $segments->status_id == 2 }}'"
                                                        wire:click="action({'id': {{ $segments->id }}, 'status_id': 1}, 'update_status')"
                                                        class="w-8 h-5 pl-2 text-gray-400 rotate-180" aria-hidden=" true"
                                                        focusable="false" data-prefix="fas" data-icon="user"
                                                        role="img" xmlns="http://www.w3.org/2000/svg"
                                                        viewBox="0 0 640 512">
                                                        <path fill="currentColor"
                                                            d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zm192 96a96 96 0 1 1 0 192 96 96 0 1 1 0-192z">
                                                        </path>
                                                    </svg>
                                                </span>
                                            @endif
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                    </x-slot>

                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $segment->links() }}
                </div>
            </div>

        </div>

    </x-slot>

</x-form>
