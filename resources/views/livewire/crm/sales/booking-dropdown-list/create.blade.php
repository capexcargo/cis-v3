<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-auto">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="text-xl text-center">
                Are you sure you want to submit this new activity type?
            </h2>

            <div class="flex justify-center space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="mt-5 space-y-3">
            <div class="grid grid-cols-12 gap-8">

                <div class="col-span-12">
                    <x-label for="activity_type" value="Activity Type" :required="true" />
                    <x-input type="text" name="activity_type" wire:model.defer='activity_type'></x-input>
                    <x-input-error for="activity_type" />
                </div>

                <div class="col-span-6">
                    <div wire:init="PickupReference">
                        <x-label for="pickup_execution_time_id" value="Pick Up Execution Time" :required="true" />
                        <x-select name="pickup_execution_time_id" wire:model='pickup_execution_time_id'>
                            <option value="">Select</option>
                            @foreach ($pickup_references as $pickup_reference)
                                <option value="{{ $pickup_reference->id }}">
                                    {{ $pickup_reference->time }} {{ $pickup_reference->unit_time }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="pickup_execution_time_id" />
                    </div>
                </div>
                <div class="col-span-6">
                    <div wire:init="DeliveryReference">
                        <x-label for="delivery_execution_time_id" value="Delivery Execution Time" :required="true" />
                        <x-select name="delivery_execution_time_id" wire:model='delivery_execution_time_id'>
                            <option value="">Select</option>
                            @foreach ($delivery_references as $delivery_reference)
                                <option value="{{ $delivery_reference->id }}">
                                    {{ $delivery_reference->time }} {{ $delivery_reference->unit_time }}
                                </option>
                            @endforeach
                        </x-select>
                        <x-input-error for="delivery_execution_time_id" />
                    </div>
                </div>
            </div>

        </div>
        <div class="flex justify-end gap-3 mt-6 space-x-3">
            <x-button type="button" wire:click="closecreatemodal" title="Cancel"
            class="bg-white px-12 text-blue hover:bg-gray-100" />
            <x-button type="button" wire:click="action({},'submit2')" title="Submit" 
            class="px-12 bg-blue text-white hover:bg-[#002161]" />
        </div>
    </form>
</div>
