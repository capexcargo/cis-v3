<x-form wire:init="load" x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    reactivate_modal: '{{ $reactivate_modal }}',
    deactivate_modal: '{{ $deactivate_modal }}',
    {{-- delete_modal: '{{ $delete_modal }}', --}}

}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_sales_opportunities_sales_stage_add')
            <x-modal id="create_modal" size="w-2/5">
                <x-slot name="title">Add Sales Stage</x-slot>
                <x-slot name="body">
                    @livewire('crm.sales.opportunities.sales-stage.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('crm_sales_opportunities_sales_stage_edit')
            @if ($sales_id && $edit_modal)
                <x-modal id="edit_modal" size="w-2/5">
                    <x-slot name="title">Edit Sales Stage</x-slot>
                    <x-slot name="body">
                        @livewire('crm.sales.opportunities.sales-stage.edit', ['id' => $sales_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        {{-- @can('crm_sales_opportunities_sales_stage_delete')
            <x-modal id="delete_modal" size="w-auto">
                <x-slot name="body">
                    <h2 class="text-xl text-center">
                        Are you sure you want to deactivate this Sales Stage?
                    </h2>
                    <div class="flex justify-center space-x-3">
                        <button type="button" wire:click="$set('delete_modal', false)"
                            class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                            No
                        </button>
                        <button type="button" wire:click="confirm"
                            class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                            Yes</button>
                    </div>
                </x-slot>
            </x-modal>
        @endcan --}}
        @can('crm_sales_opportunities_opportunity_status_deactivate')
            <x-modal id="reactivate_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            Are you sure you want to reactivate this Sales Stage?
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('reactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                            <button wire:click="updateStatus('{{ $sales_id }}', 1)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">YES</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="deactivate_modal" size="w-auto">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center">
                        <h2 class="text-xl text-center">
                            Are you sure you want to deactivate this Sales Stage?
                        </h2>
                        <div class="flex justify-center space-x-3">
                            <button wire:click="$set('deactivate_modal', false)"
                                class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                            <button wire:click="updateStatus('{{ $sales_id }}', 2)"
                                class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">YES</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
    </x-slot>

    <x-slot name="header_title">Sales Stage Management</x-slot>
    <x-slot name="header_button">
        @can('crm_sales_opportunities_sales_stage_add')
            <button wire:click="action({}, 'add')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Add Sales Stage
                </div>
            </button>
        @endcan

    </x-slot>
    <x-slot name="body">

        <div class="py-12">

            <div class="bg-white rounded-lg shadow-md">
                <x-table.table class="overflow-hidden">
                    <x-slot name="thead">
                        <x-table.th name="No." style="padding-left:2%;" />
                        <x-table.th name="Sales Stage" style="padding-left:;" />
                        <x-table.th name="Opportunity Status" style="padding-left:;" />
                        <x-table.th name="Action" style="padding-left:;" />
                    </x-slot>
                    <x-slot name="tbody">
                        @foreach ($sales_s as $i => $sales)
                            <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-semibold">
                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:2%;">
                                    <p class="w-1/6">
                                        {{ ($sales_s->currentPage() - 1) * $sales_s->links()->paginator->perPage() + $loop->iteration }}.
                                    </p>
                                </td>

                                <td class="w-1/3 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $sales->sales_stage }}
                                </td>

                                <td class="w-2/5 p-3 whitespace-nowrap" style="padding-left:;">
                                    {{ $sales->OpportunityStatus->name }}
                                </td>

                                <td class="w-1/6 p-3 whitespace-nowrap" style="padding-left:;">
                                    <div class="flex space-x-2">
                                        @can('crm_sales_opportunities_sales_stage_edit')
                                            <span title="Edit">
                                                <svg wire:click="action({'id': {{ $sales->id }}}, 'edit')"
                                                    class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="far" data-icon="edit" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                                    </path>
                                                </svg>
                                            </span>
                                        @endcan

                                        {{-- @can('crm_sales_opportunities_sales_stage_delete')
                                        <svg wire:click="action({'id': {{ $sales->id }}}, 'delete')" class="w-5 h-5 text-red"
                                            aria-hidden="true" focusable="false" data-prefix="fas"
                                            data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 448 512">
                                            <path fill="currentColor"
                                                d="M160 400C160 408.8 152.8 416 144 416C135.2 416 128 408.8 128 400V192C128 183.2 135.2 176 144 176C152.8 176 160 183.2 160 192V400zM240 400C240 408.8 232.8 416 224 416C215.2 416 208 408.8 208 400V192C208 183.2 215.2 176 224 176C232.8 176 240 183.2 240 192V400zM320 400C320 408.8 312.8 416 304 416C295.2 416 288 408.8 288 400V192C288 183.2 295.2 176 304 176C312.8 176 320 183.2 320 192V400zM317.5 24.94L354.2 80H424C437.3 80 448 90.75 448 104C448 117.3 437.3 128 424 128H416V432C416 476.2 380.2 512 336 512H112C67.82 512 32 476.2 32 432V128H24C10.75 128 0 117.3 0 104C0 90.75 10.75 80 24 80H93.82L130.5 24.94C140.9 9.357 158.4 0 177.1 0H270.9C289.6 0 307.1 9.358 317.5 24.94H317.5zM151.5 80H296.5L277.5 51.56C276 49.34 273.5 48 270.9 48H177.1C174.5 48 171.1 49.34 170.5 51.56L151.5 80zM80 432C80 449.7 94.33 464 112 464H336C353.7 464 368 449.7 368 432V128H80V432z">
                                            </path>
                                        </svg>
                                        @endcan --}}
                                        @can('crm_sales_opportunities_opportunity_status_deactivate')
                                            @if ($sales->status == 1)
                                                <span title="Deactivate">
                                                    <svg x-cloak x-show="'{{ $sales->status == 1 }}'"
                                                        wire:click="action({'id': {{ $sales->id }}, 'status': 2}, 'update_status')"
                                                        class="w-8 h-5 text-red" aria-hidden=" true" focusable="false"
                                                        data-prefix="fas" data-icon="user" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM175 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z">

                                                        </path>
                                                    </svg>
                                                </span>
                                            @else
                                                <span title="Reactivate">
                                                    <svg x-cloak x-show="'{{ $sales->status == 2 }}'"
                                                        wire:click="action({'id': {{ $sales->id }}, 'status': 1}, 'update_status')"
                                                        class="w-8 h-5 pl-2 text-blue" aria-hidden="true" focusable="false"
                                                        data-prefix="fas" data-icon="user-slash" role="img"
                                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                                        <path fill="currentColor"
                                                            d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">

                                                        </path>
                                                    </svg>
                                                </span>
                                            @endif
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $sales_s->links() }}
                </div>
            </div>







        </div>


    </x-slot>
</x-form>
