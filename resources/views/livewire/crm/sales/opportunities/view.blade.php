<div>
    <div class="whitespace-nowrap"> Booking Reference No. : <span class="text-xl text-[#003399] font-semibold">CPX00037181</span></div>
    <div class="mt-2 bg-white rounded-lg shadow-md">
        <x-table.table>
            <x-slot name="thead">
                <x-table.th name="No." />
                <x-table.th name="Waybill Number" />
            </x-slot>
            <x-slot name="tbody">
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        1.
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        <span class="underline text-[#003399]">
                            MNL0000124
                        </span>
                    </td>
                </tr>
                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                    <td class="p-3 whitespace-nowrap">
                        2.
                    </td>
                    <td class="p-3 whitespace-nowrap">
                        <span class="underline text-[#003399]">
                            MNL0000125
                        </span>
                    </td>
                </tr>
            </x-slot>
        </x-table.table>
    </div>
</div>
