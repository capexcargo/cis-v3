<x-form x-data="{
    view_modal: '{{ $view_modal }}'
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_sales_opportunities_view')
            @if ($opportunity_id && $view_modal)
                <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="body">
                        @livewire('crm.sales.opportunities.view')
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>

    <x-slot name="header_title">Opportunities</x-slot>
    <x-slot name="header_button">
        <div x-data="{ open: false }">
            <button class="flex gap-3 p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue" @click="open = !open">
                <span>Management</span>
                <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                    <path fill="currentColor"
                        d="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z" />
                </svg>
            </button>
            <div class="absolute border-2 border-gray-300 rounded-sm " style="margin-top: .4%; margin-left: -2.6rem"
                x-transition:leave="transition ease-in duration-150" x-transition:leave-start="opacity-100"
                x-transition:leave-end="opacity-0" x-show="open" @click.away="open = false"
                @keydown.escape="open = false">

                <ul class="text-gray-600 bg-white rounded shadow">
                    <li class="px-4 py-1 cursor-pointer whitespace-nowrap" x-data="{ open: false }"
                        wire:click="action({},'sales_stage')">
                        Sales Stage
                    </li>
                    <li class="px-4 py-1 cursor-pointer" x-data="{ open: false }"
                        wire:click="action({},'opportunity_status')">
                        Opportunity Status
                    </li>
                </ul>
            </div>
        </div>
    </x-slot>
    <x-slot name="body">
        <div class="flex gap-6">
            <button wire:click="opportunitiesStatus(0)"
                class="flex justify-between gap-24 px-4 py-4 bg-white border-[#003399] rounded-lg hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700 {{ $opportunity_status == 0 ? 'border-2' : 'border' }}">
                <h5 class="font-medium text-left text-gray-700 uppercase dark:text-gray-40">ALL OPPORTUNITIES</h5>
                <h5 class="text-3xl font-medium text-gray-700 dark:text-gray-400">{{ $all_opportunities }}</h5>
            </button>
            <button wire:click="opportunitiesStatus(1)"
                class="flex justify-between gap-24 px-4 py-4 bg-white border-[#003399] rounded-lg hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700 {{ $opportunity_status == 1 ? 'border-2' : 'border' }}">
                <h5 class="font-medium text-left text-gray-700 uppercase dark:text-gray-400">MY OPEN<br>OPPORTUNITIES
                </h5>
                <h5 class="text-3xl font-medium text-gray-700 dark:text-gray-400">{{ $my_open_opportunities }}</h5>
            </button>
            <button wire:click="opportunitiesStatus(2)"
                class="flex justify-between gap-24 px-4 py-4 bg-white border-[#003399] rounded-lg hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700 {{ $opportunity_status == 2 ? 'border-2' : 'border' }}">
                <h5 class="font-medium text-left text-gray-700 uppercase dark:text-gray-400">MY TEAM'S<br>OPPORTUNITIES
                </h5>
                <h5 class="text-3xl font-medium text-gray-700 dark:text-gray-400">{{ $my_teams_opportunities }}</h5>
            </button>
        </div>
        <div class="flex gap-12" style="margin-top: 3rem">
            <div class="w-48">
                <x-transparent.input type="text" value="" label="Opportunity Name"
                    name="opportunity_name_search" wire:model.defer="opportunity_name_search">
                </x-transparent.input>
            </div>
            <div class="w-48">
                <x-transparent.input type="text" value="" label="Sr Number" name="sr_no_search"
                    wire:model.defer="sr_no_search">
                </x-transparent.input>
            </div>
            <div class="w-48">
                <x-transparent.input type="date" label="Date Range" name="date_created_search"
                    wire:model.defer="date_created_search">
                </x-transparent.input>
            </div>
            <div class="">
                <x-button type="button" wire:click="search" title="Search"
                    class="text-white bg-blue hover:bg-blue-800" />
            </div>
        </div>
        <div class="grid grid-cols-1 gap-4 mt-4">
            <div class="overflow-auto rounded-lg">
                <div class="grid grid-cols-1 gap-4 mt-4">
                    <div class="grid grid-cols-10 gap-6">
                        <div class="col-span-9">
                            <ul class="flex mt-2">
                                @foreach ($sales_stage_header_cards as $i => $sales_stage_card)
                                    <li wire:click="ActiveType('{{ $sales_stage_header_cards[$i]['id'] }}')"
                                        class="px-3 py-2 text-xs cursor-pointer {{ $sales_stage_header_cards[$i]['class'] }} {{ $sales_stage == $sales_stage_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                                        <button>
                                            {{ $sales_stage_header_cards[$i]['title'] }}
                                        </button>
                                        <div class="text-md">{{ $sales_stage_header_cards[$i]['value'] }}</div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="-mt-4 bg-white rounded-lg shadow-md">

                        <x-table.table>
                            <x-slot name="thead">
                                <x-table.th name="No." />
                                <x-table.th name="Win %" />
                                <x-table.th name="Opportunity Name" />
                                <x-table.th name="SR Number" />
                                <x-table.th name="Industry" />
                                <x-table.th name="Customer Category" />
                                <th class='p-3 tracking-wider text-left border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap'
                                    scope="col">
                                    <div class="font-medium w-28">
                                        Deal Size
                                    </div>
                                </th>
                                <th class='p-3 tracking-wider text-left border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap'
                                    scope="col">
                                    <div class="font-medium w-28">
                                        Sales Stage
                                    </div>
                                </th>
                                <th class='p-3 tracking-wider text-left whitespace-normal border-2 border-t-0 border-b-0 border-gray-500'
                                    scope="col">
                                    <div class="font-medium w-28">
                                        Actual Amount Converted
                                    </div>
                                </th>
                                <x-table.th name="Waybill Count" />
                                <x-table.th name="Lead Conversion Date" />
                                <x-table.th name="Close Date" />
                                <x-table.th name="Contact Owner" />
                                <x-table.th name="Ageing Days" />
                            </x-slot>
                            <x-slot name="tbody">
                                @foreach ($opportunities as $i => $opportunity)
                                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="p-3 whitespace-nowrap">
                                            {{ ($opportunities->currentPage() - 1) * $opportunities->links()->paginator->perPage() + $loop->iteration }}.
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <div
                                                class="flex gap-2 justify-center rounded-full w-24 px-4 py-1 
                                            {{ $opportunity->opportunity_status_id == 1
                                                ? 'text-green-400'
                                                : ($opportunity->opportunity_status_id == 2
                                                    ? 'text-[#FF8800]'
                                                    : 'text-[#E50000]') }}">
                                                {{ $opportunity->opportunity_status_id }}%
                                            </div>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <p class="text-[#003399] underline"
                                                wire:click="action({'id': {{ $opportunity->id }}}, 'edit')">
                                                {{ $opportunity->opportunity_name }}</p>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            <p class="text-[#003399] underline">{{ $opportunity->sr_no }}</p>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $opportunity->industry->name ?? null }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            New
                                        </td>
                                        <td
                                            class="p-3 border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                            Php {{ number_format($opportunity->deal_size ?? null) }}
                                        </td>
                                        <td
                                            class="p-3 border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                            {{ $opportunity->saleStage->sales_stage ?? null }}
                                        </td>
                                        <td
                                            class="p-3 border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                            Php {{ number_format($opportunity->actual_amount ?? null) }}
                                        </td>
                                        <td class="p-3 text-center whitespace-nowrap">
                                            <p class="text-[#003399] underline"
                                                wire:click="action({'id': {{ $opportunity->id }}}, 'view')">2</p>
                                            </p>
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ date('M. d, Y', strtotime($opportunity->lead_conversion_date)) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $opportunity->close_date == '' ? '' : date('M. d, Y', strtotime($opportunity->close_date)) }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                            {{ $opportunity->createdBy->name }}
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                        <td class="p-3 whitespace-nowrap">
                                        </td>
                                    </tr>
                                @endforeach
                            </x-slot>
                        </x-table.table>
                        <div class="px-1 pb-2">
                            {{ $opportunities->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
