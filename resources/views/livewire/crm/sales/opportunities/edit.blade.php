<x-form wire:init="" x-data="{
    {{-- search_form: false, --}}
    view_modal: '{{ $view_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_sales_opportunities_view')
            @if ($opportunity_id && $view_modal)
                <x-modal id="view_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
                    <x-slot name="body">
                        @livewire('crm.sales.opportunities.view')
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>

    <x-slot name="header_title">
        <span>
            <div class="flex items-start justify-between" wire:click="action({}, 'back')">
                <svg class="w-8 h-8 mt-1 mr-4 text-blue-800 cursor-pointer" aria-hidden="true" focusable="false"
                    data-prefix="far" data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 448 512">
                    <path fill="currentColor"
                        d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" />
                </svg>
                <span class="mt-1 ml-4 text-gray-400">Edit Opportunity : <span
                        class="ml-2 text-black">{{ $opportunity_name }}</span></span>
            </div>
        </span>
    </x-slot>
    <x-slot name="header_button">
    </x-slot>
    <x-slot name="body">
        <div class="space-y-6">
            <div class="grid grid-cols-12 gap-12">
                <div class="col-span-5 px-8 py-6 space-y-4 bg-white rounded-lg shadow-md">
                    <div class="grid grid-cols-2 gap-12">
                        <div>
                            <x-label for="opportunity_name" value="Opportunity Name" :required="true" />
                            <input class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="opportunity_name" wire:model='opportunity_name'>
                            <x-input-error for="opportunity_name" />
                        </div>
                        <div>
                            <x-label for="sr_number" value="SR Number" />
                            <input class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="sr_number" wire:model='sr_number'>
                            <x-input-error for="sr_number" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-12">
                        <div>
                            <x-label for="" value="Company Industry" :required="true" />
                            <x-select class="w-full rounded-md cursor-pointer h-11" name="company_industry"
                                wire:model='company_industry'>
                                <option value="">Select</option>
                                @foreach ($company_industry_references as $company_industry_ref)
                                    <option value="{{ $company_industry_ref->id }}">
                                        {{ $company_industry_ref->name }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="company_industry" />
                        </div>
                        <div></div>
                    </div>
                    <div class="grid grid-cols-2 gap-12">
                        <div>
                            <x-label for="" value="Sales Stage" :required="true" />
                            <x-select class="w-full rounded-md cursor-pointer h-11" name="sales_stage"
                                wire:model='sales_stage'>
                                <option value="">Select</option>
                                @foreach ($sales_stage_references as $sales_stage_ref)
                                    <option value="{{ $sales_stage_ref->id }}">
                                        {{ $sales_stage_ref->sales_stage }}
                                    </option>
                                @endforeach
                            </x-select>
                            <x-input-error for="sales_stage" />
                        </div>
                        <div>
                            <x-label for="opportunity_status" value="Opportunity Status" />
                            <input class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="opportunity_status" wire:model='opportunity_status' disabled>
                            <x-input-error for="opportunity_status" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-12">
                        <div>
                            <x-label for="" value="Deal Size" :required="true" />
                            <input class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="deal_size" wire:model='deal_size'>
                            <x-input-error for="deal_size" />
                        </div>
                        <div>
                            <x-label for="actual_amount_converted" value="Actual Amount Converted" />
                            <input class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="actual_amount_converted" wire:model='actual_amount_converted' disabled>
                            <x-input-error for="actual_amount_converted" />
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-12">
                        <div>
                            <x-label for="booking_reference" value="Booking Reference" />
                            <input class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="booking_reference" wire:model='booking_reference'>
                            <x-input-error for="booking_reference" />
                        </div>
                        <div>
                            <div class="flex gap-4 mt-8">Waybill Count:
                                <span class="text-[#003399] underline cursor-pointer"
                                    wire:click="action({'id': {{ $opportunity->id }}}, 'view')">2</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-12">
                        <div>
                            <x-label for="lead_conversion_date" value="Lead Conversion Date" :required="true" />
                            <input type="date"
                                class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="lead_conversion_date" wire:model='lead_conversion_date'>
                            <x-input-error for="lead_conversion_date" />
                        </div>
                        <div>
                            <x-label for="close_date" value="Close Date" :required="true" />
                            <input type="date"
                                class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="close_date" wire:model='close_date'>
                            <x-input-error for="close_date" />
                        </div>
                    </div>
                    <div class="pr-8">
                        {{-- <x-label for="" value="Attachment" /> --}}
                        <div class="text-2xl font-bold text-blue">
                            <div class="flex items-center -mb-1 space-x-3">
                                <x-label for="attachment" value="Attachment" />
                            </div>
                            <x-table.table>
                                <x-slot name="thead">
                                </x-slot>
                                <x-slot name="tbody">
                                    @forelse ($attachments as $a => $attachment)
                                        @if (!$attachment['is_deleted'])
                                            <tr
                                                class="text-sm border-0 cursor-pointer even:bg-white hover:text-white hover:bg-blue-200">
                                                <td class="flex p-2 items-left justify-left whitespace-nowrap">
                                                    <div class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                        <div class="relative z-0 ">
                                                            <div class="absolute top-0 left-0">
                                                                @if ($attachment['id'] && !$attachments[$a]['attachment'])
                                                                    @if (in_array($attachment['extension'], config('filesystems.image_type')))
                                                                        <div
                                                                            class="flex-shrink-0 mb-1 mr-1 whitespace-nowrap">
                                                                            <a href="{{ Storage::disk('crm_gcs')->url($attachment['path'] . $attachment['name']) }}"
                                                                                target="_blank"><img
                                                                                    class="w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                                    src="{{ Storage::disk('crm_gcs')->url($attachment['path'] . $attachment['name']) }}"></a>
                                                                        </div>
                                                                    @endif
                                                                @else
                                                                    @if (!$attachments[$a]['attachment'])
                                                                        <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                            src="{{ $attachments[$a]['attachment'] ? $attachments[$a]['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                                    @elseif (in_array($attachments[$a]['attachment']->extension(), config('filesystems.image_type')))
                                                                        <img class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg "
                                                                            src="{{ $attachments[$a]['attachment'] ? $attachments[$a]['attachment']->temporaryUrl() : asset('images/form/add-image.png') }}">
                                                                    @else
                                                                        <svg class="object-contain w-20 h-20 mx-auto border border-gray-500 rounded-lg"
                                                                            aria-hidden="true" focusable="false"
                                                                            data-prefix="fas" data-icon="file-alt"
                                                                            role="img"
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            viewBox="0 0 384 512">
                                                                            <path fill="currentColor"
                                                                                d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z">
                                                                            </path>
                                                                        </svg>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                            <input type="file"
                                                                name="attachments.{{ $a }}.attachment"
                                                                wire:model="attachments.{{ $a }}.attachment"
                                                                class="relative z-50 block w-20 h-20 opacity-0 cursor-pointer">
                                                            <x-input-error
                                                                for="attachments.{{ $a }}.attachment" />
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="p-2 whitespace-nowrap">
                                                    <div class="flex items-center justify-center space-x-3">
                                                        @if (in_array($attachment['extension'], config('filesystems.image_type')))
                                                            <a href="{{ Storage::disk('crm_gcs')->url($attachment['path'] . $attachment['name']) }}"
                                                                target="_blank">
                                                                <p class="text-blue-600 underline underline-offset-2">
                                                                    View
                                                                </p>
                                                                <svg hidden class="w-5 h-5 text-blue"
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    viewBox="0 0 576 512" fill="currentColor">
                                                                    <path
                                                                        d="M279.6 160.4C282.4 160.1 285.2 160 288 160C341 160 384 202.1 384 256C384 309 341 352 288 352C234.1 352 192 309 192 256C192 253.2 192.1 250.4 192.4 247.6C201.7 252.1 212.5 256 224 256C259.3 256 288 227.3 288 192C288 180.5 284.1 169.7 279.6 160.4zM480.6 112.6C527.4 156 558.7 207.1 573.5 243.7C576.8 251.6 576.8 260.4 573.5 268.3C558.7 304 527.4 355.1 480.6 399.4C433.5 443.2 368.8 480 288 480C207.2 480 142.5 443.2 95.42 399.4C48.62 355.1 17.34 304 2.461 268.3C-.8205 260.4-.8205 251.6 2.461 243.7C17.34 207.1 48.62 156 95.42 112.6C142.5 68.84 207.2 32 288 32C368.8 32 433.5 68.84 480.6 112.6V112.6zM288 112C208.5 112 144 176.5 144 256C144 335.5 208.5 400 288 400C367.5 400 432 335.5 432 256C432 176.5 367.5 112 288 112z" />
                                                                </svg>
                                                            </a>
                                                        @elseif(in_array($attachment['extension'], config('filesystems.file_type')))
                                                            <svg wire:click="download({{ $attachment['id'] }})"
                                                                class="w-5 h-5 text-blue"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 512 512" fill="currentColor">
                                                                <path
                                                                    d="M480 352h-133.5l-45.25 45.25C289.2 409.3 273.1 416 256 416s-33.16-6.656-45.25-18.75L165.5 352H32c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h448c17.67 0 32-14.33 32-32v-96C512 366.3 497.7 352 480 352zM432 456c-13.2 0-24-10.8-24-24c0-13.2 10.8-24 24-24s24 10.8 24 24C456 445.2 445.2 456 432 456zM233.4 374.6C239.6 380.9 247.8 384 256 384s16.38-3.125 22.62-9.375l128-128c12.49-12.5 12.49-32.75 0-45.25c-12.5-12.5-32.76-12.5-45.25 0L288 274.8V32c0-17.67-14.33-32-32-32C238.3 0 224 14.33 224 32v242.8L150.6 201.4c-12.49-12.5-32.75-12.5-45.25 0c-12.49 12.5-12.49 32.75 0 45.25L233.4 374.6z" />
                                                            </svg>
                                                        @endif
                                                        @if (count(collect($attachments)->where('is_deleted', false)) > 1)
                                                            <a wire:click="removeAttachments({{ $a }})">
                                                                <p class="text-red-600 underline underline-offset-2">
                                                                    Remove
                                                                </p>
                                                            </a>
                                                            <svg hidden
                                                                wire:click="removeAttachments({{ $a }})"
                                                                class="w-5 h-5 text-red" aria-hidden="true"
                                                                focusable="false" data-prefix="fas"
                                                                data-icon="times-circle" role="img"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 512 512">
                                                                <path fill="currentColor"
                                                                    d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z">
                                                                </path>
                                                            </svg>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    @empty
                                        <tr>
                                            <td colspan="3">
                                                <p class="text-center">Empty.</p>
                                            </td>
                                        </tr>
                                    @endforelse
                                </x-slot>
                            </x-table.table>
                            <div class="flex items-center justify-start">
                                <button type="button" title="Add Attachment" wire:click="addAttachments"
                                    class="px-3 py-1 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                    +</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-span-7 px-8 py-6 space-y-4 bg-white rounded-lg shadow-md">
                    <div class="grid grid-cols-1 pr-8 space-y-4">
                        <div class="pr-8">
                            <x-label for="contact_person" value="Contact Person" />
                            <input class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="contact_person" wire:model='contact_person'>
                            <x-input-error for="contact_person" />
                        </div>
                        <div class="pr-8">
                            <x-label for="contact_mobile_no" value="Contact Mobile Number" :required="true" />
                            <input class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="contact_mobile_no" wire:model='contact_mobile_no'>
                            <x-input-error for="contact_mobile_no" />
                        </div>
                        <div class="pr-8">
                            <x-label for="contact_email_add" value="Contact Email Address" />
                            <input class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="contact_email_add" wire:model='contact_email_add'>
                            <x-input-error for="contact_email_add" />
                        </div>
                        <div class="pr-8">
                            <x-label for="customer_type" value="Customer Type" />
                            <input class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="customer_type" wire:model='customer_type' disabled>
                            <x-input-error for="customer_type" />
                        </div>
                        <div class="pr-8">
                            <x-label for="contact_owner" value="Contact Owner" />
                            <input class="w-full px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                name="contact_owner" wire:model='contact_owner' disabled>
                            <x-input-error for="contact_owner" />
                        </div>
                    </div>
                    <div class="grid grid-cols-1 pr-8" style="margin-top:10rem">
                        <div class="flex justify-end gap-6 pr-8">
                            <button type="button" wire:click="action({},'back')"
                                class="px-10 py-2 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-red-400">
                                Cancel
                            </button>
                            <button type="button" wire:click="action({},'save')"
                                class="px-10 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
