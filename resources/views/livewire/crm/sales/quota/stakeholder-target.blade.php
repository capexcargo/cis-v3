<div wire:init="load">
    <x-loading></x-loading>
    <div>
        <div class="px-4 mt-8 space-y-3">
            <div class="grid grid-cols-1 space-y-3">
                <div class="flex gap-8">
                    <div class="w-32 mt-2">
                        <x-label for="" value="For the Month of :" />
                    </div>
                    <div class="grid grid-cols-2 gap-4">
                        <div>
                            <x-select name="month" wire:model='month'>
                                <option value="">Month</option>
                                @foreach ($months_references as $i => $months_ref)
                                    <option value="{{ $months_ref->id }}">{{ $months_ref->display }}</option>
                                @endforeach
                            </x-select>
                        </div>
                        <div class="w-24">
                            <x-select name="year" wire:model='year'>
                                <option value="">Year</option>
                                <option value="2023">2023</option>
                                <option value="2022">2022</option>
                                <option value="2021">2021</option>
                                <option value="2020">2020</option>
                                <option value="2019">2019</option>
                            </x-select>
                        </div>
                    </div>
                </div>
                <div class="flex gap-8">
                    <div class="w-32 mt-2">
                        <x-label for="" value="Stakeholder :" />
                    </div>
                    <div class="w-52">
                        <x-select name="stakeholder" wire:model='stakeholder'>
                            <option value="">Select</option>
                            <option value="1">CSM</option>
                            <option value="2">NL / SL</option>
                            <option value="3">Metro Manila - Field</option>
                            <option value="4">Metro Manila - Walk In</option>
                            <option value="5">Provincial Branches</option>
                            <option value="6">Online Channel</option>
                        </x-select>
                    </div>
                </div>
                <div class="mt-4">
                    @if ($stakeholder == 1)
                        {{-- @dd($stakeholders) --}}

                        <div class="grid grid-cols-1">
                            <div class="overflow-auto rounded-lg">
                                <div class="grid grid-cols-1 gap-4 mt-4">
                                    <div class="-mt-4 bg-white border rounded-lg shadow-lg">
                                        <x-table.table>
                                            <x-slot name="thead">
                                                <th class='p-3 text-left'>
                                                    CSM
                                                </th>
                                                <th class='p-3 text-left'>
                                                    Corporate Volume Target
                                                </th>
                                                <th class='p-3 text-left'>
                                                    Distribution Percentage
                                                </th>
                                                <th class='p-3 text-left'>
                                                    Quota (Volume)
                                                </th>
                                                <th class='p-3 text-left'>
                                                    Quota (Amount)
                                                </th>
                                            </x-slot>
                                            <x-slot name="tbody">
                                                @foreach ($stakeholders as $i => $sh)
                                                    <tr class="cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                                        <td class="p-3 text-left whitespace-nowrap">
                                                            {{ $sh->accountTypeBT->name }}
                                                        </td>
                                                        <td class="p-3 text-center whitespace-nowrap">
                                                            300.25
                                                        </td>
                                                        <td class="p-3 whitespace-nowrap">
                                                            <div class="flex justify-center w-24">
                                                                <x-input type="text"
                                                                    name="subcategories.{{ $i }}.distribution_pct"
                                                                    wire:model.defer='subcategories.{{ $i }}.distribution_pct'>
                                                                </x-input>
                                                            </div>
                                                            <x-input-error
                                                                for="subcategories.{{ $i }}.distribution_pct" />
                                                        </td>
                                                        <td class="p-3 text-center whitespace-nowrap">
                                                            125.13
                                                        </td>
                                                        <td class="p-3 whitespace-nowrap">
                                                            <div class="flex justify-center w-24">
                                                                <x-input type="text"
                                                                    name="subcategories.{{ $i }}.amount"
                                                                    wire:model.defer='subcategories.{{ $i }}.amount'>
                                                                </x-input>
                                                            </div>
                                                            <x-input-error
                                                                for="subcategories.{{ $i }}.amount" />
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </x-slot>
                                        </x-table.table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @elseif ($stakeholder == 3)
                        <div class="grid grid-cols-1">
                            <div class="overflow-auto rounded-lg">
                                <div class="grid grid-cols-1 gap-4 mt-4">
                                    <div class="-mt-4 bg-white border rounded-lg shadow-lg">
                                        <x-table.table>
                                            <x-slot name="thead">
                                                <th class='p-3 text-left'>
                                                    No.
                                                </th>
                                                <th class='p-3 text-left'>
                                                    Metro Manila - Field
                                                </th>
                                                <th class='p-3 text-left'>
                                                    Account Type
                                                </th>
                                                <th class='p-3 text-left'>
                                                    Distribution Percentage
                                                </th>
                                                <th class='p-3 text-left'>
                                                    Quota (Volume)
                                                </th>
                                                <th class='p-3 text-left'>
                                                    Quota (Amount)
                                                </th>
                                            </x-slot>
                                            <x-slot name="tbody">
                                                @foreach ($stakeholders as $i => $sh)
                                                    <tr class="cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                                        <td class="p-3 text-left whitespace-nowrap">
                                                            {{ $sh->quadrantBT->id }}.
                                                        </td>
                                                        <td class="p-3 text-left whitespace-nowrap">
                                                            {{ $sh->quadrantBT->name }}
                                                        </td>
                                                        <td class="p-3 text-center whitespace-nowrap">
                                                            {{ $sh->accountTypeReference->name }}
                                                        </td>
                                                        <td class="p-3 whitespace-nowrap">
                                                            <div class="flex justify-center w-24">
                                                                <x-input type="text"
                                                                    name="subcategories.{{ $i }}.distribution_pct"
                                                                    wire:model.defer='subcategories.{{ $i }}.distribution_pct'>
                                                                </x-input>
                                                            </div>
                                                            <x-input-error
                                                                for="subcategories.{{ $i }}.distribution_pct" />
                                                        </td>
                                                        <td class="p-3 text-center whitespace-nowrap">
                                                            125.13
                                                        </td>
                                                        <td class="p-3 whitespace-nowrap">
                                                            <div class="flex justify-center w-24">
                                                                <x-input type="text"
                                                                    name="subcategories.{{ $i }}.amount"
                                                                    wire:model.defer='subcategories.{{ $i }}.amount'>
                                                                </x-input>
                                                            </div>
                                                            <x-input-error
                                                                for="subcategories.{{ $i }}.amount" />
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </x-slot>
                                        </x-table.table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @elseif ($stakeholder == 5)
                        <div class="grid grid-cols-1">
                            <div class="overflow-auto rounded-lg">
                                <div class="grid grid-cols-1 gap-4 mt-4">
                                    <div class="-mt-4 bg-white border rounded-lg shadow-lg">
                                        <x-table.table>
                                            <x-slot name="thead">
                                                <th class='p-3 text-left'>
                                                    No.
                                                </th>
                                                <th class='p-3 text-left'>
                                                    Provincial Branches
                                                </th>
                                                <th class='p-3 text-left'>
                                                    Account Type
                                                </th>
                                                <th class='p-3 text-left'>
                                                    Distribution Percentage
                                                </th>
                                                <th class='p-3 text-left'>
                                                    Quota (Volume)
                                                </th>
                                                <th class='p-3 text-left'>
                                                    Quota (Amount)
                                                </th>
                                            </x-slot>
                                            <x-slot name="tbody">
                                                @foreach ($stakeholders as $i => $sh)
                                                    <tr class="cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                                        <td class="p-3 text-left whitespace-nowrap">
                                                            {{ $sh->provincialBT->name }}
                                                        </td>
                                                        <td class="p-3 text-center whitespace-nowrap">
                                                            {{ $sh->accountTypeReference->name }}
                                                        </td>
                                                        <td class="p-3 whitespace-nowrap">
                                                            <div class="flex justify-center w-24">
                                                                <x-input type="text"
                                                                    name="subcategories.{{ $i }}.distribution_pct"
                                                                    wire:model.defer='subcategories.{{ $i }}.distribution_pct'>
                                                                </x-input>
                                                            </div>
                                                            <x-input-error
                                                                for="subcategories.{{ $i }}.distribution_pct" />
                                                        </td>
                                                        <td class="p-3 text-center whitespace-nowrap">
                                                            125.13
                                                        </td>
                                                        <td class="p-3 whitespace-nowrap">
                                                            <div class="flex justify-center w-24">
                                                                <x-input type="text"
                                                                    name="subcategories.{{ $i }}.amount"
                                                                    wire:model.defer='subcategories.{{ $i }}.amount'>
                                                                </x-input>
                                                            </div>
                                                            <x-input-error
                                                                for="subcategories.{{ $i }}.amount" />
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </x-slot>
                                        </x-table.table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="flex {{ empty($stakeholder) ? 'justify-center' : 'justify-end' }} px-4 mt-8">
            <button type="button" wire:click="action({'id':'{{ $stakeholder }}'},'save')"
                class="w-48 px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                Save
            </button>
        </div>
    </div>
</div>
