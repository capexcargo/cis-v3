<x-form x-data="{
    corporate_target_modal: '{{ $corporate_target_modal }}',
    stakeholder_target_modal: '{{ $stakeholder_target_modal }}',
    edit_modal: '{{ $edit_modal }}'
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_sales_quota_target')
            <x-modal id="corporate_target_modal" size="w-3/4">
                <x-slot name="body">
                    @livewire('crm.sales.quota.corporate-target')
                </x-slot>
            </x-modal>
            <x-modal id="stakeholder_target_modal" size="w-auto">
                <x-slot name="body">
                    @livewire('crm.sales.quota.stakeholder-target')
                </x-slot>
            </x-modal>
        @endcan
        @can('crm_sales_quota_edit')
            @if ($edit_modal && $stakeholder_id)
                <x-modal id="edit_modal" size="w-auto">
                    <x-slot name="body">
                        @livewire('crm.sales.quota.edit', ['id' => $stakeholder_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
    </x-slot>
    <x-slot name="header_title">Quota</x-slot>
    <x-slot name="header_button">
        <div class="flex gap-4">
            @can('crm_sales_quota_target')
                <div x-data="{ open: false }">
                    <button class="flex gap-3 p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue" @click="open = !open">
                        <span>Manage Target</span>
                        <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                            <path fill="currentColor"
                                d="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z" />
                        </svg>
                    </button>
                    <div class="absolute border-2 border-gray-300 rounded-sm " style="margin-top: .4%; margin-left: -2.2rem"
                        x-transition:leave="transition ease-in duration-150" x-transition:leave-start="opacity-100"
                        x-transition:leave-end="opacity-0" x-show="open" @click.away="open = false"
                        @keydown.escape="open = false">

                        <ul class="text-gray-600 bg-white rounded shadow">
                            <li class="px-4 py-1 cursor-pointer whitespace-nowrap" x-data="{ open: false }"
                                wire:click="action({},'corporate_target')">
                                Corporate Target
                            </li>
                            <li class="px-4 py-1 cursor-pointer" x-data="{ open: false }"
                                wire:click="action({},'stakeholder_target')">
                                Stakeholders Target
                            </li>
                        </ul>
                    </div>
                </div>
            @endcan
            <div x-data="{ open2: false }">
                <button
                    class="flex gap-3 p-2 px-3 mr-3 text-sm text-[#003399] rounded-md bg-white border border-[#003399]"
                    @click="open2 = !open2">
                    <span>Management</span>
                    <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                        <path fill="currentColor"
                            d="M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z" />
                    </svg>
                </button>
                <div class="absolute border-2 border-gray-300 rounded-sm " style="margin-top: .4%; margin-left: -3.8rem"
                    x-transition:leave="transition ease-in duration-150" x-transition:leave-start="opacity-100"
                    x-transition:leave-end="opacity-0" x-show="open2" @click.away="open2 = false"
                    @keydown.escape="open2 = false">

                    <ul class="text-gray-600 bg-white rounded shadow">
                        <li class="px-4 py-1 cursor-pointer whitespace-nowrap" x-data="{ open2: false }"
                            wire:click="action({},'stake_holder_category')">
                            Stakeholder Category
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </x-slot>
    <x-slot name="body">
        <div class="flex gap-6">
            <button wire:click="headerCard(1)"
                class="px-4 py-4 bg-white border border-[#003399] rounded-lg hover:bg-blue-100 
                {{ $header_card == 1 ? 'border-2' : 'border' }} ">
                <h5 class="font-medium text-left text-gray-700 uppercase dark:text-gray-40">CORPORATE<br>TARGET
                    DISTRIBUTION
                </h5>
            </button>
            <button wire:click="headerCard(2)"
                class="px-4 py-4 bg-white border border-[#003399] rounded-lg hover:bg-blue-100 
                {{ $header_card == 2 ? 'border-2' : 'border' }} ">
                <h5 class="font-medium text-left text-gray-700 uppercase dark:text-gray-400">STAKEHOLDERS<br>TARGET
                    DISTRIBUTION
                </h5>
            </button>
        </div>

        <div class="flex justify-between" style="margin-top: 3rem; margin-bottom:-20px">
            <div class="flex gap-12">
                @if ($header_card == 2)
                    <div class="w-48">
                        <x-transparent.select label="Stakeholder" name="stakeholder" wire:model.defer="stakeholder">
                            <option value=""></option>
                        </x-transparent.select>
                    </div>
                @endif
                <div class="w-48">
                    <x-transparent.select label="Month" name="month" wire:model.defer="month">
                        <option value=""></option>
                        @foreach ($months_references as $i => $months_ref)
                            <option value="{{ $months_ref->id }}">{{ $months_ref->display }}</option>
                        @endforeach
                    </x-transparent.select>
                </div>
                <div class="w-48">
                    <x-transparent.select label="Year" name="year" wire:model.defer="year">
                        <option value=""></option>
                        <option value="2023">2023</option>
                        <option value="2022">2022</option>
                        <option value="2021">2021</option>
                        <option value="2020">2020</option>
                        <option value="2019">2019</option>
                    </x-transparent.select>
                </div>
                <div class="">
                    <x-button type="button" wire:click="search" title="Search"
                        class="text-white bg-blue hover:bg-blue-800" />
                </div>
            </div>
            <div class="mt-6 italic">
                For the month of {{ date('M') }}, {{ date('Y') }}
            </div>
        </div>
        @if ($header_card == 1)
            <div class="overflow-auto rounded-lg">
                <div class="grid grid-cols-1 gap-4 mt-4">
                    <div class="-mt-4 rounded-lg shadow-lg">
                        <x-table.table>
                            <x-slot name="thead">
                                <div class="flex justify-between">
                                    <th style="background-color: rgba(243, 244, 246, var(--tw-bg-opacity))"></th>
                                    <th style="background-color: rgba(243, 244, 246, var(--tw-bg-opacity))"></th>
                                    <th style="background-color: rgba(243, 244, 246, var(--tw-bg-opacity))"></th>
                                    <th style="background-color: rgba(243, 244, 246, var(--tw-bg-opacity))"></th>
                                    <th style="background-color: rgba(243, 244, 246, var(--tw-bg-opacity))"></th>
                                    <th style="background-color: rgba(243, 244, 246, var(--tw-bg-opacity))"></th>
                                    <th class="w-644 -mb-4ite text6center py-14-mt-2">
                                        <div class="text-lg font-normal text-blue text-[#003399]">TARGET VOLUME
                                            PER SERVICES
                                            OFFERED</div>
                                    </th>
                                    <div></div>
                                </div>
                            </x-slot>
                            <x-slot name="thead1">
                                <th class='p-3 text-center'>
                                    <div class="w-12 font-medium">
                                        No.
                                    </div>
                                </th>
                                <th class='p-3 text-center border-2 border-t-0 border-b-0 border-gray-500'>
                                    <div class="font-medium w-28">
                                        Stakeholders
                                    </div>
                                </th>
                                <th class='p-3 text-center border-2 border-t-0 border-b-0 border-gray-500'>
                                    <div class="font-medium w-44">
                                        Account Type
                                    </div>
                                </th>
                                <th class='p-3 text-center border-2 border-t-0 border-b-0 border-gray-500'>
                                    <div class="font-medium w-28">
                                        Monthly Tonnage
                                    </div>
                                </th>
                                <th class='p-3 text-center border-2 border-t-0 border-b-0 border-gray-500'>
                                    <div class="font-medium w-28">
                                        Daily Tonnage
                                    </div>
                                </th>
                                <th class='p-3 text-center border-2 border-t-0 border-b-0 border-gray-500'>
                                    <div class="w-24 font-medium">
                                        Distribution<br>Percentage
                                    </div>
                                </th>
                                <th class="text-center">
                                    <div class="flex p-0">
                                        <div class="w-64 font-semibold text-left border-r-2 border-gray-500">
                                            <div
                                                class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                SEA
                                                FREIGHT
                                            </div>
                                            <div class="flex justify-between gap-12 px-4 mt-2">
                                                <span class="text-gray-700 whitespace-nowrap">Monthly
                                                    CBM</span>
                                                <span class="text-gray-700 whitespace-nowrap">Daily CBM</span>
                                            </div>
                                        </div>
                                        <div class="w-64 font-semibold text-left border-r-2 border-gray-500">
                                            <div
                                                class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                AIR
                                                FREIGHT
                                            </div>
                                            <div class="flex justify-between gap-12 px-4 mt-2">
                                                <span class="text-gray-700 whitespace-nowrap">Monthly
                                                    KG.</span>
                                                <span class="text-gray-700 whitespace-nowrap">Daily KG.</span>
                                            </div>
                                        </div>
                                        <div class="w-64 font-semibold text-left border-r-2 border-gray-500">
                                            <div
                                                class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                LAND FREIGHT
                                            </div>
                                            <div class="flex justify-between gap-12 px-4 mt-2">
                                                <span class="text-gray-700 whitespace-nowrap">Monthly
                                                    CBM</span>
                                                <span class="text-gray-700 whitespace-nowrap">Daily CBM</span>
                                            </div>
                                        </div>
                                        <div class="w-64 font-semibold text-left">
                                            <div
                                                class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                INTERNATIONAL
                                                FREIGHT
                                            </div>
                                            <div class="flex justify-between gap-12 px-4 mt-2">
                                                <span class="text-gray-700 whitespace-nowrap">Monthly
                                                    KG.</span>
                                                <span class="text-gray-700 whitespace-nowrap">Daily KG.</span>
                                            </div>
                                        </div>
                                    </div>
                                </th>
                            </x-slot>
                            <x-slot name="tbody">
                                @foreach ($corporate_targets as $i => $corporate_target)
                                    <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                        <td class="w-4 p-3 whitespace-nowrap">
                                            {{ $i += 1 }}.
                                        </td>
                                        <td
                                            class="p-3 border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                            {{ $corporate_target['stakeholder_category_name'] }}
                                        </td>
                                        <td
                                            class="p-3 border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                            <div class="grid grid-cols-1 space-y-6">
                                                @foreach ($corporate_target_subcategories as $corporate_target_subcategory)
                                                    @if ($corporate_target->management_id == $corporate_target_subcategory->management_id)
                                                        @if ($corporate_target_subcategory->management_id == 1)
                                                            @foreach ($corporate_target_subcategory->accountType as $i => $corporate_target_subcategory)
                                                                <p>{{ $corporate_target_subcategory->name }}</p>
                                                            @endforeach
                                                        @else
                                                            <p>{{ $corporate_target->accountTypeReference->name }}</p>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </div>
                                        </td>
                                        <td
                                            class="p-3 text-center border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                            <div class="grid grid-cols-1 space-y-6">
                                                @foreach ($corporate_target_subcategories as $corporate_target_subcategory)
                                                    @if ($corporate_target->management_id == $corporate_target_subcategory->management_id)
                                                        <p>{{ $corporate_target_subcategory->monthly_tonnage }}</p>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </td>
                                        <td
                                            class="p-3 text-center border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                            <div class="grid grid-cols-1 space-y-6">
                                                @foreach ($corporate_target_subcategories as $corporate_target_subcategory)
                                                    @if ($corporate_target->management_id == $corporate_target_subcategory->management_id)
                                                        <p>{{ number_format($corporate_target_subcategory->monthly_tonnage / 26, 2) }}
                                                        </p>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </td>
                                        <td
                                            class="p-3 text-center border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                            <div class="grid grid-cols-1 space-y-6">
                                                @foreach ($corporate_target_subcategories as $corporate_target_subcategory)
                                                    @if ($corporate_target->management_id == $corporate_target_subcategory->management_id)
                                                        <p class="text-gray-700 whitespace-nowrap">
                                                            {{ $corporate_target_subcategory->distribution_pct }}%
                                                        </p>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="flex">
                                                <div class="w-64 font-semibold text-center border-r-2 border-gray-500"
                                                    style="margin-top: -.8%; margin-bottom:-.8%">
                                                    <div class="flex justify-between gap-12 px-6 mt-2">
                                                        <div class="grid grid-cols-1 space-y-6 text-center">
                                                            @foreach ($corporate_target_subcategories as $corporate_target_subcategory)
                                                                @if ($corporate_target->management_id == $corporate_target_subcategory->management_id)
                                                                    <p class="text-gray-700 whitespace-nowrap">
                                                                        {{ $corporate_target_subcategory->monthly_cbm_sea }}
                                                                    </p>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                        <div class="grid grid-cols-1 space-y-6 text-center">
                                                            @foreach ($corporate_target_subcategories as $corporate_target_subcategory)
                                                                @if ($corporate_target->management_id == $corporate_target_subcategory->management_id)
                                                                    <p class="text-gray-700 whitespace-nowrap">
                                                                        {{ number_format($corporate_target_subcategory->monthly_cbm_sea / 26, 2) }}
                                                                    </p>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="w-64 font-semibold text-center border-r-2 border-gray-500"
                                                    style="margin-top: -.8%; margin-bottom:-.8%">
                                                    <div class="flex justify-between gap-12 px-6 mt-2">
                                                        <div class="grid grid-cols-1 space-y-6 text-center">
                                                            @foreach ($corporate_target_subcategories as $corporate_target_subcategory)
                                                                @if ($corporate_target->management_id == $corporate_target_subcategory->management_id)
                                                                    <p class="text-gray-700 whitespace-nowrap">
                                                                        {{ $corporate_target_subcategory->monthly_kg_air }}
                                                                    </p>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                        <div class="grid grid-cols-1 space-y-6 text-center">
                                                            @foreach ($corporate_target_subcategories as $corporate_target_subcategory)
                                                                @if ($corporate_target->management_id == $corporate_target_subcategory->management_id)
                                                                    <p class="text-gray-700 whitespace-nowrap">
                                                                        {{ number_format($corporate_target_subcategory->monthly_kg_air / 26, 2) }}
                                                                    </p>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="w-64 font-semibold text-center border-r-2 border-gray-500"
                                                    style="margin-top: -.8%; margin-bottom:-.8%">
                                                    <div class="flex justify-between gap-12 px-6 mt-2">
                                                        <div class="grid grid-cols-1 space-y-6 text-center">
                                                            @foreach ($corporate_target_subcategories as $corporate_target_subcategory)
                                                                @if ($corporate_target->management_id == $corporate_target_subcategory->management_id)
                                                                    <p class="text-gray-700 whitespace-nowrap">
                                                                        {{ $corporate_target_subcategory->monthly_cbm_land }}
                                                                    </p>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                        <div class="grid grid-cols-1 space-y-6 text-center">
                                                            @foreach ($corporate_target_subcategories as $corporate_target_subcategory)
                                                                @if ($corporate_target->management_id == $corporate_target_subcategory->management_id)
                                                                    <p class="text-gray-700 whitespace-nowrap">
                                                                        {{ number_format($corporate_target_subcategory->monthly_cbm_land / 26, 2) }}
                                                                    </p>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="w-64 font-semibold text-center">
                                                    <div class="flex justify-between gap-12 px-6 mt-2">
                                                        <div class="grid grid-cols-1 space-y-6 text-center">
                                                            @foreach ($corporate_target_subcategories as $corporate_target_subcategory)
                                                                @if ($corporate_target->management_id == $corporate_target_subcategory->management_id)
                                                                    <p class="text-gray-700 whitespace-nowrap">
                                                                        {{ $corporate_target_subcategory->monthly_kg_int }}
                                                                    </p>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                        <div class="grid grid-cols-1 space-y-6 text-center">
                                                            @foreach ($corporate_target_subcategories as $corporate_target_subcategory)
                                                                @if ($corporate_target->management_id == $corporate_target_subcategory->management_id)
                                                                    <p class="text-gray-700 whitespace-nowrap">
                                                                        {{ number_format($corporate_target_subcategory->monthly_kg_int / 26, 2) }}
                                                                    </p>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </x-slot>
                            <x-slot name="tbody1">
                                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                    <td class="w-4 p-3 whitespace-nowrap">
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <span class="text-sm text-blue">TOTAL :</span>
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                    </td>
                                    <td
                                        class="p-3 text-center border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                        @php
                                            foreach ($corporate_target_subcategories as $corporate_target_subcategory) {
                                                $total_monthly_tonnage[] = $corporate_target_subcategory->monthly_tonnage;
                                            }
                                        @endphp
                                        <span
                                            class="text-sm text-blue">{{ number_format(array_sum($total_monthly_tonnage)) }}</span>
                                    </td>
                                    <td
                                        class="p-3 text-center border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                        @php
                                            foreach ($corporate_target_subcategories as $corporate_target_subcategory) {
                                                $total_daily_tonnage[] = $corporate_target_subcategory->monthly_tonnage / 26;
                                            }
                                        @endphp
                                        <span
                                            class="text-sm text-blue">{{ number_format(array_sum($total_daily_tonnage)) }}</span>
                                    </td>
                                    <td
                                        class="p-3 text-center border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                        @php
                                            foreach ($corporate_target_subcategories as $corporate_target_subcategory) {
                                                $total_distribution_pct[] = $corporate_target_subcategory->distribution_pct;
                                            }
                                        @endphp
                                        <span
                                            class="text-sm text-blue">{{ array_sum($total_distribution_pct) }}%</span>
                                    </td>

                                    <td class="text-center">
                                        <div class="flex">
                                            <div class="w-64 text-center">
                                                <div class="flex justify-between gap-12 px-6 mt-2 text-sm text-blue">
                                                    <div class="grid grid-cols-1 space-y-6 text-center">
                                                        @php
                                                            foreach ($corporate_target_subcategories as $corporate_target_subcategory) {
                                                                $total_monthly_cbm_sea[] = $corporate_target_subcategory->monthly_cbm_sea;
                                                            }
                                                        @endphp
                                                        <p class="text-sm text-blue whitespace-nowrap">
                                                            {{ number_format(array_sum($total_monthly_cbm_sea)) }}
                                                        </p>
                                                    </div>
                                                    <div class="grid grid-cols-1 space-y-6 text-center">
                                                        @php
                                                            foreach ($corporate_target_subcategories as $corporate_target_subcategory) {
                                                                $total_daily_cbm_sea[] = $corporate_target_subcategory->monthly_cbm_sea / 26;
                                                            }
                                                        @endphp
                                                        <p class="text-sm text-blue whitespace-nowrap">
                                                            {{ number_format(array_sum($total_daily_cbm_sea)) }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="border-r-2 border-gray-500 "
                                                style="margin-top: -.8%; margin-bottom:-.8%; margin-left:-2px"></div>
                                            <div class="w-64 text-center border-r-2 border-gray-500"
                                                style="margin-top: -.8%; margin-bottom:-.8%;">
                                                <div class="flex justify-between gap-12 px-6 mt-4 text-sm text-blue">
                                                    <div class="grid grid-cols-1 space-y-6 text-center">
                                                        @php
                                                            foreach ($corporate_target_subcategories as $corporate_target_subcategory) {
                                                                $total_monthly_kg_air[] = $corporate_target_subcategory->monthly_kg_air;
                                                            }
                                                        @endphp
                                                        <p class="text-sm text-blue whitespace-nowrap">
                                                            {{ number_format(array_sum($total_monthly_kg_air)) }}
                                                        </p>
                                                    </div>
                                                    <div class="grid grid-cols-1 space-y-6 text-center">
                                                        @php
                                                            foreach ($corporate_target_subcategories as $corporate_target_subcategory) {
                                                                $total_daily_kg_air[] = $corporate_target_subcategory->monthly_kg_air / 26;
                                                            }
                                                        @endphp
                                                        <p class="text-sm text-blue whitespace-nowrap">
                                                            {{ number_format(array_sum($total_daily_kg_air)) }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w-64 text-center border-r-2 border-gray-500"
                                                style="margin-top: -.8%; margin-bottom:-.8%;">
                                                <div class="flex justify-between gap-12 px-6 mt-4 text-sm text-blue">
                                                    <div class="grid grid-cols-1 space-y-6 text-center">
                                                        @php
                                                            foreach ($corporate_target_subcategories as $corporate_target_subcategory) {
                                                                $total_monthly_cbm_land[] = $corporate_target_subcategory->monthly_cbm_land;
                                                            }
                                                        @endphp
                                                        <p class="text-sm text-blue whitespace-nowrap">
                                                            {{ number_format(array_sum($total_monthly_cbm_land)) }}
                                                        </p>
                                                    </div>
                                                    <div class="grid grid-cols-1 space-y-6 text-center">
                                                        @php
                                                            foreach ($corporate_target_subcategories as $corporate_target_subcategory) {
                                                                $total_daily_cbm_land[] = $corporate_target_subcategory->monthly_cbm_land / 26;
                                                            }
                                                        @endphp
                                                        <p class="text-sm text-blue whitespace-nowrap">
                                                            {{ number_format(array_sum($total_daily_cbm_land)) }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w-64 mt-2 text-center">
                                                <div class="flex justify-between gap-12 px-6">
                                                    <div
                                                        class="grid grid-cols-1 space-y-6 text-sm text-center text-blue">
                                                        @php
                                                            foreach ($corporate_target_subcategories as $corporate_target_subcategory) {
                                                                $total_monthly_kg[] = $corporate_target_subcategory->monthly_kg_int;
                                                            }
                                                        @endphp
                                                        <p class="text-sm text-blue whitespace-nowrap">
                                                            {{ number_format(array_sum($total_monthly_kg)) }}
                                                        </p>
                                                    </div>
                                                    <div
                                                        class="grid grid-cols-1 space-y-6 text-sm text-center text-blue">
                                                        @php
                                                            foreach ($corporate_target_subcategories as $corporate_target_subcategory) {
                                                                $total_daily_kg[] = $corporate_target_subcategory->monthly_kg_int / 26;
                                                            }
                                                        @endphp
                                                        <p class="text-sm text-blue whitespace-nowrap">
                                                            {{ number_format(array_sum($total_daily_kg)) }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </x-slot>
                        </x-table.table>
                        <div class="px-1 pb-2">
                        </div>
                        <div class="flex mt-4">
                            <div class="grid grid-cols-1 px-6 py-4 space-y-4 bg-white rounded-lg shadow-lg">
                                <div class="grid grid-cols-2 gap-6">
                                    <div class="text-[#003399] mt-2">Total Days Count: </div>
                                    <div>
                                        <input type="text"
                                            class="w-48 px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="total_days_count" wire:model.defer='total_days_count' disabled>
                                    </div>
                                </div>
                                <div class="grid grid-cols-2 gap-6">
                                    <div class="text-[#003399] mt-2">Monthly Total Target Amount: </div>
                                    <div>
                                        <input type="text"
                                            class="w-48 px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                            name="monthly_target_amount" wire:model.defer='monthly_target_amount'
                                            disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @elseif ($header_card == 2)
            <div class="overflow-auto rounded-lg">
                <div class="grid grid-cols-1 gap-4 mt-4">
                    <div class="flex justify-between">
                        <ul class="flex mt-2 whitespace-nowrap">
                            @foreach ($stake_holder_header_cards as $i => $stake_holder_card)
                                <li wire:click="ActiveType('{{ $stake_holder_header_cards[$i]['id'] }}')"
                                    class="px-3 py-2 text-xs cursor-pointer {{ $stake_holder_header_cards[$i]['class'] }} {{ $stake_holder == $stake_holder_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                                    <button>
                                        {{ $stake_holder_header_cards[$i]['title'] }}
                                    </button>
                                    {{-- <div class="text-md">{{ $stake_holder_header_cards[$i]['value'] }}</div> --}}
                                </li>
                            @endforeach
                        </ul>
                        <div>
                            @can('crm_sales_quota_edit')
                                @if ($stake_holder != '')
                                    <div class="">
                                        <x-button type="button" wire:click="action({id:'{{ $stake_holder }}'}, 'edit')"
                                            title="Edit"
                                            class="text-blue bg-white hover:bg-blue-800 hover:text-white border-[#003399]" />
                                    </div>
                                @endif
                            @endcan
                        </div>
                    </div>
                    @if ($stake_holder == '')
                        <div class="-mt-4 rounded-lg shadow-lg">
                            <x-table.table>
                                <x-slot name="thead">
                                    <x-table.th name="No." />
                                    <th
                                        class='p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                        <div class="font-medium w-72">
                                            Stakeholders Category
                                        </div>
                                    </th>
                                    <th class="w-1/3 text-center">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                    OVERALL ACTUAL PERFORMANCE
                                                </div>
                                                <div class="flex justify-between gap-12 px-4 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Percentage</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Percentage</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($stakeholders_targets as $i => $all_sh)
                                        <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $i += 1 }}.
                                            </td>
                                            <td class="p-3 border-r-2 border-gray-500 whitespace-nowrap">
                                                {{ $all_sh->stakeholder_category_name }}
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-6 mt-2">
                                                            <span class="text-gray-700 whitespace-nowrap">212.00</span>
                                                            <span class="text-red-500 whitespace-nowrap">29%</span>
                                                            <span
                                                                class="text-gray-700 whitespace-nowrap">570,000.00</span>
                                                            <span class="text-green-500 whitespace-nowrap">95%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                        </div>
                    @elseif($stake_holder == 1)
                        <div class="-mt-4 rounded-lg shadow-lg">
                            <x-table.table>
                                <x-slot name="thead">
                                    <th class='p-3 tracking-wider text-left w-18 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            No.
                                        </div>
                                    </th>
                                    <th class='w-1/6 p-3 tracking-wider text-left whitespace-nowrap'>
                                        <div class="font-medium w-72">
                                            CSM
                                        </div>
                                    </th>
                                    <th
                                        class='p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            Position
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                    QUOTA
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                    ACTUAL
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                    PERFORMANCE
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                    STATUS
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($stakeholders_targets as $i => $all_sh)
                                        <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $i += 1 }}.
                                            </td>
                                            <td class="p-3 underline whitespace-nowrap text-[#003399]">
                                                {{-- {{ $all_sh->accountTypeBT->name }} --}}
                                            </td>
                                            <td class="p-3 border-r-2 border-gray-500 whitespace-nowrap">
                                                Customer Relations Executive
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-6 mt-2">
                                                            <span
                                                                class="ml-4 text-gray-700 whitespace-nowrap">250.26</span>
                                                            <span
                                                                class="text-gray-700 whitespace-nowrap">150,000.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-6 mt-2">
                                                            <span
                                                                class="ml-4 text-gray-700 whitespace-nowrap">69.00</span>
                                                            <span
                                                                class="text-gray-700 whitespace-nowrap">100,000.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-12 mt-2">
                                                            <span class="text-gray-700 whitespace-nowrap">28%</span>
                                                            <span class="text-gray-700 whitespace-nowrap">67%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-10 mt-2">
                                                            <span class="text-red-500 whitespace-nowrap">BEHIND</span>
                                                            <span class="text-green-500 whitespace-nowrap">AHEAD</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                        </div>
                    @elseif($stake_holder == 2)
                        {{-- <div class="-mt-4 rounded-lg shadow-lg">
                            <x-table.table>
                                <x-slot name="thead">
                                    <th class='p-3 tracking-wider text-left w-18 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            No.
                                        </div>
                                    </th>
                                    <th
                                        class='w-1/4 p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                        <div class="font-medium w-72">
                                            NL / SL
                                        </div>
                                    </th>
                                    <th
                                        class='w-1/4 p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                        <div class="font-medium w-72">
                                            Account Type
                                        </div>
                                    </th>
                                    <th
                                        class='w-1/4 p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                        <div class="font-medium">
                                            Distribution<br>Percentage
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                    QUOTA
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                    ACTUAL
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                    PERFORMANCE
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                    STATUS
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($stakeholders_targets as $i => $all_sh)
                                        <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $i += 1 }}.
                                            </td>
                                            <td class="p-3 border-r-2 border-gray-500 whitespace-nowrap">
                                                {{ $all_sh->nlslBT->name }}
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-6 mt-2">
                                                            <span
                                                                class="ml-4 text-gray-700 whitespace-nowrap">250.26</span>
                                                            <span
                                                                class="text-gray-700 whitespace-nowrap">150,000.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-6 mt-2">
                                                            <span
                                                                class="ml-4 text-gray-700 whitespace-nowrap">69.00</span>
                                                            <span
                                                                class="text-gray-700 whitespace-nowrap">100,000.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-12 mt-2">
                                                            <span class="text-gray-700 whitespace-nowrap">28%</span>
                                                            <span class="text-gray-700 whitespace-nowrap">67%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-10 mt-2">
                                                            <span class="text-red-500 whitespace-nowrap">BEHIND</span>
                                                            <span class="text-green-500 whitespace-nowrap">AHEAD</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                        </div> --}}
                    @elseif($stake_holder == 3)
                        <div class="-mt-4 rounded-lg shadow-lg">
                            <x-table.table>
                                <x-slot name="thead">
                                    <th class='p-3 tracking-wider text-left w-18 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            No.
                                        </div>
                                    </th>
                                    <th class='w-1/6 p-3 tracking-wider text-left whitespace-nowrap'>
                                        <div class="font-medium">
                                            Metro Manila - field
                                        </div>
                                    </th>
                                    <th
                                        class='p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            Account Type
                                        </div>
                                    </th>
                                    <th
                                        class='p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            Distribution<br>Percentage
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                    QUOTA
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                    ACTUAL
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                    PERFORMANCE
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                    STATUS
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($stakeholders_targets as $i => $all_sh)
                                        <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $i += 1 }}.
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $all_sh->quadrantBT->name }}
                                            </td>
                                            <td class="p-3 border-r-2 border-gray-500 whitespace-nowrap">
                                            </td>
                                            <td class="p-3 border-r-2 border-gray-500 whitespace-nowrap">
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-6 mt-2">
                                                            <span
                                                                class="ml-4 text-gray-700 whitespace-nowrap">250.26</span>
                                                            <span
                                                                class="text-gray-700 whitespace-nowrap">150,000.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-6 mt-2">
                                                            <span
                                                                class="ml-4 text-gray-700 whitespace-nowrap">69.00</span>
                                                            <span
                                                                class="text-gray-700 whitespace-nowrap">100,000.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-12 mt-2">
                                                            <span class="text-gray-700 whitespace-nowrap">28%</span>
                                                            <span class="text-gray-700 whitespace-nowrap">67%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-10 mt-2">
                                                            <span class="text-red-500 whitespace-nowrap">BEHIND</span>
                                                            <span class="text-green-500 whitespace-nowrap">AHEAD</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                        </div>
                    @elseif($stake_holder == 4)
                        <div class="-mt-4 rounded-lg shadow-lg">
                            <x-table.table>
                                <x-slot name="thead">
                                    <th class='p-3 tracking-wider text-left w-18 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            No.
                                        </div>
                                    </th>
                                    <th class='w-1/6 p-3 tracking-wider text-left whitespace-nowrap'>
                                        <div class="font-medium">
                                            Metro Manila - Walk In
                                        </div>
                                    </th>
                                    <th
                                        class='p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            Account Type
                                        </div>
                                    </th>
                                    <th
                                        class='p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            Distribution<br>Percentage
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                    QUOTA
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                    ACTUAL
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                    PERFORMANCE
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                    STATUS
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($stakeholders_targets as $i => $all_sh)
                                        <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $i += 1 }}.
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $all_sh->walkinBT->name }}
                                            </td>
                                            <td class="p-3 border-r-2 border-gray-500 whitespace-nowrap">
                                            </td>
                                            <td class="p-3 border-r-2 border-gray-500 whitespace-nowrap">
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-6 mt-2">
                                                            <span
                                                                class="ml-4 text-gray-700 whitespace-nowrap">250.26</span>
                                                            <span
                                                                class="text-gray-700 whitespace-nowrap">150,000.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-6 mt-2">
                                                            <span
                                                                class="ml-4 text-gray-700 whitespace-nowrap">69.00</span>
                                                            <span
                                                                class="text-gray-700 whitespace-nowrap">100,000.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-12 mt-2">
                                                            <span class="text-gray-700 whitespace-nowrap">28%</span>
                                                            <span class="text-gray-700 whitespace-nowrap">67%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-10 mt-2">
                                                            <span class="text-red-500 whitespace-nowrap">BEHIND</span>
                                                            <span class="text-green-500 whitespace-nowrap">AHEAD</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                        </div>
                    @elseif($stake_holder == 5)
                        <div class="-mt-4 rounded-lg shadow-lg">
                            <x-table.table>
                                <x-slot name="thead">
                                    <th class='p-3 tracking-wider text-left w-18 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            No.
                                        </div>
                                    </th>
                                    <th class='w-1/6 p-3 tracking-wider text-left whitespace-nowrap'>
                                        <div class="font-medium">
                                            Provincial Branch
                                        </div>
                                    </th>
                                    <th
                                        class='p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            Account Type
                                        </div>
                                    </th>
                                    <th
                                        class='p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            Distribution<br>Percentage
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                    QUOTA
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                    ACTUAL
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                    PERFORMANCE
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                    STATUS
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($stakeholders_targets as $i => $all_sh)
                                        <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $i += 1 }}.
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $all_sh->provincialBT->name }}
                                            </td>
                                            <td class="p-3 border-r-2 border-gray-500 whitespace-nowrap">
                                            </td>
                                            <td class="p-3 border-r-2 border-gray-500 whitespace-nowrap">
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-6 mt-2">
                                                            <span
                                                                class="ml-4 text-gray-700 whitespace-nowrap">250.26</span>
                                                            <span
                                                                class="text-gray-700 whitespace-nowrap">150,000.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-6 mt-2">
                                                            <span
                                                                class="ml-4 text-gray-700 whitespace-nowrap">69.00</span>
                                                            <span
                                                                class="text-gray-700 whitespace-nowrap">100,000.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-12 mt-2">
                                                            <span class="text-gray-700 whitespace-nowrap">28%</span>
                                                            <span class="text-gray-700 whitespace-nowrap">67%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-10 mt-2">
                                                            <span class="text-red-500 whitespace-nowrap">BEHIND</span>
                                                            <span class="text-green-500 whitespace-nowrap">AHEAD</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                        </div>
                    @elseif($stake_holder == 6)
                        <div class="-mt-4 rounded-lg shadow-lg">
                            <x-table.table>
                                <x-slot name="thead">
                                    <th class='p-3 tracking-wider text-left w-18 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            No.
                                        </div>
                                    </th>
                                    <th class='w-1/6 p-3 tracking-wider text-left whitespace-nowrap'>
                                        <div class="font-medium">
                                            Online Channel
                                        </div>
                                    </th>
                                    <th
                                        class='p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            Account Type
                                        </div>
                                    </th>
                                    <th
                                        class='p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                        <div class="w-24 font-medium">
                                            Distribution<br>Percentage
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                    QUOTA
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                    ACTUAL
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center border-r-2 border-gray-500">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                    PERFORMANCE
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="w-1/5 text-center">
                                        <div class="grid grid-cols-1">
                                            <div class="font-semibold text-left">
                                                <div
                                                    class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                    STATUS
                                                </div>
                                                <div class="flex justify-between gap-12 px-10 mt-2">
                                                    <span class="text-gray-700 whitespace-nowrap">Volume</span>
                                                    <span class="text-gray-700 whitespace-nowrap">Amount</span>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                </x-slot>
                                <x-slot name="tbody">
                                    @foreach ($stakeholders_targets as $i => $all_sh)
                                        <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $i += 1 }}.
                                            </td>
                                            <td class="p-3 whitespace-nowrap">
                                                {{ $all_sh->channelSourceBT->name }}
                                            </td>
                                            <td class="p-3 border-r-2 border-gray-500 whitespace-nowrap">
                                            </td>
                                            <td class="p-3 border-r-2 border-gray-500 whitespace-nowrap">
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-6 mt-2">
                                                            <span
                                                                class="ml-4 text-gray-700 whitespace-nowrap">250.26</span>
                                                            <span
                                                                class="text-gray-700 whitespace-nowrap">150,000.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-6 mt-2">
                                                            <span
                                                                class="ml-4 text-gray-700 whitespace-nowrap">69.00</span>
                                                            <span
                                                                class="text-gray-700 whitespace-nowrap">100,000.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-12 mt-2">
                                                            <span class="text-gray-700 whitespace-nowrap">28%</span>
                                                            <span class="text-gray-700 whitespace-nowrap">67%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="grid grid-cols-1">
                                                    <div class="font-semibold text-center">
                                                        <div class="flex justify-between gap-12 px-10 mt-2">
                                                            <span class="text-red-500 whitespace-nowrap">BEHIND</span>
                                                            <span class="text-green-500 whitespace-nowrap">AHEAD</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </x-slot>
                            </x-table.table>
                        </div>
                    @endif
                </div>
            </div>
        @endif
    </x-slot>
</x-form>
