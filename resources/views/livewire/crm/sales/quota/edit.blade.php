<div wire:init="load">
    <x-loading></x-loading>
    <div>
        <div class="px-4 mt-8 space-y-3">
            <div class="grid grid-cols-1 space-y-3">
                <div class="mt-4">
                    <div class="grid grid-cols-1">
                        <div class="overflow-auto rounded-lg">
                            <div class="grid grid-cols-1 gap-4 mt-4">
                                <div class="-mt-4 bg-white border rounded-lg shadow-lg">
                                    @if ($stakeholder_id != 1)
                                        <x-table.table>
                                            <x-slot name="thead">
                                                <th class='p-3 tracking-wider text-left w-18 whitespace-nowrap'>
                                                    <div class="w-24 font-medium">
                                                        No.
                                                    </div>
                                                </th>
                                                <th class='w-1/6 p-3 tracking-wider text-left whitespace-nowrap'>
                                                    <div class="font-medium">
                                                        @if ($stakeholder_id == 2)
                                                            NL/SL
                                                        @elseif ($stakeholder_id == 3)
                                                            Metro Manila - Field
                                                        @elseif ($stakeholder_id == 4)
                                                            Metro Manila - Walk In
                                                        @elseif ($stakeholder_id == 5)
                                                            Provincial Branches
                                                        @elseif ($stakeholder_id == 6)
                                                            Online Channel
                                                        @endif
                                                    </div>
                                                </th>
                                                <th
                                                    class='p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                                    <div class="w-24 font-medium">
                                                        Account Type
                                                    </div>
                                                </th>
                                                <th
                                                    class='p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                                    <div class="w-24 font-medium">
                                                        Distribution<br>Percentage
                                                    </div>
                                                </th>
                                                <th class="w-1/5 text-center border-r-2 border-gray-500">
                                                    <div class="grid grid-cols-1">
                                                        <div class="font-semibold text-left">
                                                            <div
                                                                class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                                QUOTA
                                                            </div>
                                                            <div class="flex justify-between gap-12 px-10 mt-2">
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Volume</span>
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Amount</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th class="w-1/5 text-center border-r-2 border-gray-500">
                                                    <div class="grid grid-cols-1">
                                                        <div class="font-semibold text-left">
                                                            <div
                                                                class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                                ACTUAL
                                                            </div>
                                                            <div class="flex justify-between gap-12 px-10 mt-2">
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Volume</span>
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Amount</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th class="w-1/5 text-center border-r-2 border-gray-500">
                                                    <div class="grid grid-cols-1">
                                                        <div class="font-semibold text-left">
                                                            <div
                                                                class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                                PERFORMANCE
                                                            </div>
                                                            <div class="flex justify-between gap-12 px-10 mt-2">
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Volume</span>
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Amount</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th class="w-1/5 text-center">
                                                    <div class="grid grid-cols-1">
                                                        <div class="font-semibold text-left">
                                                            <div
                                                                class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                                STATUS
                                                            </div>
                                                            <div class="flex justify-between gap-12 px-10 mt-2">
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Volume</span>
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Amount</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </x-slot>
                                            {{-- <x-slot name="tbody">
                                                @foreach ($stakeholders as $i => $sh)
                                                    <tr class="cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                                        <td class="p-3 text-left whitespace-nowrap">
                                                            {{ $sh->provincialBT->name }}
                                                        </td>
                                                        <td class="p-3 text-center whitespace-nowrap">
                                                            {{ $sh->accountTypeReference->name }}
                                                        </td>
                                                        <td class="p-3 whitespace-nowrap">
                                                            <div class="flex justify-center w-24">
                                                                <x-input type="text"
                                                                    name="subcategories.{{ $i }}.distribution_pct"
                                                                    wire:model.defer='subcategories.{{ $i }}.distribution_pct'>
                                                                </x-input>
                                                            </div>
                                                            <x-input-error
                                                                for="subcategories.{{ $i }}.distribution_pct" />
                                                        </td>
                                                        <td class="p-3 text-center whitespace-nowrap">
                                                            125.13
                                                        </td>
                                                        <td class="p-3 whitespace-nowrap">
                                                            <div class="flex justify-center w-24">
                                                                <x-input type="text"
                                                                    name="subcategories.{{ $i }}.amount"
                                                                    wire:model.defer='subcategories.{{ $i }}.amount'>
                                                                </x-input>
                                                            </div>
                                                            <x-input-error
                                                                for="subcategories.{{ $i }}.amount" />
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </x-slot> --}}
                                        </x-table.table>
                                    @else
                                        <x-table.table>
                                            <x-slot name="thead">
                                                <th class='p-3 tracking-wider text-left w-18 whitespace-nowrap'>
                                                    <div class="w-24 font-medium">
                                                        No.
                                                    </div>
                                                </th>
                                                <th class='w-1/6 p-3 tracking-wider text-left whitespace-nowrap'>
                                                    <div class="font-medium w-72">
                                                        CSM
                                                    </div>
                                                </th>
                                                <th
                                                    class='p-3 tracking-wider text-left border-r-2 border-gray-500 whitespace-nowrap'>
                                                    <div class="w-24 font-medium">
                                                        Position
                                                    </div>
                                                </th>
                                                <th class="w-1/5 text-center border-r-2 border-gray-500">
                                                    <div class="grid grid-cols-1">
                                                        <div class="font-semibold text-left">
                                                            <div
                                                                class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                                QUOTA
                                                            </div>
                                                            <div class="flex justify-between gap-12 px-10 mt-2">
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Volume</span>
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Amount</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th class="w-1/5 text-center border-r-2 border-gray-500">
                                                    <div class="grid grid-cols-1">
                                                        <div class="font-semibold text-left">
                                                            <div
                                                                class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                                ACTUAL
                                                            </div>
                                                            <div class="flex justify-between gap-12 px-10 mt-2">
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Volume</span>
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Amount</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th class="w-1/5 text-center border-r-2 border-gray-500">
                                                    <div class="grid grid-cols-1">
                                                        <div class="font-semibold text-left">
                                                            <div
                                                                class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                                                PERFORMANCE
                                                            </div>
                                                            <div class="flex justify-between gap-12 px-10 mt-2">
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Volume</span>
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Amount</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th class="w-1/5 text-center">
                                                    <div class="grid grid-cols-1">
                                                        <div class="font-semibold text-left">
                                                            <div
                                                                class="text-sm font-normal text-center bg-gray-300 text-[#003399] py-1">
                                                                STATUS
                                                            </div>
                                                            <div class="flex justify-between gap-12 px-10 mt-2">
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Volume</span>
                                                                <span
                                                                    class="text-gray-700 whitespace-nowrap">Amount</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </x-slot>
                                            <x-slot name="tbody">
                                            </x-slot>
                                        </x-table.table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex {{ empty($stakeholder_id) ? 'justify-center' : 'justify-end' }} px-4 mt-8">
            <button type="button" wire:click="action({'id':'{{ $stakeholder_id }}'},'save')"
                class="w-48 px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                Save
            </button>
        </div>
    </div>
</div>
