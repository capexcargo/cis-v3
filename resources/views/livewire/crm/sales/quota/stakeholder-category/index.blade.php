<x-form x-data="{
    create_modal: '{{ $create_modal }}',
    edit_modal: '{{ $edit_modal }}',
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_sales_quota_stakeholder_category_add')
            <x-modal id="create_modal" size="w-3/4">
                <x-slot name="title">Add Stakeholder Category</x-slot>
                <x-slot name="body">
                    @livewire('crm.sales.quota.stakeholder-category.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('crm_sales_quota_stakeholder_category_edit')
            @if ($edit_modal && $stakeholder_category_id)
                <x-modal id="edit_modal" size="w-3/4">
                    <x-slot name="title">Edit Stakeholder Category</x-slot>
                    <x-slot name="body">
                        @livewire('crm.sales.quota.stakeholder-category.edit', ['id' => $stakeholder_category_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('crm_sales_quota_stakeholder_category_updatestatus')
            <x-modal id="confirmation_modal" size="w-1/4">
                <x-slot name="body">
                    <span class="relative block">
                        <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                            wire:click="$set('confirmation_modal', false)">
                        </span>
                    </span>
                    <h2 class="mb-3 text-xl font-bold text-left text-blue">
                        Are you sure you want to deactivate this stakeholder category?
                    </h2>
                    <div class="flex justify-end mt-6 space-x-3">
                        <button type="button" wire:click="$set('confirmation_modal', false)"
                            class="px-12 py-2 text-xs font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-[#003399]">
                            No
                        </button>
                        <button type="button" wire:click="submit"
                            class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                            Yes
                        </button>
                    </div>
                </x-slot>
            </x-modal>
        @endcan
    </x-slot>
    <x-slot name="header_title">
        <div class="flex items-start justify-between" wire:click="action({}, 'back')">
            <svg class="w-8 h-8 mt-1 mr-4 text-blue-800 cursor-pointer" aria-hidden="true" focusable="false"
                data-prefix="far" data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512">
                <path fill="currentColor"
                    d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z" />
            </svg>
            <span class="mt-1 ml-4 text-3xl font-semibold">Stakeholder Category Management</span>
        </div>
    </x-slot>
    @can('crm_sales_quota_stakeholder_category_add')
        <x-slot name="header_button">
            <div class="flex gap-4">
                <button wire:click="action({}, 'create')"
                    class="flex gap-3 p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue" @click="open = !open">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    <span>Add Stakeholder</span>
                </button>
            </div>
        </x-slot>
    @endcan
    <x-slot name="body">
        <div class="overflow-auto rounded-lg">
            <div class="grid grid-cols-1 gap-4 mt-4">
                <div class="-mt-4 bg-white border rounded-lg shadow-lg">
                    <x-table.table>
                        <x-slot name="thead">
                            <th class='p-3 text-left'>
                                No.
                            </th>
                            <th class='p-3 text-left'>
                                Stakeholder Category
                            </th>
                            <th class='p-3 text-left'>
                                Subcategory
                            </th>
                            <th class='p-3 text-left'>
                                Action
                            </th>
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($stakeholder_categories as $i => $stakeholder_category)
                                <tr class="cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                    <td class="p-3 whitespace-nowrap">
                                        {{ ($stakeholder_categories->currentPage() - 1) * $stakeholder_categories->links()->paginator->perPage() + $loop->iteration }}.
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        {{ $stakeholder_category->name }}
                                    </td>
                                    <td class="p-3 whitespace-nowrap">
                                        <div class="grid grid-cols-1 space-y-2">
                                            @foreach ($subcategories as $subcategory)
                                                @if ($stakeholder_category->name == $subcategory->name)
                                                    @if ($subcategory->management_id == 1)
                                                        @foreach ($subcategory->accountType as $i => $subcategory)
                                                            <p>{{ $subcategory->name }}</p>
                                                        @endforeach
                                                    @elseif ($subcategory->management_id == 2)
                                                        @foreach ($subcategory->nlsl as $i => $subcategory)
                                                            <p>{{ $subcategory->display }}</p>
                                                        @endforeach
                                                    @elseif ($subcategory->management_id == 3)
                                                        @foreach ($subcategory->quadrant as $i => $subcategory)
                                                            <p>{{ $subcategory->name }}</p>
                                                        @endforeach
                                                    @elseif ($subcategory->management_id == 4)
                                                        @foreach ($subcategory->walkin as $i => $subcategory)
                                                            <p>{{ $subcategory->name }}</p>
                                                        @endforeach
                                                    @elseif ($subcategory->management_id == 5)
                                                        @foreach ($subcategory->provincial as $i => $subcategory)
                                                            <p>{{ $subcategory->display }}</p>
                                                        @endforeach
                                                    @elseif ($subcategory->management_id == 6)
                                                        @foreach ($subcategory->channelSource as $i => $subcategory)
                                                            <p>{{ $subcategory->name }}</p>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            @endforeach
                                        </div>
                                    </td>
                                    <td class="p-3 text-right whitespace-nowrap">
                                        <div class="flex ml-5 space-x-3">
                                            @can('crm_sales_quota_stakeholder_category_edit')
                                                <svg wire:click="action({'id': {{ $stakeholder_category->id }}}, 'edit')"
                                                    class="w-5 h-5 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="far" data-icon="edit" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                    <path fill="currentColor"
                                                        d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.8 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z">
                                                    </path>
                                                </svg>
                                            @endcan
                                            @can('crm_sales_quota_stakeholder_category_updatestatus')
                                                {{-- <svg wire:click="action({'id':{{ $stakeholder_category->id }}, 'status':'1'},'update_status') }}"
                                                    class="w-6 h-5 text-blue" aria-hidden="true" focusable="false"
                                                    data-prefix="fas" data-icon="trash-alt" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                    <path fill="currentColor"
                                                        d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                                                    </path>
                                                </svg> --}}
                                                <svg wire:click="action({'id':{{ $stakeholder_category->id }}, 'status':'2'},'update_status') }}"
                                                    class="w-6 h-5 text-red" aria-hidden="true" focusable="false"
                                                    data-prefix="fas" data-icon="circle-minus" role="img"
                                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                    <path fill="currentColor"
                                                        d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM175 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z">
                                                    </path>
                                                </svg>
                                            @endcan

                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                        </x-slot>
                    </x-table.table>
                </div>
            </div>
        </div>
    </x-slot>
</x-form>
