<div x-data="{
    confirmation_modal: '{{ $confirmation_modal }}',
}">
    <x-loading></x-loading>

    <x-modal id="confirmation_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
        <x-slot name="body">
            <span class="relative block">
                <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                    wire:click="$set('confirmation_modal', false)">
                </span>
            </span>
            <h2 class="mb-3 text-xl font-bold text-left text-blue">
                Are you sure you want to update this stakeholder category?
            </h2>
            <div class="flex justify-end mt-6 space-x-3">
                <button type="button" wire:click="$set('confirmation_modal', false)"
                    class="px-12 py-2 text-xs font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:text-white hover:bg-[#003399]">
                    No
                </button>
                <button type="button" wire:click="submit"
                    class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                    Yes
                </button>
            </div>
        </x-slot>
    </x-modal>

    <form wire:submit.prevent="confirmationSubmit" autocomplete="off">
        <div class="px-4 mt-8 space-y-3">
            <div class="grid grid-cols-3 gap-4">
                <div>
                    <x-label for="" value="Stakeholder Category Name" :required="true" />
                    <x-input rows="3" cols="50" type="text" name="stakeholder_category_name"
                        wire:model.defer='stakeholder_category_name'>
                    </x-input>
                    <x-input-error for="stakeholder_category_name" />
                </div>
                <div>
                    <x-label for="" value="Search Management Data" :required="true" />
                    <x-select name="search_management_data" wire:model='search_management_data'>
                        <option value="">Select</option>
                        <option value="1">CSM</option>
                        <option value="2">NL / SL</option>
                        <option value="3">Metro Manila - Field</option>
                        <option value="4">Metro Manila - Walk In</option>
                        <option value="5">Provincial Branches</option>
                        <option value="6">Online Channel</option>
                    </x-select>
                </div>
            </div>
        </div>
        <div class="mt-10 border-b border-gray-400 -px-2"></div>
        {{-- <div class="px-4 mt-4 space-y-4"> --}}
        <div class="grid grid-flow-col gap-4 px-4 mt-4" style="grid-template-rows: repeat(15, minmax(0, 1fr))">
            @foreach ($stakeholder_mgmt_data_refs as $i => $stakeholder_mgmt_data_ref)
                <div>
                    <label class="inline-flex items-center space-x-3">
                        <input type="checkbox" class="form-checkbox" name="selected_subs.{{ $i }}"
                            value="{{ $stakeholder_mgmt_data_ref['id'] }},{{ $stakeholder_mgmt_data_ref['reference_id'] }}"
                            wire:model.defer="selected_subs.{{ $i }}">
                        <label class="">{{ $stakeholder_mgmt_data_ref['name'] }}</label>
                    </label>
                </div>
            @endforeach
            @if ($selected_subs == null)
                <x-input-error for="selected_subs" />
            @endif
        </div>
        <div class="flex justify-end px-4 mt-12 space-x-3">
            <button type="button" wire:click="$emit('close_modal', 'edit')"
                class="px-6 py-2 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                Cancel
            </button>
            <button type="submit" class="px-6 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                Submit
            </button>
        </div>
    </form>
</div>
