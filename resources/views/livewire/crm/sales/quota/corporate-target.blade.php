<div wire:init="loadReferences" x-data="{
    {{-- confirmation_modal: '{{ $confirmation_modal }}', --}}
}">
    <x-loading></x-loading>
    <form wire:submit.prevent="save" autocomplete="off">
        <div class="overflow-auto rounded-lg">
            <div class="grid grid-cols-1 gap-4 px-4 mt-4 space-y-3">
                <div class="flex gap-8">
                    <div class="w-32 mt-2">
                        <x-label for="" value="For the Month of :" />
                    </div>
                    <div class="grid grid-cols-2 gap-4">
                        <div>
                            <x-select name="month" wire:model='month'>
                                <option value="">Month</option>
                                @foreach ($months_references as $i => $months_ref)
                                    <option value="{{ $months_ref->id }}">{{ $months_ref->display }}</option>
                                @endforeach
                            </x-select>
                        </div>
                        <div class="w-24">
                            <x-select name="year" wire:model='year'>
                                <option value="">Year</option>
                                <option value="2023">2023</option>
                                <option value="2022">2022</option>
                                <option value="2021">2021</option>
                                <option value="2020">2020</option>
                                <option value="2019">2019</option>
                            </x-select>
                        </div>
                    </div>
                </div>
                <div class="">
                    <x-table.table>
                        <x-slot name="thead">
                            <div class="flex justify-between">
                                <th style="background-color: transparent"></th>
                                <th style="background-color: transparent"></th>
                                <th style="background-color: transparent"></th>
                                <th style="background-color: transparent"></th>
                                <th style="background-color: transparent"></th>
                                <th style="background-color: transparent"></th>
                                <th class="w-64 -mb-4 text-center border py-14-mt-2">
                                    <div class="text-lg font-normal text-blue text-[#003399]">TARGET VOLUME
                                        PER SERVICES
                                        OFFERED</div>
                                </th>
                                <div></div>
                            </div>
                        </x-slot>
                        <x-slot name="thead1">
                            <th class='p-3 text-center'>
                                <div class="w-12 font-medium">
                                    No.
                                </div>
                            </th>
                            <th class='p-3 text-center border-2 border-t-0 border-b-0 border-gray-500'>
                                <div class="font-medium w-28">
                                    Stakeholders
                                </div>
                            </th>
                            <th class='p-3 text-center border-2 border-t-0 border-b-0 border-gray-500'>
                                <div class="font-medium w-44">
                                    Account Type
                                </div>
                            </th>
                            <th class='p-3 text-center border-2 border-t-0 border-b-0 border-gray-500'>
                                <div class="font-medium w-28">
                                    Monthly Tonnage
                                </div>
                            </th>
                            <th class='p-3 text-center border-2 border-t-0 border-b-0 border-gray-500'>
                                <div class="font-medium w-28">
                                    Daily Tonnage
                                </div>
                            </th>
                            <th class='p-3 text-center border-2 border-t-0 border-b-0 border-gray-500'>
                                <div class="w-24 font-medium">
                                    Distribution<br>Percentage
                                </div>
                            </th>
                            <th class="text-center">
                                <div class="flex p-0">
                                    <div class="w-64 font-semibold text-left border-r-2 border-gray-500">
                                        <div class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                            SEA
                                            FREIGHT
                                        </div>
                                        <div class="flex justify-between gap-12 px-4 mt-2">
                                            <span class="text-gray-700 whitespace-nowrap">Monthly
                                                CBM</span>
                                            <span class="text-gray-700 whitespace-nowrap">Daily CBM</span>
                                        </div>
                                    </div>
                                    <div class="w-64 font-semibold text-left border-r-2 border-gray-500">
                                        <div class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                            AIR
                                            FREIGHT
                                        </div>
                                        <div class="flex justify-between gap-12 px-4 mt-2">
                                            <span class="text-gray-700 whitespace-nowrap">Monthly
                                                KG.</span>
                                            <span class="text-gray-700 whitespace-nowrap">Daily KG.</span>
                                        </div>
                                    </div>
                                    <div class="w-64 font-semibold text-left border-r-2 border-gray-500">
                                        <div class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                            LAND FREIGHT
                                        </div>
                                        <div class="flex justify-between gap-12 px-4 mt-2">
                                            <span class="text-gray-700 whitespace-nowrap">Monthly
                                                CBM</span>
                                            <span class="text-gray-700 whitespace-nowrap">Daily CBM</span>
                                        </div>
                                    </div>
                                    <div class="w-64 font-semibold text-left">
                                        <div class="text-sm font-normal text-center bg-gray-200 text-[#003399] py-1">
                                            INTERNATIONAL
                                            FREIGHT
                                        </div>
                                        <div class="flex justify-between gap-12 px-4 mt-2">
                                            <span class="text-gray-700 whitespace-nowrap">Monthly
                                                KG.</span>
                                            <span class="text-gray-700 whitespace-nowrap">Daily KG.</span>
                                        </div>
                                    </div>
                                </div>
                            </th>
                        </x-slot>
                        <x-slot name="tbody">
                            @foreach ($stakeholders as $i => $stakeholder)
                                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8]">
                                    <td class="w-4 p-3 whitespace-nowrap">
                                        {{ $stakeholder['no'] }}
                                    </td>
                                    <td class="p-3 border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                        {{ $stakeholder['stakeholder_category_name'] }}
                                    </td>
                                    <td class="p-3 border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                        <div class="grid grid-cols-1 space-y-6">
                                            @if ($stakeholder['sh_subcategory'] != null)
                                                @foreach ($stakeholder['sh_subcategory'] as $subcat)
                                                    <p>{{ $subcat['subcategory_name'] }}</p>
                                                @endforeach
                                            @else
                                                <p>{{ $stakeholder['account_type'] }}</p>
                                            @endif
                                        </div>
                                    </td>
                                    <td
                                        class="p-3 text-right border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                        <div class="grid grid-cols-1 space-y-6">
                                            @if ($stakeholder['sh_subcategory'] != null)
                                                @foreach ($stakeholder['sh_subcategory'] as $x => $subcat)
                                                    <div class="flex justify-center w-24">
                                                        <x-input type="text"
                                                            name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.monthly_tonnage"
                                                            wire:model.defer='stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.monthly_tonnage'
                                                            wire:change="computeDaily">
                                                        </x-input>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="flex justify-center w-24">
                                                    <x-input type="text"
                                                        name="stakeholders.{{ $i }}.monthly_tonnage"
                                                        wire:model.defer='stakeholders.{{ $i }}.monthly_tonnage'
                                                        wire:change="computeDaily">
                                                    </x-input>
                                                </div>
                                            @endif
                                        </div>
                                    </td>
                                    <td
                                        class="p-3 text-right border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                        <div class="grid grid-cols-1 space-y-6">
                                            @if ($stakeholder['sh_subcategory'] != null)
                                                @foreach ($stakeholder['sh_subcategory'] as $x => $subcat)
                                                    <div class="flex justify-center w-24">
                                                        <x-input class="w-full text-gray-700 h-9"
                                                            style="background-color: transparent" type="text"
                                                            name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_tonnage"
                                                            wire:model.defer='stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_tonnage'
                                                            disabled>
                                                        </x-input>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="flex justify-center w-24">
                                                    <x-input class="w-full text-gray-700 h-9"
                                                        style="background-color: transparent" type="text"
                                                        name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_tonnage"
                                                        wire:model.defer='stakeholders.{{ $i }}.daily_tonnage'
                                                        disabled>
                                                    </x-input>
                                                </div>
                                            @endif
                                        </div>
                                    </td>
                                    <td
                                        class="p-3 text-center border-2 border-t-0 border-b-0 border-gray-500 whitespace-nowrap">
                                        <div class="grid grid-cols-1 space-y-6">
                                            @if ($stakeholder['sh_subcategory'] != null)
                                                @foreach ($stakeholder['sh_subcategory'] as $x => $subcat)
                                                    <div class="flex justify-center w-24">
                                                        <x-input type="text"
                                                            name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.distribution_pct"
                                                            wire:model.defer='stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.distribution_pct'>
                                                        </x-input>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="flex justify-center w-24">
                                                    <x-input type="text"
                                                        name="stakeholders.{{ $i }}.distribution_pct"
                                                        wire:model.defer='stakeholders.{{ $i }}.distribution_pct'>
                                                    </x-input>
                                                </div>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="flex">
                                            <div class="w-64 font-semibold text-center border-r-2 border-gray-500"
                                                style="margin-top: -.8%; margin-bottom:-.8%">
                                                <div class="flex justify-between gap-12 pl-4 pr-6 mt-2">
                                                    <div class="grid grid-cols-1 space-y-6 text-center">
                                                        @if ($stakeholder['sh_subcategory'] != null)
                                                            @foreach ($stakeholder['sh_subcategory'] as $x => $subcat)
                                                                <div class="flex justify-center w-24">
                                                                    <x-input type="text"
                                                                        name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.monthly_cbm_sea"
                                                                        wire:model.defer='stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.monthly_cbm_sea'
                                                                        wire:change="computeDaily">
                                                                    </x-input>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="flex justify-center w-24">
                                                                <x-input type="text"
                                                                    name="stakeholders.{{ $i }}.monthly_cbm_sea"
                                                                    wire:model.defer='stakeholders.{{ $i }}.monthly_cbm_sea'
                                                                    wire:change="computeDaily">
                                                                </x-input>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="grid grid-cols-1 space-y-6 text-center">
                                                        @if ($stakeholder['sh_subcategory'] != null)
                                                            @foreach ($stakeholder['sh_subcategory'] as $x => $subcat)
                                                                <div class="flex justify-center w-24">
                                                                    <x-input class="w-full text-gray-700 h-9"
                                                                        style="background-color: transparent"
                                                                        type="text"
                                                                        name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_cbm_sea"
                                                                        wire:model.defer='stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_cbm_sea'
                                                                        disabled>
                                                                    </x-input>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="flex justify-center w-24">
                                                                <x-input class="w-full text-gray-700 h-9"
                                                                    style="background-color: transparent"
                                                                    type="text"
                                                                    name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_cbm_sea"
                                                                    wire:model.defer='stakeholders.{{ $i }}.daily_cbm_sea'
                                                                    disabled>
                                                                </x-input>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w-64 font-semibold text-center border-r-2 border-gray-500"
                                                style="margin-top: -.8%; margin-bottom:-.8%">
                                                <div class="flex justify-between gap-12 pl-4 pr-6 mt-2">
                                                    <div class="grid grid-cols-1 space-y-6 text-center">
                                                        @if ($stakeholder['sh_subcategory'] != null)
                                                            @foreach ($stakeholder['sh_subcategory'] as $x => $subcat)
                                                                <div class="flex justify-center w-24">
                                                                    <x-input type="text"
                                                                        name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.monthly_kg_air"
                                                                        wire:model.defer='stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.monthly_kg_air'
                                                                        wire:change="computeDaily">
                                                                    </x-input>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="flex justify-center w-24">
                                                                <x-input type="text"
                                                                    name="stakeholders.{{ $i }}.monthly_kg_air"
                                                                    wire:model.defer='stakeholders.{{ $i }}.monthly_kg_air'
                                                                    wire:change="computeDaily">
                                                                </x-input>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="grid grid-cols-1 space-y-6 text-center">
                                                        @if ($stakeholder['sh_subcategory'] != null)
                                                            @foreach ($stakeholder['sh_subcategory'] as $x => $subcat)
                                                                <div class="flex justify-center w-24">
                                                                    <x-input class="w-full text-gray-700 h-9"
                                                                        style="background-color: transparent"
                                                                        type="text"
                                                                        name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_kg_air"
                                                                        wire:model.defer='stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_kg_air'
                                                                        disabled>
                                                                    </x-input>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="flex justify-center w-24">
                                                                <x-input class="w-full text-gray-700 h-9"
                                                                    style="background-color: transparent"
                                                                    type="text"
                                                                    name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_kg_air"
                                                                    wire:model.defer='stakeholders.{{ $i }}.daily_kg_air'
                                                                    disabled>
                                                                </x-input>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w-64 font-semibold text-center border-r-2 border-gray-500"
                                                style="margin-top: -.8%; margin-bottom:-.8%">
                                                <div class="flex justify-between gap-12 pl-4 pr-6 mt-2">
                                                    <div class="grid grid-cols-1 space-y-6 text-center">
                                                        @if ($stakeholder['sh_subcategory'] != null)
                                                            @foreach ($stakeholder['sh_subcategory'] as $x => $subcat)
                                                                <div class="flex justify-center w-24">
                                                                    <x-input type="text"
                                                                        name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.monthly_cbm_land"
                                                                        wire:model.defer='stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.monthly_cbm_land'
                                                                        wire:change="computeDaily">
                                                                    </x-input>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="flex justify-center w-24">
                                                                <x-input type="text"
                                                                    name="stakeholders.{{ $i }}.monthly_cbm_land"
                                                                    wire:model.defer='stakeholders.{{ $i }}.monthly_cbm_land'
                                                                    wire:change="computeDaily">
                                                                </x-input>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="grid grid-cols-1 space-y-6 text-center">
                                                        @if ($stakeholder['sh_subcategory'] != null)
                                                            @foreach ($stakeholder['sh_subcategory'] as $x => $subcat)
                                                                <div class="flex justify-center w-24">
                                                                    <x-input class="w-full text-gray-700 h-9"
                                                                        style="background-color: transparent"
                                                                        type="text"
                                                                        name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_cbm_land"
                                                                        wire:model.defer='stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_cbm_land'
                                                                        disabled>
                                                                    </x-input>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="flex justify-center w-24">
                                                                <x-input class="w-full text-gray-700 h-9"
                                                                    style="background-color: transparent"
                                                                    type="text"
                                                                    name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_cbm_land"
                                                                    wire:model.defer='stakeholders.{{ $i }}.daily_cbm_land'
                                                                    disabled>
                                                                </x-input>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w-64 font-semibold text-center">
                                                <div class="flex justify-between gap-12 pl-4 pr-6 mt-2">
                                                    <div class="grid grid-cols-1 space-y-6 text-center">
                                                        @if ($stakeholder['sh_subcategory'] != null)
                                                            @foreach ($stakeholder['sh_subcategory'] as $x => $subcat)
                                                                <div class="flex justify-center w-24">
                                                                    <x-input type="text"
                                                                        name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.monthly_kg_int"
                                                                        wire:model.defer='stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.monthly_kg_int'
                                                                        wire:change="computeDaily">
                                                                    </x-input>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="flex justify-center w-24">
                                                                <x-input type="text"
                                                                    name="stakeholders.{{ $i }}.monthly_kg_int"
                                                                    wire:model.defer='stakeholders.{{ $i }}.monthly_kg_int'
                                                                    wire:change="computeDaily">
                                                                </x-input>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="grid grid-cols-1 space-y-6 text-center">
                                                        @if ($stakeholder['sh_subcategory'] != null)
                                                            @foreach ($stakeholder['sh_subcategory'] as $x => $subcat)
                                                                <div class="flex justify-center w-24">
                                                                    <x-input class="w-full text-gray-700 h-9"
                                                                        style="background-color: transparent"
                                                                        type="text"
                                                                        name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_kg_int"
                                                                        wire:model.defer='stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_kg_int'
                                                                        disabled>
                                                                    </x-input>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="flex justify-center w-24">
                                                                <x-input class="w-full text-gray-700 h-9"
                                                                    style="background-color: transparent"
                                                                    type="text"
                                                                    name="stakeholders.{{ $i }}.sh_subcategory.{{ $x }}.daily_kg_int"
                                                                    wire:model.defer='stakeholders.{{ $i }}.daily_kg_int'
                                                                    disabled>
                                                                </x-input>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </x-slot>
                    </x-table.table>
                    <div class="px-1 pb-2">
                    </div>
                    <div class="flex justify-between mt-4 mb-2">
                        <div class="grid grid-cols-1 px-6 py-4 space-y-4 bg-white rounded-lg shadow-lg">
                            <div class="grid grid-cols-2 gap-6">
                                <div class="text-[#003399] mt-2">Total Days Count: </div>
                                <div>
                                    <input type="text"
                                        class="w-48 px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="total_days_count" wire:model.defer='total_days_count' value="25">
                                </div>
                            </div>
                            <div class="grid grid-cols-2 gap-6">
                                <div class="text-[#003399] mt-2">Monthly Total Target Amount: </div>
                                <div>
                                    <input type="text"
                                        class="w-48 px-4 border border-gray-500 rounded-md cursor-pointer h-11"
                                        name="monthly_total_target_amount"
                                        wire:model.defer='monthly_total_target_amount'>
                                </div>
                            </div>
                        </div>
                        <div class="mt-12">
                            <button type="submit"
                                class="mt-12 w-48 px-12 py-2 text-sm flex-none bg-[#003399] text-white rounded-lg">
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
