<x-form wire:init='' x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    bookref_modal: '{{ $bookref_modal }}',
    consigneedet_modal: '{{ $consigneedet_modal }}',
    bsl_modal: '{{ $bsl_modal }}',
    no_workinst_modal: '{{ $no_workinst_modal }}',
    workinst_modal: '{{ $workinst_modal }}',
    vimg_modal: '{{ $vimg_modal }}',
    vpdf_modal: '{{ $vpdf_modal }}',
    internalrem_modal: '{{ $internalrem_modal }}',
    cancel_modal: '{{ $cancel_modal }}',
    cancelconfirm_modal: '{{ $cancelconfirm_modal }}',
    reschedule_modal: '{{ $reschedule_modal }}',
    rescheduleconfirm_modal: '{{ $rescheduleconfirm_modal }}',
    approve_cancel_modal: '{{ $approve_cancel_modal }}',
    decline_cancel_modal: '{{ $decline_cancel_modal }}',
    approve_resched_modal: '{{ $approve_resched_modal }}',
    decline_resched_modal: '{{ $decline_resched_modal }}',
    {{-- redirect_modal: '{{ $redirect_modal }}', --}}


    edit_modal: '{{ $edit_modal }}',

}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        
        @can('crm_sales_booking_mgmt_add')
            @if ($create_modal)
                <x-modal id="create_modal" size="w-3/4">
                    {{-- <x-slot name="title">Book Now</x-slot> --}}
                    <x-slot name="body">
                        @livewire('crm.sales.booking-mgmt.create')
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @can('crm_sales_booking_mgmt_edit')
            @if ($book_id && $edit_modal)
                <x-modal id="edit_modal" size="w-3/4">
                    <x-slot name="body">
                        @livewire('crm.sales.booking-mgmt.edit', ['id' => $book_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        {{-- @can('crm_service_request_sr_related_mgmt_delete')
            <x-modal id="delete_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/3">
                <x-slot name="body">
                    <h2 class="mb-3 text-lg text-center text-gray-900">
                        {{ $confirmation_message }}
                    </h2>
                    <div class="flex justify-center space-x-3">
                        <button type="button" wire:click="$set('delete_modal', false)"
                            class="px-5 py-1 mt-4 text-sm font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">No</button>
                        <button type="button" wire:click="confirm"
                            class="flex-none px-5 py-1 mt-4 text-sm text-white rounded-lg bg-blue">
                            Yes</button>
                    </div>
                </x-slot>
            </x-modal>
        @endcan --}}

        @can('crm_sales_booking_mgmt_approve_cancel')
            @if ($approve_cancel_modal)
                <x-modal id="approve_cancel_modal" size="w-auto">
                    <x-slot name="body">
                        <div class="pb-4">
                            <table>
                                <tr>
                                    <td class="text-gray-400">
                                        Reason :
                                    </td>
                                    <td class="pl-2 text-xl">
                                        {{ $approve_cancel_reason }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <hr class="px-0 border">
                        <div class="flex flex-col items-center justify-center mt-4">
                            <h2 class="text-xl text-center">
                                Are you sure you want to approve this cancellation request?
                            </h2>
                            <div class="flex justify-center space-x-3">
                                <button wire:click="$set('approve_cancel_modal', false)"
                                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border
                                border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                                <button wire:click="updateApp1('{{ $book_id }}', 1)"
                                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">APPROVE</button>
                            </div>
                        </div>
                    </x-slot>
                </x-modal>
            @endif
            @if ($decline_cancel_modal)
                <x-modal id="decline_cancel_modal" size="w-auto">
                    <x-slot name="body">
                        <div class="pb-4">
                            <table>
                                <tr>
                                    <td class="text-gray-400">
                                        Reason :
                                    </td>
                                    <td class="pl-2 text-xl">
                                        {{ $decline_cancel_reason }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <hr class="px-0 border">
                        <div class="flex flex-col items-center justify-center">
                            <h2 class="text-xl text-center">
                                Are you sure you want to decline this cancellation request?
                            </h2>
                            <div class="flex justify-center space-x-3">
                                <button wire:click="$set('decline_cancel_modal', false)"
                                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                                <button wire:click="updateApp1('{{ $book_id }}', 2)"
                                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-red">DECLINE</button>
                            </div>
                        </div>
                    </x-slot>
                </x-modal>
            @endif
        @endcan

        @can('crm_sales_booking_mgmt_approve_resched')
            @if ($approve_resched_modal)
                <x-modal id="approve_resched_modal" size="w-auto">
                    <x-slot name="body">
                        <div class="pb-4">
                            <table>
                                <tr>
                                    <td class="text-gray-400">
                                        Reason :
                                    </td>
                                    <td class="pl-2 text-xl">
                                        {{ $approve_resched_reason }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <hr class="px-0 border">
                        <div class="flex flex-col items-center justify-center mt-4">
                            <h2 class="text-xl text-center">
                                Are you sure you want to approve this reschedule request?
                            </h2>
                            <div class="flex justify-center space-x-3">
                                <button wire:click="$set('approve_resched_modal', false)"
                                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                                <button wire:click="updateApp2('{{ $book_id }}', 1)"
                                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">APPROVE</button>
                            </div>
                        </div>
                    </x-slot>
                </x-modal>
            @endif
            @if ($decline_resched_modal)
                <x-modal id="decline_resched_modal" size="w-auto">
                    <x-slot name="body">
                        <div class="pb-4">
                            <table>
                                <tr>
                                    <td class="text-gray-400">
                                        Reason :
                                    </td>
                                    <td class="pl-2 text-xl">
                                        {{ $decline_resched_reason }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <hr class="px-0 border">
                        <div class="flex flex-col items-center justify-center">
                            <h2 class="text-xl text-center">
                                Are you sure you want to decline this reschedule request?
                            </h2>
                            <div class="flex justify-center space-x-3">
                                <button wire:click="$set('decline_resched_modal', false)"
                                    class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">NO</button>
                                <button wire:click="updateApp2('{{ $book_id }}', 2)"
                                    class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-red">DECLINE</button>
                            </div>
                        </div>
                    </x-slot>
                </x-modal>
            @endif
        @endcan
        @if ($bookref_modal)
            <x-modal id="bookref_modal" size="w-3/4">
                <x-slot name="body">
                    <form>
                        <div class="grid grid-cols-12 gap-2 px-6 gap-x-4 ">
                            <div class="col-span-6">
                                @if ($IDXpickupwalk == 1)
                                    <div class="flex items-center justify-start">
                                        <div wire:click=""
                                            class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                            <span>PICK UP</span>
                                        </div>
                                        <div wire:click=""
                                            class="flex justify-between px-6 py-2 text-sm text-left bg-white">
                                            <span class="">WALK IN</span>
                                        </div>
                                    </div>
                                @else
                                    <div class="flex items-center justify-start">
                                        <div wire:click="" class="flex justify-between px-6 py-2 text-sm text-left">
                                            <span>PICK UP</span>
                                        </div>
                                        <div wire:click=""
                                            class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                            <span class="">WALK IN</span>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            {{-- {{dd($IDXcbId);}} --}}
                            <div class="col-span-6">
                                <x-button type="button" wire:click="actionBSLogs({'id':{{ $IDXcbId }}},'')"
                                    {{-- {'id':{{ $vconsignee[0][$i]['id'] }}},'c_det') } --}} title="Booking Status Logs"
                                    class=" w-22 h-10 bg-blue text-white hover:bg-[#002161]" style="float: right;" />
                            </div>
                            <div class="w-11/12 col-span-6">
                                <table class="overflow-hidden">
                                    {{-- <x-slot name="tbody"> --}}
                                    <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                                        <h1 class="text-xl font-semibold text-left text-blue">Shipper Details
                                        </h1>
                                    </div>

                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Booking Reference No. :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXbookingrefno }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Booking Status :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXbookingstat }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Shipper :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXfullname }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Company :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black">
                                            {{ $IDXcompany ?? '-' }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Contact Person :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXfname }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Contact Number :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXmobile_nos }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Email Address :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXemail_address }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Customer Type :

                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXcustomertype }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Activity Type :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black ">
                                            {{ $IDXacttype }}
                                        </td>
                                    </tr>

                                    {{-- </x-slot> --}}
                                </table>
                            </div>
                            <div class="w-11/12 col-span-6 ml-12">
                                <table class="overflow-hidden">
                                    {{-- <x-slot name="tbody" class=""> --}}
                                    <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                                        <h1 class="text-xl font-semibold text-left text-blue">Pick Up Details
                                        </h1>
                                    </div>

                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Complete Address :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black">
                                            {{ $IDXaddress }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Landmark :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            -
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Timeslot :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            {{ $IDXtimeslot }}
                                        </td>
                                    </tr>
                                    {{-- </x-slot> --}}
                                </table>
                            </div>
                        </div>
                        {{-- <div class="grid grid-cols-12 gap-0 px-0 m-0 border-b-2 border-gray-400">
                    </div> --}}
                        <div class="col-span-12 px-0 mx-0 mt-2 gap-x-0">
                            <hr class="text-black border border-gray-400 ">
                        </div>
                        <div class="grid grid-cols-12 gap-2 px-6 mt-4 gap-x-4">
                            <div class="col-span-6">
                                @if ($IDXsinglemulti == 2)
                                    <div class="flex items-center justify-start">
                                        <div wire:click=""
                                            class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                            <span>SINGLE</span>
                                        </div>
                                        <div wire:click="" class="flex justify-between px-6 py-2 text-sm text-left">
                                            <span class="">MULTIPLE</span>
                                        </div>
                                    </div>
                                @else
                                    <div class="flex items-center justify-start">
                                        <div wire:click="" class="flex justify-between px-6 py-2 text-sm text-left">
                                            <span>SINGLE</span>
                                        </div>
                                        <div wire:click=""
                                            class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                            <span class="">MULTIPLE</span>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-span-6">
                            </div>
                            <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                                <h1 class="text-xl font-semibold text-left text-blue">Consignee Details
                                </h1>
                            </div>
                            <div class="col-span-12">
                                <div class="overflow-auto bg-white rounded-lg shadow-md">
                                    <x-table.table class="overflow-hidden">
                                        <x-slot name="thead">
                                            <x-table.th name="No." style="padding-left:;" />
                                            <x-table.th name="Consignee Name" style="padding-left:%;" />
                                            <x-table.th name="Company" style="padding-left;" />
                                            <x-table.th name="Contact Person" style="padding-left:;" />
                                            <x-table.th name="Contact Number" style="padding-left:;" />
                                            <x-table.th name="Email Address" style="padding-left:;" />
                                            <x-table.th name="Customer Type" style="padding-left:;" />
                                            <x-table.th name="Drop Off Location" style="padding-left:;" />
                                        </x-slot>
                                        <x-slot name="tbody" class="text-xs">
                                            @if (isset($vconsignee[0]))
                                                @foreach ($vconsignee[0] as $i => $viewcons)
                                                    <tr
                                                        class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-semibold">
                                                        <td class="p-3 whitespace-nowrap" style="padding-left:;">
                                                            {{ $i + 1 }}.
                                                        </td>
                                                        <td class="w-1/5 p-3 whitespace-nowrap"
                                                            style="padding-left:%;">
                                                            <span
                                                                wire:click="actionvcdet({'id':{{ $vconsignee[0][$i]['id'] }}},'c_det') }}"
                                                                class="underline cursor-pointer text-blue">{{ $vconsignee[0][$i]['name'] }}</span>
                                                        </td>
                                                        <td class="w-1/5 p-3 whitespace-nowrap"
                                                            style="padding-left:;">
                                                            {{ $vconsignee[0][$i]['company_name'] ?? '-' }}
                                                        </td>
                                                        <td class="w-1/5 p-3 whitespace-nowrap"
                                                            style="padding-left:;">
                                                            {{ $vconsignee[0][$i]['name'] }}
                                                        </td>
                                                        <td class="w-1/5 p-3 whitespace-nowrap"
                                                            style="padding-left:;">
                                                            {{ $vconsignee[0][$i]['mobile_number'] }}
                                                        </td>
                                                        <td class="w-1/5 p-3 whitespace-nowrap"
                                                            style="padding-left:;">
                                                            {{ $vconsignee[0][$i]['email_address'] }}
                                                        </td>
                                                        <td class="w-1/5 p-3 whitespace-nowrap"
                                                            style="padding-left:;">
                                                            {{ $vconsignee[0][$i]['account_type_id'] == 1 ? 'Individual' : 'Corporate' }}
                                                        </td>
                                                        <td class="w-1/5 p-3 whitespace-nowrap"
                                                            style="padding-left:;">
                                                            {{ $vconsignee[0][$i]['address'] }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </x-slot>
                                    </x-table.table>
                                </div>
                            </div>
                        </div>
                    </form>
                </x-slot>
            </x-modal>
        @endif
        @if ($consigneedet_modal)
            <x-modal id="consigneedet_modal" size="w-2/5">
                <x-slot name="body">
                    <form>
                        <div class="col-span-12 p-6 text-left">
                            <div class="">
                                <table class="overflow-hidden">
                                    {{-- <x-slot name="tbody"> --}}
                                    <div class="col-span-12 mb-4 border-0 border-b-2 border-blue">
                                        <h1 class="mb-1 text-xl font-semibold text-left text-blue">Cargo Details
                                        </h1>
                                    </div>

                                    <tr class="font-normal bg-white border-none">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Quantity :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            @php $qtycnt = 0; @endphp
                                            @foreach ($VBCD as $v => $vcd)
                                                @php $qtycnt += $VBCD[$v]['quantity']; @endphp
                                            @endforeach
                                            <table>
                                                <tr>
                                                    <td>
                                                        {{ $qtycnt }}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-none">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Weight (Kg) :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            @php $qtycnt2 = 0; @endphp
                                            @foreach ($VBCD as $v => $vcd)
                                                @php $qtycnt2 += $VBCD[$v]['weight']; @endphp
                                            @endforeach
                                            <table>
                                                <tr>
                                                    <td>
                                                        {{ $qtycnt2 }}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white ">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Dimensions (LxWxH in cm) :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            @foreach ($VBCD as $v => $vcd)
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{ $VBCD[$v]['length'] }} X {{ $VBCD[$v]['width'] }} X
                                                            {{ $VBCD[$v]['height'] }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Declared Value :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXdecval }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Transport Mode :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXtransportmode }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Service Mode :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXservmode }}
                                        </td>

                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Description of Goods :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXdescgoods }}
                                        </td>
                                    </tr>
                                    <tr class="text-sm font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Mode of Payment :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXmodeofp }}
                                        </td>
                                    </tr>
                                    <tr class="text-sm font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Assigned Team :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            -
                                        </td>
                                    </tr>
                                    <tr class="text-sm font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Waybill Count :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXwcount }}
                                        </td>
                                    </tr>
                                    <tr class="text-sm font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Failed Activity Reason :
                                        </td>
                                        <td class="w-1/5 text-sm font-semibold text-left text-black whitespace-nowrap">
                                            -
                                        </td>
                                    </tr>
                                    {{-- </x-slot> --}}
                                </table>
                            </div>
                        </div>
                    </form>
                </x-slot>
            </x-modal>
        @endif
        @if ($bsl_modal)
            <x-modal id="bsl_modal" size="w-1/3">
                <x-slot name="body">
                    <div class="grid grid-cols-12 p-4">
                        <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                            <h1 class="text-lg font-semibold text-left text-blue">Booking Status Logs
                            </h1>
                        </div>
                        <div class="col-span-12 mb-4">
                            <table class="overflow-hidden">
                                {{-- <x-slot name="tbody"> --}}
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-sm text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Booking Reference No. : <span
                                            class="text-lg font-semibold text-blue">{{ $bstatrefno }}</span>
                                    </td>
                                </tr>
                                {{-- </x-slot> --}}
                            </table>
                        </div>
                        <div class="col-span-12">
                            <div class="overflow-auto bg-white border rounded-lg shadow-md">
                                <x-table.table class="overflow-hidden">
                                    <x-slot name="thead">
                                        <x-table.th name="Booking Status" style="padding-left:2%;" />
                                        <x-table.th name="Date and Time" style="padding-left:%;" />

                                    </x-slot>
                                    <x-slot name="tbody">
                                        <tr
                                            class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-medium">
                                            <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:2%;">
                                                {{ $bstatstat }}
                                            </td>
                                            <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                                {{-- {{ $viewbookingdetailsconsignee[0][$i]['account_type_id'] == 1 ? 'Individual' : 'Corporate' }} --}}
                                                {{ date('m/d/Y h:i A', strtotime($bstatcreated)) }}
                                            </td>
                                        </tr>
                                    </x-slot>
                                </x-table.table>
                            </div>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endif
        @if ($no_workinst_modal)
            <x-modal id="no_workinst_modal" size="w-1/4">
                <x-slot name="body">
                    <div class="grid grid-cols-12 px-4">
                        <div class="col-span-12">
                            <h1 class="text-xl font-bold text-left">Work Instruction
                            </h1>
                        </div>
                        <div class="col-span-12 mt-4 text-center">
                            <span class="text-lg font-normal">
                                No Work Instruction.
                            </span>
                        </div>
                    </div>
                    <div class="grid grid-cols-12">
                        <div class="col-span-12 mt-4">
                            <div class="flex justify-center">
                                <button {{-- @dd($work_ins_id) --}}
                                    wire:click="actionviewedit({'id': {{ $work_ins_id }}},'viewedit') }}"
                                    class="flex justify-center w-full py-2 text-sm font-light text-white bg-blue-800 border-2 rounded-md whitespace-nowrap hover:bg-blue-900">
                                    Update Booking
                                </button>
                            </div>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endif
        @if ($workinst_modal)
            <x-modal id="workinst_modal" size="w-1/2">
                <x-slot name="body">
                    <div class="grid grid-cols-12 px-6">
                        <div class="col-span-12">
                            <h1 class="text-xl font-bold text-left">Work Instruction
                            </h1>
                        </div>
                        @if (isset($work_ins) ?? '')
                            <div class="col-span-12 mt-2">
                                <span class="text-lg font-normal leading-none">
                                    {{ $work_ins }}
                                </span>
                            </div>
                        @endif

                    </div>

                    <div class="grid grid-cols-12 mt-2">
                        @if (isset($bookworkins->BookingAttachmentHasManyBK[0]))

                            <div class="flex justify-center col-span-12 mt-4 space-x-2 bg-gray-100 border-2 border-gray-400"
                                style="margin-left: -16px; margin-right: -16px;">
                                @foreach ($bookworkins->BookingAttachmentHasManyBK as $i => $book)
                                    @if (in_array($book['extension'], config('filesystems.image_type')))
                                        <div class="w-20 h-24 cursor-pointer" {{-- @dd($book->id) --}}
                                            {{-- href="{{ Storage::disk('crm_gcs')->url($book['path'] . $book['name']) }}"target="_blank" --}}>
                                            {{-- {{ $book->id }} --}}
                                            <img class="flex w-20 h-24 border border-gray-400 rounded-md cursor-pointer"
                                                wire:click="actionvimg({'id': {{ $book->id }}},'vimg') }}"
                                                src="{{ $book->extension != 'txt' ? Storage::disk('crm_gcs')->url($book->path . $book->name) : 'https://static.wikia.nocookie.net/logopedia/images/c/c4/Notepad_Vista_10.png' }}"
                                                alt="">
                                        </div>
                                    @elseif(in_array($book['extension'], config('filesystems.file_type')))
                                        <div class="w-20 h-24 bg-white rounded-md cursor-pointer"
                                            wire:click="actionvpdf({'id': {{ $book->id }}},'vpdf') }}">
                                            <span
                                                class="flex items-center justify-center w-20 h-24 border border-gray-400 rounded-md">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20"
                                                    viewBox="0 0 14.427 19.236">
                                                    <path id="Icon_awesome-file-pdf" data-name="Icon awesome-file-pdf"
                                                        d="M6.834,9.622A3.625,3.625,0,0,1,6.759,7.86C7.074,7.86,7.044,9.246,6.834,9.622ZM6.77,11.395A17.336,17.336,0,0,1,5.7,13.751a13.834,13.834,0,0,1,2.363-.823A4.867,4.867,0,0,1,6.77,11.395ZM3.235,16.084c0,.03.5-.2,1.311-1.51A5.193,5.193,0,0,0,3.235,16.084ZM9.317,6.011h5.109V18.334a.9.9,0,0,1-.9.9H.9a.9.9,0,0,1-.9-.9V.9A.9.9,0,0,1,.9,0H8.416V5.109A.9.9,0,0,0,9.317,6.011Zm-.3,6.454a3.771,3.771,0,0,1-1.6-2.021,5.4,5.4,0,0,0,.233-2.412.941.941,0,0,0-1.8-.255,6.247,6.247,0,0,0,.3,2.893,35.279,35.279,0,0,1-1.533,3.223s0,0-.008,0c-1.018.522-2.765,1.672-2.048,2.555a1.167,1.167,0,0,0,.808.376c.672,0,1.341-.676,2.3-2.322a21.415,21.415,0,0,1,2.968-.872,5.694,5.694,0,0,0,2.4.733.973.973,0,0,0,.74-1.631c-.522-.511-2.04-.364-2.765-.271Zm5.147-8.521L10.482.263A.9.9,0,0,0,9.843,0H9.618V4.809h4.809V4.58A.9.9,0,0,0,14.164,3.945ZM11.38,13.536c.154-.1-.094-.447-1.608-.338C11.166,13.792,11.38,13.536,11.38,13.536Z"
                                                        fill="#8d8d8d" />
                                                </svg>
                                            </span>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif


                        <div class="col-span-12 mt-4">
                            <div class="flex justify-center">
                                <button wire:click="actionviewedit({'id': {{ $work_ins_id }}},'viewedit') }}"
                                    class="flex justify-center w-1/2 py-2 text-sm font-light text-white bg-blue-800 border-2 rounded-md whitespace-nowrap hover:bg-blue-900">
                                    Update Booking
                                </button>
                            </div>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endif
        @if ($vimg_modal)
            <x-modal id="vimg_modal" size="w-1/3 h-1/2">
                <x-slot name="body">
                    @if (isset($attchm[0]))
                        <div class="flex items-center justify-center cursor-pointer" style="">
                            <img class=""
                                src="{{ $attchm[0]->extension != 'txt' ? Storage::disk('crm_gcs')->url($attchm[0]->path . $attchm[0]->name) : 'https://static.wikia.nocookie.net/logopedia/images/c/c4/Notepad_Vista_10.png' }}"
                                alt="">
                        </div>
                    @endif
                </x-slot>
            </x-modal>
        @endif
        @if ($vpdf_modal)
            <x-modal id="vpdf_modal" size="">
                <x-slot name="body">
                    @if (isset($attchm[0]))
                        <div class="flex items-center justify-center bg-white cursor-pointer "
                            style="width:450px; height: 600px;">
                            <div class="grid grid-cols-2">
                                <div class="flex justify-center col-span-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="36" height="40"
                                        viewBox="0 0 14.427 19.236">
                                        <path id="Icon_awesome-file-pdf" data-name="Icon awesome-file-pdf"
                                            d="M6.834,9.622A3.625,3.625,0,0,1,6.759,7.86C7.074,7.86,7.044,9.246,6.834,9.622ZM6.77,11.395A17.336,17.336,0,0,1,5.7,13.751a13.834,13.834,0,0,1,2.363-.823A4.867,4.867,0,0,1,6.77,11.395ZM3.235,16.084c0,.03.5-.2,1.311-1.51A5.193,5.193,0,0,0,3.235,16.084ZM9.317,6.011h5.109V18.334a.9.9,0,0,1-.9.9H.9a.9.9,0,0,1-.9-.9V.9A.9.9,0,0,1,.9,0H8.416V5.109A.9.9,0,0,0,9.317,6.011Zm-.3,6.454a3.771,3.771,0,0,1-1.6-2.021,5.4,5.4,0,0,0,.233-2.412.941.941,0,0,0-1.8-.255,6.247,6.247,0,0,0,.3,2.893,35.279,35.279,0,0,1-1.533,3.223s0,0-.008,0c-1.018.522-2.765,1.672-2.048,2.555a1.167,1.167,0,0,0,.808.376c.672,0,1.341-.676,2.3-2.322a21.415,21.415,0,0,1,2.968-.872,5.694,5.694,0,0,0,2.4.733.973.973,0,0,0,.74-1.631c-.522-.511-2.04-.364-2.765-.271Zm5.147-8.521L10.482.263A.9.9,0,0,0,9.843,0H9.618V4.809h4.809V4.58A.9.9,0,0,0,14.164,3.945ZM11.38,13.536c.154-.1-.094-.447-1.608-.338C11.166,13.792,11.38,13.536,11.38,13.536Z"
                                            fill="#8d8d8d" />
                                    </svg>
                                </div>
                                <div class="flex col-span-2">
                                    <a href="{{ Storage::disk('crm_gcs')->url($attchm[0]->path . $attchm[0]->name) }}"
                                        target="_blank" class="text-blue-800 underline cursor-pointer">Open link in
                                        new tab</a>
                                </div>
                            </div>
                        </div>
                    @endif
                </x-slot>
            </x-modal>
        @endif

        @if ($internalrem_modal)
            <x-modal id="internalrem_modal" size="w-1/3">
                <x-slot name="body">
                    <div class="grid grid-cols-12 p-4">
                        <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                            <h1 class="text-xl font-semibold text-left text-blue">Internal Remarks
                            </h1>
                        </div>

                        <div class="col-span-12">
                            <x-textarea rows="8" type="text" name="workins" disabled style="padding:3%;">
                                {{ $int_rem }}</x-textarea>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endif
        @if ($cancel_modal)
            <x-modal id="cancel_modal" size="w-auto">
                <x-slot name="body">
                    <form>
                        <div class="grid grid-cols-12 p-4">
                            <div class="col-span-12 pb-2 mb-4 whitespace-nowrap">
                                <h1 class="text-lg font-normal text-left text-black">Cancel Booking Reference no. <span
                                        class="text-xl font-semibold"> {{ $brn }}</span>
                                </h1>
                            </div>
                            <div class="col-span-12">
                                <div wire:init="CancelReasonReferenceLG">
                                    @foreach ($cancelreason_referenceLG as $cancelreason_reference)
                                        <table>
                                            <tr>
                                                <td class="pb-3">
                                                    <input type="radio" wire:model='cancel_booking_idx'
                                                        value="{{ $cancelreason_reference->id }}"
                                                        name="cancel_booking_idx">
                                                </td>
                                                <td class="">
                                                    <x-label class="ml-4 text-base font-normal " style="color: black;"
                                                        for="cancel_booking_idx"
                                                        value="{{ $cancelreason_reference->name }}" />
                                                </td>
                                            </tr>
                                        </table>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div style="float: right;">
                            <x-input-error for="cancel_booking_idx" />
                            <x-button type="button"
                                wire:click="actioncancelation({'id':{{ $cancelID }}},'validatecancelation')"
                                title="Submit" class="px-8 py-1 mt-4 bg-blue text-white hover:bg-[#002161]" />
                        </div>
                    </form>
                </x-slot>
            </x-modal>
        @endif
        @if ($cancelconfirm_modal)
            <x-modal id="cancelconfirm_modal" size="w-auto">
                <x-slot name="body">
                    <span class="relative block">
                        <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                            wire:click="$set('cancelconfirm_modal', false)">
                        </span>
                    </span>
                    <h2 class="text-xl text-center">
                        Are you sure you want to Cancel This Booking ?
                    </h2>
                    <div class="flex justify-center space-x-3">
                        <button type="button" wire:click="$set('cancelconfirm_modal', false)"
                            class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                            No
                        </button>
                        <button type="button" wire:click="submitcancel({'id':{{ $cancelID }}})"
                            class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                            Yes
                        </button>
                    </div>
                </x-slot>
            </x-modal>
        @endif
        @if ($reschedule_modal)
            <x-modal id="reschedule_modal" size="w-auto">
                <x-slot name="body">
                    <form>
                        <div class="grid grid-cols-12 p-4">
                            <div class="col-span-12 pb-2 mb-4 whitespace-nowrap">
                                <h1 class="text-lg font-normal text-left text-black">Reschedule Booking Reference no.
                                    <span class="text-xl font-semibold"> {{ $brn }}</span>
                                </h1>
                            </div>
                            <div class="col-span-12">
                                <div wire:init="ReschedReasonReferenceLG">
                                    @foreach ($reschedreason_referenceLG as $reschedreason_reference)
                                        <table>
                                            <tr>
                                                <td class="pb-3">
                                                    <input type="radio" wire:model='reschedule_booking_idx'
                                                        value="{{ $reschedreason_reference->id }}"
                                                        name="reschedule_booking_idx">
                                                </td>
                                                <td class="">
                                                    <x-label class="ml-4 text-base font-normal " style="color: black;"
                                                        for="reschedule_booking_idx"
                                                        value="{{ $reschedreason_reference->name }}" />
                                                </td>
                                            </tr>
                                        </table>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div style="float: right;">
                            <x-input-error for="reschedule_booking_idx" />
                            <x-button type="button"
                                wire:click="actionresched({'id':{{ $rescheduleID }}},'validatereschedule')"
                                title="Submit" class="px-8 py-1 mt-4 bg-blue text-white hover:bg-[#002161]" />
                        </div>
                    </form>
                </x-slot>
            </x-modal>
        @endif
        @if ($rescheduleconfirm_modal)
            <x-modal id="rescheduleconfirm_modal" size="w-auto">
                <x-slot name="body">
                    <span class="relative block">
                        <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                            wire:click="$set('rescheduleconfirm_modal', false)">
                        </span>
                    </span>
                    <h2 class="text-xl text-center">
                        Are you sure you want to Reschedule This Booking ?
                    </h2>
                    <div class="flex justify-center space-x-3">
                        <button type="button" wire:click="$set('rescheduleconfirm_modal', false)"
                            class="px-8 mr-6 py-1 mt-4 text-sm font-medium text-[#003399] transition-all duration-300 border border-[#003399] rounded-lg hover:bg-gray-200">
                            No
                        </button>
                        <button type="button" wire:click="submitreschedule({'id':{{ $rescheduleID }}})"
                            class="flex-none px-8 py-1 mt-4 ml-6 text-sm text-white rounded-lg bg-blue">
                            Yes
                        </button>
                    </div>
                </x-slot>
            </x-modal>
        @endif


    </x-slot>

    <x-slot name="header_title">Booking Management</x-slot>
    <x-slot name="header_button">
        @can('crm_sales_booking_mgmt_add')
            <button wire:click="action({}, 'add')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Book Now
                </div>
            </button>
        @endcan
    </x-slot>
    <x-slot name="body">

        <div class="flex items-center justify-start">
            <div wire:click="redirectTo({}, 'redirectTobook')"
                class="flex items-center justify-between px-4 py-1 text-sm text-white border border-gray-400 cursor-pointer rounded-l-md bg-blue">
                <span>Booking Management</span>
                <span class="ml-6"></span>

            </div>
            <div wire:click="redirectTo({}, 'redirectTobookhis')"
                class="flex justify-between px-4 py-1 text-sm text-left bg-white border border-gray-400 cursor-pointer ">
                <span class="">Booking History</span>
                <span class="ml-6"></span>
            </div>

        </div>
        <div class="grid grid-cols-6 gap-4 text-sm " style="margin-top: 2%;">
            <div class="w-5/6">
                <div>
                    <x-transparent.input type="text" label="Booking Reference No" name="name"
                        wire:model.defer="booking_reference_no" />
                </div>
            </div>
            <div class="w-5/6">
                <div class="">
                    <x-transparent.input style="font-size: 14px" type="text" onfocus="(this.type='date')"
                        onblur="(this.type='text')" label="" placeholder="Booking Date" name="name"
                        wire:model.defer="created_at" />
                </div>
            </div>
            <div class="w-5/6">
                <div class="">
                    <x-transparent.input style="font-size: 14px" type="text" onfocus="(this.type='date')"
                        onblur="(this.type='text')" label="" placeholder="Pick up Date" name="name"
                        wire:model.defer="pickup_date" />
                </div>
            </div>
            <div class="w-5/6">
                <div>
                </div>
            </div>
            <div class="w-5/6">
                <div>
                </div>
            </div>
            <div class="w-5/6">
                <div>
                </div>
            </div>

            <div class="w-5/6">
                <div>
                    <x-transparent.input type="text" label="Shipper" name="name" wire:model.defer="s_name" />
                </div>
            </div>

            <div class="w-5/6">
                <div>
                    <x-transparent.input type="text" label="Consignee" name="name"
                        wire:model.defer="c_name" />
                </div>
            </div>

            <div class="w-5/6">
                <x--transparent.select value="" label="Assigned Team" name="Booking Reference No"
                    wire:model.defer=''>
                    <option value=""></option>
                </x--transparent.select>
            </div>

            <div class="w-5/6" wire:init="FinalStatusReferenceBK">
                <x-transparent.select value="{{ $final_status_id }}" label="Booking Status" name="final_status_id"
                    wire:model.defer="final_status_id">
                    <option value=""></option>
                    @foreach ($final_referencesbk as $final_reference)
                        <option value="{{ $final_reference->id }}">
                            {{ $final_reference->name }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div>

            <div class="w-5/6" wire:init="BookingBranchReferenceBK">
                <x-transparent.select value="{{ $booking_branch_id }}" label="Branch" name="booking_branch_id"
                    wire:model.defer="booking_branch_id">
                    <option value=""></option>
                    @foreach ($bookingbranch_referencesbk as $bookingbranch_reference)
                        <option value="{{ $bookingbranch_reference->id }}">
                            {{ $bookingbranch_reference->code }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div>

            <div class="">
                <x-button type="button" wire:click="search" title="Search"
                    class="px-3 py-1.5 text-white bg-blue hover:bg-blue-800" />
            </div>
        </div>

        <div class="grid grid-cols-12 gap-4">
            <div class="col-span-4 p-4 bg-white rounded-lg shadow-md">
                <div class="grid grid-cols-12">
                    <div class="col-span-12 ml-3">
                        <h2 class="text-lg">Booking Channel</h2>
                    </div>
                </div>

                <div class="col-span-12">
                    @foreach ($bookingc_header_cards as $index => $cards)
                        @if ($cards['id'] == 3)
                            <button wire:click="$set('{{ $cards['action'] }}', {{ $cards['id'] }})"
                                :class="$wire.{{ $cards['action'] }} == '{{ $cards['id'] }}' ? 'border-2 border-blue' : ''"
                                class="flex-none px-3 py-3 mt-4 ml-3 text-black bg-white border-2 border-gray-200 rounded-lg shadow-md hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700"
                                style="width: 44.5%">
                                <div class="grid grid-cols-3">
                                    <div class="col-span-2">
                                        <p
                                            class="w-auto text-xs font-normal text-left text-gray-400 whitespace-normal">
                                            {{ $cards['title'] }}
                                        </p>
                                        <h5
                                            class="mt-2 text-2xl font-medium tracking-tight text-left text-black dark:text-white">
                                            {{ $cards['value'] }}
                                        </h5>
                                    </div>
                                    <div class="col-span-1">
                                        <h5>{!! $cards['icon'] ?? '' !!}</h5>
                                    </div>
                                </div>
                            </button>
                        @else
                            <button wire:click="$set('{{ $cards['action'] }}', {{ $cards['id'] }})"
                                :class="$wire.{{ $cards['action'] }} == '{{ $cards['id'] }}' ? 'border-2 border-blue' : ''"
                                class="flex-none px-3 py-3 mt-4 ml-3 text-black bg-white border-2 border-gray-200 rounded-lg shadow-md hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700"
                                style="width: 44.5%">
                                <div class="grid grid-cols-3">
                                    <div class="col-span-2">
                                        @if ($cards['id'] == 2 || $cards['id'] == false)
                                            <p
                                                class="w-auto text-sm font-normal text-left text-gray-400 whitespace-nowrap">
                                                {{ $cards['title'] }}
                                            </p>
                                        @else
                                            <p class="w-auto text-sm font-normal text-left text-gray-400">
                                                {{ $cards['title'] }}
                                            </p>
                                        @endif
                                        <h5
                                            class="mt-6 text-2xl font-medium tracking-tight text-left text-black dark:text-white">
                                            {{ $cards['value'] }}
                                        </h5>
                                    </div>
                                    <div class="col-span-1">
                                        <h5>{!! $cards['icon'] ?? '' !!}</h5>
                                    </div>
                                </div>
                            </button>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="col-span-8 p-6 bg-white rounded-lg shadow-md">
                <div class="grid grid-cols-12">
                    <div class="col-span-4 ml-3">
                        <h2 class="text-base">Booking Status</h2>
                    </div>
                    <div class="col-span-3">
                        <h2 class="text-sm whitespace-nowrap">Activity Completion Rate:</h2>
                    </div>
                    <div class="col-span-4">
                        <div class="flex gap-2">
                            <div class="w-full border border-[#7F7F7F] rounded-lg h-[15px] mb-6">
                                <div class="bg-[#389AEB] h-4 rounded-lg"
                                    style="width: {{ $total_booking <= 0 ? 0 : round(($total_pickup / $total_booking) * 100, 2) }}%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-12">
                    <div class="col-span-4 ml-3">
                        <h2 class="text-lg"></h2>
                    </div>
                    <div class="col-span-3">
                        <h2 class="text-base"></h2>
                    </div>
                    <div class="col-span-4">
                        <div class="flex gap-2">
                            <h2 class="-mt-4 text-base">{{ $total_pickup }}/{{ $total_booking }}</h2>
                            <h4 class="-mt-4 font-extrabold text-blue" style="padding-left: 70%;">
                                {{ $total_booking <= 0 ? 0 : round(($total_pickup / $total_booking) * 100, 2) }}%</h4>
                        </div>
                    </div>
                </div>

                <div class="col-span-12">
                    @foreach ($header_cards as $index => $card)
                        @if ($card['id'] == 2 || $card['id'] == 9 || $card['id'] == 10)
                            <button wire:click="$set('{{ $card['action'] }}', {{ $card['id'] }})"
                                :class="$wire.{{ $card['action'] }} == '{{ $card['id'] }}' ? 'border-2 border-blue' : ''"
                                class="flex-none px-3 py-3 mt-4 ml-3 bg-white border-2 border-gray-200 rounded-lg shadow-md hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700"
                                style="width: 22.5%;">
                                <div class="grid grid-cols-3">
                                    <div class="col-span-2">
                                        @if ($card['id'] == 2)
                                            <p
                                                class="w-auto text-xs font-normal text-left text-gray-400 whitespace-nowrap">
                                                {{ $card['title'] }}
                                            </p>
                                        @else
                                            <p class="w-auto text-xs font-normal text-left text-gray-400">
                                                {{ $card['title'] }}
                                            </p>
                                        @endif
                                        @if ($card['id'] == 2)
                                            <h5
                                                class="mt-4 mb-2 text-2xl font-medium tracking-tight text-left text-black dark:text-white">
                                                {{ $card['value'] }}
                                            </h5>
                                        @else
                                            <h5
                                                class="mt-2 mb-2 text-2xl font-medium tracking-tight text-left text-black dark:text-white">
                                                {{ $card['value'] }}
                                            </h5>
                                        @endif

                                    </div>
                                    <div class="col-span-1">
                                        <h5>{!! $card['icon'] ?? '' !!}</h5>
                                    </div>
                                </div>
                            </button>
                        @else
                            <button wire:click="$set('{{ $card['action'] }}', {{ $card['id'] }})"
                                :class="$wire.{{ $card['action'] }} == '{{ $card['id'] }}' ? 'border-2 border-blue' : ''"
                                class="flex-none px-3 py-3 mt-4 ml-3 bg-white border-2 border-gray-200 rounded-lg shadow-md hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700"
                                style="width: 22.5%;">
                                <div class="grid grid-cols-3">
                                    <div class="col-span-2">
                                        <p
                                            class="w-auto text-sm font-normal text-left text-gray-400 whitespace-nowrap">
                                            {{ $card['title'] }}
                                        </p>
                                        <h5
                                            class="mt-4 mb-2 text-2xl font-medium tracking-tight text-left text-black dark:text-white">
                                            {{ $card['value'] }}
                                        </h5>
                                    </div>
                                    <div class="col-span-1">
                                        <h5>{!! $card['icon'] ?? '' !!}</h5>
                                    </div>
                                </div>
                            </button>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>

        <div class="py-12">
            <div class="flex items-center justify-start">
                <ul class="flex mt-8">
                    @foreach ($status_header_cards as $i => $status_card)
                        {{-- <li class="px-3 py-1 text-sm {{ $status_header_cards[$i]['class'] }}"
                            :class="$wire.{{ $status_header_cards[$i]['action'] }} ==
                                '{{ $status_header_cards[$i]['id'] }}' ?
                                'bg-blue text-white' : ''"> --}}
                        <li
                            class="px-3 py-1 text-xs {{ $status_header_cards[$i]['class'] }} {{ $stats == $status_header_cards[$i]['id'] ? 'bg-blue text-white' : '' }}">
                            <button
                                wire:click="$set('{{ $status_header_cards[$i]['action'] }}', {{ $status_header_cards[$i]['id'] }})">
                                {{ $status_header_cards[$i]['title'] }}
                                <span class="ml-6 text-sm">{{ $status_header_cards[$i]['value'] }}</span>
                            </button>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="px-2 bg-white rounded-lg shadow-md">
                <x-table.table class="overflow-auto">

                    @foreach ($book_s as $i => $book)
                        {{-- @dd($book) --}}

                        @foreach ($status_header_cards as $i => $status_card)
                            <x-slot name="thead">
                                <x-table.th name="No." style="padding-left:%;" />
                                <x-table.th name="Booking Status" style="padding-left:%;" />
                                <x-table.th name="Booking Reference No" style="padding-left:%;" />
                                <x-table.th name="Pick up Date" style="padding-left:%;" />
                                <x-table.th name="Shipper" style="padding-left:%;" />
                                <x-table.th name="Customer Type" style="padding-left:%;" />
                                <x-table.th name="Work instruction" style="padding-left:%;" />
                                <x-table.th name="Description of Goods" style="padding-left:%;" />
                                <x-table.th name="Booked By" style="padding-left:%;" />
                                <x-table.th name="Ageing" style="padding-left:%;" />
                                <x-table.th name="Source / Marketing" style="padding-left:%;" />
                                <x-table.th name="Booking Type" style="padding-left:%;" />
                                <x-table.th name="Branch" style="padding-left:%;" />
                                <x-table.th name="Assigned Team" style="padding-left:%;" />
                                <x-table.th name="Waybill Count" style="padding-left:%;" />
                                <x-table.th name="Internal Remarks" style="padding-left:%;" />
                                @if ($stats == 6)
                                    <x-table.th name="Date Rescheduled" style="padding-left:%;" />
                                    <x-table.th name="Reson for Reschedule" style="padding-left:%;" />
                                    <x-table.th name="Requested By" style="padding-left:%;" />
                                    <x-table.th name="Approved By" style="padding-left:%;" />
                                @endif
                                @if ($stats == 7)
                                    <x-table.th name="Date Cancellation" style="padding-left:%;" />
                                    <x-table.th name="Reson for Cancellation" style="padding-left:%;" />
                                    <x-table.th name="Requested By" style="padding-left:%;" />
                                    <x-table.th name="Approved By" style="padding-left:%;" />
                                @endif
                                @if ($stats == '')
                                    <x-table.th name="Request For" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="Action" style="padding-left:;" />
                                @endif
                                <tr>
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="Channel" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    @if ($stats == 6)
                                        <x-table.th name="" style="padding-left:%;" />
                                        <x-table.th name="" style="padding-left:%;" />
                                        <x-table.th name="" style="padding-left:%;" />
                                        <x-table.th name="" style="padding-left:%;" />
                                    @endif
                                    @if ($stats == 7)
                                        <x-table.th name="" style="padding-left:%;" />
                                        <x-table.th name="" style="padding-left:%;" />
                                        <x-table.th name="" style="padding-left:%;" />
                                        <x-table.th name="" style="padding-left:%;" />
                                    @endif
                                    @if ($stats == '')
                                        <x-table.th name="Reschedule"
                                            style="padding-left:%; background-color: #00000029;" />
                                        <x-table.th name="Concellation"
                                            style="padding-left:%; background-color: #EFEFEF;" />
                                        <x-table.th name="" style="padding-left:%;" />
                                    @endif
                                </tr>
                            </x-slot>
                        @endforeach
                    @endforeach
                    @foreach ($status_header_cards as $s => $status_card)
                        <x-slot name="tbody">
                            @foreach ($book_s as $i => $book)
                                @php
                                    $dateres = '';
                                    $reasonres = '';
                                    $reqbyres = '';
                                    $appbyres = '';

                                    $datecanc = '';
                                    $reasoncanc = '';
                                    $reqbycanc = '';
                                    $appbycanc = '';
                                @endphp
                                @foreach ($book->BookingLogsHasManyBK as $res)
                                    @if ($res->status_id == 6)
                                        @php $dateres = $res->approve_date  @endphp
                                        @php $reasonres = $res->ReschedReasonReferenceLG->name  @endphp
                                        @php $appbyres = $res->ApproverLogsReferenceLG->name  @endphp
                                    @endif
                                    @if ($res->status_id == 10 && $res->approve_user_id == null)
                                        @php $reqbyres = $res->UserLogsReferenceLG->name  @endphp
                                    @endif

                                    @if ($res->status_id == 7)
                                        @php $datecanc = $res->approve_date  @endphp
                                        @php $reasoncanc = $res->CancelReasonReferenceLG->name  @endphp
                                        @php $appbycanc = $res->ApproverLogsReferenceLG->name  @endphp
                                    @endif
                                    @if ($res->status_id == 9 && $res->approve_user_id == null)
                                        @php $reqbycanc = $res->UserLogsReferenceLG->name  @endphp
                                    @endif
                                @endforeach
                                <tr class=" border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-semibold">
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        <p class="w-1/4">
                                            {{-- {{ $i + 1}}. --}}
                                            {{ ($book_s->currentPage() - 1) * $book_s->links()->paginator->perPage() + $loop->iteration }}.
                                        </p>
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        <span
                                            style="{{ $book->final_status_id == 1
                                                ? 'color:#A775FF;'
                                                : ($book->final_status_id == 2
                                                    ? 'color:#A2C1FF;'
                                                    : ($book->final_status_id == 3
                                                        ? 'color:#4D88FF;'
                                                        : ($book->final_status_id == 4
                                                            ? 'color:#FF8800;'
                                                            : ($book->final_status_id == 5
                                                                ? 'color:#003399;'
                                                                : ($book->final_status_id == 6
                                                                    ? 'color:#FF7560;'
                                                                    : ($book->final_status_id == 7
                                                                        ? 'color:#FF0000;'
                                                                        : ($book->final_status_id == 8
                                                                            ? 'color:#32CD32;'
                                                                            : ($book->final_status_id == 9
                                                                                ? 'color:#FFD04F;'
                                                                                : ($book->final_status_id == 10
                                                                                    ? 'color:#FFD04F;'
                                                                                    : ''))))))))) }} 
                                                text-xs rounded-full p-1">
                                            {{ $book->FinalStatusReferenceBK->name ?? '' }}
                                        </span>
                                        {{-- {{ $book->FinalStatusReferenceBK->name }} --}}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        <span wire:click="actionbookingref({'id':{{ $book->id }}},'bref') }}"
                                            class="underline cursor-pointer text-blue">{{ $book->booking_reference_no }}
                                        </span>
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ date('m/d/Y', strtotime($book->pickup_date)) }}
                                    </td>
                                    <td class="flex p-3 text-left w-44" style="padding-left:; width: ">
                                        {{ $book->BookingShipper->name }}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $book->BookingShipper->account_type_id == 1 ? 'Individual' : 'Corporate' }}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        @if (isset($book->attachments[0]['booking_id']) || $book->work_instruction != null)
                                            <span
                                                wire:click="actionviewwork({'id':{{ $book->id }}},'workins') }}"
                                                class="underline cursor-pointer text-blue">View</span>
                                        @else
                                            <span
                                                wire:click="actionviewwork({'id':{{ $book->id }}},'no_workins') }}"
                                                class="underline cursor-pointer text-blue">View</span>
                                        @endif
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{-- {{dd($book->BookingConsigneeHasManyBK[$i][$i]['description_goods']);}} --}}
                                        {{ ucwords($book->BookingConsigneeHasManyBK[0]['description_goods']) }}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{-- @dd($days) --}}
                                        @if (isset($book->CreatedByBK->name))
                                            {{ $book->CreatedByBK->name }}
                                        @endif
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ date_diff(date_create($book->pickup_date), date_create(date('Y-m-d')))->format('%a Day/s') }}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $book->BookingChannelReferenceBK->name }}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $book->booking_category == 1 ? 'Pick up' : 'Walk In' }}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $book->BookingBranchReferenceBK->code }}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        NONE
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        NONE
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        <span
                                            wire:click="actionviewinternrem({'id':{{ $book->id }}},'intrem') }}"
                                            class="underline cursor-pointer text-blue">View</span>
                                    </td>
                                    @if ($stats == 6)
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ date('m/d/Y h:i A', strtotime($dateres)) }}
                                        </td>
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ $reasonres }}
                                        </td>
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ $reqbyres }}
                                        </td>
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ $appbyres }}
                                        </td>
                                    @endif
                                    @if ($stats == 7)
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ date('m/d/Y h:i A', strtotime($datecanc)) }}
                                        </td>
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ $reasoncanc }}
                                        </td>
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ $reqbycanc }}
                                        </td>
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ $appbycanc }}
                                        </td>
                                    @endif
                                    @if ($stats == '')
                                        <td class="w-1/4 p-3 border-l-2 border-r-2 whitespace-nowrap"
                                            style="padding-left:;">
                                            @if ($book->final_status_id == 10 && $book->approval_status == null)
                                                <div class="flex gap-2 ml-5">
                                                    @can('crm_sales_booking_mgmt_approve_resched')
                                                        <div class="">
                                                            <svg wire:click="action({'id':{{ $book->id }}, 'approval_status': 1},'update_app2') }}"
                                                                class="w-6 h-5 text-blue" aria-hidden="true"
                                                                focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                                role="img" xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 448 512">
                                                                <path fill="currentColor"
                                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                                                                </path>
                                                            </svg>
                                                        </div>
                                                        <div class="" style="">
                                                            <svg wire:click="action({'id':{{ $book->id }}, 'approval_status': 2},'update_app2') }}"
                                                                class="w-6 h-5 text-red" aria-hidden="true"
                                                                focusable="false" data-prefix="fas"
                                                                data-icon="circle-minus" role="img"
                                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                                <path fill="currentColor"
                                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM175 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z">
                                                                </path>
                                                            </svg>
                                                        </div>
                                                    @endcan
                                                </div>
                                            @endif

                                            @if ($book->final_status_id == 6)
                                                <span
                                                    class="{{ $book->approval_status == 1 ? 'text-blue bg-blue-100 px-5' : '' }} 
                                                    text-xs rounded-full p-1">
                                                    {{ $book->approval_status == 1 ? 'Approved' : '' }}</span>
                                            @endif
                                            @if ($book->approval_status == 2 && $book->final_status_id == 10)
                                                <span
                                                    class="{{ $book->approval_status == 2 ? 'text-red bg-red-100 px-6' : '' }} 
                                                    text-xs rounded-full p-1">
                                                    {{ $book->approval_status == 2 ? 'Declined' : '' }}</span>
                                            @endif
                                        </td>
                                        <td class="p-3 border-r-2 whitespace-nowrap" style="padding-left:;">
                                            @if ($book->final_status_id == 9 && $book->approval_status == null)
                                                <div class="flex gap-2 ml-5">
                                                    @can('crm_sales_booking_mgmt_approve_cancel')
                                                        <div class="">
                                                            <svg wire:click="action({'id':{{ $book->id }}, 'approval_status': 1},'update_app1') }}"
                                                                class="w-6 h-5 text-blue" aria-hidden="true"
                                                                focusable="false" data-prefix="fas" data-icon="trash-alt"
                                                                role="img" xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 0 448 512">
                                                                <path fill="currentColor"
                                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z">
                                                                </path>
                                                            </svg>
                                                        </div>
                                                        <div class="" style="">
                                                            <svg wire:click="action({'id':{{ $book->id }}, 'approval_status': 2},'update_app1') }}"
                                                                class="w-6 h-5 text-red" aria-hidden="true"
                                                                focusable="false" data-prefix="fas"
                                                                data-icon="circle-minus" role="img"
                                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                                <path fill="currentColor"
                                                                    d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM175 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z">
                                                                </path>
                                                            </svg>
                                                        </div>
                                                    @endcan
                                                </div>
                                            @endif

                                            @if ($book->final_status_id == 7)
                                                <span
                                                    class="{{ $book->approval_status == 1 ? 'text-blue bg-blue-100 px-5' : '' }} 
                                                    text-xs rounded-full p-1">
                                                    {{ $book->approval_status == 1 ? 'Approved' : '' }}</span>
                                            @endif
                                            @if ($book->approval_status == 2 && $book->final_status_id == 9)
                                                <span
                                                    class="{{ $book->approval_status == 2 ? 'text-red bg-red-100 px-6' : '' }} 
                                                    text-xs rounded-full p-1">
                                                    {{ $book->approval_status == 2 ? 'Declined' : '' }}</span>
                                            @endif
                                        </td>


                                        <td class="relative p-3 whitespace-nowrap"
                                            style="padding-left:-1%;"x-data="{ open: false }">
                                            <svg @click="open = !open"
                                                class="w-5 h-5 cursor-pointer text-grey hover:text-blue-800 "
                                                data-prefix="far" data-icon="approve" role="img"
                                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 512">
                                                <path fill="currentColor"
                                                    d="M64 360C94.93 360 120 385.1 120 416C120 446.9 94.93 472 64 472C33.07 472 8 446.9 8 416C8 385.1 33.07 360 64 360zM64 200C94.93 200 120 225.1 120 256C120 286.9 94.93 312 64 312C33.07 312 8 286.9 8 256C8 225.1 33.07 200 64 200zM64 152C33.07 152 8 126.9 8 96C8 65.07 33.07 40 64 40C94.93 40 120 65.07 120 96C120 126.9 94.93 152 64 152z">
                                                </path>
                                            </svg>
                                            <div class="absolute" style=" margin-left: -11rem; margin-top:-50%">
                                                <ul class="text-gray-600 bg-white rounded shadow" x-show="open"
                                                    @click.away="open = false">
                                                    <li class="px-3 py-1 hover:bg-indigo-100 "
                                                        x-data="{ open: false }">
                                                        @can('crm_sales_booking_mgmt_edit')
                                                            <a
                                                                wire:click="action({'id': {{ $book->id }}},'edit') }}">Edit</a>
                                                        @endcan
                                                    </li>
                                                    <li class="px-3 py-1 hover:bg-indigo-100 "
                                                        x-data="{ open: false }">
                                                        <a 
                                                        {{-- wire:click="action({'id': {{ $book->id }}},'redirect') }}" --}}
                                                            target="_blank"
                                                            href="{{ route('oims.order-management.transaction-entry.create', ['id' => $book->id]) }}"
                                                            type="button" value="click">
                                                            Redirect to Data
                                                            Entry</a>
                                                    </li>
                                                    <li class="px-3 py-1 hover:bg-indigo-100 "
                                                        x-data="{ open: false }">
                                                        <a wire:click="action({'id': },'view') }}">View</a>
                                                    </li>
                                                    <li class="px-3 py-1 hover:bg-indigo-100 "
                                                        x-data="{ open: false }">
                                                        <a wire:click="action({'id': },'view') }}">Delete</a>
                                                    </li>
                                                    <li class="px-3 py-1 hover:bg-indigo-100 "
                                                        x-data="{ open: false }">
                                                        <a
                                                            wire:click="actionCancelBk({'id':{{ $book->id }}},'cancelbk') }}">Cancel</a>
                                                    </li>
                                                    <li class="px-3 py-1 hover:bg-indigo-100 "
                                                        x-data="{ open: false }">
                                                        <a
                                                            wire:click="actionrescheduleBk({'id':{{ $book->id }}},'reschedbk') }}">Reschedule</a>
                                                    </li>
                                                    {{-- <li class="px-3 py-1 hover:bg-indigo-100 " x-data="{ open: false }">
                                                        @can('crm_commercials_ancillary_mgmt_deactivate')
                                                            <a x-cloak x-show="'{{ $ancillary_charges_display->status == 1 }}'"
                                                                wire:click="action({'id':{{ $ancillary_charges_display->id }}, 'status': 2},'deactivate') }}">Deactivate</a>
                                                            <a x-cloak x-show="'{{ $ancillary_charges_display->status == 2 }}'"
                                                                wire:click="action({'id':{{ $ancillary_charges_display->id }}, 'status': 1},'deactivate') }}">Reactivate</a>
                                                        @endcan
                                                    </li> --}}
                                                </ul>
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </x-slot>
                    @endforeach
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $book_s->links() }}
                </div>
            </div>


        </div>

    </x-slot>
</x-form>
