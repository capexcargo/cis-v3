<x-form wire:init="load" x-data="{
    search_form: false,
    create_modal: '{{ $create_modal }}',
    bookref_modal: '{{ $bookref_modal }}',
    consigneedet_modal: '{{ $consigneedet_modal }}',
    bsl_modal: '{{ $bsl_modal }}',
    workinst_modal: '{{ $workinst_modal }}',
    internalrem_modal: '{{ $internalrem_modal }}',
    cancel_modal: '{{ $cancel_modal }}',
    cancelconfirm_modal: '{{ $cancelconfirm_modal }}',
    reschedule_modal: '{{ $reschedule_modal }}',
    rescheduleconfirm_modal: '{{ $rescheduleconfirm_modal }}',
    approve_cancel_modal: '{{ $approve_cancel_modal }}',
    decline_cancel_modal: '{{ $decline_cancel_modal }}',
    approve_resched_modal: '{{ $approve_resched_modal }}',
    decline_resched_modal: '{{ $decline_resched_modal }}',

    edit_modal: '{{ $edit_modal }}',

}">
    <x-slot name="loading">
        <x-loading />
    </x-slot>

    <x-slot name="modals">
        @can('crm_sales_booking_mgmt_add')
            <x-modal id="create_modal" size="w-3/4">
                {{-- <x-slot name="title">Book Now</x-slot> --}}
                <x-slot name="body">
                    @livewire('crm.sales.booking-mgmt.create')
                </x-slot>
            </x-modal>
        @endcan
        @can('crm_sales_booking_mgmt_edit')
            @if ($book_id && $edit_modal)
                <x-modal id="edit_modal" size="w-3/4">
                    <x-slot name="body">
                        @livewire('crm.sales.booking-mgmt.edit', ['id' => $book_id])
                    </x-slot>
                </x-modal>
            @endif
        @endcan
       
        @can('crm_sales_booking_mgmt_approve_cancel')
            <x-modal id="approve_cancel_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-blue">Approve</span>
                            this Request?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('approve_cancel_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateApp1('{{ $book_id }}', 1)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#003399] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="decline_cancel_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-red">Decline</span>
                            this Request?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('decline_cancel_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateApp1('{{ $book_id }}', 2)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#CC0000] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan

        @can('crm_sales_booking_mgmt_approve_resched')
            <x-modal id="approve_resched_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-blue">Approve</span>
                            this Request?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('approve_resched_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateApp2('{{ $book_id }}', 1)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#003399] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
            <x-modal id="decline_resched_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-3/12">
                <x-slot name="body">
                    <div class="flex flex-col items-center justify-center space-y-3">
                        <p class="text-sm font-semibold">Are you sure you want to <span class="text-red">Decline</span>
                            this Request?</p>
                        <div class="space-x-3">
                            <button wire:click="$set('decline_resched_modal', false)"
                                class="px-5 py-1 shadow-md text-sm text-blue border border-[#003399] rounded-md">Cancel</button>
                            <button wire:click="updateApp2('{{ $book_id }}', 2)"
                                class="px-9 py-1 shadow-md text-sm flex-none bg-[#CC0000] text-white rounded-md">Ok</button>
                        </div>
                    </div>
                </x-slot>
            </x-modal>
        @endcan

        <x-modal id="bookref_modal" size="w-3/4">
            <x-slot name="body">
                <form>
                    <div class="grid grid-cols-12 gap-8 p-6">
                        <div class="col-span-6 mt-6">
                            @if ($IDXpickupwalk == 1)
                                <div class="flex items-center justify-start">
                                    <div wire:click=""
                                        class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                        <span>PICK UP</span>
                                    </div>
                                    <div wire:click=""
                                        class="flex justify-between px-6 py-2 text-sm text-left bg-white border border-gray-400 cursor-pointer ">
                                        <span class="">WALK IN</span>
                                    </div>
                                </div>
                            @else
                                <div class="flex items-center justify-start">
                                    <div wire:click=""
                                        class="flex justify-between px-6 py-2 text-sm text-left bg-white border border-gray-400 cursor-pointer ">
                                        <span>PICK UP</span>
                                    </div>
                                    <div wire:click=""
                                        class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                        <span class="">WALK IN</span>
                                    </div>
                                </div>
                            @endif
                        </div>
                        {{-- {{dd($IDXcbId);}} --}}
                        <div class="col-span-6">
                            <x-button type="button" wire:click="actionBSLogs({'id':{{ $IDXcbId }}},'')"
                                {{-- {'id':{{ $vconsignee[0][$i]['id'] }}},'c_det') } --}} title="Booking Status Logs"
                                class=" w-22 h-10 mt-4 bg-blue text-white hover:bg-[#002161]" style="float: right;" />
                        </div>
                        <div class="col-span-6">
                            <x-table.table>
                                <x-slot name="tbody">
                                    <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                                        <h1 class="text-xl font-semibold text-left text-blue">Shipper Details
                                        </h1>
                                    </div>

                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Booking Reference No. :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            {{ $IDXbookingrefno }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Booking Status :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            {{ $IDXbookingstat }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Shipper :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            {{ $IDXfullname }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Company :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            {{ $IDXcompany }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Contact Person :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            {{ $IDXfname }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Contact Number :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            {{ $IDXmobile_nos }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Email Address :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            {{ $IDXemail_address }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Customer Type :

                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            {{ $IDXcustomertype }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Activity Type :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            {{ $IDXacttype }}
                                        </td>
                                    </tr>

                                </x-slot>
                            </x-table.table>
                        </div>
                        <div class="col-span-6">
                            <x-table.table>
                                <x-slot name="tbody">
                                    <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                                        <h1 class="text-xl font-semibold text-left text-blue">Pick Up Details
                                        </h1>
                                    </div>

                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Complete Address :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            {{ $IDXaddress }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Landmark :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            None
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Timeslot :
                                        </td>
                                        <td class="w-1/5 text-sm font-medium text-left text-black whitespace-nowrap">
                                            {{ $IDXtimeslot }}
                                        </td>
                                    </tr>
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                    <div class="grid grid-cols-12 gap-0 m-0 border-b-2 border-gray-400">
                    </div>
                    <div class="grid grid-cols-12 gap-8 p-6">
                        <div class="col-span-6 mt-6">
                            @if ($IDXsinglemulti == 2)
                                <div class="flex items-center justify-start">
                                    <div wire:click=""
                                        class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                        <span>SINGLE</span>
                                    </div>
                                    <div wire:click=""
                                        class="flex justify-between px-6 py-2 text-sm text-left bg-white border border-gray-400 cursor-pointer ">
                                        <span class="">MULTIPLE</span>
                                    </div>
                                </div>
                            @else
                                <div class="flex items-center justify-start">
                                    <div wire:click=""
                                        class="flex justify-between px-6 py-2 text-sm text-left bg-white border border-gray-400 cursor-pointer ">
                                        <span>SINGLE</span>
                                    </div>
                                    <div wire:click=""
                                        class="flex items-center justify-between px-6 py-2 text-sm text-white border border-gray-400 cursor-pointer bg-blue">
                                        <span class="">MULTIPLE</span>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="col-span-6">
                        </div>
                        <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                            <h1 class="text-xl font-semibold text-left text-blue">Consignee Details
                            </h1>
                        </div>
                        <div class="col-span-12">
                            <div class="overflow-auto bg-white rounded-lg shadow-md">
                                <x-table.table>
                                    <x-slot name="thead">
                                        <x-table.th name="No." style="padding-left:;" />
                                        <x-table.th name="Consignee Name" style="padding-left:%;" />
                                        <x-table.th name="Company" style="padding-left;" />
                                        <x-table.th name="Contact Person" style="padding-left:;" />
                                        <x-table.th name="Contact Number" style="padding-left:;" />
                                        <x-table.th name="Email Address" style="padding-left:;" />
                                        <x-table.th name="Customer Type" style="padding-left:;" />
                                        <x-table.th name="Drop Off Location" style="padding-left:;" />
                                    </x-slot>
                                    <x-slot name="tbody">
                                        @if (isset($vconsignee[0]))
                                            @foreach ($vconsignee[0] as $i => $viewcons)
                                                <tr
                                                    class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-semibold">
                                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                                        <p class="w-1/4">
                                                            {{ $i + 1 }}.
                                                        </p>
                                                    </td>
                                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:%;">
                                                        <span
                                                            wire:click="actionvcdet({'id':{{ $vconsignee[0][$i]['id'] }}},'c_det') }}"
                                                            class="underline cursor-pointer text-blue">{{ $vconsignee[0][$i]['name'] }}</span>
                                                    </td>
                                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                                        {{ $vconsignee[0][$i]['customer_no'] }}
                                                    </td>
                                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                                        {{ $vconsignee[0][$i]['name'] }}
                                                    </td>
                                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                                        {{ $vconsignee[0][$i]['mobile_number'] }}
                                                    </td>
                                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                                        {{ $vconsignee[0][$i]['email_address'] }}
                                                    </td>
                                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                                        {{ $vconsignee[0][$i]['account_type_id'] == 1 ? 'Individual' : 'Corporate' }}
                                                    </td>
                                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                                        {{ $vconsignee[0][$i]['address'] }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </x-slot>
                                </x-table.table>
                            </div>
                        </div>
                    </div>
                </form>
            </x-slot>
        </x-modal>

        <x-modal id="consigneedet_modal" size="w-2/5">
            <x-slot name="body">
                <form>
                    <div class="col-span-12 p-6 text-left">
                        <div class="">
                            <x-table.table>
                                <x-slot name="tbody">

                                    <div class="col-span-12 mb-4 border-0 border-b-2 border-blue">
                                        <h1 class="mb-1 text-xl font-semibold text-left text-blue">Cargo Details
                                        </h1>
                                    </div>

                                    <tr class="font-normal bg-white border-none">
                                        <td class="w-1/5 text-base text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Quantity :
                                        </td>
                                        <td
                                            class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                            @php $qtycnt = 0; @endphp
                                            @foreach ($VBCD as $v => $vcd)
                                                @php $qtycnt += $VBCD[$v]['quantity']; @endphp
                                            @endforeach
                                            <table>
                                                <tr>
                                                    <td>
                                                        {{ $qtycnt }}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-none">
                                        <td class="w-1/5 text-base text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Weight (Kg) :
                                        </td>
                                        <td
                                            class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                            @php $qtycnt2 = 0; @endphp
                                            @foreach ($VBCD as $v => $vcd)
                                                @php $qtycnt2 += $VBCD[$v]['weight']; @endphp
                                            @endforeach
                                            <table>
                                                <tr>
                                                    <td>
                                                        {{ $qtycnt2 }}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-t-2 border-black ">
                                        <td class="w-1/5 text-base text-gray-400 whitespace-nowrap"
                                            style="padding-left:;">
                                            Dimensions (LxWxH in cm) :
                                        </td>
                                        <td
                                            class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                            @foreach ($VBCD as $v => $vcd)
                                                <table>
                                                    <tr>
                                                        <td>
                                                            {{ $VBCD[$v]['length'] }} X {{ $VBCD[$v]['width'] }} X
                                                            {{ $VBCD[$v]['height'] }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Declared Value :
                                        </td>
                                        <td
                                            class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXdecval }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Transport Mode :
                                        </td>
                                        <td
                                            class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXtransportmode }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Service Mode :
                                        </td>
                                        <td
                                            class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXservmode }}
                                        </td>

                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Description of Goods :
                                        </td>
                                        <td
                                            class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXdescgoods }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Mode of Payment :
                                        </td>
                                        <td
                                            class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXmodeofp }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Assigned Team :
                                        </td>
                                        <td
                                            class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                            -
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Waybill Count :
                                        </td>
                                        <td
                                            class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                            {{ $IDXwcount }}
                                        </td>
                                    </tr>
                                    <tr class="font-normal bg-white border-0">
                                        <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                            Failed Activity Reason :
                                        </td>
                                        <td
                                            class="w-1/5 text-base font-semibold text-left text-black whitespace-nowrap">
                                            -
                                        </td>
                                    </tr>
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                </form>
            </x-slot>
        </x-modal>

        <x-modal id="bsl_modal" size="w-1/3">
            <x-slot name="body">
                <div class="grid grid-cols-12 p-4">
                    <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                        <h1 class="text-xl font-semibold text-left text-blue">Booking Status Logs
                        </h1>
                    </div>
                    <div class="col-span-12 mb-4">
                        <x-table.table>
                            <x-slot name="tbody">
                                <tr class="font-normal bg-white border-0">
                                    <td class="w-1/5 text-gray-400 whitespace-nowrap" style="padding-left:;">
                                        Booking Reference No. : <span
                                            class="text-xl font-semibold text-blue">{{ $bstatrefno }}</span>
                                    </td>
                                </tr>
                            </x-slot>
                        </x-table.table>
                    </div>
                    <div class="col-span-12">
                        <div class="overflow-auto bg-white border rounded-lg shadow-md">
                            <x-table.table>
                                <x-slot name="thead">
                                    <x-table.th name="Booking Status" style="padding-left:2%;" />
                                    <x-table.th name="Date and Time" style="padding-left:%;" />

                                </x-slot>
                                <x-slot name="tbody">
                                    <tr
                                        class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-semibold">
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:2%;">
                                            {{ $bstatstat }}
                                        </td>
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{-- {{ $viewbookingdetailsconsignee[0][$i]['account_type_id'] == 1 ? 'Individual' : 'Corporate' }} --}}
                                            {{ date('m/d/Y h:i A', strtotime($bstatcreated)) }}
                                        </td>
                                    </tr>
                                </x-slot>
                            </x-table.table>
                        </div>
                    </div>
                </div>
            </x-slot>
        </x-modal>

        <x-modal id="workinst_modal" size="w-1/3">
            <x-slot name="body">
                <div class="grid grid-cols-12 p-4">
                    <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                        <h1 class="text-xl font-semibold text-left text-blue">Work Instruction
                        </h1>
                    </div>

                    <div class="col-span-12">
                        <x-textarea rows="8" type="text" name="workins" disabled style="padding:3%;">
                            {{ $work_ins }}</x-textarea>
                    </div>
                </div>
            </x-slot>
        </x-modal>

        <x-modal id="internalrem_modal" size="w-1/3">
            <x-slot name="body">
                <div class="grid grid-cols-12 p-4">
                    <div class="col-span-12 pb-2 mb-4 border-b-2 border-blue">
                        <h1 class="text-xl font-semibold text-left text-blue">Internal Remarks
                        </h1>
                    </div>

                    <div class="col-span-12">
                        <x-textarea rows="8" type="text" name="workins" disabled style="padding:3%;">
                            {{ $int_rem }}</x-textarea>
                    </div>
                </div>
            </x-slot>
        </x-modal>

        <x-modal id="cancel_modal" size="w-1/3">
            <x-slot name="body">
                <form>
                    <div class="grid grid-cols-12 p-4">
                        <div class="col-span-12 pb-2 mb-4">
                            <h1 class="text-lg font-normal text-left text-black">Cancel Booking Reference no. <span
                                    class="text-xl font-semibold"> {{ $brn }}</span>
                            </h1>
                        </div>
                        <div class="col-span-12">
                            <div wire:init="CancelReasonReferenceLG">
                                @foreach ($cancelreason_referenceLG as $cancelreason_reference)
                                    <table>
                                        <tr>
                                            <td class="pb-3">
                                                <input type="radio" wire:model='cancel_booking_idx'
                                                    value="{{ $cancelreason_reference->id }}"
                                                    name="cancel_booking_idx">
                                            </td>
                                            <td class="">
                                                <x-label class="ml-4 text-base font-normal " style="color: black;"
                                                    for="cancel_booking_idx"
                                                    value="{{ $cancelreason_reference->name }}" />
                                            </td>
                                        </tr>
                                    </table>
                                @endforeach
                            </div>
                        </div>

                    </div>
                    <div style="float: right;">
                        <x-input-error for="cancel_booking_idx" />
                        <x-button type="button"
                            wire:click="actioncancelation({'id':{{ $cancelID }}},'validatecancelation')"
                            title="Submit" class="w-32 mt-4 bg-blue text-white hover:bg-[#002161]" />
                    </div>
                </form>
            </x-slot>
        </x-modal>

        <x-modal id="cancelconfirm_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                <span class="relative block">
                    <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                        wire:click="$set('cancelconfirm_modal', false)">
                    </span>
                </span>
                <h2 class="mb-3 text-xl font-bold text-left text-blue">
                    Are you sure you want to Cancel This Booking ?
                </h2>
                <div class="flex justify-end mt-6 space-x-3">
                    <button type="button" wire:click="$set('cancelconfirm_modal', false)"
                        class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <button type="button" wire:click="submitcancel({'id':{{ $cancelID }}})"
                        class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                        Submit
                    </button>
                </div>
            </x-slot>
        </x-modal>

        <x-modal id="reschedule_modal" size="w-1/3">
            <x-slot name="body">
                <form>
                    <div class="grid grid-cols-12 p-4">
                        <div class="col-span-12 pb-2 mb-4">
                            <h1 class="text-lg font-normal text-left text-black">Reschedule Booking Reference no. <span
                                    class="text-xl font-semibold"> {{ $brn }}</span>
                            </h1>
                        </div>
                        <div class="col-span-12">
                            <div wire:init="ReschedReasonReferenceLG">
                                @foreach ($reschedreason_referenceLG as $reschedreason_reference)
                                    <table>
                                        <tr>
                                            <td class="pb-3">
                                                <input type="radio" wire:model='reschedule_booking_idx'
                                                    value="{{ $reschedreason_reference->id }}"
                                                    name="reschedule_booking_idx">
                                            </td>
                                            <td class="">
                                                <x-label class="ml-4 text-base font-normal " style="color: black;"
                                                    for="reschedule_booking_idx"
                                                    value="{{ $reschedreason_reference->name }}" />
                                            </td>
                                        </tr>
                                    </table>
                                @endforeach
                            </div>
                        </div>

                    </div>
                    <div style="float: right;">
                        <x-input-error for="reschedule_booking_idx" />
                        <x-button type="button"
                            wire:click="actionresched({'id':{{ $rescheduleID }}},'validatereschedule')"
                            title="Submit" class="w-32 mt-4 bg-blue text-white hover:bg-[#002161]" />
                    </div>
                </form>
            </x-slot>
        </x-modal>

        <x-modal id="rescheduleconfirm_modal" size="w-11/12 xs:w-11/12 sm:w-11/12 md:w-1/4">
            <x-slot name="body">
                <span class="relative block">
                    <span class="absolute inset-y-0 right-0 flex items-center -mt-4 -mr-3 cursor-pointer"
                        wire:click="$set('rescheduleconfirm_modal', false)">
                    </span>
                </span>
                <h2 class="mb-3 text-xl font-bold text-left text-blue">
                    Are you sure you want to Reschedule This Booking ?
                </h2>
                <div class="flex justify-end mt-6 space-x-3">
                    <button type="button" wire:click="$set('rescheduleconfirm_modal', false)"
                        class="px-12 py-2 text-xs font-medium text-red-400 transition-all duration-300 border border-red-400 rounded-lg hover:text-white hover:bg-red-400">
                        Cancel
                    </button>
                    <button type="button" wire:click="submitreschedule({'id':{{ $rescheduleID }}})"
                        class="px-12 py-2 text-xs flex-none bg-[#003399] text-white rounded-lg">
                        Submit
                    </button>
                </div>
            </x-slot>
        </x-modal>


    </x-slot>

    <x-slot name="header_title">Booking Management</x-slot>
    <x-slot name="header_button">
        @can('crm_sales_booking_mgmt_add')
            <button wire:click="action({}, 'add')" class="p-2 px-3 mr-3 text-sm text-white rounded-md bg-blue">
                <div class="flex items-start justify-between">
                    <svg class="w-3 h-3 mt-1 mr-1" aria-hidden="true" focusable="false" data-prefix="far"
                        data-icon="print-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor"
                            d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z" />
                    </svg>
                    Book Now
                </div>
            </button>
        @endcan
    </x-slot>
    <x-slot name="body">

        <div class="flex items-center justify-start">
            <div wire:click="redirectTo({}, 'redirectTobook')"
                class="flex justify-between px-4 py-1 text-sm text-left bg-white border border-gray-400 cursor-pointer ">
                <span>Booking Management</span>
                <span class="ml-6"></span>

            </div>
            <div wire:click="redirectTo({}, 'redirectTobookhis')"
                class="flex items-center justify-between px-4 py-1 text-sm text-white border border-gray-400 cursor-pointer rounded-l-md bg-blue">
                <span class="">Booking History</span>
                <span class="ml-6"></span>
            </div>

        </div>
        <div class="grid grid-cols-6 gap-4 text-sm " style="margin-top: 2%;">
            {{-- <div class="w-5/6">
                <div>
                    <x-transparent.input type="date" label="Booking Date" name="name"
                        wire:model.defer="created_at" />
                </div> 
            </div> --}}
            <div class="w-5/6">
                <div>
                    <x-transparent.input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" label="" placeholder="Booking Date" name="name"
                        wire:model.defer="created_at" />
                </div>
            </div>
            {{-- <div class="w-5/6">
                <div>
                    <x-transparent.input type="date" label="Pick up Date" name="name"
                        wire:model.defer="pickup_date" />
                </div>
            </div> --}}
            <div class="w-5/6">
                <div class="">
                    <x-transparent.input type="text" onfocus="(this.type='date')" onblur="(this.type='text')" label="" placeholder="Pick up Date" name="name"
                        wire:model.defer="pickup_date" />
                </div>
            </div>

            <div class="w-5/6" wire:init="FinalStatusReferenceBK">
                <x-transparent.select value="{{ $final_status_id }}" label="Booking Status" name="final_status_id"
                    wire:model.defer="final_status_id">
                    <option value=""></option>
                    @foreach ($final_referencesbk as $final_reference)
                        <option value="{{ $final_reference->id }}">
                            {{ $final_reference->name }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div>

            <div class="w-5/6" wire:init="BookingBranchReferenceBK">
                <x-transparent.select value="{{ $booking_branch_id }}" label="Branch" name="booking_branch_id"
                    wire:model.defer="booking_branch_id">
                    <option value=""></option>
                    @foreach ($bookingbranch_referencesbk as $bookingbranch_reference)
                        <option value="{{ $bookingbranch_reference->id }}">
                            {{ $bookingbranch_reference->code }}
                        </option>
                    @endforeach
                </x-transparent.select>
            </div>

            <div class="">
                <x-button type="button" wire:click="search" title="Search"
                    class="px-3 py-1.5 text-white bg-blue hover:bg-blue-800" />
            </div>
        </div>

        <div class="grid grid-cols-12 gap-4">
            <div class="col-span-4 p-6 bg-white rounded-lg shadow-md">
                <div class="grid grid-cols-12">
                    <div class="col-span-12 ml-3">
                        <h2 class="text-lg">Booking Channel</h2>
                    </div>
                </div>

                <div class="col-span-12">
                    @foreach ($bookingc_header_cards as $index => $cards)
                        <button wire:click="$set('{{ $cards['action'] }}', {{ $cards['id'] }})"
                            :class="$wire.{{ $cards['action'] }} == '{{ $cards['id'] }}' ? 'border-2 border-blue' : ''"
                            {{-- :class="$wire.{{ $cards['action'] }} == '{{ $cards['title'] }}' ? 'border-2 border-blue' : ''" --}}
                            class="flex-none px-3 py-3 mt-4 ml-3 text-black bg-white border-2 border-gray-200 rounded-lg shadow-md hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700" style="width: 44.5%">
                            <div class="grid grid-cols-3">
                                <div class="col-span-2">
                                    <p class="w-auto font-normal text-left text-gray-400">{{ $cards['title'] }}
                                    </p>
                                    <h5
                                        class="mt-2 mb-2 text-2xl font-medium tracking-tight text-left text-black dark:text-white">
                                        {{ $cards['value'] }}
                                    </h5>
                                </div>
                                <div class="col-span-1">
                                    <h5>{!! $cards['icon'] ?? '' !!}</h5>
                                </div>
                            </div>
                        </button>
                    @endforeach
                </div>
            </div>
            <div class="col-span-8 p-6 bg-white rounded-lg shadow-md">
                <div class="grid grid-cols-12">
                    <div class="col-span-4 ml-3">
                        <h2 class="text-lg">Booking Status</h2>
                    </div>
                    <div class="col-span-3">
                        <h2 class="text-sm whitespace-nowrap">Activity Completion Rate:</h2>
                    </div>
                    <div class="col-span-4">
                        <div class="flex gap-2">
                            <div class="w-full border border-[#7F7F7F] rounded-lg h-[15px] mb-6">
                                <div class="bg-[#389AEB] h-4 rounded-lg"
                                    style="width: {{ $total_booking <= 0 ? 0 : round(($total_pickup / $total_booking) * 100, 2) }}%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid grid-cols-12">
                    <div class="col-span-4 ml-3">
                        <h2 class="text-lg"></h2>
                    </div>
                    <div class="col-span-3">
                        <h2 class="text-base"></h2>
                    </div>
                    <div class="col-span-4">
                        <div class="flex gap-2">
                            <h2 class="-mt-4 text-base">{{ $total_pickup }}/{{ $total_booking }}</h2>
                            <h4 class="-mt-4 font-extrabold text-blue" style="padding-left: 70%;">
                                {{ $total_booking <= 0 ? 0 : round(($total_pickup / $total_booking) * 100, 2) }}%</h4>
                        </div>
                    </div>
                </div>

                <div class="col-span-12">
                    @foreach ($header_cards as $index => $card)
                        <button wire:click="$set('{{ $card['action'] }}', {{ $card['id'] }})"
                            :class="$wire.{{ $card['action'] }} == '{{ $card['id'] }}' ? 'border-2 border-blue' : ''"
                            class="flex-none px-3 py-3 mt-4 ml-3 bg-white border-2 border-gray-200 rounded-lg shadow-md hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700" style="width: 22.5%;">
                            <div class="grid grid-cols-3">
                                <div class="col-span-2">
                                    <p class="w-auto font-normal text-left text-gray-400">{{ $card['title'] }}
                                    </p>
                                    <h5
                                        class="mt-2 mb-2 text-2xl font-medium tracking-tight text-left text-black dark:text-white">
                                        {{ $card['value'] }}
                                    </h5>
                                </div>
                                <div class="col-span-1">
                                    <h5>{!! $card['icon'] ?? '' !!}</h5>
                                </div>
                            </div>
                        </button>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="py-12">
            <div class="px-2 bg-white rounded-lg shadow-md">
                <x-table.table>
                    @foreach ($book_s2 as $i => $book)
                        @foreach ($status_header_cards as $i => $status_card)
                            <x-slot name="thead">
                                <x-table.th name="No." style="padding-left:%;" />
                                <x-table.th name="Booking Status" style="padding-left:%;" />
                                <x-table.th name="Booking Reference No" style="padding-left:%;" />
                                <x-table.th name="Pick up Date" style="padding-left:%;" />
                                <x-table.th name="Shipper" style="padding-left:%;" />
                                <x-table.th name="Customer Type" style="padding-left:%;" />
                                <x-table.th name="Work instruction" style="padding-left:%;" />
                                <x-table.th name="Description of Goods" style="padding-left:%;" />
                                <x-table.th name="Booked By" style="padding-left:%;" />
                                <x-table.th name="Ageing" style="padding-left:%;" />
                                <x-table.th name="Source / Marketing" style="padding-left:%;" />
                                <x-table.th name="Branch" style="padding-left:%;" />
                                <x-table.th name="Assigned Team" style="padding-left:%;" />
                                <x-table.th name="Waybill Count" style="padding-left:%;" />
                                <x-table.th name="Internal Remarks" style="padding-left:%;" />
                                @if ($stats == 6)
                                    <x-table.th name="Date Rescheduled" style="padding-left:%;" />
                                    <x-table.th name="Reson for Reschedule" style="padding-left:%;" />
                                    <x-table.th name="Requested By" style="padding-left:%;" />
                                    <x-table.th name="Approved By" style="padding-left:%;" />
                                @endif
                                @if ($stats == 7)
                                    <x-table.th name="Date Cancellation" style="padding-left:%;" />
                                    <x-table.th name="Reson for Cancellation" style="padding-left:%;" />
                                    <x-table.th name="Requested By" style="padding-left:%;" />
                                    <x-table.th name="Approved By" style="padding-left:%;" />
                                @endif
                                <tr>
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="Channel" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    <x-table.th name="" style="padding-left:%;" />
                                    @if ($stats == 6)
                                        <x-table.th name="" style="padding-left:%;" />
                                        <x-table.th name="" style="padding-left:%;" />
                                        <x-table.th name="" style="padding-left:%;" />
                                        <x-table.th name="" style="padding-left:%;" />
                                    @endif
                                    @if ($stats == 7)
                                        <x-table.th name="" style="padding-left:%;" />
                                        <x-table.th name="" style="padding-left:%;" />
                                        <x-table.th name="" style="padding-left:%;" />
                                        <x-table.th name="" style="padding-left:%;" />
                                    @endif
                                </tr>
                            </x-slot>
                        @endforeach
                    @endforeach
                    @foreach ($status_header_cards as $s => $status_card)
                        <x-slot name="tbody">
                            @foreach ($book_s2 as $i => $book)
                                @php
                                    $dateres = '';
                                    $reasonres = '';
                                    $reqbyres = '';
                                    $appbyres = '';
                                    
                                    $datecanc = '';
                                    $reasoncanc = '';
                                    $reqbycanc = '';
                                    $appbycanc = '';
                                @endphp
                                @foreach ($book->BookingLogsHasManyBK as $res)
                                    @if ($res->status_id == 6)
                                        @php $dateres = $res->approve_date  @endphp
                                        @php $reasonres = $res->ReschedReasonReferenceLG->name  @endphp
                                        @php $appbyres = $res->ApproverLogsReferenceLG->name  @endphp
                                    @endif
                                    @if ($res->status_id == 10 && $res->approve_user_id == null)
                                        @php $reqbyres = $res->UserLogsReferenceLG->name  @endphp
                                    @endif

                                    @if ($res->status_id == 7)
                                        @php $datecanc = $res->approve_date  @endphp
                                        @php $reasoncanc = $res->CancelReasonReferenceLG->name  @endphp
                                        @php $appbycanc = $res->ApproverLogsReferenceLG->name  @endphp
                                    @endif
                                    @if ($res->status_id == 9 && $res->approve_user_id == null)
                                        @php $reqbycanc = $res->UserLogsReferenceLG->name  @endphp
                                    @endif
                                @endforeach
                                <tr class="border-0 cursor-pointer hover:text-white hover:bg-[#4068b8] font-semibold">
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        <p class="w-1/4">
                                            {{ ($book_s2->currentPage() - 1) * $book_s2->links()->paginator->perPage() + $loop->iteration }}.
                                        </p>
                                    </td>
                                    
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        <span
                                            style="{{ $book->final_status_id == 1
                                                ? 'color:#A775FF;'
                                                : ($book->final_status_id == 2
                                                    ? 'color:#A2C1FF;'
                                                    : ($book->final_status_id == 3
                                                        ? 'color:#4D88FF;'
                                                        : ($book->final_status_id == 4
                                                            ? 'color:#FF8800;'
                                                            : ($book->final_status_id == 5
                                                                ? 'color:#003399;'
                                                                : ($book->final_status_id == 6
                                                                    ? 'color:#FF7560;'
                                                                    : ($book->final_status_id == 7
                                                                        ? 'color:#FF0000;'
                                                                        : ($book->final_status_id == 8
                                                                            ? 'color:#32CD32;'
                                                                            : ($book->final_status_id == 9
                                                                                ? 'color:#FFD04F;'
                                                                                : ($book->final_status_id == 10
                                                                                    ? 'color:#FFD04F;'
                                                                                    : ''))))))))) }} 
                                                text-xs rounded-full p-1">
                                            {{ $book->FinalStatusReferenceBK->name ?? '' }}
                                        </span>
                                        {{-- {{ $book->FinalStatusReferenceBK->name }} --}}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        <span wire:click="actionbookingref({'id':{{ $book->id }}},'bref') }}"
                                            class="underline cursor-pointer text-blue">{{ $book->booking_reference_no }}
                                        </span>
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ date('m/d/Y', strtotime($book->pickup_date)) }}
                                    </td>
                                    <td class="w-1/4 p-3 text-center whitespace-nowrap" style="padding-left:;">
                                        {{ $book->BookingShipper->name }}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $book->BookingShipper->account_type_id == 1 ? 'Individual' : 'Corporate' }}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        <span wire:click="actionviewwork({'id':{{ $book->id }}},'workins') }}"
                                            class="underline cursor-pointer text-blue">View</span>
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{-- {{dd($book->BookingConsigneeHasManyBK[$i][$i]['description_goods']);}} --}}
                                        {{ $book->BookingConsigneeHasManyBK[0]['description_goods'] }}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $book->CreatedByBK->name }}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        NONE
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $book->BookingChannelReferenceBK->name }}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        {{ $book->BookingBranchReferenceBK->code }}
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        NONE
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        NONE
                                    </td>
                                    <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                        <span
                                            wire:click="actionviewinternrem({'id':{{ $book->id }}},'intrem') }}"
                                            class="underline cursor-pointer text-blue">View</span>
                                    </td>
                                    @if ($stats == 6)
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ date('m/d/Y h:i A', strtotime($dateres)) }}
                                        </td>
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ $reasonres }}
                                        </td>
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ $reqbyres }}
                                        </td>
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ $appbyres }}
                                        </td>
                                    @endif
                                    @if ($stats == 7)
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ date('m/d/Y h:i A', strtotime($datecanc)) }}
                                        </td>
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ $reasoncanc }}
                                        </td>
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ $reqbycanc }}
                                        </td>
                                        <td class="w-1/4 p-3 whitespace-nowrap" style="padding-left:;">
                                            {{ $appbycanc }}
                                        </td>
                                    @endif
                                    
                            @endforeach
                        </x-slot>
                    @endforeach
                </x-table.table>
                <div class="px-1 pb-2">
                    {{ $book_s2->links() }}
                </div>
            </div>


        </div>

    </x-slot>
</x-form>
